@org.hibernate.annotations.NamedQueries({

		@org.hibernate.annotations.NamedQuery(name = "getUsers", query = "SELECT u FROM User u "
				+ "WHERE u.username = ? AND u.implementationId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUserWithOutImplementation", query = "SELECT u FROM User u "
				//+ "LEFT JOIN FETCH u.person p "
				//+ "LEFT JOIN FETCH u.implementation imp "
				+ " WHERE u.username = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUserByAdminFlag", query = "SELECT Distinct(u) FROM User u "
				+ "LEFT JOIN FETCH u.userRoles urs "
				+ "LEFT JOIN FETCH urs.role role "
				+ " WHERE u.implementationId = ? AND role.isAdmin=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllUsers", query = "SELECT u FROM User u "
				//+ "LEFT JOIN FETCH u.person p " 
				+ " WHERE u.implementationId = ? and u.enabled=1"),

		@org.hibernate.annotations.NamedQuery(name = "getActiveUserByPersonId", query = "SELECT u FROM User u "
				//+ "LEFT JOIN FETCH u.person p " 
				+ " WHERE u.personId = ? and u.enabled=1"),

		@org.hibernate.annotations.NamedQuery(name = "getAllRoles", query = "SELECT r FROM Role r "
				+ "WHERE r.implementation = ? AND r.isActive = 1"),

		@org.hibernate.annotations.NamedQuery(name = "getRolesByPersons", query = "SELECT Distinct(r) FROM Role r "
				+ " LEFT JOIN FETCH r.userRoles urs"
				+ " LEFT JOIN FETCH urs.user u"
				+ " WHERE u.personId IN (:personIds) "),

		@org.hibernate.annotations.NamedQuery(name = "getAllReferences", query = "SELECT r FROM Reference r "
				+ "WHERE r.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getReferenceAgainstUsecase", query = "SELECT r FROM Reference r "
				+ "WHERE r.usecase = ? AND r.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAuthorities", query = "SELECT a FROM Authorities a "
				+ "WHERE a.username = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getCountryStates", query = "SELECT s FROM State s "
				+ "WHERE s.countryId = ? ORDER BY s.stateCode"),

		@org.hibernate.annotations.NamedQuery(name = "getStateCities", query = "SELECT c FROM City c JOIN c.state s "
				+ "WHERE s.stateId = ? ORDER BY c.cityCode"),

		@org.hibernate.annotations.NamedQuery(name = "getUserRoles", query = "SELECT ur FROM UserRole ur "
				+ "JOIN FETCH ur.role r " + "JOIN FETCH ur.user u "
				//+ "JOIN FETCH u.person p "
				+ "WHERE u.username = ? and ur.isActive=1 "),
				
		@org.hibernate.annotations.NamedQuery(name = "getUserRoleByRoleId", query = "SELECT DISTINCT(ur) FROM UserRole ur "
				+ " JOIN FETCH ur.role r" 
				+ " JOIN FETCH ur.user u" 
				+ " WHERE ur.isActive=1 "
				+ " AND r.roleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUserRole", query = "SELECT ur FROM UserRole ur "
				+ "JOIN FETCH ur.role r " + "JOIN FETCH ur.user u "
				//+ "JOIN FETCH u.person p "
				+ "WHERE u.username = ? and r.roleId=? and ur.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getAllUserRole", query = "SELECT ur FROM UserRole ur "
				+ "JOIN FETCH ur.role r " + "JOIN FETCH ur.user u "
				//+ "JOIN FETCH u.person p "
				+ "WHERE u.implementationId = ? and ur.isActive=1 "),

		@org.hibernate.annotations.NamedQuery(name = "getParentMenuItems", query = "SELECT m FROM Menu m "
				+ "LEFT JOIN m.menu parent "
				+ "LEFT JOIN FETCH m.screen s "
				+ "WHERE s.isActive = 1 AND parent.menuItemId IS NULL ORDER BY m.itemOrder"),

		@org.hibernate.annotations.NamedQuery(name = "getChildMenuItems", query = "SELECT m FROM Menu m "
				+ "JOIN m.menu parent "
				+ "LEFT JOIN FETCH m.screen s "
				+ "WHERE s.isActive = 1 AND parent.menuItemId = ? ORDER BY m.itemOrder"),

		@org.hibernate.annotations.NamedQuery(name = "getRoleMenuRights", query = "SELECT mr FROM MenuRight mr "
				+ "JOIN FETCH mr.menu m "
				+ "JOIN mr.role r "
				+ "WHERE r.roleId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getImagesByRecordId", query = "SELECT img FROM Image img "
				+ "WHERE img.recordId = ? AND img.tableName = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getRecordIdBasedImageListByImplementation", query = "SELECT img FROM Image img "
				+ "WHERE img.recordId = ? AND img.tableName = ? AND img.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getImagesByImplementation", query = "SELECT Distinct(img) FROM Image img "
				+ "LEFT JOIN FETCH img.implementation imp "
				+ "WHERE imp.implementationId = ? ORDER BY img.imageId DESC"),

		@org.hibernate.annotations.NamedQuery(name = "getImageListByImageIds", query = "SELECT Distinct(img) FROM Image img "
				+ "LEFT JOIN FETCH img.implementation imp "
				+ "WHERE img.imageId IN (?)"),

		@org.hibernate.annotations.NamedQuery(name = "getEntityBasedImageListByImplementation", query = "SELECT Distinct(img) FROM Image img "
				+ "LEFT JOIN FETCH img.implementation imp "
				+ "WHERE imp = ? AND img.tableName = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getImagesByFileHash", query = "SELECT img FROM Image img "
				+ "WHERE img.hashedName=?"),

		@org.hibernate.annotations.NamedQuery(name = "getDocumentWithDirectory", query = "SELECT d FROM Document d "
				+ "JOIN FETCH d.directory dir WHERE d.documentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getChildDocumentsByRecordId", query = "SELECT d FROM Document d "
				+ "WHERE d.recordId = ? AND d.tableName = ? AND d.document IS NOT NULL "),

		@org.hibernate.annotations.NamedQuery(name = "getParentDocumentsByRecordId", query = "SELECT d FROM Document d "
				+ "WHERE d.recordId = ? AND d.tableName = ? AND d.document IS NULL "),

		@org.hibernate.annotations.NamedQuery(name = "getDocumentsByRecordId", query = "SELECT d FROM Document d "
				+ "WHERE d.recordId = ? AND d.tableName = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getDocumentsByFileHash", query = "SELECT d FROM Document d "
				+ "WHERE d.hashedName = ?"),

		@org.hibernate.annotations.NamedQuery(name = "findByDirName", query = "SELECT d FROM Directory d WHERE d.directoryName = ? "
				+ " AND d.recordId = ? AND d.tableName = ? AND d.displayPane = ?   "),

		@org.hibernate.annotations.NamedQuery(name = "getDirsNDocs", query = "SELECT d FROM Directory d LEFT JOIN FETCH d.documents doc "
				+ " WHERE"
				+ " d.recordId = ? AND d.tableName = ? AND d.displayPane = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getDirsNDocsById", query = "SELECT d FROM Directory d LEFT JOIN FETCH d.documents doc "
				+ " LEFT JOIN FETCH d.directories  	"
				+ " WHERE d.directoryId = ? AND d.recordId = ? AND d.tableName = ? AND d.displayPane = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSubDirs", query = "SELECT d FROM Directory d LEFT JOIN FETCH d.documents doc "
				+ " WHERE d.directory.directoryId = ? AND d.recordId = ? AND d.tableName = ? "
				+ " AND d.displayPane = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getSubDocs", query = "SELECT d FROM Document d   where d.documentId = d.document.documentId "
				+ " AND  d.documentId = ? AND d.recordId = ? AND d.tableName = ? AND d.displayPane = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getParentDir", query = "SELECT d FROM Directory d   "
				+ " WHERE d.directory.directoryId = ? AND d.recordId = ? AND d.tableName = ? AND d.displayPane = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getParentDirs", query = "SELECT d FROM Directory d   "
				+ " WHERE d.directory.directoryId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getChildDirs", query = "SELECT d FROM Directory d   "
				+ " WHERE d.directoryId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getUseCaseComment", query = "SELECT c FROM Comment c   "
				+ "  WHERE c.recordId = ? and c.useCase = ? "), /*JOIN FETCH c.person*/

		@org.hibernate.annotations.NamedQuery(name = "getEntityComment", query = "SELECT c FROM Comment c   "
				+ "  WHERE c.useCase = ? "), /*JOIN FETCH c.person*/

		@org.hibernate.annotations.NamedQuery(name = "getAlertsRoleBased", query = "SELECT a FROM Alert a   "
				+ "  WHERE a.roleId = ? AND a.isActive = 1"), /*JOIN FETCH a.screen s*/

		@org.hibernate.annotations.NamedQuery(name = "getAlertRecordBased", query = "SELECT a FROM Alert a   "
				+ "  WHERE a.recordId = ? AND a.tableName = ?"), /*JOIN FETCH a.screen s*/

		@org.hibernate.annotations.NamedQuery(name = "getAlertAllByUseCase", query = "SELECT a FROM Alert a   "
			+ "  WHERE a.tableName = ?"), /*JOIN FETCH a.screen s*/
					
		@org.hibernate.annotations.NamedQuery(name = "findDublicateAlerts", query = "SELECT a FROM Alert a   "
				+ "  WHERE a.recordId = ? AND a.tableName = ? AND a.screenId=? AND a.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getScreenBasedOnScreenName", query = "SELECT s FROM Screen s   "
				+ " WHERE s.screenName = ? "),

		/*@org.hibernate.annotations.NamedQuery(name = "getMessageToAvoidDublicate", query = "SELECT m FROM Message m   "
				+ " WHERE m.recordId = ? and m.personId=? and m.dataSpecific=? and m.processType=?"), and m.useCase = ?*/

		/*@org.hibernate.annotations.NamedQuery(name = "getMessageToAvoidSameMessage", query = "SELECT m FROM Message m   "
				+ " WHERE  m.dataSpecific=? "), m.useCase = ? AND*/

		@org.hibernate.annotations.NamedQuery(name = "getI18NMessages", query = "SELECT i FROM I18NMessages i "
				+ " WHERE i.i18nLocale = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getLookupInformation", query = "SELECT Distinct(lp) FROM LookupMaster lp   "
				+ " LEFT JOIN FETCH lp.lookupDetails lds	"
				+ " LEFT JOIN FETCH lp.lookupMaster lpp	"
				+ " WHERE lp.accessCode = ? and lp.implementation=? and lp.isActive=1 order by lds.displayName "),

		@org.hibernate.annotations.NamedQuery(name = "getLookupInformationWithOutImplementation", query = "SELECT Distinct(lp) FROM LookupMaster lp   "
				+ " LEFT JOIN FETCH lp.lookupDetails lds	"
				+ " LEFT JOIN FETCH lp.lookupMaster lpp	"
				+ " WHERE lp.accessCode = ? and lp.isActive=1 order by lds.displayName "),

		@org.hibernate.annotations.NamedQuery(name = "getLookupListForImplementation", query = "SELECT Distinct(lp) FROM LookupMaster lp   "
				+ " LEFT JOIN FETCH lp.lookupDetails lds	"
				+ " LEFT JOIN FETCH lp.lookupMaster lpp	"
				+ " WHERE lp.implementation=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLookupDetailByCode", query = "SELECT Distinct(ld) FROM LookupDetail ld   "
				+ " JOIN FETCH ld.lookupMaster lm	"
				+ " WHERE ld.accessCode = ?"
				+ " AND lm.accessCode = ?"
				+ " AND lm.implementation=? "),

		@org.hibernate.annotations.NamedQuery(name = "getLookupDetailsList", query = "SELECT Distinct(ld) FROM LookupDetail ld   "
				+ " LEFT JOIN FETCH ld.lookupMaster lpm	"
				+ " WHERE lpm.lookupMasterId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAlertRolesByImplementation", query = "SELECT Distinct(ar) FROM AlertRole ar   "
				+ " LEFT JOIN FETCH ar.moduleProcess mp "
				+ " LEFT JOIN FETCH ar.role ro	"
				+ " WHERE ar.implementationId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAlertRoleInfoById", query = "SELECT Distinct(ar) FROM AlertRole ar   "
				+ " LEFT JOIN FETCH ar.moduleProcess mp "
				+ " LEFT JOIN FETCH ar.role ro	" + " WHERE ar.alertRoleId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getModuleProcessByUseCase", query = "SELECT Distinct(ar) FROM AlertRole ar "
				+ " LEFT JOIN FETCH ar.moduleProcess mp "
				+ " LEFT JOIN FETCH ar.role ro	" + " WHERE mp.useCase = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getModuleProcessByUseCaseImpl", query = "SELECT Distinct(ar) FROM AlertRole ar "
				+ " LEFT JOIN FETCH ar.moduleProcess mp "
				+ " LEFT JOIN FETCH ar.role ro	" 
				+ " WHERE ar.implementationId = ?"
				+ " AND mp.processTitle = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getImplementationGroup", query = "SELECT i FROM Implementation i "
				+ " WHERE i.implementationId IN (:implementationIds) "),

		@org.hibernate.annotations.NamedQuery(name = "getImplementationByCompanyKey", query = "SELECT i FROM Implementation i "
				+ " WHERE i.companyKey = ? "),

})
package com.aiotech.aios.system.domain.query;