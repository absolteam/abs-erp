package com.aiotech.aios.system.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.aios.system.service.SystemService;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class TerritoryAction extends ActionSupport {

	private long countryId;
	private long stateId;
	private SystemService systemService;

	public String getCountryStates() {

		try {
			List<State> states = systemService.getCountryStates(countryId);
			ServletActionContext.getRequest().setAttribute("stateList", states);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getStateCities() {

		try {
			List<City> cities = systemService.getStateCities(stateId);
			ServletActionContext.getRequest().setAttribute("cityList", cities);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

}
