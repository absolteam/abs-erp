package com.aiotech.aios.system.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.to.AlertVO;
import com.aiotech.aios.workflow.domain.entity.AlertRole;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class AlertAction extends ActionSupport {
	private static final Logger LOGGER = LogManager
			.getLogger(AlertAction.class);
	private AlertBL alertBL;
	private SystemService systemService;
	private Boolean applyLimit;
	private List<Object> aaData;
	private String returnMessage;
	private Long alertRoleId;

	public String getAlertList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			List<UserRole> userRoles = systemService.getUserRoles(user
					.getUsername());
			List<Alert> alerts = alertBL.getAlertList(userRoles);
			if (null != alerts && alerts.size() > 0) {
				Collections.sort(alerts, new Comparator<Alert>() {
					public int compare(Alert o1, Alert o2) {
						return o2.getAlertId().compareTo(o1.getAlertId());
					}
				});
			}
			if (applyLimit != null && applyLimit == true && alerts != null
					&& alerts.size() > 30) {
				List<Alert> temAlerts = new ArrayList<Alert>();
				int i = 0;
				for (Alert alert : alerts) {
					if (i <= 30) {
						temAlerts.add(alert);
					}
					i++;
				}
				alerts = temAlerts;
				ServletActionContext.getRequest().setAttribute("applyLimit",
						applyLimit);
			}

			List<AlertVO> alertVOs = new ArrayList<AlertVO>();
			for (Alert alert : alerts) {
				AlertVO alertVO = new AlertVO();
				alertVO.convertToVO(alert);

				if (alert.getScreenId() != null) {
					alertVO.setScreen(systemService.getScreenById(alert
							.getScreenId()));
				}

				alertVOs.add(alertVO);
			}

			Collections.sort(alertVOs, new Comparator<AlertVO>() {
				public int compare(AlertVO m1, AlertVO m2) {
					return m2.getAlertId().compareTo(m1.getAlertId());
				}
			});
			ServletActionContext.getRequest().setAttribute("alertList",
					alertVOs);

			// session.setAttribute("ALERT_LIST_"+sessionObj.get("alertList"),
			// alerts);
			// session.setAttribute("WORKFLOW_LIST_REVERSE_"+sessionObj.get("jSessionId"),
			// wfReverseNotification);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String redirectAlertRolePage() throws Exception {
		LOGGER.info("Inside Method: returnSuccess()");

		ServletActionContext.getRequest().setAttribute("moduleProcessList",
				systemService.getAllModuleProcess());
		ServletActionContext.getRequest().setAttribute("rolesList",
				systemService.getAllRoles(getImplementation()));
		ServletActionContext.getRequest().setAttribute("personsList",
				alertBL.getUserBasedEmployees());

		return SUCCESS;
	}

	// Show All Projects
	public String getAllAlertRoles() {
		try {
			LOGGER.info("Inside Module: System : Method: getAllAlertRoles");
			aaData = alertBL.getAllAlertRoles();
			LOGGER.info("Module: System : Method: getAllAlertRoles: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String saveAlertRole() {
		try {
			LOGGER.info("Inside Module: System : Method: saveAlertRole");
			AlertRole alertRole = new AlertRole();
			if (alertRoleId != null && alertRoleId > 0) {
				alertRole = alertBL.getAlertService().getAlertRoleInfoById(
						alertRoleId);
			} else {
				alertRole.setImplementationId(getImplementation()
						.getImplementationId());
				alertRole.setCreatedDate(new Date());
			}
			String moduleProcessId = ServletActionContext.getRequest()
					.getParameter("moduleProcessId");
			if (moduleProcessId != null && !moduleProcessId.equals("0")) {
				ModuleProcess moduleProcess = new ModuleProcess();
				moduleProcess.setProcessId(Long.valueOf(moduleProcessId));
				alertRole.setModuleProcess(moduleProcess);
			}
			String roleId = ServletActionContext.getRequest().getParameter(
					"roleId");
			if (roleId != null && !roleId.equals("0")) {
				Role role = new Role();
				role.setRoleId(Long.valueOf(roleId));
				alertRole.setRole(role);
			}

			String personId = ServletActionContext.getRequest().getParameter(
					"personId");
			if (personId != null && !personId.equals("0")) {
				alertRole.setPersonId(Long.valueOf(personId));
			}

			alertBL.getAlertService().saveAlertRole(alertRole);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: System : Method: saveAlertRole: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			LOGGER.info("Module: System : Method: saveAlertRole Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deleteAlertRole() {
		try {
			LOGGER.info("Inside Module: System : Method: deleteAlertRole");

			AlertRole alertRole = alertBL.getAlertService()
					.getAlertRoleInfoById(alertRoleId);
			alertBL.getAlertService().deleteAlertRole(alertRole);
			returnMessage = "SUCCESS";
			LOGGER.info("Module: System : Method: deleteAlertRole: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			returnMessage = "Failure";
			LOGGER.info("Module: System : Method: deleteAlertRole Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get Session Implementation
	public Implementation getImplementation() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			return (Implementation) session.getAttribute("THIS");
		}
		return null;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public Boolean getApplyLimit() {
		return applyLimit;
	}

	public void setApplyLimit(Boolean applyLimit) {
		this.applyLimit = applyLimit;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Long getAlertRoleId() {
		return alertRoleId;
	}

	public void setAlertRoleId(Long alertRoleId) {
		this.alertRoleId = alertRoleId;
	}

}
