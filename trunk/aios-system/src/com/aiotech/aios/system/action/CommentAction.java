package com.aiotech.aios.system.action;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CommentAction extends ActionSupport  {

	private static final Logger LOGGER = LogManager
	.getLogger(CommentAction.class);
	private CommentBL commentBL;
	private String comment;
	public String saveWfRejectComment() {
		try {
			HttpSession session = ServletActionContext.getRequest()
			.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			
			long recId=(Long) session.getAttribute("WORKFLOW_RECORD_ID_"+sessionObj.get("jSessionId"));
			String tableName=(String) session.getAttribute("WORKFLOW_USECASE_"+sessionObj.get("jSessionId"));
			Comment commentIfo=new Comment();
			commentIfo.setComment(comment);
			commentIfo.setCreatedDate(new Date());
			commentIfo.setPersonId(user.getPersonId());
			commentIfo.setRecordId(recId);
			commentIfo.setUseCase(tableName);
			if(recId !=0 && tableName!=null && !tableName.equals(""))
				commentBL.getCommentService().saveComment(commentIfo);
			
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	//Identify the JAVA class package
	public static String getPackageName(Class c) {
	    String fullyQualifiedName = c.getName();
	    int lastDot = fullyQualifiedName.lastIndexOf ('.');
	    if (lastDot==-1){ return ""; }
	    return fullyQualifiedName.substring (0, lastDot);
	}
	public CommentBL getCommentBL() {
		return commentBL;
	}
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	

}
