package com.aiotech.aios.system.action;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.GenerateMenuBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class MenuAction extends ActionSupport {

	private SystemService systemService;
	private GenerateMenuBL generateMenuBL;
	private Map<String, Object> menuMap;

	public String getMenuItems() throws Exception {

		HttpSession session = ServletActionContext.getRequest().getSession();

		if (session.getAttribute("MENU_MAP") == null) {
			menuMap = generateMenuBL.performBusinessLogic(((User) session
					.getAttribute("USER")).getUsername());
			session.setAttribute("MENU_MAP", menuMap);
			session.setAttribute("QUICK_LINKS", menuMap.get("MENU_QUICK_LINKS"));
		}

		return SUCCESS;
	}

	public String returnSuccess() throws Exception {

		return SUCCESS;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public GenerateMenuBL getGenerateMenuBL() {
		return generateMenuBL;
	}

	public void setGenerateMenuBL(GenerateMenuBL generateMenuBL) {
		this.generateMenuBL = generateMenuBL;
	}

}
