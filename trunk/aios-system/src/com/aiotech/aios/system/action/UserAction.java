package com.aiotech.aios.system.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Authorities;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport {

	private SystemService systemService;
	private SystemBL systemBL;
	private String username_;
	private String password_;
	private String role_;
	private Boolean status_;
	private Long person_;
	private String userFlag;
	private int iTotalRecords;
	private int iTotalDisplayRecords;

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public Boolean getStatus_() {
		return status_;
	}

	public void setStatus_(Boolean status_) {
		this.status_ = status_;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public String getRole_() {
		return role_;
	}

	public void setRole_(String role_) {
		this.role_ = role_;
	}

	public Implementation getImplementation() {
		return (Implementation) ServletActionContext.getRequest().getSession()
				.getAttribute("THIS");
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public String getUsername_() {
		return username_;
	}

	public void setUsername_(String username_) {
		this.username_ = username_;
	}

	public String getPassword_() {
		return password_;
	}

	public void setPassword_(String password_) {
		this.password_ = password_;
	}

	public String returnSuccess() {
		return SUCCESS;
	}

	public String deleteUser() throws Exception {

		String actualUsername = username_.contains("@") ? username_ : username_
				+ "_" + getImplementation().getImplementationId().toString();
		systemService.deleteUserRoles(systemService
				.getUserRoles(actualUsername));
		systemService.deleteAuthorities(systemService
				.getAuthorities(actualUsername));
		systemService.deleteUser(actualUsername, getImplementation());
		return SUCCESS;
	}

	public String getUserRoleList() throws Exception {

		ServletActionContext.getRequest().setAttribute("rolesList",
				systemService.getAllRoles(getImplementation()));
		ServletActionContext.getRequest().setAttribute("personsList",
				systemBL.getAllPerson(getImplementation()));
		return SUCCESS;
	}

	public String getAllUsers() throws Exception {

		List<User> users = systemService.getAllUsers(getImplementation());

		iTotalRecords = users.size();
		iTotalDisplayRecords = users.size();

		JSONObject jsonResponse = new JSONObject();
		jsonResponse.put("iTotalRecords", iTotalRecords);
		jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
		JSONArray data = new JSONArray();
		StringBuilder userRoles_Names;
		StringBuilder userRoles_Ids;
		Person person = null;
		for (User user : users) {

			JSONArray array = new JSONArray();
			array.add(user.getUsername().split("_")[0]);
			if (user.getPersonId() != null) {
				person = systemBL.getPersonDAO().findById(user.getPersonId());
				array.add(person.getFirstName() + " " + person.getLastName());
				array.add(user.getPersonId());
			}
			userRoles_Names = new StringBuilder();
			userRoles_Ids = new StringBuilder();
			for (UserRole role : systemService.getUserRoles(user.getUsername())) {

				userRoles_Names.append("[" + role.getRole().getRoleName()
						+ "] ");
				userRoles_Ids.append(role.getRole().getRoleId() + ",");
			}

			array.add(userRoles_Names.toString());
			array.add(user.getEnabled() ? "Enabled" : "Disabled");
			array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" onclick=\"deleteUser(this);\">Delete</a>");
			array.add(userRoles_Ids.toString());
			data.add(array);
		}

		jsonResponse.put("aaData", data);
		ServletActionContext.getRequest()
				.setAttribute("jsonlist", jsonResponse);

		return SUCCESS;
	}

	public String addUser() throws Exception {

		if (userFlag.equals("E")) {

			return this.editUser();
		}

		User user;
		Role sysRole;
		UserRole userRole;
		String actualUsername = username_.contains("@") ? username_ : username_
				+ "_" + getImplementation().getImplementationId().toString();

		if (systemService.getUser(actualUsername) != null) {

			throw new Exception("USER ALREADY EXISTS");
		}

		user = new User();
		user.setUsername(actualUsername);
		user.setPassword(AIOSCommons.encrypt(password_));
		user.setImplementationId(getImplementation().getImplementationId());
		user.setEnabled(status_);
		user.setPersonId(person_);
		systemService.saveOrUpdateUser(user);

		for (String roleId : role_.split(",")) {

			sysRole = systemService.getRole(Long.parseLong(roleId));

			userRole = new UserRole();
			userRole.setIsActive(true);
			userRole.setRole(sysRole);
			userRole.setUser(user);
			systemService.addUserRole(userRole);
		}

		Authorities authority = new Authorities();
		authority.setUsername(user.getUsername());
		authority.setAuthority("ROLE_USER");
		systemService.addAuthorities(authority);

		return SUCCESS;
	}

	public String editUser() throws Exception {

		User user;
		Role sysRole;
		UserRole userRole;
		String actualUsername = username_.contains("@") ? username_ : username_
				+ "_" + getImplementation().getImplementationId().toString();

		if ((user = systemService.getUser(actualUsername)) == null) {

			throw new Exception("USER DOESN'T EXISTS");
		}
		user.setPersonId(person_);
		user.setEnabled(status_);
		systemService.saveOrUpdateUser(user);

		List<UserRole> userRoles = systemService.getUserRoles(actualUsername);
		String tempRoleArray[] = role_.split(",");

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (String string : tempRoleArray) {
			listOne.add(Long.parseLong(string));
		}
		for (UserRole userRole2 : userRoles) {
			if (userRole2.getRole().getRoleId() != null
					&& userRole2.getRole().getRoleId() > 0)
				listTwo.add(userRole2.getRole().getRoleId());
		}

		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		for (Long diffrentRoleId : different) {
			UserRole tempUserRole = systemService.getUserRole(actualUsername,
					diffrentRoleId);
			if (tempUserRole != null) {
				try {
					systemService.deleteUserRole(tempUserRole);
				} catch (org.hibernate.exception.ConstraintViolationException e) {
					tempUserRole.setIsActive(false);
					systemService.addUserRole(tempUserRole);
				}
			} else {
				sysRole = new Role();
				sysRole = systemService.getRole(diffrentRoleId);
				userRole = new UserRole();
				userRole.setIsActive(true);
				userRole.setRole(sysRole);
				userRole.setUser(user);

				systemService.addUserRole(userRole);
			}
		}
		for (Long similarRoleId : similar) {
			UserRole tempUserRole = systemService.getUserRole(actualUsername,
					similarRoleId);
			if (tempUserRole != null) {
				// systemService.addUserRole(tempUserRole);
			} else {
				sysRole = new Role();
				sysRole = systemService.getRole(similarRoleId);
				userRole = new UserRole();
				userRole.setIsActive(true);
				userRole.setRole(sysRole);
				userRole.setUser(user);
				systemService.addUserRole(userRole);
			}
		}

		return SUCCESS;
	}

	public Long getPerson_() {
		return person_;
	}

	public void setPerson_(Long person_) {
		this.person_ = person_;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}
}
