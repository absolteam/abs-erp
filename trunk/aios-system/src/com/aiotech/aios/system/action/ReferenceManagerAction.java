package com.aiotech.aios.system.action;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;
import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetDisposal;
import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;
import com.aiotech.aios.accounts.domain.entity.AssetService;
import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReconciliation;
import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialWastage;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.VoidPayment;
import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.hr.domain.entity.IdentityAvailabilityMaster;
import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.Reference;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.ReferenceManagerBL;
import com.aiotech.aios.system.to.ReferenceTO;
import com.opensymphony.xwork2.ActionSupport;

public class ReferenceManagerAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	SystemService systemService;
	ReferenceManagerBL referenceManagerBL;

	Integer refId;
	Implementation implementation;
	String usecase;
	String prefix;
	long refNumber;
	Integer refDigits;
	String dateTag;
	String symbolTag;
	String postfix;
	String description;
	int refNumberPosition;
	int dateTagPosition;
	int symbolTagPosition;
	Boolean isStarted;
	Boolean templateStatus;
	List<Reference> references = new ArrayList<Reference>();

	private int iTotalRecords;
	private int iTotalDisplayRecords;

	public String returnSuccess() {

		try {
			references = systemService.getAllReferences(getImplementation());
			List<ReferenceTO> validNewReferences = new ArrayList<ReferenceTO>();
			validNewReferences = filterValidEntities(getAllEntityList());
			ServletActionContext.getRequest().setAttribute(
					"VALID_NEW_REFERENCES", validNewReferences);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private List<ReferenceTO> filterValidEntities(List<ReferenceTO> list)
			throws Exception {

		List<ReferenceTO> tempList = new ArrayList<ReferenceTO>(list);
		for (Reference reference : references) {
			for (ReferenceTO referenceTO : tempList) {
				if (reference.getUsecase().contains(referenceTO.getClassName())) {
					list.remove(referenceTO);
					break;
				}
			}
		}
		return list;
	}

	private List<ReferenceTO> getAllEntityList() {

		List<ReferenceTO> list = new ArrayList<ReferenceTO>();
		ReferenceTO to;

		to = new ReferenceTO();
		to.setUsecase("Contract");
		to.setClassName(Contract.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Offer");
		to.setClassName(Offer.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Release");
		to.setClassName(Release.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Transfer Letter");
		to.setClassName("transfer");
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Clearance Letter");
		to.setClassName("clearance");
		list.add(to);


		to = new ReferenceTO();
		to.setUsecase("Property Expense");
		to.setClassName(PropertyExpense.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Journal Voucher");
		to.setClassName(Transaction.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Memo & Warning");
		to.setClassName(MemoWarning.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Open Position");
		to.setClassName(OpenPosition.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Advertising Panel");
		to.setClassName(AdvertisingPanel.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Payment Request");
		to.setClassName(PaymentRequest.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Direct Payment");
		to.setClassName(DirectPayment.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Receipts");
		to.setClassName(BankReceipts.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Bank Deposits");
		to.setClassName(BankDeposit.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Internal Transfer");
		to.setClassName(BankTransfer.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Pettycash");
		to.setClassName(PettyCash.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Reconciliation Adjustment");
		to.setClassName(ReconciliationAdjustment.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Bank Reconciliation");
		to.setClassName(BankReconciliation.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Requisition");
		to.setClassName(Requisition.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Issue Requisition");
		to.setClassName(IssueRequistion.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Issue Return");
		to.setClassName(IssueReturn.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Tender");
		to.setClassName(Tender.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Quotation");
		to.setClassName(Quotation.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Purchase Order");
		to.setClassName(Purchase.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Goods Receive");
		to.setClassName(Receive.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Goods Return");
		to.setClassName(GoodsReturn.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Debit Note");
		to.setClassName(Debit.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Purchase Invoice");
		to.setClassName(Invoice.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Purchase Payments");
		to.setClassName(Payment.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Customer Quotation");
		to.setClassName(CustomerQuotation.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Sales Order");
		to.setClassName(SalesOrder.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Sales Delivery");
		to.setClassName(SalesDeliveryNote.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Sales Invoice");
		to.setClassName(SalesInvoice.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Credit Note");
		to.setClassName(Credit.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("POS");
		to.setClassName(PointOfSale.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("EOD Balancing");
		to.setClassName(EODBalancing.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Merchandise Exchange");
		to.setClassName(MerchandiseExchange.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Asset CheckOut");
		to.setClassName(AssetCheckOut.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Asset CheckIn");
		to.setClassName(AssetCheckIn.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Asset Service");
		to.setClassName(AssetService.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Asset Disposal");
		to.setClassName(AssetDisposal.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Claim Insurance");
		to.setClassName(AssetClaimInsurance.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Claim Warranty");
		to.setClassName(AssetWarrantyClaim.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Asset Revaluation");
		to.setClassName(AssetRevaluation.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Production Receive");
		to.setClassName(ProductionReceive.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Material Transfer");
		to.setClassName(MaterialTransfer.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Material Requisition");
		to.setClassName(MaterialRequisition.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Request");
		to.setClassName(Project.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Task");
		to.setClassName(ProjectTask.class.getName());
		list.add(to);

		to = new ReferenceTO();
		to.setUsecase("Deliverable");
		to.setClassName(ProjectDelivery.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Product Split");
		to.setClassName(ProductSplit.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Sales Return");
		to.setClassName(Credit.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Material Idle Mix");
		to.setClassName(MaterialIdleMix.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Material Wastage");
		to.setClassName(MaterialWastage.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("WorkIn Process");
		to.setClassName(WorkinProcess.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Production Requisition");
		to.setClassName(ProductionRequisition.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Account Group");
		to.setClassName(AccountGroup.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Void Cheque");
		to.setClassName(VoidPayment.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Product Packaging");
		to.setClassName(ProductPackage.class.getName());
		list.add(to);
		
		to = new ReferenceTO();
		to.setUsecase("Document Handover");
		to.setClassName(IdentityAvailabilityMaster.class.getName());
		list.add(to);
		return list;
	}

	public String execute() {

		try {
			String[] entity;

			iTotalRecords = references.size();
			iTotalDisplayRecords = references.size();

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();

			for (Reference ref : references) {

				JSONArray array = new JSONArray();
				array.add(ref.getRefId());
				entity = ref.getUsecase().split("entity.");
				array.add(entity[entity.length - 1].toUpperCase());
				array.add(ref.getUsecase());
				array.add(ref.getPrefix().equals("XX$") ? "" : ref.getPrefix());
				array.add(ref.getRefNumber() == null ? ""
						: formulateReferenceNumber(ref.getRefNumber(),
								ref.getRefDigits()));
				array.add(ref.getRefDigits());

				array.add(ref.getDateTag().equals("XX$") ? "" : ref
						.getDateTag());
				array.add(ref.getSymbolTag().equals("XX$") ? "" : ref
						.getSymbolTag());
				array.add(ref.getPostfix().equals("XX$") ? "" : ref
						.getPostfix());
				array.add(ref.getDescription() + "");
				array.add(ref.getRefNumberPosition());
				array.add(ref.getDateTagPosition());
				array.add(ref.getSymbolTagPosition());
				array.add(referenceManagerBL.generateReferenceString(
						ref,
						formulateReferenceNumber(ref.getRefNumber(),
								ref.getRefDigits())));

				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	private String formulateReferenceNumber(Long refNumber, Integer refDigits) {

		try {
			int diffrence = refDigits - (refNumber + "").length();
			String stringRefNo = "";
			while (stringRefNo.length() < diffrence) {
				stringRefNo = stringRefNo + "0";
			}
			stringRefNo = stringRefNo + refNumber;
			return stringRefNo;

		} catch (Exception e) {
			e.printStackTrace();
			return "0";
		}
	}

	public String addEditReference() {

		try {
			Reference refTO = new Reference();
			if (refId == null || refId == 0) {
				refId = null;
				refTO.setIsStarted(null);
			} else {
				refTO.setIsStarted(isStarted);
			}
			refTO.setRefId(refId);
			refTO.setImplementation(getImplementation());
			refTO.setPrefix(prefix);
			refTO.setDateTag(dateTag);
			refTO.setSymbolTag(symbolTag);
			refTO.setPostfix(postfix);
			refTO.setUsecase(usecase);
			refTO.setRefNumber(refNumber);
			if (refDigits != null)
				refTO.setRefDigits(refDigits);
			else
				refTO.setRefDigits(0);
			refTO.setRefNumberPosition(new Byte(refNumberPosition + ""));
			refTO.setDateTagPosition(new Byte(dateTagPosition + ""));
			refTO.setSymbolTagPosition(new Byte(symbolTagPosition + ""));
			refTO.setDescription(description);
			systemService.addOrUpdateReference(refTO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getReferenceStamp() {

		try {
			return referenceManagerBL.getReferenceStamp(usecase,
					getImplementation());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String deleteReference() {

		Reference refTO = new Reference();
		refTO.setRefId(refId);
		try {
			systemService.deleteReference(refTO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getUsecase() {
		return usecase;
	}

	public String getPrefix() {
		return prefix;
	}

	public long getRefNumber() {
		return refNumber;
	}

	public String getDateTag() {
		return dateTag;
	}

	public String getSymbolTag() {
		return symbolTag;
	}

	public String getPostfix() {
		return postfix;
	}

	public String getDescription() {
		return description;
	}

	public int getRefNumberPosition() {
		return refNumberPosition;
	}

	public int getDateTagPosition() {
		return dateTagPosition;
	}

	public int getSymbolTagPosition() {
		return symbolTagPosition;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public void setUsecase(String usecase) {
		this.usecase = usecase;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setRefNumber(long refNumber) {
		this.refNumber = refNumber;
	}

	public void setDateTag(String dateTag) {
		this.dateTag = dateTag;
	}

	public void setSymbolTag(String symbolTag) {
		this.symbolTag = symbolTag;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRefNumberPosition(int refNumberPosition) {
		this.refNumberPosition = refNumberPosition;
	}

	public void setDateTagPosition(int dateTagPosition) {
		this.dateTagPosition = dateTagPosition;
	}

	public void setSymbolTagPosition(int symbolTagPosition) {
		this.symbolTagPosition = symbolTagPosition;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public Boolean getIsStarted() {
		return isStarted;
	}

	public void setIsStarted(Boolean isStarted) {
		this.isStarted = isStarted;
	}

	public Integer getRefDigits() {
		return refDigits;
	}

	public void setRefDigits(Integer refDigits) {
		this.refDigits = refDigits;
	}

	public Integer getRefId() {
		return refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public ReferenceManagerBL getReferenceManagerBL() {
		return referenceManagerBL;
	}

	public void setReferenceManagerBL(ReferenceManagerBL referenceManagerBL) {
		this.referenceManagerBL = referenceManagerBL;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public Boolean getTemplateStatus() {
		return templateStatus;
	}

	public void setTemplateStatus(Boolean templateStatus) {
		this.templateStatus = templateStatus;
	}
}
