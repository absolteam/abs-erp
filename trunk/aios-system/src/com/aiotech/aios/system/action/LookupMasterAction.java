package com.aiotech.aios.system.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.system.domain.entity.vo.LookupDetailVO;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.opensymphony.xwork2.ActionSupport;

public class LookupMasterAction extends ActionSupport {
	private Implementation implementation;
	private Long recordId;
	private Long lookupMasterId;
	private Long lookupDetailId;
	private String accessCode;
	private Integer dataId;
	private String displayName;
	private String displayNameArabic;
	private Boolean isActive;
	private Boolean isSystem;
	private LookupMasterBL lookupMasterBL;
	List<LookupDetail> lookupDetails;
	private LookupDetailVO lookupDetail;

	public String getActiveLookupDetails() {
		try {
			List<LookupDetail> lookupDetails = null;
			lookupDetails = lookupMasterBL.getActiveLookupDetails(accessCode,
					true);
			ServletActionContext.getRequest().setAttribute("LOOKUP_DETAILS",
					lookupDetails);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String addLookupDetail() {
		try {
			List<LookupMaster> lookupMasters = null;
			lookupMasters = lookupMasterBL.getLookupMasterService()
					.getLookupInformation(accessCode, getImplementation());
			if (lookupMasters != null && lookupMasters.size() > 0) {

				ServletActionContext.getRequest().setAttribute("LOOKUP_MASTER",
						lookupMasters.get(0));
			} else {
				LookupMaster lookupMaster = new LookupMaster();
				lookupMaster.setAccessCode(accessCode);
				lookupMaster.setDisplayName(accessCode);
				lookupMaster.setImplementation(getImplementation());
				lookupMaster.setIsActive(Byte.valueOf("1"));
				lookupMaster.setIsSystem(Byte.valueOf("0"));
				lookupMasterBL.getLookupMasterService().saveLookupInformation(
						lookupMaster, null);
				ServletActionContext.getRequest().setAttribute("LOOKUP_MASTER",
						lookupMaster);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String editLookupDetail() {
		try {

			if (lookupDetailId != null && lookupDetailId > 0) {
				LookupDetail lookupDetailTemp = lookupMasterBL
						.getLookupMasterService().getLookupDetailById(
								lookupDetailId);
				lookupDetail = new LookupDetailVO();
				lookupDetail.setLookupDetailId(lookupDetailId);
				lookupDetail.setDataId(lookupDetailTemp.getDataId());
				lookupDetail.setAccessCode(lookupDetailTemp.getAccessCode());
				lookupDetail.setDisplayName(lookupDetailTemp.getDisplayName());
				lookupDetail.setDisplayNameArabic(lookupDetailTemp
						.getDisplayNameArabic());
				lookupDetail.setArabicDisplayName(AIOSCommons
						.bytesToUTFString(lookupDetailTemp
								.getDisplayNameArabic()));
				lookupDetail.setIsSystem(lookupDetailTemp.getIsSystem());
				lookupDetail.setIsActive(lookupDetailTemp.getIsActive());
			} else {
				lookupDetail = null;
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String loadLookupDetail() {
		try {
			List<LookupDetail> lookupDetailsTemp = lookupMasterBL
					.getActiveLookupDetails(accessCode, true);
			if (lookupDetailsTemp != null && lookupDetailsTemp.size() > 0) {
				lookupDetails = new ArrayList<LookupDetail>();
				for (LookupDetail lookupDetail : lookupDetailsTemp) {
					LookupDetail lookupDetailToView = new LookupDetail();
					lookupDetailToView.setLookupDetailId(lookupDetail
							.getLookupDetailId());
					lookupDetailToView.setDisplayName(lookupDetail
							.getDisplayName());
					lookupDetailToView.setDisplayNameArabic(lookupDetail
							.getDisplayNameArabic());
					lookupDetails.add(lookupDetailToView);
				}
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveLookupDetail() {
		try {
			LookupDetail lookupDetail = new LookupDetail();
			LookupMaster lookupMaster = new LookupMaster();
			if (accessCode != null && !accessCode.trim().equalsIgnoreCase(""))
				lookupDetail.setAccessCode(accessCode);
			if (dataId != null && dataId > 0)
				lookupDetail.setDataId(dataId);

			lookupDetail.setDisplayName(displayName);
			if (displayNameArabic != null
					&& !displayNameArabic.trim().equals(""))
				lookupDetail.setDisplayNameArabic(AIOSCommons
						.stringToUTFBytes(displayNameArabic));
			if (isActive != null)
				lookupDetail.setIsActive(isActive);
			else
				lookupDetail.setIsActive(false);

			if (isSystem != null)
				lookupDetail.setIsSystem(isSystem);
			else
				lookupDetail.setIsSystem(false);
			lookupMaster.setLookupMasterId(lookupMasterId);
			lookupDetail.setLookupMaster(lookupMaster);
			if (lookupDetailId != null && lookupDetailId > 0)
				lookupDetail.setLookupDetailId(lookupDetailId);

			// To identify the dublicate code or id
			List<LookupDetail> lookupDetailsExsit = lookupMasterBL
					.getLookupMasterService().getLookupDetailsList(
							lookupMasterId);
			String returnMessage = lookupMasterBL.saveLookupDetail(
					lookupDetail, lookupDetailsExsit);
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Save Failure");
			return ERROR;
		}
	}

	public String deleteLookupDetail() {
		try {
			String returnMessage = "SUCCESS";
			if (lookupDetailId != null && lookupDetailId > 0) {
				LookupDetail lookupDetailTemp = lookupMasterBL
						.getLookupMasterService().getLookupDetailById(
								lookupDetailId);
				lookupMasterBL.getLookupMasterService().deleteLookupDetail(
						lookupDetailTemp);
			} else {
				returnMessage = "No Data has selected to delete";
			}
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					"Can not Delete, Infomration is in use!");
			return ERROR;
		}
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Long getLookupMasterId() {
		return lookupMasterId;
	}

	public void setLookupMasterId(Long lookupMasterId) {
		this.lookupMasterId = lookupMasterId;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public Integer getDataId() {
		return dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayNameArabic() {
		return displayNameArabic;
	}

	public void setDisplayNameArabic(String displayNameArabic) {
		this.displayNameArabic = displayNameArabic;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsSystem() {
		return isSystem;
	}

	public void setIsSystem(Boolean isSystem) {
		this.isSystem = isSystem;
	}

	public List<LookupDetail> getLookupDetails() {
		return lookupDetails;
	}

	public void setLookupDetails(List<LookupDetail> lookupDetails) {
		this.lookupDetails = lookupDetails;
	}

	public LookupDetailVO getLookupDetail() {
		return lookupDetail;
	}

	public void setLookupDetail(LookupDetailVO lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

}
