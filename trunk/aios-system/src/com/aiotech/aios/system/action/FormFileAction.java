package com.aiotech.aios.system.action;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.CompressionUtil;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.system.to.TreeVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/*
 * @author: Asad Ali Rathore.
 * 
 */

public class FormFileAction extends ActionSupport {

	private static final Logger LOGGER = LogManager
			.getLogger(FormFileAction.class);

	public FormFileAction() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5457259344166339229L;

	public String getNodeText() {
		return nodeText;
	}

	public void setNodeText(String nodeText) {
		this.nodeText = nodeText;
	}

	public String getParentNodeText() {
		return parentNodeText;
	}

	public void setParentNodeText(String parentNodeText) {
		this.parentNodeText = parentNodeText;
	}

	private File fileToUpload;
	private String fileToUploadConentType;
	private String fileToUploadFileName;
	private int fileIndex;
	private String nameofFileToDelete;
	private SystemService systemService;
	private String fileType;
	private String mode;
	private ImageBL imageBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private String rootLabel;
	private String nodeType;
	private String fileHash;
	private List<Object> aaData;
	private String returnStatus;
	private String spcialName;
	private String code;

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getRootLabel() {
		return rootLabel;
	}

	public void setRootLabel(String rootLabel) {
		this.rootLabel = rootLabel;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	private String hashedName;

	public String getHashedName() {
		return hashedName;
	}

	public void setHashedName(String hashedName) {
		this.hashedName = hashedName;
	}

	private long recordId;
	private String imgId;
	private String tableName;
	private String displayPane;

	// tree properties;

	private String nodeText;
	private String parentNodeText;
	private String fileNodeText;

	public String getFileNodeText() {
		return fileNodeText;
	}

	public void setFileNodeText(String fileNodeText) {
		this.fileNodeText = fileNodeText;
	}

	public String getDisplayPane() {
		return displayPane;
	}

	public void setDisplayPane(String displayPane) {
		this.displayPane = displayPane;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getImgId() {
		return imgId;
	}

	public void setImgId(String imgId) {
		this.imgId = imgId;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	private String docId;

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	private enum Messages {
		SUCCESS, ERROR, WARNING
	}

	public String getNameofFileToDelete() {
		return nameofFileToDelete;
	}

	public void setNameofFileToDelete(String nameofFileToDelete) {
		this.nameofFileToDelete = nameofFileToDelete;
	}

	public int getFileIndex() {
		return fileIndex;
	}

	public void setFileIndex(int fileIndex) {
		this.fileIndex = fileIndex;
	}

	public File getFileToUpload() {
		return fileToUpload;
	}

	public String getFileToUploadConentType() {
		return fileToUploadConentType;
	}

	public void setFileToUploadConentType(String fileToUploadConentType) {
		this.fileToUploadConentType = fileToUploadConentType;
	}

	public String getFileToUploadFileName() {
		return fileToUploadFileName;
	}

	public void setFileToUploadFileName(String fileToUploadFileName) {
		this.fileToUploadFileName = fileToUploadFileName;
	}

	public void setFileToUpload(File fileToUpload) {
		this.fileToUpload = fileToUpload;
	}

	Boolean templateAlreadyUploaded() {

		try {
			List<Map<String, Object>> uploadedTemplates = FileUtil
					.getUploadedTemplates(getTemplateUploadPath());
			for (Map<String, Object> map : uploadedTemplates)
				if (map.get("fileName").toString()
						.contains(getFileToUploadFileName()))
					return true;
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	public void uploadTemplate() {

		JSONObject json = new JSONObject();
		if (!validateFile(true) || templateAlreadyUploaded())
			throw new IllegalArgumentException(
					"Template type not supported, or already exists.");
		try {
			File userFile = getFileToUpload();
			byte[] bytes = FileUtil.getBytes(userFile);
			json.put("name", getFileToUploadFileName());
			json.put("type", getFileToUploadConentType());
			int comprssedSize = compressAndSaveOnServer(bytes, getFileType());
			json.put("size", (comprssedSize / 1024));
			if (comprssedSize != -1) {
				json.put("fileName", getFileToUploadFileName());
				json.put(
						"progressMsg",
						getProgressMessage(Messages.SUCCESS,
								"&nbsp;File uploaded successfully."));
			} else {
				json.put(
						"progressMsg",
						getProgressMessage(Messages.WARNING,
								"&nbsp;File with same name is already uploaded."));
			}
			json.put("displayPane", getDisplayPane());
			json.put("tableName", getTableName());
			json.put("fileType", getFileType());

		} catch (Exception e) {
			e.printStackTrace();
			json.put(
					"progressMsg",
					getProgressMessage(Messages.ERROR,
							"File could not be uploaded, try again."));
		} finally {
			try {
				ServletActionContext.getResponse().getWriter()
						.write(json.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void upload() {
		if (validateFile(false)) {
			JSONObject json = new JSONObject();
			try {
				File userFile = getFileToUpload();
				byte[] bytes = FileUtil.getBytes(userFile);
				json.put("name", getFileToUploadFileName());
				json.put("type", getFileToUploadConentType());
				int comprssedSize = compressAndSaveInSession(bytes,
						getFileType());
				json.put("size", (comprssedSize / 1024));
				if (comprssedSize != -1) {
					json.put("fileName", getFileToUploadFileName());
					json.put(
							"progressMsg",
							getProgressMessage(Messages.SUCCESS,
									"&nbsp;File uploaded successfully."));
					if ("doc".equals(getFileType()))
						saveInSessionUploadedNode();
				} else {
					json.put(
							"progressMsg",
							getProgressMessage(Messages.WARNING, "&nbsp;File ("
									+ getFileToUploadFileName()
									+ ") with same name is already uploaded."));
				}
				json.put("displayPane", getDisplayPane());
				json.put("tableName", getTableName());
				json.put("fileType", getFileType());

			} catch (Exception e) {
				e.printStackTrace();
				json.put(
						"progressMsg",
						getProgressMessage(Messages.ERROR, "File ("
								+ getFileToUploadFileName()
								+ ") could not be uploaded,Try Again."));
			} finally {
				try {
					ServletActionContext.getResponse().getWriter()
							.write(json.toString());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			LOGGER.error("File Type is not specified, whether it is document or image.");
			throw new IllegalArgumentException(
					"File Type is not specified, whether it is document or image.");
		}
	}

	private boolean validateFile(Boolean isTemplate) {

		if (!"".equals(getFileType()) && null != getFileType())
			return checkFileExtension(getFileToUploadFileName(), getFileType(),
					isTemplate);
		return false;
	}

	private boolean checkFileExtension(String fileName, String fileType,
			Boolean isTemplate) {

		fileName = fileName.toLowerCase();

		if (isTemplate)
			return fileName.endsWith(".doc");

		if (fileName.contains("&") || fileName.contains("#"))
			return false;

		if (fileType.equals("doc"))

			return (fileName.endsWith(".doc") || fileName.endsWith(".docx")
					|| fileName.endsWith(".pdf") || fileName.endsWith(".xps")
					|| fileName.endsWith(".rtf") || fileName.endsWith(".dot")
					|| fileName.endsWith(".dotx") || fileName.endsWith(".xls")
					|| fileName.endsWith(".xlsx") || fileName.endsWith(".jpg")
					|| fileName.endsWith(".jpeg") || fileName.endsWith(".gif")
					|| fileName.endsWith(".png") || fileName.endsWith(".bmp")
					|| fileName.endsWith(".txt") || fileName.endsWith(".csv"));

		else if (fileType.equals("img"))

			return (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")
					|| fileName.endsWith(".gif") || fileName.endsWith(".png")
					|| fileName.endsWith(".bmp") || fileName.endsWith(".jpe")
					|| fileName.endsWith(".jfif") || fileName.endsWith(".tif") || fileName
					.endsWith(".tiff"));

		return false;
	}

	private String getProgressMessage(Messages msg, String msgText) {
		StringBuilder builder = new StringBuilder();
		if (Messages.ERROR == msg) {
			builder.append("<img src = 'fileupload/error.png' />");
		} else if (Messages.SUCCESS == msg) {
			builder.append("<img src = 'fileupload/success.png' />");
		} else if (Messages.WARNING == msg) {
			builder.append("<img src = 'fileupload/warning.png' />");
		}
		builder.append(msgText);
		return builder.toString();
	}

	private String getTemplateUploadPath() {

		try {
			Implementation implementation = (Implementation) ServletActionContext
					.getRequest().getSession().getAttribute("THIS");

			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			return confProps.getProperty("file.drive")
					.concat("/aios/PrintTemplate/templates/implementation_")
					.concat(implementation.getImplementationId().toString());
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private int compressAndSaveOnServer(byte[] bytes, String templateType)
			throws UnsupportedEncodingException, IOException,
			DataFormatException {

		Map<String, byte[]> bytesMap = new HashMap<String, byte[]>();
		byte[] compressedBytes = CompressionUtil.compressBytes(bytes);
		bytesMap.put(getFileToUploadFileName(), compressedBytes);
		FileUtil.copyCachedFiles(bytesMap, null, getTemplateUploadPath()
				.concat("/").concat(getFileToUploadFileName()).concat("_"));
		return compressedBytes.length;
	}

	private synchronized int compressAndSaveInSession(byte[] bytes,
			String fileType) throws UnsupportedEncodingException, IOException {
		int compressedSize = -1;
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession(false);

		Map<String, byte[]> bytesMap = getFilesFromSession();
		if (bytesMap.get(getFileToUploadFileName()) == null) {
			byte[] compressedBytes = CompressionUtil.compressBytes(bytes);
			bytesMap.put(getFileToUploadFileName(), compressedBytes);
			compressedSize = compressedBytes.length;
		}
		synchronized (session) {
			session.setAttribute("AIOS-" + fileType + "-" + getTableName()
					+ "-" + session.getId() + "-" + getDisplayPane() + "-"
					+ getRecordId(), bytesMap);
		}
		LOGGER.info("number of files: " + bytesMap.size());

		return compressedSize;
	}

	private String extension(String path) {
		int dot = path.lastIndexOf('.');
		return path.substring(dot + 1);
	}

	private byte[] showTempFile(String fileName, byte[] extractedBytes)
			throws UnsupportedEncodingException, IOException,
			DataFormatException {
		extractedBytes = CompressionUtil.extractBytes(extractedBytes);
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String outputFilePath = confProps.getProperty("file.upload.path")
				.concat(File.separator).concat("extracted")
				.concat(File.separator).concat(fileName);
		FileUtil.writeExtracedBytesToDisk(extractedBytes, outputFilePath);
		byte[] outBytes = FileUtil.getBytes(new File(outputFilePath));
		return outBytes;
	}

	void writeBytesToResponseStream(byte[] extractedBytes) throws Exception {

		BufferedInputStream byteIn;
		DataInputStream dataIn;
		byte[] bytesToSend;
		int read = 0;

		byteIn = new BufferedInputStream(new ByteArrayInputStream(
				extractedBytes));
		dataIn = new DataInputStream(byteIn);
		bytesToSend = new byte[dataIn.available()];

		ServletActionContext.getResponse().setContentType(
				"application/force-download");
		ServletActionContext.getResponse().setHeader(
				"Content-Transfer-Encoding", "binary");
		ServletActionContext.getResponse().setHeader("Content-Disposition",
				"attachment; filename=\"" + getFileToUploadFileName() + "\"");

		while ((read = dataIn.read(bytesToSend)) != -1) {

			ServletActionContext.getResponse().getOutputStream()
					.write((bytesToSend), 0, read);
		}
	}

	public void showUploaded() throws Exception, DataFormatException {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession(false);
		Map<String, byte[]> filesMap = getFilesFromSession();
		session.removeAttribute("DecodedBytes");

		boolean success = false;
		String failureReason = "_UNKNOWNFILE";

		if (filesMap.get(getFileToUploadFileName()) != null) {
			try {
				byte[] extractedBytes = filesMap.get(getFileToUploadFileName());
				extractedBytes = showTempFile(getFileToUploadFileName(),
						extractedBytes);
				writeBytesToResponseStream(extractedBytes);
				success = true;
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				failureReason = "_EXCEPTION";
			}
		} else {

			try {
				if (AIOSCommons.isNullOrEmptyString(getImgId()) == false
						&& AIOSCommons.isNullOrEmptyString(String
								.valueOf(getRecordId())) == false) {

					Image image = new Image();
					image.setImageId(Long.parseLong(getImgId()));
					image.setRecordId(getRecordId());
					image.setTableName(getTableName());
					image.setHashedName(getHashedName());
					image.setName(getFileToUploadFileName());
					byte imageBytes[] = imageBL.showImage(image);
					writeBytesToResponseStream(imageBytes);
					success = true;
				} else if (AIOSCommons.isNullOrEmptyString(getDocId()) == false
						&& AIOSCommons.isNullOrEmptyString(String
								.valueOf(getRecordId())) == false) {
					Document doc = new Document();
					doc.setDocumentId(Long.parseLong(getDocId()));
					doc.setRecordId(getRecordId());
					doc.setTableName(getTableName());
					doc.setHashedName(getHashedName());
					doc.setName(getFileToUploadFileName());
					byte documentBytes[] = documentBL.showDocument(doc);
					writeBytesToResponseStream(documentBytes);
					success = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				failureReason = "_EXCEPTION";
			}
		}

		if (!success)
			ServletActionContext
					.getResponse()
					.getOutputStream()
					.write(("<div align=\"left\" style=\"display: block; "
							+ "width: 90%; margin: 0px auto; font-family: monospace; "
							+ "background-color: rgb(204, 204, 204); padding: 10px; "
							+ "border: thin solid rgb(170, 170, 170);\">"
							+ getErrorMessage(failureReason) + "</div></html>")
							.getBytes());
	}

	public void viewUploaded() throws Exception, DataFormatException {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession(false);
		Map<String, byte[]> filesMap = getFilesFromSession();
		session.removeAttribute("DecodedBytes");

		boolean success = false;
		String failureReason = "_UNKNOWNFILE";
		byte documentBytes[] = null;
		if (filesMap.get(getFileToUploadFileName()) != null) {
			try {
				byte[] extractedBytes = filesMap.get(getFileToUploadFileName());
				extractedBytes = showTempFile(getFileToUploadFileName(),
						extractedBytes);
				writeBytesToResponseStream(extractedBytes);
				success = true;
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				failureReason = "_EXCEPTION";
			}
		} else {

			try {
				if (AIOSCommons.isNullOrEmptyString(getImgId()) == false
						&& AIOSCommons.isNullOrEmptyString(String
								.valueOf(getRecordId())) == false) {

					Image image = new Image();
					image.setImageId(Long.parseLong(getImgId()));
					image.setRecordId(getRecordId());
					image.setTableName(getTableName());
					image.setHashedName(getHashedName());
					image.setName(getFileToUploadFileName());
					documentBytes = imageBL.showImage(image);

					success = true;
				} else if (AIOSCommons.isNullOrEmptyString(getDocId()) == false
						&& AIOSCommons.isNullOrEmptyString(String
								.valueOf(getRecordId())) == false) {
					Document doc = new Document();
					doc.setDocumentId(Long.parseLong(getDocId()));
					doc.setRecordId(getRecordId());
					doc.setTableName(getTableName());
					doc.setHashedName(getHashedName());
					doc.setName(getFileToUploadFileName());
					documentBytes = documentBL.showDocument(doc);
					success = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				failureReason = "_EXCEPTION";
			}
		}
		String ext = FilenameUtils.getExtension(getFileToUploadFileName());
		String applicationType = "";
		if (ext.equals("pdf"))
			applicationType = "application/pdf";
		else if (ext.equals("jpeg") || ext.equals("png") || ext.equals("bmp")
				|| ext.equals("jpg") || ext.equals("bmp") || ext.equals("jfif")
				|| ext.equals("tif"))
			applicationType = "image/" + ext;
		else
			success = false;

		if (success) {
			BufferedInputStream byteIn;
			DataInputStream dataIn;
			byte[] bytesToSend;
			int read = 0;

			byteIn = new BufferedInputStream(new ByteArrayInputStream(
					documentBytes));
			dataIn = new DataInputStream(byteIn);
			bytesToSend = new byte[dataIn.available()];

			ServletActionContext.getResponse().setContentType(applicationType);

			ServletActionContext.getResponse().setHeader(
					"Content-Transfer-Encoding", "binary");

			while ((read = dataIn.read(bytesToSend)) != -1) {

				ServletActionContext.getResponse().getOutputStream()
						.write((bytesToSend), 0, read);
			}
		} else {
			ServletActionContext
					.getResponse()
					.getOutputStream()
					.write(("<div align=\"left\" style=\"display: block; "
							+ "width: 90%; margin: 0px auto; font-family: monospace; "
							+ "background-color: rgb(204, 204, 204); padding: 10px; "
							+ "border: thin solid rgb(170, 170, 170);\">"
							+ getUnableToProcessMessage(failureReason) + "</div></html>")
							.getBytes());
		}
	}

	String getErrorMessage(String reason) {

		return "<span style='color: red; font-weight: bolder;'>Bad Request</span><br/><br/>"
				+ "Something went wrong, please try to close and reopen the upload panel and try again.";
	}

	String getUnableToProcessMessage(String reason) {

		return "<span style='color: red; font-weight: bolder;'>Bad Request</span><br/><br/>"
				+ "Can not view this file format.";
	}

	public void deleteUploaded() throws IOException {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession(false);
		Map<String, byte[]> filesMap = getFilesFromSession();

		if (filesMap.get(getFileToUploadFileName()) != null) {
			filesMap.remove(getFileToUploadFileName());
			session.setAttribute("AIOS-" + getFileType() + "-" + getTableName()
					+ "-" + session.getId() + "-" + getDisplayPane() + "-"
					+ getRecordId(), filesMap);
			LOGGER.info("remaining number of files: " + filesMap.size());
		} else {

			if (AIOSCommons.isNullOrEmptyString(getImgId()) == false
					&& AIOSCommons.isNullOrEmptyString(String
							.valueOf(getRecordId())) == false) {
				Image image = new Image();
				image.setImageId(Long.parseLong(getImgId()));
				image.setRecordId(getRecordId());
				image.setTableName(getTableName());
				image.setHashedName(getHashedName());
				imageBL.deleteImage(image);
			}

			if (AIOSCommons.isNullOrEmptyString(getDocId()) == false
					&& AIOSCommons.isNullOrEmptyString(String
							.valueOf(getRecordId())) == false) {
				Document doc = new Document();
				doc.setDocumentId(Long.parseLong(getDocId()));
				doc.setRecordId(getRecordId());
				doc.setTableName(getTableName());
				doc.setHashedName(getHashedName());
				documentBL.deleteDocument(doc);
			}
		}

		deleteDirectory();

		JSONObject json = new JSONObject();
		json.put("displayPane", getDisplayPane());
		ServletActionContext.getResponse().getWriter().write(json.toString());
	}

	@SuppressWarnings("unchecked")
	private void deleteDirectory() throws IOException {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		Map<String, String> files = (Map<String, String>) session
				.getAttribute("AIOS-file-structure" + "-" + getDisplayPane()
						+ "-" + getRecordId());
		Map<String, String> dirs = (Map<String, String>) session
				.getAttribute("AIOS-dir-structure" + "-" + getDisplayPane()
						+ "-" + getRecordId());
		List<Directory> dirList = (List<Directory>) session
				.getAttribute("AIOS-dirNode-structure" + "-" + getDisplayPane()
						+ "-" + getRecordId());

		String mode = (String) session.getAttribute("AIOS-tree-mod" + "-"
				+ getDisplayPane() + "-" + getRecordId());
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String path = confProps.getProperty("file.upload.path");
		if ("edit".equals(mode) && "dir".equals(getNodeType())) {
			if (getNodeText() != null && dirs != null
					&& dirs.get(getNodeText()) == null) {
				directoryBL.deleteDirectory(dirList, getNodeText(), path); // //
																			// heree
			} else if (dirs == null && getNodeText() != null) {
				directoryBL.deleteDirectory(dirList, getNodeText(), path);
			}

		} else if ("edit".equals(mode) && "file".equals(getNodeType())) {

			if (getNodeText() != null && files != null
					&& files.get(getNodeText()) == null) {
				directoryBL.deleteDocument(dirList, getNodeText(), path);
			} else if (files == null && getNodeText() != null) {
				directoryBL.deleteDocument(dirList, getNodeText(), path);
			}
		}

		directoryBL.removeDirFromSession(dirList, getNodeText());
		if (getNodeText() != null && dirs != null)
			dirs.remove(getNodeText());
		if (getNodeText() != null && files != null)
			files.remove(getNodeText());
	}

	@SuppressWarnings("unchecked")
	private Map<String, byte[]> getFilesFromSession() {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession(false);
		Map<String, byte[]> filesMap = null;
		if (session != null
				&& session.getAttribute("AIOS-" + getFileType() + "-"
						+ getTableName() + "-" + session.getId() + "-"
						+ getDisplayPane() + "-" + getRecordId()) != null) {
			filesMap = (Map<String, byte[]>) session.getAttribute("AIOS-"
					+ getFileType() + "-" + getTableName() + "-"
					+ session.getId() + "-" + getDisplayPane() + "-"
					+ getRecordId());
		} else {
			filesMap = new HashMap<String, byte[]>();

		}
		return filesMap;
	}

	public void deleteUploadedTemplate() {

		try {
			LOGGER.info(getTemplateUploadPath().concat("/").concat(
					getNameofFileToDelete().trim())
					+ " deletion result: "
					+ deleteTemplate(getNameofFileToDelete()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean deleteTemplate(String fileToDelete) {

		File templatesFolder = new File(getTemplateUploadPath());
		File[] templates = templatesFolder.listFiles();

		for (File template : templates)
			if (template.getName().contains(fileToDelete))
				return template.delete();

		return false;
	}

	public void populateUploadedTemplate() throws IOException {

		JSONObject json = new JSONObject();
		json.put("fileList",
				FileUtil.getUploadedTemplates(getTemplateUploadPath()));
		ServletActionContext.getResponse().getWriter().write(json.toString());
	}

	public void populateUploaded() throws IOException {
		List<Map<String, String>> fileList = new ArrayList<Map<String, String>>();

		fileList = populateUploadedFromSession();
		if (getRecordId() != -1) {
			fileList.addAll(populateUploadedFromDB());
		}

		JSONObject json = new JSONObject();
		json.put("fileList", fileList);
		json.put("displayPane", getDisplayPane());
		json.put("tableName", getTableName());
		ServletActionContext.getResponse().getWriter().write(json.toString());

	}

	private List<Map<String, String>> populateUploadedFromSession() {
		Map<String, byte[]> fileMap = getFilesFromSession();
		List<Map<String, String>> fileList = new ArrayList<Map<String, String>>();
		for (Map.Entry<String, byte[]> entry : fileMap.entrySet()) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("fileName", entry.getKey());
			map.put("progressMsg", "<img src = 'fileupload/success.png' />");
			byte[] bytes = ((byte[]) entry.getValue());
			map.put("size", String.valueOf((bytes.length) / 1024));
			map.put("random", String.valueOf(AIOSCommons.getRandom()));
			map.put("recId", "-1");
			map.put("tableName", getTableName());
			bytes = null;
			fileList.add(map);
		}
		return fileList;
	}

	private List<Map<String, String>> populateUploadedFromDB() {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		if ("doc".equals(getFileType())) {
			Document document = new Document();
			document.setRecordId(recordId);
			document.setTableName(tableName);
			document.setDisplayPane(displayPane);
			list = documentBL.retriveRecordDocuments(document);
		} else if ("img".equals(getFileType())) {
			Image img = new Image();
			img.setRecordId(recordId);
			img.setTableName(tableName);
			img.setDisplayPane(displayPane);
			list = imageBL.retriveRecordImages(img);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public void getTree() {
		JSONObject json = new JSONObject();
		JSONObject jsonRoot = new JSONObject();
		List<JSONObject> root = new ArrayList<JSONObject>();
		// if (getRecordId() > 0){
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		List<Directory> dirList = (List<Directory>) session
				.getAttribute("AIOS-dirNode-structure" + "-" + getDisplayPane()
						+ "-" + getRecordId());
		// if (getRecordId() > 0 && dirList == null){
		// setRecordId(2);
		if (getRecordId() > 0
				&& (dirList == null || (null != dirList && dirList.size() == 0))
				&& "doc".equals(getFileType())) {
			Directory dir = new Directory();
			dir.setTableName(getTableName());
			dir.setRecordId(getRecordId());
			dir.setDisplayPane(getDisplayPane());
			TreeVO tree = directoryBL.getDirsNDocs(dir, jsonRoot,
					getRootLabel());
			List<Directory> dirs = makeDirListForSession(tree.getDirList());

			if (dirs != null && dirs.size() > 0) {
				root.add(directoryBL.populateTreeFromSession(dirs,
						getRootLabel(), jsonRoot));
			} else if (tree.getJsonTree() != null) {
				root.add(tree.getJsonTree());
				dir.setDirectoryName(getRootLabel());
				dirs.add(dir);
			}
			session.setAttribute("AIOS-dirNode-structure" + "-"
					+ getDisplayPane() + "-" + getRecordId(), dirs);
			session.setAttribute("AIOS-tree-mod" + "-" + getDisplayPane() + "-"
					+ getRecordId(), "edit");

		} else {
			if (dirList != null && dirList.size() > 0
					&& "doc".equals(getFileType())) {
				JSONObject j = directoryBL.populateTreeFromSession(dirList,
						getRootLabel(), jsonRoot);
				root.add(j);
			}
			if (root != null && dirList == null
					|| (null != dirList && dirList.size() == 0)) {
				JSONObject jsonDir = new JSONObject();
				jsonDir.put("id", "1");
				jsonDir.put("text", rootLabel);
				jsonDir.put("select", "yes");
				jsonDir.put("tooltip ", "Directory");
				root.add(jsonDir);
				Directory dir = new Directory();
				dir.setDirectoryName(jsonDir.getString("text"));
				dir.setTableName(getTableName());
				dir.setRecordId(getRecordId());
				dir.setDisplayPane(getDisplayPane());
				if (dirList == null && "doc".equals(getFileType())) {
					dirList = new ArrayList<Directory>();
					dirList.add(dir);
					session.setAttribute("AIOS-dirNode-structure" + "-"
							+ getDisplayPane() + "-" + getRecordId(), dirList);
				}

			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", "0");
		json.accumulateAll(map);

		if (root != null && root.size() > 0)
			json.accumulate("item", root);

		try {
			ServletActionContext.getResponse().getWriter()
					.write(json.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public void saveInSessionNode() {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		Map<String, String> treeNodes = null;
		if (session.getAttribute("AIOS-dir-structure" + "-" + getDisplayPane()
				+ "-" + getRecordId()) != null) {
			treeNodes = (Map<String, String>) session
					.getAttribute("AIOS-dir-structure" + "-" + getDisplayPane()
							+ "-" + getRecordId());
		} else {
			treeNodes = new TreeMap<String, String>();
		}
		treeNodes.put(getNodeText(), getParentNodeText());
		session.setAttribute("AIOS-dir-structure" + "-" + getDisplayPane()
				+ "-" + getRecordId(), treeNodes);
		saveNodeAsDirInSession();

	}

	@SuppressWarnings("unchecked")
	private void saveNodeAsDirInSession() {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		List<Directory> dirList = null;
		if (session.getAttribute("AIOS-dirNode-structure" + "-"
				+ getDisplayPane() + "-" + getRecordId()) != null) {
			dirList = (List<Directory>) session
					.getAttribute("AIOS-dirNode-structure" + "-"
							+ getDisplayPane() + "-" + getRecordId());
		} else {
			dirList = new ArrayList<Directory>();
		}
		findNAddDirectory(dirList);
		session.setAttribute("AIOS-dirNode-structure" + "-" + getDisplayPane()
				+ "-" + getRecordId(), dirList);
	}

	private Directory saveNodeAsFileInSession(List<Directory> dirList) {

		Directory directory = null;
		if (dirList != null) {
			String checkedDirName = null;
			traversAndSetFileNode(dirList, checkedDirName, directory);
		}
		return directory;
	}

	private void traversAndSetFileNode(List<Directory> dirList,
			String checkedDirName, Directory directory) {
		for (int itr = 0; itr < dirList.size(); itr++) {
			Directory dir = dirList.get(itr);
			traversForFile(dir, checkedDirName, directory);
		}
	}

	private void traversForFile(Directory dir, String checkedDirName,
			Directory directory) {
		if (dir.getDirectoryName().equals(getParentNodeText())) {
			Document doc = new Document();
			doc.setName(getFileToUploadFileName());
			Set<Document> docs = dir.getDocuments();
			doc.setTableName(getTableName());
			doc.setRecordId(getRecordId());
			doc.setDisplayPane(getDisplayPane());
			docs.add(doc);
			dir.setDocuments(docs);
			directory = dir;
		} else {
			Set<Directory> subDirs = dir.getDirectories();
			for (Directory subDir : subDirs) {
				traversForFile(subDir, checkedDirName, directory);
			}
		}
	}

	private Directory findNAddDirectory(List<Directory> dirList) {
		Directory directory = null;
		for (Directory dir : dirList) {
			if (dir.getDirectoryName().equals(getParentNodeText())) {
				directory = addSubDir(dir, dirList);
			} else {
				Set<Directory> subDirs = dir.getDirectories();
				findNAddDirectory(new ArrayList<Directory>(subDirs));
			}
		}
		return directory;
	}

	public Directory addSubDir(Directory dir, List<Directory> dirList) {
		Set<Directory> subDirs = dir.getDirectories();
		Directory subDir = new Directory();
		subDir.setDirectory(dir);
		subDir.setDirectoryName(getNodeText());
		subDir.setRecordId(getRecordId());
		subDir.setDisplayPane(getDisplayPane());
		subDir.setTableName(getTableName());
		subDirs.add(subDir);
		dir.setDirectories(subDirs);
		dirList.set(dirList.indexOf(dir), dir);
		return subDir;
	}

	@SuppressWarnings("unchecked")
	public void saveInSessionUploadedNode() {
		HttpSession session = ServletActionContext.getRequest().getSession(
				false);
		Map<String, String> treeNodes = null;
		if (session.getAttribute("AIOS-file-structure" + "-" + getDisplayPane()
				+ "-" + getRecordId()) != null) {
			treeNodes = (Map<String, String>) session
					.getAttribute("AIOS-file-structure" + "-"
							+ getDisplayPane() + "-" + getRecordId());
		} else {
			treeNodes = new TreeMap<String, String>();
		}
		treeNodes.put(getFileToUploadFileName(), getParentNodeText());
		session.setAttribute("AIOS-file-structure" + "-" + getDisplayPane()
				+ "-" + getRecordId(), treeNodes);
		List<Directory> dirList = (List<Directory>) session
				.getAttribute("AIOS-dirNode-structure" + "-" + getDisplayPane()
						+ "-" + getRecordId());
		saveNodeAsFileInSession(dirList);
	}

	public boolean deleteFile(String filePath) {
		File file = new File(filePath);
		return file.delete();
	}

	public void saveDirsNDocs() {
		try {

			Property p = new Property();
			p.setPropertyId((long) 4);
			DocumentVO docVO = documentBL.populateDocumentVO(p, "uploadedDocs",
					true);
			Map<String, String> dirs = docVO.getDirMap();

			if (dirs != null) {
				Map<String, Directory> dirStucture = directoryBL
						.saveDirStructure(dirs, 3, "Property", "uploadedDocs");
				docVO.setDirStruct(dirStucture);
			}

			documentBL.saveUploadDocuments(docVO);

			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			AIOSCommons.removeUploaderSession(session, docVO.getDisplayPane(),
					(long) 3, "");

			imageBL.saveUploadedImages(p, docVO);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private List<Directory> makeDirListForSession(List<Directory> dirList) {
		List<Directory> dList = new ArrayList<Directory>();
		if (dirList != null && dirList.size() > 0)
			dList.add(makeForSession(dirList, dList));
		return dList;
	}

	private Directory makeForSession(List<Directory> dirList,
			List<Directory> dList) {
		if (dirList != null && dirList.size() > 0) {
			Directory dir = dirList.get(0);
			Directory d = new Directory();
			d.setDocuments(dir.getDocuments());
			Set<Directory> subDirs = new HashSet<Directory>(
					directoryBL.getSubDirs(dir));
			d.setDirectories(getNewDirSet(subDirs, d));
			d.setDirectoryId(dir.getDirectoryId());
			d.setDirectoryName(dir.getDirectoryName());
			d.setDisplayPane(dir.getDisplayPane());
			d.setRecordId(dir.getRecordId());
			d.setTableName(dir.getTableName());
			return d;
		}
		return null;
	}

	private Set<Directory> getNewDirSet(Set<Directory> subDirs, Directory d) {
		Set<Directory> newSubs = new HashSet<Directory>();
		for (Directory sub : subDirs) {
			Directory d1 = new Directory();
			d1.setDirectoryId(sub.getDirectoryId());
			d1.setDirectoryName(sub.getDirectoryName());
			d1.setDirectory(d);
			d1.setDirectories(getNewDirSet(
					new HashSet<Directory>(directoryBL.getSubDirs(sub)), d1));
			d1.setDisplayPane(sub.getDisplayPane());
			d1.setRecordId(sub.getRecordId());
			d1.setTableName(sub.getTableName());
			d1.setDocuments(sub.getDocuments());
			newSubs.add(d1);
		}
		return newSubs;
	}

	public void renameDocumentOrImage() {

		try {
			if ("doc".equals(fileType)) {
				documentBL.renameFileNameByFileHash(fileHash,
						fileToUploadFileName);
			} else if ("img".equals(fileType)) {
				imageBL.renameImageNameByFileHash(fileHash,
						fileToUploadFileName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String imagesliderRedirect() {
		return SUCCESS;
	}

	public String getImageListForSlider() {

		try {

			List<Image> images = imageBL.getImageListForSlider();
			aaData = new ArrayList<Object>();
			for (Image image : images) {
				image.setImplementation(null);
				aaData.add(image);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String updateImageSliderList() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			String selectedImages = ServletActionContext.getRequest()
					.getParameter("selectedImages");
			if (selectedImages != null && !selectedImages.equals("")) {
				String[] stringArray = selectedImages.split(",");
				Map<Long, Long> idMaps = new HashMap<Long, Long>();
				for (String string : stringArray) {
					idMaps.put(Long.valueOf(string), Long.valueOf(string));
				}
				List<Image> images = imageBL.getImageListForSlider();
				for (Image image : images) {
					if (idMaps.containsKey(image.getImageId()))
						image.setIsSlider(true);
					else
						image.setIsSlider(false);
				}
				imageBL.updateSliderImageAndPhysicalFile(images);
			}
			returnStatus = "SUCCESS";
		} catch (Exception e) {
			returnStatus = "ERROR";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String saveImageSliderInfo() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			String imageIdStr = ServletActionContext.getRequest().getParameter(
					"imageId");
			if (imageIdStr != null && !imageIdStr.equals("")) {
				Map<Long, Long> idMaps = new HashMap<Long, Long>();
				idMaps.put(Long.valueOf(imageIdStr), Long.valueOf(imageIdStr));
				List<Image> images = imageBL.getImageListForSlider();
				for (Image image : images) {
					if (idMaps.containsKey(image.getImageId())) {
						image.setSpecialName(spcialName);
						image.setCode(code);
					}

				}
				imageBL.getImageService().saveImages(images);
			}
			returnStatus = "SUCCESS";
		} catch (Exception e) {
			returnStatus = "ERROR";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getFileHash() {
		return fileHash;
	}

	public void setFileHash(String fileHash) {
		this.fileHash = fileHash;
	}

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSpcialName() {
		return spcialName;
	}

	public void setSpcialName(String spcialName) {
		this.spcialName = spcialName;
	}
}
