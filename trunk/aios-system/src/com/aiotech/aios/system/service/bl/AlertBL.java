package com.aiotech.aios.system.service.bl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.EmiDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.vo.EMIDetailVO;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceStatus;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.POStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectPaymentStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.AlertRoleVO;
import com.aiotech.aios.system.domain.entity.vo.ConfigurationVO;
import com.aiotech.aios.system.service.AlertService;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.AlertRole;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.notify.enterprise.NotifyEnterpriseService;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AlertBL {
	private static final Logger LOGGER = LogManager.getLogger(AlertBL.class);
	Implementation implementation;
	private AlertService alertService;
	AIOTechGenericDAO<ContractPayment> contractPaymentDAO;
	AIOTechGenericDAO<Contract> contractDAO;
	private AIOTechGenericDAO<Loan> loanDAO;
	private AIOTechGenericDAO<EmiDetail> emiDetailDAO;
	private AIOTechGenericDAO<LoanCharge> loanChargeDAO;
	private AIOTechGenericDAO<DirectPayment> directPaymentDAO;
	private AIOTechGenericDAO<Identity> identityDAO;
	private AIOTechGenericDAO<BankReceiptsDetail> bankReceiptsDetailDAO;
	private AIOTechGenericDAO<IdentityAvailability> identityAvailabilityDAO;
	private AIOTechGenericDAO<BankReceipts> bankReceiptsDAO;
	private NotifyEnterpriseService notifyEnterpriseService;

	private List<EMIDetailVO> emiVOList = new ArrayList<EMIDetailVO>();
	private List<LoanCharge> chargeList = new ArrayList<LoanCharge>();
	EMIDetailVO emiVO = null;
	SystemService systemService;
	private double loanBalance;
	private double emiCumulative;
	private int varianceDays;
	private int yearlyNoDate;

	// private Implementation implemenation;

	//

	public void commonUpdateUseCaseStatus(String useCase, Long recordId,
			Object status) {
		try {

			AIOTechGenericDAO<Object> dao = WorkflowUtils.getEntityDAO(useCase);
			// Fetch actual record from process's table
			Object recordObject = dao.findById(recordId);
			// Get the use case name
			/*
			 * String[] name = workflowDetailVO.getObject().getClass()
			 * .getName().split("entity.");
			 */
			String nameTemp = recordObject.getClass().getName()
					.replace(".domain.entity.", ".DOMAIN.ENTITY.");
			String[] name = nameTemp.split(".DOMAIN.ENTITY.");
			// Some times use case name is appended by _javassist, so
			// get
			// proper use case name
			if (name[name.length - 1].contains("_$$_javassist")) {
				name[name.length - 1] = name[name.length - 1].substring(0,
						name[name.length - 1].indexOf("_$$_javassist"));
			}
			// getter method of primary key
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = recordObject.getClass().getMethod(methodId, null);
			Object tempRecordidObject = method.invoke(recordObject, null);
			long temprecordId = 0;
			if (tempRecordidObject instanceof Long) {
				temprecordId = ((Long) tempRecordidObject);
			} else if (tempRecordidObject instanceof Integer) {
				temprecordId = Long.parseLong(tempRecordidObject.toString());
			}

			if (temprecordId == recordId) {
				Class[] argTypes = null;
				String approvalflagmethodId = "setStatus";
				if (useCase.equalsIgnoreCase(Purchase.class.getSimpleName()))
					argTypes = new Class[] { Integer.class };
				else
					argTypes = new Class[] { Byte.class };

				if (useCase.equalsIgnoreCase(ProjectPaymentSchedule.class
						.getSimpleName())) {
					status = ProjectPaymentStatus.Paid.getCode();
					// Update Receipt Reference
					List<BankReceipts> bankReceipts = bankReceiptsDAO
							.findByNamedQuery(
									"getBankReceiptsByRecordIdUsecase",
									recordId, useCase);
					if (bankReceipts != null && bankReceipts.size() > 0) {
						Class[] argTypes1 = null;
						argTypes1 = new Class[] { String.class };
						String approvalflagmethodId1 = "setReceiptReference";
						Method approvalmethod1 = recordObject.getClass()
								.getMethod(approvalflagmethodId1, argTypes1);
						approvalmethod1.invoke(recordObject, bankReceipts
								.get(0).getReceiptsNo());
					}
				}

				if (useCase.equalsIgnoreCase(Invoice.class.getSimpleName()))
					status = InvoiceStatus.Paid.getCode();
				else if (useCase.equalsIgnoreCase(SalesInvoice.class
						.getSimpleName()))
					status = O2CProcessStatus.Paid.getCode();
				else if (useCase.equalsIgnoreCase(Credit.class.getSimpleName()))
					status = ProjectPaymentStatus.Paid.getCode();
				else if (useCase.equalsIgnoreCase(Purchase.class
						.getSimpleName()))
					status = Integer.parseInt(POStatus.Received.getCode() + "");

				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);

			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

	}

	public void commonReverseUpdateUseCaseStatus(String useCase, Long recordId,
			Object status) {
		try {

			AIOTechGenericDAO<Object> dao = WorkflowUtils.getEntityDAO(useCase);
			// Fetch actual record from process's table
			Object recordObject = dao.findById(recordId);
			// Get the use case name
			/*
			 * String[] name = workflowDetailVO.getObject().getClass()
			 * .getName().split("entity.");
			 */
			String nameTemp = recordObject.getClass().getName()
					.replace(".domain.entity.", ".DOMAIN.ENTITY.");
			String[] name = nameTemp.split(".DOMAIN.ENTITY.");
			// Some times use case name is appended by _javassist, so
			// get
			// proper use case name
			if (name[name.length - 1].contains("_$$_javassist")) {
				name[name.length - 1] = name[name.length - 1].substring(0,
						name[name.length - 1].indexOf("_$$_javassist"));
			}
			// getter method of primary key
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = recordObject.getClass().getMethod(methodId, null);
			Object tempRecordidObject = method.invoke(recordObject, null);
			long temprecordId = 0;
			if (tempRecordidObject instanceof Long) {
				temprecordId = ((Long) tempRecordidObject);
			} else if (tempRecordidObject instanceof Integer) {
				temprecordId = Long.parseLong(tempRecordidObject.toString());
			}

			if (temprecordId == recordId) {
				Class[] argTypes = null;
				String approvalflagmethodId = "setStatus";
				if (useCase.equalsIgnoreCase(Purchase.class.getSimpleName()))
					argTypes = new Class[] { Integer.class };
				else
					argTypes = new Class[] { Byte.class };

				if (useCase.equalsIgnoreCase(ProjectPaymentSchedule.class
						.getSimpleName())) {
					status = ProjectPaymentStatus.Paid.getCode();
					// Update Receipt Reference
					List<BankReceipts> bankReceipts = bankReceiptsDAO
							.findByNamedQuery(
									"getBankReceiptsByRecordIdUsecase",
									recordId, useCase);
					if (bankReceipts != null && bankReceipts.size() > 0) {
						Class[] argTypes1 = null;
						argTypes1 = new Class[] { String.class };
						String approvalflagmethodId1 = "setReceiptReference";
						Method approvalmethod1 = recordObject.getClass()
								.getMethod(approvalflagmethodId1, argTypes1);
						approvalmethod1.invoke(recordObject, bankReceipts
								.get(0).getReceiptsNo());
					}
				}

				if (useCase.equalsIgnoreCase(Invoice.class.getSimpleName()))
					status = InvoiceStatus.Active.getCode();
				else if (useCase.equalsIgnoreCase(SalesInvoice.class
						.getSimpleName()))
					status = O2CProcessStatus.Open.getCode();
				else if (useCase.equalsIgnoreCase(Credit.class.getSimpleName()))
					status = ProjectPaymentStatus.Pending.getCode();
				else if (useCase.equalsIgnoreCase(Purchase.class
						.getSimpleName()))
					status = Integer.parseInt(POStatus.Open.getCode() + "");

				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);

			}
		} catch (Exception e) {
			// e.printStackTrace();
		}

	}

	public void contractChequeDepositDate() {
		try {
			// getImplementId();
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			cal.add(Calendar.DATE, 5);
			java.util.Date date = cal.getTime();

			LOGGER.info("current date--->" + currentDate);
			LOGGER.info("5 days after--->" + date);

			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();

			// Contract Payment getContractPaymentsChequeDeposit

			List<ContractPayment> contractPayments = contractPaymentDAO
					.findByNamedQuery("getContractPaymentsChequeDeposit",
							currentDate, date);
			for (ContractPayment contractPayment : contractPayments) {

				String contractMessage = "Cheque Need to be deposit for Contract Number ( "
						+ contractPayment.getContract().getContractNo()
						+ " ) Cheque Number ( "
						+ contractPayment.getChequeNo()
						+ " )"
						+ "Expected Cheque Deposit Date ( "
						+ DateFormat.convertDateToString(contractPayment
								.getChequeDate().toString()) + " )";
				Alert alert = new Alert();
				alert.setMessage(contractMessage);
				alert.setRecordId(contractPayment.getContractPaymentId());
				alert.setTableName("ContractPayment");
				Screen screen = new Screen();
				implementation = systemService
						.getImplementation(contractPayment.getContract()
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(ContractPayment.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ContractPaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("ContractPayment"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			Set<Alert> resultset = new HashSet<Alert>();

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Rent Schedule
	public void rentPaymentSchedule() {
		try {
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();
			Set<String> alertList = new HashSet<String>();
			List<ContractPayment> contractPayments = contractPaymentDAO
					.findByNamedQuery("getContractPayments", new Date(),
							new Date());
			for (ContractPayment payment : contractPayments) {
				double tempChequeDay = DateFormat.dayDifference(
						payment.getChequeDate(), new Date());
				if (tempChequeDay >= 0) {
					List<ContractPayment> contractRentPayments = contractPaymentDAO
							.findByNamedQuery("getContractRentPayment", payment
									.getContract().getContractId());
					double rentAmount = 0;
					double monthlyRental = 0;
					for (ContractPayment list : contractRentPayments) {
						rentAmount += list.getAmount();
					}
					int offerMonths = DateFormat.monthDifference(payment
							.getContract().getOffer().getStartDate(), payment
							.getContract().getOffer().getEndDate());
					for (int i = 0; i < offerMonths; i++) {
						if (DateFormat.dayVariance(
								DateFormat.addToMonths(payment.getContract()
										.getOffer().getStartDate(), i),
								new Date()) == 0) {
							monthlyRental = rentAmount / offerMonths;
						}
					}
					if (monthlyRental > 0) {
						String rentMessage = "Rent for the month of("
								+ DateFormat
										.convertSystemDateToString(new Date())
								+ ")" + " for Contract("
								+ payment.getContract().getContractNo() + ")";
						Alert alert = new Alert();
						alert.setMessage(rentMessage);
						alert.setRecordId(payment.getContractPaymentId());
						alert.setTableName("ContractPayment");
						alert.setIsActive(true);
						Screen screen = new Screen();
						implementation = new Implementation();
						implementation = systemService
								.getImplementation(payment.getContract()
										.getImplementation());

						// Alert Role Access
						AlertRole alertRole = getRoleIdByModuleProcess(ContractPayment.class
								.getName());
						if (alertRole.getRole() != null) {
							alert.setRoleId(alertRole.getRole().getRoleId());
						} else {
							alert.setPersonId(alertRole.getPersonId());
						}

						resultList.add(alert);
						screen = systemService
								.getScreenBasedOnScreenName("ContractRentAlert");
						alert.setScreenId(screen.getScreenId());
						paymentList.add(alert.getRecordId() + "@"
								+ alert.getTableName());
					}
					List<Alert> alertExsit = alertService.getAlerts();
					for (Alert alert2 : alertExsit) {
						if (alert2.getTableName().equals("ContractPayment"))
							alertList.add(alert2.getRecordId() + "@"
									+ alert2.getTableName());
					}
					Set<String> pl = new HashSet<String>(paymentList);
					for (String string : paymentList) {
						for (String string1 : alertList) {
							if (string1.equalsIgnoreCase(string)) {
								pl.remove(string);
							}
						}
					}
					Set<Alert> resultset = new HashSet<Alert>();
					for (String string : pl) {
						String[] strAry = string
								.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
						for (Alert cp : resultList) {
							if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
								resultset.add(cp);
							}
						}

					}
					if (resultset != null && resultset.size() > 0)
						alertService.saveAlert(resultset);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void contractExpiry() {
		try {
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			cal.add(Calendar.MONTH, 1);
			java.util.Date date = cal.getTime();

			LOGGER.info("current date--->" + currentDate);
			LOGGER.info("one month before date--->" + date);
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();

			// Contract Payment getContractPaymentsChequeDeposit

			List<Contract> contracts = contractDAO
					.findByNamedQuery("getAllRenewContractsWOImp", currentDate,
							date, currentDate);
			for (Contract contract : contracts) {

				String contractMessage = "Contract going to be Expired : Contract Number ( "
						+ contract.getContractNo()
						+ " ) "
						+ "Expected Expire Date ( "
						+ DateFormat.convertDateToString(contract.getOffer()
								.getEndDate().toString()) + " )";
				Alert alert = new Alert();
				alert.setMessage(contractMessage);
				alert.setRecordId(contract.getContractId());
				alert.setTableName("Contract");
				alert.setIsActive(true);
				Screen screen = new Screen();

				implementation = new Implementation();
				implementation = systemService.getImplementation(contract
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Contract.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				resultList.add(alert);
				screen = systemService
						.getScreenBasedOnScreenName("ContractExpiryAlert");
				alert.setScreenId(screen.getScreenId());
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Contract"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			Set<Alert> resultset = new HashSet<Alert>();

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void alertEMIPayments() {
		try {
			List<Loan> loanList = loanDAO.findByNamedQuery(
					"getLoanAlertDetails", new Date());
			Set<Alert> resultset = new HashSet<Alert>();
			for (Loan list : loanList) {
				if (checkPayments(list.getEmiFirstDuedate())) {
					if (checkAlreadyPaymentsDone(list)) {
						this.setChargeList(loanChargeDAO.findByNamedQuery(
								"getLoanChargesById", list.getLoanId()));
						buildSchedule(list);
						if (null != emiVO && !emiVO.equals("")) {
							Alert alert = new Alert();
							if (list.getLoanType() == 'P')
								alert.setMessage("EMI Payment for "
										+ list.getLoanNumber() + " for "
										+ emiVO.getPaymentFor()
										+ emiVO.getEmiDueDate()
										+ " payable amount "
										+ emiVO.getAmount());
							else
								alert.setMessage("EMI Payment for "
										+ list.getLoanNumber() + " for "
										+ emiVO.getPaymentFor()
										+ emiVO.getEmiDueDate());
							alert.setRecordId(list.getLoanId());
							alert.setTableName("EmiDetail");
							Screen screen = new Screen();
							screen.setScreenName("Schedule EMI");
							alert.setScreenId(screen.getScreenId());
							alert.setIsActive(true);
							implementation = new Implementation();
							implementation = systemService
									.getImplementation(list.getImplementation());

							// Alert Role Access
							AlertRole alertRole = getRoleIdByModuleProcess(EmiDetail.class
									.getName());
							if (alertRole.getRole() != null) {
								alert.setRoleId(alertRole.getRole().getRoleId());
							} else {
								alert.setPersonId(alertRole.getPersonId());
							}

							resultset.add(alert);
							alertService.saveAlert(resultset);
						}

					} else {
						LOGGER.info("payment has already done in advance.");
					}
				} else {
					LOGGER.info("inside else");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Bank Receipt PDC Cheque Deposit
	public void bankReceiptPDCDeposit(List<Period> periodList,
			Implementation imp) {
		try {
			List<BankReceiptsDetail> bankReceiptsDetails = bankReceiptsDetailDAO
					.findByNamedQuery("getAllPDCBankReceipts", imp);
			if (null != bankReceiptsDetails && bankReceiptsDetails.size() > 0) {
				List<BankReceiptsDetail> alertPDCReceipts = new ArrayList<BankReceiptsDetail>();
				for (Period list : periodList) {
					for (BankReceiptsDetail receipts : bankReceiptsDetails) {
						if (DateFormat.monthDifference(list.getStartTime(),
								receipts.getChequeDate()) == 0) {
							alertPDCReceipts.add(receipts);
						}
					}
				}
				Set<Alert> resultset = new HashSet<Alert>();
				List<Alert> resultList = new ArrayList<Alert>();
				Set<String> paymentList = new HashSet<String>();
				Set<String> alertList = new HashSet<String>();
				for (BankReceiptsDetail pdcReceipts : alertPDCReceipts) {
					String message = "PDC Cheque has to deposit for Cheque No ( "
							+ pdcReceipts.getBankRefNo()
							+ " ) "
							+ "& Cheque Date ( "
							+ DateFormat.convertSystemDateToString(pdcReceipts
									.getChequeDate()) + " )";
					Alert alert = new Alert();
					alert.setMessage(message);
					alert.setRecordId(pdcReceipts.getBankReceipts()
							.getBankReceiptsId());
					alert.setTableName("BankReceipts");
					Screen screen = new Screen();
					implementation = systemService
							.getImplementation(pdcReceipts.getBankReceipts()
									.getImplementation());

					// Alert Role Access
					AlertRole alertRole = getRoleIdByModuleProcess(BankReceipts.class
							.getName());
					if (alertRole.getRole() != null) {
						alert.setRoleId(alertRole.getRole().getRoleId());
					} else {
						alert.setPersonId(alertRole.getPersonId());
					}

					screen = systemService
							.getScreenBasedOnScreenName("BankReceiptPDCAlert");
					alert.setScreenId(screen.getScreenId());
					alert.setIsActive(true);
					resultList.add(alert);
					paymentList.add(alert.getRecordId() + "@"
							+ alert.getTableName());
				}
				List<Alert> alertExsit = alertService.getAlerts();
				for (Alert alert2 : alertExsit) {
					if (alert2.getTableName().equals("BankReceipts"))
						alertList.add(alert2.getRecordId() + "@"
								+ alert2.getTableName());
				}

				Set<String> pl = new HashSet<String>(paymentList);
				for (String string : paymentList) {
					for (String string1 : alertList) {
						if (string1.equalsIgnoreCase(string)) {
							pl.remove(string);
						}
					}
				}

				for (String string : pl) {
					String[] strAry = string
							.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
					for (Alert cp : resultList) {
						if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
							resultset.add(cp);
						}
					}

				}
				if (resultset != null && resultset.size() > 0)
					alertService.saveAlert(resultset);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void alertDirectPayment() {
		Calendar cal = Calendar.getInstance();
		java.util.Date currentDate = cal.getTime();
		List<DirectPayment> directPayments = directPaymentDAO.findByNamedQuery(
				"getAllUpcomingDirectPayment", currentDate);
		alertDirectPaymentProcess(directPayments);
	}

	public void alertPropertyExpense(List<PropertyExpense> propertyExpense) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();
			Set<String> alertList = new HashSet<String>();
			for (PropertyExpense expense : propertyExpense) {
				String message = "Direct Payment to be processed for Property Expense Ref Number ( "
						+ expense.getExpenseNumber()
						+ " ) "
						+ "& Payment Date ( "
						+ DateFormat.convertSystemDateToString(expense
								.getDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(expense.getExpenseId());
				alert.setTableName("PropertyExpense");
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(expense
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(PropertyExpense.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("PropertyExpenseAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("PropertyExpense"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void alertSalesInvoiceProcess(List<SalesInvoice> salesInvoices) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (SalesInvoice salesInvoice : salesInvoices) {
				String message = "";
				message = "Bank Receipt process has been added for Sales Invoice Number ( "
						+ salesInvoice.getReferenceNumber()
						+ " ) "
						+ "& Dated on ( "
						+ DateFormat.convertSystemDateToString(salesInvoice
								.getInvoiceDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(salesInvoice.getSalesInvoiceId());
				alert.setTableName(salesInvoice.getClass().getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(salesInvoice
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(SalesInvoice.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("SalesInvoiceReceiptsAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("SalesInvoice"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertDebitNoteProcess(List<Debit> debits) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (Debit debit : debits) {
				String message = "";
				message = "Bank Receipt process has been added for Debit Note Reference Number ( "
						+ debit.getDebitNumber()
						+ " ) "
						+ "& Dated on ( "
						+ DateFormat.convertSystemDateToString(debit.getDate())
						+ " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(debit.getDebitId());
				alert.setTableName(debit.getClass().getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(debit
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Debit.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("DebitNoteReceiptsAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("DebitNote"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertClaimWarranty(List<AssetWarrantyClaim> assetWarrantyClaims) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (AssetWarrantyClaim assetWarrantyClaim : assetWarrantyClaims) {
				String message = "";
				message = "Asset Warranty Claim Receipt Ref ( "
						+ assetWarrantyClaim.getClaimReference()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat
								.convertSystemDateToString(assetWarrantyClaim
										.getClaimDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(assetWarrantyClaim.getAssetWarrantyClaimId());
				alert.setTableName(assetWarrantyClaim.getClass()
						.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(assetWarrantyClaim
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(AssetWarrantyClaim.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ClaimWarrantyReceiptAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("AssetClaimWarranty"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertClaimWarrantyExpense(
			List<AssetWarrantyClaim> assetWarrantyClaims) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (AssetWarrantyClaim assetWarrantyClaim : assetWarrantyClaims) {
				String message = "";
				message = "Asset Warranty Claim Expense Ref ( "
						+ assetWarrantyClaim.getClaimReference()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat
								.convertSystemDateToString(assetWarrantyClaim
										.getClaimDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(assetWarrantyClaim.getAssetWarrantyClaimId());
				alert.setTableName(assetWarrantyClaim.getClass()
						.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(assetWarrantyClaim
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(AssetClaimInsurance.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ClaimWarrantyExpenseAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				Screen screen = null;
				if (alert2.getScreenId() != null)
					screen = systemService.getScreenById(alert2.getScreenId());
				if (screen != null
						&& alert2.getTableName().equals("AssetClaimInsurance")
						&& !screen.getScreenName().equalsIgnoreCase(
								"ClaimWarrantyReceiptAlert"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertClaimInsurance(
			List<AssetClaimInsurance> assetClaimInsurances) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (AssetClaimInsurance assetClaimInsurance : assetClaimInsurances) {
				String message = "";
				message = "Asset Claim Insurance Receipt Ref ( "
						+ assetClaimInsurance.getReferenceNumber()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat
								.convertSystemDateToString(assetClaimInsurance
										.getClaimDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(assetClaimInsurance
						.getAssetClaimInsuranceId());
				alert.setTableName(assetClaimInsurance.getClass()
						.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(assetClaimInsurance
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(AssetClaimInsurance.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ClaimInsuranceReceiptAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("AssetClaimInsurance"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertClaimInsuranceExpense(
			List<AssetClaimInsurance> assetClaimInsurances) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (AssetClaimInsurance assetClaimInsurance : assetClaimInsurances) {
				String message = "";
				message = "Asset Claim Insurance Expense Ref ( "
						+ assetClaimInsurance.getReferenceNumber()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat
								.convertSystemDateToString(assetClaimInsurance
										.getClaimDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(assetClaimInsurance
						.getAssetClaimInsuranceId());
				alert.setTableName(assetClaimInsurance.getClass()
						.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(assetClaimInsurance
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(AssetClaimInsurance.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ClaimInsuranceExpenseAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				Screen screen = null;
				if (alert2.getScreenId() != null)
					screen = systemService.getScreenById(alert2.getScreenId());
				if (screen != null
						&& alert2.getTableName().equals("AssetClaimInsurance")
						&& !screen.getScreenName().equalsIgnoreCase(
								"ClaimInsuranceReceiptAlert"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertProjectDelivery(List<ProjectDelivery> projectDeliveries) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (ProjectDelivery projectDelivery : projectDeliveries) {
				String reference = "";
				boolean projectFlag = true;
				if (projectDelivery.getProjectTaskSurrogate().getProject() != null) {
					reference = projectDelivery.getProjectTaskSurrogate()
							.getProject().getReferenceNumber();
				} else if (projectDelivery.getProjectTaskSurrogate()
						.getProjectTask() != null) {
					reference = projectDelivery.getProjectTaskSurrogate()
							.getProjectTask().getProject().getReferenceNumber();
					projectFlag = false;
				}
				String message = "";
				message = "Project Delivery for reference ( "
						+ reference
						+ " -- "
						+ projectDelivery.getReferenceNumber()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat.convertSystemDateToString(projectDelivery
								.getDeliveryDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(projectDelivery.getProjectDeliveryId());
				alert.setTableName(projectDelivery.getClass().getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(projectDelivery.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(ProjectDelivery.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}
				if (projectFlag)
					screen = systemService
							.getScreenBasedOnScreenName("ProjectEntryByDelivery");
				else
					screen = systemService
							.getScreenBasedOnScreenName("ProjectTaskEntryByDelivery");

				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("ProjectDelivery"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertProjectMileStone(List<ProjectMileStone> projectMileStones) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (ProjectMileStone projectMileStone : projectMileStones) {
				String message = "";
				/*
				 * message = "Project MileStone for reference ( " +
				 * projectMileStone.getReferenceNumber() + " ) " +
				 * " Ending on ( " +
				 * DateFormat.convertSystemDateToString(projectMileStone
				 * .getEndDate()) + " )"; Alert alert = new Alert();
				 * alert.setMessage(message);
				 * alert.setRecordId(projectMileStone.getProjectMilestoneId());
				 * alert
				 * .setTableName(projectMileStone.getClass().getSimpleName());
				 * Role role = new Role(); Screen screen = new Screen();
				 * implementation = new Implementation(); implementation =
				 * systemService
				 * .getImplementation(projectMileStone.getImplementation()); if
				 * (implementation != null)
				 * role.setRoleId(implementation.getHrRoleToalert());
				 * alert.setRoleId(role.getRoleId()); screen = systemService
				 * .getScreenBasedOnScreenName("ProjectMileStoneAlert");
				 * alert.setScreenId(screen.getScreenId());
				 * alert.setIsActive(true); resultList.add(alert);
				 * deliveryList.add(alert.getRecordId() + "@" +
				 * alert.getTableName());
				 */
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("ProjectMileStone"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertProjectPayment(
			List<ProjectPaymentSchedule> projectPaymentDetails) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (ProjectPaymentSchedule projectPaymentDetail : projectPaymentDetails) {
				String message = "";
				message = "Project Payment Receipt for payment titled ( "
						+ (projectPaymentDetail.getProjectTaskSurrogate()
								.getProject() != null ? projectPaymentDetail
								.getProjectTaskSurrogate().getProject()
								.getReferenceNumber()
								+ " - " : "")

						+ (projectPaymentDetail.getProjectTaskSurrogate()
								.getProject() != null ? projectPaymentDetail
								.getProjectTaskSurrogate().getProject()
								.getProjectTitle() : "")
						+ " ) "
						+ " Dated on ( "
						+ DateFormat
								.convertSystemDateToString(projectPaymentDetail
										.getPaymentDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(projectPaymentDetail
						.getProjectPaymentScheduleId());
				alert.setTableName(projectPaymentDetail.getClass()
						.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(projectPaymentDetail
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(ProjectPaymentSchedule.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ProjectPaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("ProjectPaymentShedule"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertProjectDepositPayment(List<Project> projects) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();
			Set<String> alertList = new HashSet<String>();

			for (Project project : projects) {
				String message = "";
				message = "Project Security Deposit for payment titled ( "
						+ project.getProjectTitle()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat.convertSystemDateToString(project
								.getDepositDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(project.getProjectId());
				alert.setTableName(Project.class.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(project
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Project.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("ProjectDepositAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName() + "@" + alert.getScreenId());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Project"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName() + "@" + alert2.getScreenId());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertDirectPaymentProcess(List<DirectPayment> directPayments) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (DirectPayment directPayment : directPayments) {
				String message = "";
				if (null != directPayment.getChequeDate()
						&& !("").equals(directPayment.getChequeDate())) {
					message = "Direct Payment process has been added for Payment Number ( "
							+ directPayment.getPaymentNumber()
							+ " ) "
							+ "& Cheque Date ( "
							+ DateFormat
									.convertSystemDateToString(directPayment
											.getChequeDate()) + " )";
				} else {
					message = "Direct Payment process has been added for Payment Number ( "
							+ directPayment.getPaymentNumber()
							+ " ) "
							+ "& Payment Date ( "
							+ DateFormat
									.convertSystemDateToString(directPayment
											.getPaymentDate()) + " )";
				}
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(directPayment.getDirectPaymentId());
				alert.setTableName("DirectPayment");
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(directPayment
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(DirectPayment.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("DirectPaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultset.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("DirectPayment"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertDirectPaymentPDCCheque(List<DirectPayment> directPayments,
			ConfigurationVO configurationVO) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();
			Set<String> alertList = new HashSet<String>();
			String message = "";
			for (DirectPayment directPayment : directPayments) {
				double totalAmount = 0;
				for (DirectPaymentDetail dpDT : directPayment
						.getDirectPaymentDetails())
					totalAmount += dpDT.getAmount();
				message = "Direct Payment of Payment Number ("
						+ directPayment.getPaymentNumber()
						+ ")"
						+ ", Cheque Date ("
						+ DateFormat.convertSystemDateToString(directPayment
								.getChequeDate()) + ") , Cheque Number ("
						+ directPayment.getChequeNumber()
						+ ") & Cheque Amount of ("
						+ AIOSCommons.formatAmount(totalAmount)
						+ ") is near to process.";

				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(directPayment.getDirectPaymentId());
				alert.setTableName("DirectPayment");
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(directPayment
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(
						DirectPayment.class.getSimpleName(), implementation);

				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("DirectPaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("DirectPayment"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0) {
				alertService.saveAlert(resultset);
				try {
					if (null != configurationVO
							&& null != configurationVO.getEmailEnabled()
							&& configurationVO.getEmailEnabled())
						alertEmail(resultset, "PDC Payment Cheque Process",
								configurationVO.getProperties());
					if (null != configurationVO
							&& null != configurationVO.getSmsEnabled()
							&& configurationVO.getSmsEnabled())
						alertSMS(resultset, configurationVO.getProperties());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertBankReceiptsPDCCheque(
			List<BankReceiptsDetail> bankReceiptsDetails,
			ConfigurationVO configurationVO) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();
			Set<String> alertList = new HashSet<String>();
			String message = "";
			for (BankReceiptsDetail bankReceiptsDetail : bankReceiptsDetails) {
				message = "Receipt of Reference Number ("
						+ bankReceiptsDetail.getBankReceipts().getReceiptsNo()
						+ ")"
						+ ", Cheque Date ("
						+ DateFormat
								.convertSystemDateToString(bankReceiptsDetail
										.getChequeDate())
						+ ") & Cheque Number ("
						+ bankReceiptsDetail.getBankRefNo()
						+ ") is near to process.";

				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(bankReceiptsDetail.getBankReceiptsDetailId());
				alert.setTableName(BankReceiptsDetail.class.getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService
						.getImplementation(bankReceiptsDetail.getBankReceipts()
								.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(
						BankReceipts.class.getSimpleName(), implementation);

				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("BankReceiptsAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals(
						BankReceiptsDetail.class.getSimpleName()))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0) {
				alertService.saveAlert(resultset);
				try {
					if (null != configurationVO
							&& null != configurationVO.getEmailEnabled()
							&& configurationVO.getEmailEnabled())
						alertEmail(resultset, "PDC Receivable Cheque Process",
								configurationVO.getProperties());
					if (null != configurationVO
							&& null != configurationVO.getSmsEnabled()
							&& configurationVO.getSmsEnabled())
						alertSMS(resultset, configurationVO.getProperties());
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void alertEmail(Set<Alert> resultset, String subject,
			Properties confProps) {
		try {
			List<UserRole> userRoles = null;
			List<String> recipientEmails = null;
			long implementationId = 0l;
			for (Alert alt : resultset) {
				if (null != alt.getRoleId()) {
					userRoles = systemService.getUserRoleByRoleId(alt
							.getRoleId());
					recipientEmails = new ArrayList<String>();
					for (UserRole userRole : userRoles) {
						implementationId = userRole.getUser()
								.getImplementationId();
						if (null != userRole.getUser().getEmailAddress())
							recipientEmails.add(userRole.getUser()
									.getEmailAddress());
					}
					notifyEnterpriseService.sendEmail(subject,
							alt.getMessage(), recipientEmails, "en",
							implementationId, null, null, confProps);
				} else {
					User user = systemService.getActiveUserByPersonId(alt
							.getPersonId());
					notifyEnterpriseService.sendEmail(subject,
							alt.getMessage(), user.getEmailAddress(), "en",
							user.getImplementationId(), alt.getPersonId(),
							null, confProps);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void alertSMS(Set<Alert> resultset, Properties confProps) {
		try {
			List<UserRole> userRoles = null;
			List<String> recipientNumbers = null;
			long implementationId = 0l;
			for (Alert alt : resultset) {
				if (null != alt.getRoleId()) {
					userRoles = systemService.getUserRoleByRoleId(alt
							.getRoleId());
					recipientNumbers = new ArrayList<String>();
					for (UserRole userRole : userRoles) {
						implementationId = userRole.getUser()
								.getImplementationId();
						if (null != userRole.getUser().getMobileNumber())
							recipientNumbers.add(userRole.getUser()
									.getMobileNumber());
					}
					notifyEnterpriseService.sendSMS(alt.getMessage(),
							recipientNumbers, "en", implementationId, null,
							null, confProps);
				} else {
					User user = systemService.getActiveUserByPersonId(alt
							.getPersonId());
					notifyEnterpriseService.sendSMS(alt.getMessage(),
							user.getMobileNumber(), "en",
							user.getImplementationId(), alt.getPersonId(),
							null, confProps);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void alertPurchasePaymentProcess(List<Payment> purchasePaymentList) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (Payment payment : purchasePaymentList) {
				String message = "";
				if (null != payment.getChequeDate()
						&& !("").equals(payment.getChequeDate())) {
					message = "Purchase Payment process has been added for Payment Number ( "
							+ payment.getPaymentNumber()
							+ " ) "
							+ "& Cheque Date ( "
							+ DateFormat.convertSystemDateToString(payment
									.getChequeDate()) + " )";
				} else {
					message = "Purchase Payment process has been added for Payment Number ( "
							+ payment.getPaymentNumber()
							+ " ) "
							+ "& Payment Date ( "
							+ DateFormat.convertSystemDateToString(payment
									.getDate()) + " )";
				}
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(payment.getPaymentId());
				alert.setTableName("Payment");
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(payment
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Payment.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("PurchasePaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultset.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Payment"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertPurchaseInvoiceProcess(List<Invoice> invoiceList) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (Invoice invoice : invoiceList) {
				String message = "Purchase invoice has been added for Invoice Number ( "
						+ invoice.getInvoiceNumber() + " ) ";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(invoice.getInvoiceId());
				alert.setTableName("Invoice");
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(invoice
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Invoice.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("PurchaseInvoiceAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultset.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Invoice"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertDirectPurchasePaymentProcess(Purchase purchase) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			String message = "";

			message = "Purchase Payment process has been added for Payment Number ( "
					+ purchase.getPurchaseNumber() + " ) ";

			Alert alert = new Alert();
			alert.setMessage(message);
			alert.setRecordId(purchase.getPurchaseId());
			alert.setTableName("Purchase");
			Screen screen = new Screen();
			implementation = new Implementation();
			implementation = systemService.getImplementation(purchase
					.getImplementation());

			// Alert Role Access
			AlertRole alertRole = getRoleIdByModuleProcess(Purchase.class
					.getName());
			if (alertRole.getRole() != null) {
				alert.setRoleId(alertRole.getRole().getRoleId());
			} else {
				alert.setPersonId(alertRole.getPersonId());
			}

			screen = systemService
					.getScreenBasedOnScreenName("DirectPurchaseAlert");
			alert.setScreenId(screen.getScreenId());
			alert.setIsActive(true);
			resultset.add(alert);
			paymentList.add(alert.getRecordId() + "@" + alert.getTableName());

			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Purchase"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void alertMaterialRequisitionProcess(
			MaterialRequisition materialRequisition) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			String message = "";

			message = "Material Requisition has been created from store >> "
					+ materialRequisition.getStore().getStoreName()
					+ " for Reference Number ("
					+ materialRequisition.getReferenceNumber() + ") ";

			Alert alert = new Alert();
			alert.setMessage(message);
			alert.setRecordId(materialRequisition.getMaterialRequisitionId());
			alert.setTableName(MaterialRequisition.class.getSimpleName());
			Screen screen = new Screen();
			implementation = new Implementation();
			implementation = systemService
					.getImplementation(materialRequisition.getImplementation());

			// Alert Role Access
			AlertRole alertRole = getRoleIdByModuleProcess(MaterialRequisition.class
					.getName());
			if (alertRole.getRole() != null) {
				alert.setRoleId(alertRole.getRole().getRoleId());
			} else {
				alert.setPersonId(alertRole.getPersonId());
			}

			screen = systemService
					.getScreenBasedOnScreenName("MaterialRequisitionAlert");
			alert.setScreenId(screen.getScreenId());
			alert.setIsActive(true);
			resultset.add(alert);
			paymentList.add(alert.getRecordId() + "@" + alert.getTableName());

			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals(
						MaterialRequisition.class.getSimpleName()))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void salesReturnDirectPayment(List<Credit> credits) {
		try {
			Set<Alert> resultset = new HashSet<Alert>();
			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> deliveryList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();
			for (Credit credit : credits) {
				String message = "";
				message = "Sales Return Payment Receipt for reference ( "
						+ credit.getCreditNumber()
						+ " ) "
						+ " Dated on ( "
						+ DateFormat.convertSystemDateToString(credit
								.getCreatedDate()) + " )";
				Alert alert = new Alert();
				alert.setMessage(message);
				alert.setRecordId(credit.getCreditId());
				alert.setTableName(credit.getClass().getSimpleName());
				Screen screen = new Screen();
				implementation = new Implementation();
				implementation = systemService.getImplementation(credit
						.getImplementation());

				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Credit.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				screen = systemService
						.getScreenBasedOnScreenName("SalesReturnPaymentAlert");
				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				deliveryList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = alertService.getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Credit"))
					alertList.add(alert2.getRecordId() + "@"
							+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(deliveryList);
			for (String string : deliveryList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				alertService.saveAlert(resultset);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<EMIDetailVO> buildSchedule(Loan loan) throws Exception {
		if (loan.getEmiType().equals("P")) {
			predictableEMI(loan);
		} else {
			unPredictableEMI(loan);
		}
		return emiVOList;
	}

	private void predictableEMI(Loan loan) throws Exception {
		predictableLoan(loan);
	}

	private void unPredictableEMI(Loan loan) throws Exception {
		unPredictableLoan(loan);
	}

	private void predictableLoan(Loan loan) throws Exception {
		emiVO = new EMIDetailVO();
		if (loan.getEmiFrequency() == 'M') {
			emiVO.setPaymentFor("month ");
			processEMIMonthly(loan);
		} else if (loan.getEmiFrequency() == 'Y') {
			emiVO.setPaymentFor("annually ");
			processEMIYearly(loan);
		} else if (loan.getEmiFrequency() == 'D') {
			emiVO.setPaymentFor("day ");
			processEMIDaily(loan);
		} else if (loan.getEmiFrequency() == 'W') {
			emiVO.setPaymentFor("week ");
			processEMIWeekly(loan);
		} else if (loan.getEmiFrequency() == 'H') {
			emiVO.setPaymentFor("semi annually ");
			processEMISemiAunnaul(loan);
		} else if (loan.getEmiFrequency() == 'Q') {
			emiVO.setPaymentFor("quarterly ");
			processEMIQuaterly(loan);
		}
	}

	private void unPredictableLoan(Loan loan) throws Exception {
		emiVO = new EMIDetailVO();
		if (loan.getEmiFrequency() == 'M') {
			emiVO.setPaymentFor("month ");
			processUnPredictEMIMonthly(loan);
		} else if (loan.getEmiFrequency() == 'Y') {
			emiVO.setPaymentFor("annually ");
			processUnPredictEMIYearly(loan);
		} else if (loan.getEmiFrequency() == 'D') {
			emiVO.setPaymentFor("day ");
			processUnPredictEMIDaily(loan);
		} else if (loan.getEmiFrequency() == 'W') {
			emiVO.setPaymentFor("week ");
			processUnPredictEMIWeekly(loan);
		} else if (loan.getEmiFrequency() == 'H') {
			emiVO.setPaymentFor("semi annually ");
			processUnPredictEMISemiAunnaul(loan);
		} else if (loan.getEmiFrequency() == 'Q') {
			emiVO.setPaymentFor("quarterly ");
			processUnPredictEMIQuaterly(loan);
		}
	}

	private EMIDetailVO processEMIMonthly(Loan loan) throws Exception {
		int monthVariance = 0;
		monthVariance = DateFormat.monthsBetween(loan.getEmiEndDate(),
				loan.getEmiFirstDuedate());
		Date nextMonth = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= monthVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextMonth) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextMonth);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextMonth);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextMonth;
				nextMonth = DateFormat.addMonth(nextMonth);
				emiSetValues(loan, currentMonth, nextMonth);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processEMIYearly(Loan loan) throws Exception {
		int yearVariance = 0;
		yearVariance = DateFormat.yearDifference(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextYear = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= yearVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextYear) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextYear);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextYear);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextYear;
				nextYear = DateFormat.addMonth(nextYear);
				emiSetValues(loan, currentMonth, nextYear);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processEMIDaily(Loan loan) throws Exception {
		long dayVariance = 0;
		dayVariance = DateFormat.dayVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextDay = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= dayVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextDay) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextDay);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextDay);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextDay;
				nextDay = DateFormat.addMonth(nextDay);
				emiSetValues(loan, currentMonth, nextDay);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processEMIWeekly(Loan loan) throws Exception {
		int weekVariance = 0;
		weekVariance = DateFormat.weekVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextWeek = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= weekVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextWeek) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextWeek);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextWeek);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextWeek;
				nextWeek = DateFormat.addMonth(nextWeek);
				emiSetValues(loan, currentMonth, nextWeek);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processEMISemiAunnaul(Loan loan) throws Exception {
		int semiVariance = 0;
		semiVariance = DateFormat.halfYearlyVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextSemi = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= semiVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextSemi) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextSemi);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextSemi);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextSemi;
				nextSemi = DateFormat.addMonth(nextSemi);
				emiSetValues(loan, currentMonth, nextSemi);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processEMIQuaterly(Loan loan) throws Exception {
		int quaterVariance = 0;
		quaterVariance = DateFormat.quaterVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextQuater = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= quaterVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextQuater) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextQuater);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextQuater);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				Date currentMonth = nextQuater;
				nextQuater = DateFormat.addMonth(nextQuater);
				emiSetValues(loan, currentMonth, nextQuater);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMIMonthly(Loan loan) throws Exception {
		int monthVariance = 0;
		Date nextMonth = loan.getEmiFirstDuedate();
		monthVariance = DateFormat.monthsBetween(loan.getEmiEndDate(),
				loan.getEmiFirstDuedate());
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= monthVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextMonth) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextMonth);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextMonth);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextMonth = DateFormat.addMonth(nextMonth);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMIYearly(Loan loan) throws Exception {
		int yearVariance = 0;
		yearVariance = DateFormat.yearDifference(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextYear = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= yearVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextYear) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextYear);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextYear);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextYear = DateFormat.addMonth(nextYear);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMIQuaterly(Loan loan) throws Exception {
		int quaterVariance = 0;
		quaterVariance = DateFormat.quaterVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextQuater = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= quaterVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextQuater) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextQuater);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextQuater);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextQuater = DateFormat.addMonth(nextQuater);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMISemiAunnaul(Loan loan)
			throws Exception {
		int semiVariance = 0;
		semiVariance = DateFormat.halfYearlyVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextSemi = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= semiVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextSemi) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextSemi);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextSemi);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextSemi = DateFormat.addMonth(nextSemi);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMIWeekly(Loan loan) throws Exception {
		int weekVariance = 0;
		weekVariance = DateFormat.weekVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextWeek = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= weekVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextWeek) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextWeek);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextWeek);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextWeek = DateFormat.addMonth(nextWeek);
			}
		}
		return emiVO;
	}

	private EMIDetailVO processUnPredictEMIDaily(Loan loan) throws Exception {
		long dayVariance = 0;
		dayVariance = DateFormat.dayVariance(loan.getEmiFirstDuedate(),
				loan.getEmiEndDate());
		Date nextDay = loan.getEmiFirstDuedate();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 5);
		Date currentDate = cal.getTime();
		for (int i = 0; i <= dayVariance; i++) {
			if (DateFormat.dayVariance(currentDate, nextDay) <= 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(nextDay);
				SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
				String dateVal = inFormat.format(nextDay);
				emiVO.setEmiDueDate(DateFormat.convertDateToString(dateVal));
				nextDay = DateFormat.addMonth(nextDay);
			}
		}
		return emiVO;
	}

	private void emiSetValues(Loan loan, Date currentDate, Date nextInstalment)
			throws Exception {
		emiVO.setAmount(roundValue(getInstallment(loan)));
	}

	private double roundValue(double amount) {
		final int precision = 2;
		BigDecimal returnValue = new BigDecimal(amount).setScale(precision,
				BigDecimal.ROUND_HALF_UP);
		return returnValue.doubleValue();
	}

	private double getInstallment(Loan loan) throws Exception {
		double loanInterest = loan.getRate();
		double loanCharges = 0d;
		for (LoanCharge list : chargeList) {
			if (list.getType() == 'I') {
				loanCharges += list.getValue();
			}
		}
		loanInterest = loanInterest + loanCharges;
		loanInterest /= 100;
		int yearVariance = 0;
		yearVariance = DateFormat.yearDifference(loan.getEmiStartDate(),
				loan.getEmiEndDate());
		return (loanInterest * loan.getAmount() / 12)
				/ (1.0 - Math.pow(((loanInterest / 12) + 1.0),
						(-(12 * yearVariance))));
	}

	private boolean checkAlreadyPaymentsDone(Loan loan) {
		try {
			List<EmiDetail> emiList = emiDetailDAO.findByNamedQuery(
					"checkEMIPaid", loan.getLoanId(),
					getCurrentPaymentDate(loan.getEmiFirstDuedate()));
			if (null != emiList && emiList.size() > 0)
				return false;
			else
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean checkPayments(Date emiFirstDuedate) {
		if (paymentDueDiff(emiFirstDuedate) == 0) {
			return true;
		} else {
			return false;
		}
	}

	private long paymentDueDiff(Date emiDueDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 5);
		Date checkDate = calendar.getTime();
		Calendar emiCalendar = Calendar.getInstance();
		emiCalendar.setTime(emiDueDate);
		int calendarMonth = calendar.get(Calendar.MONTH);
		int calendarYear = calendar.get(Calendar.YEAR);
		emiCalendar.set(Calendar.YEAR, calendarYear);
		emiCalendar.set(Calendar.MONTH, calendarMonth);
		emiDueDate = emiCalendar.getTime();
		return DateFormat.dayVariance(checkDate, emiDueDate);
	}

	private Date getCurrentPaymentDate(Date emiDueDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 5);
		Calendar emiCalendar = Calendar.getInstance();
		emiCalendar.setTime(emiDueDate);
		int calendarMonth = calendar.get(Calendar.MONTH);
		int calendarYear = calendar.get(Calendar.YEAR);
		emiCalendar.set(Calendar.YEAR, calendarYear);
		emiCalendar.set(Calendar.MONTH, calendarMonth);
		return emiCalendar.getTime();
	}

	public void saveAlerts(Object object, Alert alert) throws Exception {
		String[] name = object.getClass().getName().split("entity.");
		alert.setTableName(name[name.length - 1]);
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		alert.setRecordId((Long) method.invoke(object, null));
		Set<Alert> resultset = new HashSet<Alert>();
		resultset.add(alert);
		alertService.saveAlert(resultset);
	}

	public void alertCommonUpdate(Long alertId) throws Exception {
		Alert alert = alertService.getAlertInfo(alertId);
		alert.setIsActive(false);
		alertService.updateAlert(alert);
	}

	public List<Alert> getAlertList(List<UserRole> userRoles) throws Exception {
		List<Alert> alerts = new ArrayList<Alert>();
		for (UserRole userRole : userRoles) {
			List<Alert> alertlist = alertService.getAlertsRoleBased(userRole);
			if (alertlist != null && alertlist.size() > 0)
				alerts.addAll(alertlist);
		}
		return alerts;
	}

	public void commonSaveAlert(Alert alert) throws Exception {
		Set<Alert> resultset = new HashSet<Alert>();
		resultset.add(alert);
		alertService.saveAlert(resultset);
	}

	public void commonSaveAllAlert(List<Alert> alerts) throws Exception {
		Set<Alert> resultset = new HashSet<Alert>(alerts);
		alertService.saveAlert(resultset);
	}

	/*
	 * public RealEstateEnterpriseService getRealEstateEnterpriseService() {
	 * return realEstateEnterpriseService; } public void
	 * setRealEstateEnterpriseService( RealEstateEnterpriseService
	 * realEstateEnterpriseService) { this.realEstateEnterpriseService =
	 * realEstateEnterpriseService; }
	 */

	// Alert Role Mapping area
	public List<Object> getAllAlertRoles() throws Exception {
		List<AlertRole> alertRoles = alertService
				.getAlertRolesByImplementation(getImplemenation());
		List<Object> dataVOs = new ArrayList<Object>();
		if (null != alertRoles && alertRoles.size() > 0) {
			for (AlertRole alertRole : alertRoles)
				dataVOs.add(convertEntityTOVO(alertRole));
		}
		return dataVOs;
	}

	public AlertRoleVO convertEntityTOVO(AlertRole alertRole) {
		AlertRoleVO vo = new AlertRoleVO();

		if (alertRole.getPersonId() != null) {
			Person person = systemService.getPerson(alertRole.getPersonId());
			vo.setPersonName(person.getFirstName() + " " + person.getLastName());
			vo.setPersonId(person.getPersonId());
		}
		if (alertRole.getRole() != null) {
			vo.setRoleName(alertRole.getRole().getRoleName());
			vo.setRoleId(alertRole.getRole().getRoleId());
		}

		if (alertRole.getModuleProcess() != null) {
			vo.setScreenName(alertRole.getModuleProcess()
					.getProcessDisplayName());
			vo.setModuleProcessId(alertRole.getModuleProcess().getProcessId());
		}
		vo.setAlertRoleId(alertRole.getAlertRoleId());

		vo.setCreatedDateView(DateFormat.convertDateToString(alertRole
				.getCreatedDate().toString()));

		return vo;

	}

	public List<Person> getUserBasedEmployees() {
		List<Person> persons = null;
		try {
			persons = systemService.getAllEmployee(getImplemenation());
			List<User> users = systemService.getAllUsers(getImplemenation());
			Map<Long, Long> personIdMaps = new HashMap<Long, Long>();
			for (User user : users) {
				if (user.getPersonId() != null)
					personIdMaps.put(user.getPersonId(), user.getPersonId());
			}

			for (Person person : new ArrayList<Person>(persons)) {
				if (!personIdMaps.containsKey(person.getPersonId()))
					persons.remove(person);
			}
		} catch (Exception e) {

		}
		return persons;
	}

	// Identity Expire
	public void identityExpiryReminder() {
		try {
			// getImplementId();
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			cal.add(Calendar.DATE, 30);
			java.util.Date date = cal.getTime();

			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();

			List<Identity> identities = identityDAO.findByNamedQuery(
					"getIdentityExpiry", currentDate, date);
			for (Identity identity : identities) {
				String identityTypeName = "";
				if (identity.getLookupDetail() != null)
					identityTypeName = identity.getLookupDetail()
							.getDisplayName();
				String tmessage = identityTypeName
						+ " going to expire for ( "
						+ identity.getPerson().getFirstName()
						+ " "
						+ identity.getPerson().getLastName()
						+ " )"
						+ " Expiry Date ( "
						+ DateFormat.convertDateToString(identity
								.getExpireDate().toString()) + ")";
				Alert alert = new Alert();
				alert.setMessage(tmessage);
				alert.setRecordId(identity.getPerson().getPersonId());
				alert.setTableName("Identity");
				Screen screen = new Screen();
				implementation = new Implementation();
				if (identity.getPerson() != null) {
					if (identity.getPerson().getStatus() != null
							&& identity.getPerson().getStatus() != 1)
						continue;

					implementation = identity.getPerson().getImplementation();
					screen = systemService
							.getScreenBasedOnScreenName("PersonEdit");
				} else {
					implementation = identity.getCompany().getImplementation();
					screen = systemService
							.getScreenBasedOnScreenName("CompanyEdit");
				}
				implementation = systemService
						.getImplementation(implementation);
				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Identity.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}

				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = this.getAlertService()
					.getAlertAllByUseCase(Identity.class.getSimpleName());
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName()
						.equals(Identity.class.getSimpleName()))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			Set<Alert> resultset = new HashSet<Alert>();

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				this.getAlertService().saveAlert(resultset);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Identity Handover Notification or alert
	public void getIdentityHandOver() {
		try {
			// getImplementId();
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			cal.add(Calendar.DATE, 1);
			java.util.Date date = cal.getTime();

			List<Alert> resultList = new ArrayList<Alert>();
			Set<String> paymentList = new HashSet<String>();

			Set<String> alertList = new HashSet<String>();

			List<IdentityAvailability> identities = identityAvailabilityDAO
					.findByNamedQuery("getIdentityExpectedReturn", currentDate,
							date);
			for (IdentityAvailability identityAvail : identities) {
				String identityTypeName = "";
				String identityOf = "";
				String returnBy = ""; /*
									 * identityAvail.getPerson().getFirstName()
									 * + " " +
									 * identityAvail.getPerson().getLastName();
									 */
				if (identityAvail.getIdentity().getLookupDetail() != null)
					identityTypeName = identityAvail.getIdentity()
							.getLookupDetail().getDisplayName();
				if (identityAvail.getIdentity().getPerson() != null) {
					identityOf = identityAvail.getIdentity().getPerson()
							.getFirstName()
							+ " "
							+ identityAvail.getIdentity().getPerson()
									.getLastName();
				} else {
					identityOf = identityAvail.getIdentity().getCompany()
							.getCompanyName();
				}
				String tmessage = identityTypeName
						+ " of ( "
						+ identityOf
						+ ") "
						+ " needs to return from ( "
						+ returnBy
						+ " )"
						+ " Expected Return Date ( "
						+ DateFormat.convertDateToString(identityAvail
								.getExpectedReturnDate().toString()) + ")";
				Alert alert = new Alert();
				alert.setMessage(tmessage);
				alert.setRecordId(identityAvail.getIdentityAvailabilityId());
				alert.setTableName("IdentityAvailability");
				Screen screen = new Screen();
				implementation = systemService.getImplementation(identityAvail
						.getImplementation());
				// Alert Role Access
				AlertRole alertRole = getRoleIdByModuleProcess(Identity.class
						.getName());
				if (alertRole.getRole() != null) {
					alert.setRoleId(alertRole.getRole().getRoleId());
				} else {
					alert.setPersonId(alertRole.getPersonId());
				}
				screen = systemService
						.getScreenBasedOnScreenName("IdentityExpectedReturnAlert");

				alert.setScreenId(screen.getScreenId());
				alert.setIsActive(true);
				resultList.add(alert);
				paymentList.add(alert.getRecordId() + "@"
						+ alert.getTableName());
			}
			List<Alert> alertExsit = this.getAlertService().getAlerts();
			for (Alert alert2 : alertExsit) {
				if (alert2.getTableName().equals("Identity"))
					;
				alertList.add(alert2.getRecordId() + "@"
						+ alert2.getTableName());
			}

			Set<String> pl = new HashSet<String>(paymentList);
			for (String string : paymentList) {
				for (String string1 : alertList) {
					if (string1.equalsIgnoreCase(string)) {
						pl.remove(string);
					}
				}
			}

			Set<Alert> resultset = new HashSet<Alert>();

			for (String string : pl) {
				String[] strAry = string
						.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				for (Alert cp : resultList) {
					if (cp.getRecordId() == Integer.parseInt(strAry[0])) {
						resultset.add(cp);
					}
				}

			}
			if (resultset != null && resultset.size() > 0)
				this.getAlertService().saveAlert(resultset);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public AlertRole getRoleIdByModuleProcess(String moduleProcess) {
		List<AlertRole> alertRoles = alertService
				.getAlertAccessByUseCase(moduleProcess);
		AlertRole alertRole = null;
		if (alertRoles == null || alertRoles.size() == 0) {
			alertRole = new AlertRole();
			Role role = new Role();
			role.setRoleId(getImplemenation().getReceivableClerk());
			alertRole.setRole(role);
		} else {
			alertRole = alertRoles.get(0);
		}
		return alertRole;
	}

	public AlertRole getRoleIdByModuleProcess(String moduleProcess,
			Implementation implementation) {
		List<AlertRole> alertRoles = alertService
				.getModuleProcessByUseCaseImpl(moduleProcess,
						implementation.getImplementationId());
		AlertRole alertRole = null;
		if (alertRoles == null || alertRoles.size() == 0) {
			alertRole = new AlertRole();
			Role role = new Role();
			role.setRoleId(implementation.getReceivableClerk());
			alertRole.setRole(role);
		} else {
			alertRole = alertRoles.get(0);
		}
		return alertRole;
	}

	public void commonAlertDeleteByUsecaseAndRecord(String useCase,
			Long recordId) throws Exception {
		List<Alert> alerts = alertService.getAlertAllRecordInfo(recordId,
				useCase);
		alertService.deleteAlert(alerts);
	}

	public AlertService getAlertService() {
		return alertService;
	}

	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

	public AIOTechGenericDAO<ContractPayment> getContractPaymentDAO() {
		return contractPaymentDAO;
	}

	public void setContractPaymentDAO(
			AIOTechGenericDAO<ContractPayment> contractPaymentDAO) {
		this.contractPaymentDAO = contractPaymentDAO;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AIOTechGenericDAO<Loan> getLoanDAO() {
		return loanDAO;
	}

	public void setLoanDAO(AIOTechGenericDAO<Loan> loanDAO) {
		this.loanDAO = loanDAO;
	}

	public AIOTechGenericDAO<EmiDetail> getEmiDetailDAO() {
		return emiDetailDAO;
	}

	public void setEmiDetailDAO(AIOTechGenericDAO<EmiDetail> emiDetailDAO) {
		this.emiDetailDAO = emiDetailDAO;
	}

	public List<EMIDetailVO> getEmiVOList() {
		return emiVOList;
	}

	public void setEmiVOList(List<EMIDetailVO> emiVOList) {
		this.emiVOList = emiVOList;
	}

	public double getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(double loanBalance) {
		this.loanBalance = loanBalance;
	}

	public EMIDetailVO getEmiVO() {
		return emiVO;
	}

	public void setEmiVO(EMIDetailVO emiVO) {
		this.emiVO = emiVO;
	}

	public double getEmiCumulative() {
		return emiCumulative;
	}

	public void setEmiCumulative(double emiCumulative) {
		this.emiCumulative = emiCumulative;
	}

	public int getVarianceDays() {
		return varianceDays;
	}

	public void setVarianceDays(int varianceDays) {
		this.varianceDays = varianceDays;
	}

	public int getYearlyNoDate() {
		return yearlyNoDate;
	}

	public void setYearlyNoDate(int yearlyNoDate) {
		this.yearlyNoDate = yearlyNoDate;
	}

	public AIOTechGenericDAO<LoanCharge> getLoanChargeDAO() {
		return loanChargeDAO;
	}

	public void setLoanChargeDAO(AIOTechGenericDAO<LoanCharge> loanChargeDAO) {
		this.loanChargeDAO = loanChargeDAO;
	}

	public List<LoanCharge> getChargeList() {
		return chargeList;
	}

	public void setChargeList(List<LoanCharge> chargeList) {
		this.chargeList = chargeList;
	}

	public AIOTechGenericDAO<Contract> getContractDAO() {
		return contractDAO;
	}

	public void setContractDAO(AIOTechGenericDAO<Contract> contractDAO) {
		this.contractDAO = contractDAO;
	}

	public AIOTechGenericDAO<DirectPayment> getDirectPaymentDAO() {
		return directPaymentDAO;
	}

	public void setDirectPaymentDAO(
			AIOTechGenericDAO<DirectPayment> directPaymentDAO) {
		this.directPaymentDAO = directPaymentDAO;
	}

	public AIOTechGenericDAO<Identity> getIdentityDAO() {
		return identityDAO;
	}

	public void setIdentityDAO(AIOTechGenericDAO<Identity> identityDAO) {
		this.identityDAO = identityDAO;
	}

	public AIOTechGenericDAO<BankReceiptsDetail> getBankReceiptsDetailDAO() {
		return bankReceiptsDetailDAO;
	}

	public void setBankReceiptsDetailDAO(
			AIOTechGenericDAO<BankReceiptsDetail> bankReceiptsDetailDAO) {
		this.bankReceiptsDetailDAO = bankReceiptsDetailDAO;
	}

	public Implementation getImplemenation() {
		if (implementation == null
				|| implementation.getImplementationId() == null) {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
		}
		return implementation;
	}

	public AIOTechGenericDAO<IdentityAvailability> getIdentityAvailabilityDAO() {
		return identityAvailabilityDAO;
	}

	public void setIdentityAvailabilityDAO(
			AIOTechGenericDAO<IdentityAvailability> identityAvailabilityDAO) {
		this.identityAvailabilityDAO = identityAvailabilityDAO;
	}

	public AIOTechGenericDAO<BankReceipts> getBankReceiptsDAO() {
		return bankReceiptsDAO;
	}

	public void setBankReceiptsDAO(
			AIOTechGenericDAO<BankReceipts> bankReceiptsDAO) {
		this.bankReceiptsDAO = bankReceiptsDAO;
	}

	public NotifyEnterpriseService getNotifyEnterpriseService() {
		return notifyEnterpriseService;
	}

	@Autowired
	public void setNotifyEnterpriseService(
			NotifyEnterpriseService notifyEnterpriseService) {
		this.notifyEnterpriseService = notifyEnterpriseService;
	}

}
