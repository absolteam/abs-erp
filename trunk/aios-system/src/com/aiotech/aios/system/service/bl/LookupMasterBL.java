package com.aiotech.aios.system.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.system.service.LookupMasterService;

/**
 * @author Mohamed Rabeek
 * 
 */
public class LookupMasterBL {
	private LookupMasterService lookupMasterService;
	private Implementation implementation;

	public List<LookupDetail> getActiveLookupDetails(String code,
			boolean isImplementation) {
		List<LookupDetail> lookupDetails = null;
		List<LookupMaster> lookupMasters = null;
		if (isImplementation)
			lookupMasters = lookupMasterService.getLookupInformation(code,
					getImplementation());
		else
			lookupMasters = lookupMasterService.getLookupInformation(code);

		if (lookupMasters != null && lookupMasters.size() > 0) {
			lookupDetails = new ArrayList<LookupDetail>();
			for (LookupDetail lookupDetail : lookupMasters.get(0)
					.getLookupDetails()) {
				if (lookupDetail.getIsActive() != null
						&& lookupDetail.getIsActive())
					lookupDetails.add(lookupDetail);
			}
		}
		if (lookupDetails != null)
			Collections.sort(lookupDetails, new Comparator<LookupDetail>() {
				public int compare(LookupDetail m1, LookupDetail m2) {
					return m1.getDisplayName().compareTo(m2.getDisplayName());
				}
			});
		return lookupDetails;
	}

	public List<LookupDetail> getActiveLookupDetails(String code,
			Implementation implementation) {
		List<LookupDetail> lookupDetails = null;
		List<LookupMaster> lookupMasters = null;

		lookupMasters = lookupMasterService.getLookupInformation(code,
				implementation);

		if (lookupMasters != null && lookupMasters.size() > 0) {
			lookupDetails = new ArrayList<LookupDetail>();
			for (LookupDetail lookupDetail : lookupMasters.get(0)
					.getLookupDetails()) {
				if (lookupDetail.getIsActive() != null
						&& lookupDetail.getIsActive())
					lookupDetails.add(lookupDetail);
			}
		}
		if (lookupDetails != null)
			Collections.sort(lookupDetails, new Comparator<LookupDetail>() {
				public int compare(LookupDetail m1, LookupDetail m2) {
					return m1.getDisplayName().compareTo(m2.getDisplayName());
				}
			});
		return lookupDetails;
	}

	public String saveLookupDetail(LookupDetail lookupDetail,
			List<LookupDetail> exsitLookupDetails) {
		String returnMessage = "SUCCESS";
		for (LookupDetail lookupDetailTemp : exsitLookupDetails) {

			if (lookupDetail.getLookupDetailId() != null
					&& lookupDetailTemp.getLookupDetailId().equals(
							lookupDetail.getLookupDetailId()))
				continue;

			if (lookupDetailTemp.getAccessCode() != null
					&& lookupDetail.getAccessCode() != null
					&& lookupDetailTemp.getAccessCode().equalsIgnoreCase(
							lookupDetail.getAccessCode()))
				return "Code Exist already (" + lookupDetail.getAccessCode()
						+ ")";

			if (lookupDetailTemp.getDataId() != null
					&& lookupDetail.getDataId() != null
					&& lookupDetailTemp.getDataId().equals(
							lookupDetail.getDataId()))
				return "ID Exist already (" + lookupDetail.getDataId() + ")";

		}

		lookupMasterService.saveLookupDetail(lookupDetail);

		return returnMessage;
	}

	public LookupMasterService getLookupMasterService() {
		return lookupMasterService;
	}

	public void setLookupMasterService(LookupMasterService lookupMasterService) {
		this.lookupMasterService = lookupMasterService;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

}
