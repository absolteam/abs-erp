package com.aiotech.aios.system.service.bl;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AlertTrigger implements ServletContextListener {
	public void contextDestroyed(ServletContextEvent event) {
		//
	}
 
	public void contextInitialized(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		 try {
			 WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(context);
			 AlertBL alertBL = (AlertBL) ctx
				.getBean("alertBL");
	    	//specify sceduler task details
	    	JobDetail job = new JobDetail();
	    	job.setName("alertJob");
	    	job.setJobClass(AlertJob.class);
	    	job.setGroup("CCD");
	    	Map dataMap = job.getJobDataMap();
	    	dataMap.put("contractChequeDeposit", alertBL);
	    	
	    	
	    	//configure the scheduler time
	    	CronTrigger trigger = new CronTrigger();
	    	trigger.setName("alertJob");
	    	
	    	//That'll run a job at 11am and 5pm. That's twice a day
			trigger.setCronExpression("0 0 11,17 * * ?");
			
	    	trigger.setGroup("CCD");
	    	
	    	//schedule it
	    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	    	scheduler.start();
	    	scheduler.scheduleJob(job, trigger);
	    	
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

	
}
