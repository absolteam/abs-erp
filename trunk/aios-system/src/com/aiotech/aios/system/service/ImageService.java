package com.aiotech.aios.system.service;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ImageService {

	AIOTechGenericDAO<Image> imageDAO;

	public List<Image> getRecordIdBasedImageListByImplementation(Long recordId,
			Implementation implementation, String entity) {

		return imageDAO.findByNamedQuery(
				"getRecordIdBasedImageListByImplementation", recordId, entity,
				implementation);
	}

	public List<Image> getImagesByRecordId(long recordId, String tableName) {
		/*
		 * Image image = new Image(); image.setRecordId(recordId); return
		 * imageDAO.findByExample(image);
		 */
		return imageDAO.findByNamedQuery("getImagesByRecordId", recordId,
				tableName);
	}

	public List<Image> getImagesByImplementation(Long implementationId) {
		/*
		 * Image image = new Image(); image.setRecordId(recordId); return
		 * imageDAO.findByExample(image);
		 */
		return imageDAO.findByNamedQuery("getImagesByImplementation",
				implementationId);
	}

	public List<Image> getImageListByImageIds(String ids) {
		/*
		 * Image image = new Image(); image.setRecordId(recordId); return
		 * imageDAO.findByExample(image);
		 */
		return imageDAO.findByNamedQuery("getImageListByImageIds", ids);
	}

	public List<Image> getEntityBasedImageListByImplementation(
			Implementation implementation, String entity) {

		return imageDAO.findByNamedQuery(
				"getEntityBasedImageListByImplementation", implementation,
				entity);
	}

	public Image getImagesByFileHash(String hashName) {
		/*
		 * Image image = new Image(); image.setRecordId(recordId); return
		 * imageDAO.findByExample(image);
		 */
		List<Image> images = imageDAO.findByNamedQuery("getImagesByFileHash",
				hashName);
		if (images != null && images.size() > 0)
			return images.get(0);
		else
			return null;
	}

	public AIOTechGenericDAO<Image> getImageDAO() {
		return imageDAO;
	}

	public void setImageDAO(AIOTechGenericDAO<Image> imageDAO) {
		this.imageDAO = imageDAO;
	}

	public List<Image> getRecordImages(Image image) {
		return imageDAO.findByExample(image);
	}

	public void deleteImage(Image img) {
		imageDAO.delete(img);
	}

	public void deleteImages(List<Image> images) {
		imageDAO.deleteAll(images);
	}

	public void saveImages(List<Image> imageList) {
		for (Image image : imageList) {
			image.setImplementation(getImplementation());
		}
		getImageDAO().saveOrUpdateAll(imageList);
	}

	public void saveImage(Image image) {
		image.setImplementation(getImplementation());
		imageDAO.save(image);
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

}
