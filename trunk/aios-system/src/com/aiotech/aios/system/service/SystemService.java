package com.aiotech.aios.system.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Authorities;
import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.I18NMessages;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.Reference;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.aios.system.domain.entity.SystemVersion;
import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.MenuRight;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class SystemService {

	private AIOTechGenericDAO<Country> countryDAO;
	private AIOTechGenericDAO<State> stateDAO;
	private AIOTechGenericDAO<City> cityDAO;
	private AIOTechGenericDAO<Menu> menuDAO;
	private AIOTechGenericDAO<MenuRight> menuRightDAO;
	private AIOTechGenericDAO<UserRole> userRoleDAO;
	private AIOTechGenericDAO<User> userDAO;
	private AIOTechGenericDAO<Implementation> implementationDAO;
	private AIOTechGenericDAO<Role> roleDAO;
	private AIOTechGenericDAO<Authorities> authoritiesDAO;
	private AIOTechGenericDAO<Reference> referenceDAO;
	private AIOTechGenericDAO<Screen> screenDAO;
	private AIOTechGenericDAO<Message> messageDAO;
	private AIOTechGenericDAO<I18NMessages> i18NMessageDAO;
	private AIOTechGenericDAO<POSUserTill> pOSUserTillDAO;
	private AIOTechGenericDAO<JobAssignment> jobAssignmentDAO;
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<ModuleProcess> moduleProcessDAO;
	private AIOTechGenericDAO<SystemVersion> systemVersionDAO;

	public SystemVersion getSystemVersion() {

		List<SystemVersion> systemVersions = systemVersionDAO.getAll();
		return (systemVersions == null) ? null : systemVersions.get(0);
	}

	public Implementation getImplementationByCompanyKey(String key) {

		List<Implementation> implementations = implementationDAO
				.findByNamedQuery("getImplementationByCompanyKey", key);

		if (implementations != null && implementations.size() != 0)
			return implementations.get(0);

		return null;
	}

	public void saveOrUpdateImplementation(Implementation implementation)
			throws Exception {
		implementationDAO.saveOrUpdate(implementation);
	}

	public AIOTechGenericDAO<I18NMessages> getI18NMessageDAO() {
		return i18NMessageDAO;
	}

	public void setI18NMessageDAO(AIOTechGenericDAO<I18NMessages> i18nMessageDAO) {
		i18NMessageDAO = i18nMessageDAO;
	}

	public AIOTechGenericDAO<Reference> getReferenceDAO() {
		return referenceDAO;
	}

	public void setReferenceDAO(AIOTechGenericDAO<Reference> referenceDAO) {
		this.referenceDAO = referenceDAO;
	}

	public AIOTechGenericDAO<Authorities> getAuthoritiesDAO() {
		return authoritiesDAO;
	}

	public void setAuthoritiesDAO(AIOTechGenericDAO<Authorities> authoritiesDAO) {
		this.authoritiesDAO = authoritiesDAO;
	}

	public AIOTechGenericDAO<Role> getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(AIOTechGenericDAO<Role> roleDAO) {
		this.roleDAO = roleDAO;
	}

	public List<Country> getAllCountries() throws Exception {
		return countryDAO.getAll();
	}

	public List<I18NMessages> getI18NMessages(String locale) {
		return i18NMessageDAO.findByNamedQuery("getI18NMessages", locale);
	}

	public Authorities getAuthorities(String user) {
		return authoritiesDAO.findByNamedQuery("getAuthorities", user).get(0);
	}

	public Person getPerson(long personId) {
		return personDAO.findById(personId);
	}

	public State getStateById(long id) {
		return stateDAO.findById(id);
	}

	public City getCityById(long id) {
		return cityDAO.findById(id);
	}

	public Country getCountryById(long id) {
		return countryDAO.findById(id);
	}

	public void deleteAuthorities(Authorities authorities) {
		authoritiesDAO.delete(authorities);
	}

	public User getUser(String id) {
		List<User> users = userDAO.findByNamedQuery(
				"getUserWithOutImplementation", id);
		return (users != null && users.size() > 0) ? users.get(0) : null;
	}

	public User getUserByAdminFlag(Long implementationId) {
		List<User> users = userDAO.findByNamedQuery("getUserByAdminFlag",
				implementationId);
		return (users != null && users.size() > 0) ? users.get(0) : null;
	}

	public User getUser(String id, Implementation implementation) {
		return userDAO.findByNamedQuery("getUsers", id, implementation.getImplementationId()).get(0);
	}

	public void deleteUser(String id, Implementation implementation) {
		userDAO.delete(this.getUser(id, implementation));
	}

	public void saveOrUpdateUser(User user) throws Exception {
		userDAO.saveOrUpdate(user);
	}

	public void addUserRole(UserRole userRole) throws Exception {
		userRoleDAO.saveOrUpdate(userRole);
	}

	public void addAuthorities(Authorities authority) throws Exception {
		authoritiesDAO.saveOrUpdate(authority);
	}

	public void deleteUserRoles(List<UserRole> userRoles) throws Exception {
		userRoleDAO.deleteAll(userRoles);
	}

	public void deleteUserRole(UserRole userRole) throws Exception {
		userRoleDAO.delete(userRole);
	}

	public UserRole getUserRole(long id) throws Exception {
		return userRoleDAO.findById(id);
	}

	public UserRole getUserRole(String userName, Long roleId) throws Exception {
		List<UserRole> userRoles = new ArrayList<UserRole>();
		userRoles = userRoleDAO.findByNamedQuery("getUserRole", userName,
				roleId);
		if (userRoles != null && userRoles.size() > 0)
			return userRoles.get(0);
		else
			return null;
	}
	
	public List<UserRole> getUserRoleByRoleId(Long roleId) throws Exception {
		return userRoleDAO.findByNamedQuery("getUserRoleByRoleId",
				roleId); 
	}

	public Role getRole(long id) throws Exception {
		return roleDAO.findById(id);
	}

	public List<Role> getAllRoles(Implementation implementation)
			throws Exception {
		return roleDAO.findByNamedQuery("getAllRoles",
				implementation.getImplementationId());
	}

	public List<Role> getRolesByPersons(String personIdsTemp) throws Exception {
		List<Long> personIds = new ArrayList<Long>();
		for (String string : personIdsTemp.split(",")) {
			if (string != null && !string.equals(""))
				personIds.add(Long.valueOf(string));
		}
		Query query = roleDAO.getNamedQuery("getRolesByPersons");
		query.setParameterList("personIds", personIds);
		return query.list();

	}

	public List<User> getAllUsers(Implementation implementation)
			throws Exception {
		return userDAO.findByNamedQuery("getAllUsers",
				implementation.getImplementationId());
	}

	public User getActiveUserByPersonId(Long personId) throws Exception {
		List<User> users = userDAO.findByNamedQuery("getActiveUserByPersonId",
				personId);
		if (users != null && users.size() > 0)
			return users.get(0);
		else
			return null;
	}

	public List<City> getAllCities() throws Exception {
		return cityDAO.getAll();
	}

	public List<Implementation> getImplementationGroup(
			List<Long> implementationIds) throws Exception {

		Query query = implementationDAO.getNamedQuery("getImplementationGroup");
		query.setParameterList("implementationIds", implementationIds);

		return query.list();

	}

	public Implementation getImplementation(Implementation implementation)
			throws Exception {
		return implementationDAO.findById(implementation.getImplementationId());
	}

	public List<Implementation> getAllImplementation() throws Exception {
		return implementationDAO.getAll();
	}

	public List<State> getAllStates() throws Exception {
		return stateDAO.getAll();
	}

	public List<State> getCountryStates(long countryId) throws Exception {
		return stateDAO.findByNamedQuery("getCountryStates", countryId);
	}

	public List<City> getStateCities(long stateId) throws Exception {
		return cityDAO.findByNamedQuery("getStateCities", stateId);
	}

	public List<Menu> getParentMenuItems() throws Exception {
		return menuDAO.findByNamedQuery("getParentMenuItems");
	}

	public List<Menu> getChildMenuItems(long parentId) throws Exception {
		return menuDAO.findByNamedQuery("getChildMenuItems", parentId);
	}

	public List<MenuRight> getMenuRightsForUserRole(long roleId)
			throws Exception {
		return menuRightDAO.findByNamedQuery("getRoleMenuRights", roleId);
	}

	public List<UserRole> getUserRoles(String userName) throws Exception {
		return userRoleDAO.findByNamedQuery("getUserRoles", userName);
	}

	public List<UserRole> getAllUserRole(Implementation implementation)
			throws Exception {
		return userRoleDAO.findByNamedQuery("getAllUserRole", implementation);
	}

	public List<Reference> getAllReferences(Implementation implementation)
			throws Exception {
		return referenceDAO
				.findByNamedQuery("getAllReferences", implementation);
	}

	public List<Reference> getReferenceAgainstUsecase(
			Implementation implementation, String usecase) throws Exception {
		return referenceDAO.findByNamedQuery("getReferenceAgainstUsecase",
				usecase, implementation);
	}

	@SuppressWarnings("unchecked")
	public List<Reference> getReferenceAgainstUsecase(
			Implementation implementation, String usecase, Session session)
			throws Exception {
		String getReferenceAgainstUsecase = "SELECT r FROM Reference r "
				+ "WHERE r.usecase = ? AND r.implementation = ?";
		List<Reference> references = session
				.createQuery(getReferenceAgainstUsecase).setString(0, usecase)
				.setEntity(1, implementation).list();
		return references;
	}

	public Screen getScreenBasedOnScreenName(String screenName)
			throws Exception {
		List<Screen> screens = new ArrayList<Screen>();
		screens = screenDAO.findByNamedQuery("getScreenBasedOnScreenName",
				screenName);
		if (screens != null && screens.size() > 0)
			return screens.get(0);
		else
			return null;
	}

	public Message getMessageToAvoidDublicate(Long recordId, String useCase,
			Long personId, String dataSpecific, Byte processType)
			throws Exception {
		List<Message> messages = new ArrayList<Message>();
		messages = messageDAO.findByNamedQuery("getMessageToAvoidDublicate",
				recordId, useCase, personId, dataSpecific, processType);
		if (messages != null && messages.size() > 0)
			return messages.get(0);
		else
			return null;
	}

	public List<Message> getMessageToAvoidDublicate(String useCase,
			String dataSpecific) throws Exception {

		return messageDAO.findByNamedQuery("getMessageToAvoidSameMessage",
				useCase, dataSpecific);

	}

	public List<Message> getWorkFlowDetailMessageInfo(Long recordId,
			String useCase) throws Exception {
		return messageDAO.findByNamedQuery("getWorkFlowDetailMessageInfo",
				useCase, recordId);
	}

	public List<Person> getAllEmployee(Implementation implementation) {
		return personDAO.findByNamedQuery("getAllEmployee", implementation);
	}

	public void saveMessages(List<Message> messages) throws Exception {

		messageDAO.saveOrUpdateAll(messages);
	}

	public void addOrUpdateReference(Reference reference) throws Exception {
		referenceDAO.saveOrUpdate(reference);
	}

	public void deleteReference(Reference reference) throws Exception {
		referenceDAO.delete(reference);
	}

	public Screen getScreenById(Long screenId) throws Exception {
		return screenDAO.findById(screenId);
	}

	public List<ModuleProcess> getAllModuleProcess() throws Exception {
		return moduleProcessDAO.findByNamedQuery("getAllModuleProcesses");
	}

	public AIOTechGenericDAO<Menu> getMenuDAO() {
		return menuDAO;
	}

	public void setMenuDAO(AIOTechGenericDAO<Menu> menuDAO) {
		this.menuDAO = menuDAO;
	}

	public AIOTechGenericDAO<MenuRight> getMenuRightDAO() {
		return menuRightDAO;
	}

	public void setMenuRightDAO(AIOTechGenericDAO<MenuRight> menuRightDAO) {
		this.menuRightDAO = menuRightDAO;
	}

	public AIOTechGenericDAO<Country> getCountryDAO() {
		return countryDAO;
	}

	public void setCountryDAO(AIOTechGenericDAO<Country> countryDAO) {
		this.countryDAO = countryDAO;
	}

	public AIOTechGenericDAO<State> getStateDAO() {
		return stateDAO;
	}

	public void setStateDAO(AIOTechGenericDAO<State> stateDAO) {
		this.stateDAO = stateDAO;
	}

	public AIOTechGenericDAO<City> getCityDAO() {
		return cityDAO;
	}

	public void setCityDAO(AIOTechGenericDAO<City> cityDAO) {
		this.cityDAO = cityDAO;
	}

	public AIOTechGenericDAO<UserRole> getUserRoleDAO() {
		return userRoleDAO;
	}

	public void setUserRoleDAO(AIOTechGenericDAO<UserRole> userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}

	public AIOTechGenericDAO<Implementation> getImplementationDAO() {
		return implementationDAO;
	}

	public void setImplementationDAO(
			AIOTechGenericDAO<Implementation> implementationDAO) {
		this.implementationDAO = implementationDAO;
	}

	public AIOTechGenericDAO<User> getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(AIOTechGenericDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

	public AIOTechGenericDAO<Screen> getScreenDAO() {
		return screenDAO;
	}

	public void setScreenDAO(AIOTechGenericDAO<Screen> screenDAO) {
		this.screenDAO = screenDAO;
	}

	public AIOTechGenericDAO<Message> getMessageDAO() {
		return messageDAO;
	}

	public void setMessageDAO(AIOTechGenericDAO<Message> messageDAO) {
		this.messageDAO = messageDAO;
	}

	public AIOTechGenericDAO<POSUserTill> getpOSUserTillDAO() {
		return pOSUserTillDAO;
	}

	public void setpOSUserTillDAO(AIOTechGenericDAO<POSUserTill> pOSUserTillDAO) {
		this.pOSUserTillDAO = pOSUserTillDAO;
	}

	public AIOTechGenericDAO<JobAssignment> getJobAssignmentDAO() {
		return jobAssignmentDAO;
	}

	public void setJobAssignmentDAO(
			AIOTechGenericDAO<JobAssignment> jobAssignmentDAO) {
		this.jobAssignmentDAO = jobAssignmentDAO;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<ModuleProcess> getModuleProcessDAO() {
		return moduleProcessDAO;
	}

	public void setModuleProcessDAO(
			AIOTechGenericDAO<ModuleProcess> moduleProcessDAO) {
		this.moduleProcessDAO = moduleProcessDAO;
	}

	public AIOTechGenericDAO<SystemVersion> getSystemVersionDAO() {
		return systemVersionDAO;
	}

	public void setSystemVersionDAO(
			AIOTechGenericDAO<SystemVersion> systemVersionDAO) {
		this.systemVersionDAO = systemVersionDAO;
	}
}
