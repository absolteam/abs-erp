package com.aiotech.aios.system.service.bl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.system.to.EmailVO;

public class SendHTMLEmail {
	public static boolean sendHtmlEmail(EmailVO emailVO) {

		try {
			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");

			String protocol = confProps.getProperty("email.protocol");
			String host = confProps.getProperty("email.host");
			String auth = confProps.getProperty("email.auth");
			String port = confProps.getProperty("email.port");
			String socketFactoryPort = confProps
					.getProperty("email.socketFactoryPort");
			String socketFactoryClass = confProps
					.getProperty("email.socketFactoryClass");
			String socketFactoryFallback = confProps
					.getProperty("email.socketFactoryFallback");
			String quitwait = confProps.getProperty("email.quitwait");
			final String emailFrom = confProps.getProperty("email.from");
			final String password = confProps.getProperty("email.password");
			String emailTo = null;
			if (emailVO.getTo() != null && !emailVO.getTo().equals(""))
				emailTo = emailVO.getTo();
			else
				emailTo = emailFrom;

			Properties props = new Properties();

			props.setProperty("mail.transport.protocol", protocol);
			props.setProperty("mail.host", host);
			props.put("mail.smtp.auth", auth);
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.socketFactory.port", socketFactoryPort);
			props.put("mail.smtp.socketFactory.class", socketFactoryClass);
			props.put("mail.smtp.socketFactory.fallback", socketFactoryFallback);
			props.put("mail.smtp.starttls.enable", "true");

			props.setProperty("mail.smtp.quitwait", quitwait);
			Session mailSession = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(emailFrom,
									password);
						}
					});
			MimeMessage message = new MimeMessage(mailSession);

			message.setFrom(new InternetAddress(emailFrom, emailFrom));
			message.setSubject(emailVO.getSubject(), "UTF-8");
			message.setContent(emailVO.getMessage(), "text/html; charset=UTF-8");
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(
					emailTo));

			Transport transport = mailSession.getTransport("smtps");
			transport.connect(host, emailFrom, password);

			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}