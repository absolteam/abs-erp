package com.aiotech.aios.system.service.bl;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.hibernate.Session;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.Reference;
import com.aiotech.aios.system.service.SystemService;

public class ReferenceManagerBL {

	SystemService systemService;

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public String getReferenceStamp(String usecase,
			Implementation implementation) {

		try {
			Reference reference = new Reference();
			String refrenceStr="";
			reference = systemService.getReferenceAgainstUsecase(
					implementation, usecase).get(0);
			Long refNumb=null;
			boolean isStart=true;
			if(reference!=null && reference.getRefNumber()!=null)
				refNumb=reference.getRefNumber();
			
			if(reference.getIsStarted()==null || reference.getIsStarted()==false)
				isStart=false;
			
			if (generationPossible(reference)){
				refrenceStr=generateUpdatedReferenceNumber(reference);
					reference.setRefNumber(refNumb);
				return refrenceStr;
			}else if (intializationPossible(reference)){
				refrenceStr=intializeNGenerateUpdatedReference(reference);
				reference.setRefNumber(refNumb);
				reference.setIsStarted(isStart);
				return refrenceStr;
			}else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getReferenceStamp(String usecase,
			Implementation implementation, Session session) {

		try {
			Reference reference = new Reference();
			String refrenceStr="";
			reference = systemService.getReferenceAgainstUsecase(
					implementation, usecase, session).get(0);
			Long refNumb=null;
			boolean isStart=true;
			if(reference!=null && reference.getRefNumber()!=null)
				refNumb=reference.getRefNumber();
			
			if(reference.getIsStarted()==null || reference.getIsStarted()==false)
				isStart=false;
			
			if (generationPossible(reference)){
				refrenceStr=generateUpdatedReferenceNumber(reference);
					reference.setRefNumber(refNumb);
				return refrenceStr;
			}else if (intializationPossible(reference)){
				refrenceStr=intializeNGenerateUpdatedReference(reference);
				reference.setRefNumber(refNumb);
				reference.setIsStarted(isStart);
				return refrenceStr;
			}else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String saveReferenceStamp(String usecase,
			Implementation implementation) {

		try {
			Reference reference = new Reference();
			reference = systemService.getReferenceAgainstUsecase(
					implementation, usecase).get(0);
			if (generationPossible(reference))
				return generateUpdatedReferenceNumber(reference);
			else if (intializationPossible(reference))
				return intializeNGenerateUpdatedReference(reference);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String saveReferenceStamp(String usecase,
			Implementation implementation, Session session) { 
		try {
			Reference reference = new Reference();
			reference = systemService.getReferenceAgainstUsecase(
					implementation, usecase, session).get(0);
			if (generationPossible(reference))
				return generateUpdatedReferenceNumber(reference);
			else if (intializationPossible(reference))
				return intializeNGenerateUpdatedReference(reference);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Boolean generationPossible(Reference reference) {

		return (reference.getIsStarted() != null
				&& reference.getIsStarted() == true
				&& reference.getRefNumber() != null
				&& reference.getRefNumber() != 0 && reference.getRefDigits() != null);
	}

	private Boolean intializationPossible(Reference reference) {

		return ((reference.getIsStarted() == null || reference.getIsStarted() == false)
				&& (reference.getRefNumber() == null || reference
						.getRefNumber() == 0) && reference.getRefDigits() != null);
	}

	private String generateUpdatedReferenceNumber(Reference reference)
			throws Exception {

		reference.setRefNumber(reference.getRefNumber() + 1);
		long refNo = reference.getRefNumber();
		String generatedRefNo = null;
		int temp;

		if ((refNo + "").length() < reference.getRefDigits()) {

			temp = reference.getRefDigits() - (refNo + "").length();
			generatedRefNo = new String();
			while (generatedRefNo.length() < temp)
				generatedRefNo = generatedRefNo + "0";
			generatedRefNo = generatedRefNo.concat((refNo + ""));
		} else
			generatedRefNo = refNo + "";

		return generateReferenceString(reference, generatedRefNo);
	}
	
	
	private String intializeNGenerateUpdatedReference(Reference reference)
			throws Exception {

		long refNo = 1;
		String generatedRefNo = null;
		int temp;

		if ((refNo + "").length() < reference.getRefDigits()) {

			temp = reference.getRefDigits() - (refNo + "").length();
			generatedRefNo = new String();
			while (generatedRefNo.length() < temp)
				generatedRefNo = generatedRefNo + "0";
			generatedRefNo = generatedRefNo.concat((refNo + ""));
		} else
			generatedRefNo = refNo + "";

		reference.setIsStarted(true);
		reference.setRefNumber(refNo);
		return generateReferenceString(reference, generatedRefNo);
	}

	public String generateReferenceString(Reference reference,
			String generatedRefNo) throws Exception {

		String[] referenceString = new String[] { "XX$", "XX$", "XX$", "XX$",
				"XX$" };
		String toReturn = "";
		int i = 0;
		if(reference.getPrefix()!=null && !reference.getPrefix().trim().equals(""))
			referenceString[0] = reference.getPrefix();
		
		if(generatedRefNo!=null && !generatedRefNo.trim().equals(""))
			referenceString[reference.getRefNumberPosition()] = generatedRefNo;
		
		if(!reference.getDateTag().trim().equals("") && !reference.getDateTag().trim().equals("XX$"))
			referenceString[reference.getDateTagPosition()] = getFormatedDate(reference
					.getDateTag());
		
		if(reference.getPrefix()!=null && !reference.getPrefix().trim().equals(""))
			referenceString[reference.getSymbolTagPosition()] = reference
				.getSymbolTag();
		referenceString[4] = reference.getPostfix();

		while (i < referenceString.length) {
			if (!referenceString[i].equals("XX$"))
				toReturn += referenceString[i++] + "-";
			else
				i++;
		}
		return AIOSCommons.removeTrailingSymbols(toReturn);
	}
	
	
	private String getFormatedDate(String requiredFormat) {

		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter;
		try {
			formatter = new SimpleDateFormat(requiredFormat);
		} catch (Exception e) {
			e.printStackTrace();
			formatter = new SimpleDateFormat("MMddyyyy");
		}
		return formatter.format(currentDate.getTime());

	}
}