package com.aiotech.aios.system.service;

import java.util.List;

import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DirectoryService {
	
	private AIOTechGenericDAO<Directory> directoryDAO;

	public AIOTechGenericDAO<Directory> getDirectoryDAO() {
		return directoryDAO;
	}

	public void setDirectoryDAO(AIOTechGenericDAO<Directory> directoryDAO) {
		this.directoryDAO = directoryDAO;
	}
	
	public void persist(Directory directory){
		directoryDAO.persist(directory);
	}
	
	public Directory getDirectoryByName(Directory dir){
		List<Directory> dirList = directoryDAO.findByNamedQuery("findByDirName",dir.getDirectoryName(),
				dir.getRecordId(), dir.getTableName(),dir.getDisplayPane());
		if (dirList != null && dirList.size() > 0){
			return dirList.get(0);
		} else {
			return new Directory();
		}
	}

	public List<Directory> getDirsNDocs(Directory dir){
		return directoryDAO.findByNamedQuery("getDirsNDocs",dir.getRecordId(),dir.getTableName(),dir.getDisplayPane());
	}
	
	public Directory getDirectoryNDocs(Directory dir){
		List<Directory> dirList = directoryDAO.findByNamedQuery("getDirsNDocsById",dir.getDirectoryId(),dir.getRecordId(),dir.getTableName(),dir.getDisplayPane());
		if (null != dirList && dirList.size()> 0){
			return dirList.get(0);
		} else {
			return null;
		}
	}
	
	public List<Directory> getSubDirs(Directory dir){ // to check for -> WHERE d.directory.directoryId = ?
		return  directoryDAO.findByNamedQuery("getSubDirs",dir.getDirectoryId(),dir.getRecordId(),dir.getTableName(),dir.getDisplayPane());
	}

	public void deleteDirectories (List<Directory> dirs){
		directoryDAO.deleteAll(dirs);
	}
	public void deleteDirectory(Directory dir){
		directoryDAO.delete(dir);
	}
	
	public Directory getParentDir(Directory dir){
		List<Directory> dirList = directoryDAO.findByNamedQuery("getParentDir", dir.getDirectoryId(),dir.getRecordId(),dir.getTableName(),dir.getDisplayPane());
		if (null != dirList && dirList.size() > 0){
			return dirList.get(0);	
		}else {
			return null;
		}
	}
	
	public Directory getDirectoryById (Directory dir){
		return directoryDAO.findById(dir.getDirectoryId());
	}
	
	public Directory getDirectoryById (long dirId){
		return directoryDAO.findById(dirId);
	}
	 
	public List<Directory> getDirectoryByChildId (Directory dir){
		try{
			return directoryDAO.findByNamedQuery("getChildDirs", dir.getDirectoryId());
		}
		catch(Exception e){
			return null;
		}
	}
	
}

