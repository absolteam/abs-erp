package com.aiotech.aios.system.service;

import java.util.List;

import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CommentService {
	private AIOTechGenericDAO<Comment> commentDAO;
	
	public void saveComment(Comment comment){
		commentDAO.saveOrUpdate(comment);
	}
	
	public void saveComment(List<Comment> commentList){
		commentDAO.saveOrUpdateAll(commentList);
	}
	
	public List<Comment> getUseCaseComment(Comment comment) throws Exception {
		return commentDAO.findByNamedQuery("getUseCaseComment", comment.getRecordId(),  comment.getUseCase());
	}
	
	public List<Comment> getEntityComment(String useCase) throws Exception {
		return commentDAO.findByNamedQuery("getEntityComment", useCase);
	}

	public AIOTechGenericDAO<Comment> getCommentDAO() {
		return commentDAO;
	}

	public void setCommentDAO(AIOTechGenericDAO<Comment> commentDAO) {
		this.commentDAO = commentDAO;
	}
}
