package com.aiotech.aios.system.service.bl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AlertJob implements Job {
	public void execute(JobExecutionContext context)
			throws JobExecutionException {

		Map dataMap = context.getJobDetail().getJobDataMap();
		AlertBL cCDTask = (AlertBL) dataMap.get("contractChequeDeposit");
		// Rent Schedule
		Map rentPaymentMap = context.getJobDetail().getJobDataMap();
		AlertBL rentTask = (AlertBL) dataMap.get("rentPaymentSchedule");
		if (null != cCDTask) {
			cCDTask.alertEMIPayments();
			cCDTask.contractExpiry();
			cCDTask.alertDirectPayment();
			//cCDTask.identityExpiryReminder();
 		} else if (null != rentPaymentMap) {
 			rentTask.rentPaymentSchedule();
 		}
	}
}
