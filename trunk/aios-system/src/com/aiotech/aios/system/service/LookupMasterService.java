package com.aiotech.aios.system.service;

import java.util.List;

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Mohamed Rabeek
 * 
 */
public class LookupMasterService {

	private AIOTechGenericDAO<LookupMaster> lookupMasterDAO;
	private AIOTechGenericDAO<LookupDetail> lookupDetailDAO;

	public List<LookupMaster> getLookupInformation(String code,
			Implementation implementation) {
		return lookupMasterDAO.findByNamedQuery("getLookupInformation", code,
				implementation);
	}

	public List<LookupDetail> getLookupDetailsList(Long lookupMasterId) {
		return lookupDetailDAO.findByNamedQuery("getLookupDetailsList",
				lookupMasterId);
	}

	public List<LookupMaster> getLookupInformation(String code) {
		return lookupMasterDAO.findByNamedQuery(
				"getLookupInformationWithOutImplementation", code);
	}

	public List<LookupMaster> getLookupListForImplementation(
			Implementation implementation) {
		return lookupMasterDAO.findByNamedQuery(
				"getLookupListForImplementation", implementation);
	}

	public LookupDetail getLookupDetailById(long lookupDetailId)
			throws Exception {
		return lookupDetailDAO.findById(lookupDetailId);
	}

	public LookupDetail getLookupDetailByCode(String detailCode, String masterCode,
			Implementation implementation) throws Exception {
		List<LookupDetail> lookupDetails = lookupDetailDAO.findByNamedQuery(
				"getLookupDetailByCode", detailCode, masterCode, implementation);
		return null != lookupDetails && lookupDetails.size() > 0 ? lookupDetails
				.get(0) : null;
	}

	public void saveLookupInformation(LookupMaster lookupMaster,
			List<LookupDetail> lookupDetails) {
		lookupMasterDAO.saveOrUpdate(lookupMaster);
		if (lookupDetails != null) {
			for (LookupDetail lookupDet : lookupDetails) {
				lookupDet.setLookupMaster(lookupMaster);
			}

			lookupDetailDAO.saveOrUpdateAll(lookupDetails);
		}
	}

	public void saveLookupDetail(LookupDetail lookupDetail) {
		lookupDetailDAO.saveOrUpdate(lookupDetail);
	}
	
	public void deleteLookupDetail(LookupDetail lookupDetail) {
		lookupDetailDAO.delete(lookupDetail);
	}

	public AIOTechGenericDAO<LookupMaster> getLookupMasterDAO() {
		return lookupMasterDAO;
	}

	public void setLookupMasterDAO(
			AIOTechGenericDAO<LookupMaster> lookupMasterDAO) {
		this.lookupMasterDAO = lookupMasterDAO;
	}

	public AIOTechGenericDAO<LookupDetail> getLookupDetailDAO() {
		return lookupDetailDAO;
	}

	public void setLookupDetailDAO(
			AIOTechGenericDAO<LookupDetail> lookupDetailDAO) {
		this.lookupDetailDAO = lookupDetailDAO;
	}

}
