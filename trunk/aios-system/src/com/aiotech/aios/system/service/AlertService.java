package com.aiotech.aios.system.service;

import java.util.List;
import java.util.Set;

import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.AlertRole;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class AlertService {
	private AIOTechGenericDAO<Alert> alertDAO;
	private AIOTechGenericDAO<AlertRole> alertRoleDAO;

	public List<Alert> getAlerts() throws Exception {
		return alertDAO.getAll();
	}

	public void saveAlert(Set<Alert> alerts) {
		alertDAO.saveOrUpdateAll(alerts);
	}

	public void updateAlert(Alert alert) {
		alertDAO.saveOrUpdate(alert);
	}

	public AIOTechGenericDAO<Alert> getAlertDAO() {
		return alertDAO;
	}

	public void deleteAlert(Alert alert) {
		alertDAO.delete(alert);
	}

	public void deleteAlert(List<Alert> alerts) {
		alertDAO.deleteAll(alerts);
	}

	public List<Alert> getDublicateAlerts(Alert alert) throws Exception {
		return alertDAO.findByNamedQuery("getDublicateAlerts",
				alert.getRecordId(), alert.getTableName());
	}

	public List<Alert> getAlertsRoleBased(UserRole userRole) throws Exception {
		return alertDAO.findByNamedQuery("getAlertsRoleBased", userRole
				.getRole().getRoleId());
	}

	public Alert getAlertRecordInfo(Long recordId, String tableName)
			throws Exception {
		List<Alert> alerts = alertDAO.findByNamedQuery("getAlertRecordBased",
				recordId, tableName);
		if (alerts != null && alerts.size() > 0)
			return alerts.get(0);
		else
			return null;
	}

	public List<Alert> getAlertAllRecordInfo(Long recordId, String tableName)
			throws Exception {
		return alertDAO.findByNamedQuery("getAlertRecordBased", recordId,
				tableName);

	}
	
	public List<Alert> getAlertAllByUseCase(String tableName)
	throws Exception {
		return alertDAO.findByNamedQuery("getAlertAllByUseCase",
		tableName);

}

	public List<Alert> findDublicateAlerts(Alert alert) throws Exception {
		return alertDAO.findByNamedQuery("findDublicateAlerts",
				alert.getRecordId(), alert.getTableName(), alert.getScreenId());
	}

	public Alert getAlertInfo(Long alertId) throws Exception {
		return alertDAO.findById(alertId);
	}

	// ALert role mapping sections
	public List<AlertRole> getAlertRolesByImplementation(
			Implementation implementation) throws Exception {
		return alertRoleDAO.findByNamedQuery("getAlertRolesByImplementation",
				implementation.getImplementationId());
	}

	public void saveAlertRole(AlertRole alertRole) {
		alertRoleDAO.saveOrUpdate(alertRole);
	}

	public AlertRole getAlertRoleInfoById(Long alertRoleId) throws Exception {
		List<AlertRole> alerts = alertRoleDAO.findByNamedQuery(
				"getAlertRoleInfoById", alertRoleId);
		if (alerts != null && alerts.size() > 0)
			return alerts.get(0);
		else
			return null;
	}

	public List<AlertRole> getAlertAccessByUseCase(String useCase) {
		return alertRoleDAO.findByNamedQuery("getModuleProcessByUseCase",
				useCase);
	}

	public List<AlertRole> getModuleProcessByUseCaseImpl(String useCase,
			Long implementationId) {
		return alertRoleDAO.findByNamedQuery("getModuleProcessByUseCaseImpl",
				implementationId, useCase);
	}

	public void deleteAlertRole(AlertRole alertRole) {
		alertRoleDAO.delete(alertRole);
	}

	public void setAlertDAO(AIOTechGenericDAO<Alert> alertDAO) {
		this.alertDAO = alertDAO;
	}

	public AIOTechGenericDAO<AlertRole> getAlertRoleDAO() {
		return alertRoleDAO;
	}

	public void setAlertRoleDAO(AIOTechGenericDAO<AlertRole> alertRoleDAO) {
		this.alertRoleDAO = alertRoleDAO;
	}
}
