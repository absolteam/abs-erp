package com.aiotech.aios.system.service;

import java.util.List;

import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class DocumentService {

	private AIOTechGenericDAO<Document> documentDAO;
	private AIOTechGenericDAO<Image> imageDAO;

	public List<Document> getChildDocumentsByRecordId(long recordId,
			String tableName) {
		return documentDAO.findByNamedQuery("getChildDocumentsByRecordId",
				recordId, tableName);
	}

	public List<Document> getParentDocumentsByRecordId(long recordId,
			String tableName) {
		return documentDAO.findByNamedQuery("getParentDocumentsByRecordId",
				recordId, tableName);
	}

	public Document getDocumentsByFileHash(String hashName) {
		List<Document> documents = documentDAO.findByNamedQuery(
				"getDocumentsByFileHash", hashName);
		if (documents != null && documents.size() > 0)
			return documents.get(0);
		else
			return null;
	}

	public AIOTechGenericDAO<Document> getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(AIOTechGenericDAO<Document> documentDAO) {
		this.documentDAO = documentDAO;
	}

	public AIOTechGenericDAO<Image> getImageDAO() {
		return imageDAO;
	}

	public void setImageDAO(AIOTechGenericDAO<Image> imageDAO) {
		this.imageDAO = imageDAO;
	}

	public void saveDocuments(Document parentDoc, List<Document> childDocs) {
		// getDocumentDAO().persist(parentDoc);
		getDocumentDAO().saveOrUpdateAll(childDocs);

	}

	public void saveDocument(Document docuemnt) {
		// getDocumentDAO().persist(parentDoc);
		documentDAO.save(docuemnt);

	}
	
	public void saveDocuments(List<Document> docuemnts) {
		// getDocumentDAO().persist(parentDoc);
		documentDAO.saveOrUpdateAll(docuemnts);

	}

	public void saveImages(List<Image> imageList) {
		getImageDAO().saveOrUpdateAll(imageList);
	}

	public List<Document> getRecordDocuments(Document document) {
		return documentDAO.findByExample(document);
	}

	public void deleteDocument(Document document) {
		documentDAO.delete(document);
	}

	public void deleteDocuments(List<Document> documents) {
		documentDAO.deleteAll(documents);
	}

	public List<Document> getDocumentsByRecordId(Long recordId, String tableName) {
		// Document document = new Document();
		// document.setRecordId(recordId);
		// return documentDAO.findByExample(document);
		return documentDAO.findByNamedQuery("getDocumentsByRecordId", recordId,
				tableName);
	}

	public List<Document> getSubDocs(Document doc) {
		return documentDAO.findByNamedQuery("getSubDocs", doc.getDocument()
				.getDocumentId(), doc.getRecordId(), doc.getTableName(), doc
				.getDisplayPane());
		// changed getDocument() -> getDocumentId()
	}

	public List<Document> getDirectoryDocs(Directory dir) {
		return documentDAO.findByNamedQuery("getDirDocs", dir.getDirectoryId(),
				dir.getRecordId(), dir.getTableName(), dir.getDisplayPane());
	}

	public Document getDocumentPath(long documentId) {
		return documentDAO.findById(documentId);
	}

	public List<Document> getDocumentWithDirectory(long documentId) {
		return documentDAO.findByNamedQuery("getDocumentWithDirectory",
				documentId);
	}

}
