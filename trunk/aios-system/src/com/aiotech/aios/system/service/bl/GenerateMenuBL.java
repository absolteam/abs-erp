package com.aiotech.aios.system.service.bl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.system.domain.entity.vo.MenuVO;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.MenuRight;
import com.aiotech.aios.workflow.domain.entity.UserRole;

public class GenerateMenuBL {

	private SystemService systemService;
	Map<String, Object> data;
	Map<String, String> quickLinkMap;

	public Map<String, Object> performBusinessLogic(String username)
			throws Exception {

		data = new HashMap<String, Object>();
		data.put("MENU", getMenuItems());
		data.put(
				"MENU_RIGHTS",
				mapMenuRights(getMenuRightsAgainstRoles(getUserRoles(username))));
		data.put("MENU_QUICK_LINKS", quickLinkMap);
		return data;
	}

	private List<MenuVO> getMenuItems() throws Exception {

		List<Menu> parentMenuItemsList = systemService.getParentMenuItems();
		List<MenuVO> parentMenuVOItemsList = new ArrayList<MenuVO>();
		List<MenuVO> reportingMenuVOItemsList = new ArrayList<MenuVO>();
		MenuVO menuVOItem;

		for (Menu menuItem : parentMenuItemsList) {

			menuVOItem = MenuVO.convertTOMenuVO(menuItem);
			if (menuVOItem.getIsReport() != null
					&& menuVOItem.getIsReport() == true) {

				fetchSubMenuItemsRecursively(menuVOItem);
				reportingMenuVOItemsList.add(menuVOItem);
			} else {

				fetchSubMenuItemsRecursively(menuVOItem);
				parentMenuVOItemsList.add(menuVOItem);
			}
		}

		data.put("REPORT_MENU", reportingMenuVOItemsList);
		return parentMenuVOItemsList;
	}

	private void fetchSubMenuItemsRecursively(MenuVO parentItem)
			throws Exception {

		List<Menu> childMenuItems = systemService.getChildMenuItems(parentItem
				.getMenuItemId());
		List<MenuVO> childMenuVOItemsList = new ArrayList<MenuVO>();
		MenuVO menuVOItem;

		if (childMenuItems != null && childMenuItems.size() > 0) {

			for (Menu subMenuItem : childMenuItems) {

				menuVOItem = MenuVO.convertTOMenuVO(subMenuItem);
				fetchSubMenuItemsRecursively(menuVOItem);
				childMenuVOItemsList.add(menuVOItem);
			}
		}

		parentItem.setMenus(new ArrayList<MenuVO>(childMenuVOItemsList));
	}

	private List<MenuRight> getMenuRightsAgainstRoles(List<UserRole> userRoles)
			throws Exception {

		List<MenuRight> combinedMenuRights = new ArrayList<MenuRight>();

		for (UserRole role : userRoles)
			combinedMenuRights.addAll(systemService
					.getMenuRightsForUserRole(role.getRole().getRoleId()));

		return combinedMenuRights;
	}

	private List<UserRole> getUserRoles(String userName) throws Exception {
		return systemService.getUserRoles(userName);
	}

	private Map<Long, Boolean> mapMenuRights(List<MenuRight> combinedMenuRights) {

		Map<Long, Boolean> menuRightsMap = new HashMap<Long, Boolean>();
		quickLinkMap = new HashMap<String, String>();

		for (MenuRight menuRight : combinedMenuRights) {

			menuRightsMap.put(menuRight.getMenu().getMenuItemId(), true);

			if (menuRight.getMenu().getIsQuickLink() != null
					&& menuRight.getMenu().getIsQuickLink()) {

				quickLinkMap.put(menuRight.getMenu().getItemTitle(), menuRight
						.getMenu().getScreen().getScreenPath());
			}
		}

		return menuRightsMap;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
}
