
package com.aiotech.aios.system.to;

/********************************************************************************************
*
* Create Log
* -------------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -------------------------------------------------------------------------------------------
* 1.0		31 Oct 2010 	Nagarajan T.	 		   	Initial Version
*********************************************************************************************/

import java.io.Serializable;
import java.util.List;

public class SyMenuTO implements Serializable{
	private int menuId;
	private String menuName;
	private String userMenuName;
	private String description;
	
	private int sequenceId;
	private String prompt;
	private int subMenuId;
	private int functionId;
	private String functionName;
	private String subMenuName;
	private int enabled;
	
	private int menuTrnVal;
	private int menuLineId;
	
	private Integer trnValue;
	private int sessionId;
	private String actionFlag; 
	private Integer actualLineId;
	private Integer tempLineId;
	
	private int headerFlag;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	
	
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	public Integer getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	public int getMenuLineId() {
		return menuLineId;
	}
	public void setMenuLineId(int menuLineId) {
		this.menuLineId = menuLineId;
	}
	public int getMenuTrnVal() {
		return menuTrnVal;
	}
	public void setMenuTrnVal(int menuTrnVal) {
		this.menuTrnVal = menuTrnVal;
	}
	public int getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	public int getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(int subMenuId) {
		this.subMenuId = subMenuId;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getUserMenuName() {
		return userMenuName;
	}
	public void setUserMenuName(String userMenuName) {
		this.userMenuName = userMenuName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public Integer getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(Integer actualLineId) {
		this.actualLineId = actualLineId;
	}
	
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getSubMenuName() {
		return subMenuName;
	}
	public void setSubMenuName(String subMenuName) {
		this.subMenuName = subMenuName;
	}
	
	

}
