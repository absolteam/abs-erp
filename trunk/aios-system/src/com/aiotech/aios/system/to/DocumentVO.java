package com.aiotech.aios.system.to;

import java.util.List;
import java.util.Map;

import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.system.domain.entity.Directory;

public class DocumentVO {

	private Map.Entry<String, FileVO> entry;
	private Object object;
	private Map<String, Directory> dirStruct;
	private Map<String, String> fileStruct;
	private Map<String, String> dirMap;
	public Map<String, String> getDirMap() {
		return dirMap;
	}
	public void setDirMap(Map<String, String> dirMap) {
		this.dirMap = dirMap;
	}
	private String displayPane;
	private List<Directory> dirList;
	private boolean isEdit;
	
	public boolean isEdit() {
		return isEdit;
	}
	public void setEdit(boolean isEdit) {
		this.isEdit = isEdit;
	}
	public List<Directory> getDirList() {
		return dirList;
	}
	public void setDirList(List<Directory> dirList) {
		this.dirList = dirList;
	}
	public String getDisplayPane() {
		return displayPane;
	}
	public void setDisplayPane(String displayPane) {
		this.displayPane = displayPane;
	}
	public Map.Entry<String, FileVO> getEntry() {
		return entry;
	}
	public void setEntry(Map.Entry<String, FileVO> entry) {
		this.entry = entry;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}	
	public Map<String, Directory> getDirStruct() {
		return dirStruct;
	}
	public void setDirStruct(Map<String, Directory> dirStruct) {
		this.dirStruct = dirStruct;
	}
	public Map<String, String> getFileStruct() {
		return fileStruct;
	}
	public void setFileStruct(Map<String, String> fileStruct) {
		this.fileStruct = fileStruct;
	}

	
	
	
}
