package com.aiotech.aios.system.to;

import java.util.List;

import com.aiotech.aios.system.domain.entity.Directory;

import net.sf.json.JSONObject;

public class TreeVO {

	private JSONObject jsonTree;
	private List<Directory> dirList;
	public JSONObject getJsonTree() {
		return jsonTree;
	}
	public void setJsonTree(JSONObject jsonTree) {
		this.jsonTree = jsonTree;
	}
	public List<Directory> getDirList() {
		return dirList;
	}
	public void setDirList(List<Directory> dirList) {
		this.dirList = dirList;
	}
	
}
