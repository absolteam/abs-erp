package com.aiotech.aios.system.to;

import java.io.Serializable;
import java.util.List;

public class SyFunctionTO implements Serializable{
	private int functionId;
	private String functionName;
	private String userFunctionName;
	private String description;
	private String type;
	private int applicationId;
	private String parameters;
	private String fileName;
	
	private String applicationName;
	
	private int sessionId;
	private int trnValue;
	private String actionFlag; 
	private int actualLineId;
	private String tempLineId;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	
	public int getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(int trnValue) {
		this.trnValue = trnValue;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public String getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(String tempLineId) {
		this.tempLineId = tempLineId;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getUserFunctionName() {
		return userFunctionName;
	}
	public void setUserFunctionName(String userFunctionName) {
		this.userFunctionName = userFunctionName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getParameters() {
		return parameters;
	}
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	
}
