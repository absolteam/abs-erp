package com.aiotech.aios.system.to;

/********************************************************************************************
*
* Create Log
* -------------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -------------------------------------------------------------------------------------------
* 1.0		03 Nov 2010 	Nagarajan T.	 		   	Initial Version
*********************************************************************************************/

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.common.util.GetFormatedDate;

public class SyResponsibilityTO implements Serializable{
	private int functionId;
	private String functionName;
	private int responsibilityId;
	private String responsibilityName;
	private String description;
	private String startDate;
	private String endDate;
	private int menuId;
	private int subMenuId;
	private String menuName;
	private String userMenuName;
	private int companyId;
	private String companyIdStr;
	private int applicationId;
	private String applicationName;
	private int organizationId;
	private String organizationName;
	
	private String exclusionType;
	private String exclusionTypeName;
	private int exclusionMenuId;
	private Integer exclusionFunctionId;
	private String exclusionFunctionName;
	
	private int menuTrnVal;
	private int menuLineId;
	
	private int sessionId;
	private Integer trnValue;
	private String actionFlag; 
	private int actualLineId;
	private Integer tempLineId;
	
	private int headerFlag;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	private String dispFromDate;
	private String dispToDate;
	
	
	
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	public String getDispToDate() {
		return dispToDate;
	}
	public void setDispToDate(String dispToDate) {
		//this.dispToDate = dispToDate;
		this.dispToDate = new GetFormatedDate().getFormatedDate(dispToDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	} 
 
	public String getDispFromDate() {
		return dispFromDate;
	}
	public void setDispFromDate(String dispFromDate) {
		//this.dispFromDate = dispFromDate;
		this.dispFromDate = new GetFormatedDate().getFormatedDate(dispFromDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	}
	
	
	public int getMenuTrnVal() {
		return menuTrnVal;
	}
	public void setMenuTrnVal(int menuTrnVal) {
		this.menuTrnVal = menuTrnVal;
	}
	public int getMenuLineId() {
		return menuLineId;
	}
	public void setMenuLineId(int menuLineId) {
		this.menuLineId = menuLineId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	public int getExclusionMenuId() {
		return exclusionMenuId;
	}
	public void setExclusionMenuId(int exclusionMenuId) {
		this.exclusionMenuId = exclusionMenuId;
	}
	public String getExclusionType() {
		return exclusionType;
	}
	public void setExclusionType(String exclusionType) {
		this.exclusionType = exclusionType;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public int getResponsibilityId() {
		return responsibilityId;
	}
	public void setResponsibilityId(int responsibilityId) {
		this.responsibilityId = responsibilityId;
	}
	public String getResponsibilityName() {
		return responsibilityName;
	}
	public void setResponsibilityName(String responsibilityName) {
		this.responsibilityName = responsibilityName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	public Integer getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public String getUserMenuName() {
		return userMenuName;
	}
	public void setUserMenuName(String userMenuName) {
		this.userMenuName = userMenuName;
	}
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getCompanyIdStr() {
		return companyIdStr;
	}
	public void setCompanyIdStr(String companyIdStr) {
		this.companyIdStr = companyIdStr;
	}
	public String getExclusionTypeName() {
		return exclusionTypeName;
	}
	public void setExclusionTypeName(String exclusionTypeName) {
		this.exclusionTypeName = exclusionTypeName;
	}
	public Integer getExclusionFunctionId() {
		return exclusionFunctionId;
	}
	public void setExclusionFunctionId(Integer exclusionFunctionId) {
		this.exclusionFunctionId = exclusionFunctionId;
	}
	public String getExclusionFunctionName() {
		return exclusionFunctionName;
	}
	public void setExclusionFunctionName(String exclusionFunctionName) {
		this.exclusionFunctionName = exclusionFunctionName;
	}
	public int getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(int subMenuId) {
		this.subMenuId = subMenuId;
	}

	
}
