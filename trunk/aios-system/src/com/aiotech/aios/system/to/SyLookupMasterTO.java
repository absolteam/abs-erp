package com.aiotech.aios.system.to;

import java.util.List;

import com.aiotech.aios.common.util.GetFormatedDate;

public class SyLookupMasterTO 
{
	private int lovId;
	private String lovName;
	private String lovModule;
	private String lovDescription;	
	private String lovSysUserFlag; 
	private String lovHeaderStatus;	
	private int trn;
	private int sessionId;
	private int saveLater;	
	private String fromDate;
	private String toDate;
	private String dispFromDate;  // to display in "dd-mmm-yyyy" format only
	private String dispToDate; 
	private int headerFlag; //if the header part is modified, set it 
	
	private int applicationId;
	private String applicationName;
	
	private String lookupId;
	
	private int lovStatus; //child
	private String lovMeaning;//child
	private String lovValue;//child
	private String lovCode;
	private String lovCodeStatus; //child check box	
	private String lovCodeId;
	
	private String actionFlag;
	
	private String returndbMsg;  // db msg
	private int returndbStatus;  // db status
	private String actualLineId;
	private String trnValue;
	private String tempLineId;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	public String getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(String tempLineId) {
		this.tempLineId = tempLineId;
	}
	public String getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(String actualLineId) {
		this.actualLineId = actualLineId;
	}
	//grid VARIABLES
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	public int getLovId() {
		return lovId;
	}
	public void setLovId(int lovId) {
		this.lovId = lovId;
	}
	public String getLovName() {
		return lovName;
	}
	public void setLovName(String lovName) {
		this.lovName = lovName;
	}
	public String getLovModule() {
		return lovModule;
	}
	public void setLovModule(String lovModule) {
		this.lovModule = lovModule;
	}
	public String getLovDescription() {
		return lovDescription;
	}
	public void setLovDescription(String lovDescription) {
		this.lovDescription = lovDescription;
	}
	
	public String getLovHeaderStatus() {
		return lovHeaderStatus;
	}
	public void setLovHeaderStatus(String lovHeaderStatus) {
		this.lovHeaderStatus = lovHeaderStatus;
	}
	public int getLovStatus() {
		return lovStatus;
	}
	public void setLovStatus(int lovStatus) {
		this.lovStatus = lovStatus;
	}
	public int getTrn() {
		return trn;
	}
	public void setTrn(int trn) {
		this.trn = trn;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public int getSaveLater() {
		return saveLater;
	}
	public void setSaveLater(int saveLater) {
		this.saveLater = saveLater;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public String getDispFromDate() {
		return dispFromDate;
	}
	public void setDispFromDate(String dispFromDate) {
		//this.dispFromDate = dispFromDate;
		this.dispFromDate = new GetFormatedDate().getFormatedDate(dispFromDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	}
	public String getDispToDate() {
		return dispToDate;
	}
	public void setDispToDate(String dispToDate) {
		//this.dispToDate = dispToDate;
		this.dispToDate = new GetFormatedDate().getFormatedDate(dispToDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	}
	public String getReturndbMsg() {
		return returndbMsg;
	}
	public void setReturndbMsg(String returndbMsg) {
		this.returndbMsg = returndbMsg;
	}
	public String getLovMeaning() {
		return lovMeaning;
	}	
	public String getLovValue() {
		return lovValue;
	}
	public void setLovValue(String lovValue) {
		this.lovValue = lovValue;
	}
	public String getLovCodeStatus() {
		return lovCodeStatus;
	}
	public void setLovCodeStatus(String lovCodeStatus) {
		this.lovCodeStatus = lovCodeStatus;
	}
	public void setLovMeaning(String lovMeaning) {
		this.lovMeaning = lovMeaning;
	}
	public String getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(String trnValue) {
		this.trnValue = trnValue;
	}
	public int getReturndbStatus() {
		return returndbStatus;
	}
	public void setReturndbStatus(int returndbStatus) {
		this.returndbStatus = returndbStatus;
	}
	public String getLovSysUserFlag() {
		return lovSysUserFlag;
	}
	public void setLovSysUserFlag(String lovSysUserFlag) {
		this.lovSysUserFlag = lovSysUserFlag;
	}
	public String getLovCode() {
		return lovCode;
	}
	public void setLovCode(String lovCode) {
		this.lovCode = lovCode;
	}
	public String getLovCodeId() {
		return lovCodeId;
	}
	public void setLovCodeId(String lovCodeId) {
		this.lovCodeId = lovCodeId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public String getLookupId() {
		return lookupId;
	}
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	

}
