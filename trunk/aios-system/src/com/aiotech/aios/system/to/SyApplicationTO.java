package com.aiotech.aios.system.to;

/*******************************************************************************
*
* Create Log
* -----------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -----------------------------------------------------------------------------
* 1.0		24 Oct 2010 	Nagarajan T.	 		   Initial Version

******************************************************************************/

import java.io.Serializable;
import java.util.List;


public class SyApplicationTO implements Serializable{
	private int applicationId;
	private String applicationName;
	private String basePath;
	private String description;
	private int trnValue;
	private int sessionId;

	private int actualLineId;
	private String actionFlag; 
	private Integer tempLineId;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	
	public int getActualLineId() {
		return actualLineId;
	}

	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public Integer getTempLineId() {
		return tempLineId;
	}

	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}

	public int getTrnValue() {
		return trnValue;
	}

	public void setTrnValue(int trnValue) {
		this.trnValue = trnValue;
	}

	

	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}

	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}

	public int getSqlErrorNum() {
		return sqlErrorNum;
	}

	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}

	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}

	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}

	public Integer getGridFrom() {
		return gridFrom;
	}

	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}

	public Integer getGridTo() {
		return gridTo;
	}

	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}

	public String getGridSord() {
		return gridSord;
	}

	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List getCell() {
		return cell;
	}

	public void setCell(List cell) {
		this.cell = cell;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	

}
