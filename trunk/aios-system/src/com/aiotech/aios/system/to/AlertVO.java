/**
 * 
 */
package com.aiotech.aios.system.to;

import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.workflow.domain.entity.Screen;

/**
 * @author Usman Anwar
 * 
 */
public class AlertVO extends Alert {

	private Screen screen;
	private String createdDateString;

	public void convertToVO(Alert alert) throws Exception {
		this.setAlertId(alert.getAlertId());
		this.setIsActive(alert.getIsActive());
		this.setMessage(alert.getMessage());
		this.setRecordId(alert.getRecordId());
		this.setRoleId(alert.getRoleId());
		this.setScreenId(alert.getAlertId());
		this.setTableName(alert.getTableName());
		/*
		 * this.setCreatedDateString(alert.getCreatedDate() != null ? alert
		 * .getCreatedDate().toString() : "");
		 */
		this.setCreatedDateString(""); // temporary
	}

	public String getCreatedDateString() {
		return createdDateString;
	}

	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

}
