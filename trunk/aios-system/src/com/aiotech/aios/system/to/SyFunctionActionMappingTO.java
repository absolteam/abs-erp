package com.aiotech.aios.system.to;

import java.io.Serializable;
import java.util.List;

public class SyFunctionActionMappingTO implements Serializable{
	private int sessionId;

	private int actualLineId;
	private String actionFlag; 
	private Integer tempLineId;
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private int wfApplicationId;
	private int wfNextApplicationId;
	private String wfApplicationName;
	private String wfNextApplicationName;
	private String wfParameters;
	private String wfFunctionName;
	private int wfFunctionId;
	private int returnDBStatus;
	private String returnDBMsg;
	private String workflowName;
	private String workflowDescription;
	private int workflowId;
	private int mappingId;
	private String wfFunctionType;
	private int wfStartingLegDb;
	private String wfStartingLeg;
	private String wfMasterRespId;
	private String wfMasterUserId;
	private int wfMasterCompanyId;
	private int wfMasterApplicationId;
	private int wfCategoryId;
	private String wfCategoryName;
	private String wfCondition;
	private String securedFlow;
	private int securedFlowDB;
	private String workflowReminder;
	private int workflowReminderDB;
	private String workflowEscalation;
	private int workflowEscalationDB;
	private String wfReminderDate;
	private String wfReminderTime;
	private String validFromDate;
	private String validToDate;
	private String wfEscalationRespId;
	private String wfEscalationUserId;
	private String flowValidDays;
	private String flowValidHours;
	private String wfEscalationRespName;
	private String wfEscalationUserName;
	private String wfNextFunctionIdGroup;
	private String wfFlowChartContent;
	private String wfFlowChartCondition;
	private String dynamicRespId;
	private String dynamicUserId;
	public String getDynamicRespId() {
		return dynamicRespId;
	}
	public void setDynamicRespId(String dynamicRespId) {
		this.dynamicRespId = dynamicRespId;
	}
	public String getDynamicUserId() {
		return dynamicUserId;
	}
	public void setDynamicUserId(String dynamicUserId) {
		this.dynamicUserId = dynamicUserId;
	}
	public String getWfFlowChartContent() {
		return wfFlowChartContent;
	}
	public void setWfFlowChartContent(String wfFlowChartContent) {
		this.wfFlowChartContent = wfFlowChartContent;
	}
	public String getWfFlowChartCondition() {
		return wfFlowChartCondition;
	}
	public void setWfFlowChartCondition(String wfFlowChartCondition) {
		this.wfFlowChartCondition = wfFlowChartCondition;
	}
	public String getWfNextFunctionIdGroup() {
		return wfNextFunctionIdGroup;
	}
	public void setWfNextFunctionIdGroup(String wfNextFunctionIdGroup) {
		this.wfNextFunctionIdGroup = wfNextFunctionIdGroup;
	}
	public String getWfMappingIdGroup() {
		return wfMappingIdGroup;
	}
	public void setWfMappingIdGroup(String wfMappingIdGroup) {
		this.wfMappingIdGroup = wfMappingIdGroup;
	}
	private String wfMappingIdGroup;
	public String getWfEscalationRespName() {
		return wfEscalationRespName;
	}
	public void setWfEscalationRespName(String wfEscalationRespName) {
		this.wfEscalationRespName = wfEscalationRespName;
	}
	public String getWfEscalationUserName() {
		return wfEscalationUserName;
	}
	public void setWfEscalationUserName(String wfEscalationUserName) {
		this.wfEscalationUserName = wfEscalationUserName;
	}
	public String getFlowValidDays() {
		return flowValidDays;
	}
	public void setFlowValidDays(String flowValidDays) {
		this.flowValidDays = flowValidDays;
	}
	public String getFlowValidHours() {
		return flowValidHours;
	}
	public void setFlowValidHours(String flowValidHours) {
		this.flowValidHours = flowValidHours;
	}
	public String getSecuredFlow() {
		return securedFlow;
	}
	public void setSecuredFlow(String securedFlow) {
		this.securedFlow = securedFlow;
	}
	public int getSecuredFlowDB() {
		return securedFlowDB;
	}
	public void setSecuredFlowDB(int securedFlowDB) {
		this.securedFlowDB = securedFlowDB;
	}
	public String getWorkflowReminder() {
		return workflowReminder;
	}
	public void setWorkflowReminder(String workflowReminder) {
		this.workflowReminder = workflowReminder;
	}
	public int getWorkflowReminderDB() {
		return workflowReminderDB;
	}
	public void setWorkflowReminderDB(int workflowReminderDB) {
		this.workflowReminderDB = workflowReminderDB;
	}
	public String getWorkflowEscalation() {
		return workflowEscalation;
	}
	public void setWorkflowEscalation(String workflowEscalation) {
		this.workflowEscalation = workflowEscalation;
	}
	public int getWorkflowEscalationDB() {
		return workflowEscalationDB;
	}
	public void setWorkflowEscalationDB(int workflowEscalationDB) {
		this.workflowEscalationDB = workflowEscalationDB;
	}
	public String getWfReminderDate() {
		return wfReminderDate;
	}
	public void setWfReminderDate(String wfReminderDate) {
		this.wfReminderDate = wfReminderDate;
	}
	public String getWfReminderTime() {
		return wfReminderTime;
	}
	public void setWfReminderTime(String wfReminderTime) {
		this.wfReminderTime = wfReminderTime;
	}
	public String getValidFromDate() {
		return validFromDate;
	}
	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}
	public String getValidToDate() {
		return validToDate;
	}
	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}
	public String getWfEscalationRespId() {
		return wfEscalationRespId;
	}
	public void setWfEscalationRespId(String wfEscalationRespId) {
		this.wfEscalationRespId = wfEscalationRespId;
	}
	public String getWfEscalationUserId() {
		return wfEscalationUserId;
	}
	public void setWfEscalationUserId(String wfEscalationUserId) {
		this.wfEscalationUserId = wfEscalationUserId;
	}
	public String getWfCondition() {
		return wfCondition;
	}
	public void setWfCondition(String wfCondition) {
		this.wfCondition = wfCondition;
	}
	public int getWfCategoryId() {
		return wfCategoryId;
	}
	public void setWfCategoryId(int wfCategoryId) {
		this.wfCategoryId = wfCategoryId;
	}
	public String getWfCategoryName() {
		return wfCategoryName;
	}
	public void setWfCategoryName(String wfCategoryName) {
		this.wfCategoryName = wfCategoryName;
	}
	public String getWfMasterRespId() {
		return wfMasterRespId;
	}
	public void setWfMasterRespId(String wfMasterRespId) {
		this.wfMasterRespId = wfMasterRespId;
	}
	public String getWfMasterUserId() {
		return wfMasterUserId;
	}
	public void setWfMasterUserId(String wfMasterUserId) {
		this.wfMasterUserId = wfMasterUserId;
	}
	public int getWfMasterCompanyId() {
		return wfMasterCompanyId;
	}
	public void setWfMasterCompanyId(int wfMasterCompanyId) {
		this.wfMasterCompanyId = wfMasterCompanyId;
	}
	public int getWfMasterApplicationId() {
		return wfMasterApplicationId;
	}
	public void setWfMasterApplicationId(int wfMasterApplicationId) {
		this.wfMasterApplicationId = wfMasterApplicationId;
	}
	public int getWfStartingLegDb() {
		return wfStartingLegDb;
	}
	public void setWfStartingLegDb(int wfStartingLegDb) {
		this.wfStartingLegDb = wfStartingLegDb;
	}
	public String getWfStartingLeg() {
		return wfStartingLeg;
	}
	public void setWfStartingLeg(String wfStartingLeg) {
		this.wfStartingLeg = wfStartingLeg;
	}
	public String getWfFunctionType() {
		return wfFunctionType;
	}
	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}
	public int getMappingId() {
		return mappingId;
	}
	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}
	public int getWorkflowId() {
		return workflowId;
	}
	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}
	public String getWorkflowName() {
		return workflowName;
	}
	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}
	public String getWorkflowDescription() {
		return workflowDescription;
	}
	public void setWorkflowDescription(String workflowDescription) {
		this.workflowDescription = workflowDescription;
	}
	public String getWorkflowStatus() {
		return workflowStatus;
	}
	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}
	public int getWorkflowStatusDB() {
		return workflowStatusDB;
	}
	public void setWorkflowStatusDB(int workflowStatusDB) {
		this.workflowStatusDB = workflowStatusDB;
	}
	private String workflowStatus;
	private int workflowStatusDB;
	public int getReturnDBStatus() {
		return returnDBStatus;
	}
	public void setReturnDBStatus(int returnDBStatus) {
		this.returnDBStatus = returnDBStatus;
	}
	public String getReturnDBMsg() {
		return returnDBMsg;
	}
	public void setReturnDBMsg(String returnDBMsg) {
		this.returnDBMsg = returnDBMsg;
	}
	public int getWfFunctionId() {
		return wfFunctionId;
	}
	public void setWfFunctionId(int wfFunctionId) {
		this.wfFunctionId = wfFunctionId;
	}
	private String wfUserId;
	private String wfUserName;
	private String wfRespId;
	private String wfRespName;
	private int trnValue;
	private int wfCompanyId;
	private int wfNextCompanyId;
	private String wfNextCompanyName;
	private String wfCompanyName;
	private String wfNextFunctionName;
	private List cell;
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public String getWfNextFunctionName() {
		return wfNextFunctionName;
	}
	public void setWfNextFunctionName(String wfNextFunctionName) {
		this.wfNextFunctionName = wfNextFunctionName;
	}
	public String getWfNextCompanyName() {
		return wfNextCompanyName;
	}
	public void setWfNextCompanyName(String wfNextCompanyName) {
		this.wfNextCompanyName = wfNextCompanyName;
	}
	public String getWfCompanyName() {
		return wfCompanyName;
	}
	public void setWfCompanyName(String wfCompanyName) {
		this.wfCompanyName = wfCompanyName;
	}
	public int getWfCompanyId() {
		return wfCompanyId;
	}
	public void setWfCompanyId(int wfCompanyId) {
		this.wfCompanyId = wfCompanyId;
	}
	public int getWfNextCompanyId() {
		return wfNextCompanyId;
	}
	public void setWfNextCompanyId(int wfNextCompanyId) {
		this.wfNextCompanyId = wfNextCompanyId;
	}
	public int getWfNextFunctionId() {
		return wfNextFunctionId;
	}
	public void setWfNextFunctionId(int wfNextFunctionId) {
		this.wfNextFunctionId = wfNextFunctionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserSpecification() {
		return userSpecification;
	}
	public void setUserSpecification(String userSpecification) {
		this.userSpecification = userSpecification;
	}
	private int wfNextFunctionId;
	private String status;
	private String description;
	private String userSpecification;
	private int statusDb;
	private int userSpecificationDb;
	private String wfDescription;
	//workflowName
	
	public String getWfDescription() {
		return wfDescription;
	}
	public void setWfDescription(String wfDescription) {
		this.wfDescription = wfDescription;
	}
	public int getStatusDb() {
		return statusDb;
	}
	public void setStatusDb(int statusDb) {
		this.statusDb = statusDb;
	}
	public int getUserSpecificationDb() {
		return userSpecificationDb;
	}
	public void setUserSpecificationDb(int userSpecificationDb) {
		this.userSpecificationDb = userSpecificationDb;
	}
	public int getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(int trnValue) {
		this.trnValue = trnValue;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	public String getWfUserName() {
		return wfUserName;
	}
	public void setWfUserName(String wfUserName) {
		this.wfUserName = wfUserName;
	}
	
	public String getWfUserId() {
		return wfUserId;
	}
	public void setWfUserId(String wfUserId) {
		this.wfUserId = wfUserId;
	}
	public String getWfRespId() {
		return wfRespId;
	}
	public void setWfRespId(String wfRespId) {
		this.wfRespId = wfRespId;
	}
	public String getWfRespName() {
		return wfRespName;
	}
	public void setWfRespName(String wfRespName) {
		this.wfRespName = wfRespName;
	}
	public String getWfParameters() {
		return wfParameters;
	}
	public void setWfParameters(String wfParameters) {
		this.wfParameters = wfParameters;
	}
	public String getWfFunctionName() {
		return wfFunctionName;
	}
	public void setWfFunctionName(String wfFunctionName) {
		this.wfFunctionName = wfFunctionName;
	}
	
	public int getWfApplicationId() {
		return wfApplicationId;
	}
	public void setWfApplicationId(int wfApplicationId) {
		this.wfApplicationId = wfApplicationId;
	}
	public int getWfNextApplicationId() {
		return wfNextApplicationId;
	}
	public void setWfNextApplicationId(int wfNextApplicationId) {
		this.wfNextApplicationId = wfNextApplicationId;
	}
	public String getWfApplicationName() {
		return wfApplicationName;
	}
	public void setWfApplicationName(String wfApplicationName) {
		this.wfApplicationName = wfApplicationName;
	}
	public String getWfNextApplicationName() {
		return wfNextApplicationName;
	}
	public void setWfNextApplicationName(String wfNextApplicationName) {
		this.wfNextApplicationName = wfNextApplicationName;
	}
}
