package com.aiotech.aios.system.to;

/********************************************************************************************
*
* Create Log
* -------------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -------------------------------------------------------------------------------------------
* 1.0		04 Nov 2010 	Nagarajan T.	 		   	Initial Version
*********************************************************************************************/

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.common.util.GetFormatedDate;

public class SyUserTO implements Serializable{
	private String userName;
	private int userId;
	private String password;
	private String transPassword;
	private String description;
	private String effectiveFromDate;
	private String effectiveToDate;
	
	private int responsibilityId;
	private String responsibilityName;
	private String startDate;
	private String endDate;
	private String userRespId;
	
	private int personId;
	private String personName;
	private int userTypesId;
	private String userTypes;
	private String email;
	private String mobileNo;
	
	private String applicationId;
	private String applicationName;
	
	private int menuTrnVal;
	private int menuLineId;
	
	private int headerFlag;
	
	private int sessionId;
	private Integer trnValue;
	private String actionFlag; 
	private int actualLineId;
	private Integer tempLineId;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;

	private String notifyName;
	private int notifyId;
	
	private String dispFromDate;
	private String dispToDate;
	private String functionParameters;
	private int functionId;
	private String functionName;
	private String functionIdList;
	private String functionParamList;
	private int companyId;
	private String actualParamters;
	private String mappedParameters;
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getActualParamters() {
		return actualParamters;
	}
	public void setActualParamters(String actualParamters) {
		this.actualParamters = actualParamters;
	}
	public String getMappedParameters() {
		return mappedParameters;
	}
	public void setMappedParameters(String mappedParameters) {
		this.mappedParameters = mappedParameters;
	}
	public String getFunctionIdList() {
		return functionIdList;
	}
	public void setFunctionIdList(String functionIdList) {
		this.functionIdList = functionIdList;
	}
	public String getFunctionParamList() {
		return functionParamList;
	}
	public void setFunctionParamList(String functionParamList) {
		this.functionParamList = functionParamList;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFunctionParameters() {
		return functionParameters;
	}
	public void setFunctionParameters(String functionParameters) {
		this.functionParameters = functionParameters;
	}
	public String getDispToDate() {
		return dispToDate;
	}
	public void setDispToDate(String dispToDate) {
		//this.dispToDate = dispToDate;
		this.dispToDate = new GetFormatedDate().getFormatedDate(dispToDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	} 
 
	public String getDispFromDate() {
		return dispFromDate;
	}
	public void setDispFromDate(String dispFromDate) {
		//this.dispFromDate = dispFromDate;
		this.dispFromDate = new GetFormatedDate().getFormatedDate(dispFromDate, "yyyy-MM-dd", "sort"); // "yyyy-MM-dd" is the input format
	}
	public String getUserRespId() {
		return userRespId;
	}
	public void setUserRespId(String userRespId) {
		this.userRespId = userRespId;
	}
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	public int getMenuTrnVal() {
		return menuTrnVal;
	}
	public void setMenuTrnVal(int menuTrnVal) {
		this.menuTrnVal = menuTrnVal;
	}
	public int getMenuLineId() {
		return menuLineId;
	}
	public void setMenuLineId(int menuLineId) {
		this.menuLineId = menuLineId;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEffectiveFromDate() {
		return effectiveFromDate;
	}
	public void setEffectiveFromDate(String effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}
	public String getEffectiveToDate() {
		return effectiveToDate;
	}
	public void setEffectiveToDate(String effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}
	public int getResponsibilityId() {
		return responsibilityId;
	}
	public void setResponsibilityId(int responsibilityId) {
		this.responsibilityId = responsibilityId;
	}
	public String getResponsibilityName() {
		return responsibilityName;
	}
	public void setResponsibilityName(String responsibilityName) {
		this.responsibilityName = responsibilityName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}

	public String getNotifyName() {
		return notifyName;
	}
	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}  

	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public int getUserTypesId() {
		return userTypesId;
	}
	public void setUserTypesId(int userTypesId) {
		this.userTypesId = userTypesId;
	}
	public String getUserTypes() {
		return userTypes;
	}
	public void setUserTypes(String userTypes) {
		this.userTypes = userTypes;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getTransPassword() {
		return transPassword;
	}
	public void setTransPassword(String transPassword) {
		this.transPassword = transPassword;
	}
	
	public int getNotifyId() {
		return notifyId;
	}
	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}
}
