package com.aiotech.aios.system.to;

/********************************************************************************************
*
* Create Log
* -------------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 Description
* -------------------------------------------------------------------------------------------
* 1.0		05 Mar 2011 	Nagarajan T.	 		   	Initial Version
*********************************************************************************************/

import java.io.Serializable;
import java.util.List;

public class SyServerMaintenanceTO implements Serializable{
	private int maintenanceId;
	private String eventName;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String description;
	private int enabledFlag;
	private long timeInterval;
	
	private String userName;
	
	private Integer trnValue;
	private int sessionId;
	private String actionFlag; 
	private Integer actualLineId;
	private Integer tempLineId;
	
	private int headerFlag;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getTrnValue() {
		return trnValue;
	}
	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public Integer getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(Integer actualLineId) {
		this.actualLineId = actualLineId;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public int getMaintenanceId() {
		return maintenanceId;
	}
	public void setMaintenanceId(int maintenanceId) {
		this.maintenanceId = maintenanceId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getEnabledFlag() {
		return enabledFlag;
	}
	public void setEnabledFlag(int enabledFlag) {
		this.enabledFlag = enabledFlag;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public long getTimeInterval() {
		return timeInterval;
	}
	public void setTimeInterval(long timeInterval) {
		this.timeInterval = timeInterval;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	

}
