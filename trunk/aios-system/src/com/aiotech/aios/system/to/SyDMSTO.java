package com.aiotech.aios.system.to;

import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

public class SyDMSTO implements Serializable{
	private int actualLineId;
	private String actionFlag; 
	private Integer tempLineId;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	private String filePath;
	 private int fileId;
	 private String url;
	 private String metaUrl;
	 private String fileName;
	 private int companyId;
	 private int applicationId;
	 private int functionId;
	 private String functionType;
	 private char isFile;
	 private int childId;
	 private String childName;
	 private int parentId;
	 private String parentName;
	 private String documentVersionId;
	 private int trnId;
	 private int dmsId;
	 private int userId;
	 private String successXmlContent;
	 private String dmsIdList;
	 private String dmsDescription;
	 private String companyName;
	 private String applicationName;
	 private String functionName;
	 private String createdBy;
	 private String enableAccessControl;
	 private int dmsOrganizationId;
	 private String dmsOrganizationName;
	 private String dmsUserId;
	 private String dmsUserName;
	 private String dmsRespId;
	 private String dmsRespName;
	 private int dmsAccessGroup;
	 private int enableAccessControlDB;
	 private int headerFlag;
	 private String queryConcadStr;
	 private InputStream fileInputStream;
	 private String contentType;
	 private String dmsIdArray;
	 private String childIdArray;
	 private String childNameArray;
	 private String dmsVersionNumber;
	 private int numberOfTimes;
	 private int numberOfDays;
	 private String accessGroupName;
	 private String readWrite;
	 private String passwordProtected;
	 private String deleteAccess;
	 private String readonlyAccess;
	 private int accessGroupId;
	 private int readWriteDB;
	 private int passwordProtectedDB;
	 private int deleteAccessDB;
	 private int readonlyAccessDB;
	 private int accessId;
	 private long dmsURN;
	 private int viewAccess;
	 private int editAccess;
	 private int passwordAccess;
	 private int personId;
	 private String accessFlag;
	 private String createdDate;
	 private char uploadType;
	 private char documentCategory;
	 private String versionList[];
	 private long sessionId;
	 private int ledgerId;
	 
	 public long getSessionId() {
		return sessionId;
	}
	public void setSessionId(long sessionId) {
		this.sessionId = sessionId;
	}
	public String[] getVersionList() {
		return versionList;
	}
	public void setVersionList(String[] versionList) {
		this.versionList = versionList;
	}
	public char getDocumentCategory() {
		return documentCategory;
	}
	public void setDocumentCategory(char documentCategory) {
		this.documentCategory = documentCategory;
	}
	public char getUploadType() {
		return uploadType;
	}
	public void setUploadType(char uploadType) {
		this.uploadType = uploadType;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getAccessFlag() {
		return accessFlag;
	}
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public int getViewAccess() {
		return viewAccess;
	}
	public void setViewAccess(int viewAccess) {
		this.viewAccess = viewAccess;
	}
	public int getEditAccess() {
		return editAccess;
	}
	public void setEditAccess(int editAccess) {
		this.editAccess = editAccess;
	}
	public int getPasswordAccess() {
		return passwordAccess;
	}
	public void setPasswordAccess(int passwordAccess) {
		this.passwordAccess = passwordAccess;
	}
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public int getActualLineId() {
		return actualLineId;
	}
	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}
	public String getActionFlag() {
		return actionFlag;
	}
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	public Integer getTempLineId() {
		return tempLineId;
	}
	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}
	
	
	 public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	
	 
	public String getDmsVersionNumber() {
		return dmsVersionNumber;
	}
	public void setDmsVersionNumber(String dmsVersionNumber) {
		this.dmsVersionNumber = dmsVersionNumber;
	}
	public String getDmsIdArray() {
		return dmsIdArray;
	}
	public void setDmsIdArray(String dmsIdArray) {
		this.dmsIdArray = dmsIdArray;
	}
	public String getChildIdArray() {
		return childIdArray;
	}
	public void setChildIdArray(String childIdArray) {
		this.childIdArray = childIdArray;
	}
	public String getChildNameArray() {
		return childNameArray;
	}
	public void setChildNameArray(String childNameArray) {
		this.childNameArray = childNameArray;
	}
	 public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public InputStream getFileInputStream() {
		return fileInputStream;
	}
	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}
	 public String getQueryConcadStr() {
		return queryConcadStr;
	}
	public void setQueryConcadStr(String queryConcadStr) {
		this.queryConcadStr = queryConcadStr;
	}
	public int getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}
	 public int getEnableAccessControlDB() {
		return enableAccessControlDB;
	}
	public void setEnableAccessControlDB(int enableAccessControlDB) {
		this.enableAccessControlDB = enableAccessControlDB;
	}
	public int getNumberOfTimes() {
		return numberOfTimes;
	}
	public void setNumberOfTimes(int numberOfTimes) {
		this.numberOfTimes = numberOfTimes;
	}
	public int getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getAccessGroupName() {
		return accessGroupName;
	}
	public void setAccessGroupName(String accessGroupName) {
		this.accessGroupName = accessGroupName;
	}
	public String getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(String readWrite) {
		this.readWrite = readWrite;
	}
	public String getPasswordProtected() {
		return passwordProtected;
	}
	public void setPasswordProtected(String passwordProtected) {
		this.passwordProtected = passwordProtected;
	}
	public String getDeleteAccess() {
		return deleteAccess;
	}
	public void setDeleteAccess(String deleteAccess) {
		this.deleteAccess = deleteAccess;
	}
	public String getReadonlyAccess() {
		return readonlyAccess;
	}
	public void setReadonlyAccess(String readonlyAccess) {
		this.readonlyAccess = readonlyAccess;
	}
	public int getAccessGroupId() {
		return accessGroupId;
	}
	public void setAccessGroupId(int accessGroupId) {
		this.accessGroupId = accessGroupId;
	}
	 public long getDmsURN() {
		return dmsURN;
	}
	public void setDmsURN(long dmsURN) {
		this.dmsURN = dmsURN;
	}
	public int getAccessId() {
		return accessId;
	}
	public void setAccessId(int accessId) {
		this.accessId = accessId;
	}
	public int getReadWriteDB() {
		return readWriteDB;
	}
	public void setReadWriteDB(int readWriteDB) {
		this.readWriteDB = readWriteDB;
	}
	public int getPasswordProtectedDB() {
		return passwordProtectedDB;
	}
	public void setPasswordProtectedDB(int passwordProtectedDB) {
		this.passwordProtectedDB = passwordProtectedDB;
	}
	public int getDeleteAccessDB() {
		return deleteAccessDB;
	}
	public void setDeleteAccessDB(int deleteAccessDB) {
		this.deleteAccessDB = deleteAccessDB;
	}
	public int getReadonlyAccessDB() {
		return readonlyAccessDB;
	}
	public void setReadonlyAccessDB(int readonlyAccessDB) {
		this.readonlyAccessDB = readonlyAccessDB;
	}
	public int getDmsOrganizationId() {
		return dmsOrganizationId;
	}
	public void setDmsOrganizationId(int dmsOrganizationId) {
		this.dmsOrganizationId = dmsOrganizationId;
	}
	public String getDmsOrganizationName() {
		return dmsOrganizationName;
	}
	public void setDmsOrganizationName(String dmsOrganizationName) {
		this.dmsOrganizationName = dmsOrganizationName;
	}
	public String getDmsUserId() {
		return dmsUserId;
	}
	public void setDmsUserId(String dmsUserId) {
		this.dmsUserId = dmsUserId;
	}
	public String getDmsUserName() {
		return dmsUserName;
	}
	public void setDmsUserName(String dmsUserName) {
		this.dmsUserName = dmsUserName;
	}
	public String getDmsRespId() {
		return dmsRespId;
	}
	public void setDmsRespId(String dmsRespId) {
		this.dmsRespId = dmsRespId;
	}
	public String getDmsRespName() {
		return dmsRespName;
	}
	public void setDmsRespName(String dmsRespName) {
		this.dmsRespName = dmsRespName;
	}
	public int getDmsAccessGroup() {
		return dmsAccessGroup;
	}
	public void setDmsAccessGroup(int dmsAccessGroup) {
		this.dmsAccessGroup = dmsAccessGroup;
	}
	public String getEnableAccessControl() {
		return enableAccessControl;
	}
	public void setEnableAccessControl(String enableAccessControl) {
		this.enableAccessControl = enableAccessControl;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getDmsDescription() {
		return dmsDescription;
	}
	public void setDmsDescription(String dmsDescription) {
		this.dmsDescription = dmsDescription;
	}
	public String getDmsIdList() {
		return dmsIdList;
	}
	public void setDmsIdList(String dmsIdList) {
		this.dmsIdList = dmsIdList;
	}
	 public String getSuccessXmlContent() {
		return successXmlContent;
	}
	public void setSuccessXmlContent(String successXmlContent) {
		this.successXmlContent = successXmlContent;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDmsId() {
		return dmsId;
	}
	public void setDmsId(int dmsId) {
		this.dmsId = dmsId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int getFunctionId() {
		return functionId;
	}
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}
	public String getFunctionType() {
		return functionType;
	}
	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}
	public char getIsFile() {
		return isFile;
	}
	public void setIsFile(char isFile) {
		this.isFile = isFile;
	}
	public int getChildId() {
		return childId;
	}
	public void setChildId(int childId) {
		this.childId = childId;
	}
	public String getChildName() {
		return childName;
	}
	public void setChildName(String childName) {
		this.childName = childName;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getDocumentVersionId() {
		return documentVersionId;
	}
	public void setDocumentVersionId(String documentVersionId) {
		this.documentVersionId = documentVersionId;
	}
	public int getTrnId() {
		return trnId;
	}
	public void setTrnId(int trnId) {
		this.trnId = trnId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getMetaUrl() {
		return metaUrl;
	}
	public void setMetaUrl(String metaUrl) {
		this.metaUrl = metaUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	private boolean status;
	 public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public int getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}
	
}
