package com.aiotech.aios.system.to;

import java.io.Serializable;
import java.util.List;

public class SyCompanyProfileTO implements Serializable {

	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	
	private String validFromDate;
	private String validToDate;
	private String queryString;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private int applicationId;
	private int profileId;
	private int organizationId;
	private String organizationName;
	private String profileItem;
	private String itemValue;
	public int getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(int organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getProfileItem() {
		return profileItem;
	}
	public void setProfileItem(String profileItem) {
		this.profileItem = profileItem;
	}
	public String getItemValue() {
		return itemValue;
	}
	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}
	public int getProfileId() {
		return profileId;
	}
	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}
	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}
	public int getSqlErrorNum() {
		return sqlErrorNum;
	}
	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public String getValidFromDate() {
		return validFromDate;
	}
	public void setValidFromDate(String validFromDate) {
		this.validFromDate = validFromDate;
	}
	public String getValidToDate() {
		return validToDate;
	}
	public void setValidToDate(String validToDate) {
		this.validToDate = validToDate;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
}
