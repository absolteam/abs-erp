package com.aiotech.aios.system.to;

import java.io.Serializable;
import java.util.List;

public class SyIntenalizationTO implements Serializable{
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;
	private List cell;
	private String fmtlocaleCode;
	private String fmtmessageKey;
	private String fmtmessageValue;
	private String fmtModuleCode;
	private String fmtModuleName;
	private String fmtKeyId;
	private String 	queryAppend;
	private String applicationId;
	private String applicationName;
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getQueryAppend() {
		return queryAppend;
	}
	public void setQueryAppend(String queryAppend) {
		this.queryAppend = queryAppend;
	}
	public String getFmtKeyId() {
		return fmtKeyId;
	}
	public void setFmtKeyId(String fmtKeyId) {
		this.fmtKeyId = fmtKeyId;
	}
	public String getFmtModuleName() {
		return fmtModuleName;
	}
	public void setFmtModuleName(String fmtModuleName) {
		this.fmtModuleName = fmtModuleName;
	}
	public String getFmtlocaleCode() {
		return fmtlocaleCode;
	}
	public void setFmtlocaleCode(String fmtlocaleCode) {
		this.fmtlocaleCode = fmtlocaleCode;
	}
	public String getFmtmessageKey() {
		return fmtmessageKey;
	}
	public void setFmtmessageKey(String fmtmessageKey) {
		this.fmtmessageKey = fmtmessageKey;
	}
	public String getFmtmessageValue() {
		return fmtmessageValue;
	}
	public void setFmtmessageValue(String fmtmessageValue) {
		this.fmtmessageValue = fmtmessageValue;
	}
	public String getFmtModuleCode() {
		return fmtModuleCode;
	}
	public void setFmtModuleCode(String fmtModuleCode) {
		this.fmtModuleCode = fmtModuleCode;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
}
