package com.aiotech.aios.system.to;

public class ReferenceTO {

	String usecase;
	String className;
	
	public String getUsecase() {
		return usecase;
	}
	public String getClassName() {
		return className;
	}
	public void setUsecase(String usecase) {
		this.usecase = usecase;
	}
	public void setClassName(String className) {
		this.className = className;
	}
}
