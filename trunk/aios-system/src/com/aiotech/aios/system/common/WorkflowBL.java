/**
 * 
 */
package com.aiotech.aios.system.common;

import java.text.SimpleDateFormat;
import java.util.List;

import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;

/**
 * @author Usman Anwar
 * 
 */
public class WorkflowBL {

	private WorkflowEnterpriseService workflowEnterpriseService;

	// ------------WORKFLOW RELATED------------------------------

	public WorkflowDetailVO getWorkflowDetailVObyMessageId(Long messageId) {
		WorkflowDetailVO vo = new WorkflowDetailVO();
		try {

			if (messageId != null) {
				List<Message> messages = null;

				/*
				 * workFlowService
				 * .getMessageWithWorkflowProcessDetailsByMsgId(Long
				 * .parseLong(ServletActionContext.getRequest()
				 * .getParameter("messageId")));
				 */

				messages = workflowEnterpriseService
						.getGenerateNotificationsBL().getMessageService()
						.getMessageWithWorkflowProcessDetailsByMsgId(messageId);

				if (messages.size() > 0) {
					Message message = messages.get(0);

					WorkflowDetail workflowDetail = message
							.getWorkflowDetailProcess().getWorkflowDetail();

					// Fetch actual record from process's table
					Object recordObject = workflowEnterpriseService
							.getGenerateNotificationsBL()
							.getWorkflowService()
							.getRecord(
									workflowDetail.getWorkflow()
											.getModuleProcess().getUseCase(),
									message.getWorkflowDetailProcess()
											.getRecordId());

					/*
					 * workFlowService.getRecord(
					 * workflowDetail.getWorkflow().getModuleProcess()
					 * .getModule().getModuleName(),
					 * workflowDetail.getWorkflow().getModuleProcess()
					 * .getUseCase(), message
					 * .getWorkflowDetailProcess().getRecordId());
					 */

					// fill in workflowdetailVO necessary info and add in
					// workflowDetailVOs

					vo.setWorkflow(workflowDetail.getWorkflow());
					
					vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
					vo.setOperation(workflowDetail.getOperation());
					vo.setOperationOrder(workflowDetail.getOperationOrder());
					
					vo.setObject(recordObject);
					vo.setRecordId(message.getWorkflowDetailProcess()
							.getRecordId());
					vo.setMessage(message);

					java.text.DateFormat df = new SimpleDateFormat(
							"EEE, d MMM yyyy hh:mm aaa");
					String formatedDate = df.format(message.getCreatedDate());
					vo.setFormatednotificationTime(formatedDate);

					vo.setProcessFlag(workflowDetail.getOperation());
					vo.setWorkflowDetailProcess(message
							.getWorkflowDetailProcess());
					/*
					 * Person person = personService.getPerson(workflowDetail
					 * .getNotifyTo()); vo.setNotifyToPerson(person);
					 */

					/*
					 * Method method = recordObject.getClass().getMethod(
					 * "getCountryId", null); Country country;
					 */
					/*
					 * if (method != null) { Object callingObject = method
					 * .invoke(recordObject, null);
					 * 
					 * if (callingObject != null) { if (callingObject instanceof
					 * Integer) { country = countryService
					 * .getByCountryId((Integer) callingObject);
					 * System.out.println("country -> " + country.getCountry());
					 * if (getLocale().getLanguage().equals("en")) {
					 * vo.setCountryName(country.getCountry()); } else {
					 * vo.setCountryName(country.getCountryAr()); }
					 * 
					 * } } }
					 */
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;

	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}
	
}
