package com.aiotech.aios.system.bl;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.service.CommentService;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CommentBL {
	private CommentService commentService;
	private AIOTechGenericDAO<Person> personDAO;

	public CommentVO getCommentInfo(Comment commentInfo) throws Exception {
		List<Comment> comments = commentService.getUseCaseComment(commentInfo);
		CommentVO comment = null;
		if (comments != null && comments.size() > 0)
			return convertEntityToVO(comments.get(comments.size() - 1));
		else
			return comment;
	}

	public CommentVO convertEntityToVO(Comment comment) {
		CommentVO commentVO = new CommentVO();
		Person person = personDAO.findById(comment.getPersonId());
		commentVO.setComment(comment.getComment());
		commentVO.setCommentId(comment.getCommentId());
		commentVO.setCreatedDate(comment.getCreatedDate());
		commentVO.setMessage(comment.getMessage());
		commentVO.setPersonId(comment.getPersonId());
		commentVO.setUseCase(comment.getUseCase());
		commentVO.setWorkflowDetailProcess(comment.getWorkflowDetailProcess());
		commentVO.setName((person.getPrefix() != null ? person.getPrefix() : "")
				+ person.getFirstName() + "" + person.getLastName());
		return commentVO;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}
}
