package com.aiotech.aios.system.bl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.CompressionUtil;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.service.DirectoryService;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.to.DocumentVO;

public class DocumentBL {

	private DocumentService documentService;
	private DirectoryService directoryService;
	private String pathFile;

	public DirectoryService getDirectoryService() {
		return directoryService;
	}

	public void setDirectoryService(DirectoryService directoryService) {
		this.directoryService = directoryService;
	}

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public List<Document> retrieveCopiedDocuments(Document document)
			throws IOException, DataFormatException {
		List<Document> DocumentList = documentService
				.getRecordDocuments(document);
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		List<Document> extractedList = new ArrayList<Document>();
		for (Document img : DocumentList) {
			String filePath = confProps.getProperty("file.upload.path")
					.concat(File.separator).concat(img.getHashedName());
			byte[] fileBytes = FileUtil.getBytes(new File(filePath));
			byte[] extractedBytes = CompressionUtil.extractBytes(fileBytes);
			String outputFilePath = confProps.getProperty("file.upload.path")
					.concat(File.separator).concat("extracted")
					.concat(File.separator).concat(img.getName());
			FileUtil.writeExtracedBytesToDisk(extractedBytes, outputFilePath);
			img.setName("file:///" + outputFilePath);
			extractedList.add(img);
		}
		return extractedList;

	}

	private String getDirectory(Directory dirctory) {
		try {
			if (dirctory != null && !dirctory.equals("")) {
				pathFile += dirctory.getDirectoryName().concat("@");
				if (dirctory.getDirectory() != null
						&& !dirctory.getDirectory().equals("")) {
					dirctory = directoryService.getDirectoryByChildId(
							dirctory.getDirectory()).get(0);
					pathFile.concat(dirctory.getDirectoryName().concat("@"));
					return getDirectory(dirctory);
				}
			}
		} catch (Exception e) {
			return pathFile;
		}
		return pathFile;
	}

	public byte[] showDocument(Document document) throws IOException,
			DataFormatException {
		try {
			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Document documentPath = documentService.getDocumentPath(document
					.getDocumentId());
			String dirFoler = "";
			pathFile = "";
			Directory directory = directoryService
					.getDirectoryById(documentPath.getDirectory()
							.getDirectoryId());
			if (directory != null && !directory.equals("")) {
				dirFoler = getDirectory(directory);
			}
			String[] fileSubPath = splitValues(dirFoler);
			dirFoler = "";
			for (int i = fileSubPath.length - 1; i >= 0; i--) {
				dirFoler += fileSubPath[i].concat(File.separator);
			}
			String filePath = confProps.getProperty("file.upload.path")
					.concat(dirFoler).concat(documentPath.getHashedName());
			byte fileBytes[] = FileUtil.getBytes(new File(filePath));
			byte[] extractedBytes = CompressionUtil.extractBytes(fileBytes);
			String outputFilePath = confProps.getProperty("file.upload.path")
					.concat(File.separator).concat("extracted")
					.concat(File.separator).concat(document.getName());
			FileUtil.writeExtracedBytesToDisk(extractedBytes, outputFilePath);
			document.setName("file:///" + outputFilePath);
			byte[] outBytes = FileUtil.getBytes(new File(outputFilePath));
			return outBytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Map<String, String>> retriveRecordDocuments(Document document) {
		List<Document> docs = documentService.getRecordDocuments(document);
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		for (Document doc : docs) {
			Map<String, String> docMap = new HashMap<String, String>();
			docMap.put("fileName", doc.getName());
			docMap.put("size", String.valueOf(doc.getCompDocSize()));
			docMap.put("progressMsg", "<img src = 'fileupload/success.png' />");
			docMap.put("random", String.valueOf(AIOSCommons.getRandom()));
			docMap.put("mode", "edit");
			docMap.put("docId", String.valueOf(doc.getDocumentId()));
			docMap.put("recId", String.valueOf(doc.getRecordId()));
			docMap.put("table", doc.getTableName());
			docMap.put("hashedName", doc.getHashedName());
			list.add(docMap);
		}
		return list;
	}

	public void deleteDocument(Document document) {
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String filePath = confProps.getProperty("file.upload.path");
		Document tempDoc = documentService.getDocumentWithDirectory(
				document.getDocumentId()).get(0);
		document.setDirectory(tempDoc.getDirectory());
		filePath += getDirPathToRoot(document.getDirectory()).concat(
				document.getHashedName());
		if (deleteFile(filePath)) {
			System.out.println("Document Physical file has been deleted successfully....");
			documentService.deleteDocument(documentService
					.getDocumentPath(document.getDocumentId()));
		}else{
			System.out.println("Document Physical file not deleted....");
			documentService.deleteDocument(documentService
					.getDocumentPath(document.getDocumentId()));
		}
	}

	public String getDirPathToRoot(Directory directory) {
		String path = ""; // directory.getDirectoryName();
		Directory dir = directoryService.getDirectoryById(directory);
		while (dir != null) {
			Directory d = directoryService.getDirectoryById(dir);
			path = File.separator.concat(d.getDirectoryName() + File.separator)
					.concat(path);
			dir = d.getDirectory();
		}
		return path;
	}

	public boolean deleteDocuments(List<Object> objects, String tableName)
			throws Exception {
		boolean returnFlag = true;
		List<Document> documents = new ArrayList<Document>();
		for (Object object : objects) {

			String[] name = object.getClass().getName().split("entity.");
			String methodName = "get" + name[name.length - 1] + "Id";
			Method method = object.getClass().getMethod(methodName, null);
			documents = documentService.getDocumentsByRecordId(
					(Long) method.invoke(object, null), tableName);

			for (Document document : documents) {
				Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				String filePath = confProps.getProperty("file.upload.path")
						.concat(document.getHashedName());
				if (deleteFile(filePath)) {
					documentService.deleteDocument(document);
				} else {
					returnFlag = false;
				}
			}
		}
		return returnFlag;
	}

	public void saveDocuments(Map<String, Long> dirStruct,
			Map<String, String> fileStruct) {

		// documentService.saveDocuments(parentDoc, childDocs)
	}

	@SuppressWarnings("unchecked")
	public DocumentVO populateDocumentVO(Object object, String displayPane,
			boolean isEdit) throws Exception {
		DocumentVO docVO = new DocumentVO();
		docVO.setObject(object);
		docVO.setEdit(isEdit);

		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, String> files = (Map<String, String>) session
				.getAttribute("AIOS-file-structure" + "-" + displayPane + "-"
						+ (Long) getRecordId(docVO));
		Map<String, String> dirs = (Map<String, String>) session
				.getAttribute("AIOS-dir-structure" + "-" + displayPane + "-"
						+ (Long) getRecordId(docVO));
		List<Directory> dirList = (List<Directory>) session
				.getAttribute("AIOS-dirNode-structure" + "-" + displayPane
						+ "-" + (Long) getRecordId(docVO));

		docVO.setDirMap(dirs);
		docVO.setDirList(dirList);
		docVO.setFileStruct(files);
		docVO.setDisplayPane(displayPane);
		return docVO;
	}

	@SuppressWarnings("unchecked")
	public void saveUploadDocuments(DocumentVO docVO) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String[] name = docVO.getObject().getClass().getName().split("entity.");
		Map<String, byte[]> documentFileBytesMap = (Map<String, byte[]>) session
				.getAttribute("AIOS-doc-" + name[name.length - 1] + "-"
						+ session.getId() + "-" + docVO.getDisplayPane() + "-"
						+ (Long) getRecordId(docVO));
		Map<String, String> files = (Map<String, String>) session
				.getAttribute("AIOS-file-structure" + "-"
						+ docVO.getDisplayPane() + "-"
						+ (Long) getRecordId(docVO));
		Map<String, String> dirs = (Map<String, String>) session
				.getAttribute("AIOS-dir-structure" + "-"
						+ docVO.getDisplayPane() + "-"
						+ (Long) getRecordId(docVO));

		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");

		List<Directory> dirList = (List<Directory>) session
				.getAttribute("AIOS-dirNode-structure" + "-"
						+ docVO.getDisplayPane() + "-"
						+ (Long) getRecordId(docVO));
		if (dirList != null) {
			Map<String, FileVO> documentFilesMap = copyFilesNDirsToDisk(
					dirList, confProps, documentFileBytesMap);

			// FileUtil.copyCachedFiles(documentFileBytesMap, confProps, null);
			List<Document> documents = new ArrayList<Document>();
			Document doc = new Document();
			for (Map.Entry<String, FileVO> entry : documentFilesMap.entrySet()) {
				docVO.setEntry(entry);
				Document document = new Document();
				populateDocument(document, docVO);
				documents.add(document);
			}
			documentService.saveDocuments(doc, documents);
		}
		session.removeAttribute("AIOS-doc-" + name[name.length - 1] + "-"
				+ session.getId() + "-" + docVO.getDisplayPane() + "-"
				+ (Long) getRecordId(docVO));
	}

	private Document populateDocument(Document document, DocumentVO docVO)
			throws Exception {
		Map.Entry<String, FileVO> entry = docVO.getEntry();
		Map<String, Directory> dirStruct = docVO.getDirStruct();
		Map<String, String> fileStruct = docVO.getFileStruct();
		Object object = docVO.getObject();

		document.setName(entry.getKey());
		document.setHashedName(entry.getValue().getHashedName());
		document.setCompDocSize(entry.getValue().getCompressedSize());
		/*
		 * Directory dir = new Directory ();
		 * dir.setDirectoryId(dirStruct.get(fileStruct.get(entry.getKey())));
		 */
		if (dirStruct != null) {
			document.setDirectory(dirStruct.get(fileStruct.get(entry.getKey())));
		} else {
			Directory dir = getDocDirName(docVO.getDirList(),
					document.getName());
			document.setDirectory(dir);
		}

		document.setDisplayPane(docVO.getDisplayPane());
		String[] name = object.getClass().getName().split("entity.");
		document.setTableName(name[name.length - 1]);
		document.setRecordId(getObjectId(object));
		return document;
	}

	private Object getRecordId(DocumentVO docVO) throws Exception {
		if (docVO.isEdit()) {
			Object object = docVO.getObject();
			return getObjectId(object);
		} else {
			return (long) -1;
		}

	}
	
	public void renameFileNameByFileHash(String documentHash,String newFileName) throws Exception {
		if (documentHash!=null && !documentHash.equals("")) {
			Document document=documentService.getDocumentsByFileHash(documentHash);
			document.setName(newFileName);
			documentService.saveDocument(document);
		} 
	}

	private Long getObjectId(Object object) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		String[] name = object.getClass().getName().split("entity.");
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		return (Long) method.invoke(object, null);
	}

	public boolean deleteFile(String filePath) {
		File file = new File(filePath);
		return file.delete();
	}

	public List<Document> getSubDocuments(Document doc) {
		return documentService.getSubDocs(doc);
	}

	public Map<String, FileVO> copyFilesNDirsToDisk(List<Directory> dirList,
			Properties confProps, Map<String, byte[]> bytes)
			throws UnsupportedEncodingException, DataFormatException {
		String root = confProps.getProperty("file.upload.path");
		Map<String, FileVO> filesMap = new HashMap<String, FileVO>();
		copy(dirList, root, bytes, filesMap);
		return filesMap;
	}

	private void copy(List<Directory> dirList, String root,
			Map<String, byte[]> bytes, Map<String, FileVO> filesMap)
			throws UnsupportedEncodingException, DataFormatException {
		for (Directory dir : dirList) {
			try {
				FileUtil.createDir(root.concat(dir.getDirectoryName()
						+ File.separator));
				if (dir.getDocuments() != null && dir.getDocuments().size() > 0) {
					copyDocument(dir.getDocuments(), bytes, root.concat(dir
							.getDirectoryName() + File.separator), filesMap);
				}
				if (dir.getDirectories() != null) {
					String pRoot = root;
					root = root + dir.getDirectoryName() + File.separator;
					copy(new ArrayList<Directory>(dir.getDirectories()), root,
							bytes, filesMap);
					root = pRoot;

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private Map<String, FileVO> copyDocument(Set<Document> docs,
			Map<String, byte[]> bytes, String filePath,
			Map<String, FileVO> filesMap) throws UnsupportedEncodingException,
			DataFormatException {

		for (Document doc : docs) {
			if (bytes != null) {
				if (bytes.get(doc.getName()) != null)
					filesMap.put(doc.getName(), FileUtil.copyDocFile(bytes,
							filePath, doc.getName()));
			}
		}
		return filesMap;
	}

	private Directory getDocDirName(List<Directory> dirList, String docName) {
		Directory directory = null;
		for (Directory dir : dirList) {
			Set<Document> docs = dir.getDocuments();
			if (docs != null && docs.size() > 0) {
				for (Document doc : docs) {
					if (doc.getName().equals(docName)) {
						Directory d = directoryService.getDirectoryByName(dir);
						if (d != null && d.getDirectoryName() != null) {
							return d;
						} else {
							directoryService.persist(dir);
							return dir;
						}
					}
				}
			}
			if (dir.getDirectories() != null && dir.getDirectories().size() > 0) {
				Set<Directory> subDirs = dir.getDirectories();
				if (subDirs != null && subDirs.size() > 0) {
					directory = getDocDirName(
							new ArrayList<Directory>(subDirs), docName);
				}
			}
		}
		return directory;
	}

	public List<Document> getDirectoryDocs(Directory dir) {
		return documentService.getDirectoryDocs(dir);
	}

	// to split the values
	public static String[] splitValues(String spiltValue) {
		return spiltValue.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}
}
