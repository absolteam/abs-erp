package com.aiotech.aios.system.bl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;


public class CommonBL {
	private static final Logger LOGGER = LogManager.getLogger(CommonBL.class);
	AIOTechGenericDAO<PropertyOwnership> propertyOwnershipDAO;
	AIOTechGenericDAO<TenantOffer> tenantOfferDAO;
	
	public String deleteCompanyDependancyCheck(Long companyId){
		String returnMsg=null;
		if(companyId==null)
			return returnMsg;
		//Property Owners
		List<PropertyOwnership> propertyOwners = 
			propertyOwnershipDAO.findByNamedQuery("getOwnersBasedOnCompanyORPerson", companyId,  "COMPANY");
		if(propertyOwners!=null && propertyOwners.size()>0)
			returnMsg="Company Used by Property Details as a Owner";
		
		//Tenant Offer Checking
		List<TenantOffer> tenantOffers = 
			tenantOfferDAO.findByNamedQuery("getTenantBasedOnCompany", companyId);
		if(tenantOffers!=null && tenantOffers.size()>0)
			returnMsg="Company Used by Offer details as a Tenant";
		
		return returnMsg;
	}

	public String deletePersonDependancyCheck(Long personId){
		String returnMsg=null;
		if(personId==null)
			return returnMsg;
		//Property Owners
		List<PropertyOwnership> propertyOwners = 
			propertyOwnershipDAO.findByNamedQuery("getOwnersBasedOnCompanyORPerson", personId,  "PERSON");
		if(propertyOwners!=null && propertyOwners.size()>0)
			returnMsg="Person Used by Property Details as a Owner";
		
		//Tenant Offer Checking
		List<TenantOffer> tenantOffers = 
			tenantOfferDAO.findByNamedQuery("getTenantBasedOnPerson", personId);
		if(tenantOffers!=null && tenantOffers.size()>0)
			returnMsg="Person Used by Offer details as a Tenant";
		
		return returnMsg;
	}
	public AIOTechGenericDAO<PropertyOwnership> getPropertyOwnershipDAO() {
		return propertyOwnershipDAO;
	}

	public void setPropertyOwnershipDAO(
			AIOTechGenericDAO<PropertyOwnership> propertyOwnershipDAO) {
		this.propertyOwnershipDAO = propertyOwnershipDAO;
	}

	public AIOTechGenericDAO<TenantOffer> getTenantOfferDAO() {
		return tenantOfferDAO;
	}

	public void setTenantOfferDAO(AIOTechGenericDAO<TenantOffer> tenantOfferDAO) {
		this.tenantOfferDAO = tenantOfferDAO;
	}

}
