package com.aiotech.aios.system.bl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.LazyInitializationException;

import net.sf.json.JSONObject;

import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.service.DirectoryService;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.to.TreeVO;

public class DirectoryBL {

	private DirectoryService directoryService;
	private DocumentService documentService;

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public DirectoryService getDirectoryService() {
		return directoryService;
	}

	public void setDirectoryService(DirectoryService directoryService) {
		this.directoryService = directoryService;
	}

	public Map<String, Directory> saveDirStructure(Map<String, String> tree,
			long recordId, String tableName, String displayPane) {
		Map<String, Directory> dirMap = new TreeMap<String, Directory>();
		String lastNode = null;
		for (Map.Entry<String, String> entry : tree.entrySet()) {
			String dir = entry.getKey();
			String parentDir = entry.getValue();
			Directory pDirObj = new Directory();
			pDirObj.setDirectoryName(parentDir);
			pDirObj.setRecordId(recordId);
			pDirObj.setTableName(tableName);
			pDirObj.setDisplayPane(displayPane);
			Directory d = directoryService.getDirectoryByName(pDirObj);
			Long parentDirId = d != null ? d.getDirectoryId() : null;
			if (parentDirId == null) {
				directoryService.persist(pDirObj);
				parentDirId = pDirObj.getDirectoryId();
			} else {
				pDirObj = d;
			}

			dirMap.put(parentDir, pDirObj);

			Directory dirObj = new Directory();
			dirObj.setDirectoryName(dir);
			dirObj.setDirectory(pDirObj);
			dirObj.setRecordId(recordId);
			dirObj.setTableName(tableName);
			dirObj.setDisplayPane(displayPane);
			directoryService.persist(dirObj);
			dirMap.put(dir, dirObj);
		}
		return dirMap;
	}

	public TreeVO getDirsNDocs(Directory directory, JSONObject jsonRoot,
			String rootLabel) {
		TreeVO tree = new TreeVO();
		List<Directory> dirs = directoryService.getDirsNDocs(directory);
		jsonRoot = travers(dirs, jsonRoot, rootLabel);
		tree.setJsonTree(jsonRoot);
		tree.setDirList(dirs);
		return tree;
	}

	private JSONObject travers(List<Directory> dirs, JSONObject jsonRoot,
			String rootLabel) {
		List<JSONObject> jList = new ArrayList<JSONObject>();
		if (dirs != null && dirs.size() > 0) {
			Directory dir = dirs.get(0);
			JSONObject json = handleNode(dir, jList);
			getRootDocs(dir, json);
			jList.add(json);
			jsonRoot = json;

		}
		if (jList.size() == 0) {
			jsonRoot.put("id", "1");
			jsonRoot.put("text", rootLabel);
			jsonRoot.put("select", "yes");
			jsonRoot.put("tooltip ", "Directory");

		}
		// jsonRoot.accumulate("item", jList);
		return jsonRoot;
	}

	private JSONObject handleNode(Directory dir, List<JSONObject> jList) {
		JSONObject json = new JSONObject();
		json.put("id", dir.getDirectoryId());
		json.put("text", dir.getDirectoryName());
		json.put("select", "yes");
		json.put("tooltip ", "Directory");
		// Set<Directory> subDirs = dir.getDirectories();
		List<Directory> subDirs = directoryService.getSubDirs(dir);
		int subNodes = subDirs.size();
		if (subDirs != null && subNodes > 0) {
			for (Directory d : subDirs) {
				/*
				 * d.setRecordId(dir.getRecordId());
				 * d.setTableName(dir.getTableName());
				 * d.setDisplayPane(dir.getDisplayPane()); Directory d1 =
				 * directoryService.getDirectoryNDocs(d);
				 */
				if (d != null) {
					JSONObject j = handleNode(d, jList);
					getDirDocs(d, j);
					if (subNodes > 1) {
						json.accumulate("item", j.toString());
					} else if (subNodes == 1) {
						json.accumulate("item", "[" + j.toString() + "]");
					}
				}
			}
		}
		return json;
	}

	private void getDirDocs(Directory dir, JSONObject json) {
		if (dir.getDocuments() != null && dir.getDocuments().size() > 0) {
			Set<Document> docs = dir.getDocuments();
			int docSize = docs.size();
			for (Document doc : docs) {
				if (doc.getRecordId().equals(dir.getRecordId())
						&& doc.getTableName().equals(dir.getTableName())
						&& doc.getDisplayPane().equals(dir.getDisplayPane())) {
					JSONObject j = new JSONObject();
					j.put("id", doc.getDocumentId());
					j.put("text", doc.getName());
					j.put("select", "yes");
					List<Directory> subDirs = directoryService.getSubDirs(dir);
					if (docSize > 1) {
						json.accumulate("item", j.toString());
					} else if (subDirs != null && subDirs.size() != 0
							&& dir.getDirectory() != null) {
						json.accumulate("item", j);
					} else if (docSize == 1 && subDirs != null
							&& subDirs.size() == 1) {
						json.accumulate("item", "[" + j.toString() + "]");
					} else {
						json.accumulate("item", "[" + j.toString() + "]");
					}
				}
			}
		}
	}

	public void getRootDocs(Directory dir, JSONObject json) {
		if (dir.getDocuments() != null && dir.getDocuments().size() > 0) {
			Set<Document> docs = dir.getDocuments();
			for (Document doc : docs) {
				if (doc.getRecordId().equals(dir.getRecordId())
						&& doc.getTableName().equals(dir.getTableName())
						&& doc.getDisplayPane().equals(dir.getDisplayPane())) {
					JSONObject j = new JSONObject();
					j.put("id", String.valueOf(Math.random()));
					j.put("text", doc.getName());
					j.put("select", "yes");
					try {
						if (dir.getDirectories() != null
								&& dir.getDirectories().size() > 0) {
							json.accumulate("item", j);
						} else if (docs.size() == 1) {
							json.accumulate("item", "[" + j + "]");
						} else {
							json.accumulate("item", j);
						}
					} catch (LazyInitializationException e) {
						if (docs.size() > 1) {
							json.accumulate("item", j);
						} else {
							json.accumulate("item", "[" + j + "]");
						}
					}
				}
			}
		}
	}

	public JSONObject populateTreeFromSession(List<Directory> dirList,
			String rootLabel, JSONObject jsonRoot) {
		List<JSONObject> jList = new ArrayList<JSONObject>();
		if (dirList != null && dirList.size() > 0) {
			Directory dir = dirList.get(0);
			JSONObject json = handleSessionNode(dir, jList);
			getRootDocs(dir, json);
			jList.add(json);
			jsonRoot = json;

		}
		if (jList.size() == 0) {
			jsonRoot.put("id", "1");
			jsonRoot.put("text", rootLabel);
			jsonRoot.put("select", "yes");
			jsonRoot.put("tooltip ", "Directory");

		}
		// jsonRoot.accumulate("item", jList);
		return jsonRoot;

	}

	private JSONObject handleSessionNode(Directory dir, List<JSONObject> jList) {
		JSONObject json = new JSONObject();
		json.put("id", String.valueOf(Math.random()));
		json.put("text", dir.getDirectoryName());
		json.put("select", "yes");
		json.put("tooltip ", "Directory");
		Set<Directory> subDirs = dir.getDirectories();
		int subNodes = subDirs.size();
		if (subDirs != null && subNodes > 0) {
			for (Directory d : subDirs) {
				if (d != null) {
					JSONObject j = handleSessionNode(d, jList);
					// getDirDocs(d,j);
					getDirDocsFromSession(d, j);
					if (subNodes > 1) {
						json.accumulate("item", j.toString());
					} else if (subNodes == 1) {
						json.accumulate("item", "[" + j.toString() + "]");
					}
				}
			}
		}

		return json;
	}

	private void getDirDocsFromSession(Directory dir, JSONObject json) {
		if (dir.getDocuments() != null && dir.getDocuments().size() > 0) {
			Set<Document> docs = dir.getDocuments();
			int docSize = docs.size();
			for (Document doc : docs) {
				if (doc.getRecordId().equals(dir.getRecordId())
						&& doc.getTableName().equals(dir.getTableName())
						&& doc.getDisplayPane().equals(dir.getDisplayPane())) {
					JSONObject j = new JSONObject();
					j.put("id", String.valueOf(Math.random()));
					j.put("text", doc.getName());
					j.put("select", "yes");
					if (docSize > 1) {
						json.accumulate("item", j.toString());
					} else if (dir.getDirectories() != null
							&& dir.getDirectories().size() != 0
							&& dir.getDirectory() != null) {
						json.accumulate("item", j);
					} else if (docSize == 1 && dir.getDirectories().size() == 1) {
						json.accumulate("item", "[" + j.toString() + "]");
					} else {
						json.accumulate("item", "[" + j.toString() + "]");
					}
				}
			}
		}
	}

	public void removeDirFromSession(List<Directory> dirList, String dirName) {
		boolean nodeFoundNDeledted = false;
		try {
			if (dirList != null) {
				for (Directory dir : dirList) {
					if (dir.getDirectoryName().equals(dirName)) {
						Directory parentDirectory = dir.getDirectory();
						Set<Directory> subDirs = parentDirectory
								.getDirectories();
						if (subDirs != null) {
							subDirs.remove(dir);
							parentDirectory.setDirectories(subDirs);
							nodeFoundNDeledted = true;
						}
					} else {
						Set<Directory> subDirs = dir.getDirectories();
						removeDirFromSession(new ArrayList<Directory>(subDirs),
								dirName);
					}
				}

				if (nodeFoundNDeledted == false) {
					removeDocFromSession(dirList, dirName);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeDocFromSession(List<Directory> dirList, String docName) {

		for (Directory dir : dirList) {
			try {
				if (dir.getDocuments() != null) {
					Set<Document> docs = dir.getDocuments();
					for (Document doc : docs) {
						try {
							if (doc.getName().equals(docName)) {
								docs.remove(doc);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				} else {
					Set<Directory> subDirs = dir.getDirectories();
					removeDocFromSession(new ArrayList<Directory>(subDirs),
							docName);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public List<Directory> getSubDirs(Directory dir) {
		return directoryService.getSubDirs(dir);
	}

	public boolean deleteDirectory(List<Directory> dirList, String nodeText,
			String rootPath) throws IOException {
		boolean isDirectory = false;
		if (dirList != null) {
			for (Directory dir : dirList) {
				if (dir.getDirectoryName().equals(nodeText)) {
					documentService.deleteDocuments(new ArrayList<Document>(dir
							.getDocuments()));
					if (dir.getDirectories() != null) {
						directoryService
								.deleteDirectories(new ArrayList<Directory>(dir
										.getDirectories()));
					}

					for (Document docToDelete : dir.getDocuments()) {

						// delete all the hashed files 1 by 1
						FileUtil.delete(new File(rootPath + File.separator
								+ getDirPathToRoot(dir) + File.separator
								+ docToDelete.getHashedName()));
					}

					File emptyDirCheck = new File(rootPath + File.separator
							+ getDirPathToRoot(dir));

					if (emptyDirCheck.isDirectory()
							&& emptyDirCheck.list().length == 0) {
						// delete directory if empty
						FileUtil.delete(new File(rootPath + File.separator
								+ getDirPathToRoot(dir)));
					}

					directoryService.deleteDirectory(dir);

					isDirectory = true;
				} else {
					deleteDirectory(
							new ArrayList<Directory>(dir.getDirectories()),
							nodeText, rootPath);
				}
			}
		}
		return isDirectory;
	}

	public void deleteDocument(List<Directory> dirList, String dirName,
			String rootPath) {
		boolean nodeFoundNDeledted = false;
		for (Directory dir : dirList) {
			if (dir.getDirectoryName().equals(dirName)) {
				documentService.deleteDocuments(new ArrayList<Document>(dir
						.getDocuments()));
				deleteDocsFromDisk(new ArrayList<Document>(dir.getDocuments()));
				nodeFoundNDeledted = true;
			} else {
				Set<Directory> subDirs = dir.getDirectories();
				deleteDocument(new ArrayList<Directory>(subDirs), dirName,
						rootPath);
			}
		}

		if (nodeFoundNDeledted == false) {
			removeDoc(dirList, dirName, rootPath);
		}
	}

	public void removeDoc(List<Directory> dirList, String docName,
			String rootPath) {
		for (Directory dir : dirList) {
			if (dir.getDocuments() != null) {
				Set<Document> docs = dir.getDocuments();
				for (Document doc : docs) {
					if (doc.getName().equals(docName)) {
						List<Document> subDocsList = documentService
								.getSubDocs(doc);
						if (subDocsList != null && subDocsList.size() > 0) {
							documentService.deleteDocuments(subDocsList);
							deleteDocsFromDisk(subDocsList);
						}
						new File(rootPath + File.separator
								+ getDocPathToRoot(doc)).delete();
						documentService.deleteDocument(doc);

					}
				}

			} else {
				Set<Directory> subDirs = dir.getDirectories();
				removeDoc(new ArrayList<Directory>(subDirs), docName, rootPath);
			}
		}
	}

	public String getDocPathToRoot(Document doc) {
		String path = doc.getHashedName();
		Directory dir = directoryService.getDirectoryById(doc.getDirectory());
		while (dir != null) {
			Directory d = directoryService.getDirectoryById(dir);
			path = File.separator.concat(d.getDirectoryName() + File.separator)
					.concat(path);
			dir = d.getDirectory();
		}
		return path;
	}

	public String getDirPathToRoot(Directory directory) {
		String path = directory.getDirectoryName();
		Directory dir = directoryService.getDirectoryById(directory
				.getDirectory());
		while (dir != null) {
			Directory d = directoryService.getDirectoryById(dir);
			path = File.separator.concat(d.getDirectoryName() + File.separator)
					.concat(path);
			dir = d.getDirectory();
		}
		return path;
	}

	private void deleteDocsFromDisk(List<Document> docs) {
		for (Document doc : docs) {
			String docPath = getDocPathToRoot(doc);
			System.out.println(docPath);
		}
	}

}
