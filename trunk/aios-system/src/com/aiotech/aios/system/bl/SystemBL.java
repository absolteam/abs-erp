package com.aiotech.aios.system.bl;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.ReferenceManagerBL;
import com.aiotech.aios.system.service.bl.SendHTMLEmail;
import com.aiotech.aios.system.to.EmailVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class SystemBL {

	private static ReferenceManagerBL referenceManagerBL;
	private SystemService systemService;
	private AIOTechGenericDAO<Person> personDAO;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public ReferenceManagerBL getReferenceManagerBL() {
		return referenceManagerBL;
	}

	public Implementation updateTemplateStatus(Boolean templateStatus)
			throws Exception {

		Implementation implementation = systemService
				.getImplementation(getImplementation());
		implementation.setTemplatePrint(templateStatus);
		systemService.saveOrUpdateImplementation(implementation);
		return implementation;
	}

	public List<Person> getAllPerson(Implementation implementation) {
		return personDAO.findByNamedQuery("getAllEmployee", implementation);
	}

	public List<User> getUsersOfRole(Long roleId, List<Long> personsId)
			throws Exception {
		return workflowEnterpriseService.getUserActionBL().getUserRoleService()
				.getAllUsersOfRole(roleId, personsId);
	}

	public void sendEmail(EmailVO emailVO) {
		SendHTMLEmail.sendHtmlEmail(emailVO);
	}

	@SuppressWarnings("static-access")
	public void setReferenceManagerBL(ReferenceManagerBL referenceManagerBL) {
		this.referenceManagerBL = referenceManagerBL;
	}

	public static String getReferenceStamp(String usecase,
			Implementation implementation) {

		return referenceManagerBL.getReferenceStamp(usecase, implementation);
	}

	public static String getReferenceStamp(String usecase,
			Implementation implementation, Session session) {

		return referenceManagerBL.getReferenceStamp(usecase, implementation,
				session);
	}

	public static String saveReferenceStamp(String usecase,
			Implementation implementation) {

		return referenceManagerBL.saveReferenceStamp(usecase, implementation);
	}

	public static String saveReferenceStamp(String usecase,
			Implementation implementation, Session session) {
		return referenceManagerBL.saveReferenceStamp(usecase, implementation,
				session);
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public List<Implementation> getAllImplementations() throws Exception {

		return systemService.getAllImplementation();
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
