package com.aiotech.aios.system.bl;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.DataFormatException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.CompressionUtil;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.ImageService;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.system.to.ImageVO;

public class ImageBL {
	ImageService imageService;
	private static Properties prop;

	static {
		try {
			prop = new Properties();
			InputStream input = null;
			input = ImageBL.class.getResourceAsStream("/dbconfig.properties");
			prop.load(input);
			// Reload The config file from local drive
			prop.load(new FileInputStream(prop.getProperty("SYNCFILE-LOCATION")
					.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public List<ImageVO> retrieveCopiedImages(Image image) throws IOException,
			DataFormatException {

		List<Image> imageList = imageService.getRecordImages(image);
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		List<ImageVO> extractedList = new ArrayList<ImageVO>();
		// for (Image img : imageList) {
		for (int itr = 0; itr < imageList.size(); itr += 2) {
			ImageVO vo = new ImageVO();
			Image img = imageList.get(itr);
			vo.setImgSrc1(populateImg(confProps, img).getName());
			if ((itr + 1) < imageList.size()) {
				Image img2 = imageList.get(itr + 1);
				vo.setImgSrc2(populateImg(confProps, img2).getName());
			} else {
				String outputFilePath = ServletActionContext
						.getServletContext().getRealPath("/")
						.concat(File.separator).concat("images")
						.concat(File.separator).concat("no-image-jasper.png");
				vo.setImgSrc2("file:///" + outputFilePath);
			}
			extractedList.add(vo);
		}

		return extractedList;
	}

	private byte[] showImage(Properties confProps, Image img)
			throws IOException, DataFormatException {
		try {
			String filePath = confProps.getProperty("file.upload.path")
					.concat(File.separator).concat(img.getHashedName());
			byte[] fileBytes = FileUtil.getBytes(new File(filePath));
			byte[] extractedBytes = CompressionUtil.extractBytes(fileBytes);
			String outputFilePath = confProps.getProperty("file.upload.path")
					.concat(File.separator).concat("extracted")
					.concat(File.separator).concat(img.getName());
			FileUtil.writeExtracedBytesToDisk(extractedBytes, outputFilePath);
			img.setName("file:///" + outputFilePath);
			byte[] outBytes = FileUtil.getBytes(new File(outputFilePath));
			return outBytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Image populateImg(Properties confProps, Image img)
			throws IOException, DataFormatException {
		String filePath = confProps.getProperty("file.upload.path")
				.concat(File.separator).concat(img.getHashedName());
		byte[] fileBytes = FileUtil.getBytes(new File(filePath));
		byte[] extractedBytes = CompressionUtil.extractBytes(fileBytes);
		String outputFilePath = confProps.getProperty("file.upload.path")
				.concat(File.separator).concat("extracted")
				.concat(File.separator).concat(img.getName());
		FileUtil.writeExtracedBytesToDisk(extractedBytes, outputFilePath);
		img.setName("file:///" + outputFilePath);
		return img;
	}

	public boolean showFile(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

	public byte[] showImage(Image img) throws IOException, DataFormatException {
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String filePath = confProps.getProperty("file.upload.path").concat(
				img.getHashedName());
		byte imageBytes[] = null;
		if (showFile(filePath)) {
			imageBytes = showImage(confProps, img);
		}
		return imageBytes;
	}

	public byte[] showImageByHashedName(String hashedName,String extraFolder) throws IOException,
			DataFormatException {
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String filePath = confProps.getProperty("file.upload.path").concat(extraFolder+"/").concat(
				hashedName);
		byte extractedBytes[] = null;
		if (showFile(filePath)) {
			 extractedBytes = FileUtil.getBytes(new File(filePath));
		}
		return extractedBytes;
	}

	public List<Map<String, String>> retriveRecordImages(Image image) {
		List<Image> imgs = imageService.getRecordImages(image);
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();

		for (Image img : imgs) {
			Map<String, String> imgMap = new HashMap<String, String>();
			imgMap.put("fileName", img.getName());
			imgMap.put("size", String.valueOf(img.getCompImgSize()));
			imgMap.put("progressMsg", "<img src = 'fileupload/success.png' />");
			imgMap.put("random", String.valueOf(AIOSCommons.getRandom()));
			imgMap.put("mode", "edit");
			imgMap.put("imgId", String.valueOf(img.getImageId()));
			imgMap.put("recId", String.valueOf(img.getRecordId()));
			imgMap.put("table", img.getTableName());
			imgMap.put("hashedName", img.getHashedName());
			list.add(imgMap);
		}
		return list;
	}

	public void deleteImage(Image img) {
		Properties confProps = (Properties) ServletActionContext
				.getServletContext().getAttribute("confProps");
		String filePath = confProps.getProperty("file.upload.path").concat(
				img.getHashedName());
		if (deleteFile(filePath)) {
			imageService.deleteImage(img);
		}
	}

	public boolean deleteImages(List<Object> objects) throws Exception {
		boolean returnFlag = true;
		List<Image> images = new ArrayList<Image>();
		for (Object object : objects) {

			String[] name = object.getClass().getName().split("entity.");
			String methodName = "get" + name[name.length - 1] + "Id";
			Method method = object.getClass().getMethod(methodName, null);
			images = imageService.getImagesByRecordId(
					(Long) method.invoke(object, null), name[name.length - 1]);

			for (Image image : images) {
				Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				String filePath = confProps.getProperty("file.upload.path")
						.concat(image.getHashedName());
				if (deleteFile(filePath)) {
					imageService.deleteImage(image);
				} else {
					returnFlag = false;
				}
			}

		}
		return returnFlag;
	}

	public boolean deleteFile(String filePath) {
		File file = new File(filePath);
		return file.delete();
	}

	private Image populateImage(Map.Entry<String, FileVO> entry, Object object,
			DocumentVO docVO) throws Exception {

		Image image = new Image();
		image.setName(entry.getKey());
		image.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		image.setTableName(name[name.length - 1]);
		image.setDisplayPane(docVO.getDisplayPane());
		image.setCompImgSize(entry.getValue().getCompressedSize());
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		image.setRecordId((Long) method.invoke(object, null));

		try {

			if (image.getTableName() != null
					&& image.getTableName()
							.trim()
							.equalsIgnoreCase(
									Product.class.getSimpleName().trim())) {

				String methodName1 = "getProductName";
				Method method1 = object.getClass().getMethod(methodName1, null);
				image.setSpecialName((String) method1.invoke(object, null));

				methodName1 = "getCode";
				method1 = object.getClass().getMethod(methodName1, null);
				image.setCode((String) method1.invoke(object, null));

			} else if (image.getTableName() != null
					&& image.getTableName() == Project.class.getSimpleName()) {

				String methodName1 = "getReferenceNumber";
				Method method1 = object.getClass().getMethod(methodName1, null);
				image.setSpecialName((String) method1.invoke(object, null));
				image.setCode((String) method1.invoke(object, null));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return image;
	}

	public void saveUploadedImages(Object object, DocumentVO docVO)
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		String[] name = object.getClass().getName().split("entity.");
		Map<String, byte[]> imageFileBytesMap = (Map<String, byte[]>) session
				.getAttribute("AIOS-img-" + name[name.length - 1] + "-"
						+ session.getId() + "-" + docVO.getDisplayPane() + "-"
						+ (Long) getRecordId(docVO));
		if (imageFileBytesMap != null) {
			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Map<String, FileVO> imageFilesMap = FileUtil.copyCachedFiles(
					imageFileBytesMap, confProps, null);

			List<Image> imagesList = new ArrayList<Image>();
			for (Map.Entry<String, FileVO> entry : imageFilesMap.entrySet()) {
				imagesList.add(populateImage(entry, object, docVO));
			}

			imageService.saveImages(imagesList);
			session.removeAttribute("AIOS-img-" + name[name.length - 1] + "-"
					+ session.getId() + "-" + docVO.getDisplayPane() + "-"
					+ (Long) getRecordId(docVO));
		}
	}

	private Object getRecordId(DocumentVO docVO) throws Exception {
		if (docVO.isEdit()) {
			Object object = docVO.getObject();
			return getObjectId(object);
		} else {
			return (long) -1;
		}

	}

	public void renameImageNameByFileHash(String imageHash, String newFileName)
			throws Exception {
		if (imageHash != null && !imageHash.equals("")) {
			Image image = imageService.getImagesByFileHash(imageHash);
			image.setName(newFileName);
			imageService.saveImage(image);
		}
	}

	private Long getObjectId(Object object) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		String[] name = object.getClass().getName().split("entity.");
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		return (Long) method.invoke(object, null);
	}

	public List<Image> getImageListForSlider() throws Exception {
		return imageService.getImagesByImplementation(getImplementation()
				.getImplementationId());
	}

	public List<Image> getImageListByImageIds(String ids) throws Exception {
		return imageService.getImageListByImageIds(ids);
	}

	public List<Image> getEntityBasedImageListByImplementation(
			Implementation implementation, String entity) throws Exception {
		return imageService.getEntityBasedImageListByImplementation(
				implementation, entity);
	}

	public List<Image> getRecordIdBasedImageListByImplementation(Long recordId,
			Implementation implementation, String entity) throws Exception {
		return imageService.getRecordIdBasedImageListByImplementation(recordId,
				implementation, entity);
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void updateSliderImageAndPhysicalFile(List<Image> images)
			throws DataFormatException {

		try {
			String direct = prop.getProperty("SLIDER-IMAGE-LOCATION").trim();
			File directoryBase = new File(direct + "/"
					+ getImplementation().getCompanyKey());

			if (!directoryBase.exists()) {
				try {
					directoryBase.mkdir();
				} catch (Exception se) {
					se.printStackTrace();
				}

			}

			File directoryHorizontal = new File(direct + "/"
					+ getImplementation().getCompanyKey() + "/h");
			File directoryVertical = new File(direct + "/"
					+ getImplementation().getCompanyKey() + "/v");

			if (!directoryHorizontal.exists()) {
				try {
					directoryHorizontal.mkdir();
				} catch (Exception se) {
					se.printStackTrace();
				}

			}

			if (!directoryVertical.exists()) {
				try {
					directoryVertical.mkdir();
				} catch (Exception se) {
					se.printStackTrace();
				}

			}

			imageService.saveImages(images);

			FileUtils.cleanDirectory(directoryHorizontal);
			FileUtils.cleanDirectory(directoryVertical);

			for (Image image : images) {
				if (!image.getIsSlider())
					continue;

				File destinationFileHorizontal = new File(direct + "/"
						+ getImplementation().getCompanyKey() + "/h/"
						+ image.getName());
				File destinationFileVertical = new File(direct + "/"
						+ getImplementation().getCompanyKey() + "/v/"
						+ image.getName());

				Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				String filePath = confProps.getProperty("file.upload.path")
						.concat(image.getHashedName());
				byte[] fileBytes = FileUtil.getBytes(new File(filePath));
				byte[] extractedBytes = CompressionUtil.extractBytes(fileBytes);
				FileUtil.writeExtracedBytesToDisk(extractedBytes,
						destinationFileHorizontal.getAbsolutePath());
				FileUtil.writeExtracedBytesToDisk(extractedBytes,
						destinationFileVertical.getAbsolutePath());

				try {
					// save horizontal image
					saveFramedImageForSlider(destinationFileHorizontal,
							new File(direct + "/frames/"
									+ getImplementation().getCompanyKey()
									+ "/h_frame.png"), direct, image, "h");
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					// save vertical image
					saveFramedImageForSlider(destinationFileVertical, new File(
							direct + "/frames/"
									+ getImplementation().getCompanyKey()
									+ "/v_frame.png"), direct, image, "v");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveFramedImageForSlider(File imageFile, File frameFile,
			String sliderFolderPath, Image targetImage, String orientation)
			throws IOException {

		float[] matrix = { 0.111f, 0.111f, 0.111f, 0.111f, 0.111f, 0.111f,
				0.111f, 0.111f, 0.111f, };

		File tempBlurImageFile = new File(sliderFolderPath + "/temp_"
				+ Math.random() + ".png");
		tempBlurImageFile.createNewFile();

		/*
		 * BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, matrix));
		 * BufferedImage srcImage = ImageIO.read(imageFile); BufferedImage
		 * destImage = ImageIO.read(tempBlurImageFile);
		 */

		// apply blur effect and save in temporary location
		// op.filter(srcImage, destImage);
		// ImageIO.write(destImage, "PNG",
		// new File(tempBlurImageFile.getAbsolutePath()));

		// by default same image file can be used
		File touchedImageFile = imageFile;
		Boolean imageTouched = false;

		if (orientation.equalsIgnoreCase("h")) {

			touchedImageFile = new File(sliderFolderPath + "/temp_"
					+ Math.random() + ".png");
			touchedImageFile.createNewFile();
			imageTouched = true;

			BufferedImage enlargeThis = ImageIO.read(imageFile);
			int type = enlargeThis.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
					: enlargeThis.getType();

			ImageIO.write(enlargeImage(enlargeThis, 1840, 1000, type), "PNG",
					touchedImageFile);
		}

		// merge the frame on top of blurred image
		mergeImages(touchedImageFile, frameFile, true, tempBlurImageFile,
				false, null, orientation);
		// merge the actual image on top of framed image
		mergeImages(imageFile, tempBlurImageFile, false, null, false, null,
				orientation);

		try {
			// delete temporary image files
			tempBlurImageFile.delete();

			if (imageTouched)
				touchedImageFile.delete();

		} catch (Exception e) {
			e.printStackTrace();
		}

		BufferedImage codeBannerImage = getBannerCodeImage(new File(
				sliderFolderPath + "/frames/"
						+ getImplementation().getCompanyKey() + "/banner.png"),
				targetImage);

		mergeImages(imageFile, null, false, null, true, codeBannerImage,
				orientation);
	}

	private void mergeImages(File inner, File outter, Boolean replaceInner,
			File tempFile, Boolean isBanner, BufferedImage banner,
			String orientation) {

		try {

			if (isBanner) {

				BufferedImage orgImage = ImageIO.read(inner);
				BufferedImage bannerImage = banner;

				Graphics2D g = orgImage.createGraphics();
				g.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.8f));

				g.drawImage(bannerImage, 0, 0, null);
				g.dispose();

				ImageIO.write(orgImage, "PNG",
						new File(inner.getAbsolutePath()));
			} else {

				BufferedImage biInner = ImageIO.read(inner);
				BufferedImage biOutter = ImageIO.read(outter);

				int x = 0;
				int y = 0;

				if (!replaceInner) {
					try {
						int type = biInner.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
								: biInner.getType();
						BufferedImage resizeImageJpg = resizeImage(biInner,
								type, biOutter, orientation);
						biInner = resizeImageJpg;

						Double paddingLeft = biOutter.getWidth() * 0.07;
						Double paddingTop = biOutter.getHeight() * 0.07;

						if (orientation.equalsIgnoreCase("h")) {
							paddingLeft = biOutter.getWidth() * 0.25;
							paddingTop = biOutter.getHeight() * 0.14;
						}

						x = paddingLeft.intValue();
						y = paddingTop.intValue();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				Graphics2D g = biOutter.createGraphics();
				if (replaceInner)
					g.setComposite(AlphaComposite.getInstance(
							AlphaComposite.SRC_OVER, 0.2f));
				else
					g.setComposite(AlphaComposite.getInstance(
							AlphaComposite.SRC_OVER, 1.0f));

				g.drawImage(biInner, x, y, null);
				g.dispose();

				if (replaceInner)
					ImageIO.write(biOutter, "PNG",
							new File(tempFile.getAbsolutePath()));
				else
					ImageIO.write(biOutter, "PNG",
							new File(inner.getAbsolutePath()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private BufferedImage resizeImage(BufferedImage originalImage, int type,
			BufferedImage forResize, String resizingFor) {

		// default setting is for vertical image
		Double reduceWidthPercentage = 0.14;
		Double reduceHeightPercentage = 0.38;

		if (resizingFor.equalsIgnoreCase("h")) {
			reduceWidthPercentage = 0.50;
		}

		Double reduceWidth = forResize.getWidth() * reduceWidthPercentage;
		Double reduceHeight = forResize.getHeight() * reduceHeightPercentage;

		Double temp = forResize.getWidth() - reduceWidth;
		int IMG_WIDTH = temp.intValue();

		temp = forResize.getHeight() - reduceHeight;
		int IMG_HEIGHT = temp.intValue();

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
				type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();

		return resizedImage;
	}

	private BufferedImage enlargeImage(BufferedImage originalImage,
			int IMG_WIDTH, int IMG_HEIGHT, int type) {

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
				type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();

		return resizedImage;
	}

	private BufferedImage getBannerCodeImage(File bannerFile, Image targetImage)
			throws IOException {

		final BufferedImage image = ImageIO.read(bannerFile);
		String requiredText = "";

		if (targetImage.getCode() != null) {
			requiredText = targetImage.getCode().toUpperCase();
			if (requiredText.length() > 14) {
				requiredText = requiredText.substring(0, 14);
			}
		}

		Graphics2D g = (Graphics2D) image.getGraphics();
		AffineTransform orig = g.getTransform();

		g.setFont(new Font("ARIAL", Font.BOLD, 18));
		g.setColor(Color.WHITE);
		g.rotate(Math.toRadians(-40.0));
		g.drawString(requiredText, -30, 110);
		g.dispose();

		return image;
	}
}
