package com.aiotech.aios.workflow.util;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Quantum {
	
	String screen;
	String field;
	String condition;
	
	public String getScreen() {
		return screen;
	}
	@XmlElement
	public void setScreen(String screen) {
		this.screen = screen;
	}
	public String getField() {
		return field;
	}
	@XmlElement
	public void setField(String field) {
		this.field = field;
	}
	public String getCondition() {
		return condition;
	}
	@XmlElement
	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String toString(){
		return screen+" | "+field+" | "+condition;
	}
}
