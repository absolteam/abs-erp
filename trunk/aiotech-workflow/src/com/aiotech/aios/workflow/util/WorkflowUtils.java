/**
 * 
 */
package com.aiotech.aios.workflow.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ContextLoader;

import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */
public class WorkflowUtils {

	public static Object createNewEntityObject(
			String entityClassFullyQualifiedName) throws Exception {

		Class entityClass = Class.forName(entityClassFullyQualifiedName);
		return entityClass.getConstructor(null).newInstance(null);
	}

	public static AIOTechGenericDAO<Object> getEntityDAO(
			String entityClassFullyQualifiedName) throws Exception {

		String entityClassName = getEntityClassName(entityClassFullyQualifiedName);

		/* Get EntityService bean object from Context */
		Object serviceClassObject = getEntityService(entityClassName);

		/* Get EntityDAO object injected into Service */
		return getEntityDAO(entityClassName, serviceClassObject);
	}

	public static String getEntityClassName(String entityClassFullyQualifiedName)
			throws Exception {
		String useCaseTemp=entityClassFullyQualifiedName.replace(".domain.entity.", ".DOMAIN.ENTITY.");
		String[] splittedName = useCaseTemp.split(".DOMAIN.ENTITY.");
		//String[] splittedName = entityClassFullyQualifiedName.split("entity.");
		return splittedName[splittedName.length - 1];
	}

	public static Object getEntityObjectId(Object entityObject,
			String entityClassName) throws Exception {

		Method identityMethod = null;
		String methodName = "get" + entityClassName + "Id";
		identityMethod = entityObject.getClass().getMethod(methodName, null);
		Object recordId = identityMethod.invoke(entityObject, null);
		return recordId;

	}

	public static void setEntityObjectId(Object entityObject,
			String entityClassName, Object id) throws Exception {

		Method identityMethod = null;
		String methodName = "set" + entityClassName + "Id";

		if (id == null) {
			identityMethod = entityObject.getClass().getMethod(methodName,
					Long.class);
		} else {
			identityMethod = entityObject.getClass().getMethod(methodName,
					Long.class);
		}

		identityMethod.invoke(entityObject, id);

	}

	public static Object getEntityService(String entityClassName)
			throws Exception {

		ApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();

		return ctx.getBean(StringUtils.uncapitalize(entityClassName)
				+ "Service");
	}
	
	public static Object getEntityBL(String entityClassName)
	throws Exception {

		ApplicationContext ctx = ContextLoader
				.getCurrentWebApplicationContext();
		
		return ctx.getBean(StringUtils.uncapitalize(entityClassName)
				+ "BL");
	}

	private static AIOTechGenericDAO<Object> getEntityDAO(
			String entityClassName, Object entityServiceObject)
			throws Exception {

		String entityDAOGetter = "get" + entityClassName + "DAO";

		Method getDAOmethod = entityServiceObject.getClass().getMethod(
				entityDAOGetter, null);

		return (AIOTechGenericDAO<Object>) getDAOmethod
				.invoke(entityServiceObject);
	}

	public static Long getImplementationId() throws Exception {
		try {
			Object object = (Object) (ServletActionContext.getRequest()
					.getSession().getAttribute("THIS"));
			Method method1 = object.getClass().getMethod("getImplementationId",
					null);
			return (Long) method1.invoke(object, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
