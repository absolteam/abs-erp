/**
 * 
 */
package com.aiotech.aios.workflow.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Haris
 * 
 */
public class WorkflowConstants {

	public static enum Status {
		Confirm(4), //approval (waiting for approval)
		Deny(2), 
		NoDecession(3), // Entry
		Publish(9), // approval (waiting for publish)
		Saved(5),
		Post(6),
		Posted(7),
		Approved(8),
		Published(1),
		DeleteApproval(10), // waiting for delete approval
		Deleted(11), // finally deleted the specified record (i.e. set record's status = 11)
		DeleteRejected(12), // delete request has been rejected
		DeleteApprovalReadOnly(13), // used to show mark as read message pop-up for "delete request has been sent"
		OldPublished(50);//being used to differentiate the record from edited and old published.
		private static final Map<Integer, Status> lookup = new HashMap<Integer, Status>();

		static {
			for (Status s : EnumSet.allOf(Status.class)) {
				lookup.put(s.getCode(), s);
			}
		}

		private int code;

		private Status(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}

		public static Status get(int code) {
			return lookup.get(code);
		}
	}

	public static enum OperationGroup {
		Entry(1), // entry(save,add) process
		Preview(2), Approval(3), // Approval and preview the content
		Post(4); // post transaction
		private static final Map<Integer, OperationGroup> lookup = new HashMap<Integer, OperationGroup>();

		static {
			for (OperationGroup s : EnumSet.allOf(OperationGroup.class)) {
				lookup.put(s.getCode(), s);
			}
		}

		private int code;

		private OperationGroup(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}

		public static OperationGroup get(int code) {
			return lookup.get(code);
		}
	}
	
	public static enum ProcessMethod {
		Add(1), 
		Modify(2), Delete(3);
		 // post trModifyansaction
		private static final Map<Integer, ProcessMethod> lookup = new HashMap<Integer, ProcessMethod>();

		static {
			for (ProcessMethod s : EnumSet.allOf(ProcessMethod.class)) {
				lookup.put(s.getCode(), s);
			}
		}

		private int code;

		private ProcessMethod(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}

		public static ProcessMethod get(char code) {
			return lookup.get(code);
		}
	}
	
	public static enum ReturnStatus {
		Added(1),
		AddApproved(2), 
		AddRejected(3), 
		Modified(4),
		ModificationApproved(5),
		ModificationRejected(6),
		Delete(7),
		DeleteApproved(8),
		DeleteRejected(9),
		Success(10), 
		Error(11); 
		private static final Map<Integer, ReturnStatus> lookup = new HashMap<Integer, ReturnStatus>();

		static {
			for (ReturnStatus s : EnumSet.allOf(ReturnStatus.class)) {
				lookup.put(s.getCode(), s);
			}
		}

		private int code;

		private ReturnStatus(int code) {
			this.code = code;
		}

		public int getCode() {
			return code;
		}

		public static ReturnStatus get(int code) {
			return lookup.get(code);
		}
	}
}
