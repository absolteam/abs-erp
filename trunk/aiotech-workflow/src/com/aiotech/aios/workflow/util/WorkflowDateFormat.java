package com.aiotech.aios.workflow.util;

/*******************************************************************************
 *
 * Create Log
 * -----------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 Description
 * -----------------------------------------------------------------------------
 * 1.0		30 Aug 2010 	Mohamed rafiq.S 		   Initial Version

 ******************************************************************************/

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class WorkflowDateFormat {
	public static final String DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
	public static final String TO_DATE_FORMAT = "yyyy-mm-dd HH:mm:sss";
	public static final String DATE_PICKER_FORMAT = "MM/dd/yyyy";
	public static final String LONG_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";

	public static String getToday() {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DATE);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);
		return day + "/" + month + "/" + year;
	}

	/*
	 * Add Day/Month/Year to a Date add() is used to add values to a Calendar
	 * object. You specify which Calendar field is to be affected by the
	 * operation (Calendar.YEAR, Calendar.MONTH, Calendar.DATE).
	 */
	public static String addToDate(String DATE_FORMAT, int addMonth) {
		System.out.println("In the ADD Operation");
		// String DATE_FORMAT = "yyyy-MM-dd";
		// String DATE_FORMAT = "dd-MM-yyyy"; //Refer Java DOCS for formats
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		Date d1 = new Date();
		System.out.println("Todays date in Calendar Format : " + c1);
		System.out.println("c1.getTime() : " + c1.getTime());
		System.out.println("c1.get(Calendar.YEAR): " + c1.get(Calendar.YEAR));
		System.out.println("Todays date in Date Format : " + d1);
		c1.set(1999, 0, 20); // (year,month,date)
		System.out.println("c1.set(1999,0 ,20) : " + c1.getTime());
		c1.add(Calendar.DATE, 40);
		System.out.println("Date + 20 days is : " + sdf.format(c1.getTime()));
		return sdf.format(c1.getTime());
	}

	/*
	 * Substract Day/Month/Year to a Date roll() is used to substract values to
	 * a Calendar object. You specify which Calendar field is to be affected by
	 * the operation (Calendar.YEAR, Calendar.MONTH, Calendar.DATE).
	 * 
	 * Note: To substract, simply use a negative argument. roll() does the same
	 * thing except you specify if you want to roll up (add 1) or roll down
	 * (substract 1) to the specified Calendar field. The operation only affects
	 * the specified field while add() adjusts other Calendar fields. See the
	 * following example, roll() makes january rolls to december in the same
	 * year while add() substract the YEAR field for the correct result
	 */

	public static String subToDate(String DATE_FORMAT, int subtractionMonth) {
		System.out.println("In the SUB Operation");
		// String DATE_FORMAT = "dd-MM-yyyy";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		c1.set(1999, 0, 20);
		System.out.println("Date is : " + sdf.format(c1.getTime()));

		// roll down, substract 1 month
		c1.roll(Calendar.MONTH, false);
		System.out.println("Date roll down 1 month : "
				+ sdf.format(c1.getTime()));

		c1.set(1999, 0, 20);
		System.out.println("Date is : " + sdf.format(c1.getTime()));
		c1.add(Calendar.MONTH, subtractionMonth);
		// substract 1 month
		System.out.println("Date minus 1 month : " + sdf.format(c1.getTime()));
		return sdf.format(c1.getTime());
	}

	public static long daysBetween2Dates() {
		Calendar c1 = Calendar.getInstance(); // new GregorianCalendar();
		Calendar c2 = Calendar.getInstance(); // new GregorianCalendar();
		c1.set(1999, 0, 20);
		c2.set(1999, 0, 22);
		System.out.println("Days Between " + c1.getTime() + "\t" + c2.getTime()
				+ " is");
		System.out.println((c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000));
		return (c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000);
	}

	public static long daysBetween2Dates(Date date1, Date date2) {
		Calendar c1 = Calendar.getInstance(); // new GregorianCalendar();
		Calendar c2 = Calendar.getInstance(); // new GregorianCalendar();
		c1.setTime(date1);
		c2.setTime(date2);

		System.out.println("Days Between " + c1.getTime() + "\t" + c2.getTime()
				+ " is");
		System.out.println((c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000));
		return (c2.getTime().getTime() - c1.getTime().getTime())
				/ (24 * 3600 * 1000);
	}

	public static int daysInMonth() {
		Calendar c1 = Calendar.getInstance(); // new GregorianCalendar();
		c1.set(1999, 6, 20);
		int year = c1.get(Calendar.YEAR);
		int month = c1.get(Calendar.MONTH);
		// int days = c1.get(Calendar.DATE);
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		daysInMonths[1] += WorkflowDateFormat.isLeapYear(year) ? 1 : 0;
		System.out.println("Days in " + month + "th month for year " + year
				+ " is " + daysInMonths[c1.get(Calendar.MONTH)]);
		return daysInMonths[c1.get(Calendar.MONTH)];
	}

	public static String getDayofTheDate() {
		Date d1 = new Date();
		String day = null;
		SimpleDateFormat f = new SimpleDateFormat("EEEE");
		try {
			day = f.format(d1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("The dat for " + d1 + " is " + day);
		return day;
	}

	public static boolean validateAGivenDate(String dt) {
		// String dt = "20011223";
		// String invalidDt = "20031315";
		// String dateformat = "yyyyMMdd";
		Date dt1 = null, dt2 = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			sdf.setLenient(false);
			dt1 = sdf.parse(dt);
			// dt2 = sdf.parse(invalidDt);
			System.out.println("Date is ok = " + dt1 + "(" + dt + ")");
			return true;
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return false;
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid date");
			return false;
		}
	}

	public static void compare2Dates() {
		SimpleDateFormat fm = new SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();

		c1.set(2000, 02, 15);
		c2.set(2001, 02, 15);

		System.out.print(fm.format(c1.getTime()) + " is ");
		if (c1.before(c2)) {
			System.out.println("less than " + c2.getTime());
		} else if (c1.after(c2)) {
			System.out.println("greater than " + c2.getTime());
		} else if (c1.equals(c2)) {
			System.out.println("is equal to " + fm.format(c2.getTime()));
		}

	}

	/**
	 * This Method compares two dates.
	 * 
	 * @param date1
	 *            :
	 * @param date2
	 *            :
	 * @return: LESS, GREATER, EQUAL
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	public static String compare2Dates(Date date1, Date date2) {

		String result = "";

		Calendar c1 = Calendar.getInstance();
		c1.setTime(date1);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(date2);

		if (c1.before(c2)) {
			result = "LESS";
		} else if (c1.after(c2)) {
			result = "GREATER";
		} else if (c1.equals(c2)) {
			result = "EQUAL";
		}

		return result;
	}

	public static boolean isLeapYear(int year) {
		if (((year % 100) != 0) || ((year % 400) == 0)) {
			return true;
		}
		return false;
	}

	public static String dateFormater() {
		Date todaysDate = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		String formattedDate = formatter.format(todaysDate);
		System.out.println("Today�s date and Time is:" + formattedDate);
		return formattedDate;
	}

	/*
	 * //Added by rafiq public Date convertStringToDate(String str_date) throws
	 * ParseException{
	 * 
	 * Date date = new Date(); SimpleDateFormat formatter ; formatter = new
	 * SimpleDateFormat(DB_DATE_FORMAT); date = (Date)formatter.parse(str_date);
	 * return date; }
	 */
	public static Date convertStringToDate(String inputDate) { // format is the
																// required long
																// or short
																// format
		String format = DB_DATE_FORMAT;
		Date date = null;
		try {
			if ((inputDate != null) && (inputDate.trim().length() > 0)) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"
				date = new Date();
				SimpleDateFormat inFormat = new SimpleDateFormat(
						DATE_PICKER_FORMAT); // input format "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if ((format != null) && format.trim().equalsIgnoreCase("long")) {
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("sort")) {
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("number")) {
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("noLanguage")) {
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				} else {
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy");
				}

				date = inFormat.parse(inputDate);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return date;
		}
	}

	public static Date convertStringToDBTime(String time) {

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			return dateFormat.parse(time);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String convertDateToString(String inputDate) { // format is
																	// the
																	// required
																	// long or
																	// short
																	// format
		String outString = "";
		String format = DATE_PICKER_FORMAT;
		try {
			if ((inputDate != null) && (inputDate.trim().length() > 0)) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if ((format != null) && format.trim().equalsIgnoreCase("long")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy"); // output
																	// fromat
																	// "dd-MMMM-yyyy"
																	// ->
																	// long
																	// :
																	// "dd-MMM-yyyy"
																	// ->
																	// Short
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("sort")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("number")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("noLanguage")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				}

				Date date = inFormat.parse(inputDate);
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	@SuppressWarnings("finally")
	public static String convertDateToDate(String inputDate) {
		String outString = "";
		String format = DATE_PICKER_FORMAT;
		String time[];
		time = inputDate.split(" ");
		try {
			if ((inputDate != null) && (inputDate.trim().length() > 0)) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if ((format != null) && format.trim().equalsIgnoreCase("long")) {
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy"); // output
																		// fromat
																		// "dd-MMMM-yyyy"
																		// ->
																		// long
																		// :
																		// "dd-MMM-yyyy"
																		// ->
																		// Short
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("sort")) {
					outFormat = new SimpleDateFormat("dd-MMM-yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("number")) {
					outFormat = new SimpleDateFormat("yyyy-MM-dd");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("noLanguage")) {
					outFormat = new SimpleDateFormat("dd-MM-yyyy");
				} else {
					outFormat = new SimpleDateFormat("dd-MMMM-yyyy");
				}

				Date date = inFormat.parse(inputDate);
				outString = outFormat.format(date);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString + " " + time[1];
		}
	}

	public static String convertSystemDateToString(Date inputDate) { // format
																		// is
																		// the
																		// required
																		// long
																		// or
																		// short
																		// format
		String outString = "";
		String format = "yyyy/MM/dd HH:mm:ss";
		try {
			if (inputDate != null) {
				// String sDate= "2008-06-12 00:12:20.0"; // "2008-06-12"

				SimpleDateFormat inFormat = new SimpleDateFormat(DB_DATE_FORMAT); // input
																					// format
																					// "yyyy-MM-dd"
				SimpleDateFormat outFormat = null;
				if ((format != null) && format.trim().equalsIgnoreCase("long")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy"); // output
																	// fromat
																	// "dd-MMMM-yyyy"
																	// ->
																	// long
																	// :
																	// "dd-MMM-yyyy"
																	// ->
																	// Short
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("sort")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("number")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else if ((format != null)
						&& format.trim().equalsIgnoreCase("noLanguage")) {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				} else {
					outFormat = new SimpleDateFormat("dd/MM/yyyy");
				}

				Date date = inputDate;
				StringBuffer sb = new StringBuffer();
				FieldPosition fldPosition = null;
				outString = outFormat.format(date);

				// System.out.println("----------------------------------------------------------------"
				// + outString);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return outString;
		}
	}

	public static int monthDifference(Date from, Date to) {
		Calendar fromDate = Calendar.getInstance();
		Calendar toDate = Calendar.getInstance();
		fromDate.setTime(from);
		toDate.setTime(to);
		double monthsBetween = 0;
		// difference in month for years
		monthsBetween = (toDate.get(Calendar.YEAR) - fromDate
				.get(Calendar.YEAR)) * 12;
		// difference in month for months
		monthsBetween += toDate.get(Calendar.MONTH)
				- fromDate.get(Calendar.MONTH);
		// difference in month for days
		if ((toDate.get(Calendar.DAY_OF_MONTH) != toDate
				.getActualMaximum(Calendar.DAY_OF_MONTH))
				&& (toDate.get(Calendar.DAY_OF_MONTH) != toDate
						.getActualMaximum(Calendar.DAY_OF_MONTH))) {
			monthsBetween += ((toDate.get(Calendar.DAY_OF_MONTH) - fromDate
					.get(Calendar.DAY_OF_MONTH)) / 31d);
		}
		return (int) monthsBetween;
	}

	/**
	 * This Method adds DATE, HOURS, MONTH etc to the provided date.
	 * 
	 * @param date
	 *            : Actual Date
	 * @param field
	 *            : Calendar field
	 * @param amount
	 *            : amount to add to the field of Actual Date
	 * @return: Modified Date
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	public static Date addField(Date date, int field, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(field, amount);
		return cal.getTime();
	}

	public static String getCurrentTimeStamp() {
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat(
				"yyyy/MMM/dd HH:mm:ss");
		String dateTime = formatter.format(currentDate.getTime());
		return dateTime;
	}

	public static boolean checkValidity(Date date1, int validUpto) {

		long days = 0;

		Date today = Calendar.getInstance().getTime();

		days = WorkflowDateFormat.daysBetween2Dates(date1, today);

		if (days < validUpto) {
			return false;
		} else {
			return true;
		}

	}

	public static Integer setOldDays(Date updatedDate) {
		Date today = Calendar.getInstance().getTime();
		Long days = WorkflowDateFormat.daysBetween2Dates(updatedDate, today);
		return days.intValue();
	}

	// /**
	// * This Method adds specified no. of days to the provided date.
	// *
	// * @param date
	// * : Actual Date
	// * @param days
	// * : No. of days to add in actual date
	// * @return: Incremented Date
	// *
	// * @author Shoaib Ahmad Gondal @ 26-Sep-12
	// */
	// public static Date addDays(Date date, int days) {
	// Calendar cal = Calendar.getInstance();
	// cal.setTime(date);
	// cal.add(Calendar.DATE, days); // minus value will decrement the day
	// return cal.getTime();
	// }
	//
	// /**
	// * This Method adds specified no. of hours to the provided date's hours.
	// *
	// * @param date
	// * : Actual Date
	// * @param hours
	// * : No. of hours to add in actual date
	// * @return: Incremented Date
	// *
	// * @author Shoaib Ahmad Gondal @ 26-Sep-12
	// */
	// public static Date addHours(Date date, int hours) {
	// Calendar cal = Calendar.getInstance();
	// cal.setTime(date);
	// cal.add(Calendar.HOUR, hours); // minus value will decrement hours
	// return cal.getTime();
	// }
}
