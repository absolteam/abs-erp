package com.aiotech.aios.workflow.util;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "quantums")
public class Quantums {
	 @XmlElement(name = "quantum", type = Quantum.class)
	    private List<Quantum> quantums = new ArrayList<Quantum>();
	 
	    public Quantums() {}
	 
	    public Quantums(List<Quantum> quantums) {
	        this.quantums = quantums;
	    }
	 
	    public List<Quantum> getQuantums() {
	        return quantums;
	    }
	 
	    public void setQuantums(List<Quantum> quantums) {
	        this.quantums = quantums;
	    }
}
