package com.aiotech.aios.workflow.enterprise;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aiotech.aios.workflow.bl.GenerateNotificationsBL;
import com.aiotech.aios.workflow.bl.GetNotificationsAndAlertsBL;
import com.aiotech.aios.workflow.bl.HandleUserActionBL;
import com.aiotech.aios.workflow.bl.SaveWorkflowBL;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.vo.ApproverPublisherVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;

/**
 * Class acting as a Facade to Workflow module. Exposes workflow functionality
 * required by other modules.
 * <p>
 * Only this class should be injected as a dependency in other classes where
 * Workflow methods are to be called from.
 * 
 * @author Muhammad Haris
 * @version 1.0, 16 Apr 2013
 * 
 */
@Service
public class WorkflowEnterpriseService {

	private GetNotificationsAndAlertsBL notificationsAndAlertsBL;
	private HandleUserActionBL userActionBL;
	private GenerateNotificationsBL generateNotificationsBL;
	private SaveWorkflowBL saveWorkflowBL;
	 
	/**
	 * Get the approver and publisher details combined from workflow detail
	 * process, workflow and module process tables
	 * 
	 * @param: targetItems: list of target records
	 * @return HashMap with personId of publisher and approver; against key
	 *         format = processId_recordId
	 * @throws Exception
	 */
	public HashMap<String, ApproverPublisherVO> populateApproverPublisherDetail(
			HashMap<String, ApproverPublisherVO> targetItems) {

		try {
			return userActionBL.collectApproverAndPublisherInfo(targetItems);
		} catch (Exception e) {
			e.printStackTrace();
			return targetItems;
		}
	}

	/* GET NOTIFICATIONS Methods */
	/* ========================= */
	/**
	 * Gets workflow related notifications for a user. It should be used to show
	 * notifications on Dashboard
	 * 
	 * @param operations
	 *            list containing workflow constants. See
	 *            {@link WorkflowConstants}
	 * 
	 * @param isAlert
	 *            boolean to indicate only alert messages (like data expiration)
	 *            are to be fetched
	 * @param user
	 *            user who is logged-in
	 * @return list of notifications found for this user
	 * @throws Exception
	 */
	public List<WorkflowDetailVO> getNotifications(List<Byte> operations,
			Boolean isAlert, User user) throws Exception {
		return notificationsAndAlertsBL.getNotificationList(operations,
				isAlert, user);
	}

	/**
	 * Checks whether a flow has been escalated as defined and creates
	 * notifications for given user.
	 * <p>
	 * It should be called upon from a scheduler. Previously being called from
	 * Dashboard
	 * 
	 * @param personId
	 *            user for whom escalated notifications are to be generated
	 * @param operations
	 *            list containing workflow constants. See
	 *            {@link WorkflowConstants}
	 * @param status
	 *            boolean to indicate only unread notifications are to be
	 *            fetched
	 * @throws Exception
	 */
	public void generateEscalatedNotifications(long personId,
			List<Byte> operations, Byte status) throws Exception {
		generateNotificationsBL.generateEscalatedNotifications(personId,
				operations, status);
	}

	/* HANDLE USER ACTION Methods */
	/* ========================= */
	/**
	 * Handles action performed by a user against a workflow notification that
	 * is approval, rejection etc.
	 * 
	 * @param workflowDetailId
	 *            a long value representing id of a flow of a process the
	 *            notification belongs to
	 * @param recordId
	 *            a long value representing id of the record whose corresponding
	 *            notification is acted upon
	 * @param operation
	 *            a byte constant indicating operation performed by the user.
	 *            See {@link WorkflowConstants}
	 * @param volist
	 *            a list containing all flows for the process the notification
	 *            belongs to
	 * @param comment
	 *            a comment object representing comment provided by the user
	 * @param user
	 *            the user who acted upon the notification
	 * @throws Exception
	 */
	public WorkflowVO handleUserActionAgainstNotification(
			long workflowDetailId, long recordId, Byte operation,
			List<WorkflowDetailVO> volist, Comment comment, User user)
			throws Exception {
		WorkflowVO vo = userActionBL.handleUserActionAgainstNotification(
				workflowDetailId, recordId, operation, volist, comment, user);
		return vo;
	}

	public WorkflowVO handleUserActionAgainstNotificationForPost(
			WorkflowDetailVO workflowDetailVO, Long recordId, User user)
			throws Exception {
		WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO = null;

		objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(workflowDetailVO
				.getMessage().getMessageId());
		List<WorkflowDetailVO> volist = new ArrayList<WorkflowDetailVO>();
		volist.add(objSeletectedNotificationWorkflowDetailVO);
		WorkflowVO returnVO = new WorkflowVO();
		returnVO = this
				.handleUserActionAgainstNotification(
						objSeletectedNotificationWorkflowDetailVO
								.getWorkflowDetailId(), recordId,
						workflowDetailVO.getProcessType(), volist, null, user);
		return returnVO;
	}

	private WorkflowDetailVO getWorkflowDetailVObyMessageId(Long messageId) {
		WorkflowDetailVO vo = new WorkflowDetailVO();
		try {

			if (messageId != null) {
				List<Message> messages = null;

				messages = this.getGenerateNotificationsBL()
						.getMessageService()
						.getMessageWithWorkflowProcessDetailsByMsgId(messageId);

				if (messages.size() > 0) {
					Message message = messages.get(0);

					WorkflowDetail workflowDetail = message
							.getWorkflowDetailProcess().getWorkflowDetail();

					// Fetch actual record from process's table
					Object recordObject = this
							.getGenerateNotificationsBL()
							.getWorkflowService()
							.getRecord(
									workflowDetail.getWorkflow()
											.getModuleProcess().getUseCase(),
									message.getWorkflowDetailProcess()
											.getRecordId());

					// fill in workflowdetailVO necessary info and add in
					// workflowDetailVOs

					vo.setWorkflow(workflowDetail.getWorkflow());
					vo.setScreenId(message.getWorkflowDetailProcess()
							.getScreen().getScreenId());
					vo.setScreenPath(message.getWorkflowDetailProcess()
							.getScreen().getScreenPath());
					vo.setScreenByScreenId(message.getWorkflowDetailProcess()
							.getScreen());
					vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
					vo.setOperation(message.getWorkflowDetailProcess()
							.getOperation());
					vo.setOperationOrder(message.getWorkflowDetailProcess()
							.getWorkflowDetail().getOperationOrder());
					vo.setWorkflowDetailProcess(message
							.getWorkflowDetailProcess());
					vo.setOperationMaster(message.getWorkflowDetailProcess()
							.getWorkflowDetail().getOperationMaster());
					if (workflowDetail.getWorkflowDetail() != null)
						vo.setParentId(workflowDetail.getWorkflowDetail()
								.getWorkflowDetailId());
					vo.setObject(recordObject);
					vo.setRecordId(message.getWorkflowDetailProcess()
							.getRecordId());
					vo.setMessage(message);

					java.text.DateFormat df = new SimpleDateFormat(
							"EEE, d MMM yyyy hh:mm aaa");
					String formatedDate = df.format(message.getCreatedDate());
					vo.setFormatednotificationTime(formatedDate);

					vo.setProcessFlag(message.getWorkflowDetailProcess()
							.getOperation());
					vo.setWorkflowDetailProcess(message
							.getWorkflowDetailProcess());

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;

	}

	/* RECORD PROCESS ENTRY Methods */
	/* ========================= */
	/**
	 * Generates notifications against a data entry operation for concerned
	 * users as defined in the workflow.
	 * 
	 * @param useCase
	 *            a string representing process against which notifications are
	 *            to be generated
	 * @param recordId
	 *            a long value representing id of the record which is just
	 *            created, that is primary key of a record
	 * @param user
	 *            the user who performed the data entry operation
	 * @throws Exception
	 */
	public Integer generateNotificationsAgainstDataEntry(String useCase,
			Long recordId, User user, WorkflowDetailVO workflowDetailVO)
			throws Exception {
		return generateNotificationsBL.generateNotificationsAgainstDataEntry(
				useCase, recordId, user, workflowDetailVO);
	}

	/**
	 * Updates status of a notification to indicate that it's been acted upon by
	 * the user.
	 * 
	 * @param workflowDetailVO
	 *            a {@link WorkflowDetailVO} object representing workflow
	 *            associated with the notification
	 * @param recordId
	 *            a long value representing id of the record whose corresponding
	 *            notification status is to be updated
	 * @param status
	 *            a byte value representing notification status which is to be
	 *            set
	 * @param user
	 *            the user who acted upon the notification
	 */
	public void updateNotificationStatus(WorkflowDetailVO workflowDetailVO,
			Long recordId, Byte status, User user) throws Exception {
		generateNotificationsBL.updateNotificationStatus(workflowDetailVO,
				recordId, status, user);
	}

	/**
	 * Checks whether editing an entity object is allowed or not. If a record's
	 * current state is pending for first approval, then it is not allowed to
	 * save this record.
	 * 
	 * @param entityObject
	 *            an object of an Entity corresponding to a table
	 * @param user
	 *            the user who's wants to edit this record
	 * @return a boolean indicating whether saving is allowed or not
	 * @throws Exception
	 */
	public boolean isAllowedToEdit(Object entityObject, User user)
			throws Exception {
		return saveWorkflowBL.isAllowedToEdit(entityObject, user);
	}

	/**
	 * Saves a message to be shown as a notification. It is required by some
	 * module which needs to generate messages upon an event like data expiry.
	 * 
	 * @param message
	 *            the message to be saved
	 * @throws Exception
	 */
	public void saveMessage(Message message) throws Exception {
		generateNotificationsBL.saveMessage(message);
	}

	public boolean isDirectAddBusinessCall(Long messageId) throws Exception {
		if (messageId != null && messageId > 0)
			return generateNotificationsBL.isDirectAddBusinessCall(messageId);
		else
			return false;
	}

	public boolean isDirectEditBusinessCall(Long messageId) throws Exception {
		if (messageId != null && messageId > 0)
			return generateNotificationsBL.isDirectEditBusinessCall(messageId);
		else
			return false;
	}

	public boolean isDirectDeleteBusinessCall(Long messageId) throws Exception {
		if (messageId != null && messageId > 0)
			return generateNotificationsBL
					.isDirectDeleteBusinessCall(messageId);
		else
			return false;
	}

	/* SETTERS & GETTERS for BL Classes */
	/* ================================ */
	public GetNotificationsAndAlertsBL getNotificationsAndAlertsBL() {
		return notificationsAndAlertsBL;
	}

	@Autowired
	public void setNotificationsAndAlertsBL(
			GetNotificationsAndAlertsBL notificationsAndAlertsBL) {
		this.notificationsAndAlertsBL = notificationsAndAlertsBL;
	}

	public HandleUserActionBL getUserActionBL() {
		return userActionBL;
	}

	@Autowired
	public void setUserActionBL(HandleUserActionBL userActionBL) {
		this.userActionBL = userActionBL;
	}

	public GenerateNotificationsBL getGenerateNotificationsBL() {
		return generateNotificationsBL;
	}

	@Autowired
	public void setGenerateNotificationsBL(
			GenerateNotificationsBL generateNotificationsBL) {
		this.generateNotificationsBL = generateNotificationsBL;
	}

	public SaveWorkflowBL getSaveWorkflowBL() {
		return saveWorkflowBL;
	}

	@Autowired
	public void setSaveWorkflowBL(SaveWorkflowBL saveWorkflowBL) {
		this.saveWorkflowBL = saveWorkflowBL;
	} 
}
