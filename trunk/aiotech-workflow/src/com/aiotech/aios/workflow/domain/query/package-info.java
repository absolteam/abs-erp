@org.hibernate.annotations.NamedQueries({

		/*
		 * @author: Ahsan M.
		 * Purpose: 
		 */
		@org.hibernate.annotations.NamedQuery(name = "getDataEntryWorkFlowsOfProcess", query = "SELECT w FROM Workflow w "
				+ " WHERE w.moduleProcess = ? AND w.workflowTitle NOT LIKE '%delete%'"),

		/*
		 * @author: Ahsan M.
		 * Purpose: 
		 */
		@org.hibernate.annotations.NamedQuery(name = "getMessageByWorkflowDetailProcess", query = "SELECT m FROM Message m "
				+ " WHERE m.workflowDetailProcess = ? "),

		/*
		 * @author: Ahsan M.
		 * Purpose: 
		 */
		@org.hibernate.annotations.NamedQuery(name = "getDetailProcessForWorkflow", query = "SELECT wdp FROM WorkflowDetailProcess wdp "
				+ " WHERE wdp.workflowId = ? ORDER BY wdp.createdDate DESC "),

		/*
		 * @author: Ahsan M.
		 * Purpose: Returns all the current messages for specified record against given module process and status
		 */
		@org.hibernate.annotations.NamedQuery(name = "getMessagesForRecordBasedOnModuleProcess", query = "SELECT m FROM Message m "
				+ " JOIN m.workflowDetailProcess wdp "
				+ " JOIN wdp.workflowDetail wd "
				+ " WHERE wd.workflow.moduleProcess = ? AND m.recordId = ? "
				+ " AND m.status = ? ORDER BY wdp.workflowDetailProcessId DESC "),

		/*
		 * @author: Ahsan M.
		 * Purpose: Returns the list of operations already executed on target record in descending order
		 */
		@org.hibernate.annotations.NamedQuery(name = "findLastWorkflowDetailProcessForTargetRecord", query = "SELECT wdp FROM WorkflowDetailProcess wdp "
				+ " JOIN wdp.workflowDetail wd "
				+ " WHERE wd.workflow.moduleProcess = ? "
				+ " AND wdp.recordId = ? ORDER BY wdp.createdDate DESC"),

		/*
		 * @author: Ahsan M.
		 * Purpose: Returns the list of operations already executed on target record in descending order
		 */
		@org.hibernate.annotations.NamedQuery(name = "getLastDeleteRequestReadOnlyProcess", query = "SELECT wdp FROM WorkflowDetailProcess wdp "
				+ " JOIN wdp.workflowDetail wd "
				+ " WHERE wd.workflow.moduleProcess = ? "
				+ " AND wdp.recordId = ? AND wdp.operation = ? ORDER BY wdp.createdDate DESC"),

		/*
		 * @author: Ahsan M.
		 * Purpose: Returns the default defined delete process work flow for given module process
		 */
		@org.hibernate.annotations.NamedQuery(name = "getDeleteWorkflowForModuleProcess", query = "SELECT w FROM Workflow w "
				+ " JOIN w.workflowDetails wd "
				+ " WHERE w.moduleProcess = ? AND wd.operation = ? "),

		/*
		 * @author: Ahsan M.
		 * Purpose: This returns the last Approved or Published (depends on operation code) 
		 * 			information for specified record of specified Entity
		 */
		@org.hibernate.annotations.NamedQuery(name = "getEntityBasedWorkflowDetailProcessAgainstRecord", query = "SELECT wdp FROM WorkflowDetailProcess wdp "
				+ " WHERE wdp.workflowId IN "
				+ "(SELECT w.workflowId FROM Workflow w WHERE w.moduleProcess.processId = ?)"
				+ " AND wdp.recordId = ? AND wdp.operation = ? "
				+ " ORDER BY wdp.createdDate desc"),

		/*	
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns all active workflow details of a role
			 
			@org.hibernate.annotations.NamedQuery(name = "getWorkFlowDetails", query = "SELECT wd FROM WorkflowDetail wd "
					+ " LEFT JOIN FETCH wd.workflow w "
					+ " LEFT JOIN FETCH wd.notification n "
					+ " LEFT JOIN FETCH w.moduleProcess mp "
					//+ " LEFT JOIN FETCH wd.screen s "
					+ " LEFT JOIN FETCH mp.module  "
					//+ " LEFT JOIN FETCH wd.userRoleCombinations urc  "
					+ " WHERE w.isActive=1 and wd.role=? AND w.implementationId=? "),
		 */
		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns all active workflow details of a person
		 */
		/*	@org.hibernate.annotations.NamedQuery(name = "getWorkFlowDetialListOfPerson", query = "SELECT wd FROM WorkflowDetail wd "
					+ " LEFT JOIN FETCH wd.workflow w "
					+ " LEFT JOIN FETCH wd.notification n "
					+ " LEFT JOIN FETCH w.moduleProcess mp "
					//+ " LEFT JOIN FETCH wd.screen s "
					+ " LEFT JOIN FETCH mp.module  "
					//+ " LEFT JOIN FETCH wd.userRoleCombinations urc  "
					+ " WHERE w.isActive=1 and wd.personId=? AND w.implementationId=? "),
		 */
		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns list of all active worklow details
		 
		@org.hibernate.annotations.NamedQuery(name = "getAllWorkFlowDetails", query = "SELECT wd FROM WorkflowDetail wd "
				+ " LEFT JOIN FETCH wd.workflow w "
				+ " LEFT JOIN FETCH wd.notification n "
				+ " LEFT JOIN FETCH w.moduleProcess mp "
				//+ " LEFT JOIN FETCH wd.screen s "
				+ " LEFT JOIN FETCH mp.module  "
				//+ " LEFT JOIN FETCH wd.userRoleCombinations urc  "
				+ " WHERE w.isActive=1 AND w.implementationId=?"),*/
		/*
		
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns list of all active worklow details of a particular process
		
		 @org.hibernate.annotations.NamedQuery(name = "getWorkFlowDetailsOfProcess", query = "SELECT wd FROM WorkflowDetail wd "
		 + " LEFT JOIN FETCH wd.workflow w "
		 + " LEFT JOIN FETCH wd.notification n "
		 + " LEFT JOIN FETCH w.moduleProcess mp "
		 //+ " LEFT JOIN FETCH wd.screen s "
		 + " LEFT JOIN FETCH mp.module  "
		 //+ " LEFT JOIN FETCH wd.userRoleCombinations urc  "
		 + " WHERE w.isActive=1 and wd.operationOrder > 0 and mp.processId=? AND w.implementationId=? "),
		 */
		@org.hibernate.annotations.NamedQuery(name = "getWorkFlowOfEntryUser", query = "SELECT Distinct(w) FROM Workflow w "
				+ " LEFT JOIN FETCH w.workflowDetails wd "
				//+ " LEFT JOIN FETCH w.moduleProcess mp "
				+ " WHERE w.isActive=1 and wd.operationOrder=1  and wd.role=? and w.moduleProcess=? "),

		@org.hibernate.annotations.NamedQuery(name = "getWorkFlowOfEntryUserByPersonId", query = "SELECT Distinct(w) FROM Workflow w "
				+ " LEFT JOIN FETCH w.workflowDetails wd "
				//+ " LEFT JOIN FETCH w.moduleProcess mp "
				+ " WHERE w.isActive=1 and wd.operationOrder=1 and wd.personId=? and w.moduleProcess=? "),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns list of all active worklows of a particular process
		 */
		@org.hibernate.annotations.NamedQuery(name = "getWorkFlowsOfProcess", query = "SELECT w FROM Workflow w "
				+ " JOIN FETCH w.workflowDetails wd "
				+ " JOIN FETCH w.moduleProcess mp "
				+ " WHERE w.isActive=1 and wd.operationOrder > 0 and mp.processId=? AND w.implementationId=? "),

		/*
		 * @author: Mohamed Rabeek
		 * Purpose: This query returns list of all active worklows
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllActiveWorkflows", query = "SELECT Distinct(w) FROM Workflow w "
				+ " LEFT JOIN FETCH w.workflowDetails wd "
				+ " LEFT JOIN FETCH w.moduleProcess mp "
				+ " WHERE w.isActive=1 AND (w.workflow is Null) AND w.implementationId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getHiddenScreens", query = "SELECT s FROM Screen s "
				+ " WHERE s.isHidden=1"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch the module process records against given process title along with the child's record (active workflow details) populated
		 */
		@org.hibernate.annotations.NamedQuery(name = "getModuleProcessByTitle", query = "SELECT m FROM ModuleProcess m "
				+ " LEFT JOIN FETCH m.workflows w "
				+ " LEFT JOIN FETCH w.workflowDetails wd"
				+ " WHERE m.processTitle=? and w.isActive=1"),

		@org.hibernate.annotations.NamedQuery(name = "getModuleProcessByDisplayName", query = "SELECT m FROM ModuleProcess m "
				+ " WHERE m.processDisplayName = ?"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch the module process records against given process title and operation along with the child's record (active workflow details) populated
		 */
		@org.hibernate.annotations.NamedQuery(name = "getModuleProcessByTitleAndOperation", query = "SELECT m FROM ModuleProcess m "
				+ " LEFT JOIN FETCH m.workflows w "
				+ " LEFT JOIN FETCH w.workflowDetails wd"
				+ " WHERE m.processTitle=? and wd.operation=? and w.isActive=1 ORDER BY w.workflowId"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch all records of a workflow detail
		 */
		@org.hibernate.annotations.NamedQuery(name = "getInProcessRecords", query = "SELECT w FROM WorkflowDetailProcess w "
				+ " WHERE w.workflowDetail.workflowDetailId=? and w.operation=?"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query takes current workflow id and current operation order and returns the list of next workflows in ascending order
		 */
		@org.hibernate.annotations.NamedQuery(name = "findNextWorkflow", query = "SELECT w FROM WorkflowDetail w "
				+ " WHERE w.workflow.workflowId=? and w.operationOrder > ? order by w.operationOrder "),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query takes current workflow id and current operation order and returns the list of next workflows in descending order
		 */
		@org.hibernate.annotations.NamedQuery(name = "findPreviousWorkflow", query = "SELECT w FROM WorkflowDetail w "
				+ " WHERE w.workflow.workflowId=? and w.operationOrder < ? order by w.operationOrder DESC "),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns workflow detail process against workflow detail id and record id (workflow detail process of particular record of a process)
		 */
		@org.hibernate.annotations.NamedQuery(name = "getWorkflowProcess", query = "SELECT w FROM WorkflowDetailProcess w "
				+ " WHERE w.workflowDetail.workflowDetailId=? and w.recordId=? order by w.createdDate DESC"),
		/*
		
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns the reverse flows given workflow detail id(parent id)
		
		 @org.hibernate.annotations.NamedQuery(name = "getChildWorkflowDetail", query = "SELECT w FROM WorkflowDetail w "
		 + " WHERE w.parentId=? and w.operation=?"),*/

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: 
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllProcessOfRecord", query = "SELECT w FROM WorkflowDetailProcess w "
				+ " WHERE w.workflowId=? AND w.recordId=? ORDER BY w.createdDate DESC"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns all workflows of a process
		 */
		@org.hibernate.annotations.NamedQuery(name = "getWorkflowsOfProcess", query = "SELECT Distinct(w) FROM Workflow w "
				+ " LEFT JOIN FETCH w.workflowDetails wd "
				+ " LEFT JOIN FETCH w.moduleProcess mp "
				+ " WHERE w.moduleProcess=? and  w.isActive=1 "
				+ "  AND w.workflow IS NULL AND  (wd.operation IS NULL OR wd.operation not in(10,11,12,13)) AND (w.implementationId IS NULL OR w.implementationId=?)"),

		/*
		 * @author: Mohamed Rafiq
		 * Purpose: This query returns list of roles that have been assigned the given implementation
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllRolesAgainstImplementation", query = "SELECT r FROM Role r "
				+ " WHERE r.implementation=? "),
		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query returns list of users that have been assigned the given role
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllUsersAgainstRole", query = "SELECT Distinct(u) FROM UserRole u "
				+ " LEFT JOIN FETCH u.role role "
				+ " LEFT JOIN FETCH u.user usr " + " WHERE u.role=? "),

		/*
		 * @author: Mohamed rabeek
		 * Purpose: This query returns list of users that have been assigned the implementation
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllUsersByImplementation", query = "SELECT u FROM User u "
				+ " WHERE u.implementationId=? "),
		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch all message of given operation type and which belongs to given person
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllMessageAgainstPerson", query = "SELECT m FROM Message m "
				+ " LEFT JOIN FETCH m.workflowDetailProcess wdp "
				+ " LEFT JOIN FETCH wdp.workflowDetail "
				+ " WHERE m.personId=? and m.status=0 and wdp.operation IN (?)"),

		/*
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch message of given record if of workflow
		 */
		@org.hibernate.annotations.NamedQuery(name = "getMessageAgainstRecordOfWorkflow", query = "SELECT m FROM Message m "
				+ " WHERE m.workflowDetailProcess.workflowDetailProcessId=? AND m.recordId=? and isAlert=?"),

		/**
		 * @author Shoaib Ahmad Gondal 21-Sep-12
		 */
		@org.hibernate.annotations.NamedQuery(name = "getCommentByRecordId", query = "SELECT c FROM Comment c "
				+ "JOIN FETCH c.workflowDetailProcess w "
				+ "WHERE c.recordId = ? AND w.workflowDetailProcessId = ?"),

		/*	
		 * @author: Usman Anwar
		 * Purpose: This query returns list of notifications that have isDefault = true 
			 
			@org.hibernate.annotations.NamedQuery(name = "getAllNotifications", query = "SELECT n FROM Notification n "
					+ " WHERE n.isDefault != 1"),*/

		/**
		 * Query returns screenId of the provided screenName
		 * 
		 * @author Shoaib Ahmad Gondal @ 29-Nov-12
		 */
		@org.hibernate.annotations.NamedQuery(name = "getScreenIdByScreenName", query = "SELECT s.screenId FROM Screen s "
				+ "WHERE s.screenName = ?"),

		/**
		 * Returns screenIds of all four Treaty types
		 * 
		 * @author Shoaib Ahmad Gondal @ 12-Dec-12
		 */
		@org.hibernate.annotations.NamedQuery(name = "getScreenIdsOfTreatiesTypes", query = "SELECT s.screenId FROM Screen s "
				+ "WHERE s.screenName='Agreement' OR s.screenName='Memorandum' OR s.screenName='Protocol' "
				+ "OR s.screenName='Declaration'"),

		/**
		 * Returns list of persons belonging to given role except the given person Ids
		 * 
		 * @author Hasham
		 * 
		 * @ 10-Jan-13
		 */
		@org.hibernate.annotations.NamedQuery(name = "getPersonsAgainstRole", query = "SELECT u FROM User u "
				+ " WHERE u.userRoles=? and u.personId not in (?)"),

		/**
		 * Returns list of all module process sorted by sort order
		 * 
		 * @author Hasham
		 * 
		 * @ 28-Jan-13
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllModuleProcesses", query = "SELECT mp FROM ModuleProcess mp "
				+ " ORDER BY mp.sortOrder"),

		/**
		 * Returns Entry Workflow Detail of given workflow id
		 * 
		 * @author Hasham
		 * 
		 * @ 31-Jan-13
		 */
		@org.hibernate.annotations.NamedQuery(name = "getWorkflowDetailEntryByWorkflowId", query = "SELECT wd FROM WorkflowDetail wd "
				+ " WHERE wd.operation = 3 "
				+ " AND wd.workflow.workflowId = ? "),

		/**
		 * Returns messages against message id 
		 * 
		 * @author Usman Anwar
		 * 
		 * @ 31-Jan-13
		 */
		@org.hibernate.annotations.NamedQuery(name = "getMessageWithWorkflowProcessDetailsByMsgId", query = "SELECT Distinct(m) FROM Message m "
				+ "JOIN FETCH m.workflowDetailProcess p "
				+ "JOIN FETCH p.workflowDetail d "
				+ "JOIN FETCH d.operationMaster om "
				+ "LEFT JOIN FETCH p.screen screeId "
				+ "JOIN FETCH d.workflow w "
				+ "JOIN FETCH w.moduleProcess mp "
				+ "JOIN FETCH mp.module " + "WHERE m.messageId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMessageIfAnyRejectedMessage", query = "SELECT Distinct(m) FROM Message m "
				+ "JOIN FETCH m.workflowDetailProcess p "
				+ "JOIN FETCH p.workflowDetail d "
				+ "JOIN FETCH d.operationMaster om "
				+ "LEFT JOIN FETCH p.screen screeId "
				+ "JOIN FETCH d.workflow w "
				+ "JOIN FETCH w.moduleProcess mp "
				+ "JOIN FETCH mp.module "
				+ "WHERE w.workflowId = ? and m.recordId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getMessageInfoByMessageId", query = "SELECT Distinct(m) FROM Message m "
				+ "JOIN FETCH m.workflowDetailProcess p "
				+ "JOIN FETCH p.workflowDetail d "
				+ "JOIN FETCH d.operationMaster om "
				+ "LEFT JOIN FETCH p.screen screeId "
				+ "JOIN FETCH d.workflow w "
				+ "JOIN FETCH w.moduleProcess mp "
				+ "JOIN FETCH mp.module " + "WHERE m.messageId=?"),

		/**
		 * @author: Khawaja M. Hasham
		 * Purpose: This query fetch all messages of given record and workflow detail which have not been approved yet
		 */
		@org.hibernate.annotations.NamedQuery(name = "getUnApprovedMessageAgainstWorkflowDetailProcess", query = "SELECT m FROM Message m "
				+ " WHERE m.workflowDetailProcess.workflowDetail.workflowDetailId=? AND m.recordId=? AND status=0"),

		/**
		 * @author: Mohamed Rabeek
		 * Purpose: This query fetch all messages of given record and workflow which have not been approved yet
		 */
		@org.hibernate.annotations.NamedQuery(name = "getUnApprovedMessageAgainstWorkflow", query = "SELECT m FROM Message m "
				+ " WHERE m.workflowDetailProcess.workflowId=? AND m.recordId=? AND status=0"),
		/**
		 * @author Shoaib Ahmad Gondal
		 */
		@org.hibernate.annotations.NamedQuery(name = "getAllUsersOfRole", query = "SELECT u FROM User u "
				+ "JOIN FETCH u.userRoles ur "
				+ "JOIN FETCH ur.role r "
				+ "WHERE r.roleId = :roleId AND u.personId NOT IN (:personIds) AND u.implementationId = :implementationId "),

		@org.hibernate.annotations.NamedQuery(name = "getRoleByName", query = "SELECT r FROM Role r "
				+ "WHERE r.roleName = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getMenuByScreenId", query = "SELECT Distinct(m) FROM Menu m "
				+ " LEFT JOIN FETCH m.menu menus "
				+ " LEFT JOIN FETCH m.screen scr "
				+ "WHERE scr.screenId = ? "
				+ " AND (m.isReport IS NULL OR m.isReport = false)"),

		@org.hibernate.annotations.NamedQuery(name = "getMenuRightByMenuIdandRoleId", query = "SELECT m FROM MenuRight m "
				+ "WHERE m.menu.menuItemId = ? and m.role.roleId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkflowWithModuleProcessById", query = "SELECT w FROM Workflow w "
				+ "JOIN FETCH w.moduleProcess WHERE w.workflowId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkflowDetailsByWorkflowId", query = "SELECT Distinct(wd) FROM WorkflowDetail wd "
				+ " JOIN FETCH wd.role "
				+ " LEFT JOIN FETCH wd.operationMaster opmaster"
				+ " LEFT JOIN FETCH wd.workflowDetail parentWorkflowDetail"
				+ " LEFT JOIN FETCH wd.screenByScreenId screen"
				+ " LEFT JOIN FETCH wd.screenByRejectScreen rejectscreen"
				+ " WHERE wd.workflow.workflowId = ? AND wd.isActive = 1 AND wd.operationOrder != 0"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOperations", query = "SELECT Distinct(op) FROM OperationMaster op "
				+ " LEFT JOIN FETCH op.moduleProcess mp"
				+ " LEFT JOIN FETCH op.workflowDetails wd"
				+ " LEFT JOIN FETCH op.workflowDetailProcesses wdp"
				+ " WHERE mp.processId=? and op.implementationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOperationsWithoutProcess", query = "SELECT Distinct(op) FROM OperationMaster op "
				+ " LEFT JOIN FETCH op.moduleProcess mp"
				+ " LEFT JOIN FETCH op.workflowDetails wd"
				+ " LEFT JOIN FETCH op.workflowDetailProcesses wdp"
				+ " WHERE op.implementationId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getWorkflowByWorkflowId", query = "SELECT Distinct(w) FROM Workflow w "
				+ " LEFT JOIN FETCH w.moduleProcess mp"
				+ " LEFT JOIN FETCH w.workflowDetails wd"
				+ " LEFT JOIN FETCH wd.workflowDetailProcesses wdp"
				+ " LEFT JOIN FETCH w.workflow wdelete"
				+ " LEFT JOIN FETCH wd.operationMaster opmaster"
				+ " LEFT JOIN FETCH wd.workflowDetail parentWorkflowDetail"
				+ " LEFT JOIN FETCH wd.screenByScreenId screen"
				+ " LEFT JOIN FETCH wd.screenByRejectScreen rejectscreen"
				+ " LEFT JOIN FETCH wd.role role" + " WHERE w.workflowId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllWorkFlowDetailsForEscalation", query = "SELECT Distinct(wd) FROM WorkflowDetail wd "
				+ " LEFT JOIN FETCH wd.workflow w "
				+ " LEFT JOIN FETCH w.moduleProcess mp "
				+ " LEFT JOIN FETCH mp.module  "
				+ " LEFT JOIN FETCH wd.workflowDetailProcesses wdp "
				+ " LEFT JOIN FETCH wdp.workflowDetail wdTemp "
				+ " LEFT JOIN FETCH wdp.messages msg "
				+ " WHERE w.isActive=1 AND msg.status=0 AND wd.isEscalation=1 AND w.implementationId=?"), })
package com.aiotech.aios.workflow.domain.query;