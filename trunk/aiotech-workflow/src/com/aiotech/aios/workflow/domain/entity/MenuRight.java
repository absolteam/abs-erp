package com.aiotech.aios.workflow.domain.entity;

// Generated 14 Dec, 2014 4:32:18 PM by Hibernate Tools 3.4.0.CR1

/**
 * MenuRight generated by hbm2java
 */
public class MenuRight implements java.io.Serializable {

	private Long menuRightId;
	private Role role;
	private Menu menu;
	private Boolean isActive;

	public MenuRight() {
	}

	public MenuRight(Role role, Menu menu) {
		this.role = role;
		this.menu = menu;
	}

	public MenuRight(Role role, Menu menu, Boolean isActive) {
		this.role = role;
		this.menu = menu;
		this.isActive = isActive;
	}

	public Long getMenuRightId() {
		return this.menuRightId;
	}

	public void setMenuRightId(Long menuRightId) {
		this.menuRightId = menuRightId;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
