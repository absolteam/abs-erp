package com.aiotech.aios.workflow.domain.vo;

import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;

public class WorkflowDetailVO extends WorkflowDetail implements
		Comparable<WorkflowDetailVO> {
	private long recordId;
	private String recordName;
	private Object object;
	private int processFlag;
	private String recordNumber;
	private WorkflowDetailProcess workflowDetailProcess;
	private long notifyToPerson;
	private String formatednotificationTime;
	private Message message;
	private String countryName;
	private Long screenId;
	private Long parentId;
	private String personName;
	private String operationGroupName;
	private String screenName;
	private String rejectScreenName;
	private String escalationInformation;
	private String reminderInformation;
	private String quantumInformation;
	private String operationTitle;
	private Long rejectScreenId;
	private String rejectScreenPath;
	private String screenPath;
	
	private String approveButtonName;
	private String rejectButtonName;
	private Byte processType;
	private WorkflowDetail nextWorkflowDetail;
	private Long rejectAlertId;
	private boolean deleteFlag;
	private boolean editFlag;
	private Long messageId;
	private boolean isInform;
	
	public void convertToVO(WorkflowDetail workflowDetail) {
		this.setAccessRightType(workflowDetail.getAccessRightType());
		this.setEscalateTo(workflowDetail.getEscalateTo());
		this.setIsActive(workflowDetail.getIsActive());
		this.setIsDays(workflowDetail.getIsDays());
		this.setIsEscalation(workflowDetail.getIsEscalation());
		this.setIsReminder(workflowDetail.getIsReminder());
		this.setIsReminderDays(workflowDetail.getIsReminderDays());
		this.setOperation(workflowDetail.getOperation());
		this.setOperationMaster(workflowDetail.getOperationMaster());
		this.setOperationOrder(workflowDetail.getOperationOrder());
		this.setPersonId(workflowDetail.getPersonId());
		this.setQuantumCondition(workflowDetail.getQuantumCondition());
		this.setQuantumField(workflowDetail.getQuantumField());
		this.setQuantumScreen(workflowDetail.getQuantumField());
		this.setQuantumValue(workflowDetail.getQuantumValue());
		this.setRemindeBefore(workflowDetail.getRemindeBefore());
		this.setRole(workflowDetail.getRole());
		this.setScreenByScreenId(workflowDetail.getScreenByScreenId());
		this.setScreenByRejectScreen(workflowDetail.getScreenByRejectScreen());
		this.setValidity(workflowDetail.getValidity());
		this.setWorkflow(workflowDetail.getWorkflow());
		this.setWorkflowDetail(workflowDetail.getWorkflowDetail());
		this.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
		this.setWorkflowDetailProcesses(workflowDetail.getWorkflowDetailProcesses());
		this.setWorkflowDetails(workflowDetail.getWorkflowDetails());
	
	}

	public WorkflowDetail convertoToWorkflowDetail() {
		WorkflowDetail workflowDetail = new WorkflowDetail();
		workflowDetail.setAccessRightType(this.getAccessRightType());
		workflowDetail.setEscalateTo(this.getEscalateTo());
		workflowDetail.setIsActive(this.getIsActive());
		workflowDetail.setIsDays(this.getIsDays());
		workflowDetail.setIsEscalation(this.getIsEscalation());
		workflowDetail.setIsReminder(this.getIsReminder());
		workflowDetail.setIsReminderDays(this.getIsReminderDays());
		workflowDetail.setOperation(this.getOperation());
		workflowDetail.setOperationMaster(this.getOperationMaster());
		workflowDetail.setOperationOrder(this.getOperationOrder());
		workflowDetail.setPersonId(this.getPersonId());
		workflowDetail.setQuantumCondition(this.getQuantumCondition());
		workflowDetail.setQuantumField(this.getQuantumField());
		workflowDetail.setQuantumScreen(this.getQuantumField());
		workflowDetail.setQuantumValue(this.getQuantumValue());
		workflowDetail.setRemindeBefore(this.getRemindeBefore());
		workflowDetail.setRole(this.getRole());
		workflowDetail.setScreenByScreenId(this.getScreenByScreenId());
		workflowDetail.setScreenByRejectScreen(this.getScreenByRejectScreen());
		workflowDetail.setValidity(this.getValidity());
		workflowDetail.setWorkflow(this.getWorkflow());
		workflowDetail.setWorkflowDetail(this.getWorkflowDetail());
		workflowDetail.setWorkflowDetailId(this.getWorkflowDetailId());
		workflowDetail.setWorkflowDetailProcesses(this.getWorkflowDetailProcesses());
		workflowDetail.setWorkflowDetails(this.getWorkflowDetails());
		
		return workflowDetail;
	}

	@Override
	public int compareTo(WorkflowDetailVO workflowDetailVO) {
		// TODO Auto-generated method stub
		return this.getOperationOrder() - workflowDetailVO.getOperationOrder();
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public int getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(int processFlag) {
		this.processFlag = processFlag;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public WorkflowDetailProcess getWorkflowDetailProcess() {
		return workflowDetailProcess;
	}

	public void setWorkflowDetailProcess(
			WorkflowDetailProcess workflowDetailProcess) {
		this.workflowDetailProcess = workflowDetailProcess;
	}

	public long getNotifyToPerson() {
		return notifyToPerson;
	}

	public void setNotifyToPerson(long notifyToPerson) {
		this.notifyToPerson = notifyToPerson;
	}

	public String getFormatednotificationTime() {
		return formatednotificationTime;
	}

	public void setFormatednotificationTime(String formatednotificationTime) {
		this.formatednotificationTime = formatednotificationTime;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Long getScreenId() {
		return screenId;
	}

	public void setScreenId(Long screenId) {
		this.screenId = screenId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getOperationGroupName() {
		return operationGroupName;
	}

	public void setOperationGroupName(String operationGroupName) {
		this.operationGroupName = operationGroupName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getRejectScreenName() {
		return rejectScreenName;
	}

	public void setRejectScreenName(String rejectScreenName) {
		this.rejectScreenName = rejectScreenName;
	}

	public String getEscalationInformation() {
		return escalationInformation;
	}

	public void setEscalationInformation(String escalationInformation) {
		this.escalationInformation = escalationInformation;
	}

	public String getReminderInformation() {
		return reminderInformation;
	}

	public void setReminderInformation(String reminderInformation) {
		this.reminderInformation = reminderInformation;
	}

	public String getQuantumInformation() {
		return quantumInformation;
	}

	public void setQuantumInformation(String quantumInformation) {
		this.quantumInformation = quantumInformation;
	}

	public String getOperationTitle() {
		return operationTitle;
	}

	public void setOperationTitle(String operationTitle) {
		this.operationTitle = operationTitle;
	}

	public Long getRejectScreenId() {
		return rejectScreenId;
	}

	public void setRejectScreenId(Long rejectScreenId) {
		this.rejectScreenId = rejectScreenId;
	}

	public String getRejectScreenPath() {
		return rejectScreenPath;
	}

	public void setRejectScreenPath(String rejectScreenPath) {
		this.rejectScreenPath = rejectScreenPath;
	}

	public String getScreenPath() {
		return screenPath;
	}

	public void setScreenPath(String screenPath) {
		this.screenPath = screenPath;
	}

	public String getApproveButtonName() {
		return approveButtonName;
	}

	public void setApproveButtonName(String approveButtonName) {
		this.approveButtonName = approveButtonName;
	}

	public String getRejectButtonName() {
		return rejectButtonName;
	}

	public void setRejectButtonName(String rejectButtonName) {
		this.rejectButtonName = rejectButtonName;
	}

	public Byte getProcessType() {
		return processType;
	}

	public void setProcessType(Byte processType) {
		this.processType = processType;
	}

	public WorkflowDetail getNextWorkflowDetail() {
		return nextWorkflowDetail;
	}

	public void setNextWorkflowDetail(WorkflowDetail nextWorkflowDetail) {
		this.nextWorkflowDetail = nextWorkflowDetail;
	}

	public Long getRejectAlertId() {
		return rejectAlertId;
	}

	public void setRejectAlertId(Long rejectAlertId) {
		this.rejectAlertId = rejectAlertId;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public boolean isInform() {
		return isInform;
	}

	public void setInform(boolean isInform) {
		this.isInform = isInform;
	}

}
