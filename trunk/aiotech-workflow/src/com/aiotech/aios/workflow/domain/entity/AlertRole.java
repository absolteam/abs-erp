package com.aiotech.aios.workflow.domain.entity;

// Generated 14 Dec, 2014 4:32:18 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

/**
 * AlertRole generated by hbm2java
 */
public class AlertRole implements java.io.Serializable {

	private Long alertRoleId;
	private Role role;
	private ModuleProcess moduleProcess;
	private Long personId;
	private Date createdDate;
	private Long implementationId;

	public AlertRole() {
	}

	public AlertRole(Long implementationId) {
		this.implementationId = implementationId;
	}

	public AlertRole(Role role, ModuleProcess moduleProcess, Long personId,
			Date createdDate, Long implementationId) {
		this.role = role;
		this.moduleProcess = moduleProcess;
		this.personId = personId;
		this.createdDate = createdDate;
		this.implementationId = implementationId;
	}

	public Long getAlertRoleId() {
		return this.alertRoleId;
	}

	public void setAlertRoleId(Long alertRoleId) {
		this.alertRoleId = alertRoleId;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public ModuleProcess getModuleProcess() {
		return this.moduleProcess;
	}

	public void setModuleProcess(ModuleProcess moduleProcess) {
		this.moduleProcess = moduleProcess;
	}

	public Long getPersonId() {
		return this.personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getImplementationId() {
		return this.implementationId;
	}

	public void setImplementationId(Long implementationId) {
		this.implementationId = implementationId;
	}

}
