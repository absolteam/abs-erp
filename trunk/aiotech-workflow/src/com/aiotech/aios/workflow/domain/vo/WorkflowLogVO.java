package com.aiotech.aios.workflow.domain.vo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;

public class WorkflowLogVO extends WorkflowDetailProcess{

	private Long countryId;
	private Long[] persons;
	private Byte operation;
	private Long processId;
	private Date fromDate;
	private Date toDate;
	private String currentLanguage;
	private Map<Byte, String> workflowConstants = new HashMap<Byte, String>();

	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public Long[] getPersons() {
		return persons;
	}
	public void setPersons(Long[] persons) {
		this.persons = persons;
	}
	public Byte getOperation() {
		return operation;
	}
	public void setOperation(Byte operation) {
		this.operation = operation;
	}
	public Long getProcessId() {
		return processId;
	}
	public void setProcessId(Long processId) {
		this.processId = processId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCurrentLanguage() {
		return currentLanguage;
	}
	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}
	public Map<Byte, String> getWorkflowConstants() {
		return workflowConstants;
	}
	public void setWorkflowConstants(Map<Byte, String> workflowConstants) {
		this.workflowConstants = workflowConstants;
	}
}
