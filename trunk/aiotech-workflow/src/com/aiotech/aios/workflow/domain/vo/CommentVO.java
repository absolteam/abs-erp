package com.aiotech.aios.workflow.domain.vo;

import com.aiotech.aios.workflow.domain.entity.Comment;

public class CommentVO extends Comment{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
