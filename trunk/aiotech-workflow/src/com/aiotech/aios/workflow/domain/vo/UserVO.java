package com.aiotech.aios.workflow.domain.vo;

import com.aiotech.aios.workflow.domain.entity.User;

public class UserVO extends User {

	private String displayUsername;

	private String personName;

	public String getDisplayUsername() {
		return displayUsername;
	}

	public void setDisplayUsername(String displayUsername) {
		this.displayUsername = displayUsername;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

}
