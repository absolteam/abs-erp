package com.aiotech.aios.workflow.domain.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.util.WorkflowConstants;

public class WorkflowVO extends Workflow {

	private String moduleProcessName;
	private String processMethod;
	List<WorkflowDetailVO> workflowDetailVOs = new ArrayList<WorkflowDetailVO>();
	
	private int returnStatus;//
	private int processType;//Add, edit or delete
	private String returnStatusName;

	public WorkflowVO() {

	}

	public WorkflowVO(Workflow workflow) {
		this.setCreatedBy(workflow.getCreatedBy());
		this.setCreationDate(workflow.getCreationDate());
		this.setImplementationId(workflow.getImplementationId());
		this.setIsActive(workflow.getIsActive());
		this.setModuleProcess(workflow.getModuleProcess());
		this.setWorkflowDetails(workflow.getWorkflowDetails());
		this.setWorkflowId(workflow.getWorkflowId());
		this.setWorkflowTitle(workflow.getWorkflowTitle());

		for (WorkflowDetail workflowDetail : workflow.getWorkflowDetails()) {

			if (workflowDetail.getOperationOrder() > 0) {
				WorkflowDetailVO vo = new WorkflowDetailVO();
				vo.setWorkflow(workflowDetail.getWorkflow());
				/* vo.setNotification(workflowDetail.getNotification()); */
				vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
				vo.setOperation(workflowDetail.getOperation());

				vo.setAccessRightType(workflowDetail.getAccessRightType());
				vo.setIsActive(workflowDetail.getIsActive());
				vo.setIsDays(workflowDetail.getIsDays());

				vo.setPersonId(workflowDetail.getPersonId());
				vo.setRole(workflowDetail.getRole());
				vo.setValidity(workflowDetail.getValidity());
				vo.setWorkflow(workflowDetail.getWorkflow());
				vo.setOperationOrder(workflowDetail.getOperationOrder());
				vo.setScreenName(workflowDetail.getScreenByScreenId() != null ? workflowDetail
						.getScreenByScreenId().getScreenName() : "");
				vo.setRejectScreenName(workflowDetail.getScreenByRejectScreen() != null ? workflowDetail
						.getScreenByRejectScreen().getScreenName() : "");
				// Person notifyPerson = new Person();
				vo.setOperationGroupName(workflowDetail.getOperationMaster() != null ? WorkflowConstants.OperationGroup
						.get(workflowDetail.getOperationMaster()
								.getOperationGroup()).name() : "");
				vo.setOperationMaster(workflowDetail.getOperationMaster());
				vo.setScreenByScreenId(workflowDetail.getScreenByScreenId());
				vo.setScreenByRejectScreen(workflowDetail
						.getScreenByRejectScreen());
				vo.setOperationTitle(workflowDetail.getOperationMaster() != null ? workflowDetail
						.getOperationMaster().getTitle() : "");

				String escalationString = "";
				if (workflowDetail.getIsEscalation() != null
						&& workflowDetail.getIsEscalation()) {
					escalationString = "Validity :"
							+ workflowDetail.getValidity()
							+ " "
							+ (workflowDetail.getIsDays() != null
									&& workflowDetail.getIsDays() ? "Days"
									: "Hours");
					vo.setEscalationInformation(escalationString);
				} else {
					vo.setEscalationInformation("-NA-");
				}

				String reminderString = "";
				if (workflowDetail.getIsReminder() != null
						&& workflowDetail.getIsReminder()) {
					reminderString = "Remind Before :"
							+ workflowDetail.getRemindeBefore()
							+ " "
							+ (workflowDetail.getIsReminderDays() != null
									&& workflowDetail.getIsReminderDays() ? "Days"
									: "Hours");
					vo.setReminderInformation(reminderString);
				} else {
					vo.setReminderInformation("-NA-");
				}

				String quantumString = "";
				if (workflowDetail.getQuantumValue() != null
						&& !workflowDetail.getQuantumValue().equals("")) {
					quantumString = workflowDetail.getQuantumValue() + " (value) "
							+ workflowDetail.getQuantumCondition() + " "
							+ workflowDetail.getQuantumField();

					vo.setQuantumInformation(quantumString);
				} else {
					vo.setQuantumInformation("-NA-");
				}

				vo.setNotifyToPerson(0);
				this.workflowDetailVOs.add(vo);
			}
		}
		Collections.sort(this.workflowDetailVOs);
	}

	public Workflow convertoToWorkflow() {
		Workflow workflow = new Workflow();

		workflow.setCreatedBy(this.getCreatedBy());
		workflow.setCreationDate(this.getCreationDate());
		workflow.setImplementationId(this.getImplementationId());
		workflow.setIsActive(this.getIsActive());
		workflow.setModuleProcess(this.getModuleProcess());
		workflow.setWorkflowDetails(this.getWorkflowDetails());
		workflow.setWorkflowId(this.getWorkflowId());
		workflow.setWorkflowTitle(this.getWorkflowTitle());
		return workflow;
	}

	public List<WorkflowDetailVO> getWorkflowDetailVOs() {
		return workflowDetailVOs;
	}

	public void setWorkflowDetailVOs(List<WorkflowDetailVO> workflowDetailVOs) {
		this.workflowDetailVOs = workflowDetailVOs;
	}

	public String getModuleProcessName() {
		return moduleProcessName;
	}

	public void setModuleProcessName(String moduleProcessName) {
		this.moduleProcessName = moduleProcessName;
	}

	public String getProcessMethod() {
		return processMethod;
	}

	public void setProcessMethod(String processMethod) {
		this.processMethod = processMethod;
	}

	public int getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(int returnStatus) {
		this.returnStatus = returnStatus;
	}

	public int getProcessType() {
		return processType;
	}

	public void setProcessType(int processType) {
		this.processType = processType;
	}

	public String getReturnStatusName() {
		return returnStatusName;
	}

	public void setReturnStatusName(String returnStatusName) {
		this.returnStatusName = returnStatusName;
	}


}
