/**
 * 
 */
package com.aiotech.aios.workflow.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.service.MessageService;
import com.aiotech.aios.workflow.service.WorkflowService;
import com.aiotech.aios.workflow.util.WorkflowConstants;

/**
 * @author Haris
 * 
 */
@Service
@Transactional
public class GetNotificationsAndAlertsBL {

	private WorkflowService workflowService;
	private MessageService messageService;

	/**
	 * @author: Khawaja Hasham
	 * 
	 *          Purpose: This function fetched the notifications of given
	 *          operation types for the logged in user.
	 * 
	 * @param: List<Byte> operations : This parameter represents the operation
	 *         types of notification to be fetch
	 */
	public List<WorkflowDetailVO> getNotificationList(List<Byte> operations,
			Boolean isAlert, User user) {
		List<WorkflowDetailVO> workflowDetailVOs = new ArrayList<WorkflowDetailVO>();
		try {

			List<Message> messages = null;

			// Fetch all message of logged in person, for the given operation
			// types
			if (!isAlert) {
				workflowEscalationCall();
				messages = messageService.getAllMessageAgainstPerson(
						user.getPersonId(), operations);
			} else if (isAlert) {
				messages = messageService.getAllAlertsAgainstPerson(
						user.getPersonId(), operations);
			}

			Collections.sort(messages, new Comparator<Message>() {
				public int compare(Message m1, Message m2) {
					return m2.getMessageId().compareTo(m1.getMessageId());
				}
			});
			// Against each messages fill in the workflow detail VO
			Map<String, String> duplicateMessage = new HashMap<String, String>();
			for (Message message : messages) {
				String duplicateKey=message.getWorkflowDetailProcess()
				.getWorkflowDetailProcessId()
				+ "_"
				+ message.getPersonId()
				+ "_"
				+ message.getRecordId()
				+ "_" + message.getStatus();
				if(duplicateMessage.containsKey(duplicateKey))
					continue;
				else
					duplicateMessage.put(duplicateKey, duplicateKey);
				
				WorkflowDetail workflowDetail = message
						.getWorkflowDetailProcess().getWorkflowDetail();

				Workflow workflow = workflowService
						.getWorkflowWithModuleProcessById(workflowDetail
								.getWorkflow().getWorkflowId());

				// Fetch actual record from process's table
				Object recordObject = workflowService.getRecord(workflow
						.getModuleProcess().getUseCase(), message
						.getWorkflowDetailProcess().getRecordId());

				WorkflowDetailVO vo = new WorkflowDetailVO();
				vo.setWorkflow(workflow);
				// vo.setScreenId(workflowDetail.getScreenId());
				/* vo.setNotification(workflowDetail.getNotification()); */
				vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
				vo.setOperation(workflowDetail.getOperation());
				vo.setOperationOrder(workflowDetail.getOperationOrder());
				// vo.setParentId(workflowDetail.getParentId());
				// vo.setNextOperation(workflowDetail.getNextOperation());
				vo.setObject(recordObject);
				vo.setRecordId(message.getWorkflowDetailProcess().getRecordId());
				vo.setMessage(message);
				vo.setOperationGroupName(workflowDetail.getOperationMaster()
						.getOperationGroup() + "");

				java.text.DateFormat df = new SimpleDateFormat(
						"EEE, d MMM yyyy hh:mm aaa");
				String formatedDate = df.format(message.getCreatedDate());
				vo.setFormatednotificationTime(formatedDate);

				vo.setProcessFlag(message.getWorkflowDetailProcess()
						.getOperation());
				vo.setWorkflowDetailProcess(message.getWorkflowDetailProcess());
				vo.setScreenByScreenId(message.getWorkflowDetailProcess()
						.getScreen());
				/*
				 * Person person = personService.getPerson(workflowDetail
				 * .getNotifyTo()); vo.setNotifyToPerson(person);
				 */
				// vo.setToProcess(workflowDetail.getToProcess());
				workflowDetailVOs.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return workflowDetailVOs;
	}

	/**
	 * This Method fetches records of WorkflowDetail by provided Message object
	 * and returns WorkflowDetailVO object by setting all values
	 * 
	 * @param message
	 *            : Message object
	 * @return: WorkflowDetailVO object
	 * 
	 * @author Shoaib Ahmad Gondal 26-Sep-12
	 */
	private WorkflowDetailVO getWorkFlowDetailVOList(Message message)
			throws Exception {

		WorkflowDetail workflowDetail = new WorkflowDetail();
		WorkflowDetailVO vo = new WorkflowDetailVO();

		workflowDetail = message.getWorkflowDetailProcess().getWorkflowDetail();
		Object recordObject = workflowService.getRecord(workflowDetail
				.getWorkflow().getModuleProcess().getUseCase(), message
				.getWorkflowDetailProcess().getRecordId());

		vo = new WorkflowDetailVO();
		vo.setWorkflow(workflowDetail.getWorkflow());
		// vo.setScreenId(workflowDetail.getScreenId());
		/* vo.setNotification(workflowDetail.getNotification()); */
		vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
		vo.setOperation(workflowDetail.getOperation());
		vo.setOperationOrder(workflowDetail.getOperationOrder());
		// vo.setParentId(workflowDetail.getParentId());
		// vo.setNextOperation(workflowDetail.getNextOperation());
		vo.setObject(recordObject);
		vo.setRecordId(message.getWorkflowDetailProcess().getRecordId());
		vo.setWorkflowDetailProcess(message.getWorkflowDetailProcess());

		vo.setProcessFlag(workflowDetail.getOperation());

		return vo;
	}

	@SuppressWarnings("unchecked")
	public void workflowEscalationCall() {
		try {
			List<WorkflowDetail> workflowDetails = workflowService
					.getAllWorkFlowDetailsForEscalation();
			if (workflowDetails != null && workflowDetails.size() > 0) {
				for (WorkflowDetail workflowDetail : workflowDetails) {
					if (workflowDetail.getWorkflowDetailProcesses() == null
							|| workflowDetail.getWorkflowDetailProcesses()
									.size() == 0)
						continue;

					List<WorkflowDetailProcess> workflowDetailProcesses = new ArrayList<WorkflowDetailProcess>(
							workflowDetail.getWorkflowDetailProcesses());
					for (WorkflowDetailProcess workflowDetailProcess : workflowDetailProcesses) {
						if (workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Confirm
								.getCode()
								|| workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Post
										.getCode()) {
							// Check Escalation has done already or not
							boolean isAleadyEscalated = false;
							for (Message msg : workflowDetailProcess
									.getMessages()) {
								if (msg.getEscaletedFrom() != null
										&& msg.getEscaletedFrom() > 0) {
									isAleadyEscalated = true;
									break;
								}
							}

							if (isAleadyEscalated)
								break;
							WorkflowDetailVO workflowDetailVO = new WorkflowDetailVO();
							Message message = (new ArrayList<Message>(
									workflowDetailProcess.getMessages()))
									.get(0);
							workflowDetailVO.setMessage(message);
							WorkflowDetail workflowDetailTemp = workflowDetailProcess
									.getWorkflowDetail();
							Date currentDate = new Date();
							Calendar validityTime = Calendar.getInstance();
							// Current Date
							currentDate = validityTime.getTime();
							Date date = message.getCreatedDate();
							validityTime = Calendar.getInstance();
							validityTime.setTime(date);
							if (workflowDetailTemp.getIsDays())
								validityTime.add(Calendar.DAY_OF_MONTH,
										workflowDetailTemp.getValidity());
							else
								validityTime.add(Calendar.HOUR,
										workflowDetailTemp.getValidity());
							Calendar newCal = Calendar.getInstance();
							newCal.setTime(currentDate);
							System.out.println("validityTime=="
									+ validityTime.getTime() + " newCal=="
									+ newCal.getTime());
							if (validityTime.before(newCal)) {
								System.out
										.println("date is greater than reference that.");
								messageService
										.generateEscalationReminderMessage(
												workflowDetailProcess,
												workflowDetailProcess
														.getWorkflowDetail()
														.getWorkflow()
														.getModuleProcess()
														.getUseCase(), null,
												workflowDetailVO);
							}

							Calendar validityTimeReminder = Calendar
									.getInstance();
							// Reminder Section
							if (workflowDetailTemp.getIsReminder() != null
									&& workflowDetailTemp.getIsReminder()) {
								currentDate = new Date();
								validityTimeReminder = Calendar.getInstance();
								// Current Date
								currentDate = validityTimeReminder.getTime();
								date = message.getCreatedDate();
								validityTimeReminder = Calendar.getInstance();
								validityTimeReminder.setTime(date);
								if (workflowDetailTemp.getIsDays())
									validityTimeReminder.add(
											Calendar.DAY_OF_MONTH,
											workflowDetailTemp.getValidity());
								else
									validityTimeReminder
											.add(Calendar.HOUR,
													workflowDetailTemp
															.getValidity()
															- workflowDetailTemp
																	.getRemindeBefore());
								newCal = Calendar.getInstance();
								newCal.setTime(currentDate);
								System.out.println("validityTimeReminder=="
										+ validityTimeReminder.getTime()
										+ " newCal==" + newCal.getTime());
								if (validityTimeReminder.before(newCal)
										&& !message.getMessage().contains(">>")) {
									System.out
											.println("date is greater than reference that.");

									message.setMessage(message.getMessage()
											+ " >> Reminder: Notification will be escalated after this period '"
											+ validityTime.getTime() + "'");

									messageService.saveMessage(message);
								}
							}
						}
						break;
					}
					break;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public WorkflowService getWorkflowService() {
		return workflowService;
	}

	@Autowired
	public void setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

}
