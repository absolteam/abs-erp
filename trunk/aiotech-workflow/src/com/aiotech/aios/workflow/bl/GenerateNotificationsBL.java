/**
 * 
 */
package com.aiotech.aios.workflow.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.service.MessageService;
import com.aiotech.aios.workflow.service.UserRoleService;
import com.aiotech.aios.workflow.service.WorkflowService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Muhammad Haris
 * @date 01-04-2013
 * 
 */
@Service
@Transactional
public class GenerateNotificationsBL {

	private WorkflowService workflowService;
	private MessageService messageService;
	private UserRoleService userRoleService;

	/**
	 * This Method records an entry process in message table.
	 * 
	 * @param parentId
	 *            : workflow detail id whose reverse flow is to be find
	 * 
	 * @param useCase
	 *            : name of use case against which data is entered
	 * 
	 * @param recordId
	 *            : primary key of record entered
	 * 
	 * @return
	 * 
	 * @author Khawaja Hasham
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public Integer generateNotificationsAgainstDataEntry(String useCase,
			Long recordId, User user, WorkflowDetailVO workflowDetailVO)
			throws Exception {
		int returnStatus = WorkflowConstants.ReturnStatus.Error.getCode();
		try {
			
			//This is just alert update with rejected case
			if (workflowDetailVO.getRejectAlertId() != null
					&& workflowDetailVO.getRejectAlertId() > 0) {
				Message rejectMessage = messageService
						.getMessageInfoByMessageId(workflowDetailVO
								.getRejectAlertId());
				rejectMessage.setStatus((byte) 1);
				messageService.saveMessage(rejectMessage);
				WorkflowDetailProcess rejectWorkflowDetailProcess = rejectMessage
						.getWorkflowDetailProcess();
				rejectWorkflowDetailProcess.setCreatedBy(user.getPersonId());
				workflowService.saveWorkflowDetailProcess(rejectMessage
						.getWorkflowDetailProcess());
			}
			// fetch process information from module process table
			List<ModuleProcess> moduleProcesses = workflowService
					.getModuleProcessByTitle(useCase);
			ModuleProcess moduleProcess;
			Workflow workflow = null;
			WorkflowDetail workflowDetail;
			WorkflowDetailProcess workflowDetailProcess;
			Byte isApproveStatusForSelf = null;
			Byte isApproveStatusForDirect = null;
			Byte isApproveStatusForForward = null;
			Byte isApproveStatusForPost = null;
			Byte processType = null;
			if ((byte) workflowDetailVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Add
					.getCode()) {
				returnStatus = WorkflowConstants.ReturnStatus.AddApproved
						.getCode();
				isApproveStatusForSelf = (byte) WorkflowConstants.Status.Confirm
						.getCode();
				isApproveStatusForDirect = (byte) WorkflowConstants.Status.Published
						.getCode();
				isApproveStatusForForward = (byte) WorkflowConstants.Status.Saved
						.getCode();
				processType = (byte) WorkflowConstants.ProcessMethod.Add
						.getCode();
			} else if ((byte) workflowDetailVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Modify
					.getCode()) {
				returnStatus = WorkflowConstants.ReturnStatus.ModificationApproved
						.getCode();
				isApproveStatusForSelf = (byte) WorkflowConstants.Status.Confirm
						.getCode();
				isApproveStatusForForward = (byte) WorkflowConstants.Status.Saved
						.getCode();
				isApproveStatusForDirect = (byte) WorkflowConstants.Status.Published
						.getCode();

				processType = (byte) WorkflowConstants.ProcessMethod.Modify
						.getCode();
			} else if ((byte) workflowDetailVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Delete
					.getCode()) {
				returnStatus = WorkflowConstants.ReturnStatus.DeleteApproved
						.getCode();
				isApproveStatusForSelf = (byte) WorkflowConstants.Status.DeleteApproval
						.getCode();
				isApproveStatusForDirect = (byte) WorkflowConstants.Status.Deleted
						.getCode();
				isApproveStatusForForward = (byte) WorkflowConstants.Status.Saved
						.getCode();
				processType = (byte) WorkflowConstants.ProcessMethod.Delete
						.getCode();
			}

			isApproveStatusForPost = (byte) WorkflowConstants.Status.Post
					.getCode();

			// If process is defined against the use case
			if (moduleProcesses.size() > 0) {
				moduleProcess = moduleProcesses.get(0);

				// if flows are defined
				if (moduleProcess.getWorkflows().size() > 0) {
					List<Workflow> workflows;

					// get workflow defined against the person id
					workflows = workflowService
							.getWorkFlowOfEntryUserByPersonId(
									user.getPersonId(), moduleProcess);

					if (workflows.size() == 0) {

						// if logged in user have multiple role then check
						// against
						// which role he have entry right, fetch the entry
						// workflow
						// against that role
						List<UserRole> userRoles = userRoleService
								.getUserRoles(user);

						for (UserRole userRole : userRoles) {
							workflows = workflowService.getWorkFlowOfEntryUser(
									userRole.getRole(), moduleProcess);
							if (workflows.size() > 0) {
								break;
							}
						}
					}
					if (workflows.size() > 0) {
						workflow = workflows.get(0);

						//Update Already Rejected flow if any for the same user  with  recordID and workflowId
						if (workflowDetailVO.getRejectAlertId() == null) {
							workflowDetailVO.setWorkflow(workflow);
							workflowDetailVO.setPersonId(user.getPersonId());
							workflowDetailVO = findRejectedNotificationForSameUser(
									recordId, workflowDetailVO);
							if (workflowDetailVO.getRejectAlertId() != null
									&& workflowDetailVO.getRejectAlertId() > 0) {
								Message rejectMessage = messageService
										.getMessageInfoByMessageId(workflowDetailVO
												.getRejectAlertId());
								rejectMessage.setStatus((byte) 1);
								messageService.saveMessage(rejectMessage);
								WorkflowDetailProcess rejectWorkflowDetailProcess = rejectMessage
										.getWorkflowDetailProcess();
								rejectWorkflowDetailProcess.setCreatedBy(user
										.getPersonId());
								workflowService
										.saveWorkflowDetailProcess(rejectMessage
												.getWorkflowDetailProcess());
							}
						}

						// If any pending reject notification
						List<Message> messages = messageService
								.getMessageIfAnyRejectedMessage(
										workflow.getWorkflowId(), recordId);
						if (messages.size() > 0) {
							for (Message message2 : messages) {
								// set status and save message
								message2.setStatus((byte) 1);
								messageService.saveMessage(message2);
							}
						}

					}else{
						//No Workflow for this user but screen is configured for entry
						//Direct Process
						doDirectApprovalAndDeleteProcess(useCase, recordId,
								isApproveStatusForDirect);
					}

					if ((workflow != null)
							&& (workflow.getWorkflowDetails().size() > 0)) {
						List<WorkflowDetail> workflowDetailList = new ArrayList<WorkflowDetail>(
								workflow.getWorkflowDetails());
						Collections.sort(workflowDetailList,
								new Comparator<WorkflowDetail>() {
									public int compare(WorkflowDetail m1,
											WorkflowDetail m2) {
										return m1.getOperationOrder()
												.compareTo(
														m2.getOperationOrder());
									}
								});
						// get the entry workflow detail
						workflowDetail = workflowDetailList.get(0);

						WorkflowDetail nextLevelWorkflowDetail = null;
						WorkflowDetail nextLevelApprovalWorkflowDetail = null;

						// Find is there any next level approval
						for (WorkflowDetail workflowDeat2 : workflowDetailList) {
							if (workflowDeat2.getOperationOrder() > workflowDetail
									.getOperationOrder()
									&& (byte) workflowDeat2
											.getOperationMaster()
											.getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Approval
											.getCode()) {
								if (workflowService.quantumRuleValidationCheck(
										recordId, workflowDeat2)) {
									nextLevelApprovalWorkflowDetail = new WorkflowDetail();
									nextLevelApprovalWorkflowDetail = (workflowDeat2);
								}
								break;
							}
						}

						// find the next level detail information
						for (WorkflowDetail workflowDeat2 : workflowDetailList) {
							if (workflowDeat2.getOperationOrder() > workflowDetail
									.getOperationOrder()
									&& (byte) workflowDeat2
											.getOperationMaster()
											.getOperationGroup() != (byte) WorkflowConstants.OperationGroup.Approval
											.getCode()) {
								nextLevelWorkflowDetail = new WorkflowDetail();
								nextLevelWorkflowDetail = (workflowDeat2);
								break;
							}
						}

						// If there is any approval
						if (nextLevelApprovalWorkflowDetail != null
								&& !isDirectDeletePossible(useCase, recordId,
										user, processType)) {

							workflowDetailVO
									.setNextWorkflowDetail(nextLevelApprovalWorkflowDetail);

							// Self Notification
							workflowDetailProcess = new WorkflowDetailProcess();
							workflowDetailProcess
									.setWorkflowDetail(workflowDetail);
							workflowDetailProcess
									.setScreen(nextLevelApprovalWorkflowDetail
											.getScreenByScreenId());

							workflowDetailProcess
									.setOperation(isApproveStatusForSelf);
							workflowDetailProcess.setProcessType(processType);
							workflowDetailProcess.setCreatedDate(new Date());
							workflowDetailProcess.setRecordId(recordId);
							workflowDetailProcess.setWorkflowId(workflow
									.getWorkflowId());
							workflowDetailProcess.setCreatedBy(user
									.getPersonId());
							workflowService
									.saveWorkflowDetailProcess(workflowDetailProcess);
							messageService.saveMessageAgainstworkflowProcess(
									workflowDetailProcess, useCase,
									user.getPersonId(), workflowDetailVO);

							// Forward Record
							workflowDetailProcess = new WorkflowDetailProcess();
							workflowDetailProcess
									.setWorkflowDetail(nextLevelApprovalWorkflowDetail);
							workflowDetailProcess.setRecordId(recordId);
							workflowDetailProcess.setCreatedDate(new Date());
							workflowDetailProcess.setCreatedBy(user
									.getPersonId());
							workflowDetailProcess.setWorkflowId(workflow
									.getWorkflowId());
							workflowDetailProcess
									.setScreen(nextLevelApprovalWorkflowDetail
											.getScreenByScreenId());
							workflowDetailProcess.setProcessType(processType);
							workflowDetailProcess
									.setOperation(isApproveStatusForForward);
							workflowService
									.saveWorkflowDetailProcess(workflowDetailProcess);

							messageService.saveMessageAgainstworkflowProcess(
									workflowDetailProcess, useCase, null,
									workflowDetailVO);
							returnStatus = WorkflowConstants.ReturnStatus.Added
									.getCode();
							updateUseCaseIsApproveStatus(useCase, recordId,
									isApproveStatusForForward);
							// if there is no approval
						} else {
							workflowDetailVO
									.setNextWorkflowDetail(workflowDetail);

							// Self Notification
							workflowDetailProcess = new WorkflowDetailProcess();
							workflowDetailProcess
									.setWorkflowDetail(workflowDetail);
							workflowDetailProcess.setScreen(workflowDetail
									.getScreenByScreenId());
							workflowDetailProcess
									.setProcessType(workflowDetailVO
											.getProcessType());
							workflowDetailProcess
									.setOperation(isApproveStatusForDirect);

							workflowDetailProcess.setCreatedDate(new Date());
							workflowDetailProcess.setRecordId(recordId);
							workflowDetailProcess.setWorkflowId(workflowDetail
									.getWorkflow().getWorkflowId());
							workflowDetailProcess.setCreatedBy(user
									.getPersonId());
							workflowService
									.saveWorkflowDetailProcess(workflowDetailProcess);
							messageService.saveMessageAgainstworkflowProcess(
									workflowDetailProcess, workflowDetail
											.getWorkflow().getModuleProcess()
											.getUseCase(), user.getPersonId(),
									workflowDetailVO);
							returnStatus = WorkflowConstants.ReturnStatus.DeleteApproved
									.getCode();
							// no approval but there is another process like
							// Entry/Post
							if (nextLevelWorkflowDetail != null) {
								workflowDetailVO
										.setNextWorkflowDetail(nextLevelWorkflowDetail);

								// Self Notification
								workflowDetailProcess = new WorkflowDetailProcess();
								workflowDetailProcess
										.setWorkflowDetail(workflowDetail);
								workflowDetailProcess
										.setScreen(nextLevelWorkflowDetail
												.getScreenByScreenId());
								workflowDetailProcess
										.setProcessType(processType);
								workflowDetailProcess
										.setOperation(isApproveStatusForSelf);
								workflowDetailProcess
										.setProcessType(workflowDetailVO
												.getProcessType());
								workflowDetailProcess
										.setCreatedDate(new Date());
								workflowDetailProcess.setRecordId(recordId);
								workflowDetailProcess.setWorkflowId(workflow
										.getWorkflowId());
								workflowDetailProcess.setCreatedBy(user
										.getPersonId());
								workflowService
										.saveWorkflowDetailProcess(workflowDetailProcess);
								messageService
										.saveMessageAgainstworkflowProcess(
												workflowDetailProcess, useCase,
												user.getPersonId(),
												workflowDetailVO);

								// Forward Record
								workflowDetailProcess = new WorkflowDetailProcess();
								workflowDetailProcess
										.setWorkflowDetail(nextLevelWorkflowDetail);
								workflowDetailProcess.setRecordId(recordId);
								workflowDetailProcess
										.setCreatedDate(new Date());
								workflowDetailProcess.setCreatedBy(user
										.getPersonId());
								workflowDetailProcess.setWorkflowId(workflow
										.getWorkflowId());
								workflowDetailProcess
										.setScreen(nextLevelWorkflowDetail
												.getScreenByScreenId());
								workflowDetailProcess
										.setProcessType(processType);
								workflowDetailProcess
										.setOperation(isApproveStatusForPost);
								workflowService
										.saveWorkflowDetailProcess(workflowDetailProcess);

								messageService
										.saveMessageAgainstworkflowProcess(
												workflowDetailProcess, useCase,
												null, workflowDetailVO);
								returnStatus = WorkflowConstants.ReturnStatus.Added
										.getCode();
								updateUseCaseIsApproveStatus(useCase, recordId,
										isApproveStatusForPost);
							}

							doDirectApprovalAndDeleteProcess(useCase, recordId,
									isApproveStatusForDirect);
						}
					}
				//No Workflow is configured for this screen
				}else{
					//Direct Process
					doDirectApprovalAndDeleteProcess(useCase, recordId,
							isApproveStatusForDirect);
				}
			}
		} catch (Exception e) {
			returnStatus = WorkflowConstants.ReturnStatus.Error.getCode();
			e.printStackTrace();
		}

		// ******************************************Business
		// Call************************************************

		// find business call need and set operation mode(Add,Edit & Delete)
		WorkflowDetailVO workfowDetailVO = new WorkflowDetailVO();
		boolean businessCallFlag = false;
		if ((int) returnStatus == (int) WorkflowConstants.ReturnStatus.AddApproved
				.getCode()) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(false);
			workfowDetailVO.setDeleteFlag(false);
		} else if ((int) returnStatus == (int) WorkflowConstants.ReturnStatus.ModificationApproved
				.getCode()) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(true);
			workfowDetailVO.setDeleteFlag(false);
		} else if ((int) returnStatus == (int) WorkflowConstants.ReturnStatus.DeleteApproved
				.getCode()) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(false);
			workfowDetailVO.setDeleteFlag(true);
		}
		// Call business method
		if (businessCallFlag) {
			workfowDetailVO.setRecordId(recordId);
			workfowDetailVO.setScreenName(useCase);
			buisnessCall(workfowDetailVO);
		}
		return returnStatus;
	}

	public void buisnessCall(WorkflowDetailVO workfowDetailVO) throws Exception {
		String nameTemp = workfowDetailVO.getScreenName().replace(
				".domain.entity.", ".DOMAIN.ENTITY.");
		String[] name = nameTemp.split(".DOMAIN.ENTITY.");
		// Some times use case name is appended by _javassist, so
		// get
		// proper use case name
		if (name[name.length - 1].contains("_$$_javassist")) {
			name[name.length - 1] = name[name.length - 1].substring(0,
					name[name.length - 1].indexOf("_$$_javassist"));
		}
		Object bLObject = WorkflowUtils.getEntityBL(name[name.length - 1]);

		String methodId = "do" + name[name.length - 1] + "BusinessUpdate";
		Class[] argTypes = new Class[] { Long.class, WorkflowDetailVO.class };
		Method method = bLObject.getClass().getMethod(methodId, argTypes);
		method.invoke(bLObject, workfowDetailVO.getRecordId(), workfowDetailVO);
	}

	public WorkflowDetailVO findRejectedNotificationForSameUser(Long recordId,
			WorkflowDetailVO workflowDetailVO) throws Exception {
		List<Message> messages = messageService.getMessageIfAnyRejectedMessage(
				workflowDetailVO.getWorkflow().getWorkflowId(), recordId);
		if (messages != null && messages.size() > 0) {
			for (Message message : messages) {
				if (message!=null && (byte)message.getStatus()==(byte)0 
						&&  message.getPersonId() == workflowDetailVO.getPersonId()) {
					workflowDetailVO.setRejectAlertId(message.getMessageId());
					break;
				}
			}
		}
		return workflowDetailVO;
	}

	public void updateUseCaseIsApproveStatus(String useCase, Long recordId,
			Byte status) {
		try {
			// Fetch actual record from process's table
			Object recordObject = this.getWorkflowService().getRecord(useCase,
					recordId);
			// Get the use case name
			/*
			 * String[] name = workflowDetailVO.getObject().getClass()
			 * .getName().split("entity.");
			 */
			String nameTemp = recordObject.getClass().getName()
					.replace(".domain.entity.", ".DOMAIN.ENTITY.");
			String[] name = nameTemp.split(".DOMAIN.ENTITY.");
			// Some times use case name is appended by _javassist, so
			// get
			// proper use case name
			if (name[name.length - 1].contains("_$$_javassist")) {
				name[name.length - 1] = name[name.length - 1].substring(0,
						name[name.length - 1].indexOf("_$$_javassist"));
			}
			// getter method of primary key
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = recordObject.getClass().getMethod(methodId, null);
			Object tempRecordidObject = method.invoke(recordObject, null);
			long temprecordId = 0;
			if (tempRecordidObject instanceof Long) {
				temprecordId = ((Long) tempRecordidObject);
			} else if (tempRecordidObject instanceof Integer) {
				temprecordId = Long.parseLong(tempRecordidObject.toString());
			}
			Long relativeId = null;
			try {
				Method method2 = recordObject.getClass().getMethod(
						"getRelativeId", null);

				relativeId = (Long) method2.invoke(recordObject);
			} catch (java.lang.NoSuchMethodException e) {
				// e.printStackTrace();
			}

			if (temprecordId == recordId) {
				String approvalflagmethodId = "setIsApprove";
				Class[] argTypes = new Class[] { Byte.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void doDirectApprovalAndDeleteProcess(String useCase, Long recordId,
			Byte status) {
		try {
			// Fetch actual record from process's table
			Object recordObject = this.getWorkflowService().getRecord(useCase,
					recordId);
			// Get the use case name
			/*
			 * String[] name = workflowDetailVO.getObject().getClass()
			 * .getName().split("entity.");
			 */
			String nameTemp = recordObject.getClass().getName()
					.replace(".domain.entity.", ".DOMAIN.ENTITY.");
			String[] name = nameTemp.split(".DOMAIN.ENTITY.");
			// Some times use case name is appended by _javassist, so
			// get
			// proper use case name
			if (name[name.length - 1].contains("_$$_javassist")) {
				name[name.length - 1] = name[name.length - 1].substring(0,
						name[name.length - 1].indexOf("_$$_javassist"));
			}
			// getter method of primary key
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = recordObject.getClass().getMethod(methodId, null);
			Object tempRecordidObject = method.invoke(recordObject, null);
			long temprecordId = 0;
			if (tempRecordidObject instanceof Long) {
				temprecordId = ((Long) tempRecordidObject);
			} else if (tempRecordidObject instanceof Integer) {
				temprecordId = Long.parseLong(tempRecordidObject.toString());
			}
			Long relativeId = null;
			try {
				Method method2 = recordObject.getClass().getMethod(
						"getRelativeId", null);

				relativeId = (Long) method2.invoke(recordObject);
			} catch (java.lang.NoSuchMethodException e) {
				// e.printStackTrace();
			}

			if (temprecordId == recordId) {
				String approvalflagmethodId = "setIsApprove";
				Class[] argTypes = new Class[] { Byte.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);

				// set the "is_approve value to represent the state of
				// record (approved, rejected, publish etc.)
				updateApprovalStatus(recordObject, useCase, relativeId, status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Boolean isDirectDeletePossible(String entity, long recordId,
			User user, Byte processType) {
		try {
			if (processType == (byte) WorkflowConstants.ProcessMethod.Delete
					.getCode()) {
				ModuleProcess targetModuleProcess = workflowService
						.getModuleProcessByTitle(entity).get(0);
				List<WorkflowDetailProcess> lastProcessesListAgainstTargetRecord = workflowService
						.findLastWorkflowDetailProcessForTargetRecord(
								targetModuleProcess, recordId);
				WorkflowDetailProcess lastProcessAgainstTargetRecord = workflowService
						.findLastWorkflowDetailProcessForTargetRecord(
								targetModuleProcess, recordId).get(0);
				Integer currentStatusOfTargetRecord = lastProcessAgainstTargetRecord
						.getOperation().intValue();

				if (((currentStatusOfTargetRecord == WorkflowConstants.Status.NoDecession
						.getCode())
						|| (currentStatusOfTargetRecord == WorkflowConstants.Status.Confirm
								.getCode()) || (currentStatusOfTargetRecord == WorkflowConstants.Status.Deny
						.getCode()))
						// if size is greater than 3, that implies that this is
						// not
						// fresh data entry, and may be edit case so delete
						// approval
						// will be required, therefore don't return true and go
						// with
						// else case
						&& lastProcessesListAgainstTargetRecord.size() <= 3)
					return true;
				else
					return false;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * This Method update the is_approved value of given object's table
	 * 
	 * @param module
	 *            : name of module to which object belongs to
	 * 
	 * @param Object
	 *            : will represent the record of a table whose is_approve value
	 *            is to be updated
	 * 
	 * @param status
	 *            : value of is_approved column that is to be updated
	 * 
	 * @author Khawaja Hasham
	 */
	private void updateApprovalStatus(Object entityObject,
			String entityClassFullyQualifiedName, Long countryId, Byte status) {
		try {
			String entityClassName = WorkflowUtils
					.getEntityClassName(entityClassFullyQualifiedName);

			AIOTechGenericDAO<Object> dao = WorkflowUtils
					.getEntityDAO(entityClassFullyQualifiedName);

			// in case of publish operation
			if (status.byteValue() == WorkflowConstants.Status.Published
					.getCode()) {

				Object exampleObject = WorkflowUtils
						.createNewEntityObject(entityClassFullyQualifiedName);

				// set relative id
				Method method = null;
				try {
					Class[] argTypes = new Class[] { Long.class };
					method = exampleObject.getClass().getMethod(
							"setRelativeId", argTypes);
					method.invoke(exampleObject, countryId);
				} catch (java.lang.NoSuchMethodException e) {

				}
				// set publish date
				/*
				 * Class[] argTypesUpdatedDate = new Class[] { Date.class };
				 * method = entityObject.getClass().getMethod("setUpdatedDate",
				 * argTypesUpdatedDate); method.invoke(exampleObject, new
				 * Date());
				 */

				// publish record, if there is one record, update it else if
				// there
				// are two records then set the previous published record,s is
				// approved to 'old published' and publish the new record
				List<Object> list = dao.findByExample(exampleObject);
				if (list.size() < 2) {
					dao.merge(entityObject);
				} else if (list.size() == 2) {

					dao.getHibernateSession().evict(entityObject);

					Class[] argTypes2 = new Class[] { Byte.class };
					method = entityObject.getClass().getMethod("setIsApprove",
							argTypes2);
					method.invoke(entityObject,
							(byte) WorkflowConstants.Status.OldPublished
									.getCode());

					// set publish date
					try {
						Class[] argTypesUpdatedDate = new Class[] { Date.class };
						method = entityObject.getClass().getMethod(
								"setUpdatedDate", argTypesUpdatedDate);
						method.invoke(entityObject, new Date());
						dao.merge(entityObject);
					} catch (java.lang.NoSuchMethodException e) {

					}
					Object first = list.get(0);

					method = first.getClass().getMethod(
							"get" + entityClassName + "Id", null);
					Object firstId = null;
					Class[] argTypes1 = null;
					if (method.getReturnType().getName()
							.equals("java.lang.Long")) {
						firstId = method.invoke(first);
						argTypes1 = new Class[] { Long.class };
					} else if (method.getReturnType().getName()
							.equals("java.lang.Integer")) {
						firstId = method.invoke(first);
						argTypes1 = new Class[] { Integer.class };
					}

					method = entityObject.getClass().getMethod(
							"set" + entityClassName + "Id", argTypes1);
					method.invoke(entityObject, firstId);

					method = entityObject.getClass().getMethod("setIsApprove",
							argTypes2);
					Byte publishedByte = (byte) WorkflowConstants.Status.Published
							.getCode();
					method.invoke(entityObject, publishedByte);
					dao.merge(entityObject);

				} else {
					dao.merge(entityObject);
				}
			} else {
				dao.merge(entityObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author: Khawaja Hasham
	 * 
	 *          Purpose: This method update the status of message
	 * 
	 * @param: workflow detail VO: To represent the workflow detail whose
	 *         message's status have to be set
	 * 
	 *         record Id : Id of record against which status is to be set
	 * 
	 *         status: value of status to be set
	 */
	public void updateNotificationStatus(WorkflowDetailVO workflowDetailVO,
			Long recordId, Byte status, User user) throws Exception {

		Message message = new Message();
		message.setPersonId(user.getPersonId());
		WorkflowDetailProcess workflowDetailProcess = new WorkflowDetailProcess();
		workflowDetailProcess.setRecordId(recordId);
		workflowDetailProcess.setWorkflowDetail(workflowDetailVO
				.convertoToWorkflowDetail());

		/*
		 * List<WorkflowDetailProcess> workflowDetailProcesses = workFlowService
		 * .getWorkflowProcess(workflowDetailProcess);
		 */

		if (workflowDetailVO.getWorkflowDetailProcess() != null) {
			message.setWorkflowDetailProcess(workflowDetailVO
					.getWorkflowDetailProcess());

			// get the message whose status is to be set
			List<Message> messages = messageService
					.getMessageOfProcess(message);
			if (messages.size() > 0) {
				// set status and save message
				for (Message msg : messages) {
					msg.setStatus(status);
					messageService.saveMessage(msg);
				}
			}
		}
	}

	/**
	 * This Method performs escalation.
	 * 
	 * @param personId
	 *            : Id of the person whose messages to escalate
	 * @param operations
	 *            : List of Byte, which contains values of WorkflowConstants
	 *            enum
	 * @param status
	 *            : 0 or 1, based on 'mofa.sys_message.status' column
	 * @return
	 * @throws Exception
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	public void generateEscalatedNotifications(long personId,
			List<Byte> operations, Byte status) throws Exception {

		/*
		 * List<Message> messages = messageService
		 * .getAllMessagesByPersonAndOperations(personId, operations, status);
		 * 
		 * Date currentDate = new Date();
		 * 
		 * // for each message, check for escalation for (Message message :
		 * messages) {
		 * 
		 * Date createdDate = message.getCreatedDate();
		 * 
		 * Message escalatedMessage = new Message();
		 * 
		 * if (message.getWorkflowDetailProcess().getWorkflowDetail()
		 * .getIsDays() == 1) { // comparison on basis of days
		 * 
		 * // get number of days to compare with createdDate int remindAfterDays
		 * = message.getWorkflowDetailProcess()
		 * .getWorkflowDetail().getReminder(); int escalateAfterDays =
		 * message.getWorkflowDetailProcess()
		 * .getWorkflowDetail().getEscalation(); escalateAfterDays =
		 * escalateAfterDays + remindAfterDays;
		 * 
		 * // get escalationDate by (createdDate + escalateAfterDays) Date
		 * reminderDate = WorkflowDateFormat.addField(createdDate,
		 * Calendar.DATE, escalateAfterDays);
		 * 
		 * // if reminderDate is equal to OR less than current date, // perform
		 * escalation if ((WorkflowDateFormat .compare2Dates(reminderDate,
		 * currentDate) == "LESS") ||
		 * (WorkflowDateFormat.compare2Dates(reminderDate, currentDate) ==
		 * "EQUAL")) {
		 * 
		 * // change status of this message message.setStatus((byte) 1);
		 * message.setIsEscalated(false);
		 * 
		 * // add new message with personId of escalated person escalatedMessage
		 * = createEscalatedMessage(message);
		 * 
		 * // update message and save escalatedMessage entry
		 * messageService.saveMessage(message);
		 * messageService.saveMessage(escalatedMessage);
		 * 
		 * }
		 * 
		 * } else if (message.getWorkflowDetailProcess().getWorkflowDetail()
		 * .getIsDays() == 2) { // comparison on basis of hours
		 * 
		 * // get number of hours to compare with createdDate hours int
		 * remindAfterHours = message.getWorkflowDetailProcess()
		 * .getWorkflowDetail().getReminder(); int escalateAfterHours =
		 * message.getWorkflowDetailProcess()
		 * .getWorkflowDetail().getEscalation(); escalateAfterHours =
		 * escalateAfterHours + remindAfterHours;
		 * 
		 * // get escalateHours by (createdDate Hours + escalateAfterHours) Date
		 * reminderDate = WorkflowDateFormat.addField(createdDate,
		 * Calendar.HOUR, escalateAfterHours);
		 * 
		 * // if reminderDate is equal to OR less than current date, // fetch
		 * that record and add to WorkflowDetailVO list if ((WorkflowDateFormat
		 * .compare2Dates(reminderDate, currentDate) == "LESS") ||
		 * (WorkflowDateFormat.compare2Dates(reminderDate, currentDate) ==
		 * "EQUAL")) {
		 * 
		 * // change status of this message message.setStatus((byte) 1);
		 * message.setIsEscalated(false);
		 * 
		 * // add new message with personId of escalated person escalatedMessage
		 * = createEscalatedMessage(message);
		 * 
		 * // update message and save escalatedMessage entry
		 * messageService.saveMessage(message);
		 * messageService.saveMessage(escalatedMessage);
		 * 
		 * } }
		 * 
		 * }
		 */

	}

	/**
	 * This Method clones given Message object with updated values for personId,
	 * status and createdDate
	 * 
	 * @param message
	 *            : object to be cloned
	 * @return: New object of Message which is cloned with the provided
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */
	private Message createEscalatedMessage(Message message) {

		return message;
	}

	/**
	 * saves or updates message record in database.
	 * 
	 * @param message
	 *            : object to save or edited.
	 * 
	 * 
	 * @author Shoaib Ahmad Gondal @ 26-Sep-12
	 */

	public void saveMessage(Message message) throws Exception {
		messageService.saveMessage(message);
	}

	public void saveMessage(List<Message> messages) throws Exception {
		messageService.saveMessage(messages);
	}

	public boolean isDirectAddBusinessCall(Long messageId) throws Exception {
		boolean status = false;
		Message message = messageService.getMessageInfoByMessageId(messageId);
		if ((byte) message.getWorkflowDetailProcess().getProcessType() == (byte) 1)
			status = true;
		return status;
	}

	public boolean isDirectEditBusinessCall(Long messageId) throws Exception {
		boolean status = false;
		Message message = messageService.getMessageInfoByMessageId(messageId);
		if ((byte) message.getWorkflowDetailProcess().getProcessType() == (byte) 2)
			status = true;
		return status;
	}

	public boolean isDirectDeleteBusinessCall(Long messageId) throws Exception {
		boolean status = false;
		Message message = messageService.getMessageInfoByMessageId(messageId);
		if ((byte) message.getWorkflowDetailProcess().getProcessType() == (byte) 3)
			status = true;
		return status;
	}

	/**
	 * Returns WorkflowDetail object.
	 * 
	 * @param workflowDetailId
	 * @param preview
	 * @return
	 * @throws Exception
	 * 
	 * @author Shoaib Ahmad Gondal
	 */
	/*
	 * public boolean findQuickApproval(List<WorkflowDetail> workflowDetails) {
	 * 
	 * if (workflowDetails != null) { for (WorkflowDetail workflowDetail :
	 * workflowDetails) {
	 * 
	 * } }
	 * 
	 * }
	 */

	public WorkflowDetail getWorkflowDetailObject(Long workflowDetailId,
			Boolean preview) throws Exception {

		return workflowService.getWorkflowDetailByWorkflowDeatilId(
				workflowDetailId, preview);
	}

	public WorkflowService getWorkflowService() {
		return workflowService;
	}

	@Autowired
	public void setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	@Autowired
	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

}
