package com.aiotech.aios.workflow.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.MenuRight;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowVO;
import com.aiotech.aios.workflow.service.MenuRightService;
import com.aiotech.aios.workflow.service.MessageService;
import com.aiotech.aios.workflow.service.UserRoleService;
import com.aiotech.aios.workflow.service.WorkflowService;
import com.aiotech.aios.workflow.util.WorkflowConstants;

/*
 * @author: Khawaja Hasham
 * 
 * */
@Service
@Transactional
public class SaveWorkflowBL {

	private WorkflowService workflowService;
	private MessageService messageService;
	private UserRoleService userRoleService;
	private MenuRightService menuRightService;

	public boolean isAllowedToEdit(Object entityObject, User user)
			throws Exception {

		String[] name = entityObject.getClass().getName().split("entity.");
		String usecase = (name[name.length - 1]);
		List<ModuleProcess> moduleProcesses = workflowService
				.getModuleProcessByTitle(usecase);

		ModuleProcess moduleProcess = null;
		Workflow workflow = null;
		WorkflowDetail workflowDetail = null;

		Boolean isAllowToSave = true;

		// If process is defined against the use case
		if (moduleProcesses.size() > 0) {
			moduleProcess = moduleProcesses.get(0);

			// if flows are defined against the process
			if (moduleProcess.getWorkflows().size() > 0) {
				List<Workflow> workflows = null;

				// if logged in user have multiple role then check against
				// which role he have entry right, fetch the entry workflow
				// against that role
				for (UserRole userRole : user.getUserRoles()) {
					workflows = workflowService.getWorkFlowOfEntryUser(
							userRole.getRole(), moduleProcess);
					if (workflows.size() > 0) {
						break;
					}
				}

				if ((workflows != null) && (workflows.size() > 0)) {
					workflow = workflows.get(0);
				}

				if ((workflow != null)
						&& (workflow.getWorkflowDetails().size() > 0)) {

					// get the entry workflow detail
					workflowDetail = workflow.getWorkflowDetails().iterator()
							.next();

					List<WorkflowDetail> workflowDetails = workflowService
							.findNextWorkflow(workflowDetail);

					if ((workflowDetails != null)
							&& (workflowDetails.size() > 0)) {
						WorkflowDetailProcess workflowDetailProcess = new WorkflowDetailProcess();
						WorkflowDetail approvalWorkflowDetail = workflowDetails
								.get(0);

						workflowDetailProcess
								.setWorkflowDetail(approvalWorkflowDetail);

						Method getIdMethod = entityObject.getClass().getMethod(
								"get" + usecase + "Id", null);

						Object recordId = getIdMethod
								.invoke(entityObject, null);

						workflowDetailProcess.setRecordId((Long) recordId);

						List<WorkflowDetailProcess> approvalProcess = workflowService
								.getWorkflowProcess(workflowDetailProcess);

						if ((approvalProcess != null)
								&& (approvalProcess.size() > 0)) {
							Message message = new Message();
							message.setRecordId((Long) recordId);
							message.setWorkflowDetailProcess(approvalProcess
									.get(0));

							List<Message> messages = messageService
									.getUnApprovedMessageAgainstWorkflowDetailProcess(message);
							if ((messages != null) && (messages.size() > 0)) {
								isAllowToSave = false;
							}

						}
					}

				}
			}
		}
		return isAllowToSave;
	}

	public void saveWorkflow(WorkflowVO workflowVO,
			List<WorkflowDetailVO> recentWorkflows) throws Exception {

		boolean isRedefiningWorkflow = false;
		if (workflowVO.getWorkflowId() != null
				&& workflowVO.getWorkflowId() > 0)
			isRedefiningWorkflow = true;

		Workflow workflow = workflowVO.convertoToWorkflow();

		if (workflow.getWorkflowId() != null && workflow.getWorkflowId() > 0) {
			Workflow workflowTemp = workflowService
					.getWorkflowByWorkflowId(workflow.getWorkflowId());
			List<Long> workflowIds = new ArrayList<Long>();

			workflowIds.add(workflowTemp.getWorkflowId());
			if (workflowTemp.getWorkflow() != null
					&& workflowTemp.getWorkflow().getWorkflowId() != null)
				workflowIds.add(workflowTemp.getWorkflow().getWorkflowId());

			workflowService.deActivateWorkflows(workflowIds);
			workflow.setWorkflowId(null);
			workflowService.saveWorkflow(workflow);

		} else {
			workflowService.saveWorkflow(workflow);
		}

		int i = 0;
		Long parentId = null;
		Role role = null;

		// Sort(based on operation order) the workflow details defined
		// by
		// user as user can
		// define the workflows un-sequentially
		Collections.sort(recentWorkflows);

		// For each workflow details defined by user, define the reverse
		// workflow details
		for (WorkflowDetailVO recentwf : recentWorkflows) {

			recentwf.setWorkflow(workflow);

			// workflowDetail represents the user defined workflows
			WorkflowDetail workflowDetail = recentwf.convertoToWorkflowDetail();

			List<WorkflowDetail> addWorkflowDetails = new ArrayList<WorkflowDetail>();

			// Forward flows will always be in preview mode
			addWorkflowDetails.add(workflowDetail);
			workflowService.saveWorkflowDetails(workflowDetail);
			workflowService.getWorkflowDetailDAO().evict(workflowDetail);

			// Checking if the operation is Entry
			if (recentwf.getRole() != null
					&& (byte) recentwf.getOperationMaster().getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Entry
							.getCode()) {

				Menu menu = new Menu();

				if (recentwf.getAccessRightType() == 0) {

					List<Menu> menusForScreenTargetScreenId = menuRightService
							.getMenuByScreenId(recentwf.getScreenByScreenId()
									.getScreenId());

					if (menusForScreenTargetScreenId.size() > 1) {
						// if more then 1 menu is referring to target
						// screen than use last menu item
						menu = menusForScreenTargetScreenId
								.get(menusForScreenTargetScreenId.size() - 1);
					} else {
						List<Menu> menus = menuRightService.getMenuByScreenId(
								recentwf.getScreenByScreenId().getScreenId());
						if(menus!=null && menus.size()>0)
							menu=menus.get(0);
					}
					// Checking whether the menu has it parent or not.
					if (menu!=null && menu.getMenu() != null) {

						// Check for parentMenu in recursive fashion to
						// ensure that all menu levels are covered
						Menu parentMenu = menuRightService.getMenuById(menu
								.getMenu().getMenuItemId());

						while (parentMenu != null) {
							// Getting parent menu rights against the
							// role.
							List<MenuRight> parentMenuRights = menuRightService
									.getMenuRightByMenuIdandRoleId(
											parentMenu.getMenuItemId(),
											recentwf.getRole().getRoleId());
							// Checking whether parent menu access right
							// are
							// assigned to the role or not.
							if (parentMenuRights.size() <= 0) {
								// If no menuRights are defined then
								// parents
								// menu right is assigned to the role.
								MenuRight menuRight = new MenuRight();
								menuRight.setIsActive(true);
								menuRight.setRole(recentwf.getRole());
								menuRight.setMenu(parentMenu);
								menuRightService
										.saveOrUpdateMenuRight(menuRight);

							}
							if (parentMenu.getMenu() != null)
								parentMenu = menuRightService
										.getMenuById(parentMenu.getMenu()
												.getMenuItemId());
							else
								parentMenu = null;
						}

					}

					// revoking menu right of previous workflow
					if (isRedefiningWorkflow) {
						List<Long> workflowIds = new ArrayList<Long>();
						workflowIds.add(workflowVO.getWorkflowId());

						ArrayList<WorkflowDetail> oldWorkflowEntryDetails = (ArrayList<WorkflowDetail>) workflowService
								.getWorkflowDetailEntryByWorkflowId(workflowIds
										.get(0));

						if ((oldWorkflowEntryDetails != null)
								&& (oldWorkflowEntryDetails.size() > 0)) {
							WorkflowDetail entryDetail = oldWorkflowEntryDetails
									.get(0);
							if (entryDetail.getAccessRightType() == 0) {
								ArrayList<MenuRight> menuRightsRevoke = (ArrayList<MenuRight>) menuRightService
										.getMenuRightByMenuIdandRoleId(menu
												.getMenuItemId(), entryDetail
												.getRole().getRoleId());
								if ((menuRightsRevoke != null)
										&& (menuRightsRevoke.size() > 0)) {
									menuRightService
											.removeMenuRight(menuRightsRevoke
													.get(0));
								}
							}
						}

					}

					// Getting menu rights against the role.
					List<MenuRight> menuRights = menuRightService
							.getMenuRightByMenuIdandRoleId(
									menu.getMenuItemId(), recentwf.getRole()
											.getRoleId());

					// Checking whether menu access right are assigned
					// to the role or not.
					if (menu!=null && menuRights.size() <= 0) {
						// If no menuRights are defined then menu right
						// is assigned to the role.
						MenuRight menuRight = new MenuRight();
						menuRight.setIsActive(true);
						menuRight.setRole(recentwf.getRole());
						menuRight.setMenu(menu);
						menuRightService.saveOrUpdateMenuRight(menuRight);
					}

				} else {

				}

			}

		}

	}

	public WorkflowService getWorkflowService() {
		return workflowService;
	}

	@Autowired
	public void setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	@Autowired
	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

	public MenuRightService getMenuRightService() {
		return menuRightService;
	}

	@Autowired
	public void setMenuRightService(MenuRightService menuRightService) {
		this.menuRightService = menuRightService;
	}

}
