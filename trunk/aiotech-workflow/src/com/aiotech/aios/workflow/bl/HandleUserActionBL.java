/**
 * 
 */
package com.aiotech.aios.workflow.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.ApproverPublisherVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowVO;
import com.aiotech.aios.workflow.service.MessageService;
import com.aiotech.aios.workflow.service.UserRoleService;
import com.aiotech.aios.workflow.service.WorkflowService;
import com.aiotech.aios.workflow.util.Quantum;
import com.aiotech.aios.workflow.util.Quantums;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.common.ConstantsUtil;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */
@Service
@Transactional
public class HandleUserActionBL {

	private WorkflowService workflowService;
	private MessageService messageService;
	private UserRoleService userRoleService;

	/**
	 * @author: Ahsan M. Purpose: Task for this method is to collect and
	 *          populate the approver and publisher info (i.e. personId) with
	 *          the provided, standardized work flow information object.
	 * 
	 * @param: approverPublisherList: is list of VOs containing the target
	 *         records for which approver and publisher ids are required.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, ApproverPublisherVO> collectApproverAndPublisherInfo(
			HashMap<String, ApproverPublisherVO> approverPublisherList) {

		@SuppressWarnings("rawtypes")
		Iterator recordsIterator = approverPublisherList.entrySet().iterator();

		while (recordsIterator.hasNext()) {
			try {

				Map.Entry<String, ApproverPublisherVO> targetRecord = (Map.Entry<String, ApproverPublisherVO>) recordsIterator
						.next();
				Long processId = Long.parseLong(targetRecord.getKey()
						.toString().split("_")[0]);
				Long recordId = Long.parseLong(targetRecord.getKey().toString()
						.split("_")[1]);

				// Fetch the lastest record id against the published record id
				// for workflow detail process only
				recordId = fetchLatestRecordIdFromUseCase(processId, recordId);

				ApproverPublisherVO ap = new ApproverPublisherVO();

				ap.setProcessId(processId);
				ap.setRecordId(recordId);

				ap.setApproverId(workflowService
						.getEntityBasedWorkflowDetailProcessAgainstRecord(
								processId, recordId,
								WorkflowConstants.Status.Approved.getCode())
						.getCreatedBy());

				ap.setPublisherId(workflowService
						.getEntityBasedWorkflowDetailProcessAgainstRecord(
								processId, recordId,
								WorkflowConstants.Status.Published.getCode())
						.getCreatedBy());

				targetRecord.setValue(ap);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return approverPublisherList;
	}

	public Boolean isDirectDeletePossible(String entity, long recordId,
			User user) {
		try {
			ModuleProcess targetModuleProcess = workflowService
					.getModuleProcessByTitle(entity).get(0);
			List<WorkflowDetailProcess> lastProcessesListAgainstTargetRecord = workflowService
					.findLastWorkflowDetailProcessForTargetRecord(
							targetModuleProcess, recordId);
			WorkflowDetailProcess lastProcessAgainstTargetRecord = workflowService
					.findLastWorkflowDetailProcessForTargetRecord(
							targetModuleProcess, recordId).get(0);
			Integer currentStatusOfTargetRecord = lastProcessAgainstTargetRecord
					.getOperation().intValue();

			if (((currentStatusOfTargetRecord == WorkflowConstants.Status.NoDecession
					.getCode())
					|| (currentStatusOfTargetRecord == WorkflowConstants.Status.Saved
							.getCode()) || (currentStatusOfTargetRecord == WorkflowConstants.Status.Deny
					.getCode()))
					// if size is greater than 3, that implies that this is not
					// fresh data entry, and may be edit case so delete approval
					// will be required, therefore don't return true and go with
					// else case
					&& lastProcessesListAgainstTargetRecord.size() <= 3)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public String handleUserActionAgainstDeleteRequest(String entity,
			long recordId, User user, Boolean finalDelete,
			Boolean deleteRejected) throws Exception {

		String returnMessage = "SUCCESS";

		Workflow workflowToExecuteForDelete = null;
		Long lastProcessExecutor = null;
		Long currentPerson = null;
		List<WorkflowDetailProcess> lastProcessesListAgainstTargetRecord = null;

		// check the the record has any child record in active status
		returnMessage = checkRecordHasAnyChildRecord(entity, recordId);
		if (returnMessage != null && !returnMessage.equalsIgnoreCase("SUCCESS"))
			return returnMessage;

		ModuleProcess targetModuleProcess = workflowService
				.getModuleProcessByTitle(entity).get(0);
		List<WorkflowDetailProcess> lastProcessAgainstTargetRecordList = workflowService
				.findLastWorkflowDetailProcessForTargetRecord(
						targetModuleProcess, recordId);
		WorkflowDetailProcess lastProcessAgainstTargetRecord = null;
		if (lastProcessAgainstTargetRecordList != null
				&& lastProcessAgainstTargetRecordList.size() > 0) {
			lastProcessAgainstTargetRecord = lastProcessAgainstTargetRecordList
					.get(0);

			lastProcessesListAgainstTargetRecord = workflowService
					.findLastWorkflowDetailProcessForTargetRecord(
							targetModuleProcess, recordId);

			// deletion work flow should be fetched using "delete request"
			// operation
			// codes from work flow Detail table against given module process
			workflowToExecuteForDelete = workflowService
					.getDeleteWorkflowForModuleProcess(targetModuleProcess);

			// get the person Id of last person who performed work flow
			// operation on
			// this record. This will be used to add messages of delete
			// notification for respective person...
			lastProcessExecutor = lastProcessAgainstTargetRecord.getCreatedBy();

			// get person Id for the current user i.e. who requested the
			// deletion of
			// this record
			currentPerson = user.getPersonId();

		} else {
			finalDelete = true;
		}

		// if the record has just being just added, is waiting for approval,
		// is rejected, or has been approved for deletion then this request is
		// for direct/final deletion
		if (finalDelete) {/*
						 * 
						 * // Set isApprove Status as deleted
						 * workflowService.setRecordStatusAsDeleteOrWaitingForDelete
						 * (entity, recordId,
						 * WorkflowConstants.Status.Deleted.getCode());
						 * 
						 * if (lastProcessAgainstTargetRecord != null) { //
						 * disable previous messages being shown for approval
						 * and data // added List<Message> activeMessages =
						 * messageService
						 * .getMessagesForRecordBasedOnModuleProcessAndStatus(
						 * lastProcessAgainstTargetRecord.getRecordId(),
						 * targetModuleProcess, Byte.valueOf("0"));
						 * 
						 * if (activeMessages != null && activeMessages.size() >
						 * 0) {
						 * 
						 * for (Message message : activeMessages) {
						 * 
						 * message.setStatus((byte) 1);
						 * message.setIsAlert(false); }
						 * messageService.saveAllMessage(activeMessages); }
						 * 
						 * // insert process record for this deletion
						 * WorkflowDetailProcess deleteDetailProcess = new
						 * WorkflowDetailProcess();
						 * deleteDetailProcess.setWorkflowId
						 * (workflowToExecuteForDelete .getWorkflowId());
						 * deleteDetailProcess.setOperation(Byte
						 * .parseByte(WorkflowConstants.Status.Deleted.getCode()
						 * + ""));
						 * deleteDetailProcess.setCreatedBy(currentPerson);
						 * deleteDetailProcess.setCreatedDate(new Date());
						 * deleteDetailProcess.setRecordId(recordId);
						 * 
						 * // find and set deletion (final deleted) work flow
						 * detail for (WorkflowDetail workflowDetail :
						 * workflowToExecuteForDelete .getWorkflowDetails()) {
						 * 
						 * if (workflowDetail.getOperation().intValue() ==
						 * WorkflowConstants.Status.Deleted .getCode()) {
						 * 
						 * deleteDetailProcess.setWorkflowDetail(workflowDetail);
						 * break; } } workflowService.saveWorkflowDetailProcess(
						 * deleteDetailProcess);
						 * 
						 * // add new messages to notify of this deletion to
						 * last // approver/publisher (if available) - just for
						 * mark as // read option if
						 * (lastProcessesListAgainstTargetRecord.get(1) != null
						 * && lastProcessesListAgainstTargetRecord.get(1)
						 * .getOperation().intValue() ==
						 * WorkflowConstants.Status.DeleteApprovalReadOnly
						 * .getCode()) {
						 * 
						 * // to skip first record Boolean skip = true; // for
						 * loop here to notify all the previous approvers and //
						 * publishers, in addition to last approver/publisher,
						 * all // the // previous approvers/publishers should
						 * also know that this // data // is deleted now // and
						 * avoid adding more then one message to same person
						 * List<Long> notifiedPeople = new ArrayList<Long>();
						 * 
						 * for (WorkflowDetailProcess process :
						 * lastProcessesListAgainstTargetRecord) {
						 * 
						 * if (!skip) { if
						 * (!notifiedPeople.contains(process.getCreatedBy()
						 * .longValue())) { messageService
						 * .saveMessageAgainstworkflowDeleteProcess(
						 * process.getCreatedBy(), targetModuleProcess
						 * .getProcessTitle(), WorkflowConstants.Status.Deleted
						 * .getCode(), deleteDetailProcess);
						 * notifiedPeople.add(process.getCreatedBy()
						 * .longValue()); } } else skip = false; }
						 * 
						 * } else if (lastProcessAgainstTargetRecord != null &&
						 * lastProcessExecutor != null // &&
						 * lastProcessAgainstTargetRecord
						 * .getOperation().intValue() // != //
						 * WorkflowConstants.Status.NoDecession // .getCode() //
						 * &&
						 * lastProcessAgainstTargetRecord.getOperation().intValue
						 * () // != // WorkflowConstants.Status.Confirm //
						 * .getCode() // &&
						 * lastProcessAgainstTargetRecord.getOperation
						 * ().intValue() // != // WorkflowConstants.Status.Deny
						 * // .getCode() ) {
						 * 
						 * messageService.saveMessageAgainstworkflowDeleteProcess
						 * ( lastProcessAgainstTargetRecord.getCreatedBy(),
						 * targetModuleProcess.getProcessTitle(),
						 * WorkflowConstants.Status.Deleted.getCode(),
						 * deleteDetailProcess); } // add new messages to notify
						 * of this deletion to // current user (i.e. who has
						 * deleted the record) - just for // mark as // read
						 * option
						 * messageService.saveMessageAgainstworkflowDeleteProcess
						 * ( deleteDetailProcess.getCreatedBy(),
						 * targetModuleProcess.getProcessTitle(),
						 * WorkflowConstants.Status.Deleted.getCode(),
						 * deleteDetailProcess); }
						 */
		}
		// else if the delete request has been rejected by the respective person
		// then "find and re-enter" the lastly active (before delete approval
		// was sent) work flow detail process and messages. This will ensure
		// that lastly active messages will start showing up again to respective
		// "user-s"
		else if (deleteRejected) {/*
								 * 
								 * // disable the "waiting for delete approval"
								 * message for last detail // process...
								 * List<Message> activeMessages = messageService
								 * .
								 * getMessagesForRecordBasedOnModuleProcessAndStatus
								 * (
								 * lastProcessAgainstTargetRecord.getRecordId(),
								 * targetModuleProcess, new Byte("0"));
								 * 
								 * if (activeMessages != null &&
								 * activeMessages.size() > 0) {
								 * 
								 * for (Message message : activeMessages) {
								 * 
								 * message.setStatus((byte) 1);
								 * message.setIsAlert(false); }
								 * messageService.saveAllMessage
								 * (activeMessages); }
								 * 
								 * // insert process record to issue delete
								 * rejected notification for // target item
								 * WorkflowDetailProcess
								 * deleteRejectedDetailProcess = new
								 * WorkflowDetailProcess();
								 * deleteRejectedDetailProcess
								 * .setWorkflowId(workflowToExecuteForDelete
								 * .getWorkflowId());
								 * deleteRejectedDetailProcess.setOperation(Byte
								 * .
								 * parseByte(WorkflowConstants.Status.DeleteRejected
								 * .getCode() + ""));
								 * deleteRejectedDetailProcess
								 * .setCreatedBy(currentPerson);
								 * deleteRejectedDetailProcess
								 * .setCreatedDate(new Date());
								 * deleteRejectedDetailProcess
								 * .setRecordId(recordId);
								 * 
								 * // find and set delete rejected work flow
								 * detail for (WorkflowDetail workflowDetail :
								 * workflowToExecuteForDelete
								 * .getWorkflowDetails()) {
								 * 
								 * if (workflowDetail.getOperation().intValue()
								 * == WorkflowConstants.Status.DeleteRejected
								 * .getCode()) {
								 * 
								 * deleteRejectedDetailProcess
								 * .setWorkflowDetail(workflowDetail); break; }
								 * }
								 * 
								 * workflowService .saveWorkflowDetailProcess(
								 * deleteRejectedDetailProcess);
								 * 
								 * // add new message for
								 * "deletion request is rejected" to the //
								 * "deleter (the one who requested delete)" -
								 * for read only purpose WorkflowDetailProcess
								 * lastDeleteRequestReadOnlyProcessAgainstTargetRecord
								 * = workflowService
								 * .getLastDeleteRequestReadOnlyProcess(
								 * targetModuleProcess, recordId, (byte)
								 * WorkflowConstants
								 * .Status.DeleteApprovalReadOnly .getCode());
								 * 
								 * messageService.
								 * saveMessageAgainstworkflowDeleteProcess(
								 * lastDeleteRequestReadOnlyProcessAgainstTargetRecord
								 * .getCreatedBy(), targetModuleProcess
								 * .getProcessTitle(),
								 * WorkflowConstants.Status.DeleteRejected
								 * .getCode(), deleteRejectedDetailProcess);
								 * 
								 * // add new message for
								 * "deletion request is rejected" to the //
								 * "rejecter (current user who has rejected delete)"
								 * - for read only // purpose messageService.
								 * saveMessageAgainstworkflowDeleteProcess(
								 * deleteRejectedDetailProcess.getCreatedBy(),
								 * targetModuleProcess.getProcessTitle(),
								 * WorkflowConstants
								 * .Status.DeleteRejected.getCode(),
								 * deleteRejectedDetailProcess);
								 * 
								 * // once new messages are taken care of; we
								 * need to "re-enable" the // lastly disabled
								 * waiting for publish messages against all the
								 * work // flows currently defined for this
								 * module process List<Workflow>
								 * allDataEntryWorkflowsDefinedforTargetModuleProcess
								 * = workflowService
								 * .getDataEntryWorkFlowsOfProcess
								 * (targetModuleProcess);
								 * 
								 * List<Byte> excludeOperations = new
								 * ArrayList<Byte>(); excludeOperations
								 * .add((byte)
								 * WorkflowConstants.Status.DeleteApprovalReadOnly
								 * .getCode()); excludeOperations.add((byte)
								 * WorkflowConstants.Status.Deleted .getCode());
								 * excludeOperations .add((byte)
								 * WorkflowConstants.Status.DeleteRejected
								 * .getCode()); excludeOperations .add((byte)
								 * WorkflowConstants.Status.DeleteApproval
								 * .getCode());
								 * 
								 * for (Workflow workflow :
								 * allDataEntryWorkflowsDefinedforTargetModuleProcess
								 * ) {
								 * 
								 * // get work flow detail processes where
								 * workflow = current in // loop and workflow
								 * detail process has latest operations = //
								 * Approved & waiting for publish (i.e. 8 & 9)
								 * List<WorkflowDetailProcess> detailProcess =
								 * workflowService
								 * .getDetailProcessForWorkflow(workflow,
								 * excludeOperations);
								 * 
								 * // get the record status to update the record
								 * information if
								 * ((detailProcess.get(0).getOperation() ==
								 * WorkflowConstants.Status.Published
								 * .getCode()) ||
								 * (detailProcess.get(0).getOperation() ==
								 * WorkflowConstants.Status.Approved
								 * .getCode())) workflowService.
								 * setRecordStatusAsDeleteOrWaitingForDelete(
								 * entity, recordId, detailProcess.get(0)
								 * .getOperation());
								 * 
								 * if (detailProcess == null ||
								 * detailProcess.size() == 0 ||
								 * detailProcess.get
								 * (0).getOperation().intValue() ==
								 * WorkflowConstants.Status.Published
								 * .getCode()) continue;
								 * 
								 * WorkflowDetailProcess approvedDetailToRenew =
								 * detailProcess .get(0); WorkflowDetailProcess
								 * waitingForPublishDetailToRenew =
								 * detailProcess .get(1);
								 * 
								 * // get and re-enable all messages against the
								 * waiting for // publish notification detail
								 * process List<Message>
								 * messagesForLastPublishWaitDetailProcess =
								 * messageService
								 * .getMessageByWorkflowDetailProcess
								 * (waitingForPublishDetailToRenew);
								 * 
								 * WorkflowDetailProcess renewedPublishDetail =
								 * new WorkflowDetailProcess();
								 * renewedPublishDetail.setCreatedDate(new
								 * Date()); renewedPublishDetail
								 * .setWorkflowDetail
								 * (waitingForPublishDetailToRenew
								 * .getWorkflowDetail()); renewedPublishDetail
								 * .setCreatedBy(waitingForPublishDetailToRenew
								 * .getCreatedBy()); renewedPublishDetail
								 * .setCreatedFor(waitingForPublishDetailToRenew
								 * .getCreatedFor()); renewedPublishDetail
								 * .setOperation(waitingForPublishDetailToRenew
								 * .getOperation());
								 * renewedPublishDetail.setRecordId
								 * (waitingForPublishDetailToRenew
								 * .getRecordId()); renewedPublishDetail
								 * .setWorkflowId(waitingForPublishDetailToRenew
								 * .getWorkflowId());
								 * 
								 * workflowService.saveWorkflowDetailProcess(
								 * renewedPublishDetail);
								 * 
								 * for (Message msg :
								 * messagesForLastPublishWaitDetailProcess) {
								 * 
								 * Message renewedMessage = new Message();
								 * renewedMessage.setStatus((byte) 0);
								 * renewedMessage.setCreatedDate(new Date());
								 * renewedMessage
								 * .setDataSpecific(msg.getDataSpecific());
								 * renewedMessage.setIsAlert(msg.getIsAlert());
								 * renewedMessage
								 * .setIsEscalated(msg.getIsEscalated());
								 * renewedMessage.setMessage(msg.getMessage());
								 * renewedMessage
								 * .setMobileEmailId(msg.getMobileEmailId());
								 * renewedMessage
								 * .setPersonId(msg.getPersonId());
								 * renewedMessage
								 * .setProcessType(msg.getProcessType());
								 * renewedMessage
								 * .setRecordId(msg.getRecordId());
								 * renewedMessage
								 * .setWorkflowDetailProcess(renewedPublishDetail
								 * );
								 * 
								 * messageService.saveMessage(renewedMessage); }
								 * 
								 * // get and re-enter all messages as new
								 * records against the // approved notification
								 * detail process List<Message>
								 * messagesForLastApprovedDetailProcessToRenew =
								 * messageService
								 * .getMessageByWorkflowDetailProcess
								 * (approvedDetailToRenew);
								 * 
								 * WorkflowDetailProcess renewedApproveDetail =
								 * new WorkflowDetailProcess();
								 * renewedApproveDetail.setCreatedDate(new
								 * Date());
								 * renewedApproveDetail.setWorkflowDetail
								 * (approvedDetailToRenew .getWorkflowDetail());
								 * renewedApproveDetail
								 * .setCreatedBy(approvedDetailToRenew
								 * .getCreatedBy());
								 * renewedApproveDetail.setCreatedFor
								 * (approvedDetailToRenew .getCreatedFor());
								 * renewedApproveDetail
								 * .setOperation(approvedDetailToRenew
								 * .getOperation());
								 * renewedApproveDetail.setRecordId
								 * (approvedDetailToRenew .getRecordId());
								 * renewedApproveDetail
								 * .setWorkflowId(approvedDetailToRenew
								 * .getWorkflowId());
								 * workflowService.saveWorkflowDetailProcess
								 * (renewedApproveDetail);
								 * 
								 * for (Message msg :
								 * messagesForLastApprovedDetailProcessToRenew)
								 * {
								 * 
								 * if (msg.getStatus().intValue() == 1) {
								 * 
								 * Message renewedMessage = new Message();
								 * renewedMessage.setStatus((byte) 0);
								 * renewedMessage.setCreatedDate(new Date());
								 * renewedMessage
								 * .setDataSpecific(msg.getDataSpecific());
								 * renewedMessage.setIsAlert(msg.getIsAlert());
								 * renewedMessage
								 * .setIsEscalated(msg.getIsEscalated());
								 * renewedMessage.setMessage(msg.getMessage());
								 * renewedMessage
								 * .setMobileEmailId(msg.getMobileEmailId());
								 * renewedMessage
								 * .setPersonId(msg.getPersonId());
								 * renewedMessage
								 * .setProcessType(msg.getProcessType());
								 * renewedMessage
								 * .setRecordId(msg.getRecordId());
								 * renewedMessage
								 * .setWorkflowDetailProcess(renewedApproveDetail
								 * );
								 * 
								 * messageService.saveMessage(renewedMessage);
								 * 
								 * } else if (msg.getStatus().intValue() == 0) {
								 * 
								 * // disable the delete request related
								 * messages msg.setStatus((byte) 1);
								 * messageService.saveMessage(msg); }
								 * 
								 * } }
								 */
		}
		// else the record is in approved or published state, and this is a
		// request for add delete approval (so, add notifications for concerned
		// person to get delete approval)
		else {
			try {/*
				 * // Set isApprove Status as delete approval
				 * workflowService.setRecordStatusAsDeleteOrWaitingForDelete(
				 * entity, recordId,
				 * WorkflowConstants.Status.DeleteApproval.getCode());
				 * 
				 * // disable any last messages/notifications being shown for
				 * this // record (current active messages may be active like:
				 * data is // approved, waiting for publish or data is
				 * published) List<Message> activeMessages = messageService
				 * .getMessagesForRecordBasedOnModuleProcessAndStatus(
				 * lastProcessAgainstTargetRecord.getRecordId(),
				 * targetModuleProcess, new Byte("0"));
				 * 
				 * if (activeMessages != null && activeMessages.size() > 0) {
				 * 
				 * for (Message message : activeMessages) {
				 * 
				 * message.setStatus(new Byte("1")); message.setIsAlert(false);
				 * } messageService.saveAllMessage(activeMessages); }
				 * 
				 * // insert another process record to issue //
				 * "delete request read only" // message for target item
				 * WorkflowDetailProcess deleteDetailProcessForDeleter = new
				 * WorkflowDetailProcess(); deleteDetailProcessForDeleter
				 * .setWorkflowId(workflowToExecuteForDelete .getWorkflowId());
				 * deleteDetailProcessForDeleter .setOperation(Byte
				 * .parseByte(WorkflowConstants.Status.DeleteApprovalReadOnly
				 * .getCode() + ""));
				 * deleteDetailProcessForDeleter.setCreatedBy(currentPerson);
				 * deleteDetailProcessForDeleter.setCreatedDate(new Date());
				 * deleteDetailProcessForDeleter.setRecordId(recordId);
				 * 
				 * // find and set deletion (request to delete - read only
				 * message) // work flow detail for (WorkflowDetail
				 * workflowDetail : workflowToExecuteForDelete
				 * .getWorkflowDetails()) {
				 * 
				 * if (workflowDetail.getOperation().intValue() ==
				 * WorkflowConstants.Status.DeleteApprovalReadOnly .getCode()) {
				 * 
				 * deleteDetailProcessForDeleter
				 * .setWorkflowDetail(workflowDetail); break; } }
				 * 
				 * workflowService
				 * .saveWorkflowDetailProcess(deleteDetailProcessForDeleter);
				 * 
				 * // insert process record to issue delete request for target
				 * item WorkflowDetailProcess
				 * deleteDetailProcessForDeleteApprover = new
				 * WorkflowDetailProcess(); deleteDetailProcessForDeleteApprover
				 * .setWorkflowId(workflowToExecuteForDelete .getWorkflowId());
				 * deleteDetailProcessForDeleteApprover.setOperation(Byte
				 * .parseByte(WorkflowConstants.Status.DeleteApproval .getCode()
				 * + "")); deleteDetailProcessForDeleteApprover
				 * .setCreatedBy(lastProcessExecutor);
				 * deleteDetailProcessForDeleteApprover.setCreatedDate(new
				 * Date());
				 * deleteDetailProcessForDeleteApprover.setRecordId(recordId);
				 * 
				 * // find and set deletion (just request to delete) work flow
				 * // detail for (WorkflowDetail workflowDetail :
				 * workflowToExecuteForDelete .getWorkflowDetails()) {
				 * 
				 * if (workflowDetail.getOperation().intValue() ==
				 * WorkflowConstants.Status.DeleteApproval .getCode()) {
				 * 
				 * deleteDetailProcessForDeleteApprover
				 * .setWorkflowDetail(workflowDetail); break; } }
				 * 
				 * workflowService
				 * .saveWorkflowDetailProcess(deleteDetailProcessForDeleteApprover
				 * );
				 * 
				 * // add new message for "request for deletion" to the last //
				 * process // executor (approver or publisher) - this message
				 * will be used // for // actually deleting the record on delete
				 * approval pop-up
				 * messageService.saveMessageAgainstworkflowDeleteProcess(
				 * deleteDetailProcessForDeleteApprover.getCreatedBy(),
				 * targetModuleProcess.getProcessTitle(),
				 * WorkflowConstants.Status.DeleteApproval.getCode(),
				 * deleteDetailProcessForDeleteApprover);
				 * 
				 * // add new messages to notify of this deletion approval
				 * request // to // current user (i.e. who has initiated the
				 * deletion request) - // just // for mark as read option
				 * messageService.saveMessageAgainstworkflowDeleteProcess(
				 * deleteDetailProcessForDeleter.getCreatedBy(),
				 * targetModuleProcess.getProcessTitle(),
				 * WorkflowConstants.Status.DeleteApprovalReadOnly .getCode(),
				 * deleteDetailProcessForDeleter); returnMessage = "APPROVAL";
				 */
			} catch (Exception e) {
				// Set return status as send for delete approval request
				returnMessage = "ERROR";
			}
		}

		return returnMessage;
	}

	/**
	 * @author: Khawaja Hasham Purpose: This method perform all actions(approve,
	 *          reject and publish) which user selects.
	 * 
	 * @param: workflow detail id: To represent the workflow detail against
	 *         which operation is going to be performed
	 * 
	 * @param comment
	 *            : In case of rejection, user comment will be saved. otherwise
	 *            this param will be null. added by Shoaib Ahmad Gondal @
	 *            03-Oct-12
	 * 
	 *            processFlag : This param represents the operation which is to
	 *            be performed
	 * 
	 *            voList: This param represents the list of workflow details in
	 *            which operation is to be performed
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public WorkflowVO handleUserActionAgainstNotification(
			long workflowDetailId, long recordId, Byte processFlag,
			List<WorkflowDetailVO> volist, Comment comment, User user)
			throws Exception {
		int returnStatus = WorkflowConstants.ReturnStatus.Error.getCode();
		WorkflowVO returnVO = new WorkflowVO();
		String useCase = null;

		try {
			WorkflowDetailProcess workflowDetailProcess = new WorkflowDetailProcess();
			WorkflowDetail previousWorkflowDetail = null;
			WorkflowDetail workflowDetail = null;
			WorkflowDetail nextLevelApprovalWorkflowDetail = null;
			WorkflowDetail nextLevelWorkflowDetail = null;
			List<WorkflowDetail> workflowDetailList = null;

			for (WorkflowDetailVO workflowDetailVO : volist) {

				if (workflowDetailVO.getWorkflowDetailId() == workflowDetailId) {
					useCase = workflowDetailVO.getWorkflow().getModuleProcess()
							.getUseCase();
					// Set workflow process area such as add, edit or delete
					returnVO.setProcessType(workflowDetailVO
							.getWorkflowDetailProcess().getProcessType());

					// Update Workflow Detail process by processed person
					workflowDetailVO.getWorkflowDetailProcess().setCreatedBy(
							user.getPersonId());
					workflowService.saveWorkflowDetailProcess(workflowDetailVO
							.getWorkflowDetailProcess());

					workflowDetailVO.setProcessType(workflowDetailVO
							.getWorkflowDetailProcess().getProcessType());
					// get the entry workflow detail
					workflowDetail = workflowDetailVO
							.getWorkflowDetailProcess().getWorkflowDetail();
					workflowDetailList = new ArrayList<WorkflowDetail>(
							workflowService
									.getWorkflowDetailsByWorkflowId(workflowDetail
											.getWorkflow().getWorkflowId()));
					Collections.sort(workflowDetailList,
							new Comparator<WorkflowDetail>() {
								public int compare(WorkflowDetail m1,
										WorkflowDetail m2) {
									return m1.getOperationOrder().compareTo(
											m2.getOperationOrder());
								}
							});

					// Find is there any next level approval
					for (WorkflowDetail workflowDeat2 : workflowDetailList) {
						if (workflowDeat2.getOperationOrder() > workflowDetail
								.getOperationOrder()
								&& (byte) workflowDeat2.getOperationMaster()
										.getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Approval
										.getCode()) {
							if (workflowService.quantumRuleValidationCheck(
									recordId, workflowDeat2)) {
								nextLevelApprovalWorkflowDetail = new WorkflowDetail();
								nextLevelApprovalWorkflowDetail = (workflowDeat2);
							}
							break;
						}
					}

					// find the next level detail information
					for (WorkflowDetail workflowDeat2 : workflowDetailList) {
						if (workflowDeat2.getOperationOrder() > workflowDetail
								.getOperationOrder()
								&& (byte) workflowDeat2.getOperationMaster()
										.getOperationGroup() != (byte) WorkflowConstants.OperationGroup.Approval
										.getCode()) {
							nextLevelWorkflowDetail = new WorkflowDetail();

							nextLevelWorkflowDetail = (workflowDeat2);
							break;
						}
					}

					// in case of reject, set WorkflowDetailProcess in
					// Comment and save comment
					if ((processFlag == WorkflowConstants.Status.Deny.getCode())
							&& (comment != null)) {
						// Forward Record
						workflowDetailVO
								.setNextWorkflowDetail(workflowDetailList
										.get(0));
						workflowDetailProcess = new WorkflowDetailProcess();
						workflowDetailProcess
								.setWorkflowDetail(workflowDetailList.get(0));
						workflowDetailProcess.setScreen(workflowDetail
								.getScreenByRejectScreen());
						workflowDetailProcess.setProcessType(workflowDetailVO
								.getProcessType());
						workflowDetailProcess
								.setOperation(getOperationByAnalyse(
										processFlag,
										workflowDetailVO.getProcessType(),
										"REVERSE"));

						workflowDetailProcess.setCreatedDate(new Date());
						workflowDetailProcess.setRecordId(recordId);
						workflowDetailProcess.setWorkflowId(workflowDetailVO
								.getWorkflow().getWorkflowId());
						workflowDetailProcess.setCreatedBy(user.getPersonId());

						workflowService
								.saveWorkflowDetailProcess(workflowDetailProcess);

						messageService.saveMessageAgainstworkflowProcess(
								workflowDetailProcess, workflowDetailVO
										.getWorkflow().getModuleProcess()
										.getUseCase(), null, workflowDetailVO);

						comment.setWorkflowDetailProcess(workflowDetailProcess);
						messageService.saveComment(comment);
						if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Add
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.AddRejected
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Modify
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.ModificationRejected
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Delete
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.DeleteRejected
									.getCode();
						else
							returnStatus = WorkflowConstants.ReturnStatus.Success
									.getCode();
						// If there is any approval
					} else if (nextLevelApprovalWorkflowDetail != null) {

						workflowDetailVO.setNextWorkflowDetail(workflowDetail);

						// Self Notification
						workflowDetailProcess = new WorkflowDetailProcess();
						workflowDetailProcess.setWorkflowDetail(workflowDetail);
						workflowDetailProcess.setRecordId(recordId);
						workflowDetailProcess.setCreatedDate(new Date());
						workflowDetailProcess.setCreatedBy(user.getPersonId());
						workflowDetailProcess.setWorkflowId(workflowDetailVO
								.getWorkflow().getWorkflowId());
						workflowDetailProcess.setScreen(workflowDetail
								.getScreenByScreenId());
						workflowDetailProcess.setProcessType(workflowDetailVO
								.getProcessType());
						workflowDetailProcess
								.setOperation(getOperationByAnalyse(
										processFlag,
										workflowDetailVO.getProcessType(),
										"CURRENT"));
						workflowService
								.saveWorkflowDetailProcess(workflowDetailProcess);
						messageService.saveMessageAgainstworkflowProcess(
								workflowDetailProcess, workflowDetailVO
										.getWorkflow().getModuleProcess()
										.getUseCase(), user.getPersonId(),
								workflowDetailVO);

						// Forward Record
						workflowDetailVO
								.setNextWorkflowDetail(nextLevelApprovalWorkflowDetail);
						workflowDetailProcess = new WorkflowDetailProcess();
						workflowDetailProcess
								.setWorkflowDetail(nextLevelApprovalWorkflowDetail);
						workflowDetailProcess
								.setScreen(nextLevelApprovalWorkflowDetail
										.getScreenByScreenId());
						workflowDetailProcess.setProcessType(workflowDetailVO
								.getProcessType());
						workflowDetailProcess
								.setOperation(getOperationByAnalyse(
										processFlag,
										workflowDetailVO.getProcessType(),
										"FARWARD"));

						workflowDetailProcess.setCreatedDate(new Date());
						workflowDetailProcess.setRecordId(recordId);
						workflowDetailProcess.setWorkflowId(workflowDetailVO
								.getWorkflow().getWorkflowId());
						workflowDetailProcess.setCreatedBy(user.getPersonId());

						workflowService
								.saveWorkflowDetailProcess(workflowDetailProcess);

						messageService.saveMessageAgainstworkflowProcess(
								workflowDetailProcess, workflowDetailVO
										.getWorkflow().getModuleProcess()
										.getUseCase(), null, workflowDetailVO);
						if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Add
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.Added
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Modify
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.Modified
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Delete
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.Delete
									.getCode();
						else
							returnStatus = WorkflowConstants.ReturnStatus.Success
									.getCode();
						// if there is no approval
					} else {
						workflowDetailVO.setNextWorkflowDetail(workflowDetail);
						// Get processed detail process against workflow
						List<Byte> excludeOperation = new ArrayList<Byte>();
						excludeOperation
								.add((byte) WorkflowConstants.Status.Saved
										.getCode());
						List<WorkflowDetailProcess> workflowDetailProcesses = workflowService
								.getDetailProcessForWorkflow(
										workflowDetail.getWorkflow(),
										excludeOperation, recordId,
										workflowDetailVO.getProcessType());

						// Self Notification
						workflowDetailProcess = new WorkflowDetailProcess();
						workflowDetailProcess.setWorkflowDetail(workflowDetail);
						workflowDetailProcess.setScreen(workflowDetail
								.getScreenByScreenId());
						workflowDetailProcess.setProcessType(workflowDetailVO
								.getProcessType());
						workflowDetailProcess
								.setOperation(getOperationByAnalyse(
										processFlag,
										workflowDetailVO.getProcessType(),
										"CURRENT"));

						workflowDetailProcess.setCreatedDate(new Date());
						workflowDetailProcess.setRecordId(recordId);
						workflowDetailProcess.setWorkflowId(workflowDetailVO
								.getWorkflow().getWorkflowId());
						workflowDetailProcess.setCreatedBy(user.getPersonId());
						workflowService
								.saveWorkflowDetailProcess(workflowDetailProcess);
						messageService.saveMessageAgainstworkflowProcess(
								workflowDetailProcess, workflowDetailVO
										.getWorkflow().getModuleProcess()
										.getUseCase(), user.getPersonId(),
								workflowDetailVO);

						// Reverse to entry and approval person
						for (WorkflowDetailProcess workflowDetailProcessTemp : workflowDetailProcesses) {
							//This is flag is updated to avoid sending the sms & email
							workflowDetailVO.setInform(true);
							messageService.saveMessageAgainstworkflowProcess(
									workflowDetailProcess, workflowDetailVO
											.getWorkflow().getModuleProcess()
											.getUseCase(),
									workflowDetailProcessTemp.getCreatedBy(),
									workflowDetailVO);
							//above changes reverted  avoid sending the sms & email
							workflowDetailVO.setInform(false);
						}

						if (nextLevelWorkflowDetail != null) {
							workflowDetailVO
									.setNextWorkflowDetail(nextLevelWorkflowDetail);

							// Forward Record
							workflowDetailProcess = new WorkflowDetailProcess();
							workflowDetailProcess
									.setWorkflowDetail(nextLevelWorkflowDetail);
							workflowDetailProcess.setRecordId(recordId);
							workflowDetailProcess.setCreatedDate(new Date());
							workflowDetailProcess.setCreatedBy(user
									.getPersonId());
							workflowDetailProcess
									.setWorkflowId(workflowDetailVO
											.getWorkflow().getWorkflowId());
							workflowDetailProcess
									.setScreen(nextLevelWorkflowDetail
											.getScreenByScreenId());
							workflowDetailProcess
									.setProcessType(workflowDetailVO
											.getProcessType());
							workflowDetailProcess
									.setOperation(getOperationByAnalyse(
											(byte) WorkflowConstants.Status.Post
													.getCode(),
											workflowDetailVO.getProcessType(),
											"FARWARD"));
							workflowService
									.saveWorkflowDetailProcess(workflowDetailProcess);

							messageService.saveMessageAgainstworkflowProcess(
									workflowDetailProcess, workflowDetailVO
											.getWorkflow().getModuleProcess()
											.getUseCase(), null,
									workflowDetailVO);
						}

						if ((byte) processFlag != (byte) WorkflowConstants.Status.Posted
								.getCode())
							doDirectApprovalAndDeleteProcess(
									workflowDetailVO.getWorkflow()
											.getModuleProcess().getUseCase(),
									recordId,
									getOperationByAnalyse(processFlag,
											workflowDetailVO.getProcessType(),
											"DIRECT"));
						if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Add
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.AddApproved
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Modify
								.getCode())
							returnStatus = WorkflowConstants.ReturnStatus.ModificationApproved
									.getCode();
						else if (workflowDetailVO.getProcessType() == WorkflowConstants.ProcessMethod.Delete
								.getCode()) {
							returnStatus = WorkflowConstants.ReturnStatus.DeleteApproved
									.getCode();
							// Delete all non processed messages from message
							// table
							List<Message> needsToInactive = messageService
									.getUnApprovedMessageAgainstWorkflow(
											workflowDetailVO.getWorkflow()
													.getWorkflowId(), recordId);
							for (Message message : needsToInactive) {
								message.setStatus((byte) 1);
								messageService.saveMessage(message);
							}
						} else
							returnStatus = WorkflowConstants.ReturnStatus.Success
									.getCode();
					}

					Message message = new Message();
					// message.setPersonId(user.getPerson().getPersonId());

					message.setWorkflowDetailProcess(workflowDetailVO
							.getWorkflowDetailProcess());

					// Get the message of current notification and set
					// status to
					// 1 to mark that operation have been performed against
					// this
					// notification
					List<Message> messages = messageService
							.getMessageOfProcess(message);
					if (messages.size() > 0) {
						for (Message message2 : messages) {
							// set status and save message
							message2.setStatus((byte) 1);
							messageService.saveMessage(message2);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		returnVO.setReturnStatus(returnStatus);
		returnVO.setReturnStatusName(WorkflowConstants.ReturnStatus.get(
				returnStatus).name());

		// ******************************************Business
		// Call************************************************

		// find business call need and set operation mode(Add,Edit & Delete)
		WorkflowDetailVO workfowDetailVO = new WorkflowDetailVO();
		boolean businessCallFlag = false;
		if (returnVO.getReturnStatusName().equals(
				WorkflowConstants.ReturnStatus.AddApproved.name())) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(false);
			workfowDetailVO.setDeleteFlag(false);
		} else if (returnVO.getReturnStatusName().equals(
				WorkflowConstants.ReturnStatus.ModificationApproved.name())) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(true);
			workfowDetailVO.setDeleteFlag(false);
		} else if (returnVO.getReturnStatusName().equals(
				WorkflowConstants.ReturnStatus.DeleteApproved.name())) {
			businessCallFlag = true;
			workfowDetailVO.setEditFlag(false);
			workfowDetailVO.setDeleteFlag(true);
		}
		// Call business method
		if (businessCallFlag) {
			workfowDetailVO.setRecordId(recordId);
			workfowDetailVO.setScreenName(useCase);
			buisnessCall(workfowDetailVO);
		}
		return returnVO;
	}

	public void buisnessCall(WorkflowDetailVO workfowDetailVO) throws Exception {
		String nameTemp = workfowDetailVO.getScreenName().replace(
				".domain.entity.", ".DOMAIN.ENTITY.");
		String[] name = nameTemp.split(".DOMAIN.ENTITY.");
		// Some times use case name is appended by _javassist, so
		// get
		// proper use case name
		if (name[name.length - 1].contains("_$$_javassist")) {
			name[name.length - 1] = name[name.length - 1].substring(0,
					name[name.length - 1].indexOf("_$$_javassist"));
		}
		Object bLObject = WorkflowUtils.getEntityBL(name[name.length - 1]);

		String methodId = "do" + name[name.length - 1] + "BusinessUpdate";
		Class[] argTypes = new Class[] { Long.class, WorkflowDetailVO.class };
		Method method = bLObject.getClass().getMethod(methodId, argTypes);
		method.invoke(bLObject, workfowDetailVO.getRecordId(), workfowDetailVO);
	}

	public void doDirectApprovalAndDeleteProcess(String useCase, Long recordId,
			Byte status) {
		try {
			// Fetch actual record from process's table
			Object recordObject = this.getWorkflowService().getRecord(useCase,
					recordId);
			// Get the use case name
			/*
			 * String[] name = workflowDetailVO.getObject().getClass()
			 * .getName().split("entity.");
			 */
			String nameTemp = recordObject.getClass().getName()
					.replace(".domain.entity.", ".DOMAIN.ENTITY.");
			String[] name = nameTemp.split(".DOMAIN.ENTITY.");
			// Some times use case name is appended by _javassist, so
			// get
			// proper use case name
			if (name[name.length - 1].contains("_$$_javassist")) {
				name[name.length - 1] = name[name.length - 1].substring(0,
						name[name.length - 1].indexOf("_$$_javassist"));
			}
			// getter method of primary key
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = recordObject.getClass().getMethod(methodId, null);
			Object tempRecordidObject = method.invoke(recordObject, null);
			long temprecordId = 0;
			if (tempRecordidObject instanceof Long) {
				temprecordId = ((Long) tempRecordidObject);
			} else if (tempRecordidObject instanceof Integer) {
				temprecordId = Long.parseLong(tempRecordidObject.toString());
			}
			Long relativeId = null;
			try {
				Method method2 = recordObject.getClass().getMethod(
						"getRelativeId", null);

				relativeId = (Long) method2.invoke(recordObject);
			} catch (java.lang.NoSuchMethodException e) {
				// e.printStackTrace();
			}

			if (temprecordId == recordId) {
				String approvalflagmethodId = "setIsApprove";
				Class[] argTypes = new Class[] { Byte.class };
				Method approvalmethod = recordObject.getClass().getMethod(
						approvalflagmethodId, argTypes);
				approvalmethod.invoke(recordObject, status);

				// set the "is_approve value to represent the state of
				// record (approved, rejected, publish etc.)
				updateApprovalStatus(recordObject, useCase, relativeId, status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Byte getOperationByAnalyse(byte statusFlag, byte processType,
			String requestedSection) {
		byte returnStatus = (byte) WorkflowConstants.Status.NoDecession
				.getCode();

		// add & modify AND approved AND current=Approved
		if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& requestedSection.equalsIgnoreCase("CURRENT"))
			returnStatus = (byte) WorkflowConstants.Status.Approved.getCode();

		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& (statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode() || statusFlag == (byte) WorkflowConstants.Status.Saved
						.getCode())
				&& requestedSection.equalsIgnoreCase("DIRECT"))
			returnStatus = (byte) WorkflowConstants.Status.Published.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Posted
						.getCode()
				&& requestedSection.equalsIgnoreCase("CURRENT"))
			returnStatus = (byte) WorkflowConstants.Status.Posted.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& requestedSection.equalsIgnoreCase("FARWARD"))
			returnStatus = (byte) WorkflowConstants.Status.Saved.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& requestedSection.equalsIgnoreCase("REVERSE"))
			returnStatus = (byte) WorkflowConstants.Status.Approved.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Post.getCode()
				&& requestedSection.equalsIgnoreCase("FARWARD"))
			returnStatus = (byte) WorkflowConstants.Status.Post.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Deny.getCode()
				&& requestedSection.equalsIgnoreCase("REVERSE"))
			returnStatus = (byte) WorkflowConstants.Status.Deny.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& (requestedSection.equalsIgnoreCase("REVERSE") || requestedSection
						.equalsIgnoreCase("DIRECT")))
			returnStatus = (byte) WorkflowConstants.Status.Deleted.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& (requestedSection.equalsIgnoreCase("CURRENT")))
			returnStatus = (byte) WorkflowConstants.Status.Approved.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Approved
						.getCode()
				&& (requestedSection.equalsIgnoreCase("REVERSE")
						|| requestedSection.equalsIgnoreCase("CURRENT") || requestedSection
						.equalsIgnoreCase("FARWARD")))
			returnStatus = (byte) WorkflowConstants.Status.Saved.getCode();

		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Deny.getCode())
			returnStatus = (byte) WorkflowConstants.Status.DeleteRejected
					.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Post.getCode()
				&& requestedSection.equalsIgnoreCase("FARWARD"))
			returnStatus = (byte) WorkflowConstants.Status.Post.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Posted
						.getCode()
				&& requestedSection.equalsIgnoreCase("CURRENT"))
			returnStatus = (byte) WorkflowConstants.Status.Posted.getCode();
		else if ((processType == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode() || processType == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode())
				&& statusFlag == (byte) WorkflowConstants.Status.Published
						.getCode()
				&& (requestedSection.equalsIgnoreCase("CURRENT") || requestedSection
						.equalsIgnoreCase("FARWARD")))
			returnStatus = (byte) WorkflowConstants.Status.Published.getCode();
		return returnStatus;
	}

	/**
	 * This Method update the is_approved value of given object's table
	 * 
	 * @param module
	 *            : name of module to which object belongs to
	 * 
	 * @param Object
	 *            : will represent the record of a table whose is_approve value
	 *            is to be updated
	 * 
	 * @param status
	 *            : value of is_approved column that is to be updated
	 * 
	 * @author Khawaja Hasham
	 */
	private void updateApprovalStatus(Object entityObject,
			String entityClassFullyQualifiedName, Long countryId, Byte status) {
		try {
			String entityClassName = WorkflowUtils
					.getEntityClassName(entityClassFullyQualifiedName);

			AIOTechGenericDAO<Object> dao = WorkflowUtils
					.getEntityDAO(entityClassFullyQualifiedName);

			// in case of publish operation
			if (status.byteValue() == WorkflowConstants.Status.Published
					.getCode()) {

				Object exampleObject = WorkflowUtils
						.createNewEntityObject(entityClassFullyQualifiedName);

				// set relative id
				Method method = null;
				try {
					Class[] argTypes = new Class[] { Long.class };
					method = exampleObject.getClass().getMethod(
							"setRelativeId", argTypes);
					method.invoke(exampleObject, countryId);
				} catch (java.lang.NoSuchMethodException e) {

				}
				// set publish date
				/*
				 * Class[] argTypesUpdatedDate = new Class[] { Date.class };
				 * method = entityObject.getClass().getMethod("setUpdatedDate",
				 * argTypesUpdatedDate); method.invoke(exampleObject, new
				 * Date());
				 */

				// publish record, if there is one record, update it else if
				// there
				// are two records then set the previous published record,s is
				// approved to 'old published' and publish the new record

				// Commented by Rafiq to skip the record loading of full table
				// instead of particular relative record
				/*
				 * List<Object> list = dao.findByExample(exampleObject); for
				 * (Object object : new ArrayList<Object>(list)) { method =
				 * object.getClass().getMethod( "getRelativeId", null); Long
				 * relativeId = (Long) method.invoke(object);
				 * list.remove(object);
				 * 
				 * } if (list.size() < 2) { dao.merge(entityObject); } else if
				 * (list.size() == 2) {
				 * 
				 * dao.getHibernateSession().evict(entityObject);
				 * 
				 * Class[] argTypes2 = new Class[] { Byte.class }; method =
				 * entityObject.getClass().getMethod("setIsApprove", argTypes2);
				 * method.invoke(entityObject, (byte)
				 * WorkflowConstants.Status.OldPublished .getCode());
				 * 
				 * // set publish date try { Class[] argTypesUpdatedDate = new
				 * Class[] { Date.class }; method =
				 * entityObject.getClass().getMethod( "setUpdatedDate",
				 * argTypesUpdatedDate); method.invoke(entityObject, new
				 * Date()); dao.merge(entityObject); } catch
				 * (java.lang.NoSuchMethodException e) {
				 * 
				 * } Object first = list.get(0);
				 * 
				 * method = first.getClass().getMethod( "get" + entityClassName
				 * + "Id", null); Object firstId = null; Class[] argTypes1 =
				 * null; if (method.getReturnType().getName()
				 * .equals("java.lang.Long")) { firstId = method.invoke(first);
				 * argTypes1 = new Class[] { Long.class }; } else if
				 * (method.getReturnType().getName()
				 * .equals("java.lang.Integer")) { firstId =
				 * method.invoke(first); argTypes1 = new Class[] { Integer.class
				 * }; }
				 * 
				 * method = entityObject.getClass().getMethod( "set" +
				 * entityClassName + "Id", argTypes1);
				 * method.invoke(entityObject, firstId);
				 * 
				 * method = entityObject.getClass().getMethod("setIsApprove",
				 * argTypes2); Byte publishedByte = (byte)
				 * WorkflowConstants.Status.Published .getCode();
				 * method.invoke(entityObject, publishedByte);
				 * dao.merge(entityObject); } else { dao.merge(entityObject); }
				 */
				dao.merge(entityObject);
			} else {
				dao.merge(entityObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public User getUser(String id) {
		return userRoleService.getUser(id);
	}

	public void disableUserForPerson(Long personId) throws Exception {

		User userToDisable = userRoleService.getUserByPersonId(personId).get(0);
		/* userToDisable.setPerson(disabledPerson); */
		userToDisable.setPersonId(personId);
		userToDisable.setIsRegistered(false);
		userToDisable.setIsActive(false);
		userRoleService.saveOrUpdateUser(userToDisable);
	}

	public void updateUserRegistration(User user, Boolean status) {

		try {
			user.setIsRegistered(status);
			userRoleService.saveUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean addNewUsers(List<String> newUsers) throws Exception {

		User user;
		List<User> users = new ArrayList<User>();
		/*
		 * Implementation implementation =
		 * userRoleService.getImplementation(1L);
		 */

		try {
			for (String newPerson : newUsers) {
				user = new User();
				user.setUsername(newPerson);
				user.setEnabled(true);
				user.setPassword("dummy");
				user.setIsActive(false);
				user.setIsRegistered(false);
				/*
				 * user.setPerson(newPerson);
				 * user.setImplementation(implementation);
				 */
				users.add(user);
			}
			userRoleService.saveOrUpdateAllUser(users);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<?> genericDeletedRecordsFilter(List<Object> objects) {
		if (objects != null)
			;
		List<Object> tempObjects = new ArrayList<Object>(objects);
		for (Object object : objects) {
			// Check the record is in delete or waiting for delete approval
			if (!isValidRecord(object))
				tempObjects.remove(object);

		}

		return tempObjects;
	}

	public Boolean isValidRecord(Object object) {
		boolean flag = true;
		try {
			if (object != null) {
				Method method = object.getClass().getMethod("getIsApprove");
				Integer fieldValue = Integer.valueOf((Byte) method
						.invoke(object));
				if (fieldValue == (WorkflowConstants.Status.DeleteApproval
						.getCode())
						|| (fieldValue == WorkflowConstants.Status.Deleted
								.getCode())
						|| (fieldValue == WorkflowConstants.Status.DeleteApprovalReadOnly
								.getCode()))
					flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	public String checkRecordHasAnyChildRecord(String useCase, Long recordId) {
		String returnStatus = "SUCCESS";
		try {
			String chileUseCase = null;
			// Get the child entity name by passing parent entity name
			try {
				chileUseCase = ConstantsUtil.Business.ChildRecord.get(useCase)
						.name();
			} catch (Exception e) {
				return returnStatus;
			}

			// Fetch child records by using parent record id and entity name
			List<Object> childRecords = (List<Object>) workflowService
					.getChildRecordsByParent(useCase, recordId, chileUseCase);
			if (childRecords != null && childRecords.size() > 0) {
				for (Object object : childRecords) {
					Method method = object.getClass().getMethod("getIsApprove");
					Integer fieldValue = Integer.valueOf((Byte) method
							.invoke(object));
					// Return the child use case name if record isApprove status
					// is other than that deleted
					if (fieldValue != WorkflowConstants.Status.Deleted
							.getCode())
						return chileUseCase;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnStatus;
	}

	@SuppressWarnings("unchecked")
	public Long fetchLatestRecordIdFromUseCase(Long processId, Long recordId) {
		try {

			ModuleProcess moduleProcess = workflowService
					.getModuleProcessById(processId);
			// Fetch child records by using parent record id and entity name
			List<Object> records = null;

			records = (List<Object>) workflowService
					.fetchLatestRecordIdFromUseCase(
							moduleProcess.getProcessTitle(), recordId);
			if (records != null && records.size() > 1) {
				for (Object object : records) {
					Method method = object.getClass().getMethod(
							"get" + moduleProcess.getProcessTitle() + "Id");
					Long temprecordId = (Long) method.invoke(object, null);
					method = object.getClass().getMethod("getIsApprove");
					Byte isApprove = (Byte) method.invoke(object, null);
					if ((long) temprecordId != (long) recordId
							&& (byte) isApprove == ((byte) WorkflowConstants.Status.OldPublished
									.getCode())) {
						recordId = temprecordId;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordId;
	}

	@SuppressWarnings("unchecked")
	public List<Quantum> quantmRuleXMLParsing() {
		Quantums quantums = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Quantums.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			quantums = (Quantums) jaxbUnmarshaller
					.unmarshal(HandleUserActionBL.class
							.getResourceAsStream("/META-INF/quantums.xml"));
			System.out.println(quantums);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return quantums.getQuantums();
	}

	public Quantum quantumRule(String screen) {
		List<Quantum> list = quantmRuleXMLParsing();
		Quantum quantum = null;
		if (list != null && list.size() > 0) {
			for (Quantum quantu : list) {
				if (quantu.getScreen().equalsIgnoreCase(screen)) {
					quantum = quantu;
					break;
				}
			}
		}
		return quantum;
	}

	public List<User> getAllUsers() throws Exception {
		return userRoleService.getAllUsers();
	}

	public void saveOrUpdateUser(User user) throws Exception {
		userRoleService.saveOrUpdateUser(user);
	}

	// =============Getters Setters==================

	public WorkflowService getWorkflowService() {
		return workflowService;
	}

	@Autowired
	public void setWorkflowService(WorkflowService workflowService) {
		this.workflowService = workflowService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	@Autowired
	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	@Autowired
	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

}
