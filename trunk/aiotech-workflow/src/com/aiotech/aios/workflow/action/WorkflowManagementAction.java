/**
 * 
 */
package com.aiotech.aios.workflow.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.workflow.bl.HandleUserActionBL;
import com.aiotech.aios.workflow.bl.SaveWorkflowBL;
import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.OperationMaster;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.vo.UserVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowVO;
import com.aiotech.aios.workflow.service.WorkflowService;
import com.aiotech.aios.workflow.util.Quantum;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.opensymphony.xwork2.ActionSupport;

import flexjson.JSONSerializer;

/**
 * Fixes Required: Person Involvement is to be eliminated ProcessRoleArea
 * involvement is to be eliminated
 * 
 * @author Muhammad Haris
 */

@ParentPackage("json-default")
public class WorkflowManagementAction extends ActionSupport {

	protected static final Logger logger = Logger
			.getLogger(ActionSupport.class);

	private String processId = "-9999";
	protected String status = "";

	private List<ModuleProcess> moduleProcesses = new ArrayList<ModuleProcess>();
	private List<WorkflowDetailVO> recentWorkflows = new ArrayList<WorkflowDetailVO>();
	private List<OperationMaster> operations = new ArrayList<OperationMaster>();
	private Map<Byte, String> operationGroupList = new HashMap<Byte, String>();
	private List<WorkflowDetailVO> operationList = new ArrayList<WorkflowDetailVO>();

	protected String workflowVOsJSON = "";
	protected String rolesJSON = "";
	protected String screensJSON = "";
	protected String usersJSON = "";

	protected WorkflowService workflowService;
	private SaveWorkflowBL saveWorkflowBL;
	protected HandleUserActionBL handleUserActionBL;
	private List<Object> aaData;
	private OperationMaster operationMaster;
	private Quantum quantum;

	/* ======================== Constructor ================================ */
	@Autowired
	public WorkflowManagementAction(WorkflowService workflowService,
			HandleUserActionBL handleUserActionBL, SaveWorkflowBL saveWorkflowBL) {
		this.workflowService = workflowService;
		this.saveWorkflowBL = saveWorkflowBL;
		this.handleUserActionBL = handleUserActionBL;
	}

	/* ======================== Methods ================================ */
	@Override
	public String execute() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();

			if (request.getMethod().equals("GET")) { // initial request to load
														// page
				loadFormDetails();

			} else { // form submission
				saveWorkflow();
			}

			status = SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			status = ERROR;
		}

		return status;
	}

	public void loadFormDetails() throws Exception {
		moduleProcesses = workflowService.getAllModuleProcess();
		operationGroupList = fetchOperationGroupList();
		operationList = fetchOperationList(null);
		// saveWorkflowBL.defaultDeleteWorkflow(moduleProcesses);
		// persons = personService.getPersonByTypeId(3);
	}

	/**
	 * Gets call when user selects a process.
	 * <p>
	 * Date: 01-Apr-13
	 * 
	 * @return success or failure string
	 * 
	 * @author Shoaib Ahmad Gondal
	 */

	// @Action(value = "/onModuleProcessSelect", results = { @Result(name =
	// "success", type = "json", params = {}) })
	public String onModuleProcessSelect() {
		try {

			HttpServletRequest request = ServletActionContext.getRequest();
			Long selectedProcess = Long.parseLong(request
					.getParameter("processId"));
			ModuleProcess moduleProcess = null;

			if (selectedProcess > 0) {

				// Get already defined workflow details
				moduleProcess = workflowService
						.getModuleProcessById(selectedProcess);
				
				quantum=handleUserActionBL.quantumRule(moduleProcess.getProcessTitle());


				List<Workflow> workflows = workflowService
						.getWorkflowsOfProcess(moduleProcess);

				List<WorkflowVO> workflowVOs = new ArrayList<WorkflowVO>();
				for (Workflow w : workflows) {

					// w.setImplementationId(null);
					// w.setModuleProcess(null);

					w.setWorkflowDetails((Set<WorkflowDetail>) new HashSet<WorkflowDetail>(
							workflowService.getWorkflowDetailsByWorkflowId(w
									.getWorkflowId())));

					WorkflowVO workflowVO = new WorkflowVO(w);
					workflowVOs.add(workflowVO);
				}

				List<Role> roles = getRolesOfProcess(moduleProcess);

				JSONSerializer serializer = new JSONSerializer();

				serializer.include("workflowDetailVOs");
				serializer.exclude("moduleProcess",
						"workflowDetailVOs.workflow",
						"workflows",
						"workflowDetailVOs.screenByScreenId",
						"workflowDetailVOs.screenByRejectScreen",
						"workflowDetailVOs.operationMaster",
						"workflowDetailVOs.workflowDetails",
						"workflowDetailVOs.workflowDetailProcess");

				workflowVOsJSON = serializer.serialize(workflowVOs);

				serializer = new JSONSerializer();
				rolesJSON = serializer.serialize(roles);

				operationList = fetchOperationList(null);

				List<Screen> screens = handleUserActionBL.getWorkflowService()
						.getAllScreens();
				serializer = new JSONSerializer();
				serializer.exclude("isActive", "isHidden", "menus",
						"workflowDetails", "module");
				screensJSON = serializer.serialize(screens);

				List<User> users = getAllUsers();
				List<UserVO> usersVO=new ArrayList<UserVO>();
				UserVO userVo=null;
				for (User user : users) {
					if(user.getPersonId()==null || user.getPersonId()==0)
						continue;
					
					userVo=new UserVO();
					userVo.setUsername(user.getUsername());
					userVo.setPersonId(user.getPersonId());
					userVo.setDisplayUsername(user.getUsername().split("_")[0]);
					usersVO.add(userVo);
				}
				serializer = new JSONSerializer();
				serializer.exclude("isActive", "isRegistered", "userRoles");
				usersJSON = serializer.serialize(usersVO);
				
			}
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			return "failure";
		}
	}

	/**
	 * Operations.
	 * <p>
	 * Date: 01-Apr-14
	 * 
	 * @return success or failure string
	 * 
	 * @author Mohamed Rabeek
	 */

	public String saveWorkflowOperation() {
		try {

			operationMaster.setImplementationId(WorkflowUtils
					.getImplementationId());
			if (operationMaster.getModuleProcess() != null
					&& operationMaster.getModuleProcess().getProcessId() == null)
				operationMaster.setModuleProcess(null);

			workflowService.saveOperation(operationMaster);
			status = "SUCCESS";
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			status = "ERROR";
			return "failure";
		}
	}

	/**
	 * Operations.
	 * <p>
	 * Date: 01-Apr-14
	 * 
	 * @return success or failure string
	 * 
	 * @author Mohamed Rabeek
	 */

	public String deleteWorkflowOperation() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			Long operationMasterId = Long.parseLong(request
					.getParameter("operationMasterId"));

			OperationMaster opMaster = workflowService
					.getOperationById(operationMasterId);
			if (opMaster != null)
				workflowService.deleteOperation(opMaster);

			status = "SUCCESS";
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			status = "ERROR";
			return "failure";
		}
	}

	/**
	 * Operations.
	 * <p>
	 * Date: 01-Apr-14
	 * 
	 * @return success or failure string
	 * 
	 * @author Mohamed Rabeek
	 */

	public String reloadOperations() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			Long selectedProcess = null;
			if (request.getParameter("processId") != null
					&& !request.getParameter("processId").equals("")
					&& !request.getParameter("processId").equals("0"))
				selectedProcess = Long.parseLong(request
						.getParameter("processId"));

			operationList = fetchOperationList(selectedProcess);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			return "failure";
		}
	}
	
	/**
	 * Workflow.
	 * <p>
	 * Date: 01-Apr-14
	 * 
	 * @return success or failure string
	 * 
	 * @author Mohamed Rabeek
	 */

	public String deleteWorkflow() {
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			Long workflowId = Long.parseLong(request
					.getParameter("workflowId"));

			Workflow workflow = workflowService.getWorkflowDAO().findById(workflowId);
			if (workflow != null){
				workflow.setIsActive(false);
				workflowService.saveWorkflow(workflow);
			}

			status = "SUCCESS";
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception", e);
			status = "ERROR";
			return "failure";
		}
	}

	/**
	 * Contains generic code to fetch all roles.
	 * <p>
	 * For specific implementation override this method.
	 * 
	 * @author Shoaib Ahmad Gondal
	 * 
	 * @return
	 */
	protected List<Role> getRolesOfProcess(ModuleProcess moduleProcess)
			throws Exception {

		// fetch roles of selected role

		List<Role> roles = handleUserActionBL.getUserRoleService()
				.getAllRoles();

		/*
		 * List<ProcessRoleArea> processRoleAreas = systemService
		 * .getProcessRoleArea(moduleProcess.getProcessId());
		 */

		/*
		 * if ((processRoleAreas.size() > 0) &&
		 * !(processRoleAreas.get(0).getRoleArea().equals("Political"))) {
		 * List<Role> rolesAppend = systemService
		 * .getRolesByRoleAreaName(processRoleAreas.get(0) .getRoleArea());
		 * roles.addAll(rolesAppend); }
		 */

		return roles;
	}

	/**
	 * Contains generic code to fetch all users.
	 * <p>
	 * For specific implementation override this method.
	 * 
	 * @author Mohamed Rabeek
	 * 
	 * @return
	 */
	protected List<User> getAllUsers() throws Exception {

		List<User> userList = handleUserActionBL.getUserRoleService()
				.getAllUsersByImplementation(WorkflowUtils.getImplementationId());

		return userList;
	}

	protected void saveWorkflow() throws Exception {

		try {
			Long selectedWorkflowId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("selectedWorkflowId"));
			boolean isRedefining = Boolean.parseBoolean(ServletActionContext
					.getRequest().getParameter("isRedefine"));

			String workflowTitle = ServletActionContext
					.getRequest().getParameter("workflowTitle");
			// get the selected process (use case) against which workflows
			// have to be defined
			ModuleProcess moduleProcess = workflowService
					.getModuleProcessById(Long.parseLong(processId));

			Quantum quantum=handleUserActionBL.quantumRule(moduleProcess.getProcessTitle());
			for(WorkflowDetail wd:recentWorkflows){
				if(wd.getIsEscalation()!=null && wd.getIsEscalation()){
					if(wd.getEscalationAccessRightType()==null)
						wd.setEscalationAccessRightType(0);
				}
				if(wd.getQuantumValue()!=null && !wd.getQuantumValue().equals("")){
					wd.setQuantumScreen(quantum.getScreen());
					wd.setQuantumField(quantum.getField());
					wd.setQuantumCondition(quantum.getCondition());
				}
			}
			
			setWorkflowDetailVO(moduleProcess);
			
			WorkflowVO workflowVO =new WorkflowVO();
			workflowVO.setModuleProcess(moduleProcess);
			workflowVO.setWorkflowTitle(workflowTitle);
			workflowVO.setIsActive(true);
			if(selectedWorkflowId!=null && selectedWorkflowId>0)
				workflowVO.setWorkflowId(selectedWorkflowId);
			saveWorkflowBL.saveWorkflow(workflowVO, recentWorkflows
					);

			processId = "-9999";
			recentWorkflows.clear();
			loadFormDetails();
			confirmSave(true);

			// Commented for ERP
			/*
			 * HttpServletRequest request = ServletActionContext.getRequest();
			 * HttpServletResponse response = (HttpServletResponse)
			 * ServletActionContext .getResponse();
			 * response.sendRedirect(request.getContextPath().concat(
			 * "/workflow/mofa-workflow-management.action#!"));
			 */

		} catch (Exception e) {
			e.printStackTrace();
			confirmSave(false);
		}

	}

	private void confirmSave(boolean isSuccessful) {

		if (isSuccessful) {
			addActionMessage("Information have been saved successfully");
		} else {
			addActionError("Failed to save data");
		}
	}

	/**
	 * 
	 */
	private void setWorkflowDetailVO(ModuleProcess moduleProcess) {

		for (WorkflowDetailVO workflowDetailVO : recentWorkflows) {
			Workflow workflow = new Workflow();
			workflow.setModuleProcess(moduleProcess);
			workflowDetailVO.setWorkflow(workflow);

			Date currentDate = new Date();
			workflowDetailVO.getWorkflow().setCreationDate(currentDate);
			workflowDetailVO.getWorkflow().setIsActive(true);
			workflowDetailVO.getWorkflow().setWorkflowTitle("");
			workflowDetailVO.setIsActive(true);
		}
	}

	private Map<Byte, String> fetchOperationGroupList() {
		operationGroupList = new HashMap<Byte, String>();
		operationGroupList.put(
				(byte) WorkflowConstants.OperationGroup.Entry.getCode(),
				WorkflowConstants.OperationGroup.Entry.name());
		operationGroupList.put(
				(byte) WorkflowConstants.OperationGroup.Preview.getCode(),
				WorkflowConstants.OperationGroup.Preview.name());
		operationGroupList.put(
				(byte) WorkflowConstants.OperationGroup.Approval.getCode(),
				WorkflowConstants.OperationGroup.Approval.name());
		operationGroupList.put(
				(byte) WorkflowConstants.OperationGroup.Post.getCode(),
				WorkflowConstants.OperationGroup.Post.name());

		return operationGroupList;
	}

	@SuppressWarnings("unused")
	private List<WorkflowDetailVO> fetchOperationList(Long processId) {
		try {
			List<OperationMaster> operations = workflowService
					.getAllOperations();
			if (operations != null && operations.size() > 0) {
				WorkflowDetailVO operationVO = null;
				OperationMaster operationMaster = null;
				for (OperationMaster operation : operations) {
					if (processId != null
							&& (operation.getModuleProcess() != null
									 && (long) operation
									.getModuleProcess().getProcessId() != processId))
						continue;

					operationVO = new WorkflowDetailVO();
					operationMaster = new OperationMaster();
					operationMaster.setOperationMasterId(operation
							.getOperationMasterId());
					operationMaster.setOperationGroup(operation
							.getOperationGroup());
					operationMaster.setTitle(operation.getTitle());
					operationVO.setOperationMaster(operationMaster);
					operationVO
							.setOperationGroupName((operation
									.getOperationGroup() != null ? WorkflowConstants.OperationGroup
									.get(operation.getOperationGroup()).name()
									: ""));

					operationList.add(operationVO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return operationList;
	}

	/* ========================= Getters, Setters ============================== */
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getWorkflowVOsJSON() {
		return workflowVOsJSON;
	}

	public void setWorkflowVOsJSON(String workflowVOsJSON) {
		this.workflowVOsJSON = workflowVOsJSON;
	}

	public String getRolesJSON() {
		return rolesJSON;
	}

	public void setRolesJSON(String rolesJSON) {
		this.rolesJSON = rolesJSON;
	}

	public List<ModuleProcess> getModuleProcesses() {
		return moduleProcesses;
	}

	public void setModuleProcesses(List<ModuleProcess> moduleProcesses) {
		this.moduleProcesses = moduleProcesses;
	}

	public List<WorkflowDetailVO> getRecentWorkflows() {
		return recentWorkflows;
	}

	public void setRecentWorkflows(List<WorkflowDetailVO> recentWorkflows) {
		this.recentWorkflows = recentWorkflows;
	}

	public List<OperationMaster> getOperations() {
		return operations;
	}

	public void setOperations(List<OperationMaster> operations) {
		this.operations = operations;
	}

	public String getUsersJSON() {
		return usersJSON;
	}

	public void setUsersJSON(String usersJSON) {
		this.usersJSON = usersJSON;
	}

	public String getScreensJSON() {
		return screensJSON;
	}

	public void setScreensJSON(String screensJSON) {
		this.screensJSON = screensJSON;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public OperationMaster getOperationMaster() {
		return operationMaster;
	}

	public void setOperationMaster(OperationMaster operationMaster) {
		this.operationMaster = operationMaster;
	}

	public Map<Byte, String> getOperationGroupList() {
		return operationGroupList;
	}

	public void setOperationGroupList(Map<Byte, String> operationGroupList) {
		this.operationGroupList = operationGroupList;
	}

	public List<WorkflowDetailVO> getOperationList() {
		return operationList;
	}

	public void setOperationList(List<WorkflowDetailVO> operationList) {
		this.operationList = operationList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Quantum getQuantum() {
		return quantum;
	}

	public void setQuantum(Quantum quantum) {
		this.quantum = quantum;
	}

}
