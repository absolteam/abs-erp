/**
 * 
 */
package com.aiotech.aios.workflow.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.MenuRight;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */

public class MenuRightService {

	private AIOTechGenericDAO<Menu> menuDAO;
	private AIOTechGenericDAO<MenuRight> menuRightDAO;

	/**
	 * returns menu object against menu id.
	 * 
	 * @param menuId
	 * 
	 * @return menu object
	 * 
	 */
	public Menu getMenuById(Long menuId) throws Exception {
		return menuDAO.findById(menuId);
	}

	/**
	 * returns list of menus against screen id.
	 * 
	 * @param screenId
	 * 
	 * @return list of menus.
	 * 
	 */
	public List<Menu> getMenuByScreenId(Long screenId) throws Exception {
		return menuDAO.findByNamedQuery("getMenuByScreenId", screenId);
	}

	/**
	 * returns list of menu rights against menu id and roleId.
	 * 
	 * @param menuId
	 * 
	 * @param roleId
	 * 
	 * @return list of menu rights.
	 * 
	 */
	public List<MenuRight> getMenuRightByMenuIdandRoleId(Long menuId,
			Long roleId) throws Exception {
		Object[] args = { menuId, roleId };
		return menuRightDAO.findByNamedQuery("getMenuRightByMenuIdandRoleId",
				args);
	}

	/**
	 * saves or updates the menu right object
	 * 
	 * @param menuRight
	 * 
	 */
	public void saveOrUpdateMenuRight(MenuRight menuRight) {
		menuRightDAO.saveOrUpdate(menuRight);
	}

	/**
	 * removes menu right object
	 * 
	 * @param menuRight
	 * 
	 */
	public void removeMenuRight(MenuRight menuRight) {
		menuRightDAO.delete(menuRight);
	}

	/**
	 * returns list of menu rights against role id.
	 * 
	 * @param roleId
	 * 
	 * @return list of menu rights.
	 * 
	 */
	public List<MenuRight> getMenuRightsForUserRole(long roleId)
			throws Exception {
		return menuRightDAO.findByNamedQuery("getRoleMenuRights", roleId);
	}

	/**
	 * returns list of all root/parent menus.
	 * 
	 * 
	 * @return list of menus.
	 * 
	 */
	public List<Menu> getParentMenuItems() throws Exception {
		return menuDAO.findByNamedQuery("getParentMenuItems");
	}

	/**
	 * returns list of all child menus.
	 * 
	 * 
	 * @return list of menus.
	 * 
	 */
	public List<Menu> getChildMenuItems(long parentId) throws Exception {
		return menuDAO.findByNamedQuery("getChildMenuItems", parentId);
	}

	/**
	 * returns list of menus against menuTitle.
	 * 
	 * 
	 * @return list of menus.
	 * 
	 */
	public List<Menu> getMenuByName(String menuTitle) throws Exception {
		return menuDAO.findByNamedQuery("getMenuByName", menuTitle);
	}

	/**
	 * returns list of all menus.
	 * 
	 * 
	 * @return list of menus.
	 * 
	 */
	public List<Menu> getAllMenus() throws Exception {
		return menuDAO.getAll();
	}

	/**
	 * returns list of all menu rigths by role Id or menu name.
	 * 
	 * 
	 * @return list of menu rights.
	 * 
	 */
	public List<MenuRight> getMenuRightByRoleIdAndMenuName(Long roleId,
			String menuName) throws Exception {
		Criteria menuRightCriteria = menuRightDAO.createCriteria();

		Criteria roleCriteria = menuRightCriteria.createCriteria("role");
		if (roleId != null) {
			roleCriteria.add(Restrictions.eq("roleId", roleId));
		}

		Criteria menuCriteria = menuRightCriteria.createCriteria("menu");
		if (menuName != null) {
			menuCriteria.add(Restrictions.eq("itemTitle", menuName));
		}

		return menuRightCriteria.list();
	}

	public AIOTechGenericDAO<Menu> getMenuDAO() {
		return menuDAO;
	}

	public void setMenuDAO(AIOTechGenericDAO<Menu> menuDAO) {
		this.menuDAO = menuDAO;
	}

	public AIOTechGenericDAO<MenuRight> getMenuRightDAO() {
		return menuRightDAO;
	}

	public void setMenuRightDAO(AIOTechGenericDAO<MenuRight> menuRightDAO) {
		this.menuRightDAO = menuRightDAO;
	}

}
