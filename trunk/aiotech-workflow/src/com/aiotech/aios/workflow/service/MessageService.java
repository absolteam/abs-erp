/**
 * 
 */
package com.aiotech.aios.workflow.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.notify.enterprise.NotifyEnterpriseService;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */

public class MessageService {

	private AIOTechGenericDAO<Message> messageDAO;
	private AIOTechGenericDAO<UserRole> userRoleDAO;
	private AIOTechGenericDAO<Comment> commentDAO;

	private AIOTechGenericDAO<User> userDAO;

	private NotifyEnterpriseService notifyEnterpriseService;

	private static Properties prop;

	static {
		try {
			prop = new Properties();
			InputStream input = null;
			input = MessageService.class
					.getResourceAsStream("/resources/config.properties");
			prop.load(input);
			// Reload The config file from local drive
			prop.load(new FileInputStream(prop.getProperty("SYNCFILE-LOCATION")
					.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This Method fetch message of a workflow detail process for a person
	 * 
	 * @param message
	 *            : object contains workflow detail process id and person id
	 * 
	 * @return List<Message> : List of messages
	 * 
	 * @author Khawaja Hasham
	 */
	@SuppressWarnings("unchecked")
	public List<Message> getMessageOfProcess(Message message) {

		Criteria messageCriteria = messageDAO.createCriteria();

		Criteria workflowDetailProcessCriteria = messageCriteria
				.createCriteria("workflowDetailProcess");
		workflowDetailProcessCriteria.add(Restrictions.eq(
				"workflowDetailProcessId", message.getWorkflowDetailProcess()
						.getWorkflowDetailProcessId()));

		if (message.getPersonId() != null) {
			messageCriteria.add(Restrictions.eq("personId",
					message.getPersonId()));
		}

		return messageCriteria.list();
	}

	public List<Message> getUnApprovedMessageAgainstWorkflowDetailProcess(
			Message message) throws Exception {
		return messageDAO.findByNamedQuery(
				"getUnApprovedMessageAgainstWorkflowDetailProcess", message
						.getWorkflowDetailProcess().getWorkflowDetail()
						.getWorkflowDetailId(), message.getRecordId());
	}

	public List<Message> getUnApprovedMessageAgainstWorkflow(Long workflowId,
			Long recordId) throws Exception {
		return messageDAO.findByNamedQuery(
				"getUnApprovedMessageAgainstWorkflow", workflowId, recordId);
	}

	/**
	 * This Method saves the message record
	 * 
	 * @param Message
	 *            : object to be saved
	 * 
	 * @author Khawaja Hasham
	 */
	public void saveMessage(Message message) throws Exception {
		messageDAO.saveOrUpdate(message);
	}

	public void saveMessage(List<Message> messages) throws Exception {
		messageDAO.saveOrUpdateAll(messages);
	}

	/**
	 * This Method saves the messages for forward and reverse flows to show the
	 * notification to the user
	 * 
	 * @param WorkflowDetailProcess
	 *            : object to be saved
	 * 
	 * @param useCase
	 *            : name of use case against which operation has been performed
	 * 
	 * @param createdBy
	 *            : id of person who performed the operation
	 * 
	 * 
	 * @author Khawaja Hasham
	 */
	public Message saveMessageAgainstworkflowProcess(
			WorkflowDetailProcess workflowDetailProcess, String useCase,
			Long createdBy, WorkflowDetailVO workflowVO) {

		Message messageReturnMessage = null;
		WorkflowDetail workflowDetail = workflowDetailProcess
				.getWorkflowDetail();
		Map<String, List<Message>> messageMap = new HashMap<String, List<Message>>();
		// Creating Notification Message
		String messagePart2 = generateNotificationMessage(workflowVO,
				workflowDetailProcess);
		List<Message> messages = new ArrayList<Message>();
		if ((workflowDetail != null)
				&& (workflowDetail.getAccessRightType() == 1)
				&& (workflowDetail.getPersonId() != 0)) {
			// if it is a reverse flow assigned against person
			List<UserRole> userRoles = userRoleDAO.findByNamedQuery(
					"getAllUsersAgainstRole", workflowDetail.getRole());
			String userName = null;
			for (UserRole user : userRoles) {
				if (workflowDetail.getPersonId() == (long) user.getUser()
						.getPersonId()) {
					userName = user.getUser().getUsername();
					break;
				}
			}
			Message message = new Message();
			message.setWorkflowDetailProcess(workflowDetailProcess);
			message.setMessage(messagePart2);
			message.setStatus((byte) 0);
			message.setCreatedDate(new Date());
			message.setRecordId(workflowDetailProcess.getRecordId());
			message.setPersonId(workflowDetail.getPersonId());
			messageDAO.saveOrUpdate(message);
			messageReturnMessage = message;
			if (!messageMap.containsKey(message.getRecordId() + "-"
					+ message.getPersonId())) {
				messages.add(message);
				messageMap.put((message.getRecordId() + "-" + userName),
						messages);
			}
		} else if ((workflowDetail != null)
				&& ((workflowDetail.getAccessRightType() == 0) && (createdBy == null))
				&& (workflowDetail.getRole() != null)) {
			// if it is a forward flow
			List<UserRole> userRoles = userRoleDAO.findByNamedQuery(
					"getAllUsersAgainstRole", workflowDetail.getRole());
			for (UserRole user : userRoles) {

				Message message = new Message();
				message.setWorkflowDetailProcess(workflowDetailProcess);
				message.setMessage(messagePart2);
				message.setStatus((byte) 0);
				message.setCreatedDate(new Date());
				message.setRecordId(workflowDetailProcess.getRecordId());
				message.setPersonId(user.getUser().getPersonId());
				messageDAO.saveOrUpdate(message);
				messageReturnMessage = message;
				if (!messageMap.containsKey(message.getRecordId() + "-"
						+ user.getUser().getUsername())) {
					messages.add(message);
					messageMap.put((message.getRecordId() + "-" + user
							.getUser().getUsername()), messages);
				}
			}

		} else if ((createdBy != null) && (workflowDetail != null)
				&& (workflowDetail.getAccessRightType() == 0)
				&& (workflowDetail.getRole() != null)) {
			// if it is reverse flow assigned against role
			List<UserRole> userRoles = userRoleDAO.findByNamedQuery(
					"getAllUsersAgainstRole", workflowDetail.getRole());
			String userName = null;
			for (UserRole user : userRoles) {
				if (createdBy == (long) user.getUser().getPersonId()) {
					userName = user.getUser().getUsername();
					break;
				}
			}
			Message message = new Message();
			message.setWorkflowDetailProcess(workflowDetailProcess);
			message.setMessage(messagePart2);
			message.setStatus((byte) 0);
			message.setCreatedDate(new Date());
			message.setRecordId(workflowDetailProcess.getRecordId());
			message.setPersonId(createdBy);
			messageDAO.saveOrUpdate(message);
			messageReturnMessage = message;
			if (!messageMap.containsKey(message.getRecordId() + "-"
					+ message.getPersonId())) {
				messages.add(message);
				messageMap.put((message.getRecordId() + "-" + userName),
						messages);
			}
		}
		HttpSession session = ServletActionContext.getRequest().getSession();
		Boolean workflowEmailEnabled = session.getAttribute("email_workflow") != null ? ((String) session
				.getAttribute("email_workflow")).equalsIgnoreCase("true")
				: false;
		Boolean workflowSMSEnabled = session.getAttribute("sms_workflow") != null ? ((String) session
				.getAttribute("sms_workflow")).equalsIgnoreCase("true") : false;
		if (null != messageMap && messageMap.size() > 0
				&& (workflowEmailEnabled || workflowSMSEnabled)
				&& !workflowVO.isInform()) {
			try {
				String userName = "";
				List<User> users = null;
				for (Entry<String, List<Message>> message : messageMap
						.entrySet()) {
					userName = message.getKey().split("-")[1];
					users = userDAO.findByNamedQuery(
							"getUserWithOutImplementation", userName);
					if (null != users && users.size() > 0) {
						String approvalLink = "";
						String[] afterSplit = null;
						for (Message msg : message.getValue()) {
							if (null != users.get(0).getEmailAddress()
									&& !("").equals(users.get(0)
											.getEmailAddress())
									&& workflowEmailEnabled) {
								if ((byte) msg.getWorkflowDetailProcess()
										.getOperation() == (byte) WorkflowConstants.Status.Saved
										.getCode()) {
									afterSplit = prop
											.getProperty("WEBSERVICE-URL")
											.trim().split("service/rest/");
									String newString = afterSplit[0];
									approvalLink = "<br><a href="
											+ newString
											+ "wf/workflow_approval_email.action?processKey="
											+ msg.getMessageId()
											+ " target=_blank>Click this link to process the request</a>";

								}
								notifyEnterpriseService.sendEmail(msg
										.getMessage(), msg.getMessage()
										+ approvalLink, users.get(0)
										.getEmailAddress(), "en", users.get(0)
										.getImplementationId(), users.get(0)
										.getPersonId(), false, null);
							}
							if (null != users.get(0).getMobileNumber()
									&& !("").equals(users.get(0)
											.getMobileNumber())
									&& workflowSMSEnabled)
								notifyEnterpriseService.sendSMS(msg
										.getMessage(), users.get(0)
										.getMobileNumber(), "en", users.get(0)
										.getImplementationId(), users.get(0)
										.getPersonId(), false, null);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return messageReturnMessage;
	}

	public String generateNotificationMessage(WorkflowDetailVO workflowVO,
			WorkflowDetailProcess workflowDetailProcess) {
		String messagePart2 = "";
		if ((byte) workflowVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Add
				.getCode()) {

			if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Approved
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Published
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Deny
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ "  has been rejected";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Posted
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " has been added and waiting for "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle();
			}
		} else if ((byte) workflowVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Modify
				.getCode()) {
			if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Approved
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Published
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ "  has been modified";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Deny
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " modification  has been rejected";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Posted
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " has been modified and waiting for "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle();
			}
		} else if ((byte) workflowVO.getProcessType() == (byte) WorkflowConstants.ProcessMethod.Delete
				.getCode()) {
			if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Approved
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " delete has been approved ";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Deleted
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ "  has been deleted";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Deny
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " delete request has been rejected";
			} else if ((byte) workflowDetailProcess.getOperation() == (byte) WorkflowConstants.Status.Posted
					.getCode()) {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle()
						+ " has been done";
			} else {
				messagePart2 = workflowVO.getNextWorkflowDetail().getWorkflow()
						.getModuleProcess().getProcessDisplayName()
						+ " has been deleted and waiting for "
						+ workflowVO.getNextWorkflowDetail()
								.getOperationMaster().getTitle();
			}
		}
		return messagePart2;

	}

	public void generateEscalationReminderMessage(
			WorkflowDetailProcess workflowDetailProcess, String useCase,
			Long createdBy, WorkflowDetailVO workflowVO) {
		String messagePart2 = workflowVO.getMessage().getMessage();
		messagePart2 = messagePart2
				.substring(0, messagePart2.indexOf(">>") + 2);
		WorkflowDetail workflowDetail = workflowDetailProcess
				.getWorkflowDetail();
		if ((workflowDetail.getEscalationAccessRightType() != null)
				&& (workflowDetail.getEscalationAccessRightType() == 1)
				&& (workflowDetail.getEscalateTo() != null)) {
			// if it is a reverse flow assigned against person
			Message message = new Message();
			message.setWorkflowDetailProcess(workflowDetailProcess);
			message.setMessage(messagePart2 + "  Escalated from "
					+ workflowDetail.getEscalateTo());
			message.setStatus((byte) 0);
			message.setCreatedDate(new Date());
			message.setRecordId(workflowDetailProcess.getRecordId());
			message.setPersonId(workflowDetail.getPersonId());
			message.setEscaletedFrom(workflowDetailProcess
					.getWorkflowDetailProcessId());
			messageDAO.saveOrUpdate(message);
		} else if ((workflowDetail.getEscalationAccessRightType() != null)
				&& (workflowDetail.getEscalationAccessRightType() == 0)
				&& (workflowDetail.getEscalateTo() != null)) {
			// if it is a forward flow
			Role role = new Role();
			role.setRoleId(workflowDetail.getEscalateTo());
			List<UserRole> userRoles = userRoleDAO.findByNamedQuery(
					"getAllUsersAgainstRole", role);
			for (UserRole user : userRoles) {

				Message message = new Message();
				message.setWorkflowDetailProcess(workflowDetailProcess);
				message.setMessage(messagePart2 + "  Escalated from "
						+ user.getRole().getRoleName());
				message.setStatus((byte) 0);
				message.setCreatedDate(new Date());
				message.setRecordId(workflowDetailProcess.getRecordId());
				message.setPersonId(user.getUser().getPersonId());
				message.setEscaletedFrom(workflowDetailProcess
						.getWorkflowDetailProcessId());
				messageDAO.saveOrUpdate(message);
			}

		}
	}

	/**
	 * This Method fetch all messages of given operation against the given
	 * person id
	 * 
	 * @param Person
	 *            : for which messages have to be fetched
	 * 
	 * @return operations : list of operations to determine type of
	 *         notifications
	 * 
	 * @author Khawaja Hasham
	 */

	public List<Message> getAllMessageAgainstPerson(Long personId,
			List<Byte> operations) {
		String query = "SELECT Distinct(m) FROM Message m "
				+ " LEFT JOIN FETCH m.workflowDetailProcess wdp "
				+ " LEFT JOIN FETCH wdp.screen scr "
				+ " LEFT JOIN FETCH wdp.workflowDetail wd"
				+ " WHERE m.personId=:personId and m.status=0 and wdp.operation IN (:operations) and wd.isActive=1";

		String[] paramNames = { "personId", "operations" };
		Object[] values = { personId, operations };
		return messageDAO.findByNamedParam(query, paramNames, values);
	}

	/**
	 * This Method fetch all alerts which is to be shown to given person
	 * 
	 * @param Person
	 *            : to whom alerts will be shown
	 * 
	 * @return List<Message> : list of alerts messsages
	 * 
	 * @author Khawaja Hasham
	 */

	public List<Message> getAllAlertsAgainstPerson(Long personId,
			List<Byte> operations) {
		String query = "SELECT Distinct(m) FROM Message m "
				+ " LEFT JOIN FETCH m.workflowDetailProcess wdp "
				+ " LEFT JOIN FETCH wdp.workflowDetail wd"
				+ " WHERE m.personId=:personId and m.status=0 and wdp.operation IN (:operations) and wd.isActive=1 and m.isAlert=true";

		String[] paramNames = { "personId", "operations" };
		Object[] values = { personId, operations };
		return messageDAO.findByNamedParam(query, paramNames, values);
		// return messageDAO.findByNamedQuery("getAllMessageAgainstPerson",
		// personId, operations);
	}

	/**
	 * This Method uses criteria to fetch messages based on specified values.
	 * 
	 * @param personId
	 *            : id of the logged in user
	 * @param operations
	 *            : List of Byte contains Constants Enum values
	 * @param status
	 *            : 0 for messages which are pending , 1 for messages on which
	 *            operation is performed
	 * @return: List of Message records
	 * 
	 * @author Shoaib Ahmad Gondal 25-Sep-12
	 */

	@SuppressWarnings("unchecked")
	public List<Message> getAllMessagesByPersonAndOperations(long personId,
			List<Byte> operations, Byte status) {
		Criteria messageCriteria = messageDAO.createCriteria();

		Criteria workflowDetailProcessCriteria = messageCriteria
				.createCriteria("workflowDetailProcess");

		Criteria workflowDetailCriteria = workflowDetailProcessCriteria
				.createCriteria("workflowDetail");

		messageCriteria.add(Restrictions.eq("personId", personId));
		messageCriteria.add(Restrictions.eq("status", status));
		messageCriteria.add(Restrictions.eq("isEscalated", false));

		workflowDetailProcessCriteria.add(Restrictions.in("operation",
				operations));

		// filter notifications which have reminder enabled.
		workflowDetailCriteria.add(Restrictions.ne("isDays", (byte) 0));

		return messageCriteria.list();
	}

	/**
	 * saves the comment object in database.
	 * 
	 * @param comment
	 *            object
	 * 
	 */
	public void saveComment(Comment comment) {
		commentDAO.saveOrUpdate(comment);
	}

	/**
	 * gets the List of comments against use case.
	 * 
	 * @param comment
	 *            object
	 * 
	 * @return list of comments.
	 * 
	 */
	public List<Comment> getUseCaseComment(Comment comment) throws Exception {
		return commentDAO.findByNamedQuery("getUseCaseComment",
				comment.getRecordId(), comment.getUseCase());
	}

	/**
	 * gets comment object by record id.
	 * 
	 * @param record
	 *            Id.
	 * 
	 * @return comment object.
	 */
	public Comment getCommentByRecordId(long recordId,
			long workflowDetailProcessId) {
		if (commentDAO.findByNamedQuery("getCommentByRecordId", recordId,
				workflowDetailProcessId).size() > 0) {
			return commentDAO
					.findByNamedQuery("getCommentByRecordId", recordId,
							workflowDetailProcessId).iterator().next();
		} else {
			return null;
		}
	}

	/**
	 * returns list of messages with workflow process details against the
	 * message id.
	 * 
	 * @param messageId
	 * 
	 * @return list of messages.
	 */
	public List<Message> getMessageWithWorkflowProcessDetailsByMsgId(
			Long messageId) throws Exception {
		return messageDAO.findByNamedQuery(
				"getMessageWithWorkflowProcessDetailsByMsgId", messageId);
	}

	/**
	 * returns list of messages with workflow process details against the
	 * message id.
	 * 
	 * @param workflowId
	 *            ,recordId
	 * 
	 * @return list of messages.
	 */
	public List<Message> getMessageIfAnyRejectedMessage(Long workflowId,
			Long recordId) throws Exception {
		return messageDAO.findByNamedQuery("getMessageIfAnyRejectedMessage",
				workflowId, recordId);
	}

	public List<Message> findMessageByExample(Message message) {
		return messageDAO.findByExample(message);
	}

	public Message findMessageById(Long messageId) {
		return messageDAO.findById(messageId);
	}

	public List<UserRole> getAllUserRoleByRoleId(Role role) {
		return userRoleDAO.findByNamedQuery("getAllUsersAgainstRole", role);
	}

	public Message getMessageInfoByMessageId(Long messageId) {
		List<Message> messages = messageDAO.findByNamedQuery(
				"getMessageInfoByMessageId", messageId);
		if (messages != null && messages.size() > 0)
			return messages.get(0);
		else
			return null;
	}

	public AIOTechGenericDAO<Message> getMessageDAO() {
		return messageDAO;
	}

	public void setMessageDAO(AIOTechGenericDAO<Message> messageDAO) {
		this.messageDAO = messageDAO;
	}

	public AIOTechGenericDAO<UserRole> getUserRoleDAO() {
		return userRoleDAO;
	}

	public void setUserRoleDAO(AIOTechGenericDAO<UserRole> userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}

	public AIOTechGenericDAO<Comment> getCommentDAO() {
		return commentDAO;
	}

	public void setCommentDAO(AIOTechGenericDAO<Comment> commentDAO) {
		this.commentDAO = commentDAO;
	}

	public AIOTechGenericDAO<User> getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(AIOTechGenericDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

	public NotifyEnterpriseService getNotifyEnterpriseService() {
		return notifyEnterpriseService;
	}

	public void setNotifyEnterpriseService(
			NotifyEnterpriseService notifyEnterpriseService) {
		this.notifyEnterpriseService = notifyEnterpriseService;
	}
}
