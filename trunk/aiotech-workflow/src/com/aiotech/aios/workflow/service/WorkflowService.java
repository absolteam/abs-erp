/**
 * 
 */
package com.aiotech.aios.workflow.service;

import java.beans.Introspector;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.workflow.domain.entity.Module;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.Notification;
import com.aiotech.aios.workflow.domain.entity.OperationMaster;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowLogVO;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */

public class WorkflowService {

	private AIOTechGenericDAO<Workflow> workflowDAO;
	private AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO;
	private AIOTechGenericDAO<Module> moduleDAO;
	private AIOTechGenericDAO<ModuleProcess> moduleProcessDAO;
	private AIOTechGenericDAO<Screen> screenDAO;
	private AIOTechGenericDAO<UserRole> userRoleDAO;
	private AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO;
	private AIOTechGenericDAO<User> userDAO;
	private AIOTechGenericDAO<OperationMaster> operationMasterDAO;

	public List<Workflow> getDataEntryWorkFlowsOfProcess(
			ModuleProcess moduleProcess) throws Exception {

		return workflowDAO.findByNamedQuery("getDataEntryWorkFlowsOfProcess",
				moduleProcess);
	}

	public Workflow getWorkflowByWorkflowId(Long workflowId) throws Exception {

		List<Workflow> workflows = workflowDAO.findByNamedQuery(
				"getWorkflowByWorkflowId", workflowId);
		if (workflows != null && workflows.size() > 0)
			return workflows.get(0);
		else
			return null;
	}

	public List<WorkflowDetailProcess> getDetailProcessForWorkflow(
			Workflow workflow, List<Byte> excludeOperations, Long recordId,Byte processType) {

		String query = "SELECT Distinct(wdp) FROM WorkflowDetailProcess wdp "
				+ " WHERE wdp.workflowId = :workflowId AND wdp.operation NOT IN (:operations) AND wdp.recordId=:recordId " +
				" AND wdp.processType=:processType ORDER BY wdp.createdDate DESC ";

		String[] paramNames = { "workflowId", "operations", "recordId","processType" };
		Object[] values = { workflow.getWorkflowId(), excludeOperations,
				recordId,processType };
		return workflowDetailProcessDAO.findByNamedParam(query, paramNames,
				values);
	}

	public Workflow getDeleteWorkflowForModuleProcess(
			ModuleProcess moduleProcess) throws Exception {

		List<Workflow> defaultDeleteWorkflow = workflowDAO.findByNamedQuery(
				"getDeleteWorkflowForModuleProcess", moduleProcess,
				(byte) WorkflowConstants.Status.Deny.getCode());
		return (defaultDeleteWorkflow != null && defaultDeleteWorkflow.size() > 0) ? defaultDeleteWorkflow
				.get(0) : null;
	}

	public List<WorkflowDetailProcess> findLastWorkflowDetailProcessForTargetRecord(
			ModuleProcess moduleProcess, long recordId) {

		List<WorkflowDetailProcess> operationsPerformedOnTargetRecord = workflowDetailProcessDAO
				.findByNamedQuery(
						"findLastWorkflowDetailProcessForTargetRecord",
						moduleProcess, recordId);
		return operationsPerformedOnTargetRecord;
		/*
		 * return (operationsPerformedOnTargetRecord != null &&
		 * operationsPerformedOnTargetRecord .size() > 0) ?
		 * operationsPerformedOnTargetRecord.get(0) : null;
		 */
	}

	public WorkflowDetailProcess getLastDeleteRequestReadOnlyProcess(
			ModuleProcess moduleProcess, long recordId, Byte operation) {

		List<WorkflowDetailProcess> lastDeleteReadOnly = workflowDetailProcessDAO
				.findByNamedQuery("getLastDeleteRequestReadOnlyProcess",
						moduleProcess, recordId, operation);
		return (lastDeleteReadOnly != null && lastDeleteReadOnly.size() > 0) ? lastDeleteReadOnly
				.get(0) : null;
	}

	/**
	 * Fetches record by workflow detail id (primary key)
	 * 
	 * @param workflowDetailId
	 * @return
	 * @author Khawaja Hasham
	 */
	public WorkflowDetail getWorkflowDetail(Long workflowDetailId) {
		return workflowDetailDAO.findById(workflowDetailId);
	}

	/**
	 * Fetches latest record (first record from result being sorted desc on
	 * 'created_date') for given entity, recordId, and operation combination
	 * 
	 * @param processTitle
	 *            , recordId, operation
	 * @return
	 * @author Ahsan M.
	 */
	public WorkflowDetailProcess getEntityBasedWorkflowDetailProcessAgainstRecord(
			Long processId, Long recordId, int operation) {

		List<WorkflowDetailProcess> detailProcesses = null;
		try {
			detailProcesses = workflowDetailProcessDAO.findByNamedQuery(
					"getEntityBasedWorkflowDetailProcessAgainstRecord",
					processId, recordId, Byte.parseByte(operation + ""));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return (detailProcesses != null && detailProcesses.size() > 0) ? detailProcesses
				.get(0) : null;
	}

	/**
	 * This Method fetch all active workflow details which are defined for the
	 * given role
	 * 
	 * @param Role
	 *            : Role Entity object which contains the role id
	 * 
	 * @return List<WorkflowDetail> : List of workflow details
	 * 
	 * @author Khawaja Hasham
	 */
	public List<WorkflowDetail> getWorkFlowDetailList(Role role)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetails", role,
				WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method fetch all active workflow details which are defined for the
	 * given person
	 * 
	 * @param Person
	 *            : Person Entity object which contains the person id
	 * 
	 * @return List<WorkflowDetail> : List of workflow details
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> getWorkFlowDetailListOfPerson(Long personId)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery(
				"getWorkFlowDetialListOfPerson", personId,
				WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method fetch list of all active worklow details
	 * 
	 * @param
	 * 
	 * @return List<WorkflowDetail> : List of workflow details
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> getAllWorkFlowDetails() throws Exception {

		return workflowDetailDAO.findByNamedQuery("getAllWorkFlowDetails",
				WorkflowUtils.getImplementationId());
	}
	
	/**
	 * This Method fetch list of all active worklow details
	 * 
	 * @param
	 * 
	 * @return List<WorkflowDetail> : List of workflow details
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> getAllWorkFlowDetailsForEscalation() throws Exception {

		return workflowDetailDAO.findByNamedQuery("getAllWorkFlowDetailsForEscalation",
				WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method fetch all workflows of the given process
	 * 
	 * @param ModuleProcess
	 *            : ModuleProcess Entity object which contains process Id whose
	 *            workflows are to be fetched
	 * 
	 * @return List<Workflow> : List of workflow which belongs to given process
	 * 
	 * @author Khawaja Hasham
	 */
	public List<Workflow> getWorkflowsOfProcess(ModuleProcess moduleProcess)
			throws Exception {
		return workflowDAO.findByNamedQuery("getWorkflowsOfProcess",
				moduleProcess, WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method fetch all workflows of the given process
	 * 
	 * @param : ModuleProcess Entity object which contains process Id whose
	 *        workflows are to be fetched
	 * 
	 * @return List<Workflow> : List of workflow which belongs to given process
	 * 
	 * @author Mohamed Rafiq
	 */
	public List<Workflow> getAllActiveWorkflows() throws Exception {
		return workflowDAO.findByNamedQuery("getAllActiveWorkflows",
				WorkflowUtils.getImplementationId());
	}

	/**
	 * 
	 * @param entityClassFullyQualifiedName
	 *            Fully Qualified Name of Entity class
	 * @param recordId
	 *            id of the corresponding entity table whose record is to be
	 *            fetched
	 * @return an Entity object against the given recordId
	 * @throws Exception
	 * @author Muhammad Haris
	 * @since 27-03-2013
	 */
	public Object getRecord(String entityClassFullyQualifiedName,
			Object recordId) throws Exception {

		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		/* call findById method to get given Entity object by Id */
		return dao.findById((Number) recordId);
	}

	public Object getRecordWithChilds(String entityClassFullyQualifiedName,
			Object recordId) throws Exception {

		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		/* call findById method to get given Entity object by Id */
		Object object = dao.findById((Number) recordId);
		for (Field fieldObject : object.getClass().getDeclaredFields()) {
			Hibernate.initialize(fieldObject);
		}
		return dao.findById((Number) recordId);
	}

	public Object getChildRecordsByParent(String entityClassFullyQualifiedName,
			Object recordId, String childentityClassName) throws Exception {

		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		/* call findById method to get given Entity object by Id */
		Object object = dao.findById((Number) recordId);
		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> childDao = WorkflowUtils
				.getEntityDAO(childentityClassName);

		return childDao.findByParentId(
				Introspector.decapitalize(entityClassFullyQualifiedName),
				object);
	}

	public Object fetchLatestRecordIdFromUseCase(
			String entityClassFullyQualifiedName, Object recordId)
			throws Exception {

		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		/* call findById method to get given Entity object by Id */
		Object object = dao.findById((Number) recordId);
		Object relativeId = null;
		if (object != null) {
			Method method = object.getClass().getMethod("getRelativeId");
			relativeId = method.invoke(object, null);
		}
		List<Object> relativeObject = null;
		if (relativeId != null)
			relativeObject = dao.findByRelativeId((Number) relativeId);

		return relativeObject;
	}

	/**
	 * This Method fetch workflow detail process of given workflow detail id and
	 * record id (workflow detail process of particular record of a process)
	 * 
	 * @param workflowDetailProcess
	 *            : workflow Detail Process Entity object which contains
	 *            workflow detail id and record id
	 * 
	 * @return List<WorkflowDetailProcess> : List of workflow detail process
	 *         which belongs to a particular record of a process
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetailProcess> getWorkflowProcess(
			WorkflowDetailProcess workflowDetailProcess) {
		return workflowDetailProcessDAO
				.findByNamedQuery("getWorkflowProcess", workflowDetailProcess
						.getWorkflowDetail().getWorkflowDetailId(),
						workflowDetailProcess.getRecordId());
	}

	/**
	 * This Method fetch notification record of given notification id (primary
	 * key)
	 * 
	 * @param notificationId
	 *            : primary key against which record is to be fetched
	 * 
	 * @return Notification : Object containing the fetched record
	 * 
	 * @author Khawaja Hasham
	 */

	public Notification getNotification(Long notificationId) {
		return null;// notificationDAO.findById(notificationId);
	}

	/**
	 * This Method take workflow id and the operation order of current workflow
	 * detail and find the next workflow detail in the sequence
	 * 
	 * @param workflowId
	 *            : workflow id represents the process flows
	 * 
	 * @param operationOrder
	 *            : represents the current operation order in the process flows
	 * 
	 * @return List<WorkflowDetail> : contains list of all next workflow details
	 *         in ascending order
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> findNextWorkflow(Long workflowId,
			byte operationOrder) {
		return workflowDetailDAO.findByNamedQuery("findNextWorkflow",
				workflowId, operationOrder);
	}

	/**
	 * This Method take workflow id and the operation order of current workflow
	 * detail and find the previous workflow detail in the sequence
	 * 
	 * @param workflowId
	 *            : workflow id represents the process flows
	 * 
	 * @param operationOrder
	 *            : represents the current operation order in the process flows
	 * 
	 * @return List<WorkflowDetail> : contains list of all previous workflow
	 *         details in descending order
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> findPreviousWorkflow(Long workflowId,
			byte operationOrder) {
		return workflowDetailDAO.findByNamedQuery("findPreviousWorkflow",
				workflowId, operationOrder);
	}

	/**
	 * This Method take workflow id and the operation order of current workflow
	 * detail and find the previous workflow detail in the sequence
	 * 
	 * @param workflowDetail
	 *            : Obect which contains workflow id ( to represents the process
	 *            flows) and operationOrder ( to represents the current
	 *            operation order in the process flows)
	 * 
	 * @return List<WorkflowDetail> : contains list of all previous workflow
	 *         details in descending order
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> findNextWorkflow(WorkflowDetail workflowDetail) {

		return workflowDetailDAO.findByNamedQuery("findNextWorkflow",
				workflowDetail.getWorkflow().getWorkflowId(),
				workflowDetail.getOperationOrder());
	}

	public List<WorkflowDetail> getCurrentWorkflow(WorkflowDetail workflowDetail) {
		return workflowDetailDAO.findByNamedQuery("getCurrentWorkflow",
				workflowDetail.getWorkflow().getWorkflowId(),
				workflowDetail.getOperationOrder(),
				workflowDetail.getWorkflowDetailId());
	}

	/**
	 * This Method returns the reverse flow of given workflow detail id
	 * 
	 * @param parentId
	 *            : workflow detail id whose reverse flow is to be find
	 * 
	 * @param operation
	 *            : operation represent which reverse flow is to be fetched
	 *            (approved, rejected or published)
	 * 
	 * @return List<WorkflowDetail> : list containing reverse flows
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> getChildWorkflowDetail(Long parentId,
			byte operation) {
		return workflowDetailDAO.findByNamedQuery("getChildWorkflowDetail",
				parentId, operation);
	}

	private List<WorkflowDetailProcess> getInProcessRecords(
			Long workflowDetailId, Byte flag) {
		WorkflowDetailProcess workflowDetailProcess = new WorkflowDetailProcess();
		workflowDetailProcess.setWorkflowDetail(new WorkflowDetail());
		workflowDetailProcess.getWorkflowDetail().setWorkflowDetailId(
				workflowDetailId);
		// workflowDetailProcess.setOperation(flag);
		return workflowDetailProcessDAO.findByNamedQuery("getInProcessRecords",
				workflowDetailId, flag);
	}

	/**
	 * This Method returns the workflow of a module process whose entry right is
	 * given to the provided role
	 * 
	 * @param Role
	 *            : whom entry right has been given
	 * 
	 * @param ModuleProcess
	 *            : the process whose workflow is to be fetched
	 * 
	 * @return List<Workflow> : list containing the workflow of module process
	 * 
	 * @author Khawaja Hasham
	 */
	public List<Workflow> getWorkFlowOfEntryUser(Role role,
			ModuleProcess moduleProcess) {
		return workflowDAO.findByNamedQuery("getWorkFlowOfEntryUser", role,
				moduleProcess);
	}

	/**
	 * This Method returns the workflow of a module process whose entry right is
	 * given to the provided role
	 * 
	 * @param Role
	 *            : whom entry right has been given
	 * 
	 * @param ModuleProcess
	 *            : the process whose workflow is to be fetched
	 * 
	 * @return List<Workflow> : list containing the workflow of module process
	 * 
	 * @author Khawaja Hasham
	 */
	public List<Workflow> getWorkFlowOfEntryUserByPersonId(Long personId,
			ModuleProcess moduleProcess) {
		return workflowDAO.findByNamedQuery("getWorkFlowOfEntryUserByPersonId",
				personId, moduleProcess);
	}

	/**
	 * This Method update the is_approved value of given object's table
	 * 
	 * @param module
	 *            : name of module to which object belongs to
	 * 
	 * @param Object
	 *            : will represent the record of a table whose is_approve value
	 *            is to be updated
	 * 
	 * @author Khawaja Hasham
	 */
	public void setApproval(Object entityObject,
			String entityClassFullyQualifiedName) throws Exception {

		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		// update object
		dao.saveOrUpdate(entityObject);
	}

	/**
	 * This Method fetch all the processes of application
	 * 
	 * @return List<ModuleProcess> : all the processes of application
	 * 
	 * @author Khawaja Hasham
	 */

	public List<ModuleProcess> getAllModuleProcess() throws Exception {
		return moduleProcessDAO.findByNamedQuery("getAllModuleProcesses");
	}

	/**
	 * This Method fetch a process by title name
	 * 
	 * @return List<ModuleProcess> : will contains processes fetched by title
	 *         name
	 * 
	 * @author Khawaja Hasham
	 */

	public List<ModuleProcess> getModuleProcessByTitle(String title) {
		return moduleProcessDAO.findByNamedQuery("getModuleProcessByTitle",
				title);
	}

	public ModuleProcess getModuleProcessByDisplayName(String displayName) {
		return moduleProcessDAO.findByNamedQuery(
				"getModuleProcessByDisplayName", displayName).get(0);
	}

	public List<ModuleProcess> getModuleProcessByTitleAndOperation(
			String title, Byte operation) {
		Object[] params = { title, operation };
		return moduleProcessDAO.findByNamedQuery(
				"getModuleProcessByTitleAndOperation", params);
	}

	public ModuleProcess getModuleProcessById(Long id) {
		ModuleProcess moduleProcess = moduleProcessDAO.findById(id);
		return moduleProcess;
	}

	/**
	 * This Method fetch all the notifications
	 * 
	 * @return List<Notification> : will contains notifications
	 * 
	 * @author Khawaja Hasham
	 */

	public List<Notification> getAllNotifications() throws Exception {
		return null;// notificationDAO.findByNamedQuery("getAllNotifications");
	}

	/**
	 * This Method fetch all the active workflow details of a process
	 * 
	 * @param processId
	 *            : process against which workflows have to be fetch
	 * 
	 * @return List<WorkflowDetail> : will contains all the workflow details of
	 *         a process
	 * 
	 * @author Khawaja Hasham
	 */

	public List<WorkflowDetail> getWorkFlowDetailsOfProcess(Long processId)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery(
				"getWorkFlowDetailsOfProcess", processId);
	}

	/**
	 * This Method fetch all the active workflows of a process
	 * 
	 * @param processId
	 *            : process against which workflows have to be fetch
	 * 
	 * @return List<WorkflowDetail> : will contains all the workflows of a
	 *         process
	 * 
	 * @author Khawaja Hasham
	 */

	public List<Workflow> getWorkFlowsOfProcess(Long processId)
			throws Exception {
		return workflowDAO.findByNamedQuery("getWorkFlowsOfProcess", processId,
				WorkflowUtils.getImplementationId());
	}

	public List<Screen> getAllScreens() throws Exception {
		return screenDAO.getAll();
	}

	public List<Screen> getHiddenScreens() throws Exception {
		return screenDAO.findByNamedQuery("getHiddenScreens");
	}

	public void saveWorkflowDetails(WorkflowDetail workflowDetail)
			throws Exception {
		workflowDetailDAO.saveOrUpdate(workflowDetail);
	}

	public void saveAllWorkflowDetails(List<WorkflowDetail> workflowDetails)
			throws Exception {
		workflowDetailDAO.saveOrUpdateAll(workflowDetails);
	}

	public void saveWorkflow(Workflow workflow) throws Exception {
		workflow.setImplementationId(WorkflowUtils.getImplementationId());
		workflowDAO.saveOrUpdate(workflow);
	}

	public void saveNotification(Notification notification) throws Exception {
		// notificationDAO.saveOrUpdate(notification);
	}

	public void saveWorkflowDetailProcess(
			WorkflowDetailProcess workflowDetailProcess) {
		workflowDetailProcessDAO.saveOrUpdate(workflowDetailProcess);
	}

	/**
	 * This Method fetches WorkflowDetail by given workflowDetailId.
	 * workflow_detail join fetch workflow join fetch module_process join fetch
	 * screenByPreview or screenByEdit
	 * 
	 * @param workflowDetailId
	 * @param preview
	 *            : true -> JOIN with screenByPreview, False -> JOIN with
	 *            screenByEdit
	 * @return
	 * 
	 * @author Shoaib Ahmad Gondal 17-Sep-12
	 */

	public WorkflowDetail getWorkflowDetailByWorkflowDeatilId(
			Long workflowDetailId, boolean preview) throws Exception {

		Criteria workflowDetailCriteria = workflowDetailDAO.createCriteria();
		workflowDetailCriteria.add(Restrictions.eq("workflowDetailId",
				workflowDetailId));

		Criteria workflowCriteria = workflowDetailCriteria
				.createCriteria("workflow");

		Criteria moduleProcessCriteria = workflowCriteria
				.createCriteria("moduleProcess");

		if (preview) {
			Criteria screenByPreviewCriteria = moduleProcessCriteria
					.createCriteria("screenByPreview");
		} else {
			Criteria screenByEditCriteria = moduleProcessCriteria
					.createCriteria("screenByEdit");
		}

		return (WorkflowDetail) workflowDetailCriteria.list().iterator().next();
	}

	/**
	 * This Method deactivate the the workflow and worflow details defined
	 * against workflow
	 * 
	 * @param workflowId
	 * 
	 * @return
	 * 
	 * @author Khawaja Hasham 27-Sep-12
	 */
	public void deActivateWorkflows(Long workflowId) {

		String strQuery = "Update Workflow set isActive=false where workflowId="
				+ workflowId;
		Query query = workflowDAO.getHibernateSession().createQuery(strQuery);
		query.executeUpdate();

		strQuery = "Update WorkflowDetail set isActive=false where workflow.workflowId="
				+ workflowId;
		query = workflowDAO.getHibernateSession().createQuery(strQuery);
		query.executeUpdate();
	}

	public void deActivateWorkflows(List<Long> workflows) {

		String strQuery = "Update Workflow set isActive=false where workflowId IN(:workflows)";

		Query query = workflowDAO.getHibernateSession().createQuery(strQuery)
				.setParameterList("workflows", workflows);
		query.executeUpdate();

		String strQuery2 = "Update WorkflowDetail set isActive=false where workflow.workflowId IN(:workflows)";
		Query query2 = workflowDAO.getHibernateSession().createQuery(strQuery2)
				.setParameterList("workflows", workflows);
		query2.executeUpdate();
	}

	/**
	 * This Method finds screenId of the given screenName and returns it.
	 * 
	 * @param screenName
	 *            : Name of the screen, whose Id to find
	 * @return: screenId
	 * 
	 * @author Shoaib Ahmad Gondal @ 29-Nov-12
	 */
	public Long getScreenIdByScreenName(String screenName) {
		Query query = screenDAO.getNamedQuery("getScreenIdByScreenName");

		query.setString(0, screenName);

		return (Long) query.list().iterator().next();
	}

	/**
	 * Returns ids of 4 screens which are types of Treaties (Agreement,
	 * Memorandum, Protocol, Declaration, )
	 * 
	 * @return the list of screenId's.
	 * 
	 * @author Shoaib Ahmad Gondal @ 12-Dec-12
	 */
	public List<Long> getScreenIdsOfTreatiesTypes() {
		Query query = screenDAO.getNamedQuery("getScreenIdsOfTreatiesTypes");

		return query.list();
	}

	/**
	 * Returns Entry Workflow Detail of given workflow id
	 * 
	 * @return the list of workflow details
	 * 
	 * @author Hasham
	 * 
	 *         @ 31-Jan-13
	 */
	public List<WorkflowDetail> getWorkflowDetailEntryByWorkflowId(
			Long workflowId) {
		return workflowDetailDAO.findByNamedQuery(
				"getWorkflowDetailEntryByWorkflowId", workflowId);

	}

	public WorkflowDetail getPersonsAgainstRole(List<UserRole> userRoles,
			List<Long> personIds) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<WorkflowDetail> getWorkflowDetailsByRoleId(Long roleId)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkflowDetailsByRoleId",
				roleId);
	}

	/**
	 * Returns Workflow Object with ModuleProcess fetched.
	 * 
	 * @param workflowId
	 *            id of the workflow to fetch along with ModuleProcess.
	 * @return workflow Object
	 * @throws Exception
	 * 
	 * @author Shoaib Ahmad Gondal
	 */
	public Workflow getWorkflowWithModuleProcessById(Long workflowId)
			throws Exception {
		return workflowDAO
				.findByNamedQuery("getWorkflowWithModuleProcessById",
						workflowId).iterator().next();
	}

	/**
	 * Returns workflowDetails list by workflowId
	 * 
	 * @param workflowId
	 * @return
	 * @throws Exception
	 * 
	 * @author Shoaib Ahmad Gondal
	 */
	public List<WorkflowDetail> getWorkflowDetailsByWorkflowId(Long workflowId)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery(
				"getWorkflowDetailsByWorkflowId", workflowId);
	}

	/**
	 * Returns workflowDetailprocess list by query string
	 * 
	 * @param countryId
	 *            ,process,operation,person,date period
	 * @return
	 * @throws Exception
	 * 
	 * @author Rafiq
	 */
	public List<WorkflowDetailProcess> getWorkflowDetailProcessForBusinessLog(
			WorkflowLogVO logVO) throws Exception {
		Criteria businessLog = workflowDetailProcessDAO.createCriteria();

		/*
		 * if (logVO.getFromDate() != null && logVO.getToDate() != null) {
		 * businessLog .add(Restrictions.gt("createdDate", logVO.getFromDate())
		 * Restrictions.gt("createdDate", logVO.getFromDate()))
		 * .add(Restrictions.lt("createdDate", logVO.getToDate()));
		 * 
		 * }
		 */
		if (logVO.getFromDate() != null) {
			businessLog.add(Restrictions.conjunction().add(
					Restrictions.ge("createdDate", logVO.getFromDate())));
		}
		if (logVO.getToDate() != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(logVO.getToDate());
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			businessLog.add(Restrictions.conjunction().add(
					Restrictions.lt("createdDate", calendar.getTime())));
		}

		/*
		 * businessLog.setFetchMode("workflowDetail", FetchMode.JOIN)
		 * .setFetchMode("workflowDetail.workflow", FetchMode.JOIN)
		 * .setFetchMode("workflowDetail.workflow.moduleProcess",
		 * FetchMode.JOIN);
		 */

		businessLog.createAlias("workflowDetail", "wd");
		businessLog.createAlias("workflowDetail.workflow", "wf");
		businessLog.createAlias("workflowDetail.workflow.moduleProcess",
				"process");
		businessLog.createAlias("messages", "msgs");

		if (logVO.getOperation() != null) {
			businessLog.add(Restrictions.eq("operation", logVO.getOperation()));
		}

		if (logVO.getProcessId() != null) {
			businessLog.add(Restrictions.eq("process.processId",
					logVO.getProcessId()));
		}

		if (logVO.getPersons() != null && logVO.getPersons().length > 0) {
			businessLog.add(Restrictions.in("createdBy", logVO.getPersons()));
		}

		// Apply filter only Saved,Rejected,Approved,Published records
		businessLog.add(Restrictions.in("operation", logVO
				.getWorkflowConstants().keySet()));

		return businessLog.list();

	}

	public Boolean setRecordStatusAsDeleteOrWaitingForDelete(
			String entityClassFullyQualifiedName, Object recordId, Object code)
			throws Exception {
		Boolean flag = true;
		/* Get EntityDAO object */
		AIOTechGenericDAO<Object> dao = WorkflowUtils
				.getEntityDAO(entityClassFullyQualifiedName);

		/* call findById method to get given Entity object by Id */
		Object object = dao.findById((Number) recordId);
		Object relativeId = null;
		if (object != null) {
			Method method = object.getClass().getMethod("getRelativeId");
			relativeId = method.invoke(object, null);
		}
		List<Object> relativeObject = null;
		if (relativeId != null)
			relativeObject = dao.findByRelativeId((Number) relativeId);

		try {
			Class[] argTypes = new Class[] { Byte.class };

			if (object != null) {
				Method method = object.getClass().getMethod("setIsApprove",
						argTypes);
				method.invoke(object, Byte.valueOf(code + ""));
				dao.saveOrUpdate(object);
			}
			// Update the is approve status as like as the current record status
			if (relativeObject != null) {
				Method method = relativeObject.get(0).getClass()
						.getMethod("setIsApprove", argTypes);
				method.invoke(relativeObject.get(0), Byte.valueOf(code + ""));
				dao.saveOrUpdate(relativeObject.get(0));
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}

		return flag;
	}

	/**
	 * This Method fetch all the operations
	 * 
	 * @return List<Operation> : all the processes of application
	 * 
	 * @author Mohamed Rabeek
	 */

	public List<OperationMaster> getAllOperations(Long processId)
			throws Exception {
		return operationMasterDAO.findByNamedQuery("getAllOperations",
				processId, WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method fetch all the operations
	 * 
	 * @return List<Operation> : all the processes of application
	 * 
	 * @author Mohamed Rabeek
	 */

	public List<OperationMaster> getAllOperations() throws Exception {
		return operationMasterDAO.findByNamedQuery(
				"getAllOperationsWithoutProcess",
				WorkflowUtils.getImplementationId());
	}

	/**
	 * This Method save the operation
	 * 
	 * 
	 * @author Mohamed Rabeek
	 */

	public void saveOperation(OperationMaster operationMaster) throws Exception {
		operationMasterDAO.saveOrUpdate(operationMaster);
	}

	/**
	 * This Method save the operation
	 * 
	 * 
	 * @author Mohamed Rabeek
	 */

	public void deleteOperation(OperationMaster operationMaster)
			throws Exception {
		operationMasterDAO.delete(operationMaster);
	}

	public OperationMaster getOperationById(Long operationId) throws Exception {
		return operationMasterDAO.findById(operationId);
	}

	public boolean quantumRuleValidationCheck(Long recordId,
			WorkflowDetail workflowDetail) {
		boolean returnStatus = true;
		try {
			if (workflowDetail != null
					&& workflowDetail.getQuantumValue() != null
					&& !workflowDetail.getQuantumValue().equalsIgnoreCase("")) {

				Object object = this.getRecord(workflowDetail.getWorkflow()
						.getModuleProcess().getUseCase(), recordId);
				Method method = object.getClass()
						.getMethod(
								"get"
										+ workflowDetail.getQuantumField()
												.substring(0, 1).toUpperCase()
										+ workflowDetail.getQuantumField()
												.substring(1));
				Object value = (Object) method.invoke(object, null);
				if (value.getClass().getName().equalsIgnoreCase("java.lang.Double")
						|| value.getClass().getName()
								.equalsIgnoreCase("java.lang.Byte")
						|| value.getClass().getName().equalsIgnoreCase("java.lang.Integer")
						) {
					if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase("<")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) < Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase(">")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) < Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase("<=")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) <= Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase(">=")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) >= Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase("=")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) == Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase("!=")) {
						if (Double
								.parseDouble(workflowDetail.getQuantumValue()) != Double
								.parseDouble(value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					}

				} else if (value.getClass().getName()
						.equalsIgnoreCase("java.lang.String")
						|| value.getClass().getName().equalsIgnoreCase("java.lang.Char")) {
					if (workflowDetail.getQuantumCondition().trim()
							.equalsIgnoreCase("=")) {
						if (workflowDetail.getQuantumValue().equalsIgnoreCase(
								value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					} else {
						if (!workflowDetail.getQuantumValue().equalsIgnoreCase(
								value.toString()))
							returnStatus = true;
						else
							returnStatus = false;
					}
				} else {
					returnStatus = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnStatus = false;
		}
		return returnStatus;
	}

	/*
	 * GETTERS AND SETTERS =====================
	 */

	public AIOTechGenericDAO<Workflow> getWorkflowDAO() {
		return workflowDAO;
	}

	public void setWorkflowDAO(AIOTechGenericDAO<Workflow> workflowDAO) {
		this.workflowDAO = workflowDAO;
	}

	public AIOTechGenericDAO<WorkflowDetail> getWorkflowDetailDAO() {
		return workflowDetailDAO;
	}

	public void setWorkflowDetailDAO(
			AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO) {
		this.workflowDetailDAO = workflowDetailDAO;
	}

	public AIOTechGenericDAO<Module> getModuleDAO() {
		return moduleDAO;
	}

	public void setModuleDAO(AIOTechGenericDAO<Module> moduleDAO) {
		this.moduleDAO = moduleDAO;
	}

	public AIOTechGenericDAO<ModuleProcess> getModuleProcessDAO() {
		return moduleProcessDAO;
	}

	public void setModuleProcessDAO(
			AIOTechGenericDAO<ModuleProcess> moduleProcessDAO) {
		this.moduleProcessDAO = moduleProcessDAO;
	}

	public AIOTechGenericDAO<Screen> getScreenDAO() {
		return screenDAO;
	}

	public void setScreenDAO(AIOTechGenericDAO<Screen> screenDAO) {
		this.screenDAO = screenDAO;
	}

	public AIOTechGenericDAO<UserRole> getUserRoleDAO() {
		return userRoleDAO;
	}

	public void setUserRoleDAO(AIOTechGenericDAO<UserRole> userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}

	public AIOTechGenericDAO<WorkflowDetailProcess> getWorkflowDetailProcessDAO() {
		return workflowDetailProcessDAO;
	}

	public void setWorkflowDetailProcessDAO(
			AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO) {
		this.workflowDetailProcessDAO = workflowDetailProcessDAO;
	}

	public AIOTechGenericDAO<User> getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(AIOTechGenericDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

	public AIOTechGenericDAO<OperationMaster> getOperationMasterDAO() {
		return operationMasterDAO;
	}

	public void setOperationMasterDAO(
			AIOTechGenericDAO<OperationMaster> operationMasterDAO) {
		this.operationMasterDAO = operationMasterDAO;
	}

}
