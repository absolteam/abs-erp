/**
 * 
 */
package com.aiotech.aios.workflow.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.util.WorkflowUtils;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

/**
 * @author Haris
 * 
 */
public class UserRoleService {

	private AIOTechGenericDAO<Role> roleDAO;
	private AIOTechGenericDAO<UserRole> userRoleDAO;

	private AIOTechGenericDAO<User> userDAO;

	/**
	 * returns list of user roles against user name.
	 * 
	 * @param user
	 *            object
	 * 
	 * @return list of user roles.
	 */
	public List<UserRole> getUserRoles(User user) throws Exception {
		return userRoleDAO.findByNamedQuery("getUserRoles", user.getUsername());
	}

	/**
	 * returns list of roles by role name message id.
	 * 
	 * @param role
	 *            name
	 * 
	 * @return list of roles.
	 */
	public List<Role> getRoleByName(String roleName) {
		return roleDAO.findByNamedQuery("getRoleByName", roleName);
	}

	/**
	 * saves the user role object in database.
	 * 
	 * @param user
	 *            role object.
	 * 
	 * 
	 */
	public void addUserRole(UserRole userRole) throws Exception {
		userRoleDAO.saveOrUpdate(userRole);
	}

	/**
	 * deletes the list of user roles
	 * 
	 * @param list
	 *            of user roles.
	 * 
	 */
	public void deleteUserRoles(List<UserRole> userRoles) throws Exception {
		userRoleDAO.deleteAll(userRoles);
	}

	/**
	 * returns user role object agaist user role id. message id.
	 * 
	 * @param user
	 *            role id
	 * 
	 * @return user role object.
	 */
	public UserRole getUserRole(long id) throws Exception {
		return userRoleDAO.findById(id);
	}

	/**
	 * saves the list of user roles in database.
	 * 
	 * @param user
	 *            roles list
	 * 
	 * 
	 */
	public void saveOrUpdateUserRoles(List<UserRole> userRolesList) {
		userRoleDAO.saveOrUpdateAll(userRolesList);
	}

	/**
	 * gets the user role object against the role id.
	 * 
	 * @param role
	 *            id.
	 * 
	 * @return role object.
	 */
	public Role getRole(long id) throws Exception {
		return roleDAO.findById(id);
	}

	/**
	 * returns all the available roles.
	 * 
	 * 
	 * @return list of roles.
	 */
	public List<Role> getAllRoles() throws Exception {
		return roleDAO.findByNamedQuery("getAllRoles",
				WorkflowUtils.getImplementationId());
	}

	/**
	 * returns list of roles against role area name.
	 * 
	 * @param role
	 *            area name
	 * 
	 * @return list roles.
	 */
	public List<Role> getRolesByRoleAreaName(String roleAreaName)
			throws Exception {
		return roleDAO.findByNamedQuery("getRolesByRoleAreaName", roleAreaName);
	}

	/**
	 * returns list of roles (which are active) against role area name.
	 * 
	 * @param role
	 *            area name
	 * 
	 * @return list roles.
	 */
	public List<Role> getActiveRolesByRoleAreaName(String roleAreaName)
			throws Exception {
		return roleDAO.findByNamedQuery("getActiveRolesByRoleAreaName",
				roleAreaName);
	}

	/**
	 * returns list of all roles except the roles list it is taking as input.
	 * 
	 * @param list
	 *            of role to be excluded from result.
	 * 
	 * @return list of roles.
	 */
	public List<Role> getRoles(List<String> rolesToExclude) throws Exception {

		Criteria roleCriteria = roleDAO.createCriteria();

		if (rolesToExclude != null) {
			for (String role : rolesToExclude) {
				roleCriteria.add(Restrictions.ne("roleName", role));
			}
		}

		return roleCriteria.list();
	}

	/**
	 * saves the role object in database.
	 * 
	 * @param role
	 *            object
	 * 
	 */
	public void saveRole(Role role) throws Exception {
		roleDAO.saveOrUpdate(role);
	}

	/*
	 * public List<Role> getAllRoles(Implementation implementation) throws
	 * Exception { return roleDAO.findByNamedQuery("getAllRoles",
	 * implementation); }
	 */

	/*
	 * public List<Role> getAllRolesExceptAdmin(Implementation implementation)
	 * throws Exception { return
	 * roleDAO.findByNamedQuery("getAllRolesExceptAdmin", implementation); }
	 */

	/**
	 * returns user object against user name.
	 * 
	 * @param username
	 *            .
	 * 
	 * @return user object.
	 */
	public User getUser(String id) {
		return userDAO.findById(id);
	}

	/*
	 * @Override public User getUser(String id, Implementation implementation) {
	 * return userDAO.findByNamedQuery("getUsers", id, implementation).get(0); }
	 */

	public User getUserWithPerson(String id) {
		return userDAO.findByNamedQuery("getUserWithPerson", id).get(0);
	}
	

	/**
	 * Get all user by passing the role Id from User screen.
	 * <p>
	 * Date: 27-November-13
	 * 
	 * @param user
	 * @throws Exception
	 * 
	 * @author Mohamed rabeek
	 */
	public List<User> getUsersByRole(Long id) {
		return userDAO.findByNamedQuery("getUsersByRole", id);
	}
	
	

	public User getUserWithPersonNRole(String id) {
		return userDAO.findByNamedQuery("getUserWithPersonNRole", id).get(0);
	}

	/*
	 * public void deleteUser(String id, Implementation implementation) {
	 * userDAO.delete(this.getUser(id, implementation)); }
	 */

	public void saveOrUpdateUser(User user) throws Exception {
		userDAO.saveOrUpdate(user);
	}

	/**
	 * Gets call when adding new user from User screen.
	 * <p>
	 * Date: 05-June-13
	 * 
	 * @param user
	 * @throws Exception
	 * 
	 * @author Shoaib Ahmad Gondal
	 */
	public void saveUser(User user) throws Exception {
		userDAO.save(user);
	}

	public List<User> saveOrUpdateAllUser(List<User> users) throws Exception {
		userDAO.saveOrUpdateAll(users);
		return users;
	}

	/*
	 * @Override public List<User> getAllUsers(Implementation implementation)
	 * throws Exception { return userDAO.findByNamedQuery("getAllUsers",
	 * implementation); }
	 */

	public List<User> getAllUsersByImplementation(Long implementationId) throws Exception {
		return userDAO.findByNamedQuery("getAllUsersByImplementation",implementationId);
	}

	public List<User> getAllUsers() throws Exception {
		return userDAO.getAll();
	}
	/**
	 * returns all unregistered users
	 * 
	 * 
	 * @return list of users.
	 */
	public List<User> getAllUnRegisteredUsers() throws Exception {
		return userDAO.findByNamedQuery("getAllUnRegisteredUsers");
	}

	/**
	 * returns all registered users
	 * 
	 * 
	 * @return list of users.
	 */
	public List<User> getAllRegisteredUsers() throws Exception {
		return userDAO.findByNamedQuery("getAllRegisteredUsers");
	}

	/**
	 * returns all registered users excluding admin user
	 * 
	 * 
	 * @return list of users.
	 */
	public List<User> getAllRegisteredUsersExcludingAdmin() throws Exception {
		List<User> allUsers = userDAO.findByNamedQuery("getAllRegisteredUsers");
		List<User> usersExcludingAdmins = new ArrayList<User>();
		Boolean isAdmin = false;
		for (User user : allUsers) {
			isAdmin = false;
			for (UserRole userRole : getUserRoles(user)) {
				if (userRole.getRole().getRoleName().equals("ADMIN")) {
					isAdmin = true;
					break;
				}
			}
			if (!isAdmin) {
				usersExcludingAdmins.add(user);
			}
		}
		return usersExcludingAdmins;
	}

	/**
	 * returns all active user excluding admin
	 * 
	 * 
	 * @return list of users.
	 */
	public List<User> getAllActiveUsersExcludingAdmin() throws Exception {
		List<User> allUsers = userDAO.findByNamedQuery("getAllActiveUsers");
		List<User> usersExcludingAdmins = new ArrayList<User>();
		Boolean isAdmin = false;
		for (User user : allUsers) {
			isAdmin = false;
			for (UserRole userRole : getUserRoles(user)) {
				if (userRole.getRole().getRoleName().equals("ADMIN")) {
					isAdmin = true;
					break;
				}
			}
			if (!isAdmin) {
				usersExcludingAdmins.add(user);
			}
		}
		return usersExcludingAdmins;
	}

	public List<User> getUserByPersonId(Long personId) throws Exception {
		return userDAO.findByNamedQuery("getUserByPersonId", personId);
	}

	public List<User> getActiveUserByPersonId(Long personId) throws Exception {
		return userDAO.findByNamedQuery("getActiveUserByPersonId", personId);
	}
	
	public List<User> getAllUsersOfRole(Long roleId, List<Long> personIds) throws Exception {

		List<Long> fakeList = new ArrayList<Long>();
		fakeList.add(0L);
		Query query = userDAO.getNamedQuery("getAllUsersOfRole");
		query.setParameter("roleId", roleId);
		query.setParameter("implementationId", WorkflowUtils.getImplementationId());
		query.setParameterList("personIds", personIds.size() > 0 ? personIds
				: fakeList);

		return query.list();

		// return personDAO.findByNamedQuery("getAllPersonsOfRole", roleId);
	}

	// ==============Getters Setters============================

	public AIOTechGenericDAO<Role> getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(AIOTechGenericDAO<Role> roleDAO) {
		this.roleDAO = roleDAO;
	}

	public AIOTechGenericDAO<UserRole> getUserRoleDAO() {
		return userRoleDAO;
	}

	public void setUserRoleDAO(AIOTechGenericDAO<UserRole> userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}

	public AIOTechGenericDAO<User> getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(AIOTechGenericDAO<User> userDAO) {
		this.userDAO = userDAO;
	}

}
