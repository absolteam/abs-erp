<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

	<div>
		<div class="content">
			<div class="header_panel ui-widget-header">
				<p>Workflow</p>
			</div>

			<s:form action="workflow-management" id="workflowManagementForm"
				theme="simple" method="post">
				<div class="bordered_panel">
					<table class="countryDropDownTable">
						<tr>
							<td class="countryDropDownTableLabel">Process:</td>
							<td class="countryDropDownTableField"><s:select
									name="processId" cssClass="combobox" id="moduleProcessesList"
									list="moduleProcesses" listValue="processDisplayName"
									listKey="processId" headerKey="-9999" headerValue="--Select--"
									onselect="onModuleProcessSelect();">

								</s:select></td>

						</tr>
					</table>
				</div>


				<div class="inner_panel">
					<div class="inner_panel_header ui-widget-header">
						<p>Details</p>
					</div>


					<%--For redefine workflow --%>
					<s:hidden id="selectedWorkflowId" name="selectedWorkflowId"  />
					<s:hidden id="selectedWorkflowIndex" name="selectedWorkflowIndex"  />
					<s:hidden id="isRedefine" name="isRedefine" value="false"  />
					<div id="processFlows"
						style="background: none !important; border: none !important;">
						
						<%--Content will be appended from javascript after process selection. --%>
					</div>
					
					<div id="addWorkflow" class="inner_panel" style="margin-top:1px; background: none; display: none;">
						<br />
						<table id="recentWorkflowTable" class="contentTable ui-datatable">
							<thead>
								<tr>
									<th class="ui-state-default">User/Role</th>
									<th class="ui-state-default">Operation</th>
									<th class="ui-state-default">Operation Orders</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
						
						<br />
						<table id="addWorkflowTable" style="width:50%; float:left;">
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
				    			<td>
				    				Operation
				    			</td>
				    			<td>
				    				<div id="type1Div">
					    				<select id="type1" class="combobox"
											style="margin-left: 2px;padding: 0px !important;">
											<option value="3">Entry</option>
										</select>
									</div>
									<div id="type2Div" style="display:none;">
					    				<select id="type2" class="combobox"
											style="margin-left: 2px;padding: 0px !important;">
											<option value="4">Approval</option>
											<option value="9">Publish</option>
										</select>
									</div>
				    			</td>

				    		</tr>
				    		<tr>
				    			<td>
				    				Access Right Type
				    			</td>
				    			<td>
				    				<select id="accessRight" class="combobox"
										style="margin-left: 2px;padding: 0px !important;">
										<option value="0">Role</option>
									</select>
				    			</td>

				    		</tr>
				    		<tr>
				    			<td>
				    				User/Role
				    			</td>
				    			<td>
				    				<div id="userRolesDiv">
				    					<select id="userRoles" class="combobox" style="margin-left: 2px;padding: 0px !important;">	
				    						<option value="0">-- Select --</option>
				    					</select>
									</div>
									<div id="personsDiv" style=" display:none;">
										<s:select id="persons" cssClass="combobox"
											list="persons" listKey="personId" listValue="firstName"
											cssStyle="margin-left: 2px;padding: 0px !important;">
											
										</s:select>
									</div>
				    			</td>

				    		</tr>
				    		<tr>
				    			<td>
				    				Operation Order
				    			</td>
				    			<td>
				    				<input type="text" id="operationOrder" value="1" style="width: 180px;" disabled="disabled" 
				    					class="validate[custom[number]]"/>
				    				
				    				<input type="hidden" id="operationNumber" name="operationNumber" value="1" />
				    			</td>

				    		</tr>
				    		
						</table>
						
						<%--Recent workflows list size --%>
						<s:hidden id="recentWorkflowListSize" name="recentWorkflowListSize" value="0"  />
						<%--To keep track whether user has defined flow for all the three processes --%>
						<s:hidden id="isEntry" name="isEntry" value="0"  />
						<s:hidden id="isApproval" name="isApproval" value="0"  />
						<s:hidden id="isPublish" name="isPublish" value="0"  />
						
						
						<br />
						<div align="right" style="margin-right: 2%;">
				    		<input type="button" class="buttons ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" 
				    			id="addWorkflowButton" value="Add" onclick="addWorkflow();" />
				    		
				    		<input type="button"
				    			class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover" 
				    			id="saveWorkflowButton" value="Save" onclick="saveWorkflows();" />
				    		
				    		<input type="button" class="buttons_c ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" 
				    			value="Cancel" onclick="location.reload();" />
				    	</div>
				    	<br />
					</div>
					<br />
				</div>
			</s:form>
		</div>
	</div>
<br />
