$(document).ready(function() {

	// initialize jQuery validation engine.
	$("#workflowManagementForm").validationEngine();

});

$("#moduleProcessesList").combobox({
	selected : function(event, ui) {

		// clear existing data
		$("#processFlows").empty();

		showProcessFlowsDiv();

		onModuleProcessSelect();
	}
});

$('#accessRight').combobox({
	selected : function(event, ui) {
		showUserOrRoleCombo();
	}
});

// initialize JQuery autocompletes
$('#type1').combobox();
$('#type2').combobox();

$('#userRoles')
		.combobox(
				{
					selected : function(event, ui) {

						var selectedOperationId = $('#type1 :selected').val();

						var selectedText = $(
								'#userRoles option[value='
										+ $('#userRoles :selected').val() + ']')
								.text();

						// if operation is Entry, validate selected role
						if ((selectedOperationId == '3')
								&& $('#type1Div').is(':visible')) {
							if (!validateSelectedRoleForEntry(selectedText)) {
								event.preventDefault();

								setComboBoxValue("#userRoles", 0);
							}
						}
					}
				});
$('#persons').combobox();

/**
 * Gets call when user selects a process from processes combo.
 * <p>
 * Date: 08-Apr-13
 * 
 * @author Usman Anwar
 */
function onModuleProcessSelect() {

	var id = $('#moduleProcessesList :selected').val();

	$
			.ajax({
				type : "POST",
				dataType : "json",
				url : "aios-web/onModuleProcessSelect.action",
				traditional : true,
				data : {

					processId : id,

				},
				success : function(response) {

					clearWorkflowForm();
					fillWorkflowForm(response);
				},
				error : function(e) {
					// showErrorWindow(e.responseText, false);
					console.log(e.responseText);
				}

			});
}

/**
 * Resets the fields to their default values as they were when page loads first
 * time.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function clearWorkflowForm() {
	$("#selectedWorkflowId").val(0);
	$("#selectedWorkflowIndex").val(0);
	$("#isRedefine").val(false);
	$("#operationNumber").val(1);
	$("#operationOrder").val(1);
	$("#operationOrder").attr('disabled', 'disabled');

	if ($("#accessRight option").length > 1) {
		$("#accessRight option:last").remove();
	}

	$("#userRoles option:not(:first)").remove();

	$("#recentWorlflowTable tbody").empty();

	$("#recentWorkflowListSize").val(0);
	$("#isEntry").val(0);
	$("#isApproval").val(0);
	$("#isPublish").val(0);
}

/**
 * Displays workflows defined for a process.
 * <p>
 * Date: 10-Apr-13
 * 
 * @param response
 *            the response from the server against the ajax call made after
 *            selection of a process.
 * 
 * @author Shoaib Ahmad Gondal
 */
function fillWorkflowForm(response) {

	try {

		var workflowVOs = JSON.parse(response.workflowVOsJSON);
		var roles = JSON.parse(response.rolesJSON);

		var htmlString = "";
		var workflow = null;

		// number of workflows defined for selected process
		for ( var i = 0; i < workflowVOs.length; i++) {
			workflow = workflowVOs[i];

			// operations detail for defined workflow
			var workflowDetails = workflow.workflowDetailVOs;

			htmlString = "<table id='"
					+ i
					+ "' class='contentTable ui-datatable'> <thead> <tr> <th class='ui-state-default'> User/Role </th> <th class='ui-state-default'> Operation </th> <th class='ui-state-default'> Operation Orders</th></tr></thead><tbody class='ui-datatable-data ui-widget-content'>";

			for ( var j = 0; j < workflowDetails.length; j++) {

				var workflowDetail = workflowDetails[j];

				htmlString = htmlString + "<tr class='ui-widget-content'><td>";

				if (workflowDetail.role.roleId != null) {
					htmlString = htmlString + workflowDetail.role.roleName;

				} else if (workflowDetail.personId != null) {
					htmlString = htmlString + workflowDetail.personId
							+ " " + workflowDetail.personId;
				}

				htmlString = htmlString + "</td><td>";

				if (workflowDetail.operation === 4) {
					htmlString = htmlString + "Approval";
				} else if (workflowDetail.operation === 2) {
					htmlString = htmlString + "Reject";
				} else if (workflowDetail.operation === 3) {
					htmlString = htmlString + "Entry";
				} else if (workflowDetail.operation === 9) {
					htmlString = htmlString + "Publish";
				} else if (workflowDetail.operation === 5) {
					htmlString = htmlString + "Publish Read";
				} else if (workflowDetail.operation === 6) {
					htmlString = htmlString + "Confirm Read";
				} else if (workflowDetail.operation === 7) {
					htmlString = htmlString + "Deny Read";
				} else if (workflowDetail.operation === 8) {
					htmlString = htmlString + "Approved";
				} else if (workflowDetail.operation === 1) {
					htmlString = htmlString + "Published";
				}

				htmlString = htmlString + "</td><td>";

				htmlString = htmlString + workflowDetail.operationOrder;

				htmlString = htmlString + "</td></tr>";

			}

			htmlString = htmlString + "</tbody></table></br></br>";

			// append 'Redefine workflow' button
			htmlString = htmlString
					+ "<div style='margin-left: 20px;'><input type='button' value='Re-Define' style='height:28px; width: 91px;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover' onclick='confirmRedefine("
					+ workflow.workflowId + "," + i
					+ ", true)'/></div><br /><br/>";

			$("#processFlows").append(htmlString);

		}

		// Append 'Add new workflow' button
		htmlString = "<br /><div style='margin-left: 20px;'><input type='button' value='Add New Workflow' style='height:28px; width: 150px;' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only buttons ui-state-hover' "
				+ "onclick='redefineWorkflow(-1,-1,0); showAddWorkflowDiv();' /></div><br />";
		$("#processFlows").append(htmlString);

		htmlString = "";
		// append roles to userRoles combo
		for ( var i = 0; i < roles.length; i++) {
			htmlString = htmlString + "<option value=" + roles[i].roleId + ">"
					+ roles[i].roleName + "</option>";
		}

		$("#userRoles").append(htmlString);

	} catch (e) {
		console.log(e.message);
	}

}

/**
 * Gets Call When user clicks on Re-Define button.
 * 
 * @param workflowId
 *            id of the workflow to deactivate.
 * @param workflowIndex
 */
function confirmRedefine(workflowId, workflowIndex, isRedefine) {

	try {

		alertify.set({
			labels : {
				ok : "Continue",
				cancel : "Cancel"
			}
		});

		alertify.set({
			buttonReverse : true
		});

		alertify
				.confirm(
						"Adding workflow will terminate all the operations currently being executed under this process. <br /> Do you want to continue?",
						function(e) {
							if (e) {
								// user clicked "ok"
								redefineWorkflow(workflowId, workflowIndex,
										isRedefine);

								showAddWorkflowDiv();
							} else {
								// user clicked "cancel"
							}
						});

	} catch (e) {
		console.log(e.message);
	}

}

/**
 * Gets call when user confirms redefining a workflow. Sets some values to get
 * track that which workflow record was redefined.
 * <p>
 * Date: 10-Apr-13
 * 
 * @param workflowId
 *            the id of the workflow record.
 * @param workflowIndex
 * @param isRedefine
 *            false, true
 * 
 * @author Shoaib Ahmad Gondal
 */
function redefineWorkflow(workflowId, workflowIndex, isRedefine) {

	$("#selectedWorkflowId").val(workflowId);
	$("#selectedWorkflowIndex").val(workflowIndex);
	$("#isRedefine").val(isRedefine);
}

/**
 * Shows entry form to define workflow steps and hides existing workflow tables
 * div.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showProcessFlowsDiv() {
	$("#processFlows").show();
	$("#addWorkflow").hide();
}

/**
 * Shows existing workflow tables div and hides entry form to define workflow
 * steps.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showAddWorkflowDiv() {
	$("#addWorkflow").show();
	$("#processFlows").hide();
}

/**
 * Gets call when user is defining workflow and presses 'Add' button after
 * selecting Operation, Access Right, Role.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function addWorkflow() {

	try {

		if ($("#operationNumber").val() == 1) { // first operation (Entry)

			if ($("#userRoles").val() != 0) { // user has selected a role

				var type = $("#type1").val();
				var accessRight = $("#accessRight").val();
				var userRole = $("#userRoles").val();
				var userRoleText = $("#userRoles option:selected").text();
				var operationOrder = $("#operationOrder").val();

				addRowToRecentWorkflowTable(type, accessRight, true, userRole,
						userRoleText, operationOrder);

				showOperationType2Div();

				onFirstOperationAdd();

			}
		} else { // operations other than entry

			if (validateEntry()) {

				var type = $("#type2").val();
				var accessRight = $("#accessRight").val();
				var userRole;
				var userRoleText;
				var isRole = true;
				if (accessRight == 0) {
					userRole = $("#userRoles").val();
					userRoleText = $("#userRoles option:selected").text();
				} else {
					userRole = $("#persons").val();
					userRoleText = $("#persons option:selected").text();
					isRole = false;
				}

				var operationOrder = $("#operationOrder").val();

				addRowToRecentWorkflowTable(type, accessRight, isRole,
						userRole, userRoleText, operationOrder);

			}
		}

	} catch (e) {
		console.log(e.message);
	}

}

function saveWorkflows() {

	if (checkAllFlowsDefined()) {
		confirmSave();
	}
}

function confirmSave() {

	try {

		alertify.set({
			labels : {
				ok : "Continue",
				cancel : "Cancel"
			}
		});

		alertify.set({
			buttonReverse : true
		});

		alertify.confirm("Do you want to Save the changes?", function(e) {
			if (e) {
				// user clicked "ok", submit form
				$("#workflowManagementForm").submit();
			} else {
				// user clicked "cancel"
			}
		});

	} catch (e) {
		console.log(e.message);
	}
}

/**
 * Displays combo with operations 'Approval' and 'Publish'. Gets call when user
 * adds first operation 'Entry' while defining workflow.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showOperationType2Div() {
	$("#type1Div").hide();
	$("#type2Div").show();
}

/**
 * Displays combo with operation 'Entry'. Gets call when there is requirement to
 * show first operation only.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showOperationType1Div() {
	$("#type1Div").show();
	$("#type2Div").hide();
}

/**
 * Gets Call when user selects 'Role' or 'User' from Access Right Type combo. If
 * Role is selected, then combo containing roles will be shown, else combo with
 * person's names will be shown.
 * <p>
 * Date: 10-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function showUserOrRoleCombo() {
	var selectedValue = $("#accessRight").val();
	if (selectedValue == 0) {
		$("#userRolesDiv").show();
		$("#personsDiv").hide();
	} else if (selectedValue == 1) {
		$("#userRolesDiv").hide();
		$("#personsDiv").show();
	}
}

/**
 * Validates process entry after entry process.
 * <p>
 * Date: 11-Apr-13
 * 
 * @returns true if validation successful else false.
 * 
 * @author Shoaib Ahmad Gondal
 */
function validateEntry() {
	var result = true;

	// check if user has entered valid operation order
	var operationOrder = $("#operationOrder").val();
	var operationOrderIntValue = parseInt(operationOrder, 0);
	if (isNaN(operationOrderIntValue)
			|| ((operationOrderIntValue <= 0) || operationOrderAlreadyDefined(operationOrderIntValue))) {
		alertify.alert("Please Enter Valid Operation Order");
		$("#operationOrder").val("");
		$("#operationOrder").focus();

		result = false;
	}

	// if user has selected approval, then set isApproval to 1
	if ($("#type2").val() == 4) {
		$("#isApproval").val(1);
	}

	// if user has selected publish, then set isPublish to 1
	if ($("#type2").val() == 9) {
		$("#isPublish").val(1);
	}

	return result;
}

/**
 * Gets call to validate that the entered operation order number is not already
 * entered.
 */
function operationOrderAlreadyDefined(operationOrderToCheck) {
	var result = false;

	try {
		$('#recentWorkflowTable tbody tr').each(function(index, element) {
			var operationOrder = this.childNodes[2].textContent;
			var intValue = Number(operationOrder);

			if (operationOrderToCheck === intValue) {
				result = true;
			}
		});

	} catch (e) {
		console.log(e.message);
	}

	return result;
}

/**
 * Returns size of the recently added workflows.
 * <p>
 * Date: 11-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function getRecentWorkflowListSize() {

	try {
		var size = $("#recentWorkflowListSize").val();
		var sizeIntValue = parseInt(size, 0);

		return sizeIntValue;
	} catch (e) {
		console.log(e.message);
	}
}

/**
 * Increments size by one of recently added workflow's list.
 * <p>
 * Date: 11-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function setRecentWorkflowListSize() {
	try {
		var size = $("#recentWorkflowListSize").val();
		var sizeIntValue = parseInt(size, 0);
		sizeIntValue = sizeIntValue + 1;

		$("#recentWorkflowListSize").val(sizeIntValue);
	} catch (e) {
		console.log(e.message);
	}
}

function addRowToRecentWorkflowTable(type, accessRight, isRole, userRole,
		userRoleText, operationOrder) {

	var recentWorkflowListSize = getRecentWorkflowListSize();

	var htmlString = "<tr>";

	if (isRole) {
		htmlString = htmlString
				+ "<td><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].role.roleName' value='"
				+ userRoleText
				+ "'/><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].role.roleId' value='" + userRole
				+ "' /><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].accessRightType' value='"
				+ accessRight + "' />" + userRoleText + "</td>";
	} else {
		htmlString = htmlString
				+ "<td><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].person.firstName' value='"
				+ userRoleText
				+ "'/><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].person.personId' value='"
				+ userRole + "' />" + userRoleText + "</td>";
	}

	if (type == 3) {
		htmlString = htmlString
				+ "<td><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].operation' value='" + type
				+ "' />" + $("#type1 option:selected").text() + "</td>";
	} else {
		htmlString = htmlString
				+ "<td><input type='hidden' name='recentWorkflows["
				+ recentWorkflowListSize + "].operation' value='" + type
				+ "' />" + $("#type2 option:selected").text() + "</td>";
	}

	htmlString = htmlString + "<td><input type='hidden' name='recentWorkflows["
			+ recentWorkflowListSize + "].operationOrder' value='"
			+ operationOrder + "' />" + operationOrder + "</td>";

	htmlString = htmlString + "</tr>";

	$("#recentWorkflowTable tbody").append(htmlString);

	// increment size by one, as one row is added.
	setRecentWorkflowListSize();
}

/**
 * Performs changes after addition of entry operation.
 * <p>
 * Date: 11-Apr-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function onFirstOperationAdd() {

	// change value to indicate first operation is done.
	$("#operationNumber").val(2);

	// Add User Selection to Access Right Type combo
	$("#accessRight").append("<option value='1'>User</option>");

	// enable operationorder textfield
	$("#operationOrder").removeAttr('disabled');
	$("#operationOrder").val(2);

	// set isEntry to 1, as user has defined entry process.
	if ($("#type1").val() == 3) {
		$("#isEntry").val(1);
	}
}

/**
 * Fires when user clicks to Save workflow.
 * <p>
 * Checks whether user has defined all the three steps (Entry, approval,
 * publish) for the workflow.
 * <p>
 * Date: 11-Apr-13
 * 
 * @returns true if validated, else false
 * 
 * @author Shoaib Ahmad Gondal
 */
function checkAllFlowsDefined() {

	var result = true;

	var isEntry = $("#isEntry").val();
	var isApproval = $("#isApproval").val();
	var isPublish = $("#isPublish").val();

	if (isEntry == 0) {
		alertify.alert("Workflow for Entry operation is missing.");

		result = false;
	}

	if (isApproval == 0) {
		alertify.alert("Workflow must have atleast one Approval operation.");

		result = false;
	}

	if (isPublish == 0) {
		alertify.alert("Workflow for Pulish operation is missing.");

		result = false;
	}

	return result;
}

/**
 * Gets call when user selects a role for entry.
 * <p>
 * Validates that the selected role shouldn't have entry right in any other
 * workflow of the same process.
 * <p>
 * Date: 31-May-13
 * 
 * @author Shoaib Ahmad Gondal
 */
function validateSelectedRoleForEntry(selectedRole) {

	var result = true;
	try {

		// iterate existing workflows
		$('#processFlows table').each(function(index, element) {

			/*
			 * In case of redefining, skip that table
			 */
			if (isRedefine()) {
				var tableId = $("#selectedWorkflowIndex").val();

				if (!(this.id == tableId)) {
					var tr = element.childNodes[2].childNodes[0];
					var role = tr.childNodes[0].textContent;

					if (selectedRole == role) {

						result = false;
					}
				}
			} else {
				var tr = element.childNodes[2].childNodes[0];
				var role = tr.childNodes[0].textContent;

				if (selectedRole == role) {

					result = false;
				}
			}

		});

		if (!result) {
			showErrorMessage(
					"Invalid Role",
					"Following role can not be selected as it has Entry right in another workflow of same process");
		}
	} catch (e) {
		console.log(e.message);
	}

	return result;
}

function isRedefine() {
	var result = false;

	if ($("#isRedefine").val() == "true") {
		result = true;
	}

	return result;
}