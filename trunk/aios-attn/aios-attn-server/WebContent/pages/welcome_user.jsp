<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/general.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/HRM.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/Style_new.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/tables.css" type="text/css" />


<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
$(function(){   
	$('#register').click(function(){  
		var name=$('#name').val();
		var code=$('#code').val();
		var company=$('#company').val();
		var seqCount=$('#seqCount').val();
		var ipAddress=$('#ipAddress').val();
		var port=$('#port').val();
		var deviceRegistrationId=Number($('#deviceRegistrationId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/save-device.action", 
	     	async: false,
	     	data:{deviceRegistrationId:deviceRegistrationId,name:name,accessCode:code,
	     		company:company,seqCount:seqCount,ipAddress:ipAddress,port:port},
			dataType: "json",
			cache: false,
			success: function(response){ 
 				if(response.returnMessage=="SUCCESS"){
					window.location.reload();
				}else{
					alert("Error in save");
					return false;
				} 
			} 		
		}); 
		
	}); 

	$('#register-user').click(function(){  
		var name=$('#employee').val();
		var userId=Number($('#userId').val());
		var deviceRegistrationId=Number($('#selectDeviceRegistrationId').val());
		var userReference=Number($('#userId').val());
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/save-user.action", 
	     	async: false,
	     	data:{userId:userId,deviceRegistrationId:deviceRegistrationId,
	     		userReference:userReference,name:name},
			dataType: "json",
			cache: false,
			success: function(response){ 
 				if(response.returnMessage=="SUCCESS"){
					window.location.reload();
				}else{
					alert("Error in save");
					return false;
				} 
			} 		
		}); 
		
	}); 
	
	$('#get-logs').click(function(){  
	
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/get-logs.action", 
	     	async: false,
			dataType: "json",
			cache: false,
			success: function(response){ 
 				if(response.returnMessage=="SUCCESS"){
					window.location.reload();
				}else{
					alert("Error in save");
					return false;
				} 
			} 		
		}); 
		
	}); 
	
	$('#show-logs').click(function(){  
		
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show-logs.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(response){ 
				$('#main-content').html(response);
			} 		
		}); 
		
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<div style="width: 100%;float: left;" id="device-registration-div">
				<div style="width: 50%;margin: 10px;">
					<div style="width: 100%;margin: 10px;">
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">IP Address : </label><input type="text" id="ipAddress"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Port : </label><input type="text" id="port"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Name : </label><input type="text" id="name"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Access Code : </label><input type="text" id="code"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Company : </label><input type="text" id="company"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Seq Count : </label><input type="text" id="seqCount"  style="width: 50%;"></div>
						<input type="hidden" id="deviceRegistrationId"/>
					</div>
					<div style="width: 60%;margin: 10px;">
						<button id="edit" style="float: right;position: relative;margin: 10px;">Edit</button>
						<button id="register" style="float: right;position: relative;margin: 10px;">Register</button>
					</div>
				</div>
				<div style="width: 50%;margin: 10px;" id="device-list">
					<table style="width: 99%; margin: 0 auto;">
							<tr>  
								<th>S. No.</th>
								<th>Name</th>
								<th>Access Code</th>
								<th>Company</th>
								<th>SeqCount</th>
							</tr>
							<c:set var="cnt" value="1" />
							<c:forEach items="${DEVICES}" var="details">
							<tr> 
								<td>
									${cnt}
									<c:set var="cnt" value="${cnt + 1}" />
								</td>
								<td>
									${details.name}
								</td>
								<td>
									${details.accessCode}
								</td>
								<td>
									${details.company}
								</td>
								<td>
									${details.seqCount}
								</td>
							</tr>
							</c:forEach>
					</table>
				</div>
			</div>
			
			<div style="width: 100%;float: left;" id="user-registration-div">
				<div style="width: 50%;margin: 10px;">
					<div style="width: 100%;margin: 10px;">
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Device : </label>
							<select id="selectDeviceRegistrationId" >
								<c:forEach var="DETAIL"
												items="${DEVICES}"
												varStatus="status">
													<option value="${DETAIL.deviceRegistrationId}">${DETAIL.name}</option>
								</c:forEach>
							</select>
						</div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">Employee : </label><input type="text" id="employee"  style="width: 50%;"></div>
						<div style="width: 90%;float: left;"><label style="width: 20%;float: left;">User ID : </label><input type="text" id="userId"  style="width: 50%;"></div>
						<input type="hidden" id="userId"/>
					</div>
					<div style="width: 60%;margin: 10px;">
						<button id="edit-user" style="float: right;position: relative;margin: 10px;">Edit</button>
						<button id="register-user" style="float: right;position: relative;margin: 10px;">Register</button>
					</div>
				</div>
				<div style="width: 50%;margin: 10px;" id="user-list">
					<table style="width: 99%; margin: 0 auto;">
							<tr>  
								<th>S. No.</th>
								<th>Employee</th>
								<th>User ID</th>
							</tr>
							<c:set var="cnt" value="1" />
							<c:forEach items="${USERS}" var="details">
								<tr> 
									<td>
										${cnt}
										<c:set var="cnt" value="${cnt + 1}" />
									</td>
									<td>
										${details.employee}
									</td>
									<td>
										${details.userReference}
									</td>
								</tr>
							</c:forEach>
					</table>
				</div>
			</div>
		</div>
		<button id="show-logs" style="float: right;position: relative;margin: 10px;">Show Logs</button>
		<button id="get-logs" style="float: right;position: relative;margin: 10px;">Get Logs From Device</button>
	</div>
</div>