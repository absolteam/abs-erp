<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/general.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/HRM.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/Style_new.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/tables.css" type="text/css" />


<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
$(function(){   
	$('#cancel').click(function(){  
		alert("");
			window.location.reload();
		}); 
		
	}); 
	
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<div style="width: 100%;float: left;" id="device-registration-div">
				
				
			
			<div style="width: 100%;float: left;">
				
				<div style="width: 50%;margin: 10px;" id="log-list">
					<table style="width: 99%; margin: 0 auto;">
							<tr>  
								<th>S. No.</th>
								<th>Event No.</th>
								<th>Device</th>
								<th>Employee</th>
								<th>User ID</th>
								<th>Date</th>
								<th>Time</th>
								<th>Event Type</th>
							</tr>
							<c:set var="cnt" value="1" />
							<c:forEach items="${EVENTS}" var="details">
								<tr> 
									<td>
										${cnt}
										<c:set var="cnt" value="${cnt + 1}" />
									</td>
									<td>
										${details.eventId}
									</td>
									<td>
										${details.name}
									</td>
									<td>
										${details.employee}
									</td>
									<td>
										${details.userReference}
									</td>
									<td>
										${details.dateView}
									</td>
									<td>
										${details.timeView}
									</td>
									<td>
										${details.eventType}
									</td>
								</tr>
							</c:forEach>
					</table>
				</div>
			</div>
			<button id="cancel" style="float: right;position: relative;margin: 10px;">Main Window</button>
			
		</div>
	</div>
</div>