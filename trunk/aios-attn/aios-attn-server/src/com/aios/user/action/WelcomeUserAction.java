package com.aios.user.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpRequest;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class WelcomeUserAction extends ActionSupport {

	private String name;
	private String accessCode;
	private String employee;
	private Integer userReference;
	private String company;
	private Date date;
	private Date time;
	private String dateView;
	private String seqCount;
	private Long userId;
	private Long deviceRegistrationId;
	private Long eventsLogId;
	private Long implementationId;
	private AttendanceVO vo;
	private List<AttendanceVO> vos;
	private String returnMessage;
	private String ipAddress;
	private Integer port;
	private String payLoad;

	public String getPayLoad() {
		return payLoad;
	}

	public void setPayLoad(String payLoad) {
		this.payLoad = payLoad;
	}

	// all struts logic here
	public String execute() {

		try {
			ServletActionContext.getRequest().setAttribute("DEVICES",
					DBConnector.getDeviceList());
			ServletActionContext.getRequest().setAttribute("USERS",
					DBConnector.getUserList());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String saveDevice() {
		try {
			AttendanceVO attendanceVO = new AttendanceVO();
			attendanceVO.setCompany(company);
			attendanceVO.setAccessCode(accessCode);
			if (deviceRegistrationId != null && deviceRegistrationId > 0)
				attendanceVO.setDeviceRegistrationId(deviceRegistrationId);
			attendanceVO.setIpAddress(ipAddress);
			attendanceVO.setPort(port);
			attendanceVO.setName(name);
			attendanceVO.setSeqCount(seqCount);
			AiosHttpClient.sendDevicePost(attendanceVO);
			List<AttendanceVO> devices = DBConnector.getDeviceList();
			Map<String, AttendanceVO> deviceMaps = new HashMap<String, AttendanceVO>();
			for (AttendanceVO vo : devices) {
				deviceMaps.put(vo.getAccessCode(), vo);
			}

			if (deviceMaps.get(attendanceVO.getAccessCode()) != null)
				DBConnector.saveDevice(attendanceVO);

			returnMessage = "SUCCESS";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnMessage = "ERROR";
		}
		return SUCCESS;
	}

	public String getLogs() {
		try {
			DBConnector.batchUpdateFromDevice();
			returnMessage = "SUCCESS";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnMessage = "ERROR";
		}
		return SUCCESS;
	}

	public String saveUser() {
		try {
			AttendanceVO attendanceVO = new AttendanceVO();
			attendanceVO.setDeviceRegistrationId(deviceRegistrationId);
			attendanceVO = DBConnector.getDeviceInfo(attendanceVO);
			attendanceVO.setUserReference(userReference);
			/*
			 * if(userId!=null && userId>0) attendanceVO.setUserId(userId);
			 */

			attendanceVO.setName(name);
			AiosHttpClient.sendUserPost(attendanceVO);
			DBConnector.saveUser(attendanceVO);

			returnMessage = "SUCCESS";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnMessage = "ERROR";
		}
		return SUCCESS;
	}

	public String getAllEvents() {

		try {

			List<AttendanceVO> users = DBConnector.getUserList();
			Map<Long, AttendanceVO> userMaps = new HashMap<Long, AttendanceVO>();
			for (AttendanceVO attendanceVO : users) {
				userMaps.put(attendanceVO.getUserId(), attendanceVO);
			}

			List<AttendanceVO> devices = DBConnector.getDeviceList();
			Map<Long, AttendanceVO> deviceMaps = new HashMap<Long, AttendanceVO>();
			for (AttendanceVO attendanceVO : devices) {
				deviceMaps.put(attendanceVO.getDeviceRegistrationId(),
						attendanceVO);
			}
			ServletActionContext.getRequest().setAttribute("EVENTS",
					DBConnector.getEventList(userMaps, deviceMaps));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String updateIpAddress() {

		try {

			List<AttendanceVO> devices = DBConnector.getDeviceList();
			Map<String, AttendanceVO> deviceMaps = new HashMap<String, AttendanceVO>();
			for (AttendanceVO attendanceVO : devices) {
				deviceMaps.put(attendanceVO.getAccessCode(), attendanceVO);
			}
			vo = deviceMaps.get(accessCode);
			HttpServletRequest request = ServletActionContext.getRequest();
			String ipAddres = ServletActionContext.getRequest().getHeader(
					"X-FORWARDED-FOR");
			if (ipAddres == null) {
				ipAddres = request.getRemoteAddr();
				vo.setIpAddress(ipAddres);
				vo.setSeqId(Integer.valueOf(vo.getSeqCount()));
			}
			List<AttendanceVO> vos = new ArrayList<AttendanceVO>();
			vos.add(vo);
			DBConnector.updateDeviceInfo(vos);
			returnMessage = "SUCCESS";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			returnMessage = "ERROR";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String callFromClient() {

		try {

			List<AttendanceVO> devices = DBConnector.getDeviceList();
			Map<String, AttendanceVO> deviceMaps = new HashMap<String, AttendanceVO>();
			for (AttendanceVO attendanceVO : devices) {
				deviceMaps.put(attendanceVO.getAccessCode(), attendanceVO);
			}
			vo = deviceMaps.get(accessCode);
			Integer count = Integer.valueOf(vo.getSeqCount()) + 1;
			String url = "/device.cgi/events?action=getevent&roll-over-count=0&seq-number="
					+ count + "" + "&no-of-events=5&format=xml";
			returnMessage = url;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			returnMessage = "VOID";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String dataFromClient() {

		try {

			List<AttendanceVO> devices = DBConnector.getDeviceList();
			Map<String, AttendanceVO> deviceMaps = new HashMap<String, AttendanceVO>();
			for (AttendanceVO attendanceVO : devices) {
				deviceMaps.put(attendanceVO.getAccessCode(), attendanceVO);
			}
			vo = deviceMaps.get(accessCode);
			List<AttendanceVO> list = DBConnector.eventsFromClient(payLoad, vo);
			List<AttendanceVO> users = DBConnector.getUserList();
			Map<Integer, AttendanceVO> userMaps = new HashMap<Integer, AttendanceVO>();
			for (AttendanceVO attendanceVO : users) {
				userMaps.put(attendanceVO.getUserReference(), attendanceVO);
			}
			DBConnector.saveEvents(list, userMaps);
			Collections.sort(list, new Comparator<AttendanceVO>() {
				public int compare(AttendanceVO m1, AttendanceVO m2) {
					return m2.getSeqId().compareTo(m1.getSeqId());
				}
			});
			List<AttendanceVO> deviceUpdateList = new ArrayList<AttendanceVO>();
			Long deviceRegistrationId = (long) 0;
			for (AttendanceVO attendanceVO : list) {
				if (deviceRegistrationId == 0
						|| (long) attendanceVO.getDeviceRegistrationId() != deviceRegistrationId) {
					deviceRegistrationId = attendanceVO
							.getDeviceRegistrationId();
					deviceUpdateList.add(attendanceVO);
				}
			}
			DBConnector.updateDeviceInfo(deviceUpdateList);
			returnMessage = "SUCCESS";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			returnMessage = "VOID";
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public AttendanceVO getVo() {
		return vo;
	}

	public void setVo(AttendanceVO vo) {
		this.vo = vo;
	}

	public List<AttendanceVO> getVos() {
		return vos;
	}

	public void setVos(List<AttendanceVO> vos) {
		this.vos = vos;
	}

	public Long getImplementationId() {
		return implementationId;
	}

	public void setImplementationId(Long implementationId) {
		this.implementationId = implementationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public Integer getUserReference() {
		return userReference;
	}

	public void setUserReference(Integer userReference) {
		this.userReference = userReference;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getDateView() {
		return dateView;
	}

	public void setDateView(String dateView) {
		this.dateView = dateView;
	}

	public String getSeqCount() {
		return seqCount;
	}

	public void setSeqCount(String seqCount) {
		this.seqCount = seqCount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDeviceRegistrationId() {
		return deviceRegistrationId;
	}

	public void setDeviceRegistrationId(Long deviceRegistrationId) {
		this.deviceRegistrationId = deviceRegistrationId;
	}

	public Long getEventsLogId() {
		return eventsLogId;
	}

	public void setEventsLogId(Long eventsLogId) {
		this.eventsLogId = eventsLogId;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

}