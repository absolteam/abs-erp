package com.aios.user.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.struts2.ServletActionContext;

public class AiosHttpClient {

	private final static String USER_AGENT = "Mozilla/5.0";
	private static String authMethod = "auth";
	private static String userName = "admin";
	private static String password = "1234";
	private static String realm = "";

	public static String nonce;
	private static String webPage = "http://172.16.16.205";
	private static String name = "admin";
	public static ScheduledExecutorService nonceRefreshExecutor;
	public static String authStringEnc = "";

	public static void commonAuthentication() throws Exception {
		List<AttendanceVO> vos = DBConnector.getDeviceList();
		for (AttendanceVO attendanceVO : vos) {
			realm = attendanceVO.getIpAddress();
			/*
			 * authenticate(ServletActionContext.getRequest(),
			 * ServletActionContext.getResponse());
			 */

			try {

				String authString = name + ":" + password;
				System.out.println("auth string: " + authString);
				byte[] authEncBytes = Base64
						.encodeBase64(authString.getBytes());
				authStringEnc = new String(authEncBytes);
				System.out.println("Base64 encoded auth string: "
						+ authStringEnc);
/*
				URL url = new URL(webPage);
				URLConnection urlConnection = url.openConnection();
				urlConnection.setRequestProperty("Authorization", "Basic "
						+ authStringEnc);
				InputStream is = urlConnection.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);

				int numCharsRead;
				char[] charArray = new char[1024];
				StringBuffer sb = new StringBuffer();
				while ((numCharsRead = isr.read(charArray)) > 0) {
					sb.append(charArray, 0, numCharsRead);
				}
				String result = sb.toString();

				System.out.println("*** BEGIN ***");
				System.out.println(result);
				System.out.println("*** END ***");*/
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	/*protected static void authenticate(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {

			String authString = name + ":" + password;
			System.out.println("auth string: " + authString);
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			System.out.println("Base64 encoded auth string: " + authStringEnc);

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic "
					+ authStringEnc);
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}
			String result = sb.toString();

			System.out.println("*** BEGIN ***");
			System.out.println(result);
			System.out.println("*** END ***");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Returns the request body as String
	 * 
	 * @param request
	 * @return
	 * @throws IOException
	 */
	private static String readRequestBody(HttpServletRequest request)
			throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		try {
			InputStream inputStream = request.getInputStream();
			if (inputStream != null) {
				bufferedReader = new BufferedReader(new InputStreamReader(
						inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}
		String body = stringBuilder.toString();
		return body;
	}

	private static String getAuthenticateHeader() {
		String header = "";

		header += "Digest realm=\"" + realm + "\",";
		if (!StringUtils.isBlank(authMethod)) {
			header += "qop=" + authMethod + ",";
		}
		header += "nonce=\"" + nonce + "\",";
		header += "opaque=\"" + getOpaque(realm, nonce) + "\"";

		return header;
	}

	private static String getOpaque(String domain, String nonce) {
		return DigestUtils.md5Hex(domain + nonce);
	}

	// HTTP GET request
	private void sendGet() throws Exception {

		String url = "http://www.google.com/search?q=developer";

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);

		// add request header
		request.addHeader("User-Agent", USER_AGENT);

		HttpResponse response = client.execute(request);

		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : "
				+ response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());

	}

	/**
	 * Gets the Authorization header string minus the "AuthType" and returns a
	 * hashMap of keys and values
	 * 
	 * @param headerString
	 * @return
	 */
	private static HashMap<String, String> parseHeader(String headerString) {
		// seperte out the part of the string which tells you which Auth scheme
		// is it
		String headerStringWithoutScheme = headerString.substring(
				headerString.indexOf(" ") + 1).trim();
		HashMap<String, String> values = new HashMap<String, String>();
		String keyValueArray[] = headerStringWithoutScheme.split(",");
		for (String keyval : keyValueArray) {
			if (keyval.contains("=")) {
				String key = keyval.substring(0, keyval.indexOf("="));
				String value = keyval.substring(keyval.indexOf("=") + 1);
				values.put(key.trim(), value.replaceAll("\"", "").trim());
			}
		}
		return values;
	}

	// HTTP POST request
	public static AttendanceVO sendDevicePost(AttendanceVO attendanceVO)
			throws Exception {
		realm = attendanceVO.getIpAddress();
		webPage = attendanceVO.getIpAddress();
		/*authenticate(ServletActionContext.getRequest(),
				ServletActionContext.getResponse());*/
		String url = "http://" + attendanceVO.getIpAddress()
				+":"+ attendanceVO.getPort()
				+ "/device.cgi/device-basic-config?action=set&app=2&name="
				+ attendanceVO.getName() + "&asc-code="
				+ attendanceVO.getAccessCode();
		int status;

		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url);
		request.setHeader("Authorization", "Basic " + authStringEnc);
		request.addHeader("User-Agent", USER_AGENT);
		HttpResponse response = client.execute(request);
		// Get the response
		status = response.getStatusLine().getStatusCode();

		System.out.println("1------------" + status);

		// Enroll Device
		url = "http://" + attendanceVO.getIpAddress()
				+":"+ attendanceVO.getPort()
				+ "/device.cgi/enroll-options?action=set&enroll-on-device=1";
		request = new HttpGet(url);
		request.addHeader("User-Agent", USER_AGENT);
		response = client.execute(request);

		// Get the response
		status = response.getStatusLine().getStatusCode();
		System.out.println("2------------" + status);

		// Set Date & Time
		url = "http://" + attendanceVO.getIpAddress()
				+ "/date-time?action=set&time-zone=43";
		request = new HttpGet(url);
		request.addHeader("User-Agent", USER_AGENT);
		response = client.execute(request);

		// Get the response
		response.getStatusLine().getStatusCode();
		System.out.println("3------------" + status);

		attendanceVO.setReturnStatus(status + "");

		return attendanceVO;

	}

	// HTTP POST request
	public static AttendanceVO sendUserPost(AttendanceVO attendanceVO)
			throws Exception {

		String url = "http://" + attendanceVO.getIpAddress()
				+":"+ attendanceVO.getPort()
				+ "/device.cgi/users?action=set&user-id="
				+ attendanceVO.getUserReference() + "&ref-user-id="
				+ attendanceVO.getUserReference() + "&name="
				+ attendanceVO.getName() + "&user-active=1&format=xml";

		HttpClient client = new DefaultHttpClient();

		HttpGet get = new HttpGet(url);
		// add header
		get.setHeader("Authorization", "Basic " + authStringEnc);

		HttpResponse response = client.execute(get);
		System.out.println("\nSending 'GET' register request to URL : " + url);

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		// Enroll User
		url = "http://" + attendanceVO.getIpAddress()
				+ "/device.cgi/enrolluser?action=enroll&type=2&user-id="+attendanceVO.getUserReference()+"&finger-count=1";
		get.setHeader("Authorization", "Basic " + authStringEnc);
		client = new DefaultHttpClient();
		System.out.println("\nSending 'GET' enroll request to URL : " + url);

		get = new HttpGet(url);
		response = client.execute(get);

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());

		attendanceVO.setReturnStatus(result.toString());

		return attendanceVO;

	}

	public static String getAllEvents(AttendanceVO attendanceVO)
			throws Exception {
		Integer count=Integer.valueOf(attendanceVO.getSeqCount())+1;
		String url = "http://"
				+ attendanceVO.getIpAddress()
				+":"+ attendanceVO.getPort()
				+ "/device.cgi/events?action=getevent&roll-over-count=0&seq-number="
				+ count+""+ "&no-of-events=5&format=xml";
		HttpGet htpGet = new HttpGet(url);
		htpGet.setHeader("Authorization", "Basic " + authStringEnc);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(htpGet);
		String responseAsString = EntityUtils.toString(response.getEntity());

		return responseAsString;
	}

}