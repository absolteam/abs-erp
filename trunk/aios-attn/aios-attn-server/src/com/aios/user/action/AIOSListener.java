package com.aios.user.action;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.struts2.ServletActionContext;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

public class AIOSListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void contextInitialized(ServletContextEvent event) {
		// specify sceduler task details
		try {
			/*Properties props = new Properties();
			props.load(event.getServletContext().getResourceAsStream("/db.properties"));
			
			event.getServletContext().setAttribute("confProps", props);*/
			AiosHttpClient.commonAuthentication();

			JobDetail job = new JobDetail();
			job.setName("aiosJob");
			job.setJobClass(AiosJob.class);
			job.setGroup("AIOS");
			Map dataMap = job.getJobDataMap();
			dataMap.put("aiosJ", null);

			// configure the scheduler time
			CronTrigger trigger = new CronTrigger();
			trigger.setName("aiosJob");

			trigger.setCronExpression("0 0/5 * * * ?");
			trigger.setGroup("AIOS");

			// schedule it
			Scheduler scheduler;

			scheduler = new StdSchedulerFactory().getScheduler();

			scheduler.start();
			scheduler.scheduleJob(job, trigger);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
