package com.aios.user.action;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DBConnector {

	public static Connection getConnection() throws Exception {
		// TODO Auto-generated method stub
		try {
			// import driver
			Class.forName("com.mysql.jdbc.Driver");
			/*Properties prop = new Properties();
			prop = (Properties) ServletActionContext.getServletContext().getAttribute(
					"confProps");*/

			// connection to database
			/*Connection conn = DriverManager.getConnection(
					"jdbc:mysql://" + prop.getProperty("db.ipaddress") + ":"
							+ prop.getProperty("db.port") + "/attendance",
					prop.getProperty("db.username"),
					prop.getProperty("db.password"));*/
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/attendance","root",
					"r@aio930");
			return conn;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static void saveDevice(AttendanceVO vo) throws Exception {
		String query = new String();
		vo.setAscCode(Integer.valueOf(vo.getAccessCode()));
		vo.setImplementationId(Integer.valueOf(vo.getCompany()));
		vo.setSeqId(Integer.valueOf(vo.getSeqCount()));
		/*
		 * Connection conn = DBConnector.getConnection(); Statement state =
		 * conn.createStatement(); query =
		 * "INSERT INTO device_registration VALUES('" + vo.getName() + "'," +
		 * vo.getAscCode() + "," + vo.getSeqCount() + "," +
		 * vo.getImplementationId() + ",'" + vo.getIpAddress() + "'," +
		 * vo.getPort() + ")"; state.executeUpdate(query);
		 */

		query = "INSERT INTO device_registration(name,asc_code,current_seq_count,implementation_id,ip_address,port) values(?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DBConnector.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(1, vo.getName());
			statement.setInt(2, vo.getAscCode());
			statement.setInt(3, vo.getSeqId());
			statement.setInt(4, vo.getImplementationId());
			statement.setString(5, vo.getIpAddress());
			statement.setInt(6, vo.getPort());
			statement.executeUpdate();
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException logOrIgnore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException logOrIgnore) {
				}
		}
	}

	public static void saveUser(AttendanceVO vo) throws Exception {
		String query = new String();
		/*
		 * Statement state = DBConnector.getConnection().createStatement();
		 * String sql = "INSERT INTO users VALUES(" + vo.getUserReference() +
		 * ",'" + vo.getName() + "'," + vo.getImplementationId() + "," +
		 * vo.getDeviceRegistrationId() + ")"; state.executeUpdate(sql);
		 */

		query = "INSERT INTO users (user_reference,name,implementation_id,device_registration_id) values(?,?,?,?)";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DBConnector.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, vo.getUserReference());
			statement.setString(2, vo.getName());
			statement.setInt(3, vo.getImplementationId());
			statement.setLong(4, vo.getDeviceRegistrationId());
			statement.executeUpdate();
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException logOrIgnore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException logOrIgnore) {
				}
		}
	}

	public static void saveEvents(List<AttendanceVO> vos,
			Map<Integer, AttendanceVO> userMaps) throws Exception {
		String sql = "INSERT INTO events_log"
				+ "(user_id,date, time, event_type,event_id,seq_id) VALUES"
				+ "(?,?,?,?,?,?)";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DBConnector.getConnection();
			statement = connection.prepareStatement(sql);
			for (int i = 0; i < vos.size(); i++) {
				AttendanceVO vo = vos.get(i);
				AttendanceVO userVO = userMaps.get(vo.getUserReference());
				if (userVO != null && userVO.getUserId() > 0)
					vo.setUserId(userVO.getUserId());

				if (vo.getUserId() == null || vo.getUserId() == 0
						|| vo.getEventType() != 101)
					continue;

				if (vo.getUserId() != null && vo.getUserId() > 0)
					statement.setLong(1, vo.getUserId());
				else
					statement.setLong(1, 1);

				statement.setDate(2, convertStringToDate(vo.getDateView()));
				statement.setString(3, vo.getTimeView());
				statement.setInt(4, vo.getEventType());
				statement.setInt(5, vo.getSeqId());
				statement.setInt(6, vo.getSeqId());
				statement.addBatch();
				if ((i + 1) % 1000 == 0) {
					statement.executeBatch(); // Execute every 1000 items.
				}
			}
			statement.executeBatch();
		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException logOrIgnore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException logOrIgnore) {
				}
		}
	}

	public static Date convertStringToDate(String string) throws Exception {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
		java.util.Date parsed = format.parse(string);
		java.sql.Date sql = new java.sql.Date(parsed.getTime());
		return sql;
	}

	public static Date convertStringToDateTime(String string) throws Exception {
		DateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		java.util.Date parsed = format.parse(string);
		java.sql.Date sql = new java.sql.Date(parsed.getTime());
		return sql;
	}

	public static List<AttendanceVO> getUserList() throws Exception {
		Statement state = DBConnector.getConnection().createStatement();
		String sql = "select * from users";
		ResultSet result = state.executeQuery(sql);
		List<AttendanceVO> list = new ArrayList<AttendanceVO>();
		AttendanceVO vo = null;
		while (result.next()) {
			vo = new AttendanceVO();
			vo.setUserId(Long.valueOf(result.getString("user_id")));
			vo.setUserReference(Integer.valueOf(result
					.getString("user_reference")));
			vo.setEmployee(result.getString("name"));
			vo.setImplementationId(Integer.valueOf(result
					.getString("implementation_id")));
			vo.setDeviceRegistrationId(Long.valueOf(result
					.getString("device_registration_id")));
			list.add(vo);
		}
		return list;

	}

	public static List<AttendanceVO> getDeviceList() throws Exception {
		String sql = "select * from device_registration";

		Statement state = DBConnector.getConnection().createStatement();
		ResultSet result = state.executeQuery(sql);
		List<AttendanceVO> list = new ArrayList<AttendanceVO>();
		AttendanceVO vo = null;
		while (result.next()) {
			vo = new AttendanceVO();
			vo.setDeviceRegistrationId(Long.valueOf(result
					.getString("device_registration_id")));
			vo.setName(result.getString("name"));
			vo.setAccessCode(result.getString("asc_code"));
			vo.setIpAddress(result.getString("ip_address"));
			vo.setPort(Integer.valueOf(result.getString("port")));
			vo.setImplementationId(Integer.valueOf(result
					.getString("implementation_id")));
			vo.setSeqCount(result.getString("current_seq_count"));
			vo.setCompany(result.getString("implementation_id"));
			list.add(vo);
		}
		return list;
	}

	public static List<AttendanceVO> getEventList(
			Map<Long, AttendanceVO> userMaps, Map<Long, AttendanceVO> deviceMaps)
			throws Exception {
		String sql = "select * from events_log";

		Statement state = DBConnector.getConnection().createStatement();
		ResultSet result = state.executeQuery(sql);
		List<AttendanceVO> list = new ArrayList<AttendanceVO>();
		AttendanceVO tvo = null;
		AttendanceVO userVO = null;
		AttendanceVO deviceVO = null;
		while (result.next()) {
			tvo = new AttendanceVO();
			tvo.setEventLogId(Long.valueOf(result.getString("events_log_id")));
			tvo.setEventId(Integer.valueOf(result.getString("seq_id")));
			tvo.setUserId(Long.valueOf(result.getString("user_id")));
			tvo.setUserReference(Integer.valueOf(result.getString("user_id")));
			tvo.setDateView(result.getString("date"));
			tvo.setTimeView(result.getString("time"));
			tvo.setEventType(Integer.valueOf(result.getString("event_type")));

			userVO = userMaps.get(tvo.getUserId());
			if (userVO != null) {
				tvo.setEmployee(userVO.getEmployee());
				tvo.setUserReference(userVO.getUserReference());
				deviceVO = deviceMaps.get(userVO.getDeviceRegistrationId());
				tvo.setName(deviceVO.getName());
			}

			list.add(tvo);
		}
		return list;
	}

	public static AttendanceVO getDeviceInfo(AttendanceVO vo) throws Exception {
		String sql = "select * from device_registration where device_registration_id="
				+ vo.getDeviceRegistrationId();

		Statement state = DBConnector.getConnection().createStatement();
		ResultSet result = state.executeQuery(sql);
		while (result.next()) {
			vo.setDeviceRegistrationId(Long.valueOf(result
					.getString("device_registration_id")));
			vo.setName(result.getString("name"));
			vo.setAccessCode(result.getString("asc_code"));
			vo.setImplementationId(result.getInt("implementation_id"));
			vo.setIpAddress(result.getString("ip_address"));
			vo.setPort(result.getInt("port"));
			vo.setSeqCount(result.getInt("current_seq_count") + "");
		}
		return vo;
	}

	public static void updateDeviceInfo(List<AttendanceVO> vos)
			throws Exception {

		Connection connection = null;
		Statement statement = null;
		try {
			connection = DBConnector.getConnection();
			statement = connection.createStatement();
			String sql = null;
			for (AttendanceVO vo : vos) {
									
				sql = "UPDATE  device_registration SET ip_address='"
						+ vo.getIpAddress() + "',current_seq_count="
						+ vo.getSeqId() + " where device_registration_id="
						+ vo.getDeviceRegistrationId();
				statement.executeUpdate(sql);
			}

		} finally {
			if (statement != null)
				try {
					statement.close();
				} catch (SQLException logOrIgnore) {
				}
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException logOrIgnore) {
				}
		}
	}

	public static void batchUpdateFromDevice() {
		try {
			List<AttendanceVO> list = fetchEventsLog();
			List<AttendanceVO> users = getUserList();
			Map<Integer, AttendanceVO> userMaps = new HashMap<Integer, AttendanceVO>();
			for (AttendanceVO attendanceVO : users) {
				userMaps.put(attendanceVO.getUserReference(), attendanceVO);
			}
			saveEvents(list, userMaps);
			Collections.sort(list, new Comparator<AttendanceVO>() {
				public int compare(AttendanceVO m1, AttendanceVO m2) {
					return m2.getSeqId().compareTo(m1.getSeqId());
				}
			});
			List<AttendanceVO> deviceUpdateList = new ArrayList<AttendanceVO>();
			Long deviceRegistrationId = (long) 0;
			for (AttendanceVO attendanceVO : list) {
				if (deviceRegistrationId == 0
						|| (long) attendanceVO.getDeviceRegistrationId() != deviceRegistrationId) {
					deviceRegistrationId = attendanceVO
							.getDeviceRegistrationId();
					deviceUpdateList.add(attendanceVO);
				}
			}
			updateDeviceInfo(deviceUpdateList);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<AttendanceVO> fetchEventsLog() throws Exception {
		List<AttendanceVO> logList = new ArrayList<AttendanceVO>();
		List<AttendanceVO> devices = getDeviceList();
		AttendanceVO attendanceVO = null;
		try {
			for (AttendanceVO vo : devices) {
				String string = AiosHttpClient.getAllEvents(vo);
				Document doc = loadXMLFromString(string);

				doc.getDocumentElement().normalize();

				System.out.println("Root element :"
						+ doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("Events");

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						attendanceVO = new AttendanceVO();
						// Set Device Info
						attendanceVO.setIpAddress(vo.getIpAddress());
						attendanceVO.setDeviceRegistrationId(vo
								.getDeviceRegistrationId());
						attendanceVO.setImplementationId(vo
								.getImplementationId());
						attendanceVO.setName(vo.getName());
						attendanceVO.setAscCode(vo.getAscCode());
						attendanceVO.setPort(vo.getPort());

						Element eElement = (Element) nNode;
						attendanceVO.setSeqId(Integer.valueOf(eElement
								.getElementsByTagName("seq-No").item(0)
								.getTextContent()));
						attendanceVO.setEventId(Integer.valueOf(eElement
								.getElementsByTagName("seq-No").item(0)
								.getTextContent()));
						attendanceVO.setDateView(eElement
								.getElementsByTagName("date").item(0)
								.getTextContent());
						attendanceVO.setTimeView(eElement
								.getElementsByTagName("time").item(0)
								.getTextContent());
						attendanceVO.setEventType(Integer.valueOf(eElement
								.getElementsByTagName("event-id").item(0)
								.getTextContent()));
						attendanceVO.setUserReference(Integer.valueOf(eElement
								.getElementsByTagName("detail-1").item(0)
								.getTextContent()));
						logList.add(attendanceVO);

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return logList;
	}
	
	public static List<AttendanceVO> eventsFromClient(String string,AttendanceVO vo) throws Exception {
		List<AttendanceVO> logList = new ArrayList<AttendanceVO>();
		AttendanceVO attendanceVO = null;
		try {
				Document doc = loadXMLFromString(string);

				doc.getDocumentElement().normalize();

				System.out.println("Root element :"
						+ doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("Events");

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						attendanceVO = new AttendanceVO();
						// Set Device Info
						attendanceVO.setIpAddress(vo.getIpAddress());
						attendanceVO.setDeviceRegistrationId(vo
								.getDeviceRegistrationId());
						attendanceVO.setImplementationId(vo
								.getImplementationId());
						attendanceVO.setName(vo.getName());
						attendanceVO.setAscCode(vo.getAscCode());
						attendanceVO.setPort(vo.getPort());

						Element eElement = (Element) nNode;
						attendanceVO.setSeqId(Integer.valueOf(eElement
								.getElementsByTagName("seq-No").item(0)
								.getTextContent()));
						attendanceVO.setEventId(Integer.valueOf(eElement
								.getElementsByTagName("seq-No").item(0)
								.getTextContent()));
						attendanceVO.setDateView(eElement
								.getElementsByTagName("date").item(0)
								.getTextContent());
						attendanceVO.setTimeView(eElement
								.getElementsByTagName("time").item(0)
								.getTextContent());
						attendanceVO.setEventType(Integer.valueOf(eElement
								.getElementsByTagName("event-id").item(0)
								.getTextContent()));
						attendanceVO.setUserReference(Integer.valueOf(eElement
								.getElementsByTagName("detail-1").item(0)
								.getTextContent()));
						logList.add(attendanceVO);

					}
				}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return logList;
	}

	public static Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();

		return builder.parse(new ByteArrayInputStream(xml.getBytes()));
	}

	public static java.util.Date dateConverter(String dateStr) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		try {

			java.util.Date date = formatter.parse(dateStr);
			System.out.println(date);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;

		}
	}
}
