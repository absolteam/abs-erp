package com.aios.user.action;

import java.util.Date;

public class AttendanceVO {
	private String name;
	private String accessCode;
	private String employee;
	private Integer userReference;
	private String company;
	private Date date;
	private Date time;
	private String dateView;
	private String seqCount;
	private Long userId;
	private Long deviceRegistrationId;
	private Long eventsLogId;
	private Integer implementationId;
	public Integer getImplementationId() {
		return implementationId;
	}
	public void setImplementationId(Integer implementationId) {
		this.implementationId = implementationId;
	}
	private String ipAddress;
	private Integer port;
	private Integer eventType;
	private String returnStatus;
	private Long eventLogId;
	private Integer eventId;
	private String timeView;
	private Integer seqId;
	private Integer ascCode;

	public Integer getAscCode() {
		return ascCode;
	}
	public void setAscCode(Integer ascCode) {
		this.ascCode = ascCode;
	}
	public Integer getSeqId() {
		return seqId;
	}
	public void setSeqId(Integer seqId) {
		this.seqId = seqId;
	}
	public String getTimeView() {
		return timeView;
	}
	public void setTimeView(String timeView) {
		this.timeView = timeView;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Long getEventLogId() {
		return eventLogId;
	}
	public void setEventLogId(Long eventLogId) {
		this.eventLogId = eventLogId;
	}
	public String getReturnStatus() {
		return returnStatus;
	}
	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}
	public Integer getEventType() {
		return eventType;
	}
	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public Integer getUserReference() {
		return userReference;
	}
	public void setUserReference(Integer userReference) {
		this.userReference = userReference;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getDateView() {
		return dateView;
	}
	public void setDateView(String dateView) {
		this.dateView = dateView;
	}
	public String getSeqCount() {
		return seqCount;
	}
	public void setSeqCount(String seqCount) {
		this.seqCount = seqCount;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getDeviceRegistrationId() {
		return deviceRegistrationId;
	}
	public void setDeviceRegistrationId(Long deviceRegistrationId) {
		this.deviceRegistrationId = deviceRegistrationId;
	}
	public Long getEventsLogId() {
		return eventsLogId;
	}
	public void setEventsLogId(Long eventsLogId) {
		this.eventsLogId = eventsLogId;
	}
	
	
}
