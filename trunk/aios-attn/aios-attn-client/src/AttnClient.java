import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@SuppressWarnings("deprecation")
public class AttnClient {

	private static final Logger LOGGER = LogManager.getLogger(AttnClient.class);
	private static final String VOID = "VOID";
	private static final String SUCCESS = "SUCCESS";
	private static final String CONF_FILE = "C:\\aios\\attnClient\\attn_client.conf";

	public static void main(String[] args) throws Exception {

		Properties properties = new Properties();
		FileInputStream fileInputStream = new FileInputStream(new File(
				CONF_FILE));
		properties.load(fileInputStream);
		String result = "";
		Long pokeInterval = Long.parseLong(properties
				.getProperty("pokeIntervalSeconds")) * 1000;
		Boolean pokeIntervalRequired = Boolean.parseBoolean(properties
				.getProperty("setPokeInterval"));

		LOGGER.info("POKE INTERVAL REQUIRED? "
				+ (pokeIntervalRequired ? "YES" : "NO"));
		if (pokeIntervalRequired)
			LOGGER.info("POKE INTERVAL SET = " + pokeInterval / 1000 + "s");

		while (true) {

			LOGGER.info("INITIATING CLIENT ACTIVITY");
			try {
				result = "";

				URL pokeURL = new URL(properties.getProperty("pokeCommand"));
				URLConnection pokeConnection = pokeURL.openConnection();
				pokeConnection.connect();

				byte[] bytes = null;
				InputStream is = null;
				is = pokeConnection.getInputStream();
				bytes = new byte[is.available()];
				int read = 0;
				int numRead = 0;

				while (read < bytes.length
						&& (numRead = is.read(bytes, read, bytes.length - read)) >= 0)
					read = read + numRead;

				if (is != null)
					is.close();

				result = bytesToUTFString(bytes);
				LOGGER.info(result + " :: POKE RESULT :: ATTN CLIENT SUCCESS");

				if (!result.equalsIgnoreCase(VOID)
						&& !result.equalsIgnoreCase(SUCCESS)) {

					// if here, implies that we received a command to be
					// executed on local attendance machine
					pushDataToServer(properties,
							getResultFromDevice(properties, result));
				}

			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}

			if (pokeIntervalRequired)
				Thread.sleep(pokeInterval);
			else
				break;
		}
	}

	private static String getResultFromDevice(Properties properties,
			String command) throws Exception {

		LOGGER.info("GETTING DATA FROM DEVICE");
		byte[] encodedAuthBytes = Base64.encodeBase64(new String(properties
				.getProperty("localAttnDeviceCredentials")).getBytes());
		String encodedAuthString = new String(encodedAuthBytes);

		String commandURL = "http://"
				+ properties.getProperty("localAttnDeviceIPAndPort") + command;
		HttpGet htpGet = new HttpGet(commandURL);
		htpGet.setHeader("Authorization", "Basic " + encodedAuthString);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(htpGet);
		String responseAsString = EntityUtils.toString(response.getEntity());

		LOGGER.info("DATA RECIEVED FROM DEVICE");
		return responseAsString;
	}

	private static void pushDataToServer(Properties properties,
			String attnDeviceResult) throws Exception {

		LOGGER.info("DATA REQUESTED BY SERVER :: STARTING DATA PUSH");
		String url = properties.getProperty("pushDataCommand");

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("payLoad", attnDeviceResult));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		LOGGER.info("Server Response Code: "
				+ response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";

		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		LOGGER.info(result.toString()
				+ " :: PUSH RESULT :: CYCLE COMPLETE :: SUCCESS");
	}

	private static String bytesToUTFString(byte[] bytes) {

		try {
			if (bytes == null)
				return "";
			byte[] newBytes = new byte[bytes.length];
			for (int i = 0; i < newBytes.length; i++)
				newBytes[i] = bytes[i];
			return new String(newBytes, "utf8").trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
