/* This class is for IP Update Functionality ONLY. 
 * AttnClient shall be used for HTTPTunneling requirement.
 * */

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class IPUpdateHandler {

	private static final Logger LOGGER = LogManager
			.getLogger(IPUpdateHandler.class);

	public static void main(String[] args) {

		try {
			FileInputStream fileInputStream = new FileInputStream(new File(
					"C:\\aios\\attnClient\\attn_client.conf"));
			Properties properties = new Properties();
			properties.load(fileInputStream);

			URL myURL = new URL(properties.getProperty("attnServerAddress"));
			URLConnection myURLConnection = myURL.openConnection();
			myURLConnection.connect();

			byte[] bytes = null;
			InputStream is = null;
			is = myURLConnection.getInputStream();
			bytes = new byte[is.available()];
			int read = 0;
			int numRead = 0;

			while (read < bytes.length
					&& (numRead = is.read(bytes, read, bytes.length - read)) >= 0)
				read = read + numRead;

			if (is != null)
				is.close();

			LOGGER.info(bytesToUTFString(bytes) + " :: ATTN CLIENT SUCCESS");

		} catch (Exception e) {
			LOGGER.error(e.getMessage() + " :: ATTN CLIENT ERROR");
		}
	}

	private static String bytesToUTFString(byte[] bytes) {

		try {
			if (bytes == null)
				return "";
			byte[] newBytes = new byte[bytes.length];
			for (int i = 0; i < newBytes.length; i++)
				newBytes[i] = bytes[i];
			return new String(newBytes, "utf8").trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
