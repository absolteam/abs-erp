package com.aiotech.aios.app.dbutil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class HibernateLocalSession {
	 private static final SessionFactory sessionFactory = buildSessionFactory();
	 
	    private static SessionFactory buildSessionFactory() {
	        try {
	        	
	        	Configuration cfg1 = new AnnotationConfiguration();
	        	cfg1.configure("resources/hibernate-local.cfg.xml"); 
	        	SessionFactory sessionFactory = cfg1.buildSessionFactory();
	        	
 	            return sessionFactory;
	 
	        }
	        catch (Throwable ex) {
	            // Make sure you log the exception, as it might be swallowed
	            System.err.println("Initial SessionFactory creation failed." + ex);
	            throw new ExceptionInInitializerError(ex);
	        }
	    }
	 
	    public static SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }
	 
	    public static void shutdown() {
	    	// Close caches and connection pools
	    	getSessionFactory().close();
	    }
}
