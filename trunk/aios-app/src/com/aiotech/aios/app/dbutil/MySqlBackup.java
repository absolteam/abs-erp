package com.aiotech.aios.app.dbutil;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.log4j.Logger;

public class MySqlBackup {

	private static final Logger log = Logger.getLogger(MySqlBackup.class
			.getName());



	/**
	 * Delete Old Backup files when there is file size > 3
	 * 
	 * @param directory
	 */
	@SuppressWarnings("unused")
	private void deleteOldBackupFiles(File directory) {
		if (directory.exists()) {
			File[] listFiles = directory.listFiles();
			if (null != listFiles && listFiles.length > 3) {
				Arrays.sort(listFiles, new Comparator<File>() {
					public int compare(File f1, File f2) {
						if (((File) f1).lastModified() > ((File) f2)
								.lastModified())
							return -1;
						else if (((File) f1).lastModified() < ((File) f2)
								.lastModified())
							return +1;
						else
							return 0;
					}
				});
			} else
				return;
			for (int i = 0; i < listFiles.length; i++) {
				if (i > 2)
					listFiles[i].delete();
			}
		} else
			return;

	}
	
	public boolean backupLocalDatabase(String backupBatLocation) {
		Process runtimeProcess;
		try {
			runtimeProcess = Runtime.getRuntime().exec(backupBatLocation);
			int processComplete = runtimeProcess.waitFor();
			if (processComplete == 0) {
				log.info("Backup was taken successfully with " + backupBatLocation);
				return true;
			} else { 
				log.error("Could not take loal database backup " + backupBatLocation);
			}
		} catch (Exception ex) {
			log.error(ex.getCause());
		}
		return false;
	}

	public boolean restoreDatabase(String restoreBatFile, String restoreDBLocation) {
		Process runtimeProcess;
		try {
			runtimeProcess = Runtime.getRuntime().exec(restoreBatFile);
			int processComplete = runtimeProcess.waitFor();
			if (processComplete == 0) {
				log.info("Backup restored successfully with " + restoreBatFile);
				return true;
			} else {
				runtimeProcess = Runtime.getRuntime().exec(restoreDBLocation);
				processComplete = runtimeProcess.waitFor();
				if(processComplete == 0)
					log.info("Local Database reverted successfully with " + restoreDBLocation);
				else
					log.info("Local Database revert got failure with " + restoreDBLocation);
				log.error("Could not restore live database backup " + restoreBatFile);
			}
		} catch (Exception ex) {
			log.error(ex.getCause());
		}
		return false;
	}
}