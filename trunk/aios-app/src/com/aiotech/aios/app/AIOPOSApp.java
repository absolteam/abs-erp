package com.aiotech.aios.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.app.dbutil.HibernateLocalSession;
import com.aiotech.aios.app.dbutil.MySqlBackup;
import com.aiotech.aios.app.service.bl.TransactionHandlerBL;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.Reference;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class AIOPOSApp {
	private static final Logger log = Logger.getLogger(AIOPOSApp.class
			.getName());
	private static Properties prop;

	static {
		try {
			prop = new Properties();
			InputStream input = null;
			input = AIOPOSApp.class
					.getResourceAsStream("/resources/config.properties");
			prop.load(input);
			// Reload The config file from local drive
			prop.load(new FileInputStream(prop.getProperty("SYNCFILE-LOCATION")
					.trim()));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Properties load execetion");
		}
	}

	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String PROGRAM_FILES = System.getenv("programfiles");
	private static final String APPLICATION_URL = prop.getProperty(
			"APPLCIATION-URL").trim()
			+ prop.getProperty("APP-KEY").trim();

	public static final boolean liveConnection() {
		try {
			URL obj = new URL(APPLICATION_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			con.setDoOutput(true);
			if (con.getResponseCode() == 200)
				return true;
			else
				log.warn("Server was down...");
		} catch (Exception e) {
			log.warn("Server was down...");
		}
		return false;
	}

	/**
	 * Start application based on live status
	 * 
	 * @param liveStatus
	 */
	public static void startApplication() {
		try {
			InputStream htmlStream = AIOPOSApp.class
					.getResourceAsStream("/resources/aio.html");

			BufferedReader bufferReader = new BufferedReader(
					new InputStreamReader(htmlStream));

			StringBuilder htmlBuilder = new StringBuilder();

			String line;
			while ((line = bufferReader.readLine()) != null) {
				htmlBuilder.append(line);
			}

			String htmlString = htmlBuilder.toString().replace("#key",
					prop.getProperty("APP-KEY").trim());
			htmlString = htmlString.replace("#action",
					prop.getProperty("LOCAL-ADDRESS").trim());

			File tempHtml = File.createTempFile("aio", ".html");
			FileUtils.writeStringToFile(tempHtml, htmlString);

			@SuppressWarnings("unused")
			Process process = Runtime.getRuntime().exec(
					new String[] {
							PROGRAM_FILES.concat(prop.getProperty(
									"BROWSER-ROOT").trim()),
							tempHtml.getAbsolutePath() });
		} catch (Exception e) {
			log.info(e.getCause());
		}

	}

	private static boolean processBackUpRestoreData(boolean status) {
		boolean returnFlag = true;

		try {
			Implementation implementation = new Implementation();
			implementation.setImplementationId(Long.parseLong(prop.getProperty(
					"APP-KEY").trim()));
			TransactionHandlerBL transactionHandlerBL = new TransactionHandlerBL();
			Session localSession = HibernateLocalSession.getSessionFactory()
					.openSession();
			Reference reference = transactionHandlerBL.getSystemReference(
					implementation, localSession);
			List<PointOfSale> pointOfSales = transactionHandlerBL
					.getPointOfSale(implementation, localSession);
			localSession.flush();
			localSession.close();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				status = performSalesUpdates(pointOfSales);
				if (status)
					log.info("POS Offline transaction posted.");
				else
					log.info("POS Offline transaction post failure.");
			}
			if (status) { 
				MySqlBackup backupRestore = new MySqlBackup();
				status = backupRestore.backupLocalDatabase(prop.getProperty(
						"BACKUP-FILE-LOCATION").trim());
				if (status) {
					status = backupRestore.backupLocalDatabase(prop
							.getProperty("SECONDARY-BACKUP-FILE-LOCATION")
							.trim());
					if (status) {
						status = backupRestore.restoreDatabase(prop
								.getProperty("BAT-FILE-LOCATION").trim(), prop
								.getProperty("RESTORE-FILE-LOCATION").trim());
						if (status) {
							transactionHandlerBL.saveReference(reference);
							log.info("Database Backup taken & Live Database was restored successfully.");
						} else {
							log.info("Live Database Restore failure.");
							returnFlag = false;
						}
					} else {
						log.info("Secondary Database Backup failure.");
						returnFlag = false;
					}
				} else {
					log.info("Primary Database Backup failure.");
					returnFlag = false;
				}
			}
		} catch (Exception e) {
			log.info("Error-->" + e.getMessage());
			e.printStackTrace();
			returnFlag = false;

		}
		return returnFlag;
	}

	private static boolean performSalesUpdates(List<PointOfSale> pointOfSales) {
		try {
			Implementation implementation = new Implementation();
			implementation.setImplementationId(Long.parseLong(prop.getProperty(
					"APP-KEY").trim()));

			List<PointOfSale> localSales = null;
			Person person = null;
			List<Customer> customers = new ArrayList<Customer>();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				PointOfSale localSale = null;
				localSales = new ArrayList<PointOfSale>();
				for (PointOfSale pointOfSale : pointOfSales) {
					localSale = new PointOfSale();

					// Manipulate customer
					localSale.setCustomer(manipulateCustomer(pointOfSale));
					if (localSale.getCustomer() != null)
						customers.add(localSale.getCustomer());

					localSale.setImplementation(implementation);
					localSale.setReferenceNumber(pointOfSale
							.getReferenceNumber());
					localSale.setDeliveryOptionName(pointOfSale
							.getDeliveryOptionName());
					localSale
							.setDeliveryStatus(pointOfSale.getDeliveryStatus());
					if (pointOfSale.getLookupDetail() != null) {
						LookupDetail lookupDetail = new LookupDetail();
						lookupDetail.setLookupDetailId(pointOfSale
								.getLookupDetail().getLookupDetailId());
						localSale.setLookupDetail(lookupDetail);
					}
					if (pointOfSale.getPOSUserTill() != null) {
						POSUserTill posTill = new POSUserTill();
						posTill.setPosUserTillId(pointOfSale.getPOSUserTill()
								.getPosUserTillId());
						localSale.setPOSUserTill(posTill);
					}
					localSale.setSalesDate(pointOfSale.getSalesDate());
					localSale.setSaleType(pointOfSale.getSaleType());

					if (pointOfSale.getPersonByDriverId() != null) {
						person = new Person();
						person.setPersonId(pointOfSale.getPersonByDriverId()
								.getPersonId());
						localSale.setPersonByDriverId(person);
					}

					if (pointOfSale.getPersonByEmployeeId() != null) {
						person = new Person();
						person.setPersonId(pointOfSale.getPersonByEmployeeId()
								.getPersonId());
						localSale.setPersonByDriverId(person);
					}
					localSale
							.setDiscountMethod(pointOfSale.getDiscountMethod());
					localSale.setPromotionMethod(pointOfSale
							.getPromotionMethod());
					localSale.setTotalDiscount(pointOfSale.getTotalDiscount());

					// Details
					localSale
							.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
									manipulatePosDetails(pointOfSale)));

					// Receipts
					localSale
							.setPointOfSaleReceipts(new HashSet<PointOfSaleReceipt>(
									manipulateReceipts(pointOfSale)));

					// Offers
					if (pointOfSale.getPointOfSaleOffers() != null
							&& pointOfSale.getPointOfSaleOffers().size() > 0) {
						List<PointOfSaleOffer> offers = new ArrayList<PointOfSaleOffer>();
						PointOfSaleOffer offerTemp = null;
						for (PointOfSaleOffer offer : pointOfSale
								.getPointOfSaleOffers()) {
							offerTemp = new PointOfSaleOffer();
							offerTemp.setPointOfSaleOfferId(offer
									.getPointOfSaleOfferId());
							offers.add(offerTemp);
						}
						localSale
								.setPointOfSaleOffers(new HashSet<PointOfSaleOffer>(
										offers));
					}

					// Charges
					if (pointOfSale.getPointOfSaleCharges() != null
							&& pointOfSale.getPointOfSaleCharges().size() > 0) {
						List<PointOfSaleCharge> charges = new ArrayList<PointOfSaleCharge>();
						PointOfSaleCharge chargeTemp = null;
						LookupDetail lookupDetail = null;
						for (PointOfSaleCharge charge : pointOfSale
								.getPointOfSaleCharges()) {
							chargeTemp = new PointOfSaleCharge();
							lookupDetail = new LookupDetail();
							chargeTemp.setPointOfSaleChargeId(charge
									.getPointOfSaleChargeId());
							chargeTemp.setCharges(charge.getCharges());
							lookupDetail.setLookupDetailId(charge
									.getLookupDetail().getLookupDetailId());
							chargeTemp.setLookupDetail(lookupDetail);
							chargeTemp.setDescription(charge.getDescription());
							charges.add(chargeTemp);
						}
						localSale
								.setPointOfSaleCharges(new HashSet<PointOfSaleCharge>(
										charges));
					}

					localSales.add(localSale);
				}

				PointOfSaleVO pointOfSaleVO = new PointOfSaleVO();
				pointOfSaleVO.setPointOfSales(localSales);
				Client client = Client.create();
				WebResource webResource = client.resource(prop.getProperty(
						"WEBSERVICE-URL").trim()
						+ "pointOfSaleSync");
				ClientResponse response = webResource.type(
						MediaType.APPLICATION_XML).post(ClientResponse.class,
						pointOfSaleVO);

				String output = response.getEntity(String.class);
				log.info("sales synchronization return status..." + output);
				if (response.getStatus() == 200 || output.equals("SUCCESS")) {
					TransactionHandlerBL transactionHandlerBL = new TransactionHandlerBL();
					transactionHandlerBL.updatePointOfSale(pointOfSales);
					log.info("Success in sales synchronization..."
							+ response.getStatus());
					return true;
				} else {
					log.info("Error in sales synchronization..."
							+ response.getStatus());
					return false;
				}
			}
			return true;
		} catch (Exception e) {
			log.info("Exception Message " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	private static Customer manipulateCustomer(PointOfSale pos) {
		Customer customer = new Customer();
		if (pos.getCustomer() != null) {
			if (pos.getCustomer().getPosCustomer() != null
					&& pos.getCustomer().getPosCustomer()) {
				Person person = new Person();
				person.setFirstName(pos.getCustomer().getPersonByPersonId()
						.getFirstName());
				person.setLastName(pos.getCustomer().getPersonByPersonId()
						.getLastName());
				person.setMobile(pos.getCustomer().getPersonByPersonId()
						.getMobile());
				person.setResidentialAddress(pos.getCustomer()
						.getPersonByPersonId().getResidentialAddress());
				person.setGender(pos.getCustomer().getPersonByPersonId()
						.getGender());
				customer.setPersonByPersonId(person);
				customer.setPosCustomer(pos.getCustomer().getPosCustomer());
				Implementation implementation = new Implementation();
				implementation.setImplementationId(pos.getImplementation()
						.getImplementationId());
				customer.setImplementation(implementation);
			} else {
				customer.setCustomerId(pos.getCustomer().getCustomerId());
				customer.setPosCustomer(pos.getCustomer().getPosCustomer());
				Implementation implementation = new Implementation();
				implementation.setImplementationId(pos.getImplementation()
						.getImplementationId());
				customer.setImplementation(implementation);
				// Object
				if (pos.getCustomer().getPersonByPersonId() != null) {
					Person person = new Person();
					person.setPersonId(pos.getCustomer().getPersonByPersonId()
							.getPersonId());
					person.setFirstName(pos.getCustomer().getPersonByPersonId()
							.getFirstName());
					person.setLastName(pos.getCustomer().getPersonByPersonId()
							.getLastName());
					person.setMobile(pos.getCustomer().getPersonByPersonId()
							.getMobile());
					person.setResidentialAddress(pos.getCustomer()
							.getPersonByPersonId().getResidentialAddress());
					person.setGender(pos.getCustomer().getPersonByPersonId()
							.getGender());
					customer.setPersonByPersonId(person);
				} else {
					Company company = new Company();
					company.setCompanyId(pos.getCustomer().getCompany()
							.getCompanyId());
					company.setCompanyName(pos.getCustomer().getCompany()
							.getCompanyName());
					company.setCompanyPhone(pos.getCustomer().getCompany()
							.getCompanyPhone());
					company.setCompanyAddress(pos.getCustomer().getCompany()
							.getCompanyAddress());
					customer.setCompany(pos.getCustomer().getCompany());
				}
			}

		} else {
			customer = null;
		}
		return customer;
	}

	private static List<PointOfSaleReceipt> manipulateReceipts(PointOfSale pos) {
		List<PointOfSaleReceipt> receipts = new ArrayList<PointOfSaleReceipt>();
		if (pos.getPointOfSaleReceipts() != null) {
			PointOfSaleReceipt recceipt = null;
			for (PointOfSaleReceipt pointOfSaleReceipt : pos
					.getPointOfSaleReceipts()) {
				recceipt = new PointOfSaleReceipt();
				recceipt.setReceiptType(pointOfSaleReceipt.getReceiptType());
				recceipt.setReceipt(pointOfSaleReceipt.getReceipt());
				recceipt.setBalanceDue(pointOfSaleReceipt.getBalanceDue());
				recceipt.setTransactionNumber(pointOfSaleReceipt
						.getTransactionNumber());
				receipts.add(recceipt);
			}
		} else {
			receipts = null;
		}
		return receipts;
	}

	private static List<PointOfSaleDetail> manipulatePosDetails(PointOfSale pos) {
		List<PointOfSaleDetail> details = new ArrayList<PointOfSaleDetail>();
		if (pos.getPointOfSaleDetails() != null) {
			PointOfSaleDetail recceipt = null;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				recceipt = new PointOfSaleDetail();
				recceipt.setQuantity(detail.getQuantity());
				recceipt.setUnitRate(detail.getUnitRate());
				recceipt.setIsPercentageDiscount(detail
						.getIsPercentageDiscount());
				recceipt.setDiscountValue(detail.getDiscountValue());
				recceipt.setActualPoints(detail.getActualPoints());
				recceipt.setPromotionPoints(detail.getActualPoints());
				recceipt.setSpecialProduct(detail.getSpecialProduct());
				recceipt.setItemOrder(detail.getItemOrder());
				recceipt.setComboItemOrder(detail.getComboItemOrder());
				if (detail.getDiscountMethod() != null) {
					DiscountMethod dis = new DiscountMethod();
					dis.setDiscountMethodId(detail.getDiscountMethod()
							.getDiscountMethodId());
					recceipt.setDiscountMethod(dis);
				}
				if (detail.getProductByProductId() != null) {
					Product pro = new Product();
					pro.setProductId(detail.getProductByProductId()
							.getProductId());
					recceipt.setProductByProductId(pro);
				}

				if (detail.getProductByComboProductId() != null) {
					Product pro = new Product();
					pro.setProductId(detail.getProductByComboProductId()
							.getProductId());
					recceipt.setProductByComboProductId(pro);
				}

				if (detail.getPromotionMethod() != null) {
					PromotionMethod promo = new PromotionMethod();
					promo.setPromotionMethodId(detail.getPromotionMethod()
							.getPromotionMethodId());
					recceipt.setPromotionMethod(promo);
				}

				if (detail.getProductPricingCalc() != null) {
					ProductPricingCalc cal = new ProductPricingCalc();
					cal.setProductPricingCalcId(detail.getProductPricingCalc()
							.getProductPricingCalcId());
					recceipt.setProductPricingCalc(cal);
				}

				details.add(recceipt);
			}
		} else {
			details = null;
		}
		return details;
	}

	private static boolean updateLastRestoreDate() {
		try {
			DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
			FileInputStream inputStream = new FileInputStream(prop.getProperty(
					"SYNCFILE-LOCATION").trim());
			Properties props = new Properties();
			props.load(inputStream);
			inputStream.close();
			LocalDate lastRestoreDate = dtf.parseLocalDate(props.getProperty(
					"LAST-RESTORE-DATE").trim());
			LocalDate currentDate = DateTime.now().toLocalDate();
			if (!lastRestoreDate.isEqual(currentDate)) {
				return true;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Main Method Call
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		try { 
			FileInputStream inputStream = new FileInputStream(prop
					.getProperty("SYNCFILE-LOCATION").trim());
			Properties props = new Properties();
			props.load(inputStream); 
			String dbRestore = props.getProperty("DB-RESTORE").trim();
			if(dbRestore.equalsIgnoreCase("N")){
				FileOutputStream outStream = new FileOutputStream(prop
						.getProperty("SYNCFILE-LOCATION").trim());
				props.setProperty("DB-RESTORE", "Y");
				props.store(outStream, null); 
				boolean status = updateLastRestoreDate();
				boolean dbRestoreStatus = false;
				if (status)
					dbRestoreStatus = processBackUpRestoreData(status); 
				LocalDate currentDate = DateTime.now().toLocalDate();
				inputStream = new FileInputStream(prop
						.getProperty("SYNCFILE-LOCATION").trim());
				props = new Properties();
				props.load(inputStream);
				outStream = new FileOutputStream(prop
						.getProperty("SYNCFILE-LOCATION").trim());
				if (dbRestoreStatus)
					props.setProperty("LAST-RESTORE-DATE", currentDate.toString());
				else 
					log.info("Database synchronization has failured");
				props.setProperty("DB-RESTORE", "N");
				props.store(outStream, null);
				startApplication();
				log.info("Application has started successfully.");
			} else{
				log.info("Application is already set to start..");
			}
		} catch (Exception ex) {
			log.info("Application has failed to start. " + ex.getMessage());
		}
	}
}
