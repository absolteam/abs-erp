package com.aiotech.aios.app.service;

import java.util.List;

import org.hibernate.Session;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.Reference;

public class TransactionHandlerService {

	/**
	 * Get all PointOfSale transactions from local database
	 * 
	 * @param implementation
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PointOfSale> getAllPOSTransactions(
			Implementation implementation, Session session) throws Exception {

		String getOfflinePointOfSales = "SELECT DISTINCT(p) FROM PointOfSale p "
				+ " JOIN FETCH p.POSUserTill till"
				+ " LEFT JOIN FETCH p.customer cust"
				+ " LEFT JOIN FETCH cust.shippingDetails ship"
				+ " LEFT JOIN FETCH cust.personByPersonId cstpr"
				+ " LEFT JOIN FETCH p.pointOfSaleReceipts receipts"
				+ " LEFT JOIN FETCH p.pointOfSaleDetails saleDetails"
				+ " LEFT JOIN FETCH saleDetails.productByProductId prd"
				+ " LEFT JOIN FETCH saleDetails.productByComboProductId cprd"
				+ " LEFT JOIN FETCH saleDetails.productPricingCalc pricingCalc"
				+ " LEFT JOIN FETCH saleDetails.merchandiseExchangeDetails exchangeDetails"
				+ " LEFT JOIN FETCH exchangeDetails.merchandiseExchange exchange"
				+ " LEFT JOIN FETCH p.pointOfSaleOffers saleOffer"
				+ " LEFT JOIN FETCH saleOffer.rewardPolicy reward"
				+ " LEFT JOIN FETCH saleOffer.coupon coupon"
				+ " LEFT JOIN FETCH p.pointOfSaleCharges saleCharges"
				+ " LEFT JOIN FETCH saleCharges.lookupDetail chargesType"
				+ " WHERE p.offlineEntry = 1"
				+ " AND p.processedSession = 1"
				+ " AND p.implementation =  ?"
				+ " AND (p.deliveryStatus IS NULL OR p.deliveryStatus = 1)";
		List<PointOfSale> pointOfSales = session
				.createQuery(getOfflinePointOfSales)
				.setEntity(0, implementation).list();
		return pointOfSales;
	}

	@SuppressWarnings("unchecked")
	public Reference getReferenceAgainstUsecase(Implementation implementation,
			String usecase, Session session) throws Exception {
		String getReferenceAgainstUsecase = "SELECT r FROM Reference r "
				+ "WHERE r.usecase = ? AND r.implementation = ?";
		List<Reference> references = session
				.createQuery(getReferenceAgainstUsecase).setString(0, usecase)
				.setEntity(1, implementation).list();
		return references.get(0);
	}

	public void saveReference(Reference reference, Session session)
			throws Exception {
		session.saveOrUpdate(reference);
	}

	/**
	 * Save PointOfSale transactions to live database
	 * 
	 * @param pointOfSale
	 * @param pointOfSaleDetails
	 * @param pointOfSaleReceipts
	 * @param pointOfSaleOffers
	 * @param pointOfSaleCharges
	 * @param merchandiseExchange
	 * @param exchangeDetails
	 * @param session
	 * @throws Exception
	 */
	public void savePointOfSales(PointOfSale pointOfSale,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			MerchandiseExchange merchandiseExchange,
			List<MerchandiseExchangeDetail> exchangeDetails, Session session)
			throws Exception {
		session.saveOrUpdate(pointOfSale);
		for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
			session.saveOrUpdate(pointOfSaleDetail);
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				session.saveOrUpdate(pointOfSaleCharge);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			for (PointOfSaleOffer pointOfSaleOffer : pointOfSaleOffers)
				session.saveOrUpdate(pointOfSaleOffer);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				session.saveOrUpdate(pointOfSaleReceipt);
		}
		if (null != merchandiseExchange) {
			session.saveOrUpdate(merchandiseExchange);
			for (MerchandiseExchangeDetail merchandiseExchangeDetail : exchangeDetails) {
				merchandiseExchangeDetail
						.setMerchandiseExchange(merchandiseExchange);
				session.saveOrUpdate(merchandiseExchangeDetail);
			}
		}
	}

	public void deletePointOfSales(List<PointOfSale> pointOfSales,
			List<PointOfSaleDetail> pointOfSaleDetails,
			List<PointOfSaleCharge> pointOfSaleCharges,
			List<PointOfSaleOffer> pointOfSaleOffers,
			List<PointOfSaleReceipt> pointOfSaleReceipts,
			List<MerchandiseExchange> merchandiseExchanges,
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails,
			Session session) throws Exception {

		if (null != merchandiseExchangeDetails
				&& merchandiseExchangeDetails.size() > 0) {
			for (MerchandiseExchangeDetail merchandiseExchangeDetail : merchandiseExchangeDetails)
				session.delete(merchandiseExchangeDetail);
		}
		if (null != merchandiseExchanges && merchandiseExchanges.size() > 0) {
			for (MerchandiseExchange merchandiseExchange : merchandiseExchanges)
				session.delete(merchandiseExchange);
		}
		if (null != pointOfSaleCharges && pointOfSaleCharges.size() > 0) {
			for (PointOfSaleCharge pointOfSaleCharge : pointOfSaleCharges)
				session.delete(pointOfSaleCharge);
		}
		if (null != pointOfSaleOffers && pointOfSaleOffers.size() > 0) {
			for (PointOfSaleOffer pointOfSaleOffer : pointOfSaleOffers)
				session.delete(pointOfSaleOffer);
		}
		if (null != pointOfSaleReceipts && pointOfSaleReceipts.size() > 0) {
			for (PointOfSaleReceipt pointOfSaleReceipt : pointOfSaleReceipts)
				session.delete(pointOfSaleReceipt);
		}
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleDetail pointOfSaleDetail : pointOfSaleDetails)
				session.delete(pointOfSaleDetail);
		}
		if (null != pointOfSales && pointOfSales.size() > 0) {
			for (PointOfSale pointOfSale : pointOfSales)
				session.delete(pointOfSale);
		}
	}

	public void updateOfflineStatus(Implementation implementation,
			Session localSession) throws Exception {
		String getImplementationDetails = "SELECT DISTINCT(i) FROM Implementation i WHERE i = ?";
		implementation = (Implementation) localSession
				.createQuery(getImplementationDetails)
				.setEntity(0, implementation).uniqueResult();
		localSession.update(implementation);
	}

	public void updatePointOfSale(List<PointOfSale> pointOfSales,
			Session session) throws Exception {
		for (PointOfSale pointOfSale : pointOfSales) {
			pointOfSale.setOfflineEntry(null);
			pointOfSale.setProcessedSession((byte) 2);
			session.saveOrUpdate(pointOfSale);
		}
	}

	public void savePerson(Person person, Session session) throws Exception {
		session.saveOrUpdate(person);
	}

	public void saveCustomer(Customer customer, ShippingDetail shippingDetail,
			Session session) throws Exception {
		session.saveOrUpdate(customer);
		shippingDetail.setCustomer(customer);
		session.saveOrUpdate(shippingDetail);
	}

	@SuppressWarnings("unchecked")
	public Account getAccountDetail(String account,
			Implementation implementation, Session session) {
		String getAccountDetail = "SELECT a FROM Account a  "
				+ " WHERE a.account = ? AND a.implementation = ?";
		List<Account> accounts = session.createQuery(getAccountDetail)
				.setString(0, account).setEntity(1, implementation).list();
		if (accounts != null && accounts.size() > 0)
			return accounts.get(0);
		else
			return null;
	}

	@SuppressWarnings("unchecked")
	public Combination getCombinationAllAccountDetail(long combinationId,
			Session session) throws Exception {
		String getCombinationAllAccountDetail = "SELECT c FROM Combination c"
				+ " JOIN FETCH c.accountByCompanyAccountId company"
				+ " JOIN FETCH c.accountByCostcenterAccountId cost"
				+ " JOIN FETCH c.accountByNaturalAccountId natural"
				+ " LEFT JOIN FETCH c.accountByAnalysisAccountId analysis"
				+ " WHERE c.combinationId=?";
		List<Combination> combinations = session
				.createQuery(getCombinationAllAccountDetail)
				.setLong(0, combinationId).list();
		return null != combinations && combinations.size() > 0 ? combinations
				.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Account getLastUpdatedAnalysis(Implementation implementation,
			Integer segmentId, Session session) {
		String getLastUpdatedAnalysis = "SELECT a FROM Account a  "
				+ " WHERE a.implementation=? AND a.segment.segmentId=?"
				+ " AND a.code=(SELECT MAX(CAST(c.code AS int))"
				+ " FROM Account c WHERE c.implementation=? AND c.segment.segmentId=?) GROUP BY a.code";
		List<Account> accounts = session.createQuery(getLastUpdatedAnalysis)
				.setEntity(0, implementation).setInteger(1, segmentId)
				.setEntity(2, implementation).setInteger(3, segmentId).list();
		return accounts.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Account> validateAccountDetails(String account, String code,
			Implementation implementation, Integer segmentId, Session session)
			throws Exception {
		String validateAccountDetails = "SELECT a FROM Account a "
				+ " WHERE a.implementation=? AND (a.account = ? OR a.code = ?) AND a.segment.segmentId=? ";
		List<Account> accounts = session.createQuery(validateAccountDetails)
				.setEntity(0, implementation).setString(1, account)
				.setString(2, code).setInteger(3, segmentId).list();
		return accounts;
	}

	public void updateAccount(Account account, Session session)
			throws Exception {
		session.saveOrUpdate(account);
	}

	@SuppressWarnings("unchecked")
	public Combination getCombination(long combinationId, Session session)
			throws Exception {
		String getCombination = "SELECT c FROM Combination c WHERE c.combinationId = ?";
		List<Combination> combinations = session.createQuery(getCombination)
				.setLong(0, combinationId).list();
		return combinations.get(0);
	}

	@SuppressWarnings("unchecked")
	public Implementation getImplementation(long implementationId,
			Session session) throws Exception {
		String getImplementation = "SELECT i FROM Implementation i WHERE i.implementationId = ?";
		List<Implementation> implementation = session
				.createQuery(getImplementation).setLong(0, implementationId)
				.list();
		return implementation.get(0);
	}

	public void saveCombination(Combination combination, Session session)
			throws Exception {
		session.saveOrUpdate(combination);
	}

	public void saveLedger(Ledger ledger, Session session) throws Exception {
		session.saveOrUpdate(ledger);
	}
}
