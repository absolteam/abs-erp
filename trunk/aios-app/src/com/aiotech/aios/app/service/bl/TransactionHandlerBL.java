package com.aiotech.aios.app.service.bl;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.hibernate.Session;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchangeDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleOffer;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.app.dbutil.HibernateLocalSession;
import com.aiotech.aios.app.service.TransactionHandlerService;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.Reference;
import com.mysql.jdbc.Connection;

public class TransactionHandlerBL {

	public List<PointOfSale> getPointOfSale(Implementation implementation,
			Session session) {
		try {
			return new TransactionHandlerService().getAllPOSTransactions(
					implementation, session);
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public Reference getSystemReference(Implementation implementation,
			Session localSession) throws Exception {
		Reference reference = new Reference();
		reference = new TransactionHandlerService().getReferenceAgainstUsecase(
				implementation,
				"com.aiotech.aios.accounts.domain.entity.PointOfSale",
				localSession);
		return reference;
	}

	public String saveReferenceStamp(String usecase,
			Implementation implementation, Session session) {
		try {
			Reference reference = new Reference();
			reference = new TransactionHandlerService()
					.getReferenceAgainstUsecase(implementation, usecase,
							session);
			if (generationPossible(reference))
				return generateUpdatedReferenceNumber(reference);
			else if (intializationPossible(reference))
				return intializeNGenerateUpdatedReference(reference);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Boolean generationPossible(Reference reference) {

		return (reference.getIsStarted() != null
				&& reference.getIsStarted() == true
				&& reference.getRefNumber() != null
				&& reference.getRefNumber() != 0 && reference.getRefDigits() != null);
	}

	private Boolean intializationPossible(Reference reference) {

		return ((reference.getIsStarted() == null || reference.getIsStarted() == false)
				&& (reference.getRefNumber() == null || reference
						.getRefNumber() == 0) && reference.getRefDigits() != null);
	}

	private String generateUpdatedReferenceNumber(Reference reference)
			throws Exception {

		reference.setRefNumber(reference.getRefNumber() + 1);
		long refNo = reference.getRefNumber();
		String generatedRefNo = null;
		int temp;

		if ((refNo + "").length() < reference.getRefDigits()) {

			temp = reference.getRefDigits() - (refNo + "").length();
			generatedRefNo = new String();
			while (generatedRefNo.length() < temp)
				generatedRefNo = generatedRefNo + "0";
			generatedRefNo = generatedRefNo.concat((refNo + ""));
		} else
			generatedRefNo = refNo + "";

		return generateReferenceString(reference, generatedRefNo);
	}

	private String intializeNGenerateUpdatedReference(Reference reference)
			throws Exception {

		long refNo = 1;
		String generatedRefNo = null;
		int temp;

		if ((refNo + "").length() < reference.getRefDigits()) {

			temp = reference.getRefDigits() - (refNo + "").length();
			generatedRefNo = new String();
			while (generatedRefNo.length() < temp)
				generatedRefNo = generatedRefNo + "0";
			generatedRefNo = generatedRefNo.concat((refNo + ""));
		} else
			generatedRefNo = refNo + "";

		reference.setIsStarted(true);
		reference.setRefNumber(refNo);
		return generateReferenceString(reference, generatedRefNo);
	}

	public String generateReferenceString(Reference reference,
			String generatedRefNo) throws Exception {

		String[] referenceString = new String[] { "XX$", "XX$", "XX$", "XX$",
				"XX$" };
		String toReturn = "";
		int i = 0;
		if (reference.getPrefix() != null
				&& !reference.getPrefix().trim().equals(""))
			referenceString[0] = reference.getPrefix();

		if (generatedRefNo != null && !generatedRefNo.trim().equals(""))
			referenceString[reference.getRefNumberPosition()] = generatedRefNo;

		if (!reference.getDateTag().trim().equals("")
				&& !reference.getDateTag().trim().equals("XX$"))
			referenceString[reference.getDateTagPosition()] = getFormatedDate(reference
					.getDateTag());

		if (reference.getPrefix() != null
				&& !reference.getPrefix().trim().equals(""))
			referenceString[reference.getSymbolTagPosition()] = reference
					.getSymbolTag();
		referenceString[4] = reference.getPostfix();

		while (i < referenceString.length) {
			if (!referenceString[i].equals("XX$"))
				toReturn += referenceString[i++] + "-";
			else
				i++;
		}
		return removeTrailingSymbols(toReturn);
	}

	private String getFormatedDate(String requiredFormat) {

		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter;
		try {
			formatter = new SimpleDateFormat(requiredFormat);
		} catch (Exception e) {
			e.printStackTrace();
			formatter = new SimpleDateFormat("MMddyyyy");
		}
		return formatter.format(currentDate.getTime());

	}

	public static String removeTrailingSymbols(String text) {

		String trimmed = text.trim();
		return (trimmed.length() != 0) ? trimmed.substring(0,
				trimmed.length() - 1) : trimmed;
	}

	/**
	 * Post transaction from local database to live database
	 * 
	 * @param implementation
	 *            object
	 * @throws Exception
	 * @return the status true/false
	 */
	public boolean postOfflineTransaction(List<PointOfSale> pointOfSales,
			Session session) {
		try {
			TransactionHandlerService handlerService = new TransactionHandlerService();
			session.getTransaction().begin();
			PointOfSale pointOfSale = null;
			List<PointOfSaleDetail> pointOfSaleDetails = null;
			List<MerchandiseExchangeDetail> exchangeDetails = null;
			List<PointOfSaleCharge> pointOfSaleCharges = null;
			PointOfSaleCharge pointOfSaleCharge = null;
			List<PointOfSaleOffer> pointOfSaleOffers = null;
			PointOfSaleOffer pointOfSaleOffer = null;
			List<PointOfSaleReceipt> pointOfSaleReceipts = null;
			PointOfSaleReceipt pointOfSaleReceipt = null;
			MerchandiseExchangeDetail exchangeDetail = null;
			MerchandiseExchange merchandiseExchange = null;
			PointOfSaleDetail pointOfSaleDetail = null;

			List<PointOfSale> pointOfSaleEmployeeService = new ArrayList<PointOfSale>();
			boolean employeeService = false;
			for (PointOfSale sale : pointOfSales) {
				employeeService = false;
				if ((byte) sale.getSaleType() == (byte) 4) {
					pointOfSaleEmployeeService.add(sale);
					employeeService = true;
				}
				pointOfSale = new PointOfSale();
				BeanUtils.copyProperties(pointOfSale, sale);
				pointOfSale.setPointOfSaleId(null);
				pointOfSale.setOfflineEntry(null);
				pointOfSale.setProcessedSession((byte) 2);
				pointOfSale.setReferenceNumber(saveReferenceStamp(
						PointOfSale.class.getName(),
						pointOfSale.getImplementation(), session));
				if (!employeeService) {
					if (null != pointOfSale.getCustomer()) {
						if (null != sale.getCustomer().getCustomerRecordId()
								&& sale.getCustomer().getCustomerRecordId() > 0) {
							Customer customer = new Customer();
							customer.setCustomerId(sale.getCustomer()
									.getCustomerRecordId());
							pointOfSale.setCustomer(customer);
						} else if (null != sale.getCustomer().getPosCustomer()
								&& (boolean) sale.getCustomer()
										.getPosCustomer() == true) {
							Person person = new Person();
							person.setPersonId(sale.getCustomer()
									.getPersonByPersonId().getPersonId());
							Customer customer = saveCustomerPerson(
									sale.getCustomer().getPersonByPersonId(),
									sale.getCustomer(),
									new ArrayList<ShippingDetail>(sale
											.getCustomer().getShippingDetails())
											.get(0), session, pointOfSale
											.getImplementation());
							pointOfSale.setCustomer(customer);
						} else
							pointOfSale.setCustomer(sale.getCustomer());
					}
				}
				pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
				for (PointOfSaleDetail saleDetail : sale
						.getPointOfSaleDetails()) {
					pointOfSaleDetail = new PointOfSaleDetail();
					BeanUtils.copyProperties(pointOfSaleDetail, saleDetail);
					pointOfSaleDetail.setPointOfSaleDetailId(null);
					pointOfSaleDetail.setPointOfSale(pointOfSale);
					pointOfSaleDetails.add(pointOfSaleDetail);
					if (!employeeService) {
						if (null != saleDetail.getMerchandiseExchangeDetails()
								&& saleDetail.getMerchandiseExchangeDetails()
										.size() > 0) {
							if (null == merchandiseExchange) {
								List<MerchandiseExchangeDetail> tempExchangeDetail = new ArrayList<MerchandiseExchangeDetail>(
										saleDetail
												.getMerchandiseExchangeDetails());
								merchandiseExchange = new MerchandiseExchange();
								merchandiseExchange
										.setReferenceNumber((saveReferenceStamp(
												MerchandiseExchange.class
														.getName(), pointOfSale
														.getImplementation(),
												session)));
								merchandiseExchange.setDate(tempExchangeDetail
										.get(0).getMerchandiseExchange()
										.getDate());
								merchandiseExchange.setImplementation(sale
										.getImplementation());
							}
							exchangeDetails = new ArrayList<MerchandiseExchangeDetail>();
							for (MerchandiseExchangeDetail merchandiseExchangeDetail : saleDetail
									.getMerchandiseExchangeDetails()) {
								exchangeDetail = new MerchandiseExchangeDetail();
								BeanUtils.copyProperties(exchangeDetail,
										merchandiseExchangeDetail);
								exchangeDetail
										.setMerchandiseExchangeDetailId(null);
								exchangeDetail
										.setPointOfSaleDetail(pointOfSaleDetail);
								exchangeDetail
										.setMerchandiseExchange(merchandiseExchangeDetail
												.getMerchandiseExchange());
								exchangeDetails.add(exchangeDetail);
							}
						}
						if (null != exchangeDetails
								&& exchangeDetails.size() > 0) {
							pointOfSaleDetail
									.setMerchandiseExchangeDetails(new HashSet<MerchandiseExchangeDetail>(
											exchangeDetails));
						}
					}
				}
				pointOfSale
						.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
								pointOfSaleDetails));
				if (!employeeService
						&& null != pointOfSale.getPointOfSaleCharges()
						&& pointOfSale.getPointOfSaleCharges().size() > 0) {
					pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();
					for (PointOfSaleCharge saleCharge : pointOfSale
							.getPointOfSaleCharges()) {
						pointOfSaleCharge = new PointOfSaleCharge();
						BeanUtils.copyProperties(pointOfSaleCharge, saleCharge);
						pointOfSaleCharge.setPointOfSale(pointOfSale);
						pointOfSaleCharge.setPointOfSaleChargeId(null);
						pointOfSaleCharges.add(pointOfSaleCharge);
					}
					pointOfSale
							.setPointOfSaleCharges(new HashSet<PointOfSaleCharge>(
									pointOfSaleCharges));
				}
				if (!employeeService
						&& null != pointOfSale.getPointOfSaleOffers()
						&& pointOfSale.getPointOfSaleOffers().size() > 0) {
					pointOfSaleOffers = new ArrayList<PointOfSaleOffer>();
					for (PointOfSaleOffer saleOffer : pointOfSale
							.getPointOfSaleOffers()) {
						pointOfSaleOffer = new PointOfSaleOffer();
						BeanUtils.copyProperties(pointOfSaleOffer, saleOffer);
						pointOfSaleOffer.setPointOfSale(pointOfSale);
						pointOfSaleOffer.setPointOfSaleOfferId(null);
						pointOfSaleOffers.add(pointOfSaleOffer);
					}
					pointOfSale
							.setPointOfSaleOffers(new HashSet<PointOfSaleOffer>(
									pointOfSaleOffers));
				}
				if (!employeeService
						&& null != pointOfSale.getPointOfSaleReceipts()
						&& pointOfSale.getPointOfSaleReceipts().size() > 0) {
					pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
					for (PointOfSaleReceipt saleReceipt : pointOfSale
							.getPointOfSaleReceipts()) {
						pointOfSaleReceipt = new PointOfSaleReceipt();
						BeanUtils.copyProperties(pointOfSaleReceipt,
								saleReceipt);
						pointOfSaleReceipt.setPointOfSale(pointOfSale);
						pointOfSaleReceipt.setPointOfSaleReceiptId(null);
						pointOfSaleReceipts.add(pointOfSaleReceipt);
					}
					pointOfSale
							.setPointOfSaleReceipts(new HashSet<PointOfSaleReceipt>(
									pointOfSaleReceipts));
				}
				handlerService.savePointOfSales(pointOfSale,
						pointOfSaleDetails, pointOfSaleCharges,
						pointOfSaleOffers, pointOfSaleReceipts,
						merchandiseExchange, exchangeDetails, session);
			}
			session.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			session.getTransaction().rollback();
			ex.printStackTrace();
			return false;
		} finally {
			session.clear();
			session.close();
		}
	}

	public Customer saveCustomerPerson(Person person, Customer customer,
			ShippingDetail shippingDetail, Session session,
			Implementation implementation) {
		try {
			TransactionHandlerService handlerService = new TransactionHandlerService();
			person.setPersonId(null);
			customer.setCustomerId(null);
			shippingDetail.setShippingId(null);
			implementation = handlerService.getImplementation(
					implementation.getImplementationId(), session);
			Combination combination = getPOSCombinationInfo(
					implementation.getAccountsReceivable(),
					person.getFirstName() + "-" + person.getPersonNumber()
							+ "-CUSTOMER", session, implementation);
			customer.setCombination(combination);
			handlerService.savePerson(person, session);
			customer.setPersonByPersonId(person);
			customer.setPosCustomer(false);
			handlerService.saveCustomer(customer, shippingDetail, session);
			return customer;
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			return null;
		}
	}

	public Combination getPOSCombinationInfo(Long natualAccountCombinationId,
			String currentName, Session session, Implementation implementation)
			throws Exception {
		TransactionHandlerService handlerService = new TransactionHandlerService();
		Combination combination = null;
		if (natualAccountCombinationId == null
				|| natualAccountCombinationId == 0)
			return combination;

		Account account = handlerService.getAccountDetail(currentName,
				implementation, session);
		if (account != null) {
			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = handlerService.getCombinationAllAccountDetail(
					combination.getCombinationId(), session);
		} else {

			combination = new Combination();
			combination.setCombinationId(natualAccountCombinationId);
			combination = createAnalysisCombination(combination, currentName,
					false, session, implementation);
		}
		return combination;
	}

	public Combination createAnalysisCombination(Combination combination,
			String accountDesc, Boolean isSystem, Session session,
			Implementation implementation) throws Exception {
		Account account = createAnalyisAccount(accountDesc, isSystem, session,
				implementation);
		if (null != account && !account.equals("")) {
			TransactionHandlerService handlerService = new TransactionHandlerService();
			combination = handlerService.getCombination(
					combination.getCombinationId(), session);
			if (null != combination && !combination.equals("")) {
				Combination newCombiantion = new Combination();
				newCombiantion.setAccountByCompanyAccountId(combination
						.getAccountByCompanyAccountId());
				newCombiantion.setAccountByCostcenterAccountId(combination
						.getAccountByCostcenterAccountId());
				newCombiantion.setAccountByNaturalAccountId(combination
						.getAccountByNaturalAccountId());
				newCombiantion.setAccountByAnalysisAccountId(account);
				saveCombination(newCombiantion, session);
				return newCombiantion;
			}
			return null;
		} else {
			return null;
		}
	}

	public Account createAnalyisAccount(String accountDesc, Boolean isSystem,
			Session session, Implementation implementation) throws Exception {
		int segmentId = 4;
		TransactionHandlerService handlerService = new TransactionHandlerService();
		Account accounts = handlerService.getLastUpdatedAnalysis(
				implementation, segmentId, session);
		if (validateAccounts(
				String.valueOf(new Integer(accounts.getCode().trim()) + 1),
				accountDesc, segmentId, session, implementation)) {
			Account newAccount = new Account();
			newAccount.setCode(String.valueOf(new Integer(accounts.getCode()
					.trim()) + 1));
			newAccount.setAccount(accountDesc);
			newAccount.setSegment(accounts.getSegment());
			newAccount.setImplementation(accounts.getImplementation());
			newAccount.setAccountType(accounts.getAccountType());
			newAccount.setIsSystem(isSystem);
			newAccount.setAccountId(generateAccountId(
					accounts.getImplementation(), newAccount.getCode()));
			handlerService.updateAccount(newAccount, session);
			return newAccount;
		} else {
			return null;
		}
	}

	private Long generateAccountId(Implementation implementation,
			String accountCode) throws Exception {
		return (Long.parseLong(String
				.valueOf(
						implementation.getImplementationId().toString()
								.length())
				.concat(implementation.getImplementationId().toString())
				.concat(String.valueOf(accountCode.length())
						.concat(accountCode))));
	}

	private boolean validateAccounts(String code, String account,
			Integer segmentId, Session session, Implementation implementation)
			throws Exception {
		TransactionHandlerService handlerService = new TransactionHandlerService();
		List<Account> accountList = handlerService.validateAccountDetails(
				account, code, implementation, segmentId, session);
		if (null != accountList && accountList.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	public void saveCombination(Combination combination, Session session)
			throws Exception {
		TransactionHandlerService handlerService = new TransactionHandlerService();
		handlerService.saveCombination(combination, session);
		if (null != combination.getAccountByNaturalAccountId()
				&& !combination.getAccountByNaturalAccountId().equals("")) {
			Ledger ledger = new Ledger();
			ledger.setCombination(combination);
			ledger.setSide(true);
			handlerService.saveLedger(ledger, session);
		}
	}

	public void rollBackLivePOSTransaction(List<PointOfSale> pointOfSales,
			Session liveSession) {
		try {
			List<PointOfSaleDetail> pointOfSaleDetails = new ArrayList<PointOfSaleDetail>();
			List<PointOfSaleCharge> pointOfSaleCharges = new ArrayList<PointOfSaleCharge>();
			List<PointOfSaleOffer> pointOfSaleOffers = new ArrayList<PointOfSaleOffer>();
			List<PointOfSaleReceipt> pointOfSaleReceipts = new ArrayList<PointOfSaleReceipt>();
			List<MerchandiseExchangeDetail> merchandiseExchangeDetails = new ArrayList<MerchandiseExchangeDetail>();
			List<MerchandiseExchange> merchandiseExchanges = new ArrayList<MerchandiseExchange>();
			MerchandiseExchange merchandiseExchange = null;
			TransactionHandlerService handlerService = new TransactionHandlerService();
			for (PointOfSale pointOfSale : pointOfSales) {
				merchandiseExchange = null;
				pointOfSaleDetails.addAll(new ArrayList<PointOfSaleDetail>(
						pointOfSale.getPointOfSaleDetails()));
				if (null != pointOfSale.getPointOfSaleCharges()
						&& pointOfSale.getPointOfSaleCharges().size() > 0)
					pointOfSaleCharges.addAll(pointOfSale
							.getPointOfSaleCharges());
				if (null != pointOfSale.getPointOfSaleOffers()
						&& pointOfSale.getPointOfSaleOffers().size() > 0)
					pointOfSaleOffers
							.addAll(pointOfSale.getPointOfSaleOffers());
				if (null != pointOfSale.getPointOfSaleReceipts()
						&& pointOfSale.getPointOfSaleReceipts().size() > 0)
					pointOfSaleReceipts.addAll(pointOfSale
							.getPointOfSaleReceipts());
				for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
						.getPointOfSaleDetails()) {
					if (null != pointOfSaleDetail
							.getMerchandiseExchangeDetails()
							&& pointOfSaleDetail
									.getMerchandiseExchangeDetails().size() > 0) {
						merchandiseExchangeDetails
								.addAll(new ArrayList<MerchandiseExchangeDetail>(
										pointOfSaleDetail
												.getMerchandiseExchangeDetails()));
						merchandiseExchange = new MerchandiseExchange();
						merchandiseExchange = merchandiseExchangeDetails.get(0)
								.getMerchandiseExchange();
					}
				}
				if (null != merchandiseExchange)
					merchandiseExchanges.add(merchandiseExchange);
			}
			liveSession.getTransaction().begin();
			handlerService.deletePointOfSales(pointOfSales, pointOfSaleDetails,
					pointOfSaleCharges, pointOfSaleOffers, pointOfSaleReceipts,
					merchandiseExchanges, merchandiseExchangeDetails,
					liveSession);
			liveSession.getTransaction().commit();
		} catch (Exception e) {
			liveSession.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			liveSession.close();
		}
	}

	/**
	 * Update Offline Status to Implementation table
	 * 
	 * @param implementation
	 * @param localSession
	 */
	public void updateOfflineStatus(Implementation implementation,
			Session localSession) {
		try {
			localSession = HibernateLocalSession.getSessionFactory()
					.openSession();
			localSession.getTransaction().begin();
			TransactionHandlerService handlerService = new TransactionHandlerService();
			handlerService.updateOfflineStatus(implementation, localSession);
			localSession.getTransaction().commit();
		} catch (Exception e) {
			localSession.getTransaction().rollback();
			e.printStackTrace();
		}
	}

	public boolean updatePointOfSale(List<PointOfSale> pointOfSales,
			Session session) {
		try {
			TransactionHandlerService handlerService = new TransactionHandlerService();
			session.getTransaction().begin();
			handlerService.updatePointOfSale(pointOfSales, session);
			session.getTransaction().commit();
			return true;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			return false;
		} finally {
			session.close();
		}
	}

	public void updatePointOfSale(List<PointOfSale> pointOfSales)
			throws Exception {
		Connection connection = null;
		try {
			connection = getConnection();
			QueryRunner queryRunner = null;
			for (PointOfSale pointOfSale : pointOfSales) {
				String query = " UPDATE ac_point_of_sale SET offline_entry="
						+ null + ", processed_session =" + (byte) 2
						+ " WHERE point_of_sale_id="
						+ pointOfSale.getPointOfSaleId();
				queryRunner = new QueryRunner();
				queryRunner.update(connection, query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}

	public void saveReference(Reference reference) throws Exception {
		Connection connection = null;
		try {
			connection = getConnection();
			QueryRunner queryRunner = null;
			String query = " UPDATE sys_reference SET"
					+ "   prefix = '" + reference.getPrefix() +"'"
					+ " , ref_number = " + reference.getRefNumber()
					+ " , ref_digits = " + reference.getRefDigits()
					+ " , ref_number_position = " + reference.getRefNumberPosition()
					+ " , is_started = " + reference.getIsStarted()
					+ " WHERE ref_id = " + reference.getRefId();
			queryRunner = new QueryRunner();
			queryRunner.update(connection, query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}

	public void revertPointOfSale(List<PointOfSale> pointOfSales)
			throws Exception {
		Connection connection = null;
		try {
			connection = getConnection();
			QueryRunner queryRunner = null;
			for (PointOfSale pointOfSale : pointOfSales) {
				String query = " UPDATE ac_point_of_sale SET offline_entry="
						+ false + ", processed_session =" + (byte) 2
						+ " WHERE point_of_sale_id="
						+ pointOfSale.getPointOfSaleId();
				queryRunner = new QueryRunner();
				queryRunner.update(connection, query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}

	public static Connection getConnection() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connect = null;
		try {
			connect = (Connection) DriverManager
					.getConnection("jdbc:mysql://localhost/aios?"
							+ "user=root&password=root");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connect;
	}

	public static void closeConnection(Connection conn) {
		if (conn != null)
			DbUtils.closeQuietly(conn);
	}
}
