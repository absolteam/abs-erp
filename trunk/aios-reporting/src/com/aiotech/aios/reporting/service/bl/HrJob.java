package com.aiotech.aios.reporting.service.bl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.aiotech.aios.common.util.AttendanceReader;

public class HrJob implements Job {
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		Map dataMap = context.getJobDetail().getJobDataMap();
		HrBL dailyMorning = (HrBL) dataMap.get("hrDailyMorningSchedule");

		try {

			if (dailyMorning != null) {
				//Read Attendance from Local Attendance server to local ERP DB
				//AttendanceReader.startReadData();
				
			/*	dailyMorning.getAttendanceBL().attedanceReader(null, null);
				dailyMorning.getAttendanceBL().attendaceExcution(null, null,
						null,null);*/
				
				//Identity Expiry reminder
				dailyMorning.getAlertBL().identityExpiryReminder();

				// Identity Handover process
				dailyMorning.getAlertBL().getIdentityHandOver();

				// Promotion Demotion alert process
				dailyMorning.getPromotionDemotionBL().autoJobAssignment(null);

				// Termination & Resignation alert process
				dailyMorning.getResignationTerminationBL()
						.autoDisableTheJobAssignment(null);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
