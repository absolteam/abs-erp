package com.aiotech.aios.reporting.service.bl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ProjectManagmentJob implements Job {
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		Map dataMap = context.getJobDetail().getJobDataMap();
		ProjectManagmentBL dailyMorning = (ProjectManagmentBL) dataMap
				.get("projectManagmentDailyMorningSchedule");

		try {

			if (dailyMorning != null) {

				dailyMorning.getProjectMileStoneBL().createMileStoneAlerts();
				dailyMorning.getProjectMileStoneBL()
						.createProjectPaymentAlerts();
				dailyMorning.getProjectMileStoneBL()
						.createProjectDepositPaymentAlerts();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
