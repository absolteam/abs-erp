package com.aiotech.aios.reporting.service.bl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleDetailVO;
import com.aiotech.aios.accounts.service.AccountsEnterpriseService;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.service.HrEnterpriseService;
import com.aiotech.aios.hr.to.AttendanceTO;
import com.aiotech.aios.reporting.to.AccountsTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.DashboardVO;

public class DashboardBL {

	private AccountsEnterpriseService accountsEnterpriseService;
	private AccountsBL accountsBL;
	private Implementation implementation;
	private HrEnterpriseService hrEnterpriseService;

	public List<DashboardVO> getAllFiscalTransaction() {
		List<DashboardVO> list = getAllFiscalTransactionByCalendar();
		try {
			List<DashboardVO> modifiedList = new ArrayList<DashboardVO>();

			DashboardVO tempVo = new DashboardVO();
			tempVo.setTicks("");
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {

					if (i == 0) {
						for (DashboardVO dashboardVO2 : list.get(i)
								.getDashboardVOs()) {
							modifiedList.add(dashboardVO2);
						}
						tempVo.setDashboardVOs(modifiedList);
					} else {
						list.get(0)
								.getDashboardVOs()
								.get(0)
								.setAmount(
										(list.get(i).getDashboardVOs().get(0)
												.getAmount() != null ? list
												.get(i).getDashboardVOs()
												.get(0).getAmount() : 0.0)
												+ (list.get(0)
														.getDashboardVOs()
														.get(0).getAmount() != null ? list
														.get(0)
														.getDashboardVOs()
														.get(0).getAmount()
														: 0.0));
						list.get(0)
								.getDashboardVOs()
								.get(1)
								.setAmount(
										(list.get(i).getDashboardVOs().get(1)
												.getAmount() != null ? list
												.get(i).getDashboardVOs()
												.get(1).getAmount() : 0.0)
												+ (list.get(0)
														.getDashboardVOs()
														.get(1).getAmount() != null ? list
														.get(0)
														.getDashboardVOs()
														.get(1).getAmount()
														: 0.0));
						list.get(0)
								.getDashboardVOs()
								.get(2)
								.setAmount(
										(list.get(i).getDashboardVOs().get(2)
												.getAmount() != null ? list
												.get(i).getDashboardVOs()
												.get(2).getAmount() : 0.0)
												+ (list.get(0)
														.getDashboardVOs()
														.get(2).getAmount() != null ? list
														.get(0)
														.getDashboardVOs()
														.get(2).getAmount()
														: 0.0));
						list.get(0)
								.getDashboardVOs()
								.get(3)
								.setAmount(
										(list.get(i).getDashboardVOs().get(3)
												.getAmount() != null ? list
												.get(i).getDashboardVOs()
												.get(3).getAmount() : 0.0)
												+ (list.get(0)
														.getDashboardVOs()
														.get(3).getAmount() != null ? list
														.get(0)
														.getDashboardVOs()
														.get(3).getAmount()
														: 0.0));
					}
					tempVo.setTicks(tempVo.getTicks() + ","
							+ list.get(i).getTicks());
				}
				list = new ArrayList<DashboardVO>();
				list.add(tempVo);
			} else {
				list = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<DashboardVO> getAllFiscalTransactionByCalendar() {

		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		try {
			List<Calendar> calendars = this.getAccountsBL()
					.getAccountsEnterpriseService().getCalendarService()
					.getAllCalendar(getImplementation());
			DashboardVO dashboardVOGlobal = null;
			List<DashboardVO> subDashbordVOs = null;
			DashboardVO dashboardVO = null;
			for (Calendar calendar : calendars) {
				dashboardVOGlobal = new DashboardVO();
				subDashbordVOs = new ArrayList<DashboardVO>();
				List<LedgerFiscal> assetFiscals = accountsEnterpriseService
						.getCombinationService().getAssetStatementByCalendar(
								calendar.getCalendarId());
				List<AccountsTO> assetList = accountsBL
						.getAssetDetail(assetFiscals);
				dashboardVO = new DashboardVO();
				if (assetList != null && assetList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO asset : assetList) {

						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(asset
										.getAssetBalanceAmount())
								+ dashboardVO.getAmount());
					}
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> liablityFiscals = accountsEnterpriseService
						.getCombinationService()
						.getLiablityStatementByCalendar(
								calendar.getCalendarId());
				List<AccountsTO> liablityList = accountsBL
						.getLiabilityDetail(liablityFiscals);
				dashboardVO = new DashboardVO();
				if (liablityList != null && liablityList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO liability : liablityList) {
						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(liability
										.getLiabilityBalanceAmount())
								+ dashboardVO.getAmount());
					}

				}

				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> revenueFiscals = accountsEnterpriseService
						.getCombinationService().getRevenueStatementByCalendar(
								calendar.getCalendarId());
				List<AccountsTO> revenueList = accountsBL
						.getRevenueDetail(revenueFiscals);
				dashboardVO = new DashboardVO();
				if (revenueList != null && revenueList.size() > 0) {
					dashboardVO.setAmount(0.0);
					AccountsTO account = new AccountsTO();
					account.setRevenueList(revenueList);
					double revenueTotal = accountsBL.calcucateRevenue(account
							.getRevenueList());
					dashboardVO.setAmount(revenueTotal);
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> expenseFiscals = accountsEnterpriseService
						.getCombinationService().getExpenseStatementByCalendar(
								calendar.getCalendarId());
				List<AccountsTO> expenseList = accountsBL
						.getExpenseDetail(expenseFiscals);
				dashboardVO = new DashboardVO();
				if (expenseList != null && expenseList.size() > 0) {
					dashboardVO.setAmount(0.0);
					AccountsTO account = new AccountsTO();
					account.setExpenseList(expenseList);
					double expenseTotal = accountsBL.calcucateExpense(account
							.getExpenseList());
					dashboardVO.setAmount(expenseTotal);
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> ownersEquityFiscals = accountsEnterpriseService
						.getCombinationService()
						.getOwnersEquityStatementByCalendar(
								calendar.getCalendarId());
				List<AccountsTO> ownersEquityList = accountsBL
						.getOwnersEquityDetail(ownersEquityFiscals);
				dashboardVO = new DashboardVO();
				if (ownersEquityList != null && ownersEquityList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO accountsTO : ownersEquityList) {
						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(accountsTO
										.getLiabilityBalanceAmount())
								+ dashboardVO.getAmount());
					}

				}
				subDashbordVOs.add(dashboardVO);

				dashboardVOGlobal.setTicks(calendar.getName());
				dashboardVOGlobal.setDashboardVOs(subDashbordVOs);
				dashboardVOs.add(dashboardVOGlobal);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardVOs;

	}

	public List<DashboardVO> getAllFiscalTransactionByPeriod() {

		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		try {
			List<Period> periods = this.getAccountsBL()
					.getAccountsEnterpriseService().getCalendarService()
					.getAllPeriods(getImplementation());
			Collections.sort(periods, new Comparator<Period>() {
				public int compare(Period m1, Period m2) {
					return m2.getPeriodId().compareTo(m1.getPeriodId());
				}
			});
			DashboardVO dashboardVOGlobal = null;
			List<DashboardVO> subDashbordVOs = null;
			DashboardVO dashboardVO = null;
			int i = 0;
			for (Period period : periods) {
				if (i >= 4)
					continue;
				dashboardVOGlobal = new DashboardVO();
				subDashbordVOs = new ArrayList<DashboardVO>();
				List<LedgerFiscal> assetFiscals = accountsEnterpriseService
						.getCombinationService().getAssetStatementByPeriod(
								period.getPeriodId());
				List<AccountsTO> assetList = accountsBL
						.getAssetDetail(assetFiscals);
				dashboardVO = new DashboardVO();
				if (assetList != null && assetList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO asset : assetList) {

						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(asset
										.getAssetBalanceAmount())
								+ dashboardVO.getAmount());
					}
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> liablityFiscals = accountsEnterpriseService
						.getCombinationService().getLiablityStatementByPeriod(
								period.getPeriodId());
				List<AccountsTO> liablityList = accountsBL
						.getLiabilityDetail(liablityFiscals);
				dashboardVO = new DashboardVO();
				if (liablityList != null && liablityList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO liability : liablityList) {
						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(liability
										.getLiabilityBalanceAmount())
								+ dashboardVO.getAmount());
					}

				}

				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> revenueFiscals = accountsEnterpriseService
						.getCombinationService().getRevenueStatementByPeriod(
								period.getPeriodId());
				List<AccountsTO> revenueList = accountsBL
						.getRevenueDetail(revenueFiscals);
				dashboardVO = new DashboardVO();
				if (revenueList != null && revenueList.size() > 0) {
					dashboardVO.setAmount(0.0);
					AccountsTO account = new AccountsTO();
					account.setRevenueList(revenueList);
					double revenueTotal = accountsBL.calcucateRevenue(account
							.getRevenueList());
					dashboardVO.setAmount(revenueTotal);
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> expenseFiscals = accountsEnterpriseService
						.getCombinationService().getExpenseStatementByPeriod(
								period.getPeriodId());
				List<AccountsTO> expenseList = accountsBL
						.getExpenseDetail(expenseFiscals);
				dashboardVO = new DashboardVO();
				if (expenseList != null && expenseList.size() > 0) {
					dashboardVO.setAmount(0.0);
					AccountsTO account = new AccountsTO();
					account.setExpenseList(expenseList);
					double expenseTotal = accountsBL.calcucateExpense(account
							.getExpenseList());
					dashboardVO.setAmount(expenseTotal);
				}
				subDashbordVOs.add(dashboardVO);

				List<LedgerFiscal> ownersEquityFiscals = accountsEnterpriseService
						.getCombinationService()
						.getOwnersEquityStatementByPeriod(period.getPeriodId());
				List<AccountsTO> ownersEquityList = accountsBL
						.getOwnersEquityDetail(ownersEquityFiscals);
				dashboardVO = new DashboardVO();
				if (ownersEquityList != null && ownersEquityList.size() > 0) {
					dashboardVO.setAmount(0.0);
					for (AccountsTO accountsTO : ownersEquityList) {
						dashboardVO.setAmount(AIOSCommons
								.formatAmountToDouble(accountsTO
										.getLiabilityBalanceAmount())
								+ dashboardVO.getAmount());
					}

				}
				subDashbordVOs.add(dashboardVO);
				dashboardVOGlobal.setId(period.getPeriodId());
				dashboardVOGlobal.setTicks(period.getName());
				dashboardVOGlobal.setDashboardVOs(subDashbordVOs);
				dashboardVOs.add(dashboardVOGlobal);
				i++;
			}
			Collections.sort(dashboardVOs, new Comparator<DashboardVO>() {
				public int compare(DashboardVO m1, DashboardVO m2) {
					return m1.getId().compareTo(m2.getId());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dashboardVOs;

	}

	public List<DashboardVO> dailySales(Long storeId) {

		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		// Month calculations
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusDays(5));
		LocalDate date2 = new LocalDate(date.plusDays(1));
		DashboardVO dashboardVOTemp = null;
		while (date1.isBefore(date2)) {
			dashboardVOTemp = new DashboardVO();
			dashboardVOTemp.setStartDate(date1.toDate());
			dashboardVOTemp.setEndDate(date1.toDate());
			dashboardVOTemp.setStartDateView(date1.toString("dd-MMM-yyyy"));
			date1 = date1.plusDays(1);
			dashboardVOs.add(dashboardVOTemp);
		}

		for (DashboardVO dashboardVO : dashboardVOs) {

			List<PointOfSale> pointOfSalesList = accountsEnterpriseService
					.getPointOfSaleService().getFilteredPeriodWiseSales(
							getImplementation(), dashboardVO.getStartDate(),
							dashboardVO.getEndDate(), storeId);

			dashboardVO.setAmount(0.0);
			for (PointOfSale pos : pointOfSalesList) {

				Double totalAmount = 0.0;
				for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
					PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
							detail);

					pointOfSaleDetailVO.setProductName(detail
							.getProductByProductId().getProductName());

					Double total = detail.getUnitRate() * detail.getQuantity();

					pointOfSaleDetailVO.setTotalAmount(total);
					pointOfSaleDetailVO.setTotalPrice(total.toString());
					if (detail.getDiscountValue() != null) {
						pointOfSaleDetailVO.setDiscount(detail
								.getDiscountValue().toString());
						if (detail.getIsPercentageDiscount()) {
							pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
									.getDiscount().concat("%"));
						}
					}

					totalAmount += total;

				}

				dashboardVO.setAmount(totalAmount + dashboardVO.getAmount());

			}
		}
		return dashboardVOs;
	}

	public List<DashboardVO> monthlySales(Long storeId) {

		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusMonths(4));
		LocalDate date2 = new LocalDate(date);
		DashboardVO dashboardVOTemp = null;
		while (date1.isBefore(date2)) {
			dashboardVOTemp = new DashboardVO();
			date1 = date1.plusMonths(1);
			dashboardVOTemp.setStartDate(date1.dayOfMonth().withMinimumValue()
					.toDate());
			dashboardVOTemp.setEndDate(date1.dayOfMonth().withMaximumValue()
					.toDate());
			dashboardVOTemp.setStartDateView(date1.toString("MMM-yyyy"));
			dashboardVOs.add(dashboardVOTemp);
		}

		for (DashboardVO dashboardVO : dashboardVOs) {

			List<PointOfSale> pointOfSalesList = accountsEnterpriseService
					.getPointOfSaleService().getFilteredPeriodWiseSales(
							getImplementation(), dashboardVO.getStartDate(),
							dashboardVO.getEndDate(), storeId);

			dashboardVO.setAmount(0.0);
			for (PointOfSale pos : pointOfSalesList) {

				Double totalAmount = 0.0;
				for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
					PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
							detail);

					pointOfSaleDetailVO.setProductName(detail
							.getProductByProductId().getProductName());

					Double total = detail.getUnitRate() * detail.getQuantity();

					pointOfSaleDetailVO.setTotalAmount(total);
					pointOfSaleDetailVO.setTotalPrice(total.toString());
					if (detail.getDiscountValue() != null) {
						pointOfSaleDetailVO.setDiscount(detail
								.getDiscountValue().toString());
						if (detail.getIsPercentageDiscount()) {
							pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
									.getDiscount().concat("%"));
						}
					}

					totalAmount += total;

				}

				dashboardVO.setAmount(totalAmount + dashboardVO.getAmount());

			}
		}
		return dashboardVOs;
	}

	public List<DashboardVO> yearlySales(Long storeId) {

		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusYears(3));
		LocalDate date2 = new LocalDate(date);
		DashboardVO dashboardVOTemp = null;
		while (date1.isBefore(date2)) {
			dashboardVOTemp = new DashboardVO();
			date1 = date1.plusYears(1);
			dashboardVOTemp.setStartDate(date1.dayOfYear().withMinimumValue()
					.toDate());
			dashboardVOTemp.setEndDate(date1.dayOfYear().withMaximumValue()
					.toDate());
			dashboardVOTemp.setStartDateView(date1.toString("yyyy"));
			dashboardVOs.add(dashboardVOTemp);
		}

		for (DashboardVO dashboardVO : dashboardVOs) {

			List<PointOfSale> pointOfSalesList = accountsEnterpriseService
					.getPointOfSaleService().getFilteredPeriodWiseSales(
							getImplementation(), dashboardVO.getStartDate(),
							dashboardVO.getEndDate(), storeId);

			dashboardVO.setAmount(0.0);
			for (PointOfSale pos : pointOfSalesList) {

				Double totalAmount = 0.0;
				for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
					PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
							detail);

					pointOfSaleDetailVO.setProductName(detail
							.getProductByProductId().getProductName());

					Double total = detail.getUnitRate() * detail.getQuantity();

					pointOfSaleDetailVO.setTotalAmount(total);
					pointOfSaleDetailVO.setTotalPrice(total.toString());
					if (detail.getDiscountValue() != null) {
						pointOfSaleDetailVO.setDiscount(detail
								.getDiscountValue().toString());
						if (detail.getIsPercentageDiscount()) {
							pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
									.getDiscount().concat("%"));
						}
					}

					totalAmount += total;

				}

				dashboardVO.setAmount(totalAmount + dashboardVO.getAmount());

			}
		}
		return dashboardVOs;
	}

	public List<DashboardVO> salesByCategory(Date startDate, Date endDate,
			Long storeId) {
		Map<Long, DashboardVO> dashboarVOMaps = new HashMap<Long, DashboardVO>();

		DashboardVO dashboardVOTemp = null;
		try {
			if (null == startDate) {
				LocalDate localdt = DateTime.now().toLocalDate();
				startDate = DateFormat.convertStringToDate(DateFormat
						.convertDateToString(localdt.toString()));
				endDate = startDate;
			}
			List<PointOfSaleDetail> pointOfSalesList = accountsEnterpriseService
					.getPointOfSaleService().getFilteredCategoryBySales(
							getImplementation(), startDate, endDate, storeId);
			for (PointOfSaleDetail posDetail : pointOfSalesList) {

				if (posDetail.getProductByProductId().getProductCategory()
						.getProductCategory() != null
						&& dashboarVOMaps.containsKey(posDetail
								.getProductByProductId().getProductCategory()
								.getProductCategory().getProductCategoryId())) {
					dashboardVOTemp = dashboarVOMaps.get(posDetail
							.getProductByProductId().getProductCategory()
							.getProductCategory().getProductCategoryId());
					Double total = posDetail.getUnitRate()
							* posDetail.getQuantity();
					dashboardVOTemp.setAmount(total
							+ dashboardVOTemp.getAmount());
					dashboarVOMaps.put(posDetail.getProductByProductId()
							.getProductCategory().getProductCategory()
							.getProductCategoryId(), dashboardVOTemp);
				} else {
					dashboardVOTemp = new DashboardVO();
					Double total = posDetail.getUnitRate()
							* posDetail.getQuantity();
					dashboardVOTemp.setAmount(total);
					dashboardVOTemp.setName(posDetail.getProductByProductId()
							.getProductCategory().getProductCategory()
							.getCategoryName());
					dashboarVOMaps.put(posDetail.getProductByProductId()
							.getProductCategory().getProductCategory()
							.getProductCategoryId(), dashboardVOTemp);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ArrayList<DashboardVO>(dashboarVOMaps.values());
	}

	public List<DashboardVO> dailyProduction() {
		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		try {
			// Month calculations
			LocalDate date = new LocalDate(new Date());
			LocalDate date1 = new LocalDate(date.minusDays(5));
			LocalDate date2 = new LocalDate(date.plusDays(1));
			DashboardVO dashboardVOTemp = null;
			while (date1.isBefore(date2)) {
				dashboardVOTemp = new DashboardVO();
				dashboardVOTemp.setStartDate(date1.toDate());
				dashboardVOTemp.setEndDate(date1.toDate());
				dashboardVOTemp.setStartDateView(date1.toString("dd-MMM-yyyy"));
				dashboardVOTemp.setAmount(0.0);
				date1 = date1.plusDays(1);
				dashboardVOs.add(dashboardVOTemp);
			}

			for (DashboardVO dashboardVO : dashboardVOs) {
				List<ProductionReceiveDetail> productionReceivDetails = accountsEnterpriseService
						.getProductionReceiveService()
						.getProductionReceiveDetailsByPeriod(
								getImplementation(),
								dashboardVO.getStartDate(),
								dashboardVO.getEndDate(), null);
				for (ProductionReceiveDetail productionReceiveDetail : productionReceivDetails) {
					Double total = productionReceiveDetail.getQuantity()
							* productionReceiveDetail.getUnitRate();
					dashboardVO.setAmount(total + dashboardVO.getAmount());
				}

			}
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<DashboardVO> monthlyProduction() {
		List<DashboardVO> dashboardVOs = new ArrayList<DashboardVO>();
		try {
			LocalDate date = new LocalDate(new Date());
			LocalDate date1 = new LocalDate(date.minusMonths(4));
			LocalDate date2 = new LocalDate(date);
			DashboardVO dashboardVOTemp = null;
			while (date1.isBefore(date2)) {
				dashboardVOTemp = new DashboardVO();
				date1 = date1.plusMonths(1);
				dashboardVOTemp.setStartDate(date1.dayOfMonth()
						.withMinimumValue().toDate());
				dashboardVOTemp.setEndDate(date1.dayOfMonth()
						.withMaximumValue().toDate());
				dashboardVOTemp.setStartDateView(date1.toString("MMM-yyyy"));
				dashboardVOTemp.setAmount(0.0);
				dashboardVOs.add(dashboardVOTemp);
			}

			for (DashboardVO dashboardVO : dashboardVOs) {
				List<ProductionReceiveDetail> productionReceivDetails = accountsEnterpriseService
						.getProductionReceiveService()
						.getProductionReceiveDetailsByPeriod(
								getImplementation(),
								dashboardVO.getStartDate(),
								dashboardVO.getEndDate(), null);
				for (ProductionReceiveDetail productionReceiveDetail : productionReceivDetails) {
					Double total = productionReceiveDetail.getQuantity()
							* productionReceiveDetail.getUnitRate();
					dashboardVO.setAmount(total + dashboardVO.getAmount());
				}

			}
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<DashboardVO> employeeHeadCountsDesignation() {
		List<DashboardVO> dashboardVOs = null;
		try {
			List<JobAssignment> jobAssignments = hrEnterpriseService
					.getJobAssignmentService().getAllActiveJobAssignment(
							getImplementation());
			Map<String, DashboardVO> voMap = new HashMap<String, DashboardVO>();
			DashboardVO dashboardVO = null;
			for (JobAssignment jobAssignment : jobAssignments) {
				if (voMap.containsKey(jobAssignment.getDesignation()
						.getDesignationName())) {
					dashboardVO = voMap.get(jobAssignment.getDesignation()
							.getDesignationName());
					dashboardVO.setCount(dashboardVO.getCount() + 1);
					voMap.put(jobAssignment.getDesignation()
							.getDesignationName(), dashboardVO);
				} else {
					dashboardVO = new DashboardVO();
					dashboardVO.setCount((int) 1);
					dashboardVO.setName(jobAssignment.getDesignation()
							.getDesignationName());
					voMap.put(jobAssignment.getDesignation()
							.getDesignationName(), dashboardVO);
				}

			}
			dashboardVOs = new ArrayList<DashboardVO>(voMap.values());
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<DashboardVO> employeeHeadCountsByDepartment() {
		List<DashboardVO> dashboardVOs = null;
		try {
			List<JobAssignment> jobAssignments = hrEnterpriseService
					.getJobAssignmentService().getAllActiveJobAssignment(
							getImplementation());
			Map<String, DashboardVO> voMap = new HashMap<String, DashboardVO>();
			DashboardVO dashboardVO = null;
			for (JobAssignment jobAssignment : jobAssignments) {
				if (voMap.containsKey(jobAssignment.getCmpDeptLocation()
						.getDepartment().getDepartmentName())) {
					dashboardVO = voMap.get(jobAssignment.getCmpDeptLocation()
							.getDepartment().getDepartmentName());
					dashboardVO.setCount(dashboardVO.getCount() + 1);
					voMap.put(jobAssignment.getCmpDeptLocation()
							.getDepartment().getDepartmentName(), dashboardVO);
				} else {
					dashboardVO = new DashboardVO();
					dashboardVO.setCount((int) 1);
					dashboardVO.setName(jobAssignment.getCmpDeptLocation()
							.getDepartment().getDepartmentName());
					voMap.put(jobAssignment.getCmpDeptLocation()
							.getDepartment().getDepartmentName(), dashboardVO);
				}

			}
			dashboardVOs = new ArrayList<DashboardVO>(voMap.values());
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<DashboardVO> wagesByDesignation() {
		List<DashboardVO> dashboardVOs = null;
		try {
			List<Payroll> payrolls = hrEnterpriseService.getPayrollService()
					.getAllCommittedPayrolls(getImplementation());
			Map<String, DashboardVO> voMap = new HashMap<String, DashboardVO>();
			Map<Long, JobAssignment> dublicateEmployee = new HashMap<Long, JobAssignment>();
			DashboardVO dashboardVO = null;
			for (Payroll payroll : payrolls) {

				if (dublicateEmployee.containsKey(payroll.getJobAssignment()
						.getPerson().getPersonId()))
					continue;
				else
					dublicateEmployee.put(payroll.getJobAssignment()
							.getPerson().getPersonId(),
							payroll.getJobAssignment());

				if (voMap.containsKey(payroll.getJobAssignment()
						.getDesignation().getDesignationName())) {
					dashboardVO = voMap.get(payroll.getJobAssignment()
							.getDesignation().getDesignationName());
					dashboardVO.setAmount(payroll.getNetAmount()
							+ dashboardVO.getAmount());
					voMap.put(payroll.getJobAssignment().getDesignation()
							.getDesignationName(), dashboardVO);
				} else {
					dashboardVO = new DashboardVO();
					dashboardVO.setAmount(payroll.getNetAmount());
					dashboardVO.setName(payroll.getJobAssignment()
							.getDesignation().getDesignationName());
					voMap.put(payroll.getJobAssignment().getDesignation()
							.getDesignationName(), dashboardVO);
				}

			}
			dashboardVOs = new ArrayList<DashboardVO>(voMap.values());
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<AttendanceTO> getAbsenteesmByPeriod(Date fromDate, Date toDate) {
		List<AttendanceTO> attendanceVOs = null;
		try {
			List<Attendance> attendances = hrEnterpriseService
					.getAttendanceService().getAbsenteesmByPeriod(fromDate,
							toDate, getImplementation());
			attendanceVOs = new ArrayList<AttendanceTO>();
			AttendanceTO attendanceVO = null;
			for (Attendance attendance : attendances) {
				attendanceVO = new AttendanceTO();
				attendanceVO.setAttendanceId(attendance.getAttendanceId());
				attendanceVO.setPersonName(attendance.getJobAssignment()
						.getPerson().getFirstName()
						+ " "
						+ attendance.getJobAssignment().getPerson()
								.getLastName());
				attendanceVO.setDesignationName(attendance.getJobAssignment()
						.getDesignation().getDesignationName());
				attendanceVO.setAttedanceDate(DateFormat
						.convertDateToString(attendance.getAttendanceDate()
								+ ""));
				attendanceVOs.add(attendanceVO);
			}
			return attendanceVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<DashboardVO> wagesByDepartment() {
		List<DashboardVO> dashboardVOs = null;
		try {
			List<Payroll> payrolls = hrEnterpriseService.getPayrollService()
					.getAllCommittedPayrolls(getImplementation());
			Map<String, DashboardVO> voMap = new HashMap<String, DashboardVO>();
			DashboardVO dashboardVO = null;
			for (Payroll payroll : payrolls) {
				if (voMap.containsKey(payroll.getJobAssignment()
						.getCmpDeptLocation().getDepartment()
						.getDepartmentName())) {
					dashboardVO = voMap.get(payroll.getJobAssignment()
							.getCmpDeptLocation().getDepartment()
							.getDepartmentName());
					dashboardVO.setAmount(payroll.getNetAmount()
							+ dashboardVO.getAmount());
					voMap.put(payroll.getJobAssignment().getCmpDeptLocation()
							.getDepartment().getDepartmentName(), dashboardVO);
				} else {
					dashboardVO = new DashboardVO();
					dashboardVO.setAmount(payroll.getNetAmount());
					dashboardVO.setName(payroll.getJobAssignment()
							.getCmpDeptLocation().getDepartment()
							.getDepartmentName());
					voMap.put(payroll.getJobAssignment().getCmpDeptLocation()
							.getDepartment().getDepartmentName(), dashboardVO);
				}

			}
			dashboardVOs = new ArrayList<DashboardVO>(voMap.values());
			return dashboardVOs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public AccountsEnterpriseService getAccountsEnterpriseService() {
		return accountsEnterpriseService;
	}

	public void setAccountsEnterpriseService(
			AccountsEnterpriseService accountsEnterpriseService) {
		this.accountsEnterpriseService = accountsEnterpriseService;
	}

	public AccountsBL getAccountsBL() {
		return accountsBL;
	}

	public void setAccountsBL(AccountsBL accountsBL) {
		this.accountsBL = accountsBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public HrEnterpriseService getHrEnterpriseService() {
		return hrEnterpriseService;
	}

	public void setHrEnterpriseService(HrEnterpriseService hrEnterpriseService) {
		this.hrEnterpriseService = hrEnterpriseService;
	}
}
