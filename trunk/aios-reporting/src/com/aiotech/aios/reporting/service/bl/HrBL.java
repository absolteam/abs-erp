package com.aiotech.aios.reporting.service.bl;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.service.HrEnterpriseService;
import com.aiotech.aios.hr.service.bl.AttendanceBL;
import com.aiotech.aios.hr.service.bl.IdentityAvailabilityBL;
import com.aiotech.aios.hr.service.bl.JobAssignmentBL;
import com.aiotech.aios.hr.service.bl.JobBL;
import com.aiotech.aios.hr.service.bl.LeaveBL;
import com.aiotech.aios.hr.service.bl.LeaveProcessBL;
import com.aiotech.aios.hr.service.bl.PayrollBL;
import com.aiotech.aios.hr.service.bl.PromotionDemotionBL;
import com.aiotech.aios.hr.service.bl.ResignationTerminationBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;

public class HrBL {
	private HrEnterpriseService hrEnterpriseService;
	private ImageBL imageBL;
	private JobBL jobBL;
	private LeaveProcessBL leaveProcessBL;
	private PayrollBL payrollBL;
	private AttendanceBL attendanceBL;
	private IdentityAvailabilityBL identityAvailabilityBL;
	private JobAssignmentBL jobAssignmentBL;
	private PromotionDemotionBL promotionDemotionBL;
	private ResignationTerminationBL resignationTerminationBL;
	private AlertBL alertBL;
	private LeaveBL leaveBL;
	private Implementation implementation;
	public HrEnterpriseService getHrEnterpriseService() {
		return hrEnterpriseService;
	}

	public void setHrEnterpriseService(HrEnterpriseService hrEnterpriseService) {
		this.hrEnterpriseService = hrEnterpriseService;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public LeaveProcessBL getLeaveProcessBL() {
		return leaveProcessBL;
	}

	public void setLeaveProcessBL(LeaveProcessBL leaveProcessBL) {
		this.leaveProcessBL = leaveProcessBL;
	}

	public JobBL getJobBL() {
		return jobBL;
	}

	public void setJobBL(JobBL jobBL) {
		this.jobBL = jobBL;
	}

	public PayrollBL getPayrollBL() {
		return payrollBL;
	}

	public void setPayrollBL(PayrollBL payrollBL) {
		this.payrollBL = payrollBL;
	}

	public AttendanceBL getAttendanceBL() {
		return attendanceBL;
	}

	public void setAttendanceBL(AttendanceBL attendanceBL) {
		this.attendanceBL = attendanceBL;
	}

	public IdentityAvailabilityBL getIdentityAvailabilityBL() {
		return identityAvailabilityBL;
	}

	public void setIdentityAvailabilityBL(
			IdentityAvailabilityBL identityAvailabilityBL) {
		this.identityAvailabilityBL = identityAvailabilityBL;
	}

	public JobAssignmentBL getJobAssignmentBL() {
		return jobAssignmentBL;
	}

	public void setJobAssignmentBL(JobAssignmentBL jobAssignmentBL) {
		this.jobAssignmentBL = jobAssignmentBL;
	}

	public PromotionDemotionBL getPromotionDemotionBL() {
		return promotionDemotionBL;
	}

	public void setPromotionDemotionBL(PromotionDemotionBL promotionDemotionBL) {
		this.promotionDemotionBL = promotionDemotionBL;
	}

	public ResignationTerminationBL getResignationTerminationBL() {
		return resignationTerminationBL;
	}

	public void setResignationTerminationBL(
			ResignationTerminationBL resignationTerminationBL) {
		this.resignationTerminationBL = resignationTerminationBL;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public LeaveBL getLeaveBL() {
		return leaveBL;
	}

	public void setLeaveBL(LeaveBL leaveBL) {
		this.leaveBL = leaveBL;
	}
	
	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}
}
