package com.aiotech.aios.reporting.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.TransactionTempDetail;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.LedgerVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PurchaseVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionVO;
import com.aiotech.aios.accounts.service.AccountsEnterpriseService;
import com.aiotech.aios.accounts.service.bl.AccountGroupBL;
import com.aiotech.aios.accounts.service.bl.BankBL;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.service.bl.CreditBL;
import com.aiotech.aios.accounts.service.bl.CustomerBL;
import com.aiotech.aios.accounts.service.bl.InvoiceBL;
import com.aiotech.aios.accounts.service.bl.IssueRequistionBL;
import com.aiotech.aios.accounts.service.bl.PettyCashBL;
import com.aiotech.aios.accounts.service.bl.PointOfSaleBL;
import com.aiotech.aios.accounts.service.bl.PurchaseBL;
import com.aiotech.aios.accounts.service.bl.RequisitionBL;
import com.aiotech.aios.accounts.service.bl.SalesInvoiceBL;
import com.aiotech.aios.accounts.service.bl.StockBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.service.bl.CompanyBL;
import com.aiotech.aios.hr.service.bl.DepartmentBL;
import com.aiotech.aios.hr.service.bl.LocationBL;
import com.aiotech.aios.hr.service.bl.PersonBL;
import com.aiotech.aios.hr.service.bl.SupplierBL;
import com.aiotech.aios.project.service.bl.ProjectBL;
import com.aiotech.aios.project.service.bl.ProjectMileStoneBL;
import com.aiotech.aios.reporting.to.AccountsTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.bl.LookupMasterBL;

public class AccountsBL {
	private AccountsEnterpriseService accountsEnterpriseService;
	private CombinationBL combinationBL;
	private TransactionBL transactionBL;
	private LookupMasterBL lookupMasterBL;
	private BankBL bankBL;
	private CompanyBL companyBL;
	private PersonBL personBL;
	private StockBL stockBL;
	private ProjectMileStoneBL projectMileStoneBL;
	private CustomerBL customerBL;
	private InvoiceBL invoiceBL;
	private SalesInvoiceBL salesInvoiceBL;
	private SupplierBL supplierBL;
	private CreditBL creditBL;
	private ProjectBL projectBL;
	private PointOfSaleBL pointOfSaleBL;
	private AccountsJobBL accountsJobBL;
	private AccountGroupBL accountGroupBL;
	private LocationBL locationBL;
	private RequisitionBL requisitionBL;
	private PettyCashBL pettyCashBL;
	private IssueRequistionBL issueRequistionBL;
	private DepartmentBL departmentBL;
	private PurchaseBL purchaseBL;

	public List<Ledger> getTrailBalanceCombination(Implementation implementation)
			throws Exception {
		return accountsEnterpriseService.getCombinationService().getLedgerList(
				implementation);
	}

	public List<Ledger> getIncomenStatementCombination(
			Implementation implementation) throws Exception {
		return accountsEnterpriseService.getCombinationService()
				.getIncomeRevenueLedgerList(implementation);
	}

	// Income statement fetch from Ledger Fiscal by implementation
	public List<LedgerFiscal> getIncomenStatementLedgerFiscal(
			Implementation implementation) throws Exception {
		return accountsEnterpriseService.getCombinationService()
				.getIncomeRevenueLedgerFiscals(implementation);
	}

	// Income statement fetch from Ledger Fiscal by period Id
	public List<LedgerFiscal> getIncomenStatementLedgerFiscal(long periodId)
			throws Exception {
		return accountsEnterpriseService.getCombinationService()
				.getIncomeRevenueLedgerFiscals(periodId);
	}

	// Get all Ledger Fiscal
	public List<LedgerFiscal> getAllLedgerFiscal(Implementation implementation)
			throws Exception {
		return accountsEnterpriseService.getCombinationService()
				.getLedgerFiscalList(implementation);
	}

	public List<Ledger> getBalanceSheetCombination(Implementation implementation)
			throws Exception {
		return accountsEnterpriseService.getCombinationService()
				.getBalanceSheetLedgerList(implementation);
	}

	public List<AccountsTO> processGroupDetail(List<AccountsTO> accountsTO,
			Map<String, Map<Long, Combination>> accountGroupMap,
			Integer accountType, boolean isTrialBalance) throws Exception {
		Map<String, List<AccountsTO>> groupDetailMap = null;
		Map<String, List<AccountsTO>> mainGroupMap = new HashMap<String, List<AccountsTO>>();
		Map<String, List<AccountsTO>> accountDetailMap = null;
		AccountsTO tempAccountTO = null;
		List<AccountsTO> tempList = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		Map<Long, Combination> detailMap = null;
		Set<Long> allCombination = new HashSet<Long>();
		Map<String, AccountsTO> excludedMap = null;
		Map<Long, Map<String, AccountsTO>> excludedMapDetail = new HashMap<Long, Map<String, AccountsTO>>();
		for (AccountsTO list : accountsTO) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entryMap : list
						.getAccountsMap().entrySet()) {
					groupDetailMap = new HashMap<String, List<AccountsTO>>();
					for (AccountsTO inList : entryMap.getValue()) {
						for (Entry<String, Map<Long, Combination>> entryGroup : accountGroupMap
								.entrySet()) {
							detailMap = new HashMap<Long, Combination>(
									entryGroup.getValue());
							if (detailMap
									.containsKey(inList.getCombinationId())) {
								allCombination.add(inList.getCombinationId());
								if (null != groupDetailMap
										&& groupDetailMap.size() > 0
										&& groupDetailMap
												.containsKey(entryGroup
														.getKey())) {
									tempList = new ArrayList<AccountsTO>(
											groupDetailMap.get(entryGroup
													.getKey()));
								} else
									tempList = new ArrayList<AccountsTO>();
								tempList.add(inList);
								groupDetailMap.put(entryGroup.getKey(),
										tempList);
							} else {
								excludedMap = new HashMap<String, AccountsTO>();
								excludedMap.put(entryMap.getKey(), inList);
								excludedMapDetail.put(
										inList.getCombinationId(), excludedMap);
							}
						}
					}
					if (null != groupDetailMap && groupDetailMap.size() > 0) {
						tempList = new ArrayList<AccountsTO>();
						for (Entry<String, List<AccountsTO>> entry : groupDetailMap
								.entrySet()) {
							tempAccountTO = getAccountGroupValue(accountType,
									entry.getValue(), isTrialBalance);
							tempAccountTO.setAccountCode(entry.getKey());
							tempList.add(tempAccountTO);
						}
						accountDetailMap = new HashMap<String, List<AccountsTO>>();
						accountDetailMap.put(entryMap.getKey(), tempList);
						tempAccountTO = new AccountsTO();
						tempAccountTO.setAccountsMap(accountDetailMap);
						finalViewList.add(tempAccountTO);
					} else {
						tempList = new ArrayList<AccountsTO>(
								entryMap.getValue());
						boolean add = true;
						for (AccountsTO account : tempList) {
							if (null != list.getAccountsMap()
									&& list.getAccountsMap().size() > 0) {
								for (Entry<String, List<AccountsTO>> entry : list
										.getAccountsMap().entrySet()) {
									for (AccountsTO entryval : entry.getValue()) {
										if ((long) entryval.getCombinationId() == (long) account
												.getCombinationId()) {
											add = false;
											break;
										}
									}
								}
							} else {
								if ((long) list.getCombinationId() == (long) account
										.getCombinationId()) {
									add = false;
									break;
								}
							}
						}
						if (add)
							tempList.add(list);
						accountDetailMap = new HashMap<String, List<AccountsTO>>();
						accountDetailMap.put(entryMap.getKey(), tempList);
						tempAccountTO = new AccountsTO();
						tempAccountTO.setAccountsMap(accountDetailMap);
						finalViewList.add(tempAccountTO);
					}
				}
			} else {
				groupDetailMap = new HashMap<String, List<AccountsTO>>();
				for (Entry<String, Map<Long, Combination>> entryGroup : accountGroupMap
						.entrySet()) {
					detailMap = new HashMap<Long, Combination>(
							entryGroup.getValue());
					if (detailMap.containsKey(list.getCombinationId())) {
						if (null != groupDetailMap && groupDetailMap.size() > 0) {
							try {
								tempList = new ArrayList<AccountsTO>(
										groupDetailMap.get(entryGroup.getKey()));
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else
							tempList = new ArrayList<AccountsTO>();
						tempList.add(list);
						groupDetailMap.put(entryGroup.getKey(), tempList);
					}
				}
				if (null != groupDetailMap && groupDetailMap.size() > 0) {
					if (null != mainGroupMap && mainGroupMap.size() > 0) {
						for (Entry<String, List<AccountsTO>> entry : groupDetailMap
								.entrySet()) {
							if (mainGroupMap.containsKey(entry.getKey())) {
								tempList = new ArrayList<AccountsTO>();
								tempList.addAll(mainGroupMap.get(entry.getKey()));
								tempList.addAll(entry.getValue());
								mainGroupMap.put(entry.getKey(), tempList);
							} else
								mainGroupMap.put(entry.getKey(),
										entry.getValue());
						}
					} else {
						mainGroupMap = new HashMap<String, List<AccountsTO>>();
						for (Entry<String, List<AccountsTO>> entry : groupDetailMap
								.entrySet()) {
							mainGroupMap.put(entry.getKey(), entry.getValue());
						}
					}
				} else
					finalViewList.add(list);
			}
		}

		if (null != mainGroupMap && mainGroupMap.size() > 0) {
			tempList = new ArrayList<AccountsTO>();
			for (Entry<String, List<AccountsTO>> entry : mainGroupMap
					.entrySet()) {
				tempAccountTO = getAccountGroupValue(accountType,
						entry.getValue(), isTrialBalance);
				tempAccountTO.setAccountCode(entry.getKey());
				tempList.add(tempAccountTO);
			}
			finalViewList.addAll(tempList);
		}

		if (null != excludedMapDetail && excludedMapDetail.size() > 0) {
			Collection<Long> similar = new HashSet<Long>(
					excludedMapDetail.keySet());
			Collection<Long> different = new HashSet<Long>();
			different.addAll(excludedMapDetail.keySet());
			different.addAll(allCombination);
			similar.retainAll(allCombination);
			different.removeAll(similar);

			if (null != different && different.size() > 0) {
				for (Long exclude : different) {
					if (excludedMapDetail.containsKey(exclude)) {
						excludedMap = new HashMap<String, AccountsTO>(
								excludedMapDetail.get(exclude));
						for (AccountsTO finalview : finalViewList) {
							if (null != finalview.getAccountsMap()
									&& finalview.getAccountsMap().size() > 0) {
								for (Entry<String, List<AccountsTO>> entrytmp : finalview
										.getAccountsMap().entrySet()) {
									if (excludedMap.containsKey(entrytmp
											.getKey())) {
										Map<String, List<AccountsTO>> tempMap = new HashMap<String, List<AccountsTO>>();
										tempList = new ArrayList<AccountsTO>(
												entrytmp.getValue());
										boolean add = true;
										for (AccountsTO account : tempList) {
											if (null != account
													&& null != account
															.getCombinationId()) {
												if ((long) exclude == (long) account
														.getCombinationId()) {
													add = false;
													break;
												}
											}
										}
										if (add) {
											tempList.add(excludedMap
													.get(entrytmp.getKey()));
											tempMap.put(entrytmp.getKey(),
													tempList);
											finalview.setAccountsMap(tempMap);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return finalViewList;
	}

	public List<TransactionDetailVO> processGroupDetailNew(
			List<TransactionDetailVO> transactionDetailVOs,
			Map<String, Map<Long, Combination>> accountGroupMap,
			Integer accountType) throws Exception {
		Map<String, List<TransactionDetailVO>> groupDetailMap = null;
		Map<String, List<TransactionDetailVO>> mainGroupMap = new HashMap<String, List<TransactionDetailVO>>();
		Map<String, List<TransactionDetailVO>> accountDetailMap = null;
		TransactionDetailVO tempTransactionDetailVO = null;
		List<TransactionDetailVO> tempList = null;
		List<TransactionDetailVO> finalViewList = new ArrayList<TransactionDetailVO>();
		Map<Long, Combination> detailMap = null;
		Set<Long> allCombination = new HashSet<Long>();
		Map<String, TransactionDetailVO> excludedMap = null;
		Map<Long, Map<String, TransactionDetailVO>> excludedMapDetail = new HashMap<Long, Map<String, TransactionDetailVO>>();

		for (TransactionDetailVO list : transactionDetailVOs) {
			if (null != list.getTransactionDetailVOGroup()
					&& list.getTransactionDetailVOGroup().size() > 0) {
				for (Entry<String, List<TransactionDetailVO>> entryMap : list
						.getTransactionDetailVOGroup().entrySet()) {
					groupDetailMap = new HashMap<String, List<TransactionDetailVO>>();
					for (TransactionDetailVO inList : entryMap.getValue()) {
						for (Entry<String, Map<Long, Combination>> entryGroup : accountGroupMap
								.entrySet()) {
							detailMap = new HashMap<Long, Combination>(
									entryGroup.getValue());
							if (detailMap.containsKey(inList.getCombination()
									.getCombinationId())) {
								allCombination.add(inList.getCombination()
										.getCombinationId());
								tempList = new ArrayList<TransactionDetailVO>();
								if (null != groupDetailMap
										&& groupDetailMap.size() > 0
										&& groupDetailMap
												.containsKey(entryGroup
														.getKey()))
									tempList.addAll(groupDetailMap
											.get(entryGroup.getKey()));
								tempList.add(inList);
								groupDetailMap.put(entryGroup.getKey(),
										tempList);
							} else {
								excludedMap = new HashMap<String, TransactionDetailVO>();
								excludedMap.put(entryMap.getKey(), inList);
								excludedMapDetail.put(inList.getCombination()
										.getCombinationId(), excludedMap);
							}
						}
					}
					if (null != groupDetailMap && groupDetailMap.size() > 0) {
						tempList = new ArrayList<TransactionDetailVO>();
						for (Entry<String, List<TransactionDetailVO>> entry : groupDetailMap
								.entrySet()) {
							tempTransactionDetailVO = getAccountGroupValueNew(
									accountType, entry.getValue());
							tempTransactionDetailVO.setAccountCode(entry
									.getKey());
							tempTransactionDetailVO.setAccountDescription(entry
									.getKey());
							tempList.add(tempTransactionDetailVO);
						}
						accountDetailMap = new HashMap<String, List<TransactionDetailVO>>();
						accountDetailMap.put(entryMap.getKey(), tempList);
						tempTransactionDetailVO = new TransactionDetailVO();
						tempTransactionDetailVO
								.setTransactionDetailVOGroup(accountDetailMap);
						finalViewList.add(tempTransactionDetailVO);
					} else {
						tempList = new ArrayList<TransactionDetailVO>(
								entryMap.getValue());
						boolean add = true;
						for (TransactionDetailVO account : tempList) {
							if (null != list.getTransactionDetailVOGroup()
									&& list.getTransactionDetailVOGroup()
											.size() > 0) {
								for (Entry<String, List<TransactionDetailVO>> entry : list
										.getTransactionDetailVOGroup()
										.entrySet()) {
									for (TransactionDetailVO entryval : entry
											.getValue()) {
										if ((long) entryval.getCombination()
												.getCombinationId() == (long) account
												.getCombination()
												.getCombinationId()) {
											add = false;
											break;
										}
									}
								}
							} else {
								if ((long) list.getCombination()
										.getCombinationId() == (long) account
										.getCombination().getCombinationId()) {
									add = false;
									break;
								}
							}
						}
						if (add)
							tempList.add(list);
						accountDetailMap = new HashMap<String, List<TransactionDetailVO>>();
						accountDetailMap.put(entryMap.getKey(), tempList);
						tempTransactionDetailVO = new TransactionDetailVO();
						tempTransactionDetailVO
								.setTransactionDetailVOGroup(accountDetailMap);
						finalViewList.add(tempTransactionDetailVO);
					}
				}
			} else {
				groupDetailMap = new HashMap<String, List<TransactionDetailVO>>();
				for (Entry<String, Map<Long, Combination>> entryGroup : accountGroupMap
						.entrySet()) {
					detailMap = new HashMap<Long, Combination>(
							entryGroup.getValue());
					if (detailMap.containsKey(list.getCombination()
							.getCombinationId())) {
						tempList = new ArrayList<TransactionDetailVO>();
						if (null != groupDetailMap && groupDetailMap.size() > 0)
							tempList.addAll(groupDetailMap.get(entryGroup
									.getKey()));
						tempList.add(list);
						groupDetailMap.put(entryGroup.getKey(), tempList);
					}
				}
				if (null != groupDetailMap && groupDetailMap.size() > 0) {
					if (null != mainGroupMap && mainGroupMap.size() > 0) {
						for (Entry<String, List<TransactionDetailVO>> entry : groupDetailMap
								.entrySet()) {
							if (mainGroupMap.containsKey(entry.getKey())) {
								tempList = new ArrayList<TransactionDetailVO>();
								tempList.addAll(mainGroupMap.get(entry.getKey()));
								tempList.addAll(entry.getValue());
								mainGroupMap.put(entry.getKey(), tempList);
							} else
								mainGroupMap.put(entry.getKey(),
										entry.getValue());
						}
					} else {
						mainGroupMap = new HashMap<String, List<TransactionDetailVO>>();
						for (Entry<String, List<TransactionDetailVO>> entry : groupDetailMap
								.entrySet()) {
							mainGroupMap.put(entry.getKey(), entry.getValue());
						}
					}
				} else
					finalViewList.add(list);
			}
		}

		if (null != mainGroupMap && mainGroupMap.size() > 0) {
			tempList = new ArrayList<TransactionDetailVO>();
			for (Entry<String, List<TransactionDetailVO>> entry : mainGroupMap
					.entrySet()) {
				tempTransactionDetailVO = getAccountGroupValueNew(accountType,
						entry.getValue());
				tempTransactionDetailVO.setAccountCode(entry.getKey());
				tempTransactionDetailVO.setAccountDescription(entry.getKey());
				tempList.add(tempTransactionDetailVO);
			}
			finalViewList.addAll(tempList);
		}
		if (null != excludedMapDetail && excludedMapDetail.size() > 0) {
			Collection<Long> similar = new HashSet<Long>(
					excludedMapDetail.keySet());
			Collection<Long> different = new HashSet<Long>();
			different.addAll(excludedMapDetail.keySet());
			different.addAll(allCombination);
			similar.retainAll(allCombination);
			different.removeAll(similar);

			if (null != different && different.size() > 0) {
				for (Long exclude : different) {
					if (excludedMapDetail.containsKey(exclude)) {
						excludedMap = new HashMap<String, TransactionDetailVO>(
								excludedMapDetail.get(exclude));
						for (TransactionDetailVO finalview : finalViewList) {
							if (null != finalview.getTransactionDetailVOGroup()
									&& finalview.getTransactionDetailVOGroup()
											.size() > 0) {
								for (Entry<String, List<TransactionDetailVO>> entrytmp : finalview
										.getTransactionDetailVOGroup()
										.entrySet()) {
									if (excludedMap.containsKey(entrytmp
											.getKey())) {
										Map<String, List<TransactionDetailVO>> tempMap = new HashMap<String, List<TransactionDetailVO>>();
										tempList = new ArrayList<TransactionDetailVO>(
												entrytmp.getValue());
										boolean add = true;
										for (TransactionDetailVO account : tempList) {
											if (null != account
													&& null != account
															.getCombination()
													&& null != account
															.getCombination()
															.getCombinationId()) {
												if ((long) exclude == (long) account
														.getCombination()
														.getCombinationId()) {
													add = false;
													break;
												}
											}
										}
										if (add) {
											tempList.add(excludedMap
													.get(entrytmp.getKey()));
											tempMap.put(entrytmp.getKey(),
													tempList);
											finalview
													.setTransactionDetailVOGroup(tempMap);
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return finalViewList;
	}

	private AccountsTO getAccountGroupValue(Integer accountType,
			List<AccountsTO> accountTOs, boolean isTrialBalance)
			throws Exception {
		AccountsTO tempAccountTO = new AccountsTO();
		if ((int) accountType == (int) AccountType.Assets.getCode()) {
			tempAccountTO = this.getAssetGroupValue(accountTOs, isTrialBalance);
		} else if ((int) accountType == (int) AccountType.Liabilites.getCode()) {
			tempAccountTO = this.getLiabiltyGroupValue(accountTOs,
					isTrialBalance);
		} else if ((int) accountType == (int) AccountType.Revenue.getCode()) {
			tempAccountTO = this.getRevenueGroupValue(accountTOs,
					isTrialBalance);
		} else if ((int) accountType == (int) AccountType.Expenses.getCode()) {
			tempAccountTO = this.getExpenseGroupValue(accountTOs,
					isTrialBalance);
		} else if ((int) accountType == (int) AccountType.OwnersEquity
				.getCode()) {
			tempAccountTO = this.getLiabiltyGroupValue(accountTOs,
					isTrialBalance);
		}
		return tempAccountTO;
	}

	private TransactionDetailVO getAccountGroupValueNew(Integer accountType,
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		TransactionDetailVO tempAccountTO = new TransactionDetailVO();
		if ((int) accountType == (int) AccountType.Assets.getCode()) {
			tempAccountTO = this.getAssetGroupValueNew(transactionDetailVOs);
		} else if ((int) accountType == (int) AccountType.Liabilites.getCode()
				|| (int) accountType == (int) AccountType.OwnersEquity
						.getCode()) {
			tempAccountTO = this
					.getLiabilityGroupValueNew(transactionDetailVOs);
		} else if ((int) accountType == (int) AccountType.Revenue.getCode()) {
			tempAccountTO = this.getRevenueGroupValueNew(transactionDetailVOs);
		} else if ((int) accountType == (int) AccountType.Expenses.getCode()) {
			tempAccountTO = this.getExpenseGroupValueNew(transactionDetailVOs);
		}
		return tempAccountTO;
	}

	public List<AccountsTO> getConsolidatedAssetDetails(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, List<AccountsTO>> accountMap = new HashMap<Long, List<AccountsTO>>();
		List<AccountsTO> accountMapList = null;
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		AccountsTO accountTO = null;
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
							.getCode()) {
				accountTO = getAssetType(list, accountList, hashAccount);
				if (null != accountTO) {
					accountMapList = new ArrayList<AccountsTO>();
					if (accountTO.getAccountSubType() > 0) {
						if (accountMap.containsKey(accountTO
								.getAccountSubType())) {
							accountMapList.addAll(accountMap.get(accountTO
									.getAccountSubType()));
						}
						accountMapList.add(accountTO);
						accountMap.put(accountTO.getAccountSubType(),
								accountMapList);
					} else {
						accountList.add(accountTO);
					}
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		LookupDetail lookupDetail = null;
		AccountsTO accountsTO = null;
		Map<String, List<AccountsTO>> viewSubTypeMap = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		if (null != accountMap && accountMap.size() > 0) {
			for (Entry<Long, List<AccountsTO>> entry : accountMap.entrySet()) {
				viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
				lookupDetail = lookupMasterBL.getLookupMasterService()
						.getLookupDetailById(entry.getKey());
				accountsTO = new AccountsTO();
				viewSubTypeMap.put(lookupDetail.getDisplayName(),
						entry.getValue());
				accountsTO.setAccountsMap(viewSubTypeMap);
				finalViewList.add(accountsTO);
			}
			if (null != accountList && accountList.size() > 0)
				finalViewList.addAll(accountList);
		} else
			finalViewList.addAll(accountList);

		Collections.sort(finalViewList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		return finalViewList;
	}

	public List<AccountsTO> getConsolidatedLiabilityDetails(
			List<Ledger> ledgerList) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, List<AccountsTO>> accountMap = new HashMap<Long, List<AccountsTO>>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		AccountsTO accountTO = null;
		List<AccountsTO> accountMapList = null;
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Liabilites
							.getCode())) {
				accountTO = getLiabilityType(list, accountList, hashAccount);
				if (null != accountTO) {
					accountMapList = new ArrayList<AccountsTO>();
					if (accountTO.getAccountSubType() > 0) {
						if (accountMap.containsKey(accountTO
								.getAccountSubType())) {
							accountMapList.addAll(accountMap.get(accountTO
									.getAccountSubType()));
						}
						accountMapList.add(accountTO);
						accountMap.put(accountTO.getAccountSubType(),
								accountMapList);
					} else {
						accountList.add(accountTO);
					}
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		LookupDetail lookupDetail = null;
		AccountsTO accountsTO = null;
		Map<String, List<AccountsTO>> viewSubTypeMap = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		if (null != accountMap && accountMap.size() > 0) {
			for (Entry<Long, List<AccountsTO>> entry : accountMap.entrySet()) {
				viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
				lookupDetail = lookupMasterBL.getLookupMasterService()
						.getLookupDetailById(entry.getKey());
				accountsTO = new AccountsTO();
				viewSubTypeMap.put(lookupDetail.getDisplayName(),
						entry.getValue());
				accountsTO.setAccountsMap(viewSubTypeMap);
				finalViewList.add(accountsTO);
			}
			if (null != accountList && accountList.size() > 0)
				finalViewList.addAll(accountList);
		} else
			finalViewList.addAll(accountList);

		Collections.sort(finalViewList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		return finalViewList;
	}

	public List<AccountsTO> getConsolidatedOwnersEquityDetails(
			List<Ledger> ledgerList) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, List<AccountsTO>> accountMap = new HashMap<Long, List<AccountsTO>>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		AccountsTO accountTO = null;
		List<AccountsTO> accountMapList = null;
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.OwnersEquity
							.getCode())) {
				accountTO = getLiabilityType(list, accountList, hashAccount);
				if (null != accountTO) {
					accountMapList = new ArrayList<AccountsTO>();
					if (accountTO.getAccountSubType() > 0) {
						if (accountMap.containsKey(accountTO
								.getAccountSubType())) {
							accountMapList.addAll(accountMap.get(accountTO
									.getAccountSubType()));
						}
						accountMapList.add(accountTO);
						accountMap.put(accountTO.getAccountSubType(),
								accountMapList);
					} else {
						accountList.add(accountTO);
					}
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		LookupDetail lookupDetail = null;
		AccountsTO accountsTO = null;
		Map<String, List<AccountsTO>> viewSubTypeMap = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		if (null != accountMap && accountMap.size() > 0) {
			for (Entry<Long, List<AccountsTO>> entry : accountMap.entrySet()) {
				viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
				lookupDetail = lookupMasterBL.getLookupMasterService()
						.getLookupDetailById(entry.getKey());
				accountsTO = new AccountsTO();
				viewSubTypeMap.put(lookupDetail.getDisplayName(),
						entry.getValue());
				accountsTO.setAccountsMap(viewSubTypeMap);
				finalViewList.add(accountsTO);
			}
			if (null != accountList && accountList.size() > 0)
				finalViewList.addAll(accountList);
		} else
			finalViewList.addAll(accountList);

		Collections.sort(finalViewList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		return finalViewList;
	}

	public List<AccountsTO> getAssetDetails(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
							.getCode()) {
				AccountsTO accountTO = getAssetType(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getLiabilityDetails(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Liabilites
							.getCode() || (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.OwnersEquity
							.getCode())) {
				AccountsTO accountTO = getLiabilityType(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> listAllDebitCredit(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> debitCreditList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger ledger : ledgerList) {
			if (ledger.getSide()) {
				AccountsTO accountTO = getDebitList(ledger, debitCreditList,
						hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			} else {
				AccountsTO accountTO = getCreditList(ledger, debitCreditList,
						hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(debitCreditList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		Collections.sort(debitCreditList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getAccountTypeId().compareTo(o2.getAccountTypeId());
			}
		});
		return debitCreditList;
	}

	public List<AccountsTO> consolidatedDebitsAndCredits(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> debitCreditList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger ledger : ledgerList) {
			if (ledger.getSide()) {
				AccountsTO accountTO = getDebitList(ledger, debitCreditList,
						hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			} else {
				AccountsTO accountTO = getCreditList(ledger, debitCreditList,
						hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}

		List<AccountsTO> finalViewList = null;
		if (null != debitCreditList && debitCreditList.size() > 0) {
			finalViewList = new ArrayList<AccountsTO>();
			Map<Long, List<AccountsTO>> accountsMap = new HashMap<Long, List<AccountsTO>>();
			List<AccountsTO> tempAccountList = null;
			AccountsTO accountTO = null;
			Map<String, List<AccountsTO>> viewSubTypeMap = null;
			List<AccountsTO> accountList = new ArrayList<AccountsTO>();
			for (AccountsTO accountsTO : debitCreditList) {
				tempAccountList = new ArrayList<AccountsTO>();
				if (accountsTO.getAccountSubType() > 0) {
					if (null != accountsMap
							&& accountsMap.size() > 0
							&& accountsMap.containsKey(accountsTO
									.getAccountSubType()))
						tempAccountList.addAll(accountsMap.get(accountsTO
								.getAccountSubType()));
					tempAccountList.add(accountsTO);
					accountsMap.put(accountsTO.getAccountSubType(),
							tempAccountList);
				} else {
					accountList.add(accountsTO);
				}
			}

			LookupDetail lookupDetail = null;
			if (null != accountsMap && accountsMap.size() > 0) {
				for (Entry<Long, List<AccountsTO>> entry : accountsMap
						.entrySet()) {
					viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
					lookupDetail = lookupMasterBL.getLookupMasterService()
							.getLookupDetailById(entry.getKey());
					accountTO = new AccountsTO();
					viewSubTypeMap.put(lookupDetail.getDisplayName(),
							entry.getValue());
					accountTO.setAccountsMap(viewSubTypeMap);
					finalViewList.add(accountTO);
				}
				if (null != accountList && accountList.size() > 0)
					finalViewList.addAll(accountList);
			} else
				finalViewList.addAll(accountList);

			Collections.sort(finalViewList, new Comparator<AccountsTO>() {
				public int compare(AccountsTO o1, AccountsTO o2) {
					return o1.getCostCenterAccountId().compareTo(
							o2.getCostCenterAccountId());
				}
			});
		}
		return finalViewList;
	}

	private AccountsTO getDebitList(Ledger ledger, List<AccountsTO> debitsList,
			Map<Long, AccountsTO> hashAccount) {
		try {
			boolean flag = true;
			Combination combinationAnalysis = null;
			if (null != ledger.getCombination().getAccountByAnalysisAccountId()) {
				combinationAnalysis = accountsEnterpriseService
						.getCombinationService().getNaturalAccountCombination(
								ledger.getCombination()
										.getAccountByCompanyAccountId()
										.getAccountId(),
								ledger.getCombination()
										.getAccountByCostcenterAccountId()
										.getAccountId(),
								ledger.getCombination()
										.getAccountByNaturalAccountId()
										.getAccountId());
			} else {
				combinationAnalysis = ledger.getCombination();
			}
			AccountsTO accountTO = null;
			if (null != debitsList
					&& debitsList.size() > 0
					&& hashAccount.containsKey(combinationAnalysis
							.getCombinationId())) {
				if (ledger.getBalance() != null && ledger.getBalance() > 0) {
					double tempBalance = 0;
					double oldLedgerBalance = 0;
					accountTO = new AccountsTO();
					accountTO = hashAccount.get(combinationAnalysis
							.getCombinationId());
					if (((int) ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
							.getCode())
							|| (ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Expenses
									.getCode())) {
						if (accountTO.getLedgerSide()) {
							accountTO
									.setDebitBalanceTotal(String.valueOf(ledger
											.getBalance()
											+ Double.parseDouble(accountTO
													.getDebitBalanceTotal()
													.isEmpty() ? "0"
													: accountTO
															.getDebitBalanceTotal())));
							accountTO.setLedgerSide(true);
						} else {
							tempBalance = ledger.getBalance();
							if ((tempBalance - Double.parseDouble(accountTO
									.getCreditBalanceTotal())) >= 0) {
								accountTO
										.setDebitBalanceTotal(String.valueOf(Math.abs(tempBalance
												- Double.parseDouble(accountTO
														.getCreditBalanceTotal()))));
								accountTO.setCreditBalanceTotal(null);
								accountTO.setLedgerSide(true);
							} else {
								accountTO
										.setCreditBalanceTotal(String.valueOf(Math.abs(tempBalance
												- Double.parseDouble(accountTO
														.getCreditBalanceTotal()))));
								accountTO.setDebitBalanceTotal(null);
								accountTO.setLedgerSide(false);
							}
						}
					} else {
						if (accountTO.getLedgerSide()) {
							accountTO.setDebitBalanceTotal(String
									.valueOf(ledger.getBalance()
											+ Double.parseDouble(accountTO
													.getDebitBalanceTotal())));
							accountTO.setCreditBalanceTotal(null);
							accountTO.setLedgerSide(true);
						} else {
							tempBalance = ledger.getBalance();
							oldLedgerBalance = Double.parseDouble(accountTO
									.getCreditBalanceTotal());
							accountTO
									.setCreditBalanceTotal(String.valueOf(Math.abs(tempBalance
											- Double.parseDouble(accountTO
													.getCreditBalanceTotal()))));
							if ((-tempBalance + oldLedgerBalance) > 0) {
								accountTO.setDebitBalanceTotal(null);
								accountTO.setLedgerSide(false);
							} else {
								accountTO.setDebitBalanceTotal(accountTO
										.getCreditBalanceTotal());
								accountTO.setCreditBalanceTotal(null);
								accountTO.setLedgerSide(true);
							}
						}
					}
				}
				flag = false;
			}
			if (flag) {
				accountTO = new AccountsTO();
				accountTO.setCombinationId(combinationAnalysis
						.getCombinationId());
				accountTO.setAccountDescription(ledger.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledger.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledger.getCombination()
								.getAccountByNaturalAccountId().getCode());
				accountTO.setAccountCode(ledger.getCombination()
						.getAccountByNaturalAccountId().getAccount());
				accountTO.setDebitBalanceTotal(ledger.getBalance() == null ? ""
						: ledger.getBalance().toString());
				accountTO.setAccountTypeId(ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				accountTO.setLedgerSide(ledger.getSide());
				accountTO.setNaturalAccountId(ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountId());
				accountTO.setCostCenterAccountId(ledger.getCombination()
						.getAccountByCostcenterAccountId().getAccountId());
				if (null != ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					accountTO.setAccountSubType(ledger.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId());
					accountTO.setAccountSubTypeStr(ledger.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getDisplayName());
				}
				return accountTO;
			} else
				return null;
		} catch (Exception ex) {
			return null;
		}
	}

	private AccountsTO getCreditList(Ledger ledger,
			List<AccountsTO> creditsList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !("").equals(ledger.getCombination()
						.getAccountByAnalysisAccountId())) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		AccountsTO accountTO = null;
		if (null != creditsList
				&& !("").equals(creditsList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledger.getBalance() > 0) {
				double tempBalance = 0;
				double oldLedgerBalance = 0;
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (((int) ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
						.getCode())
						|| ((int) ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.Expenses
								.getCode())) {
					if (accountTO.getLedgerSide()) {
						tempBalance = ledger.getBalance();
						oldLedgerBalance = Double.parseDouble(accountTO
								.getDebitBalanceTotal());
						accountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(ledger.getBalance()
										- Double.parseDouble(accountTO
												.getDebitBalanceTotal()))));
						if ((-tempBalance + oldLedgerBalance) >= 0) {
							accountTO.setCreditBalanceTotal(null);
							accountTO.setLedgerSide(true);
						} else {
							accountTO.setCreditBalanceTotal(accountTO
									.getDebitBalanceTotal());
							accountTO.setDebitBalanceTotal(null);
							accountTO.setLedgerSide(false);
						}
					} else {
						accountTO.setCreditBalanceTotal(String.valueOf(Math
								.abs(ledger.getBalance()
										+ Double.parseDouble(accountTO
												.getCreditBalanceTotal()))));
						accountTO.setDebitBalanceTotal(null);
						accountTO.setLedgerSide(false);
					}
				} else {
					if (accountTO.getLedgerSide()) {
						tempBalance = ledger.getBalance();
						oldLedgerBalance = Double.parseDouble(accountTO
								.getDebitBalanceTotal());
						if (null != accountTO.getDebitBalanceTotal()
								&& Double.parseDouble(accountTO
										.getDebitBalanceTotal()) > 0) {
							accountTO.setDebitBalanceTotal(String.valueOf(Math
									.abs(ledger.getBalance()
											- Double.parseDouble(accountTO
													.getDebitBalanceTotal()))));
							if ((tempBalance - oldLedgerBalance) > 0) {
								accountTO.setCreditBalanceTotal(accountTO
										.getDebitBalanceTotal());
								accountTO.setDebitBalanceTotal(null);
								accountTO.setLedgerSide(false);
							}
						} else {
							accountTO.setCreditBalanceTotal(String.valueOf(Math
									.abs(tempBalance)));
							accountTO.setDebitBalanceTotal(null);
							accountTO.setLedgerSide(false);
						}
					} else {
						tempBalance = ledger.getBalance();
						accountTO.setCreditBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										+ Double.parseDouble(accountTO
												.getCreditBalanceTotal()))));
						accountTO.setDebitBalanceTotal(null);
						accountTO.setLedgerSide(false);
					}
				}
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAccountDescription(ledger.getCombination()
					.getAccountByCompanyAccountId().getCode()
					+ "."
					+ ledger.getCombination().getAccountByCostcenterAccountId()
							.getCode()
					+ "."
					+ ledger.getCombination().getAccountByNaturalAccountId()
							.getCode());
			accountTO.setAccountCode(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccount());
			accountTO.setCreditBalanceTotal(ledger.getBalance().toString());
			accountTO.setAccountTypeId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			accountTO.setLedgerSide(ledger.getSide());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			if (null != ledger.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail()) {
				accountTO.setAccountSubType(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getLookupDetailId());
				accountTO.setAccountSubTypeStr(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getDisplayName());
			}
			return accountTO;
		} else {
			return null;
		}
	}

	// Form Consolidated Fiscal Trial Balance
	public List<AccountsTO> listAllDebitCreditFiscal(
			List<LedgerFiscal> ledgerFiscals) throws Exception {
		List<AccountsTO> debitCreditList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerFiscals) {
			if (list.getSide()) {
				AccountsTO accountTO = getDebitFiscalList(list,
						debitCreditList, hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			} else {
				AccountsTO accountTO = getCreditFiscalList(list,
						debitCreditList, hashAccount);
				if (null != accountTO) {
					debitCreditList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(debitCreditList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return debitCreditList;
	}

	public AccountsTO setClosingBalance(AccountsTO mapView, AccountsTO listView)
			throws Exception {
		double closingBalance = 0;
		if (mapView.getLedgerSide() && listView.getLedgerSide())
			closingBalance = Double.parseDouble(mapView.getDebitBalanceTotal())
					+ Double.parseDouble(listView.getDebitBalanceTotal());
		else if (!mapView.getLedgerSide() && !listView.getLedgerSide())
			closingBalance = Double
					.parseDouble(mapView.getCreditBalanceTotal())
					+ Double.parseDouble(listView.getCreditBalanceTotal());
		else {
			if (mapView.getLedgerSide())
				closingBalance = Math.abs(Double.parseDouble(mapView
						.getDebitBalanceTotal())
						- Double.parseDouble(listView.getCreditBalanceTotal()));
			else
				closingBalance = Math.abs(Double.parseDouble(mapView
						.getCreditBalanceTotal())
						- Double.parseDouble(listView.getDebitBalanceTotal()));
		}
		mapView.setViewClosingBalance(AIOSCommons.formatAmount(closingBalance));
		return mapView;
	}

	private AccountsTO getDebitFiscalList(LedgerFiscal ledgerFiscal,
			List<AccountsTO> debitsList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		AccountsTO accountTO = null;
		Combination combinationAnalysis = null;
		if (null != ledgerFiscal.getCombination()
				.getAccountByAnalysisAccountId()
				&& !("").equals(ledgerFiscal.getCombination()
						.getAccountByAnalysisAccountId())) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledgerFiscal.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else
			combinationAnalysis = ledgerFiscal.getCombination();
		if (null != debitsList
				&& debitsList.size() > 0
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledgerFiscal.getBalance() != null
					&& ledgerFiscal.getBalance() > 0) {
				double tempBalance = 0;
				double oldLedgerBalance = 0;
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (((int) ledgerFiscal.getCombination()
						.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
						.getCode())
						|| ((int) ledgerFiscal.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.Expenses
								.getCode())) {
					if (accountTO.getLedgerSide()) {
						accountTO.setDebitBalanceTotal(String
								.valueOf(ledgerFiscal.getBalance()
										+ Double.parseDouble(accountTO
												.getDebitBalanceTotal()
												.isEmpty() ? "0" : accountTO
												.getDebitBalanceTotal())));
						accountTO.setViewDebitBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getDebitBalanceTotal())));
						accountTO.setLedgerSide(true);
					} else {
						tempBalance = ledgerFiscal.getBalance();
						if ((tempBalance - Double.parseDouble(accountTO
								.getCreditBalanceTotal())) >= 0) {
							accountTO
									.setDebitBalanceTotal(String.valueOf(Math.abs(tempBalance
											- Double.parseDouble(accountTO
													.getCreditBalanceTotal()))));
							accountTO.setViewDebitBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getDebitBalanceTotal())));
							accountTO.setCreditBalanceTotal(null);
							accountTO.setViewCreditBalance(null);
							accountTO.setLedgerSide(true);
						} else {
							accountTO
									.setCreditBalanceTotal(String.valueOf(Math.abs(tempBalance
											- Double.parseDouble(accountTO
													.getCreditBalanceTotal()))));
							accountTO.setViewCreditBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getCreditBalanceTotal())));
							accountTO.setDebitBalanceTotal(null);
							accountTO.setViewDebitBalance(null);
							accountTO.setLedgerSide(false);
						}
					}
				} else {
					if (accountTO.getLedgerSide()) {
						accountTO.setDebitBalanceTotal(String
								.valueOf(ledgerFiscal.getBalance()
										+ Double.parseDouble(accountTO
												.getDebitBalanceTotal())));
						accountTO.setViewDebitBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getDebitBalanceTotal())));
						accountTO.setCreditBalanceTotal(null);
						accountTO.setViewCreditBalance(null);
						accountTO.setLedgerSide(true);
					} else {
						tempBalance = ledgerFiscal.getBalance();
						oldLedgerBalance = Double.parseDouble(accountTO
								.getCreditBalanceTotal());
						accountTO.setCreditBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										- Double.parseDouble(accountTO
												.getCreditBalanceTotal()))));
						accountTO.setViewCreditBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getCreditBalanceTotal())));
						if ((-tempBalance + oldLedgerBalance) > 0) {
							accountTO.setDebitBalanceTotal(null);
							accountTO.setViewDebitBalance(null);
							accountTO.setLedgerSide(false);
						} else {
							accountTO.setDebitBalanceTotal(accountTO
									.getCreditBalanceTotal());
							accountTO.setViewDebitBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getDebitBalanceTotal())));
							accountTO.setCreditBalanceTotal(null);
							accountTO.setViewCreditBalance(null);
							accountTO.setLedgerSide(true);
						}
					}
				}
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAccountCode(ledgerFiscal
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledgerFiscal.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO
					.setDebitBalanceTotal(ledgerFiscal.getBalance() == null ? ""
							: ledgerFiscal.getBalance().toString());
			accountTO.setViewDebitBalance(AIOSCommons.formatAmount(Double
					.parseDouble(accountTO.getDebitBalanceTotal())));
			accountTO.setAccountTypeId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			accountTO.setLedgerSide(ledgerFiscal.getSide());
			accountTO.setNaturalAccountId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledgerFiscal.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else
			return null;
	}

	private AccountsTO getCreditFiscalList(LedgerFiscal ledgerFiscal,
			List<AccountsTO> creditsList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledgerFiscal.getCombination()
				.getAccountByAnalysisAccountId()
				&& !("").equals(ledgerFiscal.getCombination()
						.getAccountByAnalysisAccountId())) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledgerFiscal.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledgerFiscal.getCombination();
		}
		if (null != creditsList
				&& !("").equals(creditsList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledgerFiscal.getBalance() > 0) {
				double tempBalance = 0;
				double oldLedgerBalance = 0;
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (((int) ledgerFiscal.getCombination()
						.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
						.getCode())
						|| (ledgerFiscal.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.Expenses
								.getCode())) {
					if (accountTO.getLedgerSide()) {
						tempBalance = ledgerFiscal.getBalance();
						oldLedgerBalance = Double.parseDouble(accountTO
								.getDebitBalanceTotal());
						accountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(ledgerFiscal.getBalance()
										- Double.parseDouble(accountTO
												.getDebitBalanceTotal()))));
						accountTO.setViewDebitBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getDebitBalanceTotal())));
						if ((-tempBalance + oldLedgerBalance) >= 0) {
							accountTO.setCreditBalanceTotal(null);
							accountTO.setViewCreditBalance(null);
							accountTO.setLedgerSide(true);
						} else {
							accountTO.setCreditBalanceTotal(accountTO
									.getDebitBalanceTotal());
							accountTO.setViewCreditBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getCreditBalanceTotal())));
							accountTO.setDebitBalanceTotal(null);
							accountTO.setViewDebitBalance(null);
							accountTO.setLedgerSide(false);
						}
					} else {
						accountTO.setCreditBalanceTotal(String.valueOf(Math
								.abs(ledgerFiscal.getBalance()
										+ Double.parseDouble(accountTO
												.getCreditBalanceTotal()))));
						accountTO.setViewCreditBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getCreditBalanceTotal())));
						accountTO.setDebitBalanceTotal(null);
						accountTO.setViewDebitBalance(null);
						accountTO.setLedgerSide(false);
					}
				} else {
					if (accountTO.getLedgerSide()) {
						tempBalance = ledgerFiscal.getBalance();
						oldLedgerBalance = Double.parseDouble(accountTO
								.getDebitBalanceTotal());
						if (null != accountTO.getDebitBalanceTotal()
								&& Double.parseDouble(accountTO
										.getDebitBalanceTotal()) > 0) {
							accountTO.setDebitBalanceTotal(String.valueOf(Math
									.abs(ledgerFiscal.getBalance()
											- Double.parseDouble(accountTO
													.getDebitBalanceTotal()))));
							accountTO.setViewDebitBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getDebitBalanceTotal())));
							if ((tempBalance - oldLedgerBalance) > 0) {
								accountTO.setCreditBalanceTotal(accountTO
										.getDebitBalanceTotal());
								accountTO
										.setViewCreditBalance(AIOSCommons.formatAmount(Double.parseDouble(accountTO
												.getCreditBalanceTotal())));
								accountTO.setDebitBalanceTotal(null);
								accountTO.setViewDebitBalance(null);
								accountTO.setLedgerSide(false);
							}
						} else {
							accountTO.setCreditBalanceTotal(String.valueOf(Math
									.abs(tempBalance)));
							accountTO.setViewCreditBalance(AIOSCommons
									.formatAmount(Double.parseDouble(accountTO
											.getCreditBalanceTotal())));
							accountTO.setDebitBalanceTotal(null);
							accountTO.setViewDebitBalance(null);
							accountTO.setLedgerSide(false);
						}
					} else {
						tempBalance = ledgerFiscal.getBalance();
						accountTO.setCreditBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										+ Double.parseDouble(accountTO
												.getCreditBalanceTotal()))));
						accountTO.setViewCreditBalance(AIOSCommons
								.formatAmount(Double.parseDouble(accountTO
										.getCreditBalanceTotal())));
						accountTO.setDebitBalanceTotal(null);
						accountTO.setViewDebitBalance(null);
						accountTO.setLedgerSide(false);
					}
				}
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAccountCode(ledgerFiscal
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledgerFiscal.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setCreditBalanceTotal(ledgerFiscal.getBalance()
					.toString());
			accountTO.setViewCreditBalance(AIOSCommons.formatAmount(Double
					.parseDouble(accountTO.getCreditBalanceTotal())));
			accountTO.setAccountTypeId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			accountTO.setLedgerSide(ledgerFiscal.getSide());
			accountTO.setNaturalAccountId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledgerFiscal.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	public List<AccountsTO> getConsolidatedExpenseStatement(
			List<Ledger> ledgerList) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, List<AccountsTO>> accountMap = new HashMap<Long, List<AccountsTO>>();
		List<AccountsTO> accountMapList = null;
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
							.getCode()) {
				AccountsTO accountTO = getExpenseStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountMapList = new ArrayList<AccountsTO>();
					if (accountTO.getAccountSubType() > 0) {
						if (accountMap.containsKey(accountTO
								.getAccountSubType())) {
							accountMapList.addAll(accountMap.get(accountTO
									.getAccountSubType()));
						}
						accountMapList.add(accountTO);
						accountMap.put(accountTO.getAccountSubType(),
								accountMapList);
					} else {
						accountList.add(accountTO);
					}
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		LookupDetail lookupDetail = null;
		AccountsTO accountsTO = null;
		Map<String, List<AccountsTO>> viewSubTypeMap = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		if (null != accountMap && accountMap.size() > 0) {
			for (Entry<Long, List<AccountsTO>> entry : accountMap.entrySet()) {
				viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
				lookupDetail = lookupMasterBL.getLookupMasterService()
						.getLookupDetailById(entry.getKey());
				accountsTO = new AccountsTO();
				viewSubTypeMap.put(lookupDetail.getDisplayName(),
						entry.getValue());
				accountsTO.setAccountsMap(viewSubTypeMap);
				finalViewList.add(accountsTO);
			}
			if (null != accountList && accountList.size() > 0)
				finalViewList.addAll(accountList);
		} else
			finalViewList.addAll(accountList);

		Collections.sort(finalViewList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		return finalViewList;
	}

	public List<AccountsTO> getConsolidatedRevenueStatement(
			List<Ledger> ledgerList) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, List<AccountsTO>> accountMap = new HashMap<Long, List<AccountsTO>>();
		List<AccountsTO> accountMapList = null;
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Revenue
							.getCode()) {
				AccountsTO accountTO = getRevenueStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountMapList = new ArrayList<AccountsTO>();
					if (accountTO.getAccountSubType() > 0) {
						if (accountMap.containsKey(accountTO
								.getAccountSubType())) {
							accountMapList.addAll(accountMap.get(accountTO
									.getAccountSubType()));
						}
						accountMapList.add(accountTO);
						accountMap.put(accountTO.getAccountSubType(),
								accountMapList);
					} else {
						accountList.add(accountTO);
					}
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		LookupDetail lookupDetail = null;
		AccountsTO accountsTO = null;
		Map<String, List<AccountsTO>> viewSubTypeMap = null;
		List<AccountsTO> finalViewList = new ArrayList<AccountsTO>();
		if (null != accountMap && accountMap.size() > 0) {
			for (Entry<Long, List<AccountsTO>> entry : accountMap.entrySet()) {
				viewSubTypeMap = new HashMap<String, List<AccountsTO>>();
				lookupDetail = lookupMasterBL.getLookupMasterService()
						.getLookupDetailById(entry.getKey());
				accountsTO = new AccountsTO();
				viewSubTypeMap.put(lookupDetail.getDisplayName(),
						entry.getValue());
				accountsTO.setAccountsMap(viewSubTypeMap);
				finalViewList.add(accountsTO);
			}
			if (null != accountList && accountList.size() > 0)
				finalViewList.addAll(accountList);
		} else
			finalViewList.addAll(accountList);

		Collections.sort(finalViewList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO o1, AccountsTO o2) {
				return o1.getCostCenterAccountId().compareTo(
						o2.getCostCenterAccountId());
			}
		});
		return finalViewList;
	}

	public List<AccountsTO> getExpenseStatement(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
							.getCode()) {
				AccountsTO accountTO = getExpenseStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getRevenueStatement(List<Ledger> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (Ledger list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& (int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Revenue
							.getCode()) {
				AccountsTO accountTO = getRevenueStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	// Income Statement Expense Statement from Ledger Fiscal
	public List<AccountsTO> getExpenseStatementFiscal(
			List<LedgerFiscal> ledgerFiscals) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerFiscals) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& list.getCombination().getAccountByNaturalAccountId()
							.getAccountType() == (int) AccountType.Expenses
							.getCode()) {
				AccountsTO accountTO = getExpenseStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	// Income Statement Revenue Statement from Ledger Fiscal
	public List<AccountsTO> getRevenueStatementFiscal(
			List<LedgerFiscal> ledgerFiscals) throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerFiscals) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& list.getCombination().getAccountByNaturalAccountId()
							.getAccountType() == (int) AccountType.Revenue
							.getCode()) {
				AccountsTO accountTO = getRevenueStatement(list, accountList,
						hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public double calculateConsolidatedAssetTotal(List<AccountsTO> account)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : account) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						totalAmount += Double.parseDouble(accountsTO
								.getAssetBalanceAmount().replace(",", ""));
					}
				}
			} else {
				totalAmount += Double.parseDouble(list.getAssetBalanceAmount()
						.replace(",", ""));
			}
		}
		return totalAmount;
	}

	public double calculateConsolidatedLiabilityTotal(List<AccountsTO> account)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : account) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						totalAmount += Double.parseDouble(accountsTO
								.getLiabilityBalanceAmount().replace(",", ""));
					}
				}
			} else {
				totalAmount += Double.parseDouble(list
						.getLiabilityBalanceAmount().replace(",", ""));
			}
		}
		return totalAmount;
	}

	public double calculateAssetTotal(List<AccountsTO> account)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : account) {
			totalAmount += Double.parseDouble(list.getAssetBalanceAmount()
					.replace(",", ""));
		}
		return totalAmount;
	}

	public double calculateLiabilityTotal(List<AccountsTO> account)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : account) {
			totalAmount += Double.parseDouble(list.getLiabilityBalanceAmount()
					.replace(",", ""));
		}
		return totalAmount;
	}

	public Double calcucateConsolidatedExpense(List<AccountsTO> expenseList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : expenseList) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						totalAmount += Double.parseDouble(accountsTO
								.getExpenseBalanceAmount().replaceAll(",", ""));
					}
				}
			} else {
				totalAmount += Double.parseDouble(list
						.getExpenseBalanceAmount().replaceAll(",", ""));
			}
		}
		return totalAmount;
	}

	public Double calcucateConsolidatedRevenue(List<AccountsTO> revenueList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : revenueList) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						totalAmount += Double.parseDouble(accountsTO
								.getRevenueBalanceAmount().replaceAll(",", ""));
					}
				}
			} else {
				totalAmount += Double.parseDouble(list
						.getRevenueBalanceAmount().replaceAll(",", ""));
			}
		}
		return totalAmount;
	}

	public Double calcucateExpense(List<AccountsTO> expenseList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : expenseList) {
			totalAmount += Double.parseDouble(list.getExpenseBalanceAmount());
		}
		return totalAmount;
	}

	public Double calcucateRevenue(List<AccountsTO> revenueList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : revenueList) {
			totalAmount += Double.parseDouble(list.getRevenueBalanceAmount());
		}
		return totalAmount;
	}

	public Double calcucateDebit(List<AccountsTO> debitList) throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : debitList) {
			if (null != list.getDebitBalanceTotal()
					&& !list.getDebitBalanceTotal().equals("")) {
				totalAmount += Double.parseDouble(list.getDebitBalanceTotal());
			}
		}
		return totalAmount;
	}

	public Double calcucateCredit(List<AccountsTO> creditList) throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : creditList) {
			if (null != list.getCreditBalanceTotal()
					&& !list.getCreditBalanceTotal().equals("")) {
				totalAmount += Double.parseDouble(list.getCreditBalanceTotal());
			}
		}
		return totalAmount;
	}

	public Double calcucateConsolidatedDebit(List<AccountsTO> debitList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : debitList) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						if (null != accountsTO.getDebitBalanceTotal()
								&& !accountsTO.getDebitBalanceTotal()
										.equals("")) {
							totalAmount += Double
									.parseDouble(accountsTO
											.getDebitBalanceTotal().replaceAll(
													",", ""));
						}
					}
				}
			} else {
				if (null != list.getDebitBalanceTotal()
						&& !list.getDebitBalanceTotal().equals("")) {
					totalAmount += Double.parseDouble(list
							.getDebitBalanceTotal().replaceAll(",", ""));
				}
			}
		}
		return totalAmount;
	}

	public Double calcucateConsolidatedCredit(List<AccountsTO> creditList)
			throws Exception {
		double totalAmount = 0;
		for (AccountsTO list : creditList) {
			if (null != list.getAccountsMap()
					&& list.getAccountsMap().size() > 0) {
				for (Entry<String, List<AccountsTO>> entry : list
						.getAccountsMap().entrySet()) {
					for (AccountsTO accountsTO : entry.getValue()) {
						if (null != accountsTO.getCreditBalanceTotal()
								&& !accountsTO.getCreditBalanceTotal().equals(
										"")) {
							totalAmount += Double.parseDouble(accountsTO
									.getCreditBalanceTotal()
									.replaceAll(",", ""));
						}
					}
				}
			} else {
				if (null != list.getCreditBalanceTotal()
						&& !list.getCreditBalanceTotal().equals("")) {
					totalAmount += Double.parseDouble(list
							.getCreditBalanceTotal().replaceAll(",", ""));
				}
			}
		}
		return totalAmount;
	}

	private AccountsTO getExpenseStatement(Ledger ledger,
			List<AccountsTO> expenseList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != expenseList
				&& !("").equals(expenseList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledger.getSide()) {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						+ ledger.getBalance()));
			} else {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						- ledger.getBalance()));
			}
			if (Double.parseDouble(accountTO.getExpenseBalanceAmount()) >= 0)
				accountTO.setLedgerSide(true);
			else
				accountTO.setLedgerSide(false);
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAccountDescription(ledger.getCombination()
					.getAccountByCompanyAccountId().getCode()
					+ "."
					+ ledger.getCombination().getAccountByCostcenterAccountId()
							.getCode()
					+ "."
					+ ledger.getCombination().getAccountByNaturalAccountId()
							.getCode());
			accountTO.setAccountCode(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccount());
			accountTO.setExpenseAccountCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			if (ledger.getSide())
				accountTO.setExpenseBalanceAmount(ledger.getBalance()
						.toString());
			else
				accountTO.setExpenseBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			accountTO.setLedgerSide(ledger.getSide());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			accountTO.setAccountCode(accountTO.getExpenseAccountCode());
			if (null != ledger.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail())
				accountTO.setAccountSubType(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getLookupDetailId());
			return accountTO;
		} else {
			return null;
		}
	}

	private AccountsTO getRevenueStatement(Ledger ledger,
			List<AccountsTO> revenueList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != revenueList
				&& !("").equals(revenueList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledger.getSide()) {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						- ledger.getBalance()));
			} else {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						+ ledger.getBalance()));
			}
			if (Double.parseDouble(accountTO.getRevenueBalanceAmount()) >= 0)
				accountTO.setLedgerSide(false);
			else
				accountTO.setLedgerSide(true);
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAccountDescription(ledger.getCombination()
					.getAccountByCompanyAccountId().getCode()
					+ "."
					+ ledger.getCombination().getAccountByCostcenterAccountId()
							.getCode()
					+ "."
					+ ledger.getCombination().getAccountByNaturalAccountId()
							.getCode());
			accountTO.setAccountCode(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccount());
			accountTO.setRevenueAccountCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setLedgerSide(ledger.getSide());
			if (ledger.getSide())
				accountTO.setRevenueBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			else
				accountTO.setRevenueBalanceAmount(ledger.getBalance()
						.toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			accountTO.setAccountCode(accountTO.getRevenueAccountCode());
			if (null != ledger.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail())
				accountTO.setAccountSubType(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getLookupDetailId());
			return accountTO;
		} else {
			return null;
		}
	}

	// Expense Statement from Ledger Fiscal
	private AccountsTO getExpenseStatement(LedgerFiscal ledgerFiscal,
			List<AccountsTO> expenseList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledgerFiscal.getCombination()
				.getAccountByAnalysisAccountId()
				&& !ledgerFiscal.getCombination()
						.getAccountByAnalysisAccountId().equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledgerFiscal.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledgerFiscal.getCombination();
		}
		if (null != expenseList
				&& !("").equals(expenseList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledgerFiscal.getSide()) {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						+ ledgerFiscal.getBalance()));
			} else {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						- ledgerFiscal.getBalance()));
			}
			if (Double.parseDouble(accountTO.getExpenseBalanceAmount()) >= 0)
				accountTO.setLedgerSide(true);
			else
				accountTO.setLedgerSide(false);
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setExpenseAccountCode(ledgerFiscal
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledgerFiscal.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			if (ledgerFiscal.getSide())
				accountTO.setExpenseBalanceAmount(ledgerFiscal.getBalance()
						.toString());
			else
				accountTO.setExpenseBalanceAmount("-".concat(ledgerFiscal
						.getBalance().toString()));
			accountTO.setLedgerSide(ledgerFiscal.getSide());
			accountTO.setNaturalAccountId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledgerFiscal.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	// Revenue Statement from Ledger Fiscal
	private AccountsTO getRevenueStatement(LedgerFiscal ledgerFiscal,
			List<AccountsTO> revenueList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		if (null != ledgerFiscal.getCombination()
				.getAccountByAnalysisAccountId()
				&& !ledgerFiscal.getCombination()
						.getAccountByAnalysisAccountId().equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledgerFiscal.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledgerFiscal.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledgerFiscal.getCombination();
		}
		AccountsTO accountTO = null;
		if (null != revenueList
				&& !("").equals(revenueList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledgerFiscal.getSide()) {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						- ledgerFiscal.getBalance()));
			} else {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						+ ledgerFiscal.getBalance()));
			}
			if (Double.parseDouble(accountTO.getRevenueBalanceAmount()) >= 0)
				accountTO.setLedgerSide(false);
			else
				accountTO.setLedgerSide(true);
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setRevenueAccountCode(ledgerFiscal
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledgerFiscal.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setLedgerSide(ledgerFiscal.getSide());
			if (ledgerFiscal.getSide())
				accountTO.setRevenueBalanceAmount("-".concat(ledgerFiscal
						.getBalance().toString()));
			else
				accountTO.setRevenueBalanceAmount(ledgerFiscal.getBalance()
						.toString());
			accountTO.setNaturalAccountId(ledgerFiscal.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledgerFiscal.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	public TransactionDetailVO getAssetGroupValueNew(
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		LedgerVO ledgerVO = new LedgerVO();
		ledgerVO.setSide(true);
		ledgerVO.setBalance(0d);
		ledgerVO = processUpdateLedgerGroup(ledgerVO, transactionDetailVOs);
		transactionDetailVO.setCombination(ledgerVO.getCombination());
		transactionDetailVO.setIsDebit(ledgerVO.getSide());
		transactionDetailVO.setAmount(ledgerVO.getBalance());
		if (ledgerVO.getSide()) {
			transactionDetailVO.setAssetAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		} else {
			transactionDetailVO.setAssetAmount("-"
					+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setCreditAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		}
		return transactionDetailVO;
	}

	public TransactionDetailVO getExpenseGroupValueNew(
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		LedgerVO ledgerVO = new LedgerVO();
		ledgerVO.setSide(true);
		ledgerVO.setBalance(0d);
		ledgerVO = processUpdateLedgerGroup(ledgerVO, transactionDetailVOs);
		transactionDetailVO.setCombination(ledgerVO.getCombination());
		transactionDetailVO.setIsDebit(ledgerVO.getSide());
		transactionDetailVO.setAmount(ledgerVO.getBalance());
		if (ledgerVO.getSide()) {
			transactionDetailVO.setExpenseAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		} else {
			transactionDetailVO.setExpenseAmount("-"
					+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setCreditAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		}
		return transactionDetailVO;
	}

	public TransactionDetailVO getLiabilityGroupValueNew(
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		LedgerVO ledgerVO = new LedgerVO();
		ledgerVO.setSide(true);
		ledgerVO.setBalance(0d);
		ledgerVO = processUpdateLedgerGroup(ledgerVO, transactionDetailVOs);
		transactionDetailVO.setCombination(ledgerVO.getCombination());
		transactionDetailVO.setIsDebit(ledgerVO.getSide());
		transactionDetailVO.setAmount(ledgerVO.getBalance());
		if (!ledgerVO.getSide()) {
			transactionDetailVO.setLiabilityAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setCreditAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		} else {
			transactionDetailVO.setLiabilityAmount("-"
					+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		}
		return transactionDetailVO;
	}

	public TransactionDetailVO getRevenueGroupValueNew(
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		LedgerVO ledgerVO = new LedgerVO();
		ledgerVO.setSide(true);
		ledgerVO.setBalance(0d);
		ledgerVO = processUpdateLedgerGroup(ledgerVO, transactionDetailVOs);
		transactionDetailVO.setCombination(ledgerVO.getCombination());
		transactionDetailVO.setIsDebit(ledgerVO.getSide());
		transactionDetailVO.setAmount(ledgerVO.getBalance());
		if (!ledgerVO.getSide()) {
			transactionDetailVO.setRevenueAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setCreditAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		} else {
			transactionDetailVO.setRevenueAmount("-"
					+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(ledgerVO.getBalance()));
		}
		return transactionDetailVO;
	}

	public AccountsTO getAssetGroupValue(List<AccountsTO> assetList,
			boolean isTrialBalance) {
		AccountsTO finalAccountTO = new AccountsTO();
		if (isTrialBalance) {
			finalAccountTO = processAssetExpenseSet(assetList);
		} else {
			finalAccountTO.setLedgerSide(true);
			finalAccountTO.setAssetBalanceAmount("0");
			for (AccountsTO accountTO : assetList) {
				if (accountTO.getLedgerSide()) {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setAssetBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getAssetBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getAssetBalanceAmount())));
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setAssetBalanceAmount(String
								.valueOf(Double.parseDouble(finalAccountTO
										.getAssetBalanceAmount())
										+ Double.parseDouble(accountTO
												.getAssetBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getAssetBalanceAmount()) >= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					}
				} else {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setAssetBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getAssetBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getAssetBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getAssetBalanceAmount()) >= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					} else {
						finalAccountTO.setAssetBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getAssetBalanceAmount())
										+ (Double.parseDouble(finalAccountTO
												.getAssetBalanceAmount()))));
						finalAccountTO.setLedgerSide(false);
					}
				}
			}
		}
		return finalAccountTO;
	}

	public AccountsTO getLiabiltyGroupValue(List<AccountsTO> liabiltyList,
			boolean isTrialBalance) {
		AccountsTO finalAccountTO = new AccountsTO();
		if (isTrialBalance)
			finalAccountTO = processLiabilityRevenueSet(liabiltyList);
		else {
			finalAccountTO.setLiabilityBalanceAmount("0");
			finalAccountTO.setLedgerSide(true);
			for (AccountsTO accountTO : liabiltyList) {
				if (accountTO.getLedgerSide()) {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getLiabilityBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount())));
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(finalAccountTO
										.getLiabilityBalanceAmount())
										- Double.parseDouble(accountTO
												.getLiabilityBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getLiabilityBalanceAmount()) <= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					}
				} else {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getLiabilityBalanceAmount())
										- Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getLiabilityBalanceAmount()) > 0)
							finalAccountTO.setLedgerSide(false);
						else
							finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO
								.setLiabilityBalanceAmount(String.valueOf(Double
										.parseDouble(accountTO
												.getLiabilityBalanceAmount())
										+ (Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount()))));
						finalAccountTO.setLedgerSide(false);
					}
				}
			}
		}
		return finalAccountTO;
	}

	public AccountsTO getRevenueGroupValue(List<AccountsTO> revenueList,
			boolean isTrialBalance) {
		AccountsTO finalAccountTO = new AccountsTO();
		if (isTrialBalance)
			finalAccountTO = processLiabilityRevenueSet(revenueList);
		else {
			finalAccountTO.setLedgerSide(true);
			finalAccountTO.setRevenueBalanceAmount("0");
			for (AccountsTO accountTO : revenueList) {
				if (accountTO.getLedgerSide()) {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setRevenueBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getRevenueBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getRevenueBalanceAmount())));
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setRevenueBalanceAmount(String
								.valueOf(Double.parseDouble(finalAccountTO
										.getRevenueBalanceAmount())
										- Double.parseDouble(accountTO
												.getRevenueBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getRevenueBalanceAmount()) <= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					}
				} else {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setRevenueBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getRevenueBalanceAmount())
										- Double.parseDouble(finalAccountTO
												.getRevenueBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getRevenueBalanceAmount()) > 0)
							finalAccountTO.setLedgerSide(false);
						else
							finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setRevenueBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getRevenueBalanceAmount())
										+ (Double.parseDouble(finalAccountTO
												.getRevenueBalanceAmount()))));
						finalAccountTO.setLedgerSide(false);
					}
				}
			}
		}
		return finalAccountTO;
	}

	public AccountsTO getExpenseGroupValue(List<AccountsTO> expenseList,
			boolean isTrialBalance) {
		AccountsTO finalAccountTO = new AccountsTO();
		if (isTrialBalance)
			finalAccountTO = processAssetExpenseSet(expenseList);
		else {
			finalAccountTO.setLedgerSide(true);
			finalAccountTO.setExpenseBalanceAmount("0");
			for (AccountsTO accountTO : expenseList) {
				if (accountTO.getLedgerSide()) {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setExpenseBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getExpenseBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getExpenseBalanceAmount())));
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setExpenseBalanceAmount(String
								.valueOf(Double.parseDouble(finalAccountTO
										.getExpenseBalanceAmount())
										+ Double.parseDouble(accountTO
												.getExpenseBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getExpenseBalanceAmount()) >= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					}
				} else {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setExpenseBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getExpenseBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getExpenseBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getExpenseBalanceAmount()) >= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					} else {
						finalAccountTO.setExpenseBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getExpenseBalanceAmount())
										+ (Double.parseDouble(finalAccountTO
												.getExpenseBalanceAmount()))));
						finalAccountTO.setLedgerSide(false);
					}
				}
			}
		}
		return finalAccountTO;
	}

	public AccountsTO getOwnersEquityGroupValue(
			List<AccountsTO> ownersEquityList, boolean isTrialBalance) {
		AccountsTO finalAccountTO = new AccountsTO();
		if (isTrialBalance)
			finalAccountTO = processLiabilityRevenueSet(ownersEquityList);
		else {
			finalAccountTO.setLiabilityBalanceAmount("0");
			finalAccountTO.setLedgerSide(true);
			for (AccountsTO accountTO : ownersEquityList) {
				if (accountTO.getLedgerSide()) {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getLiabilityBalanceAmount())
										+ Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount())));
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(finalAccountTO
										.getLiabilityBalanceAmount())
										- Double.parseDouble(accountTO
												.getLiabilityBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getLiabilityBalanceAmount()) <= 0)
							finalAccountTO.setLedgerSide(true);
						else
							finalAccountTO.setLedgerSide(false);
					}
				} else {
					if (finalAccountTO.getLedgerSide()) {
						finalAccountTO.setLiabilityBalanceAmount(String
								.valueOf(Double.parseDouble(accountTO
										.getLiabilityBalanceAmount())
										- Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount())));
						if (Double.parseDouble(finalAccountTO
								.getLiabilityBalanceAmount()) > 0)
							finalAccountTO.setLedgerSide(false);
						else
							finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO
								.setLiabilityBalanceAmount(String.valueOf(Double
										.parseDouble(accountTO
												.getLiabilityBalanceAmount())
										+ (Double.parseDouble(finalAccountTO
												.getLiabilityBalanceAmount()))));
						finalAccountTO.setLedgerSide(false);
					}
				}
			}
		}
		return finalAccountTO;
	}

	private AccountsTO processAssetExpenseSet(List<AccountsTO> assetList) {
		AccountsTO finalAccountTO = new AccountsTO();
		finalAccountTO.setDebitBalanceTotal("0");
		finalAccountTO.setCreditBalanceTotal("0");
		finalAccountTO.setLedgerSide(true);
		double tempBalance = 0;
		for (AccountsTO accountTO : assetList) {
			if (accountTO.getLedgerSide()) {
				if (finalAccountTO.getLedgerSide()) {
					finalAccountTO.setDebitBalanceTotal(String.valueOf(Double
							.parseDouble(accountTO.getDebitBalanceTotal())
							+ Double.parseDouble(finalAccountTO
									.getDebitBalanceTotal())));
					finalAccountTO.setLedgerSide(true);
				} else {
					tempBalance = Double.parseDouble(accountTO
							.getDebitBalanceTotal());
					if ((tempBalance - Double.parseDouble(finalAccountTO
							.getCreditBalanceTotal())) >= 0) {
						finalAccountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getCreditBalanceTotal()))));
						finalAccountTO.setCreditBalanceTotal(null);
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setCreditBalanceTotal(String
								.valueOf(Math.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getCreditBalanceTotal()))));
						finalAccountTO.setDebitBalanceTotal(null);
						finalAccountTO.setLedgerSide(false);
					}
				}
			} else {
				if (finalAccountTO.getLedgerSide()) {
					tempBalance = Double.parseDouble(accountTO
							.getCreditBalanceTotal());
					if ((-tempBalance + Double.parseDouble(finalAccountTO
							.getDebitBalanceTotal())) >= 0) {
						finalAccountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(-tempBalance
										+ Double.parseDouble(finalAccountTO
												.getDebitBalanceTotal()))));
						finalAccountTO.setCreditBalanceTotal(null);
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setCreditBalanceTotal(String
								.valueOf(Math.abs(-tempBalance
										+ Double.parseDouble(finalAccountTO
												.getDebitBalanceTotal()))));
						finalAccountTO.setDebitBalanceTotal(null);
						finalAccountTO.setLedgerSide(false);
					}
				} else {
					tempBalance = Double.parseDouble(accountTO
							.getCreditBalanceTotal());
					finalAccountTO.setCreditBalanceTotal(String.valueOf(Math
							.abs(tempBalance
									+ Double.parseDouble(finalAccountTO
											.getCreditBalanceTotal()))));
					finalAccountTO.setDebitBalanceTotal(null);
					finalAccountTO.setLedgerSide(false);
				}
			}
		}
		return finalAccountTO;
	}

	private AccountsTO processLiabilityRevenueSet(List<AccountsTO> accountSet) {
		AccountsTO finalAccountTO = new AccountsTO();
		finalAccountTO.setDebitBalanceTotal("0");
		finalAccountTO.setCreditBalanceTotal("0");
		finalAccountTO.setLedgerSide(true);
		double tempBalance = 0;
		for (AccountsTO accountTO : accountSet) {
			if (accountTO.getLedgerSide()) {
				if (finalAccountTO.getLedgerSide()) {
					finalAccountTO.setDebitBalanceTotal(String.valueOf(Double
							.parseDouble(accountTO.getDebitBalanceTotal())
							+ Double.parseDouble(finalAccountTO
									.getDebitBalanceTotal())));
					finalAccountTO.setLedgerSide(true);
				} else {
					tempBalance = Double.parseDouble(accountTO
							.getDebitBalanceTotal());
					if ((tempBalance - Double.parseDouble(finalAccountTO
							.getCreditBalanceTotal())) >= 0) {
						finalAccountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getCreditBalanceTotal()))));
						finalAccountTO.setCreditBalanceTotal(null);
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setCreditBalanceTotal(String
								.valueOf(Math.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getCreditBalanceTotal()))));
						finalAccountTO.setDebitBalanceTotal(null);
						finalAccountTO.setLedgerSide(false);
					}
				}
			} else {
				if (finalAccountTO.getLedgerSide()) {
					tempBalance = Double.parseDouble(accountTO
							.getCreditBalanceTotal());
					if ((tempBalance - Double.parseDouble(finalAccountTO
							.getDebitBalanceTotal())) <= 0) {
						finalAccountTO.setDebitBalanceTotal(String.valueOf(Math
								.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getDebitBalanceTotal()))));
						finalAccountTO.setCreditBalanceTotal(null);
						finalAccountTO.setLedgerSide(true);
					} else {
						finalAccountTO.setCreditBalanceTotal(String
								.valueOf(Math.abs(tempBalance
										- Double.parseDouble(finalAccountTO
												.getDebitBalanceTotal()))));
						finalAccountTO.setDebitBalanceTotal(null);
						finalAccountTO.setLedgerSide(false);
					}
				} else {
					tempBalance = Double.parseDouble(accountTO
							.getCreditBalanceTotal());
					finalAccountTO.setCreditBalanceTotal(String.valueOf(Math
							.abs(tempBalance
									+ Double.parseDouble(finalAccountTO
											.getCreditBalanceTotal()))));
					finalAccountTO.setDebitBalanceTotal(null);
					finalAccountTO.setLedgerSide(false);
				}
			}
		}
		return finalAccountTO;
	}

	private AccountsTO getAssetType(Ledger ledger, List<AccountsTO> assetsList,
			Map<Long, AccountsTO> hashAccount) throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != assetsList
				&& assetsList.size() > 0
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledger.getBalance() > 0) {
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (ledger.getSide()) {
					accountTO.setAssetBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getAssetBalanceAmount())
							+ ledger.getBalance()));
				} else {
					accountTO.setAssetBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getAssetBalanceAmount())
							- ledger.getBalance()));
				}
				if (Double.parseDouble(accountTO.getAssetBalanceAmount()) >= 0)
					accountTO.setLedgerSide(true);
				else
					accountTO.setLedgerSide(false);
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAssetCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setAccountCode(accountTO.getAssetCode());
			accountTO.setLedgerSide(ledger.getSide());
			if (!ledger.getSide())
				accountTO.setAssetBalanceAmount("-".concat(ledger.getBalance()
						.toString()));
			else
				accountTO.setAssetBalanceAmount(ledger.getBalance().toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			if (null != ledger.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail())
				accountTO.setAccountSubType(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getLookupDetailId());
			return accountTO;
		} else {
			return null;
		}
	}

	private AccountsTO getLiabilityType(Ledger ledger,
			List<AccountsTO> liabilityList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		AccountsTO accountTO = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != liabilityList
				&& liabilityList.size() > 0
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledger.getBalance() > 0) {
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (ledger.getSide()) {
					accountTO.setLiabilityBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getLiabilityBalanceAmount())
							- ledger.getBalance()));
				} else {
					accountTO.setLiabilityBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getLiabilityBalanceAmount())
							+ ledger.getBalance()));
				}
				if (Double.parseDouble(accountTO.getLiabilityBalanceAmount()) >= 0)
					accountTO.setLedgerSide(TransactionType.Credit.getCode());
				else
					accountTO.setLedgerSide(TransactionType.Debit.getCode());
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setLiabilityCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setAccountCode(accountTO.getLiabilityCode());
			accountTO.setAccountTypeId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			accountTO.setLedgerSide(ledger.getSide());
			if (ledger.getSide() && ledger.getBalance() > 0)
				accountTO.setLiabilityBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			else
				accountTO.setLiabilityBalanceAmount(ledger.getBalance()
						.toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			if (null != ledger.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail())
				accountTO.setAccountSubType(ledger.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()
						.getLookupDetailId());
			return accountTO;
		} else {
			return null;
		}
	}

	public List<Combination> getAllCombinationList(Implementation implementation)
			throws Exception {
		List<Combination> combinationList = getAccountsEnterpriseService()
				.getCombinationService().getAllCombinationForView(
						implementation);
		return combinationList;
	}

	public List<AccountsTO> showJournalStatement(Long journalId)
			throws Exception {
		Transaction transaction = this.getTransactionBL()
				.getTransactionService().getTransactionById(journalId);
		AccountsTO accountTO = new AccountsTO();
		AccountsTO tempAccount = null;
		List<AccountsTO> accountTOList = new ArrayList<AccountsTO>();
		List<AccountsTO> transationTOList = new ArrayList<AccountsTO>();
		accountTO.setPeriodName(transaction.getPeriod().getName());
		accountTO.setCurrencyCode(transaction.getCurrency().getCurrencyPool()
				.getCode());
		accountTO.setCategoryName(transaction.getCategory().getName());
		accountTO.setFromDate(DateFormat.convertDateToString(transaction
				.getTransactionTime().toString()));
		double totalTransactionAmount = 0;
		for (TransactionDetail list : transaction.getTransactionDetails()) {
			tempAccount = new AccountsTO();
			if (null != list.getCombination().getAccountByAnalysisAccountId())
				tempAccount.setAccountCode(list.getCombination()
						.getAccountByNaturalAccountId().getAccount()
						+ "."
						+ list.getCombination().getAccountByAnalysisAccountId()
								.getAccount());
			else
				tempAccount.setAccountCode(list.getCombination()
						.getAccountByNaturalAccountId().getAccount());
			if (list.getIsDebit())
				tempAccount.setTransactionFlag("Debit");
			else
				tempAccount.setTransactionFlag("Credit");
			tempAccount.setAmount(AIOSCommons.formatAmount(Double.valueOf(list
					.getAmount())));
			tempAccount.setLineDescription(list.getDescription());
			totalTransactionAmount += list.getAmount();
			transationTOList.add(tempAccount);
		}
		accountTO.setDebitBalanceTotal(AIOSCommons.formatAmount(Double
				.valueOf(totalTransactionAmount)));
		accountTOList.add(accountTO);
		accountTO.setTransactionDetail(transationTOList);
		return accountTOList;
	}

	public AccountsTO showJournalVoucher(Long journalId) throws Exception {
		Transaction transaction = this.getTransactionBL()
				.getTransactionService().getTransactionById(journalId);
		AccountsTO accountTO = new AccountsTO();
		AccountsTO tempAccount = null;
		List<AccountsTO> transationTOList = new ArrayList<AccountsTO>();
		accountTO.setVoucherNumber(transaction.getJournalNumber());
		accountTO.setPeriodName(transaction.getPeriod().getName());
		accountTO.setCurrencyCode(transaction.getCurrency().getCurrencyPool()
				.getCode());
		accountTO.setCategoryName(transaction.getCategory().getName());
		accountTO.setFromDate(DateFormat.convertDateToString(transaction
				.getTransactionTime().toString()));
		accountTO.setDescription(transaction.getDescription());
		List<TransactionDetail> transactionDetail = new ArrayList<TransactionDetail>(
				transaction.getTransactionDetails());
		Collections.sort(transactionDetail,
				new Comparator<TransactionDetail>() {
					@Override
					public int compare(TransactionDetail t1,
							TransactionDetail t2) {
						return t1.getTransactionDetailId().compareTo(
								t2.getTransactionDetailId());
					}
				});
		Collections.sort(transactionDetail,
				new Comparator<TransactionDetail>() {
					@Override
					public int compare(TransactionDetail t1,
							TransactionDetail t2) {
						return t2.getIsDebit().compareTo(t1.getIsDebit());
					}
				});
		double totalTransactionAmount = 0;
		for (TransactionDetail list : transactionDetail) {
			tempAccount = new AccountsTO();
			if (null != list.getCombination().getAccountByAnalysisAccountId()) {
				tempAccount.setAccountCode(list
						.getCombination()
						.getAccountByCostcenterAccountId()
						.getAccount()
						.concat(".")
						.concat(list.getCombination()
								.getAccountByNaturalAccountId().getAccount())
						.concat(".")
						.concat(list.getCombination()
								.getAccountByAnalysisAccountId().getAccount()));
			} else {
				tempAccount.setAccountCode(list
						.getCombination()
						.getAccountByCostcenterAccountId()
						.getAccount()
						.concat(".")
						.concat(list.getCombination()
								.getAccountByNaturalAccountId().getAccount()));
			}
			if (list.getIsDebit()) {
				tempAccount.setDebitAmount(AIOSCommons.formatAmount(Double
						.valueOf(list.getAmount())));
				tempAccount.setDebitBalance(list.getAmount().toString());
				totalTransactionAmount += list.getAmount();
			} else {
				tempAccount.setCreditAmount(AIOSCommons.formatAmount(Double
						.valueOf(list.getAmount())));
				tempAccount.setCreditBalance(list.getAmount().toString());
			}
			tempAccount.setLineDescription(list.getDescription());
			transationTOList.add(tempAccount);
		}
		accountTO.setDebitBalanceTotal(AIOSCommons.formatAmount(Double
				.valueOf(totalTransactionAmount)));
		accountTO.setAmount(String.valueOf(totalTransactionAmount));
		accountTO.setTransactionDetail(transationTOList);
		return accountTO;
	}

	public AccountsTO showTempJournalVoucher(long journalId) throws Exception {
		TransactionTemp transaction = this.getTransactionBL()
				.getTransactionTempService().getJournalByTempId(journalId);
		AccountsTO accountTO = new AccountsTO();
		AccountsTO tempAccount = null;
		List<AccountsTO> transationTOList = new ArrayList<AccountsTO>();
		accountTO.setVoucherNumber(transaction.getJournalNumber());
		accountTO.setPeriodName(transaction.getPeriod().getName());
		accountTO.setCurrencyCode(transaction.getCurrency().getCurrencyPool()
				.getCode());
		accountTO.setCategoryName(transaction.getCategory().getName());
		accountTO.setFromDate(DateFormat.convertDateToString(transaction
				.getTransactionTime().toString()));
		accountTO.setDescription(transaction.getDescription());
		List<TransactionTempDetail> transactionTempDetail = new ArrayList<TransactionTempDetail>(
				transaction.getTransactionTempDetails());
		Collections.sort(transactionTempDetail,
				new Comparator<TransactionTempDetail>() {
					public int compare(TransactionTempDetail t1,
							TransactionTempDetail t2) {
						return t1.getTransactionTempDetailId().compareTo(
								t2.getTransactionTempDetailId());
					}
				});
		Collections.sort(transactionTempDetail,
				new Comparator<TransactionTempDetail>() {
					@Override
					public int compare(TransactionTempDetail t1,
							TransactionTempDetail t2) {
						return t2.getIsDebit().compareTo(t1.getIsDebit());
					}
				});
		double totalTransactionAmount = 0;
		for (TransactionTempDetail list : transactionTempDetail) {
			tempAccount = new AccountsTO();
			if (null != list.getCombination().getAccountByAnalysisAccountId()) {
				tempAccount.setAccountCode(list
						.getCombination()
						.getAccountByCostcenterAccountId()
						.getAccount()
						.concat(".")
						.concat(list.getCombination()
								.getAccountByNaturalAccountId().getAccount())
						.concat(".")
						.concat(list.getCombination()
								.getAccountByAnalysisAccountId().getAccount()));
			} else {
				tempAccount.setAccountCode(list
						.getCombination()
						.getAccountByCostcenterAccountId()
						.getAccount()
						.concat(".")
						.concat(list.getCombination()
								.getAccountByNaturalAccountId().getAccount()));
			}
			if (list.getIsDebit()) {
				tempAccount.setDebitAmount(AIOSCommons.formatAmount(Double
						.valueOf(list.getAmount())));
				tempAccount.setDebitBalance(list.getAmount().toString());
				totalTransactionAmount += list.getAmount();
			} else {
				tempAccount.setCreditAmount(AIOSCommons.formatAmount(Double
						.valueOf(list.getAmount())));
				tempAccount.setCreditBalance(list.getAmount().toString());
			}
			tempAccount.setLineDescription(list.getDescription());
			transationTOList.add(tempAccount);
		}
		accountTO.setDebitBalanceTotal(AIOSCommons.formatAmount(Double
				.valueOf(totalTransactionAmount)));
		accountTO.setAmount(String.valueOf(totalTransactionAmount));
		accountTO.setTransactionDetail(transationTOList);
		return accountTO;
	}

	public List<AccountsTO> createLedgerStatement(
			Implementation implementation, AccountsTO accounts,
			String accountList) throws Exception {
		boolean flag = true;
		List<AccountsTO> accountTOList = new ArrayList<AccountsTO>();
		List<Transaction> transactionList;
		// Query string builder
		String queryStr = " ";
		String[] dataArray = null;
		if (null != accountList && !("").equals(accountList)) {
			dataArray = new String[accountList.split(",").length];
			dataArray = accountList.split(",");
		}
		if (implementation != null)
			queryStr += " WHERE t.implementation.implementationId="
					+ implementation.getImplementationId();

		if (null != dataArray && dataArray.length > 1) {
			queryStr += " AND td.combination.combinationId in (" + accountList
					+ ") ";
		} else if (dataArray != null && dataArray.length == 1) {
			queryStr += " AND td.combination.combinationId=" + dataArray[0];
		}

		if (accounts.getCategoryId() > 0)
			queryStr += " AND t.category.categoryId ="
					+ accounts.getCategoryId();

		if (null != accounts.getFromDate()
				&& !("").equals(accounts.getFromDate()))
			queryStr += " AND t.transactionTime >='" + accounts.getFromDate()
					+ "'";

		if (null != accounts.getToDate() && !("").equals(accounts.getToDate()))
			queryStr += " AND t.transactionTime <='" + accounts.getToDate()
					+ "'";

		transactionList = this.getTransactionBL().getTransactionService()
				.getJournalEntriesByCombination(queryStr);

		List<TransactionDetail> transactionDetailList = new ArrayList<TransactionDetail>();
		for (int i = 0; i < transactionList.size(); i++) {
			transactionDetailList.addAll(transactionList.get(i)
					.getTransactionDetails());
		}
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (TransactionDetail list : transactionDetailList) {
			if (accountTOList.size() > 0) {
				if (!hashAccount.containsKey(list.getCombination()
						.getCombinationId())) {
					flag = true;
				}
			} else {
				flag = true;
			}
			if (flag) {
				AccountsTO accountTO = createLedgerStatementView(list,
						transactionDetailList);
				accountTOList.add(accountTO);
				hashAccount.put(accountTO.getCombinationId(), accountTO);
				flag = false;
			}
		}
		return accountTOList;
	}

	public AccountsTO createLedgerStatementView(
			TransactionDetail transactionList,
			List<TransactionDetail> transactionDetailList) throws Exception {
		AccountsTO accountTO = new AccountsTO();
		AccountsTO accountTempDesc = new AccountsTO();
		List<AccountsTO> descriptionList = new ArrayList<AccountsTO>();
		accountTO.setCombinationId(transactionList.getCombination()
				.getCombinationId());
		accountTO.setAccountTypeId(transactionList.getCombination()
				.getAccountByNaturalAccountId().getAccountType());
		if (null != transactionList.getCombination()
				.getAccountByAnalysisAccountId()) {
			accountTO.setAccountCode(transactionList
					.getCombination()
					.getAccountByNaturalAccountId()
					.getAccount()
					.concat(".")
					.concat(transactionList.getCombination()
							.getAccountByAnalysisAccountId().getAccount()));
		} else {
			accountTO.setAccountCode(transactionList.getCombination()
					.getAccountByNaturalAccountId().getAccount());
		}
		for (TransactionDetail list2 : transactionDetailList) {
			if (list2.getCombination().getCombinationId()
					.equals(accountTO.getCombinationId())) {
				accountTempDesc = new AccountsTO();
				accountTempDesc.setLineDescription(list2.getDescription());
				accountTempDesc.setVoucherNumber(list2.getTransaction()
						.getJournalNumber());
				if (list2.getIsDebit()) {
					accountTempDesc.setDebitBalance(String.valueOf(list2
							.getAmount()));
					accountTempDesc.setLedgerSide(list2.getIsDebit());
					descriptionList.add(accountTempDesc);
					accountTO.setDescriptionList(descriptionList);
				} else {
					accountTempDesc.setCreditBalance(String.valueOf(list2
							.getAmount()));
					accountTempDesc.setLedgerSide(list2.getIsDebit());
					descriptionList.add(accountTempDesc);
					accountTO.setDescriptionList(descriptionList);
				}

				if (accountTO.getAccountTypeId() == 1
						|| accountTO.getAccountTypeId() == 4) {
					if (list2.getIsDebit()) {
						if (null != accountTO.getDebitBalanceTotal()
								&& !accountTO.getDebitBalanceTotal().equals("")) {
							accountTO.setDebitBalanceTotal(String
									.valueOf(Double.parseDouble(accountTO
											.getDebitBalanceTotal())
											+ list2.getAmount()));
							accountTO.setDebitBalanceTotal(accountTO
									.getDebitBalanceTotal());
						} else {
							accountTO.setDebitBalanceTotal(String.valueOf(list2
									.getAmount()));
						}
					} else {
						if (null != accountTO.getCreditBalanceTotal()
								&& !accountTO.getCreditBalanceTotal()
										.equals("")) {
							accountTO.setCreditBalanceTotal(String
									.valueOf(Double.parseDouble(accountTO
											.getCreditBalanceTotal())
											+ list2.getAmount()));
							accountTO.setCreditBalanceTotal(accountTO
									.getCreditBalanceTotal());
						} else {
							accountTO.setCreditBalanceTotal(String
									.valueOf(list2.getAmount()));
						}
					}
				} else {
					if (list2.getIsDebit()) {
						if (null != accountTO.getDebitBalanceTotal()
								&& !accountTO.getDebitBalanceTotal().equals("")) {
							accountTO.setDebitBalanceTotal(String
									.valueOf(Double.parseDouble(accountTO
											.getDebitBalanceTotal())
											+ list2.getAmount()));
							accountTO.setDebitBalanceTotal(accountTO
									.getDebitBalanceTotal());
						} else {
							accountTO.setDebitBalanceTotal(String.valueOf(list2
									.getAmount()));
						}
					} else {
						if (null != accountTO.getCreditBalanceTotal()
								&& !accountTO.getCreditBalanceTotal()
										.equals("")) {
							accountTO.setCreditBalanceTotal(String
									.valueOf(Double.parseDouble(accountTO
											.getCreditBalanceTotal())
											+ list2.getAmount()));
							accountTO.setCreditBalanceTotal(accountTO
									.getCreditBalanceTotal());
						} else {
							accountTO.setCreditBalanceTotal(String
									.valueOf(list2.getAmount()));
						}
					}
				}
			}
		}
		String openingBalance = "";
		boolean debitFlag = false;
		if (null != accountTO.getDebitBalanceTotal()
				&& null != accountTO.getCreditBalanceTotal()) {
			openingBalance = String.valueOf(Double.parseDouble(accountTO
					.getDebitBalanceTotal())
					- Double.parseDouble(accountTO.getCreditBalanceTotal()));
			if (null != openingBalance && !("").equals(openingBalance)
					&& Double.valueOf(openingBalance) > 0) {
				debitFlag = true;
			}
		} else if (null == accountTO.getDebitBalanceTotal()
				&& null == accountTO.getCreditBalanceTotal()) {
			openingBalance = "";
		} else if (null != accountTO.getDebitBalanceTotal()
				&& null == accountTO.getCreditBalanceTotal()) {
			openingBalance = accountTO.getDebitBalanceTotal();
			debitFlag = true;
		} else if (null == accountTO.getDebitBalanceTotal()
				&& null != accountTO.getCreditBalanceTotal()) {
			openingBalance = accountTO.getCreditBalanceTotal();
		}
		if (accountTO.getAccountTypeId() == 1
				|| accountTO.getAccountTypeId() == 4) {
			if (debitFlag) {
				accountTO.setDebitBalanceTotal(String.valueOf(openingBalance));
				accountTO.setCreditBalanceTotal(null);
			} else {
				accountTO.setCreditBalanceTotal(String.valueOf(openingBalance));
				accountTO.setDebitBalanceTotal(null);
			}
		} else {
			if (!debitFlag) {
				accountTO.setCreditBalanceTotal(String.valueOf(openingBalance));
				accountTO.setDebitBalanceTotal(null);
			} else {
				accountTO.setDebitBalanceTotal(String.valueOf(openingBalance));
				accountTO.setCreditBalanceTotal(null);
			}
		}
		return accountTO;
	}

	public LedgerVO processUpdateLedger(LedgerVO ledger,
			TransactionDetailVO transactionDetail) throws Exception {
		double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
				: ledger.getBalance();
		double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
				.getAmount()) : transactionDetail.getAmount();
		double calculationAmount = 0.0;
		if ((int) ledger.getCombination().getAccountByNaturalAccountId()
				.getAccountType() == (int) AccountType.Assets.getCode()
				|| (int) ledger.getCombination().getAccountByNaturalAccountId()
						.getAccountType() == (int) AccountType.Expenses
						.getCode()) {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		} else {
			if (ledgerBalance > 0) { // GL Credit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			} else { // GL Debit Side
				if (transactionAmount > 0) { // Transaction Credit Side
					calculationAmount = Math.abs(ledger.getBalance())
							- Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				} else { // Transaction Debit Side
					calculationAmount = Math.abs(ledger.getBalance())
							+ Math.abs(transactionDetail.getAmount());
					ledger.setBalance(Math.abs(calculationAmount));
					ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
							.getCode() : TransactionType.Credit.getCode()));
				}
			}
		}
		return ledger;
	}

	public TransactionVO processClosedYearTrialBalance(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = new TransactionVO();
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();

		double debitTotal = 0;
		double creditTotal = 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		for (TransactionDetail transactionDetail : transactionDetails) {
			transactionDetailTemp = new ArrayList<TransactionDetail>();
			key = transactionDetail.getCombination().getCombinationId();
			if (null != transactionMap && transactionMap.size() > 0
					&& transactionMap.containsKey(key)) {
				transactionDetailTemp.addAll(transactionMap.get(key));
			} else {
				if (null != transactionDetail.getCombination()
						.getAccountByAnalysisAccountId()) {
					naturalCombination = accountsEnterpriseService
							.getCombinationService()
							.getNaturalAccountCombination(
									transactionDetail.getCombination()
											.getAccountByCompanyAccountId()
											.getAccountId(),
									transactionDetail.getCombination()
											.getAccountByCostcenterAccountId()
											.getAccountId(),
									transactionDetail.getCombination()
											.getAccountByNaturalAccountId()
											.getAccountId());
					key = naturalCombination.getCombinationId();
					if (transactionMap.containsKey(key))
						transactionDetailTemp.addAll(transactionMap.get(key));
				}
			}
			transactionDetailTemp.add(transactionDetail);
			transactionMap.put(key, transactionDetailTemp);
		}
		LedgerVO ledgerVO = null;
		Map<Long, TransactionDetailVO> transactionDetailVOMap = null;
		for (Entry<Long, List<TransactionDetail>> entry : transactionMap
				.entrySet()) {
			transactionDetailVOMap = new HashMap<Long, TransactionDetailVO>();
			ledgerVO = new LedgerVO();
			ledgerVO.setSide(true);
			ledgerVO.setBalance(0d);
			ledgerVO.setCombination(entry.getValue().get(0).getCombination());
			ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
			transactionDetailVO = new TransactionDetailVO();
			transactionDetailVO.setAccountCode(ledgerVO.getCombination()
					.getAccountByCompanyAccountId().getCode()
					+ "."
					+ ledgerVO.getCombination()
							.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ ledgerVO.getCombination().getAccountByNaturalAccountId()
							.getCode());
			transactionDetailVO.setAccountDescription(ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getAccount());
			transactionDetailVO.setAccountSubType(null != ledgerVO
					.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail() ? ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getLookupDetail()
					.getDisplayName() : "");
			transactionDetailVO.setCostCenterAccountId(ledgerVO
					.getCombination().getAccountByCostcenterAccountId()
					.getAccountId());
			transactionDetailVO.setAccountType(ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			if (ledgerVO.getSide()) {
				transactionDetailVO.setDebitAmount(AIOSCommons
						.formatAmount(ledgerVO.getBalance()));
				debitTotal += AIOSCommons.formatAmountToDouble(ledgerVO
						.getBalance());
				ledgerVO.setBalance(debitTotal);
			} else {
				transactionDetailVO.setCreditAmount(AIOSCommons
						.formatAmount(ledgerVO.getBalance()));
				creditTotal += AIOSCommons.formatAmountToDouble(ledgerVO
						.getBalance());
				ledgerVO.setBalance(creditTotal);
			}
			transactionDetailVO.setLedgerSide(ledgerVO.getSide());
			transactionDetailVO.setIsDebit(ledgerVO.getSide());
			transactionDetailVO.setCombination(ledgerVO.getCombination());
			transactionDetailVO.setAmount(ledgerVO.getBalance());
			transactionDetailVOMap.put(ledgerVO.getCombination()
					.getCombinationId(), transactionDetailVO);
			transactionDetailVO
					.setTransactionDetailVOMap(transactionDetailVOMap);
			transactionDetailVOs.add(transactionDetailVO);
		}
		Collections.sort(transactionDetailVOs,
				new Comparator<TransactionDetailVO>() {
					public int compare(TransactionDetailVO o1,
							TransactionDetailVO o2) {
						return o1.getCostCenterAccountId().compareTo(
								o2.getCostCenterAccountId());
					}
				});
		Collections.sort(transactionDetailVOs,
				new Comparator<TransactionDetailVO>() {
					public int compare(TransactionDetailVO o1,
							TransactionDetailVO o2) {
						return o1.getAccountType().compareTo(
								o2.getAccountType());
					}
				});
		transactionVO.setTransactionDetailVOs(transactionDetailVOs);
		transactionVO.setDebitAmount(AIOSCommons.formatAmount(debitTotal));
		transactionVO.setCreditAmount(AIOSCommons.formatAmount(creditTotal));
		return transactionVO;
	}

	public TransactionVO processTrialBalance(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = new TransactionVO();
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();

		double debitTotal = 0;
		double creditTotal = 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		for (TransactionDetail transactionDetail : transactionDetails) {
			transactionDetailTemp = new ArrayList<TransactionDetail>();
			key = transactionDetail.getCombination().getCombinationId();
			if (null != transactionMap && transactionMap.size() > 0
					&& transactionMap.containsKey(key)) {
				transactionDetailTemp.addAll(transactionMap.get(key));
			} else {
				if (null != transactionDetail.getCombination()
						.getAccountByAnalysisAccountId()) {
					naturalCombination = accountsEnterpriseService
							.getCombinationService()
							.getNaturalAccountCombination(
									transactionDetail.getCombination()
											.getAccountByCompanyAccountId()
											.getAccountId(),
									transactionDetail.getCombination()
											.getAccountByCostcenterAccountId()
											.getAccountId(),
									transactionDetail.getCombination()
											.getAccountByNaturalAccountId()
											.getAccountId());
					key = naturalCombination.getCombinationId();
					if (transactionMap.containsKey(key))
						transactionDetailTemp.addAll(transactionMap.get(key));
				}
			}
			transactionDetailTemp.add(transactionDetail);
			transactionMap.put(key, transactionDetailTemp);
		}
		LedgerVO ledgerVO = null;
		for (Entry<Long, List<TransactionDetail>> entry : transactionMap
				.entrySet()) {
			ledgerVO = new LedgerVO();
			ledgerVO.setSide(true);
			ledgerVO.setBalance(0d);
			ledgerVO.setCombination(entry.getValue().get(0).getCombination());
			ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
			transactionDetailVO = new TransactionDetailVO();
			transactionDetailVO.setAccountCode(ledgerVO.getCombination()
					.getAccountByCompanyAccountId().getCode()
					+ "."
					+ ledgerVO.getCombination()
							.getAccountByCostcenterAccountId().getCode()
					+ "."
					+ ledgerVO.getCombination().getAccountByNaturalAccountId()
							.getCode());
			transactionDetailVO.setAccountDescription(ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getAccount());
			transactionDetailVO.setAccountSubType(null != ledgerVO
					.getCombination().getAccountByNaturalAccountId()
					.getLookupDetail() ? ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getLookupDetail()
					.getDisplayName() : "");
			transactionDetailVO.setCostCenterAccountId(ledgerVO
					.getCombination().getAccountByCostcenterAccountId()
					.getAccountId());
			transactionDetailVO.setAccountType(ledgerVO.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			if (ledgerVO.getSide()) {
				transactionDetailVO.setDebitAmount(AIOSCommons
						.formatAmount(ledgerVO.getBalance()));
				debitTotal += AIOSCommons.formatAmountToDouble(ledgerVO
						.getBalance());
			} else {
				transactionDetailVO.setCreditAmount(AIOSCommons
						.formatAmount(ledgerVO.getBalance()));
				creditTotal += AIOSCommons.formatAmountToDouble(ledgerVO
						.getBalance());
			}
			transactionDetailVOs.add(transactionDetailVO);
		}
		Collections.sort(transactionDetailVOs,
				new Comparator<TransactionDetailVO>() {
					public int compare(TransactionDetailVO o1,
							TransactionDetailVO o2) {
						return o1.getCostCenterAccountId().compareTo(
								o2.getCostCenterAccountId());
					}
				});
		Collections.sort(transactionDetailVOs,
				new Comparator<TransactionDetailVO>() {
					public int compare(TransactionDetailVO o1,
							TransactionDetailVO o2) {
						return o1.getAccountType().compareTo(
								o2.getAccountType());
					}
				});
		transactionVO.setTransactionDetailVOs(transactionDetailVOs);
		transactionVO.setDebitAmount(AIOSCommons.formatAmount(debitTotal));
		transactionVO.setCreditAmount(AIOSCommons.formatAmount(creditTotal));
		return transactionVO;
	}

	public TransactionVO processRevenueIncomeStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = null;
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		double revenueTotal = 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			transactionVO = new TransactionVO();
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				ledgerVO.setCombination(entry.getValue().get(0)
						.getCombination());
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				if (ledgerVO.getSide()) {
					revenueTotal -= ledgerVO.getBalance();
					transactionDetailVO.setRevenueAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
				} else {
					revenueTotal += ledgerVO.getBalance();
					transactionDetailVO.setRevenueAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				}
				transactionDetailVOs.add(transactionDetailVO);
			}
			Collections.sort(transactionDetailVOs,
					new Comparator<TransactionDetailVO>() {
						public int compare(TransactionDetailVO o1,
								TransactionDetailVO o2) {
							return o1.getCostCenterAccountId().compareTo(
									o2.getCostCenterAccountId());
						}
					});
			transactionVO.setTransactionDetailVOs(transactionDetailVOs);
			transactionVO.setRevenueAmount(String.valueOf(revenueTotal));
		}
		return transactionVO;
	}

	public TransactionVO processExpenseIncomeStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = null;
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		double expenseTotal = 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			transactionVO = new TransactionVO();
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				ledgerVO.setCombination(entry.getValue().get(0)
						.getCombination());
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				if (ledgerVO.getSide()) {
					expenseTotal += ledgerVO.getBalance();
					transactionDetailVO.setExpenseAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				} else {
					expenseTotal -= ledgerVO.getBalance();
					transactionDetailVO.setExpenseAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
				}
				transactionDetailVOs.add(transactionDetailVO);
			}
			Collections.sort(transactionDetailVOs,
					new Comparator<TransactionDetailVO>() {
						public int compare(TransactionDetailVO o1,
								TransactionDetailVO o2) {
							return o1.getCostCenterAccountId().compareTo(
									o2.getCostCenterAccountId());
						}
					});
			transactionVO.setTransactionDetailVOs(transactionDetailVOs);
			transactionVO.setExpenseAmount(String.valueOf(expenseTotal));
		}
		return transactionVO;
	}

	public TransactionVO processAssetStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = null;
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		double assetTotal = 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			transactionVO = new TransactionVO();
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				ledgerVO.setCombination(entry.getValue().get(0)
						.getCombination());
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				if (ledgerVO.getSide()) {
					assetTotal += ledgerVO.getBalance();
					transactionDetailVO.setAssetAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				} else {
					assetTotal -= ledgerVO.getBalance();
					transactionDetailVO.setAssetAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
				}
				transactionDetailVOs.add(transactionDetailVO);
			}
			Collections.sort(transactionDetailVOs,
					new Comparator<TransactionDetailVO>() {
						public int compare(TransactionDetailVO o1,
								TransactionDetailVO o2) {
							return o1.getCostCenterAccountId().compareTo(
									o2.getCostCenterAccountId());
						}
					});
			transactionVO.setTransactionDetailVOs(transactionDetailVOs);
			transactionVO.setAssetAmount(String.valueOf(assetTotal));
		}
		return transactionVO;
	}

	public TransactionVO processLiabilityStatement(
			List<TransactionDetail> transactionDetails,
			TransactionDetailVO retainEarning) throws Exception {
		TransactionVO transactionVO = null;
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		double liabilityTotal = null != retainEarning ? retainEarning
				.getRetainEarnings() : 0;
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			transactionVO = new TransactionVO();
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				ledgerVO.setCombination(entry.getValue().get(0)
						.getCombination());
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				if (ledgerVO.getSide()) {
					liabilityTotal -= ledgerVO.getBalance();
					transactionDetailVO.setLiabilityAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
				} else {
					liabilityTotal += ledgerVO.getBalance();
					transactionDetailVO.setLiabilityAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				}
				transactionDetailVOs.add(transactionDetailVO);
			}
			Collections.sort(transactionDetailVOs,
					new Comparator<TransactionDetailVO>() {
						public int compare(TransactionDetailVO o1,
								TransactionDetailVO o2) {
							return o1.getCostCenterAccountId().compareTo(
									o2.getCostCenterAccountId());
						}
					});
			transactionVO.setTransactionDetailVOs(transactionDetailVOs);
			transactionVO.setLiabilityAmount(String.valueOf(liabilityTotal));
		}
		return transactionVO;
	}

	public List<TransactionDetailVO> processAuditTrialStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			Combination combination = null;
			Map<String, List<TransactionDetailVO>> transactionDetailGroup = new HashMap<String, List<TransactionDetailVO>>();
			List<TransactionDetailVO> tempTransactionDetailVOs = null;
			LookupDetail accountSubTypeLookup = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				combination = accountsEnterpriseService.getCombinationService()
						.getCombinationAccounts(entry.getKey());
				ledgerVO.setCombination(combination);
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				transactionDetailVO.setCombination(combination);
				transactionDetailVO.setIsDebit(ledgerVO.getSide());
				if (ledgerVO.getSide())
					transactionDetailVO.setDebitAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				else
					transactionDetailVO.setCreditAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));

				if (null != ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					long subKey = ledgerVO.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId();
					accountSubTypeLookup = lookupMasterBL
							.getLookupMasterService().getLookupDetailById(
									subKey);
					tempTransactionDetailVOs = new ArrayList<TransactionDetailVO>();
					if (transactionDetailGroup.containsKey(accountSubTypeLookup
							.getDisplayName()))
						tempTransactionDetailVOs.addAll(transactionDetailGroup
								.get(accountSubTypeLookup.getDisplayName()));
					tempTransactionDetailVOs.add(transactionDetailVO);
					transactionDetailGroup.put(
							accountSubTypeLookup.getDisplayName(),
							tempTransactionDetailVOs);
				} else
					transactionDetailVOs.add(transactionDetailVO);
			}

			if (null != transactionDetailGroup
					&& transactionDetailGroup.size() > 0) {
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO
						.setTransactionDetailVOGroup(transactionDetailGroup);
				transactionDetailVOs.add(transactionDetailVO);
			}
		}
		return transactionDetailVOs;
	}

	public List<TransactionDetailVO> processAuditAssetStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			Combination combination = null;
			Map<String, List<TransactionDetailVO>> transactionDetailGroup = new HashMap<String, List<TransactionDetailVO>>();
			List<TransactionDetailVO> tempTransactionDetailVOs = null;
			LookupDetail accountSubTypeLookup = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				combination = accountsEnterpriseService.getCombinationService()
						.getCombinationAccounts(entry.getKey());
				ledgerVO.setCombination(combination);
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				transactionDetailVO.setCombination(combination);
				transactionDetailVO.setIsDebit(ledgerVO.getSide());
				if (ledgerVO.getSide())
					transactionDetailVO.setAssetAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				else
					transactionDetailVO.setAssetAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));

				if (null != ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					long subKey = ledgerVO.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId();
					accountSubTypeLookup = lookupMasterBL
							.getLookupMasterService().getLookupDetailById(
									subKey);
					tempTransactionDetailVOs = new ArrayList<TransactionDetailVO>();
					if (transactionDetailGroup.containsKey(accountSubTypeLookup
							.getDisplayName()))
						tempTransactionDetailVOs.addAll(transactionDetailGroup
								.get(accountSubTypeLookup.getDisplayName()));
					tempTransactionDetailVOs.add(transactionDetailVO);
					transactionDetailGroup.put(
							accountSubTypeLookup.getDisplayName(),
							tempTransactionDetailVOs);
				} else
					transactionDetailVOs.add(transactionDetailVO);
			}

			if (null != transactionDetailGroup
					&& transactionDetailGroup.size() > 0) {
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO
						.setTransactionDetailVOGroup(transactionDetailGroup);
				transactionDetailVOs.add(transactionDetailVO);
			}
		}
		return transactionDetailVOs;
	}

	public List<TransactionDetailVO> processAuditLiabilityStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			Map<String, List<TransactionDetailVO>> transactionDetailGroup = new HashMap<String, List<TransactionDetailVO>>();
			List<TransactionDetailVO> tempTransactionDetailVOs = null;
			LookupDetail accountSubTypeLookup = null;
			Combination combination = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				combination = accountsEnterpriseService.getCombinationService()
						.getCombinationAccounts(entry.getKey());
				ledgerVO.setCombination(combination);
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				transactionDetailVO.setCombination(combination);
				transactionDetailVO.setIsDebit(ledgerVO.getSide());
				if (ledgerVO.getSide())
					transactionDetailVO.setLiabilityAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));
				else
					transactionDetailVO.setLiabilityAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				if (null != ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					long subKey = ledgerVO.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId();
					accountSubTypeLookup = lookupMasterBL
							.getLookupMasterService().getLookupDetailById(
									subKey);
					tempTransactionDetailVOs = new ArrayList<TransactionDetailVO>();
					if (transactionDetailGroup.containsKey(accountSubTypeLookup
							.getDisplayName()))
						tempTransactionDetailVOs.addAll(transactionDetailGroup
								.get(accountSubTypeLookup.getDisplayName()));
					tempTransactionDetailVOs.add(transactionDetailVO);
					transactionDetailGroup.put(
							accountSubTypeLookup.getDisplayName(),
							tempTransactionDetailVOs);
				} else
					transactionDetailVOs.add(transactionDetailVO);
			}
			if (null != transactionDetailGroup
					&& transactionDetailGroup.size() > 0) {
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO
						.setTransactionDetailVOGroup(transactionDetailGroup);
				transactionDetailVOs.add(transactionDetailVO);
			}
		}
		return transactionDetailVOs;
	}

	public List<TransactionDetailVO> processAuditRevenueStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			Combination combination = null;
			Map<String, List<TransactionDetailVO>> transactionDetailGroup = new HashMap<String, List<TransactionDetailVO>>();
			List<TransactionDetailVO> tempTransactionDetailVOs = null;
			LookupDetail accountSubTypeLookup = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				combination = accountsEnterpriseService.getCombinationService()
						.getCombinationAccounts(entry.getKey());
				ledgerVO.setCombination(combination);
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				transactionDetailVO.setCombination(combination);
				transactionDetailVO.setIsDebit(ledgerVO.getSide());
				if (!ledgerVO.getSide())
					transactionDetailVO.setRevenueAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				else
					transactionDetailVO.setRevenueAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));

				if (null != ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					long subKey = ledgerVO.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId();
					accountSubTypeLookup = lookupMasterBL
							.getLookupMasterService().getLookupDetailById(
									subKey);
					tempTransactionDetailVOs = new ArrayList<TransactionDetailVO>();
					if (transactionDetailGroup.containsKey(accountSubTypeLookup
							.getDisplayName()))
						tempTransactionDetailVOs.addAll(transactionDetailGroup
								.get(accountSubTypeLookup.getDisplayName()));
					tempTransactionDetailVOs.add(transactionDetailVO);
					transactionDetailGroup.put(
							accountSubTypeLookup.getDisplayName(),
							tempTransactionDetailVOs);
				} else
					transactionDetailVOs.add(transactionDetailVO);
			}

			if (null != transactionDetailGroup
					&& transactionDetailGroup.size() > 0) {
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO
						.setTransactionDetailVOGroup(transactionDetailGroup);
				transactionDetailVOs.add(transactionDetailVO);
			}
		}
		return transactionDetailVOs;
	}

	public List<TransactionDetailVO> processAuditExpenseStatement(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
		List<TransactionDetail> transactionDetailTemp = null;
		Combination naturalCombination = null;
		long key = 0l;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				key = transactionDetail.getCombination().getCombinationId();
				if (null != transactionMap && transactionMap.size() > 0
						&& transactionMap.containsKey(key)) {
					transactionDetailTemp.addAll(transactionMap.get(key));
				} else {
					if (null != transactionDetail.getCombination()
							.getAccountByAnalysisAccountId()) {
						naturalCombination = accountsEnterpriseService
								.getCombinationService()
								.getNaturalAccountCombination(
										transactionDetail.getCombination()
												.getAccountByCompanyAccountId()
												.getAccountId(),
										transactionDetail
												.getCombination()
												.getAccountByCostcenterAccountId()
												.getAccountId(),
										transactionDetail.getCombination()
												.getAccountByNaturalAccountId()
												.getAccountId());
						key = naturalCombination.getCombinationId();
						if (transactionMap.containsKey(key))
							transactionDetailTemp.addAll(transactionMap
									.get(key));
					}
				}
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(key, transactionDetailTemp);
			}
			LedgerVO ledgerVO = null;
			Combination combination = null;
			Map<String, List<TransactionDetailVO>> transactionDetailGroup = new HashMap<String, List<TransactionDetailVO>>();
			List<TransactionDetailVO> tempTransactionDetailVOs = null;
			LookupDetail accountSubTypeLookup = null;
			for (Entry<Long, List<TransactionDetail>> entry : transactionMap
					.entrySet()) {
				ledgerVO = new LedgerVO();
				ledgerVO.setSide(true);
				ledgerVO.setBalance(0d);
				combination = accountsEnterpriseService.getCombinationService()
						.getCombinationAccounts(entry.getKey());
				ledgerVO.setCombination(combination);
				ledgerVO = this.processUpdateLedger(ledgerVO, entry.getValue());
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO.setAccountCode(ledgerVO.getCombination()
						.getAccountByCompanyAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByCostcenterAccountId().getCode()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getCode());
				transactionDetailVO.setAccountDescription(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccount()
						+ "."
						+ ledgerVO.getCombination()
								.getAccountByNaturalAccountId().getAccount());
				transactionDetailVO.setCostCenterAccountId(ledgerVO
						.getCombination().getAccountByCostcenterAccountId()
						.getAccountId());
				transactionDetailVO.setAccountType(ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getAccountType());
				transactionDetailVO.setAmount(ledgerVO.getBalance());
				transactionDetailVO.setCombination(combination);
				transactionDetailVO.setIsDebit(ledgerVO.getSide());
				if (ledgerVO.getSide())
					transactionDetailVO.setExpenseAmount(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				else
					transactionDetailVO.setExpenseAmount("-"
							+ AIOSCommons.formatAmount(ledgerVO.getBalance()));

				if (null != ledgerVO.getCombination()
						.getAccountByNaturalAccountId().getLookupDetail()) {
					long subKey = ledgerVO.getCombination()
							.getAccountByNaturalAccountId().getLookupDetail()
							.getLookupDetailId();
					accountSubTypeLookup = lookupMasterBL
							.getLookupMasterService().getLookupDetailById(
									subKey);
					tempTransactionDetailVOs = new ArrayList<TransactionDetailVO>();
					if (transactionDetailGroup.containsKey(accountSubTypeLookup
							.getDisplayName()))
						tempTransactionDetailVOs.addAll(transactionDetailGroup
								.get(accountSubTypeLookup.getDisplayName()));
					tempTransactionDetailVOs.add(transactionDetailVO);
					transactionDetailGroup.put(
							accountSubTypeLookup.getDisplayName(),
							tempTransactionDetailVOs);
				} else
					transactionDetailVOs.add(transactionDetailVO);
			}

			if (null != transactionDetailGroup
					&& transactionDetailGroup.size() > 0) {
				transactionDetailVO = new TransactionDetailVO();
				transactionDetailVO
						.setTransactionDetailVOGroup(transactionDetailGroup);
				transactionDetailVOs.add(transactionDetailVO);
			}
		}
		return transactionDetailVOs;
	}

	public LedgerVO processUpdateLedger(LedgerVO ledger,
			List<TransactionDetail> transactionDetails) throws Exception {
		for (TransactionDetail transactionDetail : transactionDetails) {
			double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
					: ledger.getBalance();
			double transactionAmount = transactionDetail.getIsDebit() ? (-1 * transactionDetail
					.getAmount()) : transactionDetail.getAmount();
			double calculationAmount = 0.0;
			if ((int) ledger.getCombination().getAccountByNaturalAccountId()
					.getAccountType() == (int) AccountType.Assets.getCode()
					|| (int) ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
							.getCode()) {
				if (ledgerBalance > 0) { // GL Credit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				} else { // GL Debit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				}
			} else {
				if (ledgerBalance > 0) { // GL Credit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				} else { // GL Debit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetail.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				}
			}
		}
		return ledger;
	}

	public LedgerVO processUpdateLedgerGroup(LedgerVO ledger,
			List<TransactionDetailVO> transactionDetailVOs) throws Exception {
		for (TransactionDetailVO transactionDetailVO : transactionDetailVOs) {
			double ledgerBalance = ledger.getSide() ? (-1 * ledger.getBalance())
					: ledger.getBalance();
			double transactionAmount = transactionDetailVO.getIsDebit() ? (-1 * transactionDetailVO
					.getAmount()) : transactionDetailVO.getAmount();
			double calculationAmount = 0.0;
			ledger.setCombination(transactionDetailVO.getCombination());
			if ((int) ledger.getCombination().getAccountByNaturalAccountId()
					.getAccountType() == (int) AccountType.Assets.getCode()
					|| (int) ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
							.getCode()) {
				if (ledgerBalance > 0) { // GL Credit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				} else { // GL Debit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				}
			} else {
				if (ledgerBalance > 0) { // GL Credit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount <= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				} else { // GL Debit Side
					if (transactionAmount > 0) { // Transaction Credit Side
						calculationAmount = Math.abs(ledger.getBalance())
								- Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					} else { // Transaction Debit Side
						calculationAmount = Math.abs(ledger.getBalance())
								+ Math.abs(transactionDetailVO.getAmount());
						ledger.setBalance(Math.abs(calculationAmount));
						ledger.setSide((calculationAmount >= 0 ? TransactionType.Debit
								.getCode() : TransactionType.Credit.getCode()));
					}
				}
			}
		}
		return ledger;
	}

	public List<DirectPayment> createDirectPaymentReport(
			Implementation implementation, AccountsTO accounts)
			throws Exception {
		String queryStr = "";
		if (implementation != null)
			queryStr += " WHERE dp.implementation.implementationId="
					+ implementation.getImplementationId();
		if (accounts.getRecordId() > 0)
			queryStr += " AND dp.directPaymentId =" + accounts.getRecordId();
		return this.getAccountsEnterpriseService().getDirectPaymentService()
				.getAllDirectPaymentDetail(queryStr);
	}

	/**
	 * Accounts Build Script
	 * 
	 * @return
	 */
	public void accountsBuildUpdateScript() {
		try {
			// Receive Notes Script
			try {
				List<Receive> receives = this.getAccountsEnterpriseService()
						.getReceiveService().getAllReceiveNotes();
				List<ReceiveDetail> receiveDetails = new ArrayList<ReceiveDetail>();
				Map<Long, Double> totalPurchases = new HashMap<Long, Double>();

				for (Receive receive : receives) {
					receive.setSupplier(receive.getSupplier());

					/*
					 * for (ReceiveDetail detail : receive.getReceiveDetails())
					 * { detail.setPurchase(receive.getPurchase()); }
					 */
					receiveDetails.addAll(receive.getReceiveDetails());
				}
				this.getAccountsEnterpriseService().getReceiveService()
						.saveReceiptsDetail(receives, receiveDetails);

				for (ReceiveDetail receiveDetail : receiveDetails) {
					double receiveQty = receiveDetail.getReceiveQty();
					double totalPurchase = 0;
					if (totalPurchases.containsKey(receiveDetail.getPurchase()
							.getPurchaseId())) {
						receiveQty += totalPurchases.get(receiveDetail
								.getPurchase().getPurchaseId());
					}
					for (PurchaseDetail purchaseDetail : receiveDetail
							.getPurchase().getPurchaseDetails()) {
						totalPurchase += purchaseDetail.getQuantity();
					}
					totalPurchases.put(receiveDetail.getPurchase()
							.getPurchaseId(), receiveQty);
					if (receiveQty < totalPurchase)
						receiveDetail.getPurchase().setStatus(1);
					else
						receiveDetail.getPurchase().setStatus(2);
				}
				System.out
						.println("----------------- Receive script completed ----------------------");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			// Invoice Script
			try {
				List<Invoice> invoices = this.getAccountsEnterpriseService()
						.getInvoiceService().getAllInvoices();
				List<InvoiceDetail> invoiceDetails = new ArrayList<InvoiceDetail>();
				Map<Long, Double> totalReceives = new HashMap<Long, Double>();
				for (Invoice invoice : invoices) {
					invoice.setSupplier(invoice.getReceive().getSupplier());
					invoice.setImplementation(invoice.getReceive()
							.getImplementation());
					for (InvoiceDetail invoiceDetail : invoice
							.getInvoiceDetails()) {
						for (ReceiveDetail receiveDetail : invoice.getReceive()
								.getReceiveDetails()) {
							if (invoiceDetail
									.getProduct()
									.getProductId()
									.equals(receiveDetail.getProduct()
											.getProductId())
									|| invoiceDetail.getProduct()
											.getProductId() == receiveDetail
											.getProduct().getProductId()) {
								invoiceDetail.setReceiveDetail(receiveDetail);
							}
						}
					}
					invoiceDetails.addAll(invoice.getInvoiceDetails());
				}
				this.getAccountsEnterpriseService().getInvoiceService()
						.saveInvoice(invoices, invoiceDetails);

				for (InvoiceDetail invoiceDetail : invoiceDetails) {
					double invoiceQty = invoiceDetail.getInvoiceQty();
					if (totalReceives.containsKey(invoiceDetail
							.getReceiveDetail().getReceiveDetailId())) {
						invoiceQty += totalReceives.get(invoiceDetail
								.getReceiveDetail().getReceiveDetailId());
					}
					totalReceives.put(invoiceDetail.getReceiveDetail()
							.getReceiveDetailId(), invoiceQty);
					if (invoiceQty < invoiceDetail.getReceiveDetail()
							.getReceiveQty())
						invoiceDetail.getReceiveDetail().getReceive()
								.setStatus((byte) 1);
					else

						invoiceDetail.getReceiveDetail().getReceive()
								.setStatus((byte) 2);
				}

				System.out
						.println("----------------- Invoice script completed ----------------------");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			// Payment Script
			try {
				List<Payment> payments = this.getAccountsEnterpriseService()
						.getPaymentService().getAllPayments();
				List<PaymentDetail> paymentDetails = new ArrayList<PaymentDetail>();
				List<InvoiceDetail> invoiceDetails = null;
				for (Payment payment : payments) {
					payment.setSupplier(payment.getReceive().getSupplier());
					invoiceDetails = new ArrayList<InvoiceDetail>();
					for (ReceiveDetail receiveDetail : payment.getReceive()
							.getReceiveDetails()) {
						invoiceDetails.addAll(new ArrayList<InvoiceDetail>(
								receiveDetail.getInvoiceDetails()));
					}
					for (PaymentDetail paymentDetail : payment
							.getPaymentDetails()) {
						for (InvoiceDetail invoiceDetail : invoiceDetails) {
							if (paymentDetail
									.getProduct()
									.getProductId()
									.equals(invoiceDetail.getProduct()
											.getProductId())
									|| paymentDetail.getProduct()
											.getProductId() == invoiceDetail
											.getProduct().getProductId()) {
								paymentDetail.setInvoiceDetail(invoiceDetail);
							}
						}
					}
					paymentDetails.addAll(payment.getPaymentDetails());
				}
				this.getAccountsEnterpriseService().getPaymentService()
						.savePayment(payments, paymentDetails);

				for (PaymentDetail paymentDetail : paymentDetails) {

					if (null != paymentDetail.getInvoiceDetail())
						paymentDetail.getInvoiceDetail().getInvoice()
								.setStatus((byte) 2);

				}

				System.out
						.println("----------------- Payment script completed ----------------------");

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Fetch ledger fiscal
	 * 
	 * @return ledgerFiscals
	 */
	public List<LedgerFiscal> getLedgerFiscalAgainstPeriod(long periodId) {
		try {
			List<LedgerFiscal> ledgerFiscals = accountsEnterpriseService
					.getCombinationService().getLedgerFiscals(periodId);
			return ledgerFiscals;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	// ------------------------------------------Added by rafiq for Dashboard
	// charts--------------------------------------------------
	public List<AccountsTO> getExpenseDetail(List<LedgerFiscal> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Expenses
							.getCode())) {
				AccountsTO accountTO = getExpenseDetailManipulation(list,
						accountList, hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getRevenueDetail(List<LedgerFiscal> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Revenue
							.getCode())) {
				AccountsTO accountTO = getRevenueDetailManipulation(list,
						accountList, hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getAssetDetail(List<LedgerFiscal> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Assets
							.getCode())) {
				AccountsTO accountTO = getAssetDetailManipulation(list,
						accountList, hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getOwnersEquityDetail(List<LedgerFiscal> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.OwnersEquity
							.getCode())) {
				AccountsTO accountTO = getLiabilityDetailManipulation(list,
						accountList, hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	public List<AccountsTO> getLiabilityDetail(List<LedgerFiscal> ledgerList)
			throws Exception {
		List<AccountsTO> accountList = new ArrayList<AccountsTO>();
		Map<Long, AccountsTO> hashAccount = new HashMap<Long, AccountsTO>();
		for (LedgerFiscal list : ledgerList) {
			if (null != list.getCombination().getAccountByNaturalAccountId()
					.getAccountType()
					&& ((int) list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.Liabilites
							.getCode() || list.getCombination()
							.getAccountByNaturalAccountId().getAccountType() == (int) AccountType.OwnersEquity
							.getCode())) {
				AccountsTO accountTO = getLiabilityDetailManipulation(list,
						accountList, hashAccount);
				if (null != accountTO) {
					accountList.add(accountTO);
					hashAccount.put(accountTO.getCombinationId(), accountTO);
				}
			}
		}
		Collections.sort(accountList, new Comparator<AccountsTO>() {
			@Override
			public int compare(AccountsTO at1, AccountsTO at2) {
				return at1.getCostCenterAccountId().compareTo(
						at2.getCostCenterAccountId());
			}
		});
		return accountList;
	}

	private AccountsTO getExpenseDetailManipulation(LedgerFiscal ledger,
			List<AccountsTO> expenseList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		Combination combinationAnalysis = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != expenseList
				&& !("").equals(expenseList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			AccountsTO accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledger.getSide()) {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						+ ledger.getBalance()));
			} else {
				accountTO.setExpenseBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getExpenseBalanceAmount())
						- ledger.getBalance()));
			}
			if (Double.parseDouble(accountTO.getExpenseBalanceAmount()) >= 0)
				accountTO.setLedgerSide(true);
			else
				accountTO.setLedgerSide(false);
			flag = false;
		}
		if (flag) {
			AccountsTO accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setExpenseAccountCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			if (ledger.getSide())
				accountTO.setExpenseBalanceAmount(ledger.getBalance()
						.toString());
			else
				accountTO.setExpenseBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			accountTO.setLedgerSide(ledger.getSide());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	private AccountsTO getRevenueDetailManipulation(LedgerFiscal ledger,
			List<AccountsTO> revenueList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		AccountsTO accountTO = null;
		Combination combinationAnalysis = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != revenueList
				&& !("").equals(revenueList)
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			accountTO = new AccountsTO();
			accountTO = hashAccount.get(combinationAnalysis.getCombinationId());
			if (ledger.getSide()) {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						- ledger.getBalance()));
			} else {
				accountTO.setRevenueBalanceAmount(String.valueOf(Double
						.parseDouble(accountTO.getRevenueBalanceAmount())
						+ ledger.getBalance()));
			}
			if (Double.parseDouble(accountTO.getRevenueBalanceAmount()) >= 0)
				accountTO.setLedgerSide(false);
			else
				accountTO.setLedgerSide(true);
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setRevenueAccountCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setLedgerSide(ledger.getSide());
			if (ledger.getSide())
				accountTO.setRevenueBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			else
				accountTO.setRevenueBalanceAmount(ledger.getBalance()
						.toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	private AccountsTO getAssetDetailManipulation(LedgerFiscal ledger,
			List<AccountsTO> assetsList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		AccountsTO accountTO = null;
		Combination combinationAnalysis = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != assetsList
				&& assetsList.size() > 0
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledger.getBalance() > 0) {
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (ledger.getSide()) {
					accountTO.setAssetBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getAssetBalanceAmount())
							+ ledger.getBalance()));
				} else {
					accountTO.setAssetBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getAssetBalanceAmount())
							- ledger.getBalance()));
				}
				if (Double.parseDouble(accountTO.getAssetBalanceAmount()) >= 0)
					accountTO.setLedgerSide(true);
				else
					accountTO.setLedgerSide(false);
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setAssetCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setLedgerSide(ledger.getSide());
			if (!ledger.getSide())
				accountTO.setAssetBalanceAmount("-".concat(ledger.getBalance()
						.toString()));
			else
				accountTO.setAssetBalanceAmount(ledger.getBalance().toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	private AccountsTO getLiabilityDetailManipulation(LedgerFiscal ledger,
			List<AccountsTO> liabilityList, Map<Long, AccountsTO> hashAccount)
			throws Exception {
		boolean flag = true;
		AccountsTO accountTO = null;
		Combination combinationAnalysis = null;
		if (null != ledger.getCombination().getAccountByAnalysisAccountId()
				&& !ledger.getCombination().getAccountByAnalysisAccountId()
						.equals("")) {
			combinationAnalysis = accountsEnterpriseService
					.getCombinationService().getNaturalAccountCombination(
							ledger.getCombination()
									.getAccountByCompanyAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByCostcenterAccountId()
									.getAccountId(),
							ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountId());
		} else {
			combinationAnalysis = ledger.getCombination();
		}
		if (null != liabilityList
				&& liabilityList.size() > 0
				&& hashAccount.containsKey(combinationAnalysis
						.getCombinationId())) {
			if (ledger.getBalance() > 0) {
				accountTO = new AccountsTO();
				accountTO = hashAccount.get(combinationAnalysis
						.getCombinationId());
				if (ledger.getSide()) {
					accountTO.setLiabilityBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getLiabilityBalanceAmount())
							- ledger.getBalance()));
				} else {
					accountTO.setLiabilityBalanceAmount(String.valueOf(Double
							.parseDouble(accountTO.getLiabilityBalanceAmount())
							+ ledger.getBalance()));
				}
				if (Double.parseDouble(accountTO.getLiabilityBalanceAmount()) >= 0)
					accountTO.setLedgerSide(false);
				else
					accountTO.setLedgerSide(true);
			}
			flag = false;
		}
		if (flag) {
			accountTO = new AccountsTO();
			accountTO.setCombinationId(combinationAnalysis.getCombinationId());
			accountTO.setLiabilityCode(ledger
					.getCombination()
					.getAccountByCostcenterAccountId()
					.getAccount()
					.concat(".")
					.concat(ledger.getCombination()
							.getAccountByNaturalAccountId().getAccount()));
			accountTO.setAccountTypeId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountType());
			accountTO.setLedgerSide(ledger.getSide());
			if (ledger.getSide() && ledger.getBalance() > 0)
				accountTO.setLiabilityBalanceAmount("-".concat(ledger
						.getBalance().toString()));
			else
				accountTO.setLiabilityBalanceAmount(ledger.getBalance()
						.toString());
			accountTO.setNaturalAccountId(ledger.getCombination()
					.getAccountByNaturalAccountId().getAccountId());
			accountTO.setCostCenterAccountId(ledger.getCombination()
					.getAccountByCostcenterAccountId().getAccountId());
			return accountTO;
		} else {
			return null;
		}
	}

	// -------------------------------------------Dashboard chart work ends
	// here-------------------------------------------

	public RequisitionVO processRequisitionCycle(Requisition requisition)
			throws Exception {

		RequisitionVO requisitionVO = new RequisitionVO();
		Map<Long, RequisitionDetailVO> requisitionDetailMap = new HashMap<Long, RequisitionDetailVO>();
		Map<Long, PurchaseVO> purchaseMap = new HashMap<Long, PurchaseVO>();
		Set<Long> receiveSet = new HashSet<Long>();
		RequisitionDetailVO requisitionDetailVO = null;
		double totalRequisitionQty = 0;
		for (RequisitionDetail requisitionDetail : requisition
				.getRequisitionDetails()) {
			requisitionDetailVO = new RequisitionDetailVO();
			requisitionDetailVO.setProductName(requisitionDetail.getProduct()
					.getProductName());
			requisitionDetailVO.setProductCode(requisitionDetail.getProduct()
					.getCode());
			requisitionDetailVO.setRequisitionQty(AIOSCommons
					.formatAmount(requisitionDetail.getQuantity()));
			totalRequisitionQty += requisitionDetail.getQuantity();
			requisitionDetailMap.put(requisitionDetail.getProduct()
					.getProductId(), requisitionDetailVO);
		}
		requisitionVO.setReferenceNumber(requisition.getReferenceNumber());
		requisitionVO.setDate(DateFormat.convertDateToString(requisition
				.getRequisitionDate().toString()));
		// Quotation
		double totalQuotationQty = 0;
		double totalPurchaseQty = 0;
		double totalReceiveQty = 0;
		double totalReturnQty = 0;
		double totalIssuedQty = 0;
		double totalIssueReturnQty = 0;
		if (null != requisition.getQuotations()
				&& requisition.getQuotations().size() > 0) {
			for (Quotation rfq : requisition.getQuotations()) {
				for (QuotationDetail rfqDt : rfq.getQuotationDetails()) {
					if (requisitionDetailMap.containsKey(rfqDt.getProduct()
							.getProductId())) {
						requisitionDetailVO = requisitionDetailMap.get(rfqDt
								.getProduct().getProductId());
						requisitionDetailVO
								.setQuotationQty(AIOSCommons
										.formatAmount(null != requisitionDetailVO
												.getQuotationQty() ? ((AIOSCommons
												.formatAmountToDouble(requisitionDetailVO
														.getQuotationQty()) + rfqDt
												.getTotalUnits()))
												: rfqDt.getTotalUnits()));
						totalQuotationQty += rfqDt.getTotalUnits();
					}
				}
				// Purchase
				if (null != rfq.getPurchases() && rfq.getPurchases().size() > 0) {
					RequisitionVO requisitionTempVO = getPurchaseRequisitions(
							new ArrayList<Purchase>(rfq.getPurchases()),
							requisitionDetailMap);
					purchaseMap.putAll(requisitionTempVO.getPurchaseMap());
					if (null != requisitionTempVO.getReceiveVOSet()
							&& requisitionTempVO.getReceiveVOSet().size() > 0)
						receiveSet.addAll(requisitionTempVO.getReceiveVOSet());
					requisitionDetailMap = requisitionTempVO
							.getRequisitionDetailMap();
					totalPurchaseQty += AIOSCommons
							.formatAmountToDouble(requisitionTempVO
									.getTotalPurchaseQty());
					totalReceiveQty += AIOSCommons
							.formatAmountToDouble((null != requisitionTempVO
									.getTotalReceiveQty() ? requisitionTempVO
									.getTotalReceiveQty() : "0"));
					totalReturnQty += AIOSCommons
							.formatAmountToDouble((null != requisitionTempVO
									.getTotalReturnQty() ? requisitionTempVO
									.getTotalReturnQty() : totalReturnQty));
				}
			}
		}
		// Purchase continuous
		if (null != requisition.getPurchases()
				&& requisition.getPurchases().size() > 0) {
			RequisitionVO requisitionTempVO = getPurchaseRequisitions(
					new ArrayList<Purchase>(requisition.getPurchases()),
					requisitionDetailMap);
			purchaseMap.putAll(requisitionTempVO.getPurchaseMap());
			if (null != requisitionTempVO.getReceiveVOSet()
					&& requisitionTempVO.getReceiveVOSet().size() > 0)
				receiveSet.addAll(requisitionTempVO.getReceiveVOSet());
			requisitionDetailMap = requisitionTempVO.getRequisitionDetailMap();
			totalPurchaseQty += AIOSCommons
					.formatAmountToDouble(requisitionTempVO
							.getTotalPurchaseQty());
			totalReceiveQty += AIOSCommons
					.formatAmountToDouble((null != requisitionTempVO
							.getTotalReceiveQty() ? requisitionTempVO
							.getTotalReceiveQty() : totalReceiveQty));
			totalReturnQty += AIOSCommons
					.formatAmountToDouble((null != requisitionTempVO
							.getTotalReturnQty() ? requisitionTempVO
							.getTotalReturnQty() : totalReturnQty));
		}

		if (null != requisition.getIssueRequistions()
				&& requisition.getIssueRequistions().size() > 0) {
			List<IssueRequistionDetailVO> issueRequistionVOs = new ArrayList<IssueRequistionDetailVO>();
			IssueRequistionDetailVO issueRequistionDetailVO = null;
			for (IssueRequistion issueReq : requisition.getIssueRequistions()) {
				for (IssueRequistionDetail rqDt : issueReq
						.getIssueRequistionDetails()) {
					issueRequistionDetailVO = new IssueRequistionDetailVO();
					requisitionDetailVO = requisitionDetailMap.get(rqDt
							.getProduct().getProductId());
					requisitionDetailVO.setIssuedQty(AIOSCommons
							.formatAmount(null != requisitionDetailVO
									.getIssuedQty() ? ((AIOSCommons
									.formatAmountToDouble(requisitionDetailVO
											.getIssuedQty()) + rqDt
									.getQuantity())) : rqDt.getQuantity()));
					BeanUtils.copyProperties(issueRequistionDetailVO, rqDt);
					issueRequistionDetailVO.setReferenceNumber(issueReq
							.getReferenceNumber());
					issueRequistionDetailVO.setPersonName(issueReq.getPerson()
							.getFirstName()
							+ " "
							+ issueReq.getPerson().getLastName());
					issueRequistionDetailVO.setIssueDate(DateFormat
							.convertDateToString(issueReq.getIssueDate()
									.toString()));
					issueRequistionDetailVO.setProductName(rqDt.getProduct()
							.getProductName());
					issueRequistionVOs.add(issueRequistionDetailVO);
					totalIssuedQty += rqDt.getQuantity();
					if (null != rqDt.getIssueReturnDetails()
							&& rqDt.getIssueReturnDetails().size() > 0) {
						for (IssueReturnDetail rtDt : rqDt
								.getIssueReturnDetails()) {
							requisitionDetailVO = requisitionDetailMap.get(rqDt
									.getProduct().getProductId());
							requisitionDetailVO
									.setIssueReturnQty(AIOSCommons
											.formatAmount(null != requisitionDetailVO
													.getIssueReturnQty() ? ((AIOSCommons
													.formatAmountToDouble(requisitionDetailVO
															.getIssueReturnQty()) + rtDt
													.getReturnQuantity()))
													: rtDt.getReturnQuantity()));
							totalIssueReturnQty += rtDt.getReturnQuantity();
						}
					}
				}
			}
			requisitionVO.setIssueRequistionDetailVOs(issueRequistionVOs);
		}

		requisitionVO.setTotalRequisitionQty(AIOSCommons
				.formatAmount(totalRequisitionQty));
		requisitionVO.setTotalQuotationQty(totalQuotationQty > 0 ? AIOSCommons
				.formatAmount(totalQuotationQty) : "-");
		requisitionVO.setTotalPurchaseQty(totalPurchaseQty > 0 ? AIOSCommons
				.formatAmount(totalPurchaseQty) : "-");
		requisitionVO.setTotalReceiveQty(totalReceiveQty > 0 ? AIOSCommons
				.formatAmount(totalReceiveQty) : "-");
		requisitionVO.setTotalReturnQty(totalReceiveQty > 0 ? AIOSCommons
				.formatAmount(totalReturnQty) : "-");
		requisitionVO.setTotalIssuedQty(totalIssuedQty > 0 ? AIOSCommons
				.formatAmount(totalIssuedQty) : "-");
		requisitionVO
				.setTotalIssueReturnQty(totalIssueReturnQty > 0 ? AIOSCommons
						.formatAmount(totalIssueReturnQty) : "-");
		requisitionVO
				.setRequisitionDetailVOs(new ArrayList<RequisitionDetailVO>(
						requisitionDetailMap.values()));
		double totalPendingQty = 0;
		double pendingQty = 0;
		for (RequisitionDetailVO detVO : requisitionVO
				.getRequisitionDetailVOs()) {
			pendingQty = AIOSCommons
					.formatAmountToDouble(AIOSCommons
							.formatAmountToDouble(detVO.getRequisitionQty())
							- ((null != detVO.getReceiveQty() ? AIOSCommons
									.formatAmountToDouble(detVO.getReceiveQty())
									: 0) + (null != detVO.getReturnQty() ? AIOSCommons
									.formatAmountToDouble(detVO.getReturnQty())
									: 0)));
			totalPendingQty += pendingQty;
			detVO.setBalanceQty(pendingQty > 0 ? AIOSCommons
					.formatAmount(pendingQty) : "-");
		}
		requisitionVO
				.setTotalReceivePendingQty(totalPendingQty > 0 ? AIOSCommons
						.formatAmount(totalPendingQty) : "-");
		if (null != purchaseMap && purchaseMap.size() > 0) {
			List<PurchaseDetailVO> purchaseDetailVOs = new ArrayList<PurchaseDetailVO>();
			PurchaseDetailVO purchaseDetailVO = null;
			for (Entry<Long, PurchaseVO> poVo : purchaseMap.entrySet()) {
				for (PurchaseDetail poDt : poVo.getValue().getPurchaseDetails()) {
					purchaseDetailVO = new PurchaseDetailVO();
					purchaseDetailVO.setReferenceNumber(poVo.getValue()
							.getPurchaseNumber());
					purchaseDetailVO.setProductName(poDt.getProduct()
							.getProductName());
					purchaseDetailVO.setUnitRate(poDt.getUnitRate());
					purchaseDetailVO.setQuantity(poDt.getQuantity());
					purchaseDetailVO.setPurchaseDate(DateFormat
							.convertDateToString(poVo.getValue().getDate()
									.toString()));
					if (null != poVo.getValue().getSupplier().getPerson())
						purchaseDetailVO.setSupplierName(poVo.getValue()
								.getSupplier().getPerson().getFirstName()
								+ " "
								+ poVo.getValue().getSupplier().getPerson()
										.getLastName());
					else
						purchaseDetailVO
								.setSupplierName(null != poVo.getValue()
										.getSupplier().getCmpDeptLocation() ? poVo
										.getValue().getSupplier()
										.getCmpDeptLocation().getCompany()
										.getCompanyName()
										: poVo.getValue().getSupplier()
												.getCompany().getCompanyName());
					purchaseDetailVOs.add(purchaseDetailVO);
				}
			}
			requisitionVO.setPurchaseDetailVOs(purchaseDetailVOs);
		}

		if (null != receiveSet && receiveSet.size() > 0) {
			List<ReceiveDetailVO> receiveDetailVOs = new ArrayList<ReceiveDetailVO>();
			ReceiveDetailVO receiveDetailVO = null;
			List<ReceiveDetail> receiveDetails = accountsEnterpriseService
					.getReceiveService()
					.getReceiveDetailByReceiveId(receiveSet);
			for (ReceiveDetail rcDt : receiveDetails) {
				receiveDetailVO = new ReceiveDetailVO();
				receiveDetailVO.setReferenceNumber(rcDt.getReceive()
						.getReceiveNumber());
				receiveDetailVO.setProductName(rcDt.getProduct()
						.getProductName());
				receiveDetailVO.setUnitRate(rcDt.getUnitRate());
				receiveDetailVO.setPurchaseNumber(rcDt.getPurchase()
						.getPurchaseNumber());
				receiveDetailVO.setReceiveQty(rcDt.getReceiveQty());
				receiveDetailVO.setReceiveDate(DateFormat
						.convertDateToString(rcDt.getReceive().getReceiveDate()
								.toString()));
				if (null != rcDt.getReceive().getSupplier().getPerson())
					receiveDetailVO.setSupplierName(rcDt.getReceive()
							.getSupplier().getPerson().getFirstName()
							+ " "
							+ rcDt.getReceive().getSupplier().getPerson()
									.getLastName());
				else
					receiveDetailVO.setSupplierName(null != rcDt.getReceive()
							.getSupplier().getCmpDeptLocation() ? rcDt
							.getReceive().getSupplier().getCmpDeptLocation()
							.getCompany().getCompanyName() : rcDt.getReceive()
							.getSupplier().getCompany().getCompanyName());
				receiveDetailVOs.add(receiveDetailVO);

			}
			requisitionVO.setReceiveDetailVOs(receiveDetailVOs);
		}

		return requisitionVO;
	}

	private RequisitionVO getPurchaseRequisitions(List<Purchase> purchases,
			Map<Long, RequisitionDetailVO> requisitionDetailMap)
			throws Exception {
		double totalPurchaseQty = 0;
		double totalReceiveQty = 0;
		double totalReturnQty = 0;
		RequisitionDetailVO requisitionDetailVO = null;
		RequisitionVO tempRequistion = null;
		RequisitionVO requisitionVO = new RequisitionVO();
		Map<Long, PurchaseVO> purchaseMap = new HashMap<Long, PurchaseVO>();
		Set<Long> receiveSet = new HashSet<Long>();
		PurchaseVO purchaseVO = null;
		for (Purchase lpo : purchases) {
			for (PurchaseDetail lpoDt : lpo.getPurchaseDetails()) {
				if (requisitionDetailMap.containsKey(lpoDt.getProduct()
						.getProductId())) {
					requisitionDetailVO = requisitionDetailMap.get(lpoDt
							.getProduct().getProductId());
					requisitionDetailVO.setPurchaseQty(AIOSCommons
							.formatAmount(null != requisitionDetailVO
									.getPurchaseQty() ? ((AIOSCommons
									.formatAmountToDouble(requisitionDetailVO
											.getPurchaseQty()) + lpoDt
									.getQuantity())) : lpoDt.getQuantity()));
					totalPurchaseQty += lpoDt.getQuantity();
					purchaseVO = new PurchaseVO();
					BeanUtils.copyProperties(purchaseVO, lpo);
					purchaseMap.put(lpo.getPurchaseId(), purchaseVO);
				}
			}
			if (null != lpo.getReceiveDetails()
					&& lpo.getReceiveDetails().size() > 0) {
				tempRequistion = getReceivePurchase(
						new ArrayList<ReceiveDetail>(lpo.getReceiveDetails()),
						requisitionDetailMap);
				receiveSet.addAll(tempRequistion.getReceiveVOSet());
				requisitionDetailMap = tempRequistion.getRequisitionDetailMap();
				totalReceiveQty += AIOSCommons
						.formatAmountToDouble(tempRequistion
								.getTotalReceiveQty());
				totalReturnQty += AIOSCommons
						.formatAmountToDouble(tempRequistion
								.getTotalReturnQty());
			}
		}
		requisitionVO.setTotalPurchaseQty(AIOSCommons
				.formatAmount(totalPurchaseQty));
		requisitionVO.setTotalReceiveQty(AIOSCommons
				.formatAmount(totalReceiveQty));
		requisitionVO.setTotalReturnQty(AIOSCommons
				.formatAmount(totalReturnQty));
		requisitionVO.setPurchaseMap(purchaseMap);
		requisitionVO.setReceiveVOSet(receiveSet);
		requisitionVO.setRequisitionDetailMap(requisitionDetailMap);
		return requisitionVO;
	}

	private RequisitionVO getReceivePurchase(
			List<ReceiveDetail> receiveDetails,
			Map<Long, RequisitionDetailVO> requisitionDetailMap)
			throws Exception {
		RequisitionVO requisitionVO = new RequisitionVO();
		Set<Long> receiveSet = new HashSet<Long>();
		RequisitionVO tempRequistion = null;
		RequisitionDetailVO requisitionDetailVO = null;
		double totalReceiveQty = 0;
		double totalReturnQty = 0;
		Map<Long, GoodsReturnDetail> returnMap = new HashMap<Long, GoodsReturnDetail>();
		for (ReceiveDetail rcvDt : receiveDetails) {
			if (requisitionDetailMap.containsKey(rcvDt.getProduct()
					.getProductId())) {
				requisitionDetailVO = requisitionDetailMap.get(rcvDt
						.getProduct().getProductId());
				requisitionDetailVO.setReceiveQty(AIOSCommons
						.formatAmount(null != requisitionDetailVO
								.getReceiveQty() ? ((AIOSCommons
								.formatAmountToDouble(requisitionDetailVO
										.getReceiveQty()) + rcvDt
								.getReceiveQty())) : rcvDt.getReceiveQty()));
				totalReceiveQty += rcvDt.getReceiveQty();
				receiveSet.add(rcvDt.getReceive().getReceiveId());
			}

			if (null != rcvDt.getReceive().getGoodsReturns()
					&& rcvDt.getReceive().getGoodsReturns().size() > 0) {
				for (GoodsReturn grn : rcvDt.getReceive().getGoodsReturns()) {
					for (GoodsReturnDetail grdDt : grn.getGoodsReturnDetails())
						returnMap.put(grdDt.getReturnDetailId(), grdDt);
				}
			}
		}
		if (null != returnMap && returnMap.size() > 0) {
			tempRequistion = getGoodsReturn(new ArrayList<GoodsReturnDetail>(
					returnMap.values()), requisitionDetailMap);
			requisitionDetailMap = tempRequistion.getRequisitionDetailMap();
			totalReturnQty += AIOSCommons.formatAmountToDouble(tempRequistion
					.getTotalReturnQty());
		}
		requisitionVO.setRequisitionDetailMap(requisitionDetailMap);
		requisitionVO.setTotalReceiveQty(AIOSCommons
				.formatAmount(totalReceiveQty));
		requisitionVO.setTotalReturnQty(AIOSCommons
				.formatAmount(totalReturnQty));
		requisitionVO.setReceiveVOSet(receiveSet);
		return requisitionVO;
	}

	private RequisitionVO getGoodsReturn(
			List<GoodsReturnDetail> goodsReturnDetails,
			Map<Long, RequisitionDetailVO> requisitionDetailMap) {
		double totalReturnQty = 0;
		RequisitionVO requisitionVO = new RequisitionVO();
		RequisitionDetailVO requisitionDetailVO = null;
		for (GoodsReturnDetail grd : goodsReturnDetails) {
			if (requisitionDetailMap.containsKey(grd.getProduct()
					.getProductId())) {
				requisitionDetailVO = requisitionDetailMap.get(grd.getProduct()
						.getProductId());
				requisitionDetailVO.setReturnQty(AIOSCommons
						.formatAmount(null != requisitionDetailVO
								.getReturnQty() ? ((AIOSCommons
								.formatAmountToDouble(requisitionDetailVO
										.getReturnQty()) + grd.getReturnQty()))
								: grd.getReturnQty()));
				totalReturnQty += grd.getReturnQty();
			}
		}
		requisitionVO.setRequisitionDetailMap(requisitionDetailMap);
		requisitionVO.setTotalReturnQty(AIOSCommons
				.formatAmount(totalReturnQty));
		return requisitionVO;
	}

	// Getters and setters
	public AccountsEnterpriseService getAccountsEnterpriseService() {
		return accountsEnterpriseService;
	}

	public void setAccountsEnterpriseService(
			AccountsEnterpriseService accountsEnterpriseService) {
		this.accountsEnterpriseService = accountsEnterpriseService;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public BankBL getBankBL() {
		return bankBL;
	}

	public void setBankBL(BankBL bankBL) {
		this.bankBL = bankBL;
	}

	public CompanyBL getCompanyBL() {
		return companyBL;
	}

	public void setCompanyBL(CompanyBL companyBL) {
		this.companyBL = companyBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public StockBL getStockBL() {
		return stockBL;
	}

	public void setStockBL(StockBL stockBL) {
		this.stockBL = stockBL;
	}

	public CustomerBL getCustomerBL() {
		return customerBL;
	}

	public void setCustomerBL(CustomerBL customerBL) {
		this.customerBL = customerBL;
	}

	public ProjectMileStoneBL getProjectMileStoneBL() {
		return projectMileStoneBL;
	}

	public void setProjectMileStoneBL(ProjectMileStoneBL projectMileStoneBL) {
		this.projectMileStoneBL = projectMileStoneBL;
	}

	public InvoiceBL getInvoiceBL() {
		return invoiceBL;
	}

	public void setInvoiceBL(InvoiceBL invoiceBL) {
		this.invoiceBL = invoiceBL;
	}

	public SalesInvoiceBL getSalesInvoiceBL() {
		return salesInvoiceBL;
	}

	public void setSalesInvoiceBL(SalesInvoiceBL salesInvoiceBL) {
		this.salesInvoiceBL = salesInvoiceBL;
	}

	public SupplierBL getSupplierBL() {
		return supplierBL;
	}

	public void setSupplierBL(SupplierBL supplierBL) {
		this.supplierBL = supplierBL;
	}

	public CreditBL getCreditBL() {
		return creditBL;
	}

	public void setCreditBL(CreditBL creditBL) {
		this.creditBL = creditBL;
	}

	public ProjectBL getProjectBL() {
		return projectBL;
	}

	public void setProjectBL(ProjectBL projectBL) {
		this.projectBL = projectBL;
	}

	public PointOfSaleBL getPointOfSaleBL() {
		return pointOfSaleBL;
	}

	public void setPointOfSaleBL(PointOfSaleBL pointOfSaleBL) {
		this.pointOfSaleBL = pointOfSaleBL;
	}

	public AccountsJobBL getAccountsJobBL() {
		return accountsJobBL;
	}

	public void setAccountsJobBL(AccountsJobBL accountsJobBL) {
		this.accountsJobBL = accountsJobBL;
	}

	public AccountGroupBL getAccountGroupBL() {
		return accountGroupBL;
	}

	public void setAccountGroupBL(AccountGroupBL accountGroupBL) {
		this.accountGroupBL = accountGroupBL;
	}

	public LocationBL getLocationBL() {
		return locationBL;
	}

	public void setLocationBL(LocationBL locationBL) {
		this.locationBL = locationBL;
	}

	public RequisitionBL getRequisitionBL() {
		return requisitionBL;
	}

	public void setRequisitionBL(RequisitionBL requisitionBL) {
		this.requisitionBL = requisitionBL;
	}

	public PettyCashBL getPettyCashBL() {
		return pettyCashBL;
	}

	@Autowired
	public void setPettyCashBL(PettyCashBL pettyCashBL) {
		this.pettyCashBL = pettyCashBL;
	}

	public IssueRequistionBL getIssueRequistionBL() {
		return issueRequistionBL;
	}

	public void setIssueRequistionBL(IssueRequistionBL issueRequistionBL) {
		this.issueRequistionBL = issueRequistionBL;
	}

	public DepartmentBL getDepartmentBL() {
		return departmentBL;
	}

	public void setDepartmentBL(DepartmentBL departmentBL) {
		this.departmentBL = departmentBL;
	}

	public PurchaseBL getPurchaseBL() {
		return purchaseBL;
	}

	public void setPurchaseBL(PurchaseBL purchaseBL) {
		this.purchaseBL = purchaseBL;
	}
}
