package com.aiotech.aios.reporting.service.bl;

import com.aiotech.aios.accounts.service.bl.AccountBL;
import com.aiotech.aios.accounts.service.bl.AssetDepreciationBL;
import com.aiotech.aios.accounts.service.bl.AssetInsurancePremiumBL;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.accounts.service.bl.BankReconciliationBL;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.LedgerFiscalBL;
import com.aiotech.aios.accounts.service.bl.ProductPricingBL;
import com.aiotech.aios.accounts.service.bl.ReceiveBL;
import com.aiotech.aios.accounts.service.bl.SalesUpdateJob;
import com.aiotech.aios.accounts.service.bl.VoidPaymentBL;
import com.aiotech.aios.project.service.bl.ProjectBL;

public class AccountsJobBL{

	private AssetDepreciationBL assetDepreciationBL;
	private AssetInsurancePremiumBL assetInsurancePremiumBL; 
	private LedgerFiscalBL ledgerFiscalBL;
	private ProductPricingBL  productPricingBL;
	private ReceiveBL receiveBL;
	private SalesUpdateJob salesUpdateJob;
	private ProjectBL projectBL;
	private DirectPaymentBL directPaymentBL;
	private BankReceiptsBL bankReceiptsBL;
	private BankReconciliationBL bankReconciliationBL;
	private AccountBL accountBL;
	private VoidPaymentBL voidPaymentBL;
	
	// Getters & Setters
	public AssetDepreciationBL getAssetDepreciationBL() {
		return assetDepreciationBL;
	}

	public void setAssetDepreciationBL(AssetDepreciationBL assetDepreciationBL) {
		this.assetDepreciationBL = assetDepreciationBL;
	}

	public AssetInsurancePremiumBL getAssetInsurancePremiumBL() {
		return assetInsurancePremiumBL;
	}

	public void setAssetInsurancePremiumBL(
			AssetInsurancePremiumBL assetInsurancePremiumBL) {
		this.assetInsurancePremiumBL = assetInsurancePremiumBL;
	}

	public LedgerFiscalBL getLedgerFiscalBL() {
		return ledgerFiscalBL;
	}

	public void setLedgerFiscalBL(LedgerFiscalBL ledgerFiscalBL) {
		this.ledgerFiscalBL = ledgerFiscalBL;
	}


	public ProductPricingBL getProductPricingBL() {
		return productPricingBL;
	}

	public void setProductPricingBL(ProductPricingBL productPricingBL) {
		this.productPricingBL = productPricingBL;
	}

	public SalesUpdateJob getSalesUpdateJob() {
		return salesUpdateJob;
	}

	public void setSalesUpdateJob(SalesUpdateJob salesUpdateJob) {
		this.salesUpdateJob = salesUpdateJob;
	}

	public ProjectBL getProjectBL() {
		return projectBL;
	}

	public void setProjectBL(ProjectBL projectBL) {
		this.projectBL = projectBL;
	}

	public ReceiveBL getReceiveBL() {
		return receiveBL;
	}

	public void setReceiveBL(ReceiveBL receiveBL) {
		this.receiveBL = receiveBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public BankReconciliationBL getBankReconciliationBL() {
		return bankReconciliationBL;
	}

	public void setBankReconciliationBL(BankReconciliationBL bankReconciliationBL) {
		this.bankReconciliationBL = bankReconciliationBL;
	}

	public AccountBL getAccountBL() {
		return accountBL;
	}

	public void setAccountBL(AccountBL accountBL) {
		this.accountBL = accountBL;
	}

	public VoidPaymentBL getVoidPaymentBL() {
		return voidPaymentBL;
	}

	public void setVoidPaymentBL(VoidPaymentBL voidPaymentBL) {
		this.voidPaymentBL = voidPaymentBL;
	} 
}
