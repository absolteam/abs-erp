package com.aiotech.aios.reporting.service.bl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.HrEnterpriseService;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.realestate.domain.entity.Asset;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Maintenance;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.UnitDetail;
import com.aiotech.aios.realestate.domain.entity.vo.UnitDetailVO;
import com.aiotech.aios.realestate.service.RealEstateEnterpriseService;
import com.aiotech.aios.realestate.service.bl.ContractAgreementBL;
import com.aiotech.aios.realestate.service.bl.PropertyInformationBL;
import com.aiotech.aios.realestate.service.bl.UnitBL;
import com.aiotech.aios.realestate.to.CompanyVO;
import com.aiotech.aios.realestate.to.ContractReleaseTO;
import com.aiotech.aios.realestate.to.OfferManagementTO;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;
import com.aiotech.aios.realestate.to.PropertyOwnershipVO;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.RePropertyUnitsTO;
import com.aiotech.aios.realestate.to.converter.ContractReleaseTOConverter;
import com.aiotech.aios.realestate.to.converter.OfferTOConverter;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.realestate.to.converter.RePropertyUnitsTOConverter;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.ImageVO;
import com.opensymphony.xwork2.ActionContext;

public class RealEstateBL {

	RealEstateEnterpriseService realEstateEnterpriseService;
	HrEnterpriseService hrEnterpriseService;
	ImageBL imageBL;
	private ContractAgreementBL contractAgreementBL;
	private PropertyInformationBL propertyInfoBL;
	private UnitBL unitBL;
	private LookupMasterBL lookupMasterBL;
	private SystemService systemService;
	
	public void realEstateBuildUpdateScript(){
		try{
			List<Implementation> implementations=new ArrayList<Implementation>();
			implementations=systemService.getAllImplementation();
			for (Implementation implem : implementations) {
				
				//Property Detail
				List<LookupMaster> lookupMasters=lookupMasterBL.getLookupMasterService().getLookupInformation("PROPERTY_FEATURE", implem);
				Map<String,LookupDetail> lookupDetails=new HashMap<String,LookupDetail>();
				if(lookupMasters!=null && lookupMasters.size()>0){
					for(LookupDetail lookupDetai:lookupMasters.get(0).getLookupDetails()){
						lookupDetails.put(lookupDetai.getDisplayName(), lookupDetai);
					}
				}
				List<PropertyDetail> propertyDetails=propertyInfoBL.getPropertyService().getAllPropertyDetail(implem);
				if(propertyDetails!=null && propertyDetails.size()>0){
					for(PropertyDetail propertyDetail:propertyDetails){
						if(propertyDetail.getLookupDetail()==null || propertyDetail.getLookupDetail().getLookupDetailId()==0){
							String[] featureAndDetail = propertyDetail.getFeature().split("#");
							if(lookupDetails.get(featureAndDetail[0])!=null){
								propertyDetail.setLookupDetail(lookupDetails.get(featureAndDetail[0]));
								if(featureAndDetail.length>1)
									propertyDetail.setFeature(featureAndDetail[1]);
								else
									propertyDetail.setFeature("");
							}
						}
					}
				}
				
				
				//Property Component Detail Update
				List<LookupMaster> lookupMastersComp=lookupMasterBL.getLookupMasterService().getLookupInformation("COMPONENT_FEATURE", implem);
				Map<String,LookupDetail> lookupDetailsComp=new HashMap<String,LookupDetail>();
				if(lookupMastersComp!=null && lookupMastersComp.size()>0){
					for(LookupDetail lookupDetai:lookupMastersComp.get(0).getLookupDetails()){
						lookupDetailsComp.put(lookupDetai.getAccessCode(), lookupDetai);
					}
				}
				List<ComponentDetail> componentDetails=propertyInfoBL.getComponentService().getAllComponentDetail(implem);
				if (componentDetails != null && componentDetails.size() > 0) {
					for (ComponentDetail componentDetail : componentDetails) {
						if ((componentDetail.getLookupDetail() == null || componentDetail
								.getLookupDetail().getLookupDetailId() == 0)
								&& componentDetail.getFeature().split("#").length > 0) {
							String[] featureAndDetail = componentDetail
									.getFeature().split("#");
							if (lookupDetailsComp.get(featureAndDetail[0]) != null) {
								componentDetail
										.setLookupDetail(lookupDetailsComp
												.get(featureAndDetail[0]));
								if (featureAndDetail.length > 2)
									componentDetail
											.setFeature(featureAndDetail[2]);
								else
									componentDetail.setFeature("");
							}
						}
					}
				}
				
				
				//Property Unit Detail Update 
				
				List<LookupMaster> lookupMastersUnit=lookupMasterBL.getLookupMasterService().getLookupInformation("UNIT_FEATURE", implem);
				Map<String,LookupDetail> lookupDetailsUnit=new HashMap<String,LookupDetail>();
				if(lookupMastersUnit!=null && lookupMastersUnit.size()>0){
					for(LookupDetail lookupDetai:lookupMastersUnit.get(0).getLookupDetails()){
						lookupDetailsUnit.put(lookupDetai.getAccessCode(), lookupDetai);
					}
				}
				List<UnitDetail> unitDetails=unitBL.getUnitService().getAllUnitDetail(implem);
				if(unitDetails!=null && unitDetails.size()>0){
					for(UnitDetail unitDetail:unitDetails){
						if(unitDetail.getLookupDetail()==null || unitDetail.getLookupDetail().getLookupDetailId()==0){
							String[] featureAndDetail = unitDetail.getDetails().split("#");
							if(lookupDetailsUnit.get(featureAndDetail[0])!=null){
								unitDetail.setLookupDetail(lookupDetailsUnit.get(featureAndDetail[0]));
								if(featureAndDetail.length>2)
									unitDetail.setDetails(featureAndDetail[2]);
								else
									unitDetail.setDetails("");
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	
	public Property getPropertyDetail(Property property) throws Exception {
		Property prop = realEstateEnterpriseService.getPropertyService()
				.getPropertyById(property.getPropertyId());
		PropertyOwnership ownership = new PropertyOwnership();
		ownership.setProperty(prop);
		Set<PropertyOwnership> propertyOwners = new HashSet<PropertyOwnership>(
				realEstateEnterpriseService.getPropertyService()
						.getPropertyOwners(ownership));
		prop.setPropertyOwnerships(propertyOwners);
		Maintenance maintenance = new Maintenance();
		maintenance.setProperty(property);
		Set<Maintenance> propMaintenance = new HashSet<Maintenance>(
				realEstateEnterpriseService.getPropertyService()
						.getMaintenance(property));
		prop.setMaintenances(propMaintenance);
		prop.setPropertyDetails(new HashSet<PropertyDetail>(
				realEstateEnterpriseService.getPropertyService()
						.getPropertyDetails(property)));
		return prop;
	}

	public List<PropertyOwnershipVO> getOwnerPersons(
			Set<PropertyOwnership> ownerships) throws Exception {
		List<PropertyOwnershipVO> persons = new ArrayList<PropertyOwnershipVO>();
		for (PropertyOwnership ownership : ownerships) {
			PropertyOwnershipVO vo = new PropertyOwnershipVO();
			Person person = new Person();
			person.setPersonId(ownership.getPersonalId());
			Person p = hrEnterpriseService.getPersonService()
					.getSelectedPersons(person);
			if (p != null) {
				vo.setOwnerName(p.getFirstName() + " " + p.getLastName());
				vo.setDetails(ownership.getDetails());
				persons.add(vo);
			}
		}
		return persons;
	}

	public List<CompanyVO> getMaintenanceCompanies(
			Set<Maintenance> maintenanceComps) {
		List<CompanyVO> companyList = new ArrayList<CompanyVO>();
		for (Maintenance maintenanceComp : maintenanceComps) {
			CompanyVO vo = new CompanyVO();
			Company company = hrEnterpriseService.getCompanyService()
					.getCompanyById(maintenanceComp.getCompanyId());
			if (company == null)
				continue;
			vo.setCompanyName(company.getCompanyName());
		/*	vo.setCompanyType(company.getCompanyType() == 1 ? "Maintenance"
					: "Owner");*/
			vo.setCompanyDesc(company.getDescription());
			vo.setDetails(maintenanceComp.getDetails());
			company.getCompanyId();
			companyList.add(vo);
		}
		return companyList;
	}

	public List<ImageVO> populatePropertyDetailImgs(Property property)
			throws IOException, DataFormatException {
		Set<PropertyDetail> propDetails = property.getPropertyDetails();
		List<ImageVO> imgList = new ArrayList<ImageVO>();
		for (PropertyDetail detail : propDetails) {
			Image img = new Image();
			img.setRecordId(detail.getPropertyDetailId());
			img.setTableName("PropertyDetail");
			imgList.addAll(getImageBL().retrieveCopiedImages(img));
		}
		return imgList;
	}

	public String getPropertyAddress(Property prop) {
		StringBuilder address = new StringBuilder();
		address.append(realEstateEnterpriseService.getPropertyService()
				.getCityById((long) prop.getCity()).getCityName());
		address.append(" , ");
		address.append(realEstateEnterpriseService.getPropertyService()
				.getStateById((long) prop.getState()).getStateName());
		address.append(" , ");
		address.append(realEstateEnterpriseService.getPropertyService()
				.getCounryById((long) prop.getCountry()).getCountryName());
		return address.toString();
	}

	// Component
	public PropertyCompomentTO getComponentDetails(Component comp)
			throws Exception {
		Component component = getRealEstateEnterpriseService()
				.getComponentService().getComponentById(
						Long.parseLong(comp.getComponentId() + ""));
		Property property = getRealEstateEnterpriseService()
				.getComponentService().getPropertyById(
						component.getProperty().getPropertyId());
		ComponentType componentType = getRealEstateEnterpriseService()
				.getComponentService().getComponentTypeById(
						component.getComponentType().getComponentTypeId());
		component.setProperty(property);
		component.setComponentType(componentType);
		List<ComponentDetail> componentDetailList = getRealEstateEnterpriseService()
				.getComponentService().getComponentLines(
						Long.parseLong("" + comp.getComponentId()));

		component.setComponentDetails(new HashSet<ComponentDetail>(
				componentDetailList));
		PropertyCompomentTO to = new PropertyCompomentTO();
		to.setComponent(component);
		to.setComponentDetails(componentDetailList);

		return to;
	}

	// Unit
	public List<ImageVO> populateUnitDetailImgs(List<UnitDetailVO> uDetails)
			throws IOException, DataFormatException {
		List<ImageVO> imgList = new ArrayList<ImageVO>();
		for (UnitDetailVO detail : uDetails) {
			Image img = new Image();
			img.setRecordId(detail.getUnitDetailId());
			img.setTableName("UnitDetail");
			imgList.addAll(imageBL.retrieveCopiedImages(img));
		}
		return imgList;
	}

	public RePropertyUnitsTO getUnitDetails(Unit unit) throws Exception {
		Unit propUnit = getRealEstateEnterpriseService().getUnitService()
				.getUnitCompsWithProperty(unit);
		RePropertyUnitsTO reUnitTO = RePropertyUnitsTOConverter
				.convertToTO(propUnit);
		List<UnitDetail> unitDetails = getRealEstateEnterpriseService()
				.getUnitService().getUnitFeatureDetails(propUnit.getUnitId());
		reUnitTO.setUnitDetailVO(UnitDetailVO
				.convertToUnitDetailVO(unitDetails));
		List<Asset>unitAssetList=null;
		//Asset Details
		unitAssetList=getRealEstateEnterpriseService().getUnitService().getAssetDetails(propUnit.getUnitId());
		if(unitAssetList!=null && unitAssetList.size()>0)
			reUnitTO.setAssetList(RePropertyUnitsTOConverter.assetListToTOConverter(unitAssetList));
		return reUnitTO;
	}

	// Offer
	public OfferManagementTO getOfferDetails(Offer offer) throws Exception {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		HttpSession session = ServletActionContext.getRequest().getSession();
		// Final Value will be added into this object
		OfferManagementTO offerReport = new OfferManagementTO();
		offer = getRealEstateEnterpriseService().getOfferService()
				.getOffersById(offer.getOfferId());
		TenantOffer tenant = getRealEstateEnterpriseService().getOfferService()
				.getTenantById(offer.getOfferId());
		Person person = null;
		Company company = null;
		// Processing the tenant & offer information to report
		List<OfferManagementTO> tempInfos=new ArrayList<OfferManagementTO>();

		if (tenant.getPerson() != null) {
			person = new Person();
			person = getRealEstateEnterpriseService().getOfferService().getPersonDetails(tenant.getPerson().getPersonId());
			OfferManagementTO tempInfo=OfferTOConverter.convertOfferEntityTo(
					offer, person, tenant);
			tempInfos.add(tempInfo);
			offerReport.setOfferInfo(tempInfos);				//Set resource bundle offerInfo
			offerReport.setTenantInfo(tempInfos);

		} else {
			company = new Company();
			company = getRealEstateEnterpriseService().getOfferService()
					.getCompanyDetails(tenant.getCompany().getCompanyId());
			OfferManagementTO tempInfo=OfferTOConverter.convertOfferEntityTo(
					offer, company, tenant);
			tempInfos.add(tempInfo);
			offerReport.setOfferInfo(tempInfos);
			offerReport.setTenantInfo(tempInfos);

		}

		// Procession the offer detail records (Property/Component/Unit)
		OfferManagementTO propdetail = null;
		List<OfferDetail> detailList = getRealEstateEnterpriseService()
				.getOfferService().getOfferDetailById(offer.getOfferId());

		Property property = null;
		Component component = null;
		Unit unit = null;
		List<OfferManagementTO> propdetails=new ArrayList<OfferManagementTO>();
		for (OfferDetail offerDetail2 : detailList) {
			propdetail = new OfferManagementTO();
			if (offerDetail2.getProperty() != null
					&& offerDetail2.getProperty().getPropertyId() > 0) {
				property = getRealEstateEnterpriseService()
						.getPropertyService().getPropertyById(
								offerDetail2.getProperty().getPropertyId());
				propdetail.setPropertyName(property.getPropertyName());
				propdetail.setPropertyRent(property.getRentYr());

			} else if (offerDetail2.getComponent() != null
					&& offerDetail2.getComponent().getComponentId() > 0) {
				component = getRealEstateEnterpriseService()
						.getComponentService().getComponentById(
								offerDetail2.getComponent().getComponentId());
				propdetail.setPropertyName(component.getComponentName());
				propdetail.setPropertyRent(component.getRent());

			} else if (offerDetail2.getUnit() != null
					&& offerDetail2.getUnit().getUnitId() > 0) {
				unit = getRealEstateEnterpriseService().getUnitService()
						.getUnitDetails(offerDetail2.getUnit().getUnitId());
				propdetail.setPropertyName(unit.getUnitName());
				propdetail.setPropertyRent(unit.getRent());
			}
			propdetail.setOfferAmount(offerDetail2.getOfferAmount());
			propdetails.add(propdetail);
		}
		offerReport.setOfferDetailList(propdetails);

		Property propertyInfo = null;
		// Get the property information
		if (property != null) {
			// Find --> Property
			propertyInfo = getRealEstateEnterpriseService()
					.getPropertyService().getPropertyById(
							property.getPropertyId());

		} else if (component != null) {

			// Find component --> Property
			Component tempCcomponent = getRealEstateEnterpriseService()
					.getComponentService().getComponentById(
							component.getComponentId());

			propertyInfo = getRealEstateEnterpriseService()
					.getComponentService().getPropertyById(
							tempCcomponent.getProperty().getPropertyId());

		} else if (unit != null) {
			// Find unit-->Component-->Property
			Unit tempUnit = getRealEstateEnterpriseService().getUnitService()
					.getUnitDetails(unit.getUnitId());

			Component tempCcomponent = getRealEstateEnterpriseService()
					.getComponentService().getComponentById(
							tempUnit.getComponent().getComponentId());

			propertyInfo = getRealEstateEnterpriseService()
					.getComponentService().getPropertyById(
							tempCcomponent.getProperty().getPropertyId());
		}
		List<Property> propertys=new ArrayList<Property>();
		propertys.add(propertyInfo);
		offerReport.setProperty(propertys);
		return offerReport;
	}

	// Contract
	public ReContractAgreementTO getContractDetail(Contract contract)
			throws Exception {
		ReContractAgreementTO to = new ReContractAgreementTO();
		Contract con = getRealEstateEnterpriseService().getContractService()
				.getAllContractWithDetails(contract);
		to = ReContractAgreementTOConverter.convertToTO(con);
		Set<OfferDetail> offerDetails = con.getOffer().getOfferDetails();
		to.setProeprtyList(getProeprty(offerDetails));
		to.setContractPayments(con.getContractPayments());
		//Commented by rafiq on June 03 2012
		//to.setTenantList(getTenants(con.getOffer().getTenantOffers()));
		List<ReContractAgreementTO> tenantlst=new ArrayList<ReContractAgreementTO>();
		tenantlst.add(getTenantInfo(con.getOffer().getTenantOffers()));
		to.setTenantListReport(tenantlst);
		
		return to;

	}

	private List<HrPersonalDetailsTO> getTenants(Set<TenantOffer> tenantOffers)
			throws Exception {
		List<HrPersonalDetailsTO> persons = new ArrayList<HrPersonalDetailsTO>();
		for (TenantOffer offer : tenantOffers) {
			if(offer.getPerson()!=null){
			HrPersonalDetailsTO to = HrPersonalDetailsTOConverter
					.convertPersonToTO(offer.getPerson());
			/*to.setPersonTypes(hrEnterpriseService
					.getPersonService()
					.getPersonType(
							offer.getPerson().getPersonType().getPersonTypeId())
					.getTypeName());*/
			// to.setEmirateId(offer.getPerson().getEmiratesId());
			persons.add(to);
			}

		}
		return persons;
	}

	private ReContractAgreementTO getTenantInfo(Set<TenantOffer> tenantOffers)
			throws Exception {
		ReContractAgreementTO offerAgreementTO = new ReContractAgreementTO();
		
		for (TenantOffer offer : tenantOffers) {
			
			
			Company company=null;
			if(offer.getPerson()!=null){
			//Find Person
				Person persons= hrEnterpriseService.getPersonService().getPersonDetails(offer.getPerson().getPersonId());
				if(persons!=null){
					offerAgreementTO.setTenantName(persons.getFirstName() + " " + persons.getLastName());
					offerAgreementTO.setTenantId(Integer.parseInt(persons.getPersonId().toString()));
					offerAgreementTO.setPresentAddress(persons.getTownBirth());
					offerAgreementTO.setPermanantAddress(persons.getRegionBirth());
					offerAgreementTO.setTenantIssueDate(persons.getValidFromDate().toString());
					offerAgreementTO.setTenantIdentity(persons.getTradeLicenseNumber());
					offerAgreementTO.setTenantNationality(hrEnterpriseService.getSystemService()
							.getCountryById(Long.parseLong(persons.getNationality())).getCountryName());
					if(persons.getMobile()!=null){
					offerAgreementTO.setTenantFax(persons.getMobile().toString());
					offerAgreementTO.setTenantMobile(persons.getMobile().toString());
					}
					if(persons.getPostboxNumber()!=null)
					offerAgreementTO.setTenantPOBox(persons.getPostboxNumber());
					offerAgreementTO.setTenantType("Person");
				}
			}else{
				company=new Company();
				company=hrEnterpriseService.getCompanyService().getCompanyById(offer.getCompany().getCompanyId());
				if(company!=null){
					offerAgreementTO.setTenantName(company.getCompanyName());
					offerAgreementTO.setTenantId(Integer.parseInt(company.getCompanyId().toString()));
					offerAgreementTO.setPresentAddress(company.getDescription());
					offerAgreementTO.setTenantIssueDate("");
					offerAgreementTO.setTenantIdentity(company.getTradeNo());
					offerAgreementTO.setTenantNationality("");
					offerAgreementTO.setTenantFax("");
					offerAgreementTO.setTenantMobile("");
					offerAgreementTO.setTenantPOBox("");
					offerAgreementTO.setTenantType("Company");
				}
			}
		}
		return offerAgreementTO;
	}
	private List<RePropertyInfoTO> getProeprty(Set<OfferDetail> offerDetails)
			throws Exception {
		List<RePropertyInfoTO> contractProp = new ArrayList<RePropertyInfoTO>();
		for (OfferDetail detail : offerDetails) {
			if (null != detail.getProperty()) {
				// contractProp.add(setPropertyAdd(detail.getProperty()));
				Property prop = getRealEstateEnterpriseService()
						.getPropertyService().getPropertyWithPropertyType(
								detail.getProperty());
				contractProp.add(setPropertyAdd(prop));
			} else if (detail.getComponent() != null) {
				List<Component> comps = getRealEstateEnterpriseService()
						.getComponentService().getPropertyByComponentId(
								detail.getComponent());
				for (Component comp : comps) {
					contractProp.add(setPropertyAdd(comp.getProperty()));
				}
			} else if (detail.getUnit() != null) {
				List<Unit> units = getRealEstateEnterpriseService()
						.getUnitService().getPropertyByUnitId(detail.getUnit());
				for (Unit u : units) {
					contractProp.add(setPropertyAdd(u.getComponent()
							.getProperty()));
				}
			}
		}
		return contractProp;
	}

	private RePropertyInfoTO setPropertyAdd(Property p) throws Exception {
		// if (p != null){
		RePropertyInfoTO to = RePropertyInfoTOConverter.convertToTO(p);
		to.setAddress(getRealEstateEnterpriseService().getPropertyService()
				.getCityById(Long.parseLong("" + p.getCity())).getCityName()
				+ " "
				+ getRealEstateEnterpriseService().getPropertyService()
						.getStateById(Long.parseLong("" + p.getState()))
						.getStateName()
				+ " "
				+ getRealEstateEnterpriseService().getPropertyService()
						.getCounryById(Long.parseLong("" + p.getCountry()))
						.getCountryName());

		// contractProp.add(to);
		// }
		return to;
	}
	
	//Release
	public ContractReleaseTO getReleaseListInfo(Release release)
			throws Exception {
		ContractReleaseTO to = new ContractReleaseTO();
		Release rel = getRealEstateEnterpriseService().getReleaseService()
				.getReleaseWithAll(release);
		to = ContractReleaseTOConverter.convertToTO(rel);
		Contract c = rel.getContract();
		List<Contract> contractList = new ArrayList<Contract>();
		contractList.add(c);
		to.setReleaseDetails(rel.getReleaseDetails());
		to.setContractList(contractList);
		to.setPropertyList(getProeprty(c.getOffer().getOfferDetails()));
		to.setTenantList(getTenants(c.getOffer().getTenantOffers()));
		return to;
	}
	
	//Release Finalizing
	public ContractReleaseTO getReleaseListInfoFinal(Release release)
			throws Exception {
		ContractReleaseTO to = new ContractReleaseTO();
		Release rel = getRealEstateEnterpriseService().getReleaseService()
				.getReleaseWithAll(release);
		to = ContractReleaseTOConverter.convertToTO(rel);
		Contract c = rel.getContract();
		List<Contract> contractList = new ArrayList<Contract>();
		contractList.add(c);
		to.setReleaseDetails(rel.getReleaseDetails());
		to.setContractList(contractList);
		to.setPropertyList(getProeprty(c.getOffer().getOfferDetails()));
		List<ReContractAgreementTO> tenantInfo=new ArrayList<ReContractAgreementTO>();
		tenantInfo.add(getTenantInfo(c.getOffer().getTenantOffers()));
		to.setTenantInfo(tenantInfo);
		List<ReContractAgreementTO> offerTOList = new ArrayList<ReContractAgreementTO>();
		offerTOList = contractAgreementBL.getOfferDeatilsList(Integer.parseInt(c.getOffer().getOfferId().toString()));
		//Contract Total Amount
		double contractTotal=0;
		for (ReContractAgreementTO reContractAgreementTO : offerTOList) {
			contractTotal+=reContractAgreementTO.getRentAmount();
		}
		//Payment Process
		double damageTotal=0;
		for (ReleaseDetail releaseDetail : rel.getReleaseDetails()) {
			damageTotal+=releaseDetail.getAmount();
		}
		double otherChargers=0;
		double totalDeduction=0;
		double refundAmount=0;
		double depositAmount=0;
		double penaltyAmount=0;
		refundAmount=rel.getRefund();
		depositAmount=c.getDeposit();
		//Penalty Amount calculation
		Cancellation cancel=realEstateEnterpriseService.getCancellationService().getCancellationUseContract(c);
		HttpSession session = ServletActionContext.getRequest().getSession();
		int penaltyMonths = 0;
		if (session.getAttribute("penalty_months") != null)
			penaltyMonths = Integer.parseInt(session.getAttribute("penalty_months").toString()) ;
		if(cancel!=null && cancel.getIsPenalty()!=null && cancel.getIsPenalty()==true){
			penaltyAmount=(contractTotal/12)*(penaltyMonths);
			to.setContractTotal(contractTotal);
			to.setContractTotalStr(AIOSCommons.formatAmount(contractTotal));
			to.setPenaltyMonths(penaltyMonths);
			to.setPenaltyAmountStr(AIOSCommons.formatAmount(penaltyAmount));

		}else{
			to.setContractTotalStr(AIOSCommons.formatAmount(0));
			to.setPenaltyMonths(0);
			to.setPenaltyAmountStr(AIOSCommons.formatAmount(0));
		}
		
		otherChargers=depositAmount-(damageTotal+penaltyAmount);
		otherChargers=(otherChargers-refundAmount);
		totalDeduction=damageTotal+otherChargers+penaltyAmount;
		
		to.setDamageTotalStr(AIOSCommons.formatAmount(damageTotal));
		to.setOtherChargesStr(AIOSCommons.formatAmount(otherChargers));
		to.setRefundAmountStr(AIOSCommons.formatAmount(refundAmount));
		to.setTotalDeductionStr(AIOSCommons.formatAmount(totalDeduction));
		to.setRefundAmountInWords(AIOSCommons.convertAmountToWords(refundAmount));
		to.setDepositAmountStr(AIOSCommons.formatAmount(contractList.get(0).getDeposit()));
		
		return to;
	}
	
	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}
	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public RealEstateEnterpriseService getRealEstateEnterpriseService() {
		return realEstateEnterpriseService;
	}

	public void setRealEstateEnterpriseService(
			RealEstateEnterpriseService realEstateEnterpriseService) {
		this.realEstateEnterpriseService = realEstateEnterpriseService;
	}

	public HrEnterpriseService getHrEnterpriseService() {
		return hrEnterpriseService;
	}

	public void setHrEnterpriseService(HrEnterpriseService hrEnterpriseService) {
		this.hrEnterpriseService = hrEnterpriseService;
	}
	public ContractAgreementBL getContractAgreementBL() {
		return contractAgreementBL;
	}

	public void setContractAgreementBL(ContractAgreementBL contractAgreementBL) {
		this.contractAgreementBL = contractAgreementBL;
	}

	public UnitBL getUnitBL() {
		return unitBL;
	}

	public void setUnitBL(UnitBL unitBL) {
		this.unitBL = unitBL;
	}

	public PropertyInformationBL getPropertyInfoBL() {
		return propertyInfoBL;
	}

	public void setPropertyInfoBL(PropertyInformationBL propertyInfoBL) {
		this.propertyInfoBL = propertyInfoBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}


	public SystemService getSystemService() {
		return systemService;
	}


	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
}
