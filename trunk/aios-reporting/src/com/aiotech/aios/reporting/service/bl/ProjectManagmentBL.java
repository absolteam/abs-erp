package com.aiotech.aios.reporting.service.bl;

import com.aiotech.aios.project.service.bl.ProjectBL;
import com.aiotech.aios.project.service.bl.ProjectMileStoneBL;
import com.aiotech.aios.project.service.bl.ProjectTaskBL;
import com.aiotech.aios.reporting.to.AccountsTO;

public class ProjectManagmentBL {
	
	private ProjectMileStoneBL projectMileStoneBL;
	private ProjectBL projectBL;
	private ProjectTaskBL projectTaskBL;
	AccountsTO accountTO = null;


	public ProjectMileStoneBL getProjectMileStoneBL() {
		return projectMileStoneBL;
	}

	public void setProjectMileStoneBL(ProjectMileStoneBL projectMileStoneBL) {
		this.projectMileStoneBL = projectMileStoneBL;
	}

	public ProjectBL getProjectBL() {
		return projectBL;
	}

	public void setProjectBL(ProjectBL projectBL) {
		this.projectBL = projectBL;
	}

	public ProjectTaskBL getProjectTaskBL() {
		return projectTaskBL;
	}

	public void setProjectTaskBL(ProjectTaskBL projectTaskBL) {
		this.projectTaskBL = projectTaskBL;
	}
	
}
