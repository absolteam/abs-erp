package com.aiotech.aios.reporting.service.bl;

import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.aiotech.aios.accounts.service.bl.PosStagingSyncBL;

public class AccountsJob implements Job {
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		Map dataMap = context.getJobDetail().getJobDataMap();
		AccountsJobBL dailyMorning = (AccountsJobBL) dataMap
				.get("accountsDailyMorningSchedule");

		AccountsJobBL everyTwoHrsPOSSalesSync = (AccountsJobBL) dataMap
				.get("everyTwoHrsPOSSalesSync");

		try {

			if (dailyMorning != null) {
				dailyMorning.getDirectPaymentBL().alertDirectPaymentPDCCheque();
				dailyMorning.getBankReceiptsBL().alertBankReceiptPDCCheque();
			//	dailyMorning.getAssetDepreciationBL().createDepreciation();
			//	dailyMorning.getAssetInsurancePremiumBL()
			//			.premiumDueAutoPayment();
				// dailyMorning.getLedgerFiscalBL().processLedgerFiscal();
				dailyMorning.getProjectBL().paymentAlertByJobSheduler();
				
			}

			if (everyTwoHrsPOSSalesSync != null) {
				everyTwoHrsPOSSalesSync.getSalesUpdateJob().updateSalesSync();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			PosStagingSyncBL posStagingSyncBL = (PosStagingSyncBL) dataMap
					.get("dailyPOSStagingSync");
			posStagingSyncBL.syncPosTablesToStaging();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
