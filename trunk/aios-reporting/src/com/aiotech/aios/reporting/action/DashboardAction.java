package com.aiotech.aios.reporting.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;

import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.reporting.service.bl.DashboardBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class DashboardAction extends ActionSupport {

	private Implementation implementation;
	private List<Object> data;
	private List<Object> aaData;
	private DashboardBL dashboardBL;
	private String startDate;
	private String endDate;
	private Long storeId;
	private String name;
	private String mobileWidgetPosition;

	public String dashboardGeneralLedger() {
		return SUCCESS;
	}

	public String getAllFiscalTransactionByPeriod() {
		data = new ArrayList<Object>();
		data = new ArrayList<Object>(
				dashboardBL.getAllFiscalTransactionByPeriod());
		return SUCCESS;
	}

	public String getAllFiscalTransactionByCalendar() {
		data = new ArrayList<Object>();
		data = new ArrayList<Object>(
				dashboardBL.getAllFiscalTransactionByCalendar());
		return SUCCESS;
	}

	public String getAllFiscalTransaction() {
		data = new ArrayList<Object>();
		data = new ArrayList<Object>(dashboardBL.getAllFiscalTransaction());
		return SUCCESS;
	}

	public String dashboardSales() {
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusMonths(1));
		LocalDate date2 = new LocalDate(date);
		startDate = date1.toString("dd-MMM-yyyy");
		endDate = date2.toString("dd-MMM-yyyy");
		List<POSUserTill> posUserTills = dashboardBL
				.getAccountsEnterpriseService().getpOSUserTillService()
				.getAllPosUserTill(getImplementation());
		Map<Long, Store> sotresMap = new HashMap<Long, Store>();
		if (posUserTills != null && posUserTills.size() > 0) {
			for (POSUserTill posUserTill : posUserTills) {
				sotresMap.put(posUserTill.getStore().getStoreId(),
						posUserTill.getStore());
			}

		}
		ServletActionContext.getRequest().setAttribute("STORES",
				new ArrayList<Store>(sotresMap.values()));
		return SUCCESS;
	}

	public String dashboardDailySales() {
		data = new ArrayList<Object>(dashboardBL.dailySales(storeId));
		return SUCCESS;
	}

	public String dashboardMonthlySales() {
		data = new ArrayList<Object>(dashboardBL.monthlySales(storeId));
		return SUCCESS;
	}

	public String dashboardYearlySales() {
		data = new ArrayList<Object>(dashboardBL.yearlySales(storeId));
		return SUCCESS;
	}

	public String salesByCategory() {

		data = new ArrayList<Object>(dashboardBL.salesByCategory(
				DateFormat.convertStringToDate(startDate),
				DateFormat.convertStringToDate(endDate), storeId));
		return SUCCESS;
	}

	public String dashboardInventory() {
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusMonths(1));
		LocalDate date2 = new LocalDate(date);
		startDate = date1.toString("dd-MMM-yyyy");
		endDate = date2.toString("dd-MMM-yyyy");
		return SUCCESS;
	}

	public String dashboardHr() {
		LocalDate date = new LocalDate(new Date());
		LocalDate date1 = new LocalDate(date.minusMonths(1));
		LocalDate date2 = new LocalDate(date);
		startDate = date1.toString("dd-MMM-yyyy");
		endDate = date2.toString("dd-MMM-yyyy");
		return SUCCESS;
	}

	public String employeeHeadCounts() {
		if (name.equalsIgnoreCase("DESIGNATION"))
			data = new ArrayList<Object>(
					dashboardBL.employeeHeadCountsDesignation());
		else
			data = new ArrayList<Object>(
					dashboardBL.employeeHeadCountsByDepartment());
		return SUCCESS;
	}

	public String wagesByDepartment() {
		if (name.equalsIgnoreCase("DESIGNATION"))
			data = new ArrayList<Object>(dashboardBL.wagesByDesignation());
		else
			data = new ArrayList<Object>(dashboardBL.wagesByDepartment());
		return SUCCESS;
	}

	public String getAbsenteesmByPeriod() {
		if (startDate == null || startDate.equals("")) {
			LocalDate date = new LocalDate(new Date());
			LocalDate date1 = new LocalDate(date.minusMonths(1));
			LocalDate date2 = new LocalDate(date);
			startDate = date1.toString("dd-MMM-yyyy");
			endDate = date2.toString("dd-MMM-yyyy");
		}

		LocalDate date1 = new LocalDate(
				DateFormat.convertStringToDate(startDate));
		LocalDate date2 = new LocalDate(DateFormat.convertStringToDate(endDate));
		aaData = new ArrayList<Object>(dashboardBL.getAbsenteesmByPeriod(
				date1.toDate(), date2.toDate()));
		return SUCCESS;
	}


	public String dashboardDailyProduction() {
		data = new ArrayList<Object>(dashboardBL.dailyProduction());
		return SUCCESS;
	}

	public String dashboardMonthlyProduction() {
		data = new ArrayList<Object>(dashboardBL.monthlyProduction());
		return SUCCESS;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public DashboardBL getDashboardBL() {
		return dashboardBL;
	}

	public void setDashboardBL(DashboardBL dashboardBL) {
		this.dashboardBL = dashboardBL;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getMobileWidgetPosition() {
		return mobileWidgetPosition;
	}

	public void setMobileWidgetPosition(String mobileWidgetPosition) {
		this.mobileWidgetPosition = mobileWidgetPosition;
	}

}
