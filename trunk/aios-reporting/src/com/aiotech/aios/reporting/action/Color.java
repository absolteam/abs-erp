package com.aiotech.aios.reporting.action;

public class Color {

	public final static int WHITE = 1;

	public final static int RED = 2;

	public final static int GREEN = 3;

	public final static int PURPLE = 6;

	public final static int SKY = 7;

	public final static int CANARY = 13;

	public final static int OLIVE = 19;

	public final static int L_GRAY = 22;

	public final static int D_GRAY = 23;

	public final static int CREAM = 26;

	public final static int PINK = 29;

	public final static int NAVY = 32;

	public final static int YELLOW = 34;

	public final static int MAROON = 37;

	public final static int BLUE = 40;

	public final static int SEA = 42;

	public final static int FLESH = 47;

	public final static int PUMPKIN = 51;

	public final static int ORANGE = 52;

	public final static int DEFAULT = 64;

	public static String getColorName(int color) {
		String returnString = null;
		switch (color) {
		case Color.WHITE:
			returnString = "White";
			break;
		case Color.BLUE:
			returnString = "Blue";
			break;
		case Color.CANARY:
			returnString = "Canary";
			break;
		case Color.CREAM:
			returnString = "Cream";
			break;
		case Color.D_GRAY:
			returnString = "Dark Gray";
			break;
		case Color.FLESH:
			returnString = "Flesh";
			break;
		case Color.GREEN:
			returnString = "Green";
			break;
		case Color.L_GRAY:
			returnString = "Light Gray";
			break;
		case Color.MAROON:
			returnString = "Maroon";
			break;
		case Color.NAVY:
			returnString = "Navy";
			break;
		case Color.OLIVE:
			returnString = "Olive";
			break;
		case Color.ORANGE:
			returnString = "Orange";
			break;
		case Color.PINK:
			returnString = "Pink";
			break;
		case Color.PUMPKIN:
			returnString = "Pumpkin";
			break;
		case Color.PURPLE:
			returnString = "Purple";
			break;
		case Color.RED:
			returnString = "Red";
			break;
		case Color.SEA:
			returnString = "Sea";
			break;
		case Color.SKY:
			returnString = "Sky";
			break;
		case Color.YELLOW:
			returnString = "Yellow";
			break;
		case Color.DEFAULT:
			returnString = "Default";
			break;
	    default:
	    	returnString = "Undefined";
	        break;

		}

		return returnString;
	}
}
