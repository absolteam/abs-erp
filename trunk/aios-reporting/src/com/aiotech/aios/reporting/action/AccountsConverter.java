package com.aiotech.aios.reporting.action;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.LoanDetail;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.to.LoanTO;
import com.aiotech.aios.accounts.to.PaymentTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;

public class AccountsConverter {
	static final List<LoanTO> convertLoanDetailEntity(List<LoanDetail> loanDetailList){
		List<LoanTO> loanDetailTo=new ArrayList<LoanTO>();
		for(LoanDetail list: loanDetailList){
			loanDetailTo.add(convertDetailTo(list));
		}
		return loanDetailTo;
	}

	private static final LoanTO convertDetailTo(LoanDetail loanDetail){
		LoanTO loanDetailTO=new LoanTO();
		loanDetailTO.setLoanDetailId(loanDetail.getLoanDetailId());
		loanDetailTO.setDetailDate(DateFormat.convertDateToString(loanDetail.getDate().toString()));
		loanDetailTO.setAmount(loanDetail.getAmount());
		loanDetailTO.setDescription(loanDetail.getDescription()); 
		return loanDetailTO;
	}
	
	static final List<LoanTO> convertLoanChargesEntity(List<LoanCharge> chargesList){
		List<LoanTO> loanChargeTo=new ArrayList<LoanTO>();
		for(LoanCharge list: chargesList){
			loanChargeTo.add(convertChargeTo(list));
		}
		return loanChargeTo;
	}
	
	private static final LoanTO convertChargeTo(LoanCharge loanCharge){
		LoanTO loanChargeTO=new LoanTO();
		loanChargeTO.setOtherChargeId(loanCharge.getOtherChargeId());
		loanChargeTO.setChargeName(loanCharge.getName());
		loanChargeTO.setChargeValue(loanCharge.getValue());
		if(loanCharge.getType()=='I')
			loanChargeTO.setChargeType("Interest");
		else	
			loanChargeTO.setChargeType("Amount");
		return loanChargeTO;
	}
	
	public static final LoanTO convertLoanEntity(Loan loan){
		LoanTO loanTo=new LoanTO();
		loanTo.setLoan(loan);
		loanTo.setLoanNumber(loan.getLoanNumber());
		loanTo.setAccountNumber(loan.getBankAccount().getAccountNumber()+" ("+loan.getBankAccount().getBranchName()+")"); 
		loanTo.setLoanType(Constants.Accounts.LoanType.get(loan.getLoanType()).toString());
		if(loan.getRateType().equalsIgnoreCase("F"))
			loanTo.setInterestType("Fixed");
		else if(loan.getRateType().equalsIgnoreCase("V"))
			loanTo.setInterestType("Vary");
		else if(loan.getRateType().equalsIgnoreCase("M"))
			loanTo.setInterestType("Mixed");
		if(loan.getRateVariance()=='F')
			loanTo.setInterestMethod("Flat");
		else if(loan.getRateVariance()=='V')
			loanTo.setInterestMethod("Variance");
		loanTo.setCurrency(loan.getCurrency().getCurrencyPool().getCode());
		loanTo.setSancationDate(DateFormat.convertDateToString(loan.getSanctionDate().toString()));
		loanTo.setMaturityDate(DateFormat.convertDateToString(loan.getMaturityDate().toString()));
		loanTo.setLoanAmount(loan.getAmount());
		if(loan.getEmiType().equalsIgnoreCase("P"))
			loanTo.setEmiType("Predictable");
		else
			loanTo.setEmiType("UnPredictable");
		return loanTo;
	}
	
	public static final PaymentTO convertPaymentEntity(Payment payment){
		PaymentTO paymentTO=new PaymentTO();
		paymentTO.setPaymentId(payment.getPaymentId());
		paymentTO.setPaymentNumber(payment.getPaymentNumber());
		paymentTO.setPaymentDate(DateFormat.convertDateToString(payment.getDate().toString()));
		paymentTO.setDescription(payment.getDescription()); 
		return paymentTO;
	}
	
	public static final List<PaymentTO> convertPaymentDetailEntity(List<PaymentDetail> detailList){
		List<PaymentTO> paymentDetailList=new ArrayList<PaymentTO>(); 
		for(PaymentDetail detail: detailList){
			if(null!=detail.getLoanCharge() && !detail.getLoanCharge().equals("")){ 
				paymentDetailList.add(convertLoanChargesEntityTO(detail)); 
			}
		}
		return paymentDetailList;
	}
	
	private static final PaymentTO convertLoanChargesEntityTO(PaymentDetail detail){
		PaymentTO paymentTO=new PaymentTO();
		if(detail.getLoanCharge().getType()=='A')
			paymentTO.setChargeType("Amount");
		else
			paymentTO.setChargeType("Interest");
		paymentTO.setChargeName(detail.getLoanCharge().getName());
		paymentTO.setChargeValue(detail.getLoanCharge().getValue());
		return paymentTO;
	}
}
