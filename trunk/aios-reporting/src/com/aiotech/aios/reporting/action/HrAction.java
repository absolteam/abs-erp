package com.aiotech.aios.reporting.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.ServletActionContext;
import org.joda.time.LocalDate;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.AcademicQualifications;
import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Dependent;
import com.aiotech.aios.hr.domain.entity.Experiences;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.domain.entity.PersonBankVO;
import com.aiotech.aios.hr.domain.entity.PersonType;
import com.aiotech.aios.hr.domain.entity.Skills;
import com.aiotech.aios.hr.domain.entity.vo.AssetUsageVO;
import com.aiotech.aios.hr.domain.entity.vo.IdentityAvailabilityVO;
import com.aiotech.aios.hr.domain.entity.vo.IdentityVO;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollTransactionVO;
import com.aiotech.aios.hr.domain.entity.vo.PayrollVO;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.domain.entity.vo.SwipeInOutVO;
import com.aiotech.aios.hr.service.HrEnterpriseService;
import com.aiotech.aios.hr.service.bl.CompanyBL;
import com.aiotech.aios.hr.service.bl.IdentityAvailabilityBL;
import com.aiotech.aios.hr.service.bl.PersonBL;
import com.aiotech.aios.hr.to.AttendanceTO;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.LeaveProcessTO;
import com.aiotech.aios.hr.to.PayrollTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.reporting.service.bl.HrBL;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class HrAction extends ActionSupport {

	private HrEnterpriseService hrEnterpriseService;
	private HrBL hrBL;
	private List<LeaveProcessTO> rptList = null;
	private List<PayrollTO> payrollList = null;
	private List<AttendanceTO> attendanceList = null;

	private List<Object> aaData = null;

	private Long recordId;
	private String format;
	private String approvalFlag;
	private String fromDate;
	private String toDate;
	private int tempVar;
	private String tempString;
	private String tempString1;
	private String tempString2;
	private long tempLong;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private Map<Object, Boolean> exportParams = new HashMap<Object, Boolean>();
	JasperPrint jasperPrint;

	private IdentityAvailabilityBL availabilityBL;

	List<IdentityVO> documentsDS = null;
	List<IdentityAvailabilityVO> identityAvailabilityDS = null;
	List<PersonBankVO> personBankDS;

	private InputStream fileInputStream;
	private PersonBL personBL;
	private List<HrPersonalDetailsTO> personalDetailsList;
	private String personTypes;
	private int disabled;
	private String gender;
	private String maritalStatus;
	private String personGroup;
	private String nationality;
	private String effectiveDatesFrom;
	private String effectiveDatesTo;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private List<PersonVO> personDS;
	private int personId;
	private ArrayList<Identity> identitys;
	private ArrayList<Dependent> dependents;

	private List<LeaveProcessTO> employeesDS;
	private List<JobAssignmentVO> jobAssignmentDS;
	private List<SwipeInOutVO> attendanceSwipeDS;
	private List<PayrollTransactionVO> payrollTransactionDS;
	private List<AssetUsageVO> assetUsageDS;

	private String selectedEmployeeJobId;
	private String selectedCompanyId;
	private String selectedType;
	private String selectedMonth;
	private String selectedYear;

	private String selectedPersonId;
	private String selectedProductId;
	private String selectedLocationId;
	private String selectedAssetUsageId;

	private String personType;
	private String companyType;
	private String status;
	private CompanyBL companyBL;
	private String printCall;
	private String printableContent;

	public String approveLeave() {

		rptList = new ArrayList<LeaveProcessTO>();

		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			LeaveProcessTO to = new LeaveProcessTO();

			to = hrBL.getLeaveProcessBL().getLeaveInfoForApproval(recordId);

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadLeaveProcessInfo.action?recordId="
					+ recordId + "&approvalFlag=N&format=PDF"
					: "#");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny.getCode()
							+ ""));

			if (rptList != null && rptList.size() == 0)
				rptList.add(to);
			else
				rptList.set(0, to);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getPayrollApproval() {

		payrollList = new ArrayList<PayrollTO>();

		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			PayrollTO to = new PayrollTO();

			to = hrBL.getPayrollBL().getPayrollInfoForApproval(recordId);

			jasperRptParams.put("EARNING_TOTAL", to.getEarningTotal());
			jasperRptParams.put("DEDUCTION_TOTAL", to.getDeductionTotal());
			jasperRptParams.put("NET_PAY", to.getNetAmount());
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPayrollInfo.action?recordId="
					+ recordId + "&approvalFlag=N&format=PDF"
					: "#");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny.getCode()
							+ ""));

			if (payrollList != null && payrollList.size() == 0)
				payrollList.add(to);
			else
				payrollList.set(0, to);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getPayslip() {
		try {
			payrollList = new ArrayList<PayrollTO>();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Boolean isTemplatePrintEnabled = session
					.getAttribute("print_payslip_template") != null ? ((String) session
					.getAttribute("print_payslip_template"))
					.equalsIgnoreCase("true") : false;
			PayrollTO to = new PayrollTO();

			to = hrBL.getPayrollBL().getPayrollInfoForApproval(recordId);
			if (!isTemplatePrintEnabled) {
				try {
					String ctxRealPath = ServletActionContext
							.getServletContext().getRealPath("/");

					jasperRptParams.put("EARNING_TOTAL", to.getEarningTotal());
					jasperRptParams.put("DEDUCTION_TOTAL",
							to.getDeductionTotal());
					jasperRptParams.put("NET_PAY", to.getNetAmount());
					jasperRptParams.put(
							"AIOS_LOGO",
							"file:///"
									+ ctxRealPath.concat(File.separator)
											.concat("images" + File.separator
													+ "aiotech.jpg"));

					to.setLinkLabel("HTML".equals(getFormat()) ? "Download"
							: "");
					to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPayslip.action?recordId="
							+ recordId + "&approvalFlag=N&format=PDF"
							: "#");

					// Approve Content
					to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
							.equals(getApprovalFlag())) ? "Approve" : "");
					to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
							+ Byte.parseByte(Constants.RealEstate.Status.Approved
									.getCode() + ""));

					// Disapprove Content
					to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
							.equals(getApprovalFlag())) ? "Reject" : "");
					to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
							+ Byte.parseByte(Constants.RealEstate.Status.Deny
									.getCode() + ""));

					if (payrollList != null && payrollList.size() == 0)
						payrollList.add(to);
					else
						payrollList.set(0, to);

				} catch (Exception e) {
					e.printStackTrace();
				}
				return SUCCESS;

			} else {
				Configuration config = new Configuration();
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");

				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/hr/"
						+ hrBL.getImplementation().getImplementationId()));

				String scheme = ServletActionContext.getRequest().getScheme();
				String host = ServletActionContext.getRequest().getHeader(
						"Host");

				// includes leading forward slash
				String contextPath = ServletActionContext.getRequest()
						.getContextPath();

				String urlPath = scheme + "://" + host + contextPath;

				Template template = config
						.getTemplate("html-payslip-template.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();

				if (hrBL.getImplementation().getMainLogo() != null) {

					byte[] bFile = FileUtil.getBytes(new File(hrBL
							.getImplementation().getMainLogo()));
					bFile = Base64.encodeBase64(bFile);

					rootMap.put("logo", new String(bFile));
				} else {
					rootMap.put("logo", "");
				}

				rootMap.put("urlPath", urlPath);
				rootMap.put("PAYROLL_MASTER", to);

				rootMap.put("company", hrBL.getImplementation()
						.getCompanyName());
				Writer out = new StringWriter();
				template.process(rootMap, out);
				printableContent = out.toString();
				// System.out.println(printableContent);

				return "template";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "template";
		}
	}

	public String getEmployeePunch() {

		attendanceList = new ArrayList<AttendanceTO>();

		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			AttendanceTO to = new AttendanceTO();

			if (recordId != null && recordId > 0 && fromDate != null
					&& !fromDate.equals("")) {
				to = hrBL.getAttendanceBL().punchReport(recordId,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.equals("")) {
				to = hrBL.getAttendanceBL().punchReport(null,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));

			}

			jasperRptParams.put("TOTAL_HOURS", to.getTotalHours());

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloademployeePunch.action?recordId="
					+ recordId + "&approvalFlag=N&format=PDF"
					: "#");

			if (attendanceList != null && attendanceList.size() == 0)
				attendanceList.add(to);
			else
				attendanceList.set(0, to);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String returnSuccess() {
		return SUCCESS;
	}

	public String loadDocumentExpiryReportCriteria() {
		try {
			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			List<Company> companies = hrEnterpriseService.getCompanyService()
					.getAllCompany(implementation);
			ServletActionContext.getRequest().setAttribute("COMPANIES",
					companies);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public List<Identity> fetchDocumentExpiryReportData() {

		List<Identity> documents = new ArrayList<Identity>();
		try {

			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;
			Long personId = null;
			Long companyId = null;
			if (fromDate == null || fromDate.equals("")) {
				LocalDate date = new LocalDate(new Date());
				LocalDate date1 = new LocalDate(date.minusMonths(1));
				LocalDate date2 = new LocalDate(date);
				fromDate = date1.toString("dd-MMM-yyyy");
				toDate = date2.toString("dd-MMM-yyyy");
			}
			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			} else {

			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			if (selectedEmployeeJobId != null
					&& !selectedEmployeeJobId.equals("null")
					&& !selectedEmployeeJobId.equals("0")) {
				personId = Long.parseLong(selectedEmployeeJobId);
			}
			if (selectedCompanyId != null && !selectedCompanyId.equals("null")
					&& !selectedCompanyId.equals("0")) {
				companyId = Long.parseLong(selectedCompanyId);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}

			documents = hrEnterpriseService.getIdentityAvailabilityService()
					.getIdentityListByDifferentValues(personId, companyId,
							fromDatee, toDatee, isActive, implementation);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return documents;
	}

	public String populateDocumentExpiryTable() {

		String result = ERROR;
		try {
			aaData = new ArrayList<Object>();
			aaData.addAll(fetchDocumentExpiryReportData());
			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String documentExpiryReportXLS() {
		try {

			List<Identity> docuements = fetchDocumentExpiryReportData();

			String str = "";

			str = "Document Expiry Report\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date\n";
			str += fromDate == "null" ? ""
					: fromDate + "," + toDate == "null" ? "" : toDate + "\n\n";
			if (docuements != null && docuements.size() > 0) {
				str += "Sr. #,Document Type, Document From,Document Of, Expiry Date,Days Left,Document Number,Issed Place";
				str += "\n";
				int srNo = 1;
				for (Identity identity : docuements) {

					String type = null;
					String from = null;
					String days = null;
					if (identity.getPerson() != null) {
						type = "Person";
						from = identity.getPerson().getFirstName() + " "
								+ identity.getPerson().getLastName();
					} else {
						type = "Company";
						from = identity.getCompany().getCompanyName();
					}

					if (DateFormat.dayVariance(new Date(),
							identity.getExpireDate()) > 0) {
						Long dayss = DateFormat.dayVariance(new Date(),
								identity.getExpireDate());
						days = dayss.toString();
					} else {
						days = "0";
					}

					str += srNo
							+ ","
							+ identity.getLookupDetail().getDisplayName()
							+ ","
							+ type
							+ ","
							+ from
							+ ","
							+ DateFormat
									.convertSystemDateToStringTimeFormat(identity
											.getExpireDate()) + "," + days
							+ "," + identity.getIdentityNumber().toString()
							+ "," + identity.getIssuedPlace() + "," + "" + ",";

					str += "\n";

					srNo = srNo + 1;

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String documentExpiryReporPrintOut() {
		try {
			List<Identity> documents = fetchDocumentExpiryReportData();

			IdentityVO vo = new IdentityVO();

			ServletActionContext.getRequest().setAttribute("DOCS",
					vo.convertToVOList(documents));
			/*
			 * ServletActionContext.getRequest().setAttribute(
			 * "PURCHASE_DETAIL_INFO", purchaseDetails);
			 */

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String documentExpiryReporPDF() {
		try {
			List<Identity> documents = fetchDocumentExpiryReportData();

			IdentityVO vo = new IdentityVO();
			documentsDS = vo.convertToVOList(documents);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			/*
			 * account.setLinkLabel("HTML".equals(getFormat()) ? "Download" :
			 * ""); account.setAnchorExpression("HTML".equals(getFormat()) ?
			 * "download_balancesheet.action?format=PDF" : "#");
			 */

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	// Person Report Redirect call
	public String personReportRedirect() {
		try {

			if (ServletActionContext.getRequest().getHeader("User-Agent")
					.indexOf("Mobile") != -1) {
				boolean isMobile = true;
				ServletActionContext.getRequest().setAttribute("IS_MOBILE",
						isMobile);
			}
			List<Country> countries = personBL.getPersonService()
					.getAllCountry();
			List<LookupDetail> personTypeList = personBL.getLookupMasterBL()
					.getActiveLookupDetails("PERSON_TYPE", true);

			ServletActionContext.getRequest().setAttribute("COUNTRYLIST",
					countries);
			ServletActionContext.getRequest().setAttribute("PERSONTYPELIST",
					personTypeList);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Listing the Personal Details
	public String personReport() {
		try {

			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();

			HrPersonalDetailsTO personFilterTO = new HrPersonalDetailsTO();
			personFilterTO.setPersonTypes(personTypes);
			personFilterTO.setDisabled(disabled);
			personFilterTO.setGender(gender + "");
			personFilterTO.setMaritalStatus(maritalStatus + "");
			personFilterTO.setPersonGroup(personGroup + "");
			personFilterTO.setNationality(nationality);
			personFilterTO.setEffectiveDatesFrom(effectiveDatesFrom);
			personFilterTO.setEffectiveDatesTo(effectiveDatesTo);
			// Set the value to push to Personal Details DAO
			List<Person> person = personBL.getPersonReport(personFilterTO);

			// person=personBL.personTypeFilteration("1", person);
			personalDetailsList = HrPersonalDetailsTOConverter
					.convertToTOList(person);

			Country country = null;
			PersonType personType = null;
			if (personalDetailsList != null && personalDetailsList.size() > 0) {
				country = new Country();
				personType = new PersonType();
				for (int i = 0; i < personalDetailsList.size(); i++) {
					// Find the country
					if (personalDetailsList.get(i).getCountryCode() != null
							&& !personalDetailsList.get(i).getCountryCode()
									.equals("")) {
						country = personBL.getPersonService().getCountry(
								Long.parseLong(personalDetailsList.get(i)
										.getCountryCode()));
						personalDetailsList.get(i).setCountryBirth(
								country.getCountryName());
					} else {
						personalDetailsList.get(i).setCountryBirth("");
					}

				}

				iTotalRecords = personalDetailsList.size();
				iTotalDisplayRecords = personalDetailsList.size();

			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (HrPersonalDetailsTO list : personalDetailsList) {

				// if (list.getPersonTypes().contains("Supplier") ||
				// list.getPersonTypes().contains("Customer"))
				// continue;
				JSONArray array = new JSONArray();
				array.add(list.getPersonId());
				array.add(list.getFirstName());
				array.add(list.getLastName());
				array.add(list.getPersonTypes());
				array.add(list.getCountryBirth());
				array.add(list.getIdentification() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Listing the Personal Details
	public String personReportPrint() {
		try {

			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();

			HrPersonalDetailsTO personFilterTO = new HrPersonalDetailsTO();
			personFilterTO.setPersonTypes(personTypes);
			personFilterTO.setDisabled(disabled);
			personFilterTO.setGender(gender + "");
			personFilterTO.setMaritalStatus(maritalStatus + "");
			personFilterTO.setPersonGroup(personGroup + "");
			personFilterTO.setNationality(nationality);
			personFilterTO.setEffectiveDatesFrom(effectiveDatesFrom);
			personFilterTO.setEffectiveDatesTo(effectiveDatesTo);
			// Set the value to push to Personal Details DAO
			List<Person> persons = personBL.getPersonReport(personFilterTO);

			// person=personBL.personTypeFilteration("1", person);
			List<PersonVO> personalVOs = personBL.convertToVOList(persons);

			for (PersonVO personVO : personalVOs) {
				// Employee details
				if (personVO.getJobAssignments() != null) {
					for (JobAssignment jobAssignment : personVO
							.getJobAssignments()) {
						if (jobAssignment.getIsActive()) {
							JobAssignment jobAssignmentTemp = hrBL
									.getJobAssignmentBL()
									.getJobAssignmentService()
									.getJobAssignmentInfo(
											jobAssignment.getJobAssignmentId());
							personVO.setJobAssignmentVO(hrBL
									.getJobAssignmentBL()
									.convertEntityToVOWithJob(jobAssignmentTemp));
						}
					}
				}
			}

			ServletActionContext.getRequest().setAttribute("PERSON_MASTER",
					personFilterTO);
			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					personalVOs);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	// Listing the Personal Details
	public String printLeaveReconciliationReport() throws Exception {

		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		Date fromDatee = null;
		Date toDatee = null;
		Boolean isActive = true;

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		jobAssignments = hrBL
				.getAttendanceBL()
				.getJobService()
				.getEmployeesLeaveReconciliationsByCriteria(fromDatee, toDatee,
						isActive, implementation,
						Long.parseLong(selectedEmployeeJobId));

		List<JobAssignmentVO> jobAssignmentVOs = hrBL.getJobAssignmentBL()
				.convertEntitiesTOVOs(jobAssignments);
		ServletActionContext.getRequest().setAttribute("JOB_ASSIGNMENTS",
				jobAssignmentVOs);

		return SUCCESS;
	}

	public String downloadPersonReportXLS() {
		try {

			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();

			HrPersonalDetailsTO personFilterTO = new HrPersonalDetailsTO();
			personFilterTO.setPersonTypes(personTypes);
			personFilterTO.setDisabled(disabled);
			personFilterTO.setGender(gender + "");
			personFilterTO.setMaritalStatus(maritalStatus + "");
			personFilterTO.setPersonGroup(personGroup + "");
			personFilterTO.setNationality(nationality);
			personFilterTO.setEffectiveDatesFrom(effectiveDatesFrom);
			personFilterTO.setEffectiveDatesTo(effectiveDatesTo);
			// Set the value to push to Personal Details DAO
			List<Person> persons = personBL.getPersonReport(personFilterTO);

			List<PersonVO> personalVOs = personBL.convertToVOList(persons);

			String str = "";

			str = "Person List\n\n";
			/*
			 * str += "Filter Condition\n"; str +=
			 * "Person Type,Disabled,Gender,Marital Status, Person Group, Nationailty,Dated From, Dates To\n"
			 * ; str += personTypes + "," + disabled + "," + gender + "," +
			 * maritalStatus + "," + personGroup + "," + nationality + "," +
			 * effectiveDatesFrom + "," + effectiveDatesTo + "\n\n";
			 */
			if (persons != null && persons.size() > 0) {
				str += "Name,Number,Person Group,Nationality,Type,Gender,Status,DOB,Joining Date,Marital Status, Email, Mobile, Alternate Mobile";
				str += "\n";
				for (PersonVO person : personalVOs) {
					str += person.getPersonName()
							+ ","
							+ person.getPersonNumber()
							+ ","
							+ person.getPersonGroupCaption()
							+ ","
							+ person.getNationalityCaption()
							+ ","
							+ person.getPersonTypes()
							// + ","
							+ person.getGenderCaption() + ","
							+ person.getStatusCaption() + ","
							+ person.getBirthDate() + ","
							+ person.getEffectiveDatesFrom() + ","
							+ person.getMaritalStatusName() + ","
							+ person.getEmail() + "," + person.getMobile()
							+ "," + person.getAlternateMobile() + ",";

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printPersonReportJasper() {
		try {

			personalDetailsList = new ArrayList<HrPersonalDetailsTO>();

			HrPersonalDetailsTO personFilterTO = new HrPersonalDetailsTO();
			personFilterTO.setPersonTypes(personTypes);
			personFilterTO.setDisabled(disabled);
			personFilterTO.setGender(gender + "");
			personFilterTO.setMaritalStatus(maritalStatus + "");
			personFilterTO.setPersonGroup(personGroup + "");
			personFilterTO.setNationality(nationality);
			personFilterTO.setEffectiveDatesFrom(effectiveDatesFrom);
			personFilterTO.setEffectiveDatesTo(effectiveDatesTo);
			// Set the value to push to Personal Details DAO
			List<Person> persons = personBL.getPersonReport(personFilterTO);

			List<PersonVO> personalVOs = personBL.convertToVOList(persons);
			/*
			 * personalDetailsList = HrPersonalDetailsTOConverter
			 * .convertToTOList(persons);
			 * 
			 * personDS = personalDetailsList;
			 */
			personDS = personalVOs;
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printPersonDetailReport() {
		try {
			if (personId != 0) {

				Person person = personBL.getPersonService().getPersonDetails(
						Long.parseLong(personId + ""));
				PersonVO personVO = personBL
						.personConverterFromEntityTOVO(person);
				if (personVO.getJobAssignments() != null
						&& personVO.getJobAssignments().size() > 0) {
					for (JobAssignment jobAssignment : personVO
							.getJobAssignments()) {
						if (jobAssignment.getIsActive() != null
								&& jobAssignment.getIsActive()) {
							jobAssignment = hrBL
									.getJobAssignmentBL()
									.getJobAssignmentService()
									.getJobAssignmentInfo(
											jobAssignment.getJobAssignmentId());
							personVO.setJobAssignmentVO(hrBL
									.getJobAssignmentBL()
									.convertEntityToVOWithJob(jobAssignment));
						}

					}
				}

				identitys = new ArrayList<Identity>(person.getIdentities()); // personBL.getPersonService().getAllIdentity(person);
				dependents = new ArrayList<Dependent>(person.getDependents());
				; /*
				 * personBL.getPersonService() .getAllDependent(person);
				 */
				List<HrPersonalDetailsTO> identityList = HrPersonalDetailsTOConverter
						.convertIdentityToTOList(identitys);
				List<HrPersonalDetailsTO> dependentList = HrPersonalDetailsTOConverter
						.convertDependentToTOList(dependents);

				ServletActionContext.getRequest().setAttribute("PERSON",
						personVO);
				ServletActionContext.getRequest().setAttribute("IDENTITY_LIST",
						identityList);
				ServletActionContext.getRequest().setAttribute(
						"DEPENDENT_LIST", dependentList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String downloadPersonDetailReportXLS() {
		try {

			if (personId != 0) {

				Person person = personBL.getPersonService().getPersonDetails(
						Long.parseLong(personId + ""));
				PersonVO personVO = personBL
						.personConverterFromEntityTOVO(person);

				identitys = new ArrayList<Identity>(person.getIdentities()); // personBL.getPersonService().getAllIdentity(person);
				dependents = new ArrayList<Dependent>(person.getDependents());
				; /*
				 * personBL.getPersonService() .getAllDependent(person);
				 */
				List<HrPersonalDetailsTO> identityList = HrPersonalDetailsTOConverter
						.convertIdentityToTOList(identitys);
				List<HrPersonalDetailsTO> dependentList = HrPersonalDetailsTOConverter
						.convertDependentToTOList(dependents);

				String str = "";

				str = "Personal Detail \n\n";

				str += "Name,Number,Person Group,Nationality,Type,Gender,Status,DOB,Joining Date,Marital Status, Email, Mobile, Alternate Mobile";
				str += "\n";
				str += personVO.getPersonName()
						+ ","
						+ personVO.getPersonNumber()
						+ ","
						+ personVO.getPersonGroupCaption()
						+ ","
						+ personVO.getNationalityCaption()
						+ ","
						+ personVO.getPersonTypes()
						// + ","
						+ personVO.getGenderCaption() + ","
						+ personVO.getStatusCaption() + ","
						+ personVO.getBirthDate() + ","
						+ personVO.getEffectiveDatesFrom() + ","
						+ personVO.getMaritalStatusName() + ","
						+ personVO.getEmail() + "," + personVO.getMobile()
						+ "," + personVO.getAlternateMobile() + ",";

				str += "\n\n";

				str += "Identity, \n\n";

				str += "Identity Type,Identity Number,Expiry Date,Issued Place,Description";

				str += "\n";

				for (HrPersonalDetailsTO identity : identityList) {
					str += identity.getIdentityTypeName() + ","
							+ identity.getIdentityNumber() + ","
							+ identity.getExpireDate() + ","
							+ identity.getIssuedPlace() + ","
							+ identity.getDescription() + ",";
					str += "\n";
				}

				str += "\n\n";
				str += "Dependent, \n\n";
				str += "Name,Relationship,Address,Mobile,Phone,Email,Emergency,Dependent,Benefit";
				str += "\n";

				for (HrPersonalDetailsTO dependent : dependentList) {
					str += dependent.getDependentName() + ","
							+ dependent.getRelationship() + ","
							+ dependent.getAddress() + ","
							+ dependent.getMobile() + ","
							+ dependent.getPhone() + "," + dependent.getEmail()
							+ "," + dependent.getMobile() + "," + "" + "," + ""
							+ ",";

					str += "\n";
				}

				str += "\n\n";
				str += "Qualifications, \n\n";
				str += "Degree,Subject,Instution,Completion Year,Result,Result Type,";
				str += "\n";

				for (AcademicQualifications qualificatoins : person
						.getAcademicQualificationses()) {
					str += qualificatoins.getLookupDetail().getDisplayName()
							+ "," + qualificatoins.getMajorSubject() + ","
							+ qualificatoins.getInstution() + ","
							+ qualificatoins.getCompletionYear() + ","
							+ qualificatoins.getResult() + ","
							+ qualificatoins.getResultType() + ",";

					str += "\n";
				}

				str += "\n\n";
				str += "Experiences, \n\n";
				str += "Previous Experience,Current Post,Job Title,Company Name,Start Date,End Date,Address,";
				str += "\n";

				for (Experiences experience : person.getExperienceses()) {
					str += experience.getPreviousExperience() + ","
							+ experience.getCurrentPost() + ","
							+ experience.getJobTitle() + ","
							+ experience.getCompanyName() + ","
							+ experience.getStartDate() + ","
							+ experience.getEndDate() + ",";

					str += "\n";
				}

				str += "\n\n";
				str += "Skills, \n\n";
				str += "Skill Type,Skill Level,Description,";
				str += "\n";

				for (Skills skill : person.getSkillses()) {
					str += skill.getLookupDetail().getDisplayName() + ","
							+ skill.getSkillLevel() + ","
							+ skill.getDescription() + "," + ",";

					str += "\n";
				}

				/*
				 * if (person.getPersonType().getPersonTypeId() == 2) {
				 * 
				 * str += "\n\n"; str +=
				 * "Name,Relationship,Address,Mobile,Phone,Email,Emergency,Dependent,Benefit"
				 * ; str += "\n";
				 * 
				 * }
				 */

				str += "\n\n";
				InputStream is = new ByteArrayInputStream(str.getBytes());
				fileInputStream = is;

			}

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printPersonDetailReportJasper() {
		try {

			Person person = personBL.getPersonService().getPersonDetails(
					Long.parseLong(personId + ""));
			PersonVO personVO = personBL.personConverterFromEntityTOVO(person);
			personVO.setIdentityVOs(IdentityVO
					.convertToVOList(new ArrayList<Identity>(person
							.getIdentities())));

			personDS = new ArrayList<PersonVO>();
			personDS.add(personVO);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			// jasperRptParams.put("identityVOs", personVO.getIdentityVOs());

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * fetches list of all employees if fromDate and toDate are null. If
	 * fromDate and toDate are not null then it will only fetches the list of
	 * employees who took leaves between the input date range.
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<JobAssignment> fetchListOfEmpWhoTookLeaves() throws Exception {

		List<JobAssignment> jobs = new ArrayList<JobAssignment>();
		Date fromDatee = null;
		Date toDatee = null;
		Boolean isActive = true;

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		jobs = hrBL
				.getAttendanceBL()
				.getJobAssignmentBL()
				.getJobAssignmentService()
				.getJobAssignmentByByDifferentValues(fromDatee, toDatee,
						implementation, isActive);
		return jobs;
	}

	public String getEmployeeList() {
		try {

			List<JobAssignment> jobs = new ArrayList<JobAssignment>();
			JSONObject jsonResponse = new JSONObject();

			jobs = fetchListOfEmpWhoTookLeaves();

			if (jobs != null && jobs.size() > 0) {
				jsonResponse.put("iTotalRecords", jobs.size());
				jsonResponse.put("iTotalDisplayRecords", jobs.size());
			}
			JSONArray data = new JSONArray();
			for (JobAssignment list : jobs) {
				JSONArray array = new JSONArray();
				array.add(list.getJobAssignmentId() + "");
				array.add(list.getPerson().getPersonId() + "");
				array.add(list.getPerson().getFirstName() + " "
						+ list.getPerson().getLastName());
				array.add(list.getDesignation().getDesignationName());
				array.add(list.getDesignation().getGrade().getGradeName());
				array.add(list.getCmpDeptLocation().getCompany()
						.getCompanyName());
				array.add(list.getCmpDeptLocation().getDepartment()
						.getDepartmentName());
				array.add(list.getCmpDeptLocation().getLocation()
						.getLocationName());
				array.add(list.getSwipeId() + "");
				array.add(list.getJob().getJobId() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveHistoryOfAnEmployeePrint() {
		try {

			if (selectedEmployeeJobId != null) {

				List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();
				LeaveProcessTO mainVO = new LeaveProcessTO();

				tempList.add(fetchEmployeeLeavesHistory(Long
						.parseLong(selectedEmployeeJobId)));

				mainVO.setSubList(tempList);
				employeesDS = new ArrayList<LeaveProcessTO>();
				employeesDS.add(mainVO);
				if (ServletActionContext.getRequest().getAttribute(
						"LEAVE_MASTER") != null) {
					ServletActionContext.getRequest().removeAttribute(
							"LEAVE_MASTER");
				}
				ServletActionContext.getRequest().setAttribute("LEAVE_MASTER",
						employeesDS);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveHistoryOfAnEmployeePDF() {
		try {

			employeesDS = new ArrayList<LeaveProcessTO>();

			LeaveProcessTO mainVO = new LeaveProcessTO();
			mainVO.setCreatedDate(DateFormat
					.convertSystemDateToString(new Date()));

			List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();

			if (selectedEmployeeJobId != null) {

				LeaveProcessTO leaveTO = fetchEmployeeLeavesHistory(Long
						.parseLong(selectedEmployeeJobId));

				tempList.add(leaveTO);
				mainVO.setSubList(tempList);

				mainVO.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				mainVO.setAnchorExpression("HTML".equals(getFormat()) ? "get_hr_employee_leave_history_pdf.action?format=PDF"
						: "#");

				employeesDS.add(mainVO);

				String ctxRealPath = ServletActionContext.getServletContext()
						.getRealPath("/");
				jasperRptParams.put(
						"AIOS_LOGO",
						"file:///"
								+ ctxRealPath.concat(File.separator).concat(
										"images" + File.separator
												+ "aiotech.jpg"));
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getLeaveHistoryOfAllEmployeePDF() {
		try {

			employeesDS = new ArrayList<LeaveProcessTO>();
			LeaveProcessTO mainVO = new LeaveProcessTO();
			mainVO.setCreatedDate(DateFormat
					.convertSystemDateToString(new Date()));

			List<JobAssignment> jobs = fetchListOfEmpWhoTookLeaves();
			List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();

			for (JobAssignment jobAssignment : jobs) {

				LeaveProcessTO leaveTO = fetchEmployeeLeavesHistory(jobAssignment
						.getJobAssignmentId());

				tempList.add(leaveTO);
			}

			mainVO.setSubList(tempList);

			employeesDS.add(mainVO);

			mainVO.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			mainVO.setAnchorExpression("HTML".equals(getFormat()) ? "get_hr_all_employee_leave_history_pdf.action?format=PDF"
					: "#");

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	LeaveProcessTO fetchEmployeeLeavesHistory(Long jobAssignmentId)
			throws Exception {

		Date fromDatee = null;
		Date toDatee = null;

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		LeaveProcessTO leaveTO = hrBL.getLeaveProcessBL().getLeaveHistory(
				jobAssignmentId, fromDatee, toDatee);

		return leaveTO;

	}

	public String getLeaveHistoryAllEmployeePrint() {
		try {

			employeesDS = new ArrayList<LeaveProcessTO>();
			LeaveProcessTO mainVO = new LeaveProcessTO();
			mainVO.setCreatedDate(DateFormat
					.convertSystemDateToString(new Date()));
			List<JobAssignment> jobs = fetchListOfEmpWhoTookLeaves();
			List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();

			for (JobAssignment jobAssignment : jobs) {

				tempList.add(fetchEmployeeLeavesHistory(jobAssignment
						.getJobAssignmentId()));

			}
			mainVO.setSubList(tempList);
			employeesDS.add(mainVO);

			if (ServletActionContext.getRequest().getAttribute("LEAVE_MASTER") != null) {
				ServletActionContext.getRequest().removeAttribute(
						"LEAVE_MASTER");
			}
			ServletActionContext.getRequest().setAttribute("LEAVE_MASTER",
					employeesDS);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String anEmloyeeLeaveHistoryReportXLS() {
		try {

			if (selectedEmployeeJobId != null) {

				LeaveProcessTO to = fetchEmployeeLeavesHistory(Long
						.parseLong(selectedEmployeeJobId));

				List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();
				tempList.add(to);
				InputStream is = new ByteArrayInputStream(prepateXLSData(
						tempList).getBytes());
				fileInputStream = is;

			}

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String allEmloyeesLeaveHistoryReportXLS() {
		try {
			List<JobAssignment> jobs = fetchListOfEmpWhoTookLeaves();
			List<LeaveProcessTO> tempList = new ArrayList<LeaveProcessTO>();

			for (JobAssignment jobAssignment : jobs) {
				tempList.add(fetchEmployeeLeavesHistory(jobAssignment
						.getJobAssignmentId()));

			}
			InputStream is = new ByteArrayInputStream(prepateXLSData(tempList)
					.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	private String prepateXLSData(List<LeaveProcessTO> list) throws Exception {

		String str = "";
		str = "Leave History Report\n\n";
		for (LeaveProcessTO to : list) {

			str += "Name," + to.getPersonName();
			str += "\nCompany," + to.getCompanyName() + "\nDepartment,"
					+ to.getDepartmentName() + "\nLoccation,"
					+ to.getLocationName() + "\n\nLeave History Information\n";

			if (to.getLeaveHistoryList() != null) {
				for (LeaveProcessTO leaveProcessTO : to.getLeaveHistoryList()) {

					str += "Leave Type,Start Date,End Date,Number of Day,Contact on Leave,Leave Status,Request Status,Requested Date,Reason";
					str += "\n";
					str += leaveProcessTO.getLeaveType() + ","
							+ leaveProcessTO.getFromDate() + ","
							+ leaveProcessTO.getToDate() + ","
							+ leaveProcessTO.getNumberOfDays() + ","
							+ leaveProcessTO.getContactOnLeave() + ","
							+ leaveProcessTO.getStatusStr() + ","
							+ leaveProcessTO.getIsApprove() + ","
							+ leaveProcessTO.getCreatedDate() + ","
							+ leaveProcessTO.getReason();

					str += "\n\n";
				}
			} else {
				str += "No Leaves\n\n\n";
			}

		}

		return str;
	}

	public String getAllLeaveReconciliationDateTableList() {
		try {

			List<LeaveReconciliation> reconciliations = new ArrayList<LeaveReconciliation>();
			JSONObject jsonResponse = new JSONObject();

			reconciliations = fetchListOfLeaveReconciliation();

			if (reconciliations != null && reconciliations.size() > 0) {
				jsonResponse.put("iTotalRecords", reconciliations.size());
				jsonResponse
						.put("iTotalDisplayRecords", reconciliations.size());
			}

			JSONArray data = new JSONArray();
			for (LeaveReconciliation list : reconciliations) {

				JSONArray array = new JSONArray();
				array.add(list.getLeaveReconciliationId() + "");
				array.add(list.getJobAssignment().getPerson().getPersonId()
						+ "");
				array.add(list.getJobAssignment().getPerson().getFirstName()
						+ " "
						+ list.getJobAssignment().getPerson().getLastName());
				array.add(list.getJobAssignment().getDesignation()
						.getDesignationName());

				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getCompany().getCompanyName());
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getDepartment().getDepartmentName());
				array.add(list.getJobAssignment().getCmpDeptLocation()
						.getLocation().getLocationName());

				array.add(list.getJobLeave().getLeave().getDisplayName());

				array.add(list.getProvidedDays().toString());
				array.add(list.getEligibleDays());
				array.add(list.getTakendDays());
				array.add(1);

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	public List<LeaveReconciliation> fetchListOfLeaveReconciliation()
			throws Exception {

		List<LeaveReconciliation> leaveReconciliations = new ArrayList<LeaveReconciliation>();
		Date fromDatee = null;
		Date toDatee = null;
		Boolean isActive = true;

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		leaveReconciliations = hrBL
				.getAttendanceBL()
				.getLeaveProcessService()
				.getLeaveReconciliationsByDiffValuesWithAllEmpDetails(
						fromDatee, toDatee, isActive, implementation);
		return leaveReconciliations;
	}

	public String leaveBalanceRedirect() {
		try {
			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			List<JobAssignment> jobAssignments = hrBL.getJobAssignmentBL()
					.getJobAssignmentService()
					.getAllActiveJobAssignment(implementation);
			List<JobAssignmentVO> jobAssignmentVOs = hrBL.getJobAssignmentBL()
					.convertEntitiesTOVOs(jobAssignments);
			List<Leave> leaves = hrBL.getLeaveBL().getLeaveService()
					.getActiveLeaveList(implementation);
			ServletActionContext.getRequest().setAttribute("JOB_ASSIGNMENTS",
					jobAssignmentVOs);
			ServletActionContext.getRequest().setAttribute("LEAVES", leaves);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public String printLeaveBalance() throws Exception {

		List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
		Date currentDate = new Date();

		Boolean isActive = true;

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			currentDate = DateFormat.convertStringToDate(fromDate);
		}

		String temp[] = null;
		if (selectedPersonId != null && !selectedPersonId.equals("null")
				&& !selectedPersonId.equals("undefined")) {
			temp = selectedPersonId.split(",");
		}
		Long leaveId = null;
		if (ServletActionContext.getRequest().getParameter("leaveId") != null
				&& !ServletActionContext.getRequest().getParameter("leaveId")
						.equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"leaveId")) > 0) {
			leaveId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("leaveId"));
		}

		Map<Long, Long> maps = new HashMap<Long, Long>();

		for (String string : temp) {
			maps.put(Long.valueOf(string), Long.valueOf(string));
		}

		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		List<JobAssignment> jobAssignmentTemps = hrBL.getJobAssignmentBL()
				.getJobAssignmentService()
				.getAllActiveJobAssignment(implementation);

		for (JobAssignment jobAssignment : jobAssignmentTemps) {
			if (maps.size() == 0
					|| maps.containsKey(jobAssignment.getJobAssignmentId())) {
				jobAssignments.add(jobAssignment);
			}
		}

		List<LeaveProcessTO> leaveProcessList = new ArrayList<LeaveProcessTO>();
		for (JobAssignment jobAssignment : jobAssignments) {
			if (maps.containsKey(jobAssignment.getJobAssignmentId())) {
				List<LeaveProcessTO> leaveProcessListTemp = hrBL
						.getLeaveProcessBL().findLeaveAvailability(
								jobAssignment, currentDate);
				if (leaveProcessListTemp != null
						&& leaveProcessListTemp.size() > 0) {
					for (LeaveProcessTO leaveProcessTO : leaveProcessListTemp) {
						if (leaveId == null
								|| (long) leaveProcessTO.getLeaveId() == (long) leaveId)
							leaveProcessList.add(leaveProcessTO);

					}
				}

			}
		}

		ServletActionContext.getRequest().setAttribute("LEAVE_RECONCILIATION",
				leaveProcessList);
		ServletActionContext.getRequest().setAttribute("printedOn",
				DateFormat.convertSystemDateToString(new Date()));
		return SUCCESS;
	}

	public String printLeaveReconciliationReportXLS() {
		try {

			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			jobAssignments = hrBL
					.getAttendanceBL()
					.getJobService()
					.getEmployeesLeaveReconciliationsByCriteria(fromDatee,
							toDatee, isActive, implementation,
							Long.parseLong(selectedEmployeeJobId));

			List<JobAssignmentVO> jobAssignmentVOs = hrBL.getJobAssignmentBL()
					.convertEntitiesTOVOs(jobAssignments);

			String str = "";

			for (JobAssignmentVO ja : jobAssignmentVOs) {

				str += "Personal Detail \n\n";

				str += "Employee," + ja.getPersonName() + "\n";
				str += "Designation," + ja.getDesignationName() + "\n";
				str += "Company," + ja.getCompanyName() + "\n";
				str += "Department," + ja.getDepartmentName() + "\n";
				str += "Location" + ja.getLocationName() + "\n";

				str += "\n\n";

				str += "Leave Reconciliation, \n\n";

				str += "Leave Type,Provided Days,Eligible Days,Taken Days,Available Days";

				str += "\n";

				for (LeaveReconciliation rec : ja.getLeaveReconciliations()) {
					str += rec.getLeave().getLookupDetail().getDisplayName()
							+ "," + rec.getProvidedDays() + ","
							+ rec.getEligibleDays() + "," + rec.getTakendDays()
							+ ","
							+ (rec.getEligibleDays() - rec.getTakendDays())
							+ ",";
					str += "\n";
				}

				str += "\n\n";
			}

			str += "\n\n";
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printLeaveReconciliationReportPDF() {
		try {

			List<JobAssignment> jobAssignments = new ArrayList<JobAssignment>();
			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			jobAssignments = hrBL
					.getAttendanceBL()
					.getJobService()
					.getEmployeesLeaveReconciliationsByCriteria(fromDatee,
							toDatee, isActive, implementation,
							Long.parseLong(selectedEmployeeJobId));

			List<JobAssignmentVO> jobAssignmentVOs = hrBL.getJobAssignmentBL()
					.convertEntitiesTOVOs(jobAssignments);
			jobAssignmentDS = jobAssignmentVOs;

			/*
			 * mainVO.setLinkLabel("HTML".equals(getFormat()) ? "Download" :
			 * ""); mainVO.setAnchorExpression("HTML".equals(getFormat()) ?
			 * "get_hr_all_employee_leave_history_pdf.action?format=PDF" : "#");
			 */

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getAllSwipeInOut() {
		try {

			List<SwipeInOutVO> swipeInOuts = new ArrayList<SwipeInOutVO>();
			JSONObject jsonResponse = new JSONObject();

			swipeInOuts = fetchAttendanceSwipeList();

			if (swipeInOuts != null && swipeInOuts.size() > 0) {
				jsonResponse.put("iTotalRecords", swipeInOuts.size());
				jsonResponse.put("iTotalDisplayRecords", swipeInOuts.size());
			}

			JSONArray data = new JSONArray();
			for (SwipeInOutVO swipe : swipeInOuts) {

				JSONArray array = new JSONArray();
				array.add(swipe.getSwipeInOutId() + "");
				array.add(swipe.getEmployeeName());
				array.add(swipe.getDesignation());
				array.add(swipe.getSwipeId());
				array.add(swipe.getAttendanceDate().toString());
				array.add(swipe.getTimeInOutType());
				array.add(swipe.getTimeInOut().toString());
				array.add(swipe.getDoorNumber());

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	private List<SwipeInOutVO> fetchAttendanceSwipeList() {
		List<SwipeInOutVO> swipeInOuts = new ArrayList<SwipeInOutVO>();
		try {
			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;
			Long jobAssignId = null;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			if (selectedEmployeeJobId != null
					&& !selectedEmployeeJobId.equals("")
					&& !selectedEmployeeJobId.equals("undefined")
					&& !selectedEmployeeJobId.equals("0")) {
				jobAssignId = Long.parseLong(selectedEmployeeJobId);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			swipeInOuts = hrBL.getAttendanceBL().getFilteredSwipeInOutsVO(
					jobAssignId, fromDatee, toDatee, implementation);
			/*
			 * swipeInOuts = hrBL
			 * .getAttendanceBL().getAttendanceService().getSwipeInOutsByCriteria
			 * (fromDatee, toDatee, implementation);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return swipeInOuts;
	}

	public String printAttendanceSwipeReport() throws Exception {

		List<SwipeInOutVO> swipeInOuts = fetchAttendanceSwipeList();

		ServletActionContext.getRequest().setAttribute("SWIPEINOUTS",
				swipeInOuts);

		return SUCCESS;
	}

	public String printAttendanceSwipeReportXLS() {
		try {

			List<SwipeInOutVO> swipeInOuts = fetchAttendanceSwipeList();

			String str = "";
			str += "Attendance Swipe \n\n";

			str += "Employee,Designation,Swipe Id,Attendance Date,Swipe Type(In/Out),Time, Door Number";

			str += "\n";

			for (SwipeInOutVO swipe : swipeInOuts) {
				str += swipe.getEmployeeName() + "," + swipe.getDesignation()
						+ "," + swipe.getSwipeId() + ","
						+ swipe.getAttendanceDate() + ","
						+ swipe.getTimeInOutType() + "," + swipe.getTimeInOut()
						+ "," + swipe.getDoorNumber() + ",";
				str += "\n";
			}

			str += "\n\n";
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printAttendanceSwipeReportPDF() {
		try {

			List<SwipeInOutVO> swipeInOuts = fetchAttendanceSwipeList();

			attendanceSwipeDS = swipeInOuts;

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String loadPayrollTransactionReportCriteria() {

		try {
			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}

			List<PayrollElement> elements = hrEnterpriseService
					.getPayrollService().getAllPayrollElement(implementation);

			String[] nameOfMonth = new DateFormatSymbols().getMonths();
			ServletActionContext.getRequest().setAttribute("PAYMONTH_LIST",
					nameOfMonth);

			ServletActionContext.getRequest()
					.setAttribute("ELEMENTS", elements);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;

	}

	public String getPayrollTransactions() {
		try {

			List<PayrollTransactionVO> payrollTransactionVOs = new ArrayList<PayrollTransactionVO>();
			JSONObject jsonResponse = new JSONObject();

			payrollTransactionVOs = fetchPayrollTransactionsReportData();

			if (payrollTransactionVOs != null
					&& payrollTransactionVOs.size() > 0) {
				jsonResponse.put("iTotalRecords", payrollTransactionVOs.size());
				jsonResponse.put("iTotalDisplayRecords",
						payrollTransactionVOs.size());
			}

			JSONArray data = new JSONArray();
			for (PayrollTransactionVO trans : payrollTransactionVOs) {

				JSONArray array = new JSONArray();

				array.add(trans.getEmployeeName());
				array.add(trans.getDesignation());
				array.add(trans.getElementName());
				array.add(trans.getElementType());
				array.add(trans.getPayMonth());
				array.add(trans.getTransAmount());

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	private List<PayrollTransactionVO> fetchPayrollTransactionsReportData()
			throws Exception {

		String month = null;
		Long element = null;
		Byte isApprove = (byte) WorkflowConstants.Status.Published.getCode();

		if (selectedType != null && !selectedType.equals("null")
				&& !selectedType.equals("0")) {
			element = Long.parseLong(selectedType);
		}

		if (selectedMonth != null && !selectedMonth.equals("null")
				&& !selectedMonth.equals("0")) {
			month = selectedMonth + "," + selectedYear;
		}

		Implementation implementation = null;
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}

		List<PayrollTransaction> transactions = hrEnterpriseService
				.getPayrollService().getPayrollTransactionsByDiffValues(
						element, month, implementation, isApprove);

		List<PayrollTransactionVO> vos = new ArrayList<PayrollTransactionVO>();
		for (PayrollTransaction payrollTransaction : transactions) {
			vos.add(PayrollTransactionVO.convertToVO(payrollTransaction));
		}

		return vos;
	}

	public String getPayrollTransactionsPrintout() {

		try {

			ServletActionContext.getRequest().setAttribute("TRANSACTIONS",
					fetchPayrollTransactionsReportData());
			double amount = 0.0;
			for (PayrollTransactionVO vo : fetchPayrollTransactionsReportData()) {
				amount += AIOSCommons.formatAmountToDouble(vo.getTransAmount());
			}
			ServletActionContext.getRequest().setAttribute("totalAmount",
					AIOSCommons.formatAmount(amount));
			/*
			 * ServletActionContext.getRequest().setAttribute(
			 * "PURCHASE_DETAIL_INFO", purchaseDetails);
			 */

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getPayrollTransactionsXLS() {
		try {

			List<PayrollTransactionVO> transactions = fetchPayrollTransactionsReportData();

			String str = "";
			str += "Payroll Transactions Report \n\n";

			str += "Employee Name,Designation,Pay Element,Element Type,Pay Month, Amount";

			str += "\n";

			for (PayrollTransactionVO vo : transactions) {
				str += vo.getEmployeeName() + "," + vo.getDesignation() + ","
						+ vo.getElementName() + "," + vo.getElementType() + ","
						+ vo.getPayMonth() + "," + vo.getTransAmount() + ",";
				str += "\n";
			}

			str += "\n\n";
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String getPayrollTransactionsPDF() {
		try {

			List<PayrollTransactionVO> tempList = fetchPayrollTransactionsReportData();

			payrollTransactionDS = new ArrayList<PayrollTransactionVO>();

			PayrollTransactionVO vo = new PayrollTransactionVO();

			vo.setTransactions(tempList);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			vo.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			vo.setAnchorExpression("HTML".equals(getFormat()) ? "show_payroll_transaction_PDF.action?selectedType="
					+ selectedType
					+ "&selectedMonth="
					+ selectedMonth
					+ "selectedYear" + selectedYear + "&format=PDF"

					: "#");

			payrollTransactionDS.add(vo);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	private List<AssetUsage> fetchAssetUsageList() {
		List<AssetUsage> assetUsages = new ArrayList<AssetUsage>();
		try {
			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;

			Long usageId = null;
			Long productId = null;
			Long personId = null;
			Long locationId = null;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			if (selectedAssetUsageId != null
					&& !selectedAssetUsageId.equals("null")
					&& !selectedAssetUsageId.equals("undefined")
					&& !selectedAssetUsageId.equals("0")) {
				usageId = Long.parseLong(selectedAssetUsageId);
			}

			if (selectedPersonId != null && !selectedPersonId.equals("null")
					&& !selectedPersonId.equals("undefined")
					&& !selectedPersonId.equals("0")) {
				personId = Long.parseLong(selectedPersonId);
			}

			if (selectedProductId != null && !selectedProductId.equals("null")
					&& !selectedProductId.equals("undefined")
					&& !selectedProductId.equals("0")) {
				productId = Long.parseLong(selectedProductId);
			}

			if (selectedLocationId != null
					&& !selectedLocationId.equals("null")
					&& !selectedLocationId.equals("undefined")
					&& !selectedLocationId.equals("0")) {
				locationId = Long.parseLong(selectedLocationId);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			assetUsages = hrEnterpriseService.getAssetUsageService()
					.getAssetUssageByCriteria(personId, productId, locationId,
							usageId, fromDatee, toDatee, implementation);
			/*
			 * swipeInOuts = hrBL
			 * .getAttendanceBL().getAttendanceService().getSwipeInOutsByCriteria
			 * (fromDatee, toDatee, implementation);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assetUsages;
	}

	public String getAllAssetUsage() {
		try {

			List<AssetUsage> assetUsages = new ArrayList<AssetUsage>();
			JSONObject jsonResponse = new JSONObject();

			assetUsages = fetchAssetUsageList();

			if (assetUsages != null && assetUsages.size() > 0) {
				jsonResponse.put("iTotalRecords", assetUsages.size());
				jsonResponse.put("iTotalDisplayRecords", assetUsages.size());
			}

			JSONArray data = new JSONArray();
			for (AssetUsage asset : assetUsages) {

				JSONArray array = new JSONArray();
				array.add(asset.getAssetUsageId() + "");
				if (asset.getPersonByPersonId() != null)
					array.add(asset.getPersonByPersonId().getFirstName() + " "
							+ asset.getPersonByPersonId().getLastName() + "");
				else
					array.add("-NA-");

				if (asset.getCmpDeptLocation() != null
						&& asset.getCmpDeptLocation().getDepartment() != null) {
					array.add(asset.getCmpDeptLocation().getCompany()
							.getCompanyName()
							+ "||"
							+ asset.getCmpDeptLocation().getDepartment()
									.getDepartmentName()
							+ "||"
							+ asset.getCmpDeptLocation().getLocation()
									.getLocationName());
				} else {
					array.add(asset.getCmpDeptLocation().getCompany()
							.getCompanyName()
							+ "||"
							+ asset.getCmpDeptLocation().getLocation()
									.getLocationName());
				}

				array.add(asset.getProduct().getProductName());
				array.add(asset.getAllocatedDate().toString());
				array.add("");

				if (asset.getAssetStatus() != null
						&& asset.getAssetStatus() == 1) {
					array.add("ALLOCATED");
				} else if (asset.getAssetStatus() != null
						&& asset.getAssetStatus() == 2) {
					array.add("RETURNED");
				} else if (asset.getAssetStatus() != null
						&& asset.getAssetStatus() == 3) {
					array.add("DAMAGED");
				} else if (asset.getAssetStatus() != null
						&& asset.getAssetStatus() == 4) {
					array.add("LOST");
				} else {
					array.add("-NA-");
				}

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	public String printAssetUsageReport() throws Exception {

		List<AssetUsage> assetUsages = fetchAssetUsageList();
		List<AssetUsageVO> assetUsageVOs = AssetUsageVO
				.convertToVOList(assetUsages);

		ServletActionContext.getRequest().setAttribute("ASSET_USAGE",
				assetUsageVOs);

		return SUCCESS;
	}

	public String printAssetUsageReportXLS() {
		try {

			List<AssetUsage> assetUsages = fetchAssetUsageList();
			List<AssetUsageVO> assetUsageVOs = AssetUsageVO
					.convertToVOList(assetUsages);

			String str = "";
			str += "Company Asset Usage \n\n";

			str += "Employee Name,Departmet & Branch,Asset,Issue Date,Return Date,Status";

			str += "\n";

			for (AssetUsageVO asset : assetUsageVOs) {
				str += asset.getEmployeeName() + ","
						+ asset.getCompanyNDepartmentName() + ","
						+ asset.getAssetName() + ","
						+ asset.getAllocatedDate().toString() + ","
						+ asset.getReturnDate() + ","
						+ asset.getStrAssetStatus() + ",";
				str += "\n";
			}

			str += "\n\n";
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String printAssetUsageReportPDF() {
		try {

			List<AssetUsage> assetUsages = fetchAssetUsageList();
			List<AssetUsageVO> assetUsageVOs = AssetUsageVO
					.convertToVOList(assetUsages);

			assetUsageDS = assetUsageVOs;

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String loadDocumentHandoverReportCriteria() {
		try {
			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			List<Company> companies = hrEnterpriseService.getCompanyService()
					.getAllCompany(implementation);
			ServletActionContext.getRequest().setAttribute("COMPANIES",
					companies);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public List<IdentityAvailability> fetchDocumentHandoverReportData() {

		List<IdentityAvailability> documents = new ArrayList<IdentityAvailability>();
		try {

			Date fromDatee = null;
			Date toDatee = null;
			Boolean isActive = true;
			Long personId = null;
			Long companyId = null;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}
			if (selectedEmployeeJobId != null
					&& !selectedEmployeeJobId.equals("null")
					&& !selectedEmployeeJobId.equals("0")) {
				personId = Long.parseLong(selectedEmployeeJobId);
			}
			if (selectedCompanyId != null && !selectedCompanyId.equals("null")
					&& !selectedCompanyId.equals("0")) {
				companyId = Long.parseLong(selectedCompanyId);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}

			documents = hrEnterpriseService.getIdentityAvailabilityService()
					.getIdentityAvailabilitiesListByDifferentValues(personId,
							companyId, fromDatee, toDatee, isActive,
							implementation);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return documents;
	}

	public String populateDocumentHandoverTable() {

		String result = ERROR;
		try {
			aaData = new ArrayList<Object>();
			aaData.addAll(fetchDocumentHandoverReportData());

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String documentHandoverReportXLS() {
		try {

			List<IdentityAvailability> docuements = fetchDocumentHandoverReportData();

			String str = "";

			str = "Document Handover Report\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date\n";
			str += fromDate.equals("null") ? "," : fromDate + ",";
			str += toDate.equals("null") ? "" : toDate + "\n\n";
			if (docuements != null && docuements.size() > 0) {
				str += "Document Belongs(Person/Company),Document Type, Given Date,Expected Return Date,Actual Return Date,Purpose";
				str += "\n";

				for (IdentityAvailability identity : docuements) {

					String from = "";

					/*
					 * if (identity.getPerson() != null) {
					 * 
					 * from = identity.getIdentity().getPerson() .getFirstName()
					 * + " " + identity.getIdentity().getPerson()
					 * .getLastName(); } else {
					 * 
					 * from = identity.getIdentity().getCompany()
					 * .getCompanyName(); }
					 */

					str += from
							+ ","
							+ identity.getIdentity().getLookupDetail()
									.getDisplayName() + ","
							+ identity.getHandoverDate() + ","
							+ identity.getExpectedReturnDate() + ","
							+ identity.getActualReturnDate() + ","
							+ identity.getPurpose();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String documentHandoverReporPrintOut() {
		try {
			List<IdentityAvailability> documents = fetchDocumentHandoverReportData();

			ServletActionContext.getRequest().setAttribute(
					"DOCS",
					hrBL.getIdentityAvailabilityBL().convertEntitiesTOVOs(
							documents));
			/*
			 * ServletActionContext.getRequest().setAttribute(
			 * "PURCHASE_DETAIL_INFO", purchaseDetails);
			 */

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String documentHandoverReporPDF() {
		try {
			List<IdentityAvailability> documents = fetchDocumentHandoverReportData();

			identityAvailabilityDS = hrBL.getIdentityAvailabilityBL()
					.convertEntitiesTOVOs(documents);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String loadPersonBankDetailsReportCriteria() {
		try {
			/*
			 * Implementation implementation = null; HttpSession session =
			 * ServletActionContext.getRequest() .getSession(); if
			 * (session.getAttribute("THIS") != null) { implementation =
			 * (Implementation) session.getAttribute("THIS"); }
			 */

			List<LookupDetail> companyTypeList = companyBL.getLookupMasterBL()
					.getActiveLookupDetails("COMPANY_TYPE", false);
			ServletActionContext.getRequest().setAttribute("COMPANY_TYPES",
					companyTypeList);

			/*
			 * List<PersonType> types = personBL.getPersonService()
			 * .getAllPersonType();
			 */

			List<LookupDetail> personTypeList = personBL.getLookupMasterBL()
					.getActiveLookupDetails("PERSON_TYPE", true);

			ServletActionContext.getRequest().setAttribute("PERSON_TYPES",
					personTypeList);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public List<PersonBankVO> fetchPersonBankReportData() {

		List<PersonBankVO> vos = new ArrayList<PersonBankVO>();
		try {

			Long personType = null;
			Integer companyType = null;
			Boolean status = null;

			if (this.personType != null && !this.personType.equals("null")
					&& !this.personType.equals("undefined")
					&& !this.personType.equals("0")) {
				personType = Long.parseLong(this.personType);
			}
			if (this.companyType != null && !this.companyType.equals("null")
					&& !this.companyType.equals("undefined")
					&& !this.companyType.equals("0")) {
				companyType = Integer.parseInt(this.companyType);
			}

			if (this.status != null && !this.status.equals("null")
					&& !this.status.equals("undefined")
					&& !this.status.equals("0")) {
				status = Boolean.parseBoolean(this.status);
			}

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}

			List<PersonBank> personBanks = hrEnterpriseService
					.getPersonBankService()
					.getPersonBanksByCompanyTypePersonTypeAndStatus(personType,
							companyType, status, implementation);

			for (PersonBank personBank : personBanks) {
				PersonBankVO vo = new PersonBankVO(personBank);
				vos.add(vo);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return vos;
	}

	public String loadPersonBankReportTableData() {

		try {

			JSONObject jsonResponse = new JSONObject();

			List<PersonBankVO> vos = fetchPersonBankReportData();

			if (vos != null && vos.size() > 0) {
				jsonResponse.put("iTotalRecords", vos.size());
				jsonResponse.put("iTotalDisplayRecords", vos.size());
			}

			JSONArray data = new JSONArray();
			for (PersonBankVO vo : vos) {

				JSONArray array = new JSONArray();
				array.add(vo.getAccountHolderName() + "");

				array.add(vo.getBankName());
				array.add(vo.getAccountNumber());
				array.add(vo.getIban());
				array.add(vo.getRoutingCode());
				array.add(vo.getBranchName());
				array.add(vo.getAccountTitle());

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;

	}

	public String personBankDetailsReporPrintOut() {
		try {
			List<PersonBankVO> details = fetchPersonBankReportData();

			ServletActionContext.getRequest().setAttribute("DETAILS", details);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String personBankDetialsReportXLS() {
		try {

			List<PersonBankVO> details = fetchPersonBankReportData();

			String str = "";

			str = "Person Bank Details Report\n\n";

			if (details != null && details.size() > 0) {
				str += "Account Holder,Bank, Account Number,IBAN,Routing Code,Branch,Title";
				str += "\n";

				for (PersonBankVO vo : details) {

					str += vo.getAccountHolderName() + "," + vo.getBankName()
							+ "," + vo.getAccountNumber() + "," + vo.getIban()
							+ "," + vo.getRoutingCode() + ","
							+ vo.getBranchName() + " " + vo.getAccountTitle();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String personBankDetialsReportPDF() {
		try {

			personBankDS = fetchPersonBankReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// ===============Salary Report====================
	public String loadSalaryReportCriteria() {
		try {
			String[] nameOfMonth = new DateFormatSymbols().getMonths();
			ServletActionContext.getRequest().setAttribute("PAYMONTH_LIST",
					nameOfMonth);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public List<PayrollVO> fetchSalaryReportData() {

		List<PayrollVO> vos = new ArrayList<PayrollVO>();
		try {

			Implementation implementation = null;
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			if (session.getAttribute("THIS") != null) {
				implementation = (Implementation) session.getAttribute("THIS");
			}
			String payMonth = null;
			if (selectedMonth != null && !selectedMonth.equals("")
					&& selectedYear != null) {
				payMonth = selectedMonth + "," + selectedYear;
			}
			List<Payroll> payrolls = hrEnterpriseService.getPayrollService()
					.getAllPayroll(implementation, payMonth);
			if (payrolls != null && payrolls.size() > 0) {
				vos = hrBL.getPayrollBL().payrollIntoVOs(payrolls);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return vos;
	}

	public String loadSalaryReportTableData() {

		try {

			JSONObject jsonResponse = new JSONObject();

			List<PayrollVO> vos = fetchSalaryReportData();

			if (vos != null && vos.size() > 0) {
				jsonResponse.put("iTotalRecords", vos.size());
				jsonResponse.put("iTotalDisplayRecords", vos.size());
			}

			JSONArray data = new JSONArray();
			Payroll list = null;
			if (vos != null)
				for (PayrollVO payrollVO : vos) {
					list = payrollVO.getPayroll();
					JSONArray array = new JSONArray();
					array.add(list.getPayrollId() + "");
					array.add(list.getJobAssignment().getPerson()
							.getFirstName()
							+ " "
							+ list.getJobAssignment().getPerson().getLastName());
					array.add(list.getJobAssignment().getCmpDeptLocation()
							.getCompany().getCompanyName()
							+ "");
					array.add(list.getJobAssignment().getCmpDeptLocation()
							.getDepartment().getDepartmentName()
							+ "");
					array.add(list.getJobAssignment().getCmpDeptLocation()
							.getLocation().getLocationName()
							+ "");
					array.add(DateFormat.convertDateToString(list
							.getStartDate().toString()) + "");
					array.add(DateFormat.convertDateToString(list.getEndDate()
							.toString()) + "");
					array.add(list.getPayMonth() + "");
					array.add(list.getNetAmount().toString() + "");
					array.add(DateFormat.convertDateToString(list
							.getCreatedDate().toString()) + "");
					if (list.getStatus() != null)
						array.add(Constants.HR.PayStatus.get(list.getStatus())
								.name());
					else
						array.add("");

					if (list.getIsApprove() != null)
						array.add(WorkflowConstants.Status.get(
								list.getIsApprove()).name()
								+ "");
					else
						array.add("");

					data.add(array);
				}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;

	}

	public String salaryReportPrintOut() {
		try {
			List<PayrollVO> payrollVOs = null;
			PayrollVO payrollVO = new PayrollVO();
			payrollVOs = fetchSalaryReportData();
			// payrollVO.setPayMonth(payMonth + "," + payYear);
			payrollVO.setCreatedDateStr(DateFormat
					.convertSystemDateToString(new Date()));
			Double totalBasic = 0.0;
			Double totalEarning = 0.0;
			Double totalDeduction = 0.0;
			Double totalGross = 0.0;
			Double totalNet = 0.0;
			if (payrollVOs != null && payrollVOs.size() > 0) {
				for (PayrollVO payrollVOTemp : payrollVOs) {
					if (payrollVOTemp.getBasic() != null)
						totalBasic = totalBasic
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getBasic());

					if (payrollVOTemp.getGrossPay() != null)
						totalGross = totalGross
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getGrossPay());

					if (payrollVOTemp.getOtherEarning() != null)
						totalEarning = totalEarning
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getOtherEarning());
					if (payrollVOTemp.getOtherDeduction() != null)
						totalDeduction = totalDeduction
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getOtherDeduction());
					if (payrollVOTemp.getNetAmount() != null)
						totalNet = totalNet
								+ AIOSCommons
										.formatAmountToDouble(payrollVOTemp
												.getNetAmount());
				}
				payrollVO.setTotalBasic(AIOSCommons.formatAmount(Math
						.round(totalBasic)));
				payrollVO.setTotalGross(AIOSCommons.formatAmount(Math
						.round(totalGross)));
				payrollVO.setTotalEarning(AIOSCommons.formatAmount(Math
						.round(totalEarning)));
				payrollVO.setTotalDeduction(AIOSCommons.formatAmount(Math
						.round(totalDeduction)));
				payrollVO.setTotalNet(AIOSCommons.formatAmount(Math
						.round(totalNet)));

				payrollVO.setCompanyName(payrollVOs.get(0).getCompanyName());

				Collections.sort(payrollVOs, new Comparator<PayrollVO>() {
					public int compare(PayrollVO m1, PayrollVO m2) {
						return m1.getEmployeeName().compareTo(
								m2.getEmployeeName());
					}
				});

			}

			ServletActionContext.getRequest().setAttribute("PAYROLL_MASTER",
					payrollVO);
			ServletActionContext.getRequest().setAttribute("PAYROLL_DETAILS",
					payrollVOs);
			ServletActionContext.getRequest().setAttribute("printCall",
					printCall);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String salaryReportXLS() {
		try {

			List<PersonBankVO> details = fetchPersonBankReportData();

			String str = "";

			str = "Person Bank Details Report\n\n";

			if (details != null && details.size() > 0) {
				str += "Account Holder,Bank, Account Number,IBAN,Routing Code,Branch,Title";
				str += "\n";

				for (PersonBankVO vo : details) {

					str += vo.getAccountHolderName() + "," + vo.getBankName()
							+ "," + vo.getAccountNumber() + "," + vo.getIban()
							+ "," + vo.getRoutingCode() + ","
							+ vo.getBranchName() + " " + vo.getAccountTitle();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String salaryReportPDF() {
		try {

			personBankDS = fetchPersonBankReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// ===============Getters Setters====================

	public HrEnterpriseService getHrEnterpriseService() {
		return hrEnterpriseService;
	}

	public void setHrEnterpriseService(HrEnterpriseService hrEnterpriseService) {
		this.hrEnterpriseService = hrEnterpriseService;
	}

	public HrBL getHrBL() {
		return hrBL;
	}

	public void setHrBL(HrBL hrBL) {
		this.hrBL = hrBL;
	}

	public List<LeaveProcessTO> getRptList() {
		return rptList;
	}

	public void setRptList(List<LeaveProcessTO> rptList) {
		this.rptList = rptList;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getApprovalFlag() {
		return approvalFlag;
	}

	public void setApprovalFlag(String approvalFlag) {
		this.approvalFlag = approvalFlag;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public int getTempVar() {
		return tempVar;
	}

	public void setTempVar(int tempVar) {
		this.tempVar = tempVar;
	}

	public String getTempString() {
		return tempString;
	}

	public void setTempString(String tempString) {
		this.tempString = tempString;
	}

	public String getTempString1() {
		return tempString1;
	}

	public void setTempString1(String tempString1) {
		this.tempString1 = tempString1;
	}

	public String getTempString2() {
		return tempString2;
	}

	public void setTempString2(String tempString2) {
		this.tempString2 = tempString2;
	}

	public long getTempLong() {
		return tempLong;
	}

	public void setTempLong(long tempLong) {
		this.tempLong = tempLong;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public Map<Object, Boolean> getExportParams() {
		return exportParams;
	}

	public void setExportParams(Map<Object, Boolean> exportParams) {
		this.exportParams = exportParams;
	}

	public JasperPrint getJasperPrint() {
		return jasperPrint;
	}

	public void setJasperPrint(JasperPrint jasperPrint) {
		this.jasperPrint = jasperPrint;
	}

	public List<PayrollTO> getPayrollList() {
		return payrollList;
	}

	public void setPayrollList(List<PayrollTO> payrollList) {
		this.payrollList = payrollList;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public List<AttendanceTO> getAttendanceList() {
		return attendanceList;
	}

	public void setAttendanceList(List<AttendanceTO> attendanceList) {
		this.attendanceList = attendanceList;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public List<IdentityVO> getDocumentsDS() {
		return documentsDS;
	}

	public void setDocumentsDS(List<IdentityVO> documentsDS) {
		this.documentsDS = documentsDS;
	}

	public IdentityAvailabilityBL getAvailabilityBL() {
		return availabilityBL;
	}

	public void setAvailabilityBL(IdentityAvailabilityBL availabilityBL) {
		this.availabilityBL = availabilityBL;
	}

	public PersonBL getPersonBL() {
		return personBL;
	}

	public void setPersonBL(PersonBL personBL) {
		this.personBL = personBL;
	}

	public List<HrPersonalDetailsTO> getPersonalDetailsList() {
		return personalDetailsList;
	}

	public void setPersonalDetailsList(
			List<HrPersonalDetailsTO> personalDetailsList) {
		this.personalDetailsList = personalDetailsList;
	}

	public String getPersonTypes() {
		return personTypes;
	}

	public void setPersonTypes(String personTypes) {
		this.personTypes = personTypes;
	}

	public int getDisabled() {
		return disabled;
	}

	public void setDisabled(int disabled) {
		this.disabled = disabled;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getPersonGroup() {
		return personGroup;
	}

	public void setPersonGroup(String personGroup) {
		this.personGroup = personGroup;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getEffectiveDatesFrom() {
		return effectiveDatesFrom;
	}

	public void setEffectiveDatesFrom(String effectiveDatesFrom) {
		this.effectiveDatesFrom = effectiveDatesFrom;
	}

	public String getEffectiveDatesTo() {
		return effectiveDatesTo;
	}

	public void setEffectiveDatesTo(String effectiveDatesTo) {
		this.effectiveDatesTo = effectiveDatesTo;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<PersonVO> getPersonDS() {
		return personDS;
	}

	public void setPersonDS(List<PersonVO> personDS) {
		this.personDS = personDS;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public ArrayList<Identity> getIdentitys() {
		return identitys;
	}

	public void setIdentitys(ArrayList<Identity> identitys) {
		this.identitys = identitys;
	}

	public ArrayList<Dependent> getDependents() {
		return dependents;
	}

	public void setDependents(ArrayList<Dependent> dependents) {
		this.dependents = dependents;
	}

	public String getSelectedEmployeeJobId() {
		return selectedEmployeeJobId;
	}

	public void setSelectedEmployeeJobId(String selectedEmployeeJobId) {
		this.selectedEmployeeJobId = selectedEmployeeJobId;
	}

	public List<LeaveProcessTO> getEmployeesDS() {
		return employeesDS;
	}

	public void setEmployeesDS(List<LeaveProcessTO> employeesDS) {
		this.employeesDS = employeesDS;
	}

	public List<JobAssignmentVO> getJobAssignmentDS() {
		return jobAssignmentDS;
	}

	public void setJobAssignmentDS(List<JobAssignmentVO> jobAssignmentDS) {
		this.jobAssignmentDS = jobAssignmentDS;
	}

	public List<SwipeInOutVO> getAttendanceSwipeDS() {
		return attendanceSwipeDS;
	}

	public void setAttendanceSwipeDS(List<SwipeInOutVO> attendanceSwipeDS) {
		this.attendanceSwipeDS = attendanceSwipeDS;
	}

	public String getSelectedCompanyId() {
		return selectedCompanyId;
	}

	public void setSelectedCompanyId(String selectedCompanyId) {
		this.selectedCompanyId = selectedCompanyId;
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSelectedMonth() {
		return selectedMonth;
	}

	public void setSelectedMonth(String selectedMonth) {
		this.selectedMonth = selectedMonth;
	}

	public String getSelectedYear() {
		return selectedYear;
	}

	public void setSelectedYear(String selectedYear) {
		this.selectedYear = selectedYear;
	}

	public List<PayrollTransactionVO> getPayrollTransactionDS() {
		return payrollTransactionDS;
	}

	public void setPayrollTransactionDS(
			List<PayrollTransactionVO> payrollTransactionDS) {
		this.payrollTransactionDS = payrollTransactionDS;
	}

	public List<AssetUsageVO> getAssetUsageDS() {
		return assetUsageDS;
	}

	public void setAssetUsageDS(List<AssetUsageVO> assetUsageDS) {
		this.assetUsageDS = assetUsageDS;
	}

	public String getSelectedPersonId() {
		return selectedPersonId;
	}

	public void setSelectedPersonId(String selectedPersonId) {
		this.selectedPersonId = selectedPersonId;
	}

	public String getSelectedProductId() {
		return selectedProductId;
	}

	public void setSelectedProductId(String selectedProductId) {
		this.selectedProductId = selectedProductId;
	}

	public String getSelectedLocationId() {
		return selectedLocationId;
	}

	public void setSelectedLocationId(String selectedLocationId) {
		this.selectedLocationId = selectedLocationId;
	}

	public String getSelectedAssetUsageId() {
		return selectedAssetUsageId;
	}

	public void setSelectedAssetUsageId(String selectedAssetUsageId) {
		this.selectedAssetUsageId = selectedAssetUsageId;
	}

	public List<IdentityAvailabilityVO> getIdentityAvailabilityDS() {
		return identityAvailabilityDS;
	}

	public void setIdentityAvailabilityDS(
			List<IdentityAvailabilityVO> identityAvailabilityDS) {
		this.identityAvailabilityDS = identityAvailabilityDS;
	}

	public List<PersonBankVO> getPersonBankDS() {
		return personBankDS;
	}

	public void setPersonBankDS(List<PersonBankVO> personBankDS) {
		this.personBankDS = personBankDS;
	}

	public String getPersonType() {
		return personType;
	}

	public void setPersonType(String personType) {
		this.personType = personType;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CompanyBL getCompanyBL() {
		return companyBL;
	}

	public void setCompanyBL(CompanyBL companyBL) {
		this.companyBL = companyBL;
	}

	public String getPrintCall() {
		return printCall;
	}

	public void setPrintCall(String printCall) {
		this.printCall = printCall;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

}
