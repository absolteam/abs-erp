package com.aiotech.aios.reporting.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.aiotech.aios.accounts.action.QuotationAction;
import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AccountGroupDetail;
import com.aiotech.aios.accounts.domain.entity.Aisle;
import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;
import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;
import com.aiotech.aios.accounts.domain.entity.AssetDepreciation;
import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditDetail;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationCharge;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DebitDetail;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.DiscountMethod;
import com.aiotech.aios.accounts.domain.entity.DiscountOption;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;
import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.LoanCharge;
import com.aiotech.aios.accounts.domain.entity.LoanDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;
import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;
import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;
import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.PromotionMethod;
import com.aiotech.aios.accounts.domain.entity.PromotionOption;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;
import com.aiotech.aios.accounts.domain.entity.QuotationDetail;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryPack;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.SalesOrderCharge;
import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;
import com.aiotech.aios.accounts.domain.entity.Shelf;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.StockPeriodic;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;
import com.aiotech.aios.accounts.domain.entity.vo.AccountsDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.AisleVO;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckInVO;
import com.aiotech.aios.accounts.domain.entity.vo.AssetCheckOutVO;
import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankAccountVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankDepositDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankDepositVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankReceiptsVO;
import com.aiotech.aios.accounts.domain.entity.vo.BankTransferVO;
import com.aiotech.aios.accounts.domain.entity.vo.ChequeTransactionVO;
import com.aiotech.aios.accounts.domain.entity.vo.CombinationVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditTermVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationChargeVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerQuotationVO;
import com.aiotech.aios.accounts.domain.entity.vo.CustomerVO;
import com.aiotech.aios.accounts.domain.entity.vo.DebitDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.DebitVO;
import com.aiotech.aios.accounts.domain.entity.vo.DirectPaymentVO;
import com.aiotech.aios.accounts.domain.entity.vo.DiscountMethodVO;
import com.aiotech.aios.accounts.domain.entity.vo.DiscountOptionVO;
import com.aiotech.aios.accounts.domain.entity.vo.DiscountVO;
import com.aiotech.aios.accounts.domain.entity.vo.EODBalancingVO;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.GoodsReturnVO;
import com.aiotech.aios.accounts.domain.entity.vo.InventoryComparisonVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.InvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueRequistionVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueReturnDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.IssueReturnVO;
import com.aiotech.aios.accounts.domain.entity.vo.LedgerFiscalVO;
import com.aiotech.aios.accounts.domain.entity.vo.LedgerVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialRequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.MaterialTransferVO;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PaymentVO;
import com.aiotech.aios.accounts.domain.entity.vo.PettyCashVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleChargeVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleReceiptVO;
import com.aiotech.aios.accounts.domain.entity.vo.PointOfSaleVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductCategoryVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingCalcVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductPricingVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ProductionReceiveVO;
import com.aiotech.aios.accounts.domain.entity.vo.PromotionMethodVO;
import com.aiotech.aios.accounts.domain.entity.vo.PromotionOptionVO;
import com.aiotech.aios.accounts.domain.entity.vo.PromotionVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.ReceiveVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.RequisitionVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryChargeVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryNoteVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesDeliveryPackVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesInvoiceVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderChargesVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.SalesOrderVO;
import com.aiotech.aios.accounts.domain.entity.vo.ShelfVO;
import com.aiotech.aios.accounts.domain.entity.vo.StockVO;
import com.aiotech.aios.accounts.domain.entity.vo.StoreVO;
import com.aiotech.aios.accounts.domain.entity.vo.SupplierVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionDetailVO;
import com.aiotech.aios.accounts.domain.entity.vo.TransactionVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessProductionVO;
import com.aiotech.aios.accounts.domain.entity.vo.WorkinProcessVO;
import com.aiotech.aios.accounts.service.AccountsEnterpriseService;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.to.LoanTO;
import com.aiotech.aios.accounts.to.PaymentTO;
import com.aiotech.aios.accounts.to.SupplierTO;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.Accounts.AccountType;
import com.aiotech.aios.common.to.Constants.Accounts.BankAccountType;
import com.aiotech.aios.common.to.Constants.Accounts.CalendarStatus;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDiscountMode;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermDueDay;
import com.aiotech.aios.common.to.Constants.Accounts.CreditTermPenaltyType;
import com.aiotech.aios.common.to.Constants.Accounts.CustomerType;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountCalcMethod;
import com.aiotech.aios.common.to.Constants.Accounts.DiscountOptions;
import com.aiotech.aios.common.to.Constants.Accounts.InventorySubCategory;
import com.aiotech.aios.common.to.Constants.Accounts.InvoiceSalesType;
import com.aiotech.aios.common.to.Constants.Accounts.ItemType;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.to.Constants.Accounts.O2CProcessStatus;
import com.aiotech.aios.common.to.Constants.Accounts.POSPaymentType;
import com.aiotech.aios.common.to.Constants.Accounts.PaymentStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PeriodStatus;
import com.aiotech.aios.common.to.Constants.Accounts.PettyCashTypes;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationSubType;
import com.aiotech.aios.common.to.Constants.Accounts.ProductCalculationType;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RequisitionStatus;
import com.aiotech.aios.common.to.Constants.Accounts.RewardType;
import com.aiotech.aios.common.to.Constants.Accounts.SalesInvoiceType;
import com.aiotech.aios.common.to.Constants.Accounts.StoreBinType;
import com.aiotech.aios.common.to.Constants.Accounts.TransactionType;
import com.aiotech.aios.common.to.Constants.HR.PromotionType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectTaskSurrogate;
import com.aiotech.aios.reporting.service.bl.AccountsBL;
import com.aiotech.aios.reporting.to.AccountsTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class AccountsAction extends ActionSupport {

	private static final long serialVersionUID = -263341506191144134L;
	private static final Logger LOGGER = LogManager
			.getLogger(AccountsAction.class);
	private AccountsEnterpriseService accountsEnterpriseService;
	private AccountsBL accountsBL;
	private DirectPaymentBL directPaymentBL;
	private String format;
	private Implementation implementation;
	private Ledger ledger = null;
	private List<Ledger> ledgerList;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private List<AccountsTO> accountsList;
	private List<Payment> paymentList;
	private List<LoanTO> loanList;
	private long loanId;
	private String approvalFlag;
	private long recordId;
	private long categoryId;
	private long periodId;
	private long codeCombinationId;
	private long journalId;
	private String fromDate;
	private String toDate;
	private String accountList;
	private double transactionAmount;
	private List<BankDepositVO> bankDepositList;
	private InputStream fileInputStream;

	private List<Calendar> calenderList;
	private String selectedCalendarIds;
	private List<AccountsTO> comulativeBalanceSheet;
	private List<Object> aaData;

	private List<String> itemTypes;
	private List<Product> productsList;
	private String productId;
	private String selectedType;

	private List<Period> periods;
	private String selectedPeriod;
	private String searchCriteria;
	private List<Object> entityObjects;
	private List<AccountsTO> inventoryStatementDS;
	private String selectedProductId;
	private String selectedSupplierId;
	private String returnMessage;

	private String selectedSupplier;
	private List<DirectPaymentVO> directPaymentDS;
	private List<SupplierTO> suppliersList;
	private List<GoodsReturn> goodsReturnList;
	private List<CreditTerm> creditTermList;
	private List<Store> storeList;
	private List<Purchase> purchases;
	private List<ReceiveVO> receiveDS;
	private List<GoodsReturnVO> goodsReturnDS;
	private List<Receive> receiveNotes;
	private List<InvoiceVO> invoiceDS;
	private List<DebitVO> debitDS;
	private List<PaymentVO> paymentDS;
	private List<CustomerVO> customerDS;
	private List<CreditTermVO> creditTermDS;
	private List<CustomerQuotationVO> customerQuotationDS;
	private List<SalesOrderVO> salesOrderDS;
	private List<SalesDeliveryNoteVO> salesDeliveryDS;
	private List<SalesInvoiceVO> salesInvoiceDS;
	private List<CreditNoteVO> creditNoteDS;
	private List<BankReceiptsVO> bankReceiptsDS;
	private List<BankDepositVO> bankDepositDS;
	private List<PettyCashVO> pettyCashDS;
	private BankAccountVO bankAccountDS;
	private List<IssueRequistionVO> issueRequistionDS;
	private List<IssueReturnVO> issueReturnDS;
	private List<BankTransferVO> bankTransferDS;
	private List<StockVO> stockDS;
	private List<StoreVO> storeDS;
	private List<AssetCheckInVO> checkInDS;
	private List<AssetCheckOutVO> checkOutDS;
	private List<ProductPricingVO> productPricingDS;
	private List<DiscountVO> productDiscountDS;
	private List<PromotionVO> promotionDS;
	private List<ProductionReceiveVO> productionReceiveDS;
	private List<MaterialTransferVO> materialTransferDS;
	private List<MaterialRequisitionVO> materialRequisitionDS;
	private List<PointOfSaleVO> pointOfSaleDS;
	private List<EODBalancingVO> eodBalancingDS;
	private List<ProductCategoryVO> productCategoryVOs;
	private ProductCategoryVO productCategoryVO;
	private Byte paymentStatus;
	private String printableContent;
	private String otherInfo;

	public String returnSuccess() {
		LOGGER.info("Inside Accounts method returnSuccess()");
		return SUCCESS;
	}

	/**
	 * Get fiscal trial balance screen with periods
	 * 
	 * @return string
	 */
	public String showFiscalStatement() {
		try {
			LOGGER.info("Inside Accounts method showFiscalStatement()");
			getImplementId();
			List<Period> periods = accountsBL
					.getAccountsEnterpriseService()
					.getCalendarService()
					.getAllClosedPeriods(implementation,
							PeriodStatus.Temporary_Closed.getCode(),
							PeriodStatus.Close.getCode());
			Collections.sort(periods, new Comparator<Period>() {
				public int compare(Period o1, Period o2) {
					return o1.getStartTime().compareTo(o2.getEndTime());
				}
			});
			ServletActionContext.getRequest().setAttribute("PERIOD_LIST",
					periods);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String trasactionJVReportHtml() {
		try {
			List<TransactionVO> transactionVOs = fetchTransactionList();
			ServletActionContext.getRequest().setAttribute("TRANSACTION_HEADS",
					transactionVOs);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String journalVoucherReportXLS() {
		try {
			getImplementId();
			List<TransactionVO> transactionVOs = fetchTransactionList();

			String str = ",,,," + implementation.getCompanyName() + "\n";
			str += ",,,, JOURNAL VOUCHER \n";
			str += "REFERENCE, COA CODE, COA DESCRIPTION, TRANSACTION TYPE, TRANSACTION ID, DESCRIPTION, DEBIT, CREDIT\n";
			if (null != transactionVOs && transactionVOs.size() > 0) {
				for (TransactionVO transactionVO : transactionVOs) {
					for (TransactionDetailVO transactionDetail : transactionVO
							.getTransactionDetailVOs()) {
						str += transactionDetail.getJournalNumber() + ",";
						str += transactionDetail.getAccountCode() + ",";
						str += transactionDetail.getAccountDescription() + ",";
						str += transactionDetail.getTransactionRef() + ",";
						str += null != transactionDetail
								.getTransactionReference() ? transactionDetail
								.getTransactionReference().replaceAll(",", ";")
								: "" + ",";
						str += null != transactionDetail.getDescription() ? transactionDetail
								.getDescription().replaceAll(",", ";") + ","
								: ",";
						if (transactionDetail.getIsDebit()) {
							str += transactionDetail.getAmount() + ",";
							str += "";
						} else {
							str += ",";
							str += transactionDetail.getAmount();
						}
						str += "\n";
					}
				}
			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<TransactionVO> fetchTransactionList() throws Exception {
		Long combinationId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		getImplementId();
		List<TransactionVO> transactionVOs = null;
		if (ServletActionContext.getRequest().getParameter("categoryId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("categoryId")) > 0)
			categoryId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("categoryId"));
		if (ServletActionContext.getRequest().getParameter("combinationId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("combinationId")) > 0)
			combinationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("combinationId"));
		if (ServletActionContext.getRequest().getParameter("fromDate") != null)
			fromDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("fromDate").toString());

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());

		List<Transaction> transactions = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionFilter(implementation, categoryId,
						combinationId, fromDatee, toDatee);
		if (null != transactions && transactions.size() > 0) {
			TransactionVO transactionVO = null;
			TransactionDetailVO transactionDetailVO = null;
			transactionVOs = new ArrayList<TransactionVO>();
			List<TransactionDetailVO> transactionDetailVOs = null;
			for (Transaction transaction : transactions) {
				transactionVO = new TransactionVO();
				BeanUtils.copyProperties(transactionVO, transaction);
				transactionDetailVOs = new ArrayList<TransactionDetailVO>();
				for (TransactionDetail transactionDetail : transaction
						.getTransactionDetails()) {
					transactionDetailVO = new TransactionDetailVO();
					BeanUtils.copyProperties(transactionDetailVO,
							transactionDetail);
					transactionDetailVO
							.setTransactionRef(null != transactionDetail
									.getTransaction().getUseCase() ? transactionDetail
									.getTransaction().getUseCase() : "JV");
					transactionDetailVO.setJournalNumber(transaction
							.getJournalNumber());
					transactionDetailVO.setDebitAmount(transactionDetail
							.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
					transactionDetailVO.setCreditAmount(!transactionDetail
							.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
					transactionDetailVO
							.setAccountCode(transactionDetail.getCombination()
									.getAccountByCompanyAccountId().getCode()
									+ "-"
									+ transactionDetail.getCombination()
											.getAccountByCostcenterAccountId()
											.getCode()
									+ "-"
									+ transactionDetail.getCombination()
											.getAccountByNaturalAccountId()
											.getCode()
									+ ((null != transactionDetail
											.getCombination()
											.getAccountByAnalysisAccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByAnalysisAccountId()
													.getCode()
											: "")
											+ (null != transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId() ? "-"
													+ transactionDetail
															.getCombination()
															.getAccountByBuffer1AccountId()
															.getCode()
													: "") + (null != transactionDetail
											.getCombination()
											.getAccountByBuffer2AccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByBuffer2AccountId()
													.getCode()
											: "")));
					transactionDetailVO
							.setAccountDescription(transactionDetail
									.getCombination()
									.getAccountByNaturalAccountId()
									.getAccount()
									+ ((null != transactionDetail
											.getCombination()
											.getAccountByAnalysisAccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByAnalysisAccountId()
													.getAccount()
											: "")
											+ (null != transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId() ? "-"
													+ transactionDetail
															.getCombination()
															.getAccountByBuffer1AccountId()
															.getAccount()
													: "") + (null != transactionDetail
											.getCombination()
											.getAccountByBuffer2AccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByBuffer2AccountId()
													.getAccount()
											: "")));
					transactionDetailVOs.add(transactionDetailVO);
				}
				transactionVO.setTransactionDetailVOs(transactionDetailVOs);
				transactionVOs.add(transactionVO);
			}
		}
		return transactionVOs;
	}

	public String printLedgerReport() {
		try {
			LOGGER.info("Inside Accounts method printLedgerReport()");
			TransactionVO transactionVO = fetchTransactionLedgerList();
			ServletActionContext.getRequest().setAttribute("LEDGER_ACCOUNT",
					transactionVO);
			LOGGER.info("Inside Accounts method printLedgerReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method printLedgerReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String ledgerStatementReportXLS() {
		try {
			LOGGER.info("Inside Accounts method ledgerStatementReportXLS()");
			TransactionVO transactionVO = fetchTransactionLedgerList();
			StringBuilder str = new StringBuilder();
			getImplementId();
			str.append(",,, " + implementation.getCompanyName() + " \n");
			str.append(",,, GENERAL LEDGER ACCOUNT STATEMENT \n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionVOs()) {
				str.append(",,, " + transactionVO.getAccountDescription()
						+ " ( Chart Of ACCOUNT ) \n");
				str.append("REFERENCE, DATE, COA CODE, COA DESCRIPTION, TRANSACTION TYPE, TRANSACTION ID, DESCRIPTION, DEBIT, CREDIT, BALANCE\n");
				for (TransactionVO list : transactionVO.getTransactionVOs()) {
					for (TransactionDetailVO transactionDetail : list
							.getTransactionDetailVOs()) {
						str.append(null != transactionDetail.getJournalNumber() ? transactionDetail
								.getJournalNumber() + ","
								: ",");
						str.append(null != transactionDetail
								.getTransactionDate() ? transactionDetail
								.getTransactionDate() + "," : ",");
						str.append(transactionDetail.getAccountCode() + ",");
						str.append(transactionDetail.getAccountDescription()
								+ ",");
						str.append(null != transactionDetail
								.getTransactionRef() ? transactionDetail
								.getTransactionRef() + "," : ",");
						str.append(null != transactionDetail
								.getTransactionReference() ? transactionDetail
								.getTransactionReference().replaceAll(",", ";")
								+ "," : ",");
						str.append(null != transactionDetail.getDescription() ? transactionDetail
								.getDescription().replaceAll(",", ";") + ","
								: ",");
						if (transactionDetail.getIsDebit())
							str.append(transactionDetail.getAmount() + ",");
						else
							str.append("," + transactionDetail.getAmount());
						str.append(","
								+ AIOSCommons
										.formatAmountToDouble(transactionDetail
												.getClosingBalance()) + ",");
						str.append("\n");
					}
				}
				str.append(",,,,,TOTAL,"
						+ AIOSCommons.formatAmountToDouble(transactionVO
								.getDebitAmount())
						+ ","
						+ AIOSCommons.formatAmountToDouble(transactionVO
								.getCreditAmount())
						+ ", "
						+ AIOSCommons.formatAmountToDouble(transactionVO
								.getClosingBalance()) + "\n");
			} else
				str.append("NO RESULT FOUND.");
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			LOGGER.info("Module : Accounts method ledgerStatementReportXLS() Succes");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method ledgerStatementReportXLS() caught exception "
					+ ex);
			return ERROR;
		}
	}

	private TransactionVO fetchTransactionLedgerList() throws Exception {
		Long combinationId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		getImplementId();
		if (ServletActionContext.getRequest().getParameter("combinationId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("combinationId")) > 0)
			combinationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("combinationId"));
		if (ServletActionContext.getRequest().getParameter("fromDate") != null)
			fromDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("fromDate").toString());

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());
		Combination combination = accountsBL.getCombinationBL()
				.getCombinationService()
				.getCombinationAllAccountDetail(combinationId);
		String accountCode = null != combination.getAccountByBuffer2AccountId() ? combination
				.getAccountByBuffer2AccountId().getAccount()
				: null != combination.getAccountByBuffer1AccountId() ? combination
						.getAccountByBuffer1AccountId().getAccount()
						: null != combination.getAccountByAnalysisAccountId() ? combination
								.getAccountByAnalysisAccountId().getAccount()
								: combination.getAccountByNaturalAccountId()
										.getAccount();
		List<Combination> combinations = null;
		Set<Long> combinationIds = new HashSet<Long>();
		if (null == combination.getAccountByAnalysisAccountId()) {
			combinations = accountsBL
					.getCombinationBL()
					.getCombinationService()
					.getCombinationByCostCenterNaturalAccountId(
							combination.getAccountByCostcenterAccountId()
									.getAccountId(),
							combination.getAccountByNaturalAccountId()
									.getAccountId());
		}
		if (null != combinations)
			for (Combination comi : combinations)
				combinationIds.add(comi.getCombinationId());
		else
			combinationIds.add(combination.getCombinationId());
		List<TransactionDetail> transactionDetails = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionDetailFilter(implementation, combinationIds,
						fromDatee, toDatee);
		TransactionVO transactionVO = null;
		if (null != transactionDetails && transactionDetails.size() > 0) {
			List<TransactionVO> transactionVOs = new ArrayList<TransactionVO>();
			Map<Long, List<TransactionDetail>> transactionMap = new HashMap<Long, List<TransactionDetail>>();
			List<TransactionDetail> transactionDetailTemp = null;
			for (TransactionDetail transactionDetail : transactionDetails) {
				transactionDetailTemp = new ArrayList<TransactionDetail>();
				if (null != transactionMap
						&& transactionMap.size() > 0
						&& transactionMap.containsKey(transactionDetail
								.getCombination().getCombinationId()))
					transactionDetailTemp.addAll(transactionMap
							.get(transactionDetail.getCombination()
									.getCombinationId()));
				transactionDetailTemp.add(transactionDetail);
				transactionMap.put(transactionDetail.getCombination()
						.getCombinationId(), transactionDetailTemp);
			}
			boolean parentFlag = false;
			if (transactionMap.containsKey(combinationId))
				parentFlag = true;

			Map<Long, List<TransactionDetail>> sortedTransactionMap = new TreeMap<Long, List<TransactionDetail>>(
					new Comparator<Long>() {
						public int compare(Long o1, Long o2) {
							return o1.compareTo(o2);
						}
					});
			sortedTransactionMap.putAll(transactionMap);
			LedgerVO ledgerVO = new LedgerVO();
			ledgerVO.setSide(true);
			ledgerVO.setCombination(combination);
			double debitBalance = 0;
			double creditBalance = 0;
			ledgerVO.setBalance(0d);
			int count = 0;
			for (Entry<Long, List<TransactionDetail>> entry : sortedTransactionMap
					.entrySet()) {
				if (count == 0 && parentFlag)
					transactionVOs.add(getParentLedgerDetail(entry.getValue()));
				else
					transactionVOs.add(getChildLedgerDetail(entry.getValue()));
				count += 1;
			}
			for (TransactionVO transaction : transactionVOs) {
				for (TransactionDetailVO transactionDetail : transaction
						.getTransactionDetailVOs()) {
					debitBalance += transactionDetail.getIsDebit() ? transactionDetail
							.getAmount() : 0;
					creditBalance += !transactionDetail.getIsDebit() ? transactionDetail
							.getAmount() : 0;
					ledgerVO = accountsBL.processUpdateLedger(ledgerVO,
							transactionDetail);
					transactionDetail.setClosingBalance(AIOSCommons
							.formatAmount(ledgerVO.getBalance()));
				}
			}

			transactionVO = new TransactionVO();
			transactionVO.setAccountDescription(accountCode);
			transactionVO
					.setDebitAmount(AIOSCommons.formatAmount(debitBalance));
			transactionVO.setCreditAmount(AIOSCommons
					.formatAmount(creditBalance));
			transactionVO.setClosingBalance(AIOSCommons.formatAmount(ledgerVO
					.getBalance()));
			transactionVO.setFromDate(ServletActionContext.getRequest()
					.getParameter("fromDate").toString());
			transactionVO.setToDate(ServletActionContext.getRequest()
					.getParameter("toDate").toString());
			transactionVO.setTransactionVOs(transactionVOs);
		}
		return transactionVO;
	}

	private TransactionVO getChildLedgerDetail(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = new TransactionVO();
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		double debitBalance = 0;
		double creditBalance = 0;
		for (TransactionDetail transactionDetail : transactionDetails) {
			debitBalance += transactionDetail.getIsDebit() ? transactionDetail
					.getAmount() : 0;
			creditBalance += !transactionDetail.getIsDebit() ? transactionDetail
					.getAmount() : 0;
			transactionDetailVO
					.setAccountCode(transactionDetail.getCombination()
							.getAccountByCompanyAccountId().getCode()
							+ "-"
							+ transactionDetail.getCombination()
									.getAccountByCostcenterAccountId()
									.getCode()
							+ "-"
							+ transactionDetail.getCombination()
									.getAccountByNaturalAccountId().getCode()
							+ ((null != transactionDetail.getCombination()
									.getAccountByAnalysisAccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByAnalysisAccountId()
											.getCode() : "")
									+ (null != transactionDetail
											.getCombination()
											.getAccountByBuffer1AccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId()
													.getCode()
											: "") + (null != transactionDetail
									.getCombination()
									.getAccountByBuffer2AccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByBuffer2AccountId()
											.getCode() : "")));
			transactionDetailVO
					.setAccountDescription(((null != transactionDetail
							.getCombination().getAccountByAnalysisAccountId() ? transactionDetail
							.getCombination().getAccountByAnalysisAccountId()
							.getAccount()
							: "")
							+ (null != transactionDetail.getCombination()
									.getAccountByBuffer1AccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByBuffer1AccountId()
											.getAccount() : "") + (null != transactionDetail
							.getCombination().getAccountByBuffer2AccountId() ? "-"
							+ transactionDetail.getCombination()
									.getAccountByBuffer2AccountId()
									.getAccount()
							: "")));
		}
		double closingBalance = debitBalance - creditBalance;
		transactionDetailVO.setAmount(Math.abs(closingBalance));
		if (closingBalance >= 0) {
			transactionDetailVO.setDebitAmount(AIOSCommons
					.formatAmount(closingBalance));
			transactionDetailVO.setIsDebit(TransactionType.Debit.getCode());
		} else {
			transactionDetailVO.setCreditAmount(AIOSCommons.formatAmount(Math
					.abs(closingBalance)));
			transactionDetailVO.setIsDebit(TransactionType.Credit.getCode());
		}
		transactionDetailVOs.add(transactionDetailVO);
		transactionVO.setTransactionDetailVOs(transactionDetailVOs);
		return transactionVO;
	}

	private TransactionVO getParentLedgerDetail(
			List<TransactionDetail> transactionDetails) throws Exception {
		TransactionVO transactionVO = new TransactionVO();
		TransactionDetailVO transactionDetailVO = null;
		List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
		for (TransactionDetail transactionDetail : transactionDetails) {
			transactionDetailVO = new TransactionDetailVO();
			BeanUtils.copyProperties(transactionDetailVO, transactionDetail);
			transactionDetailVO.setJournalNumber(transactionDetail
					.getTransaction().getJournalNumber());
			transactionDetailVO.setTransactionDate(DateFormat
					.convertDateToString(transactionDetail.getTransaction()
							.getTransactionTime().toString()));
			transactionDetailVO.setTransactionRef(null != transactionDetail
					.getTransaction().getUseCase() ? transactionDetail
					.getTransaction().getUseCase() : "JV");
			transactionDetailVO
					.setDebitAmount(transactionDetail.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
			transactionDetailVO
					.setCreditAmount(!transactionDetail.getIsDebit() ? AIOSCommons
							.formatAmount(transactionDetail.getAmount()) : "");
			transactionDetailVO
					.setAccountCode(transactionDetail.getCombination()
							.getAccountByCompanyAccountId().getCode()
							+ "-"
							+ transactionDetail.getCombination()
									.getAccountByCostcenterAccountId()
									.getCode()
							+ "-"
							+ transactionDetail.getCombination()
									.getAccountByNaturalAccountId().getCode()
							+ ((null != transactionDetail.getCombination()
									.getAccountByAnalysisAccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByAnalysisAccountId()
											.getCode() : "")
									+ (null != transactionDetail
											.getCombination()
											.getAccountByBuffer1AccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId()
													.getCode()
											: "") + (null != transactionDetail
									.getCombination()
									.getAccountByBuffer2AccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByBuffer2AccountId()
											.getCode() : "")));
			transactionDetailVO
					.setAccountDescription(transactionDetail.getCombination()
							.getAccountByNaturalAccountId().getAccount()
							+ ((null != transactionDetail.getCombination()
									.getAccountByAnalysisAccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByAnalysisAccountId()
											.getAccount() : "")
									+ (null != transactionDetail
											.getCombination()
											.getAccountByBuffer1AccountId() ? "-"
											+ transactionDetail
													.getCombination()
													.getAccountByBuffer1AccountId()
													.getAccount()
											: "") + (null != transactionDetail
									.getCombination()
									.getAccountByBuffer2AccountId() ? "-"
									+ transactionDetail.getCombination()
											.getAccountByBuffer2AccountId()
											.getAccount() : "")));
			transactionVO.setAccountDescription(transactionDetailVO
					.getAccountCode()
					+ " ["
					+ transactionDetailVO.getAccountDescription() + "]");
			transactionDetailVOs.add(transactionDetailVO);
		}
		Collections.sort(transactionDetailVOs,
				new Comparator<TransactionDetailVO>() {
					public int compare(TransactionDetailVO o1,
							TransactionDetailVO o2) {
						return DateFormat.convertStringToDate(
								o1.getTransactionDate()).compareTo(
								DateFormat.convertStringToDate(o2
										.getTransactionDate()));
					}
				});
		transactionVO.setTransactionDetailVOs(transactionDetailVOs);
		return transactionVO;
	}

	public String showTrialBalanceCriteria() {
		try {
			LOGGER.info("Inside Accounts method showTrialBalanceCriteria()");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String printTrialBalanceReport() {
		try {
			LOGGER.info("Inside Accounts method printTrialBalanceReport()");
			TransactionVO transactionVO = fetchTrialBalanceData();
			ServletActionContext.getRequest().setAttribute("TRIAL_BALANCE",
					transactionVO);
			LOGGER.info("Inside Accounts method printTrialBalanceReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method printTrialBalanceReport() throws Exception");
			return ERROR;
		}
	}

	public String trialBalanceReportXls() {
		try {
			LOGGER.info("Inside Accounts method trialBalanceReportXls()");
			TransactionVO transactionVO = fetchTrialBalanceData();
			StringBuilder str = new StringBuilder();
			getImplementId();
			str.append(", " + implementation.getCompanyName() + " \n");
			str.append(", TRIAL BALANCE \n");
			if (null != transactionVO.getFromDate())
				str.append(", FROM " + transactionVO.getFromDate() + null != transactionVO
						.getToDate() ? ", TO " + transactionVO.getToDate() : "");
			str.append("\n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionVOs()) {
				str.append("COA CODE, COA DESCRIPTION, ACCOUNT SUBTYPE, DEBIT, CREDIT\n");
				for (TransactionDetailVO transactionDetail : transactionVO
						.getTransactionDetailVOs()) {
					str.append(transactionDetail.getAccountCode() + ",");
					str.append(transactionDetail.getAccountDescription() + ",");
					str.append(null != transactionDetail.getAccountSubType() ? transactionDetail
							.getAccountSubType() + ","
							: ",");
					str.append(null != transactionDetail.getDebitAmount() ? AIOSCommons
							.formatAmountToDouble(transactionDetail
									.getDebitAmount())
							+ "," : ",");
					str.append(null != transactionDetail.getCreditAmount() ? AIOSCommons
							.formatAmountToDouble(transactionDetail
									.getCreditAmount())
							+ "," : ",");
					str.append("\n");
				}
				str.append(",,TOTAL,"
						+ AIOSCommons.formatAmountToDouble(transactionVO
								.getDebitAmount())
						+ ","
						+ AIOSCommons.formatAmountToDouble(transactionVO
								.getCreditAmount()));
			} else
				str.append("NO RESULT FOUND.");
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			LOGGER.info("Module : Accounts method trialBalanceReportXls() Succes");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method trialBalanceReportXls() caught exception "
					+ ex);
			return ERROR;
		}
	}

	private TransactionVO fetchTrialBalanceData() throws Exception {
		Date fromDatee = null;
		Date toDatee = null;
		getImplementId();
		TransactionVO transactionVO = null;
		if (ServletActionContext.getRequest().getParameter("fromDate") != null)
			fromDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("fromDate").toString());

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());

		LocalDate toLocaleDate = new DateTime(toDatee).toLocalDate();
		LocalDate currentDate = DateTime.now().toLocalDate();

		boolean isLedgerBalance = (null == fromDatee
				&& (toLocaleDate.isEqual(currentDate) || toLocaleDate
						.isAfter(currentDate)) ? true : false);
		if (!isLedgerBalance) {
			List<Calendar> calendars = accountsBL.getTransactionBL()
					.getCalendarBL().getCalendarService()
					.getCalendarByDates(implementation, fromDatee, toDatee);
			Set<Long> closedPeriods = new HashSet<Long>();
			Set<Long> openPeriods = new HashSet<Long>();
			if (null != calendars && calendars.size() > 0) {
				Collections.sort(calendars, new Comparator<Calendar>() {
					public int compare(Calendar o1, Calendar o2) {
						return o1.getStartTime().compareTo(o2.getStartTime());
					}
				});
				calendars.get(calendars.size() - 1).setStatus(
						CalendarStatus.Active.getCode());
				for (Calendar calendar : calendars) {
					if ((byte) calendar.getStatus() == (byte) CalendarStatus.Inactive
							.getCode()) {
						for (Period period : calendar.getPeriods())
							closedPeriods.add(period.getPeriodId());
					} else {
						for (Period period : calendar.getPeriods())
							openPeriods.add(period.getPeriodId());
					}
				}
			}
			if (null != closedPeriods && closedPeriods.size() > 0) {
				TransactionDetailVO retainedEarnings = this
						.processClosingRetainedEarnings(null, null,
								closedPeriods);
				TransactionDetail transactionDetail = new TransactionDetail();
				BeanUtils.copyProperties(transactionDetail, retainedEarnings);
				List<TransactionDetail> transactionDetails = accountsBL
						.getTransactionBL()
						.getTransactionService()
						.getTransactionDetailStatementFilter(implementation,
								null, null, toDatee,
								AccountType.Assets.getCode(),
								AccountType.Liabilites.getCode(),
								AccountType.OwnersEquity.getCode());
				transactionDetails.add(transactionDetail);

				List<TransactionDetail> openPeriodRevenueExpense = accountsBL
						.getTransactionBL()
						.getTransactionService()
						.getTransactionDetailStatementFilter(implementation,
								openPeriods, null, null,
								AccountType.Revenue.getCode(),
								AccountType.Expenses.getCode(), null);
				transactionDetails.addAll(openPeriodRevenueExpense);

				transactionVO = accountsBL
						.processTrialBalance(transactionDetails);
			} else {
				List<TransactionDetail> transactionDetails = accountsBL
						.getTransactionBL()
						.getTransactionService()
						.getTransactionDetailFilter(implementation, null,
								fromDatee, toDatee);
				transactionVO = accountsBL
						.processTrialBalance(transactionDetails);

			}
		} else {
			transactionVO = new TransactionVO();
			List<Ledger> ledgers = accountsBL
					.getTrailBalanceCombination(implementation);
			if (null != ledgers && ledgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger l1, Ledger l2) {
						return l1.getLedgerId().compareTo(l2.getLedgerId());
					}
				});
				List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
				TransactionDetail transactionDetail = null;
				for (Ledger ledger : ledgers) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setAmount(ledger.getBalance());
					transactionDetail.setIsDebit(ledger.getSide());
					transactionDetail.setCombination(ledger.getCombination());
					transactionDetails.add(transactionDetail);
				}
				transactionVO = accountsBL
						.processTrialBalance(transactionDetails);
			}
		}
		if (null != transactionVO) {
			transactionVO.setFromDate(ServletActionContext.getRequest()
					.getParameter("fromDate").toString());
			transactionVO.setToDate(ServletActionContext.getRequest()
					.getParameter("toDate").toString());
		}
		return transactionVO;
	}

	private TransactionDetailVO processClosingRetainedEarnings(Date fromDatee,
			Date toDatee, Set<Long> periods) throws Exception {
		getImplementId();
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		List<TransactionDetail> revenueTransactionDetails = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionDetailStatementFilter(implementation, periods,
						fromDatee, toDatee, AccountType.Revenue.getCode(),
						null, null);
		TransactionVO revenueTransactionVO = accountsBL
				.processRevenueIncomeStatement(revenueTransactionDetails);
		List<TransactionDetail> expenseTransactionDetails = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionDetailStatementFilter(implementation, periods,
						fromDatee, toDatee, AccountType.Expenses.getCode(),
						null, null);
		TransactionVO expenseTransactionVO = accountsBL
				.processExpenseIncomeStatement(expenseTransactionDetails);

		double revenueTotal = AIOSCommons
				.formatAmountToDouble(null != revenueTransactionVO ? revenueTransactionVO
						.getRevenueAmount() : 0);
		double expenseTotal = AIOSCommons
				.formatAmountToDouble(null != expenseTransactionVO ? expenseTransactionVO
						.getExpenseAmount() : 0);

		double tempNetAmount = 0;
		Combination combination = accountsBL
				.getCombinationBL()
				.getCombinationService()
				.getCombinationAllAccountDetail(
						implementation.getOwnersEquity());
		transactionDetailVO.setCombination(combination);
		transactionDetailVO.setAccountDescription(combination
				.getAccountByNaturalAccountId().getAccount());
		transactionDetailVO.setAccountCode(combination
				.getAccountByCompanyAccountId().getCode()
				+ "."
				+ combination.getAccountByCostcenterAccountId().getCode()
				+ "."
				+ combination.getAccountByNaturalAccountId().getCode());
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setCreditAmount(AIOSCommons
						.formatAmount(Double.valueOf(Math.abs(tempNetAmount))));
				transactionDetailVO.setLedgerSide(TransactionType.Credit
						.getCode());
				transactionDetailVO
						.setIsDebit(TransactionType.Credit.getCode());
				transactionDetailVO.setAmount(Math.abs(tempNetAmount));
			} else {
				transactionDetailVO.setDebitAmount(AIOSCommons
						.formatAmount(Double.valueOf(Math.abs(tempNetAmount))));
				transactionDetailVO.setLedgerSide(TransactionType.Debit
						.getCode());
				transactionDetailVO.setIsDebit(TransactionType.Debit.getCode());
				transactionDetailVO.setAmount(Math.abs(tempNetAmount));
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setDebitAmount(AIOSCommons
						.formatAmount(Double.valueOf(Math.abs(tempNetAmount))));
				transactionDetailVO.setLedgerSide(TransactionType.Debit
						.getCode());
				transactionDetailVO.setIsDebit(TransactionType.Debit.getCode());
				transactionDetailVO.setAmount(Math.abs(tempNetAmount));
			} else {
				transactionDetailVO.setCreditAmount(AIOSCommons
						.formatAmount(Double.valueOf(Math.abs(tempNetAmount))));
				transactionDetailVO.setLedgerSide(TransactionType.Credit
						.getCode());
				transactionDetailVO
						.setIsDebit(TransactionType.Credit.getCode());
				transactionDetailVO.setAmount(Math.abs(tempNetAmount));
			}
		}
		return transactionDetailVO;
	}

	public String printIncomeStatementReport() {
		try {
			LOGGER.info("Inside Accounts method printIncomeStatementReport()");
			TransactionVO transactionVO = fetchIncomeStatementData();
			ServletActionContext.getRequest().setAttribute("INCOME_STATEMENT",
					transactionVO);
			LOGGER.info("Inside Accounts method printIncomeStatementReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method printIncomeStatementReport() throws Exception");
			return ERROR;
		}
	}

	public String incomeStatementReportXls() {
		try {
			LOGGER.info("Inside Accounts method incomeStatementReportXls()");
			TransactionVO transactionVO = fetchIncomeStatementData();
			StringBuilder str = new StringBuilder();
			getImplementId();
			str.append(", " + implementation.getCompanyName() + " \n");
			str.append(", INCOME STATEMENT \n");
			if (null != transactionVO.getFromDate())
				str.append(", FROM " + transactionVO.getFromDate() + null != transactionVO
						.getToDate() ? ", TO " + transactionVO.getToDate() : "");
			str.append("\n");
			str.append("REVENUE\n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionDetailVOs()) {
				for (TransactionDetailVO transactionDetail : transactionVO
						.getTransactionDetailVOs()) {
					str.append(transactionDetail.getAccountDescription() + ",");
					str.append((transactionDetail.getRevenueAmount()
							.replaceAll(",", "")) + ",");
					str.append("\n");
				}
				str.append("TOTAL,"
						+ (transactionVO.getRevenueAmount().replaceAll(",", "")));
			}
			str.append("\n\n EXPENSE\n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionDetailVOCs()) {
				for (TransactionDetailVO transactionDetail : transactionVO
						.getTransactionDetailVOCs()) {
					str.append(transactionDetail.getAccountDescription() + ",");
					str.append((transactionDetail.getExpenseAmount()
							.replaceAll(",", "")) + ",");
					str.append("\n");
				}
				str.append("TOTAL,"
						+ (transactionVO.getExpenseAmount().replaceAll(",", "")));
			}
			str.append("\n\n " + transactionVO.getLabel() + ","
					+ transactionVO.getNetAmount().replaceAll(",", ""));
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			LOGGER.info("Inside Accounts method incomeStatementReportXls() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method incomeStatementReportXls() throws Exception");
			return ERROR;
		}
	}

	private TransactionVO fetchIncomeStatementData() throws Exception {
		Date fromDatee = null;
		Date toDatee = null;
		getImplementId();
		TransactionVO transactionVO = null;
		if (ServletActionContext.getRequest().getParameter("fromDate") != null)
			fromDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("fromDate").toString());

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());
		TransactionVO revenueTransactionVO = null;
		TransactionVO expenseTransactionVO = null;
		if (null != fromDatee || null != toDatee) {
			transactionVO = new TransactionVO();
			List<TransactionDetail> revenueTransactionDetails = accountsBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailStatementFilter(implementation, null,
							fromDatee, toDatee, AccountType.Revenue.getCode(),
							null, null);
			revenueTransactionVO = accountsBL
					.processRevenueIncomeStatement(revenueTransactionDetails);
			List<TransactionDetail> expenseTransactionDetails = accountsBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailStatementFilter(implementation, null,
							fromDatee, toDatee, AccountType.Expenses.getCode(),
							null, null);
			expenseTransactionVO = accountsBL
					.processExpenseIncomeStatement(expenseTransactionDetails);
		} else {
			transactionVO = new TransactionVO();
			List<Ledger> ledgers = accountsBL
					.getIncomenStatementCombination(implementation);

			if (null != ledgers && ledgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger o1, Ledger o2) {
						return o1.getLedgerId().compareTo(o2.getLedgerId());
					}
				});
				List<TransactionDetail> revenueTransactionDetails = new ArrayList<TransactionDetail>();
				List<TransactionDetail> expenseTransactionDetails = new ArrayList<TransactionDetail>();
				TransactionDetail transactionDetail = null;
				for (Ledger ledger : ledgers) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setAmount(ledger.getBalance());
					transactionDetail.setIsDebit(ledger.getSide());
					transactionDetail.setCombination(ledger.getCombination());
					if (null != ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType()
							&& (int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Revenue
									.getCode())
						revenueTransactionDetails.add(transactionDetail);
					else
						expenseTransactionDetails.add(transactionDetail);
				}
				revenueTransactionVO = accountsBL
						.processRevenueIncomeStatement(revenueTransactionDetails);

				expenseTransactionVO = accountsBL
						.processExpenseIncomeStatement(expenseTransactionDetails);
			}
		}
		double revenueTotal = AIOSCommons
				.formatAmountToDouble(null != revenueTransactionVO ? revenueTransactionVO
						.getRevenueAmount() : 0);
		double expenseTotal = AIOSCommons
				.formatAmountToDouble(null != expenseTransactionVO ? expenseTransactionVO
						.getExpenseAmount() : 0);
		if (expenseTotal >= 0)
			transactionVO.setExpenseAmount(AIOSCommons
					.formatAmount(expenseTotal));
		else {
			transactionVO.setExpenseAmount(AIOSCommons.formatAmount(Double
					.valueOf(expenseTotal)));
			transactionVO
					.setExpenseAmount(transactionVO
							.getExpenseAmount()
							.replace('$', '-')
							.substring(
									0,
									transactionVO.getExpenseAmount().length() - 1));
		}
		if (revenueTotal >= 0)
			transactionVO.setRevenueAmount(AIOSCommons
					.formatAmount(revenueTotal));
		else {
			transactionVO.setRevenueAmount(AIOSCommons.formatAmount(Double
					.valueOf(revenueTotal)));
			transactionVO
					.setRevenueAmount(transactionVO
							.getRevenueAmount()
							.replace('$', '-')
							.substring(
									0,
									transactionVO.getRevenueAmount().length() - 1));
		}
		double tempNetAmount = 0;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				transactionVO.setLabel("Net Profit");
				transactionVO.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
			} else {
				transactionVO.setLabel("Net Loss");
				transactionVO.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
				transactionVO
						.setNetAmount(transactionVO.getNetAmount().substring(1,
								transactionVO.getNetAmount().length() - 1));
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				transactionVO.setLabel("Net Loss");
				transactionVO.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
			} else {
				transactionVO.setLabel("Net Profit");
				transactionVO.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
				transactionVO
						.setNetAmount(transactionVO.getNetAmount().substring(1,
								transactionVO.getNetAmount().length() - 1));
			}
		}
		transactionVO
				.setTransactionDetailVOs(null != revenueTransactionVO ? revenueTransactionVO
						.getTransactionDetailVOs() : null);
		transactionVO
				.setTransactionDetailVOCs(null != expenseTransactionVO ? expenseTransactionVO
						.getTransactionDetailVOs() : null);
		if (null != transactionVO) {
			transactionVO.setFromDate(ServletActionContext.getRequest()
					.getParameter("fromDate").toString());
			transactionVO.setToDate(ServletActionContext.getRequest()
					.getParameter("toDate").toString());
		}
		return transactionVO;
	}

	public String printBalanceSheetReport() {
		try {
			LOGGER.info("Inside Accounts method printBalanceSheetReport()");
			TransactionVO transactionVO = fetchBalanceSheetData();
			ServletActionContext.getRequest().setAttribute("BALANCE_SHEET",
					transactionVO);
			LOGGER.info("Inside Accounts method printBalanceSheetReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method printBalanceSheetReport() throws Exception");
			return ERROR;
		}
	}

	public String balanceSheetReportXls() {
		try {
			LOGGER.info("Inside Accounts method balanceSheetReportXls()");
			TransactionVO transactionVO = fetchBalanceSheetData();
			StringBuilder str = new StringBuilder();
			getImplementId();
			str.append(", " + implementation.getCompanyName() + " \n");
			str.append(", BALANCE SHEET \n");
			str.append(",AS AT "
					+ DateFormat.convertDateToString(DateTime.now()
							.toLocalDate().toString()));
			str.append("\n");
			str.append("ASSETS\n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionDetailVOs()) {
				for (TransactionDetailVO transactionDetail : transactionVO
						.getTransactionDetailVOs()) {
					str.append(transactionDetail.getAccountDescription() + ",");
					str.append((transactionDetail.getAssetAmount().replaceAll(
							",", "")) + ",");
					str.append("\n");
				}
				str.append("TOTAL,"
						+ (transactionVO.getAssetAmount().replaceAll(",", "")));
			}
			str.append("\n\n LIABILITIES\n");
			if (null != transactionVO
					&& null != transactionVO.getTransactionDetailVOCs()) {
				for (TransactionDetailVO transactionDetail : transactionVO
						.getTransactionDetailVOCs()) {
					str.append(transactionDetail.getAccountDescription() + ",");
					str.append((transactionDetail.getLiabilityAmount()
							.replaceAll(",", "")) + ",");
					str.append("\n");
				}
				str.append("TOTAL,"
						+ (transactionVO.getLiabilityAmount().replaceAll(",",
								"")));
			}
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			LOGGER.info("Inside Accounts method balanceSheetReportXls() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method balanceSheetReportXls() throws Exception");
			return ERROR;
		}
	}

	private TransactionDetailVO processRetainEarningsData(Date fromDatee,
			Date toDatee, Set<Long> openPeriods) throws Exception {
		getImplementId();
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		List<TransactionDetail> revenueTransactionDetails = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionDetailStatementFilter(implementation,
						openPeriods, fromDatee, toDatee,
						AccountType.Revenue.getCode(), null, null);
		TransactionVO revenueTransactionVO = accountsBL
				.processRevenueIncomeStatement(revenueTransactionDetails);
		List<TransactionDetail> expenseTransactionDetails = accountsBL
				.getTransactionBL()
				.getTransactionService()
				.getTransactionDetailStatementFilter(implementation,
						openPeriods, fromDatee, toDatee,
						AccountType.Expenses.getCode(), null, null);
		TransactionVO expenseTransactionVO = accountsBL
				.processExpenseIncomeStatement(expenseTransactionDetails);

		double revenueTotal = AIOSCommons
				.formatAmountToDouble(null != revenueTransactionVO ? revenueTransactionVO
						.getRevenueAmount() : 0);
		double expenseTotal = AIOSCommons
				.formatAmountToDouble(null != expenseTransactionVO ? expenseTransactionVO
						.getExpenseAmount() : 0);

		double tempNetAmount = 0;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setAccountDescription("Net Profit");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setRetainEarnings(tempNetAmount);
			} else {
				transactionDetailVO.setAccountDescription("Net Loss");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setLiabilityAmount(transactionDetailVO
						.getLiabilityAmount().substring(
								1,
								transactionDetailVO.getLiabilityAmount()
										.length() - 1));
				transactionDetailVO.setRetainEarnings((tempNetAmount * (-1)));
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setAccountDescription("Net Loss");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setRetainEarnings((tempNetAmount * (-1)));
			} else {
				transactionDetailVO.setAccountDescription("Net Profit");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setLiabilityAmount(transactionDetailVO
						.getLiabilityAmount().substring(
								1,
								transactionDetailVO.getLiabilityAmount()
										.length() - 1));
				transactionDetailVO.setRetainEarnings(tempNetAmount);
			}
		}
		return transactionDetailVO;
	}

	private TransactionDetailVO processRetainEarningsData(
			TransactionVO revenueTransactionVO,
			TransactionVO expenseTransactionVO) throws Exception {
		TransactionDetailVO transactionDetailVO = new TransactionDetailVO();
		double revenueTotal = AIOSCommons
				.formatAmountToDouble(null != revenueTransactionVO ? revenueTransactionVO
						.getRevenueAmount() : 0);
		double expenseTotal = AIOSCommons
				.formatAmountToDouble(null != expenseTransactionVO ? expenseTransactionVO
						.getExpenseAmount() : 0);

		double tempNetAmount = 0;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setAccountDescription("Net Profit");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setRetainEarnings(tempNetAmount);
			} else {
				transactionDetailVO.setAccountDescription("Net Loss");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setLiabilityAmount(transactionDetailVO
						.getLiabilityAmount().substring(
								1,
								transactionDetailVO.getLiabilityAmount()
										.length() - 1));
				transactionDetailVO.setRetainEarnings((tempNetAmount * (-1)));
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				transactionDetailVO.setAccountDescription("Net Loss");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setRetainEarnings((tempNetAmount * (-1)));
			} else {
				transactionDetailVO.setAccountDescription("Net Profit");
				transactionDetailVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				transactionDetailVO.setLiabilityAmount(transactionDetailVO
						.getLiabilityAmount().substring(
								1,
								transactionDetailVO.getLiabilityAmount()
										.length() - 1));
				transactionDetailVO.setRetainEarnings(tempNetAmount);
			}
		}
		return transactionDetailVO;
	}

	private TransactionVO fetchBalanceSheetData() throws Exception {
		Date toDatee = null;
		getImplementId();
		TransactionVO transactionVO = null;

		if (ServletActionContext.getRequest().getParameter("toDate") != null)
			toDatee = DateFormat.convertStringToDate(ServletActionContext
					.getRequest().getParameter("toDate").toString());
		TransactionVO assetTransactionVO = null;
		TransactionVO liabilityTransactionVO = null;
		TransactionDetailVO retainedEarnings = null;
		if (null != toDatee) {
			List<Calendar> calendars = accountsBL.getTransactionBL()
					.getCalendarBL().getCalendarService()
					.getCalendarByDates(implementation, null, toDatee);
			Set<Long> closedPeriods = new HashSet<Long>();
			Set<Long> openPeriods = new HashSet<Long>();
			if (null != calendars && calendars.size() > 0) {
				Collections.sort(calendars, new Comparator<Calendar>() {
					public int compare(Calendar o1, Calendar o2) {
						return o1.getStartTime().compareTo(o2.getStartTime());
					}
				});
				calendars.get(calendars.size() - 1).setStatus(
						CalendarStatus.Active.getCode());
				for (Calendar calendar : calendars) {
					if ((byte) calendar.getStatus() == (byte) CalendarStatus.Inactive
							.getCode()) {
						for (Period period : calendar.getPeriods())
							closedPeriods.add(period.getPeriodId());
					} else {
						for (Period period : calendar.getPeriods())
							openPeriods.add(period.getPeriodId());
					}
				}
			}

			transactionVO = new TransactionVO();

			List<TransactionDetail> assetTransactionDetails = accountsBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailStatementFilter(implementation, null,
							null, toDatee, AccountType.Assets.getCode(), null,
							null);
			List<TransactionDetail> liabilityTransactionDetails = accountsBL
					.getTransactionBL()
					.getTransactionService()
					.getTransactionDetailStatementFilter(implementation, null,
							null, toDatee, AccountType.Liabilites.getCode(),
							AccountType.OwnersEquity.getCode(), null);

			if (null != closedPeriods && closedPeriods.size() > 0) {
				TransactionDetailVO closedEarnings = this
						.processClosingRetainedEarnings(null, null,
								closedPeriods);
				TransactionDetail transactionDetail = new TransactionDetail();
				BeanUtils.copyProperties(transactionDetail, closedEarnings);
				liabilityTransactionDetails.add(transactionDetail);
			}

			// Retain Earnings
			if (null != openPeriods && openPeriods.size() > 0)
				retainedEarnings = this.processRetainEarningsData(null, null,
						openPeriods);

			assetTransactionVO = accountsBL
					.processAssetStatement(assetTransactionDetails);
			liabilityTransactionVO = accountsBL.processLiabilityStatement(
					liabilityTransactionDetails, retainedEarnings);

			transactionVO.setToDate(ServletActionContext.getRequest()
					.getParameter("toDate").toString());
		} else {
			List<Ledger> ledgers = accountsBL
					.getBalanceSheetCombination(implementation);
			if (null != ledgers && ledgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger o1, Ledger o2) {
						return o1.getLedgerId().compareTo(o2.getLedgerId());
					}
				});
				List<TransactionDetail> assetTransactionDetails = new ArrayList<TransactionDetail>();
				List<TransactionDetail> liabilityTransactionDetails = new ArrayList<TransactionDetail>();
				TransactionDetail transactionDetail = null;
				for (Ledger ledger : ledgers) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setAmount(ledger.getBalance());
					transactionDetail.setIsDebit(ledger.getSide());
					transactionDetail.setCombination(ledger.getCombination());
					if (null != ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType()
							&& (int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Assets
									.getCode())
						assetTransactionDetails.add(transactionDetail);

					else if (null != ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType()
							&& ((int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Liabilites
									.getCode() || (int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.OwnersEquity
									.getCode()))
						liabilityTransactionDetails.add(transactionDetail);
				}

				List<Ledger> revenueExpenseLedgers = accountsBL
						.getIncomenStatementCombination(implementation);
				transactionVO = new TransactionVO();
				if (null != revenueExpenseLedgers
						&& revenueExpenseLedgers.size() > 0) {
					Collections.sort(ledgers, new Comparator<Ledger>() {
						public int compare(Ledger o1, Ledger o2) {
							return o1.getLedgerId().compareTo(o2.getLedgerId());
						}
					});
					List<TransactionDetail> revenueTransactionDetails = new ArrayList<TransactionDetail>();
					List<TransactionDetail> expenseTransactionDetails = new ArrayList<TransactionDetail>();
					for (Ledger ledger : revenueExpenseLedgers) {
						transactionDetail = new TransactionDetail();
						transactionDetail.setAmount(ledger.getBalance());
						transactionDetail.setIsDebit(ledger.getSide());
						transactionDetail.setCombination(ledger
								.getCombination());
						if (null != ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType()
								&& (int) ledger.getCombination()
										.getAccountByNaturalAccountId()
										.getAccountType() == (int) AccountType.Revenue
										.getCode())
							revenueTransactionDetails.add(transactionDetail);
						else
							expenseTransactionDetails.add(transactionDetail);
					}
					TransactionVO revenueTransactionVO = accountsBL
							.processRevenueIncomeStatement(revenueTransactionDetails);

					TransactionVO expenseTransactionVO = accountsBL
							.processExpenseIncomeStatement(expenseTransactionDetails);
					retainedEarnings = this.processRetainEarningsData(
							revenueTransactionVO, expenseTransactionVO);
				}
				assetTransactionVO = accountsBL
						.processAssetStatement(assetTransactionDetails);
				liabilityTransactionVO = accountsBL.processLiabilityStatement(
						liabilityTransactionDetails, retainedEarnings);
				transactionVO.setToDate(DateFormat.convertDateToString(DateTime
						.now().toLocalDate().toString()));
			}
		}
		if (null != assetTransactionVO
				&& null != assetTransactionVO.getTransactionDetailVOs()) {
			if ((Double.valueOf((assetTransactionVO.getAssetAmount()
					.replaceAll(",", ""))) >= 0)) {
				transactionVO.setAssetAmount(AIOSCommons.formatAmount(Double
						.valueOf(assetTransactionVO.getAssetAmount()
								.replaceAll(",", ""))));
			} else {
				transactionVO.setAssetAmount(AIOSCommons.formatAmount(Double
						.valueOf(assetTransactionVO.getAssetAmount()
								.replaceAll(",", ""))));
				transactionVO.setAssetAmount(transactionVO
						.getAssetAmount()
						.replace('$', '-')
						.substring(0,
								transactionVO.getAssetAmount().length() - 1));
			}
			transactionVO.setTransactionDetailVOs(assetTransactionVO
					.getTransactionDetailVOs());
		}

		if (null != liabilityTransactionVO
				&& null != liabilityTransactionVO.getTransactionDetailVOs()) {
			List<TransactionDetailVO> tempLiability = new ArrayList<TransactionDetailVO>(
					liabilityTransactionVO.getTransactionDetailVOs());
			if (null != retainedEarnings)
				tempLiability.add(retainedEarnings);

			if ((Double.valueOf((liabilityTransactionVO.getLiabilityAmount()
					.replaceAll(",", ""))) >= 0)) {
				transactionVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(liabilityTransactionVO
								.getLiabilityAmount().replaceAll(",", ""))));
			} else {
				transactionVO.setLiabilityAmount(AIOSCommons
						.formatAmount(Double.valueOf(liabilityTransactionVO
								.getLiabilityAmount().replaceAll(",", ""))));
				transactionVO
						.setLiabilityAmount(transactionVO
								.getLiabilityAmount()
								.replace('$', '-')
								.substring(
										0,
										transactionVO.getLiabilityAmount()
												.length() - 1));
			}
			transactionVO.setTransactionDetailVOCs(tempLiability);
		}
		return transactionVO;
	}

	public String showPeriodLedgerFiscalList() {
		try {
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			if (periodId > 0) {
				List<LedgerFiscal> ledgerFiscals = accountsBL
						.getLedgerFiscalAgainstPeriod(periodId);
				List<LedgerFiscalVO> ledgerFiscalVOs = accountsBL
						.getAccountsJobBL().getLedgerFiscalBL()
						.getPeriodLedgerFiscalView(ledgerFiscals);

				if (null != ledgerFiscalVOs && ledgerFiscalVOs.size() > 0) {
					JSONArray array = null;
					for (LedgerFiscalVO ledgerFiscalVO : ledgerFiscalVOs) {
						array = new JSONArray();
						array.add(ledgerFiscalVO.getCombination()
								.getCombinationId());
						array.add(ledgerFiscalVO.getPeriodName());
						array.add(ledgerFiscalVO.getAccountName());
						array.add(ledgerFiscalVO.getAccountType());
						array.add(ledgerFiscalVO.getDebitBalance());
						array.add(ledgerFiscalVO.getCreditBalance());
						data.add(array);
					}
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String printTransaction() {
		try {
			LOGGER.info("Inside Accounts method printTransaction()");
			getImplementId();
			Transaction transaction = this.getAccountsBL()
					.getAccountsEnterpriseService().getTransactionService()
					.getTransactionById(journalId);
			List<TransactionDetail> transactionDetail = new ArrayList<TransactionDetail>(
					transaction.getTransactionDetails());
			Collections.sort(transactionDetail,
					new Comparator<TransactionDetail>() {
						public int compare(TransactionDetail t1,
								TransactionDetail t2) {
							return t2.getIsDebit().compareTo(t1.getIsDebit());
						}
					});
			transactionAmount = 0;
			for (TransactionDetail detail : transactionDetail) {
				if (detail.getIsDebit())
					transactionAmount += detail.getAmount();
			}
			ServletActionContext.getRequest().setAttribute("TRANSACTION_PRINT",
					transaction);
			ServletActionContext.getRequest().setAttribute(
					"TRANSACTION_DETAIL_PRINT", transactionDetail);
			LOGGER.info("Accounts method printTransaction() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("printTransaction Method printTransaction() caught exeception "
					+ ex);
			return ERROR;
		}
	}

	// Html print petty cash transaction
	public String printPettyCashTransaction() {
		LOGGER.info("Inside Module: Accounts : Method: printPettyCashTransaction");
		try {
			PettyCash pettyCash = accountsBL.getAccountsEnterpriseService()
					.getPettyCashService().getPettyCashById(recordId);
			List<PettyCashVO> pettyCashVOs = accountsBL.getPettyCashBL()
					.recursivePettyCashLog(pettyCash);
			pettyCashVOs = accountsBL.getPettyCashBL().createPettyCashLog(
					pettyCashVOs);
			ServletActionContext.getRequest().setAttribute("PETTY_CASH_PRINT",
					pettyCashVOs);
			PettyCashVO pettyCashLog = totalCashOutAndExpense(pettyCashVOs,
					pettyCash.getPettyCashId());
			ServletActionContext.getRequest().setAttribute(
					"PETTYCASH_SUMMARAY", pettyCashLog);
			LOGGER.info("Module: Accounts : Method: printPettyCashTransaction: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: printPettyCashTransaction: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private PettyCashVO totalCashOutAndExpense(List<PettyCashVO> pettyCashVOs,
			long pettyCashId) throws Exception {
		double cashOut = 0;
		double expenseAmount = 0;
		PettyCashVO pettyCash = null;
		if (null != pettyCashVOs && pettyCashVOs.size() > 0) {
			pettyCash = new PettyCashVO();
			for (PettyCashVO pettyCashVO : pettyCashVOs) {
				if (((byte) PettyCashTypes.Cash_transfer.getCode() == (byte) pettyCashVO
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Carry_forward.getCode() == (byte) pettyCashVO
								.getPettyCashType())) {
					cashOut += (null != pettyCashVO.getCashIn() ? pettyCashVO
							.getCashIn() : 0);
				} else if (((byte) PettyCashTypes.Spend.getCode() == (byte) pettyCashVO
						.getPettyCashType())
						|| ((byte) PettyCashTypes.Reimbursed.getCode() == (byte) pettyCashVO
								.getPettyCashType())) {
					expenseAmount += (null != pettyCashVO.getExpenseAmount() ? pettyCashVO
							.getExpenseAmount() : 0);
				}
			}
			pettyCash.setExpenseAmountFormat(AIOSCommons
					.formatAmount(expenseAmount));
			pettyCash.setCashOutFormat(AIOSCommons.formatAmount(cashOut));
			pettyCash.setRemainingBalanceFormat(AIOSCommons
					.formatAmount(cashOut - expenseAmount));

			List<PettyCash> pettyCashSetteled = accountsBL
					.getPettyCashBL()
					.getPettyCashService()
					.getParentPettyCashSettledVouchers(pettyCashId,
							PettyCashTypes.Cash_given_settlement.getCode());
			double forwaredAmount = 0;
			if (null != pettyCashSetteled && pettyCashSetteled.size() > 0) {
				for (PettyCash py : pettyCashSetteled)
					forwaredAmount += py.getCashOut();
			}
			pettyCash.setExpenseAmountFormat(AIOSCommons
					.formatAmount(expenseAmount));
			pettyCash.setCashOutFormat(AIOSCommons.formatAmount(cashOut));
			pettyCash.setCashForwaredStr(AIOSCommons
					.formatAmount(forwaredAmount));
			pettyCash.setRemainingBalanceFormat(AIOSCommons
					.formatAmount(cashOut - (expenseAmount + forwaredAmount)));
		}
		return pettyCash;
	}

	public String pettyCashReportXLS() {
		try {
			PettyCash pettyCash = accountsBL.getAccountsEnterpriseService()
					.getPettyCashService().getPettyCashById(recordId);
			List<PettyCashVO> pettyCashVOs = accountsBL.getPettyCashBL()
					.recursivePettyCashLog(pettyCash);
			pettyCashVOs = accountsBL.getPettyCashBL().createPettyCashLog(
					pettyCashVOs);
			PettyCashVO pettyCashLog = totalCashOutAndExpense(pettyCashVOs,
					pettyCash.getPettyCashId());
			String str = "";
			getImplementId();
			str += ",,,," + implementation.getCompanyName()
					+ " \n ,,,, PETTY CASH SUMMARY";
			str += "\n Voucher No, Date, Type, Employee, Expense Account, Cash In, Cash Out,"
					+ " Expense, Remaining Balance, Bill No, Settlement Voucher, Narration, Created By\n";
			for (PettyCashVO cash : pettyCashVOs) {

				str += cash.getPettyCashNo()
						+ ","
						+ cash.getPettyCashDate()
						+ ","
						+ (null != cash.getPettyCashTypeName() ? cash
								.getPettyCashTypeName() : " ")
						+ ","
						+ (null != cash.getEmployeeName() ? cash
								.getEmployeeName() : " ")
						+ ","
						+ (null != cash.getExpenseAccount() ? cash
								.getExpenseAccount() : " ") + ",";

				if (cash.getCashIn() != null && (!cash.getCashIn().equals(""))
						&& cash.getCashIn() != 0) {
					str += String.valueOf(cash.getCashIn());
				}
				str += ",";

				if (cash.getCashOut() != null
						&& (!cash.getCashOut().equals(""))
						&& cash.getCashOut() != 0) {
					str += String.valueOf(cash.getCashOut());
				}
				str += ",";

				if (cash.getExpenseAmount() != null
						&& (!cash.getExpenseAmount().equals(""))
						&& cash.getExpenseAmount() != 0) {
					str += String.valueOf(cash.getExpenseAmount());
				}
				str += ",";

				if (cash.getRemainingBalance() != null
						&& (!cash.getRemainingBalance().equals(""))
						&& cash.getRemainingBalance() != 0) {
					str += String.valueOf(cash.getRemainingBalance());
				}
				str += ",";

				str += (null != cash.getInvoiceNumber()
						&& !("").equals(cash.getInvoiceNumber()) ? cash
						.getInvoiceNumber().replaceAll(",", ";") : " ")
						+ ","
						+ (null != cash.getSettledWith() ? cash
								.getSettledWith() : " ")
						+ ","
						+ (null != cash.getDescription()
								&& !("").equals(cash.getDescription()) ? cash
								.getDescription().replaceAll(",", ";") : "");
				str += ",";
				str += (null != cash.getCreatedBy()) ? cash.getCreatedBy() : "";
				str += "\n";
			}

			str += "\n";
			str += "Petty Cash,";
			str += null != pettyCashLog.getCashOutFormat() ? AIOSCommons
					.formatAmountToDouble(pettyCashLog.getCashOutFormat()) : "";
			str += "\n";

			str += "Total Expense,";
			str += null != pettyCashLog.getExpenseAmountFormat() ? AIOSCommons
					.formatAmountToDouble(pettyCashLog.getExpenseAmountFormat())
					: "";
			str += "\n";

			str += "Carry Forward,";
			str += null != pettyCashLog.getCashForwaredStr() ? AIOSCommons
					.formatAmountToDouble(pettyCashLog.getCashForwaredStr())
					: "";
			str += "\n";

			str += "Remaining Balance,";
			str += null != pettyCashLog.getRemainingBalanceFormat() ? AIOSCommons
					.formatAmountToDouble(pettyCashLog
							.getRemainingBalanceFormat()) : "";
			str += "\n";

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getPettyCashPDF() {
		try {

			PettyCash pettyCash = accountsBL.getAccountsEnterpriseService()
					.getPettyCashService().getPettyCashById(recordId);
			List<PettyCashVO> pettyCashVOs = accountsBL.getPettyCashBL()
					.recursivePettyCashLog(pettyCash);
			pettyCashDS = accountsBL.getPettyCashBL().createPettyCashLog(
					pettyCashVOs);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	// get all inventory products
	public String productInventoryPdfPrint() {
		try {
			LOGGER.info("Module: Accounts : Method: productInventoryPdfPrint");
			productCategoryVOs = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL()
					.getInventoryProduct("I");
			productCategoryVO = new ProductCategoryVO();
			productCategoryVO.setSubCategories(productCategoryVOs);
			LOGGER.info("Module: Accounts : Method: productInventoryPdfPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: productInventoryPdfPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// get all inventory products
	public String productFinishedInventoryPdfPrint() {
		try {
			LOGGER.info("Module: Accounts : Method: productFinishedInventoryPdfPrint");
			productCategoryVOs = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL()
					.getProductFinishedInventory();
			productCategoryVO = new ProductCategoryVO();
			productCategoryVO.setSubCategories(productCategoryVOs);
			LOGGER.info("Module: Accounts : Method: productFinishedInventoryPdfPrint: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: productFinishedInventoryPdfPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showTransactionList() {
		LOGGER.info("Module: Accounts : Method: showTransactionList");
		try {
			getImplementId();
			List<Category> categoryList = this.getAccountsBL()
					.getAccountsEnterpriseService().getCategoryService()
					.getAllActiveCategories(implementation);
			List<CombinationVO> combinationVOList = this.getAccountsBL()
					.getCombinationBL()
					.generateCombinationTree(implementation, 0l);
			ServletActionContext.getRequest().setAttribute("CATEGORY_LIST",
					categoryList);

			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			LOGGER.info("Module: Accounts : Method: showTransactionList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showTransactionList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String showGeneralLedger() {
		LOGGER.info("Inside Accounts method showGeneralLedger()");
		try {
			getImplementId();
			List<Category> categoryList = this.getAccountsBL()
					.getAccountsEnterpriseService().getCategoryService()
					.getAllActiveCategories(implementation);
			List<CombinationVO> combinationVOList = this.getAccountsBL()
					.getCombinationBL()
					.generateCombinationTree(implementation, 0l);
			ServletActionContext.getRequest().setAttribute("CATEGORY_LIST",
					categoryList);

			ServletActionContext.getRequest().setAttribute(
					"COMBINATION_TREE_VIEW", combinationVOList);
			LOGGER.info("Accounts method showGeneralLedger() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("showGeneralLedger Method showGeneralLedger() caught exeception "
					+ ex);
			return ERROR;
		}
	}

	public String getLoanDetail() {
		LOGGER.info("Inside Accounts method getLoanDetail");
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();
			LoanTO loanTo = new LoanTO();
			loanId = recordId;
			Loan loan = this.getAccountsBL().getAccountsEnterpriseService()
					.getLoanService().getLoanById(loanId);
			loanTo = AccountsConverter.convertLoanEntity(loan);
			loanList = new ArrayList<LoanTO>();
			loanTo.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			loanTo.setAnchorExpression("HTML".equals(getFormat()) ? "downloadloandetail.action?loanId="
					+ loan.getLoanId() + "&approvalFlag=N&format=PDF"
					: "#");

			loanTo.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			loanTo.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			loanTo.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			loanTo.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny.getCode()
							+ ""));

			List<LoanDetail> loanDetailList = this.getAccountsBL()
					.getAccountsEnterpriseService().getLoanService()
					.getLoanDetailsById(loanId);
			List<LoanCharge> loanChargeList = this.getAccountsBL()
					.getAccountsEnterpriseService().getLoanService()
					.getLoanChargesAllById(loanId);
			loanTo.setLoanDetailList(AccountsConverter
					.convertLoanDetailEntity(loanDetailList));
			loanTo.setLoanChargeList(AccountsConverter
					.convertLoanChargesEntity(loanChargeList));
			if (loanList != null && loanList.size() == 0) {
				loanList.add(loanTo);
			} else {
				loanList.set(0, loanTo);
			}
			LOGGER.info("Inside Accounts method getLoanDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Method getLoanDetail caught exeception " + ex);
			return ERROR;
		}
	}

	public String getPaymentDetail() {
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();
			PaymentTO paymentTo = new PaymentTO();
			long paymentId = recordId;
			Payment payment = this.getAccountsBL()
					.getAccountsEnterpriseService().getPaymentService()
					.getPayment(paymentId);
			paymentTo = AccountsConverter.convertPaymentEntity(payment);
			paymentList = new ArrayList<Payment>();
			paymentTo
					.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			paymentTo
					.setAnchorExpression("HTML".equals(getFormat()) ? "downloadloandetail.action?loanId="
							+ payment.getPaymentId()
							+ "&approvalFlag=N&format=PDF"
							: "#");

			paymentTo.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			paymentTo
					.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
							+ Byte.parseByte(Constants.RealEstate.Status.Approved
									.getCode() + ""));

			paymentTo.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			paymentTo
					.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
							+ Byte.parseByte(Constants.RealEstate.Status.Deny
									.getCode() + ""));

			List<PaymentDetail> paymenetDetailList = this.getAccountsBL()
					.getAccountsEnterpriseService().getPaymentService()
					.getPaymentDetailById(paymentId);
			paymentTo.setPaymentDetailList(AccountsConverter
					.convertPaymentDetailEntity(paymenetDetailList));
			if (paymentList != null && paymentList.size() == 0) {
				paymentList.add(paymentTo);
			} else {
				paymentList.set(0, paymentTo);
			}
			LOGGER.info("Inside Accounts method getPaymentDetail Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Inside Accounts method getPaymentDetail " + ex);
			return ERROR;
		}
	}

	public String showJournalReport() {
		try {
			LOGGER.info("Inside Accounts method showJournalReport()");
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			AccountsTO account = null;
			if (journalId > 0)
				account = accountsBL.showJournalVoucher(journalId);
			else
				account = accountsBL.showTempJournalVoucher(recordId);
			account.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			account.setExportLabel("HTML".equals(getFormat()) ? "Export to Excel"
					: "");
			account.setAnchorExpression("HTML".equals(getFormat()) ? "download_journal_report.action?format=PDF"
					: "#");
			account.setExportAnchorExpression("HTML".equals(getFormat()) ? "show_journal_excel_report.action?journalId="
					+ journalId + "&format=XLS"
					: "#");
			accountsList = new ArrayList<AccountsTO>();
			accountsList.add(account);
			LOGGER.info("Inside Accounts method showJournalReport() SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showJournalReport() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String showStoreDetailReport() {
		try {
			StoreVO storeVO = fetchStoreDetailsData();

			ServletActionContext.getRequest().setAttribute("STORE_DETAILS",
					storeVO);
			LOGGER.info("Module: Accounts : Method: showStoreDetailReport: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showStoreDetailReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private StoreVO fetchStoreDetailsData() throws Exception {
		Store store = accountsBL
				.getAccountsEnterpriseService()
				.getStoreService()
				.getStoreInfoDetails(Integer.parseInt(String.valueOf(recordId)));
		StoreVO storeVO = new StoreVO(store);
		storeVO.setStorageType(store.getLookupDetail().getDisplayName());
		storeVO.setPersonName(store.getPerson().getFirstName().concat(" ")
				.concat(store.getPerson().getLastName()));
		storeVO.setPersonNumber(store.getPerson().getPersonNumber());
		List<AisleVO> aisles = new ArrayList<AisleVO>();
		for (Aisle aisleDetail : store.getAisles()) {
			AisleVO aisleVO = new AisleVO(aisleDetail);
			aisleVO.setSectionType(aisleDetail.getLookupDetail()
					.getDisplayName());

			List<ShelfVO> shelfVOs = new ArrayList<ShelfVO>();
			for (Shelf shelf : aisleDetail.getShelfs()) {
				ShelfVO shelfVO = new ShelfVO(shelf);

				shelfVO.setShelfTypeName(StoreBinType.get(shelf.getShelfType())
						.name());
				shelfVO.setRackTypeName(StoreBinType.get(
						shelf.getShelf().getShelfType()).name());
				shelfVO.setRackDimension(shelf.getShelf().getDimension());

				shelfVOs.add(shelfVO);
			}

			aisleVO.setShelfVOs(shelfVOs);
			aisles.add(aisleVO);
		}
		storeVO.setAisleVOs(aisles);

		return storeVO;
	}

	/**
	 * Store Details Excel Report
	 * 
	 * @return String
	 */
	public String storeDetalReportXLS() {
		try {
			LOGGER.info("Inside Accounts method storeDetalReportXLS()");
			StoreVO store = fetchStoreDetailsData();
			String str = "STORE DETAIL\n\n";
			str += "STORE REFERENCE, STORE NAME, STORE MANAGER, STORAGE TYPE\n";
			str += store.getStoreNumber() + "," + store.getStoreName() + ","
					+ store.getPersonName() + "," + store.getStorageType();
			str += "\n\n AISLE \n\n";
			str += "REFERENCE, DIMENSION, SECTION TYPE \n";
			for (AisleVO aisle : store.getAisleVOs()) {
				str += aisle.getAisleNumber() + "," + aisle.getDimension()
						+ "," + aisle.getSectionType();
				str += "\n\n";

				for (ShelfVO shelf : aisle.getShelfVOs()) {
					str += "\n\n SHELF TYPE, DIMENSION \n";
					str += shelf.getShelfTypeName() + ","
							+ shelf.getDimension();
					str += "\n RACK TYPE, DIMENSION \n";
					str += shelf.getRackTypeName() + ","
							+ shelf.getRackDimension();
				}
			}
			LOGGER.info("Module: Accounts method: storeDetalReportXLS() Success");
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	/**
	 * Store Details in Jasper Report
	 * 
	 * @return String
	 */
	public String storeDetailJasperReport() {
		LOGGER.info("Inside Accounts method storeDetailJasperReport()");
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			entityObjects = new ArrayList<Object>();
			StoreVO storeVO = fetchStoreDetailsData();

			storeVO.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			storeVO.setAnchorExpression("HTML".equals(getFormat()) ? "download_storedetail_jasperreport.action?format=PDF"
					: "#");
			entityObjects.add(storeVO);
			LOGGER.info("Inside Accounts method storeDetailJasperReport Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method storeDetailJasperReport goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showLedgerStatement() {
		try {
			LOGGER.info("Inside Accounts method showLedgerStatement()");
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			JRSwapFile file = new JRSwapFile(
					System.getProperty("java.io.tmpdir"), 2048, 1024);
			JRSwapFileVirtualizer virtualizer = null;
			AccountsTO accounts = new AccountsTO();
			accounts.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			accounts.setAnchorExpression("HTML".equals(getFormat()) ? "download_ledger_report.action?format=PDF"
					: "#");
			getImplementId();
			accountsList = new ArrayList<AccountsTO>();
			accounts.setCategoryId(categoryId);
			accounts.setCodeCombinationId(codeCombinationId);
			accounts.setFromDate(fromDate);
			accounts.setToDate(toDate);
			accounts.setLedgerStatement(this.getAccountsBL()
					.createLedgerStatement(implementation, accounts,
							accountList));
			for (AccountsTO list : accounts.getLedgerStatement()) {
				if (null != list.getDescriptionList()
						&& !("").equals(list.getDescriptionList())) {
					for (AccountsTO dlist : list.getDescriptionList()) {
						if (null != dlist.getDebitBalance()
								&& !("").equals(dlist.getDebitBalance())) {
							dlist.setDebitBalance(AIOSCommons
									.formatAmount(Double.valueOf(dlist
											.getDebitBalance())));
						} else {
							dlist.setCreditBalance(AIOSCommons
									.formatAmount(Double.valueOf(dlist
											.getCreditBalance())));
						}
					}
				}
				if (null != list.getDebitBalanceTotal()) {
					list.setDebitBalanceTotal(AIOSCommons.formatAmount(Double
							.valueOf(Math.abs(Double.parseDouble(list
									.getDebitBalanceTotal())))));
				} else if (null != list.getCreditBalanceTotal()) {
					list.setCreditBalanceTotal(AIOSCommons.formatAmount(Double
							.valueOf(Math.abs(Double.parseDouble(list
									.getCreditBalanceTotal())))));
				}
			}
			accountsList.add(accounts);
			virtualizer = new JRSwapFileVirtualizer(3, file, false);
			jasperRptParams.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			LOGGER.info("Inside Accounts method showLedgerStatement() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showLedgerStatement() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String showConsolidatedBalanceSheet() {
		try {
			LOGGER.info("Inside Accounts method showConsolidatedBalanceSheet");

			LOGGER.info("Inside Accounts method showConsolidatedBalanceSheet() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showConsolidatedBalanceSheet() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String printHtmlConsolidatedBalanceSheet() {
		try {
			LOGGER.info("Inside Accounts method printHtmlConsolidatedBalanceSheet");
			getImplementId();
			List<Ledger> ledgers = accountsBL
					.getBalanceSheetCombination(implementation);
			Collections.sort(ledgers, new Comparator<Ledger>() {
				public int compare(Ledger o1, Ledger o2) {
					return o1.getLedgerId().compareTo(o2.getLedgerId());
				}
			});
			TransactionVO transactionVO = null;
			List<TransactionDetail> assetTransactionDetails = new ArrayList<TransactionDetail>();
			List<TransactionDetail> liabilityTransactionDetails = new ArrayList<TransactionDetail>();
			List<TransactionDetail> ownersEquityTransactionDetails = new ArrayList<TransactionDetail>();
			TransactionDetail transactionDetail = null;
			TransactionDetailVO retainedEarnings = null;
			for (Ledger ledger : ledgers) {
				transactionDetail = new TransactionDetail();
				transactionDetail.setAmount(ledger.getBalance());
				transactionDetail.setIsDebit(ledger.getSide());
				transactionDetail.setCombination(ledger.getCombination());
				if (null != ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountType()
						&& (int) ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.Assets
								.getCode())
					assetTransactionDetails.add(transactionDetail);

				else if (null != ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountType()
						&& ((int) ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.Liabilites
								.getCode()))
					liabilityTransactionDetails.add(transactionDetail);

				else if (null != ledger.getCombination()
						.getAccountByNaturalAccountId().getAccountType()
						&& ((int) ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType() == (int) AccountType.OwnersEquity
								.getCode()))
					ownersEquityTransactionDetails.add(transactionDetail);
			}

			List<Ledger> revenueExpenseLedgers = accountsBL
					.getIncomenStatementCombination(implementation);
			transactionVO = new TransactionVO();
			if (null != revenueExpenseLedgers
					&& revenueExpenseLedgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger o1, Ledger o2) {
						return o1.getLedgerId().compareTo(o2.getLedgerId());
					}
				});

				if (null != revenueExpenseLedgers
						&& revenueExpenseLedgers.size() > 0) {
					Collections.sort(ledgers, new Comparator<Ledger>() {
						public int compare(Ledger o1, Ledger o2) {
							return o1.getLedgerId().compareTo(o2.getLedgerId());
						}
					});
					List<TransactionDetail> revenueTransactionDetails = new ArrayList<TransactionDetail>();
					List<TransactionDetail> expenseTransactionDetails = new ArrayList<TransactionDetail>();
					for (Ledger ledger : revenueExpenseLedgers) {
						transactionDetail = new TransactionDetail();
						transactionDetail.setAmount(ledger.getBalance());
						transactionDetail.setIsDebit(ledger.getSide());
						transactionDetail.setCombination(ledger
								.getCombination());
						if (null != ledger.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType()
								&& (int) ledger.getCombination()
										.getAccountByNaturalAccountId()
										.getAccountType() == (int) AccountType.Revenue
										.getCode())
							revenueTransactionDetails.add(transactionDetail);
						else
							expenseTransactionDetails.add(transactionDetail);
					}
					TransactionVO revenueTransactionVO = accountsBL
							.processRevenueIncomeStatement(revenueTransactionDetails);

					TransactionVO expenseTransactionVO = accountsBL
							.processExpenseIncomeStatement(expenseTransactionDetails);
					retainedEarnings = this.processRetainEarningsData(
							revenueTransactionVO, expenseTransactionVO);
				}
			}
			List<TransactionDetailVO> assetTransactionDetailVOs = accountsBL
					.processAuditAssetStatement(assetTransactionDetails);

			List<TransactionDetailVO> liabilityTransactionDetailVOs = accountsBL
					.processAuditLiabilityStatement(liabilityTransactionDetails);

			List<TransactionDetailVO> ownersEquityTransactionDetailVOs = accountsBL
					.processAuditLiabilityStatement(ownersEquityTransactionDetails);

			List<TransactionDetailVO> finalAssetViewList = new ArrayList<TransactionDetailVO>();
			List<TransactionDetailVO> finalLiabilityViewList = new ArrayList<TransactionDetailVO>();

			finalAssetViewList = processAccountTypeDetailNew(
					assetTransactionDetailVOs, AccountType.Assets.getCode());
			if (null == finalAssetViewList || finalAssetViewList.size() == 0)
				finalAssetViewList.addAll(assetTransactionDetailVOs);

			finalLiabilityViewList = processAccountTypeDetailNew(
					liabilityTransactionDetailVOs,
					AccountType.Liabilites.getCode());
			if (null == finalLiabilityViewList
					|| finalLiabilityViewList.size() == 0)
				finalLiabilityViewList.addAll(liabilityTransactionDetailVOs);

			List<TransactionDetailVO> ownersEquityList = processAccountTypeDetailNew(
					ownersEquityTransactionDetailVOs,
					AccountType.OwnersEquity.getCode());
			if (null != ownersEquityList && ownersEquityList.size() > 0)
				finalLiabilityViewList.addAll(ownersEquityList);
			else
				finalLiabilityViewList.addAll(ownersEquityTransactionDetailVOs);

			if (null != retainedEarnings)
				finalLiabilityViewList.add(retainedEarnings);

			List<TransactionDetailVO> detailVOs = null;
			List<TransactionDetailVO> tempDetailVOs = null;
			TransactionDetailVO detailVO = null;
			double amount = 0;
			if (null != finalAssetViewList && finalAssetViewList.size() > 0) {
				double assetAmount = 0;
				detailVOs = new ArrayList<TransactionDetailVO>();
				for (TransactionDetailVO transactionDetailVO : finalAssetViewList) {
					if (null != transactionDetailVO
							.getTransactionDetailVOGroup()
							&& transactionDetailVO
									.getTransactionDetailVOGroup().size() > 0) {
						for (Entry<String, List<TransactionDetailVO>> entry : transactionDetailVO
								.getTransactionDetailVOGroup().entrySet()) {
							detailVO = new TransactionDetailVO();
							detailVO.setAccountSubType(entry.getKey());
							tempDetailVOs = new ArrayList<TransactionDetailVO>();
							for (TransactionDetailVO vo : entry.getValue()) {
								amount = AIOSCommons.formatAmountToDouble(vo
										.getAssetAmount());
								vo.setAssetAmount(amount >= 0 ? AIOSCommons
										.formatAmount(amount) : "( "
										+ AIOSCommons.formatAmount(Math
												.abs(amount)) + " )");
								assetAmount += (amount);
								tempDetailVOs.add(vo);
							}
							detailVO.setTransactionDetailVOs(tempDetailVOs);
							detailVOs.add(detailVO);
						}
					} else {
						amount = AIOSCommons
								.formatAmountToDouble(transactionDetailVO
										.getAssetAmount());
						transactionDetailVO
								.setAssetAmount(amount >= 0 ? AIOSCommons
										.formatAmount(amount) : "( "
										+ AIOSCommons.formatAmount(Math
												.abs(amount)) + " )");
						assetAmount += (amount);
						detailVOs.add(transactionDetailVO);
					}
				}
				transactionVO.setAssetAmount(assetAmount >= 0 ? AIOSCommons
						.formatAmount(assetAmount) : "( "
						+ AIOSCommons.formatAmount(Math.abs(assetAmount))
						+ " )");
				transactionVO.setTransactionDetailVOs(detailVOs);
			}

			if (null != finalLiabilityViewList
					&& finalLiabilityViewList.size() > 0) {
				double liabilityAmount = 0;
				detailVOs = new ArrayList<TransactionDetailVO>();
				for (TransactionDetailVO transactionDetailVO : finalLiabilityViewList) {
					if (null != transactionDetailVO
							.getTransactionDetailVOGroup()
							&& transactionDetailVO
									.getTransactionDetailVOGroup().size() > 0) {
						for (Entry<String, List<TransactionDetailVO>> entry : transactionDetailVO
								.getTransactionDetailVOGroup().entrySet()) {
							detailVO = new TransactionDetailVO();
							detailVO.setAccountSubType(entry.getKey());
							tempDetailVOs = new ArrayList<TransactionDetailVO>();
							for (TransactionDetailVO vo : entry.getValue()) {
								amount = AIOSCommons.formatAmountToDouble(vo
										.getLiabilityAmount());
								vo.setLiabilityAmount(amount >= 0 ? AIOSCommons
										.formatAmount(amount) : "( "
										+ AIOSCommons.formatAmount(Math
												.abs(amount)) + " )");
								liabilityAmount += (amount);
								tempDetailVOs.add(vo);
							}
							detailVO.setTransactionDetailVOs(entry.getValue());
							detailVOs.add(detailVO);
						}
					} else {
						amount = AIOSCommons
								.formatAmountToDouble(transactionDetailVO
										.getLiabilityAmount());
						transactionDetailVO
								.setLiabilityAmount(amount >= 0 ? AIOSCommons
										.formatAmount(amount) : "( "
										+ AIOSCommons.formatAmount(Math
												.abs(amount)) + " )");
						liabilityAmount += (amount);
						detailVOs.add(transactionDetailVO);
					}
				}
				transactionVO
						.setLiabilityAmount(liabilityAmount >= 0 ? AIOSCommons
								.formatAmount(liabilityAmount) : "( "
								+ AIOSCommons.formatAmount(Math
										.abs(liabilityAmount)) + " )");
				transactionVO.setTransactionDetailVOCs(detailVOs);
			}

			ServletActionContext.getRequest().setAttribute(
					"CONSOLIDATED_BALANCESHEET", transactionVO);
			ServletActionContext.getRequest().setAttribute(
					"PRINT_DATE",
					DateFormat.convertDateToString(DateTime.now().toLocalDate()
							.toString()));
			LOGGER.info("Inside Accounts method printHtmlConsolidatedBalanceSheet() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method printHtmlConsolidatedBalanceSheet() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String printHtmlConsolidatedIncomeStatement() {
		LOGGER.info("Inside Accounts method printHtmlConsolidatedIncomeStatement");
		try {
			getImplementId();
			List<Ledger> ledgers = accountsBL
					.getIncomenStatementCombination(implementation);
			if (null != ledgers && ledgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger o1, Ledger o2) {
						return o1.getLedgerId().compareTo(o2.getLedgerId());
					}
				});

				TransactionVO transactionVO = new TransactionVO();
				List<TransactionDetail> revenueTransactionDetails = new ArrayList<TransactionDetail>();
				List<TransactionDetail> expenseTransactionDetails = new ArrayList<TransactionDetail>();

				TransactionDetail transactionDetail = null;

				for (Ledger ledger : ledgers) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setAmount(ledger.getBalance());
					transactionDetail.setIsDebit(ledger.getSide());
					transactionDetail.setCombination(ledger.getCombination());
					if (null != ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType()
							&& (int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Revenue
									.getCode())
						revenueTransactionDetails.add(transactionDetail);

					else if (null != ledger.getCombination()
							.getAccountByNaturalAccountId().getAccountType()
							&& ((int) ledger.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType() == (int) AccountType.Expenses
									.getCode()))
						expenseTransactionDetails.add(transactionDetail);

				}

				List<TransactionDetailVO> revenueTransactionDetailVO = accountsBL
						.processAuditRevenueStatement(revenueTransactionDetails);

				List<TransactionDetailVO> expenseTransactionDetailVO = accountsBL
						.processAuditExpenseStatement(expenseTransactionDetails);

				List<TransactionDetailVO> finalRevenueViewList = new ArrayList<TransactionDetailVO>();
				List<TransactionDetailVO> finalExpenseViewList = new ArrayList<TransactionDetailVO>();

				finalRevenueViewList = processAccountTypeDetailNew(
						revenueTransactionDetailVO,
						AccountType.Revenue.getCode());
				if (null == finalRevenueViewList
						|| finalRevenueViewList.size() == 0)
					finalRevenueViewList.addAll(revenueTransactionDetailVO);

				finalExpenseViewList = processAccountTypeDetailNew(
						expenseTransactionDetailVO,
						AccountType.Expenses.getCode());
				if (null == finalExpenseViewList
						|| finalExpenseViewList.size() == 0)
					finalExpenseViewList.addAll(expenseTransactionDetailVO);

				List<TransactionDetailVO> detailVOs = null;
				List<TransactionDetailVO> tempDetailVOs = null;
				TransactionDetailVO detailVO = null;
				double amount = 0;
				double expenseAmount = 0;
				double revenueAmount = 0;
				if (null != finalRevenueViewList
						&& finalRevenueViewList.size() > 0) {
					detailVOs = new ArrayList<TransactionDetailVO>();
					for (TransactionDetailVO transactionDetailVO : finalRevenueViewList) {
						if (null != transactionDetailVO
								.getTransactionDetailVOGroup()
								&& transactionDetailVO
										.getTransactionDetailVOGroup().size() > 0) {
							for (Entry<String, List<TransactionDetailVO>> entry : transactionDetailVO
									.getTransactionDetailVOGroup().entrySet()) {
								detailVO = new TransactionDetailVO();
								detailVO.setAccountSubType(entry.getKey());
								tempDetailVOs = new ArrayList<TransactionDetailVO>();
								for (TransactionDetailVO vo : entry.getValue()) {
									amount = AIOSCommons
											.formatAmountToDouble(vo
													.getRevenueAmount());
									vo.setRevenueAmount(amount >= 0 ? AIOSCommons
											.formatAmount(amount) : "( "
											+ AIOSCommons.formatAmount(Math
													.abs(amount)) + " )");
									revenueAmount += (amount);
									tempDetailVOs.add(vo);
								}
								detailVO.setTransactionDetailVOs(tempDetailVOs);
								detailVOs.add(detailVO);
							}
						} else {
							amount = AIOSCommons
									.formatAmountToDouble(transactionDetailVO
											.getRevenueAmount());
							transactionDetailVO
									.setRevenueAmount(amount >= 0 ? AIOSCommons
											.formatAmount(amount) : "( "
											+ AIOSCommons.formatAmount(Math
													.abs(amount)) + " )");
							revenueAmount += (amount);
							detailVOs.add(transactionDetailVO);
						}
					}
					transactionVO
							.setRevenueAmount(revenueAmount >= 0 ? AIOSCommons
									.formatAmount(revenueAmount) : "( "
									+ AIOSCommons.formatAmount(Math
											.abs(revenueAmount)) + " )");
					transactionVO.setTransactionDetailVOs(detailVOs);
				}

				if (null != finalExpenseViewList
						&& finalExpenseViewList.size() > 0) {
					detailVOs = new ArrayList<TransactionDetailVO>();
					for (TransactionDetailVO transactionDetailVO : finalExpenseViewList) {
						if (null != transactionDetailVO
								.getTransactionDetailVOGroup()
								&& transactionDetailVO
										.getTransactionDetailVOGroup().size() > 0) {
							for (Entry<String, List<TransactionDetailVO>> entry : transactionDetailVO
									.getTransactionDetailVOGroup().entrySet()) {
								detailVO = new TransactionDetailVO();
								detailVO.setAccountSubType(entry.getKey());
								tempDetailVOs = new ArrayList<TransactionDetailVO>();
								for (TransactionDetailVO vo : entry.getValue()) {
									amount = AIOSCommons
											.formatAmountToDouble(vo
													.getExpenseAmount());
									vo.setExpenseAmount(amount >= 0 ? AIOSCommons
											.formatAmount(amount) : "( "
											+ AIOSCommons.formatAmount(Math
													.abs(amount)) + " )");
									expenseAmount += (amount);
									tempDetailVOs.add(vo);
								}
								detailVO.setTransactionDetailVOs(entry
										.getValue());
								detailVOs.add(detailVO);
							}
						} else {
							amount = AIOSCommons
									.formatAmountToDouble(transactionDetailVO
											.getExpenseAmount());
							transactionDetailVO
									.setExpenseAmount(amount >= 0 ? AIOSCommons
											.formatAmount(amount) : "( "
											+ AIOSCommons.formatAmount(Math
													.abs(amount)) + " )");
							expenseAmount += (amount);
							detailVOs.add(transactionDetailVO);
						}
					}
					transactionVO
							.setExpenseAmount(expenseAmount >= 0 ? AIOSCommons
									.formatAmount(expenseAmount) : "( "
									+ AIOSCommons.formatAmount(Math
											.abs(expenseAmount)) + " )");
					transactionVO.setTransactionDetailVOCs(detailVOs);
				}
				if (revenueAmount > expenseAmount) {
					amount = revenueAmount > 0 ? (revenueAmount - expenseAmount)
							: (revenueAmount + expenseAmount);
					transactionVO.setLabel(amount >= 0 ? "Net Profit"
							: "Net Loss");

				} else {
					amount = expenseAmount > 0 ? (expenseAmount - revenueAmount)
							: (revenueAmount + expenseAmount);
					transactionVO.setLabel(amount <= 0 ? "Net Profit"
							: "Net Loss");
				}
				transactionVO.setNetAmount(amount > 0 ? AIOSCommons
						.formatAmount(amount) : "( "
						+ AIOSCommons.formatAmount(Math.abs(amount)) + " )");
				ServletActionContext.getRequest().setAttribute(
						"CONSOLIDATED_INCOMESTATEMENT", transactionVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"PRINT_DATE",
					DateFormat.convertDateToString(DateTime.now().toLocalDate()
							.toString()));
			LOGGER.info("Inside Accounts method printHtmlConsolidatedIncomeStatement Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method printHtmlConsolidatedIncomeStatement goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showBalanceSheet() {
		try {
			LOGGER.info("Inside Accounts method showBalanceSheet");
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();
			ledgerList = accountsBL.getBalanceSheetCombination(implementation);
			Collections.sort(ledgerList, new Comparator<Ledger>() {
				@Override
				public int compare(Ledger o1, Ledger o2) {
					return o1.getLedgerId().compareTo(o2.getLedgerId());
				}
			});
			List<AccountsTO> assetList = accountsBL.getAssetDetails(ledgerList);
			List<AccountsTO> liabilityList = accountsBL
					.getLiabilityDetails(ledgerList);
			AccountsTO account = getIncomeStament(implementation);
			liabilityList.add(account);
			account.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			account.setAnchorExpression("HTML".equals(getFormat()) ? "download_balancesheet.action?format=PDF"
					: "#");
			account.setAssetList(assetList);
			account.setLiabilityList(liabilityList);
			double assetTotal = accountsBL.calculateAssetTotal(account
					.getAssetList());
			double liabilityTotal = accountsBL.calculateLiabilityTotal(account
					.getLiabilityList());
			account.setAssetTotal(AIOSCommons.formatAmount(Math.abs(assetTotal)));
			account.setLiabilityTotal(AIOSCommons.formatAmount(Math
					.abs(liabilityTotal)));
			accountsList = new ArrayList<AccountsTO>();
			for (AccountsTO asset : account.getAssetList()) {
				if (Double.valueOf(asset.getAssetBalanceAmount().replaceAll(
						",", "")) > 0) {
					asset.setAssetBalanceAmount(AIOSCommons.formatAmount(Double
							.valueOf(asset.getAssetBalanceAmount().replaceAll(
									",", ""))));
				} else {
					asset.setAssetBalanceAmount(AIOSCommons.formatAmount(Double
							.valueOf(asset.getAssetBalanceAmount().replaceAll(
									",", ""))));
					asset.setAssetBalanceAmount(asset
							.getAssetBalanceAmount()
							.replace("$", "-")
							.substring(0,
									asset.getAssetBalanceAmount().length() - 1));
				}
			}
			for (AccountsTO liability : account.getLiabilityList()) {
				if (Double.valueOf(liability.getLiabilityBalanceAmount()
						.replaceAll(",", "")) > 0) {
					liability.setLiabilityBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(liability
									.getLiabilityBalanceAmount().replaceAll(
											",", ""))));
				} else {
					liability.setLiabilityBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(liability
									.getLiabilityBalanceAmount().replaceAll(
											",", ""))));
					liability.setLiabilityBalanceAmount(liability
							.getLiabilityBalanceAmount()
							.replace("$", "-")
							.substring(
									0,
									liability.getLiabilityBalanceAmount()
											.length() - 1));
				}
			}
			accountsList.add(account);
			LOGGER.info("Inside Accounts method showBalanceSheet Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method showBalanceSheet goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showConsolidatedTrialBalance() {
		try {
			LOGGER.info("Inside Accounts method showConsolidatedTrialBalance");

			LOGGER.info("Inside Accounts method showConsolidatedTrialBalance() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showConsolidatedTrialBalance() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String printHtmlConsolidatedTrialBalance() {
		try {
			LOGGER.info("Inside Accounts method printHtmlConsolidatedTrialBalance");
			getImplementId();
			List<Ledger> ledgers = accountsBL
					.getTrailBalanceCombination(implementation);
			if (null != ledgers && ledgers.size() > 0) {
				Collections.sort(ledgers, new Comparator<Ledger>() {
					public int compare(Ledger o1, Ledger o2) {
						return o1.getLedgerId().compareTo(o2.getLedgerId());
					}
				});
				TransactionVO transactionVO = new TransactionVO();
				List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
				TransactionDetail transactionDetail = null;
				for (Ledger ledger : ledgers) {
					transactionDetail = new TransactionDetail();
					transactionDetail.setAmount(ledger.getBalance());
					transactionDetail.setIsDebit(ledger.getSide());
					transactionDetail.setCombination(ledger.getCombination());
					transactionDetails.add(transactionDetail);
				}

				List<TransactionDetailVO> auditTransactionVO = accountsBL
						.processAuditTrialStatement(transactionDetails);

				Map<Integer, List<TransactionDetailVO>> accountTypeMap = new HashMap<Integer, List<TransactionDetailVO>>();
				List<TransactionDetailVO> finalDebitCreditViewList = new ArrayList<TransactionDetailVO>();
				List<TransactionDetailVO> tempList = null;
				TransactionDetailVO tempTransactionDetailVO = null;

				Map<String, List<TransactionDetailVO>> entryTempMap = null;
				for (TransactionDetailVO vo : auditTransactionVO) {
					if (null != vo.getTransactionDetailVOGroup()
							&& vo.getTransactionDetailVOGroup().size() > 0) {
						for (Entry<String, List<TransactionDetailVO>> entry : vo
								.getTransactionDetailVOGroup().entrySet()) {
							tempTransactionDetailVO = new TransactionDetailVO();
							entryTempMap = new HashMap<String, List<TransactionDetailVO>>();
							entryTempMap.put(entry.getKey(), entry.getValue());
							tempTransactionDetailVO
									.setTransactionDetailVOGroup(entryTempMap);
							tempList = new ArrayList<TransactionDetailVO>();
							if (null != accountTypeMap
									&& accountTypeMap.size() > 0
									&& accountTypeMap.containsKey(entryTempMap
											.get(entry.getKey()).get(0)
											.getCombination()
											.getAccountByNaturalAccountId()
											.getAccountType()))
								tempList = accountTypeMap.get(entryTempMap
										.get(entry.getKey()).get(0)
										.getCombination()
										.getAccountByNaturalAccountId()
										.getAccountType());
							tempList.add(tempTransactionDetailVO);
							accountTypeMap.put(entryTempMap.get(entry.getKey())
									.get(0).getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType(), tempList);
						}
					} else {
						tempList = new ArrayList<TransactionDetailVO>();
						if (null != accountTypeMap
								&& accountTypeMap.size() > 0
								&& accountTypeMap.containsKey(vo
										.getCombination()
										.getAccountByNaturalAccountId()
										.getAccountType()))
							tempList = accountTypeMap.get(vo.getCombination()
									.getAccountByNaturalAccountId()
									.getAccountType());
						tempList.add(vo);
						accountTypeMap.put(vo.getCombination()
								.getAccountByNaturalAccountId()
								.getAccountType(), tempList);
					}
				}

				for (Entry<Integer, List<TransactionDetailVO>> entry : accountTypeMap
						.entrySet()) {
					if ((int) entry.getKey() == (int) AccountType.Assets
							.getCode()) {
						tempList = processAccountTypeDetailNew(
								entry.getValue(), entry.getKey());
						finalDebitCreditViewList.addAll(tempList);
					} else if ((int) entry.getKey() == (int) AccountType.Liabilites
							.getCode()) {
						tempList = processAccountTypeDetailNew(
								entry.getValue(), entry.getKey());
						finalDebitCreditViewList.addAll(tempList);
					} else if ((int) entry.getKey() == (int) AccountType.Revenue
							.getCode()) {
						tempList = processAccountTypeDetailNew(
								entry.getValue(), entry.getKey());
						finalDebitCreditViewList.addAll(tempList);
					} else if ((int) entry.getKey() == (int) AccountType.Expenses
							.getCode()) {
						tempList = processAccountTypeDetailNew(
								entry.getValue(), entry.getKey());
						finalDebitCreditViewList.addAll(tempList);
					} else if ((int) entry.getKey() == (int) AccountType.OwnersEquity
							.getCode()) {
						tempList = processAccountTypeDetailNew(
								entry.getValue(), entry.getKey());
						finalDebitCreditViewList.addAll(tempList);
					}
				}

				if (null != finalDebitCreditViewList
						&& finalDebitCreditViewList.size() > 0) {
					List<TransactionDetailVO> detailVOs = new ArrayList<TransactionDetailVO>();
					List<TransactionDetailVO> tempDetailVOs = null;
					TransactionDetailVO detailVO = null;
					double debitAmount = 0;
					double creditAmount = 0;

					for (TransactionDetailVO transactionDetailVO : finalDebitCreditViewList) {
						if (null != transactionDetailVO
								.getTransactionDetailVOGroup()
								&& transactionDetailVO
										.getTransactionDetailVOGroup().size() > 0) {
							for (Entry<String, List<TransactionDetailVO>> entry : transactionDetailVO
									.getTransactionDetailVOGroup().entrySet()) {
								detailVO = new TransactionDetailVO();
								detailVO.setAccountSubType(entry.getKey());
								tempDetailVOs = new ArrayList<TransactionDetailVO>();
								for (TransactionDetailVO vo : entry.getValue()) {
									if (null != vo.getDebitAmount())
										debitAmount += AIOSCommons
												.formatAmountToDouble(vo
														.getDebitAmount());
									else
										creditAmount += AIOSCommons
												.formatAmountToDouble(vo
														.getCreditAmount());
									tempDetailVOs.add(vo);
								}
								detailVO.setTransactionDetailVOs(tempDetailVOs);
								detailVOs.add(detailVO);
							}
						} else {
							if (null != transactionDetailVO.getDebitAmount())
								debitAmount += AIOSCommons
										.formatAmountToDouble(transactionDetailVO
												.getDebitAmount());
							else
								creditAmount += AIOSCommons
										.formatAmountToDouble(transactionDetailVO
												.getCreditAmount());
							detailVOs.add(transactionDetailVO);
						}
					}
					transactionVO.setTransactionDetailVOs(detailVOs);
					transactionVO.setDebitAmount(AIOSCommons
							.formatAmount(debitAmount));
					transactionVO.setCreditAmount(AIOSCommons
							.formatAmount(creditAmount));
				}
				ServletActionContext.getRequest().setAttribute(
						"CONSOLIDATED_TRIALBALANCE", transactionVO);
			}
			ServletActionContext.getRequest().setAttribute(
					"PRINT_DATE",
					DateFormat.convertDateToString(DateTime.now().toLocalDate()
							.toString()));
			LOGGER.info("Inside Accounts method printHtmlConsolidatedTrialBalance() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method printHtmlConsolidatedTrialBalance() caught exception "
					+ ex);
			return ERROR;
		}
	}

	private List<AccountsTO> processAccountTypeDetail(
			List<AccountsTO> accountList, Integer accountType,
			boolean isTrialBalance) throws Exception {
		List<AccountGroup> accountGroups = null;
		accountGroups = accountsBL.getAccountGroupBL().getAccountGroupService()
				.getAccountGroupByType(implementation, accountType);
		Map<String, Map<Long, Combination>> accountGroupMap = new HashMap<String, Map<Long, Combination>>();
		Map<Long, Combination> detailMap = null;
		for (AccountGroup accountGroup : accountGroups) {
			detailMap = new HashMap<Long, Combination>();
			for (AccountGroupDetail groupDetail : accountGroup
					.getAccountGroupDetails()) {
				detailMap.put(groupDetail.getCombination().getCombinationId(),
						groupDetail.getCombination());
			}
			accountGroupMap.put(accountGroup.getTitle(), detailMap);
		}
		return accountsBL.processGroupDetail(accountList, accountGroupMap,
				accountType, isTrialBalance);
	}

	private List<TransactionDetailVO> processAccountTypeDetailNew(
			List<TransactionDetailVO> transactionDetailVOs, Integer accountType)
			throws Exception {
		List<AccountGroup> accountGroups = null;
		accountGroups = accountsBL.getAccountGroupBL().getAccountGroupService()
				.getAccountGroupByType(implementation, accountType);
		Map<String, Map<Long, Combination>> accountGroupMap = new HashMap<String, Map<Long, Combination>>();
		Map<Long, Combination> detailMap = null;
		for (AccountGroup accountGroup : accountGroups) {
			detailMap = new HashMap<Long, Combination>();
			for (AccountGroupDetail groupDetail : accountGroup
					.getAccountGroupDetails()) {
				detailMap.put(groupDetail.getCombination().getCombinationId(),
						groupDetail.getCombination());
			}
			accountGroupMap.put(accountGroup.getTitle(), detailMap);
		}
		return accountsBL.processGroupDetailNew(transactionDetailVOs,
				accountGroupMap, accountType);
	}

	public String showConsolidatedIncomeStatement() {
		try {
			LOGGER.info("Inside Accounts method showConsolidatedIncomeStatement");

			LOGGER.info("Inside Accounts method showConsolidatedIncomeStatement() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showConsolidatedIncomeStatement() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String showIncomeStatement() {
		LOGGER.info("Inside Accounts method showIncomeStatement");
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();
			ledgerList = accountsBL
					.getIncomenStatementCombination(implementation);
			Collections.sort(ledgerList, new Comparator<Ledger>() {
				@Override
				public int compare(Ledger l1, Ledger l2) {
					return l1.getLedgerId().compareTo(l2.getLedgerId());
				}
			});
			List<AccountsTO> expenseList = accountsBL
					.getExpenseStatement(ledgerList);
			List<AccountsTO> revenueList = accountsBL
					.getRevenueStatement(ledgerList);
			AccountsTO account = new AccountsTO();
			account.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			account.setAnchorExpression("HTML".equals(getFormat()) ? "download_incomestatement.action?format=PDF"
					: "#");
			account.setExpenseList(expenseList);
			account.setRevenueList(revenueList);
			double expenseTotal = accountsBL.calcucateExpense(account
					.getExpenseList());
			double revenueTotal = accountsBL.calcucateRevenue(account
					.getRevenueList());
			if (expenseTotal > 0)
				account.setExpenseTotal(AIOSCommons.formatAmount(expenseTotal));
			else {
				account.setExpenseTotal(AIOSCommons.formatAmount(Double
						.valueOf(expenseTotal)));
				account.setExpenseTotal(account.getExpenseTotal()
						.replace('$', '-')
						.substring(0, account.getExpenseTotal().length() - 1));
			}
			if (revenueTotal > 0)
				account.setRevenueTotal(AIOSCommons.formatAmount(revenueTotal));
			else {
				account.setRevenueTotal(AIOSCommons.formatAmount(Double
						.valueOf(revenueTotal)));
				account.setRevenueTotal(account.getRevenueTotal()
						.replace('$', '-')
						.substring(0, account.getRevenueTotal().length() - 1));
			}
			accountsList = new ArrayList<AccountsTO>();
			for (AccountsTO expense : account.getExpenseList()) {
				if (Double.valueOf(expense.getExpenseBalanceAmount()) > 0) {
					expense.setExpenseBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(expense
									.getExpenseBalanceAmount())));
				} else {
					expense.setExpenseBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(expense
									.getExpenseBalanceAmount())));
					expense.setExpenseBalanceAmount(expense
							.getExpenseBalanceAmount()
							.replace('$', '-')
							.substring(
									0,
									expense.getExpenseBalanceAmount().length() - 1));
				}
				if (!expense.getLedgerSide()
						&& Double.valueOf(expense.getExpenseBalanceAmount()
								.replaceAll(",", "")) > 0) {
					expense.setExpenseBalanceAmount("-"
							+ expense.getExpenseBalanceAmount());
				}
			}
			for (AccountsTO revenue : account.getRevenueList()) {
				if (Double.valueOf(revenue.getRevenueBalanceAmount()) > 0) {
					revenue.setRevenueBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(revenue
									.getRevenueBalanceAmount())));
				} else {
					revenue.setRevenueBalanceAmount(AIOSCommons
							.formatAmount(Double.valueOf(revenue
									.getRevenueBalanceAmount())));
					revenue.setRevenueBalanceAmount(revenue
							.getRevenueBalanceAmount()
							.replace('$', '-')
							.substring(
									0,
									revenue.getRevenueBalanceAmount().length() - 1));
				}
				if (revenue.getLedgerSide()
						&& Double.valueOf(revenue.getRevenueBalanceAmount()
								.replaceAll(",", "")) > 0) {
					revenue.setRevenueBalanceAmount("-"
							+ revenue.getRevenueBalanceAmount());
				}
			}
			double tempNetAmount;
			if (revenueTotal > expenseTotal) {
				if (revenueTotal > 0)
					tempNetAmount = revenueTotal - expenseTotal;
				else
					tempNetAmount = revenueTotal + expenseTotal;
				if (tempNetAmount > 0) {
					account.setNetLabel("Net Profit");
					account.setNetAmount(AIOSCommons.formatAmount(Double
							.valueOf(tempNetAmount)));
				} else {
					account.setNetLabel("Net Loss");
					account.setNetAmount(AIOSCommons.formatAmount(Double
							.valueOf(tempNetAmount)));
					account.setNetAmount(account.getNetAmount().substring(1,
							account.getNetAmount().length() - 1));
				}
			} else {
				if (expenseTotal > 0)
					tempNetAmount = expenseTotal - revenueTotal;
				else
					tempNetAmount = expenseTotal + revenueTotal;
				if (tempNetAmount > 0) {
					account.setNetLabel("Net Loss");
					account.setNetAmount(AIOSCommons.formatAmount(Double
							.valueOf(tempNetAmount)));
				} else {
					account.setNetLabel("Net Profit");
					account.setNetAmount(AIOSCommons.formatAmount(Double
							.valueOf(tempNetAmount)));
					account.setNetAmount(account.getNetAmount().substring(1,
							account.getNetAmount().length() - 1));
				}
			}
			accountsList.add(account);
			LOGGER.info("Inside Accounts method showIncomeStatement Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method showIncomeStatement goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Income Statement from Ledger fiscal
	public String ledgerFiscalIncomeStatement() {
		LOGGER.info("Inside Accounts method ledgerFiscalIncomeStatement");
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			accountsList = this.fetchFiscalIncomeStatement();

			LOGGER.info("Inside Accounts method ledgerFiscalIncomeStatement Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method ledgerFiscalIncomeStatement goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private List<AccountsTO> fetchFiscalIncomeStatement() throws Exception {

		List<AccountsTO> accountsToList = new ArrayList<AccountsTO>();
		getImplementId();
		List<LedgerFiscal> ledgerFiscals = accountsBL
				.getIncomenStatementLedgerFiscal(periodId);
		Collections.sort(ledgerFiscals, new Comparator<LedgerFiscal>() {
			@Override
			public int compare(LedgerFiscal l1, LedgerFiscal l2) {
				return l1.getLedgerFiscalId().compareTo(l2.getLedgerFiscalId());
			}
		});
		List<AccountsTO> expenseList = accountsBL
				.getExpenseStatementFiscal(ledgerFiscals);
		List<AccountsTO> revenueList = accountsBL
				.getRevenueStatementFiscal(ledgerFiscals);
		AccountsTO account = new AccountsTO();
		account.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
		account.setAnchorExpression("HTML".equals(getFormat()) ? "download_incomestatement_fiscalreport.action?format=PDF"
				: "#");
		account.setExpenseList(expenseList);
		account.setRevenueList(revenueList);
		double expenseTotal = accountsBL.calcucateExpense(account
				.getExpenseList());
		double revenueTotal = accountsBL.calcucateRevenue(account
				.getRevenueList());
		if (expenseTotal > 0)
			account.setExpenseTotal(AIOSCommons.formatAmount(expenseTotal));
		else {
			account.setExpenseTotal(AIOSCommons.formatAmount(Double
					.valueOf(expenseTotal)));
			account.setExpenseTotal(account.getExpenseTotal().replace('$', '-')
					.substring(0, account.getExpenseTotal().length() - 1));
		}
		if (revenueTotal > 0)
			account.setRevenueTotal(AIOSCommons.formatAmount(revenueTotal));
		else {
			account.setRevenueTotal(AIOSCommons.formatAmount(Double
					.valueOf(revenueTotal)));
			account.setRevenueTotal(account.getRevenueTotal().replace('$', '-')
					.substring(0, account.getRevenueTotal().length() - 1));
		}
		for (AccountsTO expense : account.getExpenseList()) {
			if (Double.valueOf(expense.getExpenseBalanceAmount()) > 0) {
				expense.setExpenseBalanceAmount(AIOSCommons.formatAmount(Double
						.valueOf(expense.getExpenseBalanceAmount())));
			} else {
				expense.setExpenseBalanceAmount(AIOSCommons.formatAmount(Double
						.valueOf(expense.getExpenseBalanceAmount())));
				expense.setExpenseBalanceAmount(expense
						.getExpenseBalanceAmount()
						.replace('$', '-')
						.substring(0,
								expense.getExpenseBalanceAmount().length() - 1));
			}
			if (!expense.getLedgerSide()
					&& Double.valueOf(expense.getExpenseBalanceAmount()
							.replaceAll(",", "")) > 0) {
				expense.setExpenseBalanceAmount("-"
						+ expense.getExpenseBalanceAmount());
			}
		}
		for (AccountsTO revenue : account.getRevenueList()) {
			if (Double.valueOf(revenue.getRevenueBalanceAmount()) > 0) {
				revenue.setRevenueBalanceAmount(AIOSCommons.formatAmount(Double
						.valueOf(revenue.getRevenueBalanceAmount())));
			} else {
				revenue.setRevenueBalanceAmount(AIOSCommons.formatAmount(Double
						.valueOf(revenue.getRevenueBalanceAmount())));
				revenue.setRevenueBalanceAmount(revenue
						.getRevenueBalanceAmount()
						.replace('$', '-')
						.substring(0,
								revenue.getRevenueBalanceAmount().length() - 1));
			}
			if (revenue.getLedgerSide()
					&& Double.valueOf(revenue.getRevenueBalanceAmount()
							.replaceAll(",", "")) > 0) {
				revenue.setRevenueBalanceAmount("-"
						+ revenue.getRevenueBalanceAmount());
			}
		}
		double tempNetAmount;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				account.setNetLabel("Net Profit");
				account.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
			} else {
				account.setNetLabel("Net Loss");
				account.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
				account.setNetAmount(account.getNetAmount().substring(1,
						account.getNetAmount().length() - 1));
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				account.setNetLabel("Net Loss");
				account.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
			} else {
				account.setNetLabel("Net Profit");
				account.setNetAmount(AIOSCommons.formatAmount(Double
						.valueOf(tempNetAmount)));
				account.setNetAmount(account.getNetAmount().substring(1,
						account.getNetAmount().length() - 1));
			}
		}
		accountsToList.add(account);

		return accountsToList;
	}

	public String fiscalIncomeStatementReportPrintOut() {
		LOGGER.info("Inside Accounts method ledgerFiscalIncomeStatement");
		try {

			accountsList = this.fetchFiscalIncomeStatement();

			ServletActionContext.getRequest().setAttribute("ACCOUNTS",
					accountsList);

			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method ledgerFiscalIncomeStatement goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String fiscalIncomeStatementReportXLS() {
		try {
			accountsList = this.fetchFiscalIncomeStatement();

			String str = "";

			str += "\n Fiscal Income Statement";

			str += "\n\n Revenue";

			for (AccountsTO to : accountsList) {
				for (AccountsTO revenue : to.getRevenueList()) {
					str += revenue.getRevenueAccountCode() + ",,"
							+ revenue.getRevenueBalanceAmount();
				}
				str += "\n Total(AED),," + to.getRevenueTotal();
				str += "\n\n Expense";
				for (AccountsTO expense : to.getExpenseList()) {
					str += expense.getExpenseAccountCode() + ",,"
							+ expense.getExpenseBalanceAmount();
				}
				str += "\n Total(AED),," + to.getExpenseTotal();
				str += "\n\n " + to.getNetLabel() + ",," + to.getNetAmount();
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String showTrialBalance() {
		LOGGER.info("Inside Accounts method showTrialBalance()");
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();
			AccountsTO account = new AccountsTO();
			account.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			account.setAnchorExpression("HTML".equals(getFormat()) ? "download_trialbalance_report.action?format=PDF"
					: "#");
			accountsList = new ArrayList<AccountsTO>();
			ledgerList = accountsBL.getTrailBalanceCombination(implementation);
			if (null != ledgerList && ledgerList.size() > 0) {
				Collections.sort(ledgerList, new Comparator<Ledger>() {
					@Override
					public int compare(Ledger l1, Ledger l2) {
						return l1.getLedgerId().compareTo(l2.getLedgerId());
					}
				});
				List<AccountsTO> debitCreditEntry = accountsBL
						.listAllDebitCredit(ledgerList);
				account.setDebitCreditList(debitCreditEntry);
				double debitTotal = accountsBL.calcucateDebit(account
						.getDebitCreditList());
				double creditTotal = accountsBL.calcucateCredit(account
						.getDebitCreditList());
				account.setDebitBalance(AIOSCommons.formatAmount(debitTotal));
				account.setCreditBalance(AIOSCommons.formatAmount(creditTotal));
				for (AccountsTO debit : account.getDebitCreditList()) {
					if (null != debit.getDebitBalanceTotal())
						debit.setDebitBalanceTotal(AIOSCommons
								.formatAmount(Double.parseDouble(debit
										.getDebitBalanceTotal().replaceAll(",",
												""))));
				}
				for (AccountsTO credit : account.getDebitCreditList()) {
					if (null != credit.getCreditBalanceTotal())
						credit.setCreditBalanceTotal(AIOSCommons
								.formatAmount(Double.parseDouble(credit
										.getCreditBalanceTotal().replaceAll(
												",", ""))));
				}
			}
			accountsList.add(account);
			LOGGER.info("Inside Accounts method showTrialBalance Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside Accounts method showTrialBalance goes exeception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * Process Ledger Fiscal
	 * 
	 * @return
	 */
	public String processFiscalTrialBalance() {
		try {
			LOGGER.info("Inside Accounts method processFiscalTrialBalance");
			List<LedgerFiscal> ledgerFiscals = accountsBL
					.getLedgerFiscalAgainstPeriod(periodId);
			accountsList = new ArrayList<AccountsTO>();
			if (null != ledgerFiscals && ledgerFiscals.size() > 0) {
				Collections.sort(ledgerFiscals, new Comparator<LedgerFiscal>() {
					@Override
					public int compare(LedgerFiscal l1, LedgerFiscal l2) {
						return l1.getLedgerFiscalId().compareTo(
								l2.getLedgerFiscalId());
					}
				});
				AccountsTO account = new AccountsTO();
				List<AccountsTO> debitCreditEntry = accountsBL
						.listAllDebitCreditFiscal(ledgerFiscals);
				if (null != debitCreditEntry && debitCreditEntry.size() > 0) {
					account.setDebitBalance(AIOSCommons.formatAmount(accountsBL
							.calcucateDebit(debitCreditEntry)));
					account.setCreditBalance(AIOSCommons
							.formatAmount(accountsBL
									.calcucateCredit(debitCreditEntry)));
					Map<Long, AccountsTO> fiscalMap = new HashMap<Long, AccountsTO>();
					for (AccountsTO accountsTO : debitCreditEntry)
						fiscalMap
								.put(accountsTO.getCombinationId(), accountsTO);
					Period period = accountsBL.getAccountsEnterpriseService()
							.getCalendarService()
							.getCalendarPeriodById(periodId);
					List<Period> periods = accountsBL
							.getAccountsEnterpriseService()
							.getCalendarService()
							.getFiscalAbovePeriods(
									period.getCalendar().getCalendarId(),
									period.getStartTime(), period.getEndTime(),
									periodId);
					List<AccountsTO> openingDebitCredits = null;
					if (null != periods && periods.size() > 0) {
						// Get Ledger Fiscals from the period list
						Long periodIds[] = new Long[periods.size()];
						for (int i = 0; i < periods.size(); i++)
							periodIds[i] = periods.get(i).getPeriodId();
						ledgerFiscals = accountsBL.getAccountsJobBL()
								.getLedgerFiscalBL().getLedgerFiscalService()
								.getLedgerFiscals(periodIds);
						if (null != ledgerFiscals && ledgerFiscals.size() > 0) {
							Collections.sort(ledgerFiscals,
									new Comparator<LedgerFiscal>() {
										@Override
										public int compare(LedgerFiscal l1,
												LedgerFiscal l2) {
											return l1
													.getLedgerFiscalId()
													.compareTo(
															l2.getLedgerFiscalId());
										}
									});
							openingDebitCredits = accountsBL
									.listAllDebitCreditFiscal(ledgerFiscals);
						}
					}
					fiscalMap = setFiscalTrialBalanceOpeningBalance(fiscalMap,
							openingDebitCredits);
					account.setDebitCreditList(new ArrayList<AccountsTO>(
							fiscalMap.values()));
					accountsList.add(account);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"PRINT_DATE",
					DateFormat.convertDateToString(DateTime.now().toLocalDate()
							.toString()));
			ServletActionContext.getRequest().setAttribute(
					"TRIAL_BALANCE_REPORT", accountsList);
			LOGGER.info("Inside Accounts method processFiscalTrialBalance Success ");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method processFiscalTrialBalance() caught exception "
					+ ex);
			return ERROR;
		}
	}

	private Map<Long, AccountsTO> setFiscalTrialBalanceOpeningBalance(
			Map<Long, AccountsTO> fiscalMap, List<AccountsTO> openingBalances)
			throws Exception {
		AccountsTO account = null;
		if (null != openingBalances && openingBalances.size() > 0) {
			for (AccountsTO accountsTO : openingBalances) {
				if (fiscalMap.containsKey(accountsTO.getCombinationId())) {
					account = fiscalMap.get(accountsTO.getCombinationId());
					account = accountsBL.setClosingBalance(account, accountsTO);
					fiscalMap.put(accountsTO.getCombinationId(), account);
				}
			}
		}
		for (Entry<Long, AccountsTO> entry : fiscalMap.entrySet()) {
			account = entry.getValue();
			if (null == account.getViewClosingBalance()
					|| ("").equals(account.getViewClosingBalance().trim())) {
				if (account.getLedgerSide())
					account.setViewClosingBalance(AIOSCommons
							.formatAmount(Double.parseDouble(account
									.getDebitBalanceTotal())));
				else
					account.setViewClosingBalance(AIOSCommons
							.formatAmount(Double.parseDouble(account
									.getCreditBalanceTotal())));
				entry.setValue(account);
			}
		}
		return fiscalMap;
	}

	private AccountsTO getIncomeStament(Implementation implementation)
			throws Exception {
		List<Ledger> ledgerIncomeList = accountsBL
				.getIncomenStatementCombination(implementation);
		List<AccountsTO> expenseList = accountsBL
				.getExpenseStatement(ledgerIncomeList);
		List<AccountsTO> revenueList = accountsBL
				.getRevenueStatement(ledgerIncomeList);
		AccountsTO account = new AccountsTO();
		account.setExpenseList(expenseList);
		account.setRevenueList(revenueList);
		double expenseTotal = accountsBL.calcucateExpense(account
				.getExpenseList());
		double revenueTotal = accountsBL.calcucateRevenue(account
				.getRevenueList());
		double tempNetAmount;
		if (revenueTotal > expenseTotal) {
			if (revenueTotal > 0)
				tempNetAmount = revenueTotal - expenseTotal;
			else
				tempNetAmount = revenueTotal + expenseTotal;
			if (tempNetAmount > 0) {
				account.setLiabilityCode("Net Profit");
				account.setAccountCode("Net Profit");
				account.setLiabilityBalanceAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				account.setLedgerSide(true);
			} else {
				account.setLiabilityCode("Net Loss");
				account.setAccountCode("Net Loss");
				account.setLiabilityBalanceAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				account.setLiabilityBalanceAmount("-"
						.concat(account.getLiabilityBalanceAmount()
								.substring(
										1,
										account.getLiabilityBalanceAmount()
												.length() - 1)));

				account.setLedgerSide(false);
			}
		} else {
			if (expenseTotal > 0)
				tempNetAmount = expenseTotal - revenueTotal;
			else
				tempNetAmount = expenseTotal + revenueTotal;
			if (tempNetAmount > 0) {
				account.setLiabilityCode("Net Loss");
				account.setAccountCode("Net Loss");
				account.setLiabilityBalanceAmount("-".concat(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount))));
				account.setLedgerSide(false);
			} else {
				account.setLiabilityCode("Net Profit");
				account.setAccountCode("Net Profit");
				account.setLiabilityBalanceAmount(AIOSCommons
						.formatAmount(Double.valueOf(tempNetAmount)));
				account.setLiabilityBalanceAmount(account
						.getLiabilityBalanceAmount()
						.substring(
								1,
								account.getLiabilityBalanceAmount().length() - 1));
				account.setLedgerSide(true);
			}
		}
		return account;
	}

	public String getBankDepositDownload() {
		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			getImplementId();

			bankDepositList = new ArrayList<BankDepositVO>();
			BankDepositVO bankdepositVo = new BankDepositVO();
			List<BankDepositVO> tempVOs = new ArrayList<BankDepositVO>();
			bankdepositVo.setBankDepositDetailList(tempVOs);

			long bankDepositId = recordId;
			BankDeposit bankDeposit = accountsBL.getAccountsEnterpriseService()
					.getBankDepositService().getBankDepositInfo(bankDepositId);
			if (bankDeposit != null
					&& bankDeposit.getBankDepositDetails() != null) {
				bankdepositVo.setBankDepositNumber(bankDeposit
						.getBankDepositNumber());
				bankdepositVo.setRecordDate(DateFormat
						.convertDateToString(bankDeposit.getDate().toString()));
				bankdepositVo.setDescription(bankDeposit.getDescription());
				for (BankDepositDetail bankDepositDetail : bankDeposit
						.getBankDepositDetails()) {
					BankDepositVO bdVo = new BankDepositVO();

					bdVo.setRecordAmount(AIOSCommons
							.formatAmount(bankDepositDetail.getAmount()));
					bankdepositVo.getBankDepositDetailList().add(bdVo);
				}

			}
			bankdepositVo.setLinkLabel("HTML".equals(getFormat()) ? "Download"
					: "");
			bankdepositVo
					.setAnchorExpression("HTML".equals(getFormat()) ? "bank_deposit_download.action?recordId="
							+ bankDepositId + "&approvalFlag=N&format=PDF"
							: "#");

			if (bankDepositList != null && bankDepositList.size() == 0) {
				bankDepositList.add(bankdepositVo);
			} else {
				bankDepositList.set(0, bankdepositVo);
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String directPaymentReportXLS() {
		try {
			LOGGER.info("Inside Accounts method directPaymentReportXLS()");
			getImplementId();
			accountsList = new ArrayList<AccountsTO>();
			AccountsTO accounts = new AccountsTO();
			if (recordId > 0)
				accounts.setRecordId(recordId);
			List<DirectPayment> directPaymentList = this.getAccountsBL()
					.createDirectPaymentReport(implementation, accounts);
			StringBuilder str = new StringBuilder();
			str.append("DIRECT PAYMENTS\n\n");
			for (DirectPayment list : directPaymentList) {
				str.append("PAYMENT NO." + ",");
				str.append(list.getPaymentNumber() + ",");
				str.append("PAYMENT DATE" + ",");
				str.append(DateFormat.convertDateToString(list.getPaymentDate()
						.toString()) + "\n");
				str.append("CURRENCY" + ",");
				str.append(list.getCurrency().getCurrencyPool().getCode() + ",");
				str.append("PAYMENT MODE" + ",");
				str.append(ModeOfPayment.get(list.getPaymentMode()).name()
						.replaceAll("_", " ")
						+ "\n");
				str.append("BANK" + ",");
				str.append(null != list.getBankAccount() ? list
						.getBankAccount().getBank().getBankName() : "-N/A-");
				str.append(",");
				str.append("ACCOUNT NO" + ",");
				str.append(null != list.getBankAccount() ? list
						.getBankAccount().getAccountNumber() : "-N/A-");
				str.append("\n");
				str.append("CHEQUE NO" + ",");
				str.append(null != list.getChequeNumber() ? list
						.getChequeNumber() : "-N/A-");
				str.append(",");
				str.append("CHEQUE DATE" + ",");
				str.append(null != list.getChequeDate() ? DateFormat
						.convertDateToString(list.getChequeDate().toString())
						: "-N/A-");
				str.append("\n");
				str.append("PAYABLE NAME" + ",");
				if (null != list.getSupplier().getCmpDeptLocation()
						&& !("").equals(list.getSupplier().getCmpDeptLocation()))
					str.append(list.getSupplier().getCmpDeptLocation()
							.getCompany().getCompanyName()
							+ ",");
				else
					str.append(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName())
							+ ",");
				str.append("COMBINATION" + ",");
				if (null != list.getCombination()
						.getAccountByAnalysisAccountId()
						&& !("").equals(list.getCombination()
								.getAccountByAnalysisAccountId()))
					str.append(list
							.getCombination()
							.getAccountByNaturalAccountId()
							.getAccount()
							.concat(".")
							.concat(list.getCombination()
									.getAccountByAnalysisAccountId()
									.getAccount()));
				else
					str.append(list.getCombination()
							.getAccountByNaturalAccountId().getAccount());
				str.append("\n");
				str.append("DESCRIPTION" + ",");
				str.append(null != list.getDescription() ? list
						.getDescription().replaceAll("\\,", "\\;") : "");
				str.append("\n\n");
				str.append("INVOICE NO." + "," + "COMBINATION" + "," + "AMOUNT"
						+ "," + "DESCRIPTION" + "\n");
				for (DirectPaymentDetail detail : list
						.getDirectPaymentDetails()) {
					str.append(detail.getInvoiceNumber() + ",");
					if (null != detail.getCombination()
							.getAccountByAnalysisAccountId()
							&& !("").equals(detail.getCombination()
									.getAccountByAnalysisAccountId()))
						str.append(detail
								.getCombination()
								.getAccountByNaturalAccountId()
								.getAccount()
								.concat(".")
								.concat(detail.getCombination()
										.getAccountByAnalysisAccountId()
										.getAccount())
								+ ",");
					else
						str.append(detail.getCombination()
								.getAccountByNaturalAccountId().getAccount()
								+ ",");
					str.append(detail.getAmount() + ",");
					str.append(null != detail.getDescription() ? detail
							.getDescription().replaceAll("\\,", "\\;") : "");
					str.append("\n");
				}
				str.append("\n\n\n");
			}
			LOGGER.info("Inside Accounts method directPaymentReportXLS()");
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method directPaymentReportXLS() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	public String loadComulativeBalanceSheetCriteria() {

		String result;
		try {

			calenderList = new ArrayList<Calendar>();
			calenderList = accountsEnterpriseService.getCalendarService()
					.getAllCalendar(
							(Implementation) (ServletActionContext.getRequest()
									.getSession().getAttribute("THIS")));

			result = SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}
		return result;
	}

	public String loadPeriodicBalanceSheetCriteria() {

		String result;
		try {

			periods = accountsEnterpriseService.getCalendarService()
					.getAllPeriods(
							(Implementation) (ServletActionContext.getRequest()
									.getSession().getAttribute("THIS")));

			result = SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}
		return result;
	}

	public String generatePeriodicBalanceSheet() {
		String result;
		try {

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			AccountsTO account = new AccountsTO();
			List<AccountsTO> assetList = new ArrayList<AccountsTO>();
			List<AccountsTO> liabilityList = new ArrayList<AccountsTO>();

			/* List<String> years = new ArrayList<String>(); */
			List<String> yearsAssetSum = new ArrayList<String>();
			List<String> yearsLiabilitiesSum = new ArrayList<String>();

			if (selectedPeriod != null && !selectedPeriod.equals("null")
					&& !selectedPeriod.equals("undefined")) {

				List<LedgerFiscal> ledgerFiscals = accountsEnterpriseService
						.getCombinationService()
						.getLedgerFiscalByPeriodIdWithLedger(
								Long.parseLong(selectedPeriod));

				Collections.sort(ledgerFiscals, new Comparator<LedgerFiscal>() {
					@Override
					public int compare(LedgerFiscal l1, LedgerFiscal l2) {
						return l1
								.getCombination()
								.getCombinationId()
								.compareTo(
										l2.getCombination().getCombinationId());
					}
				});

				/*
				 * yearsAssetSum.add(AIOSCommons.formatAmount(accountsBL
				 * .calculateAssetTotal(accountsBL
				 * .getAssetFiscalDetails(ledgerFiscals))));
				 * 
				 * yearsLiabilitiesSum.add(AIOSCommons.formatAmount(accountsBL
				 * .calculateLiabilityTotal(accountsBL
				 * .getLiabilityFiscalDetails(ledgerFiscals))));
				 * 
				 * assetList.addAll(accountsBL
				 * .getAssetFiscalDetails(ledgerFiscals));
				 * liabilityList.addAll(accountsBL
				 * .getLiabilityFiscalDetails(ledgerFiscals));
				 */

				liabilityList
						.add(account = getIncomeStament((Implementation) (ServletActionContext
								.getRequest().getSession().getAttribute("THIS"))));

				account.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				account.setAnchorExpression("HTML".equals(getFormat()) ? "download_balancesheet.action?format=PDF"
						: "#");
				account.setAssetList(assetList);
				account.setLiabilityList(liabilityList);

				account.setYearsAssetTotal(yearsAssetSum);
				account.setYearsLiabilitiesTotal(yearsLiabilitiesSum);
				/* account.setSelectedYears(years); */

				double assetTotal = accountsBL.calculateAssetTotal(account
						.getAssetList());
				double liabilityTotal = accountsBL
						.calculateLiabilityTotal(account.getLiabilityList());
				account.setAssetTotal(AIOSCommons.formatAmount(Math
						.abs(assetTotal)));
				account.setLiabilityTotal(AIOSCommons.formatAmount(Math
						.abs(liabilityTotal)));
				accountsList = new ArrayList<AccountsTO>();

				for (AccountsTO asset : account.getAssetList()) {
					if (Double.valueOf(asset.getAssetBalanceAmount()
							.replaceAll(",", "")) > 0) {
						asset.setAssetBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(asset
										.getAssetBalanceAmount().replaceAll(
												",", ""))));
					} else {
						asset.setAssetBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(asset
										.getAssetBalanceAmount().replaceAll(
												",", ""))));
						asset.setAssetBalanceAmount(asset
								.getAssetBalanceAmount()
								.replace("$", "-")
								.substring(
										0,
										asset.getAssetBalanceAmount().length() - 1));
					}
				}

				for (AccountsTO liability : account.getLiabilityList()) {
					if (Double.valueOf(liability.getLiabilityBalanceAmount()
							.replaceAll(",", "")) > 0) {
						liability.setLiabilityBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(liability
										.getLiabilityBalanceAmount()
										.replaceAll(",", ""))));
					} else {
						liability.setLiabilityBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(liability
										.getLiabilityBalanceAmount()
										.replaceAll(",", ""))));
						liability.setLiabilityBalanceAmount(liability
								.getLiabilityBalanceAmount()
								.replace("$", "-")
								.substring(
										0,
										liability.getLiabilityBalanceAmount()
												.length() - 1));
					}
				}
				accountsList.add(account);

			}

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}
		return result;
	}

	public String generateComulativeBalanceSheet() {
		String result;
		try {

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			AccountsTO account = new AccountsTO();
			List<AccountsTO> assetList = new ArrayList<AccountsTO>();
			List<AccountsTO> liabilityList = new ArrayList<AccountsTO>();

			List<String> years = new ArrayList<String>();
			List<String> yearsAssetSum = new ArrayList<String>();
			List<String> yearsLiabilitiesSum = new ArrayList<String>();

			if (selectedCalendarIds != null
					&& !selectedCalendarIds.equals("null")
					&& !selectedCalendarIds.equals("undefined")) {

				String[] selectedIds = selectedCalendarIds.split(",");

				for (String id : selectedIds) {

					years.add(accountsEnterpriseService.getCalendarService()
							.getCalendarDetails(Long.parseLong(id)).getName());

					List<Period> periodList = accountsEnterpriseService
							.getCalendarService().getPeriodsByCalendarId(
									Long.parseLong(id));

					if (periodList.size() > 0) {
						for (Period period : periodList) {
							List<LedgerFiscal> ledgerFiscals = accountsEnterpriseService
									.getCombinationService()
									.getLedgerFiscalByPeriodIdWithLedger(
											period.getPeriodId());

							Collections.sort(ledgerFiscals,
									new Comparator<LedgerFiscal>() {
										@Override
										public int compare(LedgerFiscal l1,
												LedgerFiscal l2) {
											return l1
													.getCombination()
													.getCombinationId()
													.compareTo(
															l2.getCombination()
																	.getCombinationId());
										}
									});

							/*
							 * yearsAssetSum
							 * .add(AIOSCommons.formatAmount(accountsBL
							 * .calculateAssetTotal(accountsBL
							 * .getAssetFiscalDetails(ledgerFiscals))));
							 * 
							 * yearsLiabilitiesSum
							 * .add(AIOSCommons.formatAmount(
							 * accountsBL.calculateLiabilityTotal(accountsBL
							 * .getLiabilityFiscalDetails(ledgerFiscals))));
							 * 
							 * assetList.addAll(accountsBL
							 * .getAssetFiscalDetails(ledgerFiscals));
							 * liabilityList.addAll(accountsBL
							 * .getLiabilityFiscalDetails(ledgerFiscals));
							 */
						}
					}
				}

				liabilityList
						.add(account = getIncomeStament((Implementation) (ServletActionContext
								.getRequest().getSession().getAttribute("THIS"))));

				account.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				account.setAnchorExpression("HTML".equals(getFormat()) ? "download_balancesheet.action?format=PDF"
						: "#");
				account.setAssetList(assetList);
				account.setLiabilityList(liabilityList);

				account.setYearsAssetTotal(yearsAssetSum);
				account.setYearsLiabilitiesTotal(yearsLiabilitiesSum);
				account.setSelectedYears(years);

				double assetTotal = accountsBL.calculateAssetTotal(account
						.getAssetList());
				double liabilityTotal = accountsBL
						.calculateLiabilityTotal(account.getLiabilityList());
				account.setAssetTotal(AIOSCommons.formatAmount(Math
						.abs(assetTotal)));
				account.setLiabilityTotal(AIOSCommons.formatAmount(Math
						.abs(liabilityTotal)));
				accountsList = new ArrayList<AccountsTO>();

				for (AccountsTO asset : account.getAssetList()) {
					if (Double.valueOf(asset.getAssetBalanceAmount()
							.replaceAll(",", "")) > 0) {
						asset.setAssetBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(asset
										.getAssetBalanceAmount().replaceAll(
												",", ""))));
					} else {
						asset.setAssetBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(asset
										.getAssetBalanceAmount().replaceAll(
												",", ""))));
						asset.setAssetBalanceAmount(asset
								.getAssetBalanceAmount()
								.replace("$", "-")
								.substring(
										0,
										asset.getAssetBalanceAmount().length() - 1));
					}
				}

				for (AccountsTO liability : account.getLiabilityList()) {
					if (Double.valueOf(liability.getLiabilityBalanceAmount()
							.replaceAll(",", "")) > 0) {
						liability.setLiabilityBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(liability
										.getLiabilityBalanceAmount()
										.replaceAll(",", ""))));
					} else {
						liability.setLiabilityBalanceAmount(AIOSCommons
								.formatAmount(Double.valueOf(liability
										.getLiabilityBalanceAmount()
										.replaceAll(",", ""))));
						liability.setLiabilityBalanceAmount(liability
								.getLiabilityBalanceAmount()
								.replace("$", "-")
								.substring(
										0,
										liability.getLiabilityBalanceAmount()
												.length() - 1));
					}
				}
				accountsList.add(account);

			}

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}
		return result;
	}

	public String showChequeReconciliation() {
		aaData = new ArrayList<Object>();
		try {

			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String generateLedgerFiscalsList() {

		String result = ERROR;
		try {

			if (selectedCalendarIds != null
					&& !selectedCalendarIds.equals("null")
					&& !selectedCalendarIds.equals("undefined")) {

				String[] selectedIds = selectedCalendarIds.split(",");
				aaData = new ArrayList<Object>();
				for (String id : selectedIds) {

					List<Period> periodList = accountsEnterpriseService
							.getCalendarService().getPeriodsByCalendarId(
									Long.parseLong(id));

					if (periodList.size() > 0) {
						List<Object> tempList = new ArrayList<Object>();
						for (Period period : periodList) {
							List<Object> tempList2 = new ArrayList<Object>(
									accountsEnterpriseService
											.getCombinationService()
											.getLedgerFiscalByPeriodIdWithLedger(
													period.getPeriodId()));

							tempList.addAll(tempList2);
						}
						aaData.addAll(tempList);

					}

				}
			}

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String loadProductLedgerReportCriteria() {
		String result = ERROR;
		try {

			getImplementId();
			productsList = accountsEnterpriseService.getProductService()
					.getAllProduct(implementation);

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String generateProductLedgerReport() {
		String result = ERROR;
		try {

			AccountsTO account = new AccountsTO();
			accountsList = new ArrayList<AccountsTO>();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			if (productId != null && !productId.equals("0")) {
				/*
				 * Product product =
				 * accountsEnterpriseService.getProductService()
				 * .getProductById(Long.parseLong(productId));
				 */

				List<TransactionDetail> transactionDetails = accountsEnterpriseService
						.getTransactionService()
						.getTransactionDetailsOfProduct(
								Long.parseLong(productId));

				/*
				 * accountsEnterpriseService .getTransactionService()
				 * .getTransactionDetailsByCombinationId(
				 * product.getCombinationByProductCombinationId()
				 * .getCombinationId());
				 */

				List<AccountsTO> accountTOList = new ArrayList<AccountsTO>();
				for (TransactionDetail list : transactionDetails) {
					AccountsTO accountTO = accountsBL
							.createLedgerStatementView(list, transactionDetails);
					accountTOList.add(accountTO);

				}
				account.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				account.setAnchorExpression("HTML".equals(getFormat()) ? "download_balancesheet.action?format=PDF"
						: "#");

				accountsList.add(account);

				result = SUCCESS;

			} else if (selectedType != null && !selectedType.equals("null")) {

				List<Product> productslist = accountsEnterpriseService
						.getProductService().getProductsByItemType(
								implementation, selectedType.charAt(0));

				List<Ledger> ledgerslist = new ArrayList<Ledger>();

				for (Product product : productslist) {

					/*
					 * if (product.getCombinationByProductCombinationId() !=
					 * null) { Ledger ledger = accountsEnterpriseService
					 * .getCombinationService()
					 * .getLederByCombinationIdWithChilds(
					 * product.getCombinationByProductCombinationId()
					 * .getCombinationId()); ledgerslist.add(ledger);
					 * 
					 * }
					 */

				}

				if (null != ledgerslist && ledgerslist.size() > 0) {
					Collections.sort(ledgerslist, new Comparator<Ledger>() {
						@Override
						public int compare(Ledger l1, Ledger l2) {
							return l1.getLedgerId().compareTo(l2.getLedgerId());
						}
					});
					List<AccountsTO> debitCreditEntry = accountsBL
							.listAllDebitCredit(ledgerslist);
					account.setDebitCreditList(debitCreditEntry);
					double debitTotal = accountsBL.calcucateDebit(account
							.getDebitCreditList());
					double creditTotal = accountsBL.calcucateCredit(account
							.getDebitCreditList());
					account.setDebitBalance(AIOSCommons
							.formatAmount(debitTotal));
					account.setCreditBalance(AIOSCommons
							.formatAmount(creditTotal));
					for (AccountsTO debit : account.getDebitCreditList()) {
						if (null != debit.getDebitBalanceTotal()
								&& !debit.getDebitBalanceTotal().isEmpty())
							debit.setDebitBalanceTotal(AIOSCommons
									.formatAmount(Double.parseDouble(debit
											.getDebitBalanceTotal().replaceAll(
													",", ""))));
					}
					for (AccountsTO credit : account.getDebitCreditList()) {
						if (null != credit.getCreditBalanceTotal())
							credit.setCreditBalanceTotal(AIOSCommons
									.formatAmount(Double.parseDouble(credit
											.getCreditBalanceTotal()
											.replaceAll(",", ""))));
					}
				}

				account.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				account.setAnchorExpression("HTML".equals(getFormat()) ? "download_balancesheet.action?format=PDF"
						: "#");
				accountsList.add(account);
				result = SUCCESS;

			}

			productId = null;
			selectedType = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String loadInventoryStatementReportCriteria() {
		try {

			System.out.println("abc");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}

	public String getInventoryStatementTableData() {

		try {

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;

	}

	public List<Product> fetchInventoryStatementReportData() {
		List<Product> products = null;
		try {

			Long productId = null;
			Date fromDatee = null;
			Date toDatee = null;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			if (selectedProductId != null && !selectedProductId.equals("null")
					&& !selectedProductId.equals("0")) {
				productId = Long.parseLong(selectedProductId);
			}

			getImplementId();
			products = accountsEnterpriseService.getProductService()
					.getAllInventoryStatement(implementation, productId,
							fromDatee, toDatee);

			Set<Product> uniqueProducts = new HashSet<Product>(products);
			products = new ArrayList<Product>(uniqueProducts);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return products;
	}

	public String inventoryStatementReportPrintOut() {
		try {
			List<Product> products = fetchInventoryStatementReportData();

			ServletActionContext.getRequest()
					.setAttribute("PRODUCTS", products);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String inventoryStatementReportXLS() {
		try {

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String inventoryStatementReportPDF() {
		try {

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public List<Object> fetchSupplierInformationReportData() {

		try {

			getImplementId();

			Long supplierId = null;
			if (selectedSupplier != null && !selectedSupplier.equals("null")
					&& !selectedSupplier.equals("undefined")
					&& !selectedSupplier.equals("0")) {
				supplierId = Long.parseLong(selectedSupplier);
			}
			List<Supplier> supplierList = accountsEnterpriseService
					.getSupplierService().getSuppliersByDifferentValues(
							supplierId, implementation);
			entityObjects = new ArrayList<Object>();
			for (Supplier supplier : supplierList) {
				entityObjects.add(new SupplierTO(supplier));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return entityObjects;
	}

	@SuppressWarnings("unchecked")
	public String getSupplierInformationsReportTableData() {
		String result = ERROR;
		try {
			selectedSupplier = null;
			JSONObject jsonResponse = new JSONObject();

			List<SupplierTO> vos = (List<SupplierTO>) (List<?>) fetchSupplierInformationReportData();

			if (vos != null && vos.size() > 0) {
				jsonResponse.put("iTotalRecords", vos.size());
				jsonResponse.put("iTotalDisplayRecords", vos.size());
			}

			JSONArray data = new JSONArray();
			for (SupplierTO vo : vos) {

				JSONArray array = new JSONArray();
				array.add(vo.getSupplierId() + "");
				array.add(vo.getSupplierNumber() + "");
				array.add(vo.getSupplierName());
				array.add(vo.getBranchName());
				array.add(vo.getType());
				array.add(vo.getPaymentTermName());

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public String getSupplierInformationsReportPrintOut() {
		String result = ERROR;
		try {

			List<SupplierTO> vos = (List<SupplierTO>) (List<?>) fetchSupplierInformationReportData();

			ServletActionContext.getRequest().setAttribute("SUPPLIERS_LIST",
					vos);

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSupplierInformationsReportPDF() {
		try {
			entityObjects = fetchSupplierInformationReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getSupplierInformationsReportXLS() {
		try {

			List<SupplierTO> vos = (List<SupplierTO>) (List<?>) fetchSupplierInformationReportData();
			String str = "";

			str = "Suppliers Details\n\n";
			/*
			 * str += "Filter Condition\n"; str +=
			 * "Person Type,Disabled,Gender,Marital Status, Person Group, Nationailty,Dated From, Dates To\n"
			 * ; str += personTypes + "," + disabled + "," + gender + "," +
			 * maritalStatus + "," + personGroup + "," + nationality + "," +
			 * effectiveDatesFrom + "," + effectiveDatesTo + "\n\n";
			 */
			if (vos != null && vos.size() > 0) {
				str += "Supplier Name,Supplier Number,Branch,Type,Payment Term";
				str += "\n";
				for (SupplierTO to : vos) {
					str += to.getSupplierName() + "," + to.getSupplierNumber()
							+ "," + to.getBranchName() + "," + to.getType()
							+ "," + to.getPaymentTermName() + ",";

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String loadSupplierStatementReportCriteria() {
		return SUCCESS;
	}

	public String getSupplierStatementTableData() {

		try {

			JSONObject jsonResponse = new JSONObject();

			List<Supplier> suppliers = fetchSupplierStatementReportData();

			if (suppliers != null && suppliers.size() > 0) {
				jsonResponse.put("iTotalRecords", suppliers.size());
				jsonResponse.put("iTotalDisplayRecords", suppliers.size());
			}

			JSONArray data = new JSONArray();
			for (Supplier s : suppliers) {

				JSONArray array = new JSONArray();
				array.add(s.getSupplierId());

				Combination combination = null;
				if (s.getCombination() != null) {
					combination = s.getCombination();
				}

				TransactionDetail transactionDetail = null;
				if (combination.getTransactionDetails() != null
						&& combination.getTransactionDetails().size() > 0) {
					transactionDetail = combination.getTransactionDetails()
							.iterator().next();
					array.add(transactionDetail.getTransaction()
							.getTransactionId());
					array.add(transactionDetail.getTransaction()
							.getTransactionTime().toString());
					array.add(transactionDetail.getTransaction().getCurrency()
							.getCurrencyPool().getCode());
					array.add(transactionDetail.getDescription());
					array.add(transactionDetail.getAmount());
				} else {
					array.add("");
					array.add("");
					array.add("");
					array.add("");
					array.add("");
				}

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;

	}

	public List<Supplier> fetchSupplierStatementReportData() {
		List<Supplier> suppliers = null;
		try {

			Long supplierId = null;
			Date fromDatee = null;
			Date toDatee = null;

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			if (selectedSupplierId != null
					&& !selectedSupplierId.equals("null")
					&& !selectedSupplierId.equals("0")) {
				supplierId = Long.parseLong(selectedSupplierId);
			}

			getImplementId();
			suppliers = accountsEnterpriseService.getSupplierService()
					.getAllSupplierStatement(implementation, supplierId,
							fromDatee, toDatee);

			Set<Supplier> uniqueSuppliers = new HashSet<Supplier>(suppliers);
			suppliers = new ArrayList<Supplier>(uniqueSuppliers);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return suppliers;
	}

	public String supplierStatementReportPrintOut() {
		try {
			List<Supplier> suppliers = fetchSupplierStatementReportData();

			ServletActionContext.getRequest().setAttribute("SUPPLIERS",
					suppliers);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String supplierStatementReportXLS() {
		try {

			List<Supplier> suppliers = fetchSupplierStatementReportData();

			String str = "";
			Double debitTotal = 0.00;
			Double creditTotal = 0.00;

			str = "Supplier Statement Report\n\n";

			str += "From, To\n";
			str += "" + fromDate + "," + toDate + "\n";

			if (suppliers != null && suppliers.size() > 0) {
				str += "Account Number,Credit, Debit";
				str += "\n";

				for (Supplier s : suppliers) {
					String accountNo = "";
					if (s.getCombination().getAccountByCompanyAccountId() != null)
						accountNo += s.getCombination()
								.getAccountByCompanyAccountId().getAccount();

					str += accountNo + ",";

					int i = 0;

					Iterator<TransactionDetail> transactionDetailIterator = s
							.getCombination().getTransactionDetails()
							.iterator();
					while (transactionDetailIterator.hasNext()) {
						TransactionDetail transactionDetail = transactionDetailIterator
								.next();

						if (i > 0) {
							str += ",";
						}
						if (!transactionDetail.getIsDebit()) {
							str += transactionDetail.getAmount() + ",";
							creditTotal += transactionDetail.getAmount()
									.longValue();
						} else {
							str += "," + transactionDetail.getAmount();
							debitTotal += transactionDetail.getAmount();
						}
						str += "\n";

						i++;
					}

				}
				str += "\n";
				str += "Total," + creditTotal + "," + debitTotal;

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String supplierStatementReportPDF() {
		try {

			List<Supplier> suppliers = fetchSupplierStatementReportData();
			List<AccountsTO> accountsTOs = new ArrayList<AccountsTO>();
			Double creditTotal = 0.0;
			Double debitTotal = 0.0;

			for (Supplier s : suppliers) {

				String accountNo = "";
				if (s.getCombination().getAccountByCompanyAccountId() != null)
					accountNo += s.getCombination()
							.getAccountByCompanyAccountId().getAccount();

				int i = 0;
				Iterator<TransactionDetail> transactionDetailIterator = s
						.getCombination().getTransactionDetails().iterator();
				while (transactionDetailIterator.hasNext()) {
					AccountsTO accountsTO = new AccountsTO();
					TransactionDetail transactionDetail = transactionDetailIterator
							.next();

					if (i > 0) {
						accountsTO.setAccountCode("");
					} else {
						accountsTO.setAccountCode(accountNo);
					}
					if (!transactionDetail.getIsDebit()) {
						accountsTO.setCreditAmount(transactionDetail
								.getAmount().toString());
						accountsTO.setCreditAmountDouble(transactionDetail
								.getAmount());
						accountsTO.setDebitAmount("");
						creditTotal += transactionDetail.getAmount();

					} else {
						accountsTO.setCreditAmount("");
						accountsTO.setDebitAmount(transactionDetail.getAmount()
								.toString());
						accountsTO.setDebitAmountDouble(transactionDetail
								.getAmount());
						debitTotal += transactionDetail.getAmount();
					}
					i++;
					accountsTOs.add(accountsTO);
				}
			}

			inventoryStatementDS = accountsTOs;

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public List<Object> fetchSupplierPaymentsInformationReportData() {

		try {

			getImplementId();

			Long supplierId = null;

			List<Supplier> supplierList = new ArrayList<Supplier>();
			if (selectedSupplier != null && !selectedSupplier.equals("null")
					&& !selectedSupplier.equals("undefined")
					&& !selectedSupplier.equals("0")) {
				supplierId = Long.parseLong(selectedSupplier);

				supplierList = accountsEnterpriseService.getSupplierService()
						.getSupplierPaymentsDetailsBySupplierId(implementation,
								supplierId);
			} else {
				supplierList = accountsEnterpriseService.getSupplierService()
						.getSupplierPaymentsDetails(implementation);
			}

			entityObjects = new ArrayList<Object>();

			List<Object> objectWithOutPT = new ArrayList<Object>();

			for (Supplier supplier : supplierList) {

				if (supplier.getCreditTerm() != null) {
					entityObjects.add(new SupplierTO(supplier));
				} else {
					objectWithOutPT.add(new SupplierTO(supplier));
				}
			}

			entityObjects.addAll(objectWithOutPT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return entityObjects;
	}

	public String getSupplierPaymentsInformationsReportTableData() {
		String result = ERROR;
		try {
			selectedSupplier = null;
			JSONObject jsonResponse = new JSONObject();

			getImplementId();
			List<Object[]> vos = accountsEnterpriseService.getSupplierService()
					.getSupplierPaymentsTotals(implementation);

			if (vos != null && vos.size() > 0) {
				jsonResponse.put("iTotalRecords", vos.size());
				jsonResponse.put("iTotalDisplayRecords", vos.size());
			}

			JSONArray data = new JSONArray();

			JSONArray array = new JSONArray();
			JSONArray arrayWithoutPT = new JSONArray();

			for (Object[] vo : vos) {

				JSONArray subArray = new JSONArray();

				subArray.add(vo[0]);
				subArray.add(vo[1]);
				subArray.add(vo[2]);
				subArray.add(vo[3]);
				subArray.add(vo[4]);

				Double sum = 0.0;
				if (vo[5] != null) {
					sum = Double.parseDouble(vo[5].toString());
				}

				if (vo[6] != null) {
					sum = sum + Double.parseDouble(vo[6].toString());
				}

				subArray.add(sum.intValue());

				if (vo[4] != null) {
					array.add(subArray);
				} else {
					arrayWithoutPT.add(subArray);
				}

			}

			data.addAll(array);
			data.addAll(arrayWithoutPT);

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public String getAllSuppliersInformation() {
		String result = ERROR;
		try {

			getImplementId();

			if (selectedSupplier == null || selectedSupplier.equals("null")
					|| selectedSupplier.equals("undefined")
					|| selectedSupplier.equals("0")) {

				List<Object[]> vos = accountsEnterpriseService
						.getSupplierService().getSupplierPaymentsTotals(
								implementation);

				List<Object[]> withPT = new ArrayList<Object[]>();
				List<Object[]> withOutPT = new ArrayList<Object[]>();

				for (Object[] objects : vos) {
					if (objects[4] != null) {
						withPT.add(objects);
					} else {
						withOutPT.add(objects);
					}
				}
				withPT.addAll(withOutPT);

				ServletActionContext.getRequest().setAttribute(
						"SUPPLIERS_DATA", withPT);
			} else {
				List<SupplierTO> supplierTos = (List<SupplierTO>) (List<?>) fetchSupplierPaymentsInformationReportData();

				ServletActionContext.getRequest().setAttribute("SUPPLIER",
						supplierTos.get(0));
			}

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSuppliersInformationPDF() {
		String result = ERROR;
		try {

			getImplementId();

			entityObjects = new ArrayList<Object>();

			if (selectedSupplier == null || selectedSupplier.equals("null")
					|| selectedSupplier.equals("undefined")
					|| selectedSupplier.equals("0") || selectedSupplier == "0") {

				List<Object[]> vos = accountsEnterpriseService
						.getSupplierService().getSupplierPaymentsTotals(
								implementation);

				List<SupplierTO> withPT = new ArrayList<SupplierTO>();
				List<SupplierTO> withOutPT = new ArrayList<SupplierTO>();

				for (Object[] objects : vos) {

					SupplierTO to = new SupplierTO();
					to.setSupplierNumber(objects[1].toString());
					to.setSupplierName(objects[2].toString());
					to.setBranchName(objects[3].toString());
					if (objects[4] != null) {
						to.setPaymentTermName(objects[4].toString());
					} else {
						to.setPaymentTermName("");
					}

					Double sum = 0.0;
					if (objects[5] != null) {
						sum = Double.parseDouble(objects[5].toString());
					}

					if (objects[6] != null) {
						sum = sum + Double.parseDouble(objects[6].toString());
					}

					to.setTotal(sum.toString());

					if (objects[4] != null) {

						withPT.add(to);
					} else {

						withOutPT.add(to);
					}
				}
				withPT.addAll(withOutPT);

				entityObjects.addAll(withPT);

			} else {
				entityObjects = fetchSupplierPaymentsInformationReportData();

			}

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSupplierAgingReportXLS() {
		try {

			List<Object[]> vos = accountsEnterpriseService.getSupplierService()
					.getSupplierPaymentsTotals(implementation);

			List<SupplierTO> withPT = new ArrayList<SupplierTO>();
			List<SupplierTO> withOutPT = new ArrayList<SupplierTO>();

			for (Object[] objects : vos) {

				SupplierTO to = new SupplierTO();
				to.setSupplierNumber(objects[1].toString());
				to.setSupplierName(objects[2].toString());
				to.setBranchName(objects[3].toString());
				if (objects[4] != null) {
					to.setPaymentTermName(objects[4].toString());
				} else {
					to.setPaymentTermName("");
				}

				Double sum = 0.0;
				if (objects[5] != null) {
					sum = Double.parseDouble(objects[5].toString());
				}

				if (objects[6] != null) {
					sum = sum + Double.parseDouble(objects[6].toString());
				}

				to.setTotal(sum.toString());

				if (objects[4] != null) {

					withPT.add(to);
				} else {

					withOutPT.add(to);
				}
			}
			withPT.addAll(withOutPT);

			String str = "";

			str = "Suppliers Aging Report\n\n";

			str += "Supplier Name,Supplier Number,Branch,Payment Term,Payment Total";
			str += "\n";
			for (SupplierTO to : withPT) {

				str += to.getSupplierName() + "," + to.getSupplierNumber()
						+ "," + to.getBranchName() + ","
						+ to.getPaymentTermName() + "," + to.getTotal();

				str += "\n";

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String showSupplierAgeingCriteria() {
		try {
			getImplementId();
			List<SupplierVO> supplierVOs = accountsBL.getSupplierBL()
					.getAllSuppliersVo(implementation);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_DETAILS",
					supplierVOs);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String supplierAgeingAnalysisXlsReport() {
		try {
			LocalDate ageingDate = DateTime.now().toLocalDate();
			if (ServletActionContext.getRequest().getParameter("ageingDate") != null)
				ageingDate = new DateTime(
						DateFormat.convertStringToDate((ServletActionContext
								.getRequest().getParameter("ageingDate")
								.toString()))).toLocalDate();
			InvoiceVO ageingResult = this.supplierAgeingAnalysisReport();
			StringBuilder str = new StringBuilder();
			str.append(",,,, ").append(implementation.getCompanyName())
					.append(" \n");
			str.append(",,,, SUPPLIER AGEING ANALYSIS \n");
			String ageingDate1 = DateFormat.convertDateToString(ageingDate
					.toString());
			boolean dueDateCheck = true;
			if (ServletActionContext.getRequest().getParameter("dueDateCheck")
					.equalsIgnoreCase("false"))
				dueDateCheck = false;
			str.append("STATEMENT DATE,").append(ageingDate1);
			str.append("\n");
			str.append("STATEMENT BY,").append(
					dueDateCheck ? "DUEDATE" : "INVOICE");
			if (null != ageingResult && null != ageingResult.getInvoiceVOs()
					&& ageingResult.getInvoiceVOs().size() > 0) {
				for (InvoiceVO mainData : ageingResult.getInvoiceVOs()) {
					str.append("\n\n");
					str.append("SUPPLIER NAME, PAYMENT TERM, INVOICE DATE, DUE DATE,");
					str.append("INVOICE REF, INVOICE NUMBER, OUTSTANDGING INVOICE, CURRENT, 1-30 AGED, 31-60 AGED, 61-90 AGED, AGED >90 \n");
					for (InvoiceVO invoiceVO : mainData.getInvoiceVOs()) {
						str.append(invoiceVO.getSupplierName()).append(",");
						str.append(
								null != invoiceVO.getPaymentTermStr() ? invoiceVO
										.getPaymentTermStr() : "").append(",");
						str.append(invoiceVO.getInvoiceDate()).append(",");
						str.append(invoiceVO.getDueDate()).append(",");
						str.append(
								null != invoiceVO.getInvoiceReference() ? invoiceVO
										.getInvoiceReference().replaceAll(",",
												";") : "").append(",");
						str.append(invoiceVO.getInvoiceNumber()).append(",");
						str.append(
								invoiceVO.getBalanceAmount()
										.replaceAll(",", "")).append(",");
						if (invoiceVO.isGroup0())
							str.append(
									invoiceVO.getInvoiceAmountStr().replaceAll(
											",", "")).append(",,,,");
						else if (invoiceVO.isGroup1())
							str.append(",")
									.append(invoiceVO.getInvoiceAmountStr()
											.replaceAll(",", "")).append(",,,");
						else if (invoiceVO.isGroup2())
							str.append(",,")
									.append(invoiceVO.getInvoiceAmountStr()
											.replaceAll(",", "")).append(",,");
						else if (invoiceVO.isGroup3())
							str.append(",,,")
									.append(invoiceVO.getInvoiceAmountStr()
											.replaceAll(",", "")).append(",");
						else if (invoiceVO.isGroup4())
							str.append(",,,,").append(
									invoiceVO.getInvoiceAmountStr().replaceAll(
											",", ""));
						str.append("\n");
					}
					str.append(",,,,, TOTAL,");
					str.append(
							AIOSCommons.formatAmountToDouble(mainData
									.getTotalInvoiceStr())).append(",");
					if (null != mainData.getGroupAmount0())
						str.append(
								AIOSCommons.formatAmountToDouble(mainData
										.getGroupAmount0())).append(",");
					else
						str.append(",");
					if (null != mainData.getGroupAmount1())
						str.append(
								AIOSCommons.formatAmountToDouble(mainData
										.getGroupAmount1())).append(",");
					else
						str.append(",");
					if (null != mainData.getGroupAmount2())
						str.append(
								AIOSCommons.formatAmountToDouble(mainData
										.getGroupAmount2())).append(",");
					else
						str.append(",");
					if (null != mainData.getGroupAmount3())
						str.append(
								AIOSCommons.formatAmountToDouble(mainData
										.getGroupAmount3())).append(",");
					else
						str.append(",");
					if (null != mainData.getGroupAmount4())
						str.append(AIOSCommons.formatAmountToDouble(mainData
								.getGroupAmount4()));
				}
				str.append("\n\n");
				str.append("TOTAL SUMMARY");
				str.append("\n");
				str.append("OUTSTANDING").append(",");
				str.append(null != ageingResult.getTotalInvoiceStr() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getTotalInvoiceStr())
						: "");
				str.append("\n");
				str.append("CURRENT").append(",");
				str.append(null != ageingResult.getGroupAmount0() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getGroupAmount0())
						: "");
				str.append("\n");
				str.append("1-30 AGED").append(",");
				str.append(null != ageingResult.getGroupAmount1() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getGroupAmount1())
						: "");
				str.append("\n");
				str.append("31-60 AGED").append(",");
				str.append(null != ageingResult.getGroupAmount2() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getGroupAmount2())
						: "");
				str.append("\n");
				str.append("61-90 AGED").append(",");
				str.append(null != ageingResult.getGroupAmount3() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getGroupAmount3())
						: "");
				str.append("\n");
				str.append("AGED >90").append(",");
				str.append(null != ageingResult.getGroupAmount4() ? AIOSCommons
						.formatAmountToDouble(ageingResult.getGroupAmount4())
						: "");
			}
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String supplierAgeingAnalysisPrintReport() {
		try {
			InvoiceVO ageingResult = this.supplierAgeingAnalysisReport();
			ServletActionContext.getRequest().setAttribute("AGEING_ANALYSIS",
					ageingResult);
			LocalDate ageingDate = DateTime.now().toLocalDate();
			boolean dueDateCheck = true;
			if (ServletActionContext.getRequest().getParameter("ageingDate") != null)
				ageingDate = new DateTime(
						DateFormat.convertStringToDate((ServletActionContext
								.getRequest().getParameter("ageingDate"))))
						.toLocalDate();
			if (ServletActionContext.getRequest().getParameter("dueDateCheck")
					.equalsIgnoreCase("false"))
				dueDateCheck = false;
			ServletActionContext.getRequest().setAttribute("PRINT_DATE",
					DateFormat.convertDateToString(ageingDate.toString()));
			ServletActionContext.getRequest().setAttribute("DUEDATE_INVOICE",
					(dueDateCheck ? "DUEDATE" : "INVOICE"));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private InvoiceVO supplierAgeingAnalysisReport() throws Exception {
		getImplementId();
		Set<Long> suppliers = null;
		if (ServletActionContext.getRequest().getParameter("suppliers") != null) {
			String supplierstr = ServletActionContext.getRequest()
					.getParameter("suppliers");
			if (null != supplierstr && !supplierstr.equals("null")
					&& !supplierstr.equals("")) {
				String supplierArray[] = splitValues(
						supplierstr.substring(1, supplierstr.length() - 1), ",");
				suppliers = new HashSet<Long>();
				for (String string : supplierArray)
					suppliers.add(Long.valueOf(string));
			}
		}
		boolean dueDateCheck = true;
		LocalDate ageingDate = DateTime.now().toLocalDate();
		if (ServletActionContext.getRequest().getParameter("ageingDate") != null)
			ageingDate = new DateTime(
					DateFormat.convertStringToDate((ServletActionContext
							.getRequest().getParameter("ageingDate"))))
					.toLocalDate();
		if (ServletActionContext.getRequest().getParameter("dueDateCheck")
				.equalsIgnoreCase("false"))
			dueDateCheck = false;
		List<Invoice> invoices = accountsBL.getInvoiceBL().getInvoiceService()
				.getSupplierNonPaidInvoices(suppliers);
		InvoiceVO ageingResult = null;
		if (null != invoices && invoices.size() > 0) {
			List<InvoiceVO> ageingResults = new ArrayList<InvoiceVO>();
			Map<Long, List<InvoiceVO>> supplierInvoiceMap = new HashMap<Long, List<InvoiceVO>>();
			List<InvoiceVO> invoiceVOTemp = null;
			for (Invoice invoice : invoices) {
				invoiceVOTemp = new ArrayList<InvoiceVO>();
				if (supplierInvoiceMap.containsKey(invoice.getSupplier()
						.getSupplierId()))
					invoiceVOTemp.addAll(supplierInvoiceMap.get(invoice
							.getSupplier().getSupplierId()));
				invoiceVOTemp.add(addSupplierVO(invoice, ageingDate,
						dueDateCheck));
				supplierInvoiceMap.put(invoice.getSupplier().getSupplierId(),
						invoiceVOTemp);
			}
			double totalInvoice;
			InvoiceVO invoiceVO = null;
			double invoiceSummary = 0;
			double group0Summary = 0;
			double group1Summary = 0;
			double group2Summary = 0;
			double group3Summary = 0;
			double group4Summary = 0;
			for (Entry<Long, List<InvoiceVO>> entry : supplierInvoiceMap
					.entrySet()) {
				invoiceVOTemp = new ArrayList<InvoiceVO>();
				invoiceVOTemp.addAll(entry.getValue());
				Collections.sort(invoiceVOTemp, new Comparator<InvoiceVO>() {
					public int compare(InvoiceVO o1, InvoiceVO o2) {
						return o1.getInvoiceDatedt().compareTo(
								o2.getInvoiceDatedt());
					}
				});
				totalInvoice = 0;
				invoiceVO = new InvoiceVO();
				for (InvoiceVO inv : invoiceVOTemp) {
					totalInvoice += inv.getInvoiceAmount();
					inv.setBalanceAmount(AIOSCommons.formatAmount(totalInvoice));
					if (inv.isGroup0()) {
						invoiceVO.setGroupAmount0(null != invoiceVO
								.getGroupAmount0() ? AIOSCommons
								.formatAmount((AIOSCommons
										.formatAmountToDouble(invoiceVO
												.getGroupAmount0()) + inv
										.getInvoiceAmount())) : AIOSCommons
								.formatAmount(inv.getInvoiceAmount()));
						group0Summary += inv.getInvoiceAmount();
					} else if (inv.isGroup1()) {
						invoiceVO.setGroupAmount1(null != invoiceVO
								.getGroupAmount1() ? AIOSCommons
								.formatAmount((AIOSCommons
										.formatAmountToDouble(invoiceVO
												.getGroupAmount1()) + inv
										.getInvoiceAmount())) : AIOSCommons
								.formatAmount(inv.getInvoiceAmount()));
						group1Summary += inv.getInvoiceAmount();
					} else if (inv.isGroup2()) {
						invoiceVO.setGroupAmount2(null != invoiceVO
								.getGroupAmount2() ? AIOSCommons
								.formatAmount((AIOSCommons
										.formatAmountToDouble(invoiceVO
												.getGroupAmount2()) + inv
										.getInvoiceAmount())) : AIOSCommons
								.formatAmount(inv.getInvoiceAmount()));
						group2Summary += inv.getInvoiceAmount();
					} else if (inv.isGroup3()) {
						invoiceVO.setGroupAmount3(null != invoiceVO
								.getGroupAmount3() ? AIOSCommons
								.formatAmount((AIOSCommons
										.formatAmountToDouble(invoiceVO
												.getGroupAmount3()) + inv
										.getInvoiceAmount())) : AIOSCommons
								.formatAmount(inv.getInvoiceAmount()));
						group3Summary += inv.getInvoiceAmount();
					} else if (inv.isGroup4()) {
						invoiceVO.setGroupAmount4(null != invoiceVO
								.getGroupAmount4() ? AIOSCommons
								.formatAmount((AIOSCommons
										.formatAmountToDouble(invoiceVO
												.getGroupAmount4()) + inv
										.getInvoiceAmount())) : AIOSCommons
								.formatAmount(inv.getInvoiceAmount()));
						group4Summary += inv.getInvoiceAmount();
					}
				}
				invoiceSummary += totalInvoice;
				invoiceVO.setTotalInvoiceStr(AIOSCommons
						.formatAmount(totalInvoice));
				invoiceVO.setInvoiceVOs(invoiceVOTemp);
				ageingResults.add(invoiceVO);
			}
			if (null != ageingResults && ageingResults.size() > 0) {
				ageingResult = new InvoiceVO();
				ageingResult.setTotalInvoiceStr(AIOSCommons
						.formatAmount(invoiceSummary));
				ageingResult.setGroupAmount0(AIOSCommons
						.formatAmount(group0Summary));
				ageingResult.setGroupAmount1(AIOSCommons
						.formatAmount(group1Summary));
				ageingResult.setGroupAmount2(AIOSCommons
						.formatAmount(group2Summary));
				ageingResult.setGroupAmount3(AIOSCommons
						.formatAmount(group3Summary));
				ageingResult.setGroupAmount4(AIOSCommons
						.formatAmount(group4Summary));
				ageingResult.setInvoiceVOs(ageingResults);
			}
		}
		return ageingResult;
	}

	private InvoiceVO addSupplierVO(Invoice invoice, LocalDate ageingDate,
			boolean dueDateCheck) throws Exception {
		InvoiceVO invoiceVO = new InvoiceVO();
		Set<String> invRef = new HashSet<String>();
		BeanUtils.copyProperties(invoiceVO, invoice);
		invoiceVO.setSupplierId(invoice.getSupplier().getSupplierId());
		if ((null != invoice.getSupplier().getCmpDeptLocation())
				|| (null != invoice.getSupplier().getCompany()))
			invoiceVO.setSupplierName(null != invoice.getSupplier()
					.getCmpDeptLocation() ? invoice.getSupplier()
					.getCmpDeptLocation().getCompany().getCompanyName()
					: invoice.getSupplier().getCompany().getCompanyName());
		else
			invoiceVO.setSupplierName(invoice.getSupplier().getPerson()
					.getFirstName().concat(" ")
					.concat(invoice.getSupplier().getPerson().getLastName()));
		invoiceVO.setInvoiceDate(DateFormat.convertDateToString(invoice
				.getDate().toString()));
		invoiceVO.setInvoiceAmountStr(AIOSCommons.formatAmount(invoice
				.getInvoiceAmount()));
		List<InvoiceDetail> invoiceDts = new ArrayList<InvoiceDetail>();
		if (null != invoice.getInvoiceDetails()
				&& invoice.getInvoiceDetails().size() > 0)
			invoiceDts.addAll(invoice.getInvoiceDetails());
		else {
			Invoice parentInvoice = getParentRecursivly(invoice.getInvoiceId());
			invoiceDts.addAll(parentInvoice.getInvoiceDetails());
		}
		for (InvoiceDetail invDt : invoiceDts)
			invRef.add(invDt.getReceiveDetail().getReceive().getReferenceNo());
		invoiceVO
				.setInvoiceReference(null != invRef && invRef.size() > 0 ? StringUtils
						.join(invRef, ',') : "");
		if (null != invoiceDts.get(0).getReceiveDetail().getPurchaseDetail()
				.getPurchase().getCreditTerm()) {
			invoiceVO.setPaymentTermStr(invoiceDts.get(0).getReceiveDetail()
					.getPurchaseDetail().getPurchase().getCreditTerm()
					.getName());
			invoiceVO.setPaymentTermDays(null != invoiceDts.get(0)
					.getReceiveDetail().getPurchaseDetail().getPurchase()
					.getCreditTerm().getDiscountDay() ? (int) Math
					.round(invoiceDts.get(0).getReceiveDetail()
							.getPurchaseDetail().getPurchase().getCreditTerm()
							.getDiscountDay()) : 0);
		} else if (null != invoice.getSupplier().getCreditTerm()) {
			invoiceVO.setPaymentTermStr(invoice.getSupplier().getCreditTerm()
					.getName());
			invoiceVO.setPaymentTermDays((int) Math.round(invoice.getSupplier()
					.getCreditTerm().getDiscountDay()));
		}
		if (null != invoiceVO.getPaymentTermDays()
				&& (int) invoiceVO.getPaymentTermDays() > 0)
			invoiceVO.setDueDate(DateFormat.convertDateToString(new LocalDate(
					DateFormat.convertStringToDate(invoiceVO.getInvoiceDate()))
					.plusDays(invoiceVO.getPaymentTermDays()).toString()));
		else
			invoiceVO.setDueDate(invoiceVO.getInvoiceDate());
		int ageingDays = 0;
		if (dueDateCheck)
			ageingDays = Days.daysBetween(
					ageingDate.toDateTime(LocalTime.now()),
					new DateTime(DateFormat.convertStringToDate(invoiceVO
							.getDueDate()))).getDays();
		else
			ageingDays = Days.daysBetween(
					ageingDate.toDateTime(LocalTime.now()),
					new DateTime(DateFormat.convertStringToDate(invoiceVO
							.getInvoiceDate()))).getDays();
		if (ageingDays >= 0)
			invoiceVO.setGroup0(true);
		else {
			ageingDays = Math.abs(ageingDays);
			if (ageingDays > 90)
				invoiceVO.setGroup4(true);
			else if (ageingDays > 60)
				invoiceVO.setGroup3(true);
			else if (ageingDays > 30)
				invoiceVO.setGroup2(true);
			else
				invoiceVO.setGroup1(true);
		}
		invoiceVO.setInvoiceDatedt(DateFormat.convertStringToDate(invoiceVO
				.getDueDate()));
		return invoiceVO;
	}

	private Invoice getParentRecursivly(long invoiceId) throws Exception {
		Invoice invoice = accountsBL.getInvoiceBL().getInvoiceService()
				.getParentInvoiceById(invoiceId);
		if (null != invoice.getInvoice())
			return getParentRecursivly(invoice.getInvoice().getInvoiceId());
		return invoice;
	}

	@SuppressWarnings("unchecked")
	public String getSingleSupplierAgingReportXLS() {
		try {

			List<SupplierTO> list = (List<SupplierTO>) (List<?>) fetchSupplierPaymentsInformationReportData();

			String str = "";

			str = "Suppliers Aging Report\n\n";
			for (SupplierTO to : list) {
				str += "Supplier Name," + to.getSupplierName()
						+ "\n Supplier Number," + to.getSupplierNumber()
						+ "\nBranch," + to.getBranchName() + "\nPayment Term,"
						+ to.getPaymentTermName() + "\nPayment Total,"
						+ to.getTotal();
				str += "\n";

				str += "Payments:\n";

				if (to.getPaymentsList() != null
						&& to.getPaymentsList().size() > 0) {

					for (SupplierTO payment : to.getPaymentsList()) {

						str += "\nInvoice #," + payment.getInvoiceNumber()
								+ "\nPayment Date," + payment.getPaymentDate()
								+ "\nCheque #," + payment.getChequeNumber();

						str += "\nProduct Name,Quantity,Unit Rate";

						if (payment.getPaymentsDetailsList() != null) {
							for (SupplierTO payments : payment
									.getPaymentsDetailsList()) {
								str += payments.getProductName() + ","
										+ payments.getQuantity() + ","
										+ payments.getUnitRate();
							}
						}
					}
				}

				str += "\n\n\n";

				str += "Direct Payments:\n";

				if (to.getDirectPaymentsList() != null
						&& to.getDirectPaymentsList().size() > 0) {

					for (SupplierTO payment : to.getDirectPaymentsList()) {

						str += "\nPayment #,"
								+ payment.getDirectPaymentNumber()
								+ "\nPayment data,"
								+ payment.getDirectPaymentDate()
								+ "\nPayment Cheque Date,"
								+ payment.getDirectPaymentChequeDate();

						str += "\nInvoice Number,Amount,Description\n";

						if (payment.getDirectPaymentsDetailsList() != null) {
							for (SupplierTO payments : payment
									.getDirectPaymentsDetailsList()) {
								str += payments
										.getDirectPaymentDetailInvoiceNumber()
										+ ","
										+ payments.getDirectPaymentAmount()
										+ ","
										+ payments
												.getDirectPaymentDetailDescription()
										+ "\n";
							}
						}
					}
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String getSuppliersHistoryJsonList() {
		try {
			getImplementId();

			JSONObject jsonResponse = new JSONObject();

			List<SupplierTO> vos = fetchSupplierHistoryData();

			if (vos != null && vos.size() > 0) {
				jsonResponse.put("iTotalRecords", vos.size());
				jsonResponse.put("iTotalDisplayRecords", vos.size());
			}

			JSONArray data = new JSONArray();

			for (SupplierTO vo : vos) {

				JSONArray array = new JSONArray();

				array.add(vo.getSupplierId());
				array.add(vo.getSupplierName());
				array.add(vo.getSupplierNumber());

				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public List<SupplierTO> fetchSupplierHistoryData() throws Exception {
		List<Supplier> suppliers = accountsEnterpriseService
				.getSupplierService().getSupplierHistory(implementation);

		List<SupplierTO> tos = new ArrayList<SupplierTO>();

		for (Supplier supplier : suppliers) {

			if (supplier.getReceives().size() > 0
					|| supplier.getGoodsReturns().size() > 0) {
				SupplierTO to = new SupplierTO();
				to.setSupplierNumber(supplier.getSupplierNumber());

				if (supplier.getPerson() != null) {
					to.setSupplierName(supplier.getPerson().getFirstName()
							+ " " + supplier.getPerson().getLastName());

				}

				if (supplier.getCmpDeptLocation().getCompany() != null) {
					to.setSupplierName(supplier.getCmpDeptLocation()
							.getCompany().getCompanyName());

				}

				List<Receive> receives = new ArrayList<Receive>(
						supplier.getReceives());
				to.setReceivesList(receives);

				List<GoodsReturn> returns = new ArrayList<GoodsReturn>(
						supplier.getGoodsReturns());
				to.setReturnsList(returns);

				tos.add(to);
			}
		}
		return tos;
	}

	public List<Object> fetchSupplierHistoryDataForPDF() throws Exception {
		List<Supplier> suppliers = accountsEnterpriseService
				.getSupplierService().getSupplierHistory(implementation);

		List<Object> tos = new ArrayList<Object>();

		for (Supplier supplier : suppliers) {

			if (supplier.getReceives().size() > 0
					|| supplier.getGoodsReturns().size() > 0) {
				SupplierTO to = new SupplierTO();
				to.setSupplierNumber(supplier.getSupplierNumber());

				if (supplier.getPerson() != null) {
					to.setSupplierName(supplier.getPerson().getFirstName()
							+ " " + supplier.getPerson().getLastName());

				}

				if (supplier.getCmpDeptLocation().getCompany() != null) {
					to.setSupplierName(supplier.getCmpDeptLocation()
							.getCompany().getCompanyName());

				}

				List<Receive> receives = new ArrayList<Receive>(
						supplier.getReceives());

				List<SupplierTO> receivesTO = new ArrayList<SupplierTO>();

				for (Receive receive : receives) {

					for (ReceiveDetail rd : receive.getReceiveDetails()) {

						SupplierTO sto = new SupplierTO();
						sto.setrNumber(receive.getReceiveNumber().toString());
						sto.setrDate(receive.getReceiveDate().toString());

						sto.setDescription(receive.getDescription());
						sto.setProductName(rd.getProduct().getProductName());
						sto.setUnitRate(rd.getUnitRate().toString());
						sto.setQuantity(rd.getReceiveQty().toString());

						receivesTO.add(sto);
					}

				}

				to.setReceiveTOs(receivesTO);

				List<GoodsReturn> returns = new ArrayList<GoodsReturn>(
						supplier.getGoodsReturns());

				List<SupplierTO> returnsTO = new ArrayList<SupplierTO>();

				for (GoodsReturn gr : returns) {

					for (GoodsReturnDetail rd : gr.getGoodsReturnDetails()) {

						SupplierTO sto = new SupplierTO();
						sto.setrNumber(gr.getReturnNumber());
						sto.setrDate(gr.getDate().toString());
						sto.setProductName(rd.getProduct().getProductName());
						sto.setQuantity(rd.getReturnQty().toString());

						returnsTO.add(sto);
					}

				}

				to.setReturnTs(returnsTO);

				tos.add(to);
			}
		}
		return tos;
	}

	public String getSuppliersHistoryPrintOut() {
		try {
			List<SupplierTO> suppliers = fetchSupplierHistoryData();

			ServletActionContext.getRequest().setAttribute("SUPPLIERS",
					suppliers);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getSuppliersHistoryPDF() {
		String result = ERROR;
		try {

			entityObjects = fetchSupplierHistoryDataForPDF();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSupplierHistoryXSL() {
		try {

			List<SupplierTO> suppliers = fetchSupplierHistoryData();

			String str = "";

			str = "Suppliers Aging Report\n\n";
			for (SupplierTO to : suppliers) {
				str += "Supplier Name," + to.getSupplierName()
						+ "\n Supplier Number," + to.getSupplierNumber();

				str += "\n";

				str += "Receives:\n";

				str += "\nReceive Number,Date ,Description,Product,Quantity, Unit Rate\n";
				for (Receive receive : to.getReceivesList()) {

					for (ReceiveDetail rd : receive.getReceiveDetails()) {

						str += receive.getReceiveNumber() + ","
								+ receive.getReceiveDate() + ","
								+ receive.getDescription() + ","
								+ rd.getProduct().getProductName() + ","
								+ rd.getReceiveQty() + "," + rd.getUnitRate()
								+ "\n";
					}

				}

				str += "\nReturns:\n";

				str += "\nReturn Number,Date ,Product,Quantity\n";
				for (GoodsReturn gr : to.getGoodsReturns()) {

					for (GoodsReturnDetail grd : gr.getGoodsReturnDetails()) {

						str += gr.getReturnNumber() + "," + gr.getDate() + ","
								+ grd.getProduct().getProductName() + ","
								+ grd.getReturnQty() + "\n";
					}

				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String showDirectPaymentCriteriaPage() throws Exception {
		ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
				directPaymentBL.getModeOfPayment());

		ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
				directPaymentBL.getReceiptSource());

		return SUCCESS;
	}

	public String getDirectPaymentJsonList() {
		try {
			getImplementId();

			JSONObject jsonResponse = null;
			List<DirectPaymentVO> directPaymentList = fetchDirectPaymentData();

			if (null != directPaymentList && !directPaymentList.equals("")) {
				jsonResponse = new JSONObject();
				JSONArray data = new JSONArray();
				JSONArray array = null;
				double totalAmount = 0;
				for (DirectPayment list : directPaymentList) {
					array = new JSONArray();
					String voidCheck = "";
					if (null != list.getVoidPayments()
							&& list.getVoidPayments().size() > 0)
						voidCheck = "<span title='Void Payment' class='void_payment_view'></span>";
					array.add(list.getCombination().getCombinationId());
					array.add(list.getPaymentNumber().concat(voidCheck));
					array.add(DateFormat.convertDateToString(list
							.getPaymentDate().toString()));
					array.add(ModeOfPayment.get(list.getPaymentMode()).name()
							.replaceAll("_", " "));
					totalAmount = 0;
					List<DirectPaymentDetail> detailList = new ArrayList<DirectPaymentDetail>(
							list.getDirectPaymentDetails());
					for (DirectPaymentDetail list2 : detailList) {
						totalAmount += list2.getAmount();
					}
					array.add(AIOSCommons.formatAmount(totalAmount));
					if (null != list.getSupplier()) {
						if (null != list.getSupplier().getPerson())
							array.add(list
									.getSupplier()
									.getPerson()
									.getFirstName()
									.concat(" ")
									.concat(list.getSupplier().getPerson()
											.getLastName()));
						else
							array.add(null != list.getSupplier().getCompany() ? list
									.getSupplier().getCompany()
									.getCompanyName()
									: list.getSupplier().getCmpDeptLocation()
											.getCompany().getCompanyName());
					} else if (null != list.getCustomer()) {
						if (null != list.getCustomer().getPersonByPersonId())
							array.add(list
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat(" ")
									.concat(list.getCustomer()
											.getPersonByPersonId()
											.getLastName()));
						else
							array.add(null != list.getCustomer().getCompany() ? list
									.getCustomer().getCompany()
									.getCompanyName()
									: list.getCustomer().getCmpDeptLocation()
											.getCompany().getCompanyName());
					} else if (null != list.getPersonByPersonId()) {
						array.add(list
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(list.getPersonByPersonId()
										.getLastName()));
					} else {
						array.add(list.getOthers());
					}
					if (null != list.getBankAccount()) {
						array.add(list.getBankAccount().getBank().getBankName());
						array.add(list.getBankAccount().getAccountNumber());
						if (null != list.getChequeNumber())
							array.add(list.getChequeNumber());
						else
							array.add("");
						if (null != list.getChequeDate())
							array.add(DateFormat.convertDateToString(list
									.getChequeDate().toString()));
						else
							array.add("");
					} else {
						array.add("");
						array.add("");
						array.add("");
						array.add("");
					}
					if (null != list.getVoidPayments()
							&& list.getVoidPayments().size() > 0)
						array.add("false");
					else
						array.add("true");
					data.add(array);
				}
				jsonResponse.put("aaData", data);

				jsonResponse.put("aaData", data);
			}

			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public List<DirectPaymentVO> fetchDirectPaymentData() throws Exception {

		Date fromDatee = null;
		Date toDatee = null;
		Byte paymentModeId = null;
		Long payeeTypeId = null;
		Boolean isBankAccount = false;
		Boolean isCustomer = false;
		Boolean isSupplier = false;
		Boolean isOthers = false;
		Boolean isPDC = false;
		getImplementId();
		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		if (ServletActionContext.getRequest().getParameter("paymentModeId") != null) {
			String paymentMode = ServletActionContext.getRequest()
					.getParameter("paymentModeId").split("@@")[0];
			paymentModeId = Byte.parseByte(paymentMode);
		}

		if (ServletActionContext.getRequest().getParameter("payeeTypeId") != null) {
			payeeTypeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("payeeTypeId"));
		}

		if (ServletActionContext.getRequest().getParameter("isBankAccount") != null) {
			isBankAccount = Boolean.parseBoolean(ServletActionContext
					.getRequest().getParameter("isBankAccount"));
		}

		if (ServletActionContext.getRequest().getParameter("isCustomer") != null) {
			isCustomer = Boolean.parseBoolean(ServletActionContext.getRequest()
					.getParameter("isCustomer"));
		}

		if (ServletActionContext.getRequest().getParameter("isSupplier") != null) {
			isSupplier = Boolean.parseBoolean(ServletActionContext.getRequest()
					.getParameter("isSupplier"));
		}

		if (ServletActionContext.getRequest().getParameter("isOthers") != null) {
			isOthers = Boolean.parseBoolean(ServletActionContext.getRequest()
					.getParameter("isBaisOthersnkAccount"));
		}

		if (ServletActionContext.getRequest().getParameter("isPDC") != null) {
			isPDC = Boolean.parseBoolean(ServletActionContext.getRequest()
					.getParameter("isPDC"));
		}

		List<DirectPayment> directPayments = accountsEnterpriseService
				.getDirectPaymentService().getFilteredDirectPayments(
						implementation, fromDatee, toDatee, paymentModeId,
						payeeTypeId, isBankAccount, isCustomer, isSupplier,
						isOthers, isPDC);

		List<DirectPaymentVO> vos = new ArrayList<DirectPaymentVO>();
		double totalAmount = 0;
		if (null != directPayments && directPayments.size() > 0) {
			for (DirectPayment directPayment : directPayments) {
				DirectPaymentVO directPaymentVO = new DirectPaymentVO(
						directPayment);
				String voidCheck = "";

				directPaymentVO.setDirectPaymentId(directPayment
						.getDirectPaymentId());
				directPaymentVO.setDirectPaymentNumber(directPayment
						.getPaymentNumber().concat(voidCheck));
				directPaymentVO.setDirectPaymentDate(DateFormat
						.convertDateToString(directPayment.getPaymentDate()
								.toString()));
				directPaymentVO.setDirectPaymentMode(ModeOfPayment
						.get(directPayment.getPaymentMode()).name()
						.replaceAll("_", " "));
				directPaymentVO.setDescription(directPayment.getDescription());
				totalAmount = 0;
				List<DirectPaymentDetail> detailList = new ArrayList<DirectPaymentDetail>(
						directPayment.getDirectPaymentDetails());
				for (DirectPaymentDetail list2 : detailList) {
					totalAmount += list2.getAmount();
				}
				directPaymentVO
						.setAmount(AIOSCommons.formatAmount(totalAmount));
				if (null != directPayment.getSupplier()) {
					if (null != directPayment.getSupplier().getPerson())
						directPaymentVO.setPayable(directPayment
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(directPayment.getSupplier().getPerson()
										.getLastName()));
					else
						directPaymentVO.setPayable(null != directPayment
								.getSupplier().getCompany() ? directPayment
								.getSupplier().getCompany().getCompanyName()
								: directPayment.getSupplier()
										.getCmpDeptLocation().getCompany()
										.getCompanyName());
				} else if (null != directPayment.getCustomer()) {
					if (null != directPayment.getCustomer()
							.getPersonByPersonId())
						directPaymentVO.setPayable(directPayment
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(directPayment.getCustomer()
										.getPersonByPersonId().getLastName()));
					else
						directPaymentVO.setPayable(null != directPayment
								.getCustomer().getCompany() ? directPayment
								.getCustomer().getCompany().getCompanyName()
								: directPayment.getCustomer()
										.getCmpDeptLocation().getCompany()
										.getCompanyName());
				} else if (null != directPayment.getPersonByPersonId()) {
					directPaymentVO.setPayable(directPayment
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(directPayment.getPersonByPersonId()
									.getLastName()));
				} else {
					directPaymentVO.setPayable(directPayment.getOthers());
				}
				if (null != directPayment.getBankAccount()) {
					directPaymentVO.setBankName(directPayment.getBankAccount()
							.getBank().getBankName());
					directPaymentVO.setAccoutNumber(directPayment
							.getBankAccount().getAccountNumber());
					if (null != directPayment.getChequeNumber())
						directPaymentVO.setPaymentChequeNumber(directPayment
								.getChequeNumber().toString());
					else
						directPaymentVO.setPaymentChequeNumber("");
					if (null != directPayment.getChequeDate())
						directPaymentVO.setPaymentChequeDate(DateFormat
								.convertDateToString(directPayment
										.getChequeDate().toString()));
					else
						directPaymentVO.setPaymentChequeDate("");
				} else {
					directPaymentVO.setBankName("");
					directPaymentVO.setAccoutNumber("");
					directPaymentVO.setPaymentChequeNumber("");
					directPaymentVO.setPaymentChequeDate("");
				}

				vos.add(directPaymentVO);
			}
			Collections.sort(vos, new Comparator<DirectPaymentVO>() {
				public int compare(DirectPaymentVO o1, DirectPaymentVO o2) {
					return o1.getPaymentNumber().compareTo(
							o2.getPaymentNumber());
				}
			});
		}
		return vos;
	}

	public String getDirectPaymentPrintOut() {
		try {
			getImplementId();
			List<DirectPaymentVO> directPayments = fetchDirectPaymentData();

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayments);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getDirectPaymentXLS() {
		try {
			List<DirectPaymentVO> directPayments = fetchDirectPaymentData();
			getImplementId();
			String str = "";
			str = ",,," + implementation.getCompanyName() + "\n";
			str += ",,,DIRECT PAYMENT REPORT\n";
			str += "Payment Number, Date, Payment Mode, Payable, Description, Bank Name, Account NO, Cheque NO, Cheque Date, Amount\n";
			for (DirectPaymentVO vo : directPayments) {
				str += vo.getPaymentNumber() + ",";
				str += vo.getDirectPaymentDate() + ",";
				str += vo.getDirectPaymentMode() + ",";
				str += vo.getPayable() + ",";
				str += null != vo.getDescription() ? vo.getDescription()
						.replaceAll(",", ";") + "," : ";";
				str += null != vo.getBankName() ? vo.getBankName() + "," : ";";
				str += null != vo.getAccoutNumber() ? vo.getAccoutNumber()
						+ "," : ";";
				str += null != vo.getPaymentChequeNumber() ? vo
						.getPaymentChequeNumber() + "," : ";";
				str += null != vo.getPaymentChequeDate() ? vo
						.getPaymentChequeDate() + "," : ";";
				str += AIOSCommons.formatAmountToDouble(vo.getAmount()) + ",";
				str += "\n";
			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getDirectPaymentTransactionPrintOut() {
		try {
			// List<DirectPaymentVO> directPayments = fetchDirectPaymentData();
			getImplementId();
			Long combinationId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("combinationId"));

			List<TransactionDetail> transactionDetails = accountsEnterpriseService
					.getTransactionService()
					.getTransactionDetailsByCombinationId(combinationId);

			List<AccountsTO> accountTOList = new ArrayList<AccountsTO>();
			for (TransactionDetail list : transactionDetails) {
				AccountsTO accountTO = accountsBL.createLedgerStatementView(
						list, transactionDetails);
				accountTOList.add(accountTO);

			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					accountTOList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getDirectPaymentPrintPDF() {
		String result = ERROR;
		try {
			getImplementId();
			directPaymentDS = fetchDirectPaymentData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getDirectPaymentTransactionPrintPDF() {
		String result = ERROR;
		try {

			Long combinationId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("combinationId"));

			List<TransactionDetail> transactionDetails = accountsEnterpriseService
					.getTransactionService()
					.getTransactionDetailsByCombinationId(combinationId);

			List<AccountsTO> accountTOList = new ArrayList<AccountsTO>();
			for (TransactionDetail list : transactionDetails) {
				AccountsTO accountTO = accountsBL.createLedgerStatementView(
						list, transactionDetails);
				accountTOList.add(accountTO);

			}

			accountsList = accountTOList;

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getGRNCriteriaData() {
		try {
			suppliersList = QuotationAction
					.convertListTO(accountsEnterpriseService
							.getSupplierService().getAllSuppliers(
									getImplementation()));
			storeList = accountsEnterpriseService.getStoreService()
					.getAllStores(getImplementation());
			productsList = accountsEnterpriseService.getProductService()
					.getAllProduct(getImplementation());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getGRNJsonList() {

		try {
			getImplementId();
			List<ReceiveVO> receiveList = this.fetchGRNReportData();
			if (receiveList != null && receiveList.size() > 0) {
				LOGGER.info("Module: Accounts : Method: showReceiveList, "
						+ " product List size() " + receiveList.size());
			}

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();

			double totalReceive = 0;
			for (Receive list : receiveList) {
				JSONArray array = new JSONArray();
				array.add(list.getReceiveId());
				array.add(list.getReceiveNumber());
				array.add(DateFormat.convertDateToString(list.getReceiveDate()
						.toString()));
				array.add(list.getReferenceNo());
				List<ReceiveDetail> receiveDetail = new ArrayList<ReceiveDetail>(
						list.getReceiveDetails());
				totalReceive = 0;
				for (ReceiveDetail list2 : receiveDetail) {
					totalReceive += list2.getUnitRate() * list2.getReceiveQty();
				}
				array.add(AIOSCommons.formatAmount(totalReceive));
				array.add(list.getSupplier().getSupplierNumber());
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson()))
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					array.add(null != list.getSupplier().getCmpDeptLocation() ? list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showReceiveList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public List<ReceiveVO> fetchGRNReportData() {
		List<Receive> receiveNotes = null;
		List<ReceiveVO> receiveNotesData = new ArrayList<ReceiveVO>();
		getImplementId();
		try {

			Long receiveId = 0L;
			Long storeId = 0L;
			Long productId = 0L;
			Long supplierId = 0L;
			Date fromDatee = null;
			Date toDatee = null;

			if (ServletActionContext.getRequest().getParameter("receiveId") != null) {
				receiveId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("receiveId"));
			}

			if (ServletActionContext.getRequest().getParameter("storeId") != null) {
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("storeId"));
			}

			if (ServletActionContext.getRequest().getParameter("productId") != null) {
				productId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("productId"));
			}

			if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
				supplierId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("supplierId"));
			}

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			receiveNotes = accountsEnterpriseService.getReceiveService()
					.getFilteredReceiveNotes(implementation, receiveId,
							toDatee, fromDatee, supplierId, storeId, productId);

			for (Receive list : receiveNotes) {
				ReceiveVO receiveVO = new ReceiveVO(list);
				receiveVO.setStrReceiveDate(DateFormat.convertDateToString(list
						.getReceiveDate().toString()));
				List<ReceiveDetail> receiveDetail = new ArrayList<ReceiveDetail>(
						list.getReceiveDetails());
				Double totalReceive = 0D;
				List<ReceiveDetailVO> receiveDetailVOs = new ArrayList<ReceiveDetailVO>();
				for (ReceiveDetail list2 : receiveDetail) {
					ReceiveDetailVO receiveDetailVO = new ReceiveDetailVO(list2);
					receiveDetailVO
							.setProductCode(list2.getProduct().getCode());
					receiveDetailVO.setProductName(list2.getProduct()
							.getProductName());
					receiveDetailVO.setItemType(ItemType.get(
							list2.getProduct().getItemType()).name());
					if (list2.getShelf() != null) {
						receiveDetailVO.setStoreName(list2.getShelf()
								.getAisle().getStore().getStoreName());
					} else {
						receiveDetailVO.setStoreName("");
					}
					receiveDetailVO.setTotalRate(list2.getReceiveQty()
							* list2.getUnitRate());
					totalReceive += list2.getUnitRate() * list2.getReceiveQty();
					receiveDetailVOs.add(receiveDetailVO);
				}
				receiveVO.setTotalReceiveAmount(totalReceive);
				receiveVO.setReceiveDetailsVO(receiveDetailVOs);
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson()))
					receiveVO.setSupplierName(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					receiveVO.setSupplierName(null != list.getSupplier()
							.getCmpDeptLocation() ? list.getSupplier()
							.getCmpDeptLocation().getCompany().getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());

				receiveNotesData.add(receiveVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return receiveNotesData;
	}

	public String getGRNPrintOut() {
		try {
			List<ReceiveVO> receiveList = this.fetchGRNReportData();

			ServletActionContext.getRequest().setAttribute("GRN", receiveList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getGRNReportPDF() {
		try {
			receiveDS = this.fetchGRNReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getGRNReportXLS() {
		try {
			List<ReceiveVO> receiveVOs = this.fetchGRNReportData();
			String str = "";

			for (ReceiveVO receiveVO : receiveVOs) {
				str += "\n\n SUPPLIER," + receiveVO.getSupplierName() + " \n\n";
				str += "GRN Number, Reference, Date, Total Cost\n";

				str += receiveVO.getReceiveNumber() + ","
						+ receiveVO.getReferenceNo() + ","
						+ receiveVO.getReceiveDate() + ","
						+ receiveVO.getTotalReceiveAmount();
				str += "\n\n";
				str += "PRODUCT CODE, PRODUCT, ITEM TYPE, STORE, UNIT RATE, QUANTITY, TOTAL\n";
				for (ReceiveDetailVO receiveDetailVO : receiveVO
						.getReceiveDetailsVO()) {
					str += receiveDetailVO.getProductCode() + ","
							+ receiveDetailVO.getProductName() + ","
							+ receiveDetailVO.getItemType() + ","
							+ receiveDetailVO.getStoreName() + ","
							+ receiveDetailVO.getUnitRate() + ","
							+ receiveDetailVO.getReceiveQty() + ","
							+ receiveDetailVO.getTotalRate();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getGoodsReturnCriteriaData() {
		try {
			getImplementId();
			suppliersList = QuotationAction
					.convertListTO(accountsEnterpriseService
							.getSupplierService().getAllSuppliers(
									implementation));
			purchases = accountsEnterpriseService.getPurchaseService()
					.getPurchaseOrders(implementation);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public List<GoodsReturnVO> fetchGoodsReturnReportData() {
		List<GoodsReturn> goodsReturns = null;
		List<GoodsReturnVO> goodsReturnVOs = new ArrayList<GoodsReturnVO>();
		getImplementId();
		try {

			Long returnId = 0L;
			Long purchaseId = 0L;
			Long supplierId = 0L;
			Date fromDatee = null;
			Date toDatee = null;

			if (ServletActionContext.getRequest().getParameter("returnId") != null) {
				returnId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("returnId"));
			}

			if (ServletActionContext.getRequest().getParameter("purchaseId") != null) {
				purchaseId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("purchaseId"));
			}

			if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
				supplierId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("supplierId"));
			}

			if (fromDate != null && !fromDate.equals("null")
					&& !fromDate.equals("undefined")) {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			if (toDate != null && !toDate.equals("null")
					&& !toDate.equals("undefined")) {
				toDatee = DateFormat.convertStringToDate(toDate);
			}

			goodsReturns = accountsEnterpriseService.getGoodsReturnService()
					.getFilteredGoodsReturn(implementation, returnId,
							supplierId, purchaseId, toDatee, fromDatee);

			for (GoodsReturn goodsReturn : goodsReturns) {
				GoodsReturnVO goodsReturnVO = new GoodsReturnVO(goodsReturn);

				// goodsReturnVO.setProductCode(goodsReturn.getPurchase().getPurchaseDetails())

				if (null != goodsReturn.getSupplier().getPerson()
						&& !("").equals(goodsReturn.getSupplier().getPerson())) {
					goodsReturnVO.setSupplierName(goodsReturn
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(goodsReturn.getSupplier().getPerson()
									.getLastName()));
				} else {
					goodsReturnVO.setSupplierName(null != goodsReturn
							.getSupplier().getCmpDeptLocation() ? goodsReturn
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName() : goodsReturn.getSupplier()
							.getCompany().getCompanyName());
				}
				goodsReturnVO.setSupplierNumber(goodsReturn.getSupplier()
						.getSupplierNumber());

				goodsReturnVO.setGoodsReturnDate(DateFormat
						.convertDateToString(goodsReturn.getDate().toString()));

				List<GoodsReturnDetailVO> goodsReturnDetailVOs = new ArrayList<GoodsReturnDetailVO>();
				for (GoodsReturnDetail grd : goodsReturn
						.getGoodsReturnDetails()) {
					for (PurchaseDetail pd : grd.getPurchase()
							.getPurchaseDetails()) {
						GoodsReturnDetailVO goodsReturnDetailVO = new GoodsReturnDetailVO(
								grd);
						goodsReturnDetailVO.setProductCode(grd.getProduct()
								.getCode());
						goodsReturnDetailVO.setProductName(grd.getProduct()
								.getProductName());
						goodsReturnDetailVO.setStrItemType(ItemType.get(
								grd.getProduct().getItemType()).name());
						goodsReturnDetailVO.setPurchaseReference(grd
								.getPurchase().getPurchaseNumber().toString());
						goodsReturnDetailVO.setPurchaseQty(pd.getQuantity()
								.toString());
						goodsReturnDetailVO.setReturnQty(grd.getReturnQty());
						goodsReturnDetailVOs.add(goodsReturnDetailVO);
					}
				}
				goodsReturnVO.setGoodsReturnDetailVOs(goodsReturnDetailVOs);

				goodsReturnVOs.add(goodsReturnVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return goodsReturnVOs;
	}

	public String getGoodsReturnJsonList() {

		try {
			getImplementId();
			List<GoodsReturnVO> goodsReturnVOs = this
					.fetchGoodsReturnReportData();

			// double totalReceive = 0;
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;

			jsonResponse.put("iTotalRecords", goodsReturnVOs.size());
			jsonResponse.put("iTotalDisplayRecords", goodsReturnVOs.size());

			for (GoodsReturnVO list : goodsReturnVOs) {
				array = new JSONArray();
				array.add(list.getGoodsReturnId());
				array.add(list.getReturnNumber());
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(list.getSupplier().getSupplierNumber());
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson())) {
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				} else {
					array.add(null != list.getSupplier().getCmpDeptLocation() ? list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				}
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showReceiveList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showReceiveList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getGoodsReturnPrintOut() {
		try {
			List<GoodsReturnVO> goodsReturnList = this
					.fetchGoodsReturnReportData();

			ServletActionContext.getRequest().setAttribute("GOODS_RETURN",
					goodsReturnList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getGoodsReturnXLS() {
		try {
			List<GoodsReturnVO> returnVOs = this.fetchGoodsReturnReportData();
			String str = "";

			for (GoodsReturnVO returnVO : returnVOs) {
				str += "\n\n SUPPLIER," + returnVO.getSupplierName() + "["
						+ returnVO.getSupplierNumber() + "] \n\n";
				str += "Reference Number, Date\n";

				str += returnVO.getReturnNumber() + ","
						+ returnVO.getGoodsReturnDate();
				str += "\n\n";
				str += "PRODUCT CODE, PRODUCT, ITEM TYPE, Purchase Reference, Purchase Quantity, Return Quantity\n";
				for (GoodsReturnDetailVO goodsReturnDetailVO : returnVO
						.getGoodsReturnDetailVOs()) {
					str += goodsReturnDetailVO.getProductCode() + ","
							+ goodsReturnDetailVO.getProductName() + ","
							+ goodsReturnDetailVO.getStrItemType() + ","
							+ goodsReturnDetailVO.getPurchaseReference() + ","
							+ goodsReturnDetailVO.getPurchaseQty() + ","
							+ goodsReturnDetailVO.getReturnQty();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getGoodsReturnPDF() {
		String result = ERROR;
		try {

			goodsReturnDS = this.fetchGoodsReturnReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getInvoiceCriteriaData() {
		try {
			getImplementId();
			suppliersList = QuotationAction
					.convertListTO(accountsEnterpriseService
							.getSupplierService().getAllSuppliers(
									implementation));
			receiveNotes = accountsEnterpriseService.getReceiveService()
					.getAllReceiveNotes(implementation);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getInvoiceJsonList() {
		try {

			List<InvoiceVO> invoiceList = this.fetchInvoiceData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", invoiceList.size());
			jsonResponse.put("iTotalDisplayRecords", invoiceList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (Invoice list : invoiceList) {
				array = new JSONArray();
				array.add(list.getInvoiceId());
				array.add(list.getSupplier().getSupplierId());
				array.add(list.getInvoiceNumber());
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(list.getSupplier().getSupplierNumber());
				if (null != list.getSupplier().getPerson())
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					array.add(null != list.getSupplier().getCmpDeptLocation() ? list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				array.add(Constants.Accounts.InvoiceStatus.get(list.getStatus()));
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<InvoiceVO> fetchInvoiceData() throws Exception {

		Long invoiceId = 0L;
		Long receiveId = 0L;
		Long supplierId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("invoiceId") != null) {
			invoiceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("invoiceId"));
		}

		if (ServletActionContext.getRequest().getParameter("receiveId") != null) {
			receiveId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("receiveId"));
		}

		if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
			supplierId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("supplierId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Invoice> invoiceList = accountsEnterpriseService
				.getInvoiceService().getFilteredInvoice(implementation,
						invoiceId, supplierId, receiveId, fromDatee, toDatee);
		List<InvoiceVO> invoiceVOs = new ArrayList<InvoiceVO>();
		for (Invoice invoice : invoiceList) {
			InvoiceVO invoiceVO = new InvoiceVO(invoice);

			if (null != invoice.getSupplier().getPerson()) {
				invoiceVO
						.setSupplierName(invoice
								.getSupplier()
								.getPerson()
								.getFirstName()
								.concat(" ")
								.concat(invoice.getSupplier().getPerson()
										.getLastName()));
			} else {
				invoiceVO.setSupplierName(null != invoice.getSupplier()
						.getCmpDeptLocation() ? invoice.getSupplier()
						.getCmpDeptLocation().getCompany().getCompanyName()
						: invoice.getSupplier().getCompany().getCompanyName());
			}

			invoiceVO.setSupplierNumber(invoice.getSupplier()
					.getSupplierNumber());
			List<InvoiceDetailVO> invoiceDetailVOs = new ArrayList<InvoiceDetailVO>();
			for (InvoiceDetail invoiceDetail : invoice.getInvoiceDetails()) {
				InvoiceDetailVO invoiceDetailVO = new InvoiceDetailVO(
						invoiceDetail);
				invoiceDetailVO.setProductCode(invoiceDetail.getProduct()
						.getCode());
				invoiceDetailVO.setProductName(invoiceDetail.getProduct()
						.getProductName());
				invoiceDetailVO.setItemType(ItemType.get(
						invoiceDetail.getProduct().getItemType()).name());
				invoiceDetailVO.setReceiveReference(invoiceDetail
						.getReceiveDetail().getReceive().getReferenceNo());
				invoiceDetailVO.setReceivedQty(invoiceDetail.getReceiveDetail()
						.getReceiveQty());
				invoiceDetailVO.setInvoiceQty(invoiceDetail.getInvoiceQty());
				invoiceDetailVO.setUnitRate(invoiceDetail.getReceiveDetail()
						.getUnitRate());
				invoiceDetailVO.setTotalRate(invoiceDetail.getInvoiceQty()
						* invoiceDetail.getReceiveDetail().getUnitRate());
				invoiceDetailVOs.add(invoiceDetailVO);
			}
			invoiceVO.setInvoiceDetailVOList(invoiceDetailVOs);

			invoiceVOs.add(invoiceVO);
		}

		return invoiceVOs;
	}

	public String getInvoicePrintOut() {
		try {
			List<InvoiceVO> invoiceList = this.fetchInvoiceData();

			ServletActionContext.getRequest().setAttribute("INVOICES",
					invoiceList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getInvoiceXLS() {
		try {
			List<InvoiceVO> invoiceVOs = this.fetchInvoiceData();
			String str = "";

			for (InvoiceVO invoiceVO : invoiceVOs) {
				str += "\n\n SUPPLIER," + invoiceVO.getSupplierName() + "["
						+ invoiceVO.getSupplierNumber() + "] \n\n";
				str += "Reference Number, Date\n";

				str += invoiceVO.getInvoiceNumber()
						+ ","
						+ (DateFormat.convertDateToString(invoiceVO.getDate()
								.toString()));
				str += "\n\n";
				str += "PRODUCT CODE, PRODUCT, ITEM TYPE, Receive Reference, Receive Quantity, Invoice Quantity, Unit Rate, Total\n";
				for (InvoiceDetailVO invoiceDetailVO : invoiceVO
						.getInvoiceDetailVOList()) {
					str += invoiceDetailVO.getProductCode() + ","
							+ invoiceDetailVO.getProductName() + ","
							+ invoiceDetailVO.getItemType() + ","
							+ invoiceDetailVO.getReceiveReference() + ","
							+ invoiceDetailVO.getReceivedQty() + ","
							+ invoiceDetailVO.getInvoiceQty() + ","
							+ invoiceDetailVO.getUnitRate() + ","
							+ invoiceDetailVO.getTotalRate();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getInvoicePDF() {
		String result = ERROR;
		try {

			invoiceDS = this.fetchInvoiceData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getDebitNoteCriteriaData() {
		try {
			getImplementId();
			suppliersList = QuotationAction
					.convertListTO(accountsEnterpriseService
							.getSupplierService().getAllSuppliers(
									implementation));
			goodsReturnList = accountsEnterpriseService.getGoodsReturnService()
					.getAllGoodsReturn(implementation);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getDebitNoteJsonList() {
		try {

			List<DebitVO> debitList = this.fetchDebitNoteData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", debitList.size());
			jsonResponse.put("iTotalDisplayRecords", debitList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (DebitVO list : debitList) {
				array = new JSONArray();
				array.add(list.getDebitId());
				array.add(list.getDebitNumber());
				array.add(list.getStrDate());
				array.add(list.getGoodsReturn().getReturnNumber());
				array.add(list.getGoodsReturn().getSupplier()
						.getSupplierNumber());
				array.add(list.getSupplierName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<DebitVO> fetchDebitNoteData() throws Exception {

		List<DebitVO> debitVOs = new ArrayList<DebitVO>();

		Long debitId = 0L;
		Long returnId = 0L;
		Long supplierId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("debitId") != null) {
			debitId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("debitId"));
		}

		if (ServletActionContext.getRequest().getParameter("recordId") != null) {
			returnId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("recordId"));
		}

		if (ServletActionContext.getRequest().getParameter("returnId") != null) {
			returnId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("returnId"));
		}

		if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
			supplierId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("supplierId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Debit> debitList = accountsEnterpriseService.getDebitService()
				.getFilteredDebits(implementation, debitId, returnId,
						supplierId, fromDatee, toDatee);

		for (Debit debit : debitList) {
			DebitVO debitVO = new DebitVO(debit);

			if (null != debit.getGoodsReturn().getSupplier().getPerson()) {
				debitVO.setSupplierName(debit
						.getGoodsReturn()
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(debit.getGoodsReturn().getSupplier()
								.getPerson().getLastName()));
			} else {
				debitVO.setSupplierName(null != debit.getGoodsReturn()
						.getSupplier().getCmpDeptLocation() ? debit
						.getGoodsReturn().getSupplier().getCmpDeptLocation()
						.getCompany().getCompanyName() : debit.getGoodsReturn()
						.getSupplier().getCompany().getCompanyName());
			}

			debitVO.setSupplierNameAndNumber(debitVO.getSupplierName()
					+ debit.getGoodsReturn().getSupplier().getSupplierNumber());
			debitVO.setStrDate(DateFormat.convertDateToString(debit.getDate()
					.toString()));

			List<DebitDetailVO> debitDetailVOs = new ArrayList<DebitDetailVO>();

			for (DebitDetail debitDetail : debitVO.getDebitDetails()) {
				DebitDetailVO debitDetailVO = new DebitDetailVO(debitDetail);

				debitDetailVO.setProductName(debitDetail.getProduct()
						.getProductName());
				debitDetailVO
						.setProductCode(debitDetail.getProduct().getCode());
				debitDetailVO.setStrItemType(ItemType.get(
						debitDetail.getProduct().getItemType()).name());
				debitDetailVO.setReturnNumber(debit.getGoodsReturn()
						.getReturnNumber());
				debitDetailVO.setReturnQty(0D);
				debitDetailVO.setNoteQty(debitDetail.getReturnQty());
				debitDetailVO.setUnitRate(debitDetail.getAmount());
				debitDetailVO.setTotal(debitDetailVO.getNoteQty()
						* debitDetailVO.getUnitRate());
				debitDetailVOs.add(debitDetailVO);

			}
			debitVO.setDebitDetailVOs(debitDetailVOs);
			debitVOs.add(debitVO);
		}

		return debitVOs;
	}

	public String getDebitNotePrintOut() {
		try {
			List<DebitVO> debitNoteList = this.fetchDebitNoteData();

			ServletActionContext.getRequest().setAttribute("DEBIT_NOTE",
					debitNoteList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getDebitNoteXLS() {
		try {
			List<DebitVO> debitList = this.fetchDebitNoteData();
			String str = "";

			for (DebitVO debitVO : debitList) {
				str += "\n\n SUPPLIER," + debitVO.getSupplierNameAndNumber()
						+ " \n\n";
				str += "Reference Number, Date\n";

				str += debitVO.getDebitNumber() + "," + debitVO.getStrDate();
				str += "\n\n";
				str += "PRODUCT CODE, PRODUCT, ITEM TYPE, Goods Return Reference, Return Quantity, Note Quantity, Unit Rate, Total\n";
				for (DebitDetailVO debitDetailVO : debitVO.getDebitDetailVOs()) {
					str += debitDetailVO.getProductCode() + ","
							+ debitDetailVO.getProductName() + ","
							+ debitDetailVO.getStrItemType() + ","
							+ debitDetailVO.getReturnNumber() + ","
							+ debitDetailVO.getReturnQty() + ","
							+ debitDetailVO.getNoteQty() + ","
							+ debitDetailVO.getUnitRate() + ","
							+ debitDetailVO.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getDebitNotePDF() {
		String result = ERROR;
		try {

			debitDS = this.fetchDebitNoteData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getInventoryPaymentCriteriaData() {
		try {
			getImplementId();
			suppliersList = QuotationAction
					.convertListTO(accountsEnterpriseService
							.getSupplierService().getAllSuppliers(
									implementation));
			receiveNotes = accountsEnterpriseService.getReceiveService()
					.getAllReceiveNotes(implementation);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getInventoryPaymentJsonList() {
		try {

			List<PaymentVO> paymentList = this.fetchInventoryPaymentData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", paymentList.size());
			jsonResponse.put("iTotalDisplayRecords", paymentList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PaymentVO list : paymentList) {
				array = new JSONArray();
				String voidCheck = "";
				if (null != list.getVoidPayments()
						&& list.getVoidPayments().size() > 0)
					voidCheck = "<span title='Void Payment' class='void_payment_view'></span>";
				array.add(list.getPaymentId());
				array.add(list.getPaymentNumber().concat(voidCheck));
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(null != list.getBankAccount() ? list.getBankAccount()
						.getAccountNumber() : "");
				array.add(null != list.getChequeDate() ? DateFormat
						.convertDateToString(list.getChequeDate().toString())
						: "");
				array.add(null != list.getChequeNumber()
						&& list.getChequeNumber() > 0 ? list.getChequeNumber()
						: "");
				if (null != list.getSupplier().getPerson())
					array.add(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					array.add(null != list.getSupplier().getCmpDeptLocation() ? list
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PaymentVO> fetchInventoryPaymentData() throws Exception {

		Long paymentId = 0L;
		Long receiveId = 0L;
		Long supplierId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("paymentId") != null) {
			paymentId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("paymentId"));
		}

		if (ServletActionContext.getRequest().getParameter("receiveId") != null) {
			receiveId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("receiveId"));
		}

		if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
			supplierId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("supplierId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Payment> paymentList = accountsEnterpriseService
				.getPaymentService().getFilteredPayments(implementation,
						paymentId, receiveId, supplierId, fromDatee, toDatee);
		List<PaymentVO> paymentVOs = new ArrayList<PaymentVO>();
		for (Payment payment : paymentList) {
			PaymentVO paymentVO = new PaymentVO(payment);
			if (payment.getReceive() != null) {
				if (null != payment.getReceive().getSupplier().getPerson()) {
					paymentVO.setSupplierName(payment
							.getReceive()
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(payment.getReceive().getSupplier()
									.getPerson().getLastName()));
				} else {
					paymentVO.setSupplierName(null != payment.getReceive()
							.getSupplier().getCmpDeptLocation() ? payment
							.getReceive().getSupplier().getCmpDeptLocation()
							.getCompany().getCompanyName() : payment
							.getReceive().getSupplier().getCompany()
							.getCompanyName());
				}
				paymentVO.setSupplierNameAndNumber(paymentVO.getSupplierName()
						+ payment.getReceive().getSupplier()
								.getSupplierNumber());
			}

			if (payment.getDate() != null) {
				paymentVO.setStrDate(DateFormat.convertDateToString(payment
						.getDate().toString()));
			}

			List<PaymentDetailVO> paymentDetailVOs = new ArrayList<PaymentDetailVO>();
			for (PaymentDetail paymentDetail : payment.getPaymentDetails()) {
				PaymentDetailVO paymentDetailVO = new PaymentDetailVO(
						paymentDetail);

				if (paymentDetail.getInvoiceDetail() != null) {
					paymentDetailVO.setProductName(paymentDetail
							.getInvoiceDetail().getProduct().getProductName());
					paymentDetailVO.setProductCode(paymentDetail
							.getInvoiceDetail().getProduct().getCode());
					paymentDetailVO.setStrItemType(ItemType.get(
							paymentDetail.getInvoiceDetail().getProduct()
									.getItemType()).name());
					paymentDetailVO
							.setInvoiceReference(paymentDetail
									.getInvoiceDetail().getInvoice()
									.getInvoiceNumber());
					paymentDetailVO.setInvoiceQty(paymentDetail
							.getInvoiceDetail().getInvoiceQty());
					paymentDetailVO.setReceiceQty(paymentDetail
							.getInvoiceDetail().getReceiveDetail()
							.getReceiveQty());
					paymentDetailVO.setUnitRate(paymentDetail
							.getInvoiceDetail().getReceiveDetail()
							.getUnitRate());
				}

				if (paymentDetailVO.getReceiceQty() != null
						&& paymentDetailVO.getUnitRate() != null) {
					paymentDetailVO.setTotal(paymentDetailVO.getReceiceQty()
							* paymentDetailVO.getUnitRate());
				}

				paymentDetailVOs.add(paymentDetailVO);
			}
			paymentVO.setPaymentDetailVOs(paymentDetailVOs);
			paymentVOs.add(paymentVO);

		}

		return paymentVOs;
	}

	public String getInventoryPaymentPrintOut() {
		try {
			List<PaymentVO> paymentList = this.fetchInventoryPaymentData();

			ServletActionContext.getRequest().setAttribute("PAYMENTS",
					paymentList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getInventoryPaymentXLS() {
		try {
			List<PaymentVO> paymentList = this.fetchInventoryPaymentData();
			String str = "";

			for (PaymentVO paymentVO : paymentList) {
				str += "\n\n SUPPLIER," + paymentVO.getSupplierNameAndNumber()
						+ " \n\n";
				str += "Reference Number, Date\n";

				str += paymentVO.getPaymentNumber() + ","
						+ paymentVO.getStrDate();
				str += "\n\n";
				str += "PRODUCT CODE, PRODUCT, ITEM TYPE, Invoice Reference, Invoice Quantity, Receive Quantity, Unit Rate, Total\n";
				for (PaymentDetailVO paymentDetailVO : paymentVO
						.getPaymentDetailVOs()) {
					str += paymentDetailVO.getProductCode() + ","
							+ paymentDetailVO.getProductName() + ","
							+ paymentDetailVO.getStrItemType() + ","
							+ paymentDetailVO.getInvoiceReference() + ","
							+ paymentDetailVO.getInvoiceQty() + ","
							+ paymentDetailVO.getReceiceQty() + ","
							+ paymentDetailVO.getUnitRate() + ","
							+ paymentDetailVO.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getInventoryPaymentPDF() {
		String result = ERROR;
		try {

			paymentDS = this.fetchInventoryPaymentData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getCustomerReportCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_TYPE_LIST", AIOSCommons.getCustomerTypes());
			ServletActionContext.getRequest().setAttribute(
					"creditTermList",
					accountsEnterpriseService.getCreditTermService()
							.getAllCreditTerms(implementation));
			ServletActionContext.getRequest().setAttribute(
					"refferalSourceList",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"REFFERAL_SOURCE", true));
			ServletActionContext.getRequest().setAttribute("MEMBER_CARD_TYPES",
					AIOSCommons.getMemberCardTypes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getCustomerJsonList() {
		try {

			List<CustomerVO> customerList = this.fetchCustomerData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", customerList.size());
			jsonResponse.put("iTotalDisplayRecords", customerList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (CustomerVO list : customerList) {
				array = new JSONArray();
				array.add(list.getCustomerId());
				array.add(list.getCustomerNumber());
				array.add(Constants.Accounts.CustomerType.get(list
						.getCustomerType()));
				array.add(null != list.getCreditTerm() ? list.getCreditTerm()
						.getName() : "");
				if (null != list.getPersonByPersonId()
						&& !("").equals(list.getPersonByPersonId())) {
					array.add("Person");
					array.add(list.getPersonByPersonId().getFirstName()
							.concat(" ")
							.concat(list.getPersonByPersonId().getLastName()));
				} else {
					array.add("Company");
					array.add(list.getCompany() != null ? list.getCompany()
							.getCompanyName() : list.getCmpDeptLocation()
							.getCompany().getCompanyName());
				}
				array.add(null != list.getPersonBySaleRepresentative() ? list
						.getPersonBySaleRepresentative()
						.getFirstName()
						.concat(" ")
						.concat(list.getPersonBySaleRepresentative()
								.getLastName()) : "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<CustomerVO> fetchCustomerData() throws Exception {

		Long customerId = 0L;
		Integer customerTypeId = 0;
		Long creditTermId = 0L;
		Long refferalSourceId = 0L;
		Byte memberTypeId = 0;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerType") != null) {
			customerTypeId = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("customerType"));
		}

		if (ServletActionContext.getRequest().getParameter("creditTermId") != null) {
			creditTermId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("creditTermId"));
		}

		if (ServletActionContext.getRequest().getParameter("refferalSourceId") != null) {
			refferalSourceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("refferalSourceId"));
		}

		if (ServletActionContext.getRequest().getParameter("memberTypeId") != null) {
			memberTypeId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("memberTypeId"));
		}

		List<Customer> customerList = accountsEnterpriseService
				.getCustomerService().getFilteredCustomers(implementation,
						customerId, customerTypeId, creditTermId,
						refferalSourceId, memberTypeId);
		List<CustomerVO> customerVOs = new ArrayList<CustomerVO>();
		for (Customer customer : customerList) {
			CustomerVO customerVO = new CustomerVO(customer);
			if (null != customer.getPersonByPersonId()) {
				customerVO.setCustomerName(customer.getPersonByPersonId()
						.getFirstName().concat(" ")
						.concat(customer.getPersonByPersonId().getLastName()));

			} else {
				customerVO.setCustomerName(null != customer
						.getCmpDeptLocation() ? customer.getCmpDeptLocation()
						.getCompany().getCompanyName() : customer.getCompany()
						.getCompanyName());
			}

			customerVO.setStrCustomerType(Constants.Accounts.CustomerType.get(
					customer.getCustomerType()).toString());
			if (customer.getMemberType() != null) {
				customerVO.setStrMemberType(Constants.Accounts.MemberCardType
						.get(customer.getMemberType()).toString());
			} else {
				customerVO.setStrMemberType("");
			}

			if (customer.getCreditTerm() != null) {
				customerVO.setStrCreditTerm(customer.getCreditTerm().getName());
			} else {
				customerVO.setStrCreditTerm("");
			}

			if (customer.getLookupDetailByRefferalSource() != null) {
				customerVO.setStrRefferalSource(customer
						.getLookupDetailByRefferalSource().getDisplayName());
			} else {
				customerVO.setStrRefferalSource("");
			}

			customerVOs.add(customerVO);

		}

		return customerVOs;
	}

	public String getCustomerReportPrintOut() {
		try {
			List<CustomerVO> customerList = this.fetchCustomerData();

			ServletActionContext.getRequest().setAttribute("CUSTOMER",
					customerList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getCustomerReportXLS() {
		try {
			List<CustomerVO> customerList = this.fetchCustomerData();
			String str = "";

			for (CustomerVO customerVO : customerList) {
				str += "\n\n Customer Number, Customer Name, Customer Type, Credit Term, Refferal Source, Member Type \n";

				str += customerVO.getCustomerNumber() + ","
						+ customerVO.getCustomerName() + ","
						+ customerVO.getStrCustomerType() + ","
						+ customerVO.getStrCreditTerm() + ","
						+ customerVO.getStrRefferalSource() + ","
						+ customerVO.getStrMemberType();
				str += "\n\n";
				str += "Shipping Name, Contact Person, Contact Number\n";
				for (ShippingDetail shippingDetail : customerVO
						.getShippingDetails()) {
					str += shippingDetail.getName() + ","
							+ shippingDetail.getContactPerson() + ","
							+ shippingDetail.getContactNo();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getCustomerReportPDF() {
		String result = ERROR;
		try {

			customerDS = this.fetchCustomerData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getCreditTermJsonList() {
		try {

			List<CreditTermVO> creditTermList = this
					.fetchCreditTermReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (CreditTermVO list : creditTermList) {
				array = new JSONArray();
				array.add(list.getCreditTermId());
				array.add(list.getName());
				array.add(null != list.getDueDay() ? CreditTermDueDay.get(
						list.getDueDay()).name() : "");
				array.add(null != list.getDiscountDay() ? list.getDiscountDay()
						: "");
				array.add(null != list.getDiscountMode() ? CreditTermDiscountMode
						.get(list.getDiscountMode()).name() : "");
				array.add(list.getDiscount());
				array.add(null != list.getPenaltyType() ? CreditTermPenaltyType
						.get(list.getPenaltyType()).name() : "");
				array.add(null != list.getPenaltyMode() ? CreditTermDiscountMode
						.get(list.getPenaltyMode()).name() : "");
				array.add(list.getPenalty());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getCreditTermReportCriteriaData() {
		try {
			ServletActionContext.getRequest().setAttribute(
					"CREDIT_TERM_BASEDAY", AIOSCommons.getDueDays());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public List<CreditTermVO> fetchCreditTermReportData() throws Exception {

		Long termId = 0L;
		Long discountFor = 0L;
		Boolean isSupplier = null;
		Date fromDatee = null;
		Date toDatee = null;
		Byte dueDay = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("termId") != null) {
			termId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("termId"));
		}

		if (ServletActionContext.getRequest().getParameter("discountFor") != null) {
			discountFor = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("discountFor"));
			if (discountFor == 1) {
				isSupplier = false;
			} else if (discountFor == 2) {
				isSupplier = true;
			}
		}

		if (ServletActionContext.getRequest().getParameter("dueDay") != null) {
			dueDay = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("dueDay"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<CreditTerm> creditTermList = accountsEnterpriseService
				.getCreditTermService().getFilteredCreditTerms(implementation,
						termId, isSupplier, dueDay);

		List<CreditTermVO> creditTermVOs = new ArrayList<CreditTermVO>();
		for (CreditTerm creditTerm : creditTermList) {
			CreditTermVO creditTermVO = new CreditTermVO(creditTerm);

			creditTermVO
					.setStrDueDay(null != creditTerm.getDueDay() ? CreditTermDueDay
							.get(creditTerm.getDueDay()).name() : "");
			creditTermVO.setStrDiscountMode(null != creditTerm
					.getDiscountMode() ? CreditTermDiscountMode.get(
					creditTerm.getDiscountMode()).name() : "");
			creditTermVO
					.setStrPenaltyType(null != creditTerm.getPenaltyType() ? CreditTermPenaltyType
							.get(creditTerm.getPenaltyType()).name() : "");
			creditTermVO
					.setStrPenaltyMode(null != creditTerm.getPenaltyMode() ? CreditTermDiscountMode
							.get(creditTerm.getPenaltyMode()).name() : "");
			if (creditTerm.getSuppliers() != null) {
				creditTermVO.setCustomerSupplierName("[Supplier]");
			} else if (creditTerm.getCustomers() != null) {
				creditTermVO.setCustomerSupplierName("[Customer]");
			}

			creditTermVOs.add(creditTermVO);

		}

		return creditTermVOs;
	}

	public String getCreditTermReportPrintOut() {
		try {
			List<CreditTermVO> creditTermList = this
					.fetchCreditTermReportData();

			ServletActionContext.getRequest().setAttribute("CREDIT_TERM",
					creditTermList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getCreditTermReportXLS() {
		try {
			List<CreditTermVO> creditTermList = this
					.fetchCreditTermReportData();
			String str = "";

			for (CreditTermVO creditTermVO : creditTermList) {
				str += "\n\n Term Name, [Customer or Supplier], Due Day, Discount Mode, Discount, Penality Mode, Penality \n";

				str += creditTermVO.getName() + ","
						+ creditTermVO.getCustomerSupplierName() + ","
						+ creditTermVO.getStrDueDay() + ","
						+ creditTermVO.getStrDiscountMode() + ","
						+ creditTermVO.getDiscount() + ","
						+ creditTermVO.getStrPenaltyMode() + ","
						+ creditTermVO.getPenalty();
				str += "\n";

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getCreditTermPDF() {
		String result = ERROR;
		try {

			creditTermDS = this.fetchCreditTermReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getCustomerQuotationCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"customerList",
					accountsEnterpriseService.getCustomerService()
							.getFilteredCustomers(implementation, null, null,
									null, null, null));
			ServletActionContext.getRequest().setAttribute("statusList",
					AIOSCommons.getO2CProcessStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getCustomerQuotationJsonList() {
		try {

			List<CustomerQuotationVO> customerQuotationList = this
					.fetchCustomerQuotationReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", customerQuotationList.size());
			jsonResponse.put("iTotalDisplayRecords",
					customerQuotationList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (CustomerQuotationVO list : customerQuotationList) {
				array = new JSONArray();
				array.add(list.getCustomerQuotationId());
				array.add(list.getReferenceNo());
				array.add(list.getOrderDateFormat());
				array.add(list.getExpiryDateFormat());
				array.add(list.getShippingDateFormat());
				array.add(list.getCustomerName());
				array.add(list.getSalesRep());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<CustomerQuotationVO> fetchCustomerQuotationReportData()
			throws Exception {

		Long quotationId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("quotationId") != null) {
			quotationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("quotationId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<CustomerQuotation> customerQuotationList = accountsEnterpriseService
				.getCustomerQuotationService().getFilteredCustomerQuotation(
						implementation, quotationId, customerId, statusId,
						fromDatee, toDatee);

		List<CustomerQuotationVO> customerQuotationVOs = new ArrayList<CustomerQuotationVO>();

		for (CustomerQuotation customerQuotation : customerQuotationList) {
			CustomerQuotationVO customerQuotationVO = new CustomerQuotationVO(
					customerQuotation);

			customerQuotationVO
					.setOrderDateFormat(DateFormat
							.convertDateToString(customerQuotation.getDate()
									.toString()));
			customerQuotationVO.setExpiryDateFormat(DateFormat
					.convertDateToString(customerQuotation.getExpiryDate()
							.toString()));
			customerQuotationVO.setShippingDateFormat(DateFormat
					.convertDateToString(customerQuotation.getShippingDate()
							.toString()));
			if (customerQuotation.getCustomer() != null) {
				customerQuotationVO
						.setCustomerName(null != customerQuotation
								.getCustomer().getPersonByPersonId() ? customerQuotation
								.getCustomer()
								.getPersonByPersonId()
								.getFirstName()
								.concat("")
								.concat(customerQuotation.getCustomer()
										.getPersonByPersonId().getLastName())
								: (null != customerQuotation.getCustomer()
										.getCompany() ? customerQuotation
										.getCustomer().getCompany()
										.getCompanyName() : customerQuotation
										.getCustomer().getCmpDeptLocation()
										.getCompany().getCompanyName()));
			}
			customerQuotationVO.setSalesRep(null != customerQuotation
					.getPerson() ? customerQuotation.getPerson().getFirstName()
					.concat(" ")
					.concat(customerQuotation.getPerson().getLastName()) : "");

			if (customerQuotation.getCustomer() != null) {
				customerQuotationVO.setCustomerNameAndNo(customerQuotationVO
						.getCustomerName()
						+ " ["
						+ customerQuotationVO.getCustomer().getCustomerNumber()
						+ "]");
			}
			customerQuotationVO.setShippingMethod(customerQuotation
					.getLookupDetailByShippingMethod().getDisplayName());
			if (customerQuotation.getLookupDetailByShippingTerm() != null) {
				customerQuotationVO.setShippingTerm(customerQuotation
						.getLookupDetailByShippingTerm().getDisplayName());
			}
			customerQuotationVO
					.setStrStatus(Constants.Accounts.O2CProcessStatus
							.get(customerQuotation.getStatus()).toString()
							.replaceAll("_", " "));

			List<CustomerQuotationDetailVO> quotationDetailVOs = new ArrayList<CustomerQuotationDetailVO>();

			for (CustomerQuotationDetail detail : customerQuotation
					.getCustomerQuotationDetails()) {
				CustomerQuotationDetailVO quotationDetailVO = new CustomerQuotationDetailVO(
						detail);
				quotationDetailVO.setProductCode(detail.getProduct().getCode());
				quotationDetailVO.setProductName(detail.getProduct()
						.getProductName());
				quotationDetailVO
						.setStoreName(detail.getStore().getStoreName());
				if (detail.getIsPercentage() != null
						&& detail.getIsPercentage()) {
					quotationDetailVO.setStrDiscountMode("Percentage");
				} else {
					quotationDetailVO.setStrDiscountMode("Amount");
				}
				quotationDetailVO.setTotal(detail.getQuantity()
						* detail.getUnitRate());
				if (detail.getDiscount() != null) {
					quotationDetailVO.setTotal(quotationDetailVO.getTotal()
							- detail.getDiscount());
				}
				quotationDetailVOs.add(quotationDetailVO);
			}
			customerQuotationVO
					.setCustomerQuotationDetailVOs(quotationDetailVOs);

			List<CustomerQuotationChargeVO> quotationChargeVOs = new ArrayList<CustomerQuotationChargeVO>();

			for (CustomerQuotationCharge charges : customerQuotation
					.getCustomerQuotationCharges()) {
				CustomerQuotationChargeVO quotationChargeVO = new CustomerQuotationChargeVO(
						charges);
				if (charges != null && charges.getIsPercentage()) {
					quotationChargeVO.setChargesMode("Percentage");
				} else {
					quotationChargeVO.setChargesMode("Amount");
				}
				quotationChargeVO.setChargesType(charges.getLookupDetail()
						.getDisplayName());

				quotationChargeVOs.add(quotationChargeVO);
			}
			customerQuotationVO
					.setCustomerQuotationChargeVOs(quotationChargeVOs);

			customerQuotationVOs.add(customerQuotationVO);

		}

		return customerQuotationVOs;
	}

	public String getCustomerQuotationReportPrintOut() {
		try {
			List<CustomerQuotationVO> customerQuotationList = this
					.fetchCustomerQuotationReportData();

			ServletActionContext.getRequest().setAttribute("CUST_QUOTATION",
					customerQuotationList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getCustomerQuotationReportXLS() {
		try {
			List<CustomerQuotationVO> customerQuotationList = this
					.fetchCustomerQuotationReportData();
			String str = "";

			for (CustomerQuotationVO quotation : customerQuotationList) {
				str += "\n Customer Name," + quotation.getCustomerNameAndNo();
				str += "\n\n Reference Number, Date, Expiry Date, Shipping Date, Shipping Method, Shipping Terms, Status \n";

				str += quotation.getReferenceNo() + ","
						+ quotation.getOrderDateFormat() + ","
						+ quotation.getExpiryDateFormat() + ","
						+ quotation.getShippingDateFormat() + ","
						+ quotation.getShippingMethod() + ","
						+ quotation.getShippingTerm() + ","
						+ quotation.getStrStatus();
				str += "\n";

				str += "\n\n Product Code, Product, Store Name, Quantity, Unit Price, Discount Mode, Discount, Total \n";
				for (CustomerQuotationDetailVO detail : quotation
						.getCustomerQuotationDetailVOs()) {
					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getStoreName() + ","
							+ detail.getQuantity() + "," + detail.getUnitRate()
							+ "," + detail.getStrDiscountMode() + ","
							+ detail.getTotal();
					str += "\n";
				}

				str += "\n\n Charges Type, Mode, Charges \n";
				for (CustomerQuotationChargeVO charges : quotation
						.getCustomerQuotationChargeVOs()) {
					str += charges.getChargesType() + ","
							+ charges.getChargesMode() + ","
							+ charges.getCharges();
					str += "\n";
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getCustomerQuotationPDF() {
		String result = ERROR;
		try {

			customerQuotationDS = this.fetchCustomerQuotationReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSalesOrderCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"customerList",
					accountsEnterpriseService.getCustomerService()
							.getFilteredCustomers(implementation, null, null,
									null, null, null));
			ServletActionContext.getRequest().setAttribute("statusList",
					AIOSCommons.getO2CProcessStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getSalesOrderJsonList() {
		try {

			List<SalesOrderVO> salesOrderList = this
					.fetchSalesOrderReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", salesOrderList.size());
			jsonResponse.put("iTotalDisplayRecords", salesOrderList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (SalesOrderVO list : salesOrderList) {
				array = new JSONArray();
				array.add(list.getSalesOrderId());
				array.add(list.getReferenceNumber());
				array.add(list.getOrderTypeDetail());
				array.add(list.getSalesDate());
				array.add(list.getShipDate());
				array.add(list.getExpirySales());
				array.add(list.getSalesStatus());
				array.add(list.getCustomerName());
				array.add(list.getSalesReperesentive());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<SalesOrderVO> fetchSalesOrderReportData() throws Exception {

		Long salesOrderId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("salesOrderId") != null) {
			salesOrderId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesOrderId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<SalesOrder> salesOrderList = accountsEnterpriseService
				.getSalesOrderService().getFilteredSalesOrder(implementation,
						salesOrderId, customerId, statusId, fromDatee, toDatee);

		List<SalesOrderVO> salesOrderVOs = new ArrayList<SalesOrderVO>();

		for (SalesOrder salesOrder : salesOrderList) {
			SalesOrderVO salesOrderVO = new SalesOrderVO(salesOrder);
			salesOrderVO.setSalesDate(DateFormat.convertDateToString(salesOrder
					.getOrderDate().toString()));
			salesOrderVO.setShipDate(DateFormat.convertDateToString(salesOrder
					.getShippingDate().toString()));
			salesOrderVO
					.setExpirySales(DateFormat.convertDateToString(salesOrder
							.getExpiryDate().toString()));
			salesOrderVO.setOrderTypeDetail(salesOrder
					.getLookupDetailByOrderType().getDisplayName());
			salesOrderVO.setCustomerName(null != salesOrder.getCustomer()
					.getPersonByPersonId() ? salesOrder
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesOrder.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesOrder.getCustomer()
					.getCompany() ? salesOrder.getCustomer().getCompany()
					.getCompanyName() : salesOrder.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			salesOrderVO
					.setSalesReperesentive(null != salesOrder.getPerson() ? salesOrder
							.getPerson().getFirstName().concat(" ")
							.concat(salesOrder.getPerson().getLastName())
							: "");
			salesOrderVO.setSalesStatus(O2CProcessStatus.get(
					salesOrder.getOrderStatus()).name());

			salesOrderVO
					.setCustomerNameAndNo(salesOrderVO.getCustomerName() + " ["
							+ salesOrder.getCustomer().getCustomerNumber()
							+ "]");

			if (salesOrder.getLookupDetailByShippingMethod() != null) {
				salesOrderVO.setShippingMethod(salesOrder
						.getLookupDetailByShippingMethod().getDisplayName());
			}

			if (salesOrder.getLookupDetailByShippingTerm() != null) {
				salesOrderVO.setShippingTerm(salesOrder
						.getLookupDetailByShippingTerm().getDisplayName());
			}
			salesOrderVO.setStrStatus(Constants.Accounts.O2CProcessStatus.get(
					salesOrder.getOrderStatus()).toString());

			List<SalesOrderDetailVO> salesOrderDetailVOs = new ArrayList<SalesOrderDetailVO>();
			for (SalesOrderDetail detail : salesOrder.getSalesOrderDetails()) {

				SalesOrderDetailVO salesOrderDetailVO = new SalesOrderDetailVO(
						detail);
				salesOrderDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesOrderDetailVO
						.setProductCode(detail.getProduct().getCode());
				salesOrderDetailVO
						.setStoreName(null != detail.getStore() ? detail
								.getStore().getStoreName() : "");
				if (detail.getIsPercentage()) {
					salesOrderDetailVO.setStrDiscountMode("Percentage");
				} else {
					salesOrderDetailVO.setStrDiscountMode("Amount");
				}
				salesOrderDetailVO.setTotal((detail.getOrderQuantity() * detail
						.getUnitRate()) - detail.getDiscount());
				salesOrderDetailVOs.add(salesOrderDetailVO);
			}
			salesOrderVO.setSalesOrderDetailVOs(salesOrderDetailVOs);
			List<SalesOrderChargesVO> salesOrderChargesVOs = new ArrayList<SalesOrderChargesVO>();
			for (SalesOrderCharge charge : salesOrder.getSalesOrderCharges()) {

				SalesOrderChargesVO salesOrderChargesVO = new SalesOrderChargesVO(
						charge);
				salesOrderChargesVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());
				if (charge.getIsPercentage()) {
					salesOrderChargesVO.setChargeMode("Percentage");
				} else {
					salesOrderChargesVO.setChargeMode("Amount");
				}
				salesOrderChargesVOs.add(salesOrderChargesVO);
			}
			salesOrderVO.setSalesOrderChargesVOs(salesOrderChargesVOs);

			salesOrderVOs.add(salesOrderVO);
		}

		return salesOrderVOs;
	}

	public String getSalesOrderReportPrintOut() {
		try {
			List<SalesOrderVO> salesOrderList = this
					.fetchSalesOrderReportData();

			ServletActionContext.getRequest().setAttribute("SALES_ORDER",
					salesOrderList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getSalesOrderReportXLS() {
		try {
			List<SalesOrderVO> salesOrderList = this
					.fetchSalesOrderReportData();
			String str = "";

			for (SalesOrderVO salesOrder : salesOrderList) {
				str += "\n Customer Name," + salesOrder.getCustomerNameAndNo();
				str += "\n\n Reference Number, Date, Expiry Date, Shipping Date, Shipping Method, Shipping Terms, Status \n";

				str += salesOrder.getReferenceNumber() + ","
						+ salesOrder.getSalesDate() + ","
						+ salesOrder.getExpirySales() + ","
						+ salesOrder.getShipDate() + ","
						+ salesOrder.getShippingMethod() + ","
						+ salesOrder.getShippingTerm() + ","
						+ salesOrder.getStrStatus();
				str += "\n";

				str += "\n\n Product Code, Product, Store Name, Quantity, Unit Price, Discount Mode, Discount, Total \n";
				for (SalesOrderDetailVO detail : salesOrder
						.getSalesOrderDetailVOs()) {
					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getStoreName() + ","
							+ detail.getOrderQuantity() + ","
							+ detail.getUnitRate() + ","
							+ detail.getStrDiscountMode() + ","
							+ detail.getTotal();
					str += "\n";
				}

				str += "\n\n Charges Type, Mode, Charges \n";
				for (SalesOrderChargesVO charges : salesOrder
						.getSalesOrderChargesVOs()) {
					str += charges.getChargeType() + ","
							+ charges.getChargeMode() + ","
							+ charges.getCharges();
					str += "\n";
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getSalesOrderPDF() {
		String result = ERROR;
		try {
			salesOrderDS = this.fetchSalesOrderReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSalesDeliveryCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"customerList",
					accountsEnterpriseService.getCustomerService()
							.getFilteredCustomers(implementation, null, null,
									null, null, null));
			ServletActionContext.getRequest().setAttribute("statusList",
					AIOSCommons.getO2CProcessStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getSalesDeliveryJsonList() {
		try {

			List<SalesDeliveryNoteVO> salesDeliveryList = this
					.fetchSalesDeliveryReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (SalesDeliveryNoteVO list : salesDeliveryList) {
				array = new JSONArray();
				array.add(list.getSalesDeliveryNoteId());
				array.add(list.getReferenceNumber());
				array.add(list.getDispatchDate());
				array.add(list.getCustomerName());
				array.add(list.getSalesPerson());
				array.add(list.getStrStatus());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<SalesDeliveryNoteVO> fetchSalesDeliveryReportData()
			throws Exception {

		Long salesDeliveryId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("salesDeliveryId") != null) {
			salesDeliveryId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesDeliveryId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<SalesDeliveryNote> salesDeliveryList = accountsEnterpriseService
				.getSalesDeliveryNoteService().getFilteredDeliveryNotes(
						implementation, salesDeliveryId, customerId, statusId,
						fromDatee, toDatee, null);

		List<SalesDeliveryNoteVO> salesDeliveryNoteVOs = new ArrayList<SalesDeliveryNoteVO>();

		for (SalesDeliveryNote salesDelivery : salesDeliveryList) {
			SalesDeliveryNoteVO deliveryNoteVO = new SalesDeliveryNoteVO(
					salesDelivery);
			deliveryNoteVO.setDispatchDate(DateFormat
					.convertDateToString(salesDelivery.getDeliveryDate()
							.toString()));
			deliveryNoteVO.setShippingDateFormat("");
			deliveryNoteVO.setExpiryDateFormat("");

			deliveryNoteVO.setCustomerName(null != salesDelivery.getCustomer()
					.getPersonByPersonId() ? salesDelivery
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesDelivery.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesDelivery
					.getCustomer().getCompany() ? salesDelivery.getCustomer()
					.getCompany().getCompanyName() : salesDelivery
					.getCustomer().getCmpDeptLocation().getCompany()
					.getCompanyName()));
			deliveryNoteVO.setSalesPerson(null != salesDelivery
					.getPersonBySalesPersonId() ? salesDelivery
					.getPersonBySalesPersonId()
					.getFirstName()
					.concat(" ")
					.concat(salesDelivery.getPersonBySalesPersonId()
							.getLastName()) : "");
			deliveryNoteVO.setStrStatus(O2CProcessStatus.get(
					salesDelivery.getStatus()).name());

			deliveryNoteVO.setCustomerNameAndNo(deliveryNoteVO
					.getCustomerName()
					+ " ["
					+ salesDelivery.getCustomer().getCustomerNumber() + "]");
			deliveryNoteVO.setShippingMethod(null != salesDelivery
					.getLookupDetailByShippingMethod() ? salesDelivery
					.getLookupDetailByShippingMethod().getDisplayName() : "");
			if (salesDelivery.getLookupDetailByShippingTerm() != null) {
				deliveryNoteVO.setShippingTerm(salesDelivery
						.getLookupDetailByShippingTerm().getDisplayName());
			}

			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
			for (SalesDeliveryDetail detail : salesDelivery
					.getSalesDeliveryDetails()) {

				SalesDeliveryDetailVO salesDeliveryDetailVO = new SalesDeliveryDetailVO(
						detail);
				salesDeliveryDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesDeliveryDetailVO.setProductCode(detail.getProduct()
						.getCode());
				if (null != detail.getShelf()) {
					salesDeliveryDetailVO.setStoreName(detail.getShelf()
							.getShelf().getAisle().getStore().getStoreName()
							+ ">>"
							+ detail.getShelf().getShelf().getAisle()
									.getSectionName()
							+ ">>"
							+ detail.getShelf().getShelf().getName()
							+ ">>"
							+ detail.getShelf().getName());
				}
				detail.setDeliveryQuantity(null != detail.getPackageUnit() ? detail
						.getPackageUnit() : detail.getDeliveryQuantity());
				salesDeliveryDetailVO.setDeliveryQuantity(detail
						.getDeliveryQuantity());
				salesDeliveryDetailVO.setUnitName(null != detail
						.getProductPackageDetail() ? detail
						.getProductPackageDetail().getLookupDetail()
						.getDisplayName() : detail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());
				if (detail.getDeliveryQuantity() != null
						&& detail.getUnitRate() != null) {
					salesDeliveryDetailVO.setTotal(detail.getDeliveryQuantity()
							* detail.getUnitRate());
				}
				if (null != detail.getDiscount() && detail.getDiscount() > 0) {
					if (detail.getIsPercentage() != null
							&& detail.getIsPercentage()) {
						salesDeliveryDetailVO.setStrDiscountMode("Percentage");
						double discount = (salesDeliveryDetailVO.getTotal() * detail
								.getDiscount()) / 100;
						salesDeliveryDetailVO.setTotal(salesDeliveryDetailVO
								.getTotal() - discount);
					} else {
						salesDeliveryDetailVO.setStrDiscountMode("Amount");
						salesDeliveryDetailVO.setTotal(salesDeliveryDetailVO
								.getTotal() - detail.getDiscount());
					}
				}

				salesDeliveryDetailVOs.add(salesDeliveryDetailVO);
			}
			deliveryNoteVO.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

			List<SalesDeliveryChargeVO> salesDeliveryChargeVOs = new ArrayList<SalesDeliveryChargeVO>();
			for (SalesDeliveryCharge charge : salesDelivery
					.getSalesDeliveryCharges()) {

				SalesDeliveryChargeVO salesDeliveryChargeVO = new SalesDeliveryChargeVO(
						charge);
				salesDeliveryChargeVO.setStrChargeType(charge.getLookupDetail()
						.getDisplayName());
				if (charge.getIsPercentage()) {
					salesDeliveryChargeVO.setStrMode("Percentage");
				} else {
					salesDeliveryChargeVO.setStrMode("Amount");
				}
				salesDeliveryChargeVOs.add(salesDeliveryChargeVO);
			}
			deliveryNoteVO.setSalesDeliveryChargeVOs(salesDeliveryChargeVOs);

			List<SalesDeliveryPackVO> salesDeliveryPackVOs = new ArrayList<SalesDeliveryPackVO>();

			for (SalesDeliveryPack packs : salesDelivery
					.getSalesDeliveryPacks()) {
				SalesDeliveryPackVO deliveryPackVO = new SalesDeliveryPackVO(
						packs);
				deliveryPackVO
						.setUnit(packs.getLookupDetail().getDisplayName());

				salesDeliveryPackVOs.add(deliveryPackVO);
			}
			deliveryNoteVO.setSalesDeliveryPackVOs(salesDeliveryPackVOs);

			salesDeliveryNoteVOs.add(deliveryNoteVO);
		}

		return salesDeliveryNoteVOs;
	}

	public String getSalesDeliveryReportPrintOut() {
		try {
			List<SalesDeliveryNoteVO> salesDeliveryList = this
					.fetchSalesDeliveryReportData();
			for (SalesDeliveryNoteVO salesDeliveryNoteVO : salesDeliveryList) {
				Collections.sort(
						salesDeliveryNoteVO.getSalesDeliveryDetailVOs(),
						new Comparator<SalesDeliveryDetailVO>() {
							public int compare(SalesDeliveryDetailVO s1,
									SalesDeliveryDetailVO s2) {
								return s1.getSalesDeliveryDetailId().compareTo(
										s2.getSalesDeliveryDetailId());
							}
						});
			}
			ServletActionContext.getRequest().setAttribute("SALES_DELIVERY",
					salesDeliveryList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getSalesDeliveryReportXLS() {
		try {
			List<SalesDeliveryNoteVO> salesDeliveryList = this
					.fetchSalesDeliveryReportData();
			String str = "";

			for (SalesDeliveryNoteVO salesDelivery : salesDeliveryList) {
				str += "\n Customer Name,"
						+ salesDelivery.getCustomerNameAndNo();
				str += "\n\n Reference Number, Date, Shipping Method, Shipping Terms, Status \n";

				str += salesDelivery.getReferenceNumber()
						+ ","
						+ salesDelivery.getDispatchDate()
						+ ","
						+ salesDelivery.getShippingMethod()
						+ ","
						+ (null != salesDelivery.getShippingTerm() ? salesDelivery
								.getShippingTerm() : "") + ","
						+ salesDelivery.getStrStatus();
				str += "\n";

				str += "\n\n Product Code, Product, Store Name, Quantity, Unit Price, Discount Mode, Discount, Total \n";
				for (SalesDeliveryDetailVO detail : salesDelivery
						.getSalesDeliveryDetailVOs()) {
					str += detail.getProductCode()
							+ ","
							+ detail.getProductName()
							+ ","
							+ detail.getStoreName()
							+ ","
							+ detail.getDeliveryQuantity()
							+ ","
							+ detail.getUnitRate()
							+ ","
							+ (null != detail.getStrDiscountMode() ? detail
									.getStrDiscountMode() : "")
							+ ","
							+ (null != detail.getDiscount() ? detail
									.getDiscount() : "") + ","
							+ detail.getTotal();
					str += "\n";
				}

				if (null != salesDelivery.getSalesDeliveryChargeVOs()
						&& salesDelivery.getSalesDeliveryChargeVOs().size() > 0) {
					str += "\n\n Charges Type, Mode, Charges \n";
					for (SalesDeliveryChargeVO charges : salesDelivery
							.getSalesDeliveryChargeVOs()) {
						str += charges.getStrChargeType() + ","
								+ charges.getStrMode() + ","
								+ charges.getCharges();
						str += "\n";
					}
				}

				if (null != salesDelivery.getSalesDeliveryChargeVOs()
						&& salesDelivery.getSalesDeliveryChargeVOs().size() > 0) {
					str += "\n\n Reference Number, Unit Name, Total Packs, Gross Weight, Contact Person \n";
					for (SalesDeliveryPackVO packs : salesDelivery
							.getSalesDeliveryPackVOs()) {
						str += packs.getReferenceNumber() + ","
								+ packs.getUnit() + "," + packs.getNoOfPacks()
								+ "," + packs.getGrossWeight() + ","
								+ packs.getDispatcher();
						str += "\n";
					}
				}
			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getSalesDeliveryPDF() {
		String result = ERROR;
		try {
			salesDeliveryDS = this.fetchSalesDeliveryReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSalesInvoiceCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"customerList",
					accountsEnterpriseService.getCustomerService()
							.getFilteredCustomers(implementation, null, null,
									null, null, null));
			ServletActionContext.getRequest().setAttribute("statusList",
					AIOSCommons.getO2CProcessStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getSalesInvoiceJsonList() {
		try {

			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesInvoiceReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", salesInvoiceList.size());
			jsonResponse.put("iTotalDisplayRecords", salesInvoiceList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (SalesInvoiceVO list : salesInvoiceList) {
				array = new JSONArray();
				array.add(list.getSalesInvoiceId());
				array.add(list.getReferenceNumber());
				array.add(list.getSalesInvoiceDate());
				array.add(list.getInvoiceTypeName());
				array.add(list.getCustomerName());
				array.add(list.getCreditTermName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getSalesProfitLossJsonList() {
		try {

			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesProfitLossReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", salesInvoiceList.size());
			jsonResponse.put("iTotalDisplayRecords", salesInvoiceList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (SalesInvoiceVO list : salesInvoiceList) {
				array = new JSONArray();
				array.add(list.getSalesInvoiceId());
				array.add(list.getReferenceNumber());
				array.add(list.getSalesInvoiceDate());
				array.add(list.getInvoiceTypeName());
				array.add(list.getCustomerName());
				array.add(list.getCreditTermName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getSalesProfitLossReportPrintOut() {
		try {
			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesProfitLossReportData();
			if (null != salesInvoiceList && salesInvoiceList.size() > 0) {
				for (SalesInvoiceVO salesInvoiceVO : salesInvoiceList) {
					Collections.sort(salesInvoiceVO.getSalesInvoiceDetailVOs(),
							new Comparator<SalesInvoiceDetailVO>() {
								public int compare(SalesInvoiceDetailVO s1,
										SalesInvoiceDetailVO s2) {
									return s1
											.getSalesInvoiceDetailId()
											.compareTo(
													s2.getSalesInvoiceDetailId());
								}
							});
				}
			}
			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoiceList);
			ServletActionContext.getRequest().setAttribute(
					"PRINT_DATE",
					DateFormat.convertDateToString(DateTime.now().toLocalDate()
							.toString()));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getSalesProfitLossReportXLS() {
		try {
			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesProfitLossReportData();
			String str = "";
			Double netTotal = 0.0;
			Double netCost = 0.0;
			Double netGain = 0.0;

			str += "Date, Sale Ref., Customer, Product Code, Product, Quantity, Unit Price, Discount, Discount Type, Total, Cost, Balance \n";

			for (SalesInvoiceVO salesInvoice : salesInvoiceList) {

				for (SalesInvoiceDetailVO detail : salesInvoice
						.getSalesInvoiceDetailVOs()) {

					str += salesInvoice.getSalesInvoiceDate()
							+ ","
							+ salesInvoice.getReferenceNumber()
							+ ","
							+ detail.getCustomerName()
							+ ","
							+ detail.getProductCode()
							+ ","
							+ detail.getProductName()
							+ ","
							+ detail.getInvoiceQuantity()
							+ ","
							+ detail.getSalesDeliveryDetail().getUnitRate()
							+ ","
							+ ((detail.getSalesDeliveryDetail().getDiscount() == null) ? ""
									: detail.getSalesDeliveryDetail()
											.getDiscount().toString())
							+ ","
							+ ((detail.getSalesDeliveryDetail()
									.getIsPercentage() == null) ? ""
									: (detail.getSalesDeliveryDetail()
											.getIsPercentage()) ? "PERCENTAGE"
											: "AMOUNT") + ","
							+ detail.getTotal() + ","
							+ detail.getProductCostPrice() + ","
							+ detail.getSaleGain();

					netTotal += detail.getTotal();
					netCost += detail.getProductCostPrice();
					netGain += detail.getSaleGain();

					str += "\n";
				}
			}

			str += ",,,,,,,,Total Sale: " + AIOSCommons.roundDecimals(netTotal)
					+ ", Net Cost: " + AIOSCommons.roundDecimals(netCost)
					+ ", Net Gain: " + AIOSCommons.roundDecimals(netGain);

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method getSalesProfitLossReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public List<SalesInvoiceVO> fetchSalesProfitLossReportData()
			throws Exception {

		Long salesInvoiceId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;
		Double totalSalesAmount = 0.0;
		Double totalCost = 0.0;
		Double qty = 0.0;
		Double totalDiscount = 0.0;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("salesInvoiceId") != null) {
			salesInvoiceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesInvoiceId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		} else {
			fromDatee = java.util.Calendar.getInstance().getTime();
			toDatee = java.util.Calendar.getInstance().getTime();
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<SalesInvoice> salesInvoiceList = accountsEnterpriseService
				.getSalesInvoiceService().getFilteredSalesInvoice(
						implementation, salesInvoiceId, customerId, statusId,
						fromDatee, toDatee);

		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();
		HashMap<String, List<SalesInvoiceDetailVO>> salesMap = new HashMap<String, List<SalesInvoiceDetailVO>>();

		for (SalesInvoice salesInvoice : salesInvoiceList) {
			SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO(salesInvoice);
			salesInvoiceVO.setSalesInvoiceDate(DateFormat
					.convertDateToString(salesInvoice.getInvoiceDate()
							.toString()));
			salesInvoiceVO.setStrCurrency(salesInvoice.getCurrency()
					.getCurrencyPool().getCode());

			if (salesInvoice.getCustomer() != null
					&& salesInvoice.getCustomer().getShippingDetails() != null
					&& salesInvoice.getCustomer().getShippingDetails().size() > 0) {
				salesInvoiceVO.setShippingAddress(salesInvoice.getCustomer()
						.getShippingDetails().iterator().next().getAddress());
			}

			if (salesInvoice.getCreditTerm() != null) {
				salesInvoiceVO.setCreditTermName(salesInvoice.getCreditTerm()
						.getName());
			} else {
				salesInvoiceVO.setCreditTermName("");
			}
			salesInvoiceVO.setStatusName(Constants.Accounts.O2CProcessStatus
					.get(salesInvoice.getStatus()).toString());
			salesInvoiceVO.setInvoiceTypeName(SalesInvoiceType.get(
					salesInvoice.getInvoiceType()).name());

			salesInvoiceVO.setCustomerName(null != salesInvoice.getCustomer()
					.getPersonByPersonId() ? salesInvoice
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesInvoice.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesInvoice
					.getCustomer().getCompany() ? salesInvoice.getCustomer()
					.getCompany().getCompanyName() : salesInvoice.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			salesInvoiceVO.setCustomerNameAndNo(salesInvoiceVO
					.getCustomerName()
					+ " ["
					+ salesInvoice.getCustomer().getCustomerNumber() + "]");

			List<SalesInvoiceDetailVO> salesInvoiceDetailVOs = new ArrayList<SalesInvoiceDetailVO>();
			ProductPricingDetail pricingDetail = null;
			Double discount = 0.0;

			for (SalesInvoiceDetail detail : salesInvoice
					.getSalesInvoiceDetails()) {

				SalesInvoiceDetailVO salesInvoiceDetailVO = new SalesInvoiceDetailVO(
						detail);

				salesInvoiceDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesInvoiceDetailVO.setProductCode(detail.getProduct()
						.getCode());
				salesInvoiceDetailVO.setCustomerName(salesInvoiceVO
						.getCustomerName());

				if (null != detail.getSalesDeliveryDetail().getDiscount())
					discount = detail.getSalesDeliveryDetail().getDiscount();
				else
					discount = 0.0;

				totalDiscount += discount;

				salesInvoiceDetailVO.setDiscountValue(discount
						* detail.getInvoiceQuantity());

				if (detail.getSalesDeliveryDetail().getIsPercentage() != null)
					salesInvoiceDetailVO
							.setDiscountType(detail.getSalesDeliveryDetail()
									.getIsPercentage() ? ("PERCENTAGE")
									: "(AMOUNT)");
				else
					salesInvoiceDetailVO.setDiscountType("-N/A-");

				salesInvoiceDetailVO.setTotal(AIOSCommons.roundDecimals(detail
						.getInvoiceQuantity() * detail.getUnitRate()));

				try {
					pricingDetail = accountsEnterpriseService
							.getProductPricingService()
							.getPricingDetailByProductId(
									detail.getProduct().getProductId());

					salesInvoiceDetailVO.setProductCostPrice(AIOSCommons
							.roundDecimals(pricingDetail.getBasicCostPrice()
									* detail.getInvoiceQuantity()));

				} catch (Exception e) {
					e.printStackTrace();
					salesInvoiceDetailVO.setProductCostPrice(0.0);
				}

				totalCost += salesInvoiceDetailVO.getProductCostPrice();
				totalSalesAmount += (null == salesInvoiceDetailVO.getTotal()) ? 0.0
						: salesInvoiceDetailVO.getTotal();
				qty += detail.getInvoiceQuantity();

				Double temp = ((null == salesInvoiceDetailVO.getTotal()) ? 0.0
						: salesInvoiceDetailVO.getTotal())
						- salesInvoiceDetailVO.getProductCostPrice();

				salesInvoiceDetailVO.setSaleGain(AIOSCommons
						.roundDecimals(Double.parseDouble(null != temp ? temp
								.toString() : "0.0")));

				salesInvoiceDetailVOs.add(salesInvoiceDetailVO);
			}

			salesInvoiceVO.setSalesInvoiceDetailVOs(salesInvoiceDetailVOs);
			salesInvoiceVOs.add(salesInvoiceVO);
		}

		List<SalesInvoiceDetailVO> forMap = new ArrayList<SalesInvoiceDetailVO>();

		for (SalesInvoiceVO invoiceVO : salesInvoiceVOs) {

			if (salesMap.get(invoiceVO.getSalesInvoiceDate()) == null) {

				forMap = new ArrayList<SalesInvoiceDetailVO>();
				forMap.addAll(invoiceVO.getSalesInvoiceDetailVOs());

				salesMap.put(invoiceVO.getSalesInvoiceDate(), forMap);
			} else {

				forMap = salesMap.get(invoiceVO.getSalesInvoiceDate());
				forMap.addAll(invoiceVO.getSalesInvoiceDetailVOs());

				salesMap.put(invoiceVO.getSalesInvoiceDate(), forMap);
			}

		}

		ServletActionContext.getRequest().setAttribute("SALE_MAP", salesMap);
		ServletActionContext.getRequest().setAttribute("TOTAL_SALES_AMOUNT",
				AIOSCommons.roundDecimals(totalSalesAmount));
		ServletActionContext.getRequest().setAttribute("TOTAL_COST_AMOUNT",
				AIOSCommons.roundDecimals(totalCost));
		ServletActionContext.getRequest().setAttribute(
				"TOTAL_GAIN_AMOUNT",
				AIOSCommons.formatAmountIncludingNegative(totalSalesAmount
						- totalCost));
		ServletActionContext.getRequest().setAttribute("TOTAL_QTY", qty);
		ServletActionContext.getRequest().setAttribute("TOTAL_DISCOUNT",
				AIOSCommons.roundDecimals(totalDiscount));

		return salesInvoiceVOs;
	}

	public List<SalesInvoiceVO> fetchSalesInvoiceReportData() throws Exception {

		Long salesInvoiceId = 0L;
		Long customerId = 0L;
		Byte statusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("salesInvoiceId") != null) {
			salesInvoiceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("salesInvoiceId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<SalesInvoice> salesInvoiceList = accountsEnterpriseService
				.getSalesInvoiceService().getFilteredSalesInvoice(
						implementation, salesInvoiceId, customerId, statusId,
						fromDatee, toDatee);

		List<SalesInvoiceVO> salesInvoiceVOs = new ArrayList<SalesInvoiceVO>();

		for (SalesInvoice salesInvoice : salesInvoiceList) {
			SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO(salesInvoice);
			salesInvoiceVO.setSalesInvoiceDate(DateFormat
					.convertDateToString(salesInvoice.getInvoiceDate()
							.toString()));
			salesInvoiceVO.setStrCurrency(salesInvoice.getCurrency()
					.getCurrencyPool().getCode());

			if (salesInvoice.getCustomer() != null
					&& salesInvoice.getCustomer().getShippingDetails() != null
					&& salesInvoice.getCustomer().getShippingDetails().size() > 0) {
				salesInvoiceVO.setShippingAddress(salesInvoice.getCustomer()
						.getShippingDetails().iterator().next().getAddress());
			}

			if (salesInvoice.getCreditTerm() != null) {
				salesInvoiceVO.setCreditTermName(salesInvoice.getCreditTerm()
						.getName());
			} else {
				salesInvoiceVO.setCreditTermName("");
			}
			salesInvoiceVO.setStatusName(Constants.Accounts.O2CProcessStatus
					.get(salesInvoice.getStatus()).toString());
			salesInvoiceVO.setInvoiceTypeName(SalesInvoiceType.get(
					salesInvoice.getInvoiceType()).name());

			salesInvoiceVO.setCustomerName(null != salesInvoice.getCustomer()
					.getPersonByPersonId() ? salesInvoice
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesInvoice.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesInvoice
					.getCustomer().getCompany() ? salesInvoice.getCustomer()
					.getCompany().getCompanyName() : salesInvoice.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			salesInvoiceVO.setCustomerNameAndNo(salesInvoiceVO
					.getCustomerName()
					+ " ["
					+ salesInvoice.getCustomer().getCustomerNumber() + "]");

			List<SalesInvoiceDetailVO> salesInvoiceDetailVOs = new ArrayList<SalesInvoiceDetailVO>();
			if (null == salesInvoice.getSalesInvoiceDetails()
					|| salesInvoice.getSalesInvoiceDetails().size() == 0) {
				salesInvoice.setSalesInvoiceDetails(salesInvoice
						.getSalesInvoice().getSalesInvoiceDetails());
			}
			for (SalesInvoiceDetail detail : salesInvoice
					.getSalesInvoiceDetails()) {

				SalesInvoiceDetailVO salesInvoiceDetailVO = new SalesInvoiceDetailVO(
						detail);
				salesInvoiceDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesInvoiceDetailVO.setProductCode(detail.getProduct()
						.getCode());

				salesInvoiceDetailVO
						.setTotal((detail.getInvoiceQuantity() * detail
								.getUnitRate()));
				salesInvoiceDetailVOs.add(salesInvoiceDetailVO);
			}

			salesInvoiceVO.setSalesInvoiceDetailVOs(salesInvoiceDetailVOs);
			salesInvoiceVOs.add(salesInvoiceVO);

		}

		return salesInvoiceVOs;
	}

	public String getSalesInvoiceDirectPrint() {
		try {
			long salesInvoiceId = 0;
			if (ServletActionContext.getRequest()
					.getParameter("salesInvoiceId") != null) {
				salesInvoiceId = Long.parseLong(ServletActionContext
						.getRequest().getParameter("salesInvoiceId"));
			}
			SalesInvoice salesInvoice = accountsBL.getSalesInvoiceBL()
					.getSalesInvoiceService()
					.getSalesInvoiceById(salesInvoiceId);
			List<SalesInvoiceDetail> salesInvoiceDetails = null;
			if (null != salesInvoice.getSalesInvoiceDetails()
					&& salesInvoice.getSalesInvoiceDetails().size() > 0)
				salesInvoiceDetails = new ArrayList<SalesInvoiceDetail>(
						salesInvoice.getSalesInvoiceDetails());
			else
				salesInvoiceDetails = new ArrayList<SalesInvoiceDetail>(
						salesInvoice.getSalesInvoice().getSalesInvoiceDetails());
			int shelfId = 0;
			for (SalesInvoiceDetail invoiceDetail : salesInvoiceDetails) {
				if (null != invoiceDetail.getSalesDeliveryDetail().getShelf()) {
					shelfId = invoiceDetail.getSalesDeliveryDetail().getShelf()
							.getShelfId();
					break;
				}
			}
			format = createSalesInvoiceTemplate(accountsBL.getSalesInvoiceBL()
					.convertEntityToVO(salesInvoice), shelfId,
					salesInvoiceDetails);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	private String createSalesInvoiceTemplate(SalesInvoiceVO salesInvoiceVO,
			int shelfId, List<SalesInvoiceDetail> salesInvoiceDetails) {
		try {
			getImplementId();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configuration config = new Configuration();
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ implementation.getImplementationId() + "/o2c"));

			Template template = config.getTemplate("sales-invoice.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();
			Shelf shelf = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStoreCompanyDetailsByShelfId(shelfId);
			format = "";
			rootMap.put("companyName", shelf.getShelf().getAisle().getStore()
					.getCmpDeptLocation().getCompany().getCompanyName());
			rootMap.put("telephone", null != shelf.getShelf().getAisle()
					.getStore().getCmpDeptLocation().getCompany()
					.getCompanyPhone() ? shelf.getShelf().getAisle().getStore()
					.getCmpDeptLocation().getCompany().getCompanyPhone() : "");
			rootMap.put("companyAddress", null != shelf.getShelf().getAisle()
					.getStore().getCmpDeptLocation().getCompany()
					.getCompanyAddress() ? shelf.getShelf().getAisle()
					.getStore().getCmpDeptLocation().getCompany()
					.getCompanyAddress() : "");
			rootMap.put("customerName", salesInvoiceVO.getCustomerName());
			rootMap.put("referenceNumber", salesInvoiceVO.getReferenceNumber());
			rootMap.put("invoiceDate", salesInvoiceVO.getSalesDate());
			rootMap.put("currency", salesInvoiceVO.getCurrency()
					.getCurrencyPool().getCode());
			rootMap.put("invoiceAmount",
					AIOSCommons.formatAmount(salesInvoiceVO.getInvoiceAmount()));
			rootMap.put("description", salesInvoiceVO.getDescription());
			rootMap.put("customerMobile", salesInvoiceVO.getCustomerMobile());
			rootMap.put("customerAddress", salesInvoiceVO.getCustomerAddress());
			rootMap.put("shippingAddress", salesInvoiceVO.getShippingAddress());
			rootMap.put("creditTerm", salesInvoiceVO.getCreditTermName());
			rootMap.put(
					"status",
					Constants.Accounts.O2CProcessStatus.get(
							salesInvoiceVO.getStatus()).toString());
			rootMap.put("totalInvoice",
					AIOSCommons.formatAmount(salesInvoiceVO.getTotalInvoice()));
			rootMap.put(
					"paidInvoice",
					null != salesInvoiceVO.getPaidInvoice()
							&& salesInvoiceVO.getPaidInvoice() > 0 ? AIOSCommons
							.formatAmount(salesInvoiceVO.getPaidInvoice())
							: null);
			rootMap.put(
					"pendingInvoice",
					null != salesInvoiceVO.getPendingInvoice()
							&& salesInvoiceVO.getPendingInvoice() > 0 ? AIOSCommons
							.formatAmount(salesInvoiceVO.getPendingInvoice())
							: null);
			rootMap.put(
					"accountant",
					null != salesInvoiceVO.getPerson() ? salesInvoiceVO
							.getPerson()
							.getFirstName()
							.concat(" "
									+ salesInvoiceVO.getPerson().getLastName())
							: "");
			SalesInvoiceDetailVO salesInvoiceDetailVO = null;
			double totalAmount = 0;
			List<SalesInvoiceDetailVO> salesInvoiceDetailVOs = new ArrayList<SalesInvoiceDetailVO>();
			for (SalesInvoiceDetail detail : salesInvoiceDetails) {
				salesInvoiceDetailVO = new SalesInvoiceDetailVO();
				salesInvoiceDetailVO.setProductName(detail.getProduct()
						.getProductName());
				salesInvoiceDetailVO.setProductCode(detail.getProduct()
						.getCode());
				salesInvoiceDetailVO.setUnitRate(detail.getUnitRate());
				salesInvoiceDetailVO.setInvoiceQuantity(detail
						.getInvoiceQuantity());
				salesInvoiceDetailVO
						.setTotal((detail.getInvoiceQuantity() * detail
								.getUnitRate()));
				salesInvoiceDetailVO.setUnitCostPrice(AIOSCommons
						.formatAmount(detail.getUnitRate()));
				salesInvoiceDetailVO.setTotalCostPrice(AIOSCommons
						.formatAmount(salesInvoiceDetailVO.getTotal()));
				totalAmount += salesInvoiceDetailVO.getTotal();
				salesInvoiceDetailVOs.add(salesInvoiceDetailVO);
			}
			rootMap.put("invoiceDetails", salesInvoiceDetailVOs);
			rootMap.put("totalAmount", AIOSCommons.formatAmount(totalAmount));
			Writer out = new StringWriter();
			template.process(rootMap, out);
			format = out.toString();
			return format;
		} catch (Exception ex) {
			ex.printStackTrace();
			;
			return null;
		}
	}

	public String getSalesInvoiceReportPrintOut() {
		try {
			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesInvoiceReportData();
			if (null != salesInvoiceList && salesInvoiceList.size() > 0) {
				for (SalesInvoiceVO salesInvoiceVO : salesInvoiceList) {
					Collections.sort(salesInvoiceVO.getSalesInvoiceDetailVOs(),
							new Comparator<SalesInvoiceDetailVO>() {
								public int compare(SalesInvoiceDetailVO s1,
										SalesInvoiceDetailVO s2) {
									return s1
											.getSalesInvoiceDetailId()
											.compareTo(
													s2.getSalesInvoiceDetailId());
								}
							});
				}
			}
			ServletActionContext.getRequest().setAttribute("SALES_INVOICE",
					salesInvoiceList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getSalesInvoiceReportXLS() {
		try {
			List<SalesInvoiceVO> salesInvoiceList = this
					.fetchSalesInvoiceReportData();
			String str = "";

			for (SalesInvoiceVO salesInvoice : salesInvoiceList) {
				str += "\n Customer Name,"
						+ salesInvoice.getCustomerNameAndNo();
				str += "\n\n Reference Number, Invoice Date, Currency, Shipping Address, Credit Term, Status \n";

				str += salesInvoice.getReferenceNumber() + ","
						+ salesInvoice.getSalesInvoiceDate() + ","
						+ salesInvoice.getStrCurrency() + ","
						+ salesInvoice.getShippingAddress() + ","
						+ salesInvoice.getCreditTermName() + ","
						+ salesInvoice.getStatusName();
				str += "\n";

				str += "\n\n Product Code, Product, Quantity, Unit Price, Total \n";
				for (SalesInvoiceDetailVO detail : salesInvoice
						.getSalesInvoiceDetailVOs()) {
					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getInvoiceQuantity() + ","
							+ detail.getUnitRate() + "," + detail.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getSalesInvoicePDF() {
		String result = ERROR;
		try {
			salesInvoiceDS = this.fetchSalesInvoiceReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getCreditNoteCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("customerList",
					accountsBL.getCustomerBL().getAllCustomers(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getCreditNoteJsonList() {
		try {

			List<CreditNoteVO> creditList = this.fetchCreditNoteReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", creditList.size());
			jsonResponse.put("iTotalDisplayRecords", creditList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (CreditNoteVO list : creditList) {
				array = new JSONArray();
				array.add(list.getCreditId());
				array.add(list.getCreditNumber());
				array.add(list.getCreditDate());
				array.add(list.getCustomerReference());
				array.add(list.getCustomerName());
				array.add(list.getDescription());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<CreditNoteVO> fetchCreditNoteReportData() throws Exception {

		Long creditId = 0L;
		Long customerId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("creditId") != null) {
			creditId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("creditId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Credit> creditList = accountsEnterpriseService.getCreditService()
				.getFilteredCreditNotes(implementation, creditId, customerId,
						fromDatee, toDatee);

		List<CreditNoteVO> creditNoteVOs = new ArrayList<CreditNoteVO>();
		SalesDeliveryNote salesDeliveryNote = null;
		for (Credit credit : creditList) {
			if (null != credit.getUseCase()
					&& credit.getUseCase().equalsIgnoreCase(
							SalesDeliveryNote.class.getSimpleName())) {
				salesDeliveryNote = accountsEnterpriseService
						.getSalesDeliveryNoteService()
						.getSalesDeliveryNoteById(credit.getRecordId());
			}
			CreditNoteVO creditNoteVO = new CreditNoteVO(credit);
			creditNoteVO.setCreditDate(DateFormat.convertDateToString(credit
					.getDate().toString()));
			creditNoteVO.setCurrencyName(credit.getCurrency().getCurrencyPool()
					.getCode());
			creditNoteVO
					.setSalesReference(null != salesDeliveryNote ? salesDeliveryNote
							.getReferenceNumber() : "");
			creditNoteVO.setCustomerName(null != credit.getCustomer()
					.getPersonByPersonId() ? credit
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(credit.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != credit.getCustomer()
					.getCompany() ? credit.getCustomer().getCompany()
					.getCompanyName() : credit.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			creditNoteVO.setCustomerNameAndNo(creditNoteVO.getCustomerName()
					+ " [" + credit.getCustomer().getCustomerNumber() + "]");
			creditNoteVO.setCustomerReference(credit.getCustomer()
					.getCustomerNumber());

			List<CreditDetailVO> creditDetailVOs = new ArrayList<CreditDetailVO>();
			for (CreditDetail detail : credit.getCreditDetails()) {

				CreditDetailVO creditDetailVO = new CreditDetailVO(detail);
				creditDetailVO.setProductName(detail.getSalesDeliveryDetail()
						.getProduct().getProductName());
				creditDetailVO.setProductCode(detail.getSalesDeliveryDetail()
						.getProduct().getCode());
				creditDetailVO.setDeliveryQuantity(detail
						.getSalesDeliveryDetail().getDeliveryQuantity());
				creditDetailVO.setInvoiceQuantity(0.0);
				for (SalesInvoiceDetail invoice : detail
						.getSalesDeliveryDetail().getSalesInvoiceDetails()) {
					creditDetailVO.setInvoiceQuantity(creditDetailVO
							.getInvoiceQuantity()
							+ invoice.getInvoiceQuantity());
				}
				creditDetailVO.setUnitRate(detail.getSalesDeliveryDetail()
						.getUnitRate());
				creditDetailVO.setTotal(detail.getReturnQuantity()
						* detail.getSalesDeliveryDetail().getUnitRate());

				if (detail.getSalesDeliveryDetail().getDiscount() != null) {
					creditDetailVO.setTotal(creditDetailVO.getTotal()
							- detail.getSalesDeliveryDetail().getDiscount());
				}
				creditDetailVOs.add(creditDetailVO);
			}

			creditNoteVO.setCreditDetailVOs(creditDetailVOs);
			creditNoteVOs.add(creditNoteVO);
		}

		return creditNoteVOs;
	}

	public String getCreditNoteReportPrintOut() {
		try {
			List<CreditNoteVO> creditList = this.fetchCreditNoteReportData();

			ServletActionContext.getRequest().setAttribute("CREDIT_NOTE",
					creditList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getCreditNoteReportXLS() {
		try {
			List<CreditNoteVO> creditList = this.fetchCreditNoteReportData();
			String str = "";

			for (CreditNoteVO credit : creditList) {
				str += "\n Customer Name," + credit.getCustomerNameAndNo();
				str += "\n\n Reference Number, Date, Currency \n";

				str += credit.getCreditNumber() + "," + credit.getCreditDate()
						+ "," + credit.getCurrencyName();

				str += "\n";

				str += "\n\n Product Code, Product, Delivery Quantity, Invoice Quantity, Return Quantity, Unit Price, Total \n";
				for (CreditDetailVO detail : credit.getCreditDetailVOs()) {
					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getDeliveryQuantity() + ","
							+ detail.getInvoiceQuantity() + ","
							+ detail.getReturnQuantity() + ","
							+ detail.getUnitRate() + "," + detail.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getCreditNotePDF() {
		String result = ERROR;
		try {
			creditNoteDS = this.fetchCreditNoteReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getBankReceiptCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("RECEIPT_SOURCE",
					AIOSCommons.getReceiptSource());
			ServletActionContext.getRequest().setAttribute(
					"CURRENCY_LIST",
					accountsBL.getTransactionBL().getCurrencyService()
							.getAllCurrency(implementation));
			ServletActionContext.getRequest().setAttribute(
					"RECEIPT_TYPES",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"RECEIPT_TYPES", true));
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					AIOSCommons.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYMENT_STATUS",
					AIOSCommons.getPaymentStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getBankReceiptJsonList() {
		try {

			List<BankReceiptsVO> bankReceiptsList = this
					.fetchBankReceiptsReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", bankReceiptsList.size());
			jsonResponse.put("iTotalDisplayRecords", bankReceiptsList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (BankReceiptsVO list : bankReceiptsList) {
				array = new JSONArray();
				array.add(list.getBankReceiptsId());
				array.add(list.getReceiptsNo());
				array.add(list.getReceiptType());
				array.add(list.getCurrencyName());
				array.add(list.getStrReceiptsDate());
				array.add(list.getReferenceNo());
				array.add(list.getReceiptSourceName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<BankReceiptsVO> fetchBankReceiptsReportData() throws Exception {

		Long selectedReceiptId = 0L;
		Long selectedSource = 0L;
		Integer selectedCurrencyId = 0;
		Long selectedReceiptTypeId = 0L;
		Byte selectedModeId = 0;
		Byte selectedStatusId = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("selectedReceiptId") != null) {
			selectedReceiptId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("selectedReceiptId"));
		}

		if (ServletActionContext.getRequest().getParameter("selectedSource") != null) {
			selectedSource = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("selectedSource"));
		}

		if (ServletActionContext.getRequest()
				.getParameter("selectedCurrencyId") != null) {
			selectedCurrencyId = Integer.parseInt(ServletActionContext
					.getRequest().getParameter("selectedCurrencyId"));
		}

		if (ServletActionContext.getRequest().getParameter(
				"selectedReceiptTypeId") != null) {
			selectedReceiptTypeId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("selectedReceiptTypeId"));
		}

		if (ServletActionContext.getRequest().getParameter("selectedModeId") != null) {
			selectedModeId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("selectedModeId"));
		}

		if (ServletActionContext.getRequest().getParameter("selectedStatusId") != null) {
			selectedStatusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("selectedStatusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<BankReceipts> receiptList = accountsEnterpriseService
				.getBankReceiptsService().getFilteredBankReceipts(
						implementation, selectedReceiptId, selectedSource,
						selectedCurrencyId, selectedReceiptTypeId,
						selectedModeId, selectedStatusId, fromDatee, toDatee);

		List<BankReceiptsVO> receiptsVOs = new ArrayList<BankReceiptsVO>();
		double totalReceiptAmount = 0;
		for (BankReceipts receipt : receiptList) {
			totalReceiptAmount = 0;
			BankReceiptsVO bankReceiptsVO = new BankReceiptsVO(receipt);
			bankReceiptsVO.setStrReceiptsDate(DateFormat
					.convertDateToString(receipt.getReceiptsDate().toString()));
			bankReceiptsVO.setCurrencyName(receipt.getCurrency()
					.getCurrencyPool().getCode());
			bankReceiptsVO.setReceiptType(receipt.getLookupDetail()
					.getDisplayName());
			bankReceiptsVO.setDescription(receipt.getDescription());
			if (receipt.getSupplier() != null) {
				if (receipt.getSupplier().getPerson() != null)
					bankReceiptsVO.setReceiptSourceName(receipt
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(receipt.getSupplier().getPerson()
									.getLastName()));
				else
					bankReceiptsVO.setReceiptSourceName(receipt.getSupplier()
							.getCompany() != null ? receipt.getSupplier()
							.getCompany().getCompanyName() : receipt
							.getSupplier().getCmpDeptLocation().getCompany()
							.getCompanyName());

				bankReceiptsVO.setReceiptSourceType("Supplier");
			} else if (receipt.getCustomer() != null) {
				if (receipt.getCustomer().getPersonByPersonId() != null)
					bankReceiptsVO.setReceiptSourceName(receipt
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat(" ")
							.concat(receipt.getCustomer().getPersonByPersonId()
									.getLastName()));
				else
					bankReceiptsVO.setReceiptSourceName(receipt.getCustomer()
							.getCompany() != null ? receipt.getCustomer()
							.getCompany().getCompanyName() : receipt
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName());

				bankReceiptsVO.setReceiptSourceType("Customer");
			} else if (receipt.getPersonByPersonId() != null) {
				bankReceiptsVO.setReceiptSourceName(receipt
						.getPersonByPersonId().getFirstName().concat(" ")
						.concat(receipt.getPersonByPersonId().getLastName()));
				bankReceiptsVO.setReceiptSourceType("Employee");
			} else {
				bankReceiptsVO.setReceiptSourceName(receipt.getOthers());
				bankReceiptsVO.setReceiptSourceType("Others");
			}

			List<BankReceiptsDetailVO> receiptsDetailVOs = new ArrayList<BankReceiptsDetailVO>();
			for (BankReceiptsDetail detail : receipt.getBankReceiptsDetails()) {
				totalReceiptAmount += detail.getAmount();
				BankReceiptsDetailVO receiptsDetailVO = new BankReceiptsDetailVO(
						detail);
				receiptsDetailVO.setPaymentModeName(ModeOfPayment
						.get(detail.getPaymentMode()).name()
						.replaceAll("_", " "));
				receiptsDetailVO.setPaymentStatusName(PaymentStatus
						.get(detail.getPaymentStatus()).name()
						.replaceAll("_", " "));
				if (detail.getChequeDate() != null) {
					receiptsDetailVO.setStrChequeDate(DateFormat
							.convertDateToString(detail.getChequeDate()
									.toString()));
				}

				if (detail.getLookupDetail() != null) {
					receiptsDetailVO.setReferenceNo(detail.getLookupDetail()
							.getDisplayName());
				} else {
					receiptsDetailVO.setReferenceNo("");
				}
				receiptsDetailVOs.add(receiptsDetailVO);
			}
			bankReceiptsVO.setReceiptAmount(AIOSCommons
					.formatAmount(totalReceiptAmount));
			bankReceiptsVO.setUseCase(receipt.getUseCase());
			bankReceiptsVO.setRecordId(receipt.getRecordId());
			bankReceiptsVO.setBankReceiptsDetailVOs(receiptsDetailVOs);
			receiptsVOs.add(bankReceiptsVO);
		}

		return receiptsVOs;
	}

	public String getBankReceiptDirectPrintOut() {
		try {
			List<BankReceiptsVO> bankReceipts = this
					.fetchBankReceiptsReportData();
			getImplementId();
			if (null != bankReceipts && bankReceipts.size() > 0) {
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				Configuration config = new Configuration();
				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/templates/implementation_"
						+ implementation.getImplementationId() + "/o2c"));
				Template template = config.getTemplate("receipts.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				format = "";
				BankReceiptsVO bankReceiptsVO = bankReceipts.get(0);
				rootMap.put("customerName",
						bankReceiptsVO.getReceiptSourceName());
				rootMap.put("receiptDate", bankReceiptsVO.getStrReceiptsDate());
				rootMap.put("receiptAmount", bankReceiptsVO.getReceiptAmount());
				rootMap.put("receiptNumber", bankReceiptsVO.getReceiptsNo());
				rootMap.put("referenceNo", bankReceiptsVO.getReferenceNo());
				rootMap.put("currency", bankReceiptsVO.getCurrencyName());
				rootMap.put("companyName", implementation.getCompanyName());
				rootMap.put("receipt", "RECEIPT");
				rootMap.put(
						"description",
						null != bankReceiptsVO.getDescription()
								&& !("").equals(bankReceiptsVO.getDescription()) ? bankReceiptsVO
								.getDescription() : null);
				rootMap.put(
						"accountant",
						null != bankReceiptsVO.getPersonByCreatedBy() ? bankReceiptsVO
								.getPersonByCreatedBy()
								.getFirstName()
								.concat(" "
										+ bankReceiptsVO.getPersonByCreatedBy()
												.getLastName())
								: "");
				rootMap.put("receiptList",
						bankReceiptsVO.getBankReceiptsDetailVOs());
				Writer out = new StringWriter();
				template.process(rootMap, out);
				format = out.toString();
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getBankReceiptDirectAcknoPrintOut() {
		try {
			List<BankReceiptsVO> bankReceipts = this
					.fetchBankReceiptsReportData();
			getImplementId();
			if (null != bankReceipts && bankReceipts.size() > 0) {
				final Properties confProps = (Properties) ServletActionContext
						.getServletContext().getAttribute("confProps");
				Configuration config = new Configuration();
				config.setDirectoryForTemplateLoading(new File(confProps
						.getProperty("file.drive")
						+ "aios/PrintTemplate/templates/implementation_"
						+ implementation.getImplementationId() + "/o2c"));
				Template template = config.getTemplate("receipts.ftl");
				Map<String, Object> rootMap = new HashMap<String, Object>();
				format = "";
				BankReceiptsVO bankReceiptsVO = bankReceipts.get(0);
				rootMap.put("customerName",
						bankReceiptsVO.getReceiptSourceName());
				rootMap.put("receiptDate", bankReceiptsVO.getStrReceiptsDate());
				rootMap.put("receiptAmount", bankReceiptsVO.getReceiptAmount());
				rootMap.put("receiptNumber", bankReceiptsVO.getReceiptsNo());
				rootMap.put("referenceNo", bankReceiptsVO.getReferenceNo());
				rootMap.put("currency", bankReceiptsVO.getCurrencyName());
				rootMap.put("companyName", implementation.getCompanyName());
				rootMap.put("receipt", "ACKNOWLEDGEMENT RECEIPT");
				rootMap.put(
						"description",
						null != bankReceiptsVO.getDescription()
								&& !("").equals(bankReceiptsVO.getDescription()) ? bankReceiptsVO
								.getDescription() : null);
				rootMap.put(
						"accountant",
						null != bankReceiptsVO.getPersonByCreatedBy() ? bankReceiptsVO
								.getPersonByCreatedBy()
								.getFirstName()
								.concat(" "
										+ bankReceiptsVO.getPersonByCreatedBy()
												.getLastName())
								: "");
				rootMap.put("receiptList",
						bankReceiptsVO.getBankReceiptsDetailVOs());
				Writer out = new StringWriter();
				template.process(rootMap, out);
				format = out.toString();
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getBankReceiptReportPrintOut() {
		try {
			List<BankReceiptsVO> bankReceiptsList = this
					.fetchBankReceiptsReportData();

			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPT",
					bankReceiptsList.size() > 0 ? bankReceiptsList.get(0)
							: null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getBankReceiptReportXLS() {
		try {
			List<BankReceiptsVO> bankReceiptsList = this
					.fetchBankReceiptsReportData();
			String str = "";

			for (BankReceiptsVO receipt : bankReceiptsList) {
				str += "\n Reciept Number, Date, Currency, Receipt Type, Receipt Source, Source \n\n ";

				str += receipt.getReceiptsNo() + ","
						+ receipt.getStrReceiptsDate() + ","
						+ receipt.getCurrencyName() + ","
						+ receipt.getReceiptType() + ","
						+ receipt.getReceiptSourceType() + ","
						+ receipt.getReceiptSourceName();

				str += "\n";

				str += "\n\n Payment Mode, Institution , Cheque Number, Cheque Date, Reference, Amount, Status \n";
				String reference = "";
				for (BankReceiptsDetailVO detail : receipt
						.getBankReceiptsDetailVOs()) {
					if (null != detail.getLookupDetail())
						reference = detail.getLookupDetail().getDisplayName();
					else
						reference = "";
					str += detail.getPaymentModeName() + ","
							+ detail.getInstitutionName() + ","
							+ detail.getBankRefNo() + ","
							+ detail.getStrChequeDate() + "," + reference + ","
							+ detail.getAmount() + ","
							+ detail.getPaymentStatusName();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getBankReceiptPDF() {
		String result = ERROR;
		try {
			bankReceiptsDS = this.fetchBankReceiptsReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getBankDepositsCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"CURRENCY_LIST",
					accountsBL.getTransactionBL().getCurrencyService()
							.getAllCurrency(implementation));
			ServletActionContext.getRequest().setAttribute(
					"RECEIPT_TYPES",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"RECEIPT_TYPES", true));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getBankDepositsJsonList() {
		try {

			List<BankDepositVO> bankDepositList = this
					.fetchBankDepositsReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", bankDepositList.size());
			jsonResponse.put("iTotalDisplayRecords", bankDepositList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (BankDepositVO list : bankDepositList) {
				array = new JSONArray();
				array.add(list.getBankDepositId());
				array.add(list.getBankDepositNumber());
				array.add(list.getRecordDate());
				array.add(list.getPersonByDepositerId().getFirstName()
						.concat(" ")
						.concat(list.getPersonByDepositerId().getLastName()));
				array.add(list.getDescription());
				array.add(list.getPersonByCreatedBy().getFirstName()
						.concat(" ")
						.concat(list.getPersonByCreatedBy().getLastName()));
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<BankDepositVO> fetchBankDepositsReportData() throws Exception {

		Long depositId = 0L;
		Integer selectedCurrencyId = 0;
		Long selectedReceiptTypeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("selectedDepositId") != null) {
			depositId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("selectedDepositId"));
		}

		if (ServletActionContext.getRequest()
				.getParameter("selectedCurrencyId") != null) {
			selectedCurrencyId = Integer.parseInt(ServletActionContext
					.getRequest().getParameter("selectedCurrencyId"));
		}

		if (ServletActionContext.getRequest().getParameter(
				"selectedReceiptTypeId") != null) {
			selectedReceiptTypeId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("selectedReceiptTypeId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<BankDeposit> depositList = accountsEnterpriseService
				.getBankDepositService().getFilteredBankDeposit(implementation,
						depositId, selectedCurrencyId, selectedReceiptTypeId,
						fromDatee, toDatee);

		List<BankDepositVO> bankDepositVOs = new ArrayList<BankDepositVO>();

		for (BankDeposit deposit : depositList) {
			BankDepositVO bankDepositVO = new BankDepositVO(deposit);
			bankDepositVO.setRecordDate(DateFormat.convertDateToString(deposit
					.getDate().toString()));
			bankDepositVO.setDepositor(deposit.getPersonByDepositerId()
					.getFirstName().concat(" ")
					.concat(deposit.getPersonByDepositerId().getLastName()));
			bankDepositVO.setCreatedByPerson(deposit.getPersonByCreatedBy()
					.getFirstName().concat(" ")
					.concat(deposit.getPersonByCreatedBy().getLastName()));
			bankDepositVO.setBankName(deposit.getBankAccount().getBank()
					.getBankName());
			bankDepositVO.setBankAccountNumber(deposit.getBankAccount()
					.getAccountNumber());
			List<BankDepositDetailVO> bankDepositDetailVOs = new ArrayList<BankDepositDetailVO>();
			for (BankDepositDetail detail : deposit.getBankDepositDetails()) {

				BankDepositDetailVO bankDepositDetailVO = new BankDepositDetailVO(
						detail);

				if (null != detail.getBankReceiptsDetail()) {
					bankDepositDetailVO.setReceiptTypeStr(detail
							.getBankReceiptsDetail().getBankReceipts()
							.getLookupDetail().getDisplayName());

					if (detail.getBankReceiptsDetail().getBankReceipts()
							.getSupplier() != null) {
						if (detail.getBankReceiptsDetail().getBankReceipts()
								.getSupplier().getPerson() != null)
							bankDepositDetailVO.setReceiptSourceName(detail
									.getBankReceiptsDetail()
									.getBankReceipts()
									.getSupplier()
									.getPerson()
									.getFirstName()
									.concat(" ")
									.concat(detail.getBankReceiptsDetail()
											.getBankReceipts().getSupplier()
											.getPerson().getLastName()));
						else
							bankDepositDetailVO
									.setReceiptSourceName(detail
											.getBankReceiptsDetail()
											.getBankReceipts().getSupplier()
											.getCompany() != null ? detail
											.getBankReceiptsDetail()
											.getBankReceipts().getSupplier()
											.getCompany().getCompanyName()
											: detail.getBankReceiptsDetail()
													.getBankReceipts()
													.getSupplier()
													.getCmpDeptLocation()
													.getCompany()
													.getCompanyName());

						bankDepositDetailVO.setReceiptSourceType("Supplier");
					} else if (detail.getBankReceiptsDetail().getBankReceipts()
							.getCustomer() != null) {
						if (detail.getBankReceiptsDetail().getBankReceipts()
								.getCustomer().getPersonByPersonId() != null)
							bankDepositDetailVO.setReceiptSourceName(detail
									.getBankReceiptsDetail()
									.getBankReceipts()
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat(" ")
									.concat(detail.getBankReceiptsDetail()
											.getBankReceipts().getCustomer()
											.getPersonByPersonId()
											.getLastName()));
						else
							bankDepositDetailVO
									.setReceiptSourceName(detail
											.getBankReceiptsDetail()
											.getBankReceipts().getCustomer()
											.getCompany() != null ? detail
											.getBankReceiptsDetail()
											.getBankReceipts().getCustomer()
											.getCompany().getCompanyName()
											: detail.getBankReceiptsDetail()
													.getBankReceipts()
													.getCustomer()
													.getCmpDeptLocation()
													.getCompany()
													.getCompanyName());

						bankDepositDetailVO.setReceiptSourceType("Customer");
					} else if (detail.getBankReceiptsDetail().getBankReceipts()
							.getPersonByPersonId() != null) {
						bankDepositDetailVO.setReceiptSourceName(detail
								.getBankReceiptsDetail()
								.getBankReceipts()
								.getPersonByPersonId()
								.getFirstName()
								.concat(" ")
								.concat(detail.getBankReceiptsDetail()
										.getBankReceipts()
										.getPersonByPersonId().getLastName()));
						bankDepositDetailVO.setReceiptSourceType("Employee");
					} else {
						bankDepositDetailVO.setReceiptSourceName(detail
								.getBankReceiptsDetail().getBankReceipts()
								.getOthers());
						bankDepositDetailVO.setReceiptSourceType("Others");
					}
				} else {
					bankDepositDetailVO.setReceiptSourceType("POS");
					bankDepositDetailVO.setReceiptSourceName("POS");
				}
				bankDepositDetailVOs.add(bankDepositDetailVO);
			}

			bankDepositVO.setBankDepositDetailVOs(bankDepositDetailVOs);
			bankDepositVOs.add(bankDepositVO);
		}

		return bankDepositVOs;
	}

	public String getBankDepositsReportPrintOut() {
		try {
			List<BankDepositVO> bankDepositList = this
					.fetchBankDepositsReportData();

			ServletActionContext.getRequest().setAttribute("BANK_DEPOSIT",
					bankDepositList.size() > 0 ? bankDepositList.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getBankDepositsReportXLS() {
		try {
			List<BankDepositVO> bankDepositList = this
					.fetchBankDepositsReportData();
			String str = "";

			for (BankDepositVO deposit : bankDepositList) {
				str += "\n Deposit Number, Date \n\n ";

				str += deposit.getBankDepositNumber() + ","
						+ deposit.getRecordDate();

				str += "\n";

				str += "\n\n Receipt Type, Bank Name , Account Number, Receipt Source, Source, Amount \n";
				for (BankDepositDetailVO detail : deposit
						.getBankDepositDetailVOs()) {
					str += detail.getReceiptType() + "," + detail.getBankName()
							+ "," + detail.getAccountNo() + ","
							+ detail.getReceiptSourceType() + ","
							+ detail.getReceiptSourceName() + ","
							+ detail.getAmount();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getBankDepositsPDF() {
		String result = ERROR;
		try {
			bankDepositDS = this.fetchBankDepositsReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getInternalTransferCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"CURRENCY_LIST",
					accountsBL.getTransactionBL().getCurrencyService()
							.getAllCurrency(implementation));
			ServletActionContext.getRequest().setAttribute(
					"RECEIPT_TYPES",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"RECEIPT_TYPES", true));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getInternalTransferJsonList() {
		try {

			List<BankTransferVO> bankTransferList = this
					.fetchInternalTransferReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", bankTransferList.size());
			jsonResponse.put("iTotalDisplayRecords", bankTransferList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (BankTransferVO list : bankTransferList) {
				array = new JSONArray();
				array.add(list.getBankTransferId());
				array.add(list.getTransferNo());
				array.add(list.getTransferDate());
				array.add(list.getTransferNature());
				array.add(list.getCurrencyName());
				array.add(list.getTransferFrom());
				array.add(list.getTransferTo());
				array.add(AIOSCommons.formatAmount(list.getTransferAmount()));
				array.add(null != list.getTransferCharges()
						&& list.getTransferCharges() > 0 ? AIOSCommons
						.formatAmount(list.getTransferCharges()) : "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<BankTransferVO> fetchInternalTransferReportData()
			throws Exception {

		Long depositId = 0L;
		Integer selectedCurrencyId = 0;
		Long selectedReceiptTypeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("selectedDepositId") != null) {
			depositId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("selectedDepositId"));
		}

		if (ServletActionContext.getRequest()
				.getParameter("selectedCurrencyId") != null) {
			selectedCurrencyId = Integer.parseInt(ServletActionContext
					.getRequest().getParameter("selectedCurrencyId"));
		}

		if (ServletActionContext.getRequest().getParameter(
				"selectedReceiptTypeId") != null) {
			selectedReceiptTypeId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("selectedReceiptTypeId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<BankTransfer> transferList = accountsEnterpriseService
				.getBankTransferService().getFilteredBankTransfer(
						implementation);

		List<BankTransferVO> bankTransferVOs = new ArrayList<BankTransferVO>();

		for (BankTransfer transfer : transferList) {
			BankTransferVO bankTransferVO = new BankTransferVO(transfer);

			bankTransferVO.setTransferDate(DateFormat
					.convertDateToString(transfer.getDate().toString()));
			bankTransferVO.setCurrencyName(transfer.getCurrency()
					.getCurrencyPool().getCode());
			bankTransferVO.setTransferNature(transfer.getLookupDetail()
					.getDisplayName());
			bankTransferVO.setTransferFrom(transfer
					.getBankAccountByDebitBankAccount().getBank().getBankName()
					+ " --> "
					+ transfer.getBankAccountByDebitBankAccount()
							.getAccountNumber());
			bankTransferVO.setTransferTo(transfer
					.getBankAccountByCreditBankAccount().getBank()
					.getBankName()
					+ " --> "
					+ transfer.getBankAccountByCreditBankAccount()
							.getAccountNumber());

			bankTransferVOs.add(bankTransferVO);
		}

		return bankTransferVOs;
	}

	public String getInternalTransferReportPrintOut() {
		try {
			List<BankTransferVO> bankTransferList = this
					.fetchInternalTransferReportData();

			ServletActionContext.getRequest().setAttribute(
					"INTERNAL_TRANSFER",
					bankTransferList.size() > 0 ? bankTransferList.get(0)
							: null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getInternalTransfersReportXLS() {
		try {
			List<BankTransferVO> bankTransferList = this
					.fetchInternalTransferReportData();
			String str = "";

			for (BankTransferVO transfer : bankTransferList) {
				str += "\n Transfer Number, Date, Currency, Transfer Nature, Transfer From, Transfer To, Transfer Amount, Transfer Charges \n\n ";

				str += transfer.getTransferNo() + ","
						+ transfer.getTransferDate() + ","
						+ transfer.getCurrencyName() + ","
						+ transfer.getTransferNature() + ","
						+ transfer.getTransferFrom() + ","
						+ transfer.getTransferTo() + ","
						+ transfer.getTransferAmount() + ","
						+ transfer.getTransferCharges();

				str += "\n";
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getInternalTransferPDF() {
		String result = ERROR;
		try {
			bankTransferDS = this.fetchInternalTransferReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getBankAccountStatementCriteriaData() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getBankAccountStatementJsonList() {
		try {

			getImplementId();
			List<BankAccount> accountList = accountsEnterpriseService
					.getBankService().getAllBankAccounts(implementation);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", accountList.size());
			jsonResponse.put("iTotalDisplayRecords", accountList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (BankAccount list : accountList) {
				array = new JSONArray();
				array.add(list.getBankAccountId());
				array.add(list.getAccountNumber());
				array.add(list.getBank().getBankName());
				array.add(list.getBranchName());
				array.add(BankAccountType.get(list.getAccountType()) != null ? BankAccountType
						.get(list.getAccountType()).name() : "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getBankAccountStatementReportPrintOut() {
		try {

			BankAccountVO bankAccount = this
					.fetchBankAccountStatementReportData();

			ServletActionContext.getRequest().setAttribute("BANK_ACCOUNT",
					bankAccount);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getBankAccountStatementReportXLS() {
		try {
			BankAccountVO bankAccount = this
					.fetchBankAccountStatementReportData();
			String str = "";

			str += "\n Bank Name, " + bankAccount.getBankName()
					+ ",,, Account Type " + bankAccount.getAccountTypeName()
					+ "\n ";
			str += "\n Account Number, " + bankAccount.getAccountNumber()
					+ ",,, Account Holder " + bankAccount.getAccountHolder()
					+ "\n ";

			str += "\n Date, Reference, Description, Debit, Credit \n\n ";

			for (AccountsDetailVO detail : bankAccount.getAccountsDetailVOs()) {

				str += detail.getDate() + "," + detail.getReferenceNo() + ","
						+ detail.getDescription() + "," + detail.getDebit()
						+ "," + detail.getCredit() + ",";
				str += "\n";
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getBankAccountStatementPDF() {
		String result = ERROR;
		try {
			bankAccountDS = this.fetchBankAccountStatementReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public BankAccountVO fetchBankAccountStatementReportData() throws Exception {

		Long bankAccountId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		if (ServletActionContext.getRequest().getParameter("accountId") != null) {
			bankAccountId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("accountId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		BankAccountVO bankAccount = accountsBL.getBankBL()
				.getBankAccountStatements(bankAccountId, fromDatee, toDatee);

		return bankAccount;
	}

	public String showRequisitionStatusPrint() {
		try {
			LOGGER.info("Module: Accounts method Inside showRequisitionStatusPrint()");
			Long requisitionId = 0L;
			if (ServletActionContext.getRequest().getParameter("requisitionId") != null) {
				requisitionId = Long.parseLong(ServletActionContext
						.getRequest().getParameter("requisitionId"));
			}
			Requisition requisition = accountsBL.getRequisitionBL()
					.getRequisitionService().getRequisitionCycle(requisitionId);
			RequisitionVO requisitionVO = accountsBL
					.processRequisitionCycle(requisition);
			ServletActionContext.getRequest().setAttribute("REQUISITION_CYCLE",
					requisitionVO);
			LOGGER.info("Module: Accounts method showRequisitionStatusPrint() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method showRequisitionStatusPrint() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String showQuotationComparisonPrint() {
		try {
			LOGGER.info("Module: Accounts method Inside showQuotationComparisonPrint()");
			Long requisitionId = 0L;
			getImplementId();
			if (ServletActionContext.getRequest().getParameter("requisitionId") != null) {
				requisitionId = Long.parseLong(ServletActionContext
						.getRequest().getParameter("requisitionId"));
			}
			Requisition requisition = accountsBL.getRequisitionBL()
					.getRequisitionService().getRequisitionById(requisitionId);
			List<QuotationDetail> quotationDetails = null;
			List<QuotationDetail> quotationFinalDetails = new ArrayList<QuotationDetail>();
			Map<Long, InventoryComparisonVO> inventoryComparisonVOMap = null;
			InventoryComparisonVO inventoryComparisonVO = null;
			Map<Long, Map<Long, InventoryComparisonVO>> supplierMap = new HashMap<Long, Map<Long, InventoryComparisonVO>>();
			Map<Long, List<InventoryComparisonVO>> productMap = new HashMap<Long, List<InventoryComparisonVO>>();
			List<InventoryComparisonVO> inventoryComparisonVOs = null;
			Map<String, List<QuotationDetail>> quotationDetailMap = new HashMap<String, List<QuotationDetail>>();
			List<QuotationDetail> quotationDetailtmp = null;
			for (RequisitionDetail requisitionDt : requisition
					.getRequisitionDetails()) {
				productMap.put(requisitionDt.getProduct().getProductId(), null);
				quotationDetails = accountsBL
						.getRequisitionBL()
						.getPurchaseBL()
						.getQuotationBL()
						.getQuotationService()
						.getQuotationDetailByProductId(
								requisitionDt.getProduct().getProductId());
				if (null != quotationDetails && quotationDetails.size() > 0) {
					quotationFinalDetails.addAll(quotationDetails);
					for (QuotationDetail quotationDetail : quotationDetails) {
						quotationDetailtmp = new ArrayList<QuotationDetail>();
						if (quotationDetailMap.containsKey(quotationDetail
								.getProduct().getProductId()
								+ "-"
								+ quotationDetail.getQuotation().getSupplier()
										.getSupplierId())) {
							quotationDetailtmp.addAll(quotationDetailMap
									.get(requisitionDt.getProduct()
											.getProductId()
											+ "-"
											+ quotationDetail.getQuotation()
													.getSupplier()
													.getSupplierId()));
						}
						quotationDetailtmp.add(quotationDetail);
						quotationDetailMap.put(requisitionDt.getProduct()
								.getProductId()
								+ "-"
								+ quotationDetail.getQuotation().getSupplier()
										.getSupplierId(), quotationDetailtmp);
					}
				}
			}
			List<SupplierVO> supplierVOs = new ArrayList<SupplierVO>();
			SupplierVO supplierVO = null;
			boolean addsupplier = true;
			for (QuotationDetail quotationDt : quotationFinalDetails) {
				if (null != supplierMap && supplierMap.size() < 5) {
					supplierMap.put(quotationDt.getQuotation().getSupplier()
							.getSupplierId(), null);
					addsupplier = true;
					if (null != supplierVOs && supplierVOs.size() > 0) {
						for (SupplierVO supplierVO2 : supplierVOs) {
							if ((long) supplierVO2.getSupplierId() == (long) quotationDt
									.getQuotation().getSupplier()
									.getSupplierId()) {
								addsupplier = false;
								break;
							}
						}
					}
					if (addsupplier) {
						supplierVO = new SupplierVO();
						supplierVO.setSupplierId(quotationDt.getQuotation()
								.getSupplier().getSupplierId());
						if (null != quotationDt.getQuotation().getSupplier()
								.getPerson()) {
							supplierVO.setSupplierName(quotationDt
									.getQuotation()
									.getSupplier()
									.getPerson()
									.getFirstName()
									.concat(" ")
									.concat(quotationDt.getQuotation()
											.getSupplier().getPerson()
											.getLastName()));
						} else if ((null != quotationDt.getQuotation()
								.getSupplier().getCmpDeptLocation() || null != quotationDt
								.getQuotation().getSupplier().getCompany())) {
							supplierVO
									.setSupplierName(null != quotationDt
											.getQuotation().getSupplier()
											.getCmpDeptLocation()
											&& !("").equals(quotationDt
													.getQuotation()
													.getSupplier()
													.getCmpDeptLocation()) ? quotationDt
											.getQuotation().getSupplier()
											.getCmpDeptLocation().getCompany()
											.getCompanyName()
											: quotationDt.getQuotation()
													.getSupplier().getCompany()
													.getCompanyName());
						}
						supplierVOs.add(supplierVO);
					}
				} else
					break;
			}

			if (null != quotationFinalDetails
					&& quotationFinalDetails.size() > 0) {
				for (QuotationDetail quotationDt : quotationFinalDetails) {
					inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
					if (supplierMap.containsKey(quotationDt.getQuotation()
							.getSupplier().getSupplierId())) {
						inventoryComparisonVOMap = supplierMap.get(quotationDt
								.getQuotation().getSupplier().getSupplierId());
						if (null != inventoryComparisonVOMap
								&& inventoryComparisonVOMap.size() > 0) {
							for (Entry<Long, InventoryComparisonVO> entry : inventoryComparisonVOMap
									.entrySet()) {
								inventoryComparisonVO = new InventoryComparisonVO();
								if ((long) quotationDt.getProduct()
										.getProductId() == (long) entry
										.getKey()) {
									inventoryComparisonVO = entry.getValue();
									inventoryComparisonVO
											.setTotalQty(inventoryComparisonVO
													.getTotalQty()
													+ quotationDt
															.getTotalUnits());
									inventoryComparisonVO
											.setTotalUnitPrice(inventoryComparisonVO
													.getTotalUnitPrice()
													+ quotationDt.getUnitRate());
								} else {
									inventoryComparisonVO
											.setTotalQty(quotationDt
													.getTotalUnits());
									inventoryComparisonVO
											.setTotalUnitPrice(quotationDt
													.getUnitRate());
									inventoryComparisonVO
											.setProductCode(quotationDt
													.getProduct().getCode());
									inventoryComparisonVO
											.setProductName(quotationDt
													.getProduct()
													.getProductName());
									inventoryComparisonVO
											.setSupplierId(quotationDt
													.getQuotation()
													.getSupplier()
													.getSupplierId());
									inventoryComparisonVO
											.setProductId(quotationDt
													.getProduct()
													.getProductId());
								}
							}
						} else {
							inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
							inventoryComparisonVO = new InventoryComparisonVO();
							inventoryComparisonVO.setTotalQty(quotationDt
									.getTotalUnits());
							inventoryComparisonVO.setTotalUnitPrice(quotationDt
									.getUnitRate());
							inventoryComparisonVO.setProductCode(quotationDt
									.getProduct().getCode());
							inventoryComparisonVO.setProductName(quotationDt
									.getProduct().getProductName());
							inventoryComparisonVO.setProductId(quotationDt
									.getProduct().getProductId());
							inventoryComparisonVO.setSupplierId(quotationDt
									.getQuotation().getSupplier()
									.getSupplierId());
						}
						if (null != quotationDetailMap
								&& quotationDetailMap.size() > 0) {
							quotationDetailtmp = quotationDetailMap
									.get(inventoryComparisonVO.getProductId()
											+ "-"
											+ inventoryComparisonVO
													.getSupplierId());
							if (null != quotationDetailtmp
									&& quotationDetailtmp.size() > 0) {
								Collections.sort(quotationDetailtmp,
										new Comparator<QuotationDetail>() {
											public int compare(
													QuotationDetail o1,
													QuotationDetail o2) {
												return o2
														.getQuotationDetailId()
														.compareTo(
																o1.getQuotationDetailId());
											}
										});
								inventoryComparisonVO.setUnitPrice(AIOSCommons
										.formatAmount(quotationDetailtmp.get(0)
												.getUnitRate()));
								inventoryComparisonVO
										.setUnitRate(AIOSCommons
												.formatAmountToDouble(inventoryComparisonVO
														.getUnitPrice()));
							} else {
								inventoryComparisonVO.setUnitPrice("-");
								inventoryComparisonVO.setUnitRate(0.0);
							}
						}
						inventoryComparisonVOMap.put(quotationDt.getProduct()
								.getProductId(), inventoryComparisonVO);
						supplierMap.put(quotationDt.getQuotation()
								.getSupplier().getSupplierId(),
								inventoryComparisonVOMap);
					}
				}
			}

			if (null != supplierMap && supplierMap.size() > 0) {
				Collections.sort(supplierVOs, new Comparator<SupplierVO>() {
					public int compare(SupplierVO o1, SupplierVO o2) {
						return o1.getSupplierId().compareTo(o2.getSupplierId());
					}
				});
				for (Entry<Long, Map<Long, InventoryComparisonVO>> entry : supplierMap
						.entrySet()) {
					inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
					inventoryComparisonVOMap = entry.getValue();
					if (null != inventoryComparisonVOMap
							&& inventoryComparisonVOMap.size() > 0) {
						for (Entry<Long, InventoryComparisonVO> prdmap : inventoryComparisonVOMap
								.entrySet()) {
							inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
							if (productMap.containsKey(prdmap.getKey())) {
								inventoryComparisonVOs = productMap.get(prdmap
										.getKey());
								if (null == inventoryComparisonVOs)
									inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
								inventoryComparisonVOs.add(prdmap.getValue());
								productMap.put(prdmap.getKey(),
										inventoryComparisonVOs);
							}
						}
					}
				}
			}
			Set<Long> supplierSet = null;
			Set<Long> supplierInvSet = null;
			for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
					.entrySet()) {
				supplierSet = new HashSet<Long>();
				for (Long sp : supplierMap.keySet())
					supplierSet.add(sp);
				if (null != entry && null != entry.getValue()) {
					supplierInvSet = new HashSet<Long>();
					inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
							entry.getValue());
					for (InventoryComparisonVO inv : inventoryComparisonVOs)
						supplierInvSet.add(inv.getSupplierId());
					Collection<Long> similar = new HashSet<Long>(supplierSet);
					Collection<Long> different = new HashSet<Long>();
					different.addAll(supplierSet);
					different.addAll(supplierInvSet);
					similar.retainAll(supplierInvSet);
					different.removeAll(similar);
					if (null != different && different.size() > 0) {
						for (Long sp : different) {
							inventoryComparisonVO = new InventoryComparisonVO();
							inventoryComparisonVO.setSupplierId(sp);
							inventoryComparisonVO.setUnitPrice("-");
							inventoryComparisonVO.setUnitRate(0.0);
							inventoryComparisonVOs.add(inventoryComparisonVO);
						}
					}
				} else {
					if (null != supplierSet && supplierSet.size() > 0) {
						inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
						for (Long sp : supplierSet) {
							inventoryComparisonVO = new InventoryComparisonVO();
							inventoryComparisonVO.setSupplierId(sp);
							inventoryComparisonVO.setUnitPrice("-");
							inventoryComparisonVO.setUnitRate(0.0);
							inventoryComparisonVOs.add(inventoryComparisonVO);
						}
					}
				}
				productMap.put(entry.getKey(), inventoryComparisonVOs);
			}
			Product product = null;
			List<InventoryComparisonVO> quotationComparisonVOs = new ArrayList<InventoryComparisonVO>();
			for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
					.entrySet()) {
				if (null != entry && null != entry.getValue()) {
					inventoryComparisonVO = new InventoryComparisonVO();
					inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
							entry.getValue());
					product = accountsBL.getAccountsEnterpriseService()
							.getProductService()
							.getBasicProductById(entry.getKey());
					inventoryComparisonVO.setProductCode(product.getCode());
					inventoryComparisonVO.setProductName(product
							.getProductName());
					double min = inventoryComparisonVOs.get(0).getUnitRate();
					for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
						if (min == 0
								&& inventoryComparisonVOs.get(i).getUnitRate() > 0)
							min = inventoryComparisonVOs.get(i).getUnitRate();
						if (inventoryComparisonVOs.get(i).getUnitRate() > 0
								&& min >= inventoryComparisonVOs.get(i)
										.getUnitRate())
							min = inventoryComparisonVOs.get(i).getUnitRate();
					}
					for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
						if ((double) inventoryComparisonVOs.get(i)
								.getUnitRate() == min)
							inventoryComparisonVOs.get(i).setPriceLevel(
									"minPrice");
					}
					Collections.sort(inventoryComparisonVOs,
							new Comparator<InventoryComparisonVO>() {
								public int compare(InventoryComparisonVO o1,
										InventoryComparisonVO o2) {
									return o1.getSupplierId().compareTo(
											o2.getSupplierId());
								}
							});
					inventoryComparisonVO
							.setInventoryComparisonVOs(inventoryComparisonVOs);
					quotationComparisonVOs.add(inventoryComparisonVO);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_QUOTECOMPARISON", quotationComparisonVOs);
			ServletActionContext.getRequest().setAttribute("REQUISITION",
					requisition);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_LIST",
					supplierVOs);
			LOGGER.info("Module: Accounts method showQuotationComparisonPrint() Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method showQuotationComparisonPrint() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getIssueRequisitionCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"LOCATION_LIST",
					accountsBL.getCompanyBL().getLocationService()
							.getAllLocation(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getIssueRequisitionJsonList() {
		try {

			List<IssueRequistionVO> issueRequisitionList = this
					.fetchIssueRequisitionReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", issueRequisitionList.size());
			jsonResponse.put("iTotalDisplayRecords",
					issueRequisitionList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (IssueRequistionVO list : issueRequisitionList) {
				array = new JSONArray();
				array.add(list.getIssueRequistionId());
				array.add(list.getReferenceNumber());
				array.add(list.getStrIssueDate());
				array.add(list.getLocationName());
				array.add(list.getRequisitionPersonName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<IssueRequistionVO> fetchIssueRequisitionReportData()
			throws Exception {

		Long requisitionId = 0L;
		Long locationId = 0L;
		Long personId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		Long productId = 0l;
		getImplementId();

		if (ServletActionContext.getRequest().getParameter("locationId") != null) {
			locationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("locationId"));
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (fromDate != null && !fromDate.equals("")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<IssueRequistion> issueRequisitionList = accountsEnterpriseService
				.getIssueRequistionService().getFilteredIssueRequistions(
						implementation, requisitionId, locationId, personId,
						fromDatee, toDatee, productId);

		List<IssueRequistionVO> issueRequistionVOs = new ArrayList<IssueRequistionVO>();
		List<IssueRequistionDetailVO> issueRequistionDetailVOsAll = new ArrayList<IssueRequistionDetailVO>();
		for (IssueRequistion requisition : issueRequisitionList) {
			IssueRequistionVO requisitionVO = new IssueRequistionVO(requisition);
			requisitionVO
					.setStrIssueDate(DateFormat.convertDateToString(requisition
							.getIssueDate().toString()));
			requisitionVO.setShippingSource(null != requisition
					.getLookupDetail() ? requisition.getLookupDetail()
					.getDisplayName() : "");
			requisitionVO.setDepartmentName(requisition.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			requisitionVO.setLocationName(requisition.getCmpDeptLocation()
					.getCompany().getCompanyName()
					+ ">>"
					+ requisition.getCmpDeptLocation().getDepartment()
							.getDepartmentName()
					+ ">>"
					+ requisition.getCmpDeptLocation().getLocation()
							.getLocationName());
			requisitionVO.setRequisitionPersonName(requisition.getPerson()
					.getFirstName().concat(" ")
					.concat(requisition.getPerson().getLastName()));

			List<IssueRequistionDetailVO> issueRequistionDetailVOs = new ArrayList<IssueRequistionDetailVO>();

			for (IssueRequistionDetail detail : requisition
					.getIssueRequistionDetails()) {

				IssueRequistionDetailVO issueRequistionDetailVO = new IssueRequistionDetailVO(
						detail);
				issueRequistionDetailVO.setProductCode(detail.getProduct()
						.getCode());
				issueRequistionDetailVO.setProductName(detail.getProduct()
						.getProductName());
				issueRequistionDetailVO.setStoreName(detail.getShelf()
						.getShelf().getAisle().getStore().getStoreName()
						+ ">>"
						+ detail.getShelf().getShelf().getAisle()
								.getSectionName()
						+ ">>"
						+ detail.getShelf().getShelf().getName()
						+ ">>"
						+ detail.getShelf().getName());
				issueRequistionDetailVO.setTotalRate(detail.getQuantity()
						* detail.getUnitRate());
				issueRequistionDetailVO.setIssueRequistionVO(requisitionVO);
				issueRequistionDetailVOs.add(issueRequistionDetailVO);

			}
			issueRequistionDetailVOsAll.addAll(issueRequistionDetailVOs);
			requisitionVO.setIssueRequistionDetailVO(issueRequistionDetailVOs);

			issueRequistionVOs.add(requisitionVO);
		}
		ServletActionContext.getRequest().setAttribute("ISSUE_DETAIL",
				issueRequistionDetailVOsAll);
		return issueRequistionVOs;
	}

	public String getIssueRequisitionReportPrintOut() {
		try {

			List<IssueRequistionVO> issueRequisitionList = this
					.fetchIssueRequisitionReportData();

			ServletActionContext
					.getRequest()
					.setAttribute(
							"ISSUE_REQUISITION",
							issueRequisitionList != null
									&& issueRequisitionList.size() > 0 ? issueRequisitionList
									.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getIssueRequisitionReportXLS() {
		try {

			List<IssueRequistionVO> issueRequisitionList = this
					.fetchIssueRequisitionReportData();
			String str = "";

			for (IssueRequistionVO requisition : issueRequisitionList) {
				str += "\n Reference Number, "
						+ requisition.getReferenceNumber() + ",,, Deparment "
						+ requisition.getDepartmentName() + "\n ";
				str += "\n Location, " + requisition.getLocationName() + "\n ";
				str += "\n Issue Date, " + requisition.getStrIssueDate()
						+ ",,, Shipping Source "
						+ requisition.getShippingSource() + "\n ";

				str += "\n Product Code, Product Name, Store Name, Issue Qunatity, Unit Price, Total \n\n ";
				for (IssueRequistionDetailVO detail : requisition
						.getIssueRequistionDetailVO()) {

					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getStoreName() + ","
							+ detail.getQuantity() + "," + detail.getUnitRate()
							+ "," + detail.getTotalRate() + ",";
					str += "\n";
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getIssueRequisitionPDF() {
		String result = ERROR;
		try {
			issueRequistionDS = this.fetchIssueRequisitionReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getIssueReturnsCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"LOCATION_LIST",
					accountsBL.getCompanyBL().getLocationService()
							.getAllLocation(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getIssueReturnsJsonList() {
		try {

			List<IssueReturnVO> issueReturnList = this
					.fetchIssueReturnReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", issueReturnList.size());
			jsonResponse.put("iTotalDisplayRecords", issueReturnList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (IssueReturnVO list : issueReturnList) {
				array = new JSONArray();
				array.add(list.getIssueReturnId());
				array.add(list.getReferenceNumber());
				array.add(list.getStrReturnDate());
				array.add(list.getLocationName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<IssueReturnVO> fetchIssueReturnReportData() throws Exception {

		Long issueReturnId = 0L;

		Long locationId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("locationId") != null) {
			locationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("locationId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		if (ServletActionContext.getRequest().getParameter("issueReturnId") != null) {
			issueReturnId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("issueReturnId"));
		}

		List<IssueReturn> issueReturnList = accountsEnterpriseService
				.getIssueReturnService().getFilteredIssueReturn(implementation,
						issueReturnId, locationId, fromDatee, toDatee);

		List<IssueReturnVO> issueReturnVOs = new ArrayList<IssueReturnVO>();

		for (IssueReturn issueReturn : issueReturnList) {
			IssueReturnVO issueReturnVO = new IssueReturnVO(issueReturn);

			issueReturnVO
					.setStrReturnDate(DateFormat
							.convertDateToString(issueReturn.getReturnDate()
									.toString()));
			issueReturnVO.setLocationName(issueReturn.getCmpDeptLocation()
					.getLocation().getLocationName());
			issueReturnVO.setDepartmentName(issueReturn.getCmpDeptLocation()
					.getDepartment().getDepartmentName());

			List<IssueReturnDetailVO> issueReturnDetailVOs = new ArrayList<IssueReturnDetailVO>();

			for (IssueReturnDetail detail : issueReturn.getIssueReturnDetails()) {

				IssueReturnDetailVO issueReturnDetailVO = new IssueReturnDetailVO(
						detail);

				issueReturnDetailVO.setProductCode(detail.getProduct()
						.getCode());
				issueReturnDetailVO.setProductName(detail.getProduct()
						.getProductName());
				issueReturnDetailVO.setIssueReference(detail
						.getIssueRequistionDetail().getIssueRequistion()
						.getReferenceNumber());
				issueReturnDetailVO.setIssueQty(detail
						.getIssueRequistionDetail().getQuantity());
				if (detail.getLookupDetail() != null) {
					issueReturnDetailVO.setMaterialCondition(detail
							.getLookupDetail().getDisplayName());
				} else {
					issueReturnDetailVO.setMaterialCondition("");
				}

				issueReturnDetailVOs.add(issueReturnDetailVO);

			}

			issueReturnVO.setIssueReturnDetailVOs(issueReturnDetailVOs);

			issueReturnVOs.add(issueReturnVO);
		}

		return issueReturnVOs;
	}

	public String getIssueReturnsReportPrintOut() {
		try {

			List<IssueReturnVO> issueReturnList = this
					.fetchIssueReturnReportData();

			ServletActionContext
					.getRequest()
					.setAttribute(
							"ISSUE_RETURNS",
							issueReturnList != null
									&& issueReturnList.size() > 0 ? issueReturnList
									.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getIssueReturnsReportXLS() {
		try {

			List<IssueReturnVO> issueReturnList = this
					.fetchIssueReturnReportData();
			String str = "";

			for (IssueReturnVO issueReturns : issueReturnList) {
				str += "\n Reference Number, "
						+ issueReturns.getReferenceNumber() + ",,, Department "
						+ issueReturns.getDepartmentName() + "\n ";
				str += "\n Return Date, " + issueReturns.getStrReturnDate()
						+ ",,, Location " + issueReturns.getLocationName()
						+ "\n ";

				str += "\n Product Code, Product Name, Issue Reference, Issue Qunatity, Return Quantity, Material Condition \n\n ";
				for (IssueReturnDetailVO detail : issueReturns
						.getIssueReturnDetailVOs()) {

					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getIssueReference() + ","
							+ detail.getIssueQty() + ","
							+ detail.getReturnQuantity() + ","
							+ detail.getMaterialCondition() + ",";
					str += "\n";
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getIssueReturnsPDF() {
		String result = ERROR;
		try {
			issueReturnDS = this.fetchIssueReturnReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProductPricingCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getProductPricingJsonList() {
		try {

			List<ProductPricingVO> productPricingList = this
					.fetchProductPricingReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", productPricingList.size());
			jsonResponse.put("iTotalDisplayRecords", productPricingList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (ProductPricingVO list : productPricingList) {
				array = new JSONArray();
				array.add(list.getProductPricingId());
				array.add(list.getPricingTitle());
				array.add(list.getCurrencyCode());
				array.add(list.getEffictiveStartDate());
				array.add(list.getEffictiveEndDate());
				array.add(list.getDescription());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<ProductPricingVO> fetchProductPricingReportData()
			throws Exception {

		Long pricingId = 0L;
		getImplementId();

		Integer storeId = 0;
		Long productId = 0L;
		Long customerId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("storeId") != null) {
			storeId = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("storeId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		if (ServletActionContext.getRequest().getParameter("pricingId") != null) {
			pricingId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("pricingId"));
		}

		List<ProductPricing> productPricingList = accountsEnterpriseService
				.getProductPricingService().getFilteredProductPricing(
						implementation, pricingId, storeId, productId,
						customerId, fromDatee, toDatee);

		List<ProductPricingVO> productPricingVOs = new ArrayList<ProductPricingVO>();

		for (ProductPricing pricing : productPricingList) {
			ProductPricingVO productPricingVO = new ProductPricingVO(pricing);

			productPricingVO.setCurrencyCode(pricing.getCurrency()
					.getCurrencyPool().getCode());
			productPricingVO.setEffictiveStartDate(DateFormat
					.convertDateToString(pricing.getStartDate().toString()));
			productPricingVO
					.setEffictiveEndDate(null != pricing.getEndDate() ? DateFormat
							.convertDateToString(pricing.getEndDate()
									.toString()) : "");

			List<ProductPricingDetailVO> productPricingDetailVOs = new ArrayList<ProductPricingDetailVO>();

			for (ProductPricingDetail detail : pricing
					.getProductPricingDetails()) {

				ProductPricingDetailVO pricingDetailVO = new ProductPricingDetailVO(
						detail);

				pricingDetailVO.setProductCode(detail.getProduct().getCode());
				pricingDetailVO.setProductName(detail.getProduct()
						.getProductName());

				List<ProductPricingCalcVO> productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
				Integer count = 0;
				for (ProductPricingCalc calc : detail.getProductPricingCalcs()) {
					ProductPricingCalcVO productPricingCalcVO = new ProductPricingCalcVO(
							calc);
					productPricingCalcVO.setPricingType(calc.getLookupDetail()
							.getDisplayName());
					productPricingCalcVO
							.setCalculationTypeCode(ProductCalculationType.get(
									calc.getCalculationType()).name());
					if (calc.getCalculationSubType() != null) {
						productPricingCalcVO
								.setCalculationSubTypeCode(ProductCalculationSubType
										.get(calc.getCalculationSubType())
										.name());
					}

					if (productPricingCalcVO.getPricingType().equalsIgnoreCase(
							"margin")
							|| productPricingCalcVO.getPricingType()
									.equalsIgnoreCase("markup")) {
						productPricingCalcVO
								.setSalesAmount(calc.getSalePrice());
					} else {
						productPricingCalcVO.setSalesAmount(calc
								.getSalePriceAmount());
					}

					if (calc.getStore() != null) {
						productPricingCalcVO.setStoreName(calc.getStore()
								.getStoreName());
					} else {
						productPricingCalcVO.setStoreName("");
					}
					productPricingCalcVOs.add(productPricingCalcVO);

					if (productPricingVO.getCustomers() != null) {
						productPricingVO.setCustomers(productPricingVO
								.getCustomers().concat(
										calc.getCustomer()
												.getPersonByPersonId()
												.getFirstName()
												+ " "
												+ calc.getCustomer()
														.getPersonByPersonId()
														.getLastName()));
						if (count <= detail.getProductPricingCalcs().size()) {
							productPricingVO.setCustomers(productPricingVO
									.getCustomers().concat(", "));
						}
					}
					count++;

				}

				pricingDetailVO.setProductPricingCalcVOs(productPricingCalcVOs);

				productPricingDetailVOs.add(pricingDetailVO);
			}

			productPricingVO
					.setProductPricingDetailVOs(productPricingDetailVOs);

			productPricingVOs.add(productPricingVO);
		}

		return productPricingVOs;
	}

	public String getProductPricingPrintOut() {
		try {

			List<ProductPricingVO> productPricingList = this
					.fetchProductPricingReportData();

			ServletActionContext
					.getRequest()
					.setAttribute(
							"PRODUCT_PRICING",
							productPricingList != null
									&& productPricingList.size() > 0 ? productPricingList
									.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProductPricingReportXLS() {
		try {

			List<ProductPricingVO> productPricingList = this
					.fetchProductPricingReportData();
			String str = "";

			for (ProductPricingVO pricing : productPricingList) {
				str += "\n Pricing Title, " + pricing.getPricingTitle()
						+ ",,, Customer(s) " + pricing.getCustomers() + "\n ";
				str += " From Date, " + pricing.getEffictiveStartDate() + "\n ";
				str += " End Date, " + pricing.getEffictiveEndDate() + "\n ";

				str += "\n Product Code, Product Name, Base Price, Standard Price, Retail Price \n\n ";
				for (ProductPricingDetailVO detail : pricing
						.getProductPricingDetailVOs()) {

					str += detail.getProductCode() + ","
							+ detail.getProductName() + ","
							+ detail.getBasicCostPrice() + ","
							+ detail.getStandardPrice() + ","
							+ detail.getSuggestedRetailPrice();
					str += "\n\n";

					str += "Pricing Type, Store, Calculatuion Type, Sub Calculatuion, Sale Price\n\n ";
					for (ProductPricingCalcVO calc : detail
							.getProductPricingCalcVOs()) {
						str += calc.getPricingType() + ","
								+ calc.getStoreName() + ","
								+ calc.getCalculationTypeCode() + ","
								+ calc.getCalculationSubTypeCode() + ","
								+ calc.getSalesAmount();
						str += "\n\n";
					}
				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getProductPricingPDF() {
		String result = ERROR;
		try {
			productPricingDS = this.fetchProductPricingReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public String downloadReorderStockReport() {
		try {
			LOGGER.info("Inside Reporting: AccountsAction : Method: downloadReorderStockReport");

			List<StockVO> reOrderStock = (List<StockVO>) accountsBL
					.getStockBL().getProductReorder();

			String str = "Product Code, Product, Category, Reorder Quantity, Available Quantity, Cost Price, Standard Price, Sale Price\n";

			for (StockVO stock : reOrderStock) {

				str += stock.getProductCode() + "," + stock.getProductName()
						+ "," + stock.getCategoryName() + ","
						+ stock.getReorderQuantity() + ","
						+ stock.getAvailableQuantity() + ","
						+ stock.getCostPrice() + "," + stock.getStandardPrice()
						+ "," + stock.getUnitRate() + "\n";
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			LOGGER.info("Module: Reporting : Method: downloadReorderStockReport Success");
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: AccountsAction : Method: downloadReorderStockReport: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getStockReportCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String getStockJsonList() {
		try {
			getImplementId();
			Long storeId = 0l;
			Integer sectionId = 0;
			Integer rackId = 0;
			Integer shelfId = 0;
			Long productId = 0l;
			List<StockVO> stocksVOs = null;
			if (ServletActionContext.getRequest().getParameter("storeId") != null) {
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("storeId"));
			}
			if (ServletActionContext.getRequest().getParameter("productId") != null) {
				productId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("productId"));
			}

			if (ServletActionContext.getRequest().getParameter("sectionId") != null) {
				sectionId = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("sectionId"));
			}

			if (ServletActionContext.getRequest().getParameter("rackId") != null) {
				rackId = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("rackId"));
			}

			if (ServletActionContext.getRequest().getParameter("shelfId") != null) {
				shelfId = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("shelfId"));
			}
			if (storeId > 0) {
				List<Store> storeList = accountsBL
						.getStockBL()
						.getStoreBL()
						.getStoreService()
						.getFilteredStoreDetails(implementation, storeId,
								productId);
				List<StoreVO> storeVOs = new ArrayList<StoreVO>();
				for (Store store : storeList) {
					StoreVO storeVO = new StoreVO(store);
					List<StockVO> stockVOs = new ArrayList<StockVO>();
					Map<Long, Long> productMap = new HashMap<Long, Long>();
					for (Stock stock : store.getStocks()) {
						if (!productMap.containsKey(stock.getProduct()
								.getProductId())) {
							List<StockVO> availStock = null;
							if (sectionId > 0 || rackId > 0 || shelfId > 0)
								availStock = accountsBL.getStockBL()
										.getStockDetails(
												stock.getProduct()
														.getProductId(),
												store.getStoreId(), sectionId,
												rackId, shelfId);
							else
								availStock = accountsBL.getStockBL()
										.getStockByStoreDetails(
												stock.getProduct()
														.getProductId(),
												store.getStoreId());
							if (availStock != null)
								stockVOs.addAll(availStock);
						}
						productMap.put(stock.getProduct().getProductId(),
								store.getStoreId());
					}
					storeVO.setStockVOs(stockVOs);
					storeVOs.add(storeVO);
				}
				stocksVOs = new ArrayList<StockVO>();
				StockVO tempStockVO = null;
				for (StoreVO storeVO : storeVOs) {
					for (StockVO stock : storeVO.getStockVOs()) {
						tempStockVO = new StockVO();
						tempStockVO
								.setProductCode(stock.getProduct().getCode());
						tempStockVO.setProductName(stock.getProduct()
								.getProductName());
						tempStockVO.setStoreName(stock.getStoreName());
						tempStockVO.setAvailableQuantity(stock
								.getAvailableQuantity());
						tempStockVO
								.setShelfName(null != stock.getShelf() ? stock
										.getShelf().getName() : "");
						tempStockVO.setProductId(stock.getProduct()
								.getProductId());
						stocksVOs.add(tempStockVO);
					}
				}
				if (productId == 0) {
					// Add Craft & Service Stocks
					List<Stock> craftStocks = accountsBL
							.getStockBL()
							.getStockService()
							.getCraftServiceProducts(
									getImplementation(),
									InventorySubCategory.Craft0Manufacturer
											.getCode(),
									InventorySubCategory.Services.getCode());
					if (null != craftStocks && craftStocks.size() > 0) {
						if (null == stocksVOs || stocksVOs.size() == 0)
							stocksVOs = new ArrayList<StockVO>();
						for (Stock stock : craftStocks) {
							stock.setShelf(null);
							stock.setStore(null);
							stocksVOs.add(accountsBL.getStockBL()
									.addStockObject(stock));
						}
					}
				}
			} else
				stocksVOs = (List<StockVO>) accountsBL.getStockBL()
						.showStockDetailByProducts();

			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			if (null != stocksVOs && stocksVOs.size() > 0) {
				List<ProductPackageDetail> productPackageDetails = null;
				Product product = null;
				for (StockVO stock : stocksVOs) {
					array = new JSONArray();
					product = accountsBL.getPurchaseBL().getProductBL()
							.getProductService()
							.getSimpleProductById(stock.getProductId());
					array.add(stock.getStoreId());
					array.add(stock.getProductCode());
					array.add(stock.getProductName());
					array.add(product.getLookupDetailByProductUnit()
							.getDisplayName());
					array.add(stock.getStoreName());
					array.add(stock.getShelfName());
					array.add(stock.getAvailableQuantity());
					data.add(array);
					productPackageDetails = accountsBL.getPurchaseBL()
							.getPackageConversionBL()
							.getProductPackageDetails(product.getProductId());
					if (null != productPackageDetails
							&& productPackageDetails.size() > 0) {
						for (ProductPackageDetail productPackageDetail : productPackageDetails) {
							array = new JSONArray();
							array.add(stock.getStoreId());
							array.add(stock.getProductCode());
							array.add(stock.getProductName());
							array.add(productPackageDetail.getLookupDetail()
									.getDisplayName());
							array.add(stock.getStoreName());
							array.add(stock.getShelfName());
							array.add(AIOSCommons
									.formatAmount(productPackageDetail
											.getUnitQuantity()
											* stock.getAvailableQuantity()));
							data.add(array);
						}
					}
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public List<StockVO> fetchStockReportData() throws Exception {

		getImplementId();
		Long storeId = 0l;
		Integer sectionId = 0;
		Integer rackId = 0;
		Integer shelfId = 0;
		Long productId = 0l;
		List<StockVO> stocksVOs = null;
		if (ServletActionContext.getRequest().getParameter("storeId") != null) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
		}
		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("sectionId") != null) {
			sectionId = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("sectionId"));
		}

		if (ServletActionContext.getRequest().getParameter("rackId") != null) {
			rackId = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("rackId"));
		}

		if (ServletActionContext.getRequest().getParameter("shelfId") != null) {
			shelfId = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("shelfId"));
		}

		if (storeId > 0) {
			List<Store> storeList = accountsBL
					.getStockBL()
					.getStoreBL()
					.getStoreService()
					.getFilteredStoreDetails(implementation, storeId, productId);
			List<StoreVO> storeVOs = new ArrayList<StoreVO>();
			for (Store store : storeList) {
				StoreVO storeVO = new StoreVO(store);
				List<StockVO> stockVOs = new ArrayList<StockVO>();
				Map<Long, Long> productMap = new HashMap<Long, Long>();
				for (Stock stock : store.getStocks()) {
					if (!productMap.containsKey(stock.getProduct()
							.getProductId())) {
						List<StockVO> availStock = null;
						if (sectionId > 0 || rackId > 0 || shelfId > 0)
							availStock = accountsBL.getStockBL()
									.getStockDetails(
											stock.getProduct().getProductId(),
											store.getStoreId(), sectionId,
											rackId, shelfId);
						else
							availStock = accountsBL.getStockBL()
									.getStockByStoreDetails(
											stock.getProduct().getProductId(),
											store.getStoreId());
						if (availStock != null)
							stockVOs.addAll(availStock);
					}
					productMap.put(stock.getProduct().getProductId(),
							store.getStoreId());
				}
				storeVO.setStockVOs(stockVOs);
				storeVOs.add(storeVO);
			}
			stocksVOs = new ArrayList<StockVO>();
			StockVO tempStockVO = null;
			for (StoreVO storeVO : storeVOs) {
				for (StockVO stock : storeVO.getStockVOs()) {
					tempStockVO = new StockVO();
					tempStockVO.setProductCode(stock.getProduct().getCode());
					tempStockVO.setProductName(stock.getProduct()
							.getProductName());
					tempStockVO.setStoreName(stock.getStoreName());
					tempStockVO.setAvailableQuantity(stock
							.getAvailableQuantity());
					tempStockVO.setShelfName(null != stock.getShelf() ? stock
							.getShelf().getName() : "");
					tempStockVO.setProductId(stock.getProduct().getProductId());
					stocksVOs.add(tempStockVO);
				}
			}
		} else
			stocksVOs = (List<StockVO>) accountsBL.getStockBL()
					.showStockDetailByProducts();
		List<StockVO> stocksVOFinal = null;
		if (null != stocksVOs && stocksVOs.size() > 0) {
			List<ProductPackageDetail> productPackageDetails = null;
			Product product = null;
			stocksVOFinal = new ArrayList<StockVO>();
			StockVO stockvo = null;
			for (StockVO stock : stocksVOs) {
				product = accountsBL.getPurchaseBL().getProductBL()
						.getProductService()
						.getSimpleProductById(stock.getProductId());
				stock.setProductUnit(product.getLookupDetailByProductUnit()
						.getDisplayName());
				stocksVOFinal.add(stock);
				productPackageDetails = accountsBL.getPurchaseBL()
						.getPackageConversionBL()
						.getProductPackageDetails(product.getProductId());
				if (null != productPackageDetails
						&& productPackageDetails.size() > 0) {
					for (ProductPackageDetail productPackageDetail : productPackageDetails) {
						stockvo = new StockVO();
						BeanUtils.copyProperties(stockvo, stock);
						stockvo.setAvailableQuantity(AIOSCommons
								.roundThreeDecimals(productPackageDetail
										.getUnitQuantity()
										* stock.getAvailableQuantity()));
						stockvo.setProductUnit(productPackageDetail
								.getLookupDetail().getDisplayName());
						stocksVOFinal.add(stockvo);
					}
				}
			}
		}
		return stocksVOFinal;
	}

	public String getStockPrintOut() {
		try {

			List<StockVO> storeList = this.fetchStockReportData();

			ServletActionContext.getRequest().setAttribute("STOCKS", storeList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getStockReportXLS() {
		try {

			List<StockVO> stocks = this.fetchStockReportData();
			String str = "";

			str += "Stock Report\n\n";

			str += "\n Product Code, Product Name, Store Name,  Quantity \n\n ";
			for (StockVO stock : stocks) {
				str += stock.getProductCode() + ",";
				str += stock.getProductName() + ",";
				str += stock.getStoreName() + ">>" + stock.getShelfName() + ","
						+ stock.getAvailableQuantity();
				str += "\n";
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getStockPDF() {
		String result = ERROR;
		try {
			stockDS = this.fetchStockReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getAssetCheckOutCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"ASSETS",
					accountsEnterpriseService.getAssetCreationService()
							.getAllAssets(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"LOCATION_LIST",
					accountsBL.getCompanyBL().getLocationService()
							.getAllLocation(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getAssetDepreciationCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"ASSETS",
					accountsEnterpriseService.getAssetCreationService()
							.getAllAssets(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getAssetJsonList() {
		try {

			List<AssetVO> assetVOs = this.fetchAssetDepreciationReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (AssetVO list : assetVOs) {
				array = new JSONArray();
				array.add(list.getAssetCreationId());
				array.add(list.getAssetNumber());
				array.add(list.getAssetName());
				array.add(list.getDepreciationDate());
				array.add(list.getDescription());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<AssetVO> fetchAssetDepreciationReportData() throws Exception {

		getImplementId();

		Long assetCreationId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("assetId") != null) {
			assetCreationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("assetId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<AssetDepreciation> assetDepreciations = accountsEnterpriseService
				.getAssetDepreciationService().getDepreciationCritriea(
						implementation, assetCreationId, fromDatee, toDatee);

		List<AssetVO> assetVOs = new ArrayList<AssetVO>();

		for (AssetDepreciation assetDepreciation : assetDepreciations) {
			AssetVO assetVO = new AssetVO();
			assetVO.setAssetCreationId(assetDepreciation.getAssetCreation()
					.getAssetCreationId());
			assetVO.setDepreciationDate(DateFormat
					.convertDateToString(assetDepreciation.getPeriod()
							.toString()));
			assetVO.setAssetNumber(assetDepreciation.getAssetCreation()
					.getAssetNumber());
			assetVO.setAssetName(assetDepreciation.getAssetCreation()
					.getAssetName());
			assetVO.setDescription(AIOSCommons.formatAmount(assetDepreciation
					.getDepreciation()));
			assetVOs.add(assetVO);
		}
		return assetVOs;
	}

	public String getAssetDepreciationPrintData() {
		try {

			List<AssetVO> assetVos = this.fetchAssetDepreciationReportData();

			ServletActionContext.getRequest().setAttribute("ASSET_DATA",
					assetVos);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getAssetCheckOutJsonList() {
		try {

			List<AssetCheckOutVO> checkOutList = this
					.fetchAssetCheckOutReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", checkOutList.size());
			jsonResponse.put("iTotalDisplayRecords", checkOutList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (AssetCheckOutVO list : checkOutList) {
				array = new JSONArray();
				array.add(list.getAssetCheckOutId());
				array.add(list.getCheckOutNumber());
				array.add(list.getAssetName());
				array.add(list.getOutDate());
				array.add(list.getOutDue());
				array.add(list.getCheckOutTo());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<AssetCheckOutVO> fetchAssetCheckOutReportData()
			throws Exception {

		getImplementId();

		Long assetCheckOutId = 0L;
		Long assetCreationId = 0L;
		Long locationId = 0L;
		Long checkOutPersonId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("assetCheckOutId") != null) {
			assetCheckOutId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("assetCheckOutId"));
		}

		if (ServletActionContext.getRequest().getParameter("assetId") != null) {
			assetCreationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("assetId"));
		}

		if (ServletActionContext.getRequest().getParameter("locationId") != null) {
			locationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("locationId"));
		}

		if (ServletActionContext.getRequest().getParameter("checkOutPerson") != null) {
			checkOutPersonId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("checkOutPerson"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<AssetCheckOut> checkOutList = accountsEnterpriseService
				.getAssetCheckOutService().getFilteredAssetCheckOuts(
						implementation, assetCheckOutId, assetCreationId,
						locationId, checkOutPersonId, fromDatee, toDatee);

		List<AssetCheckOutVO> assetCheckOutVOs = new ArrayList<AssetCheckOutVO>();

		for (AssetCheckOut checkOut : checkOutList) {
			AssetCheckOutVO checkOutVO = new AssetCheckOutVO(checkOut);

			checkOutVO.setOutDate(DateFormat.convertDateToString(checkOut
					.getCheckOutDate().toString()));
			checkOutVO.setAssetName(checkOut.getAssetCreation().getAssetName()
					+ " " + checkOut.getAssetCreation().getAssetNumber());
			checkOutVO.setDepartmentName(checkOut.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			checkOutVO.setLocationName(checkOut.getCmpDeptLocation()
					.getLocation().getLocationName());
			checkOutVO.setCheckOutTo(checkOut.getPersonByCheckOutTo()
					.getFirstName()
					+ " "
					+ checkOut.getPersonByCheckOutTo().getLastName());

			assetCheckOutVOs.add(checkOutVO);
		}

		return assetCheckOutVOs;
	}

	public String getAssetCheckOutPrintOut() {
		try {

			List<AssetCheckOutVO> checkOutList = this
					.fetchAssetCheckOutReportData();

			ServletActionContext.getRequest().setAttribute("CHECK_OUTS",
					checkOutList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getAssetCheckOutReportXLS() {
		try {

			List<AssetCheckOutVO> checkOutList = this
					.fetchAssetCheckOutReportData();
			String str = "";

			if (checkOutList != null && checkOutList.size() == 1) {
				AssetCheckOutVO checkOut = checkOutList.get(0);
				str += "\n Check Out Reference, "
						+ checkOut.getCheckOutNumber() + ",, Location , "
						+ checkOut.getLocationName() + "\n "
						+ "Check Out Date , " + checkOut.getOutDate()
						+ ",,Check Out To , " + checkOut.getCheckOutTo()
						+ "\n " + "Asset, " + checkOut.getAssetName() + "\n"
						+ "Department, " + checkOut.getDepartmentName()
						+ " \n\n ";
			} else if (checkOutList != null) {
				str += "\n Check Out Reference, Date, Asset, Department, Location, Check Out\n\n ";
				for (AssetCheckOutVO checkOut : checkOutList) {

					str += checkOut.getCheckOutNumber() + ","
							+ checkOut.getOutDate() + ","
							+ checkOut.getAssetName() + ","
							+ checkOut.getDepartmentName() + ","
							+ checkOut.getLocationName() + ","
							+ checkOut.getCheckOutTo();
					str += "\n\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getAssetCheckOutPDF() {
		String result = ERROR;
		try {
			checkOutDS = this.fetchAssetCheckOutReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getAssetCheckInCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"ASSETS",
					accountsEnterpriseService.getAssetCreationService()
							.getAllAssets(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"LOCATION_LIST",
					accountsBL.getCompanyBL().getLocationService()
							.getAllLocation(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getAssetCheckInJsonList() {
		try {

			List<AssetCheckInVO> checkInList = this
					.fetchAssetCheckInReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", checkInList.size());
			jsonResponse.put("iTotalDisplayRecords", checkInList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (AssetCheckInVO list : checkInList) {
				array = new JSONArray();
				array.add(list.getAssetCheckInId());
				array.add(list.getCheckInNumber());
				array.add(list.getAssetName());
				array.add(list.getInDate());
				array.add(list.getCheckOutTo());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<AssetCheckInVO> fetchAssetCheckInReportData() throws Exception {

		getImplementId();

		Long assetCheckInId = 0L;
		Long assetCreationId = 0L;
		Long checkInPersonId = 0L;
		Long checkOutPersonId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("assetCheckInId") != null) {
			assetCheckInId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("assetCheckInId"));
		}

		if (ServletActionContext.getRequest().getParameter("assetId") != null) {
			assetCreationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("assetId"));
		}

		if (ServletActionContext.getRequest().getParameter("checkInPerson") != null) {
			checkInPersonId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("checkInPerson"));
		}

		if (ServletActionContext.getRequest().getParameter("checkOutPerson") != null) {
			checkOutPersonId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("checkOutPerson"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<AssetCheckIn> checkInList = accountsEnterpriseService
				.getAssetCheckInService().getFilteredAssetCheckIns(
						implementation, assetCheckInId, assetCreationId,
						checkInPersonId, checkOutPersonId, fromDatee, toDatee);

		List<AssetCheckInVO> assetCheckInVOs = new ArrayList<AssetCheckInVO>();

		for (AssetCheckIn checkIn : checkInList) {
			AssetCheckInVO checkInVO = new AssetCheckInVO(checkIn);

			checkInVO.setCheckOutReference(checkIn.getAssetCheckOut()
					.getCheckOutNumber());
			checkInVO.setInDate(DateFormat.convertDateToString(checkIn
					.getCheckInDate().toString()));
			checkInVO.setAssetName(checkIn.getAssetCheckOut()
					.getAssetCreation().getAssetName()
					+ " "
					+ checkIn.getAssetCheckOut().getAssetCreation()
							.getAssetNumber());
			checkInVO.setCheckOutTo(checkIn.getAssetCheckOut()
					.getPersonByCheckOutTo().getFirstName()
					+ " "
					+ checkIn.getAssetCheckOut().getPersonByCheckOutTo()
							.getLastName());
			checkInVO.setCheckInBy(checkIn.getPerson().getFirstName() + " "
					+ checkIn.getPerson().getLastName());

			assetCheckInVOs.add(checkInVO);
		}

		return assetCheckInVOs;
	}

	public String getAssetCheckInPrintOut() {
		try {

			List<AssetCheckInVO> checkInList = this
					.fetchAssetCheckInReportData();

			ServletActionContext.getRequest().setAttribute("CHECK_IN",
					checkInList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getAssetCheckInReportXLS() {
		try {

			List<AssetCheckInVO> checkInList = this
					.fetchAssetCheckInReportData();
			String str = "";

			if (checkInList != null && checkInList.size() == 1) {
				AssetCheckInVO checkIn = checkInList.get(0);
				str += "\n Check In Reference, " + checkIn.getCheckInNumber()
						+ ",, Check Out To , " + checkIn.getCheckOutTo()
						+ "\n " + "Check Out Reference , "
						+ checkIn.getCheckOutReference() + ",,Check In By, "
						+ checkIn.getCheckInBy() + "\n " + "Check In Date, "
						+ checkIn.getInDate() + "\n" + "Asset, "
						+ checkIn.getAssetName() + " \n\n ";
			} else if (checkInList != null) {
				str += "\n Check In Reference, Check Out Reference, Check In Date, Asset, Check Out To, Check In By \n\n ";
				for (AssetCheckInVO checkIn : checkInList) {

					str += checkIn.getCheckInNumber() + ","
							+ checkIn.getCheckOutReference() + ","
							+ checkIn.getInDate() + ","
							+ checkIn.getAssetName() + ","
							+ checkIn.getCheckOutTo() + ","
							+ checkIn.getCheckInBy();
					str += "\n\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getAssetCheckInPDF() {
		String result = ERROR;
		try {
			checkInDS = this.fetchAssetCheckInReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProductDiscountCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("DISCOUNT_METHODS",
					AIOSCommons.getDiscountCalcMethods());
			ServletActionContext.getRequest().setAttribute("DISCOUNT_OPTIONS",
					AIOSCommons.getDiscountOptions());
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_CATEGORY_LIST",
					accountsEnterpriseService.getProductCategoryService()
							.getProductParentCategory(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getProductDiscountJsonList() {
		try {

			List<DiscountVO> productDiscountList = this
					.fetchProductDiscountReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", productDiscountList.size());
			jsonResponse
					.put("iTotalDisplayRecords", productDiscountList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (DiscountVO list : productDiscountList) {
				array = new JSONArray();
				array.add(list.getDiscountId());
				array.add(list.getDiscountTo());
				array.add(list.getDiscountTypeName());
				array.add(list.getPriceListName());
				array.add(list.getValidFrom());
				array.add(list.getValidTo());
				array.add(list.getStatus());
				array.add(list.getDiscountOptionName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<DiscountVO> fetchProductDiscountReportData() throws Exception {

		getImplementId();

		Long discountId = 0L;
		Byte discountMethod = 0;
		Byte discountOption = 0;
		Long productId = 0L;
		Long customerId = 0L;
		Long productCategoryId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("discountId") != null) {
			discountId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("discountId"));
		}

		if (ServletActionContext.getRequest().getParameter("discountMethodId") != null) {
			discountMethod = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("discountMethodId"));
		}

		if (ServletActionContext.getRequest().getParameter("discountOptionId") != null) {
			discountOption = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("discountOptionId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("productCategoryId") != null) {
			productCategoryId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("productCategoryId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Discount> discountList = accountsEnterpriseService
				.getDiscountService().getFilteredProductDiscounts(
						implementation, discountId, discountMethod,
						discountOption, productId, customerId,
						productCategoryId, fromDatee, toDatee);

		List<DiscountVO> discountVOs = new ArrayList<DiscountVO>();

		for (Discount discount : discountList) {
			DiscountVO discountVO = new DiscountVO(discount);

			discountVO.setPriceListName(discount.getDiscountName());
			discountVO.setDiscountTo(discount.getLookupDetail()
					.getDisplayName());
			discountVO.setValidFrom(DateFormat.convertDateToString(discount
					.getFromDate().toString()));
			discountVO.setValidTo(DateFormat.convertDateToString(discount
					.getToDate().toString()));
			discountVO.setDiscountTypeName(DiscountCalcMethod.get(
					discount.getDiscountType()).name());
			discountVO.setDiscountOptionName(DiscountOptions.get(
					discount.getDiscountOption()).name());

			ArrayList<DiscountOptionVO> discountOptionVOs = new ArrayList<DiscountOptionVO>();

			for (DiscountOption option : discount.getDiscountOptions()) {

				DiscountOptionVO discountOptionVO = new DiscountOptionVO(option);
				if (DiscountOptions.get(discount.getDiscountOption()).name()
						.equals("Customer")) {
					discountOptionVO.setCustomerReference(option.getCustomer()
							.getCustomerNumber());
					discountOptionVO.setCustomerName(null != option
							.getCustomer().getPersonByPersonId() ? option
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(option.getCustomer().getPersonByPersonId()
									.getLastName()) : (null != option
							.getCustomer().getCompany() ? option.getCustomer()
							.getCompany().getCompanyName() : option
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName()));
					// discountOptionVO.setCustomerName(option.getCustomer().getPersonByPersonId().getFirstName()
					// + " " +
					// option.getCustomer().getPersonByPersonId().getLastName());
				} else {
					discountOptionVO.setProductCode(option.getProduct()
							.getCode());
					discountOptionVO.setProductName(option.getProduct()
							.getProductName());
					if (option.getProductCategory() != null) {
						discountOptionVO.setProductCategoryName(option
								.getProductCategory().getCategoryName());
						if (option.getProductCategory().getProductCategory() != null) {
							discountOptionVO.setProductSubCategory(option
									.getProductCategory().getProductCategory()
									.getCategoryName());
						}
					}
				}
				discountOptionVOs.add(discountOptionVO);
			}

			discountVO.setDiscountOptionVOs(discountOptionVOs);

			ArrayList<DiscountMethodVO> discountMethodVOs = new ArrayList<DiscountMethodVO>();
			for (DiscountMethod method : discount.getDiscountMethods()) {

				DiscountMethodVO discountMethodVO = new DiscountMethodVO(method);

				discountMethodVOs.add(discountMethodVO);
			}
			discountVO.setDiscountMethodVOs(discountMethodVOs);

			discountVOs.add(discountVO);
		}

		return discountVOs;
	}

	public String getProductDiscountPrintOut() {
		try {

			List<DiscountVO> productDiscountList = this
					.fetchProductDiscountReportData();

			ServletActionContext.getRequest().setAttribute("DISCOUNT",
					productDiscountList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProductDiscountReportXLS() {
		try {

			List<DiscountVO> productDiscountList = this
					.fetchProductDiscountReportData();
			String str = "";

			for (DiscountVO discount : productDiscountList) {

				str += "\n Reference Number, " + ",, Discount Type , "
						+ discount.getDiscountTypeName() + "\n "
						+ "Discount Name , " + discount.getDiscountTo()
						+ ",, Pricing Type, " + discount.getPriceListName()
						+ "\n " + "Start Date, " + discount.getValidFrom()
						+ ",, Minium Sale, " + discount.getMinimumValue()
						+ "\n" + "End Date, " + discount.getValidTo()
						+ ",, Discount Option, "
						+ discount.getDiscountOptionName() + " \n\n ";

				if (discount.getDiscountOptionName().equals("Customer")) {
					str += "\n Customer Reference, Customer Name, Member Card Type, Member Card No# \n\n ";
					for (DiscountOptionVO option : discount
							.getDiscountOptionVOs()) {
						str += option.getCustomerReference() + ","
								+ option.getCustomerName() + ","
								+ option.getMemberCardType() + ","
								+ option.getMemberCardNo();
						str += "\n";
					}
				} else {
					str += "\n Product Code, Product Name, Category, Product SubCategory \n\n ";
					for (DiscountOptionVO option : discount
							.getDiscountOptionVOs()) {
						str += option.getProductCode() + ","
								+ option.getProductName() + ","
								+ option.getProductCategoryName() + ","
								+ option.getProductSubCategory();
						str += "\n";
					}
				}

				str += "\n Flat, Sales Amount, Discount \n\n ";
				for (DiscountMethodVO method : discount.getDiscountMethodVOs()) {
					str += method.getFlatAmount() + ","
							+ method.getSalesAmount() + ","
							+ method.getDiscountAmount();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getProductDiscountPDF() {
		String result = ERROR;
		try {
			productDiscountDS = this.fetchProductDiscountReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProductPromotionCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("PROMOTION_OPTIONS",
					AIOSCommons.getDiscountOptions());
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"CUSTOMER_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_CATEGORY_LIST",
					accountsEnterpriseService.getProductCategoryService()
							.getProductParentCategory(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getProductPromotionJsonList() {
		try {

			List<PromotionVO> promotionList = this
					.fetchProductPromotionReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", promotionList.size());
			jsonResponse.put("iTotalDisplayRecords", promotionList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PromotionVO list : promotionList) {
				array = new JSONArray();
				array.add(list.getPromotionId());
				array.add(list.getPromotionName());
				array.add(list.getRewardTypeName());
				array.add(list.getMininumSalesName());
				array.add(list.getSalesValue());
				array.add(list.getStatusName());
				array.add(list.getPromotionOptionName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PromotionVO> fetchProductPromotionReportData() throws Exception {

		getImplementId();

		Long promotionId = 0L;
		Byte discountOptionId = 0;
		Long productId = 0L;
		Long customerId = 0L;
		Long productCategoryId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("promotionId") != null) {
			promotionId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("promotionId"));
		}

		if (ServletActionContext.getRequest().getParameter("discountOptionId") != null) {
			discountOptionId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("discountOptionId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		if (ServletActionContext.getRequest().getParameter("productCategoryId") != null) {
			productCategoryId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("productCategoryId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Promotion> promotionList = accountsEnterpriseService
				.getPromotionService().getFilteredPromotions(implementation,
						promotionId, discountOptionId, productId, customerId,
						productCategoryId, fromDatee, toDatee);

		List<PromotionVO> promotionVOs = new ArrayList<PromotionVO>();

		for (Promotion promotion : promotionList) {
			PromotionVO promotionVO = new PromotionVO(promotion);

			promotionVO.setRewardTypeName(RewardType.get(
					promotion.getRewardType()).name());
			promotionVO.setStrEndDate(DateFormat.convertDateToString(promotion
					.getToDate().toString()));
			promotionVO.setStrStartDate(DateFormat
					.convertDateToString(promotion.getFromDate().toString()));
			promotionVO.setPriceListName(promotion.getLookupDetail()
					.getDisplayName());
			promotionVO.setPromotionOptionName(DiscountOptions.get(
					promotion.getPromotionOption()).name());

			/*
			 * discountVO.setPriceListName(discount.getLookupDetailByDiscountOnPrice
			 * ().getDisplayName());
			 * discountVO.setDiscountTo(discount.getLookupDetailByDiscountTo
			 * ().getDisplayName());
			 * discountVO.setValidFrom(DateFormat.convertDateToString
			 * (discount.getFromDate().toString()));
			 * discountVO.setValidTo(DateFormat
			 * .convertDateToString(discount.getToDate().toString()));
			 * discountVO
			 * .setDiscountTypeName(DiscountCalcMethod.get(discount.getDiscountType
			 * ()).name());
			 * discountVO.setDiscountOptionName(DiscountOptions.get(
			 * discount.getDiscountOption()).name());
			 */

			ArrayList<PromotionOptionVO> promotionOptionVOs = new ArrayList<PromotionOptionVO>();

			for (PromotionOption option : promotion.getPromotionOptions()) {

				PromotionOptionVO promotionOptionVO = new PromotionOptionVO(
						option);
				if (DiscountOptions.get(promotion.getPromotionOption()).name()
						.equals("Customer")) {
					promotionOptionVO.setCustomerReference(option.getCustomer()
							.getCustomerNumber());
					promotionOptionVO.setCustomerName(null != option
							.getCustomer().getPersonByPersonId() ? option
							.getCustomer()
							.getPersonByPersonId()
							.getFirstName()
							.concat("")
							.concat(option.getCustomer().getPersonByPersonId()
									.getLastName()) : (null != option
							.getCustomer().getCompany() ? option.getCustomer()
							.getCompany().getCompanyName() : option
							.getCustomer().getCmpDeptLocation().getCompany()
							.getCompanyName()));
					// promotionOptionVO.setCustomerName(option.getCustomer().getPersonByPersonId().getFirstName()
					// + " " +
					// option.getCustomer().getPersonByPersonId().getLastName());
				} else {
					promotionOptionVO.setProductCode(option.getProduct()
							.getCode());
					promotionOptionVO.setProductName(option.getProduct()
							.getProductName());
					if (option.getProductCategory() != null) {
						promotionOptionVO.setProductCategoryName(option
								.getProductCategory().getCategoryName());
						if (option.getProductCategory().getProductCategory() != null) {
							promotionOptionVO.setProductSubCategory(option
									.getProductCategory().getProductCategory()
									.getCategoryName());
						}
					}
				}
				promotionOptionVOs.add(promotionOptionVO);
			}
			promotionVO.setPromotionOptionVOs(promotionOptionVOs);

			ArrayList<PromotionMethodVO> promotionMethodVOs = new ArrayList<PromotionMethodVO>();
			for (PromotionMethod method : promotion.getPromotionMethods()) {

				PromotionMethodVO promotionMethodVO = new PromotionMethodVO(
						method);
				promotionMethodVO.setPromotionTypeName(PromotionType.get(
						method.getPacType()).name());
				promotionMethodVO.setCalculationTypeName("");
				if (method.getCoupon() != null) {
					promotionMethodVO.setCouponName(method.getCoupon()
							.getCouponName());
				}

				promotionMethodVOs.add(promotionMethodVO);
			}

			promotionVO.setPromotionMethodVOs(promotionMethodVOs);

			promotionVOs.add(promotionVO);
		}

		return promotionVOs;
	}

	public String getProductPromotionPrintOut() {
		try {

			List<PromotionVO> promotionList = this
					.fetchProductPromotionReportData();

			ServletActionContext.getRequest().setAttribute("PROMOTION",
					promotionList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProductPromotionReportXLS() {
		try {

			List<PromotionVO> promotionList = this
					.fetchProductPromotionReportData();
			String str = "";

			for (PromotionVO promotion : promotionList) {

				str += "\n Reference Number, " + ",, Reward Type , "
						+ promotion.getRewardTypeName() + "\n "
						+ "Promotion Name , " + promotion.getPromotionName()
						+ ",, Pricing Type, " + promotion.getPriceListName()
						+ "\n " + "Start Date, " + promotion.getStrStartDate()
						+ ",, Minium Sale, " + promotion.getMininumSalesName()
						+ "\n" + "End Date, " + promotion.getStrEndDate()
						+ ",, Promotion Option, "
						+ promotion.getPromotionOptionName() + " \n\n ";

				if (promotion.getPromotionOptionName().equals("Customer")) {
					str += "\n Customer Reference, Customer Name, Member Card Type, Member Card No# \n\n ";
					for (PromotionOptionVO option : promotion
							.getPromotionOptionVOs()) {
						str += option.getCustomerReference() + ","
								+ option.getCustomerName() + ","
								+ option.getMemberCardType() + ","
								+ option.getMemberCardNo();
						str += "\n";
					}
				} else {
					str += "\n Product Code, Product Name, Category, Product SubCategory \n\n ";
					for (PromotionOptionVO option : promotion
							.getPromotionOptionVOs()) {
						str += option.getProductCode() + ","
								+ option.getProductName() + ","
								+ option.getProductCategoryName() + ","
								+ option.getProductSubCategory();
						str += "\n";
					}
				}

				str += "\n Type, Calculation Type, Promotion, Coupon, Product Point \n\n ";
				for (PromotionMethodVO method : promotion
						.getPromotionMethodVOs()) {
					str += method.getPromotionTypeName() + ","
							+ method.getCalculationTypeName() + ","
							+ method.getPromotionAmount() + ","
							+ method.getCouponName() + ","
							+ method.getProductPoint();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getProductPromotionPDF() {
		String result = ERROR;
		try {
			promotionDS = this.fetchProductPromotionReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProductionReceiveCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION_SOURCE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PRODUCTION_SOURCE", true));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getProductionReceiveJsonList() {
		try {

			List<ProductionReceiveVO> productionReceiveList = this
					.fetchProductionReceiveReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", productionReceiveList.size());
			jsonResponse.put("iTotalDisplayRecords",
					productionReceiveList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (ProductionReceiveVO list : productionReceiveList) {
				array = new JSONArray();
				array.add(list.getProductionReceiveId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getSource());
				array.add(list.getReceivePerson());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<ProductionReceiveVO> fetchProductionReceiveReportData()
			throws Exception {

		getImplementId();

		Integer productionReceiveId = 0;
		Long sourceId = 0L;
		Long personId = 0L;
		Long productId = 0L;
		Long storeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter(
				"productionReceiveId") != null) {
			productionReceiveId = Integer.parseInt(ServletActionContext
					.getRequest().getParameter("productionReceiveId"));
		}

		if (ServletActionContext.getRequest().getParameter("sourceId") != null) {
			sourceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("sourceId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		if (ServletActionContext.getRequest().getParameter("storeId") != null) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<ProductionReceive> productionReceiveList = accountsEnterpriseService
				.getProductionReceiveService().getFilteredProductionReceive(
						implementation);

		List<ProductionReceiveVO> productionReceiveVOs = new ArrayList<ProductionReceiveVO>();

		for (ProductionReceive production : productionReceiveList) {
			ProductionReceiveVO productionReceiveVO = new ProductionReceiveVO(
					production);

			productionReceiveVO
					.setDate(DateFormat.convertDateToString(production
							.getReceiveDate().toString()));
			productionReceiveVO.setSource(production.getLookupDetail()
					.getDisplayName());
			productionReceiveVO.setReceivePerson(production
					.getPersonByReceivePerson().getFirstName()
					+ " "
					+ production.getPersonByReceivePerson().getLastName());

			ArrayList<ProductionReceiveDetailVO> productionReceiveDetailVOs = new ArrayList<ProductionReceiveDetailVO>();

			for (ProductionReceiveDetail detail : production
					.getProductionReceiveDetails()) {

				ProductionReceiveDetailVO productionReceiveDetailVO = new ProductionReceiveDetailVO(
						detail);

				productionReceiveDetailVO.setProductName(detail.getProduct()
						.getCode()
						+ "["
						+ detail.getProduct().getProductName()
						+ " ]");
				productionReceiveDetailVO.setUnitName(detail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());

				String storeName = "";

				if (detail.getShelf() != null) {
					if (detail.getShelf().getShelf() != null) {
						if (detail.getShelf().getShelf().getAisle() != null) {
							if (detail.getShelf().getShelf().getAisle()
									.getStore() != null) {
								storeName = detail.getShelf().getShelf()
										.getAisle().getStore().getStoreName()
										+ " >> ";
							}
							storeName.concat(detail.getShelf().getShelf()
									.getAisle().getLookupDetail()
									.getDisplayName()
									+ " >> ");
						}
						storeName.concat(StoreBinType.get(
								detail.getShelf().getShelf().getShelfType())
								.name());
					}
					storeName.concat(StoreBinType.get(
							detail.getShelf().getShelfType()).name());
				}

				productionReceiveDetailVO.setStoreName(storeName);
				productionReceiveDetailVO.setTotal(detail.getQuantity()
						* detail.getUnitRate());

				productionReceiveDetailVOs.add(productionReceiveDetailVO);
			}
			productionReceiveVO
					.setProductionReceiveDetailVOs(productionReceiveDetailVOs);

			productionReceiveVOs.add(productionReceiveVO);
		}

		return productionReceiveVOs;
	}

	public String getProductionReceivePrintOut() {
		try {

			List<ProductionReceiveVO> productionReceiveList = this
					.fetchProductionReceiveReportData();

			ServletActionContext.getRequest().setAttribute(
					"PRODUCTION",
					productionReceiveList.size() > 0 ? productionReceiveList
							.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProductionReceiveReportXLS() {
		try {

			List<ProductionReceiveVO> productionReceiveList = this
					.fetchProductionReceiveReportData();
			String str = "";

			for (ProductionReceiveVO production : productionReceiveList) {

				str += "\n Reference Number, "
						+ production.getReferenceNumber()
						+ ", Receive Person , " + production.getReceivePerson()
						+ "\n " + "Receive Date , " + production.getDate()
						+ ",, Description, " + production.getDescription()
						+ "\n " + "Receive Source, " + production.getSource()
						+ " \n\n ";

				str += "\n Product, Unit, Store, Quantity, Unit Price, Total \n\n ";
				for (ProductionReceiveDetailVO detail : production
						.getProductionReceiveDetailVOs()) {
					str += detail.getProductName() + "," + detail.getUnitName()
							+ "," + detail.getStoreName() + ","
							+ detail.getQuantity() + "," + detail.getUnitRate()
							+ "," + detail.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getProductionReceivePDF() {
		String result = ERROR;
		try {
			productionReceiveDS = this.fetchProductionReceiveReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getMaterialTransferCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_TYPE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"MATERIAL_TRANSFER_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_REASON",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"MATERIAL_TRANSFER_REASON", true));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getMaterialTransferJsonList() {
		try {

			List<MaterialTransferVO> materialTransferList = this
					.fetchMaterialTransferReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", materialTransferList.size());
			jsonResponse.put("iTotalDisplayRecords",
					materialTransferList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (MaterialTransferVO list : materialTransferList) {
				array = new JSONArray();
				array.add(list.getMaterialTransferId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getTransferPerson());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<MaterialTransferVO> fetchMaterialTransferReportData()
			throws Exception {

		getImplementId();

		Long materialTransferId = 0L;
		Long personId = 0L;
		Long productId = 0L;
		Long fromStoreId = 0L;
		Long toStoreId = 0L;
		Long typeId = 0L;
		Long reasonId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest()
				.getParameter("materialTransferId") != null) {
			materialTransferId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("materialTransferId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		if (ServletActionContext.getRequest().getParameter("fromStoreId") != null) {
			fromStoreId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("fromStoreId"));
		}

		if (ServletActionContext.getRequest().getParameter("toStoreId") != null) {
			toStoreId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("toStoreId"));
		}

		if (ServletActionContext.getRequest().getParameter("typeId") != null) {
			typeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("typeId"));
		}

		if (ServletActionContext.getRequest().getParameter("reasonId") != null) {
			reasonId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("reasonId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<MaterialTransfer> materialTransferList = accountsEnterpriseService
				.getMaterialTransferService().getFilteredMaterialTransfer(
						implementation, materialTransferId, personId,
						productId, fromStoreId, toStoreId, typeId, reasonId,
						fromDatee, toDatee);

		List<MaterialTransferVO> materialTransferVOs = new ArrayList<MaterialTransferVO>();

		for (MaterialTransfer transfer : materialTransferList) {
			MaterialTransferVO materialTransferVO = new MaterialTransferVO(
					transfer);

			materialTransferVO.setDate(DateFormat.convertDateToString(transfer
					.getTransferDate().toString()));
			materialTransferVO.setTransferPerson(transfer
					.getPersonByTransferPerson().getFirstName()
					+ " "
					+ transfer.getPersonByTransferPerson().getLastName());

			ArrayList<MaterialTransferDetailVO> materialTransferDetailVOs = new ArrayList<MaterialTransferDetailVO>();

			for (MaterialTransferDetail detail : transfer
					.getMaterialTransferDetails()) {

				MaterialTransferDetailVO materialTransferDetailVO = new MaterialTransferDetailVO(
						detail);

				materialTransferDetailVO.setProductName(detail.getProduct()
						.getProductName());
				materialTransferDetailVO.setStoreFrom(detail
						.getShelfByFromShelfId().getShelf().getAisle()
						.getStore().getStoreName()
						+ " >> "
						+ detail.getShelfByFromShelfId().getShelf().getAisle()
								.getSectionName()
						+ " >> "
						+ detail.getShelfByFromShelfId().getShelf().getName()
						+ " >> " + detail.getShelfByFromShelfId().getName());
				materialTransferDetailVO.setStoreTo(materialTransferDetailVO
						.getShelfByShelfId().getShelf().getAisle().getStore()
						.getStoreName()
						+ " >> "
						+ materialTransferDetailVO.getShelfByShelfId()
								.getShelf().getAisle().getSectionName()
						+ " >> "
						+ materialTransferDetailVO.getShelfByShelfId()
								.getShelf().getName()
						+ " >> "
						+ materialTransferDetailVO.getShelfByShelfId()
								.getName());
				materialTransferDetailVO.setTransferReason(null != detail
						.getLookupDetailByTransferReason() ? detail
						.getLookupDetailByTransferReason().getDisplayName()
						: "");
				materialTransferDetailVO.setTransferType(detail
						.getLookupDetailByTransferType().getDisplayName());
				materialTransferDetailVO.setTotal(detail.getQuantity()
						* detail.getUnitRate());

				materialTransferDetailVOs.add(materialTransferDetailVO);
			}
			materialTransferVO
					.setMaterialTransferDetailVOs(materialTransferDetailVOs);

			materialTransferVOs.add(materialTransferVO);
		}

		return materialTransferVOs;
	}

	public String getMaterialTransferPrintOut() {
		try {

			List<MaterialTransferVO> materialTransferList = this
					.fetchMaterialTransferReportData();

			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_TRANSFER",
					materialTransferList.size() > 0 ? materialTransferList
							.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getMaterialTransferReportXLS() {
		try {

			List<MaterialTransferVO> materialTransferList = this
					.fetchMaterialTransferReportData();
			String str = "";

			for (MaterialTransferVO transfer : materialTransferList) {

				str += "\n Reference Number, " + transfer.getReferenceNumber()
						+ ", Transfer Person , " + transfer.getTransferPerson()
						+ "\n " + "Transfer Date , " + transfer.getDate()
						+ ",, Description, " + transfer.getDescription()
						+ " \n\n ";

				str += "\n Product, Store From, Transfer Type, Transfer Reason, Store To, Quantity, Unit Price, Total \n\n ";
				for (MaterialTransferDetailVO detail : transfer
						.getMaterialTransferDetailVOs()) {
					str += detail.getProductName() + ","
							+ detail.getStoreFrom() + ","
							+ detail.getTransferType() + ","
							+ detail.getTransferReason() + ","
							+ detail.getStoreTo() + "," + detail.getQuantity()
							+ "," + detail.getUnitRate() + ","
							+ detail.getTotal();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getMaterialTransferPDF() {
		String result = ERROR;
		try {
			materialTransferDS = this.fetchMaterialTransferReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getMaterialRequisitionCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute("STATUS",
					AIOSCommons.getRequisitionStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getMaterialRequisitionJsonList() {
		try {

			List<MaterialRequisitionVO> materialRequisitionList = this
					.fetchMaterialRequisitionReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", materialRequisitionList.size());
			jsonResponse.put("iTotalDisplayRecords",
					materialRequisitionList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (MaterialRequisitionVO list : materialRequisitionList) {
				array = new JSONArray();
				array.add(list.getMaterialRequisitionId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getStatus());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<MaterialRequisitionVO> fetchMaterialRequisitionReportData()
			throws Exception {

		getImplementId();

		Long materialRequisitionId = 0L;
		Byte statusId = 0;
		Long productId = 0L;
		Long storeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter(
				"materialRequisitionId") != null) {
			materialRequisitionId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("materialRequisitionId"));
		}

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("storeId") != null) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			statusId = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<MaterialRequisition> materialRequisitionList = accountsEnterpriseService
				.getMaterialRequisitionService()
				.getFilteredMaterialRequisition(implementation,
						materialRequisitionId, productId, storeId, statusId,
						fromDatee, toDatee);

		List<MaterialRequisitionVO> materialRequisitionVOs = new ArrayList<MaterialRequisitionVO>();

		for (MaterialRequisition requisition : materialRequisitionList) {
			MaterialRequisitionVO materialRequisitionVO = new MaterialRequisitionVO(
					requisition);

			materialRequisitionVO.setDate(DateFormat
					.convertDateToString(requisition.getRequisitionDate()
							.toString()));
			materialRequisitionVO.setStoreName(requisition.getStore()
					.getStoreName());
			materialRequisitionVO.setStatus(RequisitionStatus.get(
					requisition.getRequisitionStatus()).name());

			ArrayList<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();

			for (MaterialRequisitionDetail detail : requisition
					.getMaterialRequisitionDetails()) {

				MaterialRequisitionDetailVO materialRequisitionDetailVO = new MaterialRequisitionDetailVO(
						detail);

				materialRequisitionDetailVO.setProductName(detail.getProduct()
						.getCode()
						+ "[ "
						+ detail.getProduct().getProductName() + " ]");
				materialRequisitionDetailVO.setUnitName(detail.getProduct()
						.getLookupDetailByProductUnit().getDisplayName());

				materialRequisitionDetailVOs.add(materialRequisitionDetailVO);
			}
			materialRequisitionVO
					.setMaterialRequisitionDetailVOs(materialRequisitionDetailVOs);

			materialRequisitionVOs.add(materialRequisitionVO);
		}

		return materialRequisitionVOs;
	}

	public String getMaterialRequisitionPrintOut() {
		try {

			List<MaterialRequisitionVO> materialRequisitionList = this
					.fetchMaterialRequisitionReportData();

			ServletActionContext
					.getRequest()
					.setAttribute(
							"MATERIAL_REQUISITION",
							materialRequisitionList.size() > 0 ? materialRequisitionList
									.get(0) : null);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getMaterialRequisitionReportXLS() {
		try {

			List<MaterialRequisitionVO> materialRequisitionList = this
					.fetchMaterialRequisitionReportData();
			String str = "";

			for (MaterialRequisitionVO requisition : materialRequisitionList) {

				str += "\n Reference Number, "
						+ requisition.getReferenceNumber() + ",, Store , "
						+ requisition.getStoreName() + "\n "
						+ "Requisition Date , " + requisition.getDate()
						+ ",, Description, " + requisition.getDescription()
						+ " \n\n ";

				str += "\n Product, Unit Name, Quantity \n\n ";
				for (MaterialRequisitionDetailVO detail : requisition
						.getMaterialRequisitionDetailVOs()) {
					str += detail.getProductName() + "," + detail.getUnitName()
							+ "," + detail.getQuantity();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getMaterialRequisitionPDF() {
		String result = ERROR;
		try {
			materialRequisitionDS = this.fetchMaterialRequisitionReportData();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getPOSSalesCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"TILL",
					accountsEnterpriseService.getpOSUserTillService()
							.getAllPosUserTill(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getPOSSalesPrintOut() {
		try {

			LocalDate currentDate = DateTime.now().toLocalDate();

			List<PointOfSaleVO> pointOfSales = this.fetchPOSSalesReportData();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
				PointOfSaleDetailVO pointOfSaleDetailVO = null;
				List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = null;
				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = null;
				PointOfSaleReceiptVO receiptVO = null;
				PointOfSaleChargeVO chargeVO = null;
				List<CreditDetailVO> creditDetailVOs = null;
				CreditDetailVO creditDetailVO = null;
				Credit creditNote = null;
				for (PointOfSaleVO pointOfSaleVO : pointOfSales) {
					creditNote = accountsEnterpriseService.getCreditService()
							.getCreditNoteByRecordIdUseCase(
									pointOfSaleVO.getPointOfSaleId(),
									PointOfSale.class.getSimpleName());
					if (null != creditNote) {
						creditDetailVOs = new ArrayList<CreditDetailVO>();
						for (CreditDetail creditDetail : creditNote
								.getCreditDetails()) {
							creditDetailVO = new CreditDetailVO();
							creditDetailVO.setProductName(creditDetail
									.getProduct().getProductName());
							creditDetailVO.setReturnAmount(creditDetail
									.getReturnAmount());
							creditDetailVO.setReturnQuantity(creditDetail
									.getReturnQuantity());
							creditDetailVO.setReferenceNumber(creditNote
									.getCreditNumber());
							creditDetailVO.setReturnDate(DateFormat
									.convertDateToString(creditNote.getDate()
											.toString()));
							if (null != creditNote.getIsDirectPayment()
									&& creditNote.getIsDirectPayment())
								creditDetailVO.setReturnType("Direct Payment");
							else
								creditDetailVO.setReturnType("Sales Exchange");
							creditDetailVOs.add(creditDetailVO);
						}
						pointOfSaleVO.setCreditDetailVOs(creditDetailVOs);
					}
					pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
					for (PointOfSaleDetail saleDetail : pointOfSaleVO
							.getPointOfSaleDetails()) {
						pointOfSaleDetailVO = new PointOfSaleDetailVO();
						BeanUtils.copyProperties(pointOfSaleDetailVO,
								saleDetail);
						pointOfSaleDetailVO.setProductName(saleDetail
								.getProductByProductId().getProductName());
						double discount = 0;
						double salesAmount = saleDetail.getQuantity()
								* saleDetail.getUnitRate();
						if (null != saleDetail.getIsPercentageDiscount()
								&& null != saleDetail.getDiscountValue()) {
							if (saleDetail.getIsPercentageDiscount()) {
								discount = ((salesAmount * saleDetail
										.getDiscountValue()) / 100);
							} else
								discount = saleDetail.getDiscountValue();
							salesAmount = salesAmount - discount;
							pointOfSaleDetailVO.setDiscountValue(AIOSCommons
									.roundDecimals(discount));
							pointOfSaleDetailVO.setDiscount(AIOSCommons
									.formatAmount(discount));
						}
						pointOfSaleDetailVO.setTotalPrice(AIOSCommons
								.formatAmount(salesAmount));
						pointOfSaleDetailVO.setTotalAmount(AIOSCommons
								.roundDecimals(salesAmount));
						pointOfSaleDetailVOs.add(pointOfSaleDetailVO);
					}
					pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
					pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();
					if (null != pointOfSaleVO.getPointOfSaleReceipts()) {
						for (PointOfSaleReceipt receipt : pointOfSaleVO
								.getPointOfSaleReceipts()) {
							receiptVO = new PointOfSaleReceiptVO();
							BeanUtils.copyProperties(receiptVO, receipt);
							receiptVO
									.setReceiptAmount(AIOSCommons.roundDecimals(receipt
											.getReceipt()
											- (null != receipt.getBalanceDue() ? receipt
													.getBalanceDue() : 0)));
							receiptVO
									.setReceiptAmountStr(AIOSCommons
											.formatAmount(receiptVO
													.getReceiptAmount()));
							receiptVO.setReceiptTypeStr(POSPaymentType.get(
									receipt.getReceiptType()).name());
							pointOfSaleReceiptVOs.add(receiptVO);
						}
						pointOfSaleVO
								.setPointOfSaleReceiptVOs(pointOfSaleReceiptVOs);
					}
					if (null != pointOfSaleVO.getPointOfSaleCharges()
							&& pointOfSaleVO.getPointOfSaleCharges().size() > 0) {
						pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
						for (PointOfSaleCharge charge : pointOfSaleVO
								.getPointOfSaleCharges()) {
							chargeVO = new PointOfSaleChargeVO();
							BeanUtils.copyProperties(chargeVO, charge);
							chargeVO.setChargeType(charge.getLookupDetail()
									.getDisplayName());
							chargeVO.setCharges(AIOSCommons
									.roundDecimals(charge.getCharges()));
							pointOfSaleChargeVOs.add(chargeVO);
						}
						pointOfSaleVO
								.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);
					}
				}
			}
			ServletActionContext.getRequest().setAttribute("POS", pointOfSales);
			ServletActionContext.getRequest().setAttribute("PRINT_DATE",
					DateFormat.convertDateToString(currentDate.toString()));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getPOSSalesJsonList() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			int count = 0;
			for (PointOfSaleVO list : pointOfSaleList) {
				array = new JSONArray();
				array.add(list.getPointOfSaleId());
				array.add(++count);
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getPosUserName());
				array.add(list.getSalesAmount());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PointOfSaleVO> fetchPOSSalesReportData() throws Exception {

		getImplementId();

		Long userTillId = 0L;
		Long personId = 0L;
		Long pointOfSaleId = 0L;
		Long storeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		} else {
			fromDatee = new Date();

		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		} else {
			toDatee = new Date();
		}

		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		} else {
			toDatee = new Date();
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null
				&& !ServletActionContext.getRequest().getParameter("personId")
						.equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"personId")) > 0) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		if (ServletActionContext.getRequest().getParameter("userTillId") != null
				&& !ServletActionContext.getRequest()
						.getParameter("userTillId").equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"userTillId")) > 0) {
			userTillId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("userTillId"));
		}

		if (ServletActionContext.getRequest().getParameter("pointOfSaleId") != null
				&& !ServletActionContext.getRequest()
						.getParameter("pointOfSaleId").equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"pointOfSaleId")) > 0) {
			pointOfSaleId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("pointOfSaleId"));
		}

		if (ServletActionContext.getRequest().getParameter("storeId") != null
				&& !ServletActionContext.getRequest().getParameter("storeId")
						.equals("")
				&& Integer.valueOf(ServletActionContext.getRequest()
						.getParameter("storeId")) > 0) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
		}

		List<PointOfSale> pointOfSalesList = accountsEnterpriseService
				.getPointOfSaleService().getFilteredSalesReport(implementation,
						userTillId, personId, storeId, fromDatee, toDatee,
						pointOfSaleId);

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();

		for (PointOfSale pos : pointOfSalesList) {
			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO(pos);

			pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
					.getSalesDate().toString()));
			if (pos.getPOSUserTill() != null) {
				if (pos.getPOSUserTill().getPersonByPersonId() != null) {
					pointOfSaleVO.setPosUserName(pos.getPOSUserTill()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getPOSUserTill().getPersonByPersonId()
									.getLastName());
				}
				pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
						.getStoreName());
			}
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
						detail);

				pointOfSaleDetailVO.setProductName(detail
						.getProductByProductId().getProductName());

				Double total = detail.getUnitRate() * detail.getQuantity();

				pointOfSaleDetailVO.setTotalAmount(total);
				pointOfSaleDetailVO.setTotalPrice(total.toString());
				if (detail.getDiscountValue() != null) {
					pointOfSaleDetailVO.setDiscount(detail.getDiscountValue()
							.toString());
					if (detail.getIsPercentageDiscount()) {
						pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
								.getDiscount().concat("%"));
					}
				}

				pointOfSaleDetailVOs.add(pointOfSaleDetailVO);

				totalQuantity += detail.getQuantity();
				totalAmount += total;

			}

			pointOfSaleVO.setTotalQuantity(totalQuantity);
			pointOfSaleVO.setTotalAmount(totalAmount);
			pointOfSaleVO.setSalesAmount(AIOSCommons.formatAmount(totalAmount));
			pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

			List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
			for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
				PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
						charge);
				pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());

				pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
			}
			pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

			pointOfSaleVOs.add(pointOfSaleVO);

		}
		if (null != pointOfSaleVOs && pointOfSaleVOs.size() > 0)
			Collections.sort(pointOfSaleVOs, new Comparator<PointOfSaleVO>() {
				public int compare(PointOfSaleVO s1, PointOfSaleVO s2) {
					return s1.getReferenceNumber().compareTo(
							s2.getReferenceNumber());
				}
			});
		return pointOfSaleVOs;
	}

	public String getPOSSalesCriteriaDataOld() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"TILL",
					accountsEnterpriseService.getpOSUserTillService()
							.getAllPosUserTill(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getCustomerPOSCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
					accountsBL.getCustomerBL().getAllCustomers(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getPOSSalesJsonListOld() {
		try {
			ServletActionContext.getRequest().setAttribute("reportType", "pos");

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportDataOld();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PointOfSaleVO list : pointOfSaleList) {
				array = new JSONArray();
				array.add(list.getPointOfSaleId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getPosUserName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getCustomerPOSJsonList() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchCustomerPOSReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PointOfSaleVO list : pointOfSaleList) {
				array = new JSONArray();
				array.add(list.getPointOfSaleId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getCustomerName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PointOfSaleVO> fetchPOSSalesReportDataOld() throws Exception {

		getImplementId();

		Long userTillId = 0L;
		Long personId = 0L;
		Long storeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		List<Long> pointOfSaleIds = null;
		String reportType = "";
		if (ServletActionContext.getRequest().getAttribute("reportType") != null) {
			reportType = (String) ServletActionContext.getRequest()
					.getAttribute("reportType");
			ServletActionContext.getRequest().removeAttribute("reportType");
		} else {
			reportType = ServletActionContext.getRequest().getParameter(
					"reportType");
		}

		getImplementId();

		PointOfSaleVO filterOptionView = new PointOfSaleVO();

		if (ServletActionContext.getRequest().getParameter("pointOfSaleIds") != null
				&& !ServletActionContext.getRequest()
						.getParameter("pointOfSaleIds").equals("")) {
			String strPointOfSaleIds = ServletActionContext.getRequest()
					.getParameter("pointOfSaleIds");

			String[] ids = strPointOfSaleIds.split(",");
			pointOfSaleIds = new ArrayList<Long>();
			for (int i = 0; i < ids.length; i++) {
				pointOfSaleIds.add(Long.parseLong(ids[i]));
			}

		}

		if (ServletActionContext.getRequest().getParameter("userTillId") != null
				&& !ServletActionContext.getRequest()
						.getParameter("userTillId").equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"userTillId")) > 0) {
			userTillId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("userTillId"));
			POSUserTill posUserTill = accountsEnterpriseService
					.getpOSUserTillService().getPosUserTillInformation(
							userTillId);
			filterOptionView.setTillNumber(posUserTill.getTillNumber());
		}

		if (ServletActionContext.getRequest().getParameter("storeId") != null
				&& !ServletActionContext.getRequest().getParameter("storeId")
						.equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"storeId")) > 0) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
			Store store = accountsEnterpriseService.getStoreService()
					.getStoreById(storeId);
			filterOptionView.setStoreName(store.getStoreName());
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null
				&& !ServletActionContext.getRequest().getParameter("personId")
						.equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"personId")) > 0) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
			Person person = accountsBL.getPersonBL().getPersonService()
					.getPersonDetails(personId);
			filterOptionView
					.setPosUserName((person.getPrefix() != null ? person
							.getPrefix() + " " : "")
							+ person.getFirstName()
							+ " "
							+ person.getLastName());
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
			filterOptionView.setFromDateView(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
			filterOptionView.setToDateView(toDate);
		}

		List<PointOfSale> pointOfSalesList = accountsEnterpriseService
				.getPointOfSaleService().getFilteredPointOfSales(
						implementation, pointOfSaleIds, userTillId, storeId,
						personId, fromDatee, toDatee, reportType);

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();

		Double cashReceived = 0.0;
		Double cardReceived = 0.0;
		Double chequeReceived = 0.0;
		Double couponReceived = 0.0;
		Double exchangeReceived = 0.0;

		Map<Long, ProductCategoryVO> categoryVOs = new HashMap<Long, ProductCategoryVO>();
		HashMap<Long, PointOfSaleDetailVO> groupedPointOfSaleDetailVO = new HashMap<Long, PointOfSaleDetailVO>();
		List<PointOfSaleChargeVO> allPointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();

		for (PointOfSale pos : pointOfSalesList) {
			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO(pos);

			pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
					.getSalesDate().toString()));

			if (pos.getPOSUserTill() != null) {

				if (pos.getPOSUserTill().getPersonByPersonId() != null) {
					pointOfSaleVO.setPosUserName(pos.getPOSUserTill()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getPOSUserTill().getPersonByPersonId()
									.getLastName());
				} else {
					pointOfSaleVO.setPosUserName("");
				}
				pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
						.getStoreName());
			} else {
				pointOfSaleVO.setStoreName("");
			}

			if (filterOptionView.getTotalDiscount() != null
					&& filterOptionView.getTotalDiscount() > 0.0) {
				filterOptionView.setTotalDiscount(filterOptionView
						.getTotalDiscount() + pos.getTotalDiscount());
			} else {
				filterOptionView.setTotalDiscount(pos.getTotalDiscount());
			}

			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;

			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
						detail);

				pointOfSaleDetailVO.setProductName(detail
						.getProductByProductId().getProductName());

				Double total = detail.getUnitRate() * detail.getQuantity();

				pointOfSaleDetailVO.setTotalAmount(total);
				pointOfSaleDetailVO.setTotalPrice(total.toString());
				pointOfSaleDetailVO.setDiscountDetailAmount(detail
						.getDiscountValue());
				pointOfSaleDetailVO.setTotalDetailAmount(total
						- (detail.getDiscountValue() != null ? detail
								.getDiscountValue() : 0));
				// pointOfSaleDetailVOs.add(pointOfSaleDetailVO);
				if (groupedPointOfSaleDetailVO.get(detail
						.getProductByProductId().getProductId()) != null) {
					PointOfSaleDetailVO existingRecord = groupedPointOfSaleDetailVO
							.get(detail.getProductByProductId().getProductId());
					existingRecord.setTotalAmount(existingRecord
							.getTotalAmount()
							+ pointOfSaleDetailVO.getTotalAmount());
					existingRecord.setTotalPrice(existingRecord
							.getTotalAmount().toString());
					existingRecord.setDiscountDetailAmount((existingRecord
							.getDiscountDetailAmount() != null ? existingRecord
							.getDiscountDetailAmount() : 0.0)
							+ (detail.getDiscountValue() != null ? detail
									.getDiscountValue() : 0.0));
					existingRecord
							.setTotalDetailAmount(existingRecord
									.getTotalDetailAmount()
									+ (total - (detail.getDiscountValue() != null ? detail
											.getDiscountValue() : 0)));
					groupedPointOfSaleDetailVO.put(detail
							.getProductByProductId().getProductId(),
							existingRecord);
				} else {
					groupedPointOfSaleDetailVO.put(detail
							.getProductByProductId().getProductId(),
							pointOfSaleDetailVO);
				}

				totalQuantity += detail.getQuantity();
				totalAmount += total;

				ProductCategoryVO productCatVo = null;
				if (categoryVOs.containsKey(detail.getProductByProductId()
						.getProductCategory().getProductCategoryId())) {
					productCatVo = categoryVOs.get(detail
							.getProductByProductId().getProductCategory()
							.getProductCategoryId());
					productCatVo.setProductAmount(productCatVo
							.getProductAmount() + total);
					productCatVo.setProductQuantity(productCatVo
							.getProductQuantity() + detail.getQuantity());
					productCatVo.setDiscountProductAmount((productCatVo
							.getDiscountProductAmount() != null ? productCatVo
							.getDiscountProductAmount() : 0.0)
							+ (detail.getDiscountValue() != null ? detail
									.getDiscountValue() : 0.0));
					productCatVo
							.setTotalProductAmount(productCatVo
									.getTotalProductAmount()
									+ (total - (detail.getDiscountValue() != null ? detail
											.getDiscountValue() : 0)));
				} else {
					productCatVo = new ProductCategoryVO();
					BeanUtils.copyProperties(productCatVo, detail
							.getProductByProductId().getProductCategory());
					productCatVo.setProductAmount(total);
					productCatVo.setProductQuantity(detail.getQuantity());

					productCatVo.setDiscountProductAmount(detail
							.getDiscountValue());
					productCatVo.setTotalProductAmount(total
							- (detail.getDiscountValue() != null ? detail
									.getDiscountValue() : 0));
					categoryVOs.put(detail.getProductByProductId()
							.getProductCategory().getProductCategoryId(),
							productCatVo);
				}
			}

			pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>(
					groupedPointOfSaleDetailVO.values());

			pointOfSaleVO.setTotalQuantity(totalQuantity);
			pointOfSaleVO.setTotalAmount(totalAmount);
			pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

			List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
			for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
				PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
						charge);
				pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());

				pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
			}
			pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);
			allPointOfSaleChargeVOs.addAll(pointOfSaleChargeVOs);

			if (pos.getCustomer() != null) {
				if (pos.getCustomer().getPersonByPersonId() != null) {
					pointOfSaleVO.setCustomerName(pos.getCustomer()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getCustomer().getPersonByPersonId()
									.getLastName());
				} else {
					pointOfSaleVO.setCustomerName("");
				}
				pointOfSaleVO.setCustomerNumber(pos.getCustomer()
						.getCustomerNumber());
				pointOfSaleVO.setCustomerType(CustomerType.get(
						pos.getCustomer().getCustomerType()).name());
			} else if (pos.getPersonByEmployeeId() != null) {
				pointOfSaleVO.setCustomerName(pos.getPersonByEmployeeId()
						.getFirstName()
						+ " "
						+ pos.getPersonByEmployeeId().getLastName());
				pointOfSaleVO.setCustomerNumber(pos.getPersonByEmployeeId()
						.getPersonNumber());
			}

			pointOfSaleVO.setSalesType(InvoiceSalesType.get(pos.getSaleType())
					.name());
			// POS Charges Type calculation
			for (PointOfSaleReceipt receipt : pos.getPointOfSaleReceipts()) {
				if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Cash
						.getCode())
					cashReceived += receipt.getReceipt()
							- (receipt.getBalanceDue() != null ? receipt
									.getBalanceDue() : 0d);
				if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Card
						.getCode())
					cardReceived += receipt.getReceipt();
				if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Cheque
						.getCode())
					chequeReceived += receipt.getReceipt();
				if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Coupon
						.getCode())
					couponReceived += receipt.getReceipt();
				if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Exchange
						.getCode())
					exchangeReceived += receipt.getReceipt();
			}

			pointOfSaleVOs.add(pointOfSaleVO);

		}

		if (cashReceived != null && cashReceived > 0.0)
			filterOptionView.setCashReceived(AIOSCommons
					.formatAmount(cashReceived));

		if (cardReceived != null && cardReceived > 0.0)
			filterOptionView.setCardReceived(AIOSCommons
					.formatAmount(cardReceived));

		if (chequeReceived != null && chequeReceived > 0.0)
			filterOptionView.setChequeReceived(AIOSCommons
					.formatAmount(chequeReceived));

		if (exchangeReceived != null && exchangeReceived > 0.0)
			filterOptionView.setExchangeReceived(AIOSCommons
					.formatAmount(exchangeReceived));

		if (couponReceived != null && couponReceived > 0.0)
			filterOptionView.setCouponReceived(AIOSCommons
					.formatAmount(couponReceived));

		filterOptionView.setDate(DateFormat
				.convertSystemDateToString(new Date())
				+ " "
				+ DateFormat.getTimeOnly(new Date()));

		if (pointOfSaleVOs.size() == 1) {
			PointOfSaleVO posVO = pointOfSaleVOs.get(0);
			filterOptionView.setReferenceNumber(posVO.getReferenceNumber());
			filterOptionView.setDate(posVO.getDate());
			filterOptionView.setSalesType(posVO.getSalesType());
			filterOptionView.setStoreName(posVO.getStoreName());
			filterOptionView.setTillNumber(posVO.getTillNumber());
			filterOptionView.setPosUserName(posVO.getPosUserName());
			filterOptionView.setCustomerName(posVO.getCustomerName());
			filterOptionView.setCustomerNumber(posVO.getCustomerNumber());
			filterOptionView.setViewType("Single Record");
		} else {
			filterOptionView.setViewType("Multiple Records");
		}

		ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
				filterOptionView);

		ServletActionContext.getRequest().setAttribute("POS_CATEGORY_SALES",
				new ArrayList<ProductCategoryVO>(categoryVOs.values()));
		ServletActionContext.getRequest().setAttribute(
				"POS_PRODUCT_SALES",
				new ArrayList<PointOfSaleDetailVO>(groupedPointOfSaleDetailVO
						.values()));
		ServletActionContext.getRequest().setAttribute("POS_CHARGES",
				allPointOfSaleChargeVOs);
		return pointOfSaleVOs;
	}

	public List<PointOfSaleVO> fetchCustomerPOSReportData() throws Exception {

		getImplementId();

		Long customerId = 0L;
		Long storeId = 0L;
		Date fromDatee = null;
		Date toDatee = null;
		List<Long> pointOfSaleIds = null;

		getImplementId();

		PointOfSaleVO filterOptionView = new PointOfSaleVO();

		if (ServletActionContext.getRequest().getParameter("pointOfSaleIds") != null
				&& !ServletActionContext.getRequest()
						.getParameter("pointOfSaleIds").equals("")) {
			String strPointOfSaleIds = ServletActionContext.getRequest()
					.getParameter("pointOfSaleIds");

			String[] ids = strPointOfSaleIds.split(",");
			pointOfSaleIds = new ArrayList<Long>();
			for (int i = 0; i < ids.length; i++) {
				pointOfSaleIds.add(Long.parseLong(ids[i]));
			}

		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null
				&& !ServletActionContext.getRequest()
						.getParameter("customerId").equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"customerId")) > 0) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
			Customer customer = accountsBL.getAccountsEnterpriseService()
					.getCustomerService().getCustomerDetails(customerId);
			filterOptionView.setCustomerName(null != customer
					.getPersonByPersonId() ? customer.getPersonByPersonId()
					.getFirstName().concat(" ")
					.concat(customer.getPersonByPersonId().getLastName())
					: (null != customer.getCompany() ? customer.getCompany()
							.getCompanyName() : customer.getCmpDeptLocation()
							.getCompany().getCompanyName()));
			filterOptionView.setCustomerNumber(customer.getCustomerNumber());
		}

		if (ServletActionContext.getRequest().getParameter("storeId") != null
				&& !ServletActionContext.getRequest().getParameter("storeId")
						.equals("")
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"storeId")) > 0) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("storeId"));
			Store store = accountsEnterpriseService.getStoreService()
					.getStoreById(storeId);
			filterOptionView.setStoreName(store.getStoreName());
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
			filterOptionView.setFromDateView(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
			filterOptionView.setToDateView(toDate);
		}

		List<PointOfSale> pointOfSalesList = accountsEnterpriseService
				.getPointOfSaleService().getFilteredCustomerPointOfSales(
						implementation, pointOfSaleIds, customerId, storeId,
						fromDatee, toDatee);

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();

		Double cashReceived = 0.0;
		Double cardReceived = 0.0;
		Double chequeReceived = 0.0;
		Double couponReceived = 0.0;
		Double exchangeReceived = 0.0;

		Map<Long, ProductCategoryVO> categoryVOs = new HashMap<Long, ProductCategoryVO>();
		HashMap<Long, PointOfSaleDetailVO> groupedPointOfSaleDetailVO = new HashMap<Long, PointOfSaleDetailVO>();

		for (PointOfSale pos : pointOfSalesList) {
			if (null != pos.getCustomer()) {
				PointOfSaleVO pointOfSaleVO = new PointOfSaleVO(pos);

				pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
						.getSalesDate().toString()));
				pointOfSaleVO.setCustomerName(null != pos.getCustomer()
						.getPersonByPersonId() ? pos
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(pos.getCustomer().getPersonByPersonId()
								.getLastName()) : (null != pos.getCustomer()
						.getCompany() ? pos.getCustomer().getCompany()
						.getCompanyName() : pos.getCustomer()
						.getCmpDeptLocation().getCompany().getCompanyName()));
				if (pos.getPOSUserTill() != null
						&& pos.getPOSUserTill().getStore() != null) {
					pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
							.getStoreName());
				}

				if (filterOptionView.getTotalDiscount() != null
						&& filterOptionView.getTotalDiscount() > 0.0) {
					filterOptionView.setTotalDiscount(filterOptionView
							.getTotalDiscount() + pos.getTotalDiscount());
				} else {
					filterOptionView.setTotalDiscount(pos.getTotalDiscount());
				}

				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
				Double totalQuantity = 0.0;
				Double totalAmount = 0.0;
				for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
					PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
							detail);

					pointOfSaleDetailVO.setProductName(detail
							.getProductByProductId().getProductName());

					Double total = detail.getUnitRate() * detail.getQuantity();

					pointOfSaleDetailVO.setTotalAmount(total);
					pointOfSaleDetailVO.setTotalPrice(total.toString());

					// pointOfSaleDetailVOs.add(pointOfSaleDetailVO);
					if (groupedPointOfSaleDetailVO.get(detail
							.getProductByProductId().getProductId()) != null) {
						PointOfSaleDetailVO existingRecord = groupedPointOfSaleDetailVO
								.get(detail.getProductByProductId()
										.getProductId());
						existingRecord.setTotalAmount(existingRecord
								.getTotalAmount()
								+ pointOfSaleDetailVO.getTotalAmount());
						existingRecord.setTotalPrice(existingRecord
								.getTotalAmount().toString());
						groupedPointOfSaleDetailVO.put(detail
								.getProductByProductId().getProductId(),
								existingRecord);
					} else {
						groupedPointOfSaleDetailVO.put(detail
								.getProductByProductId().getProductId(),
								pointOfSaleDetailVO);
					}

					totalQuantity += detail.getQuantity();
					totalAmount += total;

					ProductCategoryVO productCatVo = null;
					if (categoryVOs.containsKey(detail.getProductByProductId()
							.getProductCategory().getProductCategoryId())) {
						productCatVo = categoryVOs.get(detail
								.getProductByProductId().getProductCategory()
								.getProductCategoryId());
						productCatVo.setProductAmount(productCatVo
								.getProductAmount() + total);
						productCatVo.setDiscountProductAmount(productCatVo
								.getDiscountProductAmount()
								+ detail.getDiscountValue());
						productCatVo
								.setTotalProductAmount(productCatVo
										.getTotalProductAmount()
										+ (total - (detail.getDiscountValue() != null ? detail
												.getDiscountValue() : 0)));
						productCatVo.setProductQuantity(productCatVo
								.getProductQuantity() + detail.getQuantity());
					} else {
						productCatVo = new ProductCategoryVO();
						BeanUtils.copyProperties(productCatVo, detail
								.getProductByProductId().getProductCategory());
						productCatVo.setProductAmount(total);
						productCatVo.setProductQuantity(detail.getQuantity());
						productCatVo.setDiscountProductAmount(detail
								.getDiscountValue());
						productCatVo.setTotalProductAmount(total
								- (detail.getDiscountValue() != null ? detail
										.getDiscountValue() : 0));
						categoryVOs.put(detail.getProductByProductId()
								.getProductCategory().getProductCategoryId(),
								productCatVo);
					}
				}

				pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>(
						groupedPointOfSaleDetailVO.values());

				pointOfSaleVO.setTotalQuantity(totalQuantity);
				pointOfSaleVO.setTotalAmount(totalAmount);
				pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
				for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
					PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
							charge);
					pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
							.getDisplayName());

					pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
				}
				pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

				// POS Charges Type calculation
				for (PointOfSaleReceipt receipt : pos.getPointOfSaleReceipts()) {
					if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Cash
							.getCode())
						cashReceived += receipt.getReceipt()
								- (null != receipt.getBalanceDue() ? receipt
										.getBalanceDue() : 0);
					if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Card
							.getCode())
						cardReceived += receipt.getReceipt()
								- (null != receipt.getBalanceDue() ? receipt
										.getBalanceDue() : 0);
					;
					if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Cheque
							.getCode())
						chequeReceived += receipt.getReceipt()
								- (null != receipt.getBalanceDue() ? receipt
										.getBalanceDue() : 0);
					;
					if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Coupon
							.getCode())
						couponReceived += receipt.getReceipt()
								- (null != receipt.getBalanceDue() ? receipt
										.getBalanceDue() : 0);
					;
					if ((byte) receipt.getReceiptType() == (byte) Constants.Accounts.POSPaymentType.Exchange
							.getCode())
						exchangeReceived += receipt.getReceipt()
								- (null != receipt.getBalanceDue() ? receipt
										.getBalanceDue() : 0);
					;
				}

				pointOfSaleVOs.add(pointOfSaleVO);
			}

		}

		if (cashReceived != null && cashReceived > 0.0)
			filterOptionView.setCashReceived(AIOSCommons
					.formatAmount(cashReceived));

		if (cardReceived != null && cardReceived > 0.0)
			filterOptionView.setCardReceived(AIOSCommons
					.formatAmount(cardReceived));

		if (chequeReceived != null && chequeReceived > 0.0)
			filterOptionView.setChequeReceived(AIOSCommons
					.formatAmount(chequeReceived));

		if (exchangeReceived != null && exchangeReceived > 0.0)
			filterOptionView.setExchangeReceived(AIOSCommons
					.formatAmount(exchangeReceived));

		if (couponReceived != null && couponReceived > 0.0)
			filterOptionView.setCouponReceived(AIOSCommons
					.formatAmount(couponReceived));

		filterOptionView.setDate(DateFormat
				.convertSystemDateToString(new Date())
				+ " "
				+ DateFormat.getTimeOnly(new Date()));

		if (pointOfSaleVOs.size() == 1) {
			PointOfSaleVO posVO = pointOfSaleVOs.get(0);
			filterOptionView.setReferenceNumber(posVO.getReferenceNumber());
			filterOptionView.setDate(posVO.getDate());
			filterOptionView.setSalesType(posVO.getSalesType());
			filterOptionView.setStoreName(posVO.getStoreName());
			filterOptionView.setTillNumber(posVO.getTillNumber());
			filterOptionView.setPosUserName(posVO.getPosUserName());
			filterOptionView.setCustomerName(posVO.getCustomerName());
			filterOptionView.setCustomerNumber(posVO.getCustomerNumber());
			filterOptionView.setViewType("Single Record");
		} else {
			filterOptionView.setViewType("Multiple Records");
		}

		ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
				filterOptionView);

		ServletActionContext.getRequest().setAttribute("POS_CATEGORY_SALES",
				new ArrayList<ProductCategoryVO>(categoryVOs.values()));
		ServletActionContext.getRequest().setAttribute(
				"POS_PRODUCT_SALES",
				new ArrayList<PointOfSaleDetailVO>(groupedPointOfSaleDetailVO
						.values()));
		return pointOfSaleVOs;
	}

	public String getPOSSalesPrintOutOld() {
		try {

			ServletActionContext.getRequest().setAttribute("reportType", "pos");

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportDataOld();

			ServletActionContext.getRequest().setAttribute("POS",
					pointOfSaleList);
			ServletActionContext.getRequest().setAttribute(
					"VIEW_TYPE",
					pointOfSaleList.size() > 1 ? "Multiple Records"
							: "Single Record");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getPOSCustomerSalesPrintOut() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchCustomerPOSReportData();

			ServletActionContext.getRequest().setAttribute("POS",
					pointOfSaleList);
			ServletActionContext.getRequest().setAttribute(
					"VIEW_TYPE",
					pointOfSaleList.size() > 1 ? "Multiple Records"
							: "Single Record");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getPOSSalesReportXLS() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportDataOld();

			PointOfSaleVO filterOptionView = (PointOfSaleVO) ServletActionContext
					.getRequest().getAttribute("POINT_OF_SALE");
			/*
			 * ServletActionContext.getRequest().setAttribute("POINT_OF_SALE",
			 * filterOptionView);
			 * 
			 * ServletActionContext.getRequest().setAttribute("POS_CATEGORY_SALES"
			 * , new ArrayList<ProductCategoryVO>(categoryVOs.values()));
			 */

			String str = "";
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;

			if (pointOfSaleList.size() == 1) {
				str += "\n Reference No., "
						+ filterOptionView.getReferenceNumber()
						+ ", Paid By Cash  :, "
						+ filterOptionView.getCashReceived() + "\n ";
				str += "Date, " + filterOptionView.getDate()
						+ ", Paid By Card  :, "
						+ filterOptionView.getCardReceived() + "\n ";
				str += "Sale Type, " + filterOptionView.getSalesType()
						+ ", Paid By Cheque :, "
						+ filterOptionView.getChequeReceived() + "\n ";
				str += "Store , " + filterOptionView.getStoreName() + "\n ";
				str += "Till, " + filterOptionView.getTillNumber() + "\n ";
				str += "Cashier, " + filterOptionView.getPosUserName() + "\n ";
			} else if (pointOfSaleList.size() > 1) {
				str += "\n Date, " + filterOptionView.getFromDateView()
						+ " upto " + filterOptionView.getToDateView()
						+ ", Paid By Cash  :, "
						+ filterOptionView.getCashReceived() + "\n ";
				str += "Store , " + filterOptionView.getStoreName()
						+ ", Paid By Card  :, "
						+ filterOptionView.getCardReceived() + "\n ";
				str += "Till, " + filterOptionView.getTillNumber()
						+ ", Paid By Cheque :, "
						+ filterOptionView.getChequeReceived() + "\n ";
				str += "Cashier , " + filterOptionView.getPosUserName() + "\n ";
			}

			str += "\n Product, Quantity, Amount \n ";
			for (PointOfSaleVO pos : pointOfSaleList) {
				for (PointOfSaleDetailVO detail : pos.getPointOfSaleDetailVOs()) {
					str += detail.getProductName() + "," + detail.getQuantity()
							+ "," + detail.getTotalPrice();
					str += "\n";

					totalQuantity += detail.getQuantity();
					totalAmount += detail.getTotalAmount();
				}
			}

			str += "\n ,Total Quantity: " + totalQuantity + " , Total Amount: "
					+ totalAmount + " \n\n ";

			str += "\n Additional Charges \n\n ";
			str += "\n Charges Type, Amount \n ";

			totalAmount = 0.0;
			for (PointOfSaleVO pos : pointOfSaleList) {
				for (PointOfSaleChargeVO charge : pos.getPointOfSaleChargeVOs()) {
					str += charge.getChargeType() + "," + charge.getCharges();
					str += "\n";

					totalAmount += charge.getCharges();
				}
			}
			str += "\n ,, Total Amount: " + totalAmount + " \n\n ";

			str += "\n Customer Details \n\n ";
			str += "\n Customer Name, Customer Detail \n ";

			for (PointOfSaleVO pos : pointOfSaleList) {
				if (pos.getCustomerName() != null
						&& pos.getCustomerNumber() != null) {
					str += pos.getCustomerName() + ","
							+ pos.getCustomerNumber();
					str += "\n";
				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getPOSSalesPDF() {
		String result = ERROR;
		try {

			ServletActionContext.getRequest().setAttribute("reportType", "pos");

			List<PointOfSaleVO> pointOfSaleVOs = this
					.fetchPOSSalesReportDataOld();
			pointOfSaleDS = new ArrayList<PointOfSaleVO>();

			PointOfSaleVO pos = (PointOfSaleVO) ServletActionContext
					.getRequest().getAttribute("POINT_OF_SALE");

			pos.setProductCategoryVOs((List<ProductCategoryVO>) ServletActionContext
					.getRequest().getAttribute("POS_CATEGORY_SALES"));
			pos.setPointOfSaleDetailVOs((List<PointOfSaleDetailVO>) ServletActionContext
					.getRequest().getAttribute("POS_PRODUCT_SALES"));
			pos.setPointOfSaleChargeVOs((List<PointOfSaleChargeVO>) ServletActionContext
					.getRequest().getAttribute("POS_CHARGES"));
			pointOfSaleDS.add(pos);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getCustomerSalesPDF() {
		String result = ERROR;
		try {
			// pointOfSaleDS = this.fetchPOSSalesReportDataPDF();

			List<PointOfSaleVO> pointOfSaleVOs = this
					.fetchCustomerPOSReportData();
			pointOfSaleDS = new ArrayList<PointOfSaleVO>();

			PointOfSaleVO pos = (PointOfSaleVO) ServletActionContext
					.getRequest().getAttribute("POINT_OF_SALE");

			pos.setProductCategoryVOs((List<ProductCategoryVO>) ServletActionContext
					.getRequest().getAttribute("POS_CATEGORY_SALES"));
			pos.setPointOfSaleDetailVOs((List<PointOfSaleDetailVO>) ServletActionContext
					.getRequest().getAttribute("POS_PRODUCT_SALES"));
			pos.setPointOfSaleChargeVOs((List<PointOfSaleChargeVO>) ServletActionContext
					.getRequest().getAttribute("POS_CHARGES"));
			pointOfSaleDS.add(pos);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getEODBalancingCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			byte salesType = 0;
			ServletActionContext.getRequest().setAttribute(
					"TILL",
					accountsEnterpriseService.getpOSUserTillService()
							.getAllPosUserTill(implementation));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getFiscalTrialBalancePDF() {
		String result = ERROR;
		try {
			List<LedgerFiscal> ledgerFiscals = this.getAccountsBL()
					.getLedgerFiscalAgainstPeriod(periodId);
			accountsList = new ArrayList<AccountsTO>();
			if (null != ledgerFiscals && ledgerFiscals.size() > 0) {
				Collections.sort(ledgerFiscals, new Comparator<LedgerFiscal>() {
					@Override
					public int compare(LedgerFiscal l1, LedgerFiscal l2) {
						return l1.getLedgerFiscalId().compareTo(
								l2.getLedgerFiscalId());
					}
				});
				AccountsTO account = new AccountsTO();
				List<AccountsTO> debitCreditEntry = accountsBL
						.listAllDebitCreditFiscal(ledgerFiscals);
				account.setDebitCreditList(debitCreditEntry);

				account.setDebitBalance(AIOSCommons.formatAmount(accountsBL
						.calcucateDebit(account.getDebitCreditList())));
				account.setCreditBalance(AIOSCommons.formatAmount(accountsBL
						.calcucateCredit(account.getDebitCreditList())));
				account.setLinkLabel("HTML".equals(getFormat()) ? "Download"
						: "");
				account.setAnchorExpression("HTML".equals(getFormat()) ? "download_fiscaltrialbalance_report.action?format=PDF"
						: "#");
				accountsList.add(account);
			}
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			LOGGER.info("Inside Accounts method getFiscalTrialBalancePDF Success ");
			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getFiscalTrialBalanceXLS() {
		try {

			List<LedgerFiscal> ledgerFiscals = this.getAccountsBL()
					.getLedgerFiscalAgainstPeriod(periodId);
			if (null != ledgerFiscals && ledgerFiscals.size() > 0) {
				Collections.sort(ledgerFiscals, new Comparator<LedgerFiscal>() {
					@Override
					public int compare(LedgerFiscal l1, LedgerFiscal l2) {
						return l1.getLedgerFiscalId().compareTo(
								l2.getLedgerFiscalId());
					}
				});
				String str = "";

				List<AccountsTO> debitCreditEntry = accountsBL
						.listAllDebitCreditFiscal(ledgerFiscals);

				str = "\n FISCAL TRIAL BALANCE \n";

				str += "\n DESCRIPTION, ,DEBIT, CREDIT\n ";

				for (AccountsTO accountTO : debitCreditEntry) {
					str += accountTO.getAccountCode() + ",,";
					str += null != accountTO.getDebitBalanceTotal() ? accountTO
							.getDebitBalanceTotal().replaceAll(",", "") : ""
							+ ",";
					str += null != accountTO.getCreditBalanceTotal()
							.replaceAll(",", "") ? accountTO
							.getCreditBalanceTotal() : "";
					str += "\n";
				}
				str += "\n Total, , "
						+ accountsBL.calcucateDebit(debitCreditEntry) + ", "
						+ accountsBL.calcucateCredit(debitCreditEntry) + "\n ";

				InputStream is = new ByteArrayInputStream(str.getBytes());
				fileInputStream = is;
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method getFiscalTrialBalanceXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String showPOSConsolidatedSalesReport() {
		try {
			LOGGER.info("Inside Accounts method showPOSConsolidatedSalesReport()");
			getImplementId();
			List<Store> stores = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStores(implementation);
			ServletActionContext.getRequest()
					.setAttribute("STORE_LIST", stores);
			LOGGER.info("Inside Accounts method showPOSConsolidatedSalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showPOSConsolidatedSalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPOSConsolidatedYearMontlySalesReport() {
		try {
			LOGGER.info("Inside Accounts method showPOSConsolidatedYearMontlySalesReport");
			getImplementId();
			List<Store> stores = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStores(implementation);
			String[] months = new DateFormatSymbols().getMonths();
			Map<Integer, String> saleMonths = new HashMap<Integer, String>();
			java.util.Calendar now = java.util.Calendar.getInstance();
			int monthIndex = now.get(java.util.Calendar.MONTH);
			int counter = 0;
			for (String month : months) {
				saleMonths.put(++counter, month);
				if (counter > monthIndex)
					break;
			}
			int currentYear = now.get(java.util.Calendar.YEAR);
			List<String> saleYears = new ArrayList<String>();
			int tempYear = currentYear;
			for (int i = 0; i < 5; i++) {
				saleYears.add(String.valueOf(tempYear));
				tempYear -= 1;
			}
			ServletActionContext.getRequest()
					.setAttribute("STORE_LIST", stores);
			ServletActionContext.getRequest().setAttribute("SALE_MONTHS",
					saleMonths);
			ServletActionContext.getRequest().setAttribute("SALE_YEARS",
					saleYears);
			LOGGER.info("Inside Accounts method showPOSConsolidatedYearMontlySalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showPOSConsolidatedYearMontlySalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String getPOSConsolidatedSalesReport() {
		try {
			LOGGER.info("Inside Accounts method getPOSConsolidatedSalesReport()");
			getImplementId();
			Long storeId = 0l;
			String startDate = null;
			String toDate = null;
			if (ServletActionContext.getRequest().getParameter("store") != null)
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("store"));

			if (ServletActionContext.getRequest().getParameter("fromDate") != null
					&& !("").equals(ServletActionContext.getRequest()
							.getParameter("fromDate")))
				startDate = ServletActionContext.getRequest()
						.getParameter("fromDate").toString();

			if (ServletActionContext.getRequest().getParameter("toDate") != null
					&& !("").equals(ServletActionContext.getRequest()
							.getParameter("toDate")))
				toDate = ServletActionContext.getRequest()
						.getParameter("toDate").toString();

			LocalDate currentDate = DateTime.now().toLocalDate();
			if (startDate == null || ("").equals(startDate))
				startDate = DateFormat.convertDateToString(currentDate
						.toString());

			if (toDate == null || ("").equals(toDate))
				toDate = DateFormat.convertDateToString(currentDate.toString());

			List<Store> stores = null;
			if (storeId > 0) {
				Store store = accountsBL.getStockBL().getStoreBL()
						.getStoreService().getBasicStoreByStoreId(storeId);
				stores = new ArrayList<Store>();
				stores.add(store);
			} else
				stores = accountsBL.getStockBL().getStoreBL().getStoreService()
						.getStores(implementation);

			Map<String, List<PointOfSaleVO>> storeVOMap = new HashMap<String, List<PointOfSaleVO>>();
			Map<String, Map<Long, PointOfSaleDetailVO>> consolidatedSaleMap = new LinkedHashMap<String, Map<Long, PointOfSaleDetailVO>>();
			for (Store store : stores) {
				storeVOMap.put(store.getStoreName(), null);
				consolidatedSaleMap.put(store.getStoreName(), null);
			}
			String str = "";
			List<PointOfSale> pointOfSales = accountsBL
					.getPointOfSaleBL()
					.getPointOfSaleService()
					.getMonthlyPOSSales(implementation,
							DateFormat.convertStringToDate(startDate),
							DateFormat.convertStringToDate(toDate), storeId);
			Map<Long, String> productMapSet = new HashMap<Long, String>();
			if (null != pointOfSales && pointOfSales.size() > 0) {
				Map<Long, PointOfSaleDetailVO> tempSalesMap = null;
				PointOfSaleDetailVO pointOfSaleDetailVO = null;
				for (PointOfSale pointOfSale : pointOfSales) {
					if (consolidatedSaleMap.containsKey(pointOfSale
							.getPOSUserTill().getStore().getStoreName())) {
						tempSalesMap = consolidatedSaleMap.get(pointOfSale
								.getPOSUserTill().getStore().getStoreName());
						pointOfSaleDetailVO = new PointOfSaleDetailVO();
						if (null == tempSalesMap || tempSalesMap.size() == 0)
							tempSalesMap = new HashMap<Long, PointOfSaleDetailVO>();
						for (PointOfSaleDetail pointOfSaleDetail : pointOfSale
								.getPointOfSaleDetails()) {
							if (tempSalesMap.containsKey(pointOfSaleDetail
									.getProductByProductId().getProductId())) {
								pointOfSaleDetailVO = tempSalesMap
										.get(pointOfSaleDetail
												.getProductByProductId()
												.getProductId());
								pointOfSaleDetailVO
										.setQuantity(pointOfSaleDetailVO
												.getQuantity()
												+ pointOfSaleDetail
														.getQuantity());
							} else {
								pointOfSaleDetailVO = new PointOfSaleDetailVO();
								pointOfSaleDetailVO
										.setQuantity(pointOfSaleDetail
												.getQuantity());
							}
							pointOfSaleDetailVO
									.setProductName(pointOfSaleDetail
											.getProductByProductId()
											.getProductName());
							pointOfSaleDetailVO
									.setStoreName(pointOfSale.getPOSUserTill()
											.getStore().getStoreName());
							tempSalesMap.put(pointOfSaleDetail
									.getProductByProductId().getProductId(),
									pointOfSaleDetailVO);
							productMapSet.put(pointOfSaleDetail
									.getProductByProductId().getProductId(),
									pointOfSaleDetail.getProductByProductId()
											.getProductName()
											+ " ["
											+ pointOfSaleDetail
													.getProductByProductId()
													.getCode() + "]");
						}
						consolidatedSaleMap.put(pointOfSale.getPOSUserTill()
								.getStore().getStoreName(), tempSalesMap);
					}
				}

				str += ",," + implementation.getCompanyName()
						+ " - Consolidated Sales Summary from " + fromDate
						+ " to " + toDate;
				str += "\n";
				Map<String, List<PointOfSaleVO>> sortedStoreMap = new TreeMap<String, List<PointOfSaleVO>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});
				sortedStoreMap.putAll(storeVOMap);
				for (Entry<String, List<PointOfSaleVO>> storentry : sortedStoreMap
						.entrySet()) {
					str += "," + storentry.getKey();
				}

				Map<String, PointOfSaleDetailVO> storeProductMap = null;
				Map<String, PointOfSaleDetailVO> contentSortedMap = new TreeMap<String, PointOfSaleDetailVO>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});
				for (Entry<Long, String> productentry : productMapSet
						.entrySet()) {
					str += "\n";
					str += productentry.getValue();
					storeProductMap = new HashMap<String, PointOfSaleDetailVO>();
					for (Entry<String, Map<Long, PointOfSaleDetailVO>> entry : consolidatedSaleMap
							.entrySet()) {
						tempSalesMap = entry.getValue();
						if (null != tempSalesMap && tempSalesMap.size() > 0) {
							if (tempSalesMap.containsKey(productentry.getKey())) {
								storeProductMap
										.put(entry.getKey(), tempSalesMap
												.get(productentry.getKey()));
							} else {
								pointOfSaleDetailVO = new PointOfSaleDetailVO();
								pointOfSaleDetailVO.setQuantity(0d);
								pointOfSaleDetailVO
										.setStoreName(entry.getKey());
								storeProductMap.put(entry.getKey(),
										pointOfSaleDetailVO);
							}
						} else {
							pointOfSaleDetailVO = new PointOfSaleDetailVO();
							pointOfSaleDetailVO.setQuantity(0d);
							pointOfSaleDetailVO.setStoreName(entry.getKey());
							storeProductMap.put(entry.getKey(),
									pointOfSaleDetailVO);
						}
					}
					contentSortedMap.putAll(storeProductMap);
					for (Entry<String, PointOfSaleDetailVO> content : contentSortedMap
							.entrySet()) {
						str += ","
								+ AIOSCommons.roundDecimals(content.getValue()
										.getQuantity());
					}
				}
			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			LOGGER.info("Inside Accounts method getPOSConsolidatedSalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method getPOSConsolidatedSalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String getPOSConsolidatedYearMontlySalesReport() {
		try {
			LOGGER.info("Inside Accounts method getPOSConsolidatedMontlySalesReport");
			getImplementId();
			Long storeId = 0l;
			Integer monthIndex = 0;
			Integer yearIndex = 0;
			if (ServletActionContext.getRequest().getParameter("store") != null)
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("store"));
			if (ServletActionContext.getRequest().getParameter("month") != null)
				monthIndex = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("month"));
			if (ServletActionContext.getRequest().getParameter("year") != null)
				yearIndex = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("year"));

			java.util.Calendar now = java.util.Calendar.getInstance();
			int startMonth = 0;
			int endMonth = 0;
			if (monthIndex > 0) {
				startMonth = monthIndex;
				endMonth = monthIndex;
			} else {
				startMonth = java.util.Calendar.JANUARY;
				endMonth = java.util.Calendar.DECEMBER;
				startMonth += 1;
				endMonth += 1;
			}
			now.set(yearIndex, startMonth,
					(int) java.util.Calendar.DAY_OF_MONTH);
			LocalDate startYear = new LocalDate(
					now.get(java.util.Calendar.YEAR), startMonth, 1);
			LocalDate endYear = new LocalDate(now.get(java.util.Calendar.YEAR),
					endMonth, 31);

			String startYearStr = DateFormat.convertDateToString(startYear
					.toString());
			String endYearStr = DateFormat.convertDateToString(endYear
					.toString());

			List<PointOfSale> tempSales = null;
			String saleDate = "";
			double totalSales = 0;
			PointOfSaleVO pointOfSaleVO = null;
			List<PointOfSaleVO> storeSaleVO = null;

			List<Store> stores = null;
			if (storeId > 0) {
				Store store = accountsBL.getStockBL().getStoreBL()
						.getStoreService().getBasicStoreByStoreId(storeId);
				stores = new ArrayList<Store>();
				stores.add(store);
			} else
				stores = accountsBL.getStockBL().getStoreBL().getStoreService()
						.getStores(implementation);
			Map<String, List<PointOfSale>> posSalesVOMap = new HashMap<String, List<PointOfSale>>();
			Map<String, List<PointOfSaleVO>> storeVOMap = new HashMap<String, List<PointOfSaleVO>>();
			Set<String> allStoreSet = new HashSet<String>();
			for (Store store : stores) {
				posSalesVOMap.put(store.getStoreName(), null);
				storeVOMap.put(store.getStoreName(), null);
				allStoreSet.add(store.getStoreName());
			}
			tempSales = null;
			List<PointOfSale> pointOfSales = accountsBL
					.getPointOfSaleBL()
					.getPointOfSaleService()
					.getMonthlyPOSSales(implementation,
							DateFormat.convertStringToDate(startYearStr),
							DateFormat.convertStringToDate(endYearStr), storeId);
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale pointOfSale : pointOfSales) {
					if (posSalesVOMap.containsKey(pointOfSale.getPOSUserTill()
							.getStore().getStoreName())) {
						tempSales = posSalesVOMap.get(pointOfSale
								.getPOSUserTill().getStore().getStoreName());
						if (null != tempSales && tempSales.size() > 0)
							tempSales.add(pointOfSale);
						else {
							tempSales = new ArrayList<PointOfSale>();
							tempSales.add(pointOfSale);
						}
					} else {
						tempSales = new ArrayList<PointOfSale>();
						tempSales.add(pointOfSale);
					}
					posSalesVOMap.put(pointOfSale.getPOSUserTill().getStore()
							.getStoreName(), tempSales);
				}
				Map<String, List<PointOfSaleVO>> consolidatedSaleMap = new LinkedHashMap<String, List<PointOfSaleVO>>();
				String[] months = new DateFormatSymbols().getShortMonths();
				for (String month : months) {
					if (null != month && !("").equals(month))
						consolidatedSaleMap.put(month, null);
				}

				saleDate = "";
				String monthkey = "";
				totalSales = 0;
				pointOfSaleVO = null;
				storeSaleVO = null;
				String monthSpit = "";
				for (Entry<String, List<PointOfSaleVO>> salesentry : consolidatedSaleMap
						.entrySet()) {
					monthkey = salesentry.getKey();
					for (Entry<String, List<PointOfSale>> storentry : posSalesVOMap
							.entrySet()) {
						tempSales = new ArrayList<PointOfSale>();
						if (null != storentry.getValue()
								&& storentry.getValue().size() > 0) {
							for (PointOfSale sale : storentry.getValue()) {
								saleDate = DateFormat.convertDateToString(sale
										.getSalesDate().toString());
								monthSpit = saleDate.split("-")[1];
								if (monthSpit.equals(monthkey))
									tempSales.add(sale);
							}
						}
						if (null != tempSales && tempSales.size() > 0) {
							totalSales = 0;
							for (PointOfSale pointOfSale : tempSales) {
								for (PointOfSaleReceipt receipt : pointOfSale
										.getPointOfSaleReceipts()) {
									totalSales += receipt.getReceipt()
											- (null != receipt.getBalanceDue() ? receipt
													.getBalanceDue() : 0);
								}
							}
							pointOfSaleVO = new PointOfSaleVO();
							pointOfSaleVO.setStoreName(storentry.getKey());
							pointOfSaleVO.setTotalAmount(totalSales);

							if (consolidatedSaleMap.containsKey(salesentry
									.getKey())) {
								storeSaleVO = consolidatedSaleMap
										.get(salesentry.getKey());
							}
							if (null == storeSaleVO)
								storeSaleVO = new ArrayList<PointOfSaleVO>();
							storeSaleVO.add(pointOfSaleVO);
							consolidatedSaleMap.put(salesentry.getKey(),
									storeSaleVO);
						}
					}
					storeSaleVO = new ArrayList<PointOfSaleVO>();
					storeSaleVO = consolidatedSaleMap.get(salesentry.getKey());
					Set<String> includedSet = new HashSet<String>();
					if (null != storeSaleVO && storeSaleVO.size() > 0) {
						for (PointOfSaleVO sale : storeSaleVO) {
							includedSet.add(sale.getStoreName());
						}
					} else {
						storeSaleVO = new ArrayList<PointOfSaleVO>();
						for (String store : allStoreSet) {
							pointOfSaleVO = new PointOfSaleVO();
							pointOfSaleVO.setStoreName(store);
							pointOfSaleVO.setTotalAmount(0d);
							storeSaleVO.add(pointOfSaleVO);
							consolidatedSaleMap.put(salesentry.getKey(),
									storeSaleVO);
						}
					}
					if (null != includedSet && includedSet.size() > 0) {
						Collection<String> similar = new HashSet<String>(
								includedSet);
						Collection<String> different = new HashSet<String>();
						different.addAll(includedSet);
						different.addAll(allStoreSet);
						similar.retainAll(allStoreSet);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							for (String store : different) {
								pointOfSaleVO = new PointOfSaleVO();
								pointOfSaleVO.setStoreName(store);
								pointOfSaleVO.setTotalAmount(0d);
								storeSaleVO.add(pointOfSaleVO);
								consolidatedSaleMap.put(salesentry.getKey(),
										storeSaleVO);
							}
						}
					}
				}

				String str = "";
				str += ",,," + implementation.getCompanyName()
						+ " - Sales Summary "
						+ now.get(java.util.Calendar.YEAR);
				str += "\n Outlet Name";
				List<PointOfSaleVO> tempSaleVOs = null;
				List<Double> totalAmounts = new ArrayList<Double>();
				double totalAmount = 0;
				for (Entry<String, List<PointOfSaleVO>> salesentry : consolidatedSaleMap
						.entrySet()) {
					str += "," + salesentry.getKey() + "-"
							+ now.get(java.util.Calendar.YEAR);
					totalAmount = 0;
					for (PointOfSaleVO saleVO : salesentry.getValue()) {
						if (storeVOMap.containsKey(saleVO.getStoreName())) {
							tempSaleVOs = new ArrayList<PointOfSaleVO>();
							if (null != storeVOMap.get(saleVO.getStoreName())
									&& storeVOMap.get(saleVO.getStoreName())
											.size() > 0)
								tempSaleVOs.addAll(storeVOMap.get(saleVO
										.getStoreName()));
							tempSaleVOs.add(saleVO);
							storeVOMap.put(saleVO.getStoreName(), tempSaleVOs);
							totalAmount += saleVO.getTotalAmount();
						}
					}
					totalAmounts.add(totalAmount);
				}
				Map<String, List<PointOfSaleVO>> sortedStoreMap = new TreeMap<String, List<PointOfSaleVO>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});
				sortedStoreMap.putAll(storeVOMap);

				for (Entry<String, List<PointOfSaleVO>> entry : sortedStoreMap
						.entrySet()) {
					str += "\n" + entry.getKey() + ",";
					for (PointOfSaleVO saleVO : entry.getValue())
						str += AIOSCommons.roundDecimals(saleVO
								.getTotalAmount()) + ",";
				}
				str += "\n Total";
				for (Double amount : totalAmounts)
					str += "," + amount;

				InputStream is = new ByteArrayInputStream(str.getBytes());
				fileInputStream = is;
			}
			LOGGER.info("Inside Accounts method getPOSConsolidatedYearMontlySalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method getPOSConsolidatedYearMontlySalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String showPOSConsolidatedMontlySalesReport() {
		try {
			LOGGER.info("Inside Accounts method showPOSConsolidatedMontlySalesReport");
			getImplementId();
			List<Store> stores = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStores(implementation);
			String[] months = new DateFormatSymbols().getMonths();
			Map<Integer, String> saleMonths = new HashMap<Integer, String>();
			java.util.Calendar now = java.util.Calendar.getInstance();
			int monthIndex = now.get(java.util.Calendar.MONTH);
			int counter = 0;
			for (String month : months) {
				saleMonths.put(++counter, month);
				if (counter > monthIndex)
					break;
			}
			ServletActionContext.getRequest()
					.setAttribute("STORE_LIST", stores);
			ServletActionContext.getRequest().setAttribute("SALE_MONTHS",
					saleMonths);
			LOGGER.info("Inside Accounts method showPOSConsolidatedMontlySalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showPOSConsolidatedMontlySalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String getPOSConsolidatedMontlySalesReport() {
		try {
			LOGGER.info("Inside Accounts method getPOSConsolidatedMontlySalesReport");
			getImplementId();
			Long storeId = 0l;
			Integer monthIndex = 0;
			if (ServletActionContext.getRequest().getParameter("store") != null)
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("store"));
			if (ServletActionContext.getRequest().getParameter("month") != null)
				monthIndex = Integer.parseInt(ServletActionContext.getRequest()
						.getParameter("month"));
			java.util.Calendar now = java.util.Calendar.getInstance();
			LocalDate start = new LocalDate(now.get(java.util.Calendar.YEAR),
					monthIndex, 1);
			LocalDate currentDate = DateTime.now().toLocalDate();
			int filterMonth = start.getMonthOfYear();
			int currentMonth = currentDate.getMonthOfYear();
			LocalDate end = null;
			if (filterMonth == currentMonth)
				end = start.plusDays(Days.daysBetween(start, currentDate)
						.getDays());
			else
				end = start.dayOfMonth().withMaximumValue();
			String monthStart = DateFormat
					.convertDateToString(start.toString());
			String monthEnd = DateFormat.convertDateToString(end.toString());
			LocalDate previousMonth = start.minusMonths(1);
			LocalDate previousMonthEnd = previousMonth.dayOfMonth()
					.withMaximumValue();

			String previousMonthStr = DateFormat
					.convertDateToString(previousMonth.toString());
			String previousMonthEndStr = DateFormat
					.convertDateToString(previousMonthEnd.toString());

			List<PointOfSale> previousMonthSales = accountsBL
					.getPointOfSaleBL()
					.getPointOfSaleService()
					.getMonthlyPOSSales(
							implementation,
							DateFormat.convertStringToDate(previousMonthStr),
							DateFormat.convertStringToDate(previousMonthEndStr),
							storeId);
			Map<String, PointOfSaleVO> previousMonthconsolidatedSaleMap = null;
			List<PointOfSale> tempSales = null;
			String saleDate = "";
			String datekey = "";
			double totalSales = 0;
			PointOfSaleVO pointOfSaleVO = null;
			List<PointOfSaleVO> storeSaleVO = null;
			Integer daysInMonth = 0;
			if (null != previousMonthSales && previousMonthSales.size() > 0) {
				previousMonthconsolidatedSaleMap = new LinkedHashMap<String, PointOfSaleVO>();
				if (filterMonth == currentMonth)
					daysInMonth = (Days.daysBetween(start, currentDate)
							.getDays()) + 1;
				else
					daysInMonth = previousMonth.dayOfMonth().getMaximumValue();

				LocalDate tempDate = previousMonth;
				for (int i = 0; i < daysInMonth; i++) {
					previousMonthconsolidatedSaleMap
							.put(tempDate
									.dayOfWeek()
									.getAsText()
									.concat("##"
											+ DateFormat
													.convertDateToString(tempDate
															.toString())), null);
					tempDate = tempDate.plusDays(1);
				}
				for (Entry<String, PointOfSaleVO> salesentry : previousMonthconsolidatedSaleMap
						.entrySet()) {
					datekey = salesentry.getKey().split("##")[1];
					tempSales = new ArrayList<PointOfSale>();
					for (PointOfSale sale : previousMonthSales) {
						saleDate = DateFormat.convertDateToString(sale
								.getSalesDate().toString());
						if (saleDate.equals(datekey))
							tempSales.add(sale);
					}
					if (null != tempSales && tempSales.size() > 0) {
						totalSales = 0;
						for (PointOfSale pointOfSale : tempSales) {
							for (PointOfSaleReceipt receipt : pointOfSale
									.getPointOfSaleReceipts()) {
								totalSales += receipt.getReceipt()
										- (null != receipt.getBalanceDue() ? receipt
												.getBalanceDue() : 0);
							}
						}
						pointOfSaleVO = new PointOfSaleVO();
						pointOfSaleVO.setGrandTotal(totalSales);
						previousMonthconsolidatedSaleMap.put(
								salesentry.getKey(), pointOfSaleVO);
					}
				}
			}

			List<Store> stores = null;
			if (storeId > 0) {
				Store store = accountsBL.getStockBL().getStoreBL()
						.getStoreService().getBasicStoreByStoreId(storeId);
				stores = new ArrayList<Store>();
				stores.add(store);
			} else
				stores = accountsBL.getStockBL().getStoreBL().getStoreService()
						.getStores(implementation);
			Map<String, List<PointOfSale>> posSalesVOMap = new HashMap<String, List<PointOfSale>>();
			Set<String> allStoreSet = new HashSet<String>();
			for (Store store : stores) {
				posSalesVOMap.put(store.getStoreName(), null);
				allStoreSet.add(store.getStoreName());
			}
			PointOfSaleVO salesVO = null;
			tempSales = null;
			List<PointOfSale> pointOfSales = accountsBL
					.getPointOfSaleBL()
					.getPointOfSaleService()
					.getMonthlyPOSSales(implementation,
							DateFormat.convertStringToDate(monthStart),
							DateFormat.convertStringToDate(monthEnd), storeId);
			if (null != pointOfSales && pointOfSales.size() > 0) {
				for (PointOfSale pointOfSale : pointOfSales) {
					if (posSalesVOMap.containsKey(pointOfSale.getPOSUserTill()
							.getStore().getStoreName())) {
						tempSales = posSalesVOMap.get(pointOfSale
								.getPOSUserTill().getStore().getStoreName());
						if (null != tempSales && tempSales.size() > 0)
							tempSales.add(pointOfSale);
						else {
							tempSales = new ArrayList<PointOfSale>();
							tempSales.add(pointOfSale);
						}
					} else {
						tempSales = new ArrayList<PointOfSale>();
						tempSales.add(pointOfSale);
					}
					posSalesVOMap.put(pointOfSale.getPOSUserTill().getStore()
							.getStoreName(), tempSales);
				}
				Map<String, List<PointOfSaleVO>> consolidatedSaleMap = new LinkedHashMap<String, List<PointOfSaleVO>>();
				daysInMonth = 0;
				if (filterMonth == currentMonth)
					daysInMonth = (Days.daysBetween(start, currentDate)
							.getDays()) + 1;
				else
					daysInMonth = start.dayOfMonth().getMaximumValue();

				LocalDate tempDate = start;
				for (int i = 0; i < daysInMonth; i++) {
					consolidatedSaleMap
							.put(tempDate
									.dayOfWeek()
									.getAsText()
									.concat("##"
											+ DateFormat
													.convertDateToString(tempDate
															.toString())), null);
					tempDate = tempDate.plusDays(1);
				}

				saleDate = "";
				datekey = "";
				totalSales = 0;
				pointOfSaleVO = null;
				storeSaleVO = null;
				for (Entry<String, List<PointOfSaleVO>> salesentry : consolidatedSaleMap
						.entrySet()) {
					datekey = salesentry.getKey().split("##")[1];
					for (Entry<String, List<PointOfSale>> storentry : posSalesVOMap
							.entrySet()) {
						tempSales = new ArrayList<PointOfSale>();
						if (null != storentry.getValue()
								&& storentry.getValue().size() > 0) {
							for (PointOfSale sale : storentry.getValue()) {
								saleDate = DateFormat.convertDateToString(sale
										.getSalesDate().toString());
								if (saleDate.equals(datekey))
									tempSales.add(sale);
							}
						}
						if (null != tempSales && tempSales.size() > 0) {
							totalSales = 0;
							for (PointOfSale pointOfSale : tempSales) {
								for (PointOfSaleReceipt receipt : pointOfSale
										.getPointOfSaleReceipts()) {
									totalSales += receipt.getReceipt()
											- (null != receipt.getBalanceDue() ? receipt
													.getBalanceDue() : 0);
								}
							}
							pointOfSaleVO = new PointOfSaleVO();
							pointOfSaleVO.setStoreName(storentry.getKey());
							pointOfSaleVO.setTotalAmount(totalSales);

							if (consolidatedSaleMap.containsKey(salesentry
									.getKey())) {
								storeSaleVO = consolidatedSaleMap
										.get(salesentry.getKey());
							}
							if (null == storeSaleVO)
								storeSaleVO = new ArrayList<PointOfSaleVO>();
							storeSaleVO.add(pointOfSaleVO);
							consolidatedSaleMap.put(salesentry.getKey(),
									storeSaleVO);
						}
					}
					storeSaleVO = new ArrayList<PointOfSaleVO>();
					storeSaleVO = consolidatedSaleMap.get(salesentry.getKey());
					Set<String> includedSet = new HashSet<String>();
					if (null != storeSaleVO && storeSaleVO.size() > 0) {
						for (PointOfSaleVO sale : storeSaleVO) {
							includedSet.add(sale.getStoreName());
						}
					} else {
						storeSaleVO = new ArrayList<PointOfSaleVO>();
						for (String store : allStoreSet) {
							pointOfSaleVO = new PointOfSaleVO();
							pointOfSaleVO.setStoreName(store);
							pointOfSaleVO.setTotalAmount(0d);
							storeSaleVO.add(pointOfSaleVO);
							consolidatedSaleMap.put(salesentry.getKey(),
									storeSaleVO);
						}
					}
					if (null != includedSet && includedSet.size() > 0) {
						Collection<String> similar = new HashSet<String>(
								includedSet);
						Collection<String> different = new HashSet<String>();
						different.addAll(includedSet);
						different.addAll(allStoreSet);
						similar.retainAll(allStoreSet);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							for (String store : different) {
								pointOfSaleVO = new PointOfSaleVO();
								pointOfSaleVO.setStoreName(store);
								pointOfSaleVO.setTotalAmount(0d);
								storeSaleVO.add(pointOfSaleVO);
								consolidatedSaleMap.put(salesentry.getKey(),
										storeSaleVO);
							}
						}
					}
				}
				storeSaleVO = null;
				double consolidatedSales = 0;
				String pastDateKey = "";
				double pastDaySale = 0;
				DateTimeFormatter dtf = DateTimeFormat
						.forPattern("dd-MMM-yyyy");
				List<PointOfSaleVO> pastSales = null;
				for (Entry<String, List<PointOfSaleVO>> salesentry : consolidatedSaleMap
						.entrySet()) {
					datekey = salesentry.getKey();
					if (null != salesentry.getValue()
							&& salesentry.getValue().size() > 0) {
						totalSales = 0;
						pointOfSaleVO = new PointOfSaleVO();
						for (PointOfSaleVO salevo : salesentry.getValue())
							totalSales += salevo.getTotalAmount();
						pointOfSaleVO.setGrandTotal(totalSales);
						pointOfSaleVO.setStoreName("zzz");
						consolidatedSales += totalSales;
						pointOfSaleVO.setConsolidatedSales(consolidatedSales);
						tempDate = dtf.parseLocalDate(datekey.split("##")[1]);
						tempDate = tempDate.minusDays(1);
						pastDateKey = tempDate.dayOfWeek().getAsText()
								+ "##"
								+ DateFormat.convertDateToString(tempDate
										.toString());
						if (consolidatedSaleMap.containsKey(pastDateKey)) {
							pastSales = consolidatedSaleMap.get(pastDateKey);
							if (null != pastSales && pastSales.size() > 0) {
								Collections.sort(pastSales,
										new Comparator<PointOfSaleVO>() {
											public int compare(
													PointOfSaleVO o1,
													PointOfSaleVO o2) {
												return o1
														.getStoreName()
														.compareTo(
																o2.getStoreName());
											}
										});
								salesVO = pastSales.get(pastSales.size() - 1);
								pastDaySale = ((pointOfSaleVO.getGrandTotal() - salesVO
										.getGrandTotal()) / ((pointOfSaleVO
										.getGrandTotal() + salesVO
										.getGrandTotal()) / 2)) * 100;
								if (pastDaySale < 0)
									pointOfSaleVO
											.setDayAnalysis("-"
													+ String.valueOf(AIOSCommons.roundDecimals(Math
															.abs(pastDaySale))));
								else
									pointOfSaleVO
											.setDayAnalysis(String.valueOf(AIOSCommons
													.roundDecimals(pastDaySale)));
							} else
								pointOfSaleVO.setDayAnalysis("-N/A-");
						} else
							pointOfSaleVO.setDayAnalysis("-N/A-");

						tempDate = dtf.parseLocalDate(datekey.split("##")[1]);
						tempDate = tempDate.minusDays(7);
						pastDateKey = tempDate.dayOfWeek().getAsText()
								+ "##"
								+ DateFormat.convertDateToString(tempDate
										.toString());

						if (consolidatedSaleMap.containsKey(pastDateKey)) {
							pastSales = consolidatedSaleMap.get(pastDateKey);
							if (null != pastSales && pastSales.size() > 0) {
								Collections.sort(pastSales,
										new Comparator<PointOfSaleVO>() {
											public int compare(
													PointOfSaleVO o1,
													PointOfSaleVO o2) {
												return o1
														.getStoreName()
														.compareTo(
																o2.getStoreName());
											}
										});
								salesVO = pastSales.get(pastSales.size() - 1);
								pastDaySale = ((pointOfSaleVO.getGrandTotal() - salesVO
										.getGrandTotal()) / ((pointOfSaleVO
										.getGrandTotal() + salesVO
										.getGrandTotal()) / 2)) * 100;
								if (pastDaySale < 0)
									pointOfSaleVO
											.setWeekAnalysis("-"
													+ String.valueOf(AIOSCommons.roundDecimals(Math
															.abs(pastDaySale))));
								else
									pointOfSaleVO
											.setWeekAnalysis(String.valueOf(AIOSCommons
													.roundDecimals(pastDaySale)));
							} else
								pointOfSaleVO.setWeekAnalysis("-N/A-");
						} else
							pointOfSaleVO.setWeekAnalysis("-N/A-");

						tempDate = dtf.parseLocalDate(datekey.split("##")[1]);
						tempDate = tempDate.minusDays(previousMonth
								.dayOfMonth().getMaximumValue());
						pastDateKey = tempDate.dayOfWeek().getAsText()
								+ "##"
								+ DateFormat.convertDateToString(tempDate
										.toString());
						if (null != previousMonthconsolidatedSaleMap
								&& previousMonthconsolidatedSaleMap.size() > 0) {
							if (previousMonthconsolidatedSaleMap
									.containsKey(pastDateKey)) {
								salesVO = previousMonthconsolidatedSaleMap
										.get(pastDateKey);
								if (null != salesVO) {
									pastDaySale = ((pointOfSaleVO
											.getGrandTotal() - salesVO
											.getGrandTotal()) / ((pointOfSaleVO
											.getGrandTotal() + salesVO
											.getGrandTotal()) / 2)) * 100;
									if (pastDaySale < 0)
										pointOfSaleVO
												.setMonthAnalysis("-"
														+ String.valueOf(AIOSCommons
																.roundDecimals(Math
																		.abs(pastDaySale))));
									else
										pointOfSaleVO
												.setMonthAnalysis(String.valueOf(AIOSCommons
														.roundDecimals(pastDaySale)));
								} else
									pointOfSaleVO.setMonthAnalysis("-N/A-");
							} else
								pointOfSaleVO.setMonthAnalysis("-N/A-");
						} else
							pointOfSaleVO.setMonthAnalysis("-N/A-");

						if (consolidatedSaleMap
								.containsKey(salesentry.getKey()))
							storeSaleVO = consolidatedSaleMap.get(salesentry
									.getKey());
						storeSaleVO.add(pointOfSaleVO);
						consolidatedSaleMap.put(salesentry.getKey(),
								storeSaleVO);
					}
				}

				Map<String, List<PointOfSale>> sortedStoreMap = new TreeMap<String, List<PointOfSale>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});
				int counter = 0;
				sortedStoreMap.putAll(posSalesVOMap);
				String str = "";
				str += ","
						+ implementation.getCompanyName()
						+ " - Sales amounts for "
						+ start.monthOfYear().getAsText()
						+ " "
						+ java.util.Calendar.getInstance().get(
								java.util.Calendar.YEAR);
				for (int i = 0; i < sortedStoreMap.size(); i++)
					str += ",";
				str += ",,,Comparative % analysis of sales,,,";
				str += "\n";
				str += "Day,";

				for (Entry<String, List<PointOfSale>> storentry : sortedStoreMap
						.entrySet()) {
					str += "," + storentry.getKey();
					++counter;
				}
				str += ",Total Amount";
				str += ",Consolidated Sales";
				str += ",Current day with Previous day";
				str += ",Current Day with Previous Week day";
				str += ",Total Monthly sales with Previous month";
				int index = 0;
				int tempCounter = 0;
				List<String> totalSalesPerStore = new ArrayList<String>();
				for (Entry<String, List<PointOfSaleVO>> salesentry : consolidatedSaleMap
						.entrySet()) {
					str += "\n";
					str += salesentry.getKey().split("##")[0] + ","
							+ salesentry.getKey().split("##")[1] + ",";
					if (null != salesentry.getValue()
							&& salesentry.getValue().size() > 0) {
						storeSaleVO = new ArrayList<PointOfSaleVO>(
								salesentry.getValue());
						Collections.sort(storeSaleVO,
								new Comparator<PointOfSaleVO>() {
									public int compare(PointOfSaleVO o1,
											PointOfSaleVO o2) {
										return o1.getStoreName().compareTo(
												o2.getStoreName());
									}
								});
						index = 1;
						tempCounter = counter;
						totalSales = 0;
						int tempIndex = 1;
						for (PointOfSaleVO salevo : storeSaleVO) {
							if (index <= counter) {
								str += (null != salevo.getTotalAmount() ? salevo
										.getTotalAmount() : 0)
										+ ",";
								if (null != totalSalesPerStore
										&& totalSalesPerStore.size() > 0
										&& totalSalesPerStore.size() >= index) {
									tempIndex = index - 1;
									totalSales = null != totalSalesPerStore
											.get(tempIndex) ? Double
											.parseDouble(totalSalesPerStore
													.get(tempIndex))
											+ salevo.getTotalAmount() : salevo
											.getTotalAmount();
									totalSalesPerStore.set(tempIndex,
											String.valueOf(totalSales));
								} else {
									totalSalesPerStore.add(String
											.valueOf(salevo.getTotalAmount()));
								}
								index++;
							} else {
								++tempCounter;
								str += (null != salevo.getGrandTotal() ? salevo
										.getGrandTotal() : 0) + ",";
								str += (null != salevo.getConsolidatedSales() ? salevo
										.getConsolidatedSales() : 0)
										+ ",";
								str += (null != salevo.getDayAnalysis() ? salevo
										.getDayAnalysis() : 0)
										+ ",";
								str += (null != salevo.getWeekAnalysis() ? salevo
										.getWeekAnalysis() : 0)
										+ ",";
								str += (null != salevo.getMonthAnalysis() ? salevo
										.getMonthAnalysis() : 0)
										+ ",";
							}
						}
					}
				}
				str += "\n";
				str += "Total,,";

				daysInMonth = 0;
				if (filterMonth == currentMonth)
					daysInMonth = (Days.daysBetween(start, currentDate)
							.getDays()) + 1;
				else
					daysInMonth = start.dayOfMonth().getMaximumValue();

				List<String> averageSalesPerStore = new ArrayList<String>();
				for (String totalsale : totalSalesPerStore) {
					str += totalsale + ",";
					averageSalesPerStore.add(null != totalsale ? String
							.valueOf(AIOSCommons.roundDecimals(Double
									.parseDouble(totalsale) / daysInMonth))
							: "-N/A-");
				}
				str += "\n";
				str += "Average,,";
				for (String averagesale : averageSalesPerStore)
					str += averagesale + ",";
				InputStream is = new ByteArrayInputStream(str.getBytes());
				fileInputStream = is;
			}
			LOGGER.info("Inside Accounts method showPOSConsolidatedMontlySalesReport() success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Accounts method showPOSConsolidatedMontlySalesReport() caught exception "
					+ ex);
			return ERROR;
		}
	}

	public String getSummaraisedDailySales() {
		try {
			getImplementId();
			List<CustomerVO> customerVOs = accountsBL.getCustomerBL()
					.getAllCustomers(implementation);
			ServletActionContext.getRequest().setAttribute("CUSTOMERS",
					customerVOs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getSummaraisedDailySalesJson() {
		try {
			List<PointOfSaleVO> summaraisedSalesVOs = this
					.fetchSummaraisedDailySalesReport();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			if (null != summaraisedSalesVOs && summaraisedSalesVOs.size() > 0) {
				JSONArray array = null;
				for (PointOfSaleVO saleVO : summaraisedSalesVOs) {
					array = new JSONArray();
					array.add(saleVO.getRecordId());
					array.add(saleVO.getProductName());
					array.add(saleVO.getReferenceNumber());
					array.add(saleVO.getCustomerName());
					array.add(saleVO.getDate());
					array.add(saleVO.getStoreName());
					array.add(saleVO.getPosUserName());
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PointOfSaleVO> fetchSummaraisedDailySalesReport()
			throws Exception {

		Date fromDatee = null;
		Date toDatee = null;
		Long customerId = null;
		getImplementId();

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		} else {
			fromDatee = new Date();

		}

		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		} else {
			toDatee = new Date();
		}

		if (ServletActionContext.getRequest().getParameter("customerId") != null) {
			customerId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("customerId"));
		}

		List<PointOfSaleVO> pointOfSaleVOs = getDailyPointOfSaleData(fromDatee,
				toDatee, customerId);

		List<PointOfSaleVO> salesDeliveryVOs = getDailySalesDeliveryNoteData(
				fromDatee, toDatee, customerId);

		if (null != salesDeliveryVOs && salesDeliveryVOs.size() > 0) {
			if (null == pointOfSaleVOs || pointOfSaleVOs.size() == 0)
				pointOfSaleVOs = new ArrayList<PointOfSaleVO>();
			pointOfSaleVOs.addAll(salesDeliveryVOs);
		}

		List<PointOfSaleVO> projectInventoryVOs = getDailyProjectInventoryData(
				fromDatee, toDatee, customerId);

		if (null != projectInventoryVOs && projectInventoryVOs.size() > 0) {
			if (null == pointOfSaleVOs || pointOfSaleVOs.size() == 0)
				pointOfSaleVOs = new ArrayList<PointOfSaleVO>();
			pointOfSaleVOs.addAll(projectInventoryVOs);
		}

		return pointOfSaleVOs;
	}

	private List<PointOfSaleVO> getDailyProjectInventoryData(Date fromDate,
			Date toDate, Long customerId) throws Exception {
		List<Project> projects = accountsBL
				.getProjectBL()
				.getProjectService()
				.getFilteredProjects(implementation, null, customerId, null,
						null, null, null, null, fromDate, toDate);
		List<PointOfSaleVO> pointOfSaleDetails = null;
		if (null != projects && projects.size() > 0) {
			pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
			for (Project project : projects) {
				if (null != project.getProjectTaskSurrogates()
						&& project.getProjectTaskSurrogates().size() > 0) {
					for (ProjectTaskSurrogate taskSurrogate : project
							.getProjectTaskSurrogates()) {
						if (null != taskSurrogate.getProjectInventories()
								&& taskSurrogate.getProjectInventories().size() > 0) {
							pointOfSaleDetails
									.addAll(convertProjectInventorySalesToPosVO(
											project, taskSurrogate
													.getProjectInventories()));
						}
					}
				}
			}
		}
		return pointOfSaleDetails;
	}

	private List<PointOfSaleVO> getDailySalesDeliveryNoteData(Date fromDate,
			Date toDate, Long customerId) throws Exception {
		List<SalesDeliveryNote> salesDeliveryNotes = accountsEnterpriseService
				.getSalesDeliveryNoteService().getFilteredDeliveryNotes(
						implementation, null, customerId, null, fromDate,
						toDate, null);
		List<PointOfSaleVO> pointOfSaleDetails = null;
		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0) {
			pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
			for (SalesDeliveryNote salesDelivery : salesDeliveryNotes) {
				pointOfSaleDetails
						.addAll(convertSalesDeliveryToPosVO(salesDelivery));
			}
		}
		return pointOfSaleDetails;
	}

	private List<PointOfSaleVO> convertSalesDeliveryToPosVO(
			SalesDeliveryNote salesDelivery) {
		PointOfSaleVO saleVO = null;
		PointOfSaleDetail posSaleDetail = null;
		PointOfSaleCharge posSaleCharge = null;
		List<PointOfSaleVO> pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
		List<PointOfSaleDetail> posSaleDetails = new ArrayList<PointOfSaleDetail>();
		List<PointOfSaleCharge> posSaleCharges = null;
		for (SalesDeliveryDetail salesDetail : salesDelivery
				.getSalesDeliveryDetails()) {
			saleVO = new PointOfSaleVO();
			posSaleDetail = new PointOfSaleDetail();
			saleVO.setProductName(salesDetail.getProduct().getProductName());
			saleVO.setProductId(salesDetail.getProduct().getProductId());
			saleVO.setRecordId(salesDelivery.getSalesDeliveryNoteId());
			saleVO.setUseCaseId("SDN-".concat(salesDelivery
					.getSalesDeliveryNoteId().toString()));
			saleVO.setReferenceNumber(salesDelivery.getReferenceNumber());
			saleVO.setDate(DateFormat.convertDateToString(salesDelivery
					.getDeliveryDate().toString()));
			saleVO.setPosUserName(null != salesDelivery
					.getPersonBySalesPersonId() ? salesDelivery
					.getPersonBySalesPersonId()
					.getFirstName()
					.concat(" ")
					.concat(salesDelivery.getPersonBySalesPersonId()
							.getLastName()) : "");
			saleVO.setStoreName(salesDetail.getShelf().getShelf().getAisle()
					.getStore().getStoreName());
			saleVO.setCustomerName(null != salesDelivery.getCustomer()
					.getPersonByPersonId() ? salesDelivery
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(salesDelivery.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != salesDelivery
					.getCustomer().getCompany() ? salesDelivery.getCustomer()
					.getCompany().getCompanyName() : salesDelivery
					.getCustomer().getCmpDeptLocation().getCompany()
					.getCompanyName()));
			posSaleDetail.setProductByProductId(salesDetail.getProduct());
			posSaleDetail
					.setIsPercentageDiscount(salesDetail.getIsPercentage());
			posSaleDetail.setDiscountValue(salesDetail.getDiscount());
			posSaleDetail.setQuantity(salesDetail.getDeliveryQuantity());
			posSaleDetail.setUnitRate(salesDetail.getUnitRate());
			posSaleDetails.add(posSaleDetail);
			pointOfSaleDetails.add(saleVO);
		}

		if (null != salesDelivery.getSalesDeliveryCharges()
				&& salesDelivery.getSalesDeliveryCharges().size() > 0) {
			posSaleCharges = new ArrayList<PointOfSaleCharge>();
			for (SalesDeliveryCharge salesCharge : salesDelivery
					.getSalesDeliveryCharges()) {
				posSaleCharge = new PointOfSaleCharge();
				posSaleCharge.setCharges(salesCharge.getCharges());
				posSaleCharge.setLookupDetail(salesCharge.getLookupDetail());
				posSaleCharges.add(posSaleCharge);
			}
			for (PointOfSaleVO posVO : pointOfSaleDetails) {
				posVO.setPointOfSaleCharges(new HashSet<PointOfSaleCharge>(
						posSaleCharges));
			}
		}

		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleVO posVO : pointOfSaleDetails) {
				posVO.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
						posSaleDetails));
			}
		}
		return pointOfSaleDetails;
	}

	private List<PointOfSaleVO> convertProjectInventorySalesToPosVO(
			Project project, Set<ProjectInventory> projectInventories) {
		PointOfSaleVO saleVO = null;
		PointOfSaleDetail posSaleDetail = null;
		List<PointOfSaleVO> pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
		List<PointOfSaleDetail> posSaleDetails = new ArrayList<PointOfSaleDetail>();
		for (ProjectInventory projectInv : projectInventories) {
			saleVO = new PointOfSaleVO();
			posSaleDetail = new PointOfSaleDetail();
			saleVO.setProductName(projectInv.getProduct().getProductName());
			saleVO.setProductId(projectInv.getProduct().getProductId());
			saleVO.setRecordId(project.getProjectId());
			saleVO.setUseCaseId("PIS-"
					.concat(project.getProjectId().toString()));
			saleVO.setReferenceNumber(project.getReferenceNumber());
			saleVO.setDate(DateFormat.convertDateToString(project
					.getStartDate().toString()));
			saleVO.setPosUserName(null != project.getPersonByProjectLead() ? project
					.getPersonByProjectLead().getFirstName().concat(" ")
					.concat(project.getPersonByProjectLead().getLastName())
					: "");
			saleVO.setStoreName(projectInv.getShelf().getShelf().getAisle()
					.getStore().getStoreName());
			saleVO.setCustomerName(null != project.getCustomer()
					.getPersonByPersonId() ? project
					.getCustomer()
					.getPersonByPersonId()
					.getFirstName()
					.concat("")
					.concat(project.getCustomer().getPersonByPersonId()
							.getLastName()) : (null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyName() : project.getCustomer()
					.getCmpDeptLocation().getCompany().getCompanyName()));
			posSaleDetail.setProductByProductId(projectInv.getProduct());
			posSaleDetail.setIsPercentageDiscount(false); // Need to change
															// Check for
															// percentage or
															// amount
			posSaleDetail.setDiscountValue(projectInv.getDiscount());
			posSaleDetail.setQuantity(projectInv.getQuantity());
			posSaleDetail.setUnitRate(projectInv.getUnitRate());
			posSaleDetails.add(posSaleDetail);
			pointOfSaleDetails.add(saleVO);
		}
		if (null != pointOfSaleDetails && pointOfSaleDetails.size() > 0) {
			for (PointOfSaleVO posVO : pointOfSaleDetails) {
				posVO.setPointOfSaleDetails(new HashSet<PointOfSaleDetail>(
						posSaleDetails));
			}
		}
		return pointOfSaleDetails;
	}

	private List<PointOfSaleVO> getDailyPointOfSaleData(Date fromDate,
			Date toDate, Long customerId) throws Exception {
		List<PointOfSale> pointOfSales = accountsEnterpriseService
				.getPointOfSaleService().getFilteredDailySales(implementation,
						null, null, fromDate, toDate, customerId, null);
		List<PointOfSaleVO> pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
		PointOfSaleVO posVO = null;
		for (PointOfSale pointOfSale : pointOfSales) {
			for (PointOfSaleDetail posDetail : pointOfSale
					.getPointOfSaleDetails()) {
				posVO = new PointOfSaleVO();
				BeanUtils.copyProperties(posVO, pointOfSale);
				posVO.setProductName(posDetail.getProductByProductId()
						.getProductName());
				posVO.setProductId(posDetail.getProductByProductId()
						.getProductId());
				pointOfSaleDetails.add(posVO);
			}
		}

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();

		for (PointOfSaleVO pos : pointOfSaleDetails) {
			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO(pos);

			pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
					.getSalesDate().toString()));
			if (pos.getPOSUserTill() != null) {
				if (pos.getPOSUserTill().getPersonByPersonId() != null) {
					pointOfSaleVO.setPosUserName(pos.getPOSUserTill()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getPOSUserTill().getPersonByPersonId()
									.getLastName());
				}
				pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
						.getStoreName());
			}
			pointOfSaleVO.setProductName(pos.getProductName());
			if (null != pos.getCustomer()) {
				pointOfSaleVO.setCustomerName(null != pos.getCustomer()
						.getPersonByPersonId() ? pos
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat("")
						.concat(pos.getCustomer().getPersonByPersonId()
								.getLastName()) : (null != pos.getCustomer()
						.getCompany() ? pos.getCustomer().getCompany()
						.getCompanyName() : pos.getCustomer()
						.getCmpDeptLocation().getCompany().getCompanyName()));
			}
			pointOfSaleVO.setRecordId(pos.getPointOfSaleId());
			pointOfSaleVO.setUseCaseId("POS-".concat(pos.getPointOfSaleId()
					.toString()));
			pointOfSaleVO.setProductId(pos.getProductId());
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
						detail);

				pointOfSaleDetailVO.setProductName(detail
						.getProductByProductId().getProductName());

				Double total = detail.getUnitRate() * detail.getQuantity();

				pointOfSaleDetailVO.setTotalAmount(total);
				pointOfSaleDetailVO.setTotalPrice(total.toString());
				if (detail.getDiscountValue() != null) {
					pointOfSaleDetailVO.setDiscount(detail.getDiscountValue()
							.toString());
					if (detail.getIsPercentageDiscount()) {
						pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
								.getDiscount().concat("%"));
					}
				}

				pointOfSaleDetailVOs.add(pointOfSaleDetailVO);

				totalQuantity += detail.getQuantity();
				totalAmount += total;

			}

			pointOfSaleVO.setTotalQuantity(totalQuantity);
			pointOfSaleVO.setTotalAmount(totalAmount);
			pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

			List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
			for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
				PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
						charge);
				pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());

				pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
			}
			pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

			pointOfSaleVOs.add(pointOfSaleVO);

		}
		if (null != pointOfSaleVOs && pointOfSaleVOs.size() > 0)
			Collections.sort(pointOfSaleVOs, new Comparator<PointOfSaleVO>() {
				public int compare(PointOfSaleVO s1, PointOfSaleVO s2) {
					return s1.getReferenceNumber().compareTo(
							s2.getReferenceNumber());
				}
			});
		return pointOfSaleVOs;
	}

	public String getSummaraisedDailySalesPrintOut() {
		try {

			LocalDate currentDate = DateTime.now().toLocalDate();
			List<PointOfSaleVO> pointOfSaleList = this
					.fetchSummaraisedDailySalesReport();

			if (null != pointOfSaleList && pointOfSaleList.size() > 0) {

				Map<String, PointOfSaleVO> saleMap = new HashMap<String, PointOfSaleVO>();
				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
				List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = null;
				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = null;
				PointOfSaleReceiptVO receiptVO = null;
				PointOfSaleDetailVO saleDetailVO = null;
				PointOfSaleChargeVO chargeVO = null;
				Map<String, List<PointOfSaleVO>> saleDateMap = new HashMap<String, List<PointOfSaleVO>>();
				List<PointOfSaleVO> datePOS = null;

				for (PointOfSaleVO saleVO : pointOfSaleList) {

					if (!saleMap.containsKey(saleVO.getUseCaseId())) {

						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();

						for (PointOfSaleDetail saleDetail : saleVO
								.getPointOfSaleDetails()) {

							saleDetailVO = new PointOfSaleDetailVO();
							BeanUtils.copyProperties(saleDetailVO, saleDetail);
							saleDetailVO.setProductName(saleDetail
									.getProductByProductId().getProductName());
							double discount = 0;
							double salesAmount = saleDetail.getQuantity()
									* saleDetail.getUnitRate();

							if (null != saleDetail.getIsPercentageDiscount()
									&& null != saleDetail.getDiscountValue()) {

								if (saleDetail.getIsPercentageDiscount()) {
									discount = ((salesAmount * saleDetail
											.getDiscountValue()) / 100);
								} else
									discount = saleDetail.getDiscountValue();

								salesAmount = salesAmount - discount;

								saleDetailVO.setDiscountValue(AIOSCommons
										.roundDecimals(discount));
								saleDetailVO.setDiscount(AIOSCommons
										.formatAmount(discount));
							}

							saleDetailVO.setTotalPrice(AIOSCommons
									.formatAmount(salesAmount));
							saleDetailVO.setTotalAmount(AIOSCommons
									.roundDecimals(salesAmount));
							pointOfSaleDetailVOs.add(saleDetailVO);
						}

						saleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

						if (null != saleVO.getPointOfSaleReceipts()
								&& saleVO.getPointOfSaleReceipts().size() > 0) {

							pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();
							saleVO.setPosReceipt(true);

							for (PointOfSaleReceipt receipt : saleVO
									.getPointOfSaleReceipts()) {
								receiptVO = new PointOfSaleReceiptVO();
								BeanUtils.copyProperties(receiptVO, receipt);
								receiptVO
										.setReceiptAmount(AIOSCommons.roundDecimals(receipt
												.getReceipt()
												- (null != receipt
														.getBalanceDue() ? receipt
														.getBalanceDue() : 0)));
								receiptVO.setReceiptAmountStr(AIOSCommons
										.formatAmount(receiptVO
												.getReceiptAmount()));
								receiptVO.setReceiptTypeStr(POSPaymentType.get(
										receipt.getReceiptType()).name());
								pointOfSaleReceiptVOs.add(receiptVO);
							}
							saleVO.setPointOfSaleReceiptVOs(pointOfSaleReceiptVOs);
						}

						if (null != saleVO.getPointOfSaleCharges()
								&& saleVO.getPointOfSaleCharges().size() > 0) {

							pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
							saleVO.setPosCharge(true);

							for (PointOfSaleCharge charge : saleVO
									.getPointOfSaleCharges()) {

								chargeVO = new PointOfSaleChargeVO();
								BeanUtils.copyProperties(chargeVO, charge);
								chargeVO.setChargeType(charge.getLookupDetail()
										.getDisplayName());
								chargeVO.setCharges(AIOSCommons
										.roundDecimals(charge.getCharges()));
								pointOfSaleChargeVOs.add(chargeVO);
							}
							saleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

						}

						saleMap.put(saleVO.getUseCaseId(), saleVO);
						datePOS = new ArrayList<PointOfSaleVO>();

						if (saleDateMap.containsKey(saleVO.getDate())) {

							datePOS.addAll(saleDateMap.get(saleVO.getDate()));
						}

						datePOS.add(saleVO);
						saleDateMap.put(saleVO.getDate(), datePOS);
					}
				}
				if (null != saleDateMap && saleDateMap.size() > 0) {

					double totalUnitRate;
					double totalSales;
					double totalDiscount;
					double totalQuantity;
					double salesAmount;
					double totalReceipt;

					for (Entry<String, List<PointOfSaleVO>> saleVOs : saleDateMap
							.entrySet()) {

						totalUnitRate = 0;
						totalSales = 0;
						totalDiscount = 0;
						totalQuantity = 0;
						totalReceipt = 0;
						pointOfSaleList = new ArrayList<PointOfSaleVO>(
								saleVOs.getValue());

						for (PointOfSaleVO pointOfSaleVO : pointOfSaleList) {

							for (PointOfSaleDetailVO saleDetail : pointOfSaleVO
									.getPointOfSaleDetailVOs()) {
								salesAmount = 0;
								totalUnitRate += saleDetail.getUnitRate();
								totalQuantity += saleDetail.getQuantity();
								salesAmount = (saleDetail.getUnitRate() * saleDetail
										.getQuantity());

								if (null != saleDetail.getDiscountValue()) {
									salesAmount -= saleDetail
											.getDiscountValue();
								}
								totalSales += salesAmount;
								totalDiscount += saleDetail.getDiscountValue();
							}

							pointOfSaleVO.setTotalQuantity(AIOSCommons
									.roundDecimals(totalQuantity));
							pointOfSaleVO.setSalesAmount(AIOSCommons
									.formatAmount(totalSales));
							pointOfSaleVO.setCustomerDiscount(AIOSCommons
									.formatAmount(totalDiscount));
							pointOfSaleVO.setTotalDue(AIOSCommons
									.formatAmount(totalUnitRate));

							if (null != pointOfSaleVO
									.getPointOfSaleReceiptVOs()
									&& pointOfSaleVO.getPointOfSaleReceiptVOs()
											.size() > 0) {

								for (PointOfSaleReceiptVO saleReceipt : pointOfSaleVO
										.getPointOfSaleReceiptVOs()) {

									totalReceipt += saleReceipt
											.getReceiptAmount();
								}

								pointOfSaleVO.setReceivedTotal(AIOSCommons
										.formatAmount(totalReceipt));
							}
						}

						saleVOs.setValue(pointOfSaleList);
					}
				}

				Map<String, List<PointOfSaleVO>> sortedSalesMap = new TreeMap<String, List<PointOfSaleVO>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});

				sortedSalesMap.putAll(saleDateMap);

				ServletActionContext.getRequest().setAttribute("POS",
						sortedSalesMap);

			}

			ServletActionContext.getRequest().setAttribute("PRINT_DATE",
					DateFormat.convertDateToString(currentDate.toString()));
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getDailySalesDeliveryCriteriaData() {
		try {
			getImplementId();
			List<Store> store = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStores(implementation);
			ServletActionContext.getRequest().setAttribute("STORE_DETAIL",
					store);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getDailySalesDeliveryJsonList() {
		try {

			List<SalesDeliveryNoteVO> saleDeliveryNotes = this
					.fetchDailySalesDeliveryReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			int count = 0;
			if (null != saleDeliveryNotes && saleDeliveryNotes.size() > 0) {
				for (SalesDeliveryNoteVO list : saleDeliveryNotes) {
					array = new JSONArray();
					array.add(list.getSalesDeliveryNoteId());
					array.add(++count);
					array.add(list.getProductName());
					array.add(list.getReferenceNumber());
					array.add(list.getDispatchDate());
					array.add(list.getStoreName());
					array.add(list.getCustomerName());
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getDailySalesDeliveryReportXLS() {
		try {
			List<SalesDeliveryNoteVO> saleDeliveryNotes = this
					.fetchDailySalesDeliveryReportData();
			if (null != saleDeliveryNotes && saleDeliveryNotes.size() > 0) {
				getImplementId();
				Map<String, List<SalesDeliveryNoteVO>> sortedSalesMap = salesDeliveryDailySales(saleDeliveryNotes);
				StringBuilder str = new StringBuilder();
				str.append(",, SALES DELIVERY DAILY SALES\n");
				str.append(",,").append(implementation.getCompanyName());

				for (Entry<String, List<SalesDeliveryNoteVO>> pos : sortedSalesMap
						.entrySet()) {
					String totalQuantity = "";
					String salesAmount = "";
					String customerDiscount = "";
					String totalDue = "";
					str.append("\n SALES DATE: " + pos.getKey());
					str.append("\n REFERENCE, STORE, PRODUCT, QUANTITY, UNIT RATE, DISCOUNT, TOTAL \n ");
					for (SalesDeliveryNoteVO poshead : pos.getValue()) {
						for (SalesDeliveryDetailVO posdetail : poshead
								.getSalesDeliveryDetailVOs()) {
							str.append(poshead.getReferenceNumber().replaceAll(
									",", "")
									+ ","
									+ poshead.getStoreName()
											.replaceAll(",", "")
									+ ","
									+ posdetail.getProductName().replaceAll(
											",", "")
									+ ","
									+ posdetail.getPackageUnit()
									+ ","
									+ (posdetail.getUnitRate() + "")
											.replaceAll(",", "")
									+ ","
									+ getDiscountFromDetail(posdetail)
									+ ","
									+ (posdetail.getTotalRate() + "")
											.replaceAll(",", ""));
							str.append("\n");
						}

						totalQuantity = poshead.getTotalQuantity() + "";
						salesAmount = poshead.getSalesAmount();
						customerDiscount = poshead.getCustomerDiscount();
						totalDue = poshead.getTotalDue();
					}

					str.append(",,," + totalQuantity.replaceAll(",", "") + ","
							+ totalDue.replaceAll(",", "") + ","
							+ customerDiscount.replaceAll(",", "") + ","
							+ salesAmount.replaceAll(",", ""));

					boolean charges = false;
					for (SalesDeliveryNoteVO poshead : pos.getValue()) {
						if (poshead.getSalesCharge()) {
							charges = true;
							break;
						}
					}
					if (charges) {
						str.append("\n ADDITIONAL CHAEGES: " + pos.getKey());
						str.append("\n REFERENCE, CHARGES TYPE, AMOUNT \n ");

						Double chargesAmount = 0.0;

						for (SalesDeliveryNoteVO poshead : pos.getValue()) {
							for (SalesDeliveryChargeVO charge : poshead
									.getSalesDeliveryChargeVOs()) {
								str.append(poshead.getReferenceNumber()
										.replaceAll(",", "")
										+ ","
										+ charge.getStrChargeType().replaceAll(
												",", "")
										+ ","
										+ (charge.getCharges() + "")
												.replaceAll(",", ""));
								chargesAmount += charge.getCharges();
								str.append("\n");
							}
						}
						str.append(",TOTAL,"
								+ (chargesAmount + "").replaceAll(",", ""));
						str.append("\n");
					}
				}
				InputStream is = new ByteArrayInputStream(str.toString()
						.getBytes());
				fileInputStream = is;
				return SUCCESS;
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getDailySalesDeliveryPrintOut() {
		try {
			LocalDate currentDate = DateTime.now().toLocalDate();
			List<SalesDeliveryNoteVO> saleDeliveryNotes = this
					.fetchDailySalesDeliveryReportData();
			if (null != saleDeliveryNotes && saleDeliveryNotes.size() > 0) {
				Map<String, List<SalesDeliveryNoteVO>> sortedSalesMap = salesDeliveryDailySales(saleDeliveryNotes);
				ServletActionContext.getRequest().setAttribute("POS",
						sortedSalesMap);
			}
			ServletActionContext.getRequest().setAttribute("PRINT_DATE",
					DateFormat.convertDateToString(currentDate.toString()));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private Map<String, List<SalesDeliveryNoteVO>> salesDeliveryDailySales(
			List<SalesDeliveryNoteVO> saleDeliveryNotes)
			throws IllegalAccessException, InvocationTargetException {
		Map<Long, SalesDeliveryNoteVO> saleMap = new HashMap<Long, SalesDeliveryNoteVO>();
		List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = null;
		List<SalesDeliveryChargeVO> salesDeliveryChargeVOs = null;
		SalesDeliveryDetailVO saleDetailVO = null;
		SalesDeliveryChargeVO chargeVO = null;
		Map<String, List<SalesDeliveryNoteVO>> saleDateMap = new HashMap<String, List<SalesDeliveryNoteVO>>();
		List<SalesDeliveryNoteVO> datePOS = null;

		for (SalesDeliveryNoteVO saleVO : saleDeliveryNotes) {

			if (!saleMap.containsKey(saleVO.getSalesDeliveryNoteId())) {

				salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();

				for (SalesDeliveryDetail saleDetail : saleVO
						.getSalesDeliveryDetails()) {

					saleDetailVO = new SalesDeliveryDetailVO();
					BeanUtils.copyProperties(saleDetailVO, saleDetail);
					saleDetailVO.setProductName(saleDetail.getProduct()
							.getProductName());
					double discount = 0;
					double salesAmount = saleDetail.getPackageUnit()
							* saleDetail.getUnitRate();

					if (null != saleDetail.getIsPercentage()
							&& null != saleDetail.getDiscount()) {

						if (saleDetail.getIsPercentage())
							discount = ((salesAmount * saleDetail.getDiscount()) / 100);
						else
							discount = saleDetail.getDiscount();

						salesAmount = salesAmount - discount;

						saleDetailVO.setDiscount(AIOSCommons
								.roundDecimals(discount));
						saleDetailVO.setDisplayDiscount(AIOSCommons
								.formatAmount(discount));
					}

					saleDetailVO.setDisplayDiscount(AIOSCommons
							.formatAmount(discount));
					saleDetailVO.setTotalRate(AIOSCommons
							.roundDecimals(salesAmount));

					salesDeliveryDetailVOs.add(saleDetailVO);
				}

				saleVO.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

				if (null != saleVO.getSalesDeliveryChargeVOs()
						&& saleVO.getSalesDeliveryChargeVOs().size() > 0) {

					salesDeliveryChargeVOs = new ArrayList<SalesDeliveryChargeVO>();
					saleVO.setSalesCharge(true);

					for (SalesDeliveryCharge charge : saleVO
							.getSalesDeliveryCharges()) {

						chargeVO = new SalesDeliveryChargeVO();
						BeanUtils.copyProperties(chargeVO, charge);
						chargeVO.setStrChargeType(charge.getLookupDetail()
								.getDisplayName());
						chargeVO.setCharges(AIOSCommons.roundDecimals(charge
								.getCharges()));
						salesDeliveryChargeVOs.add(chargeVO);
					}
					saleVO.setSalesDeliveryChargeVOs(salesDeliveryChargeVOs);

				} else
					saleVO.setSalesCharge(false);

				saleMap.put(saleVO.getSalesDeliveryNoteId(), saleVO);
				datePOS = new ArrayList<SalesDeliveryNoteVO>();

				if (saleDateMap.containsKey(saleVO.getDispatchDate())) {
					datePOS.addAll(saleDateMap.get(saleVO.getDispatchDate()));
				}

				datePOS.add(saleVO);
				saleDateMap.put(saleVO.getDispatchDate(), datePOS);
			}
		}
		if (null != saleDateMap && saleDateMap.size() > 0) {

			double totalUnitRate;
			double totalSales;
			double totalDiscount;
			double totalQuantity;
			double salesAmount;

			for (Entry<String, List<SalesDeliveryNoteVO>> saleVOs : saleDateMap
					.entrySet()) {

				totalUnitRate = 0;
				totalSales = 0;
				totalDiscount = 0;
				totalQuantity = 0;
				saleDeliveryNotes = new ArrayList<SalesDeliveryNoteVO>(
						saleVOs.getValue());

				for (SalesDeliveryNoteVO saleDeliveryNote : saleDeliveryNotes) {

					for (SalesDeliveryDetailVO saleDetail : saleDeliveryNote
							.getSalesDeliveryDetailVOs()) {
						salesAmount = 0;
						totalUnitRate += saleDetail.getUnitRate();
						totalQuantity += saleDetail.getPackageUnit();
						salesAmount = (saleDetail.getUnitRate() * saleDetail
								.getPackageUnit());

						if (null != saleDetail.getDiscount()) {
							salesAmount -= saleDetail.getDiscount();
						}
						totalSales += salesAmount;
						totalDiscount += saleDetail.getDiscount();
					}

					saleDeliveryNote.setTotalQuantity(AIOSCommons
							.roundDecimals(totalQuantity));
					saleDeliveryNote.setSalesAmount(AIOSCommons
							.formatAmount(totalSales));
					saleDeliveryNote.setCustomerDiscount(AIOSCommons
							.formatAmount(totalDiscount));
					saleDeliveryNote.setTotalDue(AIOSCommons
							.formatAmount(totalUnitRate));

				}
				saleVOs.setValue(saleDeliveryNotes);
			}
		}
		Map<String, List<SalesDeliveryNoteVO>> sortedSalesMap = new TreeMap<String, List<SalesDeliveryNoteVO>>(
				new Comparator<String>() {
					public int compare(String o1, String o2) {
						return o1.compareTo(o2);
					}
				});
		sortedSalesMap.putAll(saleDateMap);
		return sortedSalesMap;
	}

	public List<SalesDeliveryNoteVO> fetchDailySalesDeliveryReportData()
			throws Exception {

		getImplementId();

		Date fromDatee = null;
		Date toDatee = null;
		Long storeId = 0L;
		getImplementId();

		if (ServletActionContext.getRequest().getParameter("selectedStoreId") != null) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("selectedStoreId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("") && !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		} else {
			fromDatee = new Date();

		}
		if (toDate != null && !toDate.equals("null") && !toDate.equals("")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		} else {
			toDatee = new Date();
		}

		List<SalesDeliveryNote> salesDeliveryNotes = accountsEnterpriseService
				.getSalesDeliveryNoteService().getFilteredDeliveryNotes(
						implementation, null, null, null, fromDatee, toDatee,
						storeId);

		List<SalesDeliveryNoteVO> salesDeliveryVOs = null;
		if (null != salesDeliveryNotes && salesDeliveryNotes.size() > 0) {
			List<SalesDeliveryNoteVO> salesDeliveryNoteVOs = new ArrayList<SalesDeliveryNoteVO>();
			Map<Long, SalesDeliveryNoteVO> posMap = null;
			SalesDeliveryNoteVO posVO = null;
			for (SalesDeliveryNote SalesDeliveryNote : salesDeliveryNotes) {
				posMap = new HashMap<Long, SalesDeliveryNoteVO>();
				for (SalesDeliveryDetail posDetail : SalesDeliveryNote
						.getSalesDeliveryDetails()) {
					if (!posMap.containsKey(posDetail.getProduct()
							.getProductId())) {
						posVO = new SalesDeliveryNoteVO();
						BeanUtils.copyProperties(posVO, SalesDeliveryNote);
						posVO.setProductName(posDetail.getProduct()
								.getProductName());
						posVO.setProductId(posDetail.getProduct()
								.getProductId());
						posVO.setStoreName(null != posDetail.getShelf() ? posDetail
								.getShelf().getShelf().getAisle().getStore()
								.getStoreName()
								+ ">>"
								+ posDetail.getShelf().getShelf().getAisle()
										.getSectionName()
								+ ">>"
								+ posDetail.getShelf().getShelf().getName()
								+ ">>" + posDetail.getShelf().getName()
								: "");
						salesDeliveryNoteVOs.add(posVO);
					}
				}
			}

			salesDeliveryVOs = new ArrayList<SalesDeliveryNoteVO>();
			SalesDeliveryDetailVO deliveryDetail = null;
			for (SalesDeliveryNoteVO pos : salesDeliveryNoteVOs) {
				posVO = new SalesDeliveryNoteVO();

				posVO.setDispatchDate(DateFormat.convertDateToString(pos
						.getDeliveryDate().toString()));
				if (null != pos.getCustomer().getPersonByPersonId())
					posVO.setCustomerName(pos.getCustomer()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getCustomer().getPersonByPersonId()
									.getLastName());
				else if (null != pos.getCustomer().getCompany())
					posVO.setCustomerName(pos.getCustomer().getCompany()
							.getCompanyName());
				else
					posVO.setCustomerName(pos.getCustomer()
							.getCmpDeptLocation().getCompany().getCompanyName());
				posVO.setProductName(pos.getProductName());
				posVO.setProductId(pos.getProductId());
				posVO.setSalesDeliveryNoteId(pos.getSalesDeliveryNoteId());
				posVO.setStoreName(pos.getStoreName());
				List<SalesDeliveryDetailVO> salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
				Double totalQuantity = 0.0;
				Double totalAmount = 0.0;
				for (SalesDeliveryDetail detail : pos.getSalesDeliveryDetails()) {
					deliveryDetail = new SalesDeliveryDetailVO();
					deliveryDetail.setProductName(detail.getProduct()
							.getProductName());

					Double total = detail.getUnitRate()
							* detail.getDeliveryQuantity();

					deliveryDetail.setTotalRate(AIOSCommons
							.roundDecimals(total));
					deliveryDetail.setDisplayAmount(AIOSCommons
							.formatAmount(total));
					if (detail.getDiscount() != null) {
						deliveryDetail.setDisplayDiscount(detail.getDiscount()
								.toString());
						if (detail.getIsPercentage())
							deliveryDetail.setDisplayDiscount(deliveryDetail
									.getDisplayDiscount().concat("%"));
					}

					salesDeliveryDetailVOs.add(deliveryDetail);

					totalQuantity += detail.getDeliveryQuantity();
					totalAmount += total;

				}

				posVO.setTotalDeliveryQty(totalQuantity);
				posVO.setDisplayAmount(AIOSCommons.formatAmount(totalAmount));
				posVO.setSalesDeliveryDetailVOs(salesDeliveryDetailVOs);

				List<SalesDeliveryChargeVO> salesDeliveryChargeVOs = new ArrayList<SalesDeliveryChargeVO>();
				SalesDeliveryChargeVO salesDeliveryChargeVO = null;
				for (SalesDeliveryCharge charge : pos.getSalesDeliveryCharges()) {
					salesDeliveryChargeVO = new SalesDeliveryChargeVO();
					salesDeliveryChargeVO.setStrChargeType(charge
							.getLookupDetail().getDisplayName());
					salesDeliveryChargeVOs.add(salesDeliveryChargeVO);
				}
				posVO.setSalesDeliveryChargeVOs(salesDeliveryChargeVOs);
				posVO.setReferenceNumber(pos.getReferenceNumber());
				posVO.setSalesDeliveryCharges(pos.getSalesDeliveryCharges());
				posVO.setSalesDeliveryDetails(pos.getSalesDeliveryDetails());
				salesDeliveryVOs.add(posVO);
			}
			if (null != salesDeliveryVOs && salesDeliveryVOs.size() > 0)
				Collections.sort(salesDeliveryVOs,
						new Comparator<SalesDeliveryNoteVO>() {
							public int compare(SalesDeliveryNoteVO o1,
									SalesDeliveryNoteVO o2) {
								return o1.getReferenceNumber().compareTo(
										o2.getReferenceNumber());
							}
						});
		}
		return salesDeliveryVOs;
	}

	public String getDailySalesCriteriaData() {
		try {
			getImplementId();
			Map<Byte, POSPaymentType> paymentType = new HashMap<Byte, POSPaymentType>();
			for (POSPaymentType e : EnumSet.allOf(POSPaymentType.class))
				paymentType.put(e.getCode(), e);

			ServletActionContext.getRequest().setAttribute("PAYMENT_TYPE",
					paymentType);
			ServletActionContext.getRequest().setAttribute(
					"TILL",
					accountsEnterpriseService.getpOSUserTillService()
							.getAllPosUserTill(implementation));
			List<Store> store = accountsBL.getStockBL().getStoreBL()
					.getStoreService().getStores(implementation);
			ServletActionContext.getRequest().setAttribute("STORE_DETAIL",
					store);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getDailySalesJsonList() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchDailySalesReportData();
			JSONObject jsonResponse = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray array = null;
			int count = 0;
			for (PointOfSaleVO list : pointOfSaleList) {
				array = new JSONArray();
				array.add(list.getPointOfSaleId());
				array.add(++count);
				array.add(list.getProductName());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getPosUserName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<PointOfSaleVO> fetchDailySalesReportData() throws Exception {

		getImplementId();

		Integer orderType = 0;
		Byte receiptType = 0;
		Date fromDatee = null;
		Date toDatee = null;
		Long storeId = 0L;
		getImplementId();

		if (ServletActionContext.getRequest().getParameter("orderType") != null) {
			orderType = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("orderType"));
		}

		if (ServletActionContext.getRequest().getParameter("selectedStoreId") != null) {
			storeId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("selectedStoreId"));
		}

		if (ServletActionContext.getRequest().getParameter("paymentType") != null) {
			receiptType = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("paymentType"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		} else {
			fromDatee = new Date();

		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		} else {
			toDatee = new Date();
		}

		List<PointOfSale> pointOfSalesList = accountsEnterpriseService
				.getPointOfSaleService().getFilteredDailySales(implementation,
						orderType, receiptType, fromDatee, toDatee, null,
						storeId);

		List<PointOfSaleVO> pointOfSaleDetails = new ArrayList<PointOfSaleVO>();
		Map<Long, PointOfSaleVO> posMap = null;
		PointOfSaleVO posVO = null;
		for (PointOfSale pointOfSale : pointOfSalesList) {
			posMap = new HashMap<Long, PointOfSaleVO>();
			for (PointOfSaleDetail posDetail : pointOfSale
					.getPointOfSaleDetails()) {
				if (!posMap.containsKey(posDetail.getProductByProductId())) {
					posVO = new PointOfSaleVO();
					BeanUtils.copyProperties(posVO, pointOfSale);
					posVO.setProductName(posDetail.getProductByProductId()
							.getProductName());
					posVO.setProductId(posDetail.getProductByProductId()
							.getProductId());
					pointOfSaleDetails.add(posVO);
				}
			}
		}

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();

		for (PointOfSaleVO pos : pointOfSaleDetails) {
			PointOfSaleVO pointOfSaleVO = new PointOfSaleVO(pos);

			pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
					.getSalesDate().toString()));
			if (pos.getPOSUserTill() != null) {
				if (pos.getPOSUserTill().getPersonByPersonId() != null) {
					pointOfSaleVO.setPosUserName(pos.getPOSUserTill()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getPOSUserTill().getPersonByPersonId()
									.getLastName());
				}
				pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
						.getStoreName());
			}
			pointOfSaleVO.setProductName(pos.getProductName());
			pointOfSaleVO.setProductId(pos.getProductId());
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
						detail);

				pointOfSaleDetailVO.setProductName(detail
						.getProductByProductId().getProductName());

				Double total = detail.getUnitRate() * detail.getQuantity();

				pointOfSaleDetailVO.setTotalAmount(total);
				pointOfSaleDetailVO.setTotalPrice(total.toString());
				if (detail.getDiscountValue() != null) {
					pointOfSaleDetailVO.setDiscount(detail.getDiscountValue()
							.toString());
					if (detail.getIsPercentageDiscount()) {
						pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
								.getDiscount().concat("%"));
					}
				}

				pointOfSaleDetailVOs.add(pointOfSaleDetailVO);

				totalQuantity += detail.getQuantity();
				totalAmount += total;

			}

			pointOfSaleVO.setTotalQuantity(totalQuantity);
			pointOfSaleVO.setTotalAmount(totalAmount);
			pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

			List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
			for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
				PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
						charge);
				pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());

				pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
			}
			pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

			pointOfSaleVOs.add(pointOfSaleVO);

		}
		if (null != pointOfSaleVOs && pointOfSaleVOs.size() > 0)
			Collections.sort(pointOfSaleVOs, new Comparator<PointOfSaleVO>() {
				public int compare(PointOfSaleVO s1, PointOfSaleVO s2) {
					return s1.getReferenceNumber().compareTo(
							s2.getReferenceNumber());
				}
			});
		return pointOfSaleVOs;
	}

	public String getO2CDailySalesPrintOut() {

		try {

			List<SalesInvoice> sales = accountsEnterpriseService
					.getSalesInvoiceService().getAllSalesInvoice(
							getImplementation());

			List<SalesInvoiceDetailVO> saleInvoiceDetailVOs = new ArrayList<SalesInvoiceDetailVO>();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getDailySalesPrintOut() {
		try {

			LocalDate currentDate = DateTime.now().toLocalDate();
			List<PointOfSaleVO> pointOfSaleList = this
					.fetchDailySalesReportData();

			if (null != pointOfSaleList && pointOfSaleList.size() > 0) {

				Map<Long, PointOfSaleVO> saleMap = new HashMap<Long, PointOfSaleVO>();
				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
				List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = null;
				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = null;
				PointOfSaleReceiptVO receiptVO = null;
				PointOfSaleDetailVO saleDetailVO = null;
				PointOfSaleChargeVO chargeVO = null;
				Map<String, List<PointOfSaleVO>> saleDateMap = new HashMap<String, List<PointOfSaleVO>>();
				List<PointOfSaleVO> datePOS = null;

				for (PointOfSaleVO saleVO : pointOfSaleList) {

					if (!saleMap.containsKey(saleVO.getPointOfSaleId())) {

						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();

						for (PointOfSaleDetail saleDetail : saleVO
								.getPointOfSaleDetails()) {

							saleDetailVO = new PointOfSaleDetailVO();
							BeanUtils.copyProperties(saleDetailVO, saleDetail);
							saleDetailVO.setProductName(saleDetail
									.getProductByProductId().getProductName());
							double discount = 0;
							double salesAmount = saleDetail.getQuantity()
									* saleDetail.getUnitRate();

							if (null != saleDetail.getIsPercentageDiscount()
									&& null != saleDetail.getDiscountValue()) {

								if (saleDetail.getIsPercentageDiscount()) {
									discount = ((salesAmount * saleDetail
											.getDiscountValue()) / 100);
								} else
									discount = saleDetail.getDiscountValue();

								salesAmount = salesAmount - discount;

								saleDetailVO.setDiscountValue(AIOSCommons
										.roundDecimals(discount));
								saleDetailVO.setDiscount(AIOSCommons
										.formatAmount(discount));
							}

							saleDetailVO.setTotalPrice(AIOSCommons
									.formatAmount(salesAmount));
							saleDetailVO.setTotalAmount(AIOSCommons
									.roundDecimals(salesAmount));

							try {
								ProductPricingDetail pricingDetail = accountsEnterpriseService
										.getProductPricingService()
										.getPricingDetailByProductId(
												saleDetail
														.getProductByProductId()
														.getProductId());

								if (null != pricingDetail
										&& null != pricingDetail
												.getBasicCostPrice())
									saleDetailVO
											.setProductCostPrice(pricingDetail
													.getBasicCostPrice()); // HERE
								else
									saleDetailVO.setProductCostPrice(0.0); // HERE

								saleDetailVO
										.setTotalGain(AIOSCommons.roundDecimals(salesAmount
												- (saleDetailVO
														.getProductCostPrice() * saleDetailVO
														.getQuantity())));

								saleDetailVO.setTotalCost(AIOSCommons
										.roundDecimals(saleDetailVO
												.getProductCostPrice()
												* saleDetailVO.getQuantity()));

							} catch (Exception e) {
								e.printStackTrace();
							}
							pointOfSaleDetailVOs.add(saleDetailVO);
						}

						saleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

						if (null != saleVO.getPointOfSaleReceipts()
								&& saleVO.getPointOfSaleReceipts().size() > 0) {

							pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();

							for (PointOfSaleReceipt receipt : saleVO
									.getPointOfSaleReceipts()) {
								receiptVO = new PointOfSaleReceiptVO();
								BeanUtils.copyProperties(receiptVO, receipt);
								receiptVO
										.setReceiptAmount(AIOSCommons.roundDecimals(receipt
												.getReceipt()
												- (null != receipt
														.getBalanceDue() ? receipt
														.getBalanceDue() : 0)));
								receiptVO.setReceiptAmountStr(AIOSCommons
										.formatAmount(receiptVO
												.getReceiptAmount()));
								receiptVO.setReceiptTypeStr(POSPaymentType.get(
										receipt.getReceiptType()).name());
								pointOfSaleReceiptVOs.add(receiptVO);
							}
							saleVO.setPointOfSaleReceiptVOs(pointOfSaleReceiptVOs);
						}

						if (null != saleVO.getPointOfSaleCharges()
								&& saleVO.getPointOfSaleCharges().size() > 0) {

							pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
							saleVO.setPosCharge(true);

							for (PointOfSaleCharge charge : saleVO
									.getPointOfSaleCharges()) {

								chargeVO = new PointOfSaleChargeVO();
								BeanUtils.copyProperties(chargeVO, charge);
								chargeVO.setChargeType(charge.getLookupDetail()
										.getDisplayName());
								chargeVO.setCharges(AIOSCommons
										.roundDecimals(charge.getCharges()));
								pointOfSaleChargeVOs.add(chargeVO);
							}
							saleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

						} else
							saleVO.setPosCharge(false);

						saleMap.put(saleVO.getPointOfSaleId(), saleVO);
						datePOS = new ArrayList<PointOfSaleVO>();

						if (saleDateMap.containsKey(saleVO.getDate())) {

							datePOS.addAll(saleDateMap.get(saleVO.getDate()));
						}

						datePOS.add(saleVO);
						saleDateMap.put(saleVO.getDate(), datePOS);
					}
				}
				PointOfSaleVO finalSaleVO = new PointOfSaleVO();
				if (null != saleDateMap && saleDateMap.size() > 0) {

					double totalUnitRate;
					double totalSales;
					double totalDiscount;
					double totalQuantity;
					double salesAmount;
					double totalReceipt;

					for (Entry<String, List<PointOfSaleVO>> saleVOs : saleDateMap
							.entrySet()) {

						totalUnitRate = 0;
						totalSales = 0;
						totalDiscount = 0;
						totalQuantity = 0;
						totalReceipt = 0;
						pointOfSaleList = new ArrayList<PointOfSaleVO>(
								saleVOs.getValue());

						for (PointOfSaleVO pointOfSaleVO : pointOfSaleList) {

							for (PointOfSaleDetailVO saleDetail : pointOfSaleVO
									.getPointOfSaleDetailVOs()) {
								salesAmount = 0;
								totalUnitRate += saleDetail.getUnitRate();
								totalQuantity += saleDetail.getQuantity();
								salesAmount = (saleDetail.getUnitRate() * saleDetail
										.getQuantity());

								if (null != saleDetail.getDiscountValue()) {
									salesAmount -= saleDetail
											.getDiscountValue();
								}
								totalSales += salesAmount;
								totalDiscount += saleDetail.getDiscountValue();
							}

							pointOfSaleVO.setTotalQuantity(AIOSCommons
									.roundDecimals(totalQuantity));
							pointOfSaleVO.setSalesAmount(AIOSCommons
									.formatAmount(totalSales));
							pointOfSaleVO.setCustomerDiscount(AIOSCommons
									.formatAmount(totalDiscount));
							pointOfSaleVO.setTotalDue(AIOSCommons
									.formatAmount(totalUnitRate));

							if (null != pointOfSaleVO
									.getPointOfSaleReceiptVOs()
									&& pointOfSaleVO.getPointOfSaleReceiptVOs()
											.size() > 0) {

								for (PointOfSaleReceiptVO saleReceipt : pointOfSaleVO
										.getPointOfSaleReceiptVOs()) {

									totalReceipt += saleReceipt
											.getReceiptAmount();
								}

								pointOfSaleVO.setReceivedTotal(AIOSCommons
										.formatAmount(totalReceipt));
							}
						}

						saleVOs.setValue(pointOfSaleList);
					}
				}

				Map<String, List<PointOfSaleVO>> sortedSalesMap = new TreeMap<String, List<PointOfSaleVO>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});

				sortedSalesMap.putAll(saleDateMap);

				ServletActionContext.getRequest().setAttribute("POS",
						sortedSalesMap);

				tempMapForPOSProfitReportXLS = sortedSalesMap;
			}

			if (null != pointOfSaleList && pointOfSaleList.size() > 0) {
				Map<Byte, PointOfSaleReceiptVO> receiptVOMap = new HashMap<Byte, PointOfSaleReceiptVO>();
				PointOfSaleReceiptVO pointOfSaleReceiptVO = null;
				double receiptAmount = 0;
				for (PointOfSale pointOfSale : pointOfSaleList) {

					for (PointOfSaleReceipt saleReceipt : pointOfSale
							.getPointOfSaleReceipts()) {
						if (saleReceipt.getReceipt() > 0) {
							pointOfSaleReceiptVO = new PointOfSaleReceiptVO();
							receiptAmount = saleReceipt.getReceipt();
							if (null != receiptVOMap
									&& receiptVOMap.size() > 0
									&& receiptVOMap.containsKey(saleReceipt
											.getReceiptType())) {
								pointOfSaleReceiptVO = receiptVOMap
										.get(saleReceipt.getReceiptType());
								receiptAmount += pointOfSaleReceiptVO
										.getReceiptAmount();
							}
							if (null != saleReceipt.getBalanceDue()
									&& (double) saleReceipt.getBalanceDue() > 0)
								receiptAmount -= saleReceipt.getBalanceDue();
							pointOfSaleReceiptVO
									.setReceiptAmount(receiptAmount);
							pointOfSaleReceiptVO
									.setReceiptAmountStr(AIOSCommons
											.formatAmount(pointOfSaleReceiptVO
													.getReceiptAmount()));
							pointOfSaleReceiptVO
									.setReceiptTypeStr(POSPaymentType.get(
											saleReceipt.getReceiptType())
											.name());
							receiptVOMap.put(saleReceipt.getReceiptType(),
									pointOfSaleReceiptVO);
						}
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"RECEIPT_DETAILS", receiptVOMap.values());
			}

			ServletActionContext.getRequest().setAttribute("PRINT_DATE",
					DateFormat.convertDateToString(currentDate.toString()));
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getEODSalesPrintOut() {
		try {
			LocalDate currentDate = DateTime.now().toLocalDate();
			List<PointOfSaleVO> pointOfSaleList = this
					.fetchDailySalesReportData();
			double addtionalCharges = 0;
			if (null != pointOfSaleList && pointOfSaleList.size() > 0) {
				Map<Long, PointOfSaleVO> saleMap = new HashMap<Long, PointOfSaleVO>();
				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
				List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = null;
				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = null;
				PointOfSaleReceiptVO receiptVO = null;
				PointOfSaleDetailVO saleDetailVO = null;
				PointOfSaleChargeVO chargeVO = null;
				Map<String, List<PointOfSaleVO>> saleDateMap = new HashMap<String, List<PointOfSaleVO>>();
				List<PointOfSaleVO> datePOS = null;
				for (PointOfSaleVO saleVO : pointOfSaleList) {
					if (!saleMap.containsKey(saleVO.getPointOfSaleId())) {
						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
						for (PointOfSaleDetail saleDetail : saleVO
								.getPointOfSaleDetails()) {
							saleDetailVO = new PointOfSaleDetailVO();
							BeanUtils.copyProperties(saleDetailVO, saleDetail);
							saleDetailVO.setProductName(saleDetail
									.getProductByProductId().getProductName());
							double discount = 0;
							double salesAmount = saleDetail.getQuantity()
									* saleDetail.getUnitRate();
							if (null != saleDetail.getIsPercentageDiscount()
									&& null != saleDetail.getDiscountValue()) {

								if (saleDetail.getIsPercentageDiscount()) {
									discount = ((salesAmount * saleDetail
											.getDiscountValue()) / 100);
								} else
									discount = saleDetail.getDiscountValue();

								salesAmount = salesAmount - discount;

								saleDetailVO.setDiscountValue(AIOSCommons
										.roundDecimals(discount));
								saleDetailVO.setDiscount(AIOSCommons
										.formatAmount(discount));
							}
							saleDetailVO.setTotalPrice(AIOSCommons
									.formatAmount(salesAmount));
							saleDetailVO.setTotalAmount(AIOSCommons
									.roundDecimals(salesAmount));
							pointOfSaleDetailVOs.add(saleDetailVO);
						}
						saleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
						if (null != saleVO.getPointOfSaleReceipts()
								&& saleVO.getPointOfSaleReceipts().size() > 0) {
							pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();
							for (PointOfSaleReceipt receipt : saleVO
									.getPointOfSaleReceipts()) {
								receiptVO = new PointOfSaleReceiptVO();
								BeanUtils.copyProperties(receiptVO, receipt);
								receiptVO
										.setReceiptAmount(AIOSCommons.roundDecimals(receipt
												.getReceipt()
												- (null != receipt
														.getBalanceDue() ? receipt
														.getBalanceDue() : 0)));
								receiptVO.setReceiptAmountStr(AIOSCommons
										.formatAmount(receiptVO
												.getReceiptAmount()));
								receiptVO.setReceiptTypeStr(POSPaymentType.get(
										receipt.getReceiptType()).name());
								pointOfSaleReceiptVOs.add(receiptVO);
							}
							saleVO.setPointOfSaleReceiptVOs(pointOfSaleReceiptVOs);
						}
						if (null != saleVO.getPointOfSaleCharges()
								&& saleVO.getPointOfSaleCharges().size() > 0) {

							pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
							saleVO.setPosCharge(true);

							for (PointOfSaleCharge charge : saleVO
									.getPointOfSaleCharges()) {

								chargeVO = new PointOfSaleChargeVO();
								BeanUtils.copyProperties(chargeVO, charge);
								chargeVO.setChargeType(charge.getLookupDetail()
										.getDisplayName());
								chargeVO.setCharges(AIOSCommons
										.roundDecimals(charge.getCharges()));
								addtionalCharges += charge.getCharges();
								pointOfSaleChargeVOs.add(chargeVO);
							}
							saleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

						} else
							saleVO.setPosCharge(false);

						saleMap.put(saleVO.getPointOfSaleId(), saleVO);
						datePOS = new ArrayList<PointOfSaleVO>();

						if (saleDateMap.containsKey(saleVO.getDate())) {

							datePOS.addAll(saleDateMap.get(saleVO.getDate()));
						}

						datePOS.add(saleVO);
						saleDateMap.put(saleVO.getDate(), datePOS);
					}
				}
				double totalUnitRate1 = 0;
				double totalSales1 = 0;
				double totalDiscount1 = 0;
				double totalQuantity1 = 0;
				double totalReceipt1 = 0;
				if (null != saleDateMap && saleDateMap.size() > 0) {
					double totalUnitRate;
					double totalSales;
					double totalDiscount;
					double totalQuantity;
					double salesAmount;
					double totalReceipt;
					for (Entry<String, List<PointOfSaleVO>> saleVOs : saleDateMap
							.entrySet()) {
						totalUnitRate = 0;
						totalSales = 0;
						totalDiscount = 0;
						totalQuantity = 0;
						totalReceipt = 0;
						pointOfSaleList = new ArrayList<PointOfSaleVO>(
								saleVOs.getValue());
						for (PointOfSaleVO pointOfSaleVO : pointOfSaleList) {
							for (PointOfSaleDetailVO saleDetail : pointOfSaleVO
									.getPointOfSaleDetailVOs()) {
								salesAmount = 0;
								totalUnitRate += saleDetail.getUnitRate();
								totalQuantity += saleDetail.getQuantity();
								salesAmount = (saleDetail.getUnitRate() * saleDetail
										.getQuantity());

								if (null != saleDetail.getDiscountValue()) {
									salesAmount -= saleDetail
											.getDiscountValue();
								}
								totalSales += salesAmount;
								totalDiscount += saleDetail.getDiscountValue();

								totalUnitRate1 = totalUnitRate;
								totalQuantity1 = totalQuantity;
								totalSales1 = totalSales;
								totalDiscount1 = totalDiscount;
							}
							pointOfSaleVO.setTotalQuantity(AIOSCommons
									.roundDecimals(totalQuantity));
							pointOfSaleVO.setSalesAmount(AIOSCommons
									.formatAmount(totalSales));
							pointOfSaleVO.setCustomerDiscount(AIOSCommons
									.formatAmount(totalDiscount));
							pointOfSaleVO.setTotalDue(AIOSCommons
									.formatAmount(totalUnitRate));
							if (null != pointOfSaleVO
									.getPointOfSaleReceiptVOs()
									&& pointOfSaleVO.getPointOfSaleReceiptVOs()
											.size() > 0) {
								for (PointOfSaleReceiptVO saleReceipt : pointOfSaleVO
										.getPointOfSaleReceiptVOs()) {
									totalReceipt += saleReceipt
											.getReceiptAmount();
									totalReceipt1 = totalReceipt;
								}
								pointOfSaleVO.setReceivedTotal(AIOSCommons
										.formatAmount(totalReceipt));
							}
						}
						saleVOs.setValue(pointOfSaleList);
					}
				}

				Map<Byte, PointOfSaleReceiptVO> receiptVOMap = new HashMap<Byte, PointOfSaleReceiptVO>();
				PointOfSaleReceiptVO pointOfSaleReceiptVO = null;
				double receiptAmount = 0;
				for (PointOfSale pointOfSale : pointOfSaleList) {

					for (PointOfSaleReceipt saleReceipt : pointOfSale
							.getPointOfSaleReceipts()) {
						if (saleReceipt.getReceipt() > 0) {
							pointOfSaleReceiptVO = new PointOfSaleReceiptVO();
							receiptAmount = saleReceipt.getReceipt();
							if (null != receiptVOMap
									&& receiptVOMap.size() > 0
									&& receiptVOMap.containsKey(saleReceipt
											.getReceiptType())) {
								pointOfSaleReceiptVO = receiptVOMap
										.get(saleReceipt.getReceiptType());
								receiptAmount += pointOfSaleReceiptVO
										.getReceiptAmount();
							}
							if (null != saleReceipt.getBalanceDue()
									&& (double) saleReceipt.getBalanceDue() > 0)
								receiptAmount -= saleReceipt.getBalanceDue();
							pointOfSaleReceiptVO
									.setReceiptAmount(receiptAmount);
							pointOfSaleReceiptVO
									.setReceiptAmountStr(AIOSCommons
											.formatAmount(pointOfSaleReceiptVO
													.getReceiptAmount()));
							pointOfSaleReceiptVO
									.setReceiptTypeStr(POSPaymentType.get(
											saleReceipt.getReceiptType())
											.name());
							receiptVOMap.put(saleReceipt.getReceiptType(),
									pointOfSaleReceiptVO);
						}
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"RECEIPT_DETAILS", receiptVOMap.values());

				HttpSession session = ServletActionContext.getRequest()
						.getSession();

				Boolean eodSalesTemplatePrintEnabled = session
						.getAttribute("print_eodsales_template") != null ? ((String) session
						.getAttribute("print_eodsales_template"))
						.equalsIgnoreCase("true") : false;
				if (eodSalesTemplatePrintEnabled) {
					Configuration config = new Configuration();
					final Properties confProps = (Properties) ServletActionContext
							.getServletContext().getAttribute("confProps");
					getImplementId();
					config.setDirectoryForTemplateLoading(new File(confProps
							.getProperty("file.drive")
							+ "aios/PrintTemplate/templates/implementation_"
							+ implementation.getImplementationId() + "/pos"));

					String scheme = ServletActionContext.getRequest()
							.getScheme();
					String host = ServletActionContext.getRequest().getHeader(
							"Host");

					// includes leading forward slash
					String contextPath = ServletActionContext.getRequest()
							.getContextPath();

					String urlPath = scheme + "://" + host + contextPath;

					Template template = config
							.getTemplate("html-saleseod-template.ftl");
					Map<String, Object> rootMap = new HashMap<String, Object>();
					rootMap.put("urlPath", urlPath);
					rootMap.put("printdate", currentDate.toString());
					rootMap.put("storeName", pointOfSaleList.get(0)
							.getStoreName());
					rootMap.put("totalQuantity",
							AIOSCommons.roundDecimals(totalQuantity1));
					rootMap.put("totalSales",
							AIOSCommons.formatAmount(totalSales1));
					rootMap.put("totalDiscount",
							AIOSCommons.formatAmount(totalDiscount1));
					rootMap.put("title", implementation.getCompanyName());
					rootMap.put("salesDate", fromDate);
					rootMap.put("addtionalCharges",
							AIOSCommons.formatAmount(addtionalCharges));
					rootMap.put("totalReceipt",
							AIOSCommons.formatAmount(totalReceipt1));
					rootMap.put("receiptTypes", receiptVOMap.values());
					Writer out = new StringWriter();
					template.process(rootMap, out);
					printableContent = out.toString();
				}
			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	Map<String, List<PointOfSaleVO>> tempMapForPOSProfitReportXLS = null;

	public String getDailyPOSSalesProfitReportXLS() {
		try {

			this.getDailySalesPrintOut();

			List<PointOfSaleVO> pointOfSaleList = new ArrayList<PointOfSaleVO>();

			for (Map.Entry<String, List<PointOfSaleVO>> list : tempMapForPOSProfitReportXLS
					.entrySet()) {

				pointOfSaleList.addAll(list.getValue());
			}

			String str = "";
			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;
			Double totalCost = 0.0;

			str += "\n Date, Ref no, Store, Product, Unit Rate, Quantity, Discount, Total Sale, Cost, Balance \n ";
			for (PointOfSaleVO pos : pointOfSaleList) {
				for (PointOfSaleDetailVO detail : pos.getPointOfSaleDetailVOs()) {

					str += pos.getDate()
							+ ","
							+ pos.getReferenceNumber()
							+ ","
							+ pos.getStoreName()
							+ ","
							+ detail.getProductName()
							+ ","
							+ detail.getUnitRate()
							+ ","
							+ detail.getQuantity()
							+ ","
							+ ((null != detail.getDiscount()) ? detail
									.getDiscount() : "N/A") + ","
							+ detail.getTotalPrice() + ","
							+ detail.getTotalCost() + ","
							+ detail.getTotalGain();
					str += "\n";

					totalQuantity += detail.getQuantity();
					totalAmount += detail.getTotalAmount();
					totalCost += detail.getTotalCost() == null ? 0.0 : detail
							.getTotalCost();
				}
			}

			str += "\n ,,,,,Total Quantity: " + totalQuantity
					+ " ,, Total Sale: "
					+ AIOSCommons.roundTwoDecimals(totalAmount)
					+ " , Total Cost: "
					+ AIOSCommons.roundTwoDecimals(totalCost)
					+ " , Total Gain: "
					+ AIOSCommons.roundTwoDecimals(totalAmount - totalCost)
					+ " \n\n ";

			/*
			 * str += "\n Additional Charges \n\n "; str +=
			 * "\n Charges Type, Amount \n ";
			 * 
			 * totalAmount = 0.0; for (PointOfSaleVO pos : pointOfSaleList) {
			 * for (PointOfSaleChargeVO charge : pos.getPointOfSaleChargeVOs())
			 * { str += charge.getChargeType() + "," + charge.getCharges(); str
			 * += "\n";
			 * 
			 * totalAmount += charge.getCharges(); } } str +=
			 * "\n ,, Total Amount: " + totalAmount + " \n\n ";
			 * 
			 * str += "\n Discounts : \n "; str += "\n Discount Amount \n "; for
			 * (PointOfSaleVO pos : pointOfSaleList) { for (PointOfSaleDetailVO
			 * detail : pos.getPointOfSaleDetailVOs()) { str +=
			 * detail.getDiscount(); str += "\n";
			 * 
			 * if (detail.getDiscountValue() != null) { totalDiscount +=
			 * detail.getDiscountValue(); } } }
			 * 
			 * str += "\n Total Discount: " + totalDiscount + " \n\n ";
			 */

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getDailySalesReportXLS() {
		try {

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchDailySalesReportData();

			if (null != pointOfSaleList && pointOfSaleList.size() > 0) {

				Map<Long, PointOfSaleVO> saleMap = new HashMap<Long, PointOfSaleVO>();
				List<PointOfSaleDetailVO> pointOfSaleDetailVOs = null;
				List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = null;
				List<PointOfSaleChargeVO> pointOfSaleChargeVOs = null;
				PointOfSaleReceiptVO receiptVO = null;
				PointOfSaleDetailVO saleDetailVO = null;
				PointOfSaleChargeVO chargeVO = null;
				Map<String, List<PointOfSaleVO>> saleDateMap = new HashMap<String, List<PointOfSaleVO>>();
				List<PointOfSaleVO> datePOS = null;

				for (PointOfSaleVO saleVO : pointOfSaleList) {

					if (!saleMap.containsKey(saleVO.getPointOfSaleId())) {

						pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();

						for (PointOfSaleDetail saleDetail : saleVO
								.getPointOfSaleDetails()) {

							saleDetailVO = new PointOfSaleDetailVO();
							BeanUtils.copyProperties(saleDetailVO, saleDetail);
							saleDetailVO.setProductName(saleDetail
									.getProductByProductId().getProductName());
							double discount = 0;
							double salesAmount = saleDetail.getQuantity()
									* saleDetail.getUnitRate();

							if (null != saleDetail.getIsPercentageDiscount()
									&& null != saleDetail.getDiscountValue()) {

								if (saleDetail.getIsPercentageDiscount()) {
									discount = ((salesAmount * saleDetail
											.getDiscountValue()) / 100);
								} else
									discount = saleDetail.getDiscountValue();

								salesAmount = salesAmount - discount;

								saleDetailVO.setDiscountValue(AIOSCommons
										.roundDecimals(discount));
								saleDetailVO.setDiscount(AIOSCommons
										.formatAmount(discount));
							}

							saleDetailVO.setTotalPrice(AIOSCommons
									.formatAmount(salesAmount));
							saleDetailVO.setTotalAmount(AIOSCommons
									.roundDecimals(salesAmount));

							try {
								ProductPricingDetail pricingDetail = accountsEnterpriseService
										.getProductPricingService()
										.getPricingDetailByProductId(
												saleDetail
														.getProductByProductId()
														.getProductId());

								if (null != pricingDetail
										&& null != pricingDetail
												.getBasicCostPrice())
									saleDetailVO
											.setProductCostPrice(pricingDetail
													.getBasicCostPrice()); // HERE
								else
									saleDetailVO.setProductCostPrice(0.0); // HERE

								saleDetailVO
										.setTotalGain(AIOSCommons.roundDecimals(salesAmount
												- (saleDetailVO
														.getProductCostPrice() * saleDetailVO
														.getQuantity())));

								saleDetailVO.setTotalCost(AIOSCommons
										.roundDecimals(saleDetailVO
												.getProductCostPrice()
												* saleDetailVO.getQuantity()));

							} catch (Exception e) {
								e.printStackTrace();
							}
							pointOfSaleDetailVOs.add(saleDetailVO);
						}

						saleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);

						if (null != saleVO.getPointOfSaleReceipts()
								&& saleVO.getPointOfSaleReceipts().size() > 0) {

							pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();

							for (PointOfSaleReceipt receipt : saleVO
									.getPointOfSaleReceipts()) {
								receiptVO = new PointOfSaleReceiptVO();
								BeanUtils.copyProperties(receiptVO, receipt);
								receiptVO
										.setReceiptAmount(AIOSCommons.roundDecimals(receipt
												.getReceipt()
												- (null != receipt
														.getBalanceDue() ? receipt
														.getBalanceDue() : 0)));
								receiptVO.setReceiptAmountStr(AIOSCommons
										.formatAmount(receiptVO
												.getReceiptAmount()));
								receiptVO.setReceiptTypeStr(POSPaymentType.get(
										receipt.getReceiptType()).name());
								pointOfSaleReceiptVOs.add(receiptVO);
							}
							saleVO.setPointOfSaleReceiptVOs(pointOfSaleReceiptVOs);
						}

						if (null != saleVO.getPointOfSaleCharges()
								&& saleVO.getPointOfSaleCharges().size() > 0) {

							pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
							saleVO.setPosCharge(true);

							for (PointOfSaleCharge charge : saleVO
									.getPointOfSaleCharges()) {

								chargeVO = new PointOfSaleChargeVO();
								BeanUtils.copyProperties(chargeVO, charge);
								chargeVO.setChargeType(charge.getLookupDetail()
										.getDisplayName());
								chargeVO.setCharges(AIOSCommons
										.roundDecimals(charge.getCharges()));
								pointOfSaleChargeVOs.add(chargeVO);
							}
							saleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);

						} else
							saleVO.setPosCharge(false);

						saleMap.put(saleVO.getPointOfSaleId(), saleVO);
						datePOS = new ArrayList<PointOfSaleVO>();

						if (saleDateMap.containsKey(saleVO.getDate())) {

							datePOS.addAll(saleDateMap.get(saleVO.getDate()));
						}

						datePOS.add(saleVO);
						saleDateMap.put(saleVO.getDate(), datePOS);
					}
				}
				if (null != saleDateMap && saleDateMap.size() > 0) {

					double totalUnitRate;
					double totalSales;
					double totalDiscount;
					double totalQuantity;
					double salesAmount;
					double totalReceipt;

					for (Entry<String, List<PointOfSaleVO>> saleVOs : saleDateMap
							.entrySet()) {

						totalUnitRate = 0;
						totalSales = 0;
						totalDiscount = 0;
						totalQuantity = 0;
						totalReceipt = 0;
						pointOfSaleList = new ArrayList<PointOfSaleVO>(
								saleVOs.getValue());

						for (PointOfSaleVO pointOfSaleVO : pointOfSaleList) {

							for (PointOfSaleDetailVO saleDetail : pointOfSaleVO
									.getPointOfSaleDetailVOs()) {
								salesAmount = 0;
								totalUnitRate += saleDetail.getUnitRate();
								totalQuantity += saleDetail.getQuantity();
								salesAmount = (saleDetail.getUnitRate() * saleDetail
										.getQuantity());

								if (null != saleDetail.getDiscountValue()) {
									salesAmount -= saleDetail
											.getDiscountValue();
								}
								totalSales += salesAmount;
								totalDiscount += saleDetail.getDiscountValue();
							}

							pointOfSaleVO.setTotalQuantity(AIOSCommons
									.roundDecimals(totalQuantity));
							pointOfSaleVO.setSalesAmount(AIOSCommons
									.formatAmount(totalSales));
							pointOfSaleVO.setCustomerDiscount(AIOSCommons
									.formatAmount(totalDiscount));
							pointOfSaleVO.setTotalDue(AIOSCommons
									.formatAmount(totalUnitRate));

							if (null != pointOfSaleVO
									.getPointOfSaleReceiptVOs()
									&& pointOfSaleVO.getPointOfSaleReceiptVOs()
											.size() > 0) {

								for (PointOfSaleReceiptVO saleReceipt : pointOfSaleVO
										.getPointOfSaleReceiptVOs()) {

									totalReceipt += saleReceipt
											.getReceiptAmount();
								}

								pointOfSaleVO.setReceivedTotal(AIOSCommons
										.formatAmount(totalReceipt));
							}
						}

						saleVOs.setValue(pointOfSaleList);
					}
				}

				Map<String, List<PointOfSaleVO>> sortedSalesMap = new TreeMap<String, List<PointOfSaleVO>>(
						new Comparator<String>() {
							public int compare(String o1, String o2) {
								return o1.compareTo(o2);
							}
						});

				sortedSalesMap.putAll(saleDateMap);

				/*
				 * ServletActionContext.getRequest().setAttribute("POS",
				 * sortedSalesMap);
				 */

				tempMapForPOSProfitReportXLS = sortedSalesMap;
			}

			String str = "";
			for (Entry<String, List<PointOfSaleVO>> pos : tempMapForPOSProfitReportXLS
					.entrySet()) {
				String totalQuantity = "";
				String salesAmount = "";
				String customerDiscount = "";
				String totalDue = "";
				str += "\n SALES DATE: " + pos.getKey() + "";
				str += "\n REFERENCE, STORE, PRODUCT,QUANTITY,UNIT RATE,DISCOUNT,TOTAL \n ";
				for (PointOfSaleVO poshead : pos.getValue()) {
					for (PointOfSaleDetailVO posdetail : poshead
							.getPointOfSaleDetailVOs()) {
						str += poshead.getReferenceNumber().replaceAll(",", "")
								+ ","
								+ poshead.getStoreName().replaceAll(",", "")
								+ ","
								+ posdetail.getProductName()
										.replaceAll(",", "")
								+ ","
								+ posdetail.getQuantity()
								+ ","
								+ (posdetail.getUnitRate() + "").replaceAll(
										",", "")
								+ ","
								+ getDiscountFromDetail(posdetail)
								+ ","
								+ (posdetail.getTotalPrice() + "").replaceAll(
										",", "") + "\n";

					}

					totalQuantity = poshead.getTotalQuantity() + "";
					salesAmount = poshead.getSalesAmount();
					customerDiscount = poshead.getCustomerDiscount();
					totalDue = poshead.getTotalDue();
				}

				str += ",,," + totalQuantity.replaceAll(",", "") + ","
						+ totalDue.replaceAll(",", "") + ","
						+ customerDiscount.replaceAll(",", "") + ","
						+ salesAmount.replaceAll(",", "") + "\n";

				str += "\n RECEIPT DETAILS: " + pos.getKey() + "";
				str += "\n REFERENCE, RECEIPT TYPE, AMOUNT \n ";

				Double receiptAmount = 0.0;

				for (PointOfSaleVO poshead : pos.getValue()) {
					for (PointOfSaleReceiptVO receipt : poshead
							.getPointOfSaleReceiptVOs()) {
						str += poshead.getReferenceNumber().replaceAll(",", "")
								+ ","
								+ receipt.getReceiptTypeStr().replaceAll(",",
										"")
								+ ","
								+ receipt.getReceiptAmountStr().replaceAll(",",
										"") + "\n";
						receiptAmount += receipt.getReceiptAmount();
					}

				}
				str += ",," + (receiptAmount + "").replaceAll(",", "") + "\n";

				str += "\n ADDITIONAL CHAEGES: " + pos.getKey() + "";
				str += "\n REFERENCE, CHARGES TYPE, AMOUNT \n ";

				Double chargesAmount = 0.0;

				for (PointOfSaleVO poshead : pos.getValue()) {
					for (PointOfSaleChargeVO charge : poshead
							.getPointOfSaleChargeVOs()) {
						str += poshead.getReferenceNumber().replaceAll(",", "")
								+ ","
								+ charge.getChargeType().replaceAll(",", "")
								+ ","
								+ (charge.getCharges() + "")
										.replaceAll(",", "") + "\n";
						chargesAmount += charge.getCharges();
					}

				}
				str += ",," + (chargesAmount + "").replaceAll(",", "") + "\n";
			}

			/*
			 * ServletActionContext.getRequest().setAttribute("PRINT_DATE",
			 * DateFormat.convertDateToString(currentDate.toString()));
			 */

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts method storeDetalReportXLS() caught EXCEPTION "
					+ ex);
			return ERROR;
		}
	}

	public String getDiscountFromDetail(SalesDeliveryDetailVO posdetail) {
		String returnValue = "";
		if (posdetail.getDisplayDiscount() != null) {
			if (posdetail.getIsPercentage())
				returnValue = posdetail.getDisplayDiscount() + "(Percentage)";
			else
				returnValue = posdetail.getDisplayDiscount() + "(Amount)";
		}

		return returnValue.replaceAll(",", "");
	}

	public String getDiscountFromDetail(PointOfSaleDetailVO posdetail) {
		String returnValue = "";
		if (posdetail.getDiscountValue() != null
				&& posdetail.getDiscountValue() > 0) {
			if (posdetail.getIsPercentageDiscount())
				returnValue = posdetail.getDiscountValue() + "(Percentage)";
			else
				returnValue = posdetail.getDiscountValue() + "(Amount)";
		}

		return returnValue.replaceAll(",", "");
	}

	public List<PointOfSaleVO> fetchDailySalesReportDataPDF() throws Exception {

		getImplementId();

		Integer orderType = 0;
		Byte receiptType = 0;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("orderType") != null) {
			orderType = Integer.parseInt(ServletActionContext.getRequest()
					.getParameter("orderType"));
		}

		if (ServletActionContext.getRequest().getParameter("paymentType") != null) {
			receiptType = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("paymentType"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<PointOfSale> pointOfSalesList = accountsEnterpriseService
				.getPointOfSaleService().getFilteredDailySales(implementation,
						orderType, receiptType, fromDatee, toDatee, null, null);

		List<PointOfSaleVO> pointOfSaleVOs = new ArrayList<PointOfSaleVO>();
		PointOfSaleVO pointOfSaleVO = null;
		List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
		List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();

		for (PointOfSale pos : pointOfSalesList) {
			pointOfSaleVO = new PointOfSaleVO(pos);

			pointOfSaleVO.setDate(DateFormat.convertDateToString(pos
					.getSalesDate().toString()));

			if (pos.getPOSUserTill() != null) {
				if (pos.getPOSUserTill().getPersonByPersonId() != null) {
					pointOfSaleVO.setPosUserName(pos.getPOSUserTill()
							.getPersonByPersonId().getFirstName()
							+ " "
							+ pos.getPOSUserTill().getPersonByPersonId()
									.getLastName());
				}
				pointOfSaleVO.setStoreName(pos.getPOSUserTill().getStore()
						.getStoreName());
			}

			Double totalQuantity = 0.0;
			Double totalAmount = 0.0;
			for (PointOfSaleDetail detail : pos.getPointOfSaleDetails()) {
				PointOfSaleDetailVO pointOfSaleDetailVO = new PointOfSaleDetailVO(
						detail);

				pointOfSaleDetailVO.setProductName(detail
						.getProductByProductId().getProductName());

				Double total = detail.getUnitRate() * detail.getQuantity();

				pointOfSaleDetailVO.setTotalAmount(total);
				pointOfSaleDetailVO.setTotalPrice(total.toString());
				if (detail.getDiscountValue() != null) {
					pointOfSaleDetailVO.setDiscount(detail.getDiscountValue()
							.toString());
					if (detail.getIsPercentageDiscount()) {
						pointOfSaleDetailVO.setDiscount(pointOfSaleDetailVO
								.getDiscount().concat("%"));
					}
				}

				pointOfSaleDetailVOs.add(pointOfSaleDetailVO);

				totalQuantity += detail.getQuantity();
				totalAmount += total;

			}

			pointOfSaleVO.setTotalQuantity(totalQuantity);
			pointOfSaleVO.setTotalAmount(totalAmount);

			for (PointOfSaleCharge charge : pos.getPointOfSaleCharges()) {
				PointOfSaleChargeVO pointOfSaleChargeVO = new PointOfSaleChargeVO(
						charge);
				pointOfSaleChargeVO.setChargeType(charge.getLookupDetail()
						.getDisplayName());

				pointOfSaleChargeVOs.add(pointOfSaleChargeVO);
			}

		}
		pointOfSaleVO.setPointOfSaleDetailVOs(pointOfSaleDetailVOs);
		pointOfSaleVO.setPointOfSaleChargeVOs(pointOfSaleChargeVOs);
		pointOfSaleVOs.add(pointOfSaleVO);

		return pointOfSaleVOs;
	}

	public String getDailySalesPDF() {
		String result = ERROR;
		try {
			pointOfSaleDS = this.fetchDailySalesReportDataPDF();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getEmployeeServiceJsonList() {
		try {

			ServletActionContext.getRequest().setAttribute("reportType",
					"employee");

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportDataOld();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", pointOfSaleList.size());
			jsonResponse.put("iTotalDisplayRecords", pointOfSaleList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PointOfSaleVO list : pointOfSaleList) {
				array = new JSONArray();
				array.add(list.getPointOfSaleId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getStoreName());
				array.add(list.getPosUserName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getEmployeeServicePrintOut() {
		try {

			ServletActionContext.getRequest().setAttribute("reportType",
					"employee");

			List<PointOfSaleVO> pointOfSaleList = this
					.fetchPOSSalesReportDataOld();

			ServletActionContext.getRequest().setAttribute("POS",
					pointOfSaleList);
			ServletActionContext.getRequest().setAttribute(
					"VIEW_TYPE",
					pointOfSaleList.size() > 1 ? "Multiple Records"
							: "Single Record");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getEmployeeServicePDF() {
		String result = ERROR;
		try {

			ServletActionContext.getRequest().setAttribute("reportType",
					"employee");

			List<PointOfSaleVO> pointOfSaleVOs = this
					.fetchPOSSalesReportDataOld();
			pointOfSaleDS = new ArrayList<PointOfSaleVO>();

			PointOfSaleVO pos = (PointOfSaleVO) ServletActionContext
					.getRequest().getAttribute("POINT_OF_SALE");

			pos.setProductCategoryVOs((List<ProductCategoryVO>) ServletActionContext
					.getRequest().getAttribute("POS_CATEGORY_SALES"));
			pos.setPointOfSaleDetailVOs((List<PointOfSaleDetailVO>) ServletActionContext
					.getRequest().getAttribute("POS_PRODUCT_SALES"));
			pos.setPointOfSaleChargeVOs((List<PointOfSaleChargeVO>) ServletActionContext
					.getRequest().getAttribute("POS_CHARGES"));
			pointOfSaleDS.add(pos);

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProjectReportCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_TYPE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"RESOURCE_DETAIL",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_RESOURCE_DETAIL", true));
			Map<Byte, ProjectStatus> projectStatus = new HashMap<Byte, ProjectStatus>();
			for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
				projectStatus.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PROJECT_STATUS",
					projectStatus);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
					accountsBL.getCustomerBL().getAllCustomers(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getCustomerHostoryFilterData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
					accountsBL.getCustomerBL().getAllCustomers(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getCustomerHistory() {
		try {

			List<SalesInvoiceVO> salesInvoiceVOs = generateCustomerBasedSalesHistory();
			if (salesInvoiceVOs != null)
				aaData = new ArrayList<Object>(salesInvoiceVOs);
			else
				aaData = new ArrayList<Object>();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getCustomerHistoryReportPrintOut() {
		try {

			List<SalesInvoiceVO> salesInvoiceVOs = generateCustomerBasedSalesHistory();
			ServletActionContext.getRequest().setAttribute("CUSTOMER_HISTORY",
					salesInvoiceVOs);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Show all Sales Invoice Continuous
	public List<SalesInvoiceVO> generateCustomerBasedSalesHistory() {
		try {
			List<SalesInvoiceVO> salesInvoiceVos = new ArrayList<SalesInvoiceVO>();
			{
				LOGGER.info("Inside Module: Accounts : Method: showContiniousInvoice");
				getImplementId();
				SalesInvoiceVO salesInvoiceVO = new SalesInvoiceVO();
				if (fromDate != null && !fromDate.equals("null")
						&& !fromDate.equals("undefined")) {
					salesInvoiceVO.setFormDate(DateFormat
							.convertStringToDate(fromDate));
				}
				if (toDate != null && !toDate.equals("null")
						&& !toDate.equals("undefined")) {
					salesInvoiceVO.setToDate(DateFormat
							.convertStringToDate(toDate));
				}
				Long customerId = Long.parseLong(ServletActionContext
						.getRequest().getParameter("customerId"));
				if (customerId != null && customerId > 0) {
					salesInvoiceVO.setCustomerId(customerId);
				}

				salesInvoiceVO.setPaymentStatus(paymentStatus);

				salesInvoiceVO.setImplementation(implementation);

				List<SalesInvoiceVO> salesInvoiceVosTemp = accountsBL
						.getSalesInvoiceBL()
						.getAllContiniousSalesInvoiceForReportData(
								salesInvoiceVO);
				if (salesInvoiceVosTemp != null)
					salesInvoiceVos.addAll(salesInvoiceVosTemp);

				// Project Management Customer History
				salesInvoiceVosTemp = accountsBL.getProjectBL()
						.getAllProjectPaymentByProject(salesInvoiceVO);
				if (salesInvoiceVosTemp != null)
					salesInvoiceVos.addAll(salesInvoiceVosTemp);

				// Point of Sale Customer History
				salesInvoiceVosTemp = accountsBL.getPointOfSaleBL()
						.getAllSalesByCustomer(salesInvoiceVO);
				if (salesInvoiceVosTemp != null)
					salesInvoiceVos.addAll(salesInvoiceVosTemp);

			}

			Map<Long, List<SalesInvoiceVO>> mapByCustomer = new HashMap<Long, List<SalesInvoiceVO>>();
			List<SalesInvoiceVO> tempVos = null;
			for (SalesInvoiceVO salesInvoiceVO : salesInvoiceVos) {
				if (mapByCustomer.containsKey(salesInvoiceVO.getCustomerId())) {
					tempVos = mapByCustomer.get(salesInvoiceVO.getCustomerId());
					tempVos.add(salesInvoiceVO);
					mapByCustomer.put(salesInvoiceVO.getCustomerId(), tempVos);
				} else {
					tempVos = new ArrayList<SalesInvoiceVO>();
					tempVos.add(salesInvoiceVO);
					mapByCustomer.put(salesInvoiceVO.getCustomerId(), tempVos);
				}
			}

			if (mapByCustomer != null && mapByCustomer.size() > 0) {
				SalesInvoiceVO salesInvoiceVO = null;
				salesInvoiceVos = new ArrayList<SalesInvoiceVO>();

				for (Map.Entry<Long, List<SalesInvoiceVO>> entry : mapByCustomer
						.entrySet()) {
					salesInvoiceVO = new SalesInvoiceVO();
					BeanUtils.copyProperties(salesInvoiceVO,
							mapByCustomer.get(entry.getKey()).get(0));
					salesInvoiceVO
							.setProjectSalesInvoiceVOs(new ArrayList<SalesInvoiceVO>());
					salesInvoiceVO
							.setPointOfSaleInvoiceVOs(new ArrayList<SalesInvoiceVO>());
					salesInvoiceVO
							.setSalesInvoiceVOs(new ArrayList<SalesInvoiceVO>());
					for (SalesInvoiceVO tempVO : entry.getValue()) {
						if (tempVO.getSalesFlag().equalsIgnoreCase("PROJECT")) {
							salesInvoiceVO.getProjectSalesInvoiceVOs().add(
									tempVO);
						} else if (tempVO.getSalesFlag()
								.equalsIgnoreCase("POS")) {
							salesInvoiceVO.getPointOfSaleInvoiceVOs().add(
									tempVO);
						} else {
							salesInvoiceVO.getSalesInvoiceVOs().add(tempVO);
						}
					}

					Double totalPending = 0.0;
					for (SalesInvoiceVO salesInvoiceVO2 : entry.getValue()) {
						totalPending += (salesInvoiceVO2.getPendingInvoice() != null ? salesInvoiceVO2
								.getPendingInvoice() : 0);
					}
					salesInvoiceVO.setPendingInvoice(totalPending);

					if (salesInvoiceVO.getProjectSalesInvoiceVOs().size() == 0)
						salesInvoiceVO.setProjectSalesInvoiceVOs(null);
					if (salesInvoiceVO.getPointOfSaleInvoiceVOs().size() == 0)
						salesInvoiceVO.setPointOfSaleInvoiceVOs(null);
					if (salesInvoiceVO.getSalesInvoiceVOs().size() == 0)
						salesInvoiceVO.setSalesInvoiceVOs(null);

					salesInvoiceVos.add(salesInvoiceVO);
				}
			}

			return salesInvoiceVos;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getSupplierHostoryFilterData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"SUPPLIER_LIST",
					accountsBL.getSupplierBL()
							.getAllSuppliersVo(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getSupplierHistory() {
		try {

			List<InvoiceVO> salesInvoiceVOs = showSupplierContiniousInvoice();
			if (salesInvoiceVOs != null)
				aaData = new ArrayList<Object>(salesInvoiceVOs);
			else
				aaData = new ArrayList<Object>();

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getSupplierHistoryReportPrintOut() {
		try {

			List<InvoiceVO> salesInvoiceVOs = showSupplierContiniousInvoice();
			ServletActionContext.getRequest().setAttribute("SUPPLIER_HISTORY",
					salesInvoiceVOs);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Show all Invoice Continuous
	public List<InvoiceVO> showSupplierContiniousInvoice() {
		try {
			List<InvoiceVO> salesInvoiceVOs = new ArrayList<InvoiceVO>();
			{
				LOGGER.info("Inside Module: Accounts : Method: showSupplierContiniousInvoice");
				getImplementId();
				InvoiceVO salesInvoiceVO = new InvoiceVO();
				if (fromDate != null && !fromDate.equals("null")
						&& !fromDate.equals("undefined")) {
					salesInvoiceVO.setFormDate(DateFormat
							.convertStringToDate(fromDate));
				}
				if (toDate != null && !toDate.equals("null")
						&& !toDate.equals("undefined")) {
					salesInvoiceVO.setToDate(DateFormat
							.convertStringToDate(toDate));
				}
				Long supplierId = Long.parseLong(ServletActionContext
						.getRequest().getParameter("supplierId"));
				if (supplierId != null && supplierId > 0) {
					salesInvoiceVO.setSupplierId(supplierId);
				}

				salesInvoiceVO.setImplementation(implementation);

				salesInvoiceVO.setPaymentStatus(paymentStatus);

				salesInvoiceVOs = accountsBL.getInvoiceBL()
						.getAllContiniousInvoiceForReportData(salesInvoiceVO);
			}

			Map<Long, List<InvoiceVO>> mapByCustomer = new HashMap<Long, List<InvoiceVO>>();
			List<InvoiceVO> tempVos = null;
			for (InvoiceVO salesInvoiceVO : salesInvoiceVOs) {
				if (mapByCustomer.containsKey(salesInvoiceVO.getSupplierId())) {
					tempVos = mapByCustomer.get(salesInvoiceVO.getSupplierId());
					tempVos.add(salesInvoiceVO);
					mapByCustomer.put(salesInvoiceVO.getSupplierId(), tempVos);
				} else {
					tempVos = new ArrayList<InvoiceVO>();
					tempVos.add(salesInvoiceVO);
					mapByCustomer.put(salesInvoiceVO.getSupplierId(), tempVos);
				}
			}

			if (mapByCustomer != null && mapByCustomer.size() > 0) {
				InvoiceVO salesInvoiceVO = null;
				salesInvoiceVOs = new ArrayList<InvoiceVO>();

				for (Map.Entry<Long, List<InvoiceVO>> entry : mapByCustomer
						.entrySet()) {
					salesInvoiceVO = new InvoiceVO();
					BeanUtils.copyProperties(salesInvoiceVO,
							mapByCustomer.get(entry.getKey()).get(0));
					salesInvoiceVO.setInvoiceVOs(entry.getValue());
					Double totalPending = 0.0;
					for (InvoiceVO salesInvoiceVO2 : salesInvoiceVO
							.getInvoiceVOs()) {
						totalPending += (salesInvoiceVO2.getPendingInvoice() != null ? salesInvoiceVO2
								.getPendingInvoice() : 0);
					}
					salesInvoiceVO.setPendingInvoice(totalPending);
					salesInvoiceVOs.add(salesInvoiceVO);

				}
			}

			return salesInvoiceVOs;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showSupplierContiniousInvoice Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String syncSalesTransaction() {
		try {
			accountsBL.getAccountsJobBL().getSalesUpdateJob().updateSalesSync();
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String goodsReceiveNoteReportCriteria() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: goodsReceiveNoteReportCriteria ");
			getImplementId();
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			List<SupplierVO> supplierVOs = null;
			List<Supplier> suppliers = accountsBL.getSupplierBL()
					.getSupplierService().getAllSuppliers(implementation);
			if (null != suppliers && suppliers.size() > 0) {
				supplierVOs = new ArrayList<SupplierVO>();
				for (Supplier supplier : suppliers)
					supplierVOs.add(accountsBL.getSupplierBL().addSupplierVO(
							supplier));
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierVOs);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: goodsReceiveNoteReportCriteria Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String purchaseComparisonReportCriteria() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: purchaseComparisonReportCriteria ");
			getImplementId();
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			List<SupplierVO> supplierVOs = null;
			List<Supplier> suppliers = accountsBL.getSupplierBL()
					.getSupplierService().getAllSuppliers(implementation);
			if (null != suppliers && suppliers.size() > 0) {
				supplierVOs = new ArrayList<SupplierVO>();
				for (Supplier supplier : suppliers)
					supplierVOs.add(accountsBL.getSupplierBL().addSupplierVO(
							supplier));
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierVOs);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: purchaseComparisonReportCriteria Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String salesComparisonReportCriteria() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: salesComparisonReportCriteria ");
			getImplementId();
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			List<CustomerVO> customerVOs = null;
			List<Customer> customers = accountsBL.getCustomerBL()
					.getCustomerService()
					.getAllApprovedCustomers(implementation);
			if (null != customers && customers.size() > 0) {
				customerVOs = new ArrayList<CustomerVO>();
				for (Customer customer : customers)
					customerVOs.add(accountsBL.getCustomerBL().addCustomerVO(
							customer));
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_INFO",
					customerVOs);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: salesComparisonReportCriteria Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	private static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public String purchaseSupplierComparisonReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: purchaseSupplierComparisonReport ");
			getImplementId();
			Set<Long> products = null;
			Set<Long> suppliers = null;
			if (ServletActionContext.getRequest().getParameter("products") != null) {
				String productstr = ServletActionContext.getRequest()
						.getParameter("products");
				if (null != productstr && !productstr.equals("null")
						&& !productstr.equals("")) {
					String productArray[] = splitValues(
							productstr.substring(1, productstr.length() - 1),
							",");
					products = new HashSet<Long>();
					for (String string : productArray)
						products.add(Long.valueOf(string));
				}
			}
			if (ServletActionContext.getRequest().getParameter("suppliers") != null) {
				String supplierstr = ServletActionContext.getRequest()
						.getParameter("suppliers");
				if (null != supplierstr && !supplierstr.equals("null")
						&& !supplierstr.equals("")) {
					String supplierArray[] = splitValues(
							supplierstr.substring(1, supplierstr.length() - 1),
							",");
					suppliers = new HashSet<Long>();
					for (String string : supplierArray)
						suppliers.add(Long.valueOf(string));
				}
			}
			List<Purchase> purchases = accountsBL
					.getPurchaseBL()
					.getPurchaseService()
					.gePurchaseSuppierComparision(products, suppliers,
							fromDate, toDate);
			if (null != purchases && purchases.size() > 0) {
				Map<Long, InventoryComparisonVO> inventoryComparisonVOMap = null;
				InventoryComparisonVO inventoryComparisonVO = null;
				Map<Long, Map<Long, InventoryComparisonVO>> supplierMap = new HashMap<Long, Map<Long, InventoryComparisonVO>>();
				Map<Long, List<InventoryComparisonVO>> productMap = new HashMap<Long, List<InventoryComparisonVO>>();
				List<InventoryComparisonVO> inventoryComparisonVOs = null;
				Map<String, List<PurchaseDetail>> purchaseDetailMap = new HashMap<String, List<PurchaseDetail>>();
				List<PurchaseDetail> purchaseDetailtmp = null;
				List<PurchaseDetail> purchaseFinalDetails = new ArrayList<PurchaseDetail>();
				for (Purchase purchase : purchases) {
					for (PurchaseDetail purchaseDt : purchase
							.getPurchaseDetails()) {
						purchaseDt.setPurchase(purchase);
						productMap.put(purchaseDt.getProduct().getProductId(),
								null);
						purchaseFinalDetails.add(purchaseDt);
						purchaseDetailtmp = new ArrayList<PurchaseDetail>();
						if (purchaseDetailMap.containsKey(purchaseDt
								.getProduct().getProductId()
								+ "-"
								+ purchase.getSupplier().getSupplierId())) {
							purchaseDetailtmp.addAll(purchaseDetailMap
									.get(purchaseDt.getProduct().getProductId()
											+ "-"
											+ purchase.getSupplier()
													.getSupplierId()));
						}
						purchaseDetailtmp.add(purchaseDt);
						purchaseDetailMap.put(purchaseDt.getProduct()
								.getProductId()
								+ "-"
								+ purchase.getSupplier().getSupplierId(),
								purchaseDetailtmp);
					}
				}

				List<SupplierVO> supplierVOs = new ArrayList<SupplierVO>();
				SupplierVO supplierVO = null;
				for (PurchaseDetail purchaseDt : purchaseFinalDetails) {
					if (!supplierMap.containsKey(purchaseDt.getPurchase()
							.getSupplier().getSupplierId())) {
						supplierMap.put(purchaseDt.getPurchase().getSupplier()
								.getSupplierId(), null);
						supplierVO = new SupplierVO();
						supplierVO.setSupplierId(purchaseDt.getPurchase()
								.getSupplier().getSupplierId());
						if (null != purchaseDt.getPurchase().getSupplier()
								.getPerson()) {
							supplierVO.setSupplierName(purchaseDt
									.getPurchase()
									.getSupplier()
									.getPerson()
									.getFirstName()
									.concat(" ")
									.concat(purchaseDt.getPurchase()
											.getSupplier().getPerson()
											.getLastName()));
						} else if ((null != purchaseDt.getPurchase()
								.getSupplier().getCmpDeptLocation() || null != purchaseDt
								.getPurchase().getSupplier().getCompany())) {
							supplierVO
									.setSupplierName(null != purchaseDt
											.getPurchase().getSupplier()
											.getCmpDeptLocation()
											&& !("").equals(purchaseDt
													.getPurchase()
													.getSupplier()
													.getCmpDeptLocation()) ? purchaseDt
											.getPurchase().getSupplier()
											.getCmpDeptLocation().getCompany()
											.getCompanyName()
											: purchaseDt.getPurchase()
													.getSupplier().getCompany()
													.getCompanyName());
						}
						supplierVOs.add(supplierVO);
					}
				}
				if (null != purchaseFinalDetails
						&& purchaseFinalDetails.size() > 0) {
					for (PurchaseDetail purchaseDt : purchaseFinalDetails) {
						inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
						if (supplierMap.containsKey(purchaseDt.getPurchase()
								.getSupplier().getSupplierId())) {
							inventoryComparisonVOMap = supplierMap
									.get(purchaseDt.getPurchase().getSupplier()
											.getSupplierId());
							if (null != inventoryComparisonVOMap
									&& inventoryComparisonVOMap.size() > 0) {
								for (Entry<Long, InventoryComparisonVO> entry : inventoryComparisonVOMap
										.entrySet()) {
									inventoryComparisonVO = new InventoryComparisonVO();
									if ((long) purchaseDt.getProduct()
											.getProductId() == (long) entry
											.getKey()) {
										inventoryComparisonVO = entry
												.getValue();
										inventoryComparisonVO
												.setTotalQty(inventoryComparisonVO
														.getTotalQty()
														+ purchaseDt
																.getQuantity());
										inventoryComparisonVO
												.setTotalUnitPrice(inventoryComparisonVO
														.getTotalUnitPrice()
														+ purchaseDt
																.getUnitRate());
									} else {
										inventoryComparisonVO
												.setTotalQty(purchaseDt
														.getQuantity());
										inventoryComparisonVO
												.setTotalUnitPrice(purchaseDt
														.getUnitRate());
										inventoryComparisonVO
												.setProductCode(purchaseDt
														.getProduct().getCode());
										inventoryComparisonVO
												.setProductName(purchaseDt
														.getProduct()
														.getProductName());
										inventoryComparisonVO
												.setSupplierId(purchaseDt
														.getPurchase()
														.getSupplier()
														.getSupplierId());
										inventoryComparisonVO
												.setProductId(purchaseDt
														.getProduct()
														.getProductId());
									}
								}
							} else {
								inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setTotalQty(purchaseDt
										.getQuantity());
								inventoryComparisonVO
										.setTotalUnitPrice(purchaseDt
												.getUnitRate());
								inventoryComparisonVO.setProductCode(purchaseDt
										.getProduct().getCode());
								inventoryComparisonVO.setProductName(purchaseDt
										.getProduct().getProductName());
								inventoryComparisonVO.setProductId(purchaseDt
										.getProduct().getProductId());
								inventoryComparisonVO.setSupplierId(purchaseDt
										.getPurchase().getSupplier()
										.getSupplierId());
							}
							if (null != purchaseDetailMap
									&& purchaseDetailMap.size() > 0) {
								purchaseDetailtmp = purchaseDetailMap
										.get(inventoryComparisonVO
												.getProductId()
												+ "-"
												+ inventoryComparisonVO
														.getSupplierId());
								if (null != purchaseDetailtmp
										&& purchaseDetailtmp.size() > 0) {
									Collections.sort(purchaseDetailtmp,
											new Comparator<PurchaseDetail>() {
												public int compare(
														PurchaseDetail o1,
														PurchaseDetail o2) {
													return o2
															.getPurchaseDetailId()
															.compareTo(
																	o1.getPurchaseDetailId());
												}
											});
									inventoryComparisonVO
											.setUnitPrice(AIOSCommons
													.formatAmount(purchaseDetailtmp
															.get(0)
															.getUnitRate()));
									inventoryComparisonVO
											.setUnitRate(AIOSCommons
													.formatAmountToDouble(inventoryComparisonVO
															.getUnitPrice()));
								} else {
									inventoryComparisonVO.setUnitPrice("-");
									inventoryComparisonVO.setUnitRate(0.0);
								}
							}
							inventoryComparisonVOMap.put(purchaseDt
									.getProduct().getProductId(),
									inventoryComparisonVO);
							supplierMap.put(purchaseDt.getPurchase()
									.getSupplier().getSupplierId(),
									inventoryComparisonVOMap);
						}
					}
				}
				if (null != supplierMap && supplierMap.size() > 0) {
					Collections.sort(supplierVOs, new Comparator<SupplierVO>() {
						public int compare(SupplierVO o1, SupplierVO o2) {
							return o1.getSupplierId().compareTo(
									o2.getSupplierId());
						}
					});
					for (Entry<Long, Map<Long, InventoryComparisonVO>> entry : supplierMap
							.entrySet()) {
						inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
						inventoryComparisonVOMap = entry.getValue();
						if (null != inventoryComparisonVOMap
								&& inventoryComparisonVOMap.size() > 0) {
							for (Entry<Long, InventoryComparisonVO> prdmap : inventoryComparisonVOMap
									.entrySet()) {
								inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
								if (productMap.containsKey(prdmap.getKey())) {
									inventoryComparisonVOs = productMap
											.get(prdmap.getKey());
									if (null == inventoryComparisonVOs)
										inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
									inventoryComparisonVOs.add(prdmap
											.getValue());
									productMap.put(prdmap.getKey(),
											inventoryComparisonVOs);
								}
							}
						}
					}
				}
				Set<Long> supplierSet = null;
				Set<Long> supplierInvSet = null;
				for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
						.entrySet()) {
					supplierSet = new HashSet<Long>();
					for (Long sp : supplierMap.keySet())
						supplierSet.add(sp);
					if (null != entry && null != entry.getValue()) {
						supplierInvSet = new HashSet<Long>();
						inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
								entry.getValue());
						for (InventoryComparisonVO inv : inventoryComparisonVOs)
							supplierInvSet.add(inv.getSupplierId());
						Collection<Long> similar = new HashSet<Long>(
								supplierSet);
						Collection<Long> different = new HashSet<Long>();
						different.addAll(supplierSet);
						different.addAll(supplierInvSet);
						similar.retainAll(supplierInvSet);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							for (Long sp : different) {
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setSupplierId(sp);
								inventoryComparisonVO.setUnitPrice("-");
								inventoryComparisonVO.setUnitRate(0.0);
								inventoryComparisonVOs
										.add(inventoryComparisonVO);
							}
						}
					} else {
						if (null != supplierSet && supplierSet.size() > 0) {
							inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
							for (Long sp : supplierSet) {
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setSupplierId(sp);
								inventoryComparisonVO.setUnitPrice("-");
								inventoryComparisonVO.setUnitRate(0.0);
								inventoryComparisonVOs
										.add(inventoryComparisonVO);
							}
						}
					}
					productMap.put(entry.getKey(), inventoryComparisonVOs);
				}
				Product product = null;
				List<InventoryComparisonVO> purchaseComparisonVOs = new ArrayList<InventoryComparisonVO>();
				for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
						.entrySet()) {
					if (null != entry && null != entry.getValue()) {
						inventoryComparisonVO = new InventoryComparisonVO();
						inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
								entry.getValue());
						product = accountsBL.getAccountsEnterpriseService()
								.getProductService()
								.getBasicProductById(entry.getKey());
						inventoryComparisonVO.setProductCode(product.getCode());
						inventoryComparisonVO.setProductName(product
								.getProductName());
						double min = inventoryComparisonVOs.get(0)
								.getUnitRate();
						for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
							if (min == 0
									&& inventoryComparisonVOs.get(i)
											.getUnitRate() > 0)
								min = inventoryComparisonVOs.get(i)
										.getUnitRate();
							if (inventoryComparisonVOs.get(i).getUnitRate() > 0
									&& min >= inventoryComparisonVOs.get(i)
											.getUnitRate())
								min = inventoryComparisonVOs.get(i)
										.getUnitRate();
						}
						for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
							if ((double) inventoryComparisonVOs.get(i)
									.getUnitRate() == min)
								inventoryComparisonVOs.get(i).setPriceLevel(
										"minPrice");
						}
						Collections.sort(inventoryComparisonVOs,
								new Comparator<InventoryComparisonVO>() {
									public int compare(
											InventoryComparisonVO o1,
											InventoryComparisonVO o2) {
										return o1.getSupplierId().compareTo(
												o2.getSupplierId());
									}
								});
						inventoryComparisonVO
								.setInventoryComparisonVOs(inventoryComparisonVOs);
						purchaseComparisonVOs.add(inventoryComparisonVO);
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"PURCHASE_COMPARISON", purchaseComparisonVOs);
				ServletActionContext.getRequest().setAttribute("SUPPLIER_LIST",
						supplierVOs);
			}
			LOGGER.info("Module: Accounts : Method: purchaseSupplierComparisonReport Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: purchaseSupplierComparisonReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String salesCustomerComparisonReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: salesCustomerComparisonReport ");
			getImplementId();
			Set<Long> products = null;
			Set<Long> customers = null;
			if (ServletActionContext.getRequest().getParameter("products") != null) {
				String productstr = ServletActionContext.getRequest()
						.getParameter("products");
				if (null != productstr && !productstr.equals("null")
						&& !productstr.equals("")) {
					String productArray[] = splitValues(
							productstr.substring(1, productstr.length() - 1),
							",");
					products = new HashSet<Long>();
					for (String string : productArray)
						products.add(Long.valueOf(string));
				}
			}
			if (ServletActionContext.getRequest().getParameter("customers") != null) {
				String customerstr = ServletActionContext.getRequest()
						.getParameter("customers");
				if (null != customerstr && !customerstr.equals("null")
						&& !customerstr.equals("")) {
					String customerArray[] = splitValues(
							customerstr.substring(1, customerstr.length() - 1),
							",");
					customers = new HashSet<Long>();
					for (String string : customerArray)
						customers.add(Long.valueOf(string));
				}
			}
			List<SalesDeliveryNote> salesDeliveries = accountsBL
					.getSalesInvoiceBL()
					.getSalesDeliveryNoteBL()
					.getSalesDeliveryNoteService()
					.geCustomerSalesComparision(products, customers, fromDate,
							toDate);
			if (null != salesDeliveries && salesDeliveries.size() > 0) {
				Map<Long, InventoryComparisonVO> inventoryComparisonVOMap = null;
				InventoryComparisonVO inventoryComparisonVO = null;
				Map<Long, Map<Long, InventoryComparisonVO>> customerMap = new HashMap<Long, Map<Long, InventoryComparisonVO>>();
				Map<Long, List<InventoryComparisonVO>> productMap = new HashMap<Long, List<InventoryComparisonVO>>();
				List<InventoryComparisonVO> inventoryComparisonVOs = null;
				Map<String, List<SalesDeliveryDetail>> salesDetailMap = new HashMap<String, List<SalesDeliveryDetail>>();
				List<SalesDeliveryDetail> salesDetailtmp = null;
				List<SalesDeliveryDetail> salesFinalDetails = new ArrayList<SalesDeliveryDetail>();
				for (SalesDeliveryNote salesDelivery : salesDeliveries) {
					for (SalesDeliveryDetail salesDt : salesDelivery
							.getSalesDeliveryDetails()) {
						salesDt.setSalesDeliveryNote(salesDelivery);
						productMap.put(salesDt.getProduct().getProductId(),
								null);
						salesFinalDetails.add(salesDt);
						salesDetailtmp = new ArrayList<SalesDeliveryDetail>();
						if (salesDetailMap.containsKey(salesDt.getProduct()
								.getProductId()
								+ "-"
								+ salesDelivery.getCustomer().getCustomerId())) {
							salesDetailtmp.addAll(salesDetailMap.get(salesDt
									.getProduct().getProductId()
									+ "-"
									+ salesDelivery.getCustomer()
											.getCustomerId()));
						}
						salesDetailtmp.add(salesDt);
						salesDetailMap.put(salesDt.getProduct().getProductId()
								+ "-"
								+ salesDelivery.getCustomer().getCustomerId(),
								salesDetailtmp);
					}
				}

				List<CustomerVO> customerVOs = new ArrayList<CustomerVO>();
				CustomerVO customerVO = null;
				for (SalesDeliveryDetail salesDt : salesFinalDetails) {
					if (!customerMap.containsKey(salesDt.getSalesDeliveryNote()
							.getCustomer().getCustomerId())) {
						customerMap.put(salesDt.getSalesDeliveryNote()
								.getCustomer().getCustomerId(), null);
						customerVO = new CustomerVO();
						customerVO.setCustomerId(salesDt.getSalesDeliveryNote()
								.getCustomer().getCustomerId());
						if (null != salesDt.getSalesDeliveryNote()
								.getCustomer().getPersonByPersonId()) {
							customerVO.setCustomerName(salesDt
									.getSalesDeliveryNote()
									.getCustomer()
									.getPersonByPersonId()
									.getFirstName()
									.concat(" ")
									.concat(salesDt.getSalesDeliveryNote()
											.getCustomer()
											.getPersonByPersonId()
											.getLastName()));
						} else if ((null != salesDt.getSalesDeliveryNote()
								.getCustomer().getCmpDeptLocation() || null != salesDt
								.getSalesDeliveryNote().getCustomer()
								.getCompany())) {
							customerVO
									.setCustomerName(null != salesDt
											.getSalesDeliveryNote()
											.getCustomer().getCmpDeptLocation()
											&& !("").equals(salesDt
													.getSalesDeliveryNote()
													.getCustomer()
													.getCmpDeptLocation()) ? salesDt
											.getSalesDeliveryNote()
											.getCustomer().getCmpDeptLocation()
											.getCompany().getCompanyName()
											: salesDt.getSalesDeliveryNote()
													.getCustomer().getCompany()
													.getCompanyName());
						}
						customerVOs.add(customerVO);
					}
				}
				if (null != salesFinalDetails && salesFinalDetails.size() > 0) {
					for (SalesDeliveryDetail salesDt : salesFinalDetails) {
						inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
						if (customerMap.containsKey(salesDt
								.getSalesDeliveryNote().getCustomer()
								.getCustomerId())) {
							inventoryComparisonVOMap = customerMap.get(salesDt
									.getSalesDeliveryNote().getCustomer()
									.getCustomerId());
							if (null != inventoryComparisonVOMap
									&& inventoryComparisonVOMap.size() > 0) {
								for (Entry<Long, InventoryComparisonVO> entry : inventoryComparisonVOMap
										.entrySet()) {
									inventoryComparisonVO = new InventoryComparisonVO();
									if ((long) salesDt.getProduct()
											.getProductId() == (long) entry
											.getKey()) {
										inventoryComparisonVO = entry
												.getValue();
										inventoryComparisonVO
												.setTotalQty(inventoryComparisonVO
														.getTotalQty()
														+ salesDt
																.getDeliveryQuantity());
										inventoryComparisonVO
												.setTotalUnitPrice(inventoryComparisonVO
														.getTotalUnitPrice()
														+ salesDt.getUnitRate());
									} else {
										inventoryComparisonVO
												.setTotalQty(salesDt
														.getDeliveryQuantity());
										inventoryComparisonVO
												.setTotalUnitPrice(salesDt
														.getUnitRate());
										inventoryComparisonVO
												.setProductCode(salesDt
														.getProduct().getCode());
										inventoryComparisonVO
												.setProductName(salesDt
														.getProduct()
														.getProductName());
										inventoryComparisonVO
												.setCustomerId(salesDt
														.getSalesDeliveryNote()
														.getCustomer()
														.getCustomerId());
										inventoryComparisonVO
												.setProductId(salesDt
														.getProduct()
														.getProductId());
									}
								}
							} else {
								inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setTotalQty(salesDt
										.getDeliveryQuantity());
								inventoryComparisonVO.setTotalUnitPrice(salesDt
										.getUnitRate());
								inventoryComparisonVO.setProductCode(salesDt
										.getProduct().getCode());
								inventoryComparisonVO.setProductName(salesDt
										.getProduct().getProductName());
								inventoryComparisonVO.setProductId(salesDt
										.getProduct().getProductId());
								inventoryComparisonVO.setCustomerId(salesDt
										.getSalesDeliveryNote().getCustomer()
										.getCustomerId());
							}
							if (null != salesDetailMap
									&& salesDetailMap.size() > 0) {
								salesDetailtmp = salesDetailMap
										.get(inventoryComparisonVO
												.getProductId()
												+ "-"
												+ inventoryComparisonVO
														.getCustomerId());
								if (null != salesDetailtmp
										&& salesDetailtmp.size() > 0) {
									Collections
											.sort(salesDetailtmp,
													new Comparator<SalesDeliveryDetail>() {
														public int compare(
																SalesDeliveryDetail o1,
																SalesDeliveryDetail o2) {
															return o2
																	.getSalesDeliveryDetailId()
																	.compareTo(
																			o1.getSalesDeliveryDetailId());
														}
													});
									inventoryComparisonVO
											.setUnitPrice(AIOSCommons
													.formatAmount(salesDetailtmp
															.get(0)
															.getUnitRate()));
									inventoryComparisonVO
											.setUnitRate(AIOSCommons
													.formatAmountToDouble(inventoryComparisonVO
															.getUnitPrice()));
								} else {
									inventoryComparisonVO.setUnitPrice("-");
									inventoryComparisonVO.setUnitRate(0.0);
								}
							}
							inventoryComparisonVOMap.put(salesDt.getProduct()
									.getProductId(), inventoryComparisonVO);
							customerMap.put(salesDt.getSalesDeliveryNote()
									.getCustomer().getCustomerId(),
									inventoryComparisonVOMap);
						}
					}
				}
				if (null != customerMap && customerMap.size() > 0) {
					Collections.sort(customerVOs, new Comparator<CustomerVO>() {
						public int compare(CustomerVO o1, CustomerVO o2) {
							return o1.getCustomerId().compareTo(
									o2.getCustomerId());
						}
					});
					for (Entry<Long, Map<Long, InventoryComparisonVO>> entry : customerMap
							.entrySet()) {
						inventoryComparisonVOMap = new HashMap<Long, InventoryComparisonVO>();
						inventoryComparisonVOMap = entry.getValue();
						if (null != inventoryComparisonVOMap
								&& inventoryComparisonVOMap.size() > 0) {
							for (Entry<Long, InventoryComparisonVO> prdmap : inventoryComparisonVOMap
									.entrySet()) {
								inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
								if (productMap.containsKey(prdmap.getKey())) {
									inventoryComparisonVOs = productMap
											.get(prdmap.getKey());
									if (null == inventoryComparisonVOs)
										inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
									inventoryComparisonVOs.add(prdmap
											.getValue());
									productMap.put(prdmap.getKey(),
											inventoryComparisonVOs);
								}
							}
						}
					}
				}
				Set<Long> customerSet = null;
				Set<Long> customerInvSet = null;
				for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
						.entrySet()) {
					customerSet = new HashSet<Long>();
					for (Long sp : customerMap.keySet())
						customerSet.add(sp);
					if (null != entry && null != entry.getValue()) {
						customerInvSet = new HashSet<Long>();
						inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
								entry.getValue());
						for (InventoryComparisonVO inv : inventoryComparisonVOs)
							customerInvSet.add(inv.getCustomerId());
						Collection<Long> similar = new HashSet<Long>(
								customerSet);
						Collection<Long> different = new HashSet<Long>();
						different.addAll(customerSet);
						different.addAll(customerInvSet);
						similar.retainAll(customerInvSet);
						different.removeAll(similar);
						if (null != different && different.size() > 0) {
							for (Long sp : different) {
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setCustomerId(sp);
								inventoryComparisonVO.setUnitPrice("-");
								inventoryComparisonVO.setUnitRate(0.0);
								inventoryComparisonVOs
										.add(inventoryComparisonVO);
							}
						}
					} else {
						if (null != customerSet && customerSet.size() > 0) {
							inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
							for (Long sp : customerSet) {
								inventoryComparisonVO = new InventoryComparisonVO();
								inventoryComparisonVO.setCustomerId(sp);
								inventoryComparisonVO.setUnitPrice("-");
								inventoryComparisonVO.setUnitRate(0.0);
								inventoryComparisonVOs
										.add(inventoryComparisonVO);
							}
						}
					}
					productMap.put(entry.getKey(), inventoryComparisonVOs);
				}
				Product product = null;
				List<InventoryComparisonVO> salesComparisonVOs = new ArrayList<InventoryComparisonVO>();
				for (Entry<Long, List<InventoryComparisonVO>> entry : productMap
						.entrySet()) {
					if (null != entry && null != entry.getValue()) {
						inventoryComparisonVO = new InventoryComparisonVO();
						inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>(
								entry.getValue());
						product = accountsBL.getAccountsEnterpriseService()
								.getProductService()
								.getBasicProductById(entry.getKey());
						inventoryComparisonVO.setProductCode(product.getCode());
						inventoryComparisonVO.setProductName(product
								.getProductName());
						double min = inventoryComparisonVOs.get(0)
								.getUnitRate();
						for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
							if (min == 0
									&& inventoryComparisonVOs.get(i)
											.getUnitRate() > 0)
								min = inventoryComparisonVOs.get(i)
										.getUnitRate();
							if (inventoryComparisonVOs.get(i).getUnitRate() > 0
									&& min >= inventoryComparisonVOs.get(i)
											.getUnitRate())
								min = inventoryComparisonVOs.get(i)
										.getUnitRate();
						}
						for (int i = 0; i < inventoryComparisonVOs.size(); i++) {
							if ((double) inventoryComparisonVOs.get(i)
									.getUnitRate() == min)
								inventoryComparisonVOs.get(i).setPriceLevel(
										"minPrice");
						}
						Collections.sort(inventoryComparisonVOs,
								new Comparator<InventoryComparisonVO>() {
									public int compare(
											InventoryComparisonVO o1,
											InventoryComparisonVO o2) {
										return o1.getCustomerId().compareTo(
												o2.getCustomerId());
									}
								});
						inventoryComparisonVO
								.setInventoryComparisonVOs(inventoryComparisonVOs);
						salesComparisonVOs.add(inventoryComparisonVO);
					}
				}
				ServletActionContext.getRequest().setAttribute(
						"SALES_COMPARISON", salesComparisonVOs);
				ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
						customerVOs);
			}
			LOGGER.info("Module: Accounts : Method: salesCustomerComparisonReport Success ");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: salesCustomerComparisonReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String receiveVsIssuanceReportCriteria() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: receiveVsIssuanceReportCriteria ");
			getImplementId();
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			List<SupplierVO> supplierVOs = null;
			List<Supplier> suppliers = accountsBL.getSupplierBL()
					.getSupplierService().getAllSuppliers(implementation);
			if (null != suppliers && suppliers.size() > 0) {
				supplierVOs = new ArrayList<SupplierVO>();
				for (Supplier supplier : suppliers)
					supplierVOs.add(accountsBL.getSupplierBL().addSupplierVO(
							supplier));
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute("SUPPLIER_INFO",
					supplierVOs);
			ServletActionContext.getRequest().setAttribute(
					"SECTIONS",
					accountsBL.getIssueRequistionBL().getLookupMasterBL()
							.getActiveLookupDetails("SECTIONS", true));
			ServletActionContext.getRequest().setAttribute(
					"DEPARTMENT_LIST",
					accountsBL.getDepartmentBL().getDepartmentService()
							.getAllDepartments(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: receiveVsIssuanceReportCriteria Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String receiveVsIssuanceReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: receiveVsIssuanceReport ");
			getImplementId();
			List<ReceiveVO> receiveVOsAll = receiveVsIssuanceReportReveivePart();
			List<IssueRequistionVO> issueRequisitionList = this
					.receiveVsIssuanceReportIssuancePart();
			Map<Long, ProductVO> productMaps = new HashMap<Long, ProductVO>();
			Date fromDatee = null;
			Map<Long, ProductVO> productsMapFromStock = new HashMap<Long, ProductVO>();

			// Adding products from selection list
			Set<Long> productIds = new HashSet<Long>();
			String products = null;
			if (ServletActionContext.getRequest().getParameter("products") != null) {
				products = ServletActionContext.getRequest().getParameter(
						"products");

				for (String str : products.split(",")) {
					if (str != null && !str.equals(""))
						productIds.add(Long.valueOf(str));
				}

				List<Product> producstsFromChoosenList = accountsEnterpriseService
						.getProductService().getActiveProductByProductList(
								productIds);
				ProductVO productVO = null;
				for (Product prd : producstsFromChoosenList) {
					productVO = new ProductVO();
					BeanUtils.copyProperties(productVO, prd);
					productVO.setUnitRate(0.0);
					productVO.setIssuedTotalAmount(0.0);
					productVO.setIssuedQuantity(0.0);
					productVO.setReceivedTotalAmount(0.0);
					productVO.setReceivedQuantity(0.0);
					productVO.setIssuanceNumbers("");
					productVO.setIssuanceDates("");
					productVO.setReceiveNumbers("");
					productVO.setReceiveDates("");
					productMaps.put(prd.getProductId(), productVO);
				}
			}

			if (fromDate == null || fromDate.equals("")) {
				fromDatee = new Date();
			} else {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			LocalDate date1 = new LocalDate(fromDatee);
			LocalDate previousMonth = date1.minus(org.joda.time.Period
					.months(1));
			String preMonth = previousMonth.toString("MMMM,yyyy");
			List<StockPeriodic> periodicSotcks = accountsBL.getStockBL()
					.getStockService()
					.getStockPeriodicByMonth(getImplementation(), preMonth);
			ProductVO tempProductVO = null;
			for (StockPeriodic stockPeriodic : periodicSotcks) {
				if (productsMapFromStock.containsKey(stockPeriodic.getProduct()
						.getProductId())) {
					tempProductVO = productsMapFromStock.get(stockPeriodic
							.getProduct().getProductId());
					BeanUtils.copyProperties(tempProductVO,
							stockPeriodic.getProduct());
					tempProductVO.setQuantity(stockPeriodic.getQuantity());
				} else {
					tempProductVO = new ProductVO();
					BeanUtils.copyProperties(tempProductVO,
							stockPeriodic.getProduct());
					tempProductVO.setQuantity(stockPeriodic.getQuantity());
					productsMapFromStock.put(stockPeriodic.getProduct()
							.getProductId(), tempProductVO);
				}
			}

			for (IssueRequistionVO issueRequistionVO : issueRequisitionList) {
				String issuanceNumbers = "";
				String issuanceDates = "";
				for (IssueRequistionDetailVO detailVO : issueRequistionVO
						.getIssueRequistionDetailVO()) {

					ProductVO productVO = null;
					if (productMaps.containsKey(detailVO.getProduct()
							.getProductId())) {
						productVO = productMaps.get(detailVO.getProduct()
								.getProductId());
						productVO.setUnitRate(detailVO.getUnitRate());
						issuanceNumbers = productVO.getIssuanceNumbers() + ", "
								+ issueRequistionVO.getReferenceNumber();
						issuanceDates = productVO.getIssuanceDates()
								+ ", "
								+ DateFormat
										.convertDateToString(issueRequistionVO
												.getIssueDate() + "");
						productVO.setIssuedTotalAmount(productVO
								.getIssuedTotalAmount()
								+ detailVO.getTotalRate());
						productVO.setIssuanceNumbers(issuanceNumbers);
						productVO.setIssuanceDates(issuanceDates);
						productVO.setIssuedQuantity(productVO
								.getIssuedQuantity() + detailVO.getIssuedQty());
					} else {
						productVO = new ProductVO();
						BeanUtils.copyProperties(productVO,
								detailVO.getProduct());
						productVO.setUnitRate(detailVO.getUnitRate());
						productVO.setIssuedTotalAmount(detailVO.getTotalRate());
						productVO.setIssuedQuantity(detailVO.getIssuedQty());
						productVO.setReceivedTotalAmount(0.0);
						productVO.setReceivedQuantity(0.0);
						productVO.setIssuanceNumbers(issueRequistionVO
								.getReferenceNumber());
						productVO.setIssuanceDates(DateFormat
								.convertDateToString(issueRequistionVO
										.getIssueDate() + ""));
					}
					productMaps.put(detailVO.getProduct().getProductId(),
							productVO);
				}
			}

			for (ReceiveVO receiveVO : receiveVOsAll) {
				String receiveNumbers = "";
				String receiveDates = "";

				for (ReceiveDetailVO detailVO : receiveVO.getReceiveDetailsVO()) {
					ProductVO productVO = null;
					if (productMaps.containsKey(detailVO.getProduct()
							.getProductId())) {
						productVO = productMaps.get(detailVO.getProduct()
								.getProductId());
						productVO.setUnitRate(detailVO.getUnitRate());
						receiveNumbers = productVO.getReceiveNumbers() + ","
								+ receiveVO.getReceiveNumber();
						receiveDates = productVO.getReceiveDates()
								+ ","
								+ DateFormat.convertDateToString(receiveVO
										.getReceiveDate() + "");
						productVO.setReceivedTotalAmount(productVO
								.getReceivedTotalAmount()
								+ detailVO.getTotalRate());
						productVO.setReceiveNumbers(receiveNumbers);
						productVO.setReceiveDates(receiveDates);
						productVO.setReceivedQuantity(productVO
								.getReceivedQuantity()
								+ detailVO.getReceiveQty());
					} else {
						productVO = new ProductVO();
						BeanUtils.copyProperties(productVO,
								detailVO.getProduct());
						productVO.setUnitRate(detailVO.getUnitRate());
						productVO.setReceivedTotalAmount(detailVO
								.getTotalRate());
						productVO.setReceivedQuantity(detailVO.getReceiveQty());
						productVO.setReceiveNumbers(receiveVO
								.getReceiveNumber());
						productVO.setReceiveDates(DateFormat
								.convertDateToString(receiveVO.getReceiveDate()
										+ ""));
						productVO.setIssuedTotalAmount(0.0);
						productVO.setIssuedQuantity(0.0);
					}
					productMaps.put(detailVO.getProduct().getProductId(),
							productVO);
				}
			}
			List<ProductVO> vos = new ArrayList<ProductVO>(productMaps.values());
			Collections.sort(vos, new Comparator<ProductVO>() {
				public int compare(ProductVO o1, ProductVO o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
			StringBuilder purchaseNumber = null;
			String[] strArray = null;
			Set<String> purchaseNoSet = null;
			ProductVO tempVO = null;
			for (ProductVO vo : vos) {
				if (productsMapFromStock.containsKey(vo.getProductId())) {
					tempVO = productsMapFromStock.get(vo.getProductId());
					vo.setQuantity(tempVO.getQuantity());
				}

				vo.setBalanceQuantity((vo.getQuantity() + vo
						.getReceivedQuantity()) - vo.getIssuedQuantity());
				if (vo.getReceiveNumbers() != null) {
					strArray = (vo.getReceiveNumbers() != null ? vo
							.getReceiveNumbers().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setReceiveNumbers(purchaseNumber.toString());
				}

				if (vo.getReceiveDates() != null) {
					strArray = (vo.getReceiveDates() != null ? vo
							.getReceiveDates().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setReceiveDates(purchaseNumber.toString());
				}
				if (vo.getIssuanceNumbers() != null) {
					strArray = (vo.getIssuanceNumbers() != null ? vo
							.getIssuanceNumbers().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setIssuanceNumbers(purchaseNumber.toString());
				}

				if (vo.getIssuanceDates() != null) {
					strArray = (vo.getIssuanceDates() != null ? vo
							.getIssuanceDates().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo.trim()).append(",");

					vo.setIssuanceDates(purchaseNumber.toString());
				}
			}
			ServletActionContext.getRequest().setAttribute("printedOn",
					DateFormat.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("PRODUCT_LIST", vos);
			ServletActionContext.getRequest().setAttribute("datefrom",
					ServletActionContext.getRequest().getParameter("fromDate"));
			ServletActionContext.getRequest().setAttribute("dateto",
					ServletActionContext.getRequest().getParameter("toDate"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: receiveVsIssuanceReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String receiveVsIssuanceMonthlyReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: receiveVsIssuanceMonthlyReport ");
			getImplementId();
			List<ReceiveVO> receiveVOsAll = receiveVsIssuanceReportReveivePart();
			List<IssueRequistionVO> issueRequisitionList = this
					.receiveVsIssuanceReportIssuancePart();
			Map<Long, ProductVO> productMaps = new HashMap<Long, ProductVO>();
			Date fromDatee = null;
			Map<Long, ProductVO> productsMapFromStock = new HashMap<Long, ProductVO>();
			// Adding products from selection list
			Set<Long> productIds = new HashSet<Long>();
			String products = null;
			if (ServletActionContext.getRequest().getParameter("products") != null) {
				products = ServletActionContext.getRequest().getParameter(
						"products");

				for (String str : products.split(",")) {
					if (str != null && !str.equals(""))
						productIds.add(Long.valueOf(str));
				}

				List<Product> producstsFromChoosenList = accountsEnterpriseService
						.getProductService().getActiveProductByProductList(
								productIds);
				ProductVO productVO = null;
				for (Product prd : producstsFromChoosenList) {
					productVO = new ProductVO();
					BeanUtils.copyProperties(productVO, prd);
					productVO.setUnitRate(0.0);
					productVO.setIssuedTotalAmount(0.0);
					productVO.setIssuedQuantity(0.0);
					productVO.setReceivedTotalAmount(0.0);
					productVO.setReceivedQuantity(0.0);
					productVO.setIssuanceNumbers("");
					productVO.setIssuanceDates("");
					productVO.setReceiveNumbers("");
					productVO.setReceiveDates("");
					productMaps.put(prd.getProductId(), productVO);
				}
			}
			if (fromDate == null || fromDate.equals("")
					|| fromDate.equals("null")) {
				fromDatee = new Date();
			} else {
				fromDatee = DateFormat.convertStringToDate(fromDate);
			}
			LocalDate date1 = new LocalDate(fromDatee);
			LocalDate previousMonth = date1.minus(org.joda.time.Period
					.months(1));
			String preMonth = previousMonth.toString("MMMM,yyyy");
			List<StockPeriodic> periodicSotcks = accountsBL.getStockBL()
					.getStockService()
					.getStockPeriodicByMonth(getImplementation(), preMonth);
			ProductVO tempProductVO = null;
			Map<String, ProductVO> dateMaps = null;
			for (StockPeriodic stockPeriodic : periodicSotcks) {
				if (productsMapFromStock.containsKey(stockPeriodic.getProduct()
						.getProductId())) {
					tempProductVO = productsMapFromStock.get(stockPeriodic
							.getProduct().getProductId());
					BeanUtils.copyProperties(tempProductVO,
							stockPeriodic.getProduct());
					tempProductVO.setQuantity(stockPeriodic.getQuantity());
				} else {
					tempProductVO = new ProductVO();
					BeanUtils.copyProperties(tempProductVO,
							stockPeriodic.getProduct());
					tempProductVO.setQuantity(stockPeriodic.getQuantity());
					productsMapFromStock.put(stockPeriodic.getProduct()
							.getProductId(), tempProductVO);
				}
			}

			for (IssueRequistionVO issueRequistionVO : issueRequisitionList) {
				String issuanceNumbers = "";
				String issuanceDates = "";
				for (IssueRequistionDetailVO detailVO : issueRequistionVO
						.getIssueRequistionDetailVO()) {

					ProductVO productVO = null;
					if (productMaps.containsKey(detailVO.getProduct()
							.getProductId())) {
						productVO = productMaps.get(detailVO.getProduct()
								.getProductId());
						productVO.setUnitRate(detailVO.getUnitRate());
						issuanceNumbers = productVO.getIssuanceNumbers() + ", "
								+ issueRequistionVO.getReferenceNumber();
						issuanceDates = productVO.getIssuanceDates()
								+ ", "
								+ DateFormat
										.convertDateToString(issueRequistionVO
												.getIssueDate() + "");
						productVO.setIssuedTotalAmount(productVO
								.getIssuedTotalAmount()
								+ detailVO.getTotalRate());
						productVO.setIssuanceNumbers(issuanceNumbers);
						productVO.setIssuanceDates(issuanceDates);
						productVO.setIssuedQuantity(productVO
								.getIssuedQuantity() + detailVO.getIssuedQty());
						if (dateMaps == null) {
							dateMaps = new HashMap<String, ProductVO>();
						}
						if (dateMaps.containsKey(DateFormat
								.convertDateToString(detailVO
										.getIssueRequistion().getIssueDate()
										+ "")
								+ "_" + detailVO.getProduct().getProductId())) {
							ProductVO tempVo = dateMaps.get(DateFormat
									.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
									+ "_"
									+ detailVO.getProduct().getProductId());
							tempVo.setIssuedQuantity(tempVo.getIssuedQuantity()
									+ detailVO.getQuantity());
							dateMaps.put(
									DateFormat.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						} else {
							ProductVO tempVo = new ProductVO();
							tempVo.setIssuedQuantity(detailVO.getQuantity());
							tempVo.setReceivedQuantity(0.0);
							dateMaps.put(
									DateFormat.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						}
					} else {
						productVO = new ProductVO();
						dateMaps = new HashMap<String, ProductVO>();
						BeanUtils.copyProperties(productVO,
								detailVO.getProduct());
						productVO.setUnitRate(detailVO.getUnitRate());
						productVO.setIssuedTotalAmount(detailVO.getTotalRate());
						productVO.setIssuedQuantity(detailVO.getIssuedQty());
						productVO.setReceivedTotalAmount(0.0);
						productVO.setReceivedQuantity(0.0);
						productVO.setIssuanceNumbers(issueRequistionVO
								.getReferenceNumber());
						productVO.setIssuanceDates(DateFormat
								.convertDateToString(issueRequistionVO
										.getIssueDate() + ""));

						if (dateMaps.containsKey(DateFormat
								.convertDateToString(detailVO
										.getIssueRequistion().getIssueDate()
										+ "")
								+ "_" + detailVO.getProduct().getProductId())) {
							ProductVO tempVo = dateMaps.get(DateFormat
									.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
									+ "_"
									+ detailVO.getProduct().getProductId());
							tempVo.setIssuedQuantity(tempVo.getIssuedQuantity()
									+ detailVO.getQuantity());
							dateMaps.put(
									DateFormat.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						} else {
							ProductVO tempVo = new ProductVO();
							tempVo.setIssuedQuantity(detailVO.getQuantity());
							tempVo.setReceivedQuantity(0.0);
							dateMaps.put(
									DateFormat.convertDateToString(detailVO
											.getIssueRequistion()
											.getIssueDate()
											+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						}
					}
					productVO.setProductVOsMap(dateMaps);
					productMaps.put(detailVO.getProduct().getProductId(),
							productVO);
				}
			}

			for (ReceiveVO receiveVO : receiveVOsAll) {
				String receiveNumbers = "";
				String receiveDates = "";

				for (ReceiveDetailVO detailVO : receiveVO.getReceiveDetailsVO()) {
					ProductVO productVO = null;
					if (productMaps.containsKey(detailVO.getProduct()
							.getProductId())) {
						productVO = productMaps.get(detailVO.getProduct()
								.getProductId());
						productVO.setUnitRate(detailVO.getUnitRate());
						receiveNumbers = productVO.getReceiveNumbers() + ","
								+ receiveVO.getReceiveNumber();
						receiveDates = productVO.getReceiveDates()
								+ ","
								+ DateFormat.convertDateToString(receiveVO
										.getReceiveDate() + "");
						productVO.setReceivedTotalAmount(productVO
								.getReceivedTotalAmount()
								+ detailVO.getTotalRate());
						productVO.setReceiveNumbers(receiveNumbers);
						productVO.setReceiveDates(receiveDates);
						productVO.setReceivedQuantity(productVO
								.getReceivedQuantity()
								+ detailVO.getReceiveQty());
						if (dateMaps == null) {
							dateMaps = new HashMap<String, ProductVO>();
						}
						if (dateMaps.containsKey(DateFormat
								.convertDateToString(detailVO.getReceive()
										.getReceiveDate() + "")
								+ "_" + detailVO.getProduct().getProductId())) {
							ProductVO tempVo = dateMaps.get(DateFormat
									.convertDateToString(detailVO.getReceive()
											.getReceiveDate() + "")
									+ "_"
									+ detailVO.getProduct().getProductId());
							tempVo.setReceivedQuantity(tempVo
									.getReceivedQuantity()
									+ detailVO.getReceiveQty());
							dateMaps.put(
									DateFormat
											.convertDateToString(detailVO
													.getReceive()
													.getReceiveDate()
													+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						} else {
							ProductVO tempVo = new ProductVO();
							tempVo.setReceivedQuantity(detailVO.getReceiveQty());
							tempVo.setIssuedQuantity(0.0);
							dateMaps.put(
									DateFormat
											.convertDateToString(detailVO
													.getReceive()
													.getReceiveDate()
													+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						}
					} else {
						productVO = new ProductVO();
						dateMaps = new HashMap<String, ProductVO>();
						BeanUtils.copyProperties(productVO,
								detailVO.getProduct());
						productVO.setUnitRate(detailVO.getUnitRate());
						productVO.setReceivedTotalAmount(detailVO
								.getTotalRate());
						productVO.setReceivedQuantity(detailVO.getReceiveQty());
						productVO.setReceiveNumbers(receiveVO
								.getReceiveNumber());
						productVO.setReceiveDates(DateFormat
								.convertDateToString(receiveVO.getReceiveDate()
										+ ""));
						productVO.setIssuedTotalAmount(0.0);
						productVO.setIssuedQuantity(0.0);
						if (dateMaps.containsKey(DateFormat
								.convertDateToString(detailVO.getReceive()
										.getReceiveDate() + "")
								+ "_" + detailVO.getProduct().getProductId())) {
							ProductVO tempVo = dateMaps.get(DateFormat
									.convertDateToString(detailVO.getReceive()
											.getReceiveDate() + "")
									+ "_"
									+ detailVO.getProduct().getProductId());
							tempVo.setReceivedQuantity(tempVo
									.getReceivedQuantity()
									+ detailVO.getReceiveQty());
							dateMaps.put(
									DateFormat
											.convertDateToString(detailVO
													.getReceive()
													.getReceiveDate()
													+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						} else {
							ProductVO tempVo = new ProductVO();
							tempVo.setReceivedQuantity(detailVO.getReceiveQty());
							tempVo.setIssuedQuantity(0.0);
							dateMaps.put(
									DateFormat
											.convertDateToString(detailVO
													.getReceive()
													.getReceiveDate()
													+ "")
											+ "_"
											+ detailVO.getProduct()
													.getProductId(), tempVo);
						}
					}
					productVO.setProductVOsMap(dateMaps);
					productMaps.put(detailVO.getProduct().getProductId(),
							productVO);
				}
			}
			List<ProductVO> vos = new ArrayList<ProductVO>(productMaps.values());
			Collections.sort(vos, new Comparator<ProductVO>() {
				public int compare(ProductVO o1, ProductVO o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
			StringBuilder purchaseNumber = null;
			String[] strArray = null;
			Set<String> purchaseNoSet = null;
			ProductVO tempVO = null;
			Map<String, String> finalDateMap = new HashMap<String, String>();
			Collections.sort(vos, new Comparator<ProductVO>() {
				public int compare(ProductVO o1, ProductVO o2) {
					return o1.getProductName().compareTo(o2.getProductName());
				}
			});
			for (ProductVO vo : vos) {
				for (Entry<String, ProductVO> entry : vo.getProductVOsMap()
						.entrySet()) {
					finalDateMap.put(entry.getKey().split("_")[0], entry
							.getKey().split("_")[0]);
				}
				if (productsMapFromStock.containsKey(vo.getProductId())) {
					tempVO = productsMapFromStock.get(vo.getProductId());
					vo.setQuantity(tempVO.getQuantity());
				}

				vo.setBalanceQuantity((vo.getQuantity() + vo
						.getReceivedQuantity()) - vo.getIssuedQuantity());
				if (vo.getReceiveNumbers() != null) {
					strArray = (vo.getReceiveNumbers() != null ? vo
							.getReceiveNumbers().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setReceiveNumbers(purchaseNumber.toString());
				}

				if (vo.getReceiveDates() != null) {
					strArray = (vo.getReceiveDates() != null ? vo
							.getReceiveDates().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setReceiveDates(purchaseNumber.toString());
				}
				if (vo.getIssuanceNumbers() != null) {
					strArray = (vo.getIssuanceNumbers() != null ? vo
							.getIssuanceNumbers().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setIssuanceNumbers(purchaseNumber.toString());
				}

				if (vo.getIssuanceDates() != null) {
					strArray = (vo.getIssuanceDates() != null ? vo
							.getIssuanceDates().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo.trim()).append(",");

					vo.setIssuanceDates(purchaseNumber.toString());
				}
			}

			ServletActionContext.getRequest().setAttribute("printedOn",
					DateFormat.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("PRODUCT_LIST", vos);
			List<Date> dates = new ArrayList<Date>();
			for (Entry<String, String> entry : finalDateMap.entrySet()) {
				dates.add(DateFormat.convertStringToDate(entry.getKey()));
			}
			Collections.sort(dates, new Comparator<Date>() {
				public int compare(Date o1, Date o2) {
					return o1.compareTo(o2);
				}
			});
			finalDateMap = new LinkedHashMap<String, String>();
			java.util.Calendar cal = java.util.Calendar.getInstance();

			for (Date date : dates) {
				cal.setTime(date);

				int day = cal.get(java.util.Calendar.DAY_OF_MONTH);
				finalDateMap.put(DateFormat.convertSystemDateToString(date),
						day + "");
			}
			ServletActionContext.getRequest().setAttribute("PRODUCT_MASTER",
					finalDateMap);
			ServletActionContext.getRequest().setAttribute("datefrom",
					ServletActionContext.getRequest().getParameter("fromDate"));
			ServletActionContext.getRequest().setAttribute("dateto",
					ServletActionContext.getRequest().getParameter("toDate"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: receiveVsIssuanceMonthlyReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String issuanceBySectionReport() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: issuanceBySectionReport ");
			getImplementId();
			List<IssueRequistionVO> issueRequisitionList = this
					.receiveVsIssuanceReportIssuancePart();
			Map<Long, ProductVO> productMaps = new HashMap<Long, ProductVO>();
			// Adding products from selection list
			Set<Long> productIds = new HashSet<Long>();
			String products = null;
			if (ServletActionContext.getRequest().getParameter("products") != null) {
				products = ServletActionContext.getRequest().getParameter(
						"products");

				for (String str : products.split(",")) {
					if (str != null && !str.equals(""))
						productIds.add(Long.valueOf(str));
				}

				List<Product> producstsFromChoosenList = accountsEnterpriseService
						.getProductService().getActiveProductByProductList(
								productIds);
				ProductVO productVO = null;
				for (Product prd : producstsFromChoosenList) {
					productVO = new ProductVO();
					BeanUtils.copyProperties(productVO, prd);
					productVO.setUnitRate(0.0);
					productVO.setIssuedTotalAmount(0.0);
					productVO.setIssuedQuantity(0.0);
					productVO.setReceivedTotalAmount(0.0);
					productVO.setReceivedQuantity(0.0);
					productVO.setIssuanceNumbers("");
					productVO.setIssuanceDates("");
					productVO.setReceiveNumbers("");
					productVO.setReceiveDates("");
					productMaps.put(prd.getProductId(), productVO);
				}
			}

			Map<String, IssueRequistionDetailVO> sectionMaps = null;
			for (IssueRequistionVO issueRequistionVO : issueRequisitionList) {
				String issuanceNumbers = "";
				String issuanceDates = "";
				for (IssueRequistionDetailVO detailVO : issueRequistionVO
						.getIssueRequistionDetailVO()) {
					ProductVO productVO = null;
					if (productMaps.containsKey(detailVO.getProduct()
							.getProductId())) {
						productVO = productMaps.get(detailVO.getProduct()
								.getProductId());
						productVO.setUnitRate(detailVO.getUnitRate());
						issuanceNumbers = productVO.getIssuanceNumbers() + ", "
								+ issueRequistionVO.getReferenceNumber();
						issuanceDates = productVO.getIssuanceDates()
								+ ", "
								+ DateFormat
										.convertDateToString(issueRequistionVO
												.getIssueDate() + "");
						productVO.setIssuedTotalAmount(productVO
								.getIssuedTotalAmount()
								+ detailVO.getTotalRate());
						productVO.setIssuanceNumbers(issuanceNumbers);
						productVO.setIssuanceDates(issuanceDates);
						productVO.setIssuedQuantity(productVO
								.getIssuedQuantity() + detailVO.getIssuedQty());
						if (sectionMaps.containsKey(detailVO.getLookupDetail()
								.getDisplayName())) {
							IssueRequistionDetailVO tempVo = sectionMaps
									.get(detailVO.getLookupDetail()
											.getDisplayName());
							tempVo.setQuantity(tempVo.getQuantity()
									+ detailVO.getQuantity());
							tempVo.setTotalRate(tempVo.getTotalRate()
									+ detailVO.getTotalRate());
							sectionMaps.put(detailVO.getLookupDetail()
									.getDisplayName(), detailVO);
						} else {
							sectionMaps.put(detailVO.getLookupDetail()
									.getDisplayName(), detailVO);
						}

					} else {
						productVO = new ProductVO();
						sectionMaps = new HashMap<String, IssueRequistionDetailVO>();
						BeanUtils.copyProperties(productVO,
								detailVO.getProduct());
						productVO.setUnitRate(detailVO.getUnitRate());
						productVO.setIssuedTotalAmount(detailVO.getTotalRate());
						productVO.setIssuedQuantity(detailVO.getIssuedQty());
						productVO.setReceivedTotalAmount(0.0);
						productVO.setReceivedQuantity(0.0);
						productVO.setIssuanceNumbers(issueRequistionVO
								.getReferenceNumber());
						productVO.setIssuanceDates(DateFormat
								.convertDateToString(issueRequistionVO
										.getIssueDate() + ""));
						if (sectionMaps.containsKey(detailVO.getLookupDetail()
								.getDisplayName())) {
							IssueRequistionDetailVO tempVo = sectionMaps
									.get(detailVO.getLookupDetail()
											.getDisplayName());
							tempVo.setQuantity(tempVo.getQuantity()
									+ detailVO.getQuantity());
							tempVo.setTotalRate(tempVo.getTotalRate()
									+ detailVO.getTotalRate());
							sectionMaps.put(detailVO.getLookupDetail()
									.getDisplayName(), detailVO);
						} else {
							sectionMaps.put(detailVO.getLookupDetail()
									.getDisplayName(), detailVO);
						}

					}
					productVO
							.setIssueRequistionDetailVOWithoutList(sectionMaps);
					productMaps.put(detailVO.getProduct().getProductId(),
							productVO);
				}
			}

			List<ProductVO> vos = new ArrayList<ProductVO>(productMaps.values());
			Collections.sort(vos, new Comparator<ProductVO>() {
				public int compare(ProductVO o1, ProductVO o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
			StringBuilder purchaseNumber = null;
			String[] strArray = null;
			Set<String> purchaseNoSet = null;
			Map<String, String> finalSectionMap = new HashMap<String, String>();
			for (ProductVO vo : vos) {
				for (Entry<String, IssueRequistionDetailVO> entry : vo
						.getIssueRequistionDetailVOWithoutList().entrySet()) {
					finalSectionMap.put(entry.getKey(), entry.getKey());
				}
				if (vo.getIssuanceNumbers() != null) {
					strArray = (vo.getIssuanceNumbers() != null ? vo
							.getIssuanceNumbers().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setIssuanceNumbers(purchaseNumber.toString());
				}

				if (vo.getIssuanceDates() != null) {
					strArray = (vo.getIssuanceDates() != null ? vo
							.getIssuanceDates().split(",") : null);
					purchaseNoSet = new HashSet<String>();
					for (String string : strArray) {
						purchaseNoSet.add(string.trim());
					}
					purchaseNumber = new StringBuilder();
					for (String purchaseNo : purchaseNoSet)
						purchaseNumber.append(purchaseNo).append(",");

					vo.setIssuanceDates(purchaseNumber.toString());
				}
			}
			ServletActionContext.getRequest().setAttribute("printedOn",
					DateFormat.convertSystemDateToString(new Date()));
			ServletActionContext.getRequest().setAttribute("PRODUCT_LIST", vos);
			ServletActionContext.getRequest().setAttribute("PRODUCT_MASTER",
					finalSectionMap);
			ServletActionContext.getRequest().setAttribute("datefrom",
					ServletActionContext.getRequest().getParameter("fromDate"));
			ServletActionContext.getRequest().setAttribute("dateto",
					ServletActionContext.getRequest().getParameter("toDate"));
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: receiveVsIssuanceReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public List<IssueRequistionVO> receiveVsIssuanceReportIssuancePart()
			throws Exception {

		Long departmentId = null;
		Long personId = null;
		Long sectionId = null;
		Date fromDatee = null;
		Date toDatee = null;
		String products = null;
		getImplementId();

		if (ServletActionContext.getRequest().getParameter("departmentId") != null
				&& !ServletActionContext.getRequest()
						.getParameter("departmentId").equals("0")) {
			departmentId = Long.valueOf(ServletActionContext.getRequest()
					.getParameter("departmentId"));
		}
		if (ServletActionContext.getRequest().getParameter("sectionId") != null
				&& !ServletActionContext.getRequest().getParameter("sectionId")
						.equals("0")) {
			sectionId = Long.valueOf(ServletActionContext.getRequest()
					.getParameter("sectionId"));
		}
		if (ServletActionContext.getRequest().getParameter("personId") != null
				&& !ServletActionContext.getRequest().getParameter("personId")
						.equals("0")) {
			personId = Long.valueOf(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		Set<Long> productIds = new HashSet<Long>();

		if (ServletActionContext.getRequest().getParameter("products") != null) {
			products = ServletActionContext.getRequest().getParameter(
					"products");

			for (String str : products.split(",")) {
				if (str != null && !str.equals(""))
					productIds.add(Long.valueOf(str));
			}
		}

		if (fromDate != null && !fromDate.equals("")
				&& !fromDate.equals("null")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("") && !toDate.equals("null")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<IssueRequistion> issueRequisitionList = accountsEnterpriseService
				.getIssueRequistionService().getFilteredIssueRequistions(
						implementation, fromDatee, toDatee, productIds,
						personId, departmentId, sectionId);

		List<IssueRequistionVO> issueRequistionVOs = new ArrayList<IssueRequistionVO>();
		List<IssueRequistionDetailVO> issueRequistionDetailVOsAll = new ArrayList<IssueRequistionDetailVO>();
		for (IssueRequistion requisition : issueRequisitionList) {
			IssueRequistionVO requisitionVO = new IssueRequistionVO(requisition);
			requisitionVO
					.setStrIssueDate(DateFormat.convertDateToString(requisition
							.getIssueDate().toString()));
			requisitionVO.setShippingSource(null != requisition
					.getLookupDetail() ? requisition.getLookupDetail()
					.getDisplayName() : "");
			requisitionVO.setDepartmentName(requisition.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			requisitionVO.setLocationName(requisition.getCmpDeptLocation()
					.getDepartment().getDepartmentName());
			requisitionVO.setRequisitionPersonName(requisition.getPerson()
					.getFirstName().concat(" ")
					.concat(requisition.getPerson().getLastName()));

			List<IssueRequistionDetailVO> issueRequistionDetailVOs = new ArrayList<IssueRequistionDetailVO>();

			for (IssueRequistionDetail detail : requisition
					.getIssueRequistionDetails()) {

				IssueRequistionDetailVO issueRequistionDetailVO = new IssueRequistionDetailVO();
				BeanUtils.copyProperties(issueRequistionDetailVO, detail);
				issueRequistionDetailVO.setProductCode(detail.getProduct()
						.getCode());
				issueRequistionDetailVO.setProductName(detail.getProduct()
						.getProductName());
				issueRequistionDetailVO.setStoreName(detail.getShelf()
						.getShelf().getAisle().getStore().getStoreName()
						+ ">>"
						+ detail.getShelf().getShelf().getAisle()
								.getSectionName()
						+ ">>"
						+ detail.getShelf().getShelf().getName()
						+ ">>"
						+ detail.getShelf().getName());
				issueRequistionDetailVO.setTotalRate(detail.getQuantity()
						* detail.getUnitRate());
				issueRequistionDetailVO.setIssuedQty(detail.getQuantity());
				if (detail.getIssueReturnDetails() != null) {
					for (IssueReturnDetail returnDetail : detail
							.getIssueReturnDetails()) {
						Double total = issueRequistionDetailVO.getTotalRate();
						Double totalQty = issueRequistionDetailVO
								.getIssuedQty();
						total -= returnDetail.getReturnQuantity()
								* detail.getUnitRate();
						totalQty -= returnDetail.getReturnQuantity();
						issueRequistionDetailVO.setTotalRate(total);
						issueRequistionDetailVO.setIssuedQty(totalQty);
					}
				}
				issueRequistionDetailVO.setIssueRequistionVO(requisitionVO);
				issueRequistionDetailVOs.add(issueRequistionDetailVO);

			}
			issueRequistionDetailVOsAll.addAll(issueRequistionDetailVOs);
			requisitionVO.setIssueRequistionDetailVO(issueRequistionDetailVOs);

			issueRequistionVOs.add(requisitionVO);
		}
		ServletActionContext.getRequest().setAttribute("ISSUE_DETAIL",
				issueRequistionDetailVOsAll);
		return issueRequistionVOs;
	}

	public List<ReceiveVO> receiveVsIssuanceReportReveivePart() {
		List<ReceiveVO> receiveVOsAll = new ArrayList<ReceiveVO>();
		getImplementId();
		try {

			String products = null;
			String fromDatee = null;
			String toDatee = null;
			Long departmentId = null;
			Long personId = null;
			Long sectionId = null;
			Set<Long> productIds = new HashSet<Long>();

			if (ServletActionContext.getRequest().getParameter("products") != null) {
				products = ServletActionContext.getRequest().getParameter(
						"products");

				for (String str : products.split(",")) {
					if (str != null && !str.equals(""))
						productIds.add(Long.valueOf(str));
				}
			}

			if (ServletActionContext.getRequest().getParameter("fromDate") != null) {
				fromDatee = ServletActionContext.getRequest().getParameter(
						"fromDate");
			}

			if (ServletActionContext.getRequest().getParameter("toDate") != null) {
				toDatee = ServletActionContext.getRequest().getParameter(
						"toDate");
			}

			if (ServletActionContext.getRequest().getParameter("departmentId") != null
					&& !ServletActionContext.getRequest()
							.getParameter("departmentId").equals("0")) {
				departmentId = Long.valueOf(ServletActionContext.getRequest()
						.getParameter("departmentId"));
			}
			if (ServletActionContext.getRequest().getParameter("sectionId") != null
					&& !ServletActionContext.getRequest()
							.getParameter("sectionId").equals("0")) {
				sectionId = Long.valueOf(ServletActionContext.getRequest()
						.getParameter("sectionId"));
			}
			if (ServletActionContext.getRequest().getParameter("personId") != null
					&& !ServletActionContext.getRequest()
							.getParameter("personId").equals("0")) {
				personId = Long.valueOf(ServletActionContext.getRequest()
						.getParameter("personId"));
			}

			Date fromDate = null;
			Date toDate = null;
			if (fromDatee != null && !fromDatee.equals("")) {
				fromDate = DateFormat.convertStringToDate(fromDatee);
			}

			if (toDatee != null && !toDatee.equals("")) {
				toDate = DateFormat.convertStringToDate(toDatee);
			}
			ServletActionContext.getRequest().setAttribute("fromDate",
					fromDatee);
			ServletActionContext.getRequest().setAttribute("toDate", toDatee);

			receiveNotes = accountsEnterpriseService.getReceiveService()
					.getFilteredReceiveNotes(implementation, toDate, fromDate,
							productIds, personId, departmentId);
			ReceiveVO receiveVO = null;
			List<ReceiveDetailVO> receiveDetailVOs = null;
			ReceiveDetailVO receiveDetailVO = null;
			for (Receive list : receiveNotes) {
				receiveVO = new ReceiveVO();
				BeanUtils.copyProperties(receiveVO, list);
				receiveVO.setStrReceiveDate(DateFormat.convertDateToString(list
						.getReceiveDate().toString()));
				List<ReceiveDetail> receiveDetail = new ArrayList<ReceiveDetail>(
						list.getReceiveDetails());
				receiveDetailVOs = new ArrayList<ReceiveDetailVO>();
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson()))
					receiveVO.setSupplierName(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					receiveVO.setSupplierName(null != list.getSupplier()
							.getCmpDeptLocation() ? list.getSupplier()
							.getCmpDeptLocation().getCompany().getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				for (ReceiveDetail list2 : receiveDetail) {
					receiveDetailVO = new ReceiveDetailVO();
					BeanUtils.copyProperties(receiveDetailVO, list2);
					receiveDetailVO
							.setProductCode(list2.getProduct().getCode());
					receiveDetailVO.setProductName(list2.getProduct()
							.getProductName());
					receiveDetailVO.setItemType(ItemType.get(
							list2.getProduct().getItemType()).name());
					if (list2.getShelf() != null) {
						receiveDetailVO.setStoreName(list2.getShelf()
								.getAisle().getStore().getStoreName()
								+ ">>"
								+ list2.getShelf().getAisle().getSectionName()
								+ ">>"
								+ list2.getShelf().getShelf().getName()
								+ ">>" + list2.getShelf().getName());
					} else {
						receiveDetailVO.setStoreName("");
					}
					receiveDetailVO.setTotalRate(list2.getReceiveQty()
							* list2.getUnitRate());
					receiveDetailVO.setReceiveQty(list2.getReceiveQty());
					receiveDetailVO.setDisplayUnitRate(AIOSCommons
							.formatAmount(list2.getUnitRate()));
					receiveDetailVO.setDisplayTotalAmount(AIOSCommons
							.formatAmount(list2.getUnitRate()
									* list2.getReceiveQty()));
					receiveDetailVO.setDisplayQuantity(AIOSCommons
							.formatAmount(list2.getReceiveQty()));
					receiveDetailVO.setTotalRate(list2.getUnitRate()
							* list2.getReceiveQty());
					if (list2.getGoodsReturnDetails() != null) {
						for (GoodsReturnDetail returnDetail : list2
								.getGoodsReturnDetails()) {
							if (!returnDetail.getGoodsReturn().getIsDebit())
								continue;

							Double total = receiveDetailVO.getTotalRate();
							Double totalQty = receiveDetailVO.getReceiveQty();
							total -= returnDetail.getReturnQty()
									* list2.getUnitRate();
							totalQty -= returnDetail.getReturnQty();
							receiveDetailVO.setTotalRate(total);
							receiveDetailVO.setReceiveQty(totalQty);
						}
					}

					receiveDetailVO.setReceiveVO(receiveVO);
					receiveDetailVOs.add(receiveDetailVO);
				}
				receiveVO.setReceiveDetailsVO(receiveDetailVOs);
				receiveVOsAll.add(receiveVO);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return receiveVOsAll;
	}

	public String showGoodsReceiveJsonList() {
		try {
			List<ReceiveDetailVO> receiveDetailVOs = this
					.fetchReceiveDetailReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", receiveDetailVOs.size());
			jsonResponse.put("iTotalDisplayRecords", receiveDetailVOs.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (ReceiveDetailVO list : receiveDetailVOs) {
				array = new JSONArray();
				array.add(list.getReceiveDetailId());
				array.add(list.getReceiveVO().getReceiveNumber());
				array.add(list.getReceiveVO().getStrReceiveDate());
				array.add(list.getReceiveVO().getSupplierName());
				array.add(list.getReceiveVO().getDeliveryNo() + "");
				array.add(list.getReceiveVO().getReferenceNo() + "");
				array.add(list.getStoreName());
				array.add(list.getProductName() + "--" + list.getProductCode());
				array.add(list.getDisplayQuantity());
				array.add(list.getDisplayUnitRate());
				array.add(list.getDisplayTotalAmount());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showGoodsReceiveJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String showGoodsReceiveReport() {
		try {
			List<ReceiveDetailVO> receiveDetailVOs = this
					.fetchReceiveDetailReportData();
			ReceiveVO receiveVO = null;
			if (null != receiveDetailVOs && receiveDetailVOs.size() > 0) {
				receiveVO = new ReceiveVO();
				double totalQuantity = 0;
				double totalUnitRate = 0;
				double totalAmount = 0;
				for (ReceiveDetailVO receiveDetailVO : receiveDetailVOs) {
					totalQuantity += receiveDetailVO.getReceiveQty();
					totalUnitRate += receiveDetailVO.getUnitRate();
					totalAmount += receiveDetailVO.getTotalRate();
				}
				receiveVO.setDisplayQuantity(AIOSCommons
						.formatAmount(totalQuantity));
				receiveVO.setDisplayUnitRate(AIOSCommons
						.formatAmount(totalUnitRate));
				receiveVO.setDisplayTotalAmount(AIOSCommons
						.formatAmount(totalAmount));
			}
			ServletActionContext.getRequest().setAttribute("RECEIVE_DETAIL",
					receiveDetailVOs);
			ServletActionContext.getRequest().setAttribute("RECEIVE_INFO",
					receiveVO);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showGoodsReceiveReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public List<ReceiveDetailVO> fetchReceiveDetailReportData() {
		List<ReceiveDetailVO> receiveDetailVOsAll = new ArrayList<ReceiveDetailVO>();
		getImplementId();
		try {
			Long receiveId = 0L;
			Long storeId = 0L;
			Long productId = 0L;
			Long supplierId = 0L;
			String fromDatee = null;
			String toDatee = null;

			if (ServletActionContext.getRequest().getParameter("storeId") != null) {
				storeId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("storeId"));
			}

			if (ServletActionContext.getRequest().getParameter("productId") != null) {
				productId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("productId"));
			}

			if (ServletActionContext.getRequest().getParameter("supplierId") != null) {
				supplierId = Long.parseLong(ServletActionContext.getRequest()
						.getParameter("supplierId"));
			}

			if (ServletActionContext.getRequest().getParameter("datefrom") != null) {
				fromDatee = ServletActionContext.getRequest().getParameter(
						"datefrom");
			}

			if (ServletActionContext.getRequest().getParameter("dateto") != null) {
				toDatee = ServletActionContext.getRequest().getParameter(
						"dateto");
			}
			ServletActionContext.getRequest().setAttribute("datefrom",
					ServletActionContext.getRequest().getParameter("datefrom"));
			ServletActionContext.getRequest().setAttribute(
					"dateto",
					ServletActionContext.getRequest().getParameter("dateto")
							.toString());
			Date fromDate = null;
			Date toDate = null;
			if (fromDatee != null && !fromDatee.equals("")) {
				fromDate = DateFormat.convertStringToDate(fromDatee);
			}

			if (toDatee != null && !toDatee.equals("")) {
				toDate = DateFormat.convertStringToDate(toDatee);
			}

			receiveNotes = accountsEnterpriseService.getReceiveService()
					.getFilteredReceiveNotes(implementation, receiveId, toDate,
							fromDate, supplierId, storeId, productId);
			ReceiveVO receiveVO = null;
			List<ReceiveDetailVO> receiveDetailVOs = null;
			ReceiveDetailVO receiveDetailVO = null;
			for (Receive list : receiveNotes) {
				receiveVO = new ReceiveVO();
				BeanUtils.copyProperties(receiveVO, list);
				receiveVO.setStrReceiveDate(DateFormat.convertDateToString(list
						.getReceiveDate().toString()));
				List<ReceiveDetail> receiveDetail = new ArrayList<ReceiveDetail>(
						list.getReceiveDetails());
				receiveDetailVOs = new ArrayList<ReceiveDetailVO>();
				receiveVO.setReceiveDetailsVO(receiveDetailVOs);
				if (null != list.getSupplier().getPerson()
						&& !("").equals(list.getSupplier().getPerson()))
					receiveVO.setSupplierName(list
							.getSupplier()
							.getPerson()
							.getFirstName()
							.concat(" ")
							.concat(list.getSupplier().getPerson()
									.getLastName()));
				else
					receiveVO.setSupplierName(null != list.getSupplier()
							.getCmpDeptLocation() ? list.getSupplier()
							.getCmpDeptLocation().getCompany().getCompanyName()
							: list.getSupplier().getCompany().getCompanyName());
				for (ReceiveDetail list2 : receiveDetail) {
					receiveDetailVO = new ReceiveDetailVO();
					BeanUtils.copyProperties(receiveDetailVO, list2);
					receiveDetailVO
							.setProductCode(list2.getProduct().getCode());
					receiveDetailVO.setProductName(list2.getProduct()
							.getProductName());
					receiveDetailVO.setItemType(ItemType.get(
							list2.getProduct().getItemType()).name());
					if (list2.getShelf() != null) {
						receiveDetailVO.setStoreName(list2.getShelf()
								.getAisle().getStore().getStoreName()
								+ ">>"
								+ list2.getShelf().getAisle().getSectionName()
								+ ">>"
								+ list2.getShelf().getShelf().getName()
								+ ">>" + list2.getShelf().getName());
					} else {
						receiveDetailVO.setStoreName("");
					}

					if (list2.getPurchase().getRequisition() != null
							&& list2.getPurchase().getRequisition()
									.getCmpDeptLoc() != null
							&& list2.getPurchase().getRequisition()
									.getCmpDeptLoc().getDepartment() != null) {
						receiveDetailVO.setSection(list2.getPurchase()
								.getRequisition().getCmpDeptLoc()
								.getDepartment().getDepartmentName());

					}

					if (receiveDetailVO == null || receiveDetailVO.equals("")) {
						receiveDetailVO.setSection(receiveDetailVO
								.getStoreName());
					}
					receiveDetailVO.setTotalRate(list2.getReceiveQty()
							* list2.getUnitRate());
					receiveDetailVO.setDisplayUnitRate(AIOSCommons
							.formatAmount(list2.getUnitRate()));
					receiveDetailVO.setDisplayTotalAmount(AIOSCommons
							.formatAmount(list2.getUnitRate()
									* list2.getReceiveQty()));
					receiveDetailVO.setDisplayQuantity(AIOSCommons
							.formatAmount(list2.getReceiveQty()));
					receiveDetailVO.setTotalRate(list2.getUnitRate()
							* list2.getReceiveQty());
					receiveDetailVO.setReceiveVO(receiveVO);
					receiveDetailVOs.add(receiveDetailVO);
				}
				receiveDetailVOsAll.addAll(receiveDetailVOs);
			}

			if (null != receiveDetailVOsAll && receiveDetailVOsAll.size() > 0) {
				Collections.sort(receiveDetailVOsAll,
						new Comparator<ReceiveDetailVO>() {
							public int compare(ReceiveDetailVO o1,
									ReceiveDetailVO o2) {
								return o1
										.getReceiveVO()
										.getReceiveNumber()
										.compareTo(
												o2.getReceiveVO()
														.getReceiveNumber());
							}
						});
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return receiveDetailVOsAll;
	}

	public String itemRequisitionReportCriteria() {
		try {
			LOGGER.info("Inside Module: Accounts : Method: itemRequisitionReportCriteria ");
			getImplementId();
			List<Person> persons = accountsBL.getPersonBL().getPersonList("");
			List<Product> products = accountsBL.getPointOfSaleBL()
					.getProductPricingBL().getProductBL().getProductService()
					.getOnlyProducts(implementation);
			List<Location> locations = accountsBL.getLocationBL()
					.getLocationService().getAllLocation(implementation);
			ServletActionContext.getRequest().setAttribute("PERSON_INFO",
					persons);
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute("LOCATION_INFO",
					locations);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: itemRequisitionReportCriteria Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String loadPurchaseOrderReportCriteria() {
		try {
			getImplementId();
			suppliersList = new ArrayList<SupplierTO>();
			suppliersList = accountsBL.getPurchaseBL().convertListTO(
					accountsBL.getPurchaseBL().getSupplierService()
							.getAllSuppliers(getImplementation()));
			List<Product> products = accountsBL.getPurchaseBL().getProductBL()
					.getProductService().getOnlyProducts(getImplementation());

			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					products);
			ServletActionContext.getRequest().setAttribute(
					"DEPARTMENT_LIST",
					accountsBL.getDepartmentBL().getDepartmentService()
							.getAllDepartments(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showRequisitionJsonList() {
		try {
			List<RequisitionDetailVO> requisitionDetailVOs = this
					.fetchRequisitionReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", requisitionDetailVOs.size());
			jsonResponse.put("iTotalDisplayRecords",
					requisitionDetailVOs.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (RequisitionDetailVO list : requisitionDetailVOs) {
				array = new JSONArray();
				array.add(list.getRequisitionDetailId());
				array.add(list.getRequisitionVO().getReferenceNumber());
				array.add(list.getRequisitionVO().getDate());
				array.add(list.getRequisitionVO().getPersonName());
				array.add(list.getRequisitionVO().getLocationName());
				array.add(list.getProduct().getProductName() + "--"
						+ list.getProduct().getCode());
				array.add(list.getQuantity());
				array.add(list.getRequisitionVO().getStatusStr());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showRequisitionJsonList Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	public String getIssuereceivedChequePrintout() {
		try {
			ChequeTransactionVO chequeTransactionVO = this
					.fetchIssuereceivedChequeReportData();
			ServletActionContext.getRequest().setAttribute(
					"CHEQUE_TRANSACTION", chequeTransactionVO);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: getIssuereceivedChequePrintout Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getIssuereceivedChequeXls() {
		try {
			ChequeTransactionVO chequeTransactionVO = this
					.fetchIssuereceivedChequeReportData();
			getImplementId();
			StringBuilder str = new StringBuilder();
			str.append(",, " + implementation.getCompanyName() + " \n");
			str.append(",, PRESENTED CHEQUES \n");
			if (null != chequeTransactionVO
					&& null != chequeTransactionVO.getChequeTransactionVOs()
					&& chequeTransactionVO.getChequeTransactionVOs().size() > 0) {
				str.append("REFERENCE NO, TRANSACTION DATE, TRANSACTION TYPE, PAYABLES,");
				str.append("RECEIVABLES, BANK, ACCOUNT NUMBER, CHEQUE DATE, CHEQUE NO, AMOUNT \n");
				for (ChequeTransactionVO transactionVO : chequeTransactionVO
						.getChequeTransactionVOs()) {
					str.append(transactionVO.getReferenceNumber()).append(",");
					str.append(transactionVO.getTransactionDate()).append(",");
					str.append(transactionVO.getTransactionType()).append(",");
					str.append(null != transactionVO.getPayableName()
							&& !("").equals(transactionVO.getPayableName()) ? transactionVO
							.getPayableName() + ","
							: "Nil ,");
					str.append(null != transactionVO.getReceivableName()
							&& !("").equals(transactionVO.getReceivableName()) ? transactionVO
							.getReceivableName() + ","
							: "Nil ,");
					str.append(transactionVO.getBankName()).append(",");
					str.append(transactionVO.getAccountNumber()).append(",");
					str.append(transactionVO.getChequeDate()).append(",");
					str.append(transactionVO.getChequeNumber()).append(",");
					str.append(null != transactionVO.getChequePayment() ? AIOSCommons
							.formatAmountToDouble(transactionVO
									.getChequePayment()) : AIOSCommons
							.formatAmountToDouble(transactionVO
									.getChequeReceive()));
					str.append("\n");
				}

				str.append("\n\n");
				str.append("TOTAL PAYBLES, TOTAL RECEIVABLE \n");
				str.append(null != chequeTransactionVO.getPaymentTotal() ? AIOSCommons
						.formatAmountToDouble(chequeTransactionVO
								.getPaymentTotal()) : "Nil");
				str.append(",");
				str.append(null != chequeTransactionVO.getReceiveTotal() ? AIOSCommons
						.formatAmountToDouble(chequeTransactionVO
								.getReceiveTotal()) : "Nil");
			} else
				str.append("No Record Found.");
			InputStream is = new ByteArrayInputStream(str.toString().getBytes());
			fileInputStream = is;
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: getIssuereceivedChequeXls Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	private ChequeTransactionVO fetchIssuereceivedChequeReportData()
			throws Exception {
		getImplementId();
		String fromdate = null;
		String todate = null;
		if (ServletActionContext.getRequest().getParameter("fromDate") != null
				&& !("").equals(ServletActionContext.getRequest().getParameter(
						"fromDate")))
			fromdate = ServletActionContext.getRequest().getParameter(
					"fromDate");
		if (ServletActionContext.getRequest().getParameter("toDate") != null
				&& !("").equals(ServletActionContext.getRequest().getParameter(
						"toDate")))
			todate = ServletActionContext.getRequest().getParameter("toDate");
		boolean issuedChecked = Boolean.parseBoolean(ServletActionContext
				.getRequest().getParameter("issuedChecked"));
		boolean receivedChecked = Boolean.parseBoolean(ServletActionContext
				.getRequest().getParameter("receivedChecked"));
		ChequeTransactionVO chequeTransactionVO = null;
		List<ChequeTransactionVO> chequeTransactionVOs = new ArrayList<ChequeTransactionVO>();
		if (issuedChecked) {
			List<DirectPayment> directPayments = accountsBL.getAccountsJobBL()
					.getDirectPaymentBL().getDirectPaymentService()
					.getChequeDirectPayments(implementation, fromdate, todate);
			if (null != directPayments && directPayments.size() > 0) {
				Collections.sort(directPayments,
						new Comparator<DirectPayment>() {
							public int compare(DirectPayment o1,
									DirectPayment o2) {
								return o1.getChequeDate().compareTo(
										o2.getChequeDate());
							}
						});
				for (DirectPayment directPayment : directPayments)
					chequeTransactionVOs
							.add(addDirectPaymentChequeTransaction(directPayment));
			}
		}
		if (receivedChecked) {
			List<BankDepositDetail> bankDepositDetails = accountsBL
					.getAccountsEnterpriseService().getBankDepositService()
					.getChequeBankDeposits(implementation, fromdate, todate);

			if (null != bankDepositDetails && bankDepositDetails.size() > 0) {
				Collections.sort(bankDepositDetails,
						new Comparator<BankDepositDetail>() {
							public int compare(BankDepositDetail o1,
									BankDepositDetail o2) {
								return o1
										.getBankReceiptsDetail()
										.getChequeDate()
										.compareTo(
												o2.getBankReceiptsDetail()
														.getChequeDate());
							}
						});
				for (BankDepositDetail bankDepositDetail : bankDepositDetails)
					chequeTransactionVOs
							.add(addBankReceiptChequeTransaction(bankDepositDetail));
			}
		}
		if (null != chequeTransactionVOs && chequeTransactionVOs.size() > 0) {
			chequeTransactionVO = new ChequeTransactionVO();
			double paymentTotal = 0;
			double receiptTotal = 0;
			for (ChequeTransactionVO transactionVO : chequeTransactionVOs) {
				if (transactionVO.getPaymentCheque())
					paymentTotal += AIOSCommons
							.formatAmountToDouble(transactionVO
									.getChequePayment());
				else
					receiptTotal += AIOSCommons
							.formatAmountToDouble(transactionVO
									.getChequeReceive());
			}
			chequeTransactionVO.setPaymentTotal(paymentTotal > 0 ? AIOSCommons
					.formatAmount(paymentTotal) : "");
			chequeTransactionVO.setReceiveTotal(receiptTotal > 0 ? AIOSCommons
					.formatAmount(receiptTotal) : "");
			chequeTransactionVO.setChequeTransactionVOs(chequeTransactionVOs);
		}
		return chequeTransactionVO;
	}

	private ChequeTransactionVO addBankReceiptChequeTransaction(
			BankDepositDetail bankDepositDetail) throws Exception {
		ChequeTransactionVO chequeTransactionVO = new ChequeTransactionVO();
		chequeTransactionVO.setReferenceNumber(bankDepositDetail
				.getBankDeposit().getBankDepositNumber());
		chequeTransactionVO.setTransactionDate(DateFormat
				.convertDateToString(bankDepositDetail.getBankDeposit()
						.getDate().toString()));
		chequeTransactionVO.setChequeDate(DateFormat
				.convertDateToString(bankDepositDetail.getBankReceiptsDetail()
						.getChequeDate().toString()));
		chequeTransactionVO.setChequeNumber(String.valueOf(bankDepositDetail
				.getBankReceiptsDetail().getBankRefNo()));
		chequeTransactionVO.setPaymentCheque(false);
		chequeTransactionVO.setTransactionType(BankDeposit.class
				.getSimpleName());
		chequeTransactionVO.setChequeReceive(AIOSCommons
				.formatAmount(bankDepositDetail.getAmount()));
		chequeTransactionVO.setBankName(bankDepositDetail.getBankDeposit()
				.getBankAccount().getBank().getBankName());
		chequeTransactionVO.setAccountNumber(bankDepositDetail.getBankDeposit()
				.getBankAccount().getAccountNumber());
		if (null != bankDepositDetail.getBankReceiptsDetail().getBankReceipts()
				.getSupplier()) {
			if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getSupplier().getPerson())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail()
						.getBankReceipts()
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(bankDepositDetail.getBankReceiptsDetail()
								.getBankReceipts().getSupplier().getPerson()
								.getLastName()));
			else if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getSupplier().getCompany())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail().getBankReceipts()
						.getSupplier().getCompany().getCompanyName());
			else if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getSupplier().getCmpDeptLocation())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail().getBankReceipts()
						.getSupplier().getCmpDeptLocation().getCompany()
						.getCompanyName());
		} else if (null != bankDepositDetail.getBankReceiptsDetail()
				.getBankReceipts().getCustomer()) {
			if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getCustomer().getPersonByPersonId())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail()
						.getBankReceipts()
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(bankDepositDetail.getBankReceiptsDetail()
								.getBankReceipts().getCustomer()
								.getPersonByPersonId().getLastName()));
			else if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getCustomer().getCompany())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail().getBankReceipts()
						.getCustomer().getCompany().getCompanyName());
			else if (null != bankDepositDetail.getBankReceiptsDetail()
					.getBankReceipts().getCustomer().getCmpDeptLocation())
				chequeTransactionVO.setReceivableName(bankDepositDetail
						.getBankReceiptsDetail().getBankReceipts()
						.getCustomer().getCmpDeptLocation().getCompany()
						.getCompanyName());
		} else if (null != bankDepositDetail.getBankReceiptsDetail()
				.getBankReceipts().getPersonByPersonId())
			chequeTransactionVO.setReceivableName(bankDepositDetail
					.getBankReceiptsDetail()
					.getBankReceipts()
					.getPersonByPersonId()
					.getFirstName()
					.concat(" ")
					.concat(bankDepositDetail.getBankReceiptsDetail()
							.getBankReceipts().getPersonByPersonId()
							.getLastName()));
		else
			chequeTransactionVO.setReceivableName(bankDepositDetail
					.getBankReceiptsDetail().getBankReceipts().getOthers());
		if (bankDepositDetail.getBankReceiptsDetail().getChequeDate()
				.after(java.util.Calendar.getInstance().getTime()))
			chequeTransactionVO.setPassedDateCheque("YES");
		else
			chequeTransactionVO.setPassedDateCheque("NO");
		return chequeTransactionVO;
	}

	private ChequeTransactionVO addDirectPaymentChequeTransaction(
			DirectPayment directPayment) throws Exception {
		double chequePayment = 0;
		ChequeTransactionVO chequeTransactionVO = new ChequeTransactionVO();
		chequeTransactionVO
				.setReferenceNumber(directPayment.getPaymentNumber());
		chequeTransactionVO
				.setTransactionDate(DateFormat
						.convertDateToString(directPayment.getPaymentDate()
								.toString()));
		chequeTransactionVO.setChequeDate(DateFormat
				.convertDateToString(directPayment.getChequeDate().toString()));
		chequeTransactionVO.setChequeNumber(String.valueOf(directPayment
				.getChequeNumber()));
		chequeTransactionVO.setPaymentCheque(true);
		chequeTransactionVO.setTransactionType(DirectPayment.class
				.getSimpleName());
		for (DirectPaymentDetail dpDt : directPayment.getDirectPaymentDetails())
			chequePayment += dpDt.getAmount();
		chequeTransactionVO.setChequePayment(AIOSCommons
				.formatAmount(chequePayment));
		chequeTransactionVO.setBankName(directPayment.getBankAccount()
				.getBank().getBankName());
		chequeTransactionVO.setAccountNumber(directPayment.getBankAccount()
				.getAccountNumber());
		if (null != directPayment.getSupplier()) {
			if (null != directPayment.getSupplier().getPerson())
				chequeTransactionVO.setPayableName(directPayment
						.getSupplier()
						.getPerson()
						.getFirstName()
						.concat(" ")
						.concat(directPayment.getSupplier().getPerson()
								.getLastName()));
			else if (null != directPayment.getSupplier().getCompany())
				chequeTransactionVO.setPayableName(directPayment.getSupplier()
						.getCompany().getCompanyName());
			else if (null != directPayment.getSupplier().getCmpDeptLocation())
				chequeTransactionVO.setPayableName(directPayment.getSupplier()
						.getCmpDeptLocation().getCompany().getCompanyName());
		} else if (null != directPayment.getCustomer()) {
			if (null != directPayment.getCustomer().getPersonByPersonId())
				chequeTransactionVO.setPayableName(directPayment
						.getCustomer()
						.getPersonByPersonId()
						.getFirstName()
						.concat(" ")
						.concat(directPayment.getCustomer()
								.getPersonByPersonId().getLastName()));
			else if (null != directPayment.getCustomer().getCompany())
				chequeTransactionVO.setPayableName(directPayment.getCustomer()
						.getCompany().getCompanyName());
			else if (null != directPayment.getCustomer().getCmpDeptLocation())
				chequeTransactionVO.setPayableName(directPayment.getCustomer()
						.getCmpDeptLocation().getCompany().getCompanyName());
		} else if (null != directPayment.getPersonByPersonId())
			chequeTransactionVO.setPayableName(directPayment
					.getPersonByPersonId().getFirstName().concat(" ")
					.concat(directPayment.getPersonByPersonId().getLastName()));
		else
			chequeTransactionVO.setPayableName(directPayment.getOthers());
		if (directPayment.getChequeDate().after(
				java.util.Calendar.getInstance().getTime()))
			chequeTransactionVO.setPassedDateCheque("YES");
		else
			chequeTransactionVO.setPassedDateCheque("NO");
		return chequeTransactionVO;
	}

	public String itemRequisitionReport() {
		try {
			List<RequisitionDetailVO> requisitionDetailVOs = this
					.fetchRequisitionReportData();
			ServletActionContext.getRequest().setAttribute(
					"REQUISITION_DETAIL", requisitionDetailVOs);
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: itemRequisitionReport Exception "
					+ ex);
			ex.printStackTrace();
			return null;
		}
	}

	private List<RequisitionDetailVO> fetchRequisitionReportData()
			throws Exception {
		getImplementId();
		RequisitionVO requisitionVO = null;
		String fromdate = null;
		String todate = null;
		Long personId = 0l;
		Long productId = 0l;
		Long locationId = 0l;
		if (ServletActionContext.getRequest().getParameter("datefrom") != null) {
			fromdate = ServletActionContext.getRequest().getParameter(
					"datefrom");
		}
		if (ServletActionContext.getRequest().getParameter("dateto") != null) {
			todate = ServletActionContext.getRequest().getParameter("dateto");
		}
		if (ServletActionContext.getRequest().getParameter("personId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("personId")) > 0) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}
		if (ServletActionContext.getRequest().getParameter("locationId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("locationId")) > 0) {
			locationId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("locationId"));
		}
		if (ServletActionContext.getRequest().getParameter("productId") != null
				&& Long.parseLong(ServletActionContext.getRequest()
						.getParameter("productId")) > 0) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}
		ServletActionContext.getRequest().setAttribute("datefrom",
				ServletActionContext.getRequest().getParameter("datefrom"));
		ServletActionContext.getRequest().setAttribute(
				"dateto",
				ServletActionContext.getRequest().getParameter("dateto")
						.toString());
		List<Requisition> requisitions = accountsBL
				.getInvoiceBL()
				.getReceiveBL()
				.getPurchaseBL()
				.getQuotationBL()
				.getRequisitionService()
				.getFilterRequisitionData(fromdate, todate, personId,
						locationId, productId, implementation);
		List<RequisitionDetailVO> requisitionDetailVOsAll = new ArrayList<RequisitionDetailVO>();
		if (null != requisitions && requisitions.size() > 0) {
			List<RequisitionDetailVO> requisitionDetailVOs = null;
			RequisitionDetailVO requisitionDetailVO = null;
			for (Requisition requisition : requisitions) {
				requisitionVO = new RequisitionVO();
				BeanUtils.copyProperties(requisitionVO, requisition);
				requisitionVO.setDate(DateFormat
						.convertDateToString(requisition.getRequisitionDate()
								.toString()));
				requisitionVO.setStatusStr(RequisitionStatus.get(
						requisition.getStatus()).name());
				requisitionVO.setPersonName(requisition.getPerson()
						.getFirstName()
						+ " "
						+ requisition.getPerson().getLastName());
				requisitionVO.setLocationName(requisition.getCmpDeptLoc()
						.getCompany().getCompanyName()
						+ ">>"
						+ requisition.getCmpDeptLoc().getDepartment()
								.getDepartmentName()
						+ ">>"
						+ requisition.getCmpDeptLoc().getLocation()
								.getLocationName());
				requisitionDetailVOs = new ArrayList<RequisitionDetailVO>();
				for (RequisitionDetail requisitiondetail : requisition
						.getRequisitionDetails()) {
					requisitionDetailVO = new RequisitionDetailVO();
					BeanUtils.copyProperties(requisitionDetailVO,
							requisitiondetail);
					requisitionDetailVO.setRequisitionVO(requisitionVO);
					requisitionDetailVOs.add(requisitionDetailVO);
				}
				requisitionVO.setRequisitionDetailVOs(requisitionDetailVOs);
				requisitionDetailVOsAll.addAll(requisitionDetailVOs);
			}
			Collections.sort(requisitionDetailVOsAll,
					new Comparator<RequisitionDetailVO>() {
						public int compare(RequisitionDetailVO o1,
								RequisitionDetailVO o2) {
							return o1
									.getRequisitionVO()
									.getLocationName()
									.compareTo(
											o2.getRequisitionVO()
													.getLocationName());
						}
					});
		}
		return requisitionDetailVOsAll;
	}

	public String getMaterialTransferAllPrintOut() {
		try {

			List<MaterialTransferVO> materialTransferList = this
					.fetchMaterialTransferReportData();
			List<MaterialTransferDetailVO> materialTransferDetailVOs = new ArrayList<MaterialTransferDetailVO>();
			for (MaterialTransferVO materialTransferVO : materialTransferList) {
				for (MaterialTransferDetailVO vo : materialTransferVO
						.getMaterialTransferDetailVOs()) {
					vo.setMaterilaTransferVO(materialTransferVO);
					materialTransferDetailVOs.add(vo);
				}
			}

			ServletActionContext.getRequest().setAttribute(
					"MATERIAL_TRANSFERS", materialTransferDetailVOs);
			ServletActionContext.getRequest().setAttribute("printedOn",
					DateFormat.convertSystemDateToString(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getProductionCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PRODUCT_LIST",
					accountsEnterpriseService.getProductService()
							.getAllProduct(implementation));
			ServletActionContext.getRequest().setAttribute(
					"STORE_LIST",
					accountsEnterpriseService.getStoreService().getAllStores(
							implementation));
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_TYPE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"MATERIAL_TRANSFER_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"TRANSFER_REASON",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"MATERIAL_TRANSFER_REASON", true));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getProductionJsonList() {
		try {

			List<WorkinProcessVO> vos = this.fetchProductionReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", vos.size());
			jsonResponse.put("iTotalDisplayRecords", vos.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (WorkinProcessVO list : vos) {
				array = new JSONArray();
				array.add(list.getWorkinProcessId());
				array.add(list.getReferenceNumber());
				array.add(list.getDate());
				array.add(list.getPersonName() + "");
				array.add(list.getRequisitionReference() + "");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<WorkinProcessVO> fetchProductionReportData() throws Exception {

		getImplementId();

		Long personId = 0L;
		Long productId = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		getImplementId();

		if (ServletActionContext.getRequest().getParameter("productId") != null) {
			productId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("productId"));
		}

		if (ServletActionContext.getRequest().getParameter("personId") != null) {
			personId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("personId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<WorkinProcess> workinProcessList = accountsEnterpriseService
				.getWorkinProcessService()
				.getFilteredWorkingProcess(implementation, personId, productId,
						fromDatee, toDatee);

		List<WorkinProcessVO> workinProcessVOs = new ArrayList<WorkinProcessVO>();

		for (WorkinProcess transfer : workinProcessList) {
			WorkinProcessVO vo = new WorkinProcessVO();
			BeanUtils.copyProperties(vo, transfer);
			vo.setDate(DateFormat.convertDateToString(transfer.getProcessDate()
					.toString()));
			vo.setPersonName(transfer.getPerson().getFirstName() + " "
					+ transfer.getPerson().getLastName());
			if (transfer.getProductionRequisition() != null) {
				vo.setRequisitionReference(transfer.getProductionRequisition()
						.getReferenceNumber());
				vo.setPersonName(transfer.getProductionRequisition()
						.getPerson().getFirstName()
						+ " "
						+ transfer.getProductionRequisition().getPerson()
								.getLastName());
			} else {
				vo.setRequisitionReference("");
				vo.setPersonName("");
			}
			ArrayList<WorkinProcessProductionVO> workinProcessProductionVOs = new ArrayList<WorkinProcessProductionVO>();

			for (WorkinProcessProduction detail : transfer
					.getWorkinProcessProductions()) {

				WorkinProcessProductionVO workinProcessProductionVO = new WorkinProcessProductionVO();
				BeanUtils.copyProperties(workinProcessProductionVO, detail);
				workinProcessProductionVO.setProductName(detail.getProduct()
						.getProductName());
				workinProcessProductionVO.setStoreName(detail.getShelf()
						.getShelf().getAisle().getStore().getStoreName()
						+ " >> "
						+ detail.getShelf().getShelf().getAisle()
								.getSectionName()
						+ " >> "
						+ detail.getShelf().getShelf().getName()
						+ " >> "
						+ detail.getShelf().getName());

				workinProcessProductionVOs.add(workinProcessProductionVO);
			}
			vo.setWorkinProcessProductionVOs(workinProcessProductionVOs);

			workinProcessVOs.add(vo);
		}

		return workinProcessVOs;
	}

	public String getProductionAllPrintOut() {
		try {
			List<WorkinProcessVO> vos = this.fetchProductionReportData();
			List<WorkinProcessProductionVO> workinProcessProductionVOs = new ArrayList<WorkinProcessProductionVO>();
			for (WorkinProcessVO masterVO : vos) {
				for (WorkinProcessProductionVO vo : masterVO
						.getWorkinProcessProductionVOs()) {
					vo.setWorkinProcessVO(masterVO);
					workinProcessProductionVOs.add(vo);
				}
			}

			ServletActionContext.getRequest().setAttribute("PRODUCTION_LIST",
					workinProcessProductionVOs);
			ServletActionContext.getRequest().setAttribute("printedOn",
					DateFormat.convertSystemDateToString(new Date()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// =============Getters & setters=============
	public AccountsEnterpriseService getAccountsEnterpriseService() {
		return accountsEnterpriseService;
	}

	public void setAccountsEnterpriseService(
			AccountsEnterpriseService accountsEnterpriseService) {
		this.accountsEnterpriseService = accountsEnterpriseService;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public AccountsBL getAccountsBL() {
		return accountsBL;
	}

	public void setAccountsBL(AccountsBL accountsBL) {
		this.accountsBL = accountsBL;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public List<Ledger> getLedgerList() {
		return ledgerList;
	}

	public void setLedgerList(List<Ledger> ledgerList) {
		this.ledgerList = ledgerList;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public List<AccountsTO> getAccountsList() {
		return accountsList;
	}

	public void setAccountsList(List<AccountsTO> accountsList) {
		this.accountsList = accountsList;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public String getApprovalFlag() {
		return approvalFlag;
	}

	public void setApprovalFlag(String approvalFlag) {
		this.approvalFlag = approvalFlag;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public List<LoanTO> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanTO> loanList) {
		this.loanList = loanList;
	}

	public List<Payment> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<Payment> paymentList) {
		this.paymentList = paymentList;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getCodeCombinationId() {
		return codeCombinationId;
	}

	public void setCodeCombinationId(long codeCombinationId) {
		this.codeCombinationId = codeCombinationId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public long getJournalId() {
		return journalId;
	}

	public void setJournalId(long journalId) {
		this.journalId = journalId;
	}

	public String getAccountList() {
		return accountList;
	}

	public void setAccountList(String accountList) {
		this.accountList = accountList;
	}

	public List<BankDepositVO> getBankDepositList() {
		return bankDepositList;
	}

	public void setBankDepositList(List<BankDepositVO> bankDepositList) {
		this.bankDepositList = bankDepositList;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	public List<Calendar> getCalenderList() {
		return calenderList;
	}

	public void setCalenderList(List<Calendar> calenderList) {
		this.calenderList = calenderList;
	}

	public String getSelectedCalendarIds() {
		return selectedCalendarIds;
	}

	public void setSelectedCalendarIds(String selectedCalendarIds) {
		this.selectedCalendarIds = selectedCalendarIds;
	}

	public List<AccountsTO> getComulativeBalanceSheet() {
		return comulativeBalanceSheet;
	}

	public void setComulativeBalanceSheet(
			List<AccountsTO> comulativeBalanceSheet) {
		this.comulativeBalanceSheet = comulativeBalanceSheet;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public List<String> getItemTypes() {
		return itemTypes;
	}

	public void setItemTypes(List<String> itemTypes) {
		this.itemTypes = itemTypes;
	}

	public List<Product> getProductsList() {
		return productsList;
	}

	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<Period> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Period> periods) {
		this.periods = periods;
	}

	public String getSelectedPeriod() {
		return selectedPeriod;
	}

	public void setSelectedPeriod(String selectedPeriod) {
		this.selectedPeriod = selectedPeriod;
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public List<Object> getEntityObjects() {
		return entityObjects;
	}

	public void setEntityObjects(List<Object> entityObjects) {
		this.entityObjects = entityObjects;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getSelectedSupplier() {
		return selectedSupplier;
	}

	public void setSelectedSupplier(String selectedSupplier) {
		this.selectedSupplier = selectedSupplier;
	}

	public List<AccountsTO> getInventoryStatementDS() {
		return inventoryStatementDS;
	}

	public void setInventoryStatementDS(List<AccountsTO> inventoryStatementDS) {
		this.inventoryStatementDS = inventoryStatementDS;
	}

	public String getSelectedProductId() {
		return selectedProductId;
	}

	public void setSelectedProductId(String selectedProductId) {
		this.selectedProductId = selectedProductId;
	}

	public String getSelectedSupplierId() {
		return selectedSupplierId;
	}

	public void setSelectedSupplierId(String selectedSupplierId) {
		this.selectedSupplierId = selectedSupplierId;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public List<DirectPaymentVO> getDirectPaymentDS() {
		return directPaymentDS;
	}

	public void setDirectPaymentDS(List<DirectPaymentVO> directPaymentDS) {
		this.directPaymentDS = directPaymentDS;
	}

	public List<SupplierTO> getSuppliersList() {
		return suppliersList;
	}

	public void setSuppliersList(List<SupplierTO> suppliersList) {
		this.suppliersList = suppliersList;
	}

	public List<GoodsReturn> getGoodsReturnList() {
		return goodsReturnList;
	}

	public void setGoodsReturnList(List<GoodsReturn> goodsReturnList) {
		this.goodsReturnList = goodsReturnList;
	}

	public List<CreditTerm> getCreditTermList() {
		return creditTermList;
	}

	public void setCreditTermList(List<CreditTerm> creditTermList) {
		this.creditTermList = creditTermList;
	}

	public List<Store> getStoreList() {
		return storeList;
	}

	public void setStoreList(List<Store> storeList) {
		this.storeList = storeList;
	}

	public List<ReceiveVO> getReceiveDS() {
		return receiveDS;
	}

	public void setReceiveDS(List<ReceiveVO> receiveDS) {
		this.receiveDS = receiveDS;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

	public List<GoodsReturnVO> getGoodsReturnDS() {
		return goodsReturnDS;
	}

	public void setGoodsReturnDS(List<GoodsReturnVO> goodsReturnDS) {
		this.goodsReturnDS = goodsReturnDS;
	}

	public List<Receive> getReceiveNotes() {
		return receiveNotes;
	}

	public void setReceiveNotes(List<Receive> receiveNotes) {
		this.receiveNotes = receiveNotes;
	}

	public List<InvoiceVO> getInvoiceDS() {
		return invoiceDS;
	}

	public void setInvoiceDS(List<InvoiceVO> invoiceDS) {
		this.invoiceDS = invoiceDS;
	}

	public List<DebitVO> getDebitDS() {
		return debitDS;
	}

	public void setDebitDS(List<DebitVO> debitDS) {
		this.debitDS = debitDS;
	}

	public List<PaymentVO> getPaymentDS() {
		return paymentDS;
	}

	public void setPaymentDS(List<PaymentVO> paymentDS) {
		this.paymentDS = paymentDS;
	}

	public List<CustomerVO> getCustomerDS() {
		return customerDS;
	}

	public void setCustomerDS(List<CustomerVO> customerDS) {
		this.customerDS = customerDS;
	}

	public List<CreditTermVO> getCreditTermDS() {
		return creditTermDS;
	}

	public void setCreditTermDS(List<CreditTermVO> creditTermDS) {
		this.creditTermDS = creditTermDS;
	}

	public List<CustomerQuotationVO> getCustomerQuotationDS() {
		return customerQuotationDS;
	}

	public void setCustomerQuotationDS(
			List<CustomerQuotationVO> customerQuotationDS) {
		this.customerQuotationDS = customerQuotationDS;
	}

	public List<SalesOrderVO> getSalesOrderDS() {
		return salesOrderDS;
	}

	public void setSalesOrderDS(List<SalesOrderVO> salesOrderDS) {
		this.salesOrderDS = salesOrderDS;
	}

	public List<SalesDeliveryNoteVO> getSalesDeliveryDS() {
		return salesDeliveryDS;
	}

	public void setSalesDeliveryDS(List<SalesDeliveryNoteVO> salesDeliveryDS) {
		this.salesDeliveryDS = salesDeliveryDS;
	}

	public List<SalesInvoiceVO> getSalesInvoiceDS() {
		return salesInvoiceDS;
	}

	public void setSalesInvoiceDS(List<SalesInvoiceVO> salesInvoiceDS) {
		this.salesInvoiceDS = salesInvoiceDS;
	}

	public List<CreditNoteVO> getCreditNoteDS() {
		return creditNoteDS;
	}

	public void setCreditNoteDS(List<CreditNoteVO> creditNoteDS) {
		this.creditNoteDS = creditNoteDS;
	}

	public List<BankReceiptsVO> getBankReceiptsDS() {
		return bankReceiptsDS;
	}

	public void setBankReceiptsDS(List<BankReceiptsVO> bankReceiptsDS) {
		this.bankReceiptsDS = bankReceiptsDS;
	}

	public List<BankDepositVO> getBankDepositDS() {
		return bankDepositDS;
	}

	public void setBankDepositDS(List<BankDepositVO> bankDepositDS) {
		this.bankDepositDS = bankDepositDS;
	}

	public List<PettyCashVO> getPettyCashDS() {
		return pettyCashDS;
	}

	public void setPettyCashDS(List<PettyCashVO> pettyCashDS) {
		this.pettyCashDS = pettyCashDS;
	}

	public BankAccountVO getBankAccountDS() {
		return bankAccountDS;
	}

	public void setBankAccountDS(BankAccountVO bankAccountDS) {
		this.bankAccountDS = bankAccountDS;
	}

	public List<IssueRequistionVO> getIssueRequistionDS() {
		return issueRequistionDS;
	}

	public void setIssueRequistionDS(List<IssueRequistionVO> issueRequistionDS) {
		this.issueRequistionDS = issueRequistionDS;
	}

	public List<BankTransferVO> getBankTransferDS() {
		return bankTransferDS;
	}

	public void setBankTransferDS(List<BankTransferVO> bankTransferDS) {
		this.bankTransferDS = bankTransferDS;
	}

	public List<IssueReturnVO> getIssueReturnDS() {
		return issueReturnDS;
	}

	public void setIssueReturnDS(List<IssueReturnVO> issueReturnDS) {
		this.issueReturnDS = issueReturnDS;
	}

	public List<StockVO> getStockDS() {
		return stockDS;
	}

	public void setStockDS(List<StockVO> stockDS) {
		this.stockDS = stockDS;
	}

	public List<AssetCheckInVO> getCheckInDS() {
		return checkInDS;
	}

	public void setCheckInDS(List<AssetCheckInVO> checkInDS) {
		this.checkInDS = checkInDS;
	}

	public List<AssetCheckOutVO> getCheckOutDS() {
		return checkOutDS;
	}

	public void setCheckOutDS(List<AssetCheckOutVO> checkOutDS) {
		this.checkOutDS = checkOutDS;
	}

	public List<ProductPricingVO> getProductPricingDS() {
		return productPricingDS;
	}

	public void setProductPricingDS(List<ProductPricingVO> productPricingDS) {
		this.productPricingDS = productPricingDS;
	}

	public List<DiscountVO> getProductDiscountDS() {
		return productDiscountDS;
	}

	public void setProductDiscountDS(List<DiscountVO> productDiscountDS) {
		this.productDiscountDS = productDiscountDS;
	}

	public List<PromotionVO> getPromotionDS() {
		return promotionDS;
	}

	public void setPromotionDS(List<PromotionVO> promotionDS) {
		this.promotionDS = promotionDS;
	}

	public List<ProductionReceiveVO> getProductionReceiveDS() {
		return productionReceiveDS;
	}

	public void setProductionReceiveDS(
			List<ProductionReceiveVO> productionReceiveDS) {
		this.productionReceiveDS = productionReceiveDS;
	}

	public List<MaterialTransferVO> getMaterialTransferDS() {
		return materialTransferDS;
	}

	public void setMaterialTransferDS(
			List<MaterialTransferVO> materialTransferDS) {
		this.materialTransferDS = materialTransferDS;
	}

	public List<MaterialRequisitionVO> getMaterialRequisitionDS() {
		return materialRequisitionDS;
	}

	public void setMaterialRequisitionDS(
			List<MaterialRequisitionVO> materialRequisitionDS) {
		this.materialRequisitionDS = materialRequisitionDS;
	}

	public List<PointOfSaleVO> getPointOfSaleDS() {
		return pointOfSaleDS;
	}

	public void setPointOfSaleDS(List<PointOfSaleVO> pointOfSaleDS) {
		this.pointOfSaleDS = pointOfSaleDS;
	}

	public List<EODBalancingVO> getEodBalancingDS() {
		return eodBalancingDS;
	}

	public void setEodBalancingDS(List<EODBalancingVO> eodBalancingDS) {
		this.eodBalancingDS = eodBalancingDS;
	}

	public List<StoreVO> getStoreDS() {
		return storeDS;
	}

	public void setStoreDS(List<StoreVO> storeDS) {
		this.storeDS = storeDS;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public List<ProductCategoryVO> getProductCategoryVOs() {
		return productCategoryVOs;
	}

	public void setProductCategoryVOs(List<ProductCategoryVO> productCategoryVOs) {
		this.productCategoryVOs = productCategoryVOs;
	}

	public ProductCategoryVO getProductCategoryVO() {
		return productCategoryVO;
	}

	public void setProductCategoryVO(ProductCategoryVO productCategoryVO) {
		this.productCategoryVO = productCategoryVO;
	}

	public Byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
}