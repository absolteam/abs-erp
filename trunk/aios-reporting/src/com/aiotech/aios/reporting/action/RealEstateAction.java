package com.aiotech.aios.reporting.action;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.DataFormatException;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.RealEstateEnterpriseService;
import com.aiotech.aios.realestate.to.CompanyVO;
import com.aiotech.aios.realestate.to.ContractReleaseTO;
import com.aiotech.aios.realestate.to.OfferManagementTO;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;
import com.aiotech.aios.realestate.to.PropertyOwnershipVO;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.RePropertyUnitsTO;
import com.aiotech.aios.realestate.to.converter.PropertyCompomentTOConverter;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.reporting.service.bl.RealEstateBL;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.ImageVO;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class RealEstateAction extends ActionSupport {

	RealEstateEnterpriseService realEstateEnterpriseService;
	RealEstateBL realEstateBL;
	Implementation implementation;

	private String messageId;
	private long recordId;
	private String format;
	private String approvalFlag;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String fromDate;
	private String toDate;
	private int tempVar;
	private String tempString;
	private String tempString1;
	private String tempString2;
	private long tempLong;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private Map<Object, Boolean> exportParams = new HashMap<Object, Boolean>();
	private List<Property> propertyDetail = new ArrayList<Property>();
	private List<RePropertyInfoTO> rptList = new ArrayList<RePropertyInfoTO>();
	private List<RePropertyUnitsTO> unitDS = new ArrayList<RePropertyUnitsTO>();
	private List<PropertyCompomentTO> componentDS = new ArrayList<PropertyCompomentTO>();
	private List<OfferManagementTO> offerDS = new ArrayList<OfferManagementTO>();
	private List<ReContractAgreementTO> contractDS = new ArrayList<ReContractAgreementTO>();
	private List<ContractReleaseTO> releaseDS = new ArrayList<ContractReleaseTO>();
	JasperPrint jasperPrint;
	InputStream excelStream;
	String contentDisposition;
	private HSSFCellStyle styleCurrencyFormat = null;
	private HSSFWorkbook workBook = null;
	private HSSFCell hssfCell = null;
	private HSSFSheet sheet = null;
	private HSSFCellStyle cellStyle = null;
	private HSSFFont font = null;
	private HSSFDataFormat cellFormat = null;
	private int totalColumn = 0;
	private HSSFCellStyle currentStyle = null;

	private HSSFCellStyle currentCell = null;
	private HSSFRow rowCell = null;
	private HSSFDataFormat dataFormat = null;
	private InputStream fileInputStream;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String writeExcel() {
		try {
			HSSFWorkbook workBook = new HSSFWorkbook();
			String sheeetName = "General Report";
			List headerList = new ArrayList();
			headerList.add("Property Name");
			headerList.add("Property Type");
			headerList.add("Rent Year");
			headerList.add("Property Owners");
			headerList.add("Property Status");
			rptList = new ArrayList<RePropertyInfoTO>();
			RePropertyInfoTO propertyTo = null;
			for (int i = 0; i < 4; i++) {
				propertyTo = new RePropertyInfoTO();
				if (i == 1) {
					propertyTo.setPropertyName("XE-AZ-ADL-M101B");
					propertyTo.setPropertyTypeName("Villa");
				} else if (i == 2) {
					propertyTo.setPropertyName("AE-AZ-ADL-900V");
					propertyTo.setPropertyTypeName("Building");
				} else if (i == 3) {
					propertyTo.setPropertyName("AE-AZ-ADL-M101B");
					propertyTo.setPropertyTypeName("Villa");
				} else {
					propertyTo.setPropertyName("Flat-8941");
					propertyTo.setPropertyTypeName("Building");
				}
				propertyTo.setRentPerYear("15200D");
				propertyTo.setOwnerName("Adel Al Hosani");
				propertyTo.setWorkflowStatus("AVAILABLE");
				rptList.add(propertyTo);
			}
			workBook = exportToExcel(sheeetName, headerList, rptList);
			// String[][] sarray=new String[headerList.size()][rptList.size()];
			// List<String[]> sarray=arrayConverter(rptList,headerList);

			// workBook=exportToExcel(sheeetName, headerList, sarray);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workBook.write(baos);
			excelStream = new ByteArrayInputStream(baos.toByteArray());
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public List<String[]> arrayConverter(List<RePropertyInfoTO> rptList,
			List<RePropertyInfoTO> headerList) {
		List<String[]> sarray = new ArrayList<String[]>();
		int i = 0;
		for (RePropertyInfoTO rePropertyInfoTO : rptList) {
			String[] strArry = new String[headerList.size()];
			for (int j = 0; j < headerList.size(); j++) {
				if (j == 0)
					strArry[j] = rePropertyInfoTO.getPropertyName();
				if (j == 1)
					strArry[j] = rePropertyInfoTO.getPropertyTypeName();
				if (j == 2)
					strArry[j] = rePropertyInfoTO.getRentPerYear() + "";
				if (j == 3)
					strArry[j] = rePropertyInfoTO.getOwnerName();
				if (j == 4)
					strArry[j] = rePropertyInfoTO.getWorkflowStatus();
			}
			sarray.add(strArry);
			i++;

		}

		return sarray;

	}

	public HSSFWorkbook exportToExcel(String sheeetName, List headerList,
			List<RePropertyInfoTO> rptList) throws Exception {
		workBook = new HSSFWorkbook();
		CreationHelper createHelper = workBook.getCreationHelper();
		sheet = workBook.createSheet(sheeetName);
		int rowIndex = 0;
		int cellIndex = 0;
		totalColumn = 0;
		// Create column header
		HSSFRow hssfHeader = sheet.createRow(rowIndex);
		HSSFCellStyle cellStyleHD = workBook.createCellStyle();
		cellStyleHD.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		font = workBook.createFont();

		cellStyleHD.setFillForegroundColor((short) Color.CREAM);
		cellStyleHD.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		for (Iterator cells = headerList.iterator(); cells.hasNext();) {
			setColumnWidth(cellIndex, sheet, cells.next().toString().length());
			hssfCell = hssfHeader.createCell(cellIndex++);
			totalColumn++;
		}
		cellIndex = 0;

		for (Iterator cells = headerList.iterator(); cells.hasNext();) {
			hssfCell = hssfHeader.createCell(cellIndex++);
			makeFontBold(cellStyleHD, font, hssfCell);
			hssfCell.setCellValue((String) cells.next());
		}
		cellStyle = workBook.createCellStyle();
		rowIndex = 1;
		cellIndex = 0;
		for (RePropertyInfoTO list : rptList) {
			HSSFRow hssfRow = sheet.createRow(rowIndex++);
			writeDataCell(hssfRow, cellIndex, list);
		}
		rowIndex += 4;
		cellIndex = 0;
		HSSFRow reportDateRow = sheet.createRow(rowIndex);
		reportDate(createHelper, reportDateRow);
		return workBook;
	}

	public void makeFontBold(HSSFCellStyle cellStyleRD, HSSFFont font,
			HSSFCell cell) {
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setFontName("Courier New");
		font.setItalic(true);
		font.setColor(HSSFColor.BROWN.index);
		cellStyleRD.setFont(font);
		cell.setCellStyle(cellStyleRD);
	}

	public static void setColumnWidth(int cellIndex, HSSFSheet sheet, int length) {
		if (length < 20) {
			length += 12;
		}
		sheet.setColumnWidth(cellIndex, (short) (length * 256));
	}

	public void reportDate(CreationHelper createHelper, HSSFRow reportDateRow) {
		HSSFFont font = workBook.createFont();
		HSSFCellStyle cellStyleRD = workBook.createCellStyle();
		int cellIndex = 1;
		hssfCell = reportDateRow.createCell(cellIndex++);
		hssfCell.setCellValue("Report Date");
		makeFontBold(cellStyleRD, font, hssfCell);

		hssfCell = reportDateRow.createCell(cellIndex);
		cellStyleRD.setDataFormat(createHelper.createDataFormat().getFormat(
				"dd-MMM-yyyy HH:mm:ss"));
		setColumnWidth(cellIndex, sheet, 0);
		hssfCell.setCellValue(new Date());
		makeFontBold(cellStyleRD, font, hssfCell);
		hssfCell.setCellStyle(cellStyleRD);
	}

	public void writeDataCell(HSSFRow rowCell, int cellIndex,
			RePropertyInfoTO list) {
		rowCell.createCell(cellIndex++).setCellValue(list.getPropertyName());
		rowCell.createCell(cellIndex++)
				.setCellValue(list.getPropertyTypeName());
		rowCell.createCell(cellIndex++).setCellValue(list.getRentPerYear());
		setCurrencyFormat(cellIndex, rowCell);
		rowCell.createCell(cellIndex++).setCellValue(list.getOwnerName());
		rowCell.createCell(cellIndex++).setCellValue(list.getWorkflowStatus());
		if (list.getPropertyTypeName().equalsIgnoreCase("Building")) {
			setRowBackgroundColor(rowCell, Color.BLUE);
		} else {
			setRowBackgroundColor(rowCell, Color.FLESH);
		}
	}

	public void setRowBackgroundColor(HSSFRow rowCell, int color) {
		cellStyle.setFillForegroundColor((short) color);
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		for (int i = 0; i < totalColumn; i++) {
			rowCell.getCell(i).setCellStyle(cellStyle);
		}
	}

	public void setCurrencyFormat(int cellIndex, HSSFRow rowCells) {
		cellIndex -= 1;
		HSSFCellStyle cellStyle1 = workBook.createCellStyle();
		cellFormat = workBook.createDataFormat();
		cellStyle1.setDataFormat(cellFormat.getFormat("#,##0.00")); //
		rowCells.getCell(cellIndex).setCellStyle(cellStyle1);
	}

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showDefaultJsonList() {
		try {
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (int i = 0; i < 1; i++) {
				JSONArray array = new JSONArray();
				array.add("1");
				array.add("AL MUSHRIF GARDEN");
				array.add("Building");
				array.add("152000");
				array.add("Adel Al Hosani");
				array.add("AVAILABLE");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getDefaultReport() throws Exception {

		try {
			rptList = new ArrayList<RePropertyInfoTO>();
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			RePropertyInfoTO to = new RePropertyInfoTO();
			to.setBuildingId(1);
			to.setBuildingName("AL MUSHRIF GARDEN");
			to.setPropertyTypeName("Building");
			to.setRentPerYear("152000D");
			to.setOwnerName("Adel Al Hosani");
			to.setComments("AVAILABLE");
			rptList.add(to);
			to.setDefaultList(rptList);
			exportParams = new HashMap<Object, Boolean>();
			exportParams.put(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN,
					Boolean.TRUE);
			exportParams
					.put(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,
							Boolean.TRUE);
			exportParams.put(
					JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
					Boolean.TRUE);
			exportParams.put(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
					Boolean.FALSE);
			exportParams.put(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,
					Boolean.FALSE);
			exportParams.put(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
					Boolean.FALSE);
			exportParams.put(JRXlsExporterParameter.IS_IGNORE_GRAPHICS,
					Boolean.FALSE);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String populatePropertyDetail() {

		Property property = new Property();
		property.setPropertyId(recordId);
		rptList = new ArrayList<RePropertyInfoTO>();

		try {
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			Property prop = realEstateBL.getPropertyDetail(property);
			List<PropertyOwnershipVO> personOwners = realEstateBL
					.getOwnerPersons(prop.getPropertyOwnerships());
			List<CompanyVO> maintenanceComps = realEstateBL
					.getMaintenanceCompanies(prop.getMaintenances());

			List<ImageVO> detailImages = realEstateBL
					.populatePropertyDetailImgs(prop);

			propertyDetail.add(prop);
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			Document document = new Document();
			document.setRecordId(prop.getPropertyId());

			Image image = new Image();
			image.setRecordId(prop.getPropertyId());
			image.setTableName("Property");
			List<ImageVO> imgList = realEstateBL.getImageBL()
					.retrieveCopiedImages(image);

			RePropertyInfoTO to = RePropertyInfoTOConverter.convertToTO(prop);
			to.setAddress(realEstateBL.getPropertyAddress(prop));
			to.setPropertyImages(imgList);
			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropertyDetal.action?recordId="
					+ recordId + "&approvalFlag=N&format=PDF"
					: "#");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&messageId="
					+ messageId
					+ "&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0 "
					+ "&messageId="
					+ messageId
					+ "&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			to.setPropertyOwners(personOwners);
			to.setMaintenanceComps(maintenanceComps);
			to.setDetailImgs(detailImages);
			to.setDetailList(prop.getPropertyDetails());
			if (rptList != null && rptList.size() == 0)
				rptList.add(to);
			else
				rptList.set(0, to);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private List<ImageVO> populatePropertyDetailImgs(Component comp)
			throws IOException, DataFormatException {
		Set<ComponentDetail> compDetails = comp.getComponentDetails();
		List<ImageVO> imgList = new ArrayList<ImageVO>();
		for (ComponentDetail detail : compDetails) {
			Image img = new Image();
			img.setRecordId(detail.getComponentDetailId());
			img.setTableName("ComponentDetail");
			imgList.addAll(realEstateBL.getImageBL().retrieveCopiedImages(img));
		}
		return imgList;
	}

	// Property Component
	public String getComponentDetail() {
		try {
			componentDS = new ArrayList<PropertyCompomentTO>();

			Component component = new Component();
			component.setComponentId((long) recordId);
			PropertyCompomentTO to = realEstateBL
					.getComponentDetails(component);
			PropertyCompomentTO cmpTO = PropertyCompomentTOConverter
					.convertToTO(to.getComponent());

			cmpTO.setCompDetailTOList(PropertyCompomentTOConverter
					.convertDetailToTOList(to.getComponentDetails()));
			cmpTO.setCompDetailImgs(populatePropertyDetailImgs(to
					.getComponent()));
			Image image = new Image();
			image.setRecordId(to.getComponent().getComponentId());
			image.setTableName("Component");
			List<ImageVO> imgList = realEstateBL.getImageBL()
					.retrieveCopiedImages(image);
			cmpTO.setCompImgs(imgList);
			cmpTO.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			cmpTO.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropertyCompDetail.action?componentId="
					+ component.getComponentId() + "&approvalFlag=N&format=PDF"
					: "#");
			// cmpTO.setAnchorExpression("downloadPropertyCompDetail.action?componentId="+
			// componentId + "&format=PDF");
			// Approve Content
			cmpTO.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			cmpTO.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			cmpTO.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			cmpTO.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));
			componentDS.add(cmpTO);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (componentDS != null && componentDS.size() == 0) {
				componentDS.add(cmpTO);
			} else {
				componentDS.set(0, cmpTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property unit
	public String getUnitDetail() {
		try {
			unitDS = new ArrayList<RePropertyUnitsTO>();

			Unit unit = new Unit();

			unit.setUnitId((long) recordId);
			String f = getFormat();
			RePropertyUnitsTO to = realEstateBL.getUnitDetails(unit);
			to.setUnitDetailImgs(realEstateBL.populateUnitDetailImgs(to
					.getUnitDetailVO()));
			Image image = new Image();
			image.setRecordId(to.getUnitId());
			image.setTableName("Unit");
			List<ImageVO> imgList = realEstateBL.getImageBL()
					.retrieveCopiedImages(image);
			to.setUnitImgs(imgList);
			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropertyUnitDetail.action?unitId="
					+ unit.getUnitId() + "&approvalFlag=N&format=PDF"
					: "#");
			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));
			unitDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (unitDS != null && unitDS.size() == 0) {
				unitDS.add(to);
			} else {
				unitDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property unit
	public String getUnitReport() {
		try {
			unitDS = new ArrayList<RePropertyUnitsTO>();
			RePropertyUnitsTO to = new RePropertyUnitsTO();
			List<RePropertyUnitsTO> unitList = realEstateBL.getUnitBL()
					.getUnitListForReport(Byte.parseByte(tempVar + ""));
			if (unitList != null && unitList.size() > 0) {
				to.setUnitList(unitList);
				to.setSizeUnit(unitList.size());
			}
			if (tempVar == -1)
				to.setUnitStatus("ALL");
			else
				to.setUnitStatus(Constants.RealEstate.UnitStatus.get(tempVar)
						.name());
			unitDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			jasperRptParams.put("LIMIT", unitList.size());
			if (unitDS != null && unitDS.size() == 0) {
				unitDS.add(to);
			} else {
				unitDS.set(0, to);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Offer
	public String getOfferDetail() {
		try {
			Offer offer = new Offer();
			offerDS = new ArrayList<OfferManagementTO>();
			offer.setOfferId((long) recordId);
			OfferManagementTO to = realEstateBL.getOfferDetails(offer);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropOfferDetail.action?releaseId="
					+ offer.getOfferId() + "&approvalFlag=N&format=PDF"
					: "#");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			offerDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (offerDS != null && offerDS.size() == 0) {
				offerDS.add(to);
			} else {
				offerDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Contract
	public String getContractDetail() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			Contract contract = new Contract();

			contract.setContractId((long) recordId);
			ReContractAgreementTO to = realEstateBL.getContractDetail(contract);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropContractDetail.action?contractId="
					+ contract.getContractId() + "&approvalFlag=N&format=PDF"
					: "#");
			// to.setAnchorExpression("downloadPropContractDetail.action?contractId="+
			// contractId + "&format=PDF");
			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getAllPropertiesForReport() throws Exception {

		rptList = new ArrayList<RePropertyInfoTO>();
		List<Property> properties = realEstateBL
				.getRealEstateEnterpriseService().getPropertyService()
				.getAllProperties(getImplementation());

		for (Property property : properties) {

			try {
				String ctxRealPath = ServletActionContext.getServletContext()
						.getRealPath("/");
				Property prop = realEstateBL.getPropertyDetail(property);
				List<PropertyOwnershipVO> personOwners = realEstateBL
						.getOwnerPersons(prop.getPropertyOwnerships());
				List<CompanyVO> maintenanceComps = realEstateBL
						.getMaintenanceCompanies(prop.getMaintenances());

				jasperRptParams.put(
						"AIOS_LOGO",
						"file:///"
								+ ctxRealPath.concat(File.separator).concat(
										"images" + File.separator
												+ "aiotech.jpg"));

				RePropertyInfoTO to = RePropertyInfoTOConverter
						.convertToTO(prop);
				to.setAddress(realEstateBL.getPropertyAddress(prop));
				to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
				to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadAllProperties.action?recordId="
						+ "0" + "&approvalFlag=N&format=PDF"
						: "#");
				to.setExportLabel("HTML".equals(getFormat()) ? "Export to Excel"
						: "");
				to.setExportAnchorExpression("HTML".equals(getFormat()) ? "exportAllProperties.action?recordId="
						+ "0" + "&format=XLS"
						: "#");

				to.setCompanyName(getImplementation().getCompanyName());
				if (maintenanceComps.get(0) != null)
					to.setMaintainer(maintenanceComps.get(0).getCompanyName());
				to.setOwners("");
				for (PropertyOwnershipVO ownership : personOwners) {
					to.setOwners(to.getOwners() + ownership.getOwnerName()
							+ " | ");
				}

				if (rptList != null)
					rptList.add(to);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}

	// Release Initializing approval
	public String getReleaseDetail() {
		try {
			releaseDS = new ArrayList<ContractReleaseTO>();

			Release rel = new Release();

			rel.setReleaseId((long) recordId);
			ContractReleaseTO to = realEstateBL.getReleaseListInfo(rel);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropReleaseDetail.action?releaseId="
					+ rel.getReleaseId() + "&approvalFlag=N&format=PDF"
					: "#");
			// to.setAnchorExpression("downloadPropReleaseDetail.action?releaseId="+
			// releaseId + "&format=PDF");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			releaseDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (releaseDS != null && releaseDS.size() == 0) {
				releaseDS.add(to);
			} else {
				releaseDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Release Finalizing approval
	public String getReleaseDetailFinalApproval() {
		try {
			releaseDS = new ArrayList<ContractReleaseTO>();

			Release rel = new Release();

			rel.setReleaseId((long) recordId);
			ContractReleaseTO to = realEstateBL.getReleaseListInfoFinal(rel);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropReleaseDetailApprove.action?releaseId="
					+ rel.getReleaseId() + "&approvalFlag=N&format=PDF"
					: "#");
			// to.setAnchorExpression("downloadPropReleaseDetail.action?releaseId="+
			// releaseId + "&format=PDF");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			releaseDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (releaseDS != null && releaseDS.size() == 0) {
				releaseDS.add(to);
			} else {
				releaseDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Cancellation
	public String getCancelContractDetail() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			Cancellation cancellation = new Cancellation();
			Contract contract = new Contract();

			cancellation = realEstateBL.getRealEstateEnterpriseService()
					.getCancellationService().getCancellationInfo(recordId);
			contract.setContractId(cancellation.getContract().getContractId());
			ReContractAgreementTO to = realEstateBL.getContractDetail(contract);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropContractDetail.action?contractId="
					+ contract.getContractId() + "&approvalFlag=N&format=PDF"
					: "#");
			// to.setAnchorExpression("downloadPropContractDetail.action?contractId="+
			// contractId + "&format=PDF");
			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Cancellation Finalizing approval
	public String getCancellationDetailFinalApproval() {
		try {
			releaseDS = new ArrayList<ContractReleaseTO>();

			Release rel = new Release();
			Contract contract = new Contract();
			rel.setReleaseId((long) recordId);
			Cancellation cancel = realEstateBL.getRealEstateEnterpriseService()
					.getCancellationService().getCancellationInfo(recordId);
			contract.setContractId(cancel.getContract().getContractId());
			rel = realEstateBL.getRealEstateEnterpriseService()
					.getReleaseService().getReleaseUseContract(contract);

			ContractReleaseTO to = realEstateBL.getReleaseListInfoFinal(rel);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropReleaseDetailApprove.action?releaseId="
					+ rel.getReleaseId() + "&approvalFlag=N&format=PDF"
					: "#");
			// to.setAnchorExpression("downloadPropReleaseDetail.action?releaseId="+
			// releaseId + "&format=PDF");

			// Approve Content
			to.setApproveLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Approve" : "");
			to.setApproveAnchorExpression("workflowProcess.action?finalyzed=1&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel((getApprovalFlag() != null && "Y"
					.equals(getApprovalFlag())) ? "Reject" : "");
			to.setRejectAnchorExpression("workflowProcess.action?finalyzed=0&processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			releaseDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (releaseDS != null && releaseDS.size() == 0) {
				releaseDS.add(to);
			} else {
				releaseDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Get Tenant contract list
	public String getTenantContractList() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			boolean showArabic = true;
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			String tempId[] = new String[2];
			if (tempString != null && !tempString.equals("")) {
				tempId = tempString.split("@");
			} else {
				tempId[0] = "0";
				tempId[1] = "";
			}

			if (fromDate != null && !fromDate.equals(""))
				to.setFromDate(fromDate);
			else
				to.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				to.setToDate(toDate);
			else
				to.setToDate("-NA-");

			if (tempString1 != null && !tempString1.equals(""))
				to.setTenantName(tempString1);
			else
				to.setTenantName("-NA-");

			if (tempString2 != null && !tempString2.equals(""))
				to.setUnitName(tempString2);
			else
				to.setUnitName("-NA-");

			/*
			 * List<ReContractAgreementTO> contractAgreementList =
			 * realEstateBL.getContractAgreementBL()
			 * .getContractReportFilterInformation
			 * (getImplementation(),fromDate,toDate,approvalFlag);
			 */

			List<ReContractAgreementTO> contractAgreementList = realEstateBL
					.getContractAgreementBL().getContractFilterInformation(
							getImplementation(), fromDate, toDate,
							Long.parseLong(tempId[0]), tempId[1], tempLong);
			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {

				to.setContractAgreementList(contractAgreementList);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Get Tenant contract list
	public String getContractRentalRangeReport() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			boolean showArabic = true;
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			Double fromAmount = null;
			Double toAmount = null;
			if (fromDate != null && !fromDate.equalsIgnoreCase(""))
				fromAmount = Double.parseDouble(fromDate);

			if (toDate != null && !toDate.equalsIgnoreCase(""))
				toAmount = Double.parseDouble(toDate);

			List<ReContractAgreementTO> contractAgreementList = realEstateBL
					.getContractAgreementBL()
					.getContractRentalRangeReportFilterInformation(
							implementation, fromAmount, toAmount);

			if (fromDate != null && !fromDate.equals(""))
				to.setFromDate(fromDate);
			else
				to.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				to.setToDate(toDate);
			else
				to.setToDate("-NA-");

			to.setTenantName("-NA-");
			to.setUnitName("-NA-");

			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {

				to.setContractAgreementList(contractAgreementList);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Get Unit contract list
	public String getUnitContractList() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			char activeFlag = 'Y';
			List<ReContractAgreementTO> contractAgreementList = realEstateBL
					.getContractAgreementBL().contractFilterByUnitsReport(
							recordId, activeFlag);
			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {
				to.setBuildingName(contractAgreementList.get(0)
						.getBuildingName());
				to.setPropertyFullInfo(contractAgreementList.get(0)
						.getPropertyFullInfo());
				to.setContractAgreementList(contractAgreementList);

			}
			activeFlag = 'N';
			List<ReContractAgreementTO> contractAgreementListInactive = realEstateBL
					.getContractAgreementBL().contractFilterByUnitsReport(
							recordId, activeFlag);
			if (contractAgreementListInactive != null
					&& contractAgreementListInactive.size() > 0) {
				to.setBuildingName(contractAgreementListInactive.get(0)
						.getBuildingName());
				to.setPropertyFullInfo(contractAgreementListInactive.get(0)
						.getPropertyFullInfo());
				to.setContractAgreementListInactive(contractAgreementListInactive);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Get contract payment list
	public String getContractPaymentList() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			Byte paymentStatus = Byte.parseByte(approvalFlag);
			List<ReContractAgreementTO> contractAgreementList = realEstateBL
					.getContractAgreementBL().getContractPaymentList(
							getImplementation(), paymentStatus,
							DateFormat.convertStringToDate(fromDate),
							DateFormat.convertStringToDate(toDate));
			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {
				to.setPaymentStatus(ReContractAgreementTOConverter
						.paymentStatusString(paymentStatus));
				if (fromDate != null && toDate != null && !fromDate.equals("")) {
					to.setFromDate(fromDate);
					to.setToDate(toDate);
				} else {
					to.setFromDate("-NA-");
					to.setToDate("-NA-");
				}
				to.setContractAgreementList(contractAgreementList);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property unit rental report
	public String getPropertyUnitRentalReport() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			List<ReContractAgreementTO> contractAgreementList = realEstateBL
					.getContractAgreementBL().getPropertyUnitRentalList(
							Long.parseLong(recordId + ""),
							DateFormat.convertStringToDate(fromDate),
							DateFormat.convertStringToDate(toDate));
			Double total = 0.0;
			long propid = 0;

			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {
				for (ReContractAgreementTO contlist : contractAgreementList) {
					total += contlist.getContractAmount();
					if (contlist.getBuildingId() == propid) {
						contlist.setBuildingName("");
					}
					propid = contlist.getBuildingId();
				}
				to.setBuildingName(tempString);
				if (fromDate != null && toDate != null && !fromDate.equals("")) {
					to.setFromDate(fromDate);
					to.setToDate(toDate);
				} else {
					to.setFromDate("-NA-");
					to.setToDate("-NA-");
				}
				to.setGrandTotal(total);

				to.setContractAgreementList(contractAgreementList);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			jasperRptParams.put("TOTAL_AMOUNT", total);
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property rental report
	public String getPropertyRentalReport() {
		try {
			contractDS = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			List<ReContractAgreementTO> contractAgreementList = null;
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals("")) {
				contractAgreementList = realEstateBL.getContractAgreementBL()
						.getPropertyRentalList(getImplementation(),
								DateFormat.convertStringToDate(fromDate),
								DateFormat.convertStringToDate(toDate));
			}
			Double total = 0.0;
			String area = "";
			if (contractAgreementList != null
					&& contractAgreementList.size() > 0) {
				for (ReContractAgreementTO contlist : contractAgreementList) {
					total += contlist.getContractAmount();
					if (contlist.getBuildingArea() != null
							&& contlist.getBuildingArea()
									.equalsIgnoreCase(area)) {
						contlist.setBuildingArea("");
					}
					area = contlist.getBuildingArea();
				}
				if (fromDate != null && toDate != null && !fromDate.equals("")) {
					to.setFromDate(fromDate);
					to.setToDate(toDate);
				} else {
					to.setFromDate("-NA-");
					to.setToDate("-NA-");
				}
				to.setGrandTotal(total);

				to.setContractAgreementList(contractAgreementList);

			}
			contractDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			jasperRptParams.put("TOTAL_AMOUNT", total);
			if (contractDS != null && contractDS.size() == 0) {
				contractDS.add(to);
			} else {
				contractDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property unit vacancy report
	public String getPropertyUnitAvailabilityReport() {
		try {
			rptList = new ArrayList<RePropertyInfoTO>();
			RePropertyInfoTO to = new RePropertyInfoTO();
			List<RePropertyInfoTO> propertyList = new ArrayList<RePropertyInfoTO>();
			if (recordId > 0 && fromDate != null && !fromDate.trim().equals("")) {
				propertyList = realEstateBL.getPropertyInfoBL()
						.getPropertyUnitVacancyList(
								Long.parseLong(recordId + ""),
								DateFormat.convertStringToDate(fromDate),
								DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.trim().equals("")) {
				List<Property> properties = realEstateBL.getPropertyInfoBL()
						.getPropertyService()
						.getAllProperties(getImplementation());
				for (Property property : properties) {
					List<RePropertyInfoTO> propertyListTemp = new ArrayList<RePropertyInfoTO>();
					propertyListTemp = realEstateBL.getPropertyInfoBL()
							.getPropertyUnitVacancyList(
									property.getPropertyId(),
									DateFormat.convertStringToDate(fromDate),
									DateFormat.convertStringToDate(toDate));
					propertyList.addAll(propertyListTemp);
				}
			}

			if (propertyList != null && propertyList.size() > 0) {
				if (tempString != null && !tempString.equals("")
						&& !tempString.trim().equals("--Select--")) {
					to.setPropertyName(tempString);
				} else {
					to.setPropertyName("--All Properties--");
				}
				if (fromDate != null && toDate != null) {
					to.setFromDate(fromDate);
				} else {
					to.setFromDate("-NA-");
				}

				to.setPropertyList(propertyList);
			}
			rptList.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			if (rptList != null && rptList.size() == 0) {
				rptList.add(to);
			} else {
				rptList.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	// Property size report
	public String getPropertySizeReport() {
		try {
			rptList = new ArrayList<RePropertyInfoTO>();
			RePropertyInfoTO to = new RePropertyInfoTO();
			List<RePropertyInfoTO> propertyList = realEstateBL
					.getPropertyInfoBL().propertySizeReportInfo(
							getImplementation(), Byte.parseByte(tempVar + ""));
			if (propertyList != null && propertyList.size() > 0) {
				int prpTypid = 0;
				long propid = 0;
				for (int i = 0; i < propertyList.size(); i++) {

					// Property type cell merge process
					if (propertyList.get(i).getPropertyTypeId() == prpTypid) {
						propertyList.get(i).setPropertyTypeName("");
					}
					prpTypid = propertyList.get(i).getPropertyTypeId();

					// Property name & size cell merge process
					if (propertyList.get(i).getPropertyId() == propid) {
						propertyList.get(i).setBuildingName("");
						propertyList.get(i).setLandSize("");
						propertyList.get(i).setBuildingSize("");
						propertyList.get(i).setAddress("");
						propertyList.get(i).setComponentCount("");
					}
					propid = propertyList.get(i).getPropertyId();
				}
			}
			if (tempVar != -1)
				to.setStatusStr(Constants.RealEstate.UnitStatus.get(tempVar)
						.name());
			else
				to.setStatusStr("ALL");

			to.setPropertyList(propertyList);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			if (rptList != null && rptList.size() == 0) {
				rptList.add(to);
			} else {
				rptList.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public RealEstateEnterpriseService getRealEstateEnterpriseService() {
		return realEstateEnterpriseService;
	}

	public void setRealEstateEnterpriseService(
			RealEstateEnterpriseService realEstateEnterpriseService) {
		this.realEstateEnterpriseService = realEstateEnterpriseService;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public List<Property> getPropertyDetail() {
		return propertyDetail;
	}

	public List<RePropertyInfoTO> getRptList() {
		return rptList;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public void setPropertyDetail(List<Property> propertyDetail) {
		this.propertyDetail = propertyDetail;
	}

	public void setRptList(List<RePropertyInfoTO> rptList) {
		this.rptList = rptList;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public RealEstateBL getRealEstateBL() {
		return realEstateBL;
	}

	public void setRealEstateBL(RealEstateBL realEstateBL) {
		this.realEstateBL = realEstateBL;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public List<RePropertyUnitsTO> getUnitDS() {
		return unitDS;
	}

	public void setUnitDS(List<RePropertyUnitsTO> unitDS) {
		this.unitDS = unitDS;
	}

	public List<PropertyCompomentTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<PropertyCompomentTO> componentDS) {
		this.componentDS = componentDS;
	}

	public List<ReContractAgreementTO> getContractDS() {
		return contractDS;
	}

	public void setContractDS(List<ReContractAgreementTO> contractDS) {
		this.contractDS = contractDS;
	}

	public List<OfferManagementTO> getOfferDS() {
		return offerDS;
	}

	public void setOfferDS(List<OfferManagementTO> offerDS) {
		this.offerDS = offerDS;
	}

	public String getApprovalFlag() {
		return approvalFlag;
	}

	public void setApprovalFlag(String approvalFlag) {
		this.approvalFlag = approvalFlag;
	}

	public List<ContractReleaseTO> getReleaseDS() {
		return releaseDS;
	}

	public void setReleaseDS(List<ContractReleaseTO> releaseDS) {
		this.releaseDS = releaseDS;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Map<Object, Boolean> getExportParams() {
		return exportParams;
	}

	public void setExportParams(Map<Object, Boolean> exportParams) {
		this.exportParams = exportParams;
	}

	public InputStream getExcelStream() {
		return excelStream;
	}

	public void setExcelStream(InputStream excelStream) {
		this.excelStream = excelStream;
	}

	public HSSFCell getHssfCell() {
		return hssfCell;
	}

	public void setHssfCell(HSSFCell hssfCell) {
		this.hssfCell = hssfCell;
	}

	public HSSFSheet getSheet() {
		return sheet;
	}

	public void setSheet(HSSFSheet sheet) {
		this.sheet = sheet;
	}

	public HSSFWorkbook getWorkBook() {
		return workBook;
	}

	public void setWorkBook(HSSFWorkbook workBook) {
		this.workBook = workBook;
	}

	public HSSFCellStyle getCellStyle() {
		return cellStyle;
	}

	public void setCellStyle(HSSFCellStyle cellStyle) {
		this.cellStyle = cellStyle;
	}

	public HSSFFont getFont() {
		return font;
	}

	public void setFont(HSSFFont font) {
		this.font = font;
	}

	public HSSFDataFormat getCellFormat() {
		return cellFormat;
	}

	public void setCellFormat(HSSFDataFormat cellFormat) {
		this.cellFormat = cellFormat;
	}

	public int getTotalColumn() {
		return totalColumn;
	}

	public void setTotalColumn(int totalColumn) {
		this.totalColumn = totalColumn;
	}

	public HSSFCellStyle getStyleCurrencyFormat() {
		return styleCurrencyFormat;
	}

	public void setStyleCurrencyFormat(HSSFCellStyle styleCurrencyFormat) {
		this.styleCurrencyFormat = styleCurrencyFormat;
	}

	public HSSFCellStyle getCurrentCell() {
		return currentCell;
	}

	public void setCurrentCell(HSSFCellStyle currentCell) {
		this.currentCell = currentCell;
	}

	public HSSFRow getRowCell() {
		return rowCell;
	}

	public void setRowCell(HSSFRow rowCell) {
		this.rowCell = rowCell;
	}

	public HSSFDataFormat getDataFormat() {
		return dataFormat;
	}

	public void setDataFormat(HSSFDataFormat dataFormat) {
		this.dataFormat = dataFormat;
	}

	public HSSFCellStyle getCurrentStyle() {
		return currentStyle;
	}

	public void setCurrentStyle(HSSFCellStyle currentStyle) {
		this.currentStyle = currentStyle;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public JasperPrint getJasperPrint() {
		return jasperPrint;
	}

	public void setJasperPrint(JasperPrint jasperPrint) {
		this.jasperPrint = jasperPrint;
	}

	public int getTempVar() {
		return tempVar;
	}

	public void setTempVar(int tempVar) {
		this.tempVar = tempVar;
	}

	public String getTempString() {
		return tempString;
	}

	public void setTempString(String tempString) {
		this.tempString = tempString;
	}

	public long getTempLong() {
		return tempLong;
	}

	public void setTempLong(long tempLong) {
		this.tempLong = tempLong;
	}

	public String getTempString1() {
		return tempString1;
	}

	public void setTempString1(String tempString1) {
		this.tempString1 = tempString1;
	}

	public String getTempString2() {
		return tempString2;
	}

	public void setTempString2(String tempString2) {
		this.tempString2 = tempString2;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

}