package com.aiotech.aios.reporting.action;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.Ledger;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.vo.BankDepositVO;
import com.aiotech.aios.accounts.domain.entity.vo.CreditNoteVO;
import com.aiotech.aios.accounts.to.LoanTO;
import com.aiotech.aios.common.to.Constants.Accounts.Priority;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectDeliveryStatus;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectExpenseMode;
import com.aiotech.aios.common.to.Constants.Accounts.ProjectStatus;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.project.domain.entity.vo.ProjectDeliveryVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectExpenseVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectMileStoneVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectPaymentDetailVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectTaskVO;
import com.aiotech.aios.project.domain.entity.vo.ProjectVO;
import com.aiotech.aios.project.service.ProjectManagmentEnterpriseService;
import com.aiotech.aios.reporting.service.bl.AccountsBL;
import com.aiotech.aios.reporting.service.bl.ProjectManagmentBL;
import com.aiotech.aios.reporting.to.AccountsTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class ProjectManagmentAction extends ActionSupport {

	private static final long serialVersionUID = -263341506191144134L;
	private static final Logger LOGGER = LogManager
			.getLogger(ProjectManagmentAction.class);
	private ProjectManagmentEnterpriseService projectManagmentEnterpriseService;
	private ProjectManagmentBL projectManagmentBL;
	private AccountsBL accountsBL;
	// private DirectPaymentBL directPaymentBL;
	private String format;
	private Implementation implementation;
	private Ledger ledger = null;
	private List<Ledger> ledgerList;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private List<AccountsTO> accountsList;
	private List<Payment> paymentList;
	private List<LoanTO> loanList;
	private long loanId;
	private String approvalFlag;
	private long recordId;
	private long categoryId;
	private long periodId;
	private long codeCombinationId;
	private long journalId;
	private String fromDate;
	private String toDate;
	private String accountList;
	private double transactionAmount;
	private List<BankDepositVO> bankDepositList;
	private InputStream fileInputStream;

	private List<Calendar> calenderList;
	private String selectedCalendarIds;
	private List<AccountsTO> comulativeBalanceSheet;
	private List<Object> aaData;

	private List<String> itemTypes;
	private List<Product> productsList;
	private String productId;
	private String selectedType;

	private List<Period> periods;
	private String selectedPeriod;
	private String searchCriteria;
	private List<Object> entityObjects;
	private List<AccountsTO> inventoryStatementDS;
	private String selectedProductId;
	private String selectedSupplierId;

	private String selectedSupplier;
	private String returnMessage;
	private boolean taskExpense;
	private List<ProjectMileStoneVO> projectMileStoneDS;
	private List<ProjectVO> projectDS;
	private List<ProjectDeliveryVO> projectDeliveryDS;
	private List<ProjectPaymentDetailVO> projectPaymentDS;
	private Long projectId;
	private Long projectDeliveryId;
	private Long projectTaskId;
	private String printableContent;

	public String returnSuccess() {
		LOGGER.info("Inside Accounts method returnSuccess()");
		return SUCCESS;
	}

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	public String getProjectReportCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PERSON_LIST",
					accountsBL.getPersonBL().getPersonService()
							.getAllPerson(implementation));
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_TYPE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_TYPE", true));
			ServletActionContext.getRequest().setAttribute(
					"RESOURCE_DETAIL",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_RESOURCE_DETAIL", true));
			Map<Byte, ProjectStatus> projectStatus = new HashMap<Byte, ProjectStatus>();
			for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
				projectStatus.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PROJECT_STATUS",
					projectStatus);

			Map<Byte, Priority> priorities = new HashMap<Byte, Priority>();
			for (Priority e : EnumSet.allOf(Priority.class))
				priorities.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PROJECT_PRIORITY",
					priorities);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
					accountsBL.getCustomerBL().getAllCustomers(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getProjectReportJsonList() {
		try {

			ServletActionContext.getRequest().setAttribute("reportType",
					"employee");

			List<ProjectVO> projectList = this.fetchProjectReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", projectList.size());
			jsonResponse.put("iTotalDisplayRecords", projectList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (ProjectVO list : projectList) {
				array = new JSONArray();
				array.add(list.getProjectId());
				array.add(list.getReferenceNumber());
				array.add(list.getProjectTitle());
				array.add(list.getCustomerName());
				array.add(DateFormat.convertDateToString(list.getStartDate()
						.toString()));
				if (list.getEndDate() != null) {
					array.add(DateFormat.convertDateToString(list.getEndDate()
							.toString()));
				} else {
					array.add("");
				}
				array.add(list.getProjectType());
				array.add(list.getCoordinator());
				array.add(list.getCurrentStatus());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<ProjectVO> fetchProjectReportData() throws Exception {

		getImplementId();
		Long projectId = 0L;
		Long clientId = 0L;
		Long labourId = 0L;
		Long resourceId = 0L;
		Long leadId = 0L;
		Byte status = 0;
		Long projectType = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		if (ServletActionContext.getRequest().getParameter("projectId") != null) {
			projectId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("projectId"));
		}

		if (ServletActionContext.getRequest().getParameter("clientId") != null) {
			clientId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("clientId"));
		}

		if (ServletActionContext.getRequest().getParameter("labourId") != null) {
			labourId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("labourId"));
		}

		if (ServletActionContext.getRequest().getParameter("resourceId") != null) {
			resourceId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("resourceId"));
		}

		if (ServletActionContext.getRequest().getParameter("leadId") != null) {
			leadId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("leadId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null) {
			status = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (ServletActionContext.getRequest().getParameter("projectTypeId") != null) {
			projectType = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("projectTypeId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<Project> projects = projectManagmentEnterpriseService
				.getProjectService().getFilteredProjects(implementation,
						projectId, clientId, labourId, resourceId, leadId,
						status, projectType, fromDatee, toDatee);

		List<ProjectVO> projectVOs = new ArrayList<ProjectVO>();

		for (Project project : projects) {
			ProjectVO projectVO = new ProjectVO(project);

			projectVO.setCustomerName(null != project.getCustomer()
					.getCompany() ? project.getCustomer().getCompany()
					.getCompanyName() : project.getPersonByProjectLead()
					.getFirstName().concat(" ")
					.concat(project.getPersonByProjectLead().getLastName()));
			projectVO.setCoordinator(project.getPersonByProjectLead()
					.getFirstName().concat(" ")
					.concat(project.getPersonByProjectLead().getLastName()));
			if (project.getLookupDetail() != null)
				projectVO.setProjectType(project.getLookupDetail()
						.getDisplayName());
			else
				projectVO.setProjectType("");

			projectVO.setCurrentStatus(ProjectStatus.get(
					project.getProjectStatus()).name());
			if (project.getCreditTerm() != null) {
				projectVO.setPaymentTerm(project.getCreditTerm().getName());
			}

			/*
			 * List<ProjectResourceVO> projectResourceVOs = new
			 * ArrayList<ProjectResourceVO>();
			 * 
			 * List<ProjectTaskSurrogate> tempList=new
			 * ArrayList<ProjectTaskSurrogate
			 * >(project.getProjectTaskSurrogates());
			 * 
			 * for (ProjectResource resource :
			 * tempList.get(0).getProjectResources()) { ProjectResourceVO
			 * projectResourceVO = new ProjectResourceVO( resource);
			 * projectResourceVO.setResourceTypeName(ProjectResourceType.get(
			 * resource.getResourceType()).name());
			 * projectResourceVO.setTotal(resource.getUnitPrice()
			 * resource.getCount());
			 * 
			 * if (resource.getResourceType().equals(
			 * ProjectResourceType.Labour.getCode())) {
			 * projectResourceVO.setResourceDetail(""); for
			 * (ProjectResourceDetail detail : resource
			 * .getProjectResourceDetails()) {
			 * projectResourceVO.setResourceDetail(projectResourceVO
			 * .getResourceDetail() + detail.getPerson().getFirstName() + " " +
			 * detail.getPerson().getLastName() + ", "); } } else {
			 * projectResourceVO.setResourceDetail(resource
			 * .getLookupDetail().getDisplayName()); }
			 * 
			 * projectResourceVOs.add(projectResourceVO); }
			 * 
			 * projectVO.setProjectResourceVOs(projectResourceVOs);
			 */
			projectVOs.add(projectVO);
		}
		return projectVOs;
	}

	public String getProjectReportPrintOut() {
		try {

			List<ProjectVO> projectList = this.fetchProjectReportData();

			ServletActionContext.getRequest().setAttribute("PROJECTS",
					projectList);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProjectReportPDF() {
		String result = ERROR;
		try {

			projectDS = this.fetchProjectReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getProjectDeliverablesReportCriteriaData() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute(
					"PROJECT_TYPE",
					accountsBL.getLookupMasterBL().getActiveLookupDetails(
							"PROJECT_TYPE", true));
			Map<Byte, ProjectStatus> projectStatus = new HashMap<Byte, ProjectStatus>();
			for (ProjectStatus e : EnumSet.allOf(ProjectStatus.class))
				projectStatus.put(e.getCode(), e);
			ServletActionContext.getRequest().setAttribute("PROJECT_STATUS",
					projectStatus);
			ServletActionContext.getRequest().setAttribute("CUSTOMER_LIST",
					accountsBL.getCustomerBL().getAllCustomers(implementation));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String getProjectDeliverablesReportJsonList() {
		try {

			ServletActionContext.getRequest().setAttribute("reportType",
					"employee");

			List<ProjectDeliveryVO> projectDeliveryList = this
					.fetchProjectDeliverablesReportData();
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", projectDeliveryList.size());
			jsonResponse
					.put("iTotalDisplayRecords", projectDeliveryList.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (ProjectDeliveryVO list : projectDeliveryList) {
				array = new JSONArray();
				array.add(list.getProjectDeliveryId());
				array.add(list.getReferenceNumber());
				array.add(list.getDeliveryTitle());
				array.add(list.getDate());
				array.add(list.getProjectTitle());
				array.add(list.getMileStoneTitle());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<ProjectDeliveryVO> fetchProjectDeliverablesReportData()
			throws Exception {

		getImplementId();
		Long projectDeliverableId = 0L;
		Long clientId = 0L;
		Byte status = 0;
		Long projectType = 0L;
		Date fromDatee = null;
		Date toDatee = null;

		if (ServletActionContext.getRequest().getParameter("projectDeliveryId") != null
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"projectDeliveryId")) > 0) {
			projectDeliverableId = Long.parseLong(ServletActionContext
					.getRequest().getParameter("projectDeliveryId"));
		}

		if (ServletActionContext.getRequest().getParameter("clientId") != null
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"clientId")) > 0) {
			clientId = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("clientId"));
		}

		if (ServletActionContext.getRequest().getParameter("statusId") != null
				&& Byte.valueOf(ServletActionContext.getRequest().getParameter(
						"statusId")) > 0) {
			status = Byte.parseByte(ServletActionContext.getRequest()
					.getParameter("statusId"));
		}

		if (ServletActionContext.getRequest().getParameter("projectTypeId") != null
				&& Long.valueOf(ServletActionContext.getRequest().getParameter(
						"projectTypeId")) > 0) {
			projectType = Long.parseLong(ServletActionContext.getRequest()
					.getParameter("projectTypeId"));
		}

		if (fromDate != null && !fromDate.equals("null")
				&& !fromDate.equals("undefined")) {
			fromDatee = DateFormat.convertStringToDate(fromDate);
		}
		if (toDate != null && !toDate.equals("null")
				&& !toDate.equals("undefined")) {
			toDatee = DateFormat.convertStringToDate(toDate);
		}

		List<ProjectDelivery> projectDeliveries = projectManagmentEnterpriseService
				.getProjectDeliveryService().getFilteredProjectDelivarables(
						implementation, projectDeliverableId, clientId, status,
						projectType, fromDatee, toDatee);

		List<ProjectDeliveryVO> projectDeliveryVOs = new ArrayList<ProjectDeliveryVO>();

		for (ProjectDelivery delivery : projectDeliveries) {
			ProjectDeliveryVO projectDeliveryVO = new ProjectDeliveryVO(
					delivery);

			projectDeliveryVO.setDate(DateFormat.convertDateToString(delivery
					.getDeliveryDate().toString()));

			projectDeliveryVO.setCurrentStatus(ProjectDeliveryStatus.get(
					delivery.getDeliveryStatus()).name());

			if (projectDeliveries.size() > 1) {
				projectDeliveryVO.setViewType("multiple");
			} else {
				projectDeliveryVO.setViewType("single");
			}

			projectDeliveryVOs.add(projectDeliveryVO);
		}
		return projectDeliveryVOs;
	}

	public String getProjectDeliverablesReportPrintOut() {
		try {

			List<ProjectDeliveryVO> projectDeliveryList = this
					.fetchProjectDeliverablesReportData();

			ServletActionContext.getRequest().setAttribute(
					"PROJECTS_DELIVERABLES", projectDeliveryList);

			if (projectDeliveryList.size() > 1) {
				ServletActionContext.getRequest().setAttribute("VIEW_TYPE",
						"multiple");
			} else {
				ServletActionContext.getRequest().setAttribute("VIEW_TYPE",
						"single");
			}

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProjectDeliverablesReportPDF() {
		String result = ERROR;
		try {

			projectDeliveryDS = this.fetchProjectDeliverablesReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			result = SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getSalesReturnPrintout() {
		try {

			Credit credit = accountsBL.getCreditBL().getCreditService()
					.getCreditNoteInformationById(recordId);
			CreditNoteVO creditNoteVO = accountsBL.getCreditBL()
					.convertCreditToVO(credit);
			ServletActionContext.getRequest().setAttribute("SALES_RETURN",
					creditNoteVO);

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	public String getProjectInformationPrintOut() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			Project project = projectManagmentBL.getProjectBL()
					.getProjectService().getProjectById(projectId);
			ProjectVO projectVO = projectManagmentBL.getProjectBL()
					.convertProjectToVO(project);
			double projectExpense = 0;
			if (null != projectVO && null != projectVO.getProjectExpenseVOs()) {
				for (ProjectExpenseVO expenseVO : projectVO
						.getProjectExpenseVOs())
					projectExpense += expenseVO.getAmount();
			}
			ServletActionContext.getRequest().setAttribute("MILESONTES_INFO",
					projectVO.getProjectMileStoneVOs());

			ServletActionContext.getRequest().setAttribute(
					"PROJECT_EXPENSE_INFO", projectVO.getProjectExpenseVOs());

			ServletActionContext.getRequest().setAttribute("PROJECT_INFO",
					projectVO);
			List<ProjectTaskVO> projectTaskVOs = projectManagmentBL
					.getProjectTaskBL().manipulateTaskVOByProject(projectId);
			if (null != projectTaskVOs && projectTaskVOs.size() > 0) {
				for (ProjectTaskVO taskVO : projectTaskVOs) {
					if (null != taskVO.getProjectExpenseVOs()) {
						for (ProjectExpenseVO expenseVO : taskVO
								.getProjectExpenseVOs()) {
							projectExpense += expenseVO.getAmount();
						}
					}
				}
			}
			projectVO
					.setExpenseAmount(AIOSCommons.formatAmount(projectExpense));
			ServletActionContext.getRequest().setAttribute("TASK_EXPENSES",
					projectTaskVOs);
			session.setAttribute(
					"PROJECT_INFO_" + sessionObj.get("jSessionId"), projectVO);
			session.setAttribute(
					"TASK_EXPENSES_" + sessionObj.get("jSessionId"),
					projectTaskVOs);
			returnMessage = "SUCCESS";
			getProjectInformationPrintContent();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String getProjectInformationPrintContent() {
		try {
			getImplementId();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configuration config = new Configuration();
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ implementation.getImplementationId() + "/job"));

			Template template = config.getTemplate("job-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			ProjectVO projectVO = (ProjectVO) session
					.getAttribute("PROJECT_INFO_"
							+ sessionObj.get("jSessionId"));
			format = "";
			rootMap.put("customerName", projectVO.getCustomerName());
			rootMap.put("referenceNumber", projectVO.getReferenceNumber());
			rootMap.put("jobDate", projectVO.getFromDate());
			rootMap.put("totalExpense", projectVO.getExpenseAmount());

			rootMap.put("jobTitle", projectVO.getProjectTitle());
			rootMap.put("jobType", projectVO.getProjectType());
			rootMap.put("fromDate", projectVO.getFromDate());
			rootMap.put("endDate", projectVO.getToDate());
			rootMap.put("taskTitle", null);
			rootMap.put("accountant", projectVO.getProjectLead());
			rootMap.put(
					"florist",
					null != projectVO.getProjectDecorator() ? projectVO
							.getProjectDecorator() : "");
			rootMap.put(
					"helper",
					null != projectVO.getProjectHelper() ? projectVO
							.getProjectHelper() : "");
			rootMap.put(
					"salesPerson",
					null != projectVO.getSalesPerson() ? projectVO
							.getSalesPerson() : "");
			rootMap.put(
					"manager",
					null != projectVO.getProjectManager() ? projectVO
							.getProjectManager() : "");
			List<ProjectExpenseVO> projectExpenseVOs = null;
			if (!taskExpense) {
				if (null != projectVO.getProjectExpenseVOs()
						&& projectVO.getProjectExpenseVOs().size() > 0) {
					projectExpenseVOs = projectVO.getProjectExpenseVOs();
					Collections.sort(projectExpenseVOs,
							new Comparator<ProjectExpenseVO>() {
								public int compare(ProjectExpenseVO o1,
										ProjectExpenseVO o2) {
									return o1.getProjectExpenseId().compareTo(
											o2.getProjectExpenseId());
								}
							});
				}
				taskExpense = true;
				List<ProjectExpenseVO> tempProjectExpenseVOs = new ArrayList<ProjectExpenseVO>();
				for (ProjectExpenseVO projectExpenseVO : projectExpenseVOs) {
					if ((byte) projectExpenseVO.getExpenseMode() == (byte) ProjectExpenseMode.Estimated_Expense
							.getCode()) {
						tempProjectExpenseVOs.add(projectExpenseVO);
					}
				}
				rootMap.put("expenseList", tempProjectExpenseVOs);
			}
			if (null == projectExpenseVOs || projectExpenseVOs.size() == 0) {
				List<ProjectTaskVO> projectTaskVOs = (List<ProjectTaskVO>) session
						.getAttribute("TASK_EXPENSES_"
								+ sessionObj.get("jSessionId"));

				if (null != projectTaskVOs && projectTaskVOs.size() > 0) {
					Collections.sort(projectTaskVOs,
							new Comparator<ProjectTaskVO>() {
								public int compare(ProjectTaskVO o1,
										ProjectTaskVO o2) {
									return o1.getProjectTaskId().compareTo(
											o2.getProjectTaskId());
								}
							});
					ProjectTaskVO projectTaskVO = projectTaskVOs.get(0);
					if (null != projectTaskVO
							&& null != projectTaskVO.getProjectExpenseVOs()
							&& projectTaskVO.getProjectExpenseVOs().size() > 0) {
						projectExpenseVOs = projectTaskVO
								.getProjectExpenseVOs();
						Collections.sort(projectExpenseVOs,
								new Comparator<ProjectExpenseVO>() {
									public int compare(ProjectExpenseVO o1,
											ProjectExpenseVO o2) {
										return o1
												.getProjectExpenseId()
												.compareTo(
														o2.getProjectExpenseId());
									}
								});

						List<ProjectExpenseVO> tempProjectExpenseVOs = new ArrayList<ProjectExpenseVO>();
						for (ProjectExpenseVO projectExpenseVO : projectExpenseVOs) {
							if ((byte) projectExpenseVO.getExpenseMode() == (byte) ProjectExpenseMode.Estimated_Expense
									.getCode()) {
								tempProjectExpenseVOs.add(projectExpenseVO);
							}
						}
						rootMap.put("taskTitle", projectTaskVO.getTaskTitle());
						rootMap.put("fromDate", projectTaskVO.getFromDate());
						rootMap.put("endDate", projectTaskVO.getFromDate());
						rootMap.put("splJobTitle", projectVO.getProjectTitle());
						rootMap.put("expenseList", tempProjectExpenseVOs);
					}
					projectTaskVOs.remove(0);
					if (null != projectTaskVOs && projectTaskVOs.size() > 0) {
						session.setAttribute(
								"TASK_EXPENSES_" + sessionObj.get("jSessionId"),
								projectTaskVOs);
						taskExpense = true;
					} else {
						session.setAttribute(
								"TASK_EXPENSES_" + sessionObj.get("jSessionId"),
								null);
						taskExpense = false;
					}

				} else
					taskExpense = false;
			} else {
				taskExpense = false;
				List<ProjectTaskVO> projectTaskVOs = (List<ProjectTaskVO>) session
						.getAttribute("TASK_EXPENSES_"
								+ sessionObj.get("jSessionId"));
				if (null != projectTaskVOs && projectTaskVOs.size() > 0) {
					session.setAttribute(
							"TASK_EXPENSES_" + sessionObj.get("jSessionId"),
							projectTaskVOs);
					taskExpense = true;
				}
			}
			Writer out = new StringWriter();
			template.process(rootMap, out);
			format = out.toString();
			printableContent = format;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getProjectDeliveryCustomerPrint() {
		try {
			getImplementId();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configuration config = new Configuration();
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ implementation.getImplementationId() + "/job"));

			Template template = config.getTemplate("job-delivery.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();
			format = "";
			Project project = projectManagmentBL.getProjectBL()
					.getProjectService().getProjectById(projectId);
			ProjectVO projectVO = projectManagmentBL.getProjectBL()
					.convertProjectToVO(project);
			rootMap.put("customerName", projectVO.getCustomerName());
			rootMap.put("referenceNumber", projectVO.getReferenceNumber());
			rootMap.put("jobTitle", projectVO.getProjectTitle());
			rootMap.put("deliveryDate", projectVO.getStartDate());
			rootMap.put("invoiceAmount",
					AIOSCommons.formatAmount(projectVO.getProjectValue()));
			rootMap.put("orderDate", projectVO.getStartDate());
			rootMap.put("customerMobile", projectVO.getCustomerMobile());
			rootMap.put("customerAddress", projectVO.getCustomerAddress());
			rootMap.put("taskreferenceNumber", null);
			rootMap.put("taskTitle", null);
			Writer out = new StringWriter();
			template.process(rootMap, out);
			format = out.toString();
			printableContent = format;
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	// Project Task template print
	public String getProjectTaskPrintContent() {
		try {
			getImplementId();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configuration config = new Configuration();
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ implementation.getImplementationId() + "/job"));

			Template template = config.getTemplate("job-template.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();

			ProjectTask projectTask = projectManagmentBL.getProjectTaskBL()
					.getProjectTaskService().getProjectTaskById(projectTaskId);

			ProjectTaskVO projectTaskVO = projectManagmentBL.getProjectTaskBL()
					.convertProjectTaskToVO(projectTask, null);

			Project project = projectManagmentBL.getProjectBL()
					.getProjectService()
					.getProjectById(projectTaskVO.getProject().getProjectId());
			ProjectVO projectVO = projectManagmentBL.getProjectBL()
					.convertProjectToVO(project);

			format = "";
			rootMap.put("customerName", projectVO.getCustomerName());
			rootMap.put("referenceNumber", projectVO.getReferenceNumber());
			rootMap.put("jobDate", projectVO.getFromDate());
			rootMap.put("totalExpense", projectVO.getExpenseAmount());
			rootMap.put("taskreferenceNumber",
					projectTaskVO.getReferenceNumber());
			rootMap.put("jobTitle", projectVO.getProjectTitle());
			rootMap.put("jobType", projectVO.getProjectType());
			rootMap.put("fromDate", projectVO.getFromDate());
			rootMap.put("endDate", projectVO.getToDate());
			rootMap.put("taskTitle", projectTask.getTaskTitle());
			rootMap.put("accountant", projectVO.getProjectLead());
			rootMap.put(
					"florist",
					null != projectVO.getProjectDecorator() ? projectVO
							.getProjectDecorator() : "");
			rootMap.put(
					"helper",
					null != projectVO.getProjectHelper() ? projectVO
							.getProjectHelper() : "");
			rootMap.put(
					"salesPerson",
					null != projectVO.getSalesPerson() ? projectVO
							.getSalesPerson() : "");
			rootMap.put(
					"manager",
					null != projectVO.getProjectManager() ? projectVO
							.getProjectManager() : "");
			List<ProjectExpenseVO> tempProjectExpenseVOs = null;
			List<ProjectExpenseVO> projectExpenseVOs = null;
			double projectExpense = 0;
			if (null != projectTaskVO.getProjectExpenseVOs()
					&& projectTaskVO.getProjectExpenseVOs().size() > 0) {
				tempProjectExpenseVOs = new ArrayList<ProjectExpenseVO>();
				projectExpenseVOs = projectTaskVO.getProjectExpenseVOs();
				Collections.sort(projectExpenseVOs,
						new Comparator<ProjectExpenseVO>() {
							public int compare(ProjectExpenseVO o1,
									ProjectExpenseVO o2) {
								return o1.getProjectExpenseId().compareTo(
										o2.getProjectExpenseId());
							}
						});
				for (ProjectExpenseVO projectExpenseVO : projectExpenseVOs) {
					if ((byte) projectExpenseVO.getExpenseMode() == (byte) ProjectExpenseMode.Estimated_Expense
							.getCode()) {
						projectExpense += projectExpenseVO.getAmount();
						tempProjectExpenseVOs.add(projectExpenseVO);
					}
				}
			}
			rootMap.put("totalExpense", projectExpense);
			rootMap.put("expenseList", tempProjectExpenseVOs);
			Writer out = new StringWriter();
			template.process(rootMap, out);
			format = out.toString();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getProjectTaskDeliveryPrintContent() {
		try {
			getImplementId();
			final Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			Configuration config = new Configuration();
			config.setDirectoryForTemplateLoading(new File(confProps
					.getProperty("file.drive")
					+ "aios/PrintTemplate/templates/implementation_"
					+ implementation.getImplementationId() + "/job"));

			Template template = config.getTemplate("job-delivery.ftl");
			Map<String, Object> rootMap = new HashMap<String, Object>();
			format = "";
			ProjectTask projectTask = projectManagmentBL.getProjectTaskBL()
					.getProjectTaskService().getProjectTaskById(projectTaskId);

			ProjectTaskVO projectTaskVO = projectManagmentBL.getProjectTaskBL()
					.convertProjectTaskToVO(projectTask, null);

			Project project = projectManagmentBL.getProjectBL()
					.getProjectService()
					.getProjectById(projectTaskVO.getProject().getProjectId());
			ProjectVO projectVO = projectManagmentBL.getProjectBL()
					.convertProjectToVO(project);
			rootMap.put("customerName", projectVO.getCustomerName());
			rootMap.put("referenceNumber", projectVO.getReferenceNumber());
			rootMap.put("jobTitle", projectVO.getProjectTitle());
			rootMap.put("taskTitle", projectTask.getTaskTitle());
			rootMap.put("taskreferenceNumber",
					projectTaskVO.getReferenceNumber());
			rootMap.put("deliveryDate", projectVO.getStartDate());
			rootMap.put("invoiceAmount",
					AIOSCommons.formatAmount(projectVO.getProjectValue()));
			rootMap.put("orderDate", projectVO.getStartDate());
			rootMap.put("customerMobile", projectVO.getCustomerMobile());
			rootMap.put("customerAddress", projectVO.getCustomerAddress());
			Writer out = new StringWriter();
			template.process(rootMap, out);
			format = out.toString();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			return ERROR;
		}
	}

	// Show All Projects
	public String getProjectMileStoneData() {
		try {

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show All Projects
	public String getProjectMileStoneList() {
		try {
			aaData = projectManagmentBL.getProjectMileStoneBL()
					.showAllProjectMileStones();
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show All Projects
	public String getProjectMileStoneReportPrintout() {
		try {
			aaData = projectManagmentBL.getProjectMileStoneBL()
					.showAllProjectMileStones();

			ServletActionContext.getRequest().setAttribute(
					"PROJECT_MILESTONES", aaData);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// =============Getters & setters=============

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public AccountsBL getAccountsBL() {
		return accountsBL;
	}

	public void setAccountsBL(AccountsBL accountsBL) {
		this.accountsBL = accountsBL;
	}

	public Ledger getLedger() {
		return ledger;
	}

	public void setLedger(Ledger ledger) {
		this.ledger = ledger;
	}

	public List<Ledger> getLedgerList() {
		return ledgerList;
	}

	public void setLedgerList(List<Ledger> ledgerList) {
		this.ledgerList = ledgerList;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public List<AccountsTO> getAccountsList() {
		return accountsList;
	}

	public void setAccountsList(List<AccountsTO> accountsList) {
		this.accountsList = accountsList;
	}

	public long getLoanId() {
		return loanId;
	}

	public void setLoanId(long loanId) {
		this.loanId = loanId;
	}

	public String getApprovalFlag() {
		return approvalFlag;
	}

	public void setApprovalFlag(String approvalFlag) {
		this.approvalFlag = approvalFlag;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public List<LoanTO> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanTO> loanList) {
		this.loanList = loanList;
	}

	public List<Payment> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<Payment> paymentList) {
		this.paymentList = paymentList;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getCodeCombinationId() {
		return codeCombinationId;
	}

	public void setCodeCombinationId(long codeCombinationId) {
		this.codeCombinationId = codeCombinationId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public long getJournalId() {
		return journalId;
	}

	public void setJournalId(long journalId) {
		this.journalId = journalId;
	}

	public String getAccountList() {
		return accountList;
	}

	public void setAccountList(String accountList) {
		this.accountList = accountList;
	}

	public List<BankDepositVO> getBankDepositList() {
		return bankDepositList;
	}

	public void setBankDepositList(List<BankDepositVO> bankDepositList) {
		this.bankDepositList = bankDepositList;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(long periodId) {
		this.periodId = periodId;
	}

	public List<Calendar> getCalenderList() {
		return calenderList;
	}

	public void setCalenderList(List<Calendar> calenderList) {
		this.calenderList = calenderList;
	}

	public String getSelectedCalendarIds() {
		return selectedCalendarIds;
	}

	public void setSelectedCalendarIds(String selectedCalendarIds) {
		this.selectedCalendarIds = selectedCalendarIds;
	}

	public List<AccountsTO> getComulativeBalanceSheet() {
		return comulativeBalanceSheet;
	}

	public void setComulativeBalanceSheet(
			List<AccountsTO> comulativeBalanceSheet) {
		this.comulativeBalanceSheet = comulativeBalanceSheet;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public List<String> getItemTypes() {
		return itemTypes;
	}

	public void setItemTypes(List<String> itemTypes) {
		this.itemTypes = itemTypes;
	}

	public List<Product> getProductsList() {
		return productsList;
	}

	public void setProductsList(List<Product> productsList) {
		this.productsList = productsList;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<Period> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Period> periods) {
		this.periods = periods;
	}

	public String getSelectedPeriod() {
		return selectedPeriod;
	}

	public void setSelectedPeriod(String selectedPeriod) {
		this.selectedPeriod = selectedPeriod;
	}

	public String getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(String selectedType) {
		this.selectedType = selectedType;
	}

	public String getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public List<Object> getEntityObjects() {
		return entityObjects;
	}

	public void setEntityObjects(List<Object> entityObjects) {
		this.entityObjects = entityObjects;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getSelectedSupplier() {
		return selectedSupplier;
	}

	public void setSelectedSupplier(String selectedSupplier) {
		this.selectedSupplier = selectedSupplier;
	}

	public List<AccountsTO> getInventoryStatementDS() {
		return inventoryStatementDS;
	}

	public void setInventoryStatementDS(List<AccountsTO> inventoryStatementDS) {
		this.inventoryStatementDS = inventoryStatementDS;
	}

	public String getSelectedProductId() {
		return selectedProductId;
	}

	public void setSelectedProductId(String selectedProductId) {
		this.selectedProductId = selectedProductId;
	}

	public String getSelectedSupplierId() {
		return selectedSupplierId;
	}

	public void setSelectedSupplierId(String selectedSupplierId) {
		this.selectedSupplierId = selectedSupplierId;
	}

	public List<ProjectMileStoneVO> getProjectMileStoneDS() {
		return projectMileStoneDS;
	}

	public void setProjectMileStoneDS(
			List<ProjectMileStoneVO> projectMileStoneDS) {
		this.projectMileStoneDS = projectMileStoneDS;
	}

	public List<ProjectVO> getProjectDS() {
		return projectDS;
	}

	public void setProjectDS(List<ProjectVO> projectDS) {
		this.projectDS = projectDS;
	}

	public List<ProjectDeliveryVO> getProjectDeliveryDS() {
		return projectDeliveryDS;
	}

	public void setProjectDeliveryDS(List<ProjectDeliveryVO> projectDeliveryDS) {
		this.projectDeliveryDS = projectDeliveryDS;
	}

	public List<ProjectPaymentDetailVO> getProjectPaymentDS() {
		return projectPaymentDS;
	}

	public void setProjectPaymentDS(
			List<ProjectPaymentDetailVO> projectPaymentDS) {
		this.projectPaymentDS = projectPaymentDS;
	}

	public ProjectManagmentEnterpriseService getProjectManagmentEnterpriseService() {
		return projectManagmentEnterpriseService;
	}

	public void setProjectManagmentEnterpriseService(
			ProjectManagmentEnterpriseService projectManagmentEnterpriseService) {
		this.projectManagmentEnterpriseService = projectManagmentEnterpriseService;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public ProjectManagmentBL getProjectManagmentBL() {
		return projectManagmentBL;
	}

	public void setProjectManagmentBL(ProjectManagmentBL projectManagmentBL) {
		this.projectManagmentBL = projectManagmentBL;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public boolean isTaskExpense() {
		return taskExpense;
	}

	public void setTaskExpense(boolean taskExpense) {
		this.taskExpense = taskExpense;
	}

	public Long getProjectDeliveryId() {
		return projectDeliveryId;
	}

	public void setProjectDeliveryId(Long projectDeliveryId) {
		this.projectDeliveryId = projectDeliveryId;
	}

	public Long getProjectTaskId() {
		return projectTaskId;
	}

	public void setProjectTaskId(Long projectTaskId) {
		this.projectTaskId = projectTaskId;
	}

	public String getPrintableContent() {
		return printableContent;
	}

	public void setPrintableContent(String printableContent) {
		this.printableContent = printableContent;
	}
}
