package com.aiotech.aios.reporting.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.IdentityVO;
import com.aiotech.aios.hr.domain.entity.vo.PersonVO;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.vo.UserVO;
import com.aiotech.aios.system.service.SystemEnterpriseService;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.to.WorkflowDetailVO;
import com.aiotech.aios.workflow.to.WorkflowVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class SystemAction extends ActionSupport {

	SystemEnterpriseService systemEnterpriseService;
	WorkflowEnterpriseService workflowEnterpriseService;
	PersonService personService;

	SystemBL systemBL;

	private String selectedModuleProcessId;
	private String selectedWorkflowId;

	private List<Workflow> workflows;
	private List<Object> aaData;
	private InputStream fileInputStream;
	List<WorkflowVO> workflowsDS;

	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	private String returnMessage;
	private String dashboardContent;

	public String loadWorkflowUsersReportCriteria() {

		String result = SUCCESS;

		try {
			List<ModuleProcess> modules = workflowEnterpriseService
					.getGenerateNotificationsBL().getWorkflowService()
					.getAllModuleProcess();

			ServletActionContext.getRequest()
					.setAttribute("PROCESSES", modules);

		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}

		return result;
	}

	private List<Workflow> fetchProcessWorkflows() throws Exception {

		Long processId = null;

		if (selectedModuleProcessId != null && selectedModuleProcessId != "0") {
			processId = Long.parseLong(selectedModuleProcessId);
		}

		ModuleProcess mp = new ModuleProcess();
		mp.setProcessId(processId);

		return workflowEnterpriseService.getGenerateNotificationsBL()
				.getWorkflowService().getWorkflowsOfProcess(mp);
	}

	public String loadWorkflowsUsers() {

		String result = SUCCESS;

		aaData = new ArrayList<Object>();
		try {
			List<WorkflowVO> vos = fetchWorkflowUsersReportData();

			List<JSONObject> jsonResponses = new ArrayList<JSONObject>();

			for (WorkflowVO vo : vos) {

				JSONObject jsonResponse = new JSONObject();
				JSONArray data = new JSONArray();

				for (WorkflowDetailVO detailVO : vo.getDetailVOs()) {
					JSONArray array = new JSONArray();
					array.add(detailVO.getRoleName() + "");
					array.add(detailVO.getDisplayUsername() + "");
					array.add(detailVO.getPersonName() + "");
					array.add(detailVO.getDesignation() + "");
					array.add(detailVO.getOperationName() + "");

					data.add(array);
				}

				jsonResponse.put("aaData", data);

				jsonResponses.add(jsonResponse);

			}

			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponses);

		} catch (Exception e) {
			e.printStackTrace();
			result = ERROR;
		}

		return result;
	}

	private List<WorkflowVO> fetchWorkflowUsersReportData() throws Exception {

		List<WorkflowVO> vos = new ArrayList<WorkflowVO>();
		Long workflowId = 1L;

		if (selectedWorkflowId != null && selectedWorkflowId != "0") {
			workflowId = Long.parseLong(selectedWorkflowId);
		}

		List<Workflow> workflows = new ArrayList<Workflow>();

		workflows = fetchProcessWorkflows();

		for (Workflow workflow : workflows) {

			WorkflowVO wfVO = new WorkflowVO();
			List<WorkflowDetailVO> detailvos = new ArrayList<WorkflowDetailVO>();

			List<WorkflowDetail> details = workflowEnterpriseService
					.getGenerateNotificationsBL().getWorkflowService()
					.getWorkflowDetailsByWorkflowId(workflow.getWorkflowId());

			for (WorkflowDetail workflowDetail : details) {

				List<User> userRoles = systemBL.getUsersOfRole(workflowDetail
						.getRole().getRoleId(), new ArrayList<Long>());

				for (User user2 : userRoles) {

					WorkflowDetailVO wfVo = new WorkflowDetailVO();
					List<Person> persons = personService
							.getPersonByIdWithDesignation(user2.getPersonId());

					if (persons.size() > 0) {
						Person person = persons.get(0);
						wfVo.setPersonName(person.getFirstName() + " "
								+ person.getLastName());

						if (person.getJobAssignments().size() > 0) {
							wfVo.setDesignation(person.getJobAssignments()
									.iterator().next().getDesignation()
									.getDesignationName());
						} else {
							wfVo.setDesignation("N/A");
						}
					}

					wfVo.setDisplayUsername(user2.getUsername().split("_")[0]);

					String operation = "";

					if (workflowDetail.getOperation() == 1) {
						operation = "Approval";
					} else if (workflowDetail.getOperation() == 2) {
						operation = "Reject";
					} else if (workflowDetail.getOperation() == 3) {
						operation = "Entry";
					} else if (workflowDetail.getOperation() == 4) {
						operation = "Publish";
					} else if (workflowDetail.getOperation() == 5) {
						operation = "Publish Read";
					} else if (workflowDetail.getOperation() == 6) {
						operation = "Confirm Read";
					} else if (workflowDetail.getOperation() == 7) {
						operation = "Deny Read";
					} else if (workflowDetail.getOperation() == 8) {
						operation = "Approved";
					} else if (workflowDetail.getOperation() == 9) {
						operation = "Published";
					}

					wfVo.setOperationName(operation);
					wfVo.setRoleName(workflowDetail.getRole().getRoleName());

					detailvos.add(wfVo);

				}

			}

			wfVO.setDetailVOs(detailvos);

			vos.add(wfVO);
		}

		return vos;
	}

	public String generateWorkflowUsersReportPrintout() {

		String result = SUCCESS;

		try {

			ServletActionContext.getRequest().setAttribute("WORKFLOWS",
					fetchWorkflowUsersReportData());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public String generateWorkflowUsersReportPDF() {

		String result = SUCCESS;

		try {

			workflowsDS = fetchWorkflowUsersReportData();

			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");

			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;

	}

	public String generateWorkflowUsersReportXLS() {

		String str = "";
		try {

			List<WorkflowVO> vos = fetchWorkflowUsersReportData();
			str = "Workflow Users Report \n\n";

			int counter = 1;

			for (WorkflowVO workflowVO : vos) {

				str += "Workflow" + counter + "\n";

				str += "Role,Username,Person Name,Designation,Operation";
				str += "\n";
				for (WorkflowDetailVO detailVO : workflowVO.getDetailVOs()) {

					str += detailVO.getRoleName();
					str += "," + detailVO.getDisplayUsername();
					str += "," + detailVO.getPersonName();
					str += "," + detailVO.getDesignation();
					str += "," + detailVO.getOperationName() + "\n";

				}

				str += "\n";

				counter++;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		InputStream is = new ByteArrayInputStream(str.getBytes());
		fileInputStream = is;

		return SUCCESS;

	}

	public String saveDashboardPosition() {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		try {
			user.setDashboardAccess(dashboardContent);
			systemBL.getSystemService().saveOrUpdateUser(user);
			ActionContext.getContext().getSession().put("USER", user);
			returnMessage = "SUCCESS";
		} catch (Exception e) {
			returnMessage = "ERROR";
		}
		return SUCCESS;
	}

	public SystemEnterpriseService getSystemEnterpriseService() {
		return systemEnterpriseService;
	}

	public void setSystemEnterpriseService(
			SystemEnterpriseService systemEnterpriseService) {
		this.systemEnterpriseService = systemEnterpriseService;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public String getSelectedModuleProcessId() {
		return selectedModuleProcessId;
	}

	public void setSelectedModuleProcessId(String selectedModuleProcessId) {
		this.selectedModuleProcessId = selectedModuleProcessId;
	}

	public List<Workflow> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<Workflow> workflows) {
		this.workflows = workflows;
	}

	public List<Object> getAaData() {
		return aaData;
	}

	public void setAaData(List<Object> aaData) {
		this.aaData = aaData;
	}

	public String getSelectedWorkflowId() {
		return selectedWorkflowId;
	}

	public void setSelectedWorkflowId(String selectedWorkflowId) {
		this.selectedWorkflowId = selectedWorkflowId;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public List<WorkflowVO> getWorkflowsDS() {
		return workflowsDS;
	}

	public void setWorkflowsDS(List<WorkflowVO> workflowsDS) {
		this.workflowsDS = workflowsDS;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public String getDashboardContent() {
		return dashboardContent;
	}

	public void setDashboardContent(String dashboardContent) {
		this.dashboardContent = dashboardContent;
	}

}
