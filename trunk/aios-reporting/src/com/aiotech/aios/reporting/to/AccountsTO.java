package com.aiotech.aios.reporting.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountsTO implements Serializable{ 
	private static final long serialVersionUID = 1L;  
	
	private String assetAccountCode;
	private String assetCode;
	private String assetAnalysisCode;
	private String liabilityAccountCode;
	private String liabilityAnalysisCode;
	private String liabilityCode;
	private String assetBalanceAmount;
	private String liabilityBalanceAmount;
	private String expenseBalanceAmount;
	private String revenueBalanceAmount;
	private String ownersEquityBalanceAmount;
	private String expenseAccountCode; 
	private String expenseCode;
	private String revenueAccountCode; 
	private String revenueCode;
	private Integer accountTypeId;
	private String assetTotal;
	private String liabilityTotal;
	private String expenseTotal;
	private String revenueTotal;
	private String linkLabel;
	private String anchorExpression;
	private String retainedEarningsAccount;
	private String retainedEarningsCode;
	private String retainedEarningsBalance;
	private String netLabel;
	private String netAmount;
	private String accountCode;
	private String accountDescription;
	private String debitBalance;
	private String creditBalance;
	private String debitBalanceTotal;
	private String creditBalanceTotal;
	private String amount;
	private Boolean ledgerSide;
	private Long combinationId; 
	private long categoryId;
	private long codeCombinationId;
	private String fromDate;
	private String toDate;
	private String lineDescription; 
	private String transactionFlag;
	private String periodName;
	private String currencyCode;
	private String categoryName;
	private long costCenterAccountId;
	private Long naturalAccountId;
	private String debitAmount;
	private String creditAmount;
	private Double creditAmountDouble;
	private Double debitAmountDouble;
	private String voucherNumber;
	private String description;
	private String exportLabel;
	private String exportAnchorExpression;
	private String fiscalYear;
	private String viewDebitBalance;
	private String viewCreditBalance;
	private String viewClosingBalance;
 	private long recordId; 
 	private long accountSubType;
 	private String accountSubTypeStr;
 	private List<AccountsTO> assetList=new ArrayList<AccountsTO>();
	private List<AccountsTO> liabilityList=new ArrayList<AccountsTO>();  
	private List<AccountsTO> expenseList=new ArrayList<AccountsTO>();
	private List<AccountsTO> revenueList=new ArrayList<AccountsTO>(); 
	private List<AccountsTO> debitCreditList=new ArrayList<AccountsTO>(); 
	private List<AccountsTO> ledgerStatement=new ArrayList<AccountsTO>(); 
	private List<AccountsTO> debitListBalance=new ArrayList<AccountsTO>();
	private List<AccountsTO> creditListBalance=new ArrayList<AccountsTO>();
	private List<AccountsTO> descriptionList=new ArrayList<AccountsTO>();
	private List<AccountsTO> journalStatement=new ArrayList<AccountsTO>();
	private List<AccountsTO> transactionDetail=new ArrayList<AccountsTO>();
	private List<AccountsTO> assetGroupList=new ArrayList<AccountsTO>();
	private List<AccountsTO> liabilityGroupList=new ArrayList<AccountsTO>();
	private List<AccountsTO> expenseGroupList=new ArrayList<AccountsTO>();
	private List<AccountsTO> revenueGroupList=new ArrayList<AccountsTO>();
	private List<AccountsTO> debitCreditGroupList=new ArrayList<AccountsTO>();
	private Map<String, List<AccountsTO>> accountsMap = new HashMap<String, List<AccountsTO>>();
	private Map<String, List<AccountsTO>> accountGroupMap = new HashMap<String, List<AccountsTO>>();
	private List<String> selectedYears = new ArrayList<String>();
	private List<String> yearsAssetTotal = new ArrayList<String>();
	private List<String> yearsLiabilitiesTotal = new ArrayList<String>();
	
	public List<AccountsTO> getAssetList() {
		return assetList;
	}
	public void setAssetList(List<AccountsTO> assetList) {
		this.assetList = assetList;
	} 
	public List<AccountsTO> getLiabilityList() {
		return liabilityList;
	}
	public void setLiabilityList(List<AccountsTO> liabilityList) {
		this.liabilityList = liabilityList;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	 
	public String getLiabilityBalanceAmount() {
		return liabilityBalanceAmount;
	}
	public void setLiabilityBalanceAmount(String liabilityBalanceAmount) {
		this.liabilityBalanceAmount = liabilityBalanceAmount;
	} 
	public String getAssetBalanceAmount() {
		return assetBalanceAmount;
	}
	public void setAssetBalanceAmount(String assetBalanceAmount) {
		this.assetBalanceAmount = assetBalanceAmount;
	}
	public String getAssetAccountCode() {
		return assetAccountCode;
	}
	public void setAssetAccountCode(String assetAccountCode) {
		this.assetAccountCode = assetAccountCode;
	}
	public String getLiabilityAccountCode() {
		return liabilityAccountCode;
	}
	public void setLiabilityAccountCode(String liabilityAccountCode) {
		this.liabilityAccountCode = liabilityAccountCode;
	}
	public String getAssetCode() {
		return assetCode;
	}
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}
	public String getLiabilityCode() {
		return liabilityCode;
	}
	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}
	public String getAssetAnalysisCode() {
		return assetAnalysisCode;
	}
	public void setAssetAnalysisCode(String assetAnalysisCode) {
		this.assetAnalysisCode = assetAnalysisCode;
	}
	public String getLiabilityAnalysisCode() {
		return liabilityAnalysisCode;
	}
	public void setLiabilityAnalysisCode(String liabilityAnalysisCode) {
		this.liabilityAnalysisCode = liabilityAnalysisCode;
	}
	public void setLiabilityTotal(String liabilityTotal) {
		this.liabilityTotal = liabilityTotal;
	}
	public String getLiabilityTotal() {
		return liabilityTotal;
	}
	public String getAssetTotal() {
		return assetTotal;
	}
	public void setAssetTotal(String assetTotal) {
		this.assetTotal = assetTotal;
	}
	public String getExpenseBalanceAmount() {
		return expenseBalanceAmount;
	}
	public void setExpenseBalanceAmount(String expenseBalanceAmount) {
		this.expenseBalanceAmount = expenseBalanceAmount;
	}
	public String getRevenueBalanceAmount() {
		return revenueBalanceAmount;
	}
	public void setRevenueBalanceAmount(String revenueBalanceAmount) {
		this.revenueBalanceAmount = revenueBalanceAmount;
	}
	public Integer getAccountTypeId() {
		return accountTypeId;
	}
	public void setAccountTypeId(Integer accountTypeId) {
		this.accountTypeId = accountTypeId;
	}
	public String getRetainedEarningsAccount() {
		return retainedEarningsAccount;
	}
	public void setRetainedEarningsAccount(String retainedEarningsAccount) {
		this.retainedEarningsAccount = retainedEarningsAccount;
	}
	public String getRetainedEarningsCode() {
		return retainedEarningsCode;
	}
	public void setRetainedEarningsCode(String retainedEarningsCode) {
		this.retainedEarningsCode = retainedEarningsCode;
	}
	public String getRetainedEarningsBalance() {
		return retainedEarningsBalance;
	}
	public void setRetainedEarningsBalance(String retainedEarningsBalance) {
		this.retainedEarningsBalance = retainedEarningsBalance;
	}
	public String getExpenseAccountCode() {
		return expenseAccountCode;
	}
	public void setExpenseAccountCode(String expenseAccountCode) {
		this.expenseAccountCode = expenseAccountCode;
	} 
	public String getExpenseCode() {
		return expenseCode;
	}
	public void setExpenseCode(String expenseCode) {
		this.expenseCode = expenseCode;
	}
	public String getRevenueAccountCode() {
		return revenueAccountCode;
	}
	public void setRevenueAccountCode(String revenueAccountCode) {
		this.revenueAccountCode = revenueAccountCode;
	}
	public String getRevenueCode() {
		return revenueCode;
	}
	public void setRevenueCode(String revenueCode) {
		this.revenueCode = revenueCode;
	}
	public List<AccountsTO> getExpenseList() {
		return expenseList;
	}
	public void setExpenseList(List<AccountsTO> expenseList) {
		this.expenseList = expenseList;
	}
	public List<AccountsTO> getRevenueList() {
		return revenueList;
	}
	public void setRevenueList(List<AccountsTO> revenueList) {
		this.revenueList = revenueList;
	}
	public String getExpenseTotal() {
		return expenseTotal;
	}
	public void setExpenseTotal(String expenseTotal) {
		this.expenseTotal = expenseTotal;
	}
	public String getRevenueTotal() {
		return revenueTotal;
	}
	public void setRevenueTotal(String revenueTotal) {
		this.revenueTotal = revenueTotal;
	}
	public String getNetLabel() {
		return netLabel;
	}
	public void setNetLabel(String netLabel) {
		this.netLabel = netLabel;
	}
	public String getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	public String getDebitBalance() {
		return debitBalance;
	}
	public void setDebitBalance(String debitBalance) {
		this.debitBalance = debitBalance;
	}
	public String getCreditBalance() {
		return creditBalance;
	}
	public void setCreditBalance(String creditBalance) {
		this.creditBalance = creditBalance;
	}
	public String getDebitBalanceTotal() {
		return debitBalanceTotal;
	}
	public void setDebitBalanceTotal(String debitBalanceTotal) {
		this.debitBalanceTotal = debitBalanceTotal;
	}
	public String getCreditBalanceTotal() {
		return creditBalanceTotal;
	}
	public void setCreditBalanceTotal(String creditBalanceTotal) {
		this.creditBalanceTotal = creditBalanceTotal;
	}
	public List<AccountsTO> getDebitCreditList() {
		return debitCreditList;
	}
	public void setDebitCreditList(List<AccountsTO> debitCreditList) {
		this.debitCreditList = debitCreditList;
	}
	public Boolean getLedgerSide() {
		return ledgerSide;
	}
	public void setLedgerSide(Boolean ledgerSide) {
		this.ledgerSide = ledgerSide;
	}
	public Long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public List<AccountsTO> getDebitListBalance() {
		return debitListBalance;
	}
	public void setDebitListBalance(List<AccountsTO> debitListBalance) {
		this.debitListBalance = debitListBalance;
	}
	public List<AccountsTO> getCreditListBalance() {
		return creditListBalance;
	}
	public void setCreditListBalance(List<AccountsTO> creditListBalance) {
		this.creditListBalance = creditListBalance;
	}
	public List<AccountsTO> getLedgerStatement() {
		return ledgerStatement;
	}
	public void setLedgerStatement(List<AccountsTO> ledgerStatement) {
		this.ledgerStatement = ledgerStatement;
	} 
	public String getLineDescription() {
		return lineDescription;
	}
	public void setLineDescription(String lineDescription) {
		this.lineDescription = lineDescription;
	}
	public List<AccountsTO> getDescriptionList() {
		return descriptionList;
	}
	public void setDescriptionList(List<AccountsTO> descriptionList) {
		this.descriptionList = descriptionList;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getCodeCombinationId() {
		return codeCombinationId;
	}
	public void setCodeCombinationId(long codeCombinationId) {
		this.codeCombinationId = codeCombinationId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public List<AccountsTO> getJournalStatement() {
		return journalStatement;
	}
	public void setJournalStatement(List<AccountsTO> journalStatement) {
		this.journalStatement = journalStatement;
	}
	public String getTransactionFlag() {
		return transactionFlag;
	}
	public void setTransactionFlag(String transactionFlag) {
		this.transactionFlag = transactionFlag;
	}
	public String getPeriodName() {
		return periodName;
	}
	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public List<AccountsTO> getTransactionDetail() {
		return transactionDetail;
	}
	public void setTransactionDetail(List<AccountsTO> transactionDetail) {
		this.transactionDetail = transactionDetail;
	}
	public long getNaturalAccountId() {
		return naturalAccountId;
	}
	public void setNaturalAccountId(long naturalAccountId) {
		this.naturalAccountId = naturalAccountId;
	}
	public String getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}
	public String getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getExportLabel() {
		return exportLabel;
	}
	public void setExportLabel(String exportLabel) {
		this.exportLabel = exportLabel;
	}
	public String getExportAnchorExpression() {
		return exportAnchorExpression;
	}
	public void setExportAnchorExpression(String exportAnchorExpression) {
		this.exportAnchorExpression = exportAnchorExpression;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getCostCenterAccountId() {
		return costCenterAccountId;
	}
	public void setCostCenterAccountId(Long costCenterAccountId) {
		this.costCenterAccountId = costCenterAccountId;
	}
	public long getRecordId() {
		return recordId;
	}
	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public List<String> getSelectedYears() {
		return selectedYears;
	}
	public void setSelectedYears(List<String> selectedYears) {
		this.selectedYears = selectedYears;
	}
	public List<String> getYearsAssetTotal() {
		return yearsAssetTotal;
	}
	public void setYearsAssetTotal(List<String> yearsAssetTotal) {
		this.yearsAssetTotal = yearsAssetTotal;
	}
	public List<String> getYearsLiabilitiesTotal() {
		return yearsLiabilitiesTotal;
	}
	public void setYearsLiabilitiesTotal(List<String> yearsLiabilitiesTotal) {
		this.yearsLiabilitiesTotal = yearsLiabilitiesTotal;
	}
	public Double getCreditAmountDouble() {
		return creditAmountDouble;
	}
	public void setCreditAmountDouble(Double creditAmountDouble) {
		this.creditAmountDouble = creditAmountDouble;
	}
	public Double getDebitAmountDouble() {
		return debitAmountDouble;
	}
	public void setDebitAmountDouble(Double debitAmountDouble) {
		this.debitAmountDouble = debitAmountDouble;
	}
	public String getOwnersEquityBalanceAmount() {
		return ownersEquityBalanceAmount;
	}
	public void setOwnersEquityBalanceAmount(String ownersEquityBalanceAmount) {
		this.ownersEquityBalanceAmount = ownersEquityBalanceAmount;
	}
	public String getViewDebitBalance() {
		return viewDebitBalance;
	}
	public void setViewDebitBalance(String viewDebitBalance) {
		this.viewDebitBalance = viewDebitBalance;
	}
	public String getViewCreditBalance() {
		return viewCreditBalance;
	}
	public void setViewCreditBalance(String viewCreditBalance) {
		this.viewCreditBalance = viewCreditBalance;
	}
	public void setCostCenterAccountId(long costCenterAccountId) {
		this.costCenterAccountId = costCenterAccountId;
	}
	public void setNaturalAccountId(Long naturalAccountId) {
		this.naturalAccountId = naturalAccountId;
	}
	public String getViewClosingBalance() {
		return viewClosingBalance;
	}
	public void setViewClosingBalance(String viewClosingBalance) {
		this.viewClosingBalance = viewClosingBalance;
	}
	public long getAccountSubType() {
		return accountSubType;
	}
	public void setAccountSubType(long accountSubType) {
		this.accountSubType = accountSubType;
	} 
	public Map<String, List<AccountsTO>> getAccountsMap() {
		return accountsMap;
	}
	public void setAccountsMap(Map<String, List<AccountsTO>> accountsMap) {
		this.accountsMap = accountsMap;
	}
	public List<AccountsTO> getAssetGroupList() {
		return assetGroupList;
	}
	public void setAssetGroupList(List<AccountsTO> assetGroupList) {
		this.assetGroupList = assetGroupList;
	}
	public List<AccountsTO> getLiabilityGroupList() {
		return liabilityGroupList;
	}
	public void setLiabilityGroupList(List<AccountsTO> liabilityGroupList) {
		this.liabilityGroupList = liabilityGroupList;
	}
	public List<AccountsTO> getExpenseGroupList() {
		return expenseGroupList;
	}
	public void setExpenseGroupList(List<AccountsTO> expenseGroupList) {
		this.expenseGroupList = expenseGroupList;
	}
	public List<AccountsTO> getRevenueGroupList() {
		return revenueGroupList;
	}
	public void setRevenueGroupList(List<AccountsTO> revenueGroupList) {
		this.revenueGroupList = revenueGroupList;
	}
	public String getAccountSubTypeStr() {
		return accountSubTypeStr;
	}
	public void setAccountSubTypeStr(String accountSubTypeStr) {
		this.accountSubTypeStr = accountSubTypeStr;
	}
	public List<AccountsTO> getDebitCreditGroupList() {
		return debitCreditGroupList;
	}
	public void setDebitCreditGroupList(List<AccountsTO> debitCreditGroupList) {
		this.debitCreditGroupList = debitCreditGroupList;
	}
	public Map<String, List<AccountsTO>> getAccountGroupMap() {
		return accountGroupMap;
	}
	public void setAccountGroupMap(Map<String, List<AccountsTO>> accountGroupMap) {
		this.accountGroupMap = accountGroupMap;
	}
}
