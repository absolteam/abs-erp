package com.aiotech.aios.workflow.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.bl.GenerateNotificationsBL;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.service.bl.WorkFlowBL;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class WorkFlowAction extends ActionSupport {
	private static final Logger LOGGER = LogManager
			.getLogger(WorkFlowAction.class);
	private WorkFlowBL workFlowBL;
	private SystemService systemService;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private long recordId;
	private Long workflowDetailId;
	private Byte processFlag;
	private int finalyzed;
	private String useCase;
	Implementation implementation;
	private Integer id;
	private Long workflowId;
	private Long moduleProcessId;
	private String workflowTitle;
	private String description;
	private String workflowLineDetail;
	private String returnMessage;

	private String screenPath;
	private boolean isPopupApproval;
	private WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO;
	Integer operationFlagInteger;

	private CommentBL commentBL;
	private String comment;
	private String approveReadContents;
	private String messageId;
	private WorkflowVO returnVO;

	private GenerateNotificationsBL generateNotificationsBL;

	public String execute() {
		return SUCCESS;
	}

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
			// implementationId=implementation.getImplementationId();
			// LOGGER.info("implementation id------>"+implementationId);
		}
	}

	public String workflowNotificationList() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			User user = new User();
			Byte flag = null;
			session.removeAttribute("WORKFLOW_LIST_"
					+ sessionObj.get("jSessionId") + "_" + processFlag);
			/*
			 * session.removeAttribute("WORKFLOW_RECORD_ID_"+sessionObj.get(
			 * "jSessionId"));
			 * session.removeAttribute("WORKFLOW_DETAIL_ID_"+sessionObj
			 * .get("jSessionId"));
			 */
			user = (User) sessionObj.get("USER");
			List<WorkflowDetailVO> workflowDetails = null;
			getImplementId();
			// Fetch New records
			if (WorkflowConstants.Status.NoDecession.getCode() == processFlag) {

				List<Byte> operations = new ArrayList<Byte>();
				operations.add((byte) WorkflowConstants.Status.NoDecession
						.getCode());
				operations.add((byte) WorkflowConstants.Status.Saved.getCode());
				operations.add((byte) WorkflowConstants.Status.Publish
						.getCode());
				operations.add((byte) WorkflowConstants.Status.Post.getCode());
				/*
				 * workflowDetails =
				 * workFlowBL.getNotificationList(user,flag,implementation);
				 */
				workflowDetails = workflowEnterpriseService.getNotifications(
						operations, false, user);
			}
			// Fetch approved records
			if (WorkflowConstants.Status.Approved.getCode() == processFlag) {

				List<Byte> operations = new ArrayList<Byte>();
				operations.add((byte) WorkflowConstants.Status.Approved
						.getCode());
				operations.add((byte) WorkflowConstants.Status.Deny.getCode());
				operations.add((byte) WorkflowConstants.Status.Published
						.getCode());
				operations.add((byte) WorkflowConstants.Status.Confirm
						.getCode());
				operations.add((byte) WorkflowConstants.Status.Deleted
						.getCode());
				operations.add((byte) WorkflowConstants.Status.DeleteApproval
						.getCode());
				operations
						.add((byte) WorkflowConstants.Status.Posted.getCode());
				workflowDetails = workflowEnterpriseService.getNotifications(
						operations, false, user);
			}

			ServletActionContext.getRequest().setAttribute("workflowList",
					workflowDetails);
			ServletActionContext.getRequest().setAttribute("processFlag",
					processFlag);
			session.setAttribute(
					"WORKFLOW_LIST_" + sessionObj.get("jSessionId") + "_"
							+ processFlag, workflowDetails);
			// session.setAttribute("WORKFLOW_LIST_REVERSE_"+sessionObj.get("jSessionId"),
			// wfReverseNotification);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String approveMethod() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("PROCESSFLAG_"
					+ sessionObj.get("jSessionId"));
			User user = (User) sessionObj.get("USER");
			getImplementId();
			WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO = null;

			objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(Long
					.parseLong(messageId));
			if (processFlag == WorkflowConstants.Status.Approved.getCode()) {

				recordId = (Long) session.getAttribute("WORKFLOW_RECORD_ID_"
						+ sessionObj.get("jSessionId"));
				workflowDetailId = (Long) session
						.getAttribute("WORKFLOW_DETAIL_ID_"
								+ sessionObj.get("jSessionId"));

				/*
				 * performApproval(objSeletectedNotificationWorkflowDetailVO,
				 * recordId, workflowDetailId, implementation, processFlag,
				 * null, user);
				 */

				session.removeAttribute("WORKFLOW_RECORD_ID_"
						+ sessionObj.get("jSessionId"));
				session.removeAttribute("WORKFLOW_DETAIL_ID_"
						+ sessionObj.get("jSessionId"));

			} else if (processFlag == WorkflowConstants.Status.Deny.getCode()
					&& finalyzed == 1) {

				recordId = (Long) session.getAttribute("WORKFLOW_RECORD_ID_"
						+ sessionObj.get("jSessionId"));
				workflowDetailId = (Long) session
						.getAttribute("WORKFLOW_DETAIL_ID_"
								+ sessionObj.get("jSessionId"));

				/*
				 * performApproval(objSeletectedNotificationWorkflowDetailVO,
				 * recordId, workflowDetailId, implementation, processFlag,
				 * null, user);
				 */

				session.removeAttribute("WORKFLOW_RECORD_ID_"
						+ sessionObj.get("jSessionId"));
				session.removeAttribute("WORKFLOW_DETAIL_ID_"
						+ sessionObj.get("jSessionId"));

			} else if (processFlag == WorkflowConstants.Status.Posted.getCode()) {

				/*
				 * WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO =
				 * getWorkflowDetailVObyMessageId(Long .parseLong(messageId));
				 */

				recordId = (Long) session.getAttribute("WORKFLOW_RECORD_ID_"
						+ sessionObj.get("jSessionId"));
				workflowDetailId = (Long) session
						.getAttribute("WORKFLOW_DETAIL_ID_"
								+ sessionObj.get("jSessionId"));

			}
			List<WorkflowDetailVO> volist = new ArrayList<WorkflowDetailVO>();
			volist.add(objSeletectedNotificationWorkflowDetailVO);
			returnVO = new WorkflowVO();
			returnVO = workflowEnterpriseService
					.handleUserActionAgainstNotification(workflowDetailId,
							recordId, processFlag, volist, null, user);
			ServletActionContext.getRequest().setAttribute("PROCESS_FLAG",
					processFlag);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String markAsRead() {
		try {
			if (messageId != null) {

				objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(Long
						.parseLong(messageId));

				recordId = objSeletectedNotificationWorkflowDetailVO
						.getRecordId();

				HttpSession session = ServletActionContext.getRequest()
						.getSession();
				Map<String, Object> sessionObj = ActionContext.getContext()
						.getSession();

				User user = (User) sessionObj.get("USER");

				workflowEnterpriseService.updateNotificationStatus(
						objSeletectedNotificationWorkflowDetailVO, recordId,
						(byte) 1, user);

				JSONObject jsonList = new JSONObject();
				ServletActionContext.getRequest().setAttribute("jsonlist",
						jsonList);

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String saveWfRejectComment() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");

			long recId = (Long) session.getAttribute("WORKFLOW_RECORD_ID_"
					+ sessionObj.get("jSessionId"));
			String tableName = (String) session
					.getAttribute("WORKFLOW_USECASE_"
							+ sessionObj.get("jSessionId"));

			workflowDetailId = (Long) session
					.getAttribute("WORKFLOW_DETAIL_ID_"
							+ sessionObj.get("jSessionId"));

			WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(Long
					.parseLong(messageId));

			Comment commentIfo = new Comment();
			commentIfo.setComment(comment);
			commentIfo.setCreatedDate(new Date());

			commentIfo.setPersonId(user.getPersonId());

			commentIfo.setRecordId(recId);
			commentIfo.setUseCase(tableName);

			if (recId != 0 && tableName != null && !tableName.equals("")) {
				List<WorkflowDetailVO> volist = new ArrayList<WorkflowDetailVO>();
				volist.add(objSeletectedNotificationWorkflowDetailVO);

				workflowEnterpriseService.handleUserActionAgainstNotification(
						workflowDetailId, recordId, processFlag, volist,
						commentIfo, user);
				/*
				 * performApproval(objSeletectedNotificationWorkflowDetailVO,
				 * recId, workflowDetailId, implementation, processFlag,
				 * commentIfo, user);
				 */

			}

			JSONObject jsonList = new JSONObject();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * This method carry out necessary operations when a user perform an action
	 * on dashboard notification
	 * 
	 * 
	 * @param WorkflowDetailVO
	 *            : Object containing data of notification in which user have
	 *            performed operation
	 * 
	 * @param recordId
	 *            : primary key id of record on which operation is performed
	 * 
	 * @param workflowDetailId
	 *            : id of workflow of a process
	 * 
	 * @param approveflag
	 *            : operation that have been performed by the user
	 * 
	 * @param Comment
	 *            : in case of reject operation, the comment added by user
	 * 
	 * @author Khawaja M. Hasham
	 */
	/*
	 * public void performApproval( WorkflowDetailVO
	 * objSeletectedNotificationWorkflowDetailVO, Long recordId, long
	 * workflowDetailId, Implementation implementation, Byte approveflag,
	 * Comment comment, User user) throws Exception {
	 * 
	 * List<WorkflowDetailVO> volist = new ArrayList<WorkflowDetailVO>(); // if
	 * operation is performed on reverse flow notifications if
	 * (objSeletectedNotificationWorkflowDetailVO
	 * .getWorkflowDetailProcess().getWorkflowDetail() != null) {
	 * 
	 * recordId = objSeletectedNotificationWorkflowDetailVO.getRecordId();
	 * 
	 * // Update the status in message table that operation have been //
	 * performed so that notification will not be shown again
	 * workflowEnterpriseService.updateNotificationStatus(
	 * objSeletectedNotificationWorkflowDetailVO, recordId, (byte) 1, user);
	 * 
	 * // get the parent workflow (the workflow of which current reverse // flow
	 * belongs to) WorkflowDetail parentWorkflowDetail =
	 * objSeletectedNotificationWorkflowDetailVO
	 * .getWorkflowDetailProcess().getWorkflowDetail();
	 * workflowEnterpriseService . getGenerateNotificationsBL () .
	 * getWorkflowService () . getWorkflowDetail (
	 * objSeletectedNotificationWorkflowDetailVO . getParentId ());
	 * 
	 * 
	 * // get the previous workflow for which notification is to be // generated
	 * List<WorkflowDetail> previousWorkflows = workflowEnterpriseService
	 * .getGenerateNotificationsBL() .getWorkflowService()
	 * .findPreviousWorkflow(
	 * parentWorkflowDetail.getWorkflow().getWorkflowId(),
	 * parentWorkflowDetail.getOperationOrder()); if (previousWorkflows.size() >
	 * 0) { parentWorkflowDetail = previousWorkflows.get(0); }
	 * 
	 * // fill notification details objSeletectedNotificationWorkflowDetailVO
	 * .setWorkflow(parentWorkflowDetail.getWorkflow());
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setScreenId(parentWorkflowDetail.getScreenByScreenId() .getScreenId());
	 * 
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setWorkflowDetailId(parentWorkflowDetail .getWorkflowDetailId());
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setOperation(parentWorkflowDetail.getOperation());
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setOperationOrder(parentWorkflowDetail.getOperationOrder());
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setParentId(parentWorkflowDetail.getWorkflowDetail()
	 * .getWorkflowDetailId());
	 * 
	 * objSeletectedNotificationWorkflowDetailVO
	 * .setProcessFlag(parentWorkflowDetail.getOperation());
	 * 
	 * volist.add(objSeletectedNotificationWorkflowDetailVO);
	 * 
	 * } else { volist.add(objSeletectedNotificationWorkflowDetailVO);
	 * 
	 * } // volist.add(objSeletectedNotificationWorkflowDetailVO); recordId =
	 * objSeletectedNotificationWorkflowDetailVO.getRecordId(); workflowDetailId
	 * = objSeletectedNotificationWorkflowDetailVO .getWorkflowDetailId();
	 * 
	 * // generate notification
	 * 
	 * workflowEnterpriseService.handleUserActionAgainstNotification(
	 * workflowDetailId, recordId, approveflag, volist, comment, user);
	 * 
	 * }
	 */
	public String setSessions() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("WORKFLOW_RECORD_ID_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("WORKFLOW_DETAIL_ID_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("WORKFLOW_USECASE_"
					+ sessionObj.get("jSessionId"));

			session.setAttribute(
					"WORKFLOW_RECORD_ID_" + sessionObj.get("jSessionId"),
					recordId);
			session.setAttribute(
					"WORKFLOW_DETAIL_ID_" + sessionObj.get("jSessionId"),
					workflowDetailId);
			session.setAttribute(
					"WORKFLOW_USECASE_" + sessionObj.get("jSessionId"), useCase);

			Byte flag = null;
			getImplementId();
			if (processFlag == Constants.RealEstate.Status.Approved.getCode()) {
				List<WorkflowDetailVO> volist = null;
				volist = (ArrayList<WorkflowDetailVO>) session
						.getAttribute("WORKFLOW_LIST_"
								+ sessionObj.get("jSessionId")
								+ "_"
								+ Constants.RealEstate.Status.Approved
										.getCode());
				flag = (byte) Constants.RealEstate.Status.Confirm.getCode();
				/*
				 * workFlowBL.approveMethod(workflowDetailId, recordId, flag,
				 * volist, implementation);
				 */

			}

			if (messageId != null) {
				WorkflowDetailVO detailVO = getWorkflowDetailVObyMessageId(Long
						.parseLong(messageId));
				objSeletectedNotificationWorkflowDetailVO = new WorkflowDetailVO();
				if ((byte) detailVO.getOperationMaster().getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Approval
						.getCode()) {
					objSeletectedNotificationWorkflowDetailVO
							.setApproveButtonName(detailVO.getOperationMaster()
									.getApproveButton());
					objSeletectedNotificationWorkflowDetailVO
							.setRejectButtonName(detailVO.getOperationMaster()
									.getRejectButton());
				} else if ((byte) detailVO.getOperationMaster()
						.getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Preview
						.getCode()) {
					objSeletectedNotificationWorkflowDetailVO
							.setApproveButtonName(detailVO.getOperationMaster()
									.getApproveButton());
				} else if ((byte) detailVO.getOperationMaster()
						.getOperationGroup() == (byte) WorkflowConstants.OperationGroup.Post
						.getCode()) {
					objSeletectedNotificationWorkflowDetailVO
							.setApproveButtonName(detailVO.getOperationMaster()
									.getApproveButton());
				}

				objSeletectedNotificationWorkflowDetailVO.setOperation(detailVO
						.getWorkflowDetailProcess().getOperation());
				objSeletectedNotificationWorkflowDetailVO
						.setOperationGroupName(WorkflowConstants.OperationGroup
								.get(detailVO.getOperationMaster()
										.getOperationGroup()).name());
				long screenId = detailVO.getScreenId();
				screenPath = detailVO.getScreenPath();
				if (detailVO.getScreenByScreenId().getIsHidden() != null)
					isPopupApproval = detailVO.getScreenByScreenId()
							.getIsHidden();
				else
					isPopupApproval = false;
			}

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String returnSuccess() {
		return SUCCESS;
	}

	public String showJsonWorkflow() {
		LOGGER.info("Module: System : Method: showJsonWorkflow");
		try {
			getImplementId();
			JSONObject jsonList = getWorkFlowBL().getWorkflowlist(
					implementation);
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: System : Method: showJsonWorkflow: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: System : Method: showJsonWorkflow: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String findOperation() {
		try {

			HttpServletRequest request = ServletActionContext.getRequest();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			System.out
					.println("---------------Inside find operation method----");
			// pagePath = "generic-approval"; // test for generic approval
			// pop-up

			if (request.getParameter("messageId") != null) {
				Long messageId = Long.parseLong(request
						.getParameter("messageId"));

				objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(messageId);

				operationFlagInteger = Integer
						.parseInt(objSeletectedNotificationWorkflowDetailVO
								.getOperation().toString());

				long screenId = objSeletectedNotificationWorkflowDetailVO
						.getScreenId();

				if (screenId == 2) { // show preview screen

					// get page path for that process
					WorkflowDetail workflowDetail = workflowEnterpriseService
							.getGenerateNotificationsBL()
							.getWorkflowService()
							.getWorkflowDetailByWorkflowDeatilId(
									objSeletectedNotificationWorkflowDetailVO
											.getWorkflowDetailId(),
									true);

					// id of the record to show
					recordId = objSeletectedNotificationWorkflowDetailVO
							.getRecordId();
				}

				/*
				 * set workflowDetailVO to session, to make it available to the
				 * ManagedBean of requested page.
				 */
				session.setAttribute("WORKFLOW_DETAIL_VO",
						objSeletectedNotificationWorkflowDetailVO);

				/*
				 * remove any existing managedBean from viewMap() this was done
				 * to prevent same record loading of same usecase upon different
				 * notifications selection.
				 */
				String managedBeanName = (String) session
						.getAttribute("MANAGED_BEAN");
				if ((managedBeanName != null)
						&& !managedBeanName.equalsIgnoreCase("")) {

				}

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	public String fetchMessageDetailsByMessageId() {
		try {

			HttpServletRequest request = ServletActionContext.getRequest();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			System.out
					.println("---------------Inside find operation method----");
			// pagePath = "generic-approval"; // test for generic approval
			// pop-up

			if (request.getParameter("messageId") != null) {
				Long messageId = Long.parseLong(request
						.getParameter("messageId"));

				objSeletectedNotificationWorkflowDetailVO = getWorkflowDetailVObyMessageId(messageId);
				recordId = objSeletectedNotificationWorkflowDetailVO
						.getRecordId();
				useCase = objSeletectedNotificationWorkflowDetailVO
						.getWorkflow().getModuleProcess().getUseCase();
				workflowDetailId = objSeletectedNotificationWorkflowDetailVO
						.getMessage().getWorkflowDetailProcess()
						.getWorkflowDetail().getWorkflowDetailId();

				operationFlagInteger = Integer
						.parseInt(objSeletectedNotificationWorkflowDetailVO
								.getOperation().toString());

				long screenId = objSeletectedNotificationWorkflowDetailVO
						.getScreenId();

				if (screenId == 2) { // show preview screen

					// get page path for that process
					WorkflowDetail workflowDetail = workflowEnterpriseService
							.getGenerateNotificationsBL()
							.getWorkflowService()
							.getWorkflowDetailByWorkflowDeatilId(
									objSeletectedNotificationWorkflowDetailVO
											.getWorkflowDetailId(),
									true);

					// id of the record to show
					recordId = objSeletectedNotificationWorkflowDetailVO
							.getRecordId();
				}

				/*
				 * set workflowDetailVO to session, to make it available to the
				 * ManagedBean of requested page.
				 */
				session.setAttribute("WORKFLOW_DETAIL_VO",
						objSeletectedNotificationWorkflowDetailVO);

				/*
				 * remove any existing managedBean from viewMap() this was done
				 * to prevent same record loading of same usecase upon different
				 * notifications selection.
				 */
				String managedBeanName = (String) session
						.getAttribute("MANAGED_BEAN");
				if ((managedBeanName != null)
						&& !managedBeanName.equalsIgnoreCase("")) {

				}

			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}

	}

	private WorkflowDetailVO getWorkflowDetailVObyMessageId(Long messageId) {
		WorkflowDetailVO vo = new WorkflowDetailVO();
		try {

			if (messageId != null) {
				List<Message> messages = null;

				messages = workflowEnterpriseService
						.getGenerateNotificationsBL().getMessageService()
						.getMessageWithWorkflowProcessDetailsByMsgId(messageId);

				if (messages.size() > 0) {
					Message message = messages.get(0);

					WorkflowDetail workflowDetail = message
							.getWorkflowDetailProcess().getWorkflowDetail();

					// Fetch actual record from process's table
					Object recordObject = workflowEnterpriseService
							.getGenerateNotificationsBL()
							.getWorkflowService()
							.getRecord(
									workflowDetail.getWorkflow()
											.getModuleProcess().getUseCase(),
									message.getWorkflowDetailProcess()
											.getRecordId());

					// fill in workflowdetailVO necessary info and add in
					// workflowDetailVOs

					vo.setWorkflow(workflowDetail.getWorkflow());
					vo.setScreenId(message.getWorkflowDetailProcess()
							.getScreen().getScreenId());
					vo.setScreenPath(message.getWorkflowDetailProcess()
							.getScreen().getScreenPath());
					vo.setScreenByScreenId(message.getWorkflowDetailProcess()
							.getScreen());
					vo.setWorkflowDetailId(workflowDetail.getWorkflowDetailId());
					vo.setOperation(message.getWorkflowDetailProcess()
							.getOperation());
					vo.setOperationOrder(message.getWorkflowDetailProcess()
							.getWorkflowDetail().getOperationOrder());
					vo.setWorkflowDetailProcess(message
							.getWorkflowDetailProcess());
					vo.setOperationMaster(message.getWorkflowDetailProcess()
							.getWorkflowDetail().getOperationMaster());
					if (workflowDetail.getWorkflowDetail() != null)
						vo.setParentId(workflowDetail.getWorkflowDetail()
								.getWorkflowDetailId());
					vo.setObject(recordObject);
					vo.setRecordId(message.getWorkflowDetailProcess()
							.getRecordId());
					vo.setMessage(message);

					java.text.DateFormat df = new SimpleDateFormat(
							"EEE, d MMM yyyy hh:mm aaa");
					String formatedDate = df.format(message.getCreatedDate());
					vo.setFormatednotificationTime(formatedDate);

					vo.setProcessFlag(message.getWorkflowDetailProcess()
							.getOperation());
					vo.setWorkflowDetailProcess(message
							.getWorkflowDetailProcess());

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;

	}

	// Update Workflow Message
	public String updateWorkflowMessage() {
		try {
			JSONParser parser = new JSONParser();
			Object messageObject = parser.parse(approveReadContents);
			JSONArray object = (JSONArray) messageObject;
			List<Message> messages = new ArrayList<Message>();
			Message message = null;
			for (Iterator<?> iterator = object.iterator(); iterator.hasNext();) {
				org.json.simple.JSONObject objectDetail = (org.json.simple.JSONObject) iterator
						.next();
				message = generateNotificationsBL.getMessageService()
						.findMessageById(
								Long.parseLong(objectDetail.get("messageId")
										.toString()));
				message.setStatus((byte) 1);
				messages.add(message);
			}
			generateNotificationsBL.saveMessage(messages);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String showDashBoard() {
		return SUCCESS;
	}

	public WorkFlowBL getWorkFlowBL() {
		return workFlowBL;
	}

	public void setWorkFlowBL(WorkFlowBL workFlowBL) {
		this.workFlowBL = workFlowBL;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getWorkflowDetailId() {
		return workflowDetailId;
	}

	public void setWorkflowDetailId(long workflowDetailId) {
		this.workflowDetailId = workflowDetailId;
	}

	public Byte getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(Byte processFlag) {
		this.processFlag = processFlag;
	}

	public int getFinalyzed() {
		return finalyzed;
	}

	public void setFinalyzed(int finalyzed) {
		this.finalyzed = finalyzed;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	public Long getModuleProcessId() {
		return moduleProcessId;
	}

	public void setModuleProcessId(Long moduleProcessId) {
		this.moduleProcessId = moduleProcessId;
	}

	public String getWorkflowTitle() {
		return workflowTitle;
	}

	public void setWorkflowTitle(String workflowTitle) {
		this.workflowTitle = workflowTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWorkflowLineDetail() {
		return workflowLineDetail;
	}

	public void setWorkflowLineDetail(String workflowLineDetail) {
		this.workflowLineDetail = workflowLineDetail;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public void setWorkflowDetailId(Long workflowDetailId) {
		this.workflowDetailId = workflowDetailId;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public String getScreenPath() {
		return screenPath;
	}

	public void setScreenPath(String screenPath) {
		this.screenPath = screenPath;
	}

	public boolean getIsPopupApproval() {
		return isPopupApproval;
	}

	public void setIsPopupApproval(boolean isPopupApproval) {
		this.isPopupApproval = isPopupApproval;
	}

	public WorkflowDetailVO getObjSeletectedNotificationWorkflowDetailVO() {
		return objSeletectedNotificationWorkflowDetailVO;
	}

	public void setObjSeletectedNotificationWorkflowDetailVO(
			WorkflowDetailVO objSeletectedNotificationWorkflowDetailVO) {
		this.objSeletectedNotificationWorkflowDetailVO = objSeletectedNotificationWorkflowDetailVO;
	}

	public Integer getOperationFlagInteger() {
		return operationFlagInteger;
	}

	public void setOperationFlagInteger(Integer operationFlagInteger) {
		this.operationFlagInteger = operationFlagInteger;
	}

	public GenerateNotificationsBL getGenerateNotificationsBL() {
		return generateNotificationsBL;
	}

	public void setGenerateNotificationsBL(
			GenerateNotificationsBL generateNotificationsBL) {
		this.generateNotificationsBL = generateNotificationsBL;
	}

	public WorkflowVO getReturnVO() {
		return returnVO;
	}

	public void setReturnVO(WorkflowVO returnVO) {
		this.returnVO = returnVO;
	}

	public String getApproveReadContents() {
		return approveReadContents;
	}

	public void setApproveReadContents(String approveReadContents) {
		this.approveReadContents = approveReadContents;
	}

}
