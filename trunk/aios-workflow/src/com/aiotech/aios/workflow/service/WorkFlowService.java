package com.aiotech.aios.workflow.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.service.AccountsEnterpriseService;
import com.aiotech.aios.hr.service.HrEnterpriseService;
import com.aiotech.aios.realestate.service.RealEstateEnterpriseService;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Module;
import com.aiotech.aios.workflow.domain.entity.ModuleProcess;
import com.aiotech.aios.workflow.domain.entity.Notification;
import com.aiotech.aios.workflow.domain.entity.Role;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class WorkFlowService {
	private AIOTechGenericDAO<Workflow> workflowDAO;
	private AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO;
	private AIOTechGenericDAO<Notification> notificationDAO;
	private AIOTechGenericDAO<Module> moduleDAO;
	private AIOTechGenericDAO<ModuleProcess> moduleProcessDAO;
	private AIOTechGenericDAO<Screen> screenDAO;
	private AIOTechGenericDAO<UserRole> userRoleDAO;
	private AIOTechGenericDAO<Role> roleDAO;
	private AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO;
	private AIOTechGenericDAO<Object> dao;
	private RealEstateEnterpriseService realEstateEnterpriseService;
	private HrEnterpriseService hrEnterpriseService;
	private AccountsEnterpriseService accountsEnterpriseService;

	public List<UserRole> getUserRoles(User user) throws Exception {
		return userRoleDAO.findByNamedQuery("getUserRoles", user.getUsername());
	}

	public List<WorkflowDetail> getWorkFlowDetialList(Implementation implementation,UserRole userRole)
			throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetails",
				implementation,userRole,userRole.getRole());
	}
	
	public WorkflowDetail getWorkFlowDetialInfo(Long workflowDetailId)
			throws Exception {
		return workflowDetailDAO.findById(workflowDetailId);
	}
	public List<WorkflowDetail> getWorkFlowDetialListWithoutRole(Implementation implementation)
	throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetailsWithoutRole",implementation);
	}

	public List<Workflow> getAllWorkflow(Implementation implementation) throws Exception{
		return workflowDAO.findByNamedQuery("getAllWorkflow", implementation);
	}
	
	public List<Workflow> getWorkflowList(Implementation implementation) throws Exception{
		return workflowDAO.findByNamedQuery("getWorkflowList", implementation);
	}
	
	public List<ModuleProcess> getAllModuleProcess() throws Exception{
		return moduleProcessDAO.getAll();
	}
	
	public List<Role> getAllRoles(Implementation implementation) throws Exception{
		return roleDAO.findByNamedQuery("getAllRole",implementation);
	}
	
	public List<Screen> getAllScreens() throws Exception{
		return screenDAO.getAll();
	}
	
	public List<Notification> getAllNotification() throws Exception{
		return notificationDAO.getAll();
	}
	public List<Object> getUnApproved(String modul, String usecase,Byte flag,Implementation implementation)
			throws Exception {
		List<Object> objects = null;
		Object callingObject;
		Object usecaseObject;
		Object[] objectParm = { null };
		Boolean param = null;
		String useCaseTemp=usecase.replace(".domain.entity.", ".DOMAIN.ENTITY.");
		String[] name = useCaseTemp.split(".DOMAIN.ENTITY.");
		//String usecase = (name[name.length - 1]);
		String moduleMethod = "get" + modul + "EnterpriseService";
		String usecaseService = "get" + (name[name.length - 1]) + "Service";
		String usecaseDAO = "get" + (name[name.length - 1]) + "DAO";

		Method method = this.getClass().getMethod(moduleMethod, null);
		callingObject = method.invoke(this);

		method = callingObject.getClass().getMethod(usecaseService, null);
		callingObject = method.invoke(callingObject);

		method = callingObject.getClass().getMethod(usecaseDAO, null);
		dao = (AIOTechGenericDAO<Object>) method.invoke(callingObject);

		Class cl = Class.forName(usecase);
		Constructor constructor = cl.getConstructor(null);
		usecaseObject = constructor.newInstance(null);
		
		Class[] argTypes = new Class[] { Byte.class };
		method = usecaseObject.getClass().getMethod("setIsApprove", argTypes);
		method.invoke(usecaseObject,flag);
		
		Class[] imple = new Class[] { Implementation.class };
		method = usecaseObject.getClass().getMethod("setImplementation", imple);
		method.invoke(usecaseObject,implementation);
		
		return dao.findByExample(usecaseObject);
	}

	
	public void setApproval(String modul, Object object) throws Exception {
		List<Object> objects = null;
		Object callingObject;
		// Object usecaseObject;
		Object[] objectParm = { null };
		String useCaseTemp=object.getClass().getName().replace(".domain.entity.", ".DOMAIN.ENTITY.");
		String[] name = useCaseTemp.split(".DOMAIN.ENTITY.");
		String usecase = (name[name.length - 1]);

		String moduleMethod = "get" + modul + "EnterpriseService";
		String usecaseService = "get" + usecase + "Service";
		String usecaseDAO = "get" + usecase + "DAO";

		Method method = this.getClass().getMethod(moduleMethod, null);
		callingObject = method.invoke(this);

		method = callingObject.getClass().getMethod(usecaseService, null);
		callingObject = method.invoke(callingObject);

		method = callingObject.getClass().getMethod(usecaseDAO, null);
		dao = (AIOTechGenericDAO<Object>) method.invoke(callingObject);

		dao.saveOrUpdate(object);
	}
	public void saveWorkflowDetailProcess(WorkflowDetailProcess workflowdetailproc){
		workflowDetailProcessDAO.saveOrUpdate(workflowdetailproc);
	}
	public Workflow getWorkflowInfo(Long workflowId) throws Exception{
		List<Workflow> workflows=new ArrayList<Workflow>();
		workflows=workflowDAO.findByNamedQuery("getWorkflowInfo", workflowId);
		if(workflows!=null && workflows.size()>0)
			return workflows.get(0);
		else
			return null;
	}
	public List<WorkflowDetail> getWorkFlowDetailssBasedOnWorkflow(Long workflowId)
		throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetailssBasedOnWorkflow",	workflowId);
	}
	public List<WorkflowDetail> getWorkFlowDetailsForDelete(Long workflowId)
		throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetailsForDelete", workflowId);
	}
	public List<WorkflowDetail> getWorkFlowDetailsForNotification()
		throws Exception {
		return workflowDetailDAO.findByNamedQuery("getWorkFlowDetailsForNotification");
	}
	
	public WorkflowDetail getWorkFlowDetailsForPreviousFlow
	(Long workflowId,int operation,int operationOrder)	throws Exception {
		WorkflowDetail workflowDetail=null;
		List<WorkflowDetail> workflowDetails=workflowDetailDAO.findByNamedQuery("getWorkFlowDetailsForPreviousFlow",
				workflowId,Byte.parseByte(operation+""),Byte.parseByte(operationOrder+""));
		if(workflowDetails!=null && workflowDetails.size()>0){
			workflowDetail=new WorkflowDetail();
			workflowDetail=workflowDetails.get(0);
		}
		return workflowDetail;
	}
	
	public WorkflowDetail getWorkFlowDetailsForNextFlow
	(Long workflowId,int operation,int operationOrder)	throws Exception {
		WorkflowDetail workflowDetail=null;
		List<WorkflowDetail> workflowDetails=workflowDetailDAO.findByNamedQuery("getWorkFlowDetailsForNextFlow",
				workflowId,Byte.parseByte(operation+""),Byte.parseByte(operationOrder+""));
		if(workflowDetails!=null && workflowDetails.size()>0){
			workflowDetail=new WorkflowDetail();
			workflowDetail=workflowDetails.get(0);
		}
		return workflowDetail;
	}
	
	public WorkflowDetailProcess getWorkFlowDetailProcessInfo
	(Long workflowDetailId,Long recordId)	throws Exception {
		WorkflowDetailProcess workflowDetail=null;
		List<WorkflowDetailProcess> workflowDetails=workflowDetailProcessDAO.findByNamedQuery("getWorkFlowDetailProcessInfo",
				workflowDetailId,recordId);
		if(workflowDetails!=null && workflowDetails.size()>0){
			workflowDetail=new WorkflowDetailProcess();
			workflowDetail=workflowDetails.get(0);
		}
		return workflowDetail;
	}
	
	public List<WorkflowDetailProcess> getWorkFlowDetailProcessToDelete(Long workflowId, Long recordId){
		return workflowDetailProcessDAO.findByNamedQuery("getWorkFlowDetailProcessToDelete",
				workflowId,recordId);
		
	}
	/*public List<UserRoleCombination> getUserRoleCombinationBasedOnWorkflowDetail(Long workflowDetailId)	throws Exception {
		return userRoleCombinationDAO.findByNamedQuery("getUserRoleCombinationBasedOnWorkflowDetail",	workflowDetailId);
	}*/
	public void saveWorkflow(Workflow workflow){
		workflowDAO.saveOrUpdate(workflow);
	}
	public void saveWorkflowDetails(List<WorkflowDetail> workflowdetails){
		workflowDetailDAO.saveOrUpdateAll(workflowdetails);
	}
	public void saveWorkflowDetail(WorkflowDetail workflowdetail){
		workflowDetailDAO.saveOrUpdate(workflowdetail);
	}
	/*public void saveUserRoleCombinations(List<UserRoleCombination> userRoleCombinations){
		userRoleCombinationDAO.saveOrUpdateAll(userRoleCombinations);
	}*/
	public void deleteWorkflow(Workflow workflow){
		workflowDAO.delete(workflow);
	}
	public void deleteWorkflowDetails(List<WorkflowDetail> workflowdetails){
		workflowDetailDAO.deleteAll(workflowdetails);
	}
	
	public void deleteWorkflowDetailProcesses(List<WorkflowDetailProcess> workflowdetails){
		workflowDetailProcessDAO.deleteAll(workflowdetails);
	}
	/*public void deleteUserRoleCombinations(List<UserRoleCombination> userRoleCombinations){
		userRoleCombinationDAO.deleteAll(userRoleCombinations);
	}*/
	
	// Getter & Setter part
	public AIOTechGenericDAO<Workflow> getWorkflowDAO() {
		return workflowDAO;
	}

	public void setWorkflowDAO(AIOTechGenericDAO<Workflow> workflowDAO) {
		this.workflowDAO = workflowDAO;
	}

	public AIOTechGenericDAO<WorkflowDetail> getWorkflowDetailDAO() {
		return workflowDetailDAO;
	}

	public void setWorkflowDetailDAO(
			AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO) {
		this.workflowDetailDAO = workflowDetailDAO;
	}

	public AIOTechGenericDAO<Notification> getNotificationDAO() {
		return notificationDAO;
	}

	public void setNotificationDAO(
			AIOTechGenericDAO<Notification> notificationDAO) {
		this.notificationDAO = notificationDAO;
	}

	public AIOTechGenericDAO<Module> getModuleDAO() {
		return moduleDAO;
	}

	public void setModuleDAO(AIOTechGenericDAO<Module> moduleDAO) {
		this.moduleDAO = moduleDAO;
	}

	public AIOTechGenericDAO<ModuleProcess> getModuleProcessDAO() {
		return moduleProcessDAO;
	}

	public void setModuleProcessDAO(
			AIOTechGenericDAO<ModuleProcess> moduleProcessDAO) {
		this.moduleProcessDAO = moduleProcessDAO;
	}

	 

	public AIOTechGenericDAO<UserRole> getUserRoleDAO() {
		return userRoleDAO;
	}

	public void setUserRoleDAO(AIOTechGenericDAO<UserRole> userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}

	public AIOTechGenericDAO<Object> getDao() {
		return dao;
	}

	public void setDao(AIOTechGenericDAO<Object> dao) {
		this.dao = dao;
	}

	public RealEstateEnterpriseService getRealEstateEnterpriseService() {
		return realEstateEnterpriseService;
	}

	public void setRealEstateEnterpriseService(
			RealEstateEnterpriseService realEstateEnterpriseService) {
		this.realEstateEnterpriseService = realEstateEnterpriseService;
	}

	public HrEnterpriseService getHrEnterpriseService() {
		return hrEnterpriseService;
	}

	public void setHrEnterpriseService(HrEnterpriseService hrEnterpriseService) {
		this.hrEnterpriseService = hrEnterpriseService;
	}

	public AccountsEnterpriseService getAccountsEnterpriseService() {
		return accountsEnterpriseService;
	}

	public void setAccountsEnterpriseService(
			AccountsEnterpriseService accountsEnterpriseService) {
		this.accountsEnterpriseService = accountsEnterpriseService;
	}

	public AIOTechGenericDAO<Role> getRoleDAO() {
		return roleDAO;
	}

	public void setRoleDAO(AIOTechGenericDAO<Role> roleDAO) {
		this.roleDAO = roleDAO;
	}

	public AIOTechGenericDAO<Screen> getScreenDAO() {
		return screenDAO;
	}

	public void setScreenDAO(AIOTechGenericDAO<Screen> screenDAO) {
		this.screenDAO = screenDAO;
	}

	
	public AIOTechGenericDAO<WorkflowDetailProcess> getWorkflowDetailProcessDAO() {
		return workflowDetailProcessDAO;
	}

	public void setWorkflowDetailProcessDAO(
			AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO) {
		this.workflowDetailProcessDAO = workflowDetailProcessDAO;
	}
	
}
