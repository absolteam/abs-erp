package com.aiotech.aios.workflow.service.bl;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.Message;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.UserRole;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.service.WorkFlowService;
import com.aiotech.aios.workflow.to.WorkflowDetailVO;
import com.opensymphony.xwork2.ActionContext;

public class WorkFlowBL {

	private WorkFlowService workFlowService;
	private SystemService systemService;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	public List<WorkflowDetailVO> getNotificationList(User user, Byte flag,Implementation implementation)
			throws Exception {
		List<WorkflowDetail> workflowDetails = new ArrayList<WorkflowDetail>();
		List<WorkflowDetailVO> volist = new ArrayList<WorkflowDetailVO>();
		List<UserRole> userRoles=systemService.getUserRoles(user.getUsername());
		for (UserRole userRole : userRoles) {
			
			//Work flow detail list with role information
			workflowDetails = workFlowService.getWorkFlowDetialList(implementation,userRole);
			for (WorkflowDetail workflowDetail : workflowDetails) {
				if (workflowDetail.getOperation().equals(flag) || (workflowDetail.getOperation()==0 && flag==3)) {

					List<Object> objects = workFlowService.getUnApproved(
							workflowDetail.getWorkflow().getModuleProcess()
									.getModule().getModuleName(),
							workflowDetail.getWorkflow().getModuleProcess().getUseCase(), flag,
							implementation);
					//Reverse the order 
					List<Object> tempobjects=new ArrayList<Object>();
					for (int i = objects.size()-1; i>=0; i--) {
						tempobjects.add(objects.get(i));
					}
					objects=tempobjects;
					for (Object object : objects) {
						//Find the implementation records
						String impleString="getImplementation";
						Method impMethod = object.getClass().getMethod(impleString, null);
						Implementation imp=(Implementation) impMethod.invoke(object, null);
						if (imp.getImplementationId().equals(implementation.getImplementationId())) {
							WorkflowDetailVO vo = new WorkflowDetailVO();
							vo.setWorkflow(workflowDetail.getWorkflow());
							//vo.setScreen(workflowDetail.getScreen());
							vo.setWorkflowDetailId(workflowDetail
									.getWorkflowDetailId());
							vo.setOperation(workflowDetail.getOperation());
							vo.setOperationOrder(workflowDetail.getOperationOrder());
							vo.setObject(object);

							// Fetch Record Id
							String useCaseTemp=object.getClass().getName().replace(".domain.entity.", ".DOMAIN.ENTITY.");
							String[] name = useCaseTemp.split(".DOMAIN.ENTITY.");
														
							if(name[name.length - 1].contains("_"))
								name[1]=name[name.length - 1].split("_")[0];
							
							String methodId = "get" + name[name.length - 1]
									+ "Id";
							Method method = object.getClass().getMethod(
									methodId, null);
							vo.setRecordId((Long) method.invoke(object, null));
							String methodName = "get" + name[name.length - 1]
									+ "Name";
							String methodNumber = "get" + name[name.length - 1]
									+ "No";
							try {
								if (object.getClass().getMethod(methodName,
										null) != null) {
									Method method1 = object.getClass()
											.getMethod(methodName, null);
									vo.setRecordName((String) method1.invoke(
											object, null));
								}

							} catch (NoSuchMethodException e) {
								vo.setRecordName(null);
							}
							try {
								if (object.getClass().getMethod(methodNumber,
										null) != null) {
									Method number = object.getClass()
											.getMethod(methodNumber, null);
									vo.setRecordNumber(number.invoke(object,null)+"");
								}
							} catch (NoSuchMethodException e) {
								vo.setRecordNumber(null);
							}
							vo.setProcessFlag(flag);
							
							boolean validRecord=isValidPerson(vo);
							if(validRecord)
							volist.add(vo);
						}
					}
				}
			}

			//Work flow detail list with out user/role information
			workflowDetails = workFlowService.getWorkFlowDetialListWithoutRole(implementation);
			for (WorkflowDetail workflowDetail : workflowDetails) {
				if (workflowDetail.getOperation().equals(flag)) {

					List<Object> objects1 = workFlowService.getUnApproved(
							workflowDetail.getWorkflow().getModuleProcess()
									.getModule().getModuleName(),
							workflowDetail.getWorkflow().getModuleProcess().getUseCase(), flag,
							implementation);
					//Reverse the order
					List<Object> tempobjects1=new ArrayList<Object>();
					for (int i = objects1.size()-1; i>=0; i--) {
						tempobjects1.add(objects1.get(i));
					}
					objects1=tempobjects1;
					for (Object object : objects1) {
						// Find the implementation records
						String impleString = "getImplementation";
						Method impMethod = object.getClass().getMethod(
								impleString, null);
						Implementation imp = (Implementation) impMethod.invoke(
								object, null);
						Person person = new Person();
						try {
							String createdbyString = "getPerson";
							Method createdbyMethod = object.getClass()
									.getMethod(createdbyString, null);
							person = (Person) createdbyMethod.invoke(object,
									null);
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
						}
						if (implementation != null
								&& imp != null
								&& imp.getImplementationId().equals(
										implementation.getImplementationId())
								&& person != null
								&& person.getPersonId().equals(
										userRole.getUser()
												.getPersonId())) {
							WorkflowDetailVO vo = new WorkflowDetailVO();
							vo.setWorkflow(workflowDetail.getWorkflow());
							vo.setWorkflowDetailId(workflowDetail
									.getWorkflowDetailId());
							vo.setOperation(workflowDetail.getOperation());
							vo.setOperationOrder(workflowDetail
									.getOperationOrder());
							vo.setObject(object);

							// Fetch Record Id
							String useCaseTemp = object
									.getClass()
									.getName()
									.replace(".domain.entity.",
											".DOMAIN.ENTITY.");
							String[] name = useCaseTemp
									.split(".DOMAIN.ENTITY.");

							if (name[name.length - 1].contains("_"))
								name[1] = name[name.length - 1].split("_")[0];

							String methodId = "get" + name[name.length - 1]
									+ "Id";

							Method method = object.getClass().getMethod(
									methodId, null);
							vo.setRecordId((Long) method.invoke(object, null));
							String methodName = "get" + name[name.length - 1]
									+ "Name";
							String methodNumber = "get" + name[name.length - 1]
									+ "No";
							try {
								if (object.getClass().getMethod(methodName,
										null) != null) {
									Method method1 = object.getClass()
											.getMethod(methodName, null);
									vo.setRecordName((String) method1.invoke(
											object, null));
								}

							} catch (NoSuchMethodException e) {
								vo.setRecordName(null);
							}
							try {
								if (object.getClass().getMethod(methodNumber,
										null) != null) {
									Method number = object.getClass()
											.getMethod(methodNumber, null);
									vo.setRecordNumber(number.invoke(object,
											null) + "");
								}
							} catch (NoSuchMethodException e) {
								vo.setRecordNumber(null);
							}
							vo.setProcessFlag(flag);
							volist.add(vo);
						}
					}
				}
			}
		
		//Automatic Reverse flow for only Pending (3) process
		/*if(flag==3){
			List<Workflow> workflows = workFlowService.getWorkflowList(implementation);
			for (Workflow workflow : workflows) {
				
					List<Object> objects1 = workFlowService.getUnApproved(
							workflow.getModuleProcess()
									.getModule().getModuleName(),
							workflow.getModuleProcess().getUseCase(), flag,
							implementation);
					//Reverse the order
					List<Object> tempobjects1=new ArrayList<Object>();
					for (int i = objects1.size()-1; i>=0; i--) {
						tempobjects1.add(objects1.get(i));
					}
					objects1=tempobjects1;
					for (Object object : objects1) {
						//Find the implementation records
						String impleString="getImplementation";
						Method impMethod = object.getClass().getMethod(impleString, null);
						Implementation imp=(Implementation) impMethod.invoke(object, null);
						Person person=new Person();
						try {
							String createdbyString="getPerson";
							Method createdbyMethod = object.getClass().getMethod(createdbyString, null);
							person=(Person) createdbyMethod.invoke(object, null);
						}catch (NoSuchMethodException e) {
							e.printStackTrace();
						}
						if (imp.getImplementationId().equals(implementation.getImplementationId()) && person!=null && 
								person.getPersonId().equals(userRole.getUser().getPerson().getPersonId())) {
							WorkflowDetailVO vo = new WorkflowDetailVO();
							vo.setWorkflow(workflow);
							vo.setScreen(null);
							Notification notification=new Notification();
							notification.setNotificationText(workflow.getWorkflowTitle());
							vo.setNotification(notification);
							vo.setWorkflowDetailId(null);
							vo.setOperation(null);
							vo.setOperationOrder(null);
							vo.setObject(object);
	
							// Fetch Record Id
							String[] name = object.getClass().getName()
									.split("entity.");
							String methodId = "get" + name[name.length - 1]
									+ "Id";
							Method method = object.getClass().getMethod(
									methodId, null);
							vo.setRecordId((Long) method.invoke(object, null));
							String methodName = "get" + name[name.length - 1]
									+ "Name";
							String methodNumber = "get" + name[name.length - 1]
									+ "No";
							try {
								if (object.getClass().getMethod(methodName,
										null) != null) {
									Method method1 = object.getClass()
											.getMethod(methodName, null);
									vo.setRecordName((String) method1.invoke(
											object, null));
								}
	
							} catch (NoSuchMethodException e) {
								vo.setRecordName(null);
							}
							try {
								if (object.getClass().getMethod(methodNumber,
										null) != null) {
									Method number = object.getClass()
											.getMethod(methodNumber, null);
									vo.setRecordNumber(number.invoke(object,null)+"");
								}
							} catch (NoSuchMethodException e) {
								vo.setRecordNumber(null);
							}
							vo.setProcessFlag(flag);
							boolean validRecord=isValidPerson(vo);
							if(validRecord)
							volist.add(vo);
						}
					}
				}
			}*/
		}
		return volist;
	}
	
	public void approveMethod(long workflowDetailId,long recordId,Byte processFlag,List<WorkflowDetailVO> volist,Implementation implementation ) throws Exception{
		HttpSession session = ServletActionContext.getRequest()
		.getSession();
		Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 

		User user = new User();
		user = (User) sessionObj.get("USER");
		for (WorkflowDetailVO workflowDetailVO : volist) {
			if(workflowDetailVO.getWorkflowDetailId()==workflowDetailId){
				
				String useCaseTemp=workflowDetailVO.getObject().getClass().getName().replace(".domain.entity.", ".DOMAIN.ENTITY.");
				String[] name = useCaseTemp.split(".DOMAIN.ENTITY.");
				String methodId = "get" + name[name.length - 1] + "Id";
				Method method = workflowDetailVO.getObject().getClass().getMethod(methodId, null);
				long temprecordId=((Long) method.invoke(workflowDetailVO.getObject(), null));
				if(temprecordId==recordId){
					//If exist any next flow for the same operation
					WorkflowDetail nextworkflowDetail=workFlowService.getWorkFlowDetailsForNextFlow
					(workflowDetailVO.getWorkflow().getWorkflowId(), workflowDetailVO.getProcessFlag(), 
							workflowDetailVO.getOperationOrder());
					if(nextworkflowDetail==null || processFlag==2){
						String approvalflagmethodId = "setIsApprove";
						Class[] argTypes = new Class[] { Byte.class };
						Method approvalmethod = workflowDetailVO.getObject().getClass().getMethod(approvalflagmethodId, argTypes);
						approvalmethod.invoke(workflowDetailVO.getObject(),processFlag);
						workFlowService.setApproval(workflowDetailVO.getWorkflow().getModuleProcess()
								.getModule().getModuleName(), workflowDetailVO.getObject());
					}
					
					//Save WorkflowDetail Process table
					WorkflowDetailProcess workflowdetailproc=new WorkflowDetailProcess();
					workflowdetailproc.setRecordId(recordId);
					WorkflowDetail workflowDetail=new WorkflowDetail();
					workflowDetail.setWorkflowDetailId(workflowDetailId);
					workflowdetailproc.setWorkflowDetail(workflowDetail);
					workflowdetailproc.setCreatedDate(new Date());
					if(processFlag==2){
						List<WorkflowDetailProcess> workflowDetailProcessList=
							workFlowService.getWorkFlowDetailProcessToDelete(workflowDetailVO.getWorkflow().getWorkflowId(),recordId);
						workFlowService.deleteWorkflowDetailProcesses(workflowDetailProcessList);
					}else{
						workFlowService.saveWorkflowDetailProcess(workflowdetailproc);
					}
					//Update Workflow Message information
					List<Message> messages=systemService.getWorkFlowDetailMessageInfo
					(recordId,workflowDetailVO.getWorkflow().getModuleProcess().getUseCase());
					if(messages!=null && messages.size()>0){
						for (Message message : messages) {
							message.setStatus(Byte.parseByte("1"));
						}
						systemService.saveMessages(messages);
					}
				}
			}
		}
		
	}
	
	public JSONObject getWorkflowlist(Implementation implementation) throws Exception{
		List<Workflow> workflowList=this.getWorkFlowService().getAllWorkflow(implementation);
		JSONObject jsonResponse= new JSONObject();
		if(workflowList!=null && !workflowList.equals("")){
			iTotalRecords=workflowList.size();
			iTotalDisplayRecords=workflowList.size();
		}
		jsonResponse.put("iTotalRecords", iTotalRecords);
		jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);  
		JSONArray data= new JSONArray();
		JSONArray array= null;
		long tempval=0;
		for(Workflow list: workflowList){
			array=new JSONArray(); 
			if(tempval!=list.getWorkflowId()){
				tempval=list.getWorkflowId();
				array.add(list.getWorkflowId());
				array.add(list.getWorkflowTitle());
				array.add(list.getModuleProcess().getProcessTitle());
				data.add(array);
			}  
		}
		jsonResponse.put("aaData", data);
		return jsonResponse;
	}
	
	public void workFlowSave(Workflow workflow,List<WorkflowDetail> workflowDetails) throws Exception{
		workFlowService.saveWorkflow(workflow);
		for (WorkflowDetail workflowDetail : workflowDetails) {
			workflowDetail.setWorkflow(workflow);
		}
		workFlowService.saveWorkflowDetails(workflowDetails);
		//Find the deleted record to inactive
		if(workflow.getWorkflowId()!=null){
			Collection<Long> listOne =new ArrayList<Long>();
			Collection<Long> listTwo =new ArrayList<Long>();
			for(WorkflowDetail workflowDetail:workflowDetails){
				if(workflowDetail.getWorkflowDetailId()!=null && workflowDetail.getWorkflowDetailId()>0)
					listOne.add(workflowDetail.getWorkflowDetailId());
			}
			List<WorkflowDetail> workflowDetailEditList=workFlowService.getWorkFlowDetailssBasedOnWorkflow(workflow.getWorkflowId());
			for(WorkflowDetail workflowDetail:workflowDetailEditList){
				if(workflowDetail.getWorkflowDetailId()!=null && workflowDetail.getWorkflowDetailId()>0)
					listTwo.add(workflowDetail.getWorkflowDetailId());
			}
			Collection<Long> similar = new HashSet<Long>( listOne );
	        Collection<Long> different = new HashSet<Long>();
	        different.addAll( listOne );
	        different.addAll( listTwo );
	        similar.retainAll( listTwo );
	        different.removeAll( similar );
	        
	        //Delete Process
	        List<WorkflowDetail> workflowDetailInactiveist=new ArrayList<WorkflowDetail>();
	        for(Long lng:different){
		        for(WorkflowDetail workflowDetail:workflowDetailEditList){
					if(lng!=null && workflowDetail.getWorkflowDetailId().equals(lng)){
						workflowDetail.setIsActive(false);
						workflowDetailInactiveist.add(workflowDetail);
					}
				}
	        }
	        workFlowService.saveWorkflowDetails(workflowDetailInactiveist);
		}
		/*for (WorkflowDetail workflowDetail : workflowDetails) {
			if(workflowDetail.getUserRoleCombinations()!=null && workflowDetail.getUserRoleCombinations().size()>0){
				for (UserRoleCombination userRoleCombination : workflowDetail.getUserRoleCombinations()) {
					userRoleCombination.setWorkflowDetail(workflowDetail);
					userRoleCombination.setUserRoleCombinationId(null);
				}
			}
			if(workflow.getWorkflowId()!=null && workflowDetail.getWorkflowDetailId()!=null && workflowDetail.getWorkflowDetailId()>0){
				workFlowService.deleteUserRoleCombinations
				(workFlowService.getUserRoleCombinationBasedOnWorkflowDetail(workflowDetail.getWorkflowDetailId()));
			}
			
			List<UserRoleCombination> list = new ArrayList<UserRoleCombination>(workflowDetail.getUserRoleCombinations());
			workFlowService.saveUserRoleCombinations(list);
		}*/
	}
	
	public boolean isValidPerson(WorkflowDetailVO vo) throws Exception{
		boolean returnFlag=false;
		//Find previous flow
		WorkflowDetail previousWorkflowDetail=new WorkflowDetail();
		previousWorkflowDetail=workFlowService.
		getWorkFlowDetailsForPreviousFlow(vo.getWorkflow().getWorkflowId(),vo.getProcessFlag(),
				vo.getOperationOrder());
		//Is completed current flow
			WorkflowDetailProcess workflowDetailProcess=workFlowService.getWorkFlowDetailProcessInfo
				(vo.getWorkflowDetailId(), vo.getRecordId());
		if(previousWorkflowDetail==null && workflowDetailProcess==null){//No previous flow && dont have workflow detail process for current flow
			returnFlag=true;
		}else if(previousWorkflowDetail==null && workflowDetailProcess!=null){
			returnFlag=false;
		}else if(previousWorkflowDetail!=null){
			//Is completed previous flow and current flow should not be completed
			WorkflowDetailProcess previousWorkflowDetailProcess=workFlowService.getWorkFlowDetailProcessInfo
			(previousWorkflowDetail.getWorkflowDetailId(), vo.getRecordId());
			if(previousWorkflowDetailProcess!=null && workflowDetailProcess==null)
				returnFlag=true;
			else
				returnFlag=false;
		}	
		return returnFlag;
	}
	//-----------Notification scheduler---------------------------------------
	public void notificationSchdule(){/*
		try{
			List<WorkflowDetail> workflowDetails=workFlowService.getWorkFlowDetailsForNotification();
			for (WorkflowDetail workflowDetail : workflowDetails) {
			

				Date crDate=null;
				if(((workflowDetail.getIsEmail()!=null && workflowDetail.getIsEmail()==true) || 
						(workflowDetail.getIsSms()!=null && workflowDetail.getIsSms()==true ) || 
							(workflowDetail.getIsSystem()!=null && workflowDetail.getIsSystem()==true)) &&
								((workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0)	|| 
											(workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0)))
				{		
				List<Object> objects = workFlowService.getUnApproved(
						workflowDetail.getWorkflow().getModuleProcess()
								.getModule().getModuleName(),
						workflowDetail.getWorkflow().getModuleProcess().getUseCase(), workflowDetail.getOperation(),
						workflowDetail.getWorkflow().getImplementation());
						
					for (Object object : objects) {
						List<Message> messages=new ArrayList<Message>();
						Message vo = new Message();
						String recordName=null;
						String recordNumber=null;
						//Find the implementation records
						String impleString="getImplementation";
						Method impMethod = object.getClass().getMethod(impleString, null);
						Implementation imp=(Implementation) impMethod.invoke(object, null);
						if (imp.getImplementationId().equals(workflowDetail.getWorkflow().getImplementation().getImplementationId())) {
							
							
							// Fetch Record Id
							String useCaseTemp=object.getClass().getName().replace(".domain.entity.", ".DOMAIN.ENTITY.");
							String[] name = useCaseTemp.split(".DOMAIN.ENTITY.");
							String methodId = "get" + name[name.length - 1]
									+ "Id";
							Method method = object.getClass().getMethod(
									methodId, null);
							vo.setRecordId((Long) method.invoke(object, null));
							String methodName = "get" + name[name.length - 1]
									+ "Name";
							String methodNumber = "get" + name[name.length - 1]
									+ "No";
							String createdDate = "getCreatedDate";
							try {
								if (object.getClass().getMethod(methodName,
										null) != null) {
									Method method1 = object.getClass()
											.getMethod(methodName, null);
									recordName=(String) method1.invoke(
											object, null);
								}
	
							} catch (NoSuchMethodException e) {
							}
							try {
								if (object.getClass().getMethod(methodNumber,
										null) != null) {
									Method number = object.getClass()
											.getMethod(methodNumber, null);
									recordNumber=(String) number.invoke(object,null);
								}
							} catch (NoSuchMethodException e) {
								recordNumber=null;
							}
							try {
								if (object.getClass().getMethod(createdDate,
										null) != null) {
									Method createdDateMethod = object.getClass()
											.getMethod(createdDate, null);
									crDate=(Date) createdDateMethod.invoke(object,null);
								}
							} catch (NoSuchMethodException e) {
							}
							
							//-----------------Notification Process timing--------------------------------------
							Calendar cal = Calendar.getInstance(); 
							java.util.Date currentDate=cal.getTime();
							Calendar calValidity = Calendar.getInstance();
							Calendar calReminder = Calendar.getInstance(); 
							Calendar calEscalation = Calendar.getInstance(); 
							Date dateWithValidity=null; 
							Date dateWithReminder=null; 
							Date dateWithEscalation=null;
							if(crDate!=null && workflowDetail.getIsDays()==true){
								calValidity.setTime(crDate);
								calValidity.add(Calendar.DATE, workflowDetail.getValidity());
								dateWithValidity=calValidity.getTime();
								
								if(workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0){
									calReminder.setTime(crDate);
									calReminder.add(Calendar.DATE, -(workflowDetail.getReminder()));
									dateWithReminder=calReminder.getTime();
								}
								if(workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0){
									calEscalation.setTime(crDate);
									calEscalation.add(Calendar.DATE, workflowDetail.getEscalation());
									dateWithEscalation=calEscalation.getTime();
								}
							}else if(crDate!=null &&  workflowDetail.getIsDays()==false){
								calValidity.setTime(crDate);
								calValidity.add(Calendar.HOUR, workflowDetail.getValidity());
								dateWithValidity=calValidity.getTime();
								
								if(workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0){
									calReminder.setTime(dateWithValidity);
									calReminder.add(Calendar.HOUR, -(workflowDetail.getReminder()));
									dateWithReminder=calReminder.getTime();
								}
								if(workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0){
									calEscalation.setTime(crDate);
									calEscalation.add(Calendar.HOUR, workflowDetail.getEscalation());
									dateWithEscalation=calEscalation.getTime();
								}
							}
							vo.setUseCase(workflowDetail.getWorkflow().getModuleProcess().getUseCase());
							vo.setCreatedDate(currentDate);
							vo.setStatus(Byte.parseByte("0"));
							vo.setPerson(workflowDetail.getUserRole().getUser().getPerson());
							
							//Is completed current flow
							WorkflowDetailProcess workflowDetailProcess=workFlowService.getWorkFlowDetailProcessInfo
								(workflowDetail.getWorkflowDetailId(), vo.getRecordId());
							
							//------------------------------Reminder area----------------------------------------
							//SMS
							if(dateWithReminder!=null && currentDate.compareTo(dateWithReminder)>0 && workflowDetailProcess==null 
									&& workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0
									&& workflowDetail.getIsSms()!=null && workflowDetail.getIsSms()==true){
							
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("1"),"REMINDER",
										workflowDetail.getUserRole().getUser().getPerson().getMobile());
				        	}
							//EMAIL
							if(dateWithReminder!=null && currentDate.compareTo(dateWithReminder)>0 && workflowDetailProcess==null 
									&& workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0
									&& workflowDetail.getIsEmail()!=null && workflowDetail.getIsEmail()==true){
								
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("2"),"REMINDER",
											workflowDetail.getUserRole().getUser().getPerson().getEmail());
				        	}
							//SYSTEM
							if(dateWithReminder!=null && currentDate.compareTo(dateWithReminder)>0 && workflowDetailProcess==null 
									&& workflowDetail.getReminder()!=null && workflowDetail.getReminder()>0
									&& workflowDetail.getIsSystem()!=null && workflowDetail.getIsSystem()==true){
								
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("3"),"REMINDER",null);
				        	}
							//------------Escalation area---------------------------------------------------------
							//SMS
							if(dateWithEscalation!=null && currentDate.compareTo(dateWithEscalation)>0 && workflowDetailProcess==null 
									&& workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0
									&& workflowDetail.getIsSms()!=null && workflowDetail.getIsSms()==true){
								
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("1"),"ESCALATION",
											workflowDetail.getUserRole().getUser().getPerson().getMobile());
				        	}
							//EMAIL
							if(dateWithEscalation!=null && currentDate.compareTo(dateWithEscalation)>0 && workflowDetailProcess==null 
									&& workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0
									&& workflowDetail.getIsEmail()!=null && workflowDetail.getIsEmail()==true){
								
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("2"),"ESCALATION",
											workflowDetail.getUserRole().getUser().getPerson().getEmail());
				        	}
							//SYSTEM
							if(dateWithEscalation!=null && currentDate.compareTo(dateWithEscalation)>0 && workflowDetailProcess==null 
									&& workflowDetail.getEscalation()!=null && workflowDetail.getEscalation()>0
									&& workflowDetail.getIsSystem()!=null && workflowDetail.getIsSystem()==true){
								
									manipulateMessage(messages,vo,workflowDetail,Byte.parseByte("3"),"ESCALATION",
											workflowDetail.getUserRole().getUser().getPerson().getEmail());
				        	}
							
						}
						systemService.saveMessages(messages);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	*/}
	public void manipulateMessage
	(List<Message> messages,Message message,WorkflowDetail workflowDetail,byte procType,String specifig,String mobilEmail) 
	throws Exception{/*
		Message tempMessage=new Message();
		Message alreadyExsitMessage=null;

		tempMessage.setRecordId(message.getRecordId());
		tempMessage.setUseCase(message.getUseCase());
		tempMessage.setCreatedDate(message.getCreatedDate());
		tempMessage.setStatus(Byte.parseByte("0"));
		tempMessage.setDataSpecific("WORKFLOW_"+specifig);
		tempMessage.setProcessType(procType);
		tempMessage.setToAddresses(mobilEmail);
		tempMessage.setMessage(specifig+" : A New "+workflowDetail.getWorkflow().getModuleProcess().getProcessTitle()
				+" has been added and waiting for next level process");
		if(specifig.equals("ESCALATION")){
			tempMessage.setPerson(workflowDetail.getUserRole().getUser().getPerson());
			alreadyExsitMessage=systemService.getMessageToAvoidDublicate
			(tempMessage.getRecordId(), tempMessage.getUseCase(),tempMessage.getPerson().getPersonId(), 
					"WORKFLOW_"+specifig,tempMessage.getProcessType());
			if(alreadyExsitMessage==null){
				messages.add(tempMessage);
			}
		}else{
			for(UserRoleCombination userRoleCombination:workflowDetail.getUserRoleCombinations()){
				if(userRoleCombination.getUserRole()!=null){
					tempMessage.setPerson(userRoleCombination.getUserRole().getUser().getPerson());
					alreadyExsitMessage=systemService.getMessageToAvoidDublicate
					(tempMessage.getRecordId(), tempMessage.getUseCase(),tempMessage.getPerson().getPersonId(), 
							"WORKFLOW_"+specifig,tempMessage.getProcessType());
					if(alreadyExsitMessage==null){
						messages.add(tempMessage);
					}
				}else if(userRoleCombination.getRole()!=null){
					for(UserRole userRole:userRoleCombination.getRole().getUserRoles()){
						Message tempMessage1=new Message();
						tempMessage1.setRecordId(message.getRecordId());
						tempMessage1.setUseCase(message.getUseCase());
						tempMessage1.setCreatedDate(message.getCreatedDate());
						tempMessage1.setStatus(Byte.parseByte("0"));
						tempMessage1.setDataSpecific("WORKFLOW_"+specifig);
						tempMessage1.setProcessType(procType);
						tempMessage1.setToAddresses(mobilEmail);
						tempMessage1.setMessage(specifig+" : A New "+workflowDetail.getWorkflow().getModuleProcess().getProcessTitle()
								+" has been added and waiting for next level process");
						tempMessage1.setPerson(userRole.getUser().getPerson());
						alreadyExsitMessage=systemService.getMessageToAvoidDublicate
						(tempMessage1.getRecordId(), tempMessage1.getUseCase(),tempMessage1.getPerson().getPersonId(), 
								"WORKFLOW_"+specifig,tempMessage1.getProcessType());
						if(alreadyExsitMessage==null){
							messages.add(tempMessage1);
						}
					}
				}
			}
		}
	*/}
	public WorkFlowService getWorkFlowService() {
		return workFlowService;
	}

	public void setWorkFlowService(WorkFlowService workFlowService) {
		this.workFlowService = workFlowService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	

}
