/**
 * 
 */
package com.aiotech.aios.workflow.to;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.workflow.domain.entity.Workflow;

/**
 * @author Usman
 * 
 */
public class WorkflowVO extends Workflow {

	List<WorkflowDetailVO> detailVOs = new ArrayList<WorkflowDetailVO>();

	public List<WorkflowDetailVO> getDetailVOs() {
		return detailVOs;
	}

	public void setDetailVOs(List<WorkflowDetailVO> detailVOs) {
		this.detailVOs = detailVOs;
	}

}
