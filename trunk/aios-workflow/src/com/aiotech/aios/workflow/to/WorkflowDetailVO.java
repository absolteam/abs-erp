package com.aiotech.aios.workflow.to;

import java.util.List;

import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;

public class WorkflowDetailVO extends WorkflowDetail {
	private static final long serialVersionUID = 1L;
	private long recordId;
	private String recordName;
	private Object object;
	private int processFlag;
	private String recordNumber;
	private String roleName;

	private String personName;
	private String designation;

	private String displayUsername;
	private String operationName;

	private List<WorkflowDetailVO> details;

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public int getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(int processFlag) {
		this.processFlag = processFlag;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDisplayUsername() {
		return displayUsername;
	}

	public void setDisplayUsername(String displayUsername) {
		this.displayUsername = displayUsername;
	}

	public List<WorkflowDetailVO> getDetails() {
		return details;
	}

	public void setDetails(List<WorkflowDetailVO> details) {
		this.details = details;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}
	
	

}
