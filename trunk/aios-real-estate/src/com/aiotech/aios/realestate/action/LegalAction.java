package com.aiotech.aios.realestate.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Legal;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.service.bl.LegalBL;
import com.aiotech.aios.realestate.to.LegalVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LegalAction extends ActionSupport {
	private static final Logger LOGGER = LogManager.getLogger(LegalAction.class);
	private LegalBL legalBL;
	Implementation implementation=null;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private long legalId;
	private String legalDetail;
	private String createdDate;
	private String createdPerson;
	private long createdBy;
	private long recordId;
	private String recordName;
	private String tableName;
	private String effectiveDate;
	private Long propertyId;
	
	private List<LegalVO> legalList=null;
	public void getImplementId(){
		HttpSession session = ServletActionContext.getRequest()
		.getSession();
		implementation=new Implementation();
		if(session.getAttribute("THIS")!=null){
			implementation=(Implementation)session.getAttribute("THIS");
			//implementationId=implementation.getImplementationId();
			//LOGGER.info("implementation id------>"+implementationId);
		}
	}
	public String returnSuccess() {
		return SUCCESS;
	}
	//Listing JSON
	public String execute() {

		try {
			
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/*public String usecaseBasedRecords() {
		HttpSession session = ServletActionContext.getRequest().getSession();

		try {
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 

			session.removeAttribute("LEGAL_INFO_"+sessionObj.get("jSessionId"));
			getImplementId();
			LegalVO legalvo = new LegalVO();
			legalvo.setTableName(tableName);
			legalList = new ArrayList<LegalVO>();
			legalList=legalBL.getUseCaseBasedRecords(legalvo, implementation);

			// Set the value to push to Personal Details DAO
			JSONObject jsonResponse = new JSONObject();
			getImplementId();
			//implementationId=(Long) session.get("THIS");
			if (legalList != null && legalList.size() > 0) {
				iTotalRecords = legalList.size();
				iTotalDisplayRecords = legalList.size();
			}
				jsonResponse.put("iTotalRecords", iTotalRecords);
				jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
				JSONArray data = new JSONArray();
				for (LegalVO list : legalList) {
					JSONArray array = new JSONArray();
					array.add(list.getRecordId());
					array.add(list.getRecordName());
					array.add(list.getRecordNumber());
					data.add(array);
				}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			
			ServletActionContext.getRequest().setAttribute("Table_Name",
					tableName);
			session.setAttribute("LEGAL_INFO_"+sessionObj.get("jSessionId"), legalList);
			return SUCCESS;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
	}*/
	public String recordView() {
		try {
			ServletActionContext.getRequest().setAttribute("Table_Name",
					tableName);
			return SUCCESS;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
	}
	public String legalSave() {
		try {
			HttpSession session = ServletActionContext.getRequest()
			.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			Legal legal=new Legal();
			legal.setTableName(tableName);
			Object object=null;
			legal.setRecordId(recordId);
			Property property=null;
			if(propertyId!=null && propertyId>0){
				property=new Property();
				property.setPropertyId(propertyId);
			}else{
				property=new Property();
				Contract contract=legalBL.getContractService().getContractPropertyInfoForLegal(recordId);
				if(contract!=null && contract.getOffer()!=null && contract.getOffer().getOfferDetails()!=null){
					List<OfferDetail> offerDetails=new ArrayList<OfferDetail>(contract.getOffer().getOfferDetails());
					Property temProp=new Property();
					temProp=offerDetails.get(0).getProperty();
					if(temProp!=null){
						
					}else if(offerDetails.get(0).getUnit()!=null && offerDetails.get(0).getUnit().getComponent()!=null){
						property=offerDetails.get(0).getUnit().getComponent().getProperty();
					}
				}
			}
			legal.setProperty(property);
			legal.setCreatedDate(new Date());
			legal.setEffectiveDate(DateFormat.convertStringToDate(effectiveDate));
			legal.setCreatedBy(user.getPersonId());
			legal.setDetails(legalDetail);
			object=legalBL.getUseCaseBasedRecords(legal);//Find objects
			legalBL.saveLegal(object,legal);
			ServletActionContext.getRequest().setAttribute("succMsg",
			"Legal Successfully Created");
			return SUCCESS;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Creation failure");
			return ERROR;
		}
	}
	
	public LegalBL getLegalBL() {
		return legalBL;
	}
	public void setLegalBL(LegalBL legalBL) {
		this.legalBL = legalBL;
	}
	
	
	public List<LegalVO> getLegalList() {
		return legalList;
	}
	public void setLegalList(List<LegalVO> legalList) {
		this.legalList = legalList;
	}


	public long getLegalId() {
		return legalId;
	}

	public void setLegalId(long legalId) {
		this.legalId = legalId;
	}

	public String getLegalDetail() {
		return legalDetail;
	}

	public void setLegalDetail(String legalDetail) {
		this.legalDetail = legalDetail;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedPerson() {
		return createdPerson;
	}

	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public Long getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(Long propertyId) {
		this.propertyId = propertyId;
	}
}
