package com.aiotech.aios.realestate.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.service.bl.PropertyComponentBL;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;
import com.aiotech.aios.realestate.to.converter.PropertyCompomentTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("unchecked")
public class PropertyComponentAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	// Common Variables
	private int componentId;
	private int propertyId;
	private int componentTypeId;
	private String componentName;
	private double componentSize;
	private int unitSize;
	private int componentNumber;
	private String rent;
	private int sizeUnit;
	private byte status;
	private String showPage;
	private String componentFeature;
	private String componentFeaturesName;
	private Long lookupDetailId;
	private String componentNameArabic;

	private String componentFeatureDetail;
	private String sqlReturnMessage;
	private int componentLineId;
	private long recordId;
	private int numberOfSize;
	private String messageId;

	// Grid Variables
	private Integer rows = 0;
	private Integer page = 0;
	private String sord = "desc";
	private String sidx;
	private String searchField;
	private String searchString;
	private String searchOper;
	private Integer total = 0;
	private Integer records = 0;
	private String oper;
	private Integer id;
	private String jsonResult;
	private String json;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	// Common object declarations
	private ComponentService componentService;
	private PropertyService propertyService;
	private List<PropertyCompomentTO> componentList = null;
	private PropertyCompomentTO componentTO = null;
	private PropertyComponentBL propertyComponentBL;
	private UnitService unitService;
	private CommentBL commentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private WorkflowBL workflowBL;
	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(PropertyComponentAction.class);

	private Implementation implementation = null;

	// Methods
	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	/** To redirect the action success page **/
	public String returnSuccess() {
		LOGGER.info("Module: Realestate : Method: returnSuccess");
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	/** To retrieve chart_of_account values into json grid values */

	public String showPropertyComponentList() {
		LOGGER.info("Module: Realestate : Method: showPropertyComponentList");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();
			componentTO = new PropertyCompomentTO();
			getImplementId();

			componentTO = new PropertyCompomentTO();
			List<Component> compList = componentService
					.getComponentList((Implementation) ServletActionContext
							.getRequest().getSession().getAttribute("THIS"));
			componentList = PropertyCompomentTOConverter
					.convertToTOList(compList);

			iTotalRecords = componentList.size();
			iTotalDisplayRecords = componentList.size();
			if (componentList != null && componentList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showPropertyComponentList, "
						+ " Component List size() " + componentList.size());
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			List<String> sizeUnit = new ArrayList<String>();
			sizeUnit.add("Square Feet");
			sizeUnit.add("Square Meter");
			sizeUnit.add("Square Yard");
			for (PropertyCompomentTO list : componentList) {

				if (list.getIsApproved() == Constants.RealEstate.Status.Deny
						.getCode())
					continue;

				JSONArray array = new JSONArray();
				array.add(list.getComponentId() + "");
				array.add(list.getPropertyId() + "");
				array.add(list.getComponentTypeId() + "");
				if (list.getComponentName() != null)
					array.add(list.getComponentName());
				else
					array.add("");
				if (list.getComponentType() != null)
					array.add(list.getComponentType());
				else
					array.add("");
				if (list.getPropertyName() != null)
					array.add(list.getPropertyName());
				else
					array.add("");
				// array.add(list.getComponentSize()+"");
				// array.add(list.getUnitSize()+"");
				if (list.getUnitSize() > 0) {
					switch (list.getUnitSize()) {
					case 1:
						array.add(list.getComponentSize() + " "
								+ sizeUnit.get(0));
						break;
					case 2:
						array.add(list.getComponentSize() + " "
								+ sizeUnit.get(1));
						break;
					case 3:
						array.add(list.getComponentSize() + " "
								+ sizeUnit.get(2));
						break;
					}
				} else {
					array.add(list.getComponentSize());
				}

				if (list.getStatus() == 1)
					array.add("AVAILABLE");
				else if (list.getStatus() == 2)
					array.add("PENDING");
				else if (list.getStatus() == 3)
					array.add("RENTED");
				else if (list.getStatus() == 4)
					array.add("SOLD");
				else if (list.getStatus() == 5)
					array.add("RENOVATION");
				else if (list.getStatus() == 6)
					array.add("OFFERED");
				else if (list.getStatus() == 0)
					array.add("PENDING");
				else
					array.add("PENDING");
				data.add(array);

			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Realestate : Method: showPropertyComponentList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: showPropertyComponentList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get the entry page
	public String showComponentEntry() {
		LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry");
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			session.removeAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showJournalVoucherAddEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get edit component
	public String showComponentEditEntry() {
		LOGGER.info("Module: Accounts : Method: showComponentEditEntry");
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			session.removeAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId"));

			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow

			if (recordId != 0)
				componentId = (int) recordId;

			componentList = new ArrayList<PropertyCompomentTO>();
			componentTO = new PropertyCompomentTO();
			if (componentId > 0) {
				Component component = componentService.getComponentById(Long
						.parseLong(componentId + ""));
				Property property = componentService.getPropertyById(component
						.getProperty().getPropertyId());
				ComponentType componentType = componentService
						.getComponentTypeById(component.getComponentType()
								.getComponentTypeId());
				component.setProperty(property);
				component.setComponentType(componentType);
				componentTO = PropertyCompomentTOConverter
						.convertEntityTOTo(component);

				List<ComponentDetail> componentDetailList = componentService
						.getComponentLines(Long.parseLong(componentId + ""));
				componentList = PropertyCompomentTOConverter
						.convertDetailToTOList(componentDetailList);
				session.setAttribute(
						"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
						componentDetailList);

				// Property Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (recordId != 0) {
					comment.setRecordId(Long.parseLong(componentId + ""));
					comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Component");
					comment = commentBL.getCommentInfo(comment);
				}
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
				if (messageId != null) {

					WorkflowDetailVO vo = workflowBL
							.getWorkflowDetailVObyMessageId(Long
									.parseLong(messageId));
					sessionObj.put("WORKFLOW_DETAILVO", vo);

				}

				// Comment process End
			}
			ServletActionContext.getRequest().setAttribute("COMPONENT_INFO",
					componentTO);
			ServletActionContext.getRequest().setAttribute(
					"COMPONENT_LINE_INFO", componentList);
			ServletActionContext.getRequest().setAttribute(
					"COMPONENT_INFO_SIZE", componentList.size());
			LOGGER.info("Module: Accounts : Method: showComponentEditEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Accounts : Method: showComponentEditEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get properties
	public String showProperties() {
		LOGGER.info("Module: Accounts : Method: showProperties");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();
			getImplementId();
			List<Property> propertyList = propertyService
					.getComponenetProperties(implementation);
			if (null != propertyList)
				componentList = PropertyCompomentTOConverter
						.convertToTOPropertyList(propertyList);
			iTotalRecords = componentList.size();
			iTotalDisplayRecords = componentList.size();
			if (componentList != null && componentList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showPropertyComponentList, "
						+ " Component List size() " + componentList.size());
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (PropertyCompomentTO list : componentList) {
				JSONArray array = new JSONArray();
				array.add(list.getPropertyId());
				array.add(list.getPropertyName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showProperties: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showProperties: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get properties
	public String showComponentProperties() {
		LOGGER.info("Module: Accounts : Method: showComponentProperties");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();
			getImplementId();
			List<Property> propertyList = propertyService
					.getComponenetProperties(implementation);
			if (null != propertyList)
				componentList = PropertyCompomentTOConverter
						.convertToTOPropertyList(propertyList);
			ServletActionContext.getRequest().setAttribute("PROPERTIES_LIST",
					componentList);
			LOGGER.info("Module: Accounts : Method: showComponentProperties: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showComponentProperties: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get all component types
	public String showComponentTypes() {
		LOGGER.info("Module: Accounts : Method: showComponentTypes");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();
			List<ComponentType> componentTypeList = componentService
					.getAllComponentTypes();
			if (null != componentTypeList)
				componentList = PropertyCompomentTOConverter
						.convertToTOTypeList(componentTypeList);
			iTotalRecords = componentList.size();
			iTotalDisplayRecords = componentList.size();
			if (componentList != null && componentList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showPropertyComponentList, "
						+ " Component List size() " + componentList.size());
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (PropertyCompomentTO list : componentList) {
				JSONArray array = new JSONArray();
				array.add(list.getComponentTypeId());
				array.add(list.getComponentType());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Accounts : Method: showComponentTypes: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showComponentTypes: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get all component types
	public String showComponentTypeList() {
		LOGGER.info("Module: Accounts : Method: showComponentTypeList");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();
			List<ComponentType> componentTypeList = componentService
					.getAllComponentTypes();
			if (null != componentTypeList)
				componentList = PropertyCompomentTOConverter
						.convertToTOTypeList(componentTypeList);
			if (componentList != null && componentList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showPropertyComponentList, "
						+ " Component List size() " + componentList.size());
			}
			ServletActionContext.getRequest().setAttribute(
					"COMPONENT_TYPELIST", componentList);
			LOGGER.info("Module: Accounts : Method: showComponentTypeList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showComponentTypeList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get the features
	public String showFeatureDetails() {
		LOGGER.info("Module: Accounts : Method: showFeatureDetails");
		try {
			componentList = new ArrayList<PropertyCompomentTO>();

			List<LookupMaster> lookupMaster = propertyComponentBL
					.getLookupMasterBL()
					.getLookupMasterService()
					.getLookupInformation("COMPONENT_FEATURE",
							getImplementation());
			if (lookupMaster != null && lookupMaster.size() > 0
					&& lookupMaster.get(0).getLookupDetails() != null) {
				for (LookupDetail lookupDetail : lookupMaster.get(0)
						.getLookupDetails()) {
					componentTO = new PropertyCompomentTO();
					componentTO.setComponentFeature(lookupDetail
							.getAccessCode() != null ? lookupDetail
							.getAccessCode() : "");
					componentTO
							.setComponentFeatureId(lookupDetail.getDataId() != null ? lookupDetail
									.getDataId() : 0);
					componentTO.setComponentFeatureDetails(lookupDetail
							.getDisplayName());
					componentTO.setLookupDetailId(lookupDetail
							.getLookupDetailId());
					componentList.add(componentTO);
				}
			}
			/*
			 * componentTO = new PropertyCompomentTO();
			 * componentTO.setComponentFeature("F-101");
			 * componentTO.setComponentFeatureId(1);
			 * componentTO.setComponentFeatureDetails("Near Airport");
			 * componentList.add(componentTO);
			 * 
			 * componentTO = new PropertyCompomentTO();
			 * componentTO.setComponentFeature("F-102");
			 * componentTO.setComponentFeatureId(2);
			 * componentTO.setComponentFeatureDetails("Swimming Pool");
			 * componentList.add(componentTO);
			 * 
			 * componentTO = new PropertyCompomentTO();
			 * componentTO.setComponentFeature("F-103");
			 * componentTO.setComponentFeatureId(3);
			 * componentTO.setComponentFeatureDetails("Parking");
			 * componentList.add(componentTO);
			 */
			ServletActionContext.getRequest().setAttribute(
					"componentfeatureList", componentList);
			LOGGER.info("Module: Accounts : Method: componentfeatureList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to get line entry
	public String showComponentLineDetails() {
		LOGGER.info("Module: Accounts : Method: showComponentLineDetails");
		try {
			ServletActionContext.getRequest().setAttribute("rowId", id);
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			LOGGER.info("Module: Accounts : Method: showComponentLineDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showComponentLineDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to add add lines save
	public String componentAddAddSave() {
		LOGGER.info("Module: Accounts : Method: componentAddAddSave");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ComponentDetail componentDetails = new ComponentDetail();
			List<ComponentDetail> componentList = null;
			/*
			 * componentDetails.setFeature(componentFeature + "#" +
			 * componentFeaturesName + "#" + componentFeatureDetail);
			 */

			componentDetails.setFeature(componentFeatureDetail);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(lookupDetailId);
			componentDetails.setLookupDetail(lookupDetail);
			if (session.getAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				componentList = new ArrayList<ComponentDetail>();
			else
				componentList = (List<ComponentDetail>) session
						.getAttribute("COMPONENT_INFO_"
								+ sessionObj.get("jSessionId"));
			componentList.add(componentDetails);

			session.setAttribute(
					"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
					componentList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentAddAddSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentAddAddSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to add edit lines save
	public String componentAddEditSave() {
		LOGGER.info("Module: Accounts : Method: componentAddEditSave");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ComponentDetail componentDetails = new ComponentDetail();
			List<ComponentDetail> componentList = null;
			/*
			 * componentDetails.setFeature(componentFeature + "#" +
			 * componentFeaturesName + "#" + componentFeatureDetail);
			 */
			componentDetails.setFeature(componentFeatureDetail);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(lookupDetailId);
			componentDetails.setLookupDetail(lookupDetail);
			componentList = (List<ComponentDetail>) session
					.getAttribute("COMPONENT_INFO_"
							+ sessionObj.get("jSessionId"));
			componentList.set(id - 1, componentDetails);

			session.setAttribute(
					"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
					componentList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentAddEditSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentAddEditSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to add delete lines save
	public String componentAddDelete() {
		LOGGER.info("Module: Accounts : Method: componentAddDelete");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			List<ComponentDetail> componentList = null;
			componentList = (List<ComponentDetail>) session
					.getAttribute("COMPONENT_INFO_"
							+ sessionObj.get("jSessionId"));

			/*
			 * if (componentList.get(id - 1).getComponentDetailId() != null)
			 * componentService
			 * .deletePropertyComponentDetail(componentList.get(id - 1));
			 */

			componentList.remove(id - 1);

			session.setAttribute(
					"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
					componentList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentAddDelete Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentAddDelete: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to discard components
	public String componentDiscard() {
		LOGGER.info("Module: Accounts : Method: componentDiscard");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			session.removeAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: componentDiscard Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentDiscard: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save add entry property component
	public String savePropertyComponent() {
		LOGGER.info("Module: Accounts : Method: savePropertyComponent");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			User user = (User) sessionObj.get("USER");

			Component component = new Component();
			Property property = new Property();

			Person person = new Person();
			person.setPersonId(user.getPersonId());

			component.setPerson(person);
			component.setCreatedDate(new Date());
			ComponentType componentType = new ComponentType();
			property.setPropertyId(Long.parseLong(propertyId + ""));
			componentType.setComponentTypeId(componentTypeId);
			component.setComponentType(componentType);
			component.setProperty(property);

			component.setComponentName(componentName);
			if (componentNameArabic != null)
				component.setComponentNameArabic(AIOSCommons
						.stringToUTFBytes(componentNameArabic));
			component.setComponentNo(componentNumber);
			component.setRent(Double.parseDouble(rent));
			component.setSize(componentSize);
			component.setSizeUnit(sizeUnit);
			component.setStatus(status);
			component.setNoOfUnits(numberOfSize);
			getImplementId();
			component.setImplementation(implementation);
			
				component.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));
			List<ComponentDetail> componentList = (ArrayList<ComponentDetail>) session
					.getAttribute("COMPONENT_INFO_"
							+ sessionObj.get("jSessionId"));
			
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

			// Component Detail Delete Process
			if (componentId > 0) {
				List<ComponentDetail> componentExsistDbList = new ArrayList<ComponentDetail>();
				componentExsistDbList = componentService.getComponentLines(Long
						.parseLong(componentId + ""));
				List<ComponentDetail> componentDeleteList = new ArrayList<ComponentDetail>();
				componentDeleteList = getComponentDetailSimiler(componentList,
						componentExsistDbList);
				componentService.deleteComponents(componentDeleteList);
				workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
						.getCode());
			}else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			

			componentService.savePropertyComponents(component, componentList);
			if (component.getRelativeId() == null) {
				component.setRelativeId(component.getComponentId());
				componentService.savePropertyComponents(component, null);
			}

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Component", component.getComponentId(), user,workflowDetailVo);

			// rejection case edit screen
			WorkflowDetailVO vo = null;
			if (sessionObj.containsKey("WORKFLOW_DETAILVO")) {
				vo = (WorkflowDetailVO) sessionObj.get("WORKFLOW_DETAILVO");

				sessionObj.remove("WORKFLOW_DETAILVO");
				workflowEnterpriseService.updateNotificationStatus(vo,
						vo.getRecordId(), (byte) 1, user);
			}

			for (ComponentDetail componentDetail : component
					.getComponentDetails())
				propertyComponentBL.saveUploadedImages(componentDetail, false);

			session.removeAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AIOS-img-" + "ComponentDetail-"
					+ session.getId());
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: savePropertyComponent Action Success");
			ServletActionContext.getRequest().setAttribute("successMsg",
					"Successfully Created");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			sqlReturnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			ServletActionContext.getRequest().setAttribute("errorMsg",
					"Creation Failure.");
			LOGGER.info("Module: Accounts : Method: savePropertyComponent: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to edit add lines save
	public String componentEditAddSave() {
		LOGGER.info("Module: Accounts : Method: componentEditAddSave");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ComponentDetail componentDetails = new ComponentDetail();
			Component component = new Component();
			List<ComponentDetail> componentList = null;
			/*
			 * componentDetails.setFeature(componentFeature + "#" +
			 * componentFeaturesName + "#" + componentFeatureDetail);
			 */
			componentDetails.setFeature(componentFeatureDetail);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(lookupDetailId);
			componentDetails.setLookupDetail(lookupDetail);
			component.setComponentId(Long.parseLong(componentId + ""));
			componentDetails.setComponent(component);
			if (session.getAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				componentList = new ArrayList<ComponentDetail>();
			else
				componentList = (List<ComponentDetail>) session
						.getAttribute("COMPONENT_INFO_"
								+ sessionObj.get("jSessionId"));
			componentList.add(componentDetails);

			session.setAttribute(
					"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
					componentList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentEditAddSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentEditAddSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to edit edit lines save
	public String componentEditEditSave() {
		LOGGER.info("Module: Accounts : Method: componentEditEditSave");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			ComponentDetail componentDetails = new ComponentDetail();
			Component component = new Component();
			List<ComponentDetail> componentList = null;
			/*
			 * componentDetails.setFeature(componentFeature + "#" +
			 * componentFeaturesName + "#" + componentFeatureDetail);
			 */

			componentDetails.setFeature(componentFeatureDetail);
			LookupDetail lookupDetail = new LookupDetail();
			lookupDetail.setLookupDetailId(lookupDetailId);
			componentDetails.setLookupDetail(lookupDetail);
			component.setComponentId(Long.parseLong(componentId + ""));
			componentDetails.setComponent(component);
			componentDetails.setComponentDetailId(Long
					.parseLong(componentLineId + ""));
			if (session.getAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				componentList = new ArrayList<ComponentDetail>();
			else
				componentList = (List<ComponentDetail>) session
						.getAttribute("COMPONENT_INFO_"
								+ sessionObj.get("jSessionId"));
			componentList.set(id - 1, componentDetails);

			session.setAttribute(
					"COMPONENT_INFO_" + sessionObj.get("jSessionId"),
					componentList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentEditEditSave Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: componentEditEditSave: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to save the entry
	public String componentUpdate() {
		LOGGER.info("Module: Accounts : Method: componentUpdate");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Component component = new Component();
			Property property = new Property();
			Component componentToEdit = componentService.getComponentById(Long
					.parseLong(componentId + ""));
			ComponentType componentType = new ComponentType();
			property.setPropertyId(Long.parseLong(propertyId + ""));
			componentType.setComponentTypeId(componentTypeId);
			component.setComponentType(componentType);
			component.setProperty(property);

			component.setComponentName(componentName);
			component.setComponentNameArabic(AIOSCommons
					.stringToUTFBytes(componentNameArabic));
			component.setComponentNo(componentNumber);
			component.setRent(Double.parseDouble(rent.replace(",", "")));
			component.setSize(componentSize);
			component.setSizeUnit(sizeUnit);
			component.setStatus(status);
			component.setNoOfUnits(numberOfSize);
			component.setComponentId(Long.parseLong(componentId + ""));
			getImplementId();
			component.setImplementation(implementation);

			if (componentToEdit.getIsApprove() == Constants.RealEstate.Status.Deny
					.getCode()) {
				component.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));
			} else {
				component.setIsApprove(componentToEdit.getIsApprove());
			}

			List<ComponentDetail> componentDetailList = (ArrayList<ComponentDetail>) session
					.getAttribute("COMPONENT_INFO_"
							+ sessionObj.get("jSessionId"));
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			// Component Detail Delete Process
			if (componentId > 0) {
				List<ComponentDetail> componentExsistDbList = new ArrayList<ComponentDetail>();
				componentExsistDbList = componentService.getComponentLines(Long
						.parseLong(componentId + ""));
				List<ComponentDetail> componentDeleteList = new ArrayList<ComponentDetail>();
				componentDeleteList = getComponentDetailSimiler(
						componentDetailList, componentExsistDbList);
				componentService.deleteComponents(componentDeleteList);
				workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
						.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}

			componentService.savePropertyComponents(component,
					componentDetailList);

			User user = (User) sessionObj.get("USER");
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Component", component.getComponentId(), user,
					workflowDetailVo);

			// rejection case edit screen
			WorkflowDetailVO vo = null;
			if (sessionObj.get("WORKFLOW_DETAILVO") != null) {

				vo = (WorkflowDetailVO) sessionObj.get("WORKFLOW_DETAILVO");
				sessionObj.remove("WORKFLOW_DETAILVO");

				workflowEnterpriseService.updateNotificationStatus(vo,
						vo.getRecordId(), (byte) 1, user);

			}

			for (ComponentDetail componentDetail : component
					.getComponentDetails())
				propertyComponentBL.saveUploadedImages(componentDetail, true);

			session.removeAttribute("COMPONENT_INFO_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AIOS-img-" + "ComponentDetail-"
					+ session.getId());

			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			ServletActionContext.getRequest().setAttribute("successMsg",
					"Successfully Modified.");

			LOGGER.info("Module: Accounts : Method: componentUpdate Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Failure";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			ServletActionContext.getRequest().setAttribute("errorMsg",
					"Modification Failure.");
			ex.printStackTrace();
			return ERROR;
		}
	}

	public List<ComponentDetail> getComponentDetailSimiler(
			List<ComponentDetail> componentDetails,
			List<ComponentDetail> componentDetailEdits) {
		List<ComponentDetail> componentDetailUpdates = new ArrayList<ComponentDetail>();

		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		for (ComponentDetail componentDetail1 : componentDetails) {
			if (componentDetail1.getComponentDetailId() != null
					&& componentDetail1.getComponentDetailId() > 0)
				listOne.add(componentDetail1.getComponentDetailId());
		}
		for (ComponentDetail componentDetail2 : componentDetailEdits) {
			if (componentDetail2.getComponentDetailId() != null
					&& componentDetail2.getComponentDetailId() > 0)
				listTwo.add(componentDetail2.getComponentDetailId());
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);

		// Delete Process
		for (Long lng : different) {
			for (ComponentDetail componentDetail3 : componentDetailEdits) {
				if (lng != null
						&& componentDetail3.getComponentDetailId().equals(lng)) {
					componentDetailUpdates.add(componentDetail3);
				}
			}
		}
		return componentDetailUpdates;
	}

	// to delete component entry
	public String componentDelete() {
		LOGGER.info("Module: Accounts : Method: componentDelete");
		try {
			// Component component=new Component();
			// component.setComponentName(componentName);
			// component.setComponentId(Long.parseLong(componentId+""));
			if (unitService.getUnitListComponentBased((long) componentId)
					.size() != 0) {
				ServletActionContext.getRequest().setAttribute(
						"COMPONENT_CHILD_EXISTS", 1);
				return SUCCESS;
			}
			Component component = componentService.getComponentById(Long
					.parseLong(componentId + ""));
			List<ComponentDetail> componentDList = new ArrayList<ComponentDetail>();
			componentDList = componentService.getComponentLines(Long
					.parseLong(componentId + ""));
			componentService.deletePropertyComponent(component, componentDList);
			sqlReturnMessage = "Successfully Deleted";
			ServletActionContext.getRequest().setAttribute("succMsg",
					sqlReturnMessage);
			ServletActionContext.getRequest().setAttribute(
					"COMPONENT_CHILD_EXISTS", 0);
			LOGGER.info("Module: Accounts : Method: componentDelete Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			sqlReturnMessage = "Can't Delete Information is in use already";
			ServletActionContext.getRequest().setAttribute("errMsg",
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: componentDelete: throws Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Getter&setters
	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public int getComponentTypeId() {
		return componentTypeId;
	}

	public void setComponentTypeId(int componentTypeId) {
		this.componentTypeId = componentTypeId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public double getComponentSize() {
		return componentSize;
	}

	public void setComponentSize(double componentSize) {
		this.componentSize = componentSize;
	}

	public int getUnitSize() {
		return unitSize;
	}

	public void setUnitSize(int unitSize) {
		this.unitSize = unitSize;
	}

	public int getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}

	public String getRent() {
		return rent;
	}

	public void setRent(String rent) {
		this.rent = rent;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getSearchOper() {
		return searchOper;
	}

	public void setSearchOper(String searchOper) {
		this.searchOper = searchOper;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getRecords() {
		return records;
	}

	public void setRecords(Integer records) {
		this.records = records;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public List<PropertyCompomentTO> getComponentList() {
		return componentList;
	}

	public void setComponentList(List<PropertyCompomentTO> componentList) {
		this.componentList = componentList;
	}

	public PropertyCompomentTO getComponentTO() {
		return componentTO;
	}

	public void setComponentTO(PropertyCompomentTO componentTO) {
		this.componentTO = componentTO;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public String getComponentFeature() {
		return componentFeature;
	}

	public void setComponentFeature(String componentFeature) {
		this.componentFeature = componentFeature;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public int getSizeUnit() {
		return sizeUnit;
	}

	public void setSizeUnit(int sizeUnit) {
		this.sizeUnit = sizeUnit;
	}

	public int getComponentLineId() {
		return componentLineId;
	}

	public void setComponentLineId(int componentLineId) {
		this.componentLineId = componentLineId;
	}

	public int getITotalRecords() {
		return iTotalRecords;
	}

	public void setITotalRecords(int totalRecords) {
		iTotalRecords = totalRecords;
	}

	public int getITotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setITotalDisplayRecords(int totalDisplayRecords) {
		iTotalDisplayRecords = totalDisplayRecords;
	}

	public String getComponentFeaturesName() {
		return componentFeaturesName;
	}

	public void setComponentFeaturesName(String componentFeaturesName) {
		this.componentFeaturesName = componentFeaturesName;
	}

	public String getComponentFeatureDetail() {
		return componentFeatureDetail;
	}

	public void setComponentFeatureDetail(String componentFeatureDetail) {
		this.componentFeatureDetail = componentFeatureDetail;
	}

	public PropertyComponentBL getPropertyComponentBL() {
		return propertyComponentBL;
	}

	public void setPropertyComponentBL(PropertyComponentBL propertyComponentBL) {
		this.propertyComponentBL = propertyComponentBL;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	private ImageBL imageBL;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	// jasper Datasourc;
	private List<PropertyCompomentTO> componentDS;
	private String format;

	/*
	 * private List<ImageVO> populatePropertyDetailImgs(Component comp) throws
	 * IOException, DataFormatException { Set<ComponentDetail> compDetails =
	 * comp.getComponentDetails(); List<ImageVO> imgList = new
	 * ArrayList<ImageVO>(); for (ComponentDetail detail : compDetails) { Image
	 * img = new Image(); img.setRecordId(detail.getComponentDetailId());
	 * img.setTableName("ComponentDetail");
	 * imgList.addAll(imageBL.retrieveCopiedImages(img)); } return imgList; }
	 * 
	 * public String getComponentDetail() { try { componentDS = new
	 * ArrayList<PropertyCompomentTO>(); if(recordId!=0)
	 * componentId=(int)recordId;
	 * 
	 * Component component = new Component(); component.setComponentId((long)
	 * componentId); PropertyCompomentTO to = propertyComponentBL
	 * .getComponentDetails(component); PropertyCompomentTO cmpTO =
	 * PropertyCompomentTOConverter .convertToTO(to.getComponent());
	 * 
	 * cmpTO.setCompDetailTOList(PropertyCompomentTOConverter
	 * .convertDetailToTOList(to.getComponentDetails()));
	 * cmpTO.setCompDetailImgs(populatePropertyDetailImgs(to .getComponent()));
	 * Image image = new Image();
	 * image.setRecordId(to.getComponent().getComponentId());
	 * image.setTableName("Component"); List<ImageVO> imgList =
	 * imageBL.retrieveCopiedImages(image); cmpTO.setCompImgs(imgList);
	 * cmpTO.setLinkLabel("HTML".equals(getFormat())? "Download" : "");
	 * cmpTO.setAnchorExpression("HTML".equals(getFormat())?
	 * "downloadPropertyCompDetail.action?componentId="+ componentId +
	 * "&format=PDF" : "#");
	 * //cmpTO.setAnchorExpression("downloadPropertyCompDetail.action?componentId="
	 * + componentId + "&format=PDF"); //Approve Content
	 * cmpTO.setApproveLinkLabel("Approve");
	 * cmpTO.setApproveAnchorExpression("workflowProcess.action?processFlag="
	 * +Byte.parseByte(Constants.RealEstate.Status.Approved.getCode()+""));
	 * //Disapprove Content cmpTO.setRejectLinkLabel("Reject");
	 * cmpTO.setRejectAnchorExpression
	 * ("workflowProcess.action?processFlag="+Byte
	 * .parseByte(Constants.RealEstate.Status.Disapproved.getCode()+""));
	 * componentDS.add(cmpTO); String ctxRealPath =
	 * ServletActionContext.getServletContext() .getRealPath("/");
	 * jasperRptParams.put("AIOS_LOGO", "file:///" +
	 * ctxRealPath.concat(File.separator).concat( "images" + File.separator +
	 * "aiotech.jpg")); if (componentDS != null && componentDS.size() == 0) {
	 * componentDS.add(cmpTO); } else { componentDS.set(0, cmpTO); } } catch
	 * (Exception e) { e.printStackTrace(); } return SUCCESS; }
	 */

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public List<PropertyCompomentTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<PropertyCompomentTO> componentDS) {
		this.componentDS = componentDS;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public int getNumberOfSize() {
		return numberOfSize;
	}

	public void setNumberOfSize(int numberOfSize) {
		this.numberOfSize = numberOfSize;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	public String getComponentNameArabic() {
		return componentNameArabic;
	}

	public void setComponentNameArabic(String componentNameArabic) {
		this.componentNameArabic = componentNameArabic;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}

}
