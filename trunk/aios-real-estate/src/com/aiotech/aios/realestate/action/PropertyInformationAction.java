package com.aiotech.aios.realestate.action;

/*******************************************************************************************
 *
 * Create Log
 * -------------------------------------------------------------------------------------------
 * Ver 		Created Date 	Created By                 	Description
 * -------------------------------------------------------------------------------------------
 * 1.0		25 Feb 2012 	Muhammad Haris.	 		   	Initial Version
 *********************************************************************************************/

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Maintenance;
import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.vo.MaintenanceVO;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.service.bl.PropertyInformationBL;
import com.aiotech.aios.realestate.service.bl.UnitBL;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PropertyInformationAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PropertyInformationAction.class);

	private PropertyService propertyService;
	
	private WorkflowBL workflowBL;
	private String messageId;

	private PersonService personService;
	private SystemService systemService;
	private ComponentService componentService;
	private UnitService unitService;

	private CombinationBL combinationBL;

	private PropertyInformationBL propertyInfoBL;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private DirectoryBL directoryBL;
	private UnitBL unitBL;
	private CommentBL commentBL;

	private Integer buildingId;
	private String buildingName;
	private String buildingNameArabic;
	private String propertyCode;
	private String propertyCodeArabic;

	private int propertyTypeId;
	private String propertyTypeName;
	private String propertyTypeCode;
	private String floorNo;
	private String plotNo;
	private String addressLine1;
	private String addressLine2;
	private String fromDate;
	private String propertyTypeNature;
	private String toDate;
	private Integer yearOfBuild;
	private String countryCode;
	private int countryId;
	private String countryName;
	private String stateName;
	private int stateId;
	private String cityName;
	private int cityId;
	private String districtName;
	private int districtId;
	private String landSize;
	private String propertyTax;
	private String marketValue;
	private String image;
	private byte propStatus;
	private String propStatusCode;
	private int propStatusId;
	private String measurementCode;
	private String measurementValue;
	private String codeCombination;
	private String format;
	private String rentPerYr;
	private int propertyInfoOwnerId;
	private int ownerId;
	private String ownerName;
	private String ownerNameArabic;

	private int currentOwnerId;
	private String currentOwnerType;
	private int ownerTypeId;
	private String ownerType;
	private double percentage;
	private String uaeNo;
	private int propertyInfoFeatureId;
	private int featureId;
	private String featureCode;
	private String features;
	private String measurements;
	private String remarks;
	private int propertyComponentId;
	private int componentType;
	private String componentTypeName;
	private int noOfComponent;
	private int noOfFlatInComponent;
	private String totalArea;
	private long maintenanceCompanyId;
	private String maintenanceCompanyDetails;
	private String maintenanceCompanyName;
	private long workflowDetailId;
	private long recordId;
	private int companyId;
	private long unitRecordId;
	private long managementDecisionId;
	private String managementDecision;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String siteplanReceiver;
	private String returnMessage;
	private String accountNo;
	private String clearanceType;
	private String isUnitClearance;
	private String generalServiceType;
	private String accessCode;
	private Long lookupDetailId;
	private Long propertyDetailId;
	String jsonResult;
	String json;

	boolean editingProperty = false;
	String acMeter;
	String serviceMeter;
	String fireMeter;

	Implementation implementation;
	private Integer buildingSizeUnit;
	private Double buildingSize;
	private Byte isApprove;
	private int legalProcess;
	private InputStream fileInputStream;
	private boolean isServerSideValidation;
	private String feature;
	// accounts

	private int combinationId;
	private int revenueId;
	private String id;
	private String addEditFlag;
	RePropertyInfoTO propertyInfoTO = null;
	private List<RePropertyInfoTO> propertyList;
	private ManagementDecision decision = null;
	private static final Properties COMMON_MESSAGE = new Properties();
	static {
		try {
			COMMON_MESSAGE.load(PropertyInformationAction.class
					.getClassLoader().getResourceAsStream(
							"common.message.properties"));
		} catch (IOException e) {
			LOGGER.error("Module: Real Estate : COMMON_MESSAGE exception = \n"
					+ e + "!!");
		}
	}

	public String returnSuccess() {
		ServletActionContext.getRequest().setAttribute("rowid", id);
		ServletActionContext.getRequest().setAttribute("AddEditFlag",
				addEditFlag);
		ServletActionContext.getRequest().setAttribute("LEGAL_PROCESS",
				legalProcess);
		return SUCCESS;
	}

	public String execute() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			List<Property> properties = propertyInfoBL.getPropertyService()
					.getAllProperties(getImplementation());

			/*
			 * propertyList = RePropertyInfoTOConverter
			 * .convertToTOList(properties);
			 */

			propertyList = propertyInfoBL.propertyOwnerDetailInfo(properties);
			if (propertyList != null && propertyList.size() > 0) {
				LOGGER.info("Property Info details record length-->"
						+ propertyList.size());
			}

			iTotalRecords = propertyList.size();
			iTotalDisplayRecords = propertyList.size();

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (RePropertyInfoTO property : propertyList) {

				JSONArray array = new JSONArray();
				String tempLegal = "";
				String tempDecision = "";
				if (property.getLegallist() != null
						&& property.getLegallist().size() > 0)
					tempLegal = "<span title='Legal Information' class='legal_icon_view'></span>";

				if (property.getManagementDecisions() != null
						&& !property.getManagementDecisions().equals(""))
					tempDecision = "<span title='Management Decision' class='decision_icon_view'></span>";
				array.add(property.getBuildingId().toString());
				if (property.getBuildingName() != null)
					array.add(property.getBuildingName() + tempLegal + " "
							+ tempDecision);
				else
					array.add("");

				String proType = propertyService.getPropertyTypeById(
						property.getPropertyTypeId()).getType();
				if (proType != null)
					array.add(proType);
				else
					array.add("");

				if (property.getRentPerYear() != null)
					array.add(property.getRentPerYear());
				else
					array.add(0.0);

				if (property.getOwners() != null)
					array.add(property.getOwners());
				else
					array.add("");
				if (property.getOwnersArabic() != null)
					array.add(property.getOwnersArabic());
				else
					array.add("");
				if (property.isStatus() == 1)
					array.add("AVAILABLE");
				else if (property.isStatus() == 2)
					array.add("PENDING");
				else if (property.isStatus() == 3)
					array.add("RENTED");
				else if (property.isStatus() == 4)
					array.add("SOLD");
				else if (property.isStatus() == 5)
					array.add("RENOVATION");
				else if (property.isStatus() == 6)
					array.add("OFFERED");
				else if (property.isStatus() == 0)
					array.add("PENDING");
				else
					array.add("PENDING");
				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getSiteplanInfo() {

		try {
			Property property = propertyService.getPropertyById(buildingId);
			Map<String, String> sitePlanInfo = new HashMap<String, String>();
			property.setSiteplanRecipient(siteplanReceiver);
			buildingName = property.getPropertyName();
			addressLine1 = property.getAddressOne();
			addressLine2 = property.getAddressTwo();
			plotNo = property.getPlotNo();
			siteplanReceiver = AIOSCommons.translatetoArabic(siteplanReceiver);
			List<Property> list = new ArrayList<Property>();
			list.add(property);
			List<RePropertyInfoTO> temp = propertyInfoBL
					.propertyOwnerDetailInfo(list);
			ownerNameArabic = temp.get(0).getOwnersArabic();

			sitePlanInfo.put("buildingName", buildingName);
			sitePlanInfo.put("ownerName", ownerNameArabic);
			sitePlanInfo.put("addressLine1", addressLine1);
			sitePlanInfo.put("addressLine1", plotNo);
			sitePlanInfo.put("addressLine1", addressLine2);
			sitePlanInfo.put("siteplanReceiver", siteplanReceiver);
			ServletActionContext.getRequest().getSession()
					.setAttribute("SITEPLAN_INFO", sitePlanInfo);
			propertyService.updateProperty(property);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	public String getClearanceOrTransferInfo() {

		if (buildingId == null || buildingId == 0)
			return ERROR;
		if (isUnitClearance.contains("1"))
			return unitClearanceInfo();
		else
			return propertyClearanceInfo();
	}

	private String unitClearanceInfo() {

		try {
			Unit unit = unitBL.getUnitbyId(buildingId);
			if (unit == null) {

				buildingName = "";
				addressLine1 = "";
				ownerName = "";
				accountNo = "";
				return ERROR;
			}

			buildingName = unit.getComponent().getProperty().getPropertyName()
					.concat(" - " + unit.getUnitName());
			List<Property> list = new ArrayList<Property>();
			Map<String, String> clearanceInfo = new HashMap<String, String>();
			addressLine1 = unit.getComponent().getProperty().getAddressOne();
			list.add(unit.getComponent().getProperty());
			List<RePropertyInfoTO> temp = propertyInfoBL
					.propertyOwnerDetailInfo(list);
			ownerName = temp.get(0).getOwnersArabic();

			clearanceInfo.put("buildingName", buildingName);
			clearanceInfo.put("addressLine1", addressLine1);
			clearanceInfo.put("clearanceType", clearanceType);
			clearanceInfo.put("accountNo", accountNo);
			clearanceInfo.put("ownerName", ownerName);
			ServletActionContext.getRequest().getSession()
					.setAttribute("CLEARANCE_INFO", clearanceInfo);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	private String propertyClearanceInfo() {

		if (buildingId == null || buildingId == 0)
			return ERROR;

		try {
			Property property = propertyService.getPropertyById(buildingId);
			List<Property> list = new ArrayList<Property>();
			Map<String, String> clearanceInfo = new HashMap<String, String>();
			buildingName = property.getPropertyName();
			addressLine1 = property.getAddressOne();
			list.add(property);
			List<RePropertyInfoTO> temp = propertyInfoBL
					.propertyOwnerDetailInfo(list);
			ownerName = temp.get(0).getOwnersArabic();

			clearanceInfo.put("buildingName", buildingName);
			clearanceInfo.put("addressLine1", addressLine1);
			clearanceInfo.put("clearanceType", clearanceType);
			clearanceInfo.put("accountNo", accountNo);
			clearanceInfo.put("ownerName", ownerName);
			ServletActionContext.getRequest().getSession()
					.setAttribute("CLEARANCE_INFO", clearanceInfo);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	public String showAddScreen() {
		try {
			List<PropertyType> propertyTypes = propertyInfoBL
					.getPropertyService().getAllPropertyTypes();
			ServletActionContext.getRequest().setAttribute("propertyTypes",
					propertyTypes);
			resetBeanValues();
			removeSessionEntries(false);
			ServletActionContext.getRequest().setAttribute("ACCOUNT_INFO",
				"TRUE");
			ServletActionContext.getRequest().setAttribute("implemenation",
					getImplementation());
			editingProperty = false;
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public void resetBeanValues() {
		this.buildingId = null;
		this.buildingName = "";
		this.buildingNameArabic = "";
		this.plotNo = "";
		this.floorNo = "";
		this.addressLine1 = "";
		this.addressLine2 = "";
		this.countryId = 0;
		this.stateId = 0;
		this.cityId = 0;
		this.districtId = 0;
		this.propertyTypeId = 0;
		this.fromDate = "";
		this.toDate = "";
		this.landSize = "";
		this.measurementCode = "";
		this.propStatus = 1;
		this.propertyTax = "";
		this.marketValue = "";
		this.yearOfBuild = null;
		this.rentPerYr = "";
		this.acMeter = "";
		this.serviceMeter = "";
		this.buildingSize = 0.0;
		this.fireMeter = "";
		this.managementDecisionId = 0;
		this.managementDecision = "";
		this.propertyCode = "";
		this.propertyCodeArabic = "";
	}

	public String getOwners() throws Exception {

		// Get companies
		List<Company> companies = propertyService
				.getAllOwnerCompanies(getImplementation());

		// Get People (Persons) with type = owner
		List<Person> personOwners = propertyService
				.getAllPeopleWithTypeOwner(getImplementation());

		List<RePropertyInfoTO> ownersList = new ArrayList<RePropertyInfoTO>();

		if (companies != null) {
			for (Company company : companies) {

				propertyInfoTO = new RePropertyInfoTO();
				propertyInfoTO.setOwnerId(company.getCompanyId());
				propertyInfoTO.setOwnerName(company.getCompanyName());
				//propertyInfoTO.setOwnerTypeId(company.getCompanyType());
				propertyInfoTO.setOwnerType("COMPANY");
				propertyInfoTO.setUaeNo(company.getTradeNo());

				ownersList.add(propertyInfoTO);
			}
		}

		if (personOwners != null) {
			for (Person person : personOwners) {

				propertyInfoTO = new RePropertyInfoTO();
				propertyInfoTO.setOwnerId(Integer.parseInt(person.getPersonId()
						.toString()));
				propertyInfoTO.setOwnerName(person.getFirstName() + " "
						+ person.getLastName());
				propertyInfoTO.setOwnerType("PERSON");
				try {
					propertyInfoTO.setUaeNo(personService
							.getAllIdentity(person).get(0).getIdentityNumber());
				} catch (Exception e) {
					e.printStackTrace();
				}

				ownersList.add(propertyInfoTO);
			}
		}

		ServletActionContext.getRequest().setAttribute("itemList", ownersList);

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String addOwner(HttpSession session) {

		List<PropertyOwnership> owners;
		PropertyOwnership owner = new PropertyOwnership();
		owner.setPersonalId((long) getOwnerId());
		owner.setDetails(getOwnerType());
		owner.setPercentage(getPercentage());

		if (session.getAttribute("PROPERTY_OWNERS") == null) {
			owners = new ArrayList<PropertyOwnership>();
		} else {
			owners = (List<PropertyOwnership>) session
					.getAttribute("PROPERTY_OWNERS");
		}

		owners.add(owner);
		session.setAttribute("PROPERTY_OWNERS", owners);
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String addEditOwner() throws Exception {

		HttpSession session = ServletActionContext.getRequest().getSession();
		Boolean ownersEdited = false;
		List<PropertyOwnership> owners = (List<PropertyOwnership>) session
				.getAttribute("PROPERTY_OWNERS");
		int index = 0;

		if (owners != null) {
			for (PropertyOwnership ownership : owners) {

				if ((ownership.getPersonalId() == getCurrentOwnerId())
						&& (ownership.getDetails().contains(currentOwnerType))) {

					ownership.setPersonalId((long) getOwnerId());
					ownership.setDetails(getOwnerType());
					ownership.setPercentage(getPercentage());
					/*
					 * if (ownership.getPropertyOwnershipId() != null) {
					 * propertyService.updatePropertyOwner(ownership); }
					 */

					owners.set(index, ownership);
					ownersEdited = true;
					break;
				}
				index++;
			}
		}
		if (ownersEdited)
			session.setAttribute("PROPERTY_OWNERS", owners);
		else
			addOwner(session);

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String deleteOwner() throws Exception {

		HttpSession session = ServletActionContext.getRequest().getSession();
		List<PropertyOwnership> owners = (List<PropertyOwnership>) session
				.getAttribute("PROPERTY_OWNERS");
		int index = 0;

		for (PropertyOwnership ownership : owners) {

			if ((ownership.getPersonalId() == getOwnerId())
					&& (ownership.getDetails().contains(getOwnerType()))) {

				/*
				 * if (ownership.getPropertyOwnershipId() != null) {
				 * propertyService.deleteOwnership(ownership); }
				 */

				owners.remove(index);
				break;
			}
			index++;
		}

		session.setAttribute("PROPERTY_OWNERS", owners);
		return SUCCESS;
	}

	public String getFeatureDetails() {

		List<RePropertyInfoTO> featuresList = new ArrayList<RePropertyInfoTO>();

		featuresList = getFeatureAll();

		ServletActionContext.getRequest()
				.setAttribute("itemList", featuresList);

		return SUCCESS;

	}
	
	public String featureSave() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			PropertyDetail propertyDetail = new PropertyDetail();
			List<PropertyDetail> addedFeatures = null;
			propertyDetail.setPropertyDetailId(getPropertyDetailId());
			propertyDetail.setFeature(getFeature());
			LookupDetail lookupDetail=new LookupDetail();
			lookupDetail.setLookupDetailId(getLookupDetailId());
			propertyDetail.setLookupDetail(lookupDetail);
			
			if (session.getAttribute("PROPERTY_FEATURES") == null) {
				addedFeatures = new ArrayList<PropertyDetail>();
			} else {
				addedFeatures = (List<PropertyDetail>) session
						.getAttribute("PROPERTY_FEATURES");
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				addedFeatures.add(propertyDetail);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				addedFeatures.set(Integer.parseInt(id) - 1, propertyDetail);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				addedFeatures.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"PROPERTY_FEATURES",
					addedFeatures);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}




	public String getMaintenanceDetails() throws Exception {

		List<MaintenanceVO> maintenanceCompanyList = new ArrayList<MaintenanceVO>();
		List<Company> maintenanceCompanies = propertyService
				.getAllMaintenanceCompanies(getImplementation());
		MaintenanceVO propertyMaintenance = null;

		for (Company company : maintenanceCompanies) {

			propertyMaintenance = new MaintenanceVO();
			propertyMaintenance.setCompanyId((long) company.getCompanyId());
			propertyMaintenance.setMaintenanceCompanyName(company
					.getCompanyName());
			propertyMaintenance.setDetails(company.getDescription());
			propertyMaintenance.setMaintenanceCompanyTradeId(company
					.getTradeNo());

			maintenanceCompanyList.add(propertyMaintenance);
		}

		ServletActionContext.getRequest().setAttribute("itemList",
				maintenanceCompanyList);

		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String savePropertyInfo() {
		isServerSideValidation=false;
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		long documentRecId = -1;
		Boolean isSessionEntry = false;
		try {
			Property propertyToEdit = propertyService
					.getPropertyById((long) buildingId);

			propertyInfoTO.setPropertyTypeId(propertyTypeId);

			if (buildingId != null && buildingId == 0)
				buildingId = null;
			else
				propertyInfoTO.setBuildingId(buildingId);

			propertyInfoTO.setBuildingName(buildingName);
			propertyInfoTO.setPropertyTypeName(propertyTypeName);
			propertyInfoTO.setFloorNo(floorNo);
			propertyInfoTO.setPlotNo(plotNo);
			propertyInfoTO.setAddressLine1(addressLine1);
			propertyInfoTO.setAddressLine2(addressLine2);
			propertyInfoTO.setCountryCode(countryCode);
			propertyInfoTO.setStateName(stateName);
			propertyInfoTO.setCityName(cityName);
			propertyInfoTO.setDistrictName(districtName);
			propertyInfoTO.setFromDate(fromDate);
			propertyInfoTO.setToDate(toDate);
			propertyInfoTO.setPropStatus(propStatus);
			propertyInfoTO.setLandSize(landSize);
			propertyInfoTO.setFromDate(fromDate);
			propertyInfoTO.setToDate(toDate);
			propertyInfoTO.setPropStatusId(propStatusId);
			propertyInfoTO.setLandSize(landSize);
			propertyInfoTO.setPropertyTax(propertyTax);
			propertyInfoTO.setYearOfBuild(yearOfBuild);
			propertyInfoTO.setBuildingSize(AIOSCommons
					.roundTwoDecimals(buildingSize));
			propertyInfoTO.setBuildingSizeUnit(buildingSizeUnit);
			propertyInfoTO.setMeasurementCode(measurementCode);
			propertyInfoTO.setServiceMeter(serviceMeter);
			propertyInfoTO.setAcMeter(acMeter);
			propertyInfoTO.setFireMeter(fireMeter);
			propertyInfoTO.setPropertyCode(propertyCode);
			propertyInfoTO.setPropertyCodeArabic(propertyCodeArabic);
			propertyInfoTO.setBuildingNameArabic(buildingNameArabic);

			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));
			
			
			//Added by Rafiq to understand the Duplicate Property Name
			Account propertyAccount = combinationBL.getCombinationService().getAccountDetail(buildingName, getImplementation());
			if (propertyAccount != null
					&& propertyAccount.getAccountId() != null
					&& (buildingId == null || buildingId == 0)) {
				isServerSideValidation = true;
				ServletActionContext
						.getRequest()
						.setAttribute(
								"RETURN_ERROR_MESSAGE",
								"Property Name (or) Chart Of Account '"
										+ buildingName
										+ "' already exist, please provide unique information.");
				return ERROR;
			}

			try {
				propertyInfoTO.setMarketValue(marketValue);
			} catch (Exception e) {
				propertyInfoTO.setMarketValue("0");
			}

			try {
				propertyInfoTO.setRentPerYear(rentPerYr);
			} catch (Exception e) {
				propertyInfoTO.setRentPerYear("0");
			}

			if (buildingId != null && buildingId > 0) {
				List<PropertyOwnership> ownersSessionList = (List<PropertyOwnership>) session
						.getAttribute("PROPERTY_OWNERS");
				List<PropertyDetail> featureSessionList = (List<PropertyDetail>) session
						.getAttribute("PROPERTY_FEATURES");
				this.getPropertyInfoBL().checkOwnersDeleteList(
						ownersSessionList, Long.valueOf(buildingId));
				this.getPropertyInfoBL().checkFeatureDeleteList(
						featureSessionList, Long.valueOf(buildingId));
			}
			// Owners
			propertyInfoTO
					.setPropertyOwnerships(new HashSet<PropertyOwnership>(
							(List<PropertyOwnership>) session
									.getAttribute("PROPERTY_OWNERS")));

			// Maintenance
			try {
				Maintenance propertyMaintenance = new Maintenance();
				propertyMaintenance.setCompanyId((long) getCompanyId());
				propertyMaintenance.setDetails(maintenanceCompanyDetails);
				ArrayList<Maintenance> propertyMaintenanceList = new ArrayList<Maintenance>();
				propertyMaintenanceList.add(propertyMaintenance);

				propertyInfoTO.setMaintenances(new HashSet<Maintenance>(
						propertyMaintenanceList));
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Features
			if(session.getAttribute("PROPERTY_FEATURES")!=null)
				propertyInfoTO.setPropertyDetails(new HashSet<PropertyDetail>(
					(List<PropertyDetail>) session
							.getAttribute("PROPERTY_FEATURES")));

			Property propertyToAdd = RePropertyInfoTOConverter
					.convertToEntity(propertyInfoTO);
			propertyToAdd.setImplementation(getImplementation());

			// For account integration
			Transaction transaction = new Transaction();
			if (buildingId != null && !buildingId.equals("")) {
				Property property1 = propertyInfoBL.getPropertyService()
						.getPropertyById(buildingId);
				propertyToAdd.setPerson(property1.getPerson());
				propertyToAdd.setCreatedDate(property1.getCreatedDate());
				if (propertyToEdit.getIsApprove() == Constants.RealEstate.Status.Deny
						.getCode()) {
					propertyToAdd.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
				} else {
					propertyToAdd.setIsApprove(propertyToEdit.getIsApprove());
				}
				if (managementDecision != null
						&& !managementDecision.equals("")) {
					decision = new ManagementDecision();
					decision.setDecisionDetail(managementDecision);
				}
				propertyInfoBL.addProperty(propertyToAdd, transaction,
						decision, true);

				if (propertyTypeId == Constants.RealEstate.PropertyType.Villa
						.getCode()
						|| propertyTypeId == Constants.RealEstate.PropertyType.Flat
								.getCode()
						|| propertyTypeId == Constants.RealEstate.PropertyType.Shop
								.getCode()) {

					try {
						long recordId = 0;
						unitBL.getUnit(unitRecordId, recordId, false);
					} catch (Exception e) {
						e.printStackTrace();
						LOGGER.error("ERROR GETTING UNIT RECORD AGAINEST 'unitRecordId' :: IN PROPERTY EDIT ACTION METHOD");
					}
				}
			} else {
				/*if(!(propertyTypeNature).equalsIgnoreCase("empty land")) {
					// Create analysis account(Revenue)
					propertyToAdd.setPerson(user.getPerson());
					propertyToAdd.setCreatedDate(new Date());
	
					final Integer ASSET_ACCOUNT = 1;
					final Integer REVENUE_ACCOUNT = 3;
					final Integer EXPENSE_ACCOUNT = 4;
	
					Combination costCombination = combinationBL
							.createCostCenter(
									buildingName + " (" + propertyTypeNature + ")",
									true); // COST CENTER
					Combination assetCombination = combinationBL
							.createNaturalAccount(costCombination, buildingName,
									ASSET_ACCOUNT, true); // NATURAL ACCOUNT(ASSET)
	
					Combination revenueCombination = combinationBL
							.createNaturalAccount(costCombination,
									"Rent Income " + buildingName, REVENUE_ACCOUNT,
									true); // NATURAL ACCOUNT(REVENUE)
	
					Combination expenseCombination = combinationBL
							.createNaturalAccount(costCombination,
									"Expense " + buildingName, EXPENSE_ACCOUNT,
									true); // NATURAL ACCOUNT(EXPENSE)
	
					Combination revenueAnalysis = new Combination();
					revenueAnalysis.setCombinationId(revenueCombination
							.getCombinationId());
					revenueAnalysis = combinationBL
							.createAnalysisCombination(revenueAnalysis,
									"Other Income " + buildingName, true); // OTHER
																			// INCOME(UNDER
																			// REVENUE)
	
					Combination expenseAnalysisMaintainance = new Combination();
					expenseAnalysisMaintainance.setCombinationId(expenseCombination
							.getCombinationId());
					expenseAnalysisMaintainance = combinationBL
							.createAnalysisCombination(expenseAnalysisMaintainance,
									"Maintainance " + buildingName, true); // MAINTAINANCE(UNDER
																			// EXPENSE)
	
					Combination expenseAnalysisUtility = new Combination();
					expenseAnalysisUtility.setCombinationId(expenseCombination
							.getCombinationId());
					expenseAnalysisUtility = combinationBL
							.createAnalysisCombination(expenseAnalysisUtility,
									"Utility " + buildingName, true); // UTILITY(UNDER
																		// EXPENSE)
	
					Combination expenseAnalysisOther = new Combination();
					expenseAnalysisOther.setCombinationId(expenseCombination
							.getCombinationId());
					expenseAnalysisOther = combinationBL
							.createAnalysisCombination(expenseAnalysisOther,
									"Others " + buildingName, true); // OTHER(UNDER
																		// EXPENSE)
	
					transaction = propertyInfoBL.createTransaction(
							getImplementation(), marketValue, buildingName);
	
					TransactionDetail transactionDetail = new TransactionDetail();
					transactionDetail = propertyInfoBL.createTransactionDetail(
							marketValue, assetCombination, true, buildingName
									+ " Debited");
					Set<TransactionDetail> transactionList = new HashSet<TransactionDetail>();
					transactionList.add(transactionDetail);
	
					// CREDIT SIDE
					
					 * final long ACCOUNT_TYPE_ID = 5; Combination ownersEquity =
					 * this
					 * .getPropertyInfoBL().getTransactionBL().getJournalService(
					 * ).getFilterCombination( ACCOUNT_TYPE_ID,
					 * getImplementation()); // OWNERS EQUITY
					 
					Combination ownersEquity = this
							.getPropertyInfoBL()
							.getTransactionBL()
							.getCombinationService()
							.getCombinationAccountDetail(
									this.getImplementation().getOwnersEquity());
					transactionDetail = propertyInfoBL.createTransactionDetail(
							marketValue, ownersEquity, false, ownersEquity
									.getAccountByNaturalAccountId().getAccount()
									+ " Credited");
					transactionList.add(transactionDetail);
					transaction.setTransactionDetails(transactionList);
				}*/
				propertyToAdd.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.NoDecession
								.getCode() + ""));

				isSessionEntry = (propertyTypeId == Constants.RealEstate.PropertyType.Villa
						.getCode()
						|| propertyTypeId == Constants.RealEstate.PropertyType.Flat
								.getCode() || propertyTypeId == Constants.RealEstate.PropertyType.Shop
						.getCode()) ? true : false;

				if (isSessionEntry) {
					
					//Added by rafiq to change the property isApprove status default as approved

					propertyToAdd.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Deny
									.getCode() + ""));
					
					Component component = new Component();
					ComponentType componentType = new ComponentType();
					componentType.setComponentTypeId(1);
					component.setComponentName(propertyToAdd.getPropertyName());
					component.setComponentNo(1);
					component.setComponentType(componentType);
					component.setSize(propertyToAdd.getBuildingSize());
					component.setSizeUnit(propertyToAdd.getSizeUnit());
					component.setRent(propertyToAdd.getRentYr());
					component.setStatus((byte) 1);
					component.setImplementation(getImplementation());
					
					Person person = new Person();
					person.setPersonId(user.getPersonId());
					component.setPerson(person);
					
					
					component.setCreatedDate(new Date());
					component.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Deny
									.getCode() + ""));

					ServletActionContext.getRequest().getSession()
							.setAttribute("NEW_COMPONENT_FOR_UNIT", component);
					ServletActionContext
							.getRequest()
							.getSession()
							.setAttribute("NEW_PROPERTY_FOR_UNIT",
									propertyToAdd);
					ServletActionContext
							.getRequest()
							.getSession()
							.setAttribute("NEW_PROPERTY_TRANSACTION",
									transaction);

					unitBL.getAssetList();
					ServletActionContext.getRequest().setAttribute(
							"PRODUCT_INFO", unitBL.getProductList());
				} else {

					propertyInfoBL.addProperty(propertyToAdd, transaction,
							decision, false);
				}
			}
			// transaction.callTransaction();
		} catch (Exception e) {
			e.printStackTrace();
				if (buildingId != null && buildingId == 0)
					ServletActionContext.getRequest().setAttribute("RETURN_ERROR_MESSAGE","Property Creation Failure");
				else
					ServletActionContext.getRequest().setAttribute("RETURN_ERROR_MESSAGE","Property Update Failure");
			return ERROR;
		} finally {
			if(!isServerSideValidation){
				removeSessionEntries(isSessionEntry);
				editingProperty = false;
	
				if (!isSessionEntry) {
	
					if (buildingId != null && buildingId > 0)
						documentRecId = buildingId;
	
					AIOSCommons.removeUploaderSession(session, "uploadedDocs",
							documentRecId, "Property");
					AIOSCommons.removeUploaderSession(session, "uploadedImages",
							documentRecId, "Property");
				}
			}
		}

		return SUCCESS;
	}

	private void removeSessionEntries(Boolean isSessionEntry) {

		ServletActionContext.getRequest().removeAttribute(
				"PROPERTY_OWNERS_DISPLAY");
		ServletActionContext.getRequest().removeAttribute(
				"PROPERTY_FEATURES_DISPLAY");

		HttpSession session = ServletActionContext.getRequest().getSession();
		session.removeAttribute("PROPERTY_FEATURES");
		session.removeAttribute("PROPERTY_OWNERS");
		session.removeAttribute("PROPERTY_MAINTENANCE");
		AIOSCommons.removeUploaderSession(session, "uploadedDocs", -1L,
				"Property");
		AIOSCommons.removeUploaderSession(session, "uploadedImages", -1L,
				"Property");

		if (!isSessionEntry) {

			session.removeAttribute("AIOS-img-" + "Property-" + session.getId());
			session.removeAttribute("AIOS-img-" + "PropertyDetail-"
					+ session.getId());
		}
	}

	public String deletePropertyDetails() throws Exception {
		try {
			boolean returnState = propertyInfoBL.deleteProperty(
					(long) buildingId, propertyTypeId);
			if (returnState == true)
				returnMessage = "SUCCESS";
			else
				returnMessage = "Sorry Can't Delete. Property being used by some other place";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return SUCCESS;
		} catch (Exception e) {
			returnMessage = "Sorry Can't Delete. Property being used by some other place";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			return ERROR;
		}
	}

	public String getPropertyToEdit() throws Exception {
		propertyInfoTO = new RePropertyInfoTO();

		// Identifying the process flow whether is coming from Master Gird Table
		// or Workflow

		if (recordId != 0) {
			buildingId = (int) recordId;
		}

		AIOSCommons.removeUploaderSession(ServletActionContext.getRequest()
				.getSession(), "uploadedDocs", (long) buildingId, "Property");
		AIOSCommons.removeUploaderSession(ServletActionContext.getRequest()
				.getSession(), "uploadedImages", (long) buildingId, "Property");

		Property propertyToEdit = propertyService
				.getPropertyById((long) buildingId);

		removeSessionEntries(false);
		editingProperty = true;
		ManagementDecision managementDecisions = propertyInfoBL
				.getManagementDecisionBL()
				.getManagementDecision(propertyToEdit);
		if (managementDecisions != null && !managementDecisions.equals("")) {
			propertyInfoTO = RePropertyInfoTOConverter
					.convertDecisionToTO(managementDecisions);
			managementDecisionId = propertyInfoTO.getManagementDecisionId();
			managementDecision = propertyInfoTO.getManagementDecision();
		} else {
			managementDecisionId = 0;
			managementDecision = "";
		}
		propertyInfoTO = new RePropertyInfoTO();
		propertyInfoTO = RePropertyInfoTOConverter.convertToTO(propertyToEdit);

		buildingName = propertyInfoTO.getBuildingName();
		buildingNameArabic = propertyInfoTO.getBuildingNameArabic();
		plotNo = propertyInfoTO.getPlotNo();
		floorNo = propertyInfoTO.getFloorNo();
		addressLine1 = propertyInfoTO.getAddressLine1();
		addressLine2 = propertyInfoTO.getAddressLine2();
		countryId = propertyInfoTO.getCountryId();
		stateId = propertyInfoTO.getStateId();
		stateName = systemService.getStateById(stateId).getStateName();
		cityId = propertyInfoTO.getCityId();
		cityName = systemService.getCityById(cityId).getCityName();
		districtId = propertyInfoTO.getDistrictId();
		propertyTypeId = propertyInfoTO.getPropertyTypeId();
		fromDate = propertyInfoTO.getFromDate();
		toDate = propertyInfoTO.getToDate();
		landSize = propertyInfoTO.getLandSize();
		measurementCode = propertyInfoTO.getMeasurementCode();
		propStatus = propertyInfoTO.getPropStatus();
		propertyTax = propertyInfoTO.getPropertyTax();
		marketValue = propertyInfoTO.getMarketValue() + "";
		yearOfBuild = propertyInfoTO.getYearOfBuild();
		measurementValue = propertyInfoTO.getMeasurementValue();
		measurementCode = propertyInfoTO.getMeasurementCode();
		rentPerYr = propertyInfoTO.getRentPerYear() + "";
		acMeter = propertyInfoTO.getAcMeter();
		serviceMeter = propertyInfoTO.getServiceMeter();
		if (propertyInfoTO.getBuildingSize() != null)
			buildingSize = Double.parseDouble(propertyInfoTO.getBuildingSize());
		buildingSizeUnit = propertyInfoTO.getBuildingSizeUnit();
		isApprove = propertyInfoTO.getIsApprove();
		fireMeter = propertyInfoTO.getFireMeter();

		List<PropertyType> propertyTypes = propertyService
				.getAllPropertyTypes();
		ServletActionContext.getRequest().setAttribute("propertyTypes",
				propertyTypes);

		Set<PropertyOwnership> combinedOwnerships = new HashSet<PropertyOwnership>();
		combinedOwnerships.addAll(propertyService
				.getOwnerCompaniesForProperty(propertyToEdit));
		combinedOwnerships.addAll(propertyService
				.getOwnerPersonForProperty(propertyToEdit));

		// Initialize list of owners for this property
		propertyInfoTO.setPropertyOwnerships(combinedOwnerships);

		// Initialize list of PropertyDetails (Features) for this property
		propertyInfoTO.setPropertyDetails(new HashSet<PropertyDetail>(
				propertyService.getPropertyDetails(propertyToEdit)));

		/*
		 * // Initialize Maintenance for this property
		 * propertyInfoTO.setMaintenances(new
		 * HashSet<Maintenance>(propertyService
		 * .getMaintenance(propertyToEdit)));
		 */

		List<Maintenance> maintenanceInfos = new ArrayList<Maintenance>();
		maintenanceInfos = propertyService.getMaintenance(propertyToEdit);

		// Fetch Owners into session
		List<RePropertyInfoTO> currentOwnersForDisplay = new ArrayList<RePropertyInfoTO>();
		List<PropertyOwnership> currentOwnersForSession = new ArrayList<PropertyOwnership>();
		RePropertyInfoTO tempPropertyInfoTO = null;
		Company company;
		Person person;

		for (PropertyOwnership ownership : propertyInfoTO
				.getPropertyOwnerships()) {

			tempPropertyInfoTO = new RePropertyInfoTO();
			tempPropertyInfoTO.setOwnerId(Integer.parseInt(ownership
					.getPersonalId().toString()));
			tempPropertyInfoTO.setOwnerType(ownership.getDetails());

			if (ownership.getDetails().contains("COMPANY")) {

				company = propertyService.getCompanyById(Integer
						.parseInt(ownership.getPersonalId().toString()));
				tempPropertyInfoTO.setOwnerName(company.getCompanyName());
				tempPropertyInfoTO.setUaeNo(company.getTradeNo());
				tempPropertyInfoTO.setOwnerTypeId(0);
				tempPropertyInfoTO.setPropertyInfoOwnerId(0);
			} else {

				person = propertyService.getPersonbyId(ownership
						.getPersonalId());
				tempPropertyInfoTO.setOwnerName(person.getFirstName());
				tempPropertyInfoTO.setPropertyInfoOwnerId(1);
				try {
					tempPropertyInfoTO.setUaeNo(personService
							.getAllIdentity(person).get(0).getIdentityNumber());
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

			tempPropertyInfoTO.setPercentage(ownership.getPercentage());

			currentOwnersForDisplay.add(tempPropertyInfoTO);
		}

		ServletActionContext.getRequest().setAttribute(
				"PROPERTY_OWNERS_DISPLAY", currentOwnersForDisplay);

		currentOwnersForSession.addAll(new ArrayList<PropertyOwnership>(
				propertyInfoTO.getPropertyOwnerships()));
		ServletActionContext.getRequest().getSession()
				.setAttribute("PROPERTY_OWNERS", currentOwnersForSession);

		// Fetch Features into session
		/*List<RePropertyInfoTO> currentFeaturesForDisplay = new ArrayList<RePropertyInfoTO>();
		List<PropertyDetail> currentFeaturesForSession = new ArrayList<PropertyDetail>();
		int i = 1;
		currentFeaturesForSession.addAll(new ArrayList<PropertyDetail>(
				propertyInfoTO.getPropertyDetails()));
		for (PropertyDetail detail : propertyInfoTO.getPropertyDetails()) {

			List<RePropertyInfoTO> featuresList = new ArrayList<RePropertyInfoTO>();

			featuresList = getFeatureAll();

			tempPropertyInfoTO = new RePropertyInfoTO();
			tempPropertyInfoTO.setLineNumber(i);
			
			tempPropertyInfoTO.setFeatureCode(detail.getLookupDetail().getAccessCode());
			tempPropertyInfoTO.setFeatures(detail.getLookupDetail().getDisplayName());
			tempPropertyInfoTO.setMeasurements(detail.getFeature());
			tempPropertyInfoTO.setPropertyInfoFeatureId(Integer.parseInt(detail
					.getPropertyDetailId().toString()));
			tempPropertyInfoTO.setFeatureId(Integer.parseInt(detail
					.getPropertyDetailId().toString()));
			
			tempPropertyInfoTO.setLookupDetailId(detail.getLookupDetail().getLookupDetailId());
			currentFeaturesForDisplay.add(tempPropertyInfoTO);
		}*/

		List<PropertyDetail> propertyDetails=new ArrayList<PropertyDetail>(propertyInfoTO.getPropertyDetails());
		ServletActionContext.getRequest().setAttribute(
				"PROPERTY_FEATURES_DISPLAY", propertyDetails);

		ServletActionContext.getRequest().getSession()
				.setAttribute("PROPERTY_FEATURES",propertyDetails);

		// Property Comment Information --Added by rafiq
		CommentVO comment = new CommentVO();
		if (recordId != 0) {
			try {
				comment.setRecordId(Long.parseLong(buildingId + ""));
				comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Property");
				comment = commentBL.getCommentInfo(comment);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		ServletActionContext.getRequest().setAttribute("COMMENT_IFNO", comment);
		
		if (messageId != null) {
			
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			WorkflowDetailVO vo = workflowBL
					.getWorkflowDetailVObyMessageId(Long
							.parseLong(messageId));
			sessionObj.put("WORKFLOW_DETAILVO", vo);

		}
		
		// Comment process End

		// Fetch Maintenance into session
		try {
			MaintenanceVO maintenanceVO = null;
			for (Maintenance maintenaneTemp : maintenanceInfos) {
				maintenanceVO = new MaintenanceVO();
				maintenanceVO = MaintenanceVO
						.convertToMaintenanceVO(maintenaneTemp);
				company = propertyService.getCompanyById(Integer
						.parseInt(maintenanceVO.getCompanyId().toString()));
				if (company != null && company.getCompanyName() != null){
					maintenanceVO.setMaintenanceCompanyName(company
							.getCompanyName());
					maintenanceVO
						.setMaintenanceCompanyTradeId(company.getTradeNo());
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_MAINTENANCE", maintenanceVO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (propertyTypeId == Constants.RealEstate.PropertyType.Villa.getCode()
				|| propertyTypeId == Constants.RealEstate.PropertyType.Flat
						.getCode()
				|| propertyTypeId == Constants.RealEstate.PropertyType.Shop
						.getCode()) {

			try {
				unitRecordId = unitService
						.getUnitListComponentBased(
								componentService
										.getComponentsForProperty(buildingId)
										.get(0).getComponentId()).get(0)
						.getUnitId();
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("ERROR GETTING 'unitRecordId' :: IN PROPERTY EDIT ACTION METHOD");
			}
		}

		return SUCCESS;
	}

	public List<RePropertyInfoTO> getFeatureAll() {
		List<RePropertyInfoTO> featuresList = new ArrayList<RePropertyInfoTO>();
		
		List<LookupDetail> lookupDetails=propertyInfoBL.getLookupMasterBL().getActiveLookupDetails("PROPERTY_FEATURE", true);
		if(lookupDetails!=null && lookupDetails.size()>0){
			for(LookupDetail lookupDetail:lookupDetails){
				propertyInfoTO = new RePropertyInfoTO();
				propertyInfoTO.setFeatureCode(lookupDetail.getAccessCode());
				propertyInfoTO.setFeatureId(lookupDetail.getDataId());
				propertyInfoTO.setFeatures(lookupDetail.getDisplayName());
				propertyInfoTO.setLookupDetailId(lookupDetail.getLookupDetailId());
				featuresList.add(propertyInfoTO);
			}
		}

		

	/*	propertyInfoTO = new RePropertyInfoTO();
		propertyInfoTO.setFeatureCode("F-102");
		propertyInfoTO.setFeatureId(AIOSCommons.getRandom());
		propertyInfoTO.setFeatures("_Swimming Pool");
		featuresList.add(propertyInfoTO);

		propertyInfoTO = new RePropertyInfoTO();
		propertyInfoTO.setFeatureCode("F-103");
		propertyInfoTO.setFeatureId(AIOSCommons.getRandom());
		propertyInfoTO.setFeatures("_Balconi");
		featuresList.add(propertyInfoTO);*/
		return featuresList;
	}

	public String getPropertySizeDetailsReport() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			propertyList = propertyInfoBL.propertySizeReportInfo(
					getImplementation(), propStatus);

			iTotalRecords = propertyList.size();
			iTotalDisplayRecords = propertyList.size();

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (RePropertyInfoTO property : propertyList) {

				JSONArray array = new JSONArray();
				array.add(property.getPropertyId());
				array.add(property.getPropertyTypeName());
				array.add(property.getBuildingName());
				array.add(property.getLandSize());
				array.add(property.getBuildingSize());
				array.add(property.getComponentCount());
				array.add(property.getComponentName());
				array.add(property.getComponentSize());
				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPropertySizeDetailsReportPrint() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			propertyList = propertyInfoBL.propertySizeReportInfo(
					getImplementation(), propStatus);
			if (propertyList != null && propertyList.size() > 0) {
				int prpTypid = 0;
				long propid = 0;
				for (int i = 0; i < propertyList.size(); i++) {

					// Property type cell merge process
					if (propertyList.get(i).getPropertyTypeId() == prpTypid) {
						propertyList.get(i).setPropertyTypeName("");
					}
					prpTypid = propertyList.get(i).getPropertyTypeId();

					// Property name & size cell merge process
					if (propertyList.get(i).getPropertyId() == propid) {
						propertyList.get(i).setBuildingName("");
						propertyList.get(i).setLandSize("");
						propertyList.get(i).setBuildingSize("");
						propertyList.get(i).setAddress("");
						propertyList.get(i).setComponentCount("");
					}
					propid = propertyList.get(i).getPropertyId();
				}
			}
			if (propStatus != -1)
				propertyInfoTO.setStatusStr(Constants.RealEstate.UnitStatus
						.get(propStatus).name());
			else
				propertyInfoTO.setStatusStr("ALL");

			ServletActionContext.getRequest().setAttribute("PROPERTY_MASTER",
					propertyInfoTO);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					propertyList);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPropertySizeDetailsReportXLS() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			propertyList = propertyInfoBL.propertySizeReportInfo(
					getImplementation(), propStatus);
			if (propertyList != null && propertyList.size() > 0) {
				int prpTypid = 0;
				long propid = 0;
				for (int i = 0; i < propertyList.size(); i++) {

					// Property type cell merge process
					if (propertyList.get(i).getPropertyTypeId() == prpTypid) {
						propertyList.get(i).setPropertyTypeName("");
					}
					prpTypid = propertyList.get(i).getPropertyTypeId();

					// Property name & size cell merge process
					if (propertyList.get(i).getPropertyId() == propid) {
						propertyList.get(i).setBuildingName("");
						propertyList.get(i).setLandSize("");
						propertyList.get(i).setBuildingSize("");
						propertyList.get(i).setAddress("");
						propertyList.get(i).setComponentCount("");
					}
					propid = propertyList.get(i).getPropertyId();
				}
			}
			if (propStatus != -1)
				propertyInfoTO.setStatusStr(Constants.RealEstate.UnitStatus
						.get(propStatus).name());
			else
				propertyInfoTO.setStatusStr("ALL");

			String str = "Property Size Details\n\n";
			str += "Filter Options\n\n";
			str += "Status\n" + propertyInfoTO.getStatusStr();

			str += "\n\nProperty Information\n";
			str += "Property Type,Property Name,Location,Land Size,Property Size,No.of Floors,Component Name,Component Size\n";

			for (RePropertyInfoTO list : propertyList) {
				str += list.getPropertyTypeName() + ","
						+ list.getBuildingName() + "," + list.getAddress()
						+ "," + list.getLandSize() + ","
						+ list.getBuildingSize() + ","
						+ list.getComponentCount() + ","
						+ list.getComponentName() + ","
						+ list.getComponentSize() + "\n";

			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String discardAddEdit() {
		removeSessionEntries(false);
		editingProperty = false;
		return SUCCESS;
	}

	public String propertyUnitAvailabilityReportRedirect() {
		try {
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			setImplementation(((Implementation) (ServletActionContext
					.getRequest().getSession().getAttribute("THIS"))));

			List<Property> properties = propertyInfoBL.getPropertyService()
					.getAllProperties(getImplementation());

			propertyList = RePropertyInfoTOConverter
					.convertToTOList(properties);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					propertyList);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPropertyUnitAvailabilityList() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			if (buildingId > 0 && fromDate != null
					&& !fromDate.trim().equals("")) {
				propertyList = propertyInfoBL.getPropertyUnitVacancyList(
						Long.parseLong(buildingId + ""),
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.trim().equals("")) {
				List<Property> properties = propertyInfoBL.getPropertyService()
						.getAllProperties(getImplementation());
				for (Property property : properties) {
					List<RePropertyInfoTO> propertyListTemp = new ArrayList<RePropertyInfoTO>();
					propertyListTemp = propertyInfoBL
							.getPropertyUnitVacancyList(
									property.getPropertyId(),
									DateFormat.convertStringToDate(fromDate),
									DateFormat.convertStringToDate(toDate));
					propertyList.addAll(propertyListTemp);
				}

			}

			if (propertyList != null) {
				iTotalRecords = propertyList.size();
				iTotalDisplayRecords = propertyList.size();
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (RePropertyInfoTO property : propertyList) {

				JSONArray array = new JSONArray();
				array.add(property.getPropertyId() + "");
				array.add(property.getPropertyName() + "");
				array.add(property.getComponentName() + "");
				array.add(property.getUnitName() + "");
				array.add(property.getStatusStr() + "");
				array.add(property.getContractNumber() + "");
				array.add(property.getTenantName() + "");
				array.add(property.getFromDate() + "");
				array.add(property.getToDate() + "");
				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getPropertyUnitAvailabilityReportPrint() {

		try {

			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			if (buildingId > 0 && fromDate != null
					&& !fromDate.trim().equals("")) {
				propertyList = propertyInfoBL.getPropertyUnitVacancyList(
						Long.parseLong(buildingId + ""),
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.trim().equals("")) {
				List<Property> properties = propertyInfoBL.getPropertyService()
						.getAllProperties(getImplementation());
				for (Property property : properties) {
					List<RePropertyInfoTO> propertyListTemp = new ArrayList<RePropertyInfoTO>();
					propertyListTemp = propertyInfoBL
							.getPropertyUnitVacancyList(
									property.getPropertyId(),
									DateFormat.convertStringToDate(fromDate),
									DateFormat.convertStringToDate(toDate));
					propertyList.addAll(propertyListTemp);
				}

			}
			if (buildingName != null && !buildingName.trim().equals("")
					&& !buildingName.trim().equals("--Select--"))
				propertyInfoTO.setPropertyName(buildingName);
			else
				propertyInfoTO.setPropertyName("--All Property--");

			propertyInfoTO.setFromDate(fromDate);
			ServletActionContext.getRequest().setAttribute("PROPERTY_MASTER",
					propertyInfoTO);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					propertyList);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing property unit rental downlaod xls
	public String getPropertyUnitAvailabilityReportXLS() {

		try {
			propertyList = new ArrayList<RePropertyInfoTO>();
			propertyInfoTO = new RePropertyInfoTO();
			if (buildingId > 0 && fromDate != null
					&& !fromDate.trim().equals("")) {
				propertyList = propertyInfoBL.getPropertyUnitVacancyList(
						Long.parseLong(buildingId + ""),
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if (fromDate != null && !fromDate.trim().equals("")) {
				List<Property> properties = propertyInfoBL.getPropertyService()
						.getAllProperties(getImplementation());
				for (Property property : properties) {
					List<RePropertyInfoTO> propertyListTemp = new ArrayList<RePropertyInfoTO>();
					propertyListTemp = propertyInfoBL
							.getPropertyUnitVacancyList(
									property.getPropertyId(),
									DateFormat.convertStringToDate(fromDate),
									DateFormat.convertStringToDate(toDate));
					propertyList.addAll(propertyListTemp);
				}

			}
			String str = "Property Vacant Detail\n\n";
			str += "Filter Options\n\n";
			str += "Property,From\n";
			if (buildingName != null && !buildingName.trim().equals("")
					&& !buildingName.trim().equals("--Select--"))
				str += buildingName;
			else
				str += "--All Property--";
			str += ",";
			if (fromDate != null && !fromDate.equals("")) {
				str += fromDate;
			} else {
				str += "-NA-";
			}

			str += "\n\nProperty Information\n";
			str += "Property,Component,Unit,Status,Contract,Tenant,Period Start's,Period End's\n";

			for (RePropertyInfoTO list : propertyList) {
				str += list.getPropertyName() + "," + list.getComponentName()
						+ "," + list.getUnitName() + "," + list.getStatusStr()
						+ "," + list.getContractNumber() + ","
						+ list.getTenantName() + "," + list.getFromDate() + ","
						+ list.getToDate() + "\n";

			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public long getMaintenanceCompanyId() {
		return maintenanceCompanyId;
	}

	public void setMaintenanceCompanyId(int maintenanceCompanyId) {
		this.maintenanceCompanyId = maintenanceCompanyId;
	}

	public String getMaintenanceCompanyDetails() {
		return maintenanceCompanyDetails;
	}

	public void setMaintenanceCompanyDetails(String maintenanceCompanyDetails) {
		this.maintenanceCompanyDetails = maintenanceCompanyDetails;
	}

	public String getMaintenanceCompanyName() {
		return maintenanceCompanyName;
	}

	public void setMaintenanceCompanyName(String maintenanceCompanyName) {
		this.maintenanceCompanyName = maintenanceCompanyName;
	}

	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public int getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(int propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public String getFloorNo() {
		return floorNo;
	}

	public void setFloorNo(String floorNo) {
		this.floorNo = floorNo;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getYearOfBuild() {
		return yearOfBuild;
	}

	public void setYearOfBuild(Integer yearOfBuild) {
		this.yearOfBuild = yearOfBuild;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public String getLandSize() {
		return landSize;
	}

	public void setLandSize(String landSize) {
		this.landSize = landSize;
	}

	public String getPropertyTax() {
		return propertyTax;
	}

	public void setPropertyTax(String propertyTax) {
		this.propertyTax = propertyTax;
	}

	public String getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(String marketValue) {
		this.marketValue = marketValue;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPropertyInfoOwnerId() {
		return propertyInfoOwnerId;
	}

	public void setPropertyInfoOwnerId(int propertyInfoOwnerId) {
		this.propertyInfoOwnerId = propertyInfoOwnerId;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public String getUaeNo() {
		return uaeNo;
	}

	public void setUaeNo(String uaeNo) {
		this.uaeNo = uaeNo;
	}

	public int getPropertyInfoFeatureId() {
		return propertyInfoFeatureId;
	}

	public void setPropertyInfoFeatureId(int propertyInfoFeatureId) {
		this.propertyInfoFeatureId = propertyInfoFeatureId;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getMeasurements() {
		return measurements;
	}

	public void setMeasurements(String measurements) {
		this.measurements = measurements;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<RePropertyInfoTO> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<RePropertyInfoTO> propertyList) {
		this.propertyList = propertyList;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getPropertyTypeName() {
		return propertyTypeName;
	}

	public void setPropertyTypeName(String propertyTypeName) {
		this.propertyTypeName = propertyTypeName;
	}

	public byte getPropStatus() {
		return propStatus;
	}

	public void setPropStatus(byte propStatus) {
		this.propStatus = propStatus;
	}

	public int getPropStatusId() {
		return propStatusId;
	}

	public void setPropStatusId(int propStatusId) {
		this.propStatusId = propStatusId;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public int getOwnerTypeId() {
		return ownerTypeId;
	}

	public void setOwnerTypeId(int ownerTypeId) {
		this.ownerTypeId = ownerTypeId;
	}

	public int getFeatureId() {
		return featureId;
	}

	public void setFeatureId(int featureId) {
		this.featureId = featureId;
	}

	public int getPropertyComponentId() {
		return propertyComponentId;
	}

	public void setPropertyComponentId(int propertyComponentId) {
		this.propertyComponentId = propertyComponentId;
	}

	public int getNoOfComponent() {
		return noOfComponent;
	}

	public void setNoOfComponent(int noOfComponent) {
		this.noOfComponent = noOfComponent;
	}

	public int getNoOfFlatInComponent() {
		return noOfFlatInComponent;
	}

	public void setNoOfFlatInComponent(int noOfFlatInComponent) {
		this.noOfFlatInComponent = noOfFlatInComponent;
	}

	public String getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(String totalArea) {
		this.totalArea = totalArea;
	}

	public int getComponentType() {
		return componentType;
	}

	public void setComponentType(int componentType) {
		this.componentType = componentType;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	public String getPropertyTypeCode() {
		return propertyTypeCode;
	}

	public void setPropertyTypeCode(String propertyTypeCode) {
		this.propertyTypeCode = propertyTypeCode;
	}

	public String getPropStatusCode() {
		return propStatusCode;
	}

	public void setPropStatusCode(String propStatusCode) {
		this.propStatusCode = propStatusCode;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getMeasurementCode() {
		return measurementCode;
	}

	public void setMeasurementCode(String measurementCode) {
		this.measurementCode = measurementCode;
	}

	public String getMeasurementValue() {
		return measurementValue;
	}

	public void setMeasurementValue(String measurementValue) {
		this.measurementValue = measurementValue;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public String getRentPerYr() {
		return rentPerYr;
	}

	public void setRentPerYr(String rentPerYr) {
		this.rentPerYr = rentPerYr;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public boolean isEditingProperty() {
		return editingProperty;
	}

	public String getAcMeter() {
		return acMeter;
	}

	public void setAcMeter(String acMeter) {
		this.acMeter = acMeter;
	}

	public String getServiceMeter() {
		return serviceMeter;
	}

	public void setServiceMeter(String serviceMeter) {
		this.serviceMeter = serviceMeter;
	}

	public int getCurrentOwnerId() {
		return currentOwnerId;
	}

	public void setCurrentOwnerId(int currentOwnerId) {
		this.currentOwnerId = currentOwnerId;
	}

	public void setEditingProperty(boolean editingProperty) {
		this.editingProperty = editingProperty;
	}

	public int getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(int combinationId) {
		this.combinationId = combinationId;
	}

	public String getCodeCombination() {
		return codeCombination;
	}

	public void setCodeCombination(String codeCombination) {
		this.codeCombination = codeCombination;
	}

	

	public long getWorkflowDetailId() {
		return workflowDetailId;
	}

	public void setWorkflowDetailId(long workflowDetailId) {
		this.workflowDetailId = workflowDetailId;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public PropertyInformationBL getPropertyInfoBL() {
		return propertyInfoBL;
	}

	public void setPropertyInfoBL(PropertyInformationBL propertyInfoBL) {
		this.propertyInfoBL = propertyInfoBL;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public RePropertyInfoTO getPropertyInfoTO() {
		return propertyInfoTO;
	}

	public void setPropertyInfoTO(RePropertyInfoTO propertyInfoTO) {
		this.propertyInfoTO = propertyInfoTO;
	}

	public String getCurrentOwnerType() {
		return currentOwnerType;
	}

	public void setCurrentOwnerType(String currentOwnerType) {
		this.currentOwnerType = currentOwnerType;
	}

	public Double getBuildingSize() {
		return buildingSize;
	}

	public void setBuildingSize(Double buildingSize) {
		this.buildingSize = buildingSize;
	}

	public Integer getBuildingSizeUnit() {
		return buildingSizeUnit;
	}

	public void setBuildingSizeUnit(Integer buildingSizeUnit) {
		this.buildingSizeUnit = buildingSizeUnit;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}

	public int getRevenueId() {
		return revenueId;
	}

	public void setRevenueId(int revenueId) {
		this.revenueId = revenueId;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public long getUnitRecordId() {
		return unitRecordId;
	}

	public void setUnitRecordId(long unitRecordId) {
		this.unitRecordId = unitRecordId;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public UnitBL getUnitBL() {
		return unitBL;
	}

	public void setUnitBL(UnitBL unitBL) {
		this.unitBL = unitBL;
	}

	public String getFireMeter() {
		return fireMeter;
	}

	public void setFireMeter(String fireMeter) {
		this.fireMeter = fireMeter;
	}

	public long getManagementDecisionId() {
		return managementDecisionId;
	}

	public void setManagementDecisionId(long managementDecisionId) {
		this.managementDecisionId = managementDecisionId;
	}

	public String getManagementDecision() {
		return managementDecision;
	}

	public void setManagementDecision(String managementDecision) {
		this.managementDecision = managementDecision;
	}

	public ManagementDecision getDecision() {
		return decision;
	}

	public void setDecision(ManagementDecision decision) {
		this.decision = decision;
	}

	public int getLegalProcess() {
		return legalProcess;
	}

	public void setLegalProcess(int legalProcess) {
		this.legalProcess = legalProcess;
	}

	public String getPropertyCode() {
		return propertyCode;
	}

	public String getPropertyCodeArabic() {
		return propertyCodeArabic;
	}

	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}

	public void setPropertyCodeArabic(String propertyCodeArabic) {
		this.propertyCodeArabic = propertyCodeArabic;
	}

	public String getSiteplanReceiver() {
		return siteplanReceiver;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public String getClearanceType() {
		return clearanceType;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setClearanceType(String clearanceType) {
		this.clearanceType = clearanceType;
	}

	public void setSiteplanReceiver(String siteplanReceiver) {
		this.siteplanReceiver = siteplanReceiver;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public String getBuildingNameArabic() {
		return buildingNameArabic;
	}

	public void setBuildingNameArabic(String buildingNameArabic) {
		this.buildingNameArabic = buildingNameArabic;
	}

	public String getIsUnitClearance() {
		return isUnitClearance;
	}

	public void setIsUnitClearance(String isUnitClearance) {
		this.isUnitClearance = isUnitClearance;
	}

	public String getGeneralServiceType() {
		return generalServiceType;
	}

	public void setGeneralServiceType(String generalServiceType) {
		this.generalServiceType = generalServiceType;
	}

	public String getOwnerNameArabic() {
		return ownerNameArabic;
	}

	public void setOwnerNameArabic(String ownerNameArabic) {
		this.ownerNameArabic = ownerNameArabic;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getPropertyTypeNature() {
		return propertyTypeNature;
	}

	public void setPropertyTypeNature(String propertyTypeNature) {
		this.propertyTypeNature = propertyTypeNature;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public boolean isServerSideValidation() {
		return isServerSideValidation;
	}

	public void setServerSideValidation(boolean isServerSideValidation) {
		this.isServerSideValidation = isServerSideValidation;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public Long getPropertyDetailId() {
		return propertyDetailId;
	}

	public void setPropertyDetailId(Long propertyDetailId) {
		this.propertyDetailId = propertyDetailId;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	

}
