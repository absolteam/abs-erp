package com.aiotech.aios.realestate.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentDetail;
import com.aiotech.aios.common.to.Constants.Accounts.ModeOfPayment;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.CompressionUtil;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.vo.JobAssignmentVO;
import com.aiotech.aios.realestate.to.ContractReleaseTO;
import com.aiotech.aios.realestate.to.OfferManagementTO;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.utils.spring.util.PrintHelper;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PrintAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private final String tempFilePath = "aios/PrintTemplate/server/";
	private final String templateFilePath = "aios/PrintTemplate/templates/implementation_";
	private String caller;
	private String template;
	private Boolean templateStatus;
	private SystemBL systemBL;
	private InputStream fileInputStream;
	HashMap<String, String> tagList = new HashMap<String, String>();

	public String execute() {

		try {
			extractTemplateForPrint();
			synchronized (tagList) {
				if (caller.equals("contractAction"))
					generatePrintableDocForApplet(getPopulatedContractTagList());

				else if (caller.equals("offerAction"))
					generatePrintableDocForApplet(getPopulatedOfferTagList());

				else if (caller.equals("releaseAction"))
					generatePrintableDocForApplet(getPopulatedReleaseTagList());

				else if (caller.equals("clearanceAction"))
					generatePrintableDocForApplet(getPopulatedClearanceTagList());

				else if (caller.equals("siteplanAction"))
					generatePrintableDocForApplet(getPopulatedSiteplanTagList());

				else if (caller.equals("receiptAction"))
					generatePrintableDocForApplet(getPopulatedReceiptTagList());

				else if (caller.equals("chequePaymentAction"))
					generatePrintableDocForApplet(getPopulatedChequePaymentTagList());

				else if (caller.equals("inventoryChequePaymentAction"))
					generatePrintableDocForApplet(getInventoryChequePaymentTagList());

				else if (caller.equals("jobAssignmentAction"))
					generateAndDownloadDoc(getPopulatedEmployeeContractTagList());
			}
			FileUtil.delete(new File(getCompletePathOfTemplate()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String updateTemplatePrintStatus() throws Exception {

		Implementation implementation = systemBL
				.updateTemplateStatus(templateStatus);
		ServletActionContext.getRequest().getSession()
				.setAttribute("THIS", implementation);
		return SUCCESS;
	}

	private void extractTemplateForPrint() {

		try {
			List<Map<String, Object>> availableTemplates = FileUtil
					.getUploadedTemplates(getTemplateUploadPath());
			String fileName[];
			for (Map<String, Object> map : availableTemplates) {

				String str = map.get("fileName").toString();
				fileName = str.split("_");
				if (fileName[0].equalsIgnoreCase(template + ".doc"))
					FileUtil.writeExtracedBytesToDisk(CompressionUtil
							.extractBytes((byte[]) map.get("fileData")),
							getCompletePathOfTemplate());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getCompletePathOfTemplate() {

		return getTemplateUploadPath().concat("/").concat(template)
				.concat(".doc");
	}

	private String getTemplateUploadPath() {

		try {
			Implementation implementation = (Implementation) ServletActionContext
					.getRequest().getSession().getAttribute("THIS");
			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			return confProps.getProperty("file.drive").concat(
					templateFilePath.concat(implementation
							.getImplementationId().toString()));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private String generateAndDownloadDoc(HashMap<String, String> _tagList) {

		File temporaryFile = null;

		try {

			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			if (!PrintHelper.generateDocument(_tagList, template,
					getImplementation()))
				return clearSession(ERROR);

			temporaryFile = new File(confProps.getProperty("file.drive")
					.concat(tempFilePath) + tagList.get("tempFileName"));
			fileInputStream = new FileInputStream(temporaryFile);

			return clearSession(SUCCESS);

		} catch (Exception e) {
			System.out.println("PRINT ACTION - REAL ESTATE");
			e.printStackTrace();
			return clearSession(ERROR);
		}
	}

	private String generatePrintableDocForApplet(
			HashMap<String, String> _tagList) {

		try {

			Properties confProps = (Properties) ServletActionContext
					.getServletContext().getAttribute("confProps");
			if (!PrintHelper.generateDocument(_tagList, template,
					getImplementation()))
				return clearSession(ERROR);

			PrintHelper.sendResponseToApplet(tagList.get("tempFileName"),
					confProps.getProperty("file.drive").concat(tempFilePath));
			return clearSession(SUCCESS);

		} catch (Exception e) {
			System.out.println("PRINT ACTION - REAL ESTATE");
			e.printStackTrace();
			return clearSession(ERROR);
		}
	}

	private HashMap<String, String> getPopulatedEmployeeContractTagList() {

		tagList = new HashMap<String, String>();
		JobAssignmentVO jobAssignmentVO = (JobAssignmentVO) ServletActionContext
				.getRequest().getSession().getAttribute("EMP_CONTRACT_DATA");

		String prefix = jobAssignmentVO.getPersonVO().getPrefix();
		String firstName = jobAssignmentVO.getPersonVO().getFirstName();
		String midName = jobAssignmentVO.getPersonVO().getMiddleName();
		String lastName = jobAssignmentVO.getPersonVO().getLastName();

		String nameToShow = (prefix != null) ? prefix.trim() + " " : "";
		nameToShow += (firstName != null) ? firstName.trim() + " " : "";
		nameToShow += (midName != null) ? midName.trim() + " " : "";
		nameToShow += (lastName != null) ? lastName.trim() : "";

		// String prefixAR = "السيد";
		String firstNameAR = jobAssignmentVO.getPersonVO()
				.getFirstNameArabicStr();
		String midNameAR = jobAssignmentVO.getPersonVO()
				.getMiddleNameArabicStr();
		String lastNameAR = jobAssignmentVO.getPersonVO()
				.getLastNameArabicStr();

		String nameToShowAR = "";
		String nameToShowAR2 = "";

		/*
		 * if (midNameAR != null) { nameToShowAR += midNameAR; }
		 */
		if (firstNameAR != null && !firstNameAR.trim().isEmpty()) {
			nameToShowAR += firstNameAR;
		}
		if (lastNameAR != null && !lastNameAR.trim().isEmpty()) {
			nameToShowAR += lastNameAR;
		}

		if (firstNameAR != null && !firstNameAR.trim().isEmpty()) {
			nameToShowAR2 += firstNameAR;
		}
		if (lastNameAR != null && !lastNameAR.trim().isEmpty()) {
			nameToShowAR2 += lastNameAR;
		}

		tagList.put("_name1_", nameToShow);
		tagList.put("_nameAR1_", nameToShowAR.trim());

		tagList.put("_position_", jobAssignmentVO.getDesignationName());
		tagList.put("_positionAR_", jobAssignmentVO.getDesignationNameAR());

		tagList.put("_joining_", jobAssignmentVO.getPersonVO()
				.getEffectiveDatesFrom());
		tagList.put("_joiningAR_", jobAssignmentVO.getPersonVO()
				.getEffectiveDatesFrom());

		tagList.put("_grossSal_", jobAssignmentVO.getCrossSalary().intValue()
				+ "");
		tagList.put("_grossSalAR_", jobAssignmentVO.getCrossSalary().intValue()
				+ "");

		tagList.put("_inWords_", AIOSCommons
				.convertAmountToWords(jobAssignmentVO.getCrossSalary()));
		tagList.put("_inWordsAR_", AIOSCommons
				.convertAmountToWords(jobAssignmentVO.getCrossSalary()));

		tagList.put("_name2_", nameToShow);
		tagList.put("_nameAR2_", nameToShowAR2.trim());

		tagList.put("_nationality_", jobAssignmentVO.getPersonVO()
				.getNationalityCaption());
		tagList.put("_nationalityAR_", jobAssignmentVO.getPersonVO()
				.getNationalityCaptionAR());

		tagList.put("_passport_", jobAssignmentVO.getPassportNumber());
		tagList.put("_passportAR_", jobAssignmentVO.getPassportNumber());

		tagList.put("_accomodation_", jobAssignmentVO.getAccomodation());
		tagList.put("_accomodationAR_", jobAssignmentVO.getAccomodationAR());

		tagList.put("_salaryDetails_", jobAssignmentVO.getSalaryDetails());
		tagList.put("_salaryDetailsAR_", jobAssignmentVO.getSalaryDetailsAR());

		tagList.put("tempFileName",
				"emp-contract-".concat(AIOSCommons.getRandom() + ".doc"));

		return tagList;
	}

	private HashMap<String, String> getPopulatedReceiptTagList() {

		tagList = new HashMap<String, String>();
		DirectPayment directPayment = (DirectPayment) ServletActionContext
				.getRequest().getSession().getAttribute("DIRECT_PAYMENT");

		Double amount = 0d;
		for (DirectPaymentDetail directPaymentDetail : directPayment
				.getDirectPaymentDetails()) {
			amount += directPaymentDetail.getAmount();
		}

		tagList.put("amount_", amount.toString());
		tagList.put("receiptNo", directPayment.getPaymentNumber());
		tagList.put("issueDate_", DateFormat.convertDateToString(directPayment
				.getPaymentDate().toString()));

		String tenantName = "";
		if (directPayment.getSupplier().getPerson() != null) {
			tenantName = directPayment.getSupplier().getPerson().getFirstName()
					+ " "
					+ directPayment.getSupplier().getPerson().getLastName();
		} else {
			tenantName = directPayment.getSupplier().getCmpDeptLocation()
					.getCompany().getCompanyName();
		}
		tagList.put("tenantName", tenantName);

		tagList.put("wordAmount", AIOSCommons.convertAmountToWords(amount));

		String paymentDetails = "";
		paymentDetails = ModeOfPayment.get(directPayment.getPaymentMode())
				.name().replaceAll("_", " ");
		if (directPayment.getBankAccount() != null) {
			paymentDetails += ""
					+ directPayment.getBankAccount().getBank().getBankName()
					+ " (" + directPayment.getBankAccount().getAccountNumber()
					+ " )" + directPayment.getChequeNumber();
		}
		tagList.put("paymentDetail", paymentDetails);

		tagList.put("chequeDate", "");
		tagList.put("bankNames", "");
		tagList.put("PDCDetails", "");

		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		return tagList;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, String> getPopulatedSiteplanTagList() {

		tagList = new HashMap<String, String>();
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, String> siteplanInfo = (Map<String, String>) session
				.getAttribute("SITEPLAN_INFO");

		tagList.put("printDate",
				AIOSCommons.getCurrentTimeStamp().split(" ")[0]);
		tagList.put("Ù…Ù…ØªÙ„ÙƒØ§Øª", siteplanInfo.get("buildingName"));
		tagList.put("ownerName", siteplanInfo.get("ownerName"));
		tagList.put("Ø¹Ù†ÙˆØ§Ù†", siteplanInfo.get("addressLine1"));
		tagList.put("Ù…Ø³ØªÙ„Ù…", siteplanInfo.get("siteplanReceiver"));

		return tagList;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, String> getPopulatedClearanceTagList() {

		tagList = new HashMap<String, String>();
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, String> clearanceInfo = (Map<String, String>) session
				.getAttribute("CLEARANCE_INFO");
		String clearanceType = clearanceInfo.get("clearanceType");

		if (clearanceType.toLowerCase().equals("withdis"))
			clearanceType = "Ù…Ø¹";
		else
			clearanceType = "Ø¨Ø¯ÙˆÙ†";

		tagList.put("issueDate_",
				AIOSCommons.getCurrentTimeStamp().split(" ")[0]);
		tagList.put("ØªØºÙŠÙŠØ±", clearanceType);
		tagList.put("Ø­Ø³Ø§Ø¨", clearanceInfo.get("accountNo"));
		tagList.put("Ø¨Ù†Ø§Ø¡", clearanceInfo.get("buildingName"));
		tagList.put("Ø¹Ù†ÙˆØ§Ù†", clearanceInfo.get("addressLine1"));
		tagList.put("ownerName", clearanceInfo.get("ownerName"));

		return tagList;
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, String> getPopulatedReleaseTagList() {

		HttpSession session = ServletActionContext.getRequest().getSession();
		ReContractAgreementTO agreementTO = (ReContractAgreementTO) session
				.getAttribute("AGREEMENT_DETAILS");
		ReContractAgreementTO agreementTOTenants = (ReContractAgreementTO) session
				.getAttribute("AGREEMENT_TENANT_DETAILS");
		List<ReContractAgreementTO> agreementTOOffer = (List<ReContractAgreementTO>) session
				.getAttribute("AGREEMENT_OFFER_DETAILS");
		String temp = "";
		tagList = new HashMap<String, String>();

		tagList.put("contractNo", agreementTO.getContractNumber());
		tagList.put("issueDate_", agreementTO.getContractDate());

		tagList.put("tenantName", agreementTOTenants.getTenantName());
		try {
			temp = agreementTOTenants.getTenantIssueDate().split(" ")[0];
		} catch (Exception e) {
			temp = agreementTOTenants.getTenantIssueDate();
		}
		tagList.put("tenantIssueDate", temp);
		tagList.put("tenantID", agreementTOTenants.getTenantIdentity());
		tagList.put("tenantNational", agreementTOTenants.getTenantNationality());
		tagList.put("tenantFax", agreementTOTenants.getTenantFax());
		tagList.put("tenantTel", agreementTOTenants.getTenantMobile());
		tagList.put("tenantPOBox", agreementTOTenants.getTenantPOBox());

		tagList.put("street_", agreementTOOffer.get(0).getBuildingStreet());
		tagList.put("city_", agreementTOOffer.get(0).getBuildingCity());
		tagList.put("area_", agreementTOOffer.get(0).getBuildingArea());
		tagList.put("plotNo", agreementTOOffer.get(0).getBuildingPlot());
		tagList.put("buildingName", agreementTOOffer.get(0).getBuildingName());

		temp = "";
		for (ReContractAgreementTO offered : agreementTOOffer) {
			temp += offered.getFlatNumber() + " - ";
		}
		tagList.put("specifiedProperty",
				AIOSCommons.removeTrailingSymbols(temp));
		tagList.put("endDate", agreementTOTenants.getToDate());
		tagList.put("startDate", agreementTOTenants.getFromDate());
		tagList.put("depositAmount",
				AIOSCommons.formatAmount(agreementTO.getDepositAmount()));

		List<ContractReleaseTO> releaseTO = (List<ContractReleaseTO>) session
				.getAttribute("RELEASE_DETAILS");
		int maxDamageEntries = 10, damageCount = 1;
		if (releaseTO != null) {

			for (ContractReleaseTO contractReleaseTO : releaseTO) {

				tagList.put("damageAsset", contractReleaseTO.getAssetName());
				tagList.put("damageDetail", contractReleaseTO.getDamage());
				tagList.put("damageAmount",
						contractReleaseTO.getBalanceAmount() + "");
			}
		}

		tagList.put("damageTotal",
				(String) session.getAttribute("DAMAGE_TOTAL"));
		tagList.put("otherCharges",
				(String) session.getAttribute("OTHER_CHARGES"));
		tagList.put("deduction_",
				(String) session.getAttribute("TOTAL_DEDUCTION"));
		tagList.put("deposit_",
				AIOSCommons.formatAmount(agreementTO.getDepositAmount()));
		tagList.put("refund_", (String) session.getAttribute("REFUND_AMOUNT"));

		this.verifyDamageTagsCompletion(damageCount, maxDamageEntries);

		String releaseType = (String) session
				.getAttribute("RELEASE_TRANSFER_SUBJECT");
		String transferAccount = (String) session
				.getAttribute("RELEASE_TRANSFER_ACCOUNT");

		if (releaseType.toLowerCase().equals("withdis")) {
			releaseType = "Ù…Ø¹";
		} else {
			releaseType = "Ø¨Ø¯ÙˆÙ†";
		}

		tagList.put("ØªØºÙŠÙŠØ±", releaseType);
		tagList.put("Ø­Ø³Ø§Ø¨", transferAccount);
		tagList.put("Ø¨Ù†Ø§Ø¡", agreementTOOffer.get(0).getBuildingName());
		tagList.put("Ø¹Ù†ÙˆØ§Ù†", agreementTOOffer.get(0).getBuildingArea());
		tagList.put("ownerName", agreementTOTenants.getTenantName());

		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		return tagList;
	}

	private HashMap<String, String> getPopulatedOfferTagList() {

		OfferManagementTO agreementTO = (OfferManagementTO) ServletActionContext
				.getRequest().getSession().getAttribute("OFFER_DETAILS_INFO");
		OfferManagementTO offerTO = (OfferManagementTO) ServletActionContext
				.getRequest().getSession()
				.getAttribute("OFFER_CANVAS_DETAILS_PRINT");
		String propertyTitle = "";
		Double totalRentAmount = 0.0;
		String totalPropertySize = "";
		Double propertySize = 0.0;
		String propertyType = "";
		tagList = new HashMap<String, String>();

		tagList.put("tenantName", agreementTO.getTenantName());
		tagList.put("tenantName_arabic", agreementTO.getTenantNameArabic() + "");
		tagList.put("tenantAddress", agreementTO.getAddress() + "");
		tagList.put("tenantAddress_arabic",
				AIOSCommons.translatetoArabic(agreementTO.getAddress()) + "");

		if ((offerTO.getComponentFlag() != 0 && offerTO.getComponentFlag() == 1)
				|| offerTO.getPropertyType().toLowerCase().equals("villa")) {

			propertyTitle = offerTO.getPropertyName();
			totalRentAmount = offerTO.getOfferAmount();
			totalPropertySize = offerTO.getPropertySize();
			propertyType = offerTO.getPropertyType();
		} else {

			List<OfferManagementTO> componentList = new ArrayList<OfferManagementTO>();
			List<OfferManagementTO> unitList = new ArrayList<OfferManagementTO>();
			componentList = offerTO.getComponentList();
			unitList = offerTO.getUnitList();

			if (componentList != null && !componentList.equals("")) {

				propertyType = "Component";
				for (OfferManagementTO component : componentList) {

					propertyTitle = propertyTitle.concat(component
							.getComponentName().concat(", "));
					totalRentAmount += component.getOfferAmount();
					propertySize += convertToDouble(component.getPropertySize());
				}
			} else if (unitList != null && !unitList.equals("")) {

				propertyType = "Unit";
				for (OfferManagementTO unit : unitList) {

					propertyTitle = propertyTitle.concat(unit.getUnitName()
							.concat(", "));
					totalRentAmount += unit.getOfferAmount();
					propertySize += convertToDouble(unit.getPropertySize());
				}
			}
			totalPropertySize = propertySize.toString();
		}

		tagList.put("propertyType", propertyType);
		tagList.put("offeredProperty",
				AIOSCommons.removeTrailingSymbols(propertyTitle));
		tagList.put("totalRentAmount",
				AIOSCommons.formatAmount(totalRentAmount));
		tagList.put("totalNetArea", totalPropertySize);
		tagList.put("contractPeriod", agreementTO.getStartDate() + " - "
				+ agreementTO.getEndDate());
		tagList.put("offerValidity", agreementTO.getValidityDays() + "");
		tagList.put("securityDeposit", agreementTO.getSecurityDeposit());
		tagList.put("refNo", getUserStamp());

		// Bank Account Details
		tagList.put("bankAccountTitle", agreementTO.getBankAccountTitle());
		tagList.put("bankAccountNumber", agreementTO.getBankAccountNumber());
		tagList.put("bankIBAN", agreementTO.getBankIBAN());
		tagList.put("bankName", agreementTO.getBankName());
		tagList.put("bankBranch", agreementTO.getBankBranch());
		tagList.put("bankCity", agreementTO.getBankCity());
		tagList.put("offerPrintOption1", agreementTO.getOfferPrintOption1());

		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		return tagList;
	}

	private String getUserStamp() {

		try {
			User loggedUser = (User) ServletActionContext.getRequest()
					.getSession().getAttribute("USER");
			Person personUser = new Person();
			personUser.setPersonId(loggedUser.getPersonId());
			String refStamp = loggedUser
					.getUsername()
					.concat("-"
							+ personUser
									.getPersonId()
									.toString()
									.concat("-"
											+ AIOSCommons.getCurrentTimeStamp()));
			return AIOSCommons.desEncrypt(refStamp);
		} catch (Exception e) {
			e.printStackTrace();
			return "--";
		}
	}

	private Double convertToDouble(String value) {

		try {
			if (value != null && value.contains(" "))
				return Double.parseDouble(value.split(" ")[0]);
			else if (value != null)
				return Double.parseDouble(value);
			else
				return 0.0;
		} catch (Exception e) {
			e.printStackTrace();
			return 0.0;
		}
	}

	@SuppressWarnings("unchecked")
	private HashMap<String, String> getPopulatedContractTagList() {

		HttpSession session = ServletActionContext.getRequest().getSession();
		ReContractAgreementTO agreementTO = (ReContractAgreementTO) session
				.getAttribute("AGREEMENT_DETAILS");
		ReContractAgreementTO agreementTOTenants = (ReContractAgreementTO) session
				.getAttribute("AGREEMENT_TENANT_DETAILS");
		List<ReContractAgreementTO> agreementTOOffer = (List<ReContractAgreementTO>) session
				.getAttribute("AGREEMENT_OFFER_DETAILS");
		List<ReContractAgreementTO> agreementTOPayment = (List<ReContractAgreementTO>) session
				.getAttribute("AGREEMENT_PAYMENT_DETAILS");
		String temp = "", specifiedProperty = "";
		Double contractAmount = 0.0;
		int paymentNo = 1, maxPaymentEntries = 5;
		String chqNo = "", banks = "", dates = "";
		tagList = new HashMap<String, String>();

		tagList.put("contractNo", agreementTO.getContractNumber());
		tagList.put("issueDate_", agreementTO.getContractDate());

		try {
			tagList.put("ownerName", agreementTOOffer.get(0)
					.getOfferedPropertyOwners());
			tagList.put("Ø§Ø³Ù… Ù…Ø§Ù„Ùƒ", AIOSCommons
					.translatetoArabic(agreementTOOffer.get(0)
							.getOfferedPropertyOwners()));
		} catch (Exception e) {
			e.printStackTrace();
			tagList.put("ownerName", "Adel Abdul Hameed Al Hosani");
			tagList.put("Ø§Ø³Ù… Ù…Ø§Ù„Ùƒ",
					"ï»£ïº£ï»£Ø¯ ï»‹ïº‘Ø¯ Ø§ï»Ÿïº£ï»£ï»´Ø¯ Ø§ï»Ÿïº£Ùˆïº³ï»§ï»²");
		}

		tagList.put("tenantName", agreementTOTenants.getTenantName());
		try {
			temp = agreementTOTenants.getTenantIssueDate().split(" ")[0];
		} catch (Exception e) {
			temp = agreementTOTenants.getTenantIssueDate();
		}
		tagList.put("tenantIssueDate", temp);
		tagList.put("tenantID",
				agreementTOTenants.getTenantIdentity() == null ? "n/a"
						: agreementTOTenants.getTenantIdentity());
		tagList.put("tenantNational",
				agreementTOTenants.getTenantNationality() == null ? "n/a"
						: agreementTOTenants.getTenantNationality());
		tagList.put("tenantFax",
				agreementTOTenants.getTenantFax() == null ? "n/a"
						: agreementTOTenants.getTenantFax());
		tagList.put("tenantTel",
				agreementTOTenants.getTenantMobile() == null ? "n/a"
						: agreementTOTenants.getTenantMobile());
		tagList.put("tenantPOBox",
				agreementTOTenants.getTenantPOBox() == null ? "n/a"
						: agreementTOTenants.getTenantPOBox());

		tagList.put("sector_", agreementTOOffer.get(0).getBuildingStreet());
		tagList.put("city_", agreementTOOffer.get(0).getBuildingCity());
		tagList.put("area_", agreementTOOffer.get(0).getBuildingArea());
		tagList.put("plotNo", agreementTOOffer.get(0).getBuildingPlot());
		tagList.put("buildingName", agreementTOOffer.get(0).getBuildingName());

		specifiedProperty = "";
		for (ReContractAgreementTO offered : agreementTOOffer) {
			specifiedProperty += offered.getFlatNumber() + " - ";
		}
		tagList.put("specifiedProperty",
				AIOSCommons.removeTrailingSymbols(specifiedProperty));
		tagList.put("leasePurpose", agreementTO.getLeasedFor() == null ? ""
				: agreementTO.getLeasedFor());
		tagList.put("endDate", agreementTOTenants.getToDate());
		tagList.put("startDate", agreementTOTenants.getFromDate());

		for (ReContractAgreementTO payment : agreementTOPayment)
			contractAmount += payment.getChequeAmount();

		tagList.put("wordAmount",
				AIOSCommons.convertAmountToWords(contractAmount));
		tagList.put("amount_", AIOSCommons.formatAmount(contractAmount));
		tagList.put("depositAmount",
				AIOSCommons.formatAmount(agreementTO.getDepositAmount()));

		for (ReContractAgreementTO payment : agreementTOPayment) {

			if (payment.getChequeNumber() != null
					&& payment.getChequeNumber() != "") {

				tagList.put("paymentType" + paymentNo, payment.getFeeType());
				tagList.put("paymentMethod" + paymentNo, "Cheque");
				tagList.put("amount" + paymentNo,
						AIOSCommons.formatAmount(payment.getChequeAmount()));
				tagList.put("checkNo" + paymentNo, payment.getChequeNumber());
				tagList.put("bankName" + paymentNo, payment.getBankName());
				tagList.put("DueDate" + paymentNo, payment.getChequeDate());
			} else if (payment.getChequeNumber() == null
					&& payment.getBankName() != null
					&& payment.getChequeDate() != null) {

				tagList.put("paymentType" + paymentNo, payment.getFeeType());
				tagList.put("paymentMethod" + paymentNo, "Direct Transfer");
				tagList.put("amount" + paymentNo,
						AIOSCommons.formatAmount(payment.getChequeAmount()));
				tagList.put("checkNo" + paymentNo, "---");
				tagList.put("bankName" + paymentNo, payment.getBankName());
				tagList.put("DueDate" + paymentNo, payment.getChequeDate());

			} else {

				tagList.put("paymentType" + paymentNo, payment.getFeeType());
				tagList.put("paymentMethod" + paymentNo, "Cash");
				tagList.put("amount" + paymentNo,
						AIOSCommons.formatAmount(payment.getChequeAmount()));
				tagList.put("checkNo" + paymentNo, "---");
				tagList.put("bankName" + paymentNo, "---");
				tagList.put("DueDate" + paymentNo, "---");
			}

			chqNo += (payment.getChequeNumber() != null && payment
					.getChequeNumber() != "") ? payment.getChequeNumber()
					+ " (" + payment.getChequeAmount() + ")" + " - " : "Cash ("
					+ payment.getChequeAmount() + ") - ";

			banks += (payment.getBankName() != null && payment.getBankName() != "") ? payment
					.getBankName() + " - "
					: "";

			dates += (payment.getChequeNumber() != null && payment
					.getChequeNumber() != "") ? payment.getChequeDate() + " - "
					: "";
			paymentNo++;
		}

		this.verifyPaymentTagsCompletion(paymentNo, maxPaymentEntries);
		tagList.put("remarks_",
				AIOSCommons.translatetoArabic(agreementTO.getRemarks()));
		tagList.put("PaymentDetail", AIOSCommons.removeTrailingSymbols(chqNo));
		tagList.put("chequeDate", AIOSCommons.removeTrailingSymbols(dates));
		tagList.put("bankNames", AIOSCommons.removeTrailingSymbols(banks));

		String temp1 = (String) session.getAttribute("TRANSFER_SUBJECT");
		String transferAccount = (String) session
				.getAttribute("TRANSFER_ACCOUNT");
		String temp2 = "", temp3 = "";
		String prefix = agreementTOTenants.getPrefix();

		if (temp1.equals("Connect and Transfer")) {
			temp1 = "ÙˆØµÙ„ Ùˆ";
			temp2 = "";
			temp3 = "Ø¨ÙˆØµÙ„ Ùˆ";
		} else {
			temp1 = "";
			temp2 = "Ø¨Ø¯ÙˆÙ† Ù‚Ø·Ø¹ Ø§Ù„ØªÙŠØ§Ø±";
			temp3 = "";
		}

		if (prefix.equals(""))
			prefix = "Ø§Ù„Ø³Ø§Ø¯Ø©";
		else
			prefix = AIOSCommons.translatetoArabic(agreementTOTenants
					.getPrefix());

		tagList.put("ØªØºÙŠÙŠØ±", temp1);
		tagList.put("Ù†Ù‡Ø§ÙŠØ©", temp2);
		tagList.put("Ù…Ù†Ø®Ù�Ø¶", temp3);
		tagList.put("Ø¨Ø§Ø¯Ø¦Ø©", prefix);
		tagList.put("Ø­Ø³Ø§Ø¨", transferAccount);
		tagList.put("ownerName_arabic", AIOSCommons
				.translatetoArabic(agreementTOTenants.getTenantName()));
		tagList.put(
				"property_unit",
				agreementTOOffer.get(0).getBuildingName()
						+ (AIOSCommons.removeTrailingSymbols(specifiedProperty)
								.equals(agreementTOOffer.get(0)
										.getBuildingName()) ? " -" : ", "
								+ specifiedProperty));

		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		tagList.put("propertyNameArabic", agreementTO.getPropertyNameArabic());
		tagList.put("componentNameArabic", agreementTO.getComponentNameArabic());
		tagList.put("unitNameArabic", agreementTO.getUnitNameArabic());
		return tagList;
	}

	private HashMap<String, String> getPopulatedChequePaymentTagList() {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		tagList = new HashMap<String, String>();
		DirectPayment directPayment = (DirectPayment) ServletActionContext
				.getRequest().getSession()
				.getAttribute("DIRECT_PAYMENT_" + sessionObj.get("jSessionId"));

		Double amountTemp = 0d;
		for (DirectPaymentDetail directPaymentDetail : directPayment
				.getDirectPaymentDetails()) {
			amountTemp += directPaymentDetail.getAmount();
		}
		String amount = AIOSCommons.formatAmount(amountTemp)
				.replaceAll(",", "");

		tagList.put("voucherNumber", directPayment.getPaymentNumber());
		String payeeName = "";
		if (directPayment.getSupplier() != null
				&& directPayment.getSupplier().getPerson() != null) {
			payeeName = directPayment.getSupplier().getPerson().getFirstName()
					+ " "
					+ directPayment.getSupplier().getPerson().getLastName();
		} else if (directPayment.getSupplier() != null
				&& directPayment.getSupplier().getCompany() != null) {
			payeeName = directPayment.getSupplier().getCompany()
					.getCompanyName();
		} else if (directPayment.getCustomer() != null
				&& directPayment.getCustomer().getPersonByPersonId() != null) {
			payeeName = directPayment.getCustomer().getPersonByPersonId()
					.getFirstName()
					+ " "
					+ directPayment.getCustomer().getPersonByPersonId()
							.getLastName();
		} else if (directPayment.getCustomer() != null
				&& directPayment.getCustomer().getCompany() != null) {
			payeeName = directPayment.getCustomer().getCompany()
					.getCompanyName();
		} else if (directPayment.getOthers() != null) {
			payeeName = directPayment.getOthers();
		}
		tagList.put("payeeName", payeeName);
		tagList.put("accountPayee", " ");
		tagList.put("chequeAmount", AIOSCommons.formatAmount(amountTemp));
		tagList.put("paymentDescription", directPayment.getDescription());
		tagList.put("total", AIOSCommons.formatAmount(amountTemp));
		tagList.put("chequeNumber",
				AIOSCommons.intToString(directPayment.getChequeNumber(), 6));
		String[] strAmount = amount.split("\\.");
		String temp = AIOSCommons.convertAmountToWords(strAmount[0])
				.replaceFirst("Only", "Dirhams");
		if (strAmount[1] != null && !strAmount[1].equals("")
				&& Integer.valueOf(strAmount[1]) > 0) {
			temp += " And "
					+ AIOSCommons.convertAmountToWords(strAmount[1])
							.replaceFirst("Only", "Fils");
		}
		temp += " Only";
		tagList.put("amountInWords", temp);
		tagList.put("voucherDate", DateFormat.convertDateToString(directPayment
				.getPaymentDate().toString()));
		tagList.put("chequeDate", DateFormat.convertDateToString(directPayment
				.getChequeDate().toString()));
		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		return tagList;
	}

	// Print Inventory Cheque
	private HashMap<String, String> getInventoryChequePaymentTagList() {
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		tagList = new HashMap<String, String>();
		Payment payment = (Payment) ServletActionContext.getRequest()
				.getSession()
				.getAttribute("PAYMENT_PRINT_" + sessionObj.get("jSessionId"));
		double amountTemp = 0;
		double invoiceQty = 0;
		double unitRate = 0;
		for (PaymentDetail paymentDetail : payment.getPaymentDetails()) {
			invoiceQty = paymentDetail.getInvoiceDetail().getInvoiceQty();
			unitRate = paymentDetail.getInvoiceDetail().getReceiveDetail()
					.getUnitRate();
			amountTemp += (invoiceQty * unitRate);
		}
		if (null != payment.getDiscountReceived()
				&& payment.getDiscountReceived() > 0)
			amountTemp -= payment.getDiscountReceived();
		String amount = AIOSCommons.formatAmount(amountTemp)
				.replaceAll(",", "");
		tagList.put("voucherNumber", payment.getPaymentNumber());
		String payeeName = "";
		if (payment.getSupplier() != null
				&& payment.getSupplier().getPerson() != null) {
			payeeName = payment.getSupplier().getPerson().getFirstName() + " "
					+ payment.getSupplier().getPerson().getLastName();
		} else if (payment.getSupplier() != null
				&& payment.getSupplier().getCompany() != null) {
			payeeName = payment.getSupplier().getCompany().getCompanyName();
		}
		tagList.put("payeeName", payeeName);
		tagList.put("accountPayee", " ");
		tagList.put("chequeAmount", AIOSCommons.formatAmount(amountTemp));
		tagList.put("paymentDescription", payment.getDescription());
		tagList.put("total", AIOSCommons.formatAmount(amountTemp));
		tagList.put("chequeNumber",
				AIOSCommons.intToString(payment.getChequeNumber(), 6));
		String[] strAmount = amount.split("\\.");
		String temp = AIOSCommons.convertAmountToWords(strAmount[0])
				.replaceFirst("Only", "Dirhams");
		if (strAmount[1] != null && !strAmount[1].equals("")
				&& Integer.valueOf(strAmount[1]) > 0) {
			temp += " And "
					+ AIOSCommons.convertAmountToWords(strAmount[1])
							.replaceFirst("Only", "Fils");
		}
		temp += " Only";
		tagList.put("amountInWords", temp);
		tagList.put("voucherDate",
				DateFormat.convertDateToString(payment.getDate().toString()));
		tagList.put("chequeDate", DateFormat.convertDateToString(payment
				.getChequeDate().toString()));
		tagList.put("tempFileName",
				"temp-doc-".concat(AIOSCommons.getRandom() + ""));
		ServletActionContext
				.getRequest()
				.getSession()
				.removeAttribute(
						"PAYMENT_PRINT_" + sessionObj.get("jSessionId"));
		return tagList;
	}

	private String clearSession(String status) {

		/*
		 * HttpSession session = ServletActionContext.getRequest().getSession();
		 * session.removeAttribute("AGREEMENT_DETAILS");
		 * session.removeAttribute("AGREEMENT_TENANT_DETAILS");
		 * session.removeAttribute("AGREEMENT_OFFER_DETAILS");
		 * session.removeAttribute("AGREEMENT_PAYMENT_DETAILS");
		 * session.removeAttribute("OFFER_DETAILS_INFO");
		 */

		ServletActionContext.getRequest().getSession()
				.removeAttribute("DIRECT_PAYMENT");
		return status;
	}

	private void verifyPaymentTagsCompletion(int count, int maxPaymentEntries) {

		while (count <= maxPaymentEntries) {

			tagList.put("paymentType" + count, "");
			tagList.put("paymentMethod" + count, "");
			tagList.put("amount" + count, "");
			tagList.put("checkNo" + count, "");
			tagList.put("bankName" + count, "");
			tagList.put("DueDate" + count++, "");
		}
	}

	private void verifyDamageTagsCompletion(int count, int maxDamageEntries) {

		while (count <= maxDamageEntries) {

			tagList.put("damageAsset" + count, "");
			tagList.put("damageDetail" + count, "");
			tagList.put("damageAmount" + count++, "");
		}
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getImplementation() {

		return "implementation_".concat(((Implementation) (ServletActionContext
				.getRequest().getSession().getAttribute("THIS")))
				.getImplementationId().toString());
	}

	public Implementation getImplementationObj() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public Boolean getTemplateStatus() {
		return templateStatus;
	}

	public void setTemplateStatus(Boolean templateStatus) {
		this.templateStatus = templateStatus;
	}

	public SystemBL getSystemBL() {
		return systemBL;
	}

	public void setSystemBL(SystemBL systemBL) {
		this.systemBL = systemBL;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}
}
