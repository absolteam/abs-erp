package com.aiotech.aios.realestate.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Asset;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.UnitDetail;
import com.aiotech.aios.realestate.domain.entity.UnitType;
import com.aiotech.aios.realestate.domain.entity.vo.UnitDetailVO;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.service.bl.PropertyInformationBL;
import com.aiotech.aios.realestate.service.bl.UnitBL;
import com.aiotech.aios.realestate.to.RePropertyUnitsTO;
import com.aiotech.aios.realestate.to.converter.RePropertyUnitsTOConverter;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.domain.entity.LookupMaster;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class RePropertyUnitsAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(PropertyInformationAction.class);

	private UnitService unitService;
	private UnitBL unitBL;
	private ComponentService componentService;
	private PropertyService propertyService;
	private PropertyInformationBL propertyInformationBL;

	private long unitId;
	private long componentId;
	private String componentName;
	private String componentNameArabic;
	private String unitNameArabic;
	private Long lookupDetailId;
	private int unitTypeId;
	private String unitType;
	private int unitNumber;
	private String unitName;
	private int rooms;
	private String rent;
	private int status;
	private long unitDetailId;
	private String unitDetails;
	private String addEditFlag;
	private long propertyId;
	private String propertyName;
	private String meterNo;
	private Double size;
	private Integer sizeUnit;
	private String unitFeatures;
	private String unitFeatureDetails;
	private long recordId;
	String jsonResult;
	String json;
	private int rowId;
	private Boolean isIdenticalEntry;

	private Byte bedroom;
	private String bedroomSize;
	private Byte bathroom;
	private String bathroomSize;
	private Byte hall;
	private String hallSize;
	private Byte kitchen;
	private String kitchenSize;
	private Byte serventRoom;
	private Byte driverRoom;
	private Byte frontYard;
	private Byte backYard;
	private Byte frontGarden;
	private Byte backGarden;
	private Byte garage;
	private Byte swimmingPool;
	private Byte inPantry;
	private Byte outPantry;
	private Byte podeo;
	private Byte fountain;
	private Byte guestRoom;
	private Byte securityRoom;
	private Byte kennel;
	private Byte tennisYard;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String unitAssetDetails;
	private byte furnished;

	List<RePropertyUnitsTO> unitList = null;
	RePropertyUnitsTO unitTo = null;
	List<Unit> unit = null;
	Unit unitInfo = null;
	Component component = null;
	UnitType unitTyp = null;
	Property property = null;
	List<Component> componentList = null;
	List<UnitType> unitTypeList = null;
	List<UnitDetail> unitFeatureList = null;
	private List<Asset> assetDetailList;
	private String id;
	Implementation implementation = null;
	private InputStream fileInputStream;
	private String messageId;
	private WorkflowBL workflowBL;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");

		}
	}

	// JSON Listing Redirect
	public String returnSuccess() {

		ServletActionContext.getRequest().setAttribute("rowid", id);
		return SUCCESS;
	}

	// Listing JSON
	public String getUintsList() {

		try {
			unitList = new ArrayList<RePropertyUnitsTO>();
			// Set the value to push to Personal Details DAO
			unitTo = new RePropertyUnitsTO();
			unit = new ArrayList<Unit>();
			component = new Component();
			unitTyp = new UnitType();
			property = new Property();

			// Call Implementation
			getImplementId();
			/*
			 * if(status==-1 || status==0){ unit =
			 * unitService.getAllUnits(implementation); } else{ unit =
			 * unitService
			 * .getAllUnitsWithStatus(implementation,Byte.parseByte(status+""));
			 * 
			 * }
			 */
			unit = unitService.getAllUnits(implementation);
			if (unit != null && unit.size() > 0) {
				for (int i = 0; i < unit.size(); i++) {
					// Component
					component = unitService.getComponentDetails(unit.get(i)
							.getComponent().getComponentId());
					property = component.getProperty();
					unitTyp = unitService.getUnitTypeDetails(unit.get(i)
							.getUnitType().getUnitTypeId());
					unit.get(i).setComponent(component);
					unit.get(i).setUnitType(unitTyp);
				}
				iTotalRecords = unitList.size();
				iTotalDisplayRecords = unitList.size();
			}
			unitList = RePropertyUnitsTOConverter.convertToTOList(unit);

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			List<String> sizeUnit = new ArrayList<String>();
			sizeUnit.add("Square Feet");
			sizeUnit.add("Square Meter");
			sizeUnit.add("Square Yard");
			for (RePropertyUnitsTO list : unitList) {

				if (list.getIsApproved() == Constants.RealEstate.Status.Deny
						.getCode())
					continue;

				JSONArray array = new JSONArray();
				array.add(list.getUnitId() + "");
				array.add(list.getUnitName() + "");
				array.add(list.getUnitType() + "");
				array.add(list.getComponentName() + "");
				array.add(list.getPropertyName() + "");
				if (list.getSizeUnit() > 0) {
					switch (list.getSizeUnit()) {
					case 1:
						array.add(list.getSize() + " " + sizeUnit.get(0));
						break;
					case 2:
						array.add(list.getSize() + " " + sizeUnit.get(1));
						break;
					case 3:
						array.add(list.getSize() + " " + sizeUnit.get(2));
						break;
					}
				} else {
					array.add(list.getSize());
				}
				if (list.getStatus() == 1)
					array.add("AVAILABLE");
				else if (list.getStatus() == 2)
					array.add("PENDING");
				else if (list.getStatus() == 3)
					array.add("RENTED");
				else if (list.getStatus() == 4)
					array.add("SOLD");
				else if (list.getStatus() == 5)
					array.add("RENOVATION");
				else if (list.getStatus() == 6)
					array.add("OFFERED");
				else if (list.getStatus() == 0)
					array.add("PENDING");
				else
					array.add("PENDING");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON
	public String getUintsListReportJson() {

		try {
			unitList = new ArrayList<RePropertyUnitsTO>();
			// Set the value to push to Personal Details DAO
			unitTo = new RePropertyUnitsTO();
			unit = new ArrayList<Unit>();
			component = new Component();
			unitTyp = new UnitType();
			property = new Property();

			// Call Implementation
			getImplementId();
			if (status == -1 || status == 0) {
				unit = unitService.getAllUnits(implementation);
			} else {
				unit = unitService.getAllUnitsWithStatus(implementation,
						Byte.parseByte(status + ""));

			}
			if (unit != null && unit.size() > 0) {
				for (Unit unitTemp : unit) {
					// Component
					component = unitService.getComponentDetails(unitTemp
							.getComponent().getComponentId());
					property = component.getProperty();
					unitTyp = unitService.getUnitTypeDetails(unitTemp
							.getUnitType().getUnitTypeId());
					unitTemp.setComponent(component);
					unitTemp.setUnitType(unitTyp);
				}
				iTotalRecords = unitList.size();
				iTotalDisplayRecords = unitList.size();
			}
			unitList = RePropertyUnitsTOConverter.convertToTOList(unit);

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			List<String> sizeUnit = new ArrayList<String>();
			sizeUnit.add("Square Feet");
			sizeUnit.add("Square Meter");
			sizeUnit.add("Square Yard");
			for (RePropertyUnitsTO list : unitList) {

				if (list.getIsApproved() == Constants.RealEstate.Status.Deny
						.getCode())
					continue;

				JSONArray array = new JSONArray();
				array.add(list.getUnitId() + "");
				array.add(list.getUnitName() + "");
				array.add(list.getUnitType() + "");
				array.add(list.getComponentName() + "");
				array.add(list.getPropertyName() + "");
				if (list.getSizeUnit() > 0) {
					switch (list.getSizeUnit()) {
					case 1:
						array.add(list.getSize() + " " + sizeUnit.get(0));
						break;
					case 2:
						array.add(list.getSize() + " " + sizeUnit.get(1));
						break;
					case 3:
						array.add(list.getSize() + " " + sizeUnit.get(2));
						break;
					}
				} else {
					array.add(0);
				}
				if (list.getStatus() == 1)
					array.add("AVAILABLE");
				else if (list.getStatus() == 2)
					array.add("PENDING");
				else if (list.getStatus() == 3)
					array.add("RENTED");
				else if (list.getStatus() == 4)
					array.add("SOLD");
				else if (list.getStatus() == 5)
					array.add("RENOVATION");
				else if (list.getStatus() == 6)
					array.add("OFFERED");
				else if (list.getStatus() == 0)
					array.add("PENDING");
				else
					array.add("PENDING");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getUintsListReport() {
		try {
			RePropertyUnitsTO unitMaster = new RePropertyUnitsTO();

			if (status == -1)
				unitMaster.setUnitStatus("ALL");
			else
				unitMaster.setUnitStatus(Constants.RealEstate.UnitStatus.get(
						status).name());
			List<RePropertyUnitsTO> unitList = unitBL.getUnitListForReport(Byte
					.parseByte(status + ""));
			ServletActionContext.getRequest().setAttribute("UNIT_MASTER",
					unitMaster);
			ServletActionContext.getRequest().setAttribute("UNIT_LIST",
					unitList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getUintsListReportXLS() {
		try {

			List<RePropertyUnitsTO> unitList = unitBL.getUnitListForReport(Byte
					.parseByte(status + ""));

			String str = "Property Units Filter\n";
			str += "Unit Status,";
			if (status == -1)
				str += "ALL\n\n";
			else
				str += Constants.RealEstate.UnitStatus.get(status).name()
						+ "\n\n";
			str += "Property Units\n";
			str += "Property Name,Component Name,Unit Name,Unit Number,Unit Type,Unit Size,Rent,Unit Status,Unit Details\n";
			for (RePropertyUnitsTO rePropertyUnitsTO : unitList) {
				str += rePropertyUnitsTO.getPropertyName()
						+ ","
						+ rePropertyUnitsTO.getComponentName()
						+ ","
						+ rePropertyUnitsTO.getUnitName()
						+ ","
						+ rePropertyUnitsTO.getUnitNumber()
						+ ","
						+ rePropertyUnitsTO.getUnitType()
						+ ","
						+ rePropertyUnitsTO.getSizeWithUom()
						+ ","
						+ rePropertyUnitsTO.getRent()
						+ ","
						+ rePropertyUnitsTO.getUnitStatus()
						+ ","
						+ rePropertyUnitsTO.getUnitFullInfo().replaceAll(",",
								"/") + "\n";
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String addUnitGet() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		session.removeAttribute("NEW_COMPONENT_FOR_UNIT");
		session.removeAttribute("NEW_PROPERTY_FOR_UNIT");
		// Identifying the process flow whether is coming from Master Gird Table
		// or Workflow

		if (recordId != 0)
			unitId = (int) recordId;
		
		
		if (messageId != null) {

			WorkflowDetailVO vo = workflowBL
					.getWorkflowDetailVObyMessageId(Long
							.parseLong(messageId));
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			sessionObj.put("WORKFLOW_DETAILVO", vo);

		}

		return unitBL.getUnit(unitId, recordId, isIdenticalEntry);
	}

	public String getAssetRow() {
		try {
			unitBL.getAssetList();
			ServletActionContext.getRequest().setAttribute("rowid", rowId);
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					unitBL.getProductList());
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String addUnitFeatureGet() {

		try {
			// Call Implementation
			getImplementId();
			// Component List
			componentList = unitService.getAllComponent(implementation);

			// Unit Type List
			unitTypeList = unitService.getAllUnitType();

			List<UnitDetailVO> unitDetailVO = new ArrayList<UnitDetailVO>();

			if (unitId != 0) {
				// Unit Details
				unitInfo = unitService.getUnitDetails(unitId);
				unitTo = RePropertyUnitsTOConverter.convertToTO(unitInfo);
				// Unit feature
				unitFeatureList = unitService.getUnitFeatureDetails(unitId);
				unitDetailVO = UnitDetailVO
						.convertToUnitDetailVO(unitFeatureList);
			}

			ServletActionContext.getRequest().setAttribute("UNITINFO", unitTo);
			ServletActionContext.getRequest().setAttribute("FEATURE_DETAILS",
					unitDetailVO);
			// unitFeatureList);
			ServletActionContext.getRequest().setAttribute("COMPONENTLIST",
					componentList);
			ServletActionContext.getRequest().setAttribute("UNITYPELIST",
					unitTypeList);
			ServletActionContext.getRequest().setAttribute("UNITYPELIST",
					unitTypeList);

			return SUCCESS;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}

	}

	// to get the features
	public String showFeatureDetails() {
		try {
			unitList = new ArrayList<RePropertyUnitsTO>();

			List<LookupMaster> lookupMaster=unitBL.getLookupMasterBL()
			.getLookupMasterService().getLookupInformation("UNIT_FEATURE", getImplementation());
			if(lookupMaster!=null && lookupMaster.size()>0 && lookupMaster.get(0).getLookupDetails()!=null){
				for(LookupDetail lookupDetail:lookupMaster.get(0).getLookupDetails()){
					unitTo = new RePropertyUnitsTO();
					unitTo.setUnitFeature(lookupDetail.getAccessCode());
					unitTo.setUnitFeatureId(lookupDetail.getDataId());
					unitTo.setUnitFeatureDetail(lookupDetail.getDisplayName());
					unitTo.setLookupDetailId(lookupDetail.getLookupDetailId());
					unitList.add(unitTo);
				}
			}
			/*unitList = new ArrayList<RePropertyUnitsTO>();
			unitTo = new RePropertyUnitsTO();
			unitTo.setUnitFeature("F-101");
			unitTo.setUnitFeatureId(1);
			unitTo.setUnitFeatureDetail("Shelf");
			unitList.add(unitTo);

			unitTo = new RePropertyUnitsTO();
			unitTo.setUnitFeature("F-102");
			unitTo.setUnitFeatureId(2);
			unitTo.setUnitFeatureDetail("Furniture");
			unitList.add(unitTo);

			unitTo = new RePropertyUnitsTO();
			unitTo.setUnitFeature("F-103");
			unitTo.setUnitFeatureId(3);
			unitTo.setUnitFeatureDetail("Visitor Hall");
			unitList.add(unitTo);*/
			ServletActionContext.getRequest().setAttribute("UNITFEATUER",
					unitList);
			LOGGER.info("Module: RE : Method: UNITFEATUER: Action Success");

			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: UNITFEATUER: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String addUnitFeatureSave() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			List<UnitDetail> unitFeaturesList = null;
			UnitDetail unitFeature = new UnitDetail();
			/*unitFeature.setDetails(unitFeatures + "#" + unitDetails + "#"
					+ unitFeatureDetails);*/
			unitFeature.setDetails(unitFeatureDetails);
			LookupDetail lookupDetail=new LookupDetail();
			lookupDetail.setLookupDetailId(lookupDetailId);
			unitFeature.setLookupDetail(lookupDetail);
			
			if (session.getAttribute("REUNITFEATURE_"
					+ sessionObj.get("jSessionId")) == null) {
				unitFeaturesList = new ArrayList<UnitDetail>();
			} else {
				unitFeaturesList = (List<UnitDetail>) session
						.getAttribute("REUNITFEATURE_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				unitFeaturesList.add(unitFeature);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");

				try {
					if (unitFeaturesList.get(Integer.parseInt(id) - 1)
							.getUnitDetailId() != null) {
						unitFeaturesList.get(Integer.parseInt(id) - 1)
								.setDetails(unitFeature.getDetails());
						unitService.updateUnitDetail(unitFeaturesList
								.get(Integer.parseInt(id) - 1));
						unitFeaturesList.set(Integer.parseInt(id) - 1,
								unitFeaturesList.get(Integer.parseInt(id) - 1));
					} else {
						unitFeaturesList.set(Integer.parseInt(id) - 1,
								unitFeature);
					}
				} catch (Exception e) {
					e.printStackTrace();
					unitFeaturesList.set(Integer.parseInt(id) - 1, unitFeature);
				}

			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");

				try {
					if (unitFeaturesList.get(Integer.parseInt(id) - 1)
							.getUnitDetailId() != null)
						unitService.deleteUnitDetail(unitFeaturesList
								.get(Integer.parseInt(id) - 1));
				} catch (Exception e) {
					e.printStackTrace();
				}

				unitFeaturesList.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"REUNITFEATURE_" + sessionObj.get("jSessionId"),
					unitFeaturesList);

			return SUCCESS;
		} catch (Exception e) {

			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		} finally {

			ServletActionContext.getRequest().removeAttribute("UNITINFO");
			ServletActionContext.getRequest()
					.removeAttribute("FEATURE_DETAILS");
			ServletActionContext.getRequest().removeAttribute("COMPONENTLIST");
			ServletActionContext.getRequest().removeAttribute("UNITYPELIST");
		}
	}

	@SuppressWarnings("unchecked")
	public String unitAddFinalSave() {

		HttpSession session = ServletActionContext.getRequest().getSession();

		try {

			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			unitInfo = new Unit();
			component = new Component();
			unitTyp = new UnitType();
			Unit unitToEdit =null;
			List<Asset> deletedAssetList=null;
			getImplementId();
			unitInfo.setImplementation(implementation);
			if (unitId != 0) {
				unitToEdit = unitService.getUnitInformation(unitId);
							
				unitInfo.setUnitId(unitId);
				Unit unit1 = unitBL.getUnitService().getUnitDetails(unitId);
				unitInfo.setPerson(unit1.getPerson());
				unitInfo.setCreatedDate(unit1.getCreatedDate());
				if (componentService.getComponentById(componentId)
						.getIsApprove() == Constants.RealEstate.Status.Deny
						.getCode()) {

					unitInfo.setIsApprove(unitToEdit.getIsApprove());
					/*
					 * unitInfo.setIsApprove(Byte
					 * .parseByte(Constants.RealEstate.Status.Hidden .getCode()
					 * + ""));
					 */
				} else {

					if (unitToEdit.getIsApprove() == Constants.RealEstate.Status.Deny
							.getCode()) {
						unitInfo.setIsApprove(Byte
								.parseByte(Constants.RealEstate.Status.NoDecession
										.getCode() + ""));
					} else {
						unitInfo.setIsApprove(unitToEdit.getIsApprove());
					}
				}

			} else {
				
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				
				unitInfo.setPerson(person);
				unitInfo.setCreatedDate(new Date());
				
					unitInfo.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Published
									.getCode() + ""));
			}

			if ((property = (Property) session
					.getAttribute("NEW_PROPERTY_FOR_UNIT")) != null) {

				component = (Component) session
						.getAttribute("NEW_COMPONENT_FOR_UNIT");
				Transaction transaction = (Transaction) session
						.getAttribute("NEW_PROPERTY_TRANSACTION");
				propertyInformationBL.addProperty(property, transaction, null,
						false);
				component.setProperty(property);
				componentService.savePropertyComponents(component, null);
				unitInfo.setComponent(component);
				unitInfo.setRent(property.getRentYr());
				/*
				 * unitInfo.setIsApprove(Byte
				 * .parseByte(Constants.RealEstate.Status.Hidden.getCode() +
				 * ""));
				 */
			} else {

				component.setComponentId(componentId);
				unitInfo.setComponent(component);
				try {
					unitInfo.setRent(Double.parseDouble(rent.replace(",", "")));
				} catch (Exception e) {
					unitInfo.setRent(0.0);
				}
			}

			unitTyp.setUnitTypeId(unitTypeId);
			unitInfo.setUnitType(unitTyp);
			unitInfo.setUnitNo(unitNumber);
			unitInfo.setUnitName(unitName);
			unitInfo.setUnitNameArabic(AIOSCommons.stringToUTFBytes(unitNameArabic));
			// unitInfo.setRooms(rooms);
			unitInfo.setMeter(meterNo);
			unitInfo.setSize(size);
			unitInfo.setSizeUnit(sizeUnit);
			unitInfo.setStatus((byte) status);
			unitInfo.setFurnished(furnished);

			unitInfo.setBedroom(bedroom);
			unitInfo.setBedroomSize(bedroomSize);
			unitInfo.setBathroom(bathroom);
			unitInfo.setBathroomSize(bathroomSize);
			unitInfo.setHall(hall);
			unitInfo.setHallSize(hallSize);
			unitInfo.setKitchen(kitchen);
			unitInfo.setKitchenSize(kitchenSize);
			unitInfo.setServentRoom(serventRoom);
			unitInfo.setDriverRoom(driverRoom);
			unitInfo.setFrontYard(frontYard);
			unitInfo.setBackYard(backYard);
			unitInfo.setFrontGarden(frontGarden);
			unitInfo.setBackGarden(backGarden);
			unitInfo.setGarage(garage);
			unitInfo.setSwimmingPool(swimmingPool);
			unitInfo.setInPantry(inPantry);
			unitInfo.setOutPantry(outPantry);
			unitInfo.setPodeo(podeo);
			unitInfo.setFountain(fountain);
			unitInfo.setGuestRoom(guestRoom);
			unitInfo.setSecurityRoom(securityRoom);
			unitInfo.setKennel(kennel);
			unitInfo.setTennisYard(tennisYard);

			unitFeatureList = (ArrayList<UnitDetail>) session
					.getAttribute("REUNITFEATURE_"
							+ sessionObj.get("jSessionId"));
			List<Asset> assetSessionList = (ArrayList<Asset>) session
					.getAttribute("REUNITASSET_DETAILS_"
							+ sessionObj.get("jSessionId"));
			String delimeter = "#@";
			if (unitAssetDetails != null && !unitAssetDetails.equals("")) {
				String assetArray[] = splitArrayValues(unitAssetDetails,
						delimeter);
				List<String> assetList = Arrays.asList(assetArray);
				for (int i = 0; i < assetList.size(); i++) {
					assetDetailList = RePropertyUnitsTOConverter
							.assetToConverter(assetList);
				}
			} else {
				assetDetailList = null;
			}
			if (assetSessionList != null && assetSessionList.size()>0) {
				for (int i = 0; i < assetSessionList.size(); i++) {
					assetDetailList.get(i).setAssetId(
							(assetSessionList.get(i).getAssetId()));
				}
			}
			//Added by rafiq
			if(unitId != 0 && unitToEdit.getAssets()!=null && unitToEdit.getAssets().size()>0 
					&& assetDetailList!=null && assetDetailList.size()>0)
				deletedAssetList=getDeletedAssetList(assetDetailList,new ArrayList<Asset>(unitToEdit.getAssets()));
			
			unitBL.saveUnit(unitInfo, unitFeatureList, assetDetailList,deletedAssetList);
			for (UnitDetail detail : unitInfo.getUnitDetails()) {
				if (unitId > 0)
					unitBL.saveUploadedImages(detail, true);
				else
					unitBL.saveUploadedImages(detail, false);
			}
			LOGGER.info("Units save success");
			session.removeAttribute("AIOS-img-" + "UnitDetail-"
					+ session.getId());

			unitFeatureList = null;
			session.setAttribute("REUNITFEATURE", unitFeatureList);

			if (unitId != 0)
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Unit Updated Succesfully");
			else
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Unit Added Succesfully");
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			if (unitId != 0)
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Unit Update Failure");
			else
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Unit Add Failure");
			return ERROR;
		} finally {
			session.removeAttribute("NEW_COMPONENT_FOR_UNIT");
			session.removeAttribute("NEW_PROPERTY_FOR_UNIT");
			session.removeAttribute("NEW_PROPERTY_TRANSACTION");
			AIOSCommons.removeUploaderSession(session, "uploadedDocs",
					(long) -1, "Unit");
		}
	}

	public static String[] splitArrayValues(String spiltValue, String delimeter) {
		return spiltValue.split("" + delimeter.trim()
				+ "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public String deleteUnit() {
		try {
			unitInfo = new Unit();
			unitFeatureList = new ArrayList<UnitDetail>();
			List<Asset> assets = new ArrayList<Asset>();
			unitInfo = unitService.getUnitDetails(Long.parseLong(unitId + ""));
			unitFeatureList = unitService.getUnitFeatureDetails(Long
					.parseLong(unitId + ""));
			assets = unitService.getAssetDetails(unitId);
			imageBL.deleteImages(new ArrayList<Object>(unitFeatureList));
			unitService.deleteUnits(unitInfo, unitFeatureList, assets);
			ServletActionContext.getRequest().setAttribute("succMsg",
					"Trnasaction Successfull");

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Trnasaction Failure");
			return ERROR;
		}
	}

	public String getComponentList() {

		try {

			componentList = unitService
					.getAllComponentPropertyBased(propertyId);
			ServletActionContext.getRequest().setAttribute("COMPONENTLIST",
					componentList);

			return SUCCESS;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}

	}
	
	
	public List<Asset> getDeletedAssetList(List<Asset> pageAssets,
			List<Asset> dbAssets){
        List<Asset> deletedAssets=new ArrayList<Asset>();

		Collection<Long> listOne =new ArrayList<Long>();
		Collection<Long> listTwo =new ArrayList<Long>();
		for(Asset asset1:pageAssets){
			if(asset1.getAssetId()!=null && asset1.getAssetId()>0)
				listOne.add(asset1.getAssetId());
		}
		for(Asset asset2:dbAssets){
			if(asset2.getAssetId()!=null && asset2.getAssetId()>0)
				listTwo.add(asset2.getAssetId());
		}
		Collection<Long> similar = new HashSet<Long>( listOne );
        Collection<Long> different = new HashSet<Long>();
        different.addAll( listOne );
        different.addAll( listTwo );
        similar.retainAll( listTwo );
        different.removeAll( similar );
        
      //Delete Process
        for(Long lng:different){
	        for(Asset asset3:dbAssets){
				if(lng!=null && asset3.getAssetId().equals(lng)){
					deletedAssets.add(asset3);
				}
			}
        }
        return deletedAssets;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public String getJsonResult() {
		return jsonResult;
	}

	public void setJsonResult(String jsonResult) {
		this.jsonResult = jsonResult;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public long getComponentId() {
		return componentId;
	}

	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public int getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(int unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public int getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}

	public int getRooms() {
		return rooms;
	}

	public void setRooms(int rooms) {
		this.rooms = rooms;
	}

	public String getRent() {
		return rent;
	}

	public void setRent(String rent) {
		this.rent = rent;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getUnitDetailId() {
		return unitDetailId;
	}

	public void setUnitDetailId(long unitDetailId) {
		this.unitDetailId = unitDetailId;
	}

	public String getUnitDetails() {
		return unitDetails;
	}

	public void setUnitDetails(String unitDetails) {
		this.unitDetails = unitDetails;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getMeterNo() {
		return meterNo;
	}

	public void setMeterNo(String meterNo) {
		this.meterNo = meterNo;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public Integer getSizeUnit() {
		return sizeUnit;
	}

	public void setSizeUnit(Integer sizeUnit) {
		this.sizeUnit = sizeUnit;
	}

	public String getUnitFeatures() {
		return unitFeatures;
	}

	public void setUnitFeatures(String unitFeatures) {
		this.unitFeatures = unitFeatures;
	}

	public String getUnitFeatureDetails() {
		return unitFeatureDetails;
	}

	public void setUnitFeatureDetails(String unitFeatureDetails) {
		this.unitFeatureDetails = unitFeatureDetails;
	}

	public UnitBL getUnitBL() {
		return unitBL;
	}

	public void setUnitBL(UnitBL unitBL) {
		this.unitBL = unitBL;
	}

	/*
	 * public String getUnitDetail() { try{ componentDS = new
	 * ArrayList<RePropertyUnitsTO>(); Unit unit = new Unit(); if(recordId!=0)
	 * unitId=(int)recordId;
	 * 
	 * unit.setUnitId((long) unitId); String f = getFormat(); RePropertyUnitsTO
	 * to = unitBL.getUnitDetails(unit);
	 * to.setUnitDetailImgs(populateUnitDetailImgs(to.getUnitDetailVO())); Image
	 * image = new Image(); image.setRecordId(to.getUnitId());
	 * image.setTableName("Unit"); List<ImageVO> imgList =
	 * imageBL.retrieveCopiedImages(image); to.setUnitImgs(imgList);
	 * to.setLinkLabel("HTML".equals(getFormat())? "Download" : "");
	 * to.setAnchorExpression("HTML".equals(getFormat())?
	 * "downloadPropertyUnitDetail.action?unitId="+ unitId + "&format=PDF" :
	 * "#"); //Approve Content to.setApproveLinkLabel("Approve");
	 * to.setApproveAnchorExpression
	 * ("workflowProcess.action?processFlag="+Byte.parseByte
	 * (Constants.RealEstate.Status.Approved.getCode()+"")); //Disapprove
	 * Content to.setRejectLinkLabel("Reject");
	 * to.setRejectAnchorExpression("workflowProcess.action?processFlag="
	 * +Byte.parseByte(Constants.RealEstate.Status.Disapproved.getCode()+""));
	 * componentDS.add(to); String ctxRealPath =
	 * ServletActionContext.getServletContext().getRealPath("/");
	 * jasperRptParams.put("AIOS_LOGO", "file:///" +
	 * ctxRealPath.concat(File.separator).concat("images" + File.separator +
	 * "aiotech.jpg")); if (componentDS!= null && componentDS.size() == 0){
	 * componentDS.add(to); }else{ componentDS.set(0, to); } }catch (Exception
	 * e){ e.printStackTrace(); } return SUCCESS; }
	 * 
	 * 
	 * private List<ImageVO> populateUnitDetailImgs(List<UnitDetailVO> uDetails)
	 * throws IOException, DataFormatException { List<ImageVO> imgList = new
	 * ArrayList<ImageVO>(); for (UnitDetailVO detail : uDetails) { Image img =
	 * new Image(); img.setRecordId(detail.getUnitDetailId());
	 * img.setTableName("UnitDetail");
	 * imgList.addAll(imageBL.retrieveCopiedImages(img)); } return imgList; }
	 */
	// jasper ;
	private List<RePropertyUnitsTO> componentDS;
	private String format;
	private ImageBL imageBL;

	public List<RePropertyUnitsTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<RePropertyUnitsTO> componentDS) {
		this.componentDS = componentDS;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public int getRowId() {
		return rowId;
	}

	public void setRowId(int rowId) {
		this.rowId = rowId;
	}

	public String getUnitAssetDetails() {
		return unitAssetDetails;
	}

	public void setUnitAssetDetails(String unitAssetDetails) {
		this.unitAssetDetails = unitAssetDetails;
	}

	public List<Asset> getAssetDetailList() {
		return assetDetailList;
	}

	public void setAssetDetailList(List<Asset> assetDetailList) {
		this.assetDetailList = assetDetailList;
	}

	public byte getFurnished() {
		return furnished;
	}

	public void setFurnished(byte furnished) {
		this.furnished = furnished;
	}

	public Byte getBedroom() {
		return bedroom;
	}

	public String getBedroomSize() {
		return bedroomSize;
	}

	public Byte getBathroom() {
		return bathroom;
	}

	public String getBathroomSize() {
		return bathroomSize;
	}

	public Byte getHall() {
		return hall;
	}

	public String getHallSize() {
		return hallSize;
	}

	public Byte getKitchen() {
		return kitchen;
	}

	public String getKitchenSize() {
		return kitchenSize;
	}

	public Byte getServentRoom() {
		return serventRoom;
	}

	public Byte getDriverRoom() {
		return driverRoom;
	}

	public Byte getFrontYard() {
		return frontYard;
	}

	public Byte getBackYard() {
		return backYard;
	}

	public Byte getFrontGarden() {
		return frontGarden;
	}

	public Byte getBackGarden() {
		return backGarden;
	}

	public Byte getGarage() {
		return garage;
	}

	public Byte getSwimmingPool() {
		return swimmingPool;
	}

	public Byte getInPantry() {
		return inPantry;
	}

	public Byte getOutPantry() {
		return outPantry;
	}

	public Byte getPodeo() {
		return podeo;
	}

	public Byte getFountain() {
		return fountain;
	}

	public Byte getGuestRoom() {
		return guestRoom;
	}

	public Byte getSecurityRoom() {
		return securityRoom;
	}

	public Byte getKennel() {
		return kennel;
	}

	public Byte getTennisYard() {
		return tennisYard;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public Component getComponent() {
		return component;
	}

	public Property getProperty() {
		return property;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setBedroom(Byte bedroom) {
		this.bedroom = bedroom;
	}

	public void setBedroomSize(String bedroomSize) {
		this.bedroomSize = bedroomSize;
	}

	public void setBathroom(Byte bathroom) {
		this.bathroom = bathroom;
	}

	public void setBathroomSize(String bathroomSize) {
		this.bathroomSize = bathroomSize;
	}

	public void setHall(Byte hall) {
		this.hall = hall;
	}

	public void setHallSize(String hallSize) {
		this.hallSize = hallSize;
	}

	public void setKitchen(Byte kitchen) {
		this.kitchen = kitchen;
	}

	public void setKitchenSize(String kitchenSize) {
		this.kitchenSize = kitchenSize;
	}

	public void setServentRoom(Byte serventRoom) {
		this.serventRoom = serventRoom;
	}

	public void setDriverRoom(Byte driverRoom) {
		this.driverRoom = driverRoom;
	}

	public void setFrontYard(Byte frontYard) {
		this.frontYard = frontYard;
	}

	public void setBackYard(Byte backYard) {
		this.backYard = backYard;
	}

	public void setFrontGarden(Byte frontGarden) {
		this.frontGarden = frontGarden;
	}

	public void setBackGarden(Byte backGarden) {
		this.backGarden = backGarden;
	}

	public void setGarage(Byte garage) {
		this.garage = garage;
	}

	public void setSwimmingPool(Byte swimmingPool) {
		this.swimmingPool = swimmingPool;
	}

	public void setInPantry(Byte inPantry) {
		this.inPantry = inPantry;
	}

	public void setOutPantry(Byte outPantry) {
		this.outPantry = outPantry;
	}

	public void setPodeo(Byte podeo) {
		this.podeo = podeo;
	}

	public void setFountain(Byte fountain) {
		this.fountain = fountain;
	}

	public void setGuestRoom(Byte guestRoom) {
		this.guestRoom = guestRoom;
	}

	public void setSecurityRoom(Byte securityRoom) {
		this.securityRoom = securityRoom;
	}

	public void setKennel(Byte kennel) {
		this.kennel = kennel;
	}

	public void setTennisYard(Byte tennisYard) {
		this.tennisYard = tennisYard;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public void setComponentList(List<Component> componentList) {
		this.componentList = componentList;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PropertyInformationBL getPropertyInformationBL() {
		return propertyInformationBL;
	}

	public void setPropertyInformationBL(
			PropertyInformationBL propertyInformationBL) {
		this.propertyInformationBL = propertyInformationBL;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public Boolean getIsIdenticalEntry() {
		return isIdenticalEntry;
	}

	public void setIsIdenticalEntry(Boolean isIdenticalEntry) {
		this.isIdenticalEntry = isIdenticalEntry;
	}

	public String getComponentNameArabic() {
		return componentNameArabic;
	}

	public void setComponentNameArabic(String componentNameArabic) {
		this.componentNameArabic = componentNameArabic;
	}

	

	public String getUnitNameArabic() {
		return unitNameArabic;
	}

	public void setUnitNameArabic(String unitNameArabic) {
		this.unitNameArabic = unitNameArabic;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}
	
	
	
	
}
