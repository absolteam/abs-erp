package com.aiotech.aios.realestate.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.service.bl.CancellationBL;
import com.aiotech.aios.realestate.service.bl.ContractAgreementBL;
import com.aiotech.aios.realestate.to.CancellationTO;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CancellationAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(CancellationAction.class);
	
	private CancellationBL cancellationBL;
	private ContractAgreementBL contractAgreementBL;
	private CommentBL commentBL; 
	
	Contract contract=null;
	private long contractId;
	Implementation implementation=null;
	List<CancellationTO> cancelTOList=null;
	private long cancellationId;
	private Boolean isPenalty;
	private Boolean isRequested;
	private double penaltyAmount;
	private long recordId;
	private long alertId;
	private String effectiveDate;
	private WorkflowEnterpriseService workflowEnterpriseService;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	
	public void getImplementId(){
		HttpSession session = ServletActionContext.getRequest()
		.getSession();
		implementation=new Implementation();
		if(session.getAttribute("THIS")!=null){
			implementation=(Implementation)session.getAttribute("THIS");
		}
	}
	public String returnSuccess(){
		// To set the chile record row id to identify.
		//ServletActionContext.getRequest().setAttribute("rowid", id);
		//ServletActionContext.getRequest().setAttribute("AddEditFlag", addEditFlag);
		return SUCCESS;
	}
	
	//Listing JSON
	public String execute() {

		try {
			cancelTOList = new ArrayList<CancellationTO>();
			// Set the value to push to Personal Details DAO
			//CancellationTO cancelTO = new CancellationTO();
			JSONObject jsonResponse = new JSONObject();
			getImplementId();
			//implementationId=(Long) session.get("THIS");
			cancelTOList = cancellationBL.getReleaseListInformation(implementation);
			if (cancelTOList != null && cancelTOList.size() > 0) {
				iTotalRecords = cancelTOList.size();
				iTotalDisplayRecords = cancelTOList.size();
			}
				jsonResponse.put("iTotalRecords", iTotalRecords);
				jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
				JSONArray data = new JSONArray();
				for (CancellationTO list : cancelTOList) {
					JSONArray array = new JSONArray();
					array.add(list.getCancellationId());
					array.add(list.getContractId());
					array.add(list.getContractNumber());
					array.add(list.getTenantName());
					array.add(list.getContractDate());
					array.add(list.getFromDate());
					array.add(list.getToDate());
					
					
					data.add(array);
				}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}
	//Edit View
	public String getCancellationContractInfo(){
		HttpSession session = ServletActionContext.getRequest()
		.getSession();
		try {
			session.removeAttribute("CONTRACT_MAINTAIN_DETAILS");
			session.removeAttribute("AIOS-doc-" + "Cancellation-" + session.getId());
			ReContractAgreementTO agreementTO = new ReContractAgreementTO();
			
			if (recordId != 0)
				cancellationId = (int) recordId;
			
			contract = new Contract();
			Offer offer=new Offer();
			ReContractAgreementTO offerAgreementTO = new ReContractAgreementTO();
			
			 Cancellation cancel=cancellationBL.getCancellationService().getCancellationInfo(cancellationId);
			 CancellationTO cancelTo=new CancellationTO();
			 
			 cancelTo.setCancellationId(cancel.getCancellationId());
			 cancelTo.setContractId(cancel.getContract().getContractId());
			 if(cancel.getIsRequested()!=null)
				 cancelTo.setIsRequested(cancel.getIsRequested());
			 if(cancel.getIsPenalty()!=null)
				 cancelTo.setIsPenalty(cancel.getIsPenalty());

			 cancelTo.setPenaltyAmount(cancel.getPenaltyAmount());
			 if(cancel.getCancellationEffectDate()!=null)
				 cancelTo.setEffectiveDate(DateFormat.convertDateToString(cancel.getCancellationEffectDate().toString()));
			 
			contract = contractAgreementBL.getContractService().getContractDetails(cancel.getContract().getContractId());
			agreementTO = ReContractAgreementTOConverter.convertToTO(contract);
			

			offer=contractAgreementBL.getContractService().getOfferDetails(contract.getOffer().getOfferId());
			offerAgreementTO=ReContractAgreementTOConverter.convertToOfferTO(offer);
			
			List<ReContractAgreementTO> offerTOList = new ArrayList<ReContractAgreementTO>();
			offerTOList = contractAgreementBL.getOfferDeatilsList(Integer.parseInt(contract.getOffer().getOfferId()+""));
			ServletActionContext.getRequest().setAttribute("OFFER_DETAIL_LIST",offerTOList);
			
			//Find Tenent offer
			List<TenantOffer>tenentOffers=contractAgreementBL.getContractService().getTenantOfferDetails(offer.getOfferId());
			Company company=null;
			if(tenentOffers!=null && tenentOffers.size()>0 && tenentOffers.get(0).getPerson()!=null){
			//Find Person
				List<Person>persons=contractAgreementBL.getContractService().getPersonDetails(tenentOffers.get(0).getPerson());
				offerAgreementTO.setTenantName(persons.get(0).getFirstName()+" "+persons.get(0).getLastName());
				offerAgreementTO.setTenantId(Integer.parseInt(persons.get(0).getPersonId().toString()));
				offerAgreementTO.setPresentAddress(persons.get(0).getTownBirth());
				offerAgreementTO.setPermanantAddress(persons.get(0).getRegionBirth());
				offerAgreementTO.setTenantNationality(this.getCancellationBL().getSystemService()
						.getCountryById(Long.parseLong(persons.get(0).getNationality())).getCountryName());
				if(persons.get(0).getMobile()!=null){
				offerAgreementTO.setTenantFax(persons.get(0).getMobile().toString());
				offerAgreementTO.setTenantMobile(persons.get(0).getMobile().toString());
				}
				if(persons.get(0).getPostboxNumber()!=null)
				offerAgreementTO.setTenantPOBox(persons.get(0).getPostboxNumber());
			}else{
				company=new Company();
				company=contractAgreementBL.getOfferService().getCompanyDetails(tenentOffers.get(0).getCompany().getCompanyId());
				if(company!=null){
					offerAgreementTO.setTenantName(company.getCompanyName());
					offerAgreementTO.setTenantId(Integer.parseInt(company.getCompanyId().toString()));
					offerAgreementTO.setPresentAddress(company.getDescription());
					offerAgreementTO.setTenantIssueDate("");
					offerAgreementTO.setTenantIdentity(company.getTradeNo());
					offerAgreementTO.setTenantNationality("");
					offerAgreementTO.setTenantFax("");
					offerAgreementTO.setTenantMobile("");
					offerAgreementTO.setTenantPOBox("");
				}
			}
			
			ServletActionContext.getRequest().setAttribute("OFFER_INFO", offerAgreementTO);
			ServletActionContext.getRequest().setAttribute("bean", agreementTO);
			
			
			ServletActionContext.getRequest().setAttribute("CANCELLATION_INFO", cancelTo);
			
			//Property Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if(recordId!=0){
					comment.setRecordId(Long.parseLong(cancellationId+""));
					comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Component");
					comment=commentBL.getCommentInfo(comment);
				
			}
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO", comment);
			//Comment process End
			
			// for printout
			session.setAttribute("AGREEMENT_DETAILS", agreementTO);
			session.setAttribute("AGREEMENT_TENANT_DETAILS", offerAgreementTO);
			session.setAttribute("AGREEMENT_OFFER_DETAILS", offerTOList);
			
			
			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}
	
	//Save
	public String cancelContractSave() {

		try {
			/*HttpSession session = ServletActionContext.getRequest()
			.getSession();*/
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			contract = new Contract();
			Cancellation cancelToEdit=cancellationBL.getCancellationService().getCancellationInfo(cancellationId);

			Cancellation cancel=new Cancellation();
				contract.setContractId(contractId);
				cancel.setContract(contract);
				getImplementId();
				cancel.setImplementation(implementation);
				cancel.setPenaltyAmount(penaltyAmount);
				cancel.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Confirm
								.getCode()
								+ ""));
				cancel.setCreatedDate(new Date());
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				cancel.setPerson(person);
			
			cancellationBL.getCancellationService().cancelSave(cancel);
			
			if (cancel.getRelativeId() == null) {
				cancel.setRelativeId(cancel.getCancellationId());
				cancellationBL.getCancellationService().cancelSave(cancel); 
			}
			
			
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Cancellation", cancel.getCancellationId(), user,
					workflowDetailVo);


			LOGGER.info("Canellation save success");

			if (cancellationId != 0)
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Request Initiated Succesfully");
			else
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Request Initiated Succesfully");
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			if (cancellationId != 0)
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Request Initiation Failure");
			else
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Request Initiation Failure");
			return SUCCESS;
		}
	}

	//Final save
	public String finalSaveCancellation() { 
		try {
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			Cancellation  cancel=cancellationBL.getCancellationService().getCancellationInfo(cancellationId);
			cancel.setIsPenalty(isPenalty);
			cancel.setIsRequested(isRequested);
			cancel.setCancellationEffectDate(DateFormat.convertStringToDate(effectiveDate));
			Contract contract = this.getCancellationBL().getContractService().getContractOffer(
					cancel.getContract().getContractId());
			cancel.setContractEndDate(contract.getOffer().getEndDate()); 
			Offer offer=new Offer(); 
			offer = contract.getOffer();
			if(cancel.getIsPenalty())
				cancel.setPenaltyAmount(cancellationBL.getPenantyAmount(cancel, offer));
		
			cancel.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.NoDecession
							.getCode()
							+ ""));
			cancellationBL.getCancellationService().cancelSave(cancel);
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Cancellation", cancel.getCancellationId(), user,
					workflowDetailVo);
			ServletActionContext.getRequest().setAttribute("succMsg",
						"Data saved Succesfully");
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
						"Data Save Failure");
			return SUCCESS;
		}
	}
	
	public String afterfinalSave() { 
		try {
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			Cancellation  cancel=cancellationBL.getCancellationService().getCancellationInfo(cancellationId);
			
			Release release =new Release();
			release.setContract(cancel.getContract());
			release.setIsInitialize(true);
			release.setReleaseEffectDate(cancel.getCancellationEffectDate());
			getImplementId();
			release.setImplementation(implementation);
			release.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.Confirm
							.getCode()
							+ ""));
			release.setCreatedDate(new Date());
			Person person = new Person();
			person.setPersonId(user.getPersonId());
			release.setPerson(person);
			cancellationBL.getReleaseService().updateRelease(release); 
			ServletActionContext.getRequest().setAttribute("succMsg",
						"Data saved Succesfully");
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Release", release.getReleaseId(), user, workflowDetailVo);
			
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
						"Data Save Failure");
			return SUCCESS;
		}
	}
	/*public String showCancelContractAlert() {
		try {
			LOGGER.info("Inside Module RE: Method showCancelContractAlert()");
			CancellationTO cancellationTO = this.getCancellationBL()
					.processContractView(recordId);
			cancellationTO.setAlertId(alertId);
			ServletActionContext.getRequest().setAttribute(
					"CANCELLATION_ALERT", cancellationTO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module RE: Method showCancelContractAlert() in expection "
					+ ex);
			return ERROR;
		}
	}*/
	
	public CancellationBL getCancellationBL() {
		return cancellationBL;
	}

	public void setCancellationBL(CancellationBL cancellationBL) {
		this.cancellationBL = cancellationBL;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}
	public ContractAgreementBL getContractAgreementBL() {
		return contractAgreementBL;
	}
	public void setContractAgreementBL(ContractAgreementBL contractAgreementBL) {
		this.contractAgreementBL = contractAgreementBL;
	}
	public long getCancellationId() {
		return cancellationId;
	}
	public void setCancellationId(long cancellationId) {
		this.cancellationId = cancellationId;
	}
	public Boolean getIsPenalty() {
		return isPenalty;
	}
	public void setIsPenalty(Boolean isPenalty) {
		this.isPenalty = isPenalty;
	}
	public Boolean getIsRequested() {
		return isRequested;
	}
	public void setIsRequested(Boolean isRequested) {
		this.isRequested = isRequested;
	}
	public double getPenaltyAmount() {
		return penaltyAmount;
	}
	public void setPenaltyAmount(double penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}
	public long getRecordId() {
		return recordId;
	}
	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}
	public CommentBL getCommentBL() {
		return commentBL;
	}
	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}
	public long getAlertId() {
		return alertId;
	}
	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	} 
	
}
