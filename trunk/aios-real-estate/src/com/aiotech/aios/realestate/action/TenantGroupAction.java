package com.aiotech.aios.realestate.action;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.service.TenantGroupService;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.opensymphony.xwork2.ActionSupport;

public class TenantGroupAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private TenantGroupService tenantGroupService;

	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String addEditFlag;

	private Integer tenantGrpId;
	private String tenantGrpTitle;
	private Double deposit;
	private Double contractFee;
	private Double rent;
	private Double penalty;
	private String details;
	private Set<TenantOffer> tenantOffers = new HashSet<TenantOffer>(0);

	public String returnSuccess() {

		return SUCCESS;
	}

	public String getAllTenantGroups() {

		try {
			List<TenantGroup> groups = tenantGroupService
					.getAllGroups(getImplementation());

			iTotalRecords = groups.size();
			iTotalDisplayRecords = groups.size();

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();

			for (TenantGroup group : groups) {

				JSONArray array = new JSONArray();
				array.add(group.getTenantGrpId());
				array.add(group.getTenantGrpTitle().split("_")[0]);
				array.add(group.getDeposit());
				array.add(group.getContractFee());
				array.add(group.getRent());
				array.add(group.getPenalty());
				array.add(group.getDetails());

				data.add(array);
			}

			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String addEditTenantGroup() throws Exception {

		TenantGroup tenantGroup = new TenantGroup();
		if (tenantGrpId != null && tenantGrpId != 0)
			tenantGroup.setTenantGrpId(tenantGrpId);
		else
			tenantGroup.setTenantGrpId(null);
		tenantGroup.setTenantGrpTitle(tenantGrpTitle.concat("_" + getImplementation().getImplementationId()));
		tenantGroup.setDeposit(deposit);
		tenantGroup.setContractFee(contractFee);
		tenantGroup.setRent(rent);
		tenantGroup.setPenalty(penalty);
		tenantGroup.setDetails(details);
		tenantGroup.setImplementation(getImplementation());

		tenantGroupService.addUpdateTenantGroup(tenantGroup);
		
		return SUCCESS;
	}

	public String deleteTenantGroup() {

		TenantGroup tenantGroup = new TenantGroup();
		if (tenantGrpId != 0) {
			
			tenantGroup.setTenantGrpId(tenantGrpId);
			tenantGroupService.deleteTenantGroup(tenantGroup);
		}		
		return SUCCESS;
	}

	public TenantGroupService getTenantGroupService() {
		return tenantGroupService;
	}

	public void setTenantGroupService(TenantGroupService tenantGroupService) {
		this.tenantGroupService = tenantGroupService;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public Integer getTenantGrpId() {
		return tenantGrpId;
	}

	public String getTenantGrpTitle() {
		return tenantGrpTitle;
	}

	public Double getDeposit() {
		return deposit;
	}

	public Double getContractFee() {
		return contractFee;
	}

	public Double getRent() {
		return rent;
	}

	public Double getPenalty() {
		return penalty;
	}

	public String getDetails() {
		return details;
	}

	public Set<TenantOffer> getTenantOffers() {
		return tenantOffers;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public void setTenantGrpId(Integer tenantGrpId) {
		this.tenantGrpId = tenantGrpId;
	}

	public void setTenantGrpTitle(String tenantGrpTitle) {
		this.tenantGrpTitle = tenantGrpTitle;
	}

	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}

	public void setContractFee(Double contractFee) {
		this.contractFee = contractFee;
	}

	public void setRent(Double rent) {
		this.rent = rent;
	}

	public void setPenalty(Double penalty) {
		this.penalty = penalty;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setTenantOffers(Set<TenantOffer> tenantOffers) {
		this.tenantOffers = tenantOffers;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}
}
