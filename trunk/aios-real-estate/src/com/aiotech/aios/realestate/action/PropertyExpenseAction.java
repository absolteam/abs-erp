package com.aiotech.aios.realestate.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.common.to.Constants.RealEstate.PropertyExpenseType;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.realestate.domain.entity.PropertyExpenseDetail;
import com.aiotech.aios.realestate.service.bl.PropertyExpenseBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.opensymphony.xwork2.ActionSupport;

public class PropertyExpenseAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private long expenseId;
	private String date;
	private Byte expenseType;
	private String description;
	private String voucherNumber;
	private String expenseDetails;
	private long propertyId;
	private Integer id;
	private String returnMessage;
	private long recordId;
	private long alertId;
	private PropertyExpenseBL propertyExpenseBL;

	private static final Logger LOGGER = LogManager
			.getLogger(PropertyExpenseAction.class);

	public String returnSuccess() {
		LOGGER.info("Module: RE : Method: returnSuccess");
		return SUCCESS;
	}

	public String showPropertyExpenseList() {
		LOGGER.info("Inside Module: RE : Method: showPropertyExpenseList");
		try {
			JSONObject jsonList = this.getPropertyExpenseBL()
					.getPropertyExpenseList();
			ServletActionContext.getRequest()
					.setAttribute("jsonlist", jsonList);
			LOGGER.info("Module: RE : Method: showPropertyExpenseList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: showPropertyExpenseList Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPropertyExpenseEntry() {
		LOGGER.info("Inside Module: RE : Method: showPropertyExpenseEntry");
		try {
			if (expenseId > 0) {
				PropertyExpense propertyExpense = this
						.getPropertyExpenseBL()
						.getPropertyExpenseService()
						.getPropertyExpenseById(expenseId);
				List<PropertyExpenseDetail> expenseDetailList = this
						.getPropertyExpenseBL().getPropertyExpenseService()
						.getPropertyExpenseDetail(expenseId);
				ServletActionContext.getRequest().setAttribute(
						"PROPERTY_EXPENSE", propertyExpense);
				ServletActionContext.getRequest().setAttribute(
						"PROPERTY_EXPENSE_DETAIL", expenseDetailList);
			} else {
				voucherNumber = SystemBL.getReferenceStamp(
						PropertyExpense.class.getName(), this
								.getPropertyExpenseBL().getImplementationId());
				ServletActionContext.getRequest().setAttribute(
						"REFERENCE_NUMBER", voucherNumber);
			}
			Map<Byte, PropertyExpenseType> expenseTypes = new HashMap<Byte, PropertyExpenseType>();
			for (PropertyExpenseType e : EnumSet
					.allOf(PropertyExpenseType.class)) {
				expenseTypes.put(e.getCode(), e);
			}
			ServletActionContext.getRequest().setAttribute("EXPENSE_TYPE",
					expenseTypes);
			LOGGER.info("Module: RE : Method: showPropertyExpenseEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: showPropertyExpenseEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Show Direct Payment to process expense payment
	public String showPaymentAlert() {
		LOGGER.info("Inside Module: RE : Method: showPaymentAlert");
		try {
			String paymentNumber = "";
			if (recordId != 0)
				expenseId = recordId;
			List<Currency> currencyList = null;
			PropertyExpense propertyExpense = this
					.getPropertyExpenseBL()
					.getPropertyExpenseService()
					.getPropertyExpenseById(expenseId);
			currencyList = this.getPropertyExpenseBL().getDirectPaymentBL()
					.getTransactionBL().getCurrencyService()
					.getAllCurrency(propertyExpense.getImplementation());
			// Direct Payment set process

			DirectPayment directPayment = new DirectPayment();
			List<DirectPayment> paymentList = this.getPropertyExpenseBL()
					.getDirectPaymentBL().getDirectPaymentService()
					.getAllDirectPayments(propertyExpense.getImplementation());
			if (null != paymentList && paymentList.size() > 0) {
				List<Long> directPaymentNumber = new ArrayList<Long>();
				for (DirectPayment list : paymentList) {
					directPaymentNumber.add(Long.parseLong(list
							.getPaymentNumber()));
				}
				paymentNumber = String.valueOf(Collections
						.max(directPaymentNumber));
				paymentNumber = String
						.valueOf(Long.parseLong(paymentNumber) + 1);
			} else {
				paymentNumber = "1000";
			}
			directPayment.setPaymentNumber(paymentNumber);
			directPayment.setRecordId(expenseId);
			directPayment.setUseCase(PropertyExpense.class.getSimpleName());
			// Direct Payment details creation
			directPayment
					.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
							getDirectPaymentDetailList(propertyExpense)));

			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					propertyExpenseBL.getDirectPaymentBL().getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					propertyExpenseBL.getDirectPaymentBL().getReceiptSource());
			LOGGER.info("Module: RE : Method: showPaymentAlert: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: showPaymentAlert Exception " + ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Create Direct payment Detail
	public List<DirectPaymentDetail> getDirectPaymentDetailList(
			PropertyExpense propertyExpense) throws Exception {
		List<DirectPaymentDetail> directPaymentDetails = new ArrayList<DirectPaymentDetail>();
		DirectPaymentDetail directPaymentDetail = null;
		Combination combination = this.getPropertyExpenseBL()
				.getBuildingExpenseAccount(propertyExpense);
		for (PropertyExpenseDetail list : propertyExpense
				.getPropertyExpenseDetails()) {
			directPaymentDetail = new DirectPaymentDetail();
			directPaymentDetail.setInvoiceNumber(list.getReferenceNo());
			directPaymentDetail.setCombination(combination);
			directPaymentDetail.setDescription(list.getDescription());
			directPaymentDetail.setAmount(list.getAmount());
			directPaymentDetails.add(directPaymentDetail);
		}
		return directPaymentDetails;
	}

	public String processPaymentPrint() {
		LOGGER.info("Inside Module: RE : Method: processPaymentPrint");
		try {
			PropertyExpense propertyExpense = this
					.getPropertyExpenseBL()
					.getPropertyExpenseService()
					.getPropertyExpenseById(expenseId);
			List<PropertyExpenseDetail> expenseDetailList = this
					.getPropertyExpenseBL().getPropertyExpenseService()
					.getPropertyExpenseDetail(expenseId);
			propertyExpense.setPrintTaken(true);
			this.getPropertyExpenseBL().getPropertyExpenseService()
					.updatePropertyExpense(propertyExpense);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_EXPENSE_PRINT", propertyExpense);
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_EXPENSE_DETAIL_PRINT", expenseDetailList);
			LOGGER.info("Module: Accounts : Method: showDirectPayment Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: processPaymentPrint Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String processPaymentPrintView() {
		LOGGER.info("Inside Module: RE : Method: processPaymentPrintView");
		try {
			PropertyExpense propertyExpense = this
					.getPropertyExpenseBL()
					.getPropertyExpenseService()
					.getPropertyExpenseById(expenseId);
			List<PropertyExpenseDetail> expenseDetailList = this
					.getPropertyExpenseBL().getPropertyExpenseService()
					.getPropertyExpenseDetail(expenseId);
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_EXPENSE_PRINT", propertyExpense);
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_EXPENSE_DETAIL_PRINT", expenseDetailList);
			LOGGER.info("Module: Accounts : Method: processPaymentPrintView Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: processPaymentPrintView Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Save Property Expense & Alert Direct payment
	public String savePropertyExpense() {
		LOGGER.info("Inside Module: RE : Method: savePropertyExpense()");
		try {
			PropertyExpense expense = new PropertyExpense();
			if (expenseId > 0) {
				expense.setExpenseId(expenseId);
				DirectPayment directPayment = this
						.getPropertyExpenseBL()
						.getDirectPaymentBL()
						.getDirectPaymentService()
						.getDirectPaymentByRecordIdAndUseCase(expenseId,
								PropertyExpense.class.getSimpleName());
				if (null != directPayment && !("").equals(directPayment)) {
					returnMessage = "Payment has been made for this expense.";
					ServletActionContext.getRequest().setAttribute(
							"RETURN_MESSAGE", returnMessage);
					LOGGER.info("Module: RE : Method: savePropertyExpense(): Action error");
					return ERROR;
				}
			}
			expense.setExpenseNumber(voucherNumber);
			expense.setDate(DateFormat.convertStringToDate(date));
			expense.setExpenseType(expenseType);
			expense.setDescription(description);
			Property property = new Property();
			property.setPropertyId(propertyId);
			expense.setProperty(property);
			expense.setImplementation(this.getPropertyExpenseBL()
					.getImplementationId());
			PropertyExpenseDetail expenseDetail = null;
			List<PropertyExpenseDetail> expenseDetailList = new ArrayList<PropertyExpenseDetail>();
			String[] lineDetailArray = splitValues(expenseDetails, "@@");
			for (String string : lineDetailArray) {
				String[] detailList = splitValues(string, "__");
				expenseDetail = new PropertyExpenseDetail();
				expenseDetail.setAmount(Double.parseDouble(detailList[0]));
				if (null != detailList[1] && !detailList[1].equals("##"))
					expenseDetail.setDescription(detailList[1]);
				if (null != detailList[2] && !detailList[2].equals("##"))
					expenseDetail.setReferenceNo(detailList[2]);
				if (Long.parseLong(detailList[3]) > 0)
					expenseDetail.setExpenseDetailId(Long
							.parseLong(detailList[3]));

				expenseDetailList.add(expenseDetail);
			}
			this.getPropertyExpenseBL().savePropertyExpense(expense,
					expenseDetailList);
			returnMessage = "SUCCESS";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			LOGGER.info("Module: RE : Method: savePropertyExpense(): Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: savePropertyExpense() Exception "
					+ ex);
			returnMessage = "Internal Error!";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String deletePropertyExpense() {
		LOGGER.info("Inside Module: RE : Method: deletePropertyExpense");
		try {
			PropertyExpense propertyExpense = this
					.getPropertyExpenseBL()
					.getPropertyExpenseService()
					.getPropertyExpenseById(expenseId);
			boolean deleted = this.getPropertyExpenseBL()
					.deletePropertyExpense(propertyExpense);
			if (deleted) {
				returnMessage = "SUCCESS";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: RE : Method: deletePropertyExpense(): Action Success");
				return SUCCESS;
			} else {
				returnMessage = "Payment has been marked as void, can't be delete.";
				ServletActionContext.getRequest().setAttribute(
						"RETURN_MESSAGE", returnMessage);
				LOGGER.info("Module: RE : Method: deletePropertyExpense(): Action error");
				return ERROR;
			}

		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: deletePropertyExpense Exception "
					+ ex);
			returnMessage = "Internal Error!";
			ServletActionContext.getRequest().setAttribute("RETURN_MESSAGE",
					returnMessage);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String showPropertyExpenseAddRow() {
		LOGGER.info("Inside Module: RE : Method: showPropertyExpenseAddRow");
		try {
			ServletActionContext.getRequest().setAttribute("ROW_ID", id);
			LOGGER.info("Module: RE : Method: showPropertyExpenseEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: RE : Method: showPropertyExpenseEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public PropertyExpenseBL getPropertyExpenseBL() {
		return propertyExpenseBL;
	}

	public void setPropertyExpenseBL(PropertyExpenseBL propertyExpenseBL) {
		this.propertyExpenseBL = propertyExpenseBL;
	}

	public long getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(long expenseId) {
		this.expenseId = expenseId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Byte getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(Byte expenseType) {
		this.expenseType = expenseType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getExpenseDetails() {
		return expenseDetails;
	}

	public void setExpenseDetails(String expenseDetails) {
		this.expenseDetails = expenseDetails;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
}
