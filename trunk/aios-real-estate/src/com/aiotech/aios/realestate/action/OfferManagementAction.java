package com.aiotech.aios.realestate.action;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.vo.PropertyVO;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.OfferService;
import com.aiotech.aios.realestate.service.TenantGroupService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.service.bl.OfferManagementBL;
import com.aiotech.aios.realestate.to.OfferManagementTO;
import com.aiotech.aios.realestate.to.converter.OfferTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class OfferManagementAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	// Common Variables
	private int offerId;
	private int propertyId;
	private int componentId;
	private int unitId;
	private int offerNumber;
	private String offerDate;
	private int validityDays;
	private int extensionDays;
	private String startDate;
	private String endDate;
	private int id;
	private String showPage;
	private String propertyName;
	private double propertyRent;
	private int componentFlag;
	private String offerFor;
	private double offerAmount;
	private String sqlReturnMessage;
	private int tenantId;
	private String contractFrom;
	private String contractTo;
	private String propertyType;
	private String offerStringAmount;
	private int tenantGroupId;
	private int tenantOfferId;
	private byte offerStatus;
	private String offerTenantFor;
	private String unitTypeName;
	private Double securityDeposit;
	private Long personBankId;
	private String offerPrintOption1;

	// Grid Variables
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private long recordId;

	// Reporting
	private String reportFilter;
	private InputStream fileInputStream;
	private String tenantName;
	private String unitName;

	// Common Objects
	private OfferService offerService;
	private OfferManagementBL offerBL;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private OfferManagementTO offerManagementTO = null;
	private List<OfferManagementTO> offerList = null;
	Implementation implementation = null;
	private UnitService unitService;
	private ComponentService componentService;
	private ContractService contractService;
	TenantGroupService tenantGroupService;
	private String componentList;
	private String unitList;
	private List<Component> compList = null;
	private List<Unit> unitLists = null;
	private CommentBL commentBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	// Logger Property
	private static final Logger LOGGER = LogManager
			.getLogger(OfferManagementAction.class);

	/** To redirect the action success page **/
	public String returnSuccess() {
		LOGGER.info("Module: Realestate : Method: returnSuccess");
		ServletActionContext.getRequest().setAttribute("rowId", id);
		return SUCCESS;
	}

	// to list the grid values
	public String showOfferFormList() {
		LOGGER.info("Module: Realestate : Method: showOfferFormList");
		try {
			offerList = new ArrayList<OfferManagementTO>();
			offerManagementTO = new OfferManagementTO();
			getImplementId();
			List<Offer> offerEntityList = offerService
					.getAllOffers(implementation);
			offerList = OfferTOConverter.convertToTOList(offerEntityList);

			iTotalRecords = offerList.size();
			iTotalDisplayRecords = offerList.size();
			if (offerList != null && offerList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showOfferFormList, "
						+ " Component List size() " + offerList.size());
			}

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (OfferManagementTO list : offerList) {
				JSONArray array = new JSONArray();
				array.add(list.getOfferId());
				array.add(list.getOfferNumber() + "");

				// Find Tenent offer
				List<TenantOffer> tenentOffers = this.contractService
						.getTenantOfferDetails(list.getOfferId());
				Company company = null;
				if (tenentOffers != null && tenentOffers.size() > 0
						&& tenentOffers.get(0).getPerson() != null) {
					// Find Person
					List<Person> persons = this.contractService
							.getPersonDetails(tenentOffers.get(0).getPerson());
					list.setTenantName(persons.get(0).getFirstName() + " "
							+ persons.get(0).getLastName());
				} else {
					company = new Company();
					company = offerService.getCompanyDetails(tenentOffers
							.get(0).getCompany().getCompanyId());
					if (company != null) {
						list.setTenantName(company.getCompanyName());
					}
				}
				array.add(list.getTenantName() + "");
				array.add(list.getOfferDate() + "");
				array.add(getOfferCreatedBy(list.getCreatedId()));
				array.add(list.getCreatedDate());
				array.add("");// Status
				array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"offer_print\">Print</a>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Realestate : Method: showOfferFormList: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: showOfferFormList: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getOfferCreatedBy(long personId) throws Exception {
		Person person = offerBL.getPersonService().getPersonDetails(personId);
		return person.getFirstName() + " " + person.getMiddleName() + " "
				+ person.getLastName();
	}

	// to get add canvas view
	public String showCanvasAccordian() {
		LOGGER.info("Module: Realestate : Method: showCanvasAccordian");
		try {
			// Get Property Type List
			List<PropertyType> propertyTypeList = offerService
					.getAllPropertyTypes();
			offerList = OfferTOConverter
					.convertPropertyTypeToTOList(propertyTypeList);
			ServletActionContext.getRequest().setAttribute("PROPERTY_TYPE",
					offerList);
			// To get property details
			getImplementId();
			List<Property> propertyList = offerService
					.getOfferProperties(implementation);
			offerList = new ArrayList<OfferManagementTO>();
			offerList = OfferTOConverter.convertPropertyToTOList(propertyList);
			for (OfferManagementTO list : offerList) {
				boolean flag = true;
				if (list.getPropertyType().trim().equalsIgnoreCase("building")
						|| list.getPropertyType().trim()
								.equalsIgnoreCase("compound")) {
					int totalComponents = 0;
					compList = new ArrayList<Component>();
					compList = componentService.getComponentsForProperty(list
							.getPropertyId());
					if (compList != null && !compList.equals(""))
						totalComponents = compList.size();
					else
						totalComponents = 0;
					offerManagementTO = new OfferManagementTO();
					if (totalComponents != list.getPropertyFloors()) {
						offerManagementTO.setProcessStatus(false);
					} else {
						for (Component component : compList) {
							if (flag == true) {
								unitLists = unitService
										.getUnitListComponentBased(component
												.getComponentId());
								int totalUnits = 0;
								totalComponents = 0;
								if (unitLists != null && !unitLists.equals(""))
									totalUnits = unitLists.size();
								else
									totalComponents = 0;
								if (totalUnits != component.getNoOfUnits()) {
									flag = false;
								} else {
									flag = true;
								}
							}
						}
						if (flag == true)
							list.setProcessStatus(true);
						else
							list.setProcessStatus(false);
					}
				} else {
					list.setProcessStatus(true);
				}
			}
			City city = new City();
			Country country = new Country();
			State state = new State();
			for (int i = 0; i < offerList.size(); i++) {
				if (offerList.get(i).isProcessStatus() == true) {
					city = offerService.getCityById(Long.parseLong(offerList
							.get(i).getCityId() + ""));
					offerList.get(i).setCityName(city.getCityName());
					country = offerService.getCountryById(Long
							.parseLong(offerList.get(i).getCountryId() + ""));
					offerList.get(i).setCountryName(country.getCountryName());
					state = offerService.getStateById(Long.parseLong(offerList
							.get(i).getStateId() + ""));
					offerList.get(i).setStateName(state.getStateName());
				}
			}
			LOGGER.info("Module: Realestate : Method: showOfferFormList: Action Success");
			ServletActionContext.getRequest().setAttribute("PROPERTY_INFO",
					offerList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: showCanvasAccordian: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// To get building details
	public String showBuildingDetails() {
		LOGGER.info("Module: Realestate : Method: showBuildingDetails");
		try {
			// To Get Component Details
			PropertyVO propertyVO = offerBL.getComponentUnitDetails(Long
					.parseLong(propertyId + ""));
			List<OfferManagementTO> offerList = offerBL.getOfferedDetails(Long
					.parseLong(propertyId + ""));
			if (offerList != null && !offerList.equals("")
					&& offerList.size() > 0) {
				if (offerList.get(0).getPropertyId() > 0) {
					propertyVO
							.setOfferNumber(offerList.get(0).getOfferNumber());
					propertyVO.setTenantName(offerList.get(0).getTenantName());
				} else {
					for (OfferManagementTO list : offerList)
						propertyVO = offerBL.offeredForUnits(propertyVO, list);
				}
			}
			ServletActionContext.getRequest().setAttribute(
					"OFFER_PROPERTY_INFO", propertyVO);
			if (propertyVO.getPropertyType().getType().trim()
					.equalsIgnoreCase("Compound"))
				return "compound";
			if (propertyVO.getPropertyType().getType().trim()
					.equalsIgnoreCase("flat")
					|| propertyVO.getPropertyType().getType().trim()
							.equalsIgnoreCase("shop")
					|| propertyVO.getPropertyType().getType().trim()
							.equalsIgnoreCase("villa"))
				return "flat";
			LOGGER.info("Module: Realestate : Method: showBuildingDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: showBuildingDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get tenant details
	public String showTenants() {
		LOGGER.info("Module: Realestate : Method: showTenants");
		try {
			offerList = new ArrayList<OfferManagementTO>();
			getImplementId();
			List<Person> tenantList = offerService
					.getAllTenants(implementation);
			offerList.addAll(OfferTOConverter.convertTenantToTO(tenantList));
			int companyType = 6;
			List<Company> companyList = offerService.getAllTenantCompanies(
					implementation, companyType);
			offerList.addAll(OfferTOConverter.convertCompanyToTO(companyList));
			ServletActionContext.getRequest().setAttribute("TENANT_DETAILS",
					offerList);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (OfferManagementTO list : offerList) {
				JSONArray array = new JSONArray();
				array.add(list.getTenantId() + "@#" + list.getAddressDetails());
				array.add(list.getTenantName());
				array.add(list.getTenantNumber());
				array.add(list.getAddressDetails());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			LOGGER.info("Module: Realestate : Method: showTenants: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: showTenants: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get offer line entry
	public String showOfferLineDetails() {
		LOGGER.info("Module: Realestate : Method: showOfferLineDetails");
		try {
			ServletActionContext.getRequest()
					.setAttribute("showPage", showPage);
			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest()
					.setAttribute("offerFor", offerFor);
			LOGGER.info("Module: Accounts : Method: showOfferLineDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: showOfferLineDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to store session add add line details
	@SuppressWarnings("unchecked")
	public String saveOfferAddLineDetails() {
		LOGGER.info("Module: Realestate : Method: saveOfferAddLineDetails");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			OfferDetail offerDetail = new OfferDetail();
			Component component = new Component();
			Unit unit = new Unit();
			Property property = new Property();
			List<OfferDetail> detailList = null;

			offerDetail.setOfferAmount(offerAmount);
			component.setComponentId(Long.parseLong(componentId + ""));
			unit.setUnitId(Long.parseLong(unitId + ""));
			offerDetail.setComponent(component);
			offerDetail.setUnit(unit);
			offerDetail.setProperty(property);

			if (session.getAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId")) == null)
				detailList = new ArrayList<OfferDetail>();
			else
				detailList = (List<OfferDetail>) session
						.getAttribute("OFFER_LINES_INFO_"
								+ sessionObj.get("jSessionId"));
			detailList.add(offerDetail);
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveOfferAddLineDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: saveOfferAddLineDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to edit session store object
	@SuppressWarnings("unchecked")
	public String saveOfferAddEditLineDetails() {
		LOGGER.info("Module: Realestate : Method: saveOfferAddEditLineDetails");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			OfferDetail offerDetail = new OfferDetail();
			Component component = new Component();
			Unit unit = new Unit();
			Property property = new Property();
			List<OfferDetail> detailList = null;

			offerDetail.setOfferAmount(offerAmount);
			component.setComponentId(Long.parseLong(componentId + ""));
			unit.setUnitId(Long.parseLong(unitId + ""));
			offerDetail.setComponent(component);
			offerDetail.setUnit(unit);
			offerDetail.setProperty(property);
			detailList = (List<OfferDetail>) session
					.getAttribute("OFFER_LINES_INFO_"
							+ sessionObj.get("jSessionId"));
			detailList.set(id - 1, offerDetail);
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveOfferAddEditLineDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: saveOfferAddEditLineDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to remove session store object
	@SuppressWarnings("unchecked")
	public String saveOfferAddDeleteLineDetails() {
		LOGGER.info("Module: Realestate : Method: saveOfferAddDeleteLineDetails");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			List<OfferDetail> detailList = null;
			detailList = (List<OfferDetail>) session
					.getAttribute("OFFER_LINES_INFO_"
							+ sessionObj.get("jSessionId"));
			detailList.remove(id - 1);
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveOfferAddDeleteLineDetails: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: saveOfferAddDeleteLineDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to discard from session details
	public String discardOfferDetails() {
		LOGGER.info("Module: Realestate : Method: discardOfferDetails");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			LOGGER.info("Module: Accounts : Method: discardOfferDetails Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: discardOfferDetails: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to get offer entry screen
	public String showAddOfferEntry() {
		LOGGER.info("Module: Realestate : Method: showAddOfferEntry");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			AIOSCommons.removeUploaderSession(session, "offerDocs", (long) -1,
					"Offer");
			List<OfferDetail> detailList = null;
			detailList = new ArrayList<OfferDetail>();
			offerManagementTO = new OfferManagementTO();
			offerList = new ArrayList<OfferManagementTO>();
			if (propertyType != null && !propertyType.equals("")
					&& !propertyType.equalsIgnoreCase("villa")) {
				if (componentList != null && !componentList.equals("")) {
					String[] componentArray = splitValues(componentList);
					List<String> componentList = Arrays.asList(componentArray);
					offerList = OfferTOConverter.convertStringTOList(
							componentList, propertyName);
					detailList.addAll(OfferTOConverter
							.convertComponentEntity(offerList));
					offerManagementTO.setComponentList(offerList);
					offerManagementTO.setOfferFor("COMPONENT");
				} else if (unitList != null && !unitList.equals("")) {
					String[] unitArray = splitValues(unitList);
					List<String> unitList = Arrays.asList(unitArray);
					offerList = OfferTOConverter.convertStringUnitTOList(
							unitList, propertyName);
					detailList.addAll(OfferTOConverter
							.convertUnitEntity(offerList));
					offerManagementTO.setUnitList(offerList);
					offerManagementTO.setOfferFor("UNITS");
				}
			}
			Date date = new Date();
			offerManagementTO.setComponentFlag(componentFlag);
			offerManagementTO.setPropertyId(propertyId);
			offerManagementTO.setPropertyName(propertyName);
			offerManagementTO.setPropertyRent(propertyRent);
			offerManagementTO.setPropertyTypes(propertyType);
			offerManagementTO.setPropertyType(propertyType.toLowerCase());
			if (propertyType.equalsIgnoreCase("villa") || componentFlag == 1) {
				detailList = new ArrayList<OfferDetail>();
				offerManagementTO.setOfferFor("PROPERTY");
				offerManagementTO.setOfferAmount(propertyRent);
				detailList.add(OfferTOConverter
						.convertToPropertyEntity(offerManagementTO));
			}
			offerManagementTO.setOfferDate(DateFormat
					.convertSystemDateToString(date));
			ServletActionContext.getRequest().getSession()
					.removeAttribute("OFFER_DETAILS_INFO");
			ServletActionContext.getRequest().setAttribute(
					"OFFER_CANVAS_DETAILS", offerManagementTO);
			// Store in session
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);

			// Offer Number generation
			getImplementId();
			List<Offer> offerServiceList = new ArrayList<Offer>();
			offerServiceList = offerService.getOfferNumber(implementation);
			List<Integer> offerNum = new ArrayList<Integer>();
			for (Offer list : offerServiceList) {
				offerNum.add(list.getOfferNo());
			}
			if (offerServiceList.size() > 0) {
				offerNumber = Collections.max(offerNum);
				offerNumber += 1;
			} else {
				offerNumber = 4050;
			}

			ServletActionContext.getRequest().setAttribute("OFFER_NUMBER",
					offerNumber);
			tenantGroupId = 1;
			ServletActionContext.getRequest().setAttribute(
					"TENANT_GROUPS_LIST",
					tenantGroupService.getAllGroups(getImplementation()));
			LOGGER.info("Module: Realestate : Method: showAddOfferEntry: Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: showAddOfferEntry: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	// to show offer edit screen
	public String editOffer() {
		LOGGER.info("Module: Realestate : Method: editOffer");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			session.removeAttribute("OFFER_CANVAS_DETAILS_PRINT");
			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow

			if (recordId != 0) {
				offerId = (int) recordId;
			}

			AIOSCommons.removeUploaderSession(session, "offerDocs",
					(long) offerId, "Offer");

			Offer offer = new Offer();
			offer = offerService.getOffersById(Long.parseLong(offerId + ""));
			TenantOffer tenant = offerService.getTenantById(Long
					.parseLong(offerId + ""));
			tenantGroupId = tenant.getTenantGroup().getTenantGrpId();
			Person person = null;
			Company company = null;
			OfferManagementTO offerManagementTO = null;
			if (tenant.getPerson() != null) {
				person = new Person();
				person = offerService.getPersonDetails(tenant.getPerson()
						.getPersonId());
				offerManagementTO = new OfferManagementTO();
				offerManagementTO = OfferTOConverter.convertOfferEntityTo(
						offer, person, tenant);
				ServletActionContext.getRequest().getSession()
						.setAttribute("OFFER_DETAILS_INFO", offerManagementTO);
			} else {
				company = new Company();
				company = offerService.getCompanyDetails(tenant.getCompany()
						.getCompanyId());
				offerManagementTO = new OfferManagementTO();
				offerManagementTO = OfferTOConverter.convertOfferEntityTo(
						offer, company, tenant);
				ServletActionContext.getRequest().getSession()
						.setAttribute("OFFER_DETAILS_INFO", offerManagementTO);
			}
			offerList = new ArrayList<OfferManagementTO>();
			List<OfferDetail> detailList = offerService.getOfferDetailById(Long
					.parseLong(offerId + ""));
			if (detailList.get(0).getProperty() != null
					&& detailList.get(0).getProperty().getPropertyId() > 0) {
				OfferDetail detail = offerService.getOfferPropertyById(Long
						.parseLong(offerId + ""));
				offerManagementTO.setPropertyId(detail.getProperty()
						.getPropertyId().intValue());
				offerManagementTO.setPropertyName(detail.getProperty()
						.getPropertyName());
				offerManagementTO.setPropertyRent(detail.getProperty()
						.getRentYr());
				offerManagementTO.setOfferAmount(detail.getOfferAmount());
				offerManagementTO.setOfferLineId(detail.getOfferDetailId()
						.intValue());
				offerManagementTO.setPropertyTypes(detail.getProperty()
						.getPropertyType().getType());
				try {
					offerManagementTO.setPropertySize(detail.getProperty()
							.getBuildingSize()
							+ " "
							+ getSizeUnit(detail.getProperty()
									.getBuildingSizeUnit()));
				} catch (Exception e) {
					e.printStackTrace();
					offerManagementTO.setPropertySize(detail.getProperty()
							.getBuildingSize() + "");
				}
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
				offerManagementTO.setComponentFlag(1);
			} else if (detailList.get(0).getComponent() != null
					&& detailList.get(0).getComponent().getComponentId() > 0) {
				detailList = offerService.getOfferComponentsById(Long
						.parseLong(offerId + ""));
				Component component = componentService
						.getComponentProperty(detailList.get(0).getComponent()
								.getComponentId());
				offerList = OfferTOConverter.convertComponentTOList(detailList,
						component);
				offerManagementTO.setComponentList(offerList);
				offerManagementTO.setOfferFor("COMPONENT");
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
			} else if (detailList.get(0).getUnit().getUnitId() > 0) {
				detailList = offerService.getOfferUnitsById(Long
						.parseLong(offerId + ""));
				Unit unit = unitService.getUnitCompsWithProperty(detailList
						.get(0).getUnit());
				offerList = OfferTOConverter.convertUnitsTOList(detailList,
						unit);
				offerManagementTO.setUnitList(offerList);
				offerManagementTO.setOfferFor("UNITS");
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
			}
			ServletActionContext.getRequest().setAttribute(
					"OFFER_CANVAS_DETAILS", offerManagementTO);

			// Property Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (recordId != 0) {
				comment.setRecordId(Long.parseLong(offerId + ""));
				comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Offer");
				comment = commentBL.getCommentInfo(comment);

			}
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO", comment);
			// Comment process End

			// Store in session
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);
			ServletActionContext.getRequest().setAttribute(
					"TENANT_GROUPS_LIST",
					tenantGroupService.getAllGroups(getImplementation()));
			LOGGER.info("Module: Accounts : Method: editOffer Action Success");

			session.setAttribute("OFFER_CANVAS_DETAILS_PRINT",
					offerManagementTO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: editOffer: throws Exception "
					+ ex);
			return ERROR;
		}
	}
	
	//Offer Print Call
	public String getOfferInfoToPrint() {
		LOGGER.info("Module: Realestate : Method: getOfferInfoToPrint");
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			session.removeAttribute("OFFER_CANVAS_DETAILS_PRINT");
			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow

			if (recordId != 0) {
				offerId = (int) recordId;
			}

			AIOSCommons.removeUploaderSession(session, "offerDocs",
					(long) offerId, "Offer");

			Offer offer = new Offer();
			offer = offerService.getOffersById(Long.parseLong(offerId + ""));
			TenantOffer tenant = offerService.getTenantById(Long
					.parseLong(offerId + ""));
			tenantGroupId = tenant.getTenantGroup().getTenantGrpId();
			Person person = null;
			Company company = null;
			OfferManagementTO offerManagementTO = null;
			if (tenant.getPerson() != null) {
				person = new Person();
				person = offerService.getPersonDetails(tenant.getPerson()
						.getPersonId());
				offerManagementTO = new OfferManagementTO();
				offerManagementTO = OfferTOConverter.convertOfferEntityTo(
						offer, person, tenant);
				ServletActionContext.getRequest().getSession()
						.setAttribute("OFFER_DETAILS_INFO", offerManagementTO);
			} else {
				company = new Company();
				company = offerService.getCompanyDetails(tenant.getCompany()
						.getCompanyId());
				offerManagementTO = new OfferManagementTO();
				offerManagementTO = OfferTOConverter.convertOfferEntityTo(
						offer, company, tenant);
				ServletActionContext.getRequest().getSession()
						.setAttribute("OFFER_DETAILS_INFO", offerManagementTO);
			}
			offerList = new ArrayList<OfferManagementTO>();
			List<OfferDetail> detailList = offerService.getOfferDetailById(Long
					.parseLong(offerId + ""));
			if (detailList.get(0).getProperty() != null
					&& detailList.get(0).getProperty().getPropertyId() > 0) {
				OfferDetail detail = offerService.getOfferPropertyById(Long
						.parseLong(offerId + ""));
				offerManagementTO.setPropertyId(detail.getProperty()
						.getPropertyId().intValue());
				offerManagementTO.setPropertyName(detail.getProperty()
						.getPropertyName());
				offerManagementTO.setPropertyRent(detail.getProperty()
						.getRentYr());
				offerManagementTO.setOfferAmount(detail.getOfferAmount());
				offerManagementTO.setOfferLineId(detail.getOfferDetailId()
						.intValue());
				offerManagementTO.setPropertyTypes(detail.getProperty()
						.getPropertyType().getType());
				try {
					offerManagementTO.setPropertySize(detail.getProperty()
							.getBuildingSize()
							+ " "
							+ getSizeUnit(detail.getProperty()
									.getBuildingSizeUnit()));
				} catch (Exception e) {
					e.printStackTrace();
					offerManagementTO.setPropertySize(detail.getProperty()
							.getBuildingSize() + "");
				}
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
				offerManagementTO.setComponentFlag(1);
			} else if (detailList.get(0).getComponent() != null
					&& detailList.get(0).getComponent().getComponentId() > 0) {
				detailList = offerService.getOfferComponentsById(Long
						.parseLong(offerId + ""));
				Component component = componentService
						.getComponentProperty(detailList.get(0).getComponent()
								.getComponentId());
				offerList = OfferTOConverter.convertComponentTOList(detailList,
						component);
				offerManagementTO.setComponentList(offerList);
				offerManagementTO.setOfferFor("COMPONENT");
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
			} else if (detailList.get(0).getUnit().getUnitId() > 0) {
				detailList = offerService.getOfferUnitsById(Long
						.parseLong(offerId + ""));
				Unit unit = unitService.getUnitCompsWithProperty(detailList
						.get(0).getUnit());
				offerList = OfferTOConverter.convertUnitsTOList(detailList,
						unit);
				offerManagementTO.setUnitList(offerList);
				offerManagementTO.setOfferFor("UNITS");
				propertyType = "Property";
				offerManagementTO.setPropertyType(propertyType.toLowerCase());
			}
			ServletActionContext.getRequest().setAttribute(
					"OFFER_CANVAS_DETAILS", offerManagementTO);

			//Bank Informatin 
			if(personBankId!=null){
				PersonBank personBank=offerBL.getPersonBankBL().getPersonBankService().getPersonBankInfo(personBankId);
				offerManagementTO.setBankAccountTitle(personBank.getAccountTitle());
				offerManagementTO.setBankAccountNumber(personBank.getAccountNumber());
				offerManagementTO.setBankIBAN(personBank.getIban());
				offerManagementTO.setBankBranch(personBank.getBranchName());
				offerManagementTO.setBankCity(personBank.getBranchCity());
				offerManagementTO.setBankName(personBank.getBankName());
			}
			
			//From Print options 
			offerManagementTO.setOfferPrintOption1(offerPrintOption1);

			// Store in session
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			session.setAttribute(
					"OFFER_LINES_INFO_" + sessionObj.get("jSessionId"),
					detailList);
			ServletActionContext.getRequest().setAttribute(
					"TENANT_GROUPS_LIST",
					tenantGroupService.getAllGroups(getImplementation()));
			LOGGER.info("Module: Accounts : Method: getOfferInfoToPrint Action Success");

			session.setAttribute("OFFER_CANVAS_DETAILS_PRINT",
					offerManagementTO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: getOfferInfoToPrint: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	private String getSizeUnit(int code) {

		switch (code) {
		case 1:
			return "Square Feet";
		case 2:
			return "Square Meter";
		case 3:
			return "Square Yard";
		default:
			return "";
		}
	}

	public void saveOfferUploads(Offer offer, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(offer, "offerDocs",
				editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, offer.getOfferId(), "Offer", "offerDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	// to save offer details
	@SuppressWarnings("unchecked")
	public String saveOfferDetails() {
		LOGGER.info("Module: Realestate : Method: saveOfferDetails");
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			Offer offer = new Offer();
			User user = (User) sessionObj.get("USER");
			TenantOffer tenantOffer = new TenantOffer();
			Person person = null;
			Company company = null;
			if (offerTenantFor.equalsIgnoreCase("Tenant")) {
				person = new Person();
				person.setPersonId(Long.parseLong(tenantId + ""));
			} else {
				company = new Company();
				company.setCompanyId(Long.parseLong(tenantId + ""));
			}
			tenantOffer.setPerson(person);
			tenantOffer.setCompany(company);
			getImplementId();
			List<Offer> offerServiceList = offerService
					.getOfferNumber(implementation);
			List<Integer> offerNumber = new ArrayList<Integer>();
			for (Offer list : offerServiceList) {
				offerNumber.add(list.getOfferNo());
			}
			int tempNumber = 0;
			if (offerServiceList.size() > 0) {
				tempNumber = Collections.max(offerNumber);
				tempNumber += 1;
			} else {
				tempNumber = 4050;
			}
			offer.setOfferDate(DateFormat.convertStringToDate(offerDate));
			offer.setStartDate(DateFormat.convertStringToDate(contractFrom));
			offer.setEndDate(DateFormat.convertStringToDate(contractTo));
			offer.setExtension(extensionDays);
			offer.setValidityDays(validityDays);
			offer.setDeposit(securityDeposit);
			getImplementId();
			offer.setImplementation(implementation);
			offer.setOfferNo(tempNumber);
			
			Person personn = new Person();
			personn.setPersonId(user.getPersonId());
			
			offer.setPerson(personn);
			offer.setCreatedDate(new Date());
			String offerAmountArray[] = splitArrayValues(offerStringAmount);
			List<String> offerAmountList = Arrays.asList(offerAmountArray);
			offerList = OfferTOConverter
					.convertAmountStringTOList(offerAmountList);
			
				offer.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Published
								.getCode() + ""));
			
			
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			if (offer != null && offer.getOfferId() != null
					&& offer.getOfferId() > 0) {

				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
								.getCode());
			} else {
				workflowDetailVo
						.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
								.getCode());
			}
			// tenantGroupId=1;
			TenantGroup tenantGroup = offerService
					.getTenantGroup(tenantGroupId);
			tenantOffer.setTenantGroup(tenantGroup);
			List<OfferDetail> detailList = (ArrayList<OfferDetail>) session
					.getAttribute("OFFER_LINES_INFO_"
							+ sessionObj.get("jSessionId"));
			for (int i = 0; i < detailList.size(); i++) {
				detailList.get(i).setOfferAmount(
						offerList.get(i).getOfferAmount());
			}
			offerService.saveOfferForm(offer, detailList, tenantOffer);
			
			
			if (offer.getRelativeId() == null) {
				offer.setRelativeId(offer.getOfferId());
				offerService.saveOfferForm(offer, detailList, tenantOffer); 
			}
			
			
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Offer", offer.getOfferId(), user, workflowDetailVo);

			
			
			
			
			
			try {
				saveOfferUploads(offer, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			offerBL.updatePropertyStatus(offerFor, detailList, offerStatus);
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: saveOfferDetails Action Success");

			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: saveOfferDetails: throws Exception "
					+ ex);
		} finally {
			AIOSCommons.removeUploaderSession(session, "offerDocs", (long) -1,
					"Offer");
		}
		return ERROR;
	}

	// to update offer details
	@SuppressWarnings("unchecked")
	public String updateOfferDetails() {
		LOGGER.info("Module: Realestate : Method: updateOfferDetails");
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			Offer offer = new Offer();
			Offer offer1 = offerService.getOffersById(Long.parseLong(offerId
					+ ""));
			TenantOffer tenantOffer = new TenantOffer();
			Person person = null;
			Company company = null;
			if (offerTenantFor.equalsIgnoreCase("Tenant")) {
				person = new Person();
				person.setPersonId(Long.parseLong(tenantId + ""));
			} else {
				company = new Company();
				company.setCompanyId(Long.parseLong(tenantId + ""));
			}
			tenantOffer.setPerson(person);
			tenantOffer.setCompany(company);
			tenantOffer.setTenantOfferId(Long.parseLong(tenantOfferId + ""));
			offer.setPerson(offer1.getPerson());
			offer.setCreatedDate(offer1.getCreatedDate());
			offer.setOfferDate(DateFormat.convertStringToDate(offerDate));
			offer.setStartDate(DateFormat.convertStringToDate(contractFrom));
			offer.setEndDate(DateFormat.convertStringToDate(contractTo));
			offer.setExtension(extensionDays);
			offer.setValidityDays(validityDays);
			getImplementId();
			offer.setImplementation(implementation);
			offer.setOfferNo(offerNumber);
			offer.setDeposit(securityDeposit);
			offer.setOfferId(Long.parseLong(offerId + ""));
			if (offer1.getIsApprove() == Constants.RealEstate.Status.Deny
					.getCode()) {
				offer.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.NoDecession
								.getCode() + ""));
			} else {
				offer.setIsApprove(offer1.getIsApprove());
			}
			String offerAmountArray[] = splitArrayValues(offerStringAmount);
			List<String> offerAmountList = Arrays.asList(offerAmountArray);
			offerList = OfferTOConverter
					.convertAmountStringTOList(offerAmountList);

			// tenantGroupId=1;
			TenantGroup tenantGroup = offerService
					.getTenantGroup(tenantGroupId);
			tenantOffer.setTenantGroup(tenantGroup);
			List<OfferDetail> detailList = (ArrayList<OfferDetail>) session
					.getAttribute("OFFER_LINES_INFO_"
							+ sessionObj.get("jSessionId"));
			for (int i = 0; i < detailList.size(); i++) {
				detailList.get(i).setOfferAmount(
						offerList.get(i).getOfferAmount());
			}
			offerService.saveOfferForm(offer, detailList, tenantOffer);
			try {
				saveOfferUploads(offer, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			session.removeAttribute("OFFER_LINES_INFO_"
					+ sessionObj.get("jSessionId"));
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: updateOfferDetails Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Realestate : Method: updateOfferDetails: throws Exception "
					+ ex);
		} finally {
			if (offerId != 0)
				AIOSCommons.removeUploaderSession(session, "offerDocs",
						(long) offerId, "Offer");
		}
		return ERROR;
	}

	// to delete the offer
	public String deleteOffer() {
		LOGGER.info("Module: Realestate : Method: deleteOffer");
		try {
			Offer offer = new Offer();
			offer = offerService.getOffersById(Long.parseLong(offerId + ""));
			List<OfferDetail> detailList = new ArrayList<OfferDetail>();
			detailList = offerService.getOfferDetailById(Long.parseLong(offerId
					+ ""));
			if (detailList.get(0).getProperty() != null) {
				offerFor = "PROPERTY";
			} else if (detailList.get(0).getComponent() != null) {
				offerFor = "COMPONENT";
			} else if (detailList.get(0).getUnit() != null) {
				offerFor = "UNITS";
			}
			TenantOffer tenant = offerService.getTenantById(Long
					.parseLong(offerId + ""));
			offerService.deleteOfferDetails(offer, detailList, tenant);
			offerStatus = 1;
			offerBL.updatePropertyStatus(offerFor, detailList, offerStatus);
			sqlReturnMessage = "Success";
			ServletActionContext.getRequest().setAttribute(sqlReturnMessage,
					sqlReturnMessage);
			LOGGER.info("Module: Accounts : Method: deleteOffer Action Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Module: Realestate : Method: deleteOffer: throws Exception "
					+ ex);
			return ERROR;
		}
	}

	public String getFilteredOfferReport() {

		try {
			String csvRecords = "";
			getImplementId();
			offerList = new ArrayList<OfferManagementTO>();
			String ctxRealPath = ""
					+ ServletActionContext.getServletContext().getRealPath("/");
			offerManagementTO = new OfferManagementTO();
			List<Offer> offerEntityList = offerBL.getFilteredOfferList(
					implementation, startDate, endDate, reportFilter, tenantId,
					unitId);

			offerList = OfferTOConverter.convertToTOList(offerEntityList);

			iTotalRecords = offerList.size();
			iTotalDisplayRecords = offerList.size();
			if (offerList != null && offerList.size() > 0) {
				LOGGER.info("Module: Realestate : Method: showOfferFormList, "
						+ " Component List size() " + offerList.size());
			}

			setFilterTags();

			// PDF
			offerManagementTO.setStartDate(startDate);
			offerManagementTO.setEndDate(endDate);
			offerManagementTO.setTenantName(tenantName);
			offerManagementTO.setUnitName(unitName);

			// XLS
			csvRecords = "Offer Details\n\n Filter Option\n Range From,Range Upto,Tenant,Unit\n";
			csvRecords = csvRecords.concat(startDate + "," + endDate + ","
					+ tenantName + "," + unitName + "\n\n");
			csvRecords = csvRecords
					.concat("Offer No., Tenant, Offer Date, Offer Period, Created By,Rent,Details\n");

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();

			for (OfferManagementTO offer : offerList) {

				JSONArray array = new JSONArray();
				array.add(offer.getOfferId());
				array.add(offer.getOfferNumber() + "");

				List<TenantOffer> tenantOffers = contractService
						.getTenantOfferDetails(offer.getOfferId());

				if (tenantOffers != null && tenantOffers.size() > 0
						&& tenantOffers.get(0).getPerson() != null) {

					List<Person> persons = this.contractService
							.getPersonDetails(tenantOffers.get(0).getPerson());
					offer.setTenantName(persons.get(0).getFirstName().trim()
							+ " " + persons.get(0).getMiddleName().trim() + " "
							+ persons.get(0).getLastName().trim());
				} else {

					Company company = new Company();
					company = offerService.getCompanyDetails(tenantOffers
							.get(0).getCompany().getCompanyId());

					if (company != null)
						offer.setTenantName(company.getCompanyName());
				}

				try {
					offer.setCreatedBy(getOfferCreatedBy(offer.getCreatedId())
							+ "");
					offer.setOfferDetails(new HashSet<OfferDetail>(offerService
							.getOfferDetailById((long) offer.getOfferId())));
				} catch (Exception e) {
					e.printStackTrace();
				}

				Double totalRent = 0.0;
				String details = "";
				for (OfferDetail offerDetail : offer.getOfferDetails()) {
					String tempstr = "";
					if (offerDetail.getUnit().getUnitName() != null)
						tempstr = offerDetail.getUnit().getUnitName();
					totalRent += offerDetail.getOfferAmount();
					details = details.concat(" Offered: "
							+ tempstr
							+ " Of "
							+ offerDetail.getUnit().getComponent()
									.getProperty().getPropertyName()
							+ " Offer Amount : " + offerDetail.getOfferAmount()
							+ " | ");

				}
				offer.setTotalOfferAmount(totalRent);
				offer.setDetails(details);

				array.add(offer.getTenantName() + "");
				array.add(offer.getOfferDate() + "");
				array.add(offer.getStartDate() + " - " + offer.getEndDate());
				array.add(offer.getCreatedBy());

				csvRecords = csvRecords.concat(getOfferString(offer));
				data.add(array);
			}

			fileInputStream = new ByteArrayInputStream(csvRecords.getBytes());
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			ServletActionContext.getRequest().setAttribute("PRINT_OFFER_LIST",
					offerList);
			// PDF request data binding
			componentDS = new ArrayList<OfferManagementTO>();
			offerManagementTO.setOfferList(offerList);
			componentDS.add(offerManagementTO);
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String offerRentalRangeReport() {

		try {
			String csvRecords = "";
			getImplementId();
			offerList = new ArrayList<OfferManagementTO>();

			String ctxRealPath = ""
					+ ServletActionContext.getServletContext().getRealPath("/");
			Double fromAmount = null;
			Double toAmount = null;
			if (startDate != null && !startDate.equalsIgnoreCase(""))
				fromAmount = Double.parseDouble(startDate);

			if (endDate != null && !endDate.equalsIgnoreCase(""))
				toAmount = Double.parseDouble(endDate);

			offerManagementTO = new OfferManagementTO();
			List<Offer> offerEntityList = offerBL.getOfferRantalRangeList(
					implementation, fromAmount, toAmount);

			offerList = OfferTOConverter.convertToTOList(offerEntityList);

			iTotalRecords = offerList.size();
			iTotalDisplayRecords = offerList.size();

			// PDF
			if (startDate != null && !startDate.equals(""))
				offerManagementTO.setStartDate(startDate);
			else
				offerManagementTO.setStartDate("-NA-");
			if (endDate != null && !endDate.equals(""))
				offerManagementTO.setEndDate(endDate);
			else
				offerManagementTO.setEndDate("-NA-");
			offerManagementTO.setTenantName("-NA-");
			offerManagementTO.setUnitName("-NA-");

			// XLS
			csvRecords = "Offer Details\n\n Filter Option\n Range From,Range Upto,Tenant,Unit\n";
			csvRecords = csvRecords.concat(offerManagementTO.getStartDate()
					+ "," + offerManagementTO.getEndDate() + ","
					+ offerManagementTO.getTenantName() + ","
					+ offerManagementTO.getUnitName() + "\n\n");
			csvRecords = csvRecords
					.concat("Offer No., Tenant, Offer Date, Offer Period, Created By,Rent,Details\n");

			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (OfferManagementTO offer : offerList) {

				JSONArray array = new JSONArray();
				array.add(offer.getOfferId());
				array.add(offer.getOfferNumber() + "");

				List<TenantOffer> tenantOffers = contractService
						.getTenantOfferDetails(offer.getOfferId());

				if (tenantOffers != null && tenantOffers.size() > 0
						&& tenantOffers.get(0).getPerson() != null) {

					List<Person> persons = this.contractService
							.getPersonDetails(tenantOffers.get(0).getPerson());
					offer.setTenantName(persons.get(0).getFirstName().trim()
							+ " " + persons.get(0).getMiddleName().trim() + " "
							+ persons.get(0).getLastName().trim());
				} else {

					Company company = new Company();
					company = offerService.getCompanyDetails(tenantOffers
							.get(0).getCompany().getCompanyId());

					if (company != null)
						offer.setTenantName(company.getCompanyName());
				}

				try {
					offer.setCreatedBy(getOfferCreatedBy(offer.getCreatedId())
							+ "");
					offer.setOfferDetails(new HashSet<OfferDetail>(offerService
							.getOfferDetailById((long) offer.getOfferId())));
				} catch (Exception e) {
					e.printStackTrace();
				}

				Double totalRent = 0.0;
				String details = "";
				for (OfferDetail offerDetail : offer.getOfferDetails()) {
					totalRent += offerDetail.getOfferAmount();
					details = details.concat("Offered: "
							+ offerDetail.getUnit().getUnitName()
							+ " Of "
							+ offerDetail.getUnit().getComponent()
									.getProperty().getPropertyName()
							+ " Offer Amount : " + offerDetail.getOfferAmount()
							+ " | ");
				}
				offer.setTotalOfferAmount(totalRent);
				offer.setDetails(details);

				array.add(offer.getTenantName() + "");
				array.add(offer.getOfferDate() + "");
				array.add(offer.getStartDate() + " - " + offer.getEndDate());
				array.add(offer.getCreatedBy());
				// Rent process
				array.add(offer.getOfferAmount());

				csvRecords = csvRecords.concat(getOfferString(offer));

				data.add(array);
			}

			fileInputStream = new ByteArrayInputStream(csvRecords.getBytes());
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			ServletActionContext.getRequest().setAttribute("PRINT_OFFER_LIST",
					offerList);

			// PDF request data binding
			componentDS = new ArrayList<OfferManagementTO>();
			offerManagementTO.setOfferList(offerList);
			componentDS.add(offerManagementTO);
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	private void setFilterTags() {

		if (reportFilter.contains("tenantCompany")
				|| reportFilter.contains("tenantPerson"))
			tenantName = offerList.get(0).getTenantName();
		else
			tenantName = "Any";

		if (reportFilter.contains("unit"))
			unitName = offerList.get(0).getOfferDetails().iterator().next()
					.getUnit().getUnitName();
		else
			unitName = "Any";

		if (!reportFilter.toLowerCase().contains("period")) {
			endDate = "Any";
			startDate = "Any";
		}
	}

	private String getOfferString(OfferManagementTO to) {

		return to.getOfferNumber() + ", " + to.getTenantName() + ", "
				+ to.getOfferDate() + ", " + to.getStartDate() + " - "
				+ to.getEndDate() + ", " + to.getCreatedBy() + ", "
				+ to.getTotalOfferAmount() + ", " + to.getDetails() + "\n";
	}
	
	public String getOfferPrintOptions(){
		try {
			List<PersonBank> personBanks=offerBL.getPersonBankBL().getPersonBankService().getAllOwnerPersonBanks(getImplementation());
			ServletActionContext.getRequest().setAttribute("BANK_ACCOUNTS",personBanks);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getOfferTenantAndUnit() {

		getTenantOfferDetails();
		return getAllOfferedUnits();
	}

	public String getTenantOfferDetails() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("TENANT_INFO_OFFER",
					offerBL.getAllTenantOffers(implementation));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String getAllOfferedUnits() {
		try {
			getImplementId();
			ServletActionContext.getRequest().setAttribute("UNIT_INFO_OFFER",
					offerBL.getAllOfferedUnits(implementation));
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to split the values
	public static String[] splitValues(String spiltValue) {
		return spiltValue.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// to split the values
	public static String[] splitArrayValues(String spiltValue) {
		return spiltValue.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	// Getters & Setters
	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public int getOfferNumber() {
		return offerNumber;
	}

	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}

	public String getOfferDate() {
		return offerDate;
	}

	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}

	public int getValidityDays() {
		return validityDays;
	}

	public void setValidityDays(int validityDays) {
		this.validityDays = validityDays;
	}

	public int getExtensionDays() {
		return extensionDays;
	}

	public void setExtensionDays(int extensionDays) {
		this.extensionDays = extensionDays;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getShowPage() {
		return showPage;
	}

	public void setShowPage(String showPage) {
		this.showPage = showPage;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public OfferService getOfferService() {
		return offerService;
	}

	public void setOfferService(OfferService offerService) {
		this.offerService = offerService;
	}

	public OfferManagementTO getOfferManagementTO() {
		return offerManagementTO;
	}

	public void setOfferManagementTO(OfferManagementTO offerManagementTO) {
		this.offerManagementTO = offerManagementTO;
	}

	public List<OfferManagementTO> getOfferList() {
		return offerList;
	}

	public void setOfferList(List<OfferManagementTO> offerList) {
		this.offerList = offerList;
	}

	public int getComponentId() {
		return componentId;
	}

	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}

	public int getUnitId() {
		return unitId;
	}

	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}

	public OfferManagementBL getOfferBL() {
		return offerBL;
	}

	public void setOfferBL(OfferManagementBL offerBL) {
		this.offerBL = offerBL;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public int getComponentFlag() {
		return componentFlag;
	}

	public void setComponentFlag(int componentFlag) {
		this.componentFlag = componentFlag;
	}

	public String getComponentList() {
		return componentList;
	}

	public void setComponentList(String componentList) {
		this.componentList = componentList;
	}

	public String getUnitList() {
		return unitList;
	}

	public void setUnitList(String unitList) {
		this.unitList = unitList;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public double getPropertyRent() {
		return propertyRent;
	}

	public void setPropertyRent(double propertyRent) {
		this.propertyRent = propertyRent;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getOfferFor() {
		return offerFor;
	}

	public void setOfferFor(String offerFor) {
		this.offerFor = offerFor;
	}

	public double getOfferAmount() {
		return offerAmount;
	}

	public void setOfferAmount(double offerAmount) {
		this.offerAmount = offerAmount;
	}

	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}

	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getContractFrom() {
		return contractFrom;
	}

	public void setContractFrom(String contractFrom) {
		this.contractFrom = contractFrom;
	}

	public String getContractTo() {
		return contractTo;
	}

	public void setContractTo(String contractTo) {
		this.contractTo = contractTo;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getOfferStringAmount() {
		return offerStringAmount;
	}

	public void setOfferStringAmount(String offerStringAmount) {
		this.offerStringAmount = offerStringAmount;
	}

	public int getTenantGroupId() {
		return tenantGroupId;
	}

	public void setTenantGroupId(int tenantGroupId) {
		this.tenantGroupId = tenantGroupId;
	}

	public int getTenantOfferId() {
		return tenantOfferId;
	}

	public void setTenantOfferId(int tenantOfferId) {
		this.tenantOfferId = tenantOfferId;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public byte getOfferStatus() {
		return offerStatus;
	}

	public void setOfferStatus(byte offerStatus) {
		this.offerStatus = offerStatus;
	}

	public String getOfferDetail() {
		try {
			componentDS = new ArrayList<OfferManagementTO>();
			Offer offer = new Offer();
			if (recordId > 0)
				offerId = (int) recordId;
			offer.setOfferId((long) offerId);
			OfferManagementTO to = offerBL.getOfferWithContract(offer);

			to.setLinkLabel("HTML".equals(getFormat()) ? "Download" : "");
			to.setAnchorExpression("HTML".equals(getFormat()) ? "downloadPropOfferDetail.action?releaseId="
					+ offerId + "&format=PDF"
					: "#");

			// Approve Content
			to.setApproveLinkLabel("Approve");
			to.setApproveAnchorExpression("workflowProcess.action?processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Approved
							.getCode() + ""));

			// Disapprove Content
			to.setRejectLinkLabel("Reject");
			to.setRejectAnchorExpression("workflowProcess.action?processFlag="
					+ Byte.parseByte(Constants.RealEstate.Status.Deny
							.getCode() + ""));

			componentDS.add(to);
			String ctxRealPath = ServletActionContext.getServletContext()
					.getRealPath("/");
			jasperRptParams.put(
					"AIOS_LOGO",
					"file:///"
							+ ctxRealPath.concat(File.separator).concat(
									"images" + File.separator + "aiotech.jpg"));
			if (componentDS != null && componentDS.size() == 0) {
				componentDS.add(to);
			} else {
				componentDS.set(0, to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	private List<OfferManagementTO> componentDS;
	private String format;
	private ImageBL imageBL;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	public List<OfferManagementTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<OfferManagementTO> componentDS) {
		this.componentDS = componentDS;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getOfferTenantFor() {
		return offerTenantFor;
	}

	public void setOfferTenantFor(String offerTenantFor) {
		this.offerTenantFor = offerTenantFor;
	}

	public String getUnitTypeName() {
		return unitTypeName;
	}

	public void setUnitTypeName(String unitTypeName) {
		this.unitTypeName = unitTypeName;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}

	public TenantGroupService getTenantGroupService() {
		return tenantGroupService;
	}

	public void setTenantGroupService(TenantGroupService tenantGroupService) {
		this.tenantGroupService = tenantGroupService;
	}

	public List<Component> getCompList() {
		return compList;
	}

	public void setCompList(List<Component> compList) {
		this.compList = compList;
	}

	public List<Unit> getUnitLists() {
		return unitLists;
	}

	public void setUnitLists(List<Unit> unitLists) {
		this.unitLists = unitLists;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public Double getSecurityDeposit() {
		return securityDeposit;
	}

	public void setSecurityDeposit(Double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public String getReportFilter() {
		return reportFilter;
	}

	public void setReportFilter(String reportFilter) {
		this.reportFilter = reportFilter;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Long getPersonBankId() {
		return personBankId;
	}

	public void setPersonBankId(Long personBankId) {
		this.personBankId = personBankId;
	}

	public String getOfferPrintOption1() {
		return offerPrintOption1;
	}

	public void setOfferPrintOption1(String offerPrintOption1) {
		this.offerPrintOption1 = offerPrintOption1;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}
	
}
