package com.aiotech.aios.realestate.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.dom4j.IllegalAddException;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.to.Constants.RealEstate.ContractFeeType;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.TenantGroupService;
import com.aiotech.aios.realestate.service.bl.ContractAgreementBL;
import com.aiotech.aios.realestate.service.bl.OfferManagementBL;
import com.aiotech.aios.realestate.service.bl.PropertyInformationBL;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.common.WorkflowBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.ReferenceManagerBL;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ReContractAgreementAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager
			.getLogger(ReContractAgreementAction.class);
	// Master Details Variables
	private int offerId;
	private int offerNumber;
	private int buildingId;
	private int buildingNumber;
	private long alertId;
	private String buildingName;
	private int tenantId;
	private String tenantName;
	private String presentAddress;
	private String permanantAddress;

	private int contractId;
	private String contractNumber;
	private String contractDate;
	private int tenantTypeId;
	private String tenantTypeName;
	private String paymentMethod;
	private int noOfCheques;

	private String fromDate;
	private String toDate;

	private double cashAmount;
	private double chequeAmount;
	private double grandTotal;

	private int rentFees;
	private double depositAmount;

	// Rent Details Variables
	private int offerDetailId;
	private int contractRentId;
	private int lineNumber;
	private String flatNumber;
	private int propertyFlatId;
	private double rentAmount;
	private double contractAmount;
	private double balance;
	private int noOfDays;
	private long unitId;

	// Fee Details
	private int contractFeeId;
	private int feeId;
	private String feeDescription;
	private double feeAmount;

	// Payment Details
	private int contractPaymentId;
	private String bankName;
	private String chequeDate;
	private String chequeNumber;

	private String description;

	// Renewal Details
	private String extendedToDate;

	// workflow variables
	private int applicationId;
	private Long notificationId;
	private String notificationType;
	private int companyId;
	private int workflowId;
	private String workflowName;
	private String workflowStatus;
	private int categoryId;
	private int notifyFunctionId;
	private int functionId;
	private String wfFunctionType;
	private String workflowParam;
	private int mappingId;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String requestReason;
	private int sessionPersonId;

	private int headerLedgerId;
	private int wfCategoryId;
	private String functionType;

	private String approvedStatus;
	private String approvedStatusName;

	private String comments;

	private String trnValue;

	private int headerFlag;

	private String actionFlag;
	private String actionFlag1;
	private int actualLineId;
	private String tempLineId;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	private String renewalFlag;
	private long recordId;
	private long paymentReceived;
	private int legalProcess;
	ContractService contractService;
	SystemService systemService;
	TenantGroupService tenantGroupService;
	PersonService personService;
	private Byte paymentStatus;
	private String paymentStatusStr;
	private String percentage;
	private String paidDate;

	ContractAgreementBL contractAgreementBL;
	OfferManagementBL offerBL;
	CommentBL commentBL;
	PropertyInformationBL propertyInformationBL;
	ReferenceManagerBL referenceManagerBL;
	private BankReceiptsBL bankReceiptsBL;

	// Grid To variabls, getter & setters
	List<ReContractAgreementTO> agreementTOList = null;
	ReContractAgreementTO agreementTO = null;
	Contract contract = null;
	List<Contract> contracts = null;
	ContractPayment contractPayment = null;
	List<ContractPayment> contractPayments = null;

	Offer offer = null;
	TenantOffer tenentOffer = null;
	List<TenantOffer> tenentOffers = null;
	List<Person> persons = null;
	private String id;
	private String addEditFlag;
	private Implementation implementation;

	private String remarks;
	private String contractTransferType;
	private String contractTransferText;
	private String contractPrintStyle;
	private Boolean isMultipleUnit;
	private String leasedFor;
	private String postDetails;
	private String contractTransferAccountNumber;
	private InputStream fileInputStream;
	private String unitName;
	private String offerLineDetail;
	private String feeType;
	private int processFlag;

	private String messageId;
	private WorkflowBL workflowBL;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
			// implementationId=implementation.getImplementationId();
			// LOGGER.info("implementation id------>"+implementationId);
		}
	}

	// JSON Listing Redirect
	public String returnSuccess() {
		try {

			ContractFeeType[] feeTypes = Constants.RealEstate.ContractFeeType
					.values();
			ServletActionContext.getRequest()
					.setAttribute("FEETYPES", feeTypes);

			ServletActionContext.getRequest().setAttribute("rowid", id);
			ServletActionContext.getRequest().setAttribute("addEditFlag",
					addEditFlag);
			ServletActionContext.getRequest().setAttribute("LEGAL_PROCESS",
					legalProcess);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	// Listing JSON
	public String execute() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();

			contracts = new ArrayList<Contract>();
			// implementationId=(Long) session.get("THIS");
			getImplementId();
			agreementTOList = contractAgreementBL
					.getContractListInformation(implementation);
			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				if (list.isCancelled() == false) {
					JSONArray array = new JSONArray();
					String tempLegal = "";
					if (list.getLegallist() != null
							&& list.getLegallist().size() > 0)
						tempLegal = "<span title='Legal Information' class='legal_icon_view'></span>";

					array.add(list.getContractId());
					array.add(list.getContractNumber() + tempLegal);
					array.add(list.getTenantName());
					array.add(list.getContractDate());
					array.add(list.getFromDate());
					array.add(list.getToDate());
					array.add(getOfferCreatedBy(list.getCreatedBy()));
					array.add(list.getCreatedDate());
					array.add("<a href=\"#\" id="
							+ list.getContractId()
							+ " style=\"text-decoration: none; color: grey\" class=\"transfer-popup\">Print</a>");
					if (list.isCancellationInitiated() == true) {
						array.add("Y");
					} else {
						array.add("N");
					}
					array.add(list.getPropertyId() + "");
					if (list.getIsDeposited() != null && list.getIsDeposited()) {
						array.add("1");
					} else {
						array.add("");
					}
					data.add(array);
				}
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String getOfferCreatedBy(long personId) throws Exception {
		Person person = offerBL.getPersonService().getPersonDetails(personId);
		return person.getFirstName() + " " + person.getMiddleName();
	}

	// Contract filter tenant
	public String getOfferTenants() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			getImplementId();
			agreementTOList = contractAgreementBL
					.getContractTenantInformation(implementation);
			ServletActionContext.getRequest().setAttribute("TENANT_INFO",
					agreementTOList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get all units
	public String showAllUnits() {
		LOGGER.info("Inside RE-showAllUnits");
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			getImplementId();
			agreementTOList = contractAgreementBL
					.getContractUnitInformation(implementation);
			ServletActionContext.getRequest().setAttribute("UNIT_DETAIL_INFO",
					agreementTOList);
			LOGGER.info("Inside RE-showAllUnits success");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Inside RE-showAllUnits throws Exception");
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get all units contracts
	public String contractUnitFilteredReport() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			char activeFlag = 'Y';
			agreementTOList = contractAgreementBL.contractFilterByUnitsReport(
					unitId, activeFlag);
			if (agreementTOList != null && agreementTOList.size() > 0) {
				to = agreementTOList.get(0);
			}

			activeFlag = 'N';
			List<ReContractAgreementTO> agreementTOListInactive = contractAgreementBL
					.contractFilterByUnitsReport(unitId, activeFlag);
			if (agreementTOListInactive != null
					&& agreementTOListInactive.size() > 0) {
				to = agreementTOListInactive.get(0);
			}

			ServletActionContext.getRequest().setAttribute("CONTRACT_INFO", to);
			ServletActionContext.getRequest().setAttribute(
					"CONTRACT_LIST_ACTIVE", agreementTOList);
			ServletActionContext.getRequest().setAttribute(
					"CONTRACT_LIST_INACTIVE", agreementTOListInactive);
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get all units contracts
	public String contractUnitFilteredReportXLS() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			char activeFlag = 'Y';
			String str = "";
			String header = "";
			agreementTOList = contractAgreementBL.contractFilterByUnitsReport(
					unitId, activeFlag);
			if (agreementTOList != null && agreementTOList.size() > 0) {
				to = agreementTOList.get(0);
				header = "Property Name,Property Details\n";
				header += to.getBuildingName()
						+ ","
						+ (to.getPropertyFullInfo() != null ? to
								.getPropertyFullInfo().replaceAll(",", "-")
								: "") + "\n\n";
				str += header;
				if (agreementTOList != null && agreementTOList.size() > 0) {
					str += "Active Contracts\n";
					str += "Contract No.,Contract Date,Deposit Amount,Contract Fee,Tenant Name,Tenant Info,Contract Period,Rent Amount\n";

					for (ReContractAgreementTO contractTo : agreementTOList) {
						str += contractTo.getContractNumber()
								+ ","
								+ contractTo.getContractDate()
								+ ","
								+ contractTo.getDepositAmount()
								+ ","
								+ contractTo.getContractAmount()
								+ ","
								+ contractTo.getTenantName()
								+ ","
								+ contractTo.getTenantFullInfo().replaceAll(
										",", "-") + ","
								+ contractTo.getContractPeriod() + ","
								+ contractTo.getRentAmount() + "\n";

					}

				}
			}

			activeFlag = 'N';
			List<ReContractAgreementTO> agreementTOListInactive = contractAgreementBL
					.contractFilterByUnitsReport(unitId, activeFlag);
			if (agreementTOListInactive != null
					&& agreementTOListInactive.size() > 0) {
				to = agreementTOListInactive.get(0);
				if (header == null || header.equalsIgnoreCase("")) {
					header = "Property Name,Property Details\n";
					header += to.getBuildingName()
							+ ","
							+ (to.getPropertyFullInfo() != null ? to
									.getPropertyFullInfo().replaceAll(",", "-")
									: "") + "\n\n";
				}
				str += header;
				if (agreementTOListInactive != null
						&& agreementTOListInactive.size() > 0) {
					str += "Contract History\n";
					str += "Contract No.,Contract Date,Deposit Amount,Contract Fee,Tenant Name,Tenant Info,Contract Period,Rent Amount\n";

					for (ReContractAgreementTO contractTo : agreementTOListInactive) {
						str += contractTo.getContractNumber()
								+ ","
								+ contractTo.getContractDate()
								+ ","
								+ contractTo.getDepositAmount()
								+ ","
								+ contractTo.getContractAmount()
								+ ","
								+ contractTo.getTenantName()
								+ ","
								+ contractTo.getTenantFullInfo().replaceAll(
										",", "-") + ","
								+ contractTo.getContractPeriod() + ","
								+ contractTo.getRentAmount() + "\n";

					}

				}
			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;

		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Get all active contracts
	public String getActiveContract() {
		LOGGER.info("Inside RE-getActiveContract");
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			JSONObject jsonResponse = new JSONObject();
			contracts = new ArrayList<Contract>();
			char activeFlag = 'Y';
			agreementTOList = contractAgreementBL.contractFilterByUnits(unitId,
					activeFlag);
			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			} else {
				iTotalRecords = 0;
				iTotalDisplayRecords = 0;
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"contract_print\">Print</a>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception ex) {
			LOGGER.info("Inside RE-getActiveContract throws Exception");
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Inactive contract
	public String getInactiveContract() {
		LOGGER.info("Inside RE-getInactiveContract");
		try {
			List<ReContractAgreementTO> agreementList = new ArrayList<ReContractAgreementTO>();
			JSONObject jsonResponse = new JSONObject();
			contracts = new ArrayList<Contract>();
			char activeFlag = 'N';
			agreementList = contractAgreementBL.contractFilterByUnits(unitId,
					activeFlag);
			if (agreementList != null && agreementList.size() > 0) {
				iTotalRecords = agreementList.size();
				iTotalDisplayRecords = agreementList.size();
			} else {
				iTotalRecords = 0;
				iTotalDisplayRecords = 0;
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"contract_print\">Print</a>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception ex) {
			LOGGER.info("Inside RE-getInactiveContract throws Exception");
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Contract filter search
	public String contractFilterSearch() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();
			contracts = new ArrayList<Contract>();
			getImplementId();
			String tempId[] = new String[2];
			if (tenantName != null && !tenantName.equals("")) {
				tempId = tenantName.split("@");
			} else {
				tempId[0] = "0";
				tempId[1] = "";
			}

			agreementTOList = contractAgreementBL.getContractFilterInformation(
					implementation, fromDate, toDate,
					Long.parseLong(tempId[0]), tempId[1], unitId);

			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getFlatNumber());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"contract_print\">Print</a>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	//
	// Contract filter search for report
	public String contractFilterSearchReport() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			contracts = new ArrayList<Contract>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			getImplementId();
			String tempId[] = new String[2];
			if (tenantName != null && !tenantName.equals("")) {
				tempId = tenantName.split("@");
			} else {
				tempId[0] = "0";
				tempId[1] = "";
			}

			agreementTOList = contractAgreementBL.getContractFilterInformation(
					implementation, fromDate, toDate,
					Long.parseLong(tempId[0]), tempId[1], unitId);

			if (fromDate != null && !fromDate.equals(""))
				to.setFromDate(fromDate);
			else
				to.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				to.setToDate(toDate);
			else
				to.setToDate("-NA-");

			if (tenantTypeName != null && !tenantTypeName.equals(""))
				to.setTenantName(tenantTypeName);
			else
				to.setTenantName("-NA-");

			if (unitName != null && !unitName.equals(""))
				to.setUnitName(unitName);
			else
				to.setUnitName("-NA-");

			ServletActionContext.getRequest().setAttribute("TENANT_INFO", to);
			ServletActionContext.getRequest().setAttribute("CONTRACT_INFO", to);
			ServletActionContext.getRequest().setAttribute(
					"TENANT_CONTRACT_LIST", agreementTOList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Contract filter search for report as XLS
	public String contractFilterSearchReportXLS() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			contracts = new ArrayList<Contract>();
			ReContractAgreementTO tenant = new ReContractAgreementTO();
			getImplementId();
			String str = "";

			String tempId[] = new String[2];
			if (tenantName != null && !tenantName.equals("")) {
				tempId = tenantName.split("@");
			} else {
				tempId[0] = "0";
				tempId[1] = "";
			}

			agreementTOList = contractAgreementBL.getContractFilterInformation(
					implementation, fromDate, toDate,
					Long.parseLong(tempId[0]), tempId[1], unitId);

			if (fromDate != null && !fromDate.equals(""))
				tenant.setFromDate(fromDate);
			else
				tenant.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				tenant.setToDate(toDate);
			else
				tenant.setToDate("-NA-");

			if (tenantTypeName != null && !tenantTypeName.equals(""))
				tenant.setTenantName(tenantTypeName);
			else
				tenant.setTenantName("-NA-");

			if (unitName != null && !unitName.equals(""))
				tenant.setUnitName(unitName);
			else
				tenant.setUnitName("-NA-");

			str = "Contract Details\n\n";
			str += "Filter Condition\n";
			str += "From Date,To Date,Tenant,Unit\n";
			str += tenant.getFromDate() + "," + tenant.getToDate() + ","
					+ tenant.getTenantName() + "," + tenant.getUnitName()
					+ "\n\n";
			if (agreementTOList != null && agreementTOList.size() > 0) {
				str += "Contract No.,Contract Date,Deposit Amount,Contract Fee,Tenant Name,Tenant Info,Property Name,Contract Period,Property Details,Rent Amount";
				str += "\n";
				for (ReContractAgreementTO to : agreementTOList) {
					str += to.getContractNumber()
							+ ","
							+ to.getContractDate()
							+ ","
							+ to.getDepositAmount()
							+ ","
							+ to.getContractAmount()
							+ ","
							+ to.getTenantName()
							+ ","
							+ to.getTenantFullInfo().replaceAll(",", "-")
							+ ","
							+ to.getBuildingName()
							+ ","
							+ to.getContractPeriod()
							+ ","
							+ (to.getPropertyFullInfo() != null ? to
									.getPropertyFullInfo().replaceAll(",", "-")
									: "") + "," + to.getRentAmount();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON release renewal list
	public String getReleaseRenewList() {

		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();

			contracts = new ArrayList<Contract>();
			// implementationId=(Long) session.get("THIS");
			getImplementId();
			agreementTOList = contractAgreementBL
					.getContractReleaseRenewInformation(implementation);
			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add(getOfferCreatedBy(list.getCreatedBy()));
				array.add(list.getCreatedDate());
				array.add("<a href='#' onclick='return getRenew("
						+ list.getContractId()
						+ ")'><font size='5' color='blue'>Renew</font></a>"
						+ " / " + "<a href='#' onclick='return getRelease("
						+ list.getContractId()
						+ ")'><font size='5' color='blue'>Release</font></a>");
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String addContractDetails() {

		List<ReContractAgreementTO> offerTOList = new ArrayList<ReContractAgreementTO>();
		ReContractAgreementTO offerAgreementTO = null;
		HttpSession session = ServletActionContext.getRequest().getSession();

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			session.removeAttribute("CONTRACT_PAYMENTS_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AGREEMENT_DETAILS"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AGREEMENT_PAYMENT_DETAILS"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AGREEMENT_TENANT_DETAILS"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AGREEMENT_OFFER_DETAILS"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("TRANSFER_SUBJECT"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("TRANSFER_TEXT"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("TRANSFER_ACCOUNT"
					+ sessionObj.get("jSessionId"));

			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow

			if (recordId != 0) {
				contractId = (int) recordId;
			}

			if (messageId != null) {

				WorkflowDetailVO vo = workflowBL
						.getWorkflowDetailVObyMessageId(Long
								.parseLong(messageId));

				sessionObj.put("WORKFLOW_DETAILVO", vo);

			}

			AIOSCommons.removeUploaderSession(session, "contractDocs",
					(long) contractId, "Contract");

			List<Integer> intelist = new ArrayList<Integer>();
			agreementTO = new ReContractAgreementTO();
			getImplementId();

			if (contractId != 0) {
				offerAgreementTO = new ReContractAgreementTO();
				agreementTOList = new ArrayList<ReContractAgreementTO>();
				contract = new Contract();
				contractPayments = new ArrayList<ContractPayment>();
				offer = new Offer();

				contract = contractAgreementBL.getContractService()
						.getContractDetails(Long.parseLong(contractId + ""));
				agreementTO = ReContractAgreementTOConverter
						.convertToTO(contract);

				// Contract Number generate

				offer = contractAgreementBL.getContractService()
						.getOfferDetails(contract.getOffer().getOfferId());
				offerAgreementTO = ReContractAgreementTOConverter
						.convertToOfferTO(offer);

				// Find Tenant offer
				tenentOffers = this.contractService.getTenantOfferDetails(offer
						.getOfferId());
				Company company = null;
				if (tenentOffers != null && tenentOffers.size() > 0
						&& tenentOffers.get(0).getPerson() != null) {
					// Find Person
					persons = this.contractService
							.getPersonDetails(tenentOffers.get(0).getPerson());
					if (persons != null && persons.size() > 0) {
						offerAgreementTO.setTenantName(persons.get(0)
								.getFirstName()
								+ " "
								+ persons.get(0).getMiddleName()
								+ " "
								+ persons.get(0).getLastName());
						offerAgreementTO.setTenantNameArabic(AIOSCommons
								.bytesToUTFString(persons.get(0)
										.getFirstNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(persons.get(0)
										.getMiddleNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(persons.get(0)
										.getLastNameArabic()));
						if (persons.get(0).getPrefix() != null)
							offerAgreementTO.setPrefix(persons.get(0)
									.getPrefix());
						offerAgreementTO.setTenantId(Integer.parseInt(persons
								.get(0).getPersonId().toString()));
						offerAgreementTO.setPresentAddress(persons.get(0)
								.getTownBirth());
						offerAgreementTO.setPermanantAddress(persons.get(0)
								.getRegionBirth());
						offerAgreementTO.setTenantIssueDate(persons.get(0)
								.getValidFromDate().toString());
						try {
							List<Identity> identity = personService
									.getAllIdentity(persons.get(0));
							offerAgreementTO
									.setTenantIdentity(getIdentityType(identity
											.get(0).getIdentityType())
											+ " - "
											+ identity.get(0)
													.getIdentityNumber());
						} catch (Exception e) {
							e.printStackTrace();
							offerAgreementTO.setTenantIdentity("n/a");
						}
						offerAgreementTO.setTenantNationality(systemService
								.getCountryById(
										Long.parseLong(persons.get(0)
												.getNationality()))
								.getCountryName());
						if (persons.get(0).getMobile() != null) {
							offerAgreementTO.setTenantMobile(persons.get(0)
									.getMobile().toString());
						}
						if (persons.get(0).getAlternateTelephone() != null)
							offerAgreementTO.setTenantFax(persons.get(0)
									.getAlternateTelephone().toString());
						if (persons.get(0).getPostboxNumber() != null)
							offerAgreementTO.setTenantPOBox(persons.get(0)
									.getPostboxNumber());
					}
				} else {
					company = new Company();
					company = contractAgreementBL.getOfferService()
							.getCompanyDetails(
									tenentOffers.get(0).getCompany()
											.getCompanyId());
					if (company != null) {
						offerAgreementTO
								.setTenantName(company.getCompanyName());
						offerAgreementTO.setTenantNameArabic(AIOSCommons
								.bytesToUTFString(company
										.getCompanyNameArabic()));
						offerAgreementTO.setTenantId(Integer.parseInt(company
								.getCompanyId().toString()));
						offerAgreementTO.setPresentAddress(company
								.getCompanyAddress());
						if (company.getTradeNoIssueDate() != null)
							offerAgreementTO.setTenantIssueDate(company
									.getTradeNoIssueDate().toString());
						else
							offerAgreementTO.setTenantIssueDate("n/a");
						offerAgreementTO
								.setTenantIdentity(company.getTradeNo());
						offerAgreementTO.setTenantNationality(company
								.getCompanyOrigin());
						offerAgreementTO.setTenantFax(company.getCompanyFax());
						offerAgreementTO.setTenantMobile(company
								.getCompanyPhone());
						offerAgreementTO.setTenantPOBox(company
								.getCompanyPobox());
						offerAgreementTO.setPrefix("");
					}
				}

				contractPayments = contractAgreementBL.getContractService()
						.getContractPaymentList(contractId);
				session.setAttribute(
						"CONTRACT_PAYMENTS_" + sessionObj.get("jSessionId"),
						contractPayments);
				agreementTOList = ReContractAgreementTOConverter
						.convertToPaymentTOList(contractPayments);

				offerTOList = contractAgreementBL.getOfferDeatilsList(Integer
						.parseInt(contract.getOffer().getOfferId() + ""));

				// To show the Property name & component name against to the
				// Offered/Rented unit
				if (offerTOList != null && offerTOList.size() > 0) {
					offerAgreementTO.setPropertyName(offerTOList.get(0)
							.getBuildingName());
					offerAgreementTO.setComponentName(offerTOList.get(0)
							.getComponentName());
				}

				ServletActionContext.getRequest().setAttribute(
						"OFFER_DETAIL_LIST", offerTOList);

				if (agreementTOList != null && agreementTOList.size() > 0)
					ServletActionContext.getRequest().setAttribute(
							"countSize3", agreementTOList.size());

				ServletActionContext.getRequest().setAttribute(
						"CONTRACT_PAYMENT", agreementTOList);
				ServletActionContext.getRequest().setAttribute("OFFER_INFO",
						offerAgreementTO);

				// Property Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (recordId != 0) {
					comment.setRecordId(Long.parseLong(contractId + ""));
					comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Contract");
					comment = commentBL.getCommentInfo(comment);
				}
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
				// Comment process End

			} else {
				contractNumber = "0";
				contracts = contractAgreementBL.getContractService()
						.getAllContractsExceptIsApproveValidation(
								implementation);
				if (contractNumber == null || contractNumber.trim().equals("0")
						|| contractNumber.trim().equals("")) {
					if (contracts != null && contracts.size() > 0) {
						for (int i = 0; i < contracts.size(); i++) {
							intelist.add(Integer.parseInt(contracts.get(i)
									.getContractNo()));
						}
						LOGGER.info("Contract Number Max()--"
								+ Collections.max(intelist));

						int temp = Collections.max(intelist);
						temp += 1;
						contractNumber = temp + "";
					} else {
						contractNumber = "1000";
					}

				}
				agreementTO.setContractNumber(contractNumber);
				/*
				 * if (implementation.getContractFee() != null &&
				 * implementation.getContractDeposit() != null &&
				 * !implementation.getContractFee().equals("") &&
				 * !implementation.getContractDeposit().equals("")) {
				 * ServletActionContext
				 * .getRequest().setAttribute("ACCOUNT_INFO", "TRUE");
				 * ServletActionContext
				 * .getRequest().setAttribute("implemenation", implementation);
				 * } else {
				 * ServletActionContext.getRequest().setAttribute("ACCOUNT_INFO"
				 * , "FALSE"); }
				 */
			}
			List<Person> persons = contractAgreementBL.getPersonService()
					.getAllPerson(implementation);
			agreementTO.setRenewalFlag(renewalFlag);

			ServletActionContext.getRequest().setAttribute("bean", agreementTO);
			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					persons);

			// for printout
			try {
				if (contractTransferAccountNumber != null
						&& contractTransferAccountNumber.split(",").length > 1) {
					isMultipleUnit = true;
				} else {
					isMultipleUnit = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				isMultipleUnit = false;
			}
			session.setAttribute("AGREEMENT_DETAILS", agreementTO);
			session.setAttribute("AGREEMENT_PAYMENT_DETAILS", agreementTOList);
			session.setAttribute("AGREEMENT_TENANT_DETAILS", offerAgreementTO);
			session.setAttribute("AGREEMENT_OFFER_DETAILS", offerTOList);
			session.setAttribute("TRANSFER_SUBJECT", contractTransferType);
			session.setAttribute("TRANSFER_TEXT",
					AIOSCommons.translatetoArabic(contractTransferText));
			session.setAttribute("TRANSFER_ACCOUNT",
					contractTransferAccountNumber);

			String referenceStamp = SystemBL.getReferenceStamp(
					Contract.class.getName(), implementation);
			if (referenceStamp != null)
				session.setAttribute("CONTRACT_REF", referenceStamp);
			else
				session.setAttribute("CONTRACT_REF", "---");

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	private String getIdentityType(int code) throws Exception {

		switch (code) {
		case 1:
			return "Emirates ID";
		case 2:
			return "Labour Card";
		case 3:
			return "Passport";
		default:
			return "ID";
		}
	}

	@SuppressWarnings("unchecked")
	public String contractAddPaymentSave() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			contractPayment = new ContractPayment();
			if (contractPaymentId != 0)
				contractPayment.setContractPaymentId(Long
						.parseLong(contractPaymentId + ""));
			if (chequeNumber != null && !chequeNumber.trim().equals("-NA-")
					&& !chequeNumber.trim().equals("") && bankName != null
					&& !bankName.trim().equals("")
					&& !bankName.trim().equals("-NA-")) {
				contractPayment.setBankName(bankName);
				contractPayment.setChequeDate(DateFormat
						.convertStringToDate(chequeDate));
				contractPayment.setChequeNo(chequeNumber);
			} else if ((chequeNumber == null
					|| chequeNumber.trim().equals("-NA-") || chequeNumber
					.trim().equals(""))
					&& (bankName != null && !bankName.trim().equals("") && !bankName
							.trim().equals("-NA-"))) {
				contractPayment.setBankName(bankName);
				contractPayment.setChequeDate(DateFormat
						.convertStringToDate(chequeDate));
			}
			contractPayment.setAmount(chequeAmount);
			if (feeType != null && !feeType.trim().equals(""))
				contractPayment.setFeeType(Byte.parseByte(feeType));
			// Payment status
			if (paymentStatus != null && paymentStatus > 0)
				contractPayment.setPaymentStatus(paymentStatus);

			if (session.getAttribute("CONTRACT_PAYMENTS_"
					+ sessionObj.get("jSessionId")) == null) {
				contractPayments = new ArrayList<ContractPayment>();
			} else {
				contractPayments = (List<ContractPayment>) session
						.getAttribute("CONTRACT_PAYMENTS_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				contractPayments.add(contractPayment);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				contractPayments.set(Integer.parseInt(id) - 1, contractPayment);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				contractPayments.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"CONTRACT_PAYMENTS_" + sessionObj.get("jSessionId"),
					contractPayments);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	@SuppressWarnings("unchecked")
	public String updateContractPayment() {

		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			contract = contractAgreementBL.getContractService()
					.getContractDetails(Long.parseLong(contractId + ""));

			contractPayments = (ArrayList<ContractPayment>) session
					.getAttribute("CONTRACT_PAYMENTS_"
							+ sessionObj.get("jSessionId"));
			for (ContractPayment cps : contractPayments) {

				String[] overallSplit = paymentStatusStr.split(",");
				for (String string : overallSplit) {
					String[] finalSplit = string.split("#");
					if (Long.parseLong(finalSplit[0]) == cps
							.getContractPaymentId()) {
						if (null != finalSplit[1]
								&& !finalSplit[1].equals("null")
								&& !finalSplit[1].equals("")) {
							cps.setPaymentStatus(Byte.parseByte(finalSplit[1]));
							if (null != finalSplit[1]
									&& Byte.parseByte(finalSplit[1]) == 1) {
								cps.setPaidDate(DateFormat
										.convertStringToDate(finalSplit[2]));
							}
						}

					}
				}

				/*
				 * String[] overallPaidSplit = paidDate.split(","); for (String
				 * string : overallPaidSplit) { String[] finalSplit =
				 * string.split("#"); if (finalSplit.length > 1 && finalSplit[1]
				 * != null && !finalSplit[1].equals("") &&
				 * Long.parseLong(finalSplit[0]) == cps .getContractPaymentId())
				 * { cps.setPaidDate(DateFormat
				 * .convertStringToDate(finalSplit[1])); } }
				 */
			}
			this.getContractService().addContract(contract, contractPayments);
			if (null != postDetails && !postDetails.equals(""))
				session.setAttribute(
						"POST_DETAILS_" + sessionObj.get("jSessionId"),
						postDetails);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	// Retrieve Offer Details
	public String getOfferList() {
		try {
			// Call Implementation
			getImplementId();
			agreementTOList = contractAgreementBL.getOfferList(implementation);
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getOfferId());
				array.add(list.getOfferNumber());
				array.add(list.getTenantName());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add(list.getTenantId());
				array.add(list.getTenantType());

				array.add(list.getTenantGroup().getTenantGrpTitle());
				array.add(list.getTenantGroup().getDeposit());
				array.add(list.getTenantGroup().getContractFee());
				array.add(list.getTenantGroup().getRent());
			}
			ServletActionContext.getRequest().setAttribute(
					"CONTRACT_OFFER_LIST", agreementTOList);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public String offerDetailList() {

		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();

			agreementTOList = contractAgreementBL.getOfferDeatilsList(offerId);

			ServletActionContext.getRequest().setAttribute("OFFER_DETAIL_LIST",
					agreementTOList);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String retrieveChequeFeeDetails() {
		try {
			agreementTO = new ReContractAgreementTO();
			agreementTO.setNoOfCheques(noOfCheques);
			if (agreementTO != null) {
				ServletActionContext.getRequest().setAttribute("rentFeeList",
						agreementTO);
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String propertyContractDetailApprove() {
		try {
			Company company = null;
			Contract contract = contractAgreementBL.getContractService()
					.getContractDetails(recordId);
			offer = contractAgreementBL.getContractService().getOfferDetails(
					contract.getOffer().getOfferId());
			tenentOffers = this.contractService.getTenantOfferDetails(offer
					.getOfferId());
			Property property = new Property();
			List<Property> propertyDetail = new ArrayList<Property>();
			if (null != offer.getOfferId()) {
				List<OfferDetail> offerDetail = this.getOfferBL()
						.getOfferService()
						.getOfferDetailById(offer.getOfferId());
				if (null != offerDetail && offerDetail.size() > 0) {
					for (OfferDetail list : offerDetail) {
						if (null != list.getProperty()
								&& !list.getProperty().equals("")) {
							property = this
									.getContractAgreementBL()
									.getPropertyService()
									.getPropertyById(
											list.getProperty().getPropertyId());
						} else {
							Unit units = this.getContractAgreementBL()
									.getUnitService()
									.getUnitCompsWithProperty(list.getUnit());
							property.setPropertyId(units.getComponent()
									.getProperty().getPropertyId());
							property.setPropertyName(units
									.getComponent()
									.getProperty()
									.getPropertyName()
									.concat(".")
									.concat(units.getComponent()
											.getComponentName()).concat(".")
									.concat(units.getUnitName()));
							property.setAddressOne(units.getComponent()
									.getProperty().getAddressOne());
						}
						propertyDetail.add(property);
					}
				}
			}
			if (tenentOffers != null && tenentOffers.size() > 0
					&& tenentOffers.get(0).getPerson() != null) {
				persons = this.contractService.getPersonDetails(tenentOffers
						.get(0).getPerson());
			} else {
				company = contractAgreementBL
						.getOfferService()
						.getCompanyDetails(
								tenentOffers.get(0).getCompany().getCompanyId());
			}
			contractPayments = contractAgreementBL.getContractService()
					.getContractPaymentList(recordId);
			ServletActionContext.getRequest().setAttribute("CONTRACT_INFO",
					contract);
			ServletActionContext.getRequest().setAttribute("CONTRACT_PAYMENT",
					contractPayments);
			ServletActionContext.getRequest().setAttribute("CONTRACT_PERSON",
					persons);
			ServletActionContext.getRequest().setAttribute("CONTRACT_COMPANY",
					company);
			ServletActionContext.getRequest().setAttribute("CONTRACT_PROPERTY",
					propertyDetail);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Show Contract Rental Monthly Transaction
	public String showRentalPayment() {
		try {
			contractPaymentId = (int) recordId;
			ContractPayment contractPayment = this.getContractAgreementBL()
					.getContractService()
					.getContractPaymentByPaymentId(contractPaymentId);
			List<ContractPayment> contractRentPayments = this
					.getContractAgreementBL()
					.getContractService()
					.getContractRentPayment(
							contractPayment.getContract().getContractId());
			double rentAmount = 0;
			double monthlyRental = 0;
			for (ContractPayment list : contractRentPayments) {
				rentAmount += list.getAmount();
			}
			int offerMonths = DateFormat.monthDifference(contractPayment
					.getContract().getOffer().getStartDate(), contractPayment
					.getContract().getOffer().getEndDate());
			for (int i = 0; i < offerMonths; i++) {
				if (monthlyRental == 0
						&& DateFormat.dayVariance(
								DateFormat.addToMonths(contractPayment
										.getContract().getOffer()
										.getStartDate(), i), new Date()) == 0) {
					monthlyRental = rentAmount / offerMonths;
				}
			}
			List<Currency> currencyList = this
					.getContractAgreementBL()
					.getTransactionBL()
					.getCurrencyService()
					.getAllCurrency(
							contractPayment.getContract().getImplementation());
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency()) {
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
				}
			}
			List<Category> categoryList = this
					.getContractAgreementBL()
					.getTransactionBL()
					.getCategoryBL()
					.getCategoryService()
					.getAllActiveCategories(
							contractPayment.getContract().getImplementation());
			if (null != categoryList && categoryList.size() > 0) {
				ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
						categoryList);
			} else {
				ServletActionContext.getRequest().setAttribute("CATEGORY_INFO",
						null);
			}
			ServletActionContext.getRequest().setAttribute("showPage", "add");
			List<TransactionDetail> transactionDetails = this
					.createTransactionDetail(contractPayment.getContract(),
							monthlyRental);
			ServletActionContext.getRequest().setAttribute("JOURNAL_LINE_INFO",
					transactionDetails);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Create Transaction Detail
	private List<TransactionDetail> createTransactionDetail(Contract contract,
			double monthlyRental) throws Exception {
		List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();
		// Get Property Rent Income Account
		Property property = this.getContractAgreementBL().getPropertyDetails(
				contract.getContractId());
		Account rentAccount = this
				.getContractAgreementBL()
				.getTransactionBL()
				.getCombinationService()
				.getAccountDetail("Rent Income " + property.getPropertyName(),
						contract.getImplementation());
		TransactionDetail transactionDetail = new TransactionDetail();
		transactionDetail.setCombination(contractAgreementBL.getTransactionBL()
				.getCombinationService()
				.getCombinationByNaturalAccount(rentAccount.getAccountId()));
		transactionDetail.setAmount(monthlyRental);
		transactionDetail.setIsDebit(false);
		transactionDetails.add(transactionDetail);
		transactionDetail = new TransactionDetail();
		transactionDetail.setCombination(this
				.getContractAgreementBL()
				.getTransactionBL()
				.getCombinationService()
				.getCombinationAccountDetail(
						getImplementation().getUnearnedRevenue()));
		transactionDetail.setAmount(monthlyRental);
		transactionDetail.setIsDebit(true);
		transactionDetails.add(transactionDetail);
		return transactionDetails;
	}

	public String saveContractAgreement() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			contract = new Contract();
			offer = new Offer();
			List<Integer> intelist = new ArrayList<Integer>();
			// Call Implementation
			getImplementId();
			boolean editFlag = false;
			if (renewalFlag != null && renewalFlag.trim().equals("1")) {

				// Update the previous contract in order to avoid to future
				Contract contractinfo = contractAgreementBL
						.getContractService().getContractDetails(
								Long.parseLong(contractId + ""));
				Byte isapprove = 1;
				contract.setIsApprove(isapprove);
				Byte isrenew = 1;
				contractinfo.setIsRenewal(isrenew);

				if (contractId != 0)
					contractAgreementBL.saveContractAgreement(contractinfo,
							implementation, true);
				else
					contractAgreementBL.saveContractAgreement(contractinfo,
							implementation, false);

				contractId = 0;
				contracts = contractAgreementBL.getContractService()
						.getAllContractsExceptIsApproveValidation(
								implementation);

				if (contracts != null && contracts.size() > 0) {
					for (int i = 0; i < contracts.size(); i++) {
						intelist.add(Integer.parseInt(contracts.get(i)
								.getContractNo()));
					}
					LOGGER.info("Contract Number Max()--"
							+ Collections.max(intelist));

					int temp = Collections.max(intelist);
					temp += 1;
					contractNumber = temp + "";
				}

				contract.setContractNo(contractNumber);
			} else if ((renewalFlag == null || renewalFlag.trim().equals("0") || renewalFlag
					.trim().equals("")) && (contractId == 0)) {
				contracts = contractAgreementBL.getContractService()
						.getAllContractsExceptIsApproveValidation(
								implementation);

				if (contracts != null && contracts.size() > 0) {
					for (int i = 0; i < contracts.size(); i++) {
						intelist.add(Integer.parseInt(contracts.get(i)
								.getContractNo()));
					}
					LOGGER.info("Contract Number Max()--"
							+ Collections.max(intelist));

					int temp = Collections.max(intelist);
					temp += 1;
					contractNumber = temp + "";
				}

				contract.setContractNo(contractNumber);
			} else {
				contract.setContractNo(contractNumber);
			}

			if (contractId != 0) {
				Contract contract1 = contractAgreementBL.getContractService()
						.getContractDetails(Long.valueOf(contractId));
				List<ContractPayment> contractPaymentList = contractAgreementBL
						.getContractService()
						.getContractPaymentList(contractId);
				session.setAttribute(
						"CONTRACT_DBPAYMENTS_" + sessionObj.get("jSessionId"),
						contractPaymentList);
				for (ContractPayment list : contractPaymentList) {
					if (null != list.getIsDeposited() && list.getIsDeposited()) {
						editFlag = true;
					}
				}
				contract.setPerson(contract1.getPerson());
				contract.setCreatedDate(contract1.getCreatedDate());
				contract.setContractId(Long.valueOf(contractId));
				contract.setPrintTaken(contract1.getPrintTaken());
				if (contract1.getIsApprove() == Constants.RealEstate.Status.Deny
						.getCode()) {
					contract.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
				} else {
					// contract.setIsApprove(contract1.getIsApprove());
					contract.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
				}
			} else {
				Person person = new Person();
				person.setPersonId(user.getPersonId());
				contract.setPerson(person);
				contract.setCreatedDate(new Date());
				if (implementation.getIsWorkflow() != null
						&& implementation.getIsWorkflow()) {
					contract.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.NoDecession
									.getCode() + ""));
				} else {
					contract.setIsApprove(Byte
							.parseByte(Constants.RealEstate.Status.Confirm
									.getCode() + ""));
				}

			}

			contract.setContractDate(DateFormat
					.convertStringToDate(contractDate));
			offer.setOfferId(Long.valueOf(offerId));
			contract.setOffer(offer);
			contract.setContractFee(contractAmount);
			contract.setDeposit(depositAmount);
			contract.setInstallments(noOfCheques);
			contract.setPaymentReceived(paymentReceived);
			contract.setImplementation(implementation);
			contract.setRemarks(remarks);
			contract.setLeasedFor(leasedFor);

			if (!editFlag) {
				if (contractId != 0)
					contractAgreementBL.saveContractAgreement(contract,
							implementation, true);
				else
					contractAgreementBL.saveContractAgreement(contract,
							implementation, false);
			} else
				ServletActionContext
						.getRequest()
						.setAttribute("errMsg",
								"Contract Updated Failure, already receipts generated.");

			if (contractId != 0)
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Contract Updated Succesfully");
			else
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Contract Created Succesfully");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (contractId != 0) {
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Contract Update Failure");
				AIOSCommons.removeUploaderSession(session, "contractDocs",
						(long) contractId, "Contract");
			} else {
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Contract Create Failure");
				AIOSCommons.removeUploaderSession(session, "contractDocs",
						(long) -1, "Contract");
			}
		} finally {

		}
		return SUCCESS;
	}

	// Delete Offer Details
	public String deleteContract() {
		try {
			contract = new Contract();
			getImplementId();
			if (contractId != 0)
				contract.setContractId(Long.valueOf(contractId));
			contractAgreementBL.deleteContract(contract, implementation);
			if (null != contract.getPrintTaken() && contract.getPrintTaken()) {
				contractAgreementBL.deleteContractTempTransaction(contract);
			}
			ServletActionContext.getRequest().setAttribute("succMsg",
					"Successfully Deleted");
			return SUCCESS;
		} catch (Exception e) {
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Delete Failure");
			return ERROR;
		}
	}

	public String getReleaseRenewUpdate() {

		try {

			contract = new Contract();
			contracts = new ArrayList<Contract>();
			Release release = new Release();
			if (contractId != 0) {
				contract = contractAgreementBL.getContractService()
						.getContractDetails(Long.parseLong(contractId + ""));
				contract.setIsRenewal(Byte.parseByte(renewalFlag));
				contractAgreementBL.getContractService().updateContract(
						contract);
				release.setContract(contract);
				release.setIsInitialize(true);
				getImplementId();
				release.setImplementation(implementation);

				release.setIsApprove(Byte
						.parseByte(Constants.RealEstate.Status.Confirm
								.getCode() + ""));
				if (contract.getIsRenewal() == 1)
					contractAgreementBL.getContractService().createRelease(
							release);
				else {
					Release existingRelease = contractAgreementBL
							.getContractReleaseBL().getReleaseService()
							.getReleaseUseContract(contract);
					if (existingRelease == null)
						contractAgreementBL.getContractService().createRelease(
								release);
					else
						throw new IllegalAddException("Release Already Exists");
				}
			}
			LOGGER.info("Contract save success");

			if (contractId != 0)
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Contract Updated Succesfully");
			else
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Contract Added Succesfully");
			return SUCCESS;
		} catch (IllegalAddException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg",
					"Contract is already in Release Process");
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			if (contractId != 0)
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Contract Update Failure");
			else
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Contract Add Failure");
			return SUCCESS;
		}
	}

	// contract transaction
	public String contractTransaction() {
		try {
			Contract contract = contractAgreementBL.getContractService()
					.getContractDetails(Long.valueOf(contractId));
			if ((null == contract.getPrintTaken() || !contract.getPrintTaken())
					&& null != Constants.RealEstate.Status.get((Integer
							.valueOf(contract.getIsApprove())))
					&& (Constants.RealEstate.Status.Approved)
							.equals(Constants.RealEstate.Status.get((Integer
									.valueOf(contract.getIsApprove()))))) {
				getImplementId();
				contractAgreementBL.saveContractTransaction(contract,
						implementation);
				contractAgreementBL.saveContractAlert(contract, implementation);
				contract.setPrintTaken(true);
				contractAgreementBL.getContractService().updateContract(
						contract);
			}
			ServletActionContext.getRequest()
					.setAttribute("message", "Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest()
					.setAttribute("message", "Failure");
			return ERROR;
		}
	}

	public String contractPrintTakenStatus() {
		try {
			Contract contract = contractAgreementBL.getContractService()
					.getContractDetails(Long.valueOf(recordId));
			ServletActionContext.getRequest().setAttribute(
					"CONTRACT_INFORMATION", contract);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	public String contractRevertUpdateTransaction() {
		try {
			if (processFlag == 1) {
				getImplementId();
				contractAgreementBL.revertTransaction(contractId);
				Contract contract = contractAgreementBL.getContractService()
						.getContractDetails(Long.valueOf(contractId));
				contractAgreementBL.saveContractTransaction(contract,
						implementation);
			}
			ServletActionContext.getRequest()
					.setAttribute("message", "Success");
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// show contract transaction
	public String showContractTransaction() {
		try {
			agreementTO = new ReContractAgreementTO();
			agreementTO = contractAgreementBL.getContractDetails(recordId);
			agreementTO.setAlertId(alertId);
			ServletActionContext.getRequest().setAttribute(
					"CONTRACT_TRANSCATION", agreementTO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			ServletActionContext.getRequest()
					.setAttribute("message", "Failure");
			return ERROR;
		}
	}

	// print payment
	public String getContractPaymentPrint() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			agreementTO = new ReContractAgreementTO();

			ReContractAgreementTO tempAgreement = null;
			agreementTO = contractAgreementBL
					.getContractDetailPaymentPaid(contractId);
			List<ReContractAgreementTO> tempList = new ArrayList<ReContractAgreementTO>(
					agreementTO.getContractAgreementList());
			/*
			 * if (null != session.getAttribute("POST_DETAILS_" +
			 * sessionObj.get("jSessionId"))) { postDetails = (String)
			 * session.getAttribute("POST_DETAILS_" +
			 * sessionObj.get("jSessionId")); } if (postDetails != null &&
			 * !postDetails.equals("")) { String[] postValues =
			 * splitValues(postDetails, "#"); for (int i = 0; i <
			 * postValues.length; i++) { String[] postCheques =
			 * splitValues(postValues[i], "@"); tempAgreement = new
			 * ReContractAgreementTO();
			 * tempAgreement.setAttribute1(postCheques[0]);
			 * tempAgreement.setChequeNumber(postCheques[1]);
			 * tempList.add(tempAgreement); }
			 * agreementTO.setPostDetails(tempList); } else {
			 * agreementTO.setPostDetails(null); }
			 */

			List<ReContractAgreementTO> pdcPaymentListTO = new ArrayList<ReContractAgreementTO>();
			for (ReContractAgreementTO allContractPayment : tempList) {
				if (allContractPayment.getIsPdc() != null
						&& allContractPayment.getIsPdc()) {
					pdcPaymentListTO.add(allContractPayment);
					agreementTO.getContractAgreementList().remove(
							allContractPayment);
				}
			}
			agreementTO.setPostDetails(pdcPaymentListTO);
			session.setAttribute(
					"CONTRACT_PRINT_DETAILS_" + sessionObj.get("jSessionId"),
					agreementTO);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON
	public String getContractPaymentList() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();

			contracts = new ArrayList<Contract>();
			// implementationId=(Long) session.get("THIS");
			getImplementId();
			paymentStatus = Byte.parseByte(paymentStatusStr);

			agreementTOList = contractAgreementBL.getContractPaymentList(
					implementation, paymentStatus,
					DateFormat.convertStringToDate(fromDate),
					DateFormat.convertStringToDate(toDate));
			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractPaymentId());
				array.add(list.getContractNumber());
				array.add(list.getOfferNumber());
				array.add(list.getTenantName());
				array.add(list.getBuildingName());
				array.add(list.getFlatNumber());
				array.add(list.getPaymentType());
				array.add(list.getPaymentMethod());
				array.add(list.getChequeAmount());
				array.add(list.getBankName());
				array.add(list.getChequeDate());
				array.add(list.getChequeNumber());
				array.add(list.getPaymentVisibility());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// contract payment Listing print
	public String contractPaymentReportPrint() {

		try {

			agreementTOList = new ArrayList<ReContractAgreementTO>();
			getImplementId();
			paymentStatus = Byte.parseByte(paymentStatusStr);
			agreementTOList = contractAgreementBL.getContractPaymentList(
					implementation, paymentStatus,
					DateFormat.convertStringToDate(fromDate),
					DateFormat.convertStringToDate(toDate));

			ReContractAgreementTO paymentmaster = new ReContractAgreementTO();
			paymentmaster.setPaymentStatus(ReContractAgreementTOConverter
					.paymentStatusString(paymentStatus));
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals("")) {
				paymentmaster.setFromDate(fromDate);
				paymentmaster.setToDate(toDate);
			} else {
				paymentmaster.setFromDate("-NA-");
				paymentmaster.setToDate("-NA-");
			}
			ServletActionContext.getRequest().setAttribute("PAYMENT_MASTER",
					paymentmaster);
			ServletActionContext.getRequest().setAttribute("PAYMENT_LIST",
					agreementTOList);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// show bank receipts entry
	public String showContractBankReceipts() {
		try {

			List<LookupDetail> receiptTypes = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("Receipt_Types", false);

			List<LookupDetail> payeeTypes = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("PAYEE_TYPE", false);

			List<LookupDetail> modeOfPaymetns = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("MODE_OF_PAYMENT", false);

			Calendar calendar = this
					.getBankReceiptsBL()
					.getTransactionBL()
					.getCalendarBL()
					.getCalendarService()
					.getAtiveCalendar(
							this.getBankReceiptsBL().getImplementation());
			Period period = this.getBankReceiptsBL().getTransactionBL()
					.getCalendarBL().getCalendarService()
					.getActivePeriod(calendar.getCalendarId());

			List<Currency> currencyList = this
					.getBankReceiptsBL()
					.getTransactionBL()
					.getCurrencyService()
					.getAllCurrency(
							this.getBankReceiptsBL().getImplementation());
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			Contract contract = null;
			BankReceipts bankReceipts = null;
			if (recordId > 0) {
				contract = contractAgreementBL.getContractService()
						.getContractInfoForTransaction(Long.valueOf(recordId));
				bankReceipts = new BankReceipts();
				List<TenantOffer> tenantOffers = new ArrayList<TenantOffer>(
						contract.getOffer().getTenantOffers());
				Account account = new Account();
				StringBuilder accountCode = new StringBuilder();
				if (tenantOffers.get(0).getPerson() != null) {
					accountCode.append(tenantOffers
							.get(0)
							.getPerson()
							.getFirstName()
							.trim()
							.concat(" ")
							.concat(tenentOffers.get(0).getPerson()
									.getLastName().trim()));
				} else {
					accountCode.append(tenantOffers.get(0).getCompany()
							.getCompanyName().trim());
				}

				// set Bank Receipt Master informations
				// bankReceipts.setContract(contract);
				account = contractAgreementBL.getCustomerDetail(contract);
				Combination combination = contractAgreementBL
						.getCombinationByAccount(account.getAccountId());
				bankReceipts.setCombination(combination);
				bankReceipts.setOthers(accountCode.toString());
				bankReceipts.setReferenceNo(contract.getContractNo());

				// Identify the receipt type for contract
				for (LookupDetail ld : receiptTypes) {
					if (ld.getAccessCode().trim().equals("COR"))
						bankReceipts.setLookupDetail(ld);
				}

				List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
				// Contract Payments to bankReceiptDetails
				for (ContractPayment contractPayment : contract
						.getContractPayments()) {
					BankReceiptsDetail bankReceiptsDetail = new BankReceiptsDetail();
					bankReceiptsDetail.setRecordId(contractPayment
							.getContractPaymentId());
					bankReceiptsDetail.setAmount(contractPayment.getAmount());
					bankReceiptsDetail.setInstitutionName(contractPayment
							.getBankName());
					if (contractPayment.getChequeDate() != null)
						bankReceiptsDetail.setChequeDate(contractPayment
								.getChequeDate());

					if (contractPayment.getChequeNo() != null)
						bankReceiptsDetail.setBankRefNo(contractPayment
								.getChequeNo());

					// Set the contract payment method by identifying the
					// information
					Byte paymentMethod = null;
					if (bankReceiptsDetail.getChequeDate() == null
							&& bankReceiptsDetail.getBankRefNo() == null) {
						paymentMethod = 1;// Cash
					} else if (bankReceiptsDetail.getChequeDate() != null
							&& bankReceiptsDetail.getBankRefNo() != null) {
						paymentMethod = 2;
					} else if (bankReceiptsDetail.getChequeDate() != null
							&& bankReceiptsDetail.getBankRefNo() == null) {
						paymentMethod = 3;
					}
					bankReceiptsDetail.setPaymentMode(paymentMethod);

					Combination pdccombination = null;

					if (paymentMethod != 1
							&& bankReceiptsDetail.getChequeDate() != null
							&& DateFormat.monthDifference(
									period.getStartTime(),
									bankReceiptsDetail.getChequeDate()) > 0) {
						pdccombination = new Combination();
						pdccombination.setCombinationId(getImplementation()
								.getPdcReceived());
						pdccombination = this
								.getBankReceiptsBL()
								.getTransactionBL()
								.getCombinationService()
								.getCombinationAllAccountDetail(
										pdccombination.getCombinationId());

					} else if (paymentMethod != 1) {
						pdccombination = new Combination();
						pdccombination.setCombinationId(getImplementation()
								.getClearingAccount());
						pdccombination = this
								.getBankReceiptsBL()
								.getTransactionBL()
								.getCombinationService()
								.getCombinationAllAccountDetail(
										pdccombination.getCombinationId());
					}
					bankReceiptsDetail.setCombination(pdccombination);

					bankReceiptsDetails.add(bankReceiptsDetail);
				}

				bankReceipts
						.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
								bankReceiptsDetails));
				// Get Property Rent Income Account
				Property property = this.getContractAgreementBL()
						.getPropertyDetails(recordId);
				Account rentAccount = this
						.getContractAgreementBL()
						.getTransactionBL()
						.getCombinationService()
						.getAccountDetail(
								"Rent Income " + property.getPropertyName(),
								contract.getImplementation());
				Combination rentCombination = contractAgreementBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationByNaturalAccount(
								rentAccount.getAccountId());
				ServletActionContext.getRequest().setAttribute(
						"RENT_COMBINATION", rentCombination);
			}
			ServletActionContext.getRequest().setAttribute("PERIOD_INFO",
					period.getStartTime());
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					modeOfPaymetns);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_STATUS",
					this.getBankReceiptsBL().getLookupMasterBL()
							.getActiveLookupDetails("PAYMENT_STATUS", false));
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_NO",
					SystemBL.getReferenceStamp(BankReceipts.class.getName(),
							bankReceiptsBL.getImplementation()));
			ServletActionContext.getRequest().setAttribute("RECEIPT_TYPES",
					receiptTypes);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			ServletActionContext.getRequest().setAttribute("RECEIPT_SOURCE",
					payeeTypes);
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"CONTRACT");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public LookupDetail getPaymentMethodLookupInfo(
			List<LookupDetail> lookupDetails, Byte paymethod) {
		LookupDetail lookupDetail = null;
		for (LookupDetail templookupDetail : lookupDetails) {
			if (paymethod == 1
					&& templookupDetail.getAccessCode().trim().equals("CSH")) {
				return templookupDetail;
			} else if (paymethod == 2
					&& templookupDetail.getAccessCode().trim().equals("CHQ")) {
				return templookupDetail;
			} else if (paymethod == 3
					&& templookupDetail.getAccessCode().trim().equals("WRT")) {
				return templookupDetail;
			}
		}

		return lookupDetail;
	}

	// contractPaymentReportXLS
	public String contractPaymentReportXLS() {

		try {

			agreementTOList = new ArrayList<ReContractAgreementTO>();
			getImplementId();
			paymentStatus = Byte.parseByte(paymentStatusStr);
			agreementTOList = contractAgreementBL.getContractPaymentList(
					implementation, paymentStatus,
					DateFormat.convertStringToDate(fromDate),
					DateFormat.convertStringToDate(toDate));
			String str = "Filter Options\n\n";
			str += "Pament Status,Effective From, Effective To\n"
					+ ReContractAgreementTOConverter
							.paymentStatusString(paymentStatus) + ",";
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals("")) {
				str += fromDate + "," + toDate;
			} else {
				str += "-NA-,-NA-";
			}

			str += "Contract Payments Information\n";
			str += "Contract Number,Tenant Name,Property Name,Component/Unit,Payment Type,"
					+ "Payment Mode,Amount,Bank Name,Cheque Date,Cheque No.,Payment Status\n";

			for (ReContractAgreementTO list : agreementTOList) {
				str += list.getContractNumber() + "," + list.getTenantName()
						+ "," + list.getBuildingName() + ","
						+ list.getFlatNumber() + "," + list.getPaymentType()
						+ "," + list.getPaymentMethod() + ","
						+ list.getChequeAmount() + "," + list.getBankName()
						+ "," + list.getChequeDate() + ","
						+ list.getChequeNumber() + ","
						+ list.getPaymentVisibility() + "\n";

			}
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// property info
	public String propertyUnitRentReportRedirect() {

		try {
			getImplementId();
			List<Property> properties = propertyInformationBL
					.getPropertyService().getAllProperties(implementation);
			List<RePropertyInfoTO> propertyList = propertyInformationBL
					.propertyOwnerDetailInfo(properties);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					propertyList);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing JSON property unit rental
	public String getPropertyUnitRentalList() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();

			contracts = new ArrayList<Contract>();
			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (buildingId > 0 && fromDate != null && toDate != null)
				agreementTOList = contractAgreementBL
						.getPropertyUnitRentalList(
								Long.parseLong(buildingId + ""),
								DateFormat.convertStringToDate(fromDate),
								DateFormat.convertStringToDate(toDate));

			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getBuildingId() + "");
				array.add(list.getBuildingName());
				array.add(list.getTenantName());
				array.add(list.getContractPeriod());
				array.add(list.getContractAmount());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing property unit rental print
	public String getPropertyUnitRentalReportPrint() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();

			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (buildingId > 0 && fromDate != null && toDate != null)
				agreementTOList = contractAgreementBL
						.getPropertyUnitRentalList(
								Long.parseLong(buildingId + ""),
								DateFormat.convertStringToDate(fromDate),
								DateFormat.convertStringToDate(toDate));
			Double total = 0.0;
			long propid = 0;
			for (ReContractAgreementTO to : agreementTOList) {
				total += to.getContractAmount();
				if (to.getBuildingId() == propid) {
					to.setBuildingName("");
				}
				propid = to.getBuildingId();
			}

			agreementTO.setBuildingName(buildingName);
			agreementTO.setContractPeriod(fromDate + " through " + toDate);

			ServletActionContext.getRequest().setAttribute("PROPERTY_MASTER",
					agreementTO);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					agreementTOList);
			ServletActionContext.getRequest().setAttribute("TOTAL", total);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing property unit rental downlaod xls
	public String getPropertyUnitRentalReportXLS() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();

			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (buildingId > 0 && fromDate != null && toDate != null)
				agreementTOList = contractAgreementBL
						.getPropertyUnitRentalList(
								Long.parseLong(buildingId + ""),
								DateFormat.convertStringToDate(fromDate),
								DateFormat.convertStringToDate(toDate));
			Double total = 0.0;
			long propid = 0;
			for (ReContractAgreementTO to : agreementTOList) {
				total += to.getContractAmount();
				if (to.getBuildingId() == propid) {
					to.setBuildingName("");
				}
				propid = to.getBuildingId();
			}

			String str = "Filter Options\n\n";
			str += "Property,Effective From, Effective To\n" + buildingName
					+ ",";
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals("")) {
				str += fromDate + "," + toDate;
			} else {
				str += "-NA-,-NA-";
			}

			str += "\nRental Information\n";
			str += "Unit,Tenant,Period,Amount\n";

			for (ReContractAgreementTO list : agreementTOList) {
				str += list.getBuildingName() + "," + list.getTenantName()
						+ "," + list.getContractPeriod() + ","
						+ list.getContractAmount() + "\n";

			}
			str += ",," + "Total : ," + total;
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Propety rental
	// code-----------------------------------------------------------------------
	// Listing JSON property rental
	public String getPropertyRentalList() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();

			contracts = new ArrayList<Contract>();
			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals(""))
				agreementTOList = contractAgreementBL.getPropertyRentalList(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));

			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getBuildingId() + "");
				array.add(list.getBuildingArea());
				array.add(list.getBuildingName());
				array.add(list.getContractAmount());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing property rental print
	public String getPropertyRentalReportPrint() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();

			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals(""))
				agreementTOList = contractAgreementBL.getPropertyRentalList(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			Double total = 0.0;
			String area = "";
			for (ReContractAgreementTO to : agreementTOList) {
				total += to.getContractAmount();
				if (to.getBuildingArea() != null
						&& to.getBuildingArea().equalsIgnoreCase(area)) {
					to.setBuildingArea("");
				}
				area = to.getBuildingArea();
			}

			agreementTO.setContractPeriod(fromDate + " through " + toDate);

			ServletActionContext.getRequest().setAttribute("PROPERTY_MASTER",
					agreementTO);
			ServletActionContext.getRequest().setAttribute("PROPERTY_LIST",
					agreementTOList);
			ServletActionContext.getRequest().setAttribute("TOTAL", total);

			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Listing property rental downlaod xls
	public String getPropertyRentalReportXLS() {

		try {
			legalProcess = 0;// To reset the previous request attribute
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();

			// implementationId=(Long) session.get("THIS");
			getImplementId();
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals(""))
				agreementTOList = contractAgreementBL.getPropertyRentalList(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			Double total = 0.0;
			String area = "";
			for (ReContractAgreementTO to : agreementTOList) {
				total += to.getContractAmount();
				if (to.getBuildingArea() != null
						&& to.getBuildingArea().equalsIgnoreCase(area)) {
					to.setBuildingArea("");
				}
				area = to.getBuildingArea();
			}

			String str = "Filter Options\n\n";
			str += "Effective From, Effective To\n";
			if (fromDate != null && !fromDate.equals("") && toDate != null
					&& !toDate.equals("")) {
				str += fromDate + "," + toDate;
			} else {
				str += "-NA-,-NA-";
			}

			str += "\nRental Information\n";
			str += "Area,Property,Amount\n";

			for (ReContractAgreementTO list : agreementTOList) {
				str += list.getBuildingArea() + "," + list.getBuildingName()
						+ "," + list.getContractAmount() + "\n";

			}
			str += "," + "Total : ," + total;
			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	// Contract filter search
	public String contractRentalRangeReport() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			JSONObject jsonResponse = new JSONObject();
			contracts = new ArrayList<Contract>();
			getImplementId();

			Double fromAmount = null;
			Double toAmount = null;
			if (fromDate != null && !fromDate.equalsIgnoreCase(""))
				fromAmount = Double.parseDouble(fromDate);

			if (toDate != null && !toDate.equalsIgnoreCase(""))
				toAmount = Double.parseDouble(toDate);

			agreementTOList = contractAgreementBL
					.getContractRentalRangeReportFilterInformation(
							implementation, fromAmount, toAmount);

			if (agreementTOList != null && agreementTOList.size() > 0) {
				iTotalRecords = agreementTOList.size();
				iTotalDisplayRecords = agreementTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ReContractAgreementTO list : agreementTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getFlatNumber());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				array.add(list.getOfferAmount());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	//
	// Contract filter search for report
	public String contractRentalRangeReportPrint() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			contracts = new ArrayList<Contract>();
			ReContractAgreementTO to = new ReContractAgreementTO();
			getImplementId();
			Double fromAmount = null;
			Double toAmount = null;
			if (fromDate != null && !fromDate.equalsIgnoreCase(""))
				fromAmount = Double.parseDouble(fromDate);

			if (toDate != null && !toDate.equalsIgnoreCase(""))
				toAmount = Double.parseDouble(toDate);

			agreementTOList = contractAgreementBL
					.getContractRentalRangeReportFilterInformation(
							implementation, fromAmount, toAmount);

			if (fromDate != null && !fromDate.equals(""))
				to.setFromDate(fromDate);
			else
				to.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				to.setToDate(toDate);
			else
				to.setToDate("-NA-");

			ServletActionContext.getRequest().setAttribute("TENANT_INFO", to);
			ServletActionContext.getRequest().setAttribute("CONTRACT_INFO", to);
			ServletActionContext.getRequest().setAttribute(
					"TENANT_CONTRACT_LIST", agreementTOList);
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Contract filter search for report as XLS
	public String contractRentalRangeReportXLS() {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			// Set the value to push to Personal Details DAO
			agreementTO = new ReContractAgreementTO();
			contracts = new ArrayList<Contract>();
			ReContractAgreementTO tenant = new ReContractAgreementTO();
			getImplementId();
			String str = "";
			Double fromAmount = null;
			Double toAmount = null;
			if (fromDate != null && !fromDate.equalsIgnoreCase(""))
				fromAmount = Double.parseDouble(fromDate);

			if (toDate != null && !toDate.equalsIgnoreCase(""))
				toAmount = Double.parseDouble(toDate);

			agreementTOList = contractAgreementBL
					.getContractRentalRangeReportFilterInformation(
							implementation, fromAmount, toAmount);

			if (fromDate != null && !fromDate.equals(""))
				tenant.setFromDate(fromDate);
			else
				tenant.setFromDate("-NA-");

			if (toDate != null && !toDate.equals(""))
				tenant.setToDate(toDate);
			else
				tenant.setToDate("-NA-");

			str = "Contract Details\n\n";
			str += "Filter Condition\n";
			str += "Amount Range Greater,Amount Range Less\n";
			str += tenant.getFromDate() + "," + tenant.getToDate() + "\n\n";
			if (agreementTOList != null && agreementTOList.size() > 0) {
				str += "Contract No.,Contract Date,Deposit Amount,Contract Fee,Tenant Name,Tenant Info,Property Name,Contract Period,Property Details,Rent Amount";
				str += "\n";
				for (ReContractAgreementTO to : agreementTOList) {
					str += to.getContractNumber()
							+ ","
							+ to.getContractDate()
							+ ","
							+ to.getDepositAmount()
							+ ","
							+ to.getContractAmount()
							+ ","
							+ to.getTenantName()
							+ ","
							+ to.getTenantFullInfo().replaceAll(",", "-")
							+ ","
							+ to.getBuildingName()
							+ ","
							+ to.getContractPeriod()
							+ ","
							+ (to.getPropertyFullInfo() != null ? to
									.getPropertyFullInfo().replaceAll(",", "-")
									: "") + "," + to.getOfferAmount();

					str += "\n";

				}

			}

			InputStream is = new ByteArrayInputStream(str.getBytes());
			fileInputStream = is;

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			return ERROR;
		}
	}

	// to split the values
	public static String[] splitValues(String spiltValue, String delimeter) {
		return spiltValue.split(delimeter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	// Renewal Get
	public String getRenewContractDetails() {

		List<ReContractAgreementTO> offerTOList = new ArrayList<ReContractAgreementTO>();
		ReContractAgreementTO offerAgreementTO = new ReContractAgreementTO();
		HttpSession session = ServletActionContext.getRequest().getSession();

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();

			session.removeAttribute("CONTRACT_PAYMENTS_"
					+ sessionObj.get("jSessionId"));
			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow

			if (recordId != 0)
				contractId = (int) recordId;

			List<Integer> intelist = new ArrayList<Integer>();
			agreementTO = new ReContractAgreementTO();
			getImplementId();

			if (contractId != 0) {
				agreementTOList = new ArrayList<ReContractAgreementTO>();
				contract = new Contract();
				contractPayments = new ArrayList<ContractPayment>();
				offer = new Offer();

				contract = contractAgreementBL.getContractService()
						.getContractDetails(Long.parseLong(contractId + ""));
				agreementTO = ReContractAgreementTOConverter
						.convertToTO(contract);

				// Contract Number generate

				offer = contractAgreementBL.getContractService()
						.getOfferDetails(contract.getOffer().getOfferId());
				offerAgreementTO = ReContractAgreementTOConverter
						.convertToOfferTO(offer);

				// Find Tenant offer
				tenentOffers = this.contractService.getTenantOfferDetails(offer
						.getOfferId());
				Company company = null;
				if (tenentOffers != null && tenentOffers.size() > 0
						&& tenentOffers.get(0).getPerson() != null) {
					// Find Person
					persons = this.contractService
							.getPersonDetails(tenentOffers.get(0).getPerson());
					if (persons != null && persons.size() > 0) {
						offerAgreementTO.setTenantName(persons.get(0)
								.getFirstName()
								+ " "
								+ persons.get(0).getMiddleName()
								+ " "
								+ persons.get(0).getLastName());
						offerAgreementTO.setTenantNameArabic(AIOSCommons
								.bytesToUTFString(persons.get(0)
										.getFirstNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(persons.get(0)
										.getMiddleNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(persons.get(0)
										.getLastNameArabic()));
						if (persons.get(0).getPrefix() != null)
							offerAgreementTO.setPrefix(persons.get(0)
									.getPrefix());
						offerAgreementTO.setTenantId(Integer.parseInt(persons
								.get(0).getPersonId().toString()));
						offerAgreementTO.setPresentAddress(persons.get(0)
								.getTownBirth());
						offerAgreementTO.setPermanantAddress(persons.get(0)
								.getRegionBirth());
						offerAgreementTO.setTenantIssueDate(persons.get(0)
								.getValidFromDate().toString());
						try {
							List<Identity> identity = personService
									.getAllIdentity(persons.get(0));
							offerAgreementTO
									.setTenantIdentity(getIdentityType(identity
											.get(0).getIdentityType())
											+ " - "
											+ identity.get(0)
													.getIdentityNumber());
						} catch (Exception e) {
							e.printStackTrace();
							offerAgreementTO.setTenantIdentity("n/a");
						}
						offerAgreementTO.setTenantNationality(systemService
								.getCountryById(
										Long.parseLong(persons.get(0)
												.getNationality()))
								.getCountryName());
						if (persons.get(0).getMobile() != null) {
							offerAgreementTO.setTenantMobile(persons.get(0)
									.getMobile().toString());
						}
						if (persons.get(0).getAlternateTelephone() != null)
							offerAgreementTO.setTenantFax(persons.get(0)
									.getAlternateTelephone().toString());
						if (persons.get(0).getPostboxNumber() != null)
							offerAgreementTO.setTenantPOBox(persons.get(0)
									.getPostboxNumber());
					}
				} else {
					company = new Company();
					company = contractAgreementBL.getOfferService()
							.getCompanyDetails(
									tenentOffers.get(0).getCompany()
											.getCompanyId());
					if (company != null) {
						offerAgreementTO
								.setTenantName(company.getCompanyName());
						offerAgreementTO.setTenantNameArabic(AIOSCommons
								.bytesToUTFString(company
										.getCompanyNameArabic()));
						offerAgreementTO.setTenantId(Integer.parseInt(company
								.getCompanyId().toString()));
						offerAgreementTO.setPresentAddress(company
								.getCompanyAddress());
						if (company.getTradeNoIssueDate() != null)
							offerAgreementTO.setTenantIssueDate(company
									.getTradeNoIssueDate().toString());
						else
							offerAgreementTO.setTenantIssueDate("n/a");
						offerAgreementTO
								.setTenantIdentity(company.getTradeNo());
						offerAgreementTO.setTenantNationality(company
								.getCompanyOrigin());
						offerAgreementTO.setTenantFax(company.getCompanyFax());
						offerAgreementTO.setTenantMobile(company
								.getCompanyPhone());
						offerAgreementTO.setTenantPOBox(company
								.getCompanyPobox());
						offerAgreementTO.setPrefix("");
					}
				}

				contractPayments = contractAgreementBL.getContractService()
						.getContractPaymentList(contractId);

				agreementTOList = ReContractAgreementTOConverter
						.convertToPaymentTOList(contractPayments);

				offerTOList = contractAgreementBL.getOfferDeatilsList(Integer
						.parseInt(contract.getOffer().getOfferId() + ""));

				ServletActionContext.getRequest().setAttribute(
						"OFFER_DETAIL_LIST", offerTOList);

				if (agreementTOList != null && agreementTOList.size() > 0)
					ServletActionContext.getRequest().setAttribute(
							"countSize3", agreementTOList.size());

				ServletActionContext.getRequest().setAttribute(
						"CONTRACT_PAYMENT", agreementTOList);
				ServletActionContext.getRequest().setAttribute("OFFER_INFO",
						offerAgreementTO);

				// Property Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (recordId != 0) {
					comment.setRecordId(Long.parseLong(contractId + ""));
					comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Contract");
					comment = commentBL.getCommentInfo(comment);
				}
				ServletActionContext.getRequest().getSession()
						.setAttribute("COMMENT_IFNO", comment);
				// Comment process End

			}
			List<Person> persons = contractAgreementBL.getPersonService()
					.getAllPerson(implementation);
			agreementTO.setRenewalFlag(renewalFlag);

			// Contract Number show
			contractNumber = "0";
			contracts = contractAgreementBL.getContractService()
					.getAllContractsExceptIsApproveValidation(implementation);
			if (contractNumber == null || contractNumber.trim().equals("0")
					|| contractNumber.trim().equals("")) {
				if (contracts != null && contracts.size() > 0) {
					for (int i = 0; i < contracts.size(); i++) {
						intelist.add(Integer.parseInt(contracts.get(i)
								.getContractNo()));
					}
					LOGGER.info("Contract Number Max()--"
							+ Collections.max(intelist));

					int temp = Collections.max(intelist);
					temp += 1;
					contractNumber = temp + "";
				} else {
					contractNumber = "1000";
				}

			}
			agreementTO.setContractNumber(contractNumber);

			session.setAttribute("AGREEMENT_DETAILS", agreementTO);
			session.setAttribute("AGREEMENT_PAYMENT_DETAILS", agreementTOList);
			session.setAttribute("AGREEMENT_TENANT_DETAILS", offerAgreementTO);
			session.setAttribute("AGREEMENT_OFFER_DETAILS", offerTOList);
			session.setAttribute("TRANSFER_SUBJECT", contractTransferType);
			if (contractTransferText != null)
				session.setAttribute("TRANSFER_TEXT",
						AIOSCommons.translatetoArabic(contractTransferText));
			session.setAttribute("TRANSFER_ACCOUNT",
					contractTransferAccountNumber);

			String referenceStamp = SystemBL.getReferenceStamp(
					Contract.class.getName(), implementation);
			if (referenceStamp != null)
				session.setAttribute("CONTRACT_REF", referenceStamp);
			else
				session.setAttribute("CONTRACT_REF", "---");

			ServletActionContext.getRequest().setAttribute("bean", agreementTO);
			ServletActionContext.getRequest().setAttribute("PERSON_LIST",
					persons);

			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String renewSaveContractAgreement() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			contract = new Contract();
			offer = new Offer();
			List<Integer> intelist = new ArrayList<Integer>();
			// Call Implementation
			getImplementId();

			// Update the previous contract in order to avoid to future
			contract = contractAgreementBL.getContractService()
					.getContractDetails(Long.parseLong(contractId + ""));

			Contract updateContract = contractAgreementBL.getContractService()
					.getContractDetails(Long.parseLong(contractId + ""));
			contract.setIsRenewal(null);

			contracts = contractAgreementBL.getContractService()
					.getAllContractsExceptIsApproveValidation(implementation);

			if (contracts != null && contracts.size() > 0) {
				for (int i = 0; i < contracts.size(); i++) {
					intelist.add(Integer.parseInt(contracts.get(i)
							.getContractNo()));
				}
				LOGGER.info("Contract Number Max()--"
						+ Collections.max(intelist));

				int temp = Collections.max(intelist);
				temp += 1;
				contractNumber = temp + "";
			}

			contract.setContractNo(contractNumber);

			Person person = new Person();
			person.setPersonId(user.getPersonId());
			contract.setPerson(person);
			contract.setCreatedDate(new Date());

			contract.setContractDate(DateFormat
					.convertStringToDate(contractDate));
			Offer tempOffer = new Offer();
			tempOffer = contractAgreementBL.getOfferService().getOffersById(
					Long.parseLong(offerId + ""));

			tempOffer.setStartDate(DateFormat.convertStringToDate(fromDate));
			tempOffer.setEndDate(DateFormat.convertStringToDate(toDate));
			tempOffer.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.Confirm.getCode()
							+ ""));
			List<OfferDetail> offerDetailList = contractAgreementBL
					.getOfferService().getOfferDetailById(
							tempOffer.getOfferId());
			Set<OfferDetail> offDet = new HashSet<OfferDetail>(
					getOfferLineDetail(offerLineDetail, tempOffer,
							offerDetailList));
			tempOffer.setOfferDetails(offDet);

			contract.setOffer(tempOffer);
			contract.setContractFee(contractAmount);
			contract.setDeposit(depositAmount);
			contract.setInstallments(noOfCheques);
			contract.setPaymentReceived(paymentReceived);
			contract.setImplementation(implementation);
			contract.setRemarks(remarks);
			contract.setLeasedFor(leasedFor);
			TenantOffer tenantOffer = contractAgreementBL.getOfferService()
					.getTenantById(contract.getOffer().getOfferId());
			tenantOffer.setTenantOfferId(null);
			contractAgreementBL.renewSaveContractAgreement(contract,
					implementation, false, tenantOffer, updateContract);

			ServletActionContext.getRequest().setAttribute("succMsg",
					"Contract Renewed Succesfully");

			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();

			ServletActionContext.getRequest().setAttribute("errMsg",
					"Contract Renewal Failure");
			AIOSCommons.removeUploaderSession(session, "contractDocs",
					(long) -1, "Contract");

		} finally {

		}
		return SUCCESS;
	}

	public List<OfferDetail> getOfferLineDetail(String offerLineDetail,
			Offer offer, List<OfferDetail> tempOfferDetailList) {
		List<OfferDetail> offerDetails = new ArrayList<OfferDetail>();
		if (offerLineDetail != null && !offerLineDetail.equals("")) {
			String[] recordArray = new String[offerLineDetail.split("##").length];
			recordArray = offerLineDetail.split("##");
			for (String record : recordArray) {
				OfferDetail offerDetail = new OfferDetail();

				String[] dataArray = new String[record.split("@").length];
				dataArray = record.split("@");
				if (!dataArray[0].equals("-1")
						&& !dataArray[0].trim().equals(""))
					offerDetail.setOfferDetailId(Long.parseLong(dataArray[0]));
				if (!dataArray[1].equals("-1")
						&& !dataArray[1].trim().equals(""))
					offerDetail.setOfferAmount(Double.valueOf(dataArray[1]));

				for (OfferDetail tempOfferDetail : tempOfferDetailList) {
					if (tempOfferDetail.getOfferDetailId() != null
							&& tempOfferDetail.getOfferDetailId().equals(
									offerDetail.getOfferDetailId())) {
						tempOfferDetail.setOfferAmount(offerDetail
								.getOfferAmount());
						tempOfferDetail.setOfferDetailId(null);
						offerDetails.add(tempOfferDetail);
					}
				}
			}
		}
		return offerDetails;
	}

	public String getTrnValue() {
		return trnValue;
	}

	public void setTrnValue(String trnValue) {
		this.trnValue = trnValue;
	}

	public int getHeaderFlag() {
		return headerFlag;
	}

	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public int getActualLineId() {
		return actualLineId;
	}

	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}

	public String getTempLineId() {
		return tempLineId;
	}

	public void setTempLineId(String tempLineId) {
		this.tempLineId = tempLineId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public int getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanantAddress() {
		return permanantAddress;
	}

	public void setPermanantAddress(String permanantAddress) {
		this.permanantAddress = permanantAddress;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public int getTenantTypeId() {
		return tenantTypeId;
	}

	public void setTenantTypeId(int tenantTypeId) {
		this.tenantTypeId = tenantTypeId;
	}

	public String getTenantTypeName() {
		return tenantTypeName;
	}

	public void setTenantTypeName(String tenantTypeName) {
		this.tenantTypeName = tenantTypeName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getNoOfCheques() {
		return noOfCheques;
	}

	public void setNoOfCheques(int noOfCheques) {
		this.noOfCheques = noOfCheques;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public double getRentAmount() {
		return rentAmount;
	}

	public void setRentAmount(double rentAmount) {
		this.rentAmount = rentAmount;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public int getFeeId() {
		return feeId;
	}

	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	public String getFeeDescription() {
		return feeDescription;
	}

	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public int getOfferNumber() {
		return offerNumber;
	}

	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}

	public int getContractRentId() {
		return contractRentId;
	}

	public void setContractRentId(int contractRentId) {
		this.contractRentId = contractRentId;
	}

	public int getContractFeeId() {
		return contractFeeId;
	}

	public void setContractFeeId(int contractFeeId) {
		this.contractFeeId = contractFeeId;
	}

	public int getContractPaymentId() {
		return contractPaymentId;
	}

	public void setContractPaymentId(int contractPaymentId) {
		this.contractPaymentId = contractPaymentId;
	}

	public int getOfferDetailId() {
		return offerDetailId;
	}

	public void setOfferDetailId(int offerDetailId) {
		this.offerDetailId = offerDetailId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public int getPropertyFlatId() {
		return propertyFlatId;
	}

	public void setPropertyFlatId(int propertyFlatId) {
		this.propertyFlatId = propertyFlatId;
	}

	public String getExtendedToDate() {
		return extendedToDate;
	}

	public void setExtendedToDate(String extendedToDate) {
		this.extendedToDate = extendedToDate;
	}

	public int getRentFees() {
		return rentFees;
	}

	public void setRentFees(int rentFees) {
		this.rentFees = rentFees;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getNotifyFunctionId() {
		return notifyFunctionId;
	}

	public void setNotifyFunctionId(int notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	public String getWfFunctionType() {
		return wfFunctionType;
	}

	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}

	public String getWorkflowParam() {
		return workflowParam;
	}

	public void setWorkflowParam(String workflowParam) {
		this.workflowParam = workflowParam;
	}

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getRequestReason() {
		return requestReason;
	}

	public void setRequestReason(String requestReason) {
		this.requestReason = requestReason;
	}

	public int getSessionPersonId() {
		return sessionPersonId;
	}

	public void setSessionPersonId(int sessionPersonId) {
		this.sessionPersonId = sessionPersonId;
	}

	public int getHeaderLedgerId() {
		return headerLedgerId;
	}

	public void setHeaderLedgerId(int headerLedgerId) {
		this.headerLedgerId = headerLedgerId;
	}

	public int getWfCategoryId() {
		return wfCategoryId;
	}

	public void setWfCategoryId(int wfCategoryId) {
		this.wfCategoryId = wfCategoryId;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getApprovedStatus() {
		return approvedStatus;
	}

	public void setApprovedStatus(String approvedStatus) {
		this.approvedStatus = approvedStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getApprovedStatusName() {
		return approvedStatusName;
	}

	public void setApprovedStatusName(String approvedStatusName) {
		this.approvedStatusName = approvedStatusName;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}

	public ContractAgreementBL getContractAgreementBL() {
		return contractAgreementBL;
	}

	public void setContractAgreementBL(ContractAgreementBL contractAgreementBL) {
		this.contractAgreementBL = contractAgreementBL;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	private String format;
	private ImageBL imageBL;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();
	private List<ReContractAgreementTO> componentDS;

	/*
	 * public String getContractDetail() { try{ componentDS = new
	 * ArrayList<ReContractAgreementTO>(); Contract contract = new Contract();
	 * if(recordId >0) contractId=(int) recordId; contract.setContractId((long)
	 * contractId); ReContractAgreementTO to =
	 * contractAgreementBL.getContractDetail(contract);
	 * 
	 * to.setLinkLabel("HTML".equals(getFormat())? "Download" : "");
	 * to.setAnchorExpression("HTML".equals(getFormat())?
	 * "downloadPropContractDetail.action?contractId="+ contractId +
	 * "&format=PDF" : "#");
	 * //to.setAnchorExpression("downloadPropContractDetail.action?contractId="+
	 * contractId + "&format=PDF"); //Approve Content
	 * to.setApproveLinkLabel("Approve");
	 * to.setApproveAnchorExpression("workflowProcess.action?processFlag="
	 * +Byte.parseByte(Constants.RealEstate.Status.Approved.getCode()+""));
	 * 
	 * //Disapprove Content to.setRejectLinkLabel("Reject");
	 * to.setRejectAnchorExpression
	 * ("workflowProcess.action?processFlag="+Byte.parseByte
	 * (Constants.RealEstate.Status.Disapproved.getCode()+""));
	 * 
	 * componentDS.add(to); String ctxRealPath =
	 * ServletActionContext.getServletContext().getRealPath("/");
	 * jasperRptParams.put("AIOS_LOGO", "file:///" +
	 * ctxRealPath.concat(File.separator).concat("images" + File.separator +
	 * "aiotech.jpg")); if (componentDS!= null && componentDS.size() == 0){
	 * componentDS.add(to); }else{ componentDS.set(0, to); } }catch (Exception
	 * e){ e.printStackTrace(); } return SUCCESS; }
	 */

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	public List<ReContractAgreementTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<ReContractAgreementTO> componentDS) {
		this.componentDS = componentDS;
	}

	public OfferManagementBL getOfferBL() {
		return offerBL;
	}

	public void setOfferBL(OfferManagementBL offerBL) {
		this.offerBL = offerBL;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public long getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(long paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getActionFlag1() {
		return actionFlag1;
	}

	public void setActionFlag1(String actionFlag1) {
		this.actionFlag1 = actionFlag1;
	}

	public TenantGroupService getTenantGroupService() {
		return tenantGroupService;
	}

	public void setTenantGroupService(TenantGroupService tenantGroupService) {
		this.tenantGroupService = tenantGroupService;
	}

	public int getLegalProcess() {
		return legalProcess;
	}

	public void setLegalProcess(int legalProcess) {
		this.legalProcess = legalProcess;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getContractTransferType() {
		return contractTransferType;
	}

	public void setContractTransferType(String contractTransferType) {
		this.contractTransferType = contractTransferType;
	}

	public String getContractTransferText() {
		return contractTransferText;
	}

	public void setContractTransferText(String contractTransferText) {
		this.contractTransferText = contractTransferText;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public String getLeasedFor() {
		return leasedFor;
	}

	public void setLeasedFor(String leasedFor) {
		this.leasedFor = leasedFor;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public String getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(String postDetails) {
		this.postDetails = postDetails;
	}

	public String getContractTransferAccountNumber() {
		return contractTransferAccountNumber;
	}

	public void setContractTransferAccountNumber(
			String contractTransferAccountNumber) {
		this.contractTransferAccountNumber = contractTransferAccountNumber;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getContractPrintStyle() {
		return contractPrintStyle;
	}

	public void setContractPrintStyle(String contractPrintStyle) {
		this.contractPrintStyle = contractPrintStyle;
	}

	public Byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentStatusStr() {
		return paymentStatusStr;
	}

	public void setPaymentStatusStr(String paymentStatusStr) {
		this.paymentStatusStr = paymentStatusStr;
	}

	public Boolean getIsMultipleUnit() {
		return isMultipleUnit;
	}

	public void setIsMultipleUnit(Boolean isMultipleUnit) {
		this.isMultipleUnit = isMultipleUnit;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public PropertyInformationBL getPropertyInformationBL() {
		return propertyInformationBL;
	}

	public void setPropertyInformationBL(
			PropertyInformationBL propertyInformationBL) {
		this.propertyInformationBL = propertyInformationBL;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public ReferenceManagerBL getReferenceManagerBL() {
		return referenceManagerBL;
	}

	public void setReferenceManagerBL(ReferenceManagerBL referenceManagerBL) {
		this.referenceManagerBL = referenceManagerBL;
	}

	public String getOfferLineDetail() {
		return offerLineDetail;
	}

	public void setOfferLineDetail(String offerLineDetail) {
		this.offerLineDetail = offerLineDetail;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public int getProcessFlag() {
		return processFlag;
	}

	public void setProcessFlag(int processFlag) {
		this.processFlag = processFlag;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public WorkflowBL getWorkflowBL() {
		return workflowBL;
	}

	public void setWorkflowBL(WorkflowBL workflowBL) {
		this.workflowBL = workflowBL;
	}

}
