package com.aiotech.aios.realestate.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.service.bl.BankReceiptsBL;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.CancellationService;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.ReleaseService;
import com.aiotech.aios.realestate.service.bl.ContractAgreementBL;
import com.aiotech.aios.realestate.service.bl.ContractReleaseBL;
import com.aiotech.aios.realestate.to.ContractReleaseTO;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.converter.ContractReleaseTOConverter;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.bl.SystemBL;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ContractReleaseAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager
			.getLogger(ReContractAgreementAction.class);

	private long contractId;
	private long releaseId;
	private long releaseDetailId;
	private String initializationFlag;
	private String assetName;
	private String damage;
	private double balanceAmount;
	private String id;
	private boolean waterCert;
	private boolean electricityCert;
	private boolean addcCert;
	private boolean checkPayment;
	private boolean checkDamage;
	private double grandTotal;
	private boolean releaseStatus;
	private long recordId;
	private boolean printTaken;
	private double penaltyAmount;
	private String effectiveDate;

	// For Print
	private String releaseType;
	private String transferAccount;

	ReleaseService releaseService;
	PersonService personService;
	ContractService contractService;

	ContractReleaseBL contractReleaseBL;
	ContractAgreementBL contractAgreementBL;
	private DocumentBL documentBL;
	private SystemService systemService;
	private CancellationService cancellationService;
	private CommentBL commentBL;
	private DirectPaymentBL directPaymentBL;
	private BankReceiptsBL bankReceiptsBL;

	List<ContractReleaseTO> releaseTOList = null;
	ContractReleaseTO releaseTO = null;

	Release release = null;
	ReContractAgreementTO agreementTO = null;
	Contract contract = null;
	List<Contract> contracts = null;
	ContractPayment contractPayment = null;
	List<ContractPayment> contractPayments = null;
	TenantOffer tenentOffer = null;
	List<TenantOffer> tenentOffers = null;
	Offer offer = null;

	private String addEditFlag;
	private Implementation implementation;
	private int iTotalRecords;
	private int iTotalDisplayRecords;
	List<Person> persons = null;
	private long alertId;

	public void getImplementId() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		implementation = new Implementation();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
	}

	public String returnSuccess() {
		// To set the chile record row id to identify.
		ServletActionContext.getRequest().setAttribute("rowid", id);
		ServletActionContext.getRequest().setAttribute("AddEditFlag",
				addEditFlag);
		return SUCCESS;
	}

	// Listing JSON
	public String execute() {
		try {
			releaseTOList = new ArrayList<ContractReleaseTO>();
			// Set the value to push to Personal Details DAO
			releaseTO = new ContractReleaseTO();
			JSONObject jsonResponse = new JSONObject();
			getImplementId();
			releaseTOList = contractReleaseBL
					.getReleaseListInformation(implementation);
			if (releaseTOList != null && releaseTOList.size() > 0) {
				iTotalRecords = releaseTOList.size();
				iTotalDisplayRecords = releaseTOList.size();
			}
			jsonResponse.put("iTotalRecords", iTotalRecords);
			jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
			JSONArray data = new JSONArray();
			for (ContractReleaseTO list : releaseTOList) {
				JSONArray array = new JSONArray();
				array.add(list.getReleaseId());
				array.add(list.getContractId());
				array.add(list.getContractNumber());
				array.add(list.getTenantName());
				array.add(list.getContractDate());
				array.add(list.getFromDate());
				array.add(list.getToDate());
				// Step 1
				if (list.isCheckDamage() == true && list.isWaterCert() == true
						&& list.isElectricityCert() == true
						&& list.isCheckPayment() == false) {
					array.add("Payment Process");
					array.add("No Print");
				} else if (list.isCheckDamage() == true
						&& list.isCheckPayment() == true
						&& list.isAddcCert() == true) {
					array.add("Released");
					array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"release_print\">Print</a>");
				} else if (list.isCheckDamage() == true
						&& list.isCheckPayment() == true) {
					array.add("ADDC Process");
					array.add("<a href=\"#\" style=\"text-decoration: none; color: grey\" class=\"release_print\">Print</a>");
				} else if (list.isCheckDamage() == true) {
					array.add("Document Process");
					array.add("No Print");
				} else if (list.isCheckDamage() == false) {
					array.add("Release Inspection");
					array.add("No Print");
				} else {
					array.add("Initiated");
					array.add("No Print");
				}

				data.add(array);
			}
			jsonResponse.put("aaData", data);
			ServletActionContext.getRequest().setAttribute("jsonlist",
					jsonResponse);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public String releaseInfoGet() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			session.removeAttribute("CONTRACT_MAINTAIN_DETAILS_"
					+ sessionObj.get("jSessionId"));
			session.removeAttribute("AIOS-doc-" + "Release-" + session.getId());
			// Identifying the process flow whether is coming from Master Gird
			// Table or Workflow
			if (recordId != 0)
				releaseId = (int) recordId;

			agreementTO = new ReContractAgreementTO();
			contract = new Contract();
			offer = new Offer();
			ReContractAgreementTO offerAgreementTO = new ReContractAgreementTO();
			releaseTO = new ContractReleaseTO();
			release = new Release();
			getImplementId();
			release = contractReleaseBL.getReleaseService().getReleaseInfo(
					releaseId);
			releaseTO = ContractReleaseTOConverter.convertToTO(release);

			contract = contractAgreementBL.getContractService()
					.getContractDetails(release.getContract().getContractId());
			agreementTO = ReContractAgreementTOConverter.convertToTO(contract);
			agreementTO.setReleaseId(releaseId);

			offer = contractAgreementBL.getContractService().getOfferDetails(
					contract.getOffer().getOfferId());
			offerAgreementTO = ReContractAgreementTOConverter
					.convertToOfferTO(offer);

			List<ReContractAgreementTO> offerTOList = new ArrayList<ReContractAgreementTO>();
			offerTOList = contractAgreementBL.getOfferDeatilsList(Integer
					.parseInt(contract.getOffer().getOfferId() + ""));
			// Contract Total Amount
			double contractTotal = 0;
			for (ReContractAgreementTO reContractAgreementTO : offerTOList) {
				contractTotal += reContractAgreementTO.getRentAmount();
			}

			// Find Tenent offer
			tenentOffers = contractAgreementBL.getContractService()
					.getTenantOfferDetails(offer.getOfferId());
			Company company = null;
			if (tenentOffers != null && tenentOffers.size() > 0
					&& tenentOffers.get(0).getPerson() != null) {
				// Find Person
				persons = this.contractService.getPersonDetails(tenentOffers
						.get(0).getPerson());
				if (persons != null && persons.size() > 0) {
					offerAgreementTO.setTenantName(persons.get(0)
							.getFirstName()
							+ " "
							+ persons.get(0).getMiddleName()
							+ " "
							+ persons.get(0).getLastName());
					offerAgreementTO.setTenantNameArabic(AIOSCommons
							.bytesToUTFString(persons.get(0)
									.getFirstNameArabic())
							+ " "
							+ AIOSCommons.bytesToUTFString(persons.get(0)
									.getMiddleNameArabic())
							+ " "
							+ AIOSCommons.bytesToUTFString(persons.get(0)
									.getLastNameArabic()));
					if (persons.get(0).getPrefix() != null)
						offerAgreementTO.setPrefix(persons.get(0).getPrefix());
					offerAgreementTO.setTenantId(Integer.parseInt(persons
							.get(0).getPersonId().toString()));
					offerAgreementTO.setPresentAddress(persons.get(0)
							.getTownBirth());
					offerAgreementTO.setPermanantAddress(persons.get(0)
							.getRegionBirth());
					offerAgreementTO.setTenantIssueDate(persons.get(0)
							.getValidFromDate().toString());
					try {
						List<Identity> identity = personService
								.getAllIdentity(persons.get(0));
						offerAgreementTO
								.setTenantIdentity(getIdentityType(identity
										.get(0).getIdentityType())
										+ " - "
										+ identity.get(0).getIdentityNumber());
					} catch (Exception e) {
						e.printStackTrace();
						offerAgreementTO.setTenantIdentity("n/a");
					}
					offerAgreementTO.setTenantNationality(systemService
							.getCountryById(
									Long.parseLong(persons.get(0)
											.getNationality()))
							.getCountryName());
					if (persons.get(0).getMobile() != null) {
						offerAgreementTO.setTenantMobile(persons.get(0)
								.getMobile().toString());
					}
					if (persons.get(0).getAlternateTelephone() != null)
						offerAgreementTO.setTenantFax(persons.get(0)
								.getAlternateTelephone().toString());
					if (persons.get(0).getPostboxNumber() != null)
						offerAgreementTO.setTenantPOBox(persons.get(0)
								.getPostboxNumber());
				}
			} else {
				company = new Company();
				company = contractAgreementBL
						.getOfferService()
						.getCompanyDetails(
								tenentOffers.get(0).getCompany().getCompanyId());
				if (company != null) {
					offerAgreementTO.setTenantName(company.getCompanyName());
					offerAgreementTO.setTenantNameArabic(AIOSCommons
							.bytesToUTFString(company.getCompanyNameArabic()));
					offerAgreementTO.setTenantId(Integer.parseInt(company
							.getCompanyId().toString()));
					offerAgreementTO.setPresentAddress(company
							.getCompanyAddress());
					if (company.getTradeNoIssueDate() != null)
						offerAgreementTO.setTenantIssueDate(company
								.getTradeNoIssueDate().toString());
					else
						offerAgreementTO.setTenantIssueDate("n/a");
					offerAgreementTO.setTenantIdentity(company.getTradeNo());
					offerAgreementTO.setTenantNationality(company
							.getCompanyOrigin());
					offerAgreementTO.setTenantFax(company.getCompanyFax());
					offerAgreementTO.setTenantMobile(company.getCompanyPhone());
					offerAgreementTO.setTenantPOBox(company.getCompanyPobox());
					offerAgreementTO.setPrefix("");
				}
			}

			// Property Comment Information --Added by rafiq
			CommentVO comment = new CommentVO();
			if (recordId != 0) {
				comment.setRecordId(Long.parseLong(releaseId + ""));
				comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Release");
				comment = commentBL.getCommentInfo(comment);

			}
			ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
					comment);
			// Comment process End

			List<ReleaseDetail> releaseDetailsList = contractReleaseBL
					.getReleaseService().getAllReleaseDetail(releaseId);

			releaseTOList = new ArrayList<ContractReleaseTO>();
			releaseTOList = ContractReleaseTOConverter
					.convertToDetailTOList(releaseDetailsList);

			double damageTotal = 0;
			for (ReleaseDetail releaseDetail : releaseDetailsList) {
				damageTotal += releaseDetail.getAmount();
			}
			double otherChargers = 0;
			double totalDeduction = 0;
			double refundAmount = 0;
			double depositAmount = 0;
			double penaltyAmount = 0;
			refundAmount = releaseTO.getBalanceAmount();
			depositAmount = agreementTO.getDepositAmount();
			otherChargers = depositAmount - damageTotal;
			otherChargers = otherChargers - refundAmount;
			// Penalty Amount calculation 
			int penaltyMonths = 0;
			if (session.getAttribute("penalty_months") != null)
				penaltyMonths = Integer.parseInt(session.getAttribute("penalty_months").toString()) ;
			Cancellation cancel = cancellationService
					.getCancellationUseContract(contract);
			if (cancel != null && cancel.getIsPenalty() != null
					&& cancel.getIsPenalty()) {
				if (cancel.getIsRequested())
					penaltyAmount = (contractTotal / 12)
							* (penaltyMonths);
				else {
					penaltyAmount = (contractTotal / 12)
							* (penaltyMonths);
					penaltyAmount *= -1;
				}
				releaseTO.setContractTotal(contractTotal);
				releaseTO.setContractTotalStr(AIOSCommons
						.formatAmount(contractTotal));
				releaseTO.setPenaltyMonths(penaltyMonths);
			}
			releaseTO.setPenaltyAmount(penaltyAmount);
			releaseTO.setPenaltyAmountStr(AIOSCommons.formatAmount(Math
					.abs(penaltyAmount)));
			totalDeduction = damageTotal + otherChargers;

			// for printout
			session.setAttribute("AGREEMENT_DETAILS", agreementTO);
			session.setAttribute("AGREEMENT_TENANT_DETAILS", offerAgreementTO);
			session.setAttribute("AGREEMENT_OFFER_DETAILS", offerTOList);
			session.setAttribute(
					"CONTRACT_MAINTAIN_DETAILS_" + sessionObj.get("jSessionId"),
					releaseDetailsList);
			session.setAttribute("RELEASE_TRANSFER_SUBJECT", releaseType);
			session.setAttribute("RELEASE_TRANSFER_ACCOUNT", transferAccount);

			session.setAttribute("DAMAGE_TOTAL",
					AIOSCommons.formatAmount(damageTotal));
			session.setAttribute("OTHER_CHARGES",
					AIOSCommons.formatAmount(otherChargers));
			session.setAttribute("REFUND_AMOUNT",
					AIOSCommons.formatAmount(refundAmount));
			session.setAttribute("TOTAL_DEDUCTION",
					AIOSCommons.formatAmount(totalDeduction));
			session.setAttribute("REFUND_AMOUNT_WORDS",
					AIOSCommons.convertAmountToWords(refundAmount));

			session.setAttribute("OFFER_DETAIL_LIST", offerTOList);
			session.setAttribute("OFFER_INFO", offerAgreementTO);
			session.setAttribute("bean", agreementTO);
			session.setAttribute("RELEASE_DETAILS", releaseTOList);
			session.setAttribute("RELEASE_INFO", releaseTO);

			return SUCCESS;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return ERROR;
		}
	}

	private String getIdentityType(int code) throws Exception {

		switch (code) {
		case 1:
			return "Emirates ID";
		case 2:
			return "Labour Card";
		case 3:
			return "Passport";
		default:
			return "ID";
		}
	}

	@SuppressWarnings("unchecked")
	public String saveReleaseMaintainDetail() {
		try {
			HttpSession session = ServletActionContext.getRequest()
					.getSession();
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			List<ReleaseDetail> releaseDetailsList = null;
			ReleaseDetail releaseDetail = new ReleaseDetail();
			release = new Release();
			release.setReleaseId(releaseId);
			if (releaseDetailId != 0)
				releaseDetail.setReleaseDetailsId(releaseDetailId);
			releaseDetail.setRelease(release);
			releaseDetail.setAssetName(assetName);
			releaseDetail.setDamage(damage);
			releaseDetail.setAmount(balanceAmount);
			if (null == session.getAttribute("CONTRACT_MAINTAIN_DETAILS_"
					+ sessionObj.get("jSessionId"))) {
				releaseDetailsList = new ArrayList<ReleaseDetail>();
			} else {
				releaseDetailsList = (List<ReleaseDetail>) session
						.getAttribute("CONTRACT_MAINTAIN_DETAILS_"
								+ sessionObj.get("jSessionId"));
			}
			if (addEditFlag != null && addEditFlag.equals("A")) {
				LOGGER.info("Add Process");
				releaseDetailsList.add(releaseDetail);
			} else if (addEditFlag.equals("E")) {
				LOGGER.info("Edit Process");
				releaseDetailsList.set(Integer.parseInt(id) - 1, releaseDetail);
			} else if (addEditFlag.equals("D")) {
				LOGGER.info("Delete Process");
				releaseDetailsList.remove(Integer.parseInt(id) - 1);
			}
			session.setAttribute(
					"CONTRACT_MAINTAIN_DETAILS_" + sessionObj.get("jSessionId"),
					releaseDetailsList);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute("errMsg", "Error");
			return ERROR;
		}
	}

	@SuppressWarnings({ "unchecked" })
	public String finalSaveReleaseMaintainDetail() {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Person person = new Person();
		person.setPersonId(user.getPersonId());
		long documentRecId = -1;
		try {
			Release release = new Release();
			Release release1 = new Release();
			Contract contra = new Contract();
			releaseTO = new ContractReleaseTO();
			release.setReleaseId(releaseId);
			release1 = contractReleaseBL.getReleaseService().getReleaseInfo(
					releaseId);
			if (releaseId > 0) {
				release.setCreatedDate(release1.getCreatedDate());
				release.setPerson(release1.getPerson());
				release.setIsApprove(release1.getIsApprove());
			} else {
				release.setCreatedDate(new Date());
				release.setPerson(person);
				release.setIsApprove((byte) Constants.RealEstate.Status.Confirm
						.getCode());
			}
			contra.setContractId(contractId);
			release.setContract(contract);
			release.setIsInitialize(true);

			getImplementId();
			release.setImplementation(implementation);
			// Step 1
			if (release1 != null && release1.getCheckDamages() != null
					&& release1.getCheckDamages() == true) {
				release.setCheckDamages(checkDamage);
				release.setCheckWaterCert(waterCert);
				release.setCheckEnergyCert(electricityCert);
				release.setIsApprove(release1.getIsApprove());
				contractReleaseBL.saveWaterDocuments(release, true);
				contractReleaseBL.saveEnergyDocuments(release, true);
				documentRecId = releaseId;
			} else {
				release.setCheckDamages(true);
			}
			// step2
			if (checkDamage == true && release1.getCheckWaterCert() != null
					&& release1.getCheckWaterCert() == true
					&& release1.getCheckEnergyCert() != null
					&& release1.getCheckEnergyCert() == true) {
				release.setCheckPayment(true);
				release.setRefund(balanceAmount);
				release.setIsApprove(release1.getIsApprove());
				// Cancellation Update call
				releaseTO.setContractId(release1.getContract().getContractId());
				releaseTO.setPenaltyAmount(penaltyAmount);

				Cancellation cancel = cancellationService
						.getCancellationUseContract(release1.getContract());
				if (cancel != null)
					cancellationUpdate(releaseTO);

			}
			// Setp 3
			/*
			 * if(checkDamage==true && checkPayment==true &&
			 * release1.getPrintTaken()!=null &&
			 * release1.getPrintTaken()==false){
			 * release.setPrintTaken(printTaken); }else{
			 * release.setPrintTaken(false); }
			 */
			// Setp 4
			if (release1.getPrintTaken() != null
					&& release1.getPrintTaken() == true) {
				release.setPrintTaken(true);
				release.setCheckAddcCert(addcCert);
				if (effectiveDate != null && !effectiveDate.equals(""))
					release.setReleaseEffectDate(DateFormat
							.convertStringToDate(effectiveDate));
				else
					release.setReleaseEffectDate(new Date());
				contractReleaseBL.saveAddcDocuments(release, true);

				// Relase complete

				// Update the respective property/units status from RENTED
				// to OFFERED
				Contract contract = release.getContract();
				if (contract != null) {

					Offer offer = contract.getOffer();
					Property propertyToRelease;
					Component componentToRelease;
					Unit unitToRelease;

					if (offer != null) {

						List<OfferDetail> targetOffers = contractAgreementBL
								.getOfferService().getOfferDetailById(
										offer.getOfferId());

						for (OfferDetail detail : targetOffers) {

							if (detail.getProperty() != null) {

								propertyToRelease = contractAgreementBL
										.getPropertyService().getPropertyById(
												detail.getProperty()
														.getPropertyId());
								propertyToRelease.setStatus((byte) 1);
								contractAgreementBL.getPropertyService()
										.addProperty(propertyToRelease);
							} else if (detail.getComponent() != null) {

								componentToRelease = contractAgreementBL
										.getComponentService()
										.getComponentById(
												detail.getComponent()
														.getComponentId());
								componentToRelease.setStatus((byte) 1);
								contractAgreementBL.getComponentService()
										.savePropertyComponents(
												componentToRelease, null);
							} else if (detail.getUnit() != null) {

								unitToRelease = contractAgreementBL
										.getUnitService().getUnitInformation(
												detail.getUnit().getUnitId());
								unitToRelease.setStatus((byte) 1);
								contractAgreementBL.getUnitService().addUnits(
										unitToRelease, null, null);
							}
						}
					}
				}
			}

			List<ReleaseDetail> releaseDetailsList = null;

			releaseDetailsList = (ArrayList<ReleaseDetail>) session
					.getAttribute("CONTRACT_MAINTAIN_DETAILS_"
							+ sessionObj.get("jSessionId"));
			contractReleaseBL.saveRelease(release, releaseDetailsList);

			if (releaseId != 0)
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Succesfully Data Updated");
			else
				ServletActionContext.getRequest().setAttribute("succMsg",
						"Succesfully Data Added");
			return SUCCESS;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
			if (releaseId != 0)
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Data Update Failure");
			else
				ServletActionContext.getRequest().setAttribute("errMsg",
						"Data Add Failure");
			return SUCCESS;
		} finally {

			AIOSCommons.removeUploaderSession(session, "waterDocs",
					documentRecId, "Release");
			AIOSCommons.removeUploaderSession(session, "electricityDocs",
					documentRecId, "Release");
			AIOSCommons.removeUploaderSession(session, "addcDocs",
					documentRecId, "Release");
		}
	}

	public void cancellationUpdate(ContractReleaseTO releaseTO)
			throws Exception {

		Cancellation cancel = new Cancellation();
		Contract contract = new Contract();
		contract.setContractId(releaseTO.getContractId());
		cancel = cancellationService.getCancellationUseContract(contract);
		if (implementation.getIsWorkflow() != null
				&& implementation.getIsWorkflow()) {
			cancel.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.NoDecession
							.getCode() + ""));
		} else {
			cancel.setIsApprove(Byte
					.parseByte(Constants.RealEstate.Status.Confirm.getCode()
							+ ""));
		}
		cancellationService.cancelSave(cancel);

	}

	public String releasePrintUpdate() {
		try {
			Release release = new Release();
			release = contractReleaseBL.getReleaseService()
					.getReleaseFullInformation(releaseId);
			if (release != null
					&& (release.getPrintTaken() == null || release
							.getPrintTaken() == false)) {
				release.setPrintTaken(true);
				contractReleaseBL.releasePrintUpdate(release);

			}
			return SUCCESS;
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.info("Transaction fails " + ex);
			return ERROR;
		}
	}

	public String releaseDirectPayment() {

		LOGGER.info("Inside Module: Accounts : Method: releaseDirectPayment");
		try {
			String paymentNumber = "";
			if (recordId != 0)
				releaseId = (int) recordId;

			Release release = null;
			DirectPayment directPayment = null;

			List<Currency> currencyList = null;
			if (releaseId > 0) {
				release = contractReleaseBL.getReleaseService()
						.getReleaseFullInformation(releaseId);
				currencyList = this.getDirectPaymentBL().getTransactionBL()
						.getCurrencyService()
						.getAllCurrency(release.getImplementation());
				directPayment = new DirectPayment();

				// Direct Payment set process
				List<DirectPayment> paymentList = this.getDirectPaymentBL()
						.getDirectPaymentService()
						.getAllDirectPayments(release.getImplementation());
				if (null != paymentList && paymentList.size() > 0) {
					List<Long> directPaymentNumber = new ArrayList<Long>();
					for (DirectPayment list : paymentList) {
						directPaymentNumber.add(Long.parseLong(list
								.getPaymentNumber()));
					}
					paymentNumber = String.valueOf(Collections
							.max(directPaymentNumber));
					paymentNumber = String.valueOf(Long
							.parseLong(paymentNumber) + 1);
				} else {
					paymentNumber = "1000";
				}
				directPayment.setPaymentNumber(paymentNumber);
				directPayment.setRecordId(release.getReleaseId());
				directPayment.setUseCase(Release.class.getSimpleName());
				directPayment
						.setOthers(getCustomerDetail(release.getContract()));
				// Direct Payment details creation
				directPayment
						.setDirectPaymentDetails(new HashSet<DirectPaymentDetail>(
								getDirectPaymentDetailList(release)));

			}
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}

			ServletActionContext.getRequest().setAttribute("DIRECT_PAYMENT",
					directPayment);
			ServletActionContext.getRequest().setAttribute(
					"DIRECT_PAYMENT_DETAIL",
					directPayment.getDirectPaymentDetails());

			ServletActionContext.getRequest().setAttribute("CURRENCY_INFO",
					currencyList);
			ServletActionContext.getRequest().setAttribute("PAYMENT_MODE",
					directPaymentBL.getModeOfPayment());
			ServletActionContext.getRequest().setAttribute("PAYEE_TYPES",
					directPaymentBL.getReceiptSource());
			LOGGER.info("Module: Accounts : Method: releaseDirectPayment SUCCESS");
			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: releaseDirectPayment Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	// Tenant Account
	public String getCustomerDetail(Contract contract) throws Exception {
		tenentOffer = contractAgreementBL.getOfferService().getTenantById(
				contract.getOffer().getOfferId());
		if (tenentOffer.getPerson() != null) {
			Person person = contractAgreementBL.getOfferService()
					.getPersonDetails(tenentOffer.getPerson().getPersonId());
			return person.getFirstName().concat(" ")
					.concat(person.getLastName());
		} else {
			Company company = contractAgreementBL.getOfferService()
					.getCompanyDetails(tenentOffer.getCompany().getCompanyId());
			return company.getCompanyName();
		}
	}

	public List<DirectPaymentDetail> getDirectPaymentDetailList(Release release)
			throws Exception {

		List<DirectPaymentDetail> directPaymentDetails = new ArrayList<DirectPaymentDetail>();
		// Contract Payment
		List<ContractPayment> contractPayments = new ArrayList<ContractPayment>(
				release.getContract().getContractPayments());
		if (contractPayments != null) {
			DirectPaymentDetail directPaymentDetail = null;
			double offerMonths = DateFormat.offerMonthDifference(release
					.getContract().getOffer().getStartDate(), release
					.getContract().getOffer().getEndDate());
			double rentAmount = 0.0;
			for (ContractPayment list : contractPayments) {
				if ((list.getFeeType()).toString().equals("2")) {// Deposit
																	// Return
					directPaymentDetail = new DirectPaymentDetail();
					directPaymentDetail.setAmount(list.getAmount());
					Combination depositcombination = new Combination();
					depositcombination.setCombinationId(getImplementation()
							.getUnearnedRevenue());
					depositcombination = this
							.getDirectPaymentBL()
							.getTransactionBL()
							.getCombinationService()
							.getCombinationAllAccountDetail(
									depositcombination.getCombinationId());
					directPaymentDetail.setCombination(depositcombination);
					directPaymentDetails.add(directPaymentDetail);
				} else if ((list.getFeeType()).toString().equals("3")) {
					rentAmount += list.getAmount();
				}
			}
			double monthlyRental = rentAmount / offerMonths;
			int totalRentedMonths = DateFormat.monthDifference(
					release.getContract().getOffer().getStartDate(),
					(release.getReleaseEffectDate() != null ? release
							.getReleaseEffectDate() : new Date()));

			double totalRentAmount = totalRentedMonths * monthlyRental;

			// Cancellation penalty from Owner to tenant
			if (release.getContract().getCancellations() != null
					&& release.getContract().getCancellations().size() > 0) {
				Cancellation cancellation = new ArrayList<Cancellation>(release
						.getContract().getCancellations()).get(0);
				if (!cancellation.getIsRequested()
						&& cancellation.getIsPenalty()) {
					directPaymentDetail = new DirectPaymentDetail();
					directPaymentDetail.setAmount(cancellation
							.getPenaltyAmount());
					// Get Property Other Charges Account
					Property property = this.getContractAgreementBL()
							.getPropertyDetails(
									release.getContract().getContractId());
					Account otherChargesAccount = this
							.getContractAgreementBL()
							.getTransactionBL()
							.getCombinationService()
							.getAccountDetail(
									"Others " + property.getPropertyName(),
									release.getContract().getImplementation());
					Combination chargesCombination = contractAgreementBL
							.getTransactionBL()
							.getCombinationService()
							.getCombinationAllAccountDetail(
									otherChargesAccount.getAccountId());
					directPaymentDetail.setCombination(chargesCombination);
					directPaymentDetails.add(directPaymentDetail);
				}
			}

			double cashReturnRent = 0;
			double pdcReturn = 0;
			double returnRent = 0;
			for (ContractPayment contractPayment : contractPayments) {

				// Set the contract payment method by identifying the
				// information
				Byte paymentMethod = null;
				if (contractPayment.getChequeDate() == null
						&& contractPayment.getChequeNo() == null) {
					paymentMethod = 1;// Cash
				} else if (contractPayment.getChequeDate() != null
						&& contractPayment.getChequeNo() != null) {
					paymentMethod = 2;
				} else if (contractPayment.getChequeDate() != null
						&& contractPayment.getChequeNo() == null) {
					paymentMethod = 3;
				}

				if ((contractPayment.getFeeType().toString()).equals("3")) {// Rent
					if (paymentMethod == 1) {
						// Total amount should be greater then or equal to the
						// current payment
						if (totalRentAmount > 0) {
							Double tempAmount = (contractPayment.getAmount() - totalRentAmount);
							cashReturnRent += (tempAmount > 0 ? tempAmount : 0);
							totalRentAmount = Math
									.abs((tempAmount < 0 ? tempAmount : 0));
						} /*
						 * else { Double tempAmount =
						 * (contractPayment.getAmount() + totalRentAmount);
						 * cashReturnRent += (tempAmount > 0 ? tempAmount : 0);
						 * totalRentAmount = tempAmount; }
						 */
					} else {
						if (contractPayment.getIsPdc() != null
								&& contractPayment.getIsPdc()) {
							pdcReturn += contractPayment.getAmount();
						} else {

							if (totalRentAmount > 0) {
								Double tempAmount = (contractPayment
										.getAmount() - totalRentAmount);
								returnRent += (tempAmount > 0 ? tempAmount : 0);
								totalRentAmount = Math
										.abs((tempAmount < 0 ? tempAmount : 0));
							} /*
							 * else { Double tempAmount = (contractPayment
							 * .getAmount() + totalRentAmount); returnRent +=
							 * (tempAmount > 0 ? tempAmount : 0);
							 * totalRentAmount = tempAmount; }
							 */
						}

					}

				}
			}

			// Cash return
			if (cashReturnRent > 0) {
				directPaymentDetail = new DirectPaymentDetail();
				directPaymentDetail.setAmount(cashReturnRent);
				Combination depositcombination = new Combination();
				depositcombination.setCombinationId(getImplementation()
						.getUnearnedRevenue());
				depositcombination = this
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								depositcombination.getCombinationId());
				directPaymentDetail.setCombination(depositcombination);
				directPaymentDetails.add(directPaymentDetail);
			}

			// UnEarned Return
			if (returnRent > 0) {
				directPaymentDetail = new DirectPaymentDetail();
				directPaymentDetail.setAmount(returnRent);
				/*
				 * Property property = contractAgreementBL
				 * .getPropertyDetails(release.getContract() .getContractId());
				 * Account account = this .getDirectPaymentBL()
				 * .getTransactionBL() .getCombinationService()
				 * .getAccountDetail( "Rent Income " +
				 * property.getPropertyName(), release.getImplementation());
				 */
				Combination unEarnedcombination = this
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								getImplementation().getUnearnedRevenue());
				directPaymentDetail.setCombination(unEarnedcombination);
				directPaymentDetails.add(directPaymentDetail);
			}

			// PDC return amount
			if (pdcReturn > 0) {
				directPaymentDetail = new DirectPaymentDetail();
				directPaymentDetail.setAmount(pdcReturn);
				Combination depositcombination = new Combination();
				depositcombination.setCombinationId(getImplementation()
						.getPdcReceived());
				depositcombination = this
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								depositcombination.getCombinationId());
				directPaymentDetail.setCombination(depositcombination);
				directPaymentDetails.add(directPaymentDetail);
			}
		}
		return directPaymentDetails;
	}

	// show bank receipts entry
	public String releaseBankReceipt() {
		try {

			List<LookupDetail> receiptTypes = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("Receipt_Types", false);

			List<LookupDetail> payeeTypes = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("PAYEE_TYPE", false);

			List<LookupDetail> modeOfPaymetns = this.getBankReceiptsBL()
					.getLookupMasterBL()
					.getActiveLookupDetails("MODE_OF_PAYMENT", false);

			Calendar calendar = this
					.getBankReceiptsBL()
					.getTransactionBL()
					.getCalendarBL()
					.getCalendarService()
					.getAtiveCalendar(
							this.getBankReceiptsBL().getImplementation());
			Period period = this.getBankReceiptsBL().getTransactionBL()
					.getCalendarBL().getCalendarService()
					.getActivePeriod(calendar.getCalendarId());

			List<Currency> currencyList = this
					.getBankReceiptsBL()
					.getTransactionBL()
					.getCurrencyService()
					.getAllCurrency(
							this.getBankReceiptsBL().getImplementation());
			for (Currency list : currencyList) {
				if (list.getDefaultCurrency())
					ServletActionContext.getRequest().setAttribute(
							"DEFAULT_CURRENCY", list.getCurrencyId());
			}
			Release release = null;
			BankReceipts bankReceipts = null;
			if (recordId > 0) {
				release = contractReleaseBL.getReleaseService()
						.getReleaseFullInformation(recordId);
				bankReceipts = new BankReceipts();
				List<TenantOffer> tenantOffers = new ArrayList<TenantOffer>(
						release.getContract().getOffer().getTenantOffers());
				StringBuilder accountCode = new StringBuilder();
				if (tenantOffers.get(0).getPerson() != null) {
					accountCode.append(tenantOffers
							.get(0)
							.getPerson()
							.getFirstName()
							.trim()
							.concat(" ")
							.concat(tenentOffers.get(0).getPerson()
									.getLastName().trim()));
				} else {
					accountCode.append(tenantOffers.get(0).getCompany()
							.getCompanyName().trim());
				}

				// set Bank Receipt Master informations
				// Get Property Other Charges Account
				Property property = this.getContractAgreementBL()
						.getPropertyDetails(
								release.getContract().getContractId());
				Account otherIncomeAccount = this
						.getContractAgreementBL()
						.getTransactionBL()
						.getCombinationService()
						.getAccountDetail(
								"Other Income " + property.getPropertyName(),
								release.getContract().getImplementation());
				Combination otherIncomeCombination = contractAgreementBL
						.getTransactionBL()
						.getCombinationService()
						.getCombinationByAccount(
								otherIncomeAccount.getAccountId());
				otherIncomeCombination = this
						.getDirectPaymentBL()
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAllAccountDetail(
								otherIncomeCombination.getCombinationId());
				bankReceipts.setCombination(otherIncomeCombination);
				bankReceipts.setOthers(accountCode.toString());
				bankReceipts.setReferenceNo(release.getContract()
						.getContractNo());
				ServletActionContext.getRequest().setAttribute(
						"RENT_COMBINATION", otherIncomeCombination);
				// Identify the receipt type for contract
				for (LookupDetail ld : receiptTypes) {
					if (ld.getAccessCode().trim().equals("RER"))
						bankReceipts.setLookupDetail(ld);
				}
				double otherCharges = 0.0;
				double penaltyCharges = 0;
				List<BankReceiptsDetail> bankReceiptsDetails = new ArrayList<BankReceiptsDetail>();
				BankReceiptsDetail bankReceiptsDetail = null;
				if (release.getContract().getCancellations() != null
						&& release.getContract().getCancellations().size() > 0) {
					Cancellation cancellation = new ArrayList<Cancellation>(
							release.getContract().getCancellations()).get(0);
					if (cancellation.getIsRequested()
							&& cancellation.getIsPenalty()) {
						penaltyCharges += cancellation.getPenaltyAmount();
					}
				}

				// Damage charges
				Double releaseDamageCharges = 0.0;
				if (release.getReleaseDetails() != null
						&& release.getReleaseDetails().size() > 0) {
					for (ReleaseDetail releaseDetail : release
							.getReleaseDetails()) {
						releaseDamageCharges += releaseDetail.getAmount();
					}
					bankReceiptsDetail = new BankReceiptsDetail();
					bankReceiptsDetail.setRecordId(release.getReleaseId());
					bankReceiptsDetail
							.setUseCase(Release.class.getSimpleName());
					bankReceiptsDetail.setAmount(releaseDamageCharges);
					bankReceiptsDetail.setDescription("Damage Charges");
					bankReceiptsDetails.add(bankReceiptsDetail);
				}

				// Other charges calculation
				double depositAmount = 0.0;
				for (ContractPayment list : release.getContract()
						.getContractPayments()) {
					if ((list.getFeeType()).toString().equals("2"))// Deposit
						depositAmount += list.getAmount();
				}

				double tempAmount = (depositAmount - releaseDamageCharges);
				if (release.getRefund() >= 0)
					otherCharges += (Math.abs(tempAmount) - (release
							.getRefund() + penaltyCharges));
				else
					otherCharges += (Math.abs((Math.abs(tempAmount) - Math
							.abs(release.getRefund() + penaltyCharges))));
				// Final Other charges
				if (otherCharges > 0) {
					bankReceiptsDetail = new BankReceiptsDetail();
					bankReceiptsDetail.setRecordId(release.getReleaseId());
					bankReceiptsDetail
							.setUseCase(Release.class.getSimpleName());
					bankReceiptsDetail
							.setAmount((otherCharges + penaltyCharges));
					bankReceiptsDetail
							.setDescription("Other Charges + Penalty");
					bankReceiptsDetails.add(bankReceiptsDetail);
				}
				bankReceipts
						.setBankReceiptsDetails(new HashSet<BankReceiptsDetail>(
								bankReceiptsDetails));
			}
			ServletActionContext.getRequest().setAttribute("PERIOD_INFO",
					period.getStartTime());
			ServletActionContext.getRequest().setAttribute("MODE_OF_PAYMENT",
					modeOfPaymetns);
			ServletActionContext.getRequest().setAttribute(
					"PAYMENT_STATUS",
					this.getBankReceiptsBL().getLookupMasterBL()
							.getActiveLookupDetails("PAYMENT_STATUS", false));
			ServletActionContext.getRequest().setAttribute(
					"BANK_RECEIPTS_NO",
					SystemBL.getReferenceStamp(BankReceipts.class.getName(),
							bankReceiptsBL.getImplementation()));
			ServletActionContext.getRequest().setAttribute("RECEIPT_TYPES",
					receiptTypes);
			ServletActionContext.getRequest().setAttribute("CURRENCY_LIST",
					currencyList);
			ServletActionContext.getRequest().setAttribute("RECEIPT_SOURCE",
					payeeTypes);
			ServletActionContext.getRequest().setAttribute("BANK_RECEIPTS",
					bankReceipts);
			ServletActionContext.getRequest().setAttribute("RECEIPT_FROM",
					"RELEASE");

			return SUCCESS;
		} catch (Exception ex) {
			LOGGER.info("Module: Accounts : Method: showBankReceiptsEntry Exception "
					+ ex);
			ex.printStackTrace();
			return ERROR;
		}
	}

	public ReleaseService getReleaseService() {
		return releaseService;
	}

	public void setReleaseService(ReleaseService releaseService) {
		this.releaseService = releaseService;
	}

	public ContractReleaseBL getContractReleaseBL() {
		return contractReleaseBL;
	}

	public void setContractReleaseBL(ContractReleaseBL contractReleaseBL) {
		this.contractReleaseBL = contractReleaseBL;
	}

	public ContractAgreementBL getContractAgreementBL() {
		return contractAgreementBL;
	}

	public void setContractAgreementBL(ContractAgreementBL contractAgreementBL) {
		this.contractAgreementBL = contractAgreementBL;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}

	public long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(long releaseId) {
		this.releaseId = releaseId;
	}

	public long getReleaseDetailId() {
		return releaseDetailId;
	}

	public void setReleaseDetailId(long releaseDetailId) {
		this.releaseDetailId = releaseDetailId;
	}

	public String getInitializationFlag() {
		return initializationFlag;
	}

	public void setInitializationFlag(String initializationFlag) {
		this.initializationFlag = initializationFlag;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getDamage() {
		return damage;
	}

	public void setDamage(String damage) {
		this.damage = damage;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getAddEditFlag() {
		return addEditFlag;
	}

	public void setAddEditFlag(String addEditFlag) {
		this.addEditFlag = addEditFlag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isWaterCert() {
		return waterCert;
	}

	public void setWaterCert(boolean waterCert) {
		this.waterCert = waterCert;
	}

	public boolean isElectricityCert() {
		return electricityCert;
	}

	public void setElectricityCert(boolean electricityCert) {
		this.electricityCert = electricityCert;
	}

	public boolean isAddcCert() {
		return addcCert;
	}

	public void setAddcCert(boolean addcCert) {
		this.addcCert = addcCert;
	}

	public boolean isCheckPayment() {
		return checkPayment;
	}

	public void setCheckPayment(boolean checkPayment) {
		this.checkPayment = checkPayment;
	}

	public boolean isCheckDamage() {
		return checkDamage;
	}

	public void setCheckDamage(boolean checkDamage) {
		this.checkDamage = checkDamage;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public boolean isReleaseStatus() {
		return releaseStatus;
	}

	public void setReleaseStatus(boolean releaseStatus) {
		this.releaseStatus = releaseStatus;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	private List<ContractReleaseTO> componentDS;
	private String format;
	private ImageBL imageBL;
	private Map<String, Object> jasperRptParams = new HashMap<String, Object>();

	public List<ContractReleaseTO> getComponentDS() {
		return componentDS;
	}

	public void setComponentDS(List<ContractReleaseTO> componentDS) {
		this.componentDS = componentDS;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public Map<String, Object> getJasperRptParams() {
		return jasperRptParams;
	}

	public void setJasperRptParams(Map<String, Object> jasperRptParams) {
		this.jasperRptParams = jasperRptParams;
	}

	/*
	 * public String getReleaseDetail() { try{ componentDS = new
	 * ArrayList<ContractReleaseTO>();
	 * 
	 * Release rel = new Release(); if(recordId >0) releaseId=(int) recordId;
	 * rel.setReleaseId((long) releaseId); ContractReleaseTO to =
	 * contractReleaseBL.getReleaseListInfo(rel);
	 * 
	 * 
	 * to.setLinkLabel("HTML".equals(getFormat())? "Download" : "");
	 * to.setAnchorExpression("HTML".equals(getFormat())?
	 * "downloadPropReleaseDetail.action?releaseId="+ releaseId + "&format=PDF"
	 * : "#");
	 * //to.setAnchorExpression("downloadPropReleaseDetail.action?releaseId="+
	 * releaseId + "&format=PDF"); //Approve Content
	 * to.setApproveLinkLabel("Approve");
	 * to.setApproveAnchorExpression("workflowProcess.action?processFlag="
	 * +Byte.parseByte(Constants.RealEstate.Status.Approved.getCode()+""));
	 * 
	 * //Disapprove Content to.setRejectLinkLabel("Reject");
	 * to.setRejectAnchorExpression
	 * ("workflowProcess.action?processFlag="+Byte.parseByte
	 * (Constants.RealEstate.Status.Disapproved.getCode()+""));
	 * 
	 * componentDS.add(to); String ctxRealPath =
	 * ServletActionContext.getServletContext().getRealPath("/");
	 * jasperRptParams.put("AIOS_LOGO", "file:///" +
	 * ctxRealPath.concat(File.separator).concat("images" + File.separator +
	 * "aiotech.jpg")); if (componentDS!= null && componentDS.size() == 0){
	 * componentDS.add(to); }else{ componentDS.set(0, to); } }catch (Exception
	 * e){ e.printStackTrace(); } return SUCCESS; }
	 */
	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public boolean isPrintTaken() {
		return printTaken;
	}

	public void setPrintTaken(boolean printTaken) {
		this.printTaken = printTaken;
	}

	public CancellationService getCancellationService() {
		return cancellationService;
	}

	public void setCancellationService(CancellationService cancellationService) {
		this.cancellationService = cancellationService;
	}

	public double getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(double penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public String getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	public String getTransferAccount() {
		return transferAccount;
	}

	public void setTransferAccount(String transferAccount) {
		this.transferAccount = transferAccount;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public BankReceiptsBL getBankReceiptsBL() {
		return bankReceiptsBL;
	}

	public void setBankReceiptsBL(BankReceiptsBL bankReceiptsBL) {
		this.bankReceiptsBL = bankReceiptsBL;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

}
