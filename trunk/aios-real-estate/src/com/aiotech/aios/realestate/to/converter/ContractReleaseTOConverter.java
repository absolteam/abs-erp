package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;
import com.aiotech.aios.realestate.to.ContractReleaseTO;

public class ContractReleaseTOConverter {
	
	public static List<ContractReleaseTO> convertToTOList(
			List<Release> releases) throws Exception {

		List<ContractReleaseTO> tosList = new ArrayList<ContractReleaseTO>();

		for (Release release : releases) {
			tosList.add(convertToTO(release));
		}

		return tosList;
	}
	
	public static ContractReleaseTO convertToTO(Release release) throws Exception {
		ContractReleaseTO infoTO = new ContractReleaseTO();
		infoTO.setReleaseId(release.getReleaseId());
		infoTO.setContractId(release.getContract().getContractId());
		if(release.getIsInitialize()!=null)
			infoTO.setInitializationFlag(release.getIsInitialize().toString());
		
		if(release.getCheckWaterCert()!=null)
			infoTO.setWaterCert(release.getCheckWaterCert());
		
		if(release.getCheckEnergyCert()!=null)
			infoTO.setElectricityCert(release.getCheckEnergyCert());
		
		if(release.getCheckAddcCert()!=null)
			infoTO.setAddcCert(release.getCheckAddcCert());
		
		if(release.getCheckDamages()!=null)
			infoTO.setCheckDamage(release.getCheckDamages());
		
		if(release.getCheckPayment()!=null)
			infoTO.setCheckPayment(release.getCheckPayment());
		
		if(release.getRefund()!=null)
			infoTO.setBalanceAmount(release.getRefund());
		
		if(release.getPrintTaken()!=null)
			infoTO.setPrintTaken(release.getPrintTaken());
		
		if(release.getReleaseEffectDate()!=null)
			infoTO.setEffectiveDate(DateFormat.convertDateToString(release.getReleaseEffectDate().toString()));

		return infoTO;
	}

	
	public static List<ContractReleaseTO> convertToDetailTOList(
			List<ReleaseDetail> releaseDetails) throws Exception {

		List<ContractReleaseTO> tosList = new ArrayList<ContractReleaseTO>();

		for (ReleaseDetail releaseDetail : releaseDetails) {
			tosList.add(convertToDetailTO(releaseDetail));
		}

		return tosList;
	}
	
	public static ContractReleaseTO convertToDetailTO(ReleaseDetail releaseDetail) throws Exception {
		ContractReleaseTO infoTO = new ContractReleaseTO();
		infoTO.setReleaseId(releaseDetail.getRelease().getReleaseId());
		infoTO.setReleaseDetailId(releaseDetail.getReleaseDetailsId());
		infoTO.setAssetName(releaseDetail.getAssetName());
		infoTO.setDamage(releaseDetail.getDamage());
		infoTO.setBalanceAmount(releaseDetail.getAmount());
		return infoTO;
	}
}
