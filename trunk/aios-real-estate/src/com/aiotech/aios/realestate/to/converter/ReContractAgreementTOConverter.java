package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;

public class ReContractAgreementTOConverter {

	public static List<ReContractAgreementTO> convertToTOList(
			List<Contract> contracts) throws Exception {

		List<ReContractAgreementTO> tosList = new ArrayList<ReContractAgreementTO>();

		for (Contract contract : contracts) {
			tosList.add(convertToTO(contract));
		}

		return tosList;
	}
	
	public static ReContractAgreementTO convertToTO(Contract contract) throws Exception {
		ReContractAgreementTO infoTO = new ReContractAgreementTO();
		infoTO.setContractId(Integer.parseInt(contract.getContractId().toString()));
		infoTO.setContractNumber(contract.getContractNo());
		if(contract.getContractDate()!=null)
		infoTO.setContractDate(DateFormat.convertDateToString(contract.getContractDate().toString()));
		infoTO.setContractAmount(contract.getContractFee());
		infoTO.setDepositAmount(contract.getDeposit());
		infoTO.setNoOfCheques(contract.getInstallments());
		infoTO.setCreatedBy(contract.getPerson().getPersonId());
		infoTO.setCreatedDate(DateFormat.convertDateToString(contract.getCreatedDate().toString()));
		if(contract.getPaymentReceived()!=null)
		infoTO.setPaymentReceived(contract.getPaymentReceived());
		if(contract.getRemarks() != null)
			infoTO.setRemarks(contract.getRemarks());
		if(contract.getLeasedFor() != null)
			infoTO.setLeasedFor(contract.getLeasedFor());
		
		return infoTO;
	}
	//Contract Payment
	public static List<ReContractAgreementTO> convertToPaymentTOList(List<ContractPayment> contractPayments) throws Exception { 
		List<ReContractAgreementTO> tosList = new ArrayList<ReContractAgreementTO>(); 
		for (ContractPayment contractPayment : contractPayments) {
			tosList.add(convertToPaymentTO(contractPayment));
		} 
		return tosList;
	} 
	
	public static List<ReContractAgreementTO> convertToPaymentList(List<ContractPayment> contractPayments, Date financialPeriod) throws Exception { 
		List<ReContractAgreementTO> tosList = new ArrayList<ReContractAgreementTO>(); 
		for (ContractPayment contractPayment : contractPayments) {
			tosList.add(convertToPayment(contractPayment,financialPeriod));
		} 
		return tosList;
	} 
	
	public static ReContractAgreementTO convertToPaymentTO(ContractPayment contractPayment) throws Exception {
		ReContractAgreementTO infoTO = new ReContractAgreementTO();
		infoTO.setContractPaymentId(contractPayment.getContractPaymentId());
		if(contractPayment.getBankName()!=null)
			infoTO.setBankName(contractPayment.getBankName());
		else
			infoTO.setBankName("-NA-");
		if(contractPayment.getChequeNo()!=null && !contractPayment.getChequeNo().trim().equals(""))
			infoTO.setChequeNumber(contractPayment.getChequeNo());
		else
			infoTO.setChequeNumber("-NA-");
		if(contractPayment.getChequeDate()!=null)
			infoTO.setChequeDate(DateFormat.convertDateToString(contractPayment.getChequeDate().toString()));
		else
			infoTO.setChequeDate("-NA-");
		
		infoTO.setChequeAmount(contractPayment.getAmount()); 
		infoTO.setAttribute1(AIOSCommons.formatAmount(contractPayment.getAmount()));
		if(contractPayment.getPaymentStatus()!=null)
		infoTO.setPaymentStatus(contractPayment.getPaymentStatus().toString());
		infoTO.setPaymentVisibility(paymentStatusString(contractPayment.getPaymentStatus()));
		
		if(contractPayment.getBankName()!=null && contractPayment.getChequeDate()!=null){
			infoTO.setPaymentMethod("Cheque");
		}else if(contractPayment.getBankName()!=null && contractPayment.getChequeDate()==null){
			infoTO.setPaymentMethod("Direct Transfer");
		}else{
			infoTO.setPaymentMethod("Cash");
		}
		
		if(contractPayment.getFeeType()!=null){
			infoTO.setFeeTypeId(contractPayment.getFeeType());
			infoTO.setFeeType(Constants.RealEstate.ContractFeeType.get(contractPayment.getFeeType()).name());
		}
		
		infoTO.setIsDeposited(contractPayment.getIsDeposited());
		infoTO.setIsPdc(contractPayment.getIsPdc());
		
		return infoTO;
	}
	public static String paymentStatusString(Byte contractPayment) throws Exception {
		String str="";
		if(contractPayment!=null && contractPayment==1)
			str="Paid";
		else if(contractPayment!=null && contractPayment==2)
			str="Pending";
		else if(contractPayment!=null && contractPayment==3)
			str="Cancelled";
		else if(contractPayment!=null && contractPayment==-1)
			str="ALL";
		else
			str="Not Paid";
		return str;
	}
	
	public static ReContractAgreementTO convertToPayment(ContractPayment contractPayment, Date financialPeriod) throws Exception {
		ReContractAgreementTO infoTO = new ReContractAgreementTO();
		infoTO.setContractPaymentId(contractPayment.getContractPaymentId());
		infoTO.setBankName(contractPayment.getBankName());
		infoTO.setChequeNumber(contractPayment.getChequeNo());
		if(contractPayment.getChequeDate()!=null)
			infoTO.setChequeDate(DateFormat.convertDateToString(contractPayment.getChequeDate().toString()));
		infoTO.setChequeAmount(contractPayment.getAmount()); 
		infoTO.setAttribute1(AIOSCommons.formatAmount(contractPayment.getAmount()));
		infoTO.setFeeDescription(Constants.RealEstate.ContractFeeType.get(contractPayment.getFeeType()).toString());
		infoTO.setFeeId(contractPayment.getFeeType());
		if(contractPayment.getPaidDate()!=null)
			infoTO.setPaidDate(DateFormat.convertDateToString(contractPayment.getPaidDate().toString())); 
		if(null!=infoTO.getChequeDate() && !infoTO.getChequeDate().equals("")){
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(contractPayment.getChequeDate()); 
			double dayDifference=DateFormat.dayDifference(financialPeriod, calendar.getTime());   
			if(dayDifference>0){
				infoTO.setActionFlag(true);  
			}else{
				infoTO.setActionFlag(false); 
			}
		} 
		if(contractPayment.getPaymentStatus()!=null )
			infoTO.setPaymentStatus(contractPayment.getPaymentStatus().toString());

		if(contractPayment.getPaymentStatus()!=null && contractPayment.getPaymentStatus()!=1 ){
			infoTO.setPaymentVisibility("Y");
		}else if(contractPayment.getPaymentStatus()==null){
			infoTO.setPaymentVisibility("Y");
		}else{
			infoTO.setPaymentVisibility("N");
		}
		return infoTO;
	}
	
	//Offer
	public static List<ReContractAgreementTO> convertToOfferTOList(
			List<Offer> offers) throws Exception {

		List<ReContractAgreementTO> tosList = new ArrayList<ReContractAgreementTO>();

		for (Offer offer : offers) {
			tosList.add(convertToOfferTO(offer));
		}

		return tosList;
	}
	
	public static ReContractAgreementTO convertToOfferTO(Offer offer) throws Exception {
		ReContractAgreementTO infoTO = new ReContractAgreementTO();
		infoTO.setOfferId(Integer.parseInt(offer.getOfferId().toString()));
		infoTO.setOfferNumber(offer.getOfferNo());
		if(offer.getStartDate()!=null)
			infoTO.setFromDate(DateFormat.convertDateToString(offer.getStartDate().toString()));
		if(offer.getEndDate()!=null)
			infoTO.setToDate(DateFormat.convertDateToString(offer.getEndDate().toString()));
		if(offer.getDeposit() != null)
			infoTO.setDepositAmount(offer.getDeposit());
		return infoTO;
	}
	
	//Offer Detail
	public static List<ReContractAgreementTO> convertToOfferDetailTOList(
			List<OfferDetail> offerDetails) throws Exception {

		List<ReContractAgreementTO> tosList = new ArrayList<ReContractAgreementTO>();

		for (OfferDetail offerdetail : offerDetails) {
			tosList.add(convertToOfferDetailTO(offerdetail));
		}

		return tosList;
	}
	
	public static ReContractAgreementTO convertToOfferDetailTO(OfferDetail offerdetail) throws Exception {
		ReContractAgreementTO infoTO = new ReContractAgreementTO();
		infoTO.setOfferDetailId(Integer.parseInt(offerdetail.getOfferDetailId().toString()));
		infoTO.setOfferId(offerdetail.getOffer().getOfferId());
		if(offerdetail.getProperty()!=null)
		infoTO.setPropertyId(offerdetail.getProperty().getPropertyId());
		if(offerdetail.getComponent()!=null)
		infoTO.setComponentId(offerdetail.getComponent().getComponentId());
		if(offerdetail.getUnit()!=null)
		infoTO.setUnitId(offerdetail.getUnit().getUnitId());
		infoTO.setContractAmount(offerdetail.getOfferAmount());
		return infoTO;
	}
}
