package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.to.OfferManagementTO;

public class OfferTOConverter {

	public static List<OfferManagementTO> convertToTOList(
			List<Offer> offerEntityList) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (Offer offerEntity : offerEntityList) {
			offerList.add(converterEntityTO(offerEntity));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertPropertyTypeToTOList(
			List<PropertyType> propertyTypeList) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (PropertyType typeList : propertyTypeList) {
			offerList.add(converterTypeEntityTO(typeList));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertPropertyToTOList(
			List<Property> propertyList) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (Property typeList : propertyList) {
			offerList.add(converterPropertyEntityTO(typeList));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertStringTOList(
			List<String> componentList, String propertyName) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (String stringList : componentList) {
			offerList.add(converterStringTO(stringList, propertyName));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertComponentTOList(
			List<OfferDetail> detailList, Component component) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (OfferDetail list : detailList) {
			offerList.add(converterCompEntityTO(list, component));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertUnitsTOList(
			List<OfferDetail> detailList, Unit unit) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (OfferDetail list : detailList) {
			offerList.add(converterUnitsEntityTO(list, unit));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertStringUnitTOList(
			List<String> unitList, String propertyName) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (String stringList : unitList) {
			offerList.add(converterUnitStringTO(stringList, propertyName));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertTenantToTO(
			List<Person> personList) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (Person pList : personList) {
			offerList.add(convertPersonTO(pList));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertCompanyToTO(
			List<Company> companyList) {
		List<OfferManagementTO> offerList = new ArrayList<OfferManagementTO>();
		for (Company cList : companyList) {
			offerList.add(convertCompanyTO(cList));
		}
		return offerList;
	}

	public static List<OfferManagementTO> convertAmountStringTOList(
			List<String> amountString) {
		List<OfferManagementTO> offerAmountList = new ArrayList<OfferManagementTO>();
		for (String list : amountString) {
			offerAmountList.add(convertAmountStringList(list));
		}
		return offerAmountList;
	}

	public static List<OfferDetail> convertComponentEntity(
			List<OfferManagementTO> offerComponent) {
		List<OfferDetail> offerDetail = new ArrayList<OfferDetail>();
		for (OfferManagementTO list : offerComponent) {
			offerDetail.add(convertToOfferComponentDetailEntity(list));
		}
		return offerDetail;
	}

	public static List<OfferDetail> convertUnitEntity(
			List<OfferManagementTO> offerUnits) {
		List<OfferDetail> offerDetail = new ArrayList<OfferDetail>();
		for (OfferManagementTO list : offerUnits) {
			offerDetail.add(convertToOfferUnitDetailEntity(list));
		}
		return offerDetail;
	}

	public static OfferManagementTO converterEntityTO(Offer offerEntity) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setOfferId(offerEntity.getOfferId().intValue());
		offerTO.setOfferNumber(offerEntity.getOfferNo());
		offerTO.setOfferDate(DateFormat.convertDateToString(offerEntity
				.getOfferDate().toString()));
		offerTO.setValidityDays(offerEntity.getValidityDays());
		offerTO.setExtensionDays(offerEntity.getExtension());
		offerTO.setStartDate(DateFormat.convertDateToString(offerEntity
				.getStartDate().toString()));
		offerTO.setEndDate(DateFormat.convertDateToString(offerEntity
				.getEndDate().toString()));
		offerTO.setCreatedDate(DateFormat.convertDateToString(offerEntity
				.getCreatedDate().toString()));
		offerTO.setCreatedId(offerEntity.getPerson().getPersonId());
		offerTO.setStatus(Byte.parseByte("1"));
		return offerTO;
	}

	public static OfferManagementTO converterTypeEntityTO(
			PropertyType propertyType) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setPropertyTypeId(propertyType.getPropertyTypeId());
		offerTO.setPropertyType(propertyType.getType());
		return offerTO;
	}

	public static OfferManagementTO converterPropertyEntityTO(Property property) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setPropertyId(property.getPropertyId().intValue());
		offerTO.setPropertyName(property.getPropertyName());
		offerTO.setPropertyTypeId(property.getPropertyType()
				.getPropertyTypeId());
		offerTO.setPropertyType(property.getPropertyType().getType());
		if (property.getFloors() != null && !property.getFloors().equals(""))
			offerTO.setPropertyFloors(property.getFloors());
		else
			offerTO.setPropertyFloors(0);
		offerTO.setCityId(property.getCity());
		offerTO.setStateId(property.getState());
		offerTO.setCountryId(property.getCountry());
		return offerTO;
	}

	public static OfferManagementTO converterStringTO(String stringVal,
			String propertyName) {
		OfferManagementTO offerTO = new OfferManagementTO();
		String[] tempArray = stringVal.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for (int i = 0; i < tempArray.length; i++) {
			offerTO.setComponentId(Integer.parseInt(tempArray[0]));
			offerTO.setComponentName(tempArray[1] + " of " + propertyName);
			offerTO.setComponentType(tempArray[2]);
			if (tempArray[2] != null && !tempArray[3].equals("")
					&& !tempArray[3].equals("null")) {
				offerTO.setComponentRent(Double.parseDouble(tempArray[3]));
				offerTO.setOfferAmount(Double.parseDouble(tempArray[3]));
			}
		}
		return offerTO;
	}

	public static OfferManagementTO converterUnitStringTO(String stringVal,
			String propertyName) {
		OfferManagementTO offerTO = new OfferManagementTO();
		String[] tempArray = stringVal.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for (int i = 0; i < tempArray.length; i++) {
			offerTO.setUnitId(Integer.parseInt(tempArray[0]));
			offerTO.setUnitName(tempArray[1] + " of " + propertyName);
			offerTO.setUnitType(tempArray[2]);
			if (tempArray[3] != null && !tempArray[3].equals("")
					&& !tempArray[3].equals("null")) {
				offerTO.setUnitRent(Double.parseDouble(tempArray[3]));
				offerTO.setOfferAmount(Double.parseDouble(tempArray[3]));
			}
			/*
			 * else{ offerTO.setUnitRent(0); offerTO.setOfferAmount(0); }
			 */
		}
		return offerTO;
	}

	public static OfferManagementTO convertPersonTO(Person personList) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setTenantId(personList.getPersonId().intValue());
		offerTO.setTenantName(personList.getFirstName() + " "
				+ personList.getLastName());
		for (Identity list : personList.getIdentities()) {
			offerTO.setTenantNumber(list.getIdentityNumber());
		}
		offerTO.setAddressDetails("Tenant");
		return offerTO;
	}

	public static OfferManagementTO convertCompanyTO(Company company) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setTenantId(company.getCompanyId().intValue());
		offerTO.setTenantName(company.getCompanyName());
		offerTO.setTenantNumber(company.getTradeNo());
		offerTO.setAddressDetails("Company");
		return offerTO;
	}

	public static OfferManagementTO convertAmountStringList(String amountList) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setOfferAmount(Double.parseDouble(amountList.trim()));
		return offerTO;
	}

	public static OfferDetail convertToOfferComponentDetailEntity(
			OfferManagementTO offerComponent) {
		OfferDetail offerDetailTo = new OfferDetail();
		Component component = new Component();
		component.setComponentId(Long.parseLong(offerComponent.getComponentId()
				+ ""));
		component.setComponentName(offerComponent.getComponentName());
		component.setRent(offerComponent.getComponentRent());
		offerDetailTo.setComponent(component);
		return offerDetailTo;
	}

	public static OfferDetail convertToOfferUnitDetailEntity(
			OfferManagementTO offerUnits) {
		OfferDetail offerDetailTo = new OfferDetail();
		Unit unit = new Unit();
		unit.setUnitId(Long.parseLong(offerUnits.getUnitId() + ""));
		unit.setUnitName(offerUnits.getUnitName());
		unit.setRent(offerUnits.getUnitRent());
		offerDetailTo.setUnit(unit);
		return offerDetailTo;
	}

	public static OfferDetail convertToPropertyEntity(
			OfferManagementTO offerProperty) {
		OfferDetail offerDetailTo = new OfferDetail();
		Property property = new Property();
		property.setPropertyId(Long.parseLong(offerProperty.getPropertyId()
				+ ""));
		property.setPropertyName(offerProperty.getPropertyName());
		property.setRentYr(offerProperty.getPropertyRent());
		offerDetailTo.setProperty(property);
		return offerDetailTo;
	}

	public static OfferManagementTO convertOfferEntityTo(Offer offer,
			Person person, TenantOffer tenant) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setOfferId(offer.getOfferId().intValue());
		offerTO.setOfferNumber(offer.getOfferNo());
		offerTO.setExtensionDays(offer.getExtension());
		offerTO.setValidityDays(offer.getValidityDays());
		offerTO.setOfferDate(DateFormat.convertDateToString(offer
				.getOfferDate().toString()));
		offerTO.setStartDate(DateFormat.convertDateToString(offer
				.getStartDate().toString()));
		offerTO.setEndDate(DateFormat.convertDateToString(offer.getEndDate()
				.toString()));

		try {
			if (offer.getDeposit() == null) {

				if (tenant.getTenantGroup().getDeposit() < 1.0)
					offerTO.setSecurityDeposit("0");
				else
					offerTO.setSecurityDeposit(tenant.getTenantGroup()
							.getDeposit().toString().trim());
			} else
				offerTO.setSecurityDeposit(offer.getDeposit().toString().trim());

		} catch (Exception e) {
			offerTO.setSecurityDeposit("0");
			e.printStackTrace();
		}

		offerTO.setTenantOfferId(tenant.getTenantOfferId().intValue());
		offerTO.setTenantId(person.getPersonId().intValue());
		offerTO.setTenantName(person.getFirstName() + " "
				+ person.getMiddleName() + " " + person.getLastName());
		offerTO.setTenantNameArabic(AIOSCommons.bytesToUTFString(person
				.getFirstNameArabic())
				+ " "
				+ AIOSCommons.bytesToUTFString(person.getMiddleNameArabic())
				+ " "
				+ AIOSCommons.bytesToUTFString(person.getLastNameArabic()));
		offerTO.setAddress(person.getResidentialAddress() + "");
		if (person.getIdentities() != null && person.getIdentities().size() > 0) {
			for (Identity list : person.getIdentities())
				offerTO.setAddressDetails(list.getIdentityNumber());
		}
		offerTO.setCommonId("Tenant");
		return offerTO;
	}

	public static OfferManagementTO convertOfferEntityTo(Offer offer,
			Company company, TenantOffer tenant) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setOfferId(offer.getOfferId().intValue());
		offerTO.setOfferNumber(offer.getOfferNo());
		offerTO.setExtensionDays(offer.getExtension());
		offerTO.setValidityDays(offer.getValidityDays());
		offerTO.setOfferDate(DateFormat.convertDateToString(offer
				.getOfferDate().toString()));
		offerTO.setStartDate(DateFormat.convertDateToString(offer
				.getStartDate().toString()));
		offerTO.setEndDate(DateFormat.convertDateToString(offer.getEndDate()
				.toString()));

		try {
			if (offer.getDeposit() == null) {

				if (tenant.getTenantGroup().getDeposit() < 1.0)
					offerTO.setSecurityDeposit("0");
				else
					offerTO.setSecurityDeposit(tenant.getTenantGroup()
							.getDeposit().toString().trim());
			} else
				offerTO.setSecurityDeposit(offer.getDeposit().toString().trim());

		} catch (Exception e) {
			offerTO.setSecurityDeposit("0");
			e.printStackTrace();
		}

		offerTO.setTenantOfferId(tenant.getTenantOfferId().intValue());
		offerTO.setTenantId(company.getCompanyId().intValue());
		offerTO.setTenantName(company.getCompanyName());
		offerTO.setTenantNameArabic(AIOSCommons.bytesToUTFString(company
				.getCompanyNameArabic()));
		offerTO.setAddressDetails(company.getTradeNo());
		offerTO.setAddress(company.getCompanyAddress());
		offerTO.setCommonId("Company");
		return offerTO;
	}

	public static OfferManagementTO converterCompEntityTO(OfferDetail detail,
			Component component) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setComponentFlag(0);
		offerTO.setComponentId(detail.getComponent().getComponentId()
				.intValue());
		offerTO.setComponentName(detail.getComponent().getComponentName()
				+ " of " + component.getProperty().getPropertyName()); // +" of type "+component.getComponentType().getType()+", of the "+component.getProperty().getPropertyName());
		offerTO.setComponentType(component.getComponentType().getType());
		if (detail.getComponent().getRent() != null
				&& !detail.getComponent().getRent().equals(""))
			offerTO.setComponentRent(detail.getComponent().getRent());
		offerTO.setOfferAmount(detail.getOfferAmount());
		offerTO.setOfferLineId(detail.getOfferDetailId().intValue());
		offerTO.setPropertySize(detail.getComponent().getSize() + " "
				+ getSizeUnit(detail.getComponent().getSizeUnit()));
		return offerTO;
	}

	public static OfferManagementTO converterUnitsEntityTO(OfferDetail detail,
			Unit unit) {
		OfferManagementTO offerTO = new OfferManagementTO();
		offerTO.setComponentFlag(0);
		offerTO.setUnitId(detail.getUnit().getUnitId().intValue());
		offerTO.setUnitName(detail.getUnit().getUnitName() + " of "
				+ unit.getComponent().getProperty().getPropertyName());// +" of type "+detail.getUnit().getUnitType().getType()+", of the "+unit.getComponent().getProperty().getPropertyName());
		offerTO.setUnitType(detail.getUnit().getUnitType().getType());
		if (detail.getUnit().getRent() != null
				&& !detail.getUnit().getRent().equals(""))
			offerTO.setUnitRent(detail.getUnit().getRent());
		offerTO.setOfferAmount(detail.getOfferAmount());
		offerTO.setOfferLineId(detail.getOfferDetailId().intValue());
		try {
			if (detail.getUnit().getSizeUnit() != null
					&& detail.getUnit().getSize() != null)
				offerTO.setPropertySize(detail.getUnit().getSize() + " "
						+ getSizeUnit(detail.getUnit().getSizeUnit()));
		} catch (Exception e) {
			e.printStackTrace();
			offerTO.setPropertySize(detail.getUnit().getSize() + "");
		}
		try {
			if (detail.getUnit().getBedroomSize() != null)
				offerTO.setUnitBedrooms(detail.getUnit().getBedroomSize());
			else
				offerTO.setUnitBedrooms("--");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (detail.getUnit().getBathroomSize() != null)
				offerTO.setUnitBathrooms(detail.getUnit().getBathroomSize());
			else
				offerTO.setUnitBathrooms("--");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerTO;
	}

	private static String getSizeUnit(int code) {

		switch (code) {
		case 1:
			return "Square Feet";
		case 2:
			return "Square Meter";
		case 3:
			return "Square Yard";
		default:
			return "";
		}
	}
}
