package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;


public class PropertyCompomentTOConverter {

	public static List<PropertyCompomentTO> convertToTOList(List<Component> components) throws Exception {

		List<PropertyCompomentTO> tosList = new ArrayList<PropertyCompomentTO>(); 
		for (Component component : components) {
			tosList.add(convertToTO(component));
		} 
		return tosList;
	}
	
	public static List<PropertyCompomentTO> convertToTOPropertyList(List<Property> property) throws Exception {

		List<PropertyCompomentTO> tosList = new ArrayList<PropertyCompomentTO>(); 
		for (Property pros : property) {
			tosList.add(convertPropertyToTO(pros));
		} 
		return tosList;
	}

	public static List<PropertyCompomentTO> convertToTOTypeList(List<ComponentType> componentTypes) throws Exception {

		List<PropertyCompomentTO> tosList = new ArrayList<PropertyCompomentTO>(); 
		for (ComponentType componentType : componentTypes) {
			tosList.add(convertComponentTypeToTO(componentType));
		} 
		return tosList;
	}
	
	public static List<PropertyCompomentTO>convertDetailToTOList(List<ComponentDetail> componentList){
		List<PropertyCompomentTO> tosList = new ArrayList<PropertyCompomentTO>(); 
		for(ComponentDetail detail: componentList){
			tosList.add(convertDetailToTO(detail));
		}
		return tosList;
	}
	
	public static PropertyCompomentTO convertToTO(Component component){
		PropertyCompomentTO componentTO= new PropertyCompomentTO();
		componentTO.setComponentId(component.getComponentId().intValue());
		componentTO.setPropertyId(component.getProperty().getPropertyId().intValue());
		componentTO.setComponentTypeId(component.getComponentType().getComponentTypeId());
		componentTO.setComponentName(component.getComponentName());
		componentTO.setComponentNameArabic(AIOSCommons.bytesToUTFString(component.getComponentNameArabic()));
		componentTO.setPropertyName(component.getProperty().getPropertyName());
		componentTO.setComponentPropertyType(component.getProperty().getPropertyType().getPropertyTypeId().toString());
		componentTO.setComponentType(component.getComponentType().getType());
		componentTO.setComponentSize(component.getSize());
		componentTO.setUnitSize(component.getSizeUnit().intValue());
		componentTO.setRent(component.getRent());
		componentTO.setStatus(component.getStatus());
		componentTO.setIsApproved(component.getIsApprove());
		return componentTO;
	}
	
	public static PropertyCompomentTO convertPropertyToTO(Property property){
		PropertyCompomentTO componentTO= new PropertyCompomentTO();
		componentTO.setPropertyId(property.getPropertyId().intValue());
		componentTO.setPropertyName(property.getPropertyName());
		componentTO.setPropertySize(property.getSize().doubleValue());
		return componentTO;
	}
	
	public static PropertyCompomentTO convertComponentTypeToTO(ComponentType componentType){
		PropertyCompomentTO componentTO= new PropertyCompomentTO();
		componentTO.setComponentTypeId(componentType.getComponentTypeId());
		componentTO.setComponentType(componentType.getType());
		return componentTO;
	}
	
	public static PropertyCompomentTO convertEntityTOTo(Component component){
		PropertyCompomentTO componentTO= new PropertyCompomentTO();
		componentTO.setComponentId(component.getComponentId().intValue());
		componentTO.setComponentName(component.getComponentName());
		componentTO.setComponentNameArabic(AIOSCommons.bytesToUTFString(component.getComponentNameArabic()));
		componentTO.setComponentNumber(component.getComponentNo());
		componentTO.setComponentSize(component.getSize());
		componentTO.setUnitSize(component.getSizeUnit());
		componentTO.setRent(component.getRent());
		componentTO.setStatus(component.getStatus());
		componentTO.setPropertyId(component.getProperty().getPropertyId().intValue());
		componentTO.setPropertyName(component.getProperty().getPropertyName());
		componentTO.setPropertySize(component.getProperty().getSize());
		componentTO.setComponentTypeId(component.getComponentType().getComponentTypeId());
		componentTO.setComponentType(component.getComponentType().getType());
		componentTO.setNumberOfSize(component.getNoOfUnits());
		return componentTO;
	}
	
	public static PropertyCompomentTO convertDetailToTO(ComponentDetail component){
		PropertyCompomentTO componentTO= new PropertyCompomentTO();
		
		componentTO.setComponentLineId(component.getComponentDetailId().byteValue());
		componentTO.setComponentFeature(component.getLookupDetail().getDisplayName());
		componentTO.setComponentFeatureCode(component.getLookupDetail().getAccessCode());
		componentTO.setComponentFeatureDetails(component.getFeature());
		componentTO.setLookupDetailId(component.getLookupDetail().getLookupDetailId());
		/*String[] featureStrings = new String[3];
		featureStrings = component.getFeature().split("#");
		try {			
			if(featureStrings.length == 0) {
				componentTO.setComponentFeature(component.getFeature());
				componentTO.setComponentFeatureCode(component.getFeature());
				componentTO.setComponentFeatureDetails(component.getFeature());
			} else {
				componentTO.setComponentFeature(featureStrings[1]);
				componentTO.setComponentFeatureCode(featureStrings[0]);
				componentTO.setComponentFeatureDetails(featureStrings[2]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		return componentTO;
	}
}
