package com.aiotech.aios.realestate.to;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;

public class OfferManagementTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//Common Variables 
	private int offerId;
	private int offerNumber;
	private String offerDate;
	private int validityDays;
	private int extensionDays;
	private String startDate;
	private String endDate;
	private int propertyTypeId;
	private String propertyType;
	private int propertyId;
	private String propertyName;
	private int cityId;
	private String cityName;
	private int countryId;
	private String countryName;
	private int stateId;
	private String stateName;
	private List<OfferManagementTO> componentList;
	private List<OfferManagementTO> unitList;
	private String componentValue;
	private String unitValue;
	private int componentFlag;
	private String tenantName;
	private String tenantNameArabic;
	private int tenantId;
	private String addressDetails;
	private String contractTo;
	private String contractFrom;
	private double propertyRent;
	private double componentRent;
	private double unitRent;
	private int componentId;
	private String componentName;
	private int unitId;
	private String unitName; 
	private String tenantNumber; 
	private double offerAmount;
	private String offerFor;
	private int offerLineId;	
	private int tenantOfferId;
	private String tradeNumber;
	private String commonId;
	private byte status;
	private String componentType;
	private String unitType; 
	private String propertyTypes;
	private int propertyFloors;
	private boolean processStatus;
	private String createdDate;
	private long createdId;
	private String securityDeposit;
	private String propertySize;
	private String address;
	private String createdBy;
	private String details;
	private double totalOfferAmount;
	private String unitBedrooms;
	private String bankAccountTitle;
	private String bankAccountNumber;
	private String bankIBAN;
	private String bankName;
	private String bankBranch;
	private String bankCity;
	private String offerSubject;
	private String offerPrintOption1;
	
	public String getUnitBedrooms() {
		return unitBedrooms;
	}
	public void setUnitBedrooms(String unitBedrooms) {
		this.unitBedrooms = unitBedrooms;
	}
	public String getUnitBathrooms() {
		return unitBathrooms;
	}
	public void setUnitBathrooms(String unitBathrooms) {
		this.unitBathrooms = unitBathrooms;
	}
	private String unitBathrooms;
	
	//Jasper
	private Set<OfferDetail> offerDetails;
	private String linkLabel;
	private String anchorExpression;
	private List<OfferManagementTO> offerList;
	private List<RePropertyInfoTO> propertyList;
	private List<HrPersonalDetailsTO> tenantList;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private List<OfferManagementTO> offerInfo;
	private List<OfferManagementTO> offerDetailList;
	private List<OfferManagementTO> tenantInfo;
	private List<Property> property;
	public List<HrPersonalDetailsTO> getTenantList() {
		return tenantList;
	}
	public void setTenantList(List<HrPersonalDetailsTO> tenantList) {
		this.tenantList = tenantList;
	}
	public List<RePropertyInfoTO> getPropertyList() {
		return propertyList;
	}
	public void setPropertyList(List<RePropertyInfoTO> propertyList) {
		this.propertyList = propertyList;
	}
	public List<OfferManagementTO> getOfferList() {
		return offerList;
	}
	public void setOfferList(List<OfferManagementTO> offerList) {
		this.offerList = offerList;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public Set<OfferDetail> getOfferDetails() {
		return offerDetails;
	}
	public void setOfferDetails(Set<OfferDetail> offerDetails) {
		this.offerDetails = offerDetails;
	}
	//Getters&setters
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public int getOfferNumber() {
		return offerNumber;
	}
	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}
	public String getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}
	public int getValidityDays() {
		return validityDays;
	}
	public void setValidityDays(int validityDays) {
		this.validityDays = validityDays;
	}
	public int getExtensionDays() {
		return extensionDays;
	}
	public void setExtensionDays(int extensionDays) {
		this.extensionDays = extensionDays;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getPropertyTypeId() {
		return propertyTypeId;
	}
	public void setPropertyTypeId(int propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public int getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public int getComponentFlag() {
		return componentFlag;
	}
	public void setComponentFlag(int componentFlag) {
		this.componentFlag = componentFlag;
	}
	public String getComponentValue() {
		return componentValue;
	}
	public void setComponentValue(String componentValue) {
		this.componentValue = componentValue;
	}
	public String getUnitValue() {
		return unitValue;
	}
	public void setUnitValue(String unitValue) {
		this.unitValue = unitValue;
	}
	public List<OfferManagementTO> getComponentList() {
		return componentList;
	}
	public void setComponentList(List<OfferManagementTO> componentList) {
		this.componentList = componentList;
	}
	public List<OfferManagementTO> getUnitList() {
		return unitList;
	}
	public void setUnitList(List<OfferManagementTO> unitList) {
		this.unitList = unitList;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public int getTenantId() {
		return tenantId;
	}
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}
	public String getAddressDetails() {
		return addressDetails;
	}
	public void setAddressDetails(String addressDetails) {
		this.addressDetails = addressDetails;
	}
	public String getContractTo() {
		return contractTo;
	}
	public void setContractTo(String contractTo) {
		this.contractTo = contractTo;
	}
	public String getContractFrom() {
		return contractFrom;
	}
	public void setContractFrom(String contractFrom) {
		this.contractFrom = contractFrom;
	}
	public double getPropertyRent() {
		return propertyRent;
	}
	public void setPropertyRent(double propertyRent) {
		this.propertyRent = propertyRent;
	}
	public double getComponentRent() {
		return componentRent;
	}
	public void setComponentRent(double componentRent) {
		this.componentRent = componentRent;
	}
	public double getUnitRent() {
		return unitRent;
	}
	public void setUnitRent(double unitRent) {
		this.unitRent = unitRent;
	}
	public int getComponentId() {
		return componentId;
	}
	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public int getUnitId() {
		return unitId;
	}
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getTenantNumber() {
		return tenantNumber;
	}
	public void setTenantNumber(String tenantNumber) {
		this.tenantNumber = tenantNumber;
	}
	public double getOfferAmount() {
		return offerAmount;
	}
	public void setOfferAmount(double offerAmount) {
		this.offerAmount = offerAmount;
	}
	public String getOfferFor() {
		return offerFor;
	}
	public void setOfferFor(String offerFor) {
		this.offerFor = offerFor;
	}
	public int getOfferLineId() {
		return offerLineId;
	}
	public void setOfferLineId(int offerLineId) {
		this.offerLineId = offerLineId;
	}
	public int getTenantOfferId() {
		return tenantOfferId;
	}
	public void setTenantOfferId(int tenantOfferId) {
		this.tenantOfferId = tenantOfferId;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getTradeNumber() {
		return tradeNumber;
	}
	public void setTradeNumber(String tradeNumber) {
		this.tradeNumber = tradeNumber;
	}
	public String getCommonId() {
		return commonId;
	}
	public void setCommonId(String commonId) {
		this.commonId = commonId;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public List<OfferManagementTO> getOfferDetailList() {
		return offerDetailList;
	}
	public void setOfferDetailList(List<OfferManagementTO> offerDetailList) {
		this.offerDetailList = offerDetailList;
	}
	public List<OfferManagementTO> getOfferInfo() {
		return offerInfo;
	}
	public void setOfferInfo(List<OfferManagementTO> offerInfo) {
		this.offerInfo = offerInfo;
	}
	public List<OfferManagementTO> getTenantInfo() {
		return tenantInfo;
	}
	public void setTenantInfo(List<OfferManagementTO> tenantInfo) {
		this.tenantInfo = tenantInfo;
	}
	public List<Property> getProperty() {
		return property;
	}
	public void setProperty(List<Property> property) {
		this.property = property;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public String getComponentType() {
		return componentType;
	}
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getPropertyTypes() {
		return propertyTypes;
	}
	public void setPropertyTypes(String propertyTypes) {
		this.propertyTypes = propertyTypes;
	}
	public int getPropertyFloors() {
		return propertyFloors;
	}
	public void setPropertyFloors(int propertyFloors) {
		this.propertyFloors = propertyFloors;
	}
	public boolean isProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(boolean processStatus) {
		this.processStatus = processStatus;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public long getCreatedId() {
		return createdId;
	}
	public void setCreatedId(long createdId) {
		this.createdId = createdId;
	}
	public String getSecurityDeposit() {
		return securityDeposit;
	}
	public void setSecurityDeposit(String securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public String getPropertySize() {
		return propertySize;
	}
	public void setPropertySize(String propertySize) {
		this.propertySize = propertySize;
	}
	public String getTenantNameArabic() {
		return tenantNameArabic;
	}
	public void setTenantNameArabic(String tenantNameArabic) {
		this.tenantNameArabic = tenantNameArabic;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public double getTotalOfferAmount() {
		return totalOfferAmount;
	}
	public void setTotalOfferAmount(double totalOfferAmount) {
		this.totalOfferAmount = totalOfferAmount;
	}
	public String getBankAccountTitle() {
		return bankAccountTitle;
	}
	public void setBankAccountTitle(String bankAccountTitle) {
		this.bankAccountTitle = bankAccountTitle;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getBankIBAN() {
		return bankIBAN;
	}
	public void setBankIBAN(String bankIBAN) {
		this.bankIBAN = bankIBAN;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getBankCity() {
		return bankCity;
	}
	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}
	public String getOfferSubject() {
		return offerSubject;
	}
	public void setOfferSubject(String offerSubject) {
		this.offerSubject = offerSubject;
	}
	public String getOfferPrintOption1() {
		return offerPrintOption1;
	}
	public void setOfferPrintOption1(String offerPrintOption1) {
		this.offerPrintOption1 = offerPrintOption1;
	}
	
}
