package com.aiotech.aios.realestate.to;

public class PropertyOwnershipVO {

	private String ownerName;
	private String details;
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
}
