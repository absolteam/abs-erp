package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;

public class RePropertyInfoTOConverter {

	public static List<RePropertyInfoTO> convertToTOList(
			List<Property> properties) throws Exception {

		List<RePropertyInfoTO> tosList = new ArrayList<RePropertyInfoTO>();

		for (Property property : properties) {
			tosList.add(convertToTO(property));
		}

		return tosList;
	}
	
	public static RePropertyInfoTO convertDecisionToTO(ManagementDecision decision)throws Exception {
		RePropertyInfoTO infoTO = new RePropertyInfoTO();
		infoTO.setManagementDecision(decision.getDecisionDetail());
		infoTO.setManagementDecisionId(decision.getManagementDecisionId());
		return infoTO;
	}
	
	public static RePropertyInfoTO convertToTO(Property property) throws Exception {
		RePropertyInfoTO infoTO = new RePropertyInfoTO();
		infoTO.setId(property.getPropertyId().intValue());
		infoTO.setBuildingId(property.getPropertyId().intValue());
		infoTO.setPropertyTypeId(property.getPropertyType().getPropertyTypeId());
		infoTO.setPropertyTypeName(property.getPropertyName());
		infoTO.setBuildingName(property.getPropertyName());
		infoTO.setBuildingNameArabic(AIOSCommons.bytesToUTFString(property.getPropertyNameArabic()));
		infoTO.setPlotNo(property.getPlotNo());
		infoTO.setFloorNo((property.getFloors() == null) ? "" : property.getFloors().toString());
		infoTO.setAddressLine1(property.getAddressOne());
		infoTO.setAddressLine2(property.getAddressTwo());
		infoTO.setCountryCode(property.getCountry().toString());
		infoTO.setCountryId(property.getCountry());
		infoTO.setStateId(property.getState());
		infoTO.setCityId(property.getCity());
		infoTO.setPropStatus(property.getStatus());
		infoTO.setLandSize(property.getSize().toString());
		infoTO.setYearOfBuild(property.getYearOfBuild());
		infoTO.setPropertyTax(property.getTax());
		infoTO.setMarketValue(AIOSCommons.roundTwoDecimals(property.getCost()));
		infoTO.setRentPerYear(AIOSCommons.roundTwoDecimals(property.getRentYr()));
		infoTO.setMeasurementCode(property.getSizeUnit().toString());				
		infoTO.setStateName(property.getState().toString());
		infoTO.setCityName(property.getCity().toString());
		infoTO.setPropertyName(property.getPropertyName());		
		infoTO.setStatus(property.getStatus());
		infoTO.setSize(property.getSize());
		infoTO.setSizeUnit(property.getSizeUnit());
		infoTO.setCost(AIOSCommons.roundTwoDecimals(property.getCost()));
		infoTO.setAcMeter(property.getAcMeter());
		infoTO.setServiceMeter(property.getServiceMeter());
		infoTO.setBuildingSize(AIOSCommons.roundTwoDecimals(property.getBuildingSize()));
		infoTO.setBuildingSizeUnit(property.getBuildingSizeUnit());
		infoTO.setIsApprove(property.getIsApprove());
		infoTO.setFireMeter(property.getFireAlarmMeter());
		infoTO.setPropertyCode(property.getPropertyCode());
		infoTO.setPropertyCodeArabic(AIOSCommons.bytesToUTFString(property.getPropertyCodeArabic()));
		
		return infoTO;
	}

	public static Property convertToEntity(RePropertyInfoTO infoTO)
			throws Exception {

		Property property = new Property();
		
		if (infoTO.getBuildingId() != null && infoTO.getBuildingId() != 0)	
			property.setPropertyId((long)infoTO.getBuildingId());
		
		property.setPropertyType(new PropertyType());
		property.getPropertyType().setPropertyTypeId(infoTO.getPropertyTypeId());
		property.setPropertyName(infoTO.getBuildingName());
		property.setPropertyNameArabic(AIOSCommons.stringToUTFBytes(infoTO.getBuildingNameArabic()));
		property.setPlotNo(infoTO.getPlotNo());
		try {
			property.setFloors(Integer.parseInt(infoTO.getFloorNo()));
		} catch (Exception e) {
			// floors not applicable
		}
		property.setAddressOne(infoTO.getAddressLine1());
		property.setAddressTwo(infoTO.getAddressLine2());
		property.setCountry(Integer.parseInt(infoTO.getCountryCode()));
		property.setState(Integer.parseInt(infoTO.getStateName()));
		property.setCity(Integer.parseInt(infoTO.getCityName()));
		property.setStatus(infoTO.getPropStatus());
		property.setSize(Double.valueOf(infoTO.getLandSize())); 
		property.setSizeUnit(Integer.parseInt(infoTO.getMeasurementCode())); 
		property.setYearOfBuild(infoTO.getYearOfBuild());
		property.setTax(infoTO.getPropertyTax());
		if(infoTO.getMarketValue()!=null)
		property.setCost(Double.parseDouble(infoTO.getMarketValue()));
		if(infoTO.getRentPerYear()!=null)
		property.setRentYr(Double.parseDouble(infoTO.getRentPerYear()));
		property.setServiceMeter(infoTO.getServiceMeter());
		property.setAcMeter(infoTO.getAcMeter());
		if(infoTO.getBuildingSize()!=null)
		property.setBuildingSize(Double.valueOf(infoTO.getBuildingSize()));
		property.setBuildingSizeUnit(infoTO.getBuildingSizeUnit());
		property.setIsApprove(infoTO.getIsApprove());
		property.setFireAlarmMeter(infoTO.getFireMeter());
		property.setPropertyCode(infoTO.getPropertyCode());
		property.setPropertyCodeArabic(AIOSCommons.stringToUTFBytes(infoTO.getPropertyCodeArabic()));
		
		property.setPropertyOwnerships(infoTO.getPropertyOwnerships());
		property.setMaintenances(infoTO.getMaintenances());
		property.setPropertyDetails(infoTO.getPropertyDetails());
		
		return property;
	}
	
	public static String measurment(Integer unit){
		String str=null;
		if(unit!=null && unit==1)
			str="Square Feet";
		else if(unit==2)
			str="Square Meter";
		else if(unit==3)
			str="Square Meter";
		
		return str;
	}
}
