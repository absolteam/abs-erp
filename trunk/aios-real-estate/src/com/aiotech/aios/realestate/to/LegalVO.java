package com.aiotech.aios.realestate.to;

import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Legal;

public class LegalVO {
	private List<Legal> legals;
	private long legalId;
	private String legalDetail;
	private String createdDate;
	private String createdPerson;
	private long createdBy;
	private long recordId;
	private String recordName;
	private String tableName;
	private String effectiveDate;
	private Object object;
	private String recordNumber;
	public List<Legal> getLegals() {
		return legals;
	}

	public void setLegals(List<Legal> legals) {
		this.legals = legals;
	}

	public long getLegalId() {
		return legalId;
	}

	public void setLegalId(long legalId) {
		this.legalId = legalId;
	}

	public String getLegalDetail() {
		return legalDetail;
	}

	public void setLegalDetail(String legalDetail) {
		this.legalDetail = legalDetail;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedPerson() {
		return createdPerson;
	}

	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getRecordId() {
		return recordId;
	}

	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}

	public String getRecordName() {
		return recordName;
	}

	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

}
