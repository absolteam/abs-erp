package com.aiotech.aios.realestate.to;

/*******************************************************************************************
*
* Create Log
* -------------------------------------------------------------------------------------------
* Ver 		Created Date 	Created By                 	Description
* -------------------------------------------------------------------------------------------
* 1.0		10th May 2011 	Nagarajan T.	 		   	Initial Version
*********************************************************************************************/

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.mapping.Array;

import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Legal;
import com.aiotech.aios.realestate.domain.entity.Maintenance;
import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.vo.MaintenanceVO;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.to.ImageVO;

public class RePropertyInfoTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer buildingId;
	private int buildingNumber;
	private String buildingName;
	private String buildingNameArabic;

	private int propertyTypeId;
	private String propertyTypeName;
	private String propertyTypeCode;
	private String floorNo;
	private String plotNo;
	private String addressLine1;
	private String addressLine2;
	private String fromDate;
	private String toDate;
	private Integer yearOfBuild;
	private String countryCode;
	private int countryId;
	private String stateName;
	private int stateId;
	private String cityName;
	private int cityId;
	private String districtName;
	private int districtId;
	private String landSize;
	private String propertyTax;
	private String marketValue;
	private String image;
	private byte propStatus;
	private String propStatusCode;
	private int propStatusId;
	private String measurementCode;
	private String measurementValue;
	private String rentPerYear;
	private String cost;
	private String acMeter;
	private byte isApprove;
	private String maintainer;
	private String companyName;	
	private String exportLabel;
	private String exportAnchorExpression;
	private String fireMeter;
	private List<Legal> legallist;
	private ManagementDecision managementDecisions;
	private long managementDecisionId;
	private String managementDecision;

	private String propertyCode;
	private String propertyCodeArabic;
	private String statusStr;
	private String componentName;
	private String componentSize;
	private String componentCount;
	private long propertyId;
	private List<RePropertyInfoTO> propertyList;
	private String contractNumber;	
	private String tenantName;
	private String unitName;
	
	public String getFireMeter() {
		return fireMeter;
	}

	public void setFireMeter(String fireMeter) {
		this.fireMeter = fireMeter;
	}

	public String getExportAnchorExpression() {
		return exportAnchorExpression;
	}

	public void setExportAnchorExpression(String exportAnchorExpression) {
		this.exportAnchorExpression = exportAnchorExpression;
	}

	public String getExportLabel() {
		return exportLabel;
	}

	public void setExportLabel(String exportLabel) {
		this.exportLabel = exportLabel;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getMaintainer() {
		return maintainer;
	}

	public String getOwners() {
		return owners;
	}

	public void setMaintainer(String maintainer) {
		this.maintainer = maintainer;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	private String owners;
	private String ownersArabic;

	public String getOwnersArabic() {
		return ownersArabic;
	}

	public void setOwnersArabic(String ownersArabic) {
		this.ownersArabic = ownersArabic;
	}

	public byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(byte isApprove) {
		this.isApprove = isApprove;
	}

	private int propertyInfoOwnerId;
	private long ownerId;
	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	private String ownerName;
	private int ownerTypeId;
	private String ownerType;
	private double percentage;
	private String uaeNo;
	
	private int propertyInfoFeatureId;
	private int lineNumber;
	private int featureId;
	private String featureCode;
	private String features;
	private Long lookupDetailId;
	private String measurements;
	private String remarks;
	
	private int propertyComponentId;
	private int componentType;
	private String componentTypeName;
	private int noOfComponent;
	private int noOfFlatInComponent;
	private String totalArea;
	
	//workflow variables
	private int applicationId;
	private Long notificationId; 
	private String notificationType;
	private int companyId;
	private int workflowId;
	private String workflowName;
	private String workflowStatus;
	private int categoryId; 
	private int notifyFunctionId;
	private int functionId;
	private String wfFunctionType;
	private String workflowParam;	
	private int mappingId;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String requestReason;
	private int sessionPersonId;
	
	private int headerLedgerId;
	private int wfCategoryId;
	private String functionType;
	
	private String comments;
	
	private String approvedStatus;
	
	private Integer trnValue;
	private int sessionId;

	private int actualLineId;
	private String actionFlag; 
	private Integer tempLineId;
	
	private int headerFlag;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	

	private String sidx;
	private int count;
	private Integer id;
	
	private String buildingSize;
	private Integer buildingSizeUnit;
	private List<RePropertyInfoTO> defaultList;
	
	public String getBuildingSize() {
		return buildingSize;
	}

	public void setBuildingSize(String buildingSize) {
		this.buildingSize = buildingSize;
	}

	public Integer getBuildingSizeUnit() {
		return buildingSizeUnit;
	}

	public void setBuildingSizeUnit(Integer buildingSizeUnit) {
		this.buildingSizeUnit = buildingSizeUnit;
	}
	
	private List<Document> propertyDocuments;    
	
    private List<ImageVO> propertyImages;


	private List<PropertyOwnershipVO> propertyOwners; 
    private List<CompanyVO> maintenanceComps;
    private List<ImageVO> detailImgs;  
    private Set<PropertyDetail> detailList;
     
	public String getAcMeter() {
		return acMeter;
	}

	public void setAcMeter(String acMeter) {
		this.acMeter = acMeter;
	}

	public String getServiceMeter() {
		return serviceMeter;
	}

	public void setServiceMeter(String serviceMeter) {
		this.serviceMeter = serviceMeter;
	}

	private String serviceMeter;

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}
    
	public Set<PropertyDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(Set<PropertyDetail> detailList) {
		this.detailList = detailList;
	}

	public List<ImageVO> getPropertyImages() {
		return propertyImages;
	}
	
	public List<PropertyOwnershipVO> getPropertyOwners() {
		return propertyOwners;
	}

	public void setPropertyOwners(List<PropertyOwnershipVO> propertyOwners) {
		this.propertyOwners = propertyOwners;
	}

	public List<CompanyVO> getMaintenanceComps() {
		return maintenanceComps;
	}

	public void setMaintenanceComps(List<CompanyVO> maintenanceComps) {
		this.maintenanceComps = maintenanceComps;
	}

	public List<ImageVO> getDetailImgs() {
		return detailImgs;
	}

	public void setDetailImgs(List<ImageVO> detailImgs) {
		this.detailImgs = detailImgs;
	}

	public void setPropertyImages(List<ImageVO> propertyImages) {
		this.propertyImages = propertyImages;
	}

	private String propertyName;
    private String address;
    private byte status;
    private String anchorExpression;
    private String linkLabel;
    private double size;
	private int sizeUnit;
	
	private String approveAnchorExpression;
	private String approveLinkLabel;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public byte isStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public int getSizeUnit() {
		return sizeUnit;
	}

	public void setSizeUnit(int sizeUnit) {
		this.sizeUnit = sizeUnit;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public List<Document> getPropertyDocuments() {
		return propertyDocuments;
	}

	public void setPropertyDocuments(List<Document> propertyDocuments) {
		this.propertyDocuments = propertyDocuments;
	}


	

	// for new TO
	private Maintenance propertyMaintenance;
	private Set<OfferDetail> offerDetails = new HashSet<OfferDetail>(0);
    private Set<Maintenance> maintenances = new HashSet<Maintenance>(0);
    private Set<MaintenanceVO> maintenancesVO = new HashSet<MaintenanceVO>(0);
	private Set<PropertyDetail> propertyDetails = new HashSet<PropertyDetail>(0);
    private Set<Offer> offers = new HashSet<Offer>(0);
    private Set<PropertyOwnership> propertyOwnerships = new HashSet<PropertyOwnership>(0);
    private Set<Legal> legals = new HashSet<Legal>(0);
    private Set<Component> components = new HashSet<Component>(0);
	
	private List cell;

	public RePropertyInfoTO() {
		
	}

	public Set<MaintenanceVO> getMaintenancesVO() {
		return maintenancesVO;
	}

	public void setMaintenancesVO(Set<MaintenanceVO> maintenancesVO) {
		this.maintenancesVO = maintenancesVO;
	}
	
	public Set<OfferDetail> getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(Set<OfferDetail> offerDetails) {
		this.offerDetails = offerDetails;
	}

	public Set<Maintenance> getMaintenances() {
		return maintenances;
	}

	public void setMaintenances(Set<Maintenance> maintenances) {
		this.maintenances = maintenances;
	}

	public Set<PropertyDetail> getPropertyDetails() {
		return propertyDetails;
	}

	public void setPropertyDetails(Set<PropertyDetail> propertyDetails) {
		this.propertyDetails = propertyDetails;
	}

	public Set<Offer> getOffers() {
		return offers;
	}

	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}

	public Set<PropertyOwnership> getPropertyOwnerships() {
		return propertyOwnerships;
	}

	public void setPropertyOwnerships(Set<PropertyOwnership> propertyOwnerships) {
		this.propertyOwnerships = propertyOwnerships;
	}

	public Set<Legal> getLegals() {
		return legals;
	}

	public void setLegals(Set<Legal> legals) {
		this.legals = legals;
	}

	public Set<Component> getComponents() {
		return components;
	}

	public void setComponents(Set<Component> components) {
		this.components = components;
	}
	
	public Maintenance getPropertyMaintenance() {
		return propertyMaintenance;
	}

	public void setPropertyMaintenance(Maintenance propertyMaintenance) {
		this.propertyMaintenance = propertyMaintenance;
	}
	
	public Integer getTrnValue() {
		return trnValue;
	}

	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getActualLineId() {
		return actualLineId;
	}

	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public Integer getTempLineId() {
		return tempLineId;
	}

	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}

	public int getHeaderFlag() {
		return headerFlag;
	}

	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}

	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}

	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}

	public int getSqlErrorNum() {
		return sqlErrorNum;
	}

	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}

	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}

	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}

	public String getSqlProgramName() {
		return sqlProgramName;
	}

	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}

	public Integer getGridFrom() {
		return gridFrom;
	}

	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}

	public Integer getGridTo() {
		return gridTo;
	}

	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}

	public String getGridSord() {
		return gridSord;
	}

	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List getCell() {
		return cell;
	}

	public void setCell(List cell) {
		this.cell = cell;
	}

	public Integer getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getFloorNo() {
		return floorNo;
	}

	public void setFloorNo(String floorNo) {
		this.floorNo = floorNo;
	}

	public String getPlotNo() {
		return plotNo;
	}

	public void setPlotNo(String plotNo) {
		this.plotNo = plotNo;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public Integer getYearOfBuild() {
		return yearOfBuild;
	}

	public void setYearOfBuild(Integer yearOfBuild) {
		this.yearOfBuild = yearOfBuild;
	}


	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public int getDistrictId() {
		return districtId;
	}

	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}

	public String getLandSize() {
		return landSize;
	}

	public void setLandSize(String landSize) {
		this.landSize = landSize;
	}

	public String getPropertyTax() {
		return propertyTax;
	}

	public void setPropertyTax(String propertyTax) {
		this.propertyTax = propertyTax;
	}

	public String getMarketValue() {
		return marketValue;
	}

	public void setMarketValue(String marketValue) {
		this.marketValue = marketValue;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPropertyInfoOwnerId() {
		return propertyInfoOwnerId;
	}

	public void setPropertyInfoOwnerId(int propertyInfoOwnerId) {
		this.propertyInfoOwnerId = propertyInfoOwnerId;
	}

	
	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public String getUaeNo() {
		return uaeNo;
	}

	public void setUaeNo(String uaeNo) {
		this.uaeNo = uaeNo;
	}

	public int getPropertyInfoFeatureId() {
		return propertyInfoFeatureId;
	}

	public void setPropertyInfoFeatureId(int propertyInfoFeatureId) {
		this.propertyInfoFeatureId = propertyInfoFeatureId;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getMeasurements() {
		return measurements;
	}

	public void setMeasurements(String measurements) {
		this.measurements = measurements;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public int getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(int propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public String getPropertyTypeName() {
		return propertyTypeName;
	}

	public void setPropertyTypeName(String propertyTypeName) {
		this.propertyTypeName = propertyTypeName;
	}

	public byte getPropStatus() {
		return propStatus;
	}

	public void setPropStatus(byte propStatus) {
		this.propStatus = propStatus;
	}

	public int getPropStatusId() {
		return propStatusId;
	}

	public void setPropStatusId(int propStatusId) {
		this.propStatusId = propStatusId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public int getOwnerTypeId() {
		return ownerTypeId;
	}

	public void setOwnerTypeId(int ownerTypeId) {
		this.ownerTypeId = ownerTypeId;
	}

	public int getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public int getFeatureId() {
		return featureId;
	}

	public void setFeatureId(int featureId) {
		this.featureId = featureId;
	}

	public int getPropertyComponentId() {
		return propertyComponentId;
	}

	public void setPropertyComponentId(int propertyComponentId) {
		this.propertyComponentId = propertyComponentId;
	}

	public int getComponentType() {
		return componentType;
	}

	public void setComponentType(int componentType) {
		this.componentType = componentType;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	public int getNoOfComponent() {
		return noOfComponent;
	}

	public void setNoOfComponent(int noOfComponent) {
		this.noOfComponent = noOfComponent;
	}

	public int getNoOfFlatInComponent() {
		return noOfFlatInComponent;
	}

	public void setNoOfFlatInComponent(int noOfFlatInComponent) {
		this.noOfFlatInComponent = noOfFlatInComponent;
	}

	public String getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(String totalArea) {
		this.totalArea = totalArea;
	}

	public String getPropertyTypeCode() {
		return propertyTypeCode;
	}

	public void setPropertyTypeCode(String propertyTypeCode) {
		this.propertyTypeCode = propertyTypeCode;
	}

	public String getPropStatusCode() {
		return propStatusCode;
	}

	public void setPropStatusCode(String propStatusCode) {
		this.propStatusCode = propStatusCode;
	}

	public String getMeasurementCode() {
		return measurementCode;
	}

	public void setMeasurementCode(String measurementCode) {
		this.measurementCode = measurementCode;
	}

	public String getMeasurementValue() {
		return measurementValue;
	}

	public void setMeasurementValue(String measurementValue) {
		this.measurementValue = measurementValue;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getNotifyFunctionId() {
		return notifyFunctionId;
	}

	public void setNotifyFunctionId(int notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	public String getWfFunctionType() {
		return wfFunctionType;
	}

	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}

	public String getWorkflowParam() {
		return workflowParam;
	}

	public void setWorkflowParam(String workflowParam) {
		this.workflowParam = workflowParam;
	}

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getRequestReason() {
		return requestReason;
	}

	public void setRequestReason(String requestReason) {
		this.requestReason = requestReason;
	}

	public int getSessionPersonId() {
		return sessionPersonId;
	}

	public void setSessionPersonId(int sessionPersonId) {
		this.sessionPersonId = sessionPersonId;
	}

	public int getHeaderLedgerId() {
		return headerLedgerId;
	}

	public void setHeaderLedgerId(int headerLedgerId) {
		this.headerLedgerId = headerLedgerId;
	}

	public int getWfCategoryId() {
		return wfCategoryId;
	}

	public void setWfCategoryId(int wfCategoryId) {
		this.wfCategoryId = wfCategoryId;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getApprovedStatus() {
		return approvedStatus;
	}

	public void setApprovedStatus(String approvedStatus) {
		this.approvedStatus = approvedStatus;
	}
	
	public String getRentPerYear() {
		return rentPerYear;
	}

	public void setRentPerYear(String rentPerYear) {
		this.rentPerYear = rentPerYear;
	}

	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}

	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}

	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}

	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}

	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}

	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}

	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}

	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}

	public List<RePropertyInfoTO> getDefaultList() {
		return defaultList;
	}

	public void setDefaultList(List<RePropertyInfoTO> defaultList) {
		this.defaultList = defaultList;
	}
	public List<Legal> getLegallist() {
		return legallist;
	}

	public void setLegallist(List<Legal> legallist) {
		this.legallist = legallist;
	}

	public long getManagementDecisionId() {
		return managementDecisionId;
	}

	public void setManagementDecisionId(long managementDecisionId) {
		this.managementDecisionId = managementDecisionId;
	}

	public String getManagementDecision() {
		return managementDecision;
	}

	public void setManagementDecision(String managementDecision) {
		this.managementDecision = managementDecision;
	}

	public ManagementDecision getManagementDecisions() {
		return managementDecisions;
	}

	public void setManagementDecisions(ManagementDecision managementDecisions) {
		this.managementDecisions = managementDecisions;
	}
	
	public String getPropertyCode() {
		return propertyCode;
	}

	public String getPropertyCodeArabic() {
		return propertyCodeArabic;
	}

	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}

	public void setPropertyCodeArabic(String propertyCodeArabic) {
		this.propertyCodeArabic = propertyCodeArabic;
	}
	
	public String getBuildingNameArabic() {
		return buildingNameArabic;
	}

	public void setBuildingNameArabic(String buildingNameArabic) {
		this.buildingNameArabic = buildingNameArabic;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentSize() {
		return componentSize;
	}

	public void setComponentSize(String componentSize) {
		this.componentSize = componentSize;
	}

	public String getComponentCount() {
		return componentCount;
	}

	public void setComponentCount(String componentCount) {
		this.componentCount = componentCount;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public List<RePropertyInfoTO> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<RePropertyInfoTO> propertyList) {
		this.propertyList = propertyList;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}

	
	
}
