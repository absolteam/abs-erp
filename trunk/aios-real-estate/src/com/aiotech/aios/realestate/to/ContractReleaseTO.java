package com.aiotech.aios.realestate.to;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;

public class ContractReleaseTO implements Serializable {


	// Master Details Variables
	private long offerId;
	private int offerNumber;
	private int tenantId;
	private String tenantName;
	private String presentAddress;
	private String permanantAddress;
	
	private long contractId;
	private String contractNumber;
	private String contractDate;
	private int tenantTypeId;
	private String tenantTypeName;
	private String paymentMethod;
	
	private String fromDate;
	private String toDate;
	
	private double cashAmount;
	private double chequeAmount;
	private double grandTotal;
	
	private int rentFees;
	private double depositAmount;
	
	// Rent Details Variables
	private int offerDetailId;
	private int contractRentId;
	private int lineNumber;
	private String flatNumber;
	private double rentAmount;
	private double contractAmount;
	private double balance;
	private double balanceAmount;
	private int noOfDays;
	private int propertyFlatId;
	private long propertyId;
	private long componentId;
	private long unitId;
	private String propertyName;
	private String componentName;
	private String unitName;
	private String depositAmountStr;
	private String otherChargesStr;
	private String totalDeductionStr;
	private String refundAmountStr;
	private String damageTotalStr;
	private String refundAmountInWords;
	// Fee Details
	private int contractFeeId;
	private int feeId;
	private String feeDescription;
	private double feeAmount;
	
	//Payment Details
	private long contractPaymentId;
	private String bankName;
	private String chequeDate;
	private String chequeNumber;
	private long releaseId;
	private long releaseDetailId;
	private String initializationFlag;
	private String assetName;
	private String damage;
	private String id;
	private boolean waterCert;
	private boolean electricityCert;
	private boolean addcCert;
	private boolean checkPayment;
	private boolean checkDamage;
	private boolean printTaken;
	private double penaltyAmount;
	private String penaltyAmountStr;
	private int penaltyMonths;
	private double contractTotal;
	private String contractTotalStr;
	private String effectiveDate;
	// Renewal Details
	
	
	// Jasper
	
	private List<RePropertyInfoTO> propertyList;
	private List<HrPersonalDetailsTO> tenantList; 
	private List<ReContractAgreementTO> tenantInfo;
	private String linkLabel;
	private String anchorExpression;
	private List<Contract> contractList;
	private Set<ReleaseDetail> releaseDetails;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	public Set<ReleaseDetail> getReleaseDetails() {
		return releaseDetails;
	}

	public void setReleaseDetails(Set<ReleaseDetail> releaseDetails) {
		this.releaseDetails = releaseDetails;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public List<RePropertyInfoTO> getPropertyList() {
		return propertyList;
	}

	public List<HrPersonalDetailsTO> getTenantList() {
		return tenantList;
	}

	public void setTenantList(List<HrPersonalDetailsTO> tenantList) {
		this.tenantList = tenantList;
	}

	public void setPropertyList(List<RePropertyInfoTO> propertyList) {
		this.propertyList = propertyList;
	}

	private List cell;

	

	public List getCell() {
		return cell;
	}

	public void setCell(List cell) {
		this.cell = cell;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanantAddress() {
		return permanantAddress;
	}

	public void setPermanantAddress(String permanantAddress) {
		this.permanantAddress = permanantAddress;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public int getTenantTypeId() {
		return tenantTypeId;
	}

	public void setTenantTypeId(int tenantTypeId) {
		this.tenantTypeId = tenantTypeId;
	}

	public String getTenantTypeName() {
		return tenantTypeName;
	}

	public void setTenantTypeName(String tenantTypeName) {
		this.tenantTypeName = tenantTypeName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public double getRentAmount() {
		return rentAmount;
	}

	public void setRentAmount(double rentAmount) {
		this.rentAmount = rentAmount;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public int getFeeId() {
		return feeId;
	}

	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	public String getFeeDescription() {
		return feeDescription;
	}

	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public int getOfferNumber() {
		return offerNumber;
	}

	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}

	public int getContractRentId() {
		return contractRentId;
	}

	public void setContractRentId(int contractRentId) {
		this.contractRentId = contractRentId;
	}

	public int getContractFeeId() {
		return contractFeeId;
	}

	public void setContractFeeId(int contractFeeId) {
		this.contractFeeId = contractFeeId;
	}

	public long getContractPaymentId() {
		return contractPaymentId;
	}

	public void setContractPaymentId(long contractPaymentId) {
		this.contractPaymentId = contractPaymentId;
	}

	public int getOfferDetailId() {
		return offerDetailId;
	}

	public void setOfferDetailId(int offerDetailId) {
		this.offerDetailId = offerDetailId;
	}

	

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public int getPropertyFlatId() {
		return propertyFlatId;
	}

	public void setPropertyFlatId(int propertyFlatId) {
		this.propertyFlatId = propertyFlatId;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public int getRentFees() {
		return rentFees;
	}

	public void setRentFees(int rentFees) {
		this.rentFees = rentFees;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}
	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public long getComponentId() {
		return componentId;
	}

	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(long releaseId) {
		this.releaseId = releaseId;
	}

	public long getReleaseDetailId() {
		return releaseDetailId;
	}

	public void setReleaseDetailId(long releaseDetailId) {
		this.releaseDetailId = releaseDetailId;
	}

	public String getInitializationFlag() {
		return initializationFlag;
	}

	public void setInitializationFlag(String initializationFlag) {
		this.initializationFlag = initializationFlag;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getDamage() {
		return damage;
	}

	public void setDamage(String damage) {
		this.damage = damage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isWaterCert() {
		return waterCert;
	}

	public void setWaterCert(boolean waterCert) {
		this.waterCert = waterCert;
	}

	public boolean isElectricityCert() {
		return electricityCert;
	}

	public void setElectricityCert(boolean electricityCert) {
		this.electricityCert = electricityCert;
	}

	public boolean isAddcCert() {
		return addcCert;
	}

	public void setAddcCert(boolean addcCert) {
		this.addcCert = addcCert;
	}

	public boolean isCheckPayment() {
		return checkPayment;
	}

	public void setCheckPayment(boolean checkPayment) {
		this.checkPayment = checkPayment;
	}

	public boolean isCheckDamage() {
		return checkDamage;
	}

	public void setCheckDamage(boolean checkDamage) {
		this.checkDamage = checkDamage;
	}

	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}

	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}

	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}

	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}

	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}

	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}

	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}

	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}

	public String getDepositAmountStr() {
		return depositAmountStr;
	}

	public void setDepositAmountStr(String depositAmountStr) {
		this.depositAmountStr = depositAmountStr;
	}

	public String getOtherChargesStr() {
		return otherChargesStr;
	}

	public void setOtherChargesStr(String otherChargesStr) {
		this.otherChargesStr = otherChargesStr;
	}

	public String getTotalDeductionStr() {
		return totalDeductionStr;
	}

	public void setTotalDeductionStr(String totalDeductionStr) {
		this.totalDeductionStr = totalDeductionStr;
	}

	public String getRefundAmountStr() {
		return refundAmountStr;
	}

	public void setRefundAmountStr(String refundAmountStr) {
		this.refundAmountStr = refundAmountStr;
	}

	public String getDamageTotalStr() {
		return damageTotalStr;
	}

	public void setDamageTotalStr(String damageTotalStr) {
		this.damageTotalStr = damageTotalStr;
	}

	public String getRefundAmountInWords() {
		return refundAmountInWords;
	}

	public void setRefundAmountInWords(String refundAmountInWords) {
		this.refundAmountInWords = refundAmountInWords;
	}

	public List<ReContractAgreementTO> getTenantInfo() {
		return tenantInfo;
	}

	public void setTenantInfo(List<ReContractAgreementTO> tenantInfo) {
		this.tenantInfo = tenantInfo;
	}

	public boolean isPrintTaken() {
		return printTaken;
	}

	public void setPrintTaken(boolean printTaken) {
		this.printTaken = printTaken;
	}

	public double getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(double penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public int getPenaltyMonths() {
		return penaltyMonths;
	}

	public void setPenaltyMonths(int penaltyMonths) {
		this.penaltyMonths = penaltyMonths;
	}

	public double getContractTotal() {
		return contractTotal;
	}

	public void setContractTotal(double contractTotal) {
		this.contractTotal = contractTotal;
	}

	public String getContractTotalStr() {
		return contractTotalStr;
	}

	public void setContractTotalStr(String contractTotalStr) {
		this.contractTotalStr = contractTotalStr;
	}

	public String getPenaltyAmountStr() {
		return penaltyAmountStr;
	}

	public void setPenaltyAmountStr(String penaltyAmountStr) {
		this.penaltyAmountStr = penaltyAmountStr;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	
	

}
