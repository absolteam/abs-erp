package com.aiotech.aios.realestate.to;

import java.io.Serializable; 
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.system.to.ImageVO;

public class PropertyCompomentTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//Common Variables
	private Component component;
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	private int componentId;
	private int propertyId;
	private int componentTypeId;
	private String componentName;
	private String componentNameArabic;
	private double componentSize;
	private int unitSize;
	private int componentNumber;
	private double rent;
	
	private byte status;
	private String propertyName;
	private String componentType;
	private int componentLineId;
	private int componentFeatureId;
	private String componentFeature;
	private String componentFeatureDetails;
	private String componentFeatureCode; 
	private String linkLabel;
	private String anchorExpression;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private String componentPropertyType;
	private Byte isApproved;
	private int numberOfSize;
	private double propertySize;
	private Long lookupDetailId;
	// jasper data source;
	
	public double getPropertySize() {
		return propertySize;
	}
	public void setPropertySize(double propertySize) {
		this.propertySize = propertySize;
	}
	public Byte getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}
	public String getComponentPropertyType() {
		return componentPropertyType;
	}
	public void setComponentPropertyType(String componentPropertyType) {
		this.componentPropertyType = componentPropertyType;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	
	private List<ComponentDetail> componentDetails;
	private List<ImageVO> compImgs;
	private List<ImageVO> compDetailImgs;
	private List<PropertyCompomentTO> compDetailTOList;
	
	public List<PropertyCompomentTO> getCompDetailTOList() {
		return compDetailTOList;
	}
	public void setCompDetailTOList(List<PropertyCompomentTO> compDetailTOList) {
		this.compDetailTOList = compDetailTOList;
	}
	public List<ImageVO> getCompDetailImgs() {
		return compDetailImgs;
	}
	public void setCompDetailImgs(List<ImageVO> compDetailImgs) {
		this.compDetailImgs = compDetailImgs;
	}
	public List<ImageVO> getCompImgs() {
		return compImgs;
	}
	public void setCompImgs(List<ImageVO> compImgs) {
		this.compImgs = compImgs;
	}
	public List<ComponentDetail> getComponentDetails() {
		return componentDetails;
	}
	public void setComponentDetails(List<ComponentDetail> componentDetails) {
		this.componentDetails = componentDetails;
	}
	//Grid variable Declarations
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	private String sidx;
	private int count;
	private Integer id;  
	
	//Sql Return Types
	private String sqlReturnMessage; 
	private int sqlReturnStatus;
	private String sqlProgramName; 
	
	
	
	//Getter&setters
	public int getComponentId() {
		return componentId;
	}
	public void setComponentId(int componentId) {
		this.componentId = componentId;
	}
	public int getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}
	public int getComponentTypeId() {
		return componentTypeId;
	}
	public void setComponentTypeId(int componentTypeId) {
		this.componentTypeId = componentTypeId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public double getComponentSize() {
		return componentSize;
	}
	public void setComponentSize(double componentSize) {
		this.componentSize = componentSize;
	}
	public int getUnitSize() {
		return unitSize;
	}
	public void setUnitSize(int unitSize) {
		this.unitSize = unitSize;
	}
	public int getComponentNumber() {
		return componentNumber;
	}
	public void setComponentNumber(int componentNumber) {
		this.componentNumber = componentNumber;
	}
	public double getRent() {
		return rent;
	}
	public void setRent(double rent) {
		this.rent = rent;
	}

	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}

	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getComponentType() {
		return componentType;
	}
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}
	public Integer getGridFrom() {
		return gridFrom;
	}
	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}
	public Integer getGridTo() {
		return gridTo;
	}
	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}
	public String getGridSord() {
		return gridSord;
	}
	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	 
	public String getSqlReturnMessage() {
		return sqlReturnMessage;
	}
	public void setSqlReturnMessage(String sqlReturnMessage) {
		this.sqlReturnMessage = sqlReturnMessage;
	}
	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}
	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}
	public String getSqlProgramName() {
		return sqlProgramName;
	}
	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}
	public int getComponentFeatureId() {
		return componentFeatureId;
	}
	public void setComponentFeatureId(int componentFeatureId) {
		this.componentFeatureId = componentFeatureId;
	}
	public String getComponentFeature() {
		return componentFeature;
	}
	public void setComponentFeature(String componentFeature) {
		this.componentFeature = componentFeature;
	}
	public String getComponentFeatureDetails() {
		return componentFeatureDetails;
	}
	public void setComponentFeatureDetails(String componentFeatureDetails) {
		this.componentFeatureDetails = componentFeatureDetails;
	}
	public int getComponentLineId() {
		return componentLineId;
	}
	public void setComponentLineId(int componentLineId) {
		this.componentLineId = componentLineId;
	}
	 
	public String getComponentFeatureCode() {
		return componentFeatureCode;
	}
	public void setComponentFeatureCode(String componentFeatureCode) {
		this.componentFeatureCode = componentFeatureCode;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public int getNumberOfSize() {
		return numberOfSize;
	}
	public void setNumberOfSize(int numberOfSize) {
		this.numberOfSize = numberOfSize;
	}
	public Long getLookupDetailId() {
		return lookupDetailId;
	}
	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}
	public String getComponentNameArabic() {
		return componentNameArabic;
	}
	public void setComponentNameArabic(String componentNameArabic) {
		this.componentNameArabic = componentNameArabic;
	}
}
