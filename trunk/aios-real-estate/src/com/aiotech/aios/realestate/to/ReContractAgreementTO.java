package com.aiotech.aios.realestate.to;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Legal;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;

public class ReContractAgreementTO implements Serializable{

	private static final long serialVersionUID = 1L;
	// Master Details Variables
	private long offerId;
	private int offerNumber;
	private int buildingId;
	private int buildingNumber;
	private String buildingName;
	private String buildingNameArabic;

	private int tenantId;
	private String tenantName;
	private String tenantNameArabic;
	private String prefix;

	private String presentAddress;
	private String permanantAddress;
	
	private String tenantIssueDate;
	private String tenantIdentity;
	private String tenantNationality;
	private String tenantFax;
	private String tenantMobile;
	private String tenantPOBox;
	private TenantGroup tenantGroup;

	private String buildingStreet;
	private String buildingCity;
	private String buildingArea;
	private String buildingPlot;

	private int contractId;
	private String contractNumber;
	private String contractDate;
	private int tenantTypeId;
	private String tenantTypeName;
	private String paymentMethod;
	private int noOfCheques;
	
	private String fromDate;
	private String toDate;
	
	private double cashAmount;
	private double chequeAmount;
	private double grandTotal;
	
	private int rentFees;
	private double depositAmount;
	private String remarks;
	private String serviceMeter;
	private String leasedFor;

	// Rent Details Variables
	private long alertId;
	private int offerDetailId;
	private int contractRentId;
	private int lineNumber;
	private String flatNumber;
	private double rentAmount;
	private double contractAmount;
	private double balance;
	private double balanceAmount;
	private int noOfDays;
	private int propertyFlatId;
	private long propertyId;
	private long componentId;
	private long unitId;
	private String propertyName;
	private String componentName;
	private String unitName;
	private String propertyNameArabic;
	private String componentNameArabic;
	private String unitNameArabic;
	private long releaseId;
	private long releaseDetailId;
	private String initializationFlag;
	private String renewalFlag;
	private String tenantType;
	private List<ReContractAgreementTO> postDetails;
	// Fee Details
	private int contractFeeId;
	private int feeId;
	private String feeDescription;
	private double feeAmount;
	
	//Payment Details
	private long contractPaymentId;
	private String bankName;
	private String chequeDate;
	private String chequeNumber;
	
	private String description;
	
	// Renewal Details
	private String extendedToDate;
	
	//workflow variables
	private int applicationId;
	private Long notificationId; 
	private String notificationType;
	private int companyId;
	private int workflowId;
	private String workflowName;
	private String workflowStatus;
	private int categoryId; 
	private int notifyFunctionId;
	private int functionId;
	private String wfFunctionType;
	private String workflowParam;	
	private int mappingId;
	private String attribute1;
	private String attribute2;
	private String attribute3;
	private String requestReason;
	private int sessionPersonId;
	
	private int headerLedgerId;
	private int wfCategoryId;
	private String functionType;
	
	private String approvedStatus;
	private String approvedStatusName;
	
	private String comments;
	
	private Integer trnValue;
	private int sessionId;

	private int actualLineId;
	private Boolean actionFlag; 
	private Integer tempLineId;
	
	private int headerFlag;
	
	private String sqlReturnMsg;
	private int sqlErrorNum;
	private int sqlReturnStatus;
	private String sqlProgramName;
	
	private Integer gridFrom;
	private Integer gridTo;
	private String gridSord;
	

	private String sidx;
	private int count;
	private Integer id; 
	
	private long paymentReceived;
	
	private String offeredPropertyOwners;
	private String offeredPropertyOwners_arabic;

	private String createdDate;
	private long createdBy;
	private double offerAmount;
	private String contractPeriod;
	private String propertyFullInfo;
	private String tenantFullInfo;
	private String accountCode;
	private String accountDescription;
	private long accountId;
	// Jasper
	
	private List<RePropertyInfoTO> proeprtyList;
	private Set<ContractPayment> contractPayments;
	private List<HrPersonalDetailsTO> tenantList;
	private String linkLabel;
	private String anchorExpression;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	private List<Legal> legallist;
	private List<ReContractAgreementTO> contractAgreementList;
	private List<ReContractAgreementTO> contractAgreementListInactive;
	private List<ReContractAgreementTO> tenantListReport;
	private boolean isCancelled;
	private boolean cancellationInitiated;
	private ReContractAgreementTO tenantInfo;
	private String paymentStatus;
	private String paymentVisibility;
	private String paymentType;
	private String offerAmountInPercentage;
	private String percentageStr;
	private String paidDate;
	private Boolean isDeposited;
	private Boolean isPdc;
	
	private String feeType;
	private byte feeTypeId;
	private List<BankReceiptsDetail> bankReceiptsDetails;
	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public List<HrPersonalDetailsTO> getTenantList() {
		return tenantList;
	}

	public void setTenantList(List<HrPersonalDetailsTO> tenantList) {
		this.tenantList = tenantList;
	}

	public Set<ContractPayment> getContractPayments() {
		return contractPayments;
	}

	public void setContractPayments(Set<ContractPayment> contractPayments) {
		this.contractPayments = contractPayments;
	}

	public List<RePropertyInfoTO> getProeprtyList() {
		return proeprtyList;
	}

	public void setProeprtyList(List<RePropertyInfoTO> proeprtyList) {
		this.proeprtyList = proeprtyList;
	}

	public Integer getTrnValue() {
		return trnValue;
	}

	public void setTrnValue(Integer trnValue) {
		this.trnValue = trnValue;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public int getActualLineId() {
		return actualLineId;
	}

	public void setActualLineId(int actualLineId) {
		this.actualLineId = actualLineId;
	} 
	public Integer getTempLineId() {
		return tempLineId;
	}

	public void setTempLineId(Integer tempLineId) {
		this.tempLineId = tempLineId;
	}

	public int getHeaderFlag() {
		return headerFlag;
	}

	public void setHeaderFlag(int headerFlag) {
		this.headerFlag = headerFlag;
	}

	public String getSqlReturnMsg() {
		return sqlReturnMsg;
	}

	public void setSqlReturnMsg(String sqlReturnMsg) {
		this.sqlReturnMsg = sqlReturnMsg;
	}

	public int getSqlErrorNum() {
		return sqlErrorNum;
	}

	public void setSqlErrorNum(int sqlErrorNum) {
		this.sqlErrorNum = sqlErrorNum;
	}

	public int getSqlReturnStatus() {
		return sqlReturnStatus;
	}

	public void setSqlReturnStatus(int sqlReturnStatus) {
		this.sqlReturnStatus = sqlReturnStatus;
	}

	public String getSqlProgramName() {
		return sqlProgramName;
	}

	public void setSqlProgramName(String sqlProgramName) {
		this.sqlProgramName = sqlProgramName;
	}

	public Integer getGridFrom() {
		return gridFrom;
	}

	public void setGridFrom(Integer gridFrom) {
		this.gridFrom = gridFrom;
	}

	public Integer getGridTo() {
		return gridTo;
	}

	public void setGridTo(Integer gridTo) {
		this.gridTo = gridTo;
	}

	public String getGridSord() {
		return gridSord;
	}

	public void setGridSord(String gridSord) {
		this.gridSord = gridSord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	} 
	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public int getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(int buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanantAddress() {
		return permanantAddress;
	}

	public void setPermanantAddress(String permanantAddress) {
		this.permanantAddress = permanantAddress;
	}

	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public int getTenantTypeId() {
		return tenantTypeId;
	}

	public void setTenantTypeId(int tenantTypeId) {
		this.tenantTypeId = tenantTypeId;
	}

	public String getTenantTypeName() {
		return tenantTypeName;
	}

	public void setTenantTypeName(String tenantTypeName) {
		this.tenantTypeName = tenantTypeName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public int getNoOfCheques() {
		return noOfCheques;
	}

	public void setNoOfCheques(int noOfCheques) {
		this.noOfCheques = noOfCheques;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public double getRentAmount() {
		return rentAmount;
	}

	public void setRentAmount(double rentAmount) {
		this.rentAmount = rentAmount;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public int getFeeId() {
		return feeId;
	}

	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	public String getFeeDescription() {
		return feeDescription;
	}

	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public int getOfferNumber() {
		return offerNumber;
	}

	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}

	public int getContractRentId() {
		return contractRentId;
	}

	public void setContractRentId(int contractRentId) {
		this.contractRentId = contractRentId;
	}

	public int getContractFeeId() {
		return contractFeeId;
	}

	public void setContractFeeId(int contractFeeId) {
		this.contractFeeId = contractFeeId;
	}

	public long getContractPaymentId() {
		return contractPaymentId;
	}

	public void setContractPaymentId(long contractPaymentId) {
		this.contractPaymentId = contractPaymentId;
	}

	public int getOfferDetailId() {
		return offerDetailId;
	}

	public void setOfferDetailId(int offerDetailId) {
		this.offerDetailId = offerDetailId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public int getPropertyFlatId() {
		return propertyFlatId;
	}

	public void setPropertyFlatId(int propertyFlatId) {
		this.propertyFlatId = propertyFlatId;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getExtendedToDate() {
		return extendedToDate;
	}

	public void setExtendedToDate(String extendedToDate) {
		this.extendedToDate = extendedToDate;
	}

	public int getRentFees() {
		return rentFees;
	}

	public void setRentFees(int rentFees) {
		this.rentFees = rentFees;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(int workflowId) {
		this.workflowId = workflowId;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getNotifyFunctionId() {
		return notifyFunctionId;
	}

	public void setNotifyFunctionId(int notifyFunctionId) {
		this.notifyFunctionId = notifyFunctionId;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	public String getWfFunctionType() {
		return wfFunctionType;
	}

	public void setWfFunctionType(String wfFunctionType) {
		this.wfFunctionType = wfFunctionType;
	}

	public String getWorkflowParam() {
		return workflowParam;
	}

	public void setWorkflowParam(String workflowParam) {
		this.workflowParam = workflowParam;
	}

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getAttribute3() {
		return attribute3;
	}

	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}

	public String getRequestReason() {
		return requestReason;
	}

	public void setRequestReason(String requestReason) {
		this.requestReason = requestReason;
	}

	public int getSessionPersonId() {
		return sessionPersonId;
	}

	public void setSessionPersonId(int sessionPersonId) {
		this.sessionPersonId = sessionPersonId;
	}

	public int getHeaderLedgerId() {
		return headerLedgerId;
	}

	public void setHeaderLedgerId(int headerLedgerId) {
		this.headerLedgerId = headerLedgerId;
	}

	public int getWfCategoryId() {
		return wfCategoryId;
	}

	public void setWfCategoryId(int wfCategoryId) {
		this.wfCategoryId = wfCategoryId;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getApprovedStatus() {
		return approvedStatus;
	}

	public void setApprovedStatus(String approvedStatus) {
		this.approvedStatus = approvedStatus;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getApprovedStatusName() {
		return approvedStatusName;
	}

	public void setApprovedStatusName(String approvedStatusName) {
		this.approvedStatusName = approvedStatusName;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public long getComponentId() {
		return componentId;
	}

	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(long releaseId) {
		this.releaseId = releaseId;
	}

	public long getReleaseDetailId() {
		return releaseDetailId;
	}

	public void setReleaseDetailId(long releaseDetailId) {
		this.releaseDetailId = releaseDetailId;
	}

	public String getInitializationFlag() {
		return initializationFlag;
	}

	public void setInitializationFlag(String initializationFlag) {
		this.initializationFlag = initializationFlag;
	}

	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}

	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}

	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}

	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}

	public long getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(long paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}

	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}

	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}

	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	
	
	public String getTenantIssueDate() {
		return tenantIssueDate;
	}

	public String getTenantIdentity() {
		return tenantIdentity;
	}

	public String getTenantNationality() {
		return tenantNationality;
	}

	public String getTenantFax() {
		return tenantFax;
	}

	public String getTenantMobile() {
		return tenantMobile;
	}

	public String getTenantPOBox() {
		return tenantPOBox;
	}

	public void setTenantIssueDate(String tenantIssueDate) {
		this.tenantIssueDate = tenantIssueDate;
	}

	public void setTenantIdentity(String tenantIdentity) {
		this.tenantIdentity = tenantIdentity;
	}

	public void setTenantNationality(String tenantNationality) {
		this.tenantNationality = tenantNationality;
	}

	public void setTenantFax(String tenantFax) {
		this.tenantFax = tenantFax;
	}

	public void setTenantMobile(String tenantMobile) {
		this.tenantMobile = tenantMobile;
	}

	public void setTenantPOBox(String tenantPOBox) {
		this.tenantPOBox = tenantPOBox;
	}


	public String getBuildingStreet() {
		return buildingStreet;
	}

	public String getBuildingCity() {
		return buildingCity;
	}

	public String getBuildingArea() {
		return buildingArea;
	}

	public String getBuildingPlot() {
		return buildingPlot;
	}

	public void setBuildingStreet(String buildingStreet) {
		this.buildingStreet = buildingStreet;
	}

	public void setBuildingCity(String buildingCity) {
		this.buildingCity = buildingCity;
	}

	public void setBuildingArea(String buildingArea) {
		this.buildingArea = buildingArea;
	}

	public void setBuildingPlot(String buildingPlot) {
		this.buildingPlot = buildingPlot;
	}

	public String getTenantType() {
		return tenantType;
	}

	public void setTenantType(String tenantType) {
		this.tenantType = tenantType;
	}

	public List<Legal> getLegallist() {
		return legallist;
	}

	public void setLegallist(List<Legal> legallist) {
		this.legallist = legallist;
	}
	
	public TenantGroup getTenantGroup() {
		return tenantGroup;
	}

	public void setTenantGroup(TenantGroup tenantGroup) {
		this.tenantGroup = tenantGroup;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	} 
	
	public String getOfferedPropertyOwners() {
		return offeredPropertyOwners;
	}

	public void setOfferedPropertyOwners(String offeredPropertyOwners) {
		this.offeredPropertyOwners = offeredPropertyOwners;
	}
	
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public String getServiceMeter() {
		return serviceMeter;
	}

	public void setServiceMeter(String serviceMeter) {
		this.serviceMeter = serviceMeter;
	}

	public List<ReContractAgreementTO> getContractAgreementList() {
		return contractAgreementList;
	}

	public void setContractAgreementList(
			List<ReContractAgreementTO> contractAgreementList) {
		this.contractAgreementList = contractAgreementList;
	}

	public String getLeasedFor() {
		return leasedFor;
	}

	public void setLeasedFor(String leasedFor) {
		this.leasedFor = leasedFor;
	}

	public String getTenantNameArabic() {
		return tenantNameArabic;
	}

	public void setTenantNameArabic(String tenantNameArabic) {
		this.tenantNameArabic = tenantNameArabic;
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	public boolean isCancellationInitiated() {
		return cancellationInitiated;
	}

	public void setCancellationInitiated(boolean cancellationInitiated) {
		this.cancellationInitiated = cancellationInitiated;
	}

	public List<ReContractAgreementTO> getTenantListReport() {
		return tenantListReport;
	}

	public void setTenantListReport(List<ReContractAgreementTO> tenantListReport) {
		this.tenantListReport = tenantListReport;
	}

	public List<ReContractAgreementTO> getPostDetails() {
		return postDetails;
	}

	public void setPostDetails(List<ReContractAgreementTO> postDetails) {
		this.postDetails = postDetails;
	}

	public Boolean getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(Boolean actionFlag) {
		this.actionFlag = actionFlag;
	}

	public ReContractAgreementTO getTenantInfo() {
		return tenantInfo;
	}

	public void setTenantInfo(ReContractAgreementTO tenantInfo) {
		this.tenantInfo = tenantInfo;
	}

	public double getOfferAmount() {
		return offerAmount;
	}

	public void setOfferAmount(double offerAmount) {
		this.offerAmount = offerAmount;
	}

	public String getContractPeriod() {
		return contractPeriod;
	}

	public void setContractPeriod(String contractPeriod) {
		this.contractPeriod = contractPeriod;
	}

	public String getPropertyFullInfo() {
		return propertyFullInfo;
	}

	public void setPropertyFullInfo(String propertyFullInfo) {
		this.propertyFullInfo = propertyFullInfo;
	} 
	

	public String getBuildingNameArabic() {
		return buildingNameArabic;
	}

	public void setBuildingNameArabic(String buildingNameArabic) {
		this.buildingNameArabic = buildingNameArabic;
	}

	public String getTenantFullInfo() {
		return tenantFullInfo;
	}

	public void setTenantFullInfo(String tenantFullInfo) {
		this.tenantFullInfo = tenantFullInfo;
	}

	public List<ReContractAgreementTO> getContractAgreementListInactive() {
		return contractAgreementListInactive;
	}

	public void setContractAgreementListInactive(
			List<ReContractAgreementTO> contractAgreementListInactive) {
		this.contractAgreementListInactive = contractAgreementListInactive;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	

	public String getOfferedPropertyOwners_arabic() {
		return offeredPropertyOwners_arabic;
	}

	public void setOfferedPropertyOwners_arabic(String offeredPropertyOwners_arabic) {
		this.offeredPropertyOwners_arabic = offeredPropertyOwners_arabic;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentVisibility() {
		return paymentVisibility;
	}

	public void setPaymentVisibility(String paymentVisibility) {
		this.paymentVisibility = paymentVisibility;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getOfferAmountInPercentage() {
		return offerAmountInPercentage;
	}

	public void setOfferAmountInPercentage(String offerAmountInPercentage) {
		this.offerAmountInPercentage = offerAmountInPercentage;
	}

	public String getPercentageStr() {
		return percentageStr;
	}

	public void setPercentageStr(String percentageStr) {
		this.percentageStr = percentageStr;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getFeeType() {
		return feeType;
	}

	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	public byte getFeeTypeId() {
		return feeTypeId;
	}

	public void setFeeTypeId(byte feeTypeId) {
		this.feeTypeId = feeTypeId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public Boolean getIsDeposited() {
		return isDeposited;
	}

	public void setIsDeposited(Boolean isDeposited) {
		this.isDeposited = isDeposited;
	}

	public Boolean getIsPdc() {
		return isPdc;
	}

	public void setIsPdc(Boolean isPdc) {
		this.isPdc = isPdc;
	}

	public List<BankReceiptsDetail> getBankReceiptsDetails() {
		return bankReceiptsDetails;
	}

	public void setBankReceiptsDetails(List<BankReceiptsDetail> bankReceiptsDetails) {
		this.bankReceiptsDetails = bankReceiptsDetails;
	}

	public String getPropertyNameArabic() {
		return propertyNameArabic;
	}

	public void setPropertyNameArabic(String propertyNameArabic) {
		this.propertyNameArabic = propertyNameArabic;
	}

	public String getComponentNameArabic() {
		return componentNameArabic;
	}

	public void setComponentNameArabic(String componentNameArabic) {
		this.componentNameArabic = componentNameArabic;
	}

	public String getUnitNameArabic() {
		return unitNameArabic;
	}

	public void setUnitNameArabic(String unitNameArabic) {
		this.unitNameArabic = unitNameArabic;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
}
