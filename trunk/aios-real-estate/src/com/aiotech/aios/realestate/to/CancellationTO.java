package com.aiotech.aios.realestate.to;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;

public class CancellationTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long cancellationId;

	// Master Details Variables
	private long offerId;
	private int offerNumber;
	private int tenantId;
	private String tenantName;
	private String presentAddress;
	private String permanantAddress;
	
	private long contractId;
	private String contractNumber;
	private String contractDate;
	private int tenantTypeId;
	private String tenantTypeName;
	private String paymentMethod;
	
	private String fromDate;
	private String toDate;
	
	private double cashAmount;
	private double chequeAmount;
	private double grandTotal;
	
	private int rentFees;
	private double depositAmount;
	
	// Rent Details Variables
	private int offerDetailId;
	private int contractRentId;
	private int lineNumber;
	private String flatNumber;
	private double rentAmount;
	private double contractAmount;
	private double balance;
	private double balanceAmount;
	private int noOfDays;
	private int propertyFlatId;
	private long propertyId;
	private long componentId;
	private long unitId;
	private String propertyName;
	private String componentName;
	private String unitName;
	private String depositAmountStr;
	private String otherChargesStr;
	private String totalDeductionStr;
	private String refundAmountStr;
	private String damageTotalStr;
	private String refundAmountInWords;
	private String description;
	private String amount;
	private String paidDate;
	private String effectiveDate;
	// Fee Details
	private int contractFeeId;
	private int feeId;
	private String feeDescription;
	private double feeAmount;
	
	//Payment Details
	private long contractPaymentId;
	private String bankName;
	private String chequeDate;
	private String chequeNumber;

	private Boolean isPenalty;
	private Boolean isRequested;
	private double penaltyAmount;
	private double totalRentAmount;
	private String rentalReturn;
	private Boolean pdcAccount;
	private String pdcAccountCode;
	private long pdcAccountId;
	private long unEarnedAccountId;
	private String unEarnedAccountCode;
	private String incomeAccountCode;
	private long incomeAccountId;
	private String expenseAccountCode;
	private long expenseAccountId;
	private long depositAccountId;
	private String depositAccountCode;
	private long alertId;
	private List<CancellationTO> cancellationList;
	// Jasper
	
	private List<RePropertyInfoTO> propertyList;
	private List<HrPersonalDetailsTO> tenantList; 
	private List<ReContractAgreementTO> tenantInfo;
	private String linkLabel;
	private String anchorExpression;
	private List<Contract> contractList;
	private Set<ReleaseDetail> releaseDetails;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	public Set<ReleaseDetail> getReleaseDetails() {
		return releaseDetails;
	}

	public void setReleaseDetails(Set<ReleaseDetail> releaseDetails) {
		this.releaseDetails = releaseDetails;
	}

	public List<Contract> getContractList() {
		return contractList;
	}

	public void setContractList(List<Contract> contractList) {
		this.contractList = contractList;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public List<RePropertyInfoTO> getPropertyList() {
		return propertyList;
	}

	public List<HrPersonalDetailsTO> getTenantList() {
		return tenantList;
	}

	public void setTenantList(List<HrPersonalDetailsTO> tenantList) {
		this.tenantList = tenantList;
	}

	public void setPropertyList(List<RePropertyInfoTO> propertyList) {
		this.propertyList = propertyList;
	}

	 

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanantAddress() {
		return permanantAddress;
	}

	public void setPermanantAddress(String permanantAddress) {
		this.permanantAddress = permanantAddress;
	}

	public long getContractId() {
		return contractId;
	}

	public void setContractId(long contractId) {
		this.contractId = contractId;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public int getTenantTypeId() {
		return tenantTypeId;
	}

	public void setTenantTypeId(int tenantTypeId) {
		this.tenantTypeId = tenantTypeId;
	}

	public String getTenantTypeName() {
		return tenantTypeName;
	}

	public void setTenantTypeName(String tenantTypeName) {
		this.tenantTypeName = tenantTypeName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public double getChequeAmount() {
		return chequeAmount;
	}

	public void setChequeAmount(double chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	public double getRentAmount() {
		return rentAmount;
	}

	public void setRentAmount(double rentAmount) {
		this.rentAmount = rentAmount;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public int getFeeId() {
		return feeId;
	}

	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	public String getFeeDescription() {
		return feeDescription;
	}

	public void setFeeDescription(String feeDescription) {
		this.feeDescription = feeDescription;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public int getOfferNumber() {
		return offerNumber;
	}

	public void setOfferNumber(int offerNumber) {
		this.offerNumber = offerNumber;
	}

	public int getContractRentId() {
		return contractRentId;
	}

	public void setContractRentId(int contractRentId) {
		this.contractRentId = contractRentId;
	}

	public int getContractFeeId() {
		return contractFeeId;
	}

	public void setContractFeeId(int contractFeeId) {
		this.contractFeeId = contractFeeId;
	}

	public long getContractPaymentId() {
		return contractPaymentId;
	}

	public void setContractPaymentId(long contractPaymentId) {
		this.contractPaymentId = contractPaymentId;
	}

	public int getOfferDetailId() {
		return offerDetailId;
	}

	public void setOfferDetailId(int offerDetailId) {
		this.offerDetailId = offerDetailId;
	}

	

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public int getPropertyFlatId() {
		return propertyFlatId;
	}

	public void setPropertyFlatId(int propertyFlatId) {
		this.propertyFlatId = propertyFlatId;
	}

	public double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public int getRentFees() {
		return rentFees;
	}

	public void setRentFees(int rentFees) {
		this.rentFees = rentFees;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}
	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public long getComponentId() {
		return componentId;
	}

	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	public long getUnitId() {
		return unitId;
	}

	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}

	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}

	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}

	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}

	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}

	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}

	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}

	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}

	public String getDepositAmountStr() {
		return depositAmountStr;
	}

	public void setDepositAmountStr(String depositAmountStr) {
		this.depositAmountStr = depositAmountStr;
	}

	public String getOtherChargesStr() {
		return otherChargesStr;
	}

	public void setOtherChargesStr(String otherChargesStr) {
		this.otherChargesStr = otherChargesStr;
	}

	public String getTotalDeductionStr() {
		return totalDeductionStr;
	}

	public void setTotalDeductionStr(String totalDeductionStr) {
		this.totalDeductionStr = totalDeductionStr;
	}

	public String getRefundAmountStr() {
		return refundAmountStr;
	}

	public void setRefundAmountStr(String refundAmountStr) {
		this.refundAmountStr = refundAmountStr;
	}

	public String getDamageTotalStr() {
		return damageTotalStr;
	}

	public void setDamageTotalStr(String damageTotalStr) {
		this.damageTotalStr = damageTotalStr;
	}

	public String getRefundAmountInWords() {
		return refundAmountInWords;
	}

	public void setRefundAmountInWords(String refundAmountInWords) {
		this.refundAmountInWords = refundAmountInWords;
	}

	public List<ReContractAgreementTO> getTenantInfo() {
		return tenantInfo;
	}

	public void setTenantInfo(List<ReContractAgreementTO> tenantInfo) {
		this.tenantInfo = tenantInfo;
	}

	public long getCancellationId() {
		return cancellationId;
	}

	public void setCancellationId(long cancellationId) {
		this.cancellationId = cancellationId;
	}

	
	public Boolean getIsPenalty() {
		return isPenalty;
	}

	public void setIsPenalty(Boolean isPenalty) {
		this.isPenalty = isPenalty;
	}

	public Boolean getIsRequested() {
		return isRequested;
	}

	public void setIsRequested(Boolean isRequested) {
		this.isRequested = isRequested;
	}

	public double getPenaltyAmount() {
		return penaltyAmount;
	}

	public void setPenaltyAmount(double penaltyAmount) {
		this.penaltyAmount = penaltyAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	public double getTotalRentAmount() {
		return totalRentAmount;
	}

	public void setTotalRentAmount(double totalRentAmount) {
		this.totalRentAmount = totalRentAmount;
	}

	public String getRentalReturn() {
		return rentalReturn;
	}

	public void setRentalReturn(String rentalReturn) {
		this.rentalReturn = rentalReturn;
	}

	public Boolean getPdcAccount() {
		return pdcAccount;
	}

	public void setPdcAccount(Boolean pdcAccount) {
		this.pdcAccount = pdcAccount;
	}

	public String getPdcAccountCode() {
		return pdcAccountCode;
	}

	public void setPdcAccountCode(String pdcAccountCode) {
		this.pdcAccountCode = pdcAccountCode;
	}

	public long getPdcAccountId() {
		return pdcAccountId;
	}

	public void setPdcAccountId(long pdcAccountId) {
		this.pdcAccountId = pdcAccountId;
	}

	public long getUnEarnedAccountId() {
		return unEarnedAccountId;
	}

	public void setUnEarnedAccountId(long unEarnedAccountId) {
		this.unEarnedAccountId = unEarnedAccountId;
	}

	public String getUnEarnedAccountCode() {
		return unEarnedAccountCode;
	}

	public void setUnEarnedAccountCode(String unEarnedAccountCode) {
		this.unEarnedAccountCode = unEarnedAccountCode;
	}

	public List<CancellationTO> getCancellationList() {
		return cancellationList;
	}

	public void setCancellationList(List<CancellationTO> cancellationList) {
		this.cancellationList = cancellationList;
	}

	public String getIncomeAccountCode() {
		return incomeAccountCode;
	}

	public void setIncomeAccountCode(String incomeAccountCode) {
		this.incomeAccountCode = incomeAccountCode;
	}

	public long getIncomeAccountId() {
		return incomeAccountId;
	}

	public void setIncomeAccountId(long incomeAccountId) {
		this.incomeAccountId = incomeAccountId;
	}

	public String getExpenseAccountCode() {
		return expenseAccountCode;
	}

	public void setExpenseAccountCode(String expenseAccountCode) {
		this.expenseAccountCode = expenseAccountCode;
	}

	public long getExpenseAccountId() {
		return expenseAccountId;
	}

	public void setExpenseAccountId(long expenseAccountId) {
		this.expenseAccountId = expenseAccountId;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public long getDepositAccountId() {
		return depositAccountId;
	}

	public void setDepositAccountId(long depositAccountId) {
		this.depositAccountId = depositAccountId;
	}

	public String getDepositAccountCode() {
		return depositAccountCode;
	}

	public void setDepositAccountCode(String depositAccountCode) {
		this.depositAccountCode = depositAccountCode;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	} 
}
