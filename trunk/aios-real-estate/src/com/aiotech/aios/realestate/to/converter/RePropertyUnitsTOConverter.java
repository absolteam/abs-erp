package com.aiotech.aios.realestate.to.converter;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.realestate.action.RePropertyUnitsAction;
import com.aiotech.aios.realestate.domain.entity.Asset;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.to.RePropertyUnitsTO;

public class RePropertyUnitsTOConverter {

	public static List<RePropertyUnitsTO> convertToTOList(
			List<Unit> unit) throws Exception {

		List<RePropertyUnitsTO> tosList = new ArrayList<RePropertyUnitsTO>();

		for (Unit cals : unit) {
			tosList.add(convertToTO(cals));
		}

		return tosList;
	}
	
	public static List<Asset> assetToConverter(List<String> assetList){
		List<Asset> detailList=new ArrayList<Asset>();
		for(int i=0;i<assetList.size();i++){
			detailList.add(convertAssetEntity(assetList.get(i)));
		}
		return detailList;
	}
	

	public static Asset convertAssetEntity(String assetDetail){
		Asset asset=new Asset();
		Product prod=new Product();
		String delimeter="__"; 
		String assetArray[]=RePropertyUnitsAction.splitArrayValues(assetDetail, delimeter);
		prod.setProductId(Long.parseLong(assetArray[0]));
		asset.setDetail(assetArray[1]);
		if(!assetArray[2].trim().equals("-1") && !assetArray[2].trim().equals("") 
				&& !assetArray[2].trim().equals("0") )
			asset.setAssetId(Long.parseLong(assetArray[2]));
		asset.setProduct(prod);
		return asset;
	}

	public static RePropertyUnitsTO convertToTO(Unit unit) throws Exception {
		RePropertyUnitsTO infoTO = new RePropertyUnitsTO();
		try{
		infoTO.setUnitId(unit.getUnitId());
		infoTO.setUnitName(unit.getUnitName());
		infoTO.setUnitNameArabic(AIOSCommons.bytesToUTFString(unit.getUnitNameArabic()));
		if(unit.getUnitNo()!=null)
		infoTO.setUnitNumber(unit.getUnitNo());
		if(unit.getUnitType()!=null && unit.getUnitType().getUnitTypeId()!=null){
			infoTO.setUnitTypeId(unit.getUnitType().getUnitTypeId());
			infoTO.setUnitType(unit.getUnitType().getType());
		}else{
			infoTO.setUnitTypeId(unit.getUnitType().getUnitTypeId());
		}
		if(unit.getComponent()!=null && unit.getComponent().getComponentName()!=null){
			infoTO.setComponentId(unit.getComponent().getComponentId());
			infoTO.setComponentName(unit.getComponent().getComponentName());
			infoTO.setPropertyName(unit.getComponent().getProperty().getPropertyName());
		}else{
			infoTO.setComponentId(unit.getComponent().getComponentId());
		}
		if(unit.getRooms()!=null)
		infoTO.setRooms(unit.getRooms());
		if(unit.getRent()!=null)
		infoTO.setRent(unit.getRent());
		if(unit.getStatus()!=null){
			infoTO.setStatus(Integer.parseInt(unit.getStatus()+""));	
			infoTO.setUnitStatus(Constants.RealEstate.UnitStatus.get(Integer.parseInt(unit.getStatus()+"")).toString());
		}
		infoTO.setMeterNo(unit.getMeter());
		if(unit.getSizeUnit()!=null)
			infoTO.setSizeUnit(unit.getSizeUnit());
		else
			infoTO.setSizeUnit(1);
		if(unit.getSize()!=null){
			infoTO.setSize(unit.getSize());
			infoTO.setSizeWithUom(unit.getSize()+" "+convertUOM(infoTO.getSizeUnit()));
		}
		if(unit.getFurnished()!=null)
		infoTO.setFurnished(unit.getFurnished());
		if(unit.getFurnished()!=null)
		infoTO.setFurnishedCondition(convertFurnishedStatus(unit.getFurnished()));	

		infoTO.setIsApproved(unit.getIsApprove());
		
		infoTO.setBedroom(unit.getBedroom());
		infoTO.setBedroomSize(unit.getBedroomSize());
		infoTO.setBathroom(unit.getBathroom());
		infoTO.setBathroomSize(unit.getBathroomSize());
		infoTO.setHall(unit.getHall());
		infoTO.setHallSize(unit.getHallSize());
		infoTO.setKitchen(unit.getKitchen());
		infoTO.setKitchenSize(unit.getKitchenSize());
		infoTO.setServentRoom(unit.getServentRoom());
		infoTO.setDriverRoom(unit.getDriverRoom());
		infoTO.setFrontYard(unit.getFrontYard());
		infoTO.setBackYard(unit.getBackYard());
		infoTO.setFrontGarden(unit.getFrontGarden());
		infoTO.setBackGarden(unit.getBackGarden());
		infoTO.setGarage(unit.getGarage());
		infoTO.setSwimmingPool(unit.getSwimmingPool());
		infoTO.setInPantry(unit.getInPantry());
		infoTO.setOutPantry(unit.getOutPantry());
		infoTO.setPodeo(unit.getPodeo());
		infoTO.setFountain(unit.getFountain());
		infoTO.setGuestRoom(unit.getGuestRoom());
		infoTO.setSecurityRoom(unit.getSecurityRoom());
		infoTO.setKennel(unit.getKennel());
		infoTO.setTennisYard(unit.getTennisYard());
		}catch(NullPointerException nl){
			nl.printStackTrace();
		}
		return infoTO;
	}
	
	
	public static List<RePropertyUnitsTO> assetListToTOConverter(List<Asset> assets){
		List<RePropertyUnitsTO> assetList=new ArrayList<RePropertyUnitsTO>();
		for(int i=0;i<assets.size();i++){
			assetList.add(convertAssetToTO(assets.get(i)));
		}
		return assetList;
	}
	
	public static RePropertyUnitsTO convertAssetToTO(Asset asset){
		RePropertyUnitsTO to=new RePropertyUnitsTO();
		to.setAssetCode(asset.getProduct().getCode());
		to.setAssetName(asset.getProduct().getProductName());
		//to.setUom(asset.getProduct().getProductUnit().getUnitName());
		to.setDescription(asset.getDetail());
		return to;
	}
	
	public static String convertUOM(int sizeUnit){
		String sizeWithUom="";
		if(sizeUnit==1)
			sizeWithUom="Square Feet";
		else if(sizeUnit==2)
			sizeWithUom="Square Meter";
		else if(sizeUnit==3)
			sizeWithUom="Square Yard";
			
		return sizeWithUom;
	}
	
	public static String convertFurnishedStatus(int furnished){
		String furnishedCondition="";
		if(furnished==1)
			furnishedCondition="Furnished";
		else if(furnished==2)
			furnishedCondition="Semi-Furnished";
		else if(furnished==3)
			furnishedCondition="Unfurnished";
			
		return furnishedCondition;
	}
	
}
