package com.aiotech.aios.realestate.to;

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.UnitDetail;
import com.aiotech.aios.realestate.domain.entity.vo.UnitDetailVO;
import com.aiotech.aios.system.to.ImageVO;

public class RePropertyUnitsTO implements Serializable{
	
	private long unitId;
	private long componentId;
	private String componentName;
	private int unitTypeId;
	private String unitType;
	private int unitNumber;
	private String unitName;
	private String unitNameArabic;
	private int rooms;
	private double rent;
	private int status;
	private String unitStatus;
	private long unitDetailId;
	private String unitDetails;
	
	private String unitFeature;
	private int unitFeatureId;
	private String unitFeatureDetail;
	private long propertyId;
	private String propertyName;
	private byte furnished; 
	private Byte isApproved;
	private List<UnitDetailVO> unitDetailVO;
	
	private Byte bedroom;
	private String bedroomSize;
	private Byte bathroom;
	private String bathroomSize;
	private Byte hall;
	private String hallSize;
	private Byte kitchen;
	private String kitchenSize;
	private Byte serventRoom;
	private Byte driverRoom;
	private Byte frontYard;
	private Byte backYard;
	private Byte frontGarden;
	private Byte backGarden;
	private Byte garage;
	private Byte swimmingPool;
	private Byte inPantry;
	private Byte outPantry;
	private Byte podeo;
	private Byte fountain;
	private Byte guestRoom;
	private Byte securityRoom;
	private Byte kennel;
	private Byte tennisYard;
	private List<RePropertyUnitsTO> assetList;
	private String assetCode;
	private String assetName;
	private String uom;
	private String description;
	private String unitFullInfo;
	private List<RePropertyUnitsTO> unitList;
	private String sizeWithUom;
	private String furnishedCondition;
	private Long lookupDetailId;
	public Byte getBedroom() {
		return bedroom;
	}
	public String getBedroomSize() {
		return bedroomSize;
	}
	public Byte getBathroom() {
		return bathroom;
	}
	public String getBathroomSize() {
		return bathroomSize;
	}
	public Byte getHall() {
		return hall;
	}
	public String getHallSize() {
		return hallSize;
	}
	public Byte getKitchen() {
		return kitchen;
	}
	public String getKitchenSize() {
		return kitchenSize;
	}
	public Byte getServentRoom() {
		return serventRoom;
	}
	public Byte getDriverRoom() {
		return driverRoom;
	}
	public Byte getFrontYard() {
		return frontYard;
	}
	public Byte getBackYard() {
		return backYard;
	}
	public Byte getFrontGarden() {
		return frontGarden;
	}
	public Byte getBackGarden() {
		return backGarden;
	}
	public Byte getGarage() {
		return garage;
	}
	public Byte getSwimmingPool() {
		return swimmingPool;
	}
	public Byte getInPantry() {
		return inPantry;
	}
	public Byte getOutPantry() {
		return outPantry;
	}
	public Byte getPodeo() {
		return podeo;
	}
	public Byte getFountain() {
		return fountain;
	}
	public Byte getGuestRoom() {
		return guestRoom;
	}
	public Byte getSecurityRoom() {
		return securityRoom;
	}
	public Byte getKennel() {
		return kennel;
	}
	public Byte getTennisYard() {
		return tennisYard;
	}
	public void setBedroom(Byte bedroom) {
		this.bedroom = bedroom;
	}
	public void setBedroomSize(String bedroomSize) {
		this.bedroomSize = bedroomSize;
	}
	public void setBathroom(Byte bathroom) {
		this.bathroom = bathroom;
	}
	public void setBathroomSize(String bathroomSize) {
		this.bathroomSize = bathroomSize;
	}
	public void setHall(Byte hall) {
		this.hall = hall;
	}
	public void setHallSize(String hallSize) {
		this.hallSize = hallSize;
	}
	public void setKitchen(Byte kitchen) {
		this.kitchen = kitchen;
	}
	public void setKitchenSize(String kitchenSize) {
		this.kitchenSize = kitchenSize;
	}
	public void setServentRoom(Byte serventRoom) {
		this.serventRoom = serventRoom;
	}
	public void setDriverRoom(Byte driverRoom) {
		this.driverRoom = driverRoom;
	}
	public void setFrontYard(Byte frontYard) {
		this.frontYard = frontYard;
	}
	public void setBackYard(Byte backYard) {
		this.backYard = backYard;
	}
	public void setFrontGarden(Byte frontGarden) {
		this.frontGarden = frontGarden;
	}
	public void setBackGarden(Byte backGarden) {
		this.backGarden = backGarden;
	}
	public void setGarage(Byte garage) {
		this.garage = garage;
	}
	public void setSwimmingPool(Byte swimmingPool) {
		this.swimmingPool = swimmingPool;
	}
	public void setInPantry(Byte inPantry) {
		this.inPantry = inPantry;
	}
	public void setOutPantry(Byte outPantry) {
		this.outPantry = outPantry;
	}
	public void setPodeo(Byte podeo) {
		this.podeo = podeo;
	}
	public void setFountain(Byte fountain) {
		this.fountain = fountain;
	}
	public void setGuestRoom(Byte guestRoom) {
		this.guestRoom = guestRoom;
	}
	public void setSecurityRoom(Byte securityRoom) {
		this.securityRoom = securityRoom;
	}
	public void setKennel(Byte kennel) {
		this.kennel = kennel;
	}
	public void setTennisYard(Byte tennisYard) {
		this.tennisYard = tennisYard;
	}
	public List<UnitDetailVO> getUnitDetailVO() {
		return unitDetailVO;
	}
	public void setUnitDetailVO(List<UnitDetailVO> unitDetailVO) {
		this.unitDetailVO = unitDetailVO;
	}
	public List<UnitDetail> getPropUnitDetails() {
		return propUnitDetails;
	}
	public void setPropUnitDetails(List<UnitDetail> propUnitDetails) {
		this.propUnitDetails = propUnitDetails;
	}
	public List<ImageVO> getUnitDetailImgs() {
		return unitDetailImgs;
	}
	public void setUnitDetailImgs(List<ImageVO> unitDetailImgs) {
		this.unitDetailImgs = unitDetailImgs;
	}
	public List<ImageVO> getUnitImgs() {
		return unitImgs;
	}
	public void setUnitImgs(List<ImageVO> unitImgs) {
		this.unitImgs = unitImgs;
	}
	public String getLinkLabel() {
		return linkLabel;
	}
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}
	public String getAnchorExpression() {
		return anchorExpression;
	}
	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}
	private List<UnitDetail> propUnitDetails;
	private List<ImageVO> unitDetailImgs;
	private List<ImageVO> unitImgs;
	private String linkLabel;
	private String anchorExpression;
	private String approveLinkLabel;
	private String approveAnchorExpression;
	private String meterNo;
	private String rejectAnchorExpression;
	private String rejectLinkLabel;
	public String getMeterNo() {
		return meterNo;
	}
	public void setMeterNo(String meterNo) {
		this.meterNo = meterNo;
	}
	private Double size;
	public Double getSize() {
		return size;
	}
	public void setSize(Double size) {
		this.size = size;
	}
	public Integer getSizeUnit() {
		return sizeUnit;
	}
	public void setSizeUnit(Integer sizeUnit) {
		this.sizeUnit = sizeUnit;
	}
	private Integer sizeUnit;
	
	// grid
	private Integer rows = 0;

	// Get the requested page. By default grid sets this to 1.
	private Integer page = 0;

	// sorting order - asc or desc
	private String sord = "asc";

	// get index row - i.e. user click to sort.
	private String sidx;

	// Your Total Pages
	private Integer total = 0;

	// All Records
	private Integer records = 0;

	private String oper = "edit";
	private Integer id;
	private List cell;
	
	public long getUnitId() {
		return unitId;
	}
	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}
	public long getComponentId() {
		return componentId;
	}
	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public int getUnitTypeId() {
		return unitTypeId;
	}
	public void setUnitTypeId(int unitTypeId) {
		this.unitTypeId = unitTypeId;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	public int getRooms() {
		return rooms;
	}
	public void setRooms(int rooms) {
		this.rooms = rooms;
	}
	public double getRent() {
		return rent;
	}
	public void setRent(double rent) {
		this.rent = rent;
	}
	
	public long getUnitDetailId() {
		return unitDetailId;
	}
	public void setUnitDetailId(long unitDetailId) {
		this.unitDetailId = unitDetailId;
	}
	public String getUnitDetails() {
		return unitDetails;
	}
	public void setUnitDetails(String unitDetails) {
		this.unitDetails = unitDetails;
	}
	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getRecords() {
		return records;
	}
	public void setRecords(Integer records) {
		this.records = records;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List getCell() {
		return cell;
	}
	public void setCell(List cell) {
		this.cell = cell;
	}
	
	public String getUnitFeature() {
		return unitFeature;
	}
	public void setUnitFeature(String unitFeature) {
		this.unitFeature = unitFeature;
	}
	public int getUnitFeatureId() {
		return unitFeatureId;
	}
	public void setUnitFeatureId(int unitFeatureId) {
		this.unitFeatureId = unitFeatureId;
	}
	public String getUnitFeatureDetail() {
		return unitFeatureDetail;
	}
	public void setUnitFeatureDetail(String unitFeatureDetail) {
		this.unitFeatureDetail = unitFeatureDetail;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public long getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getApproveLinkLabel() {
		return approveLinkLabel;
	}
	public void setApproveLinkLabel(String approveLinkLabel) {
		this.approveLinkLabel = approveLinkLabel;
	}
	public String getApproveAnchorExpression() {
		return approveAnchorExpression;
	}
	public void setApproveAnchorExpression(String approveAnchorExpression) {
		this.approveAnchorExpression = approveAnchorExpression;
	}
	public String getRejectAnchorExpression() {
		return rejectAnchorExpression;
	}
	public void setRejectAnchorExpression(String rejectAnchorExpression) {
		this.rejectAnchorExpression = rejectAnchorExpression;
	}
	public String getRejectLinkLabel() {
		return rejectLinkLabel;
	}
	public void setRejectLinkLabel(String rejectLinkLabel) {
		this.rejectLinkLabel = rejectLinkLabel;
	}
	public byte getFurnished() {
		return furnished;
	}
	public void setFurnished(byte furnished) {
		this.furnished = furnished;
	}
	public Byte getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(Byte isApproved) {
		this.isApproved = isApproved;
	}
	public List<RePropertyUnitsTO> getAssetList() {
		return assetList;
	}
	public void setAssetList(List<RePropertyUnitsTO> assetList) {
		this.assetList = assetList;
	}
	public String getAssetCode() {
		return assetCode;
	}
	public void setAssetCode(String assetCode) {
		this.assetCode = assetCode;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnitStatus() {
		return unitStatus;
	}
	public void setUnitStatus(String unitStatus) {
		this.unitStatus = unitStatus;
	}
	public String getUnitFullInfo() {
		return unitFullInfo;
	}
	public void setUnitFullInfo(String unitFullInfo) {
		this.unitFullInfo = unitFullInfo;
	}
	public List<RePropertyUnitsTO> getUnitList() {
		return unitList;
	}
	public void setUnitList(List<RePropertyUnitsTO> unitList) {
		this.unitList = unitList;
	}
	public String getSizeWithUom() {
		return sizeWithUom;
	}
	public void setSizeWithUom(String sizeWithUom) {
		this.sizeWithUom = sizeWithUom;
	}
	public String getFurnishedCondition() {
		return furnishedCondition;
	}
	public void setFurnishedCondition(String furnishedCondition) {
		this.furnishedCondition = furnishedCondition;
	}
	public Long getLookupDetailId() {
		return lookupDetailId;
	}
	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}
	public String getUnitNameArabic() {
		return unitNameArabic;
	}
	public void setUnitNameArabic(String unitNameArabic) {
		this.unitNameArabic = unitNameArabic;
	}
	
}
