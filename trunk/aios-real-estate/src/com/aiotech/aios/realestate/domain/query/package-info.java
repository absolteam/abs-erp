@org.hibernate.annotations.NamedQueries( {

		@org.hibernate.annotations.NamedQuery(name = "getAllPropertyTypes", query = "SELECT p FROM PropertyType p ORDER BY type"),

		@org.hibernate.annotations.NamedQuery(name = "getPropertiesCount", query = "SELECT COUNT(p.propertyId) FROM Property p"),

		@org.hibernate.annotations.NamedQuery(name = "getAllProperties", query = "SELECT p FROM Property p "
				+ " WHERE p.implementation=? and (p.isApprove=1 or p.isApprove=4) "),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllPropertiesReport", query = "SELECT p FROM Property p "
				+ " JOIN FETCH p.propertyType pt "
				+ " JOIN FETCH p.components c "
				+ " WHERE p.implementation=? and (p.isApprove=1 or p.isApprove=4) ORDER BY pt,p"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllPropertiesSizeReport", query = "SELECT p FROM Property p "
				+ " JOIN FETCH p.propertyType pt "
				+ " JOIN FETCH p.components c "
				+ " WHERE p.implementation=? and p.status=? and (p.isApprove=1 or p.isApprove=4) ORDER BY pt,p "),
				
		@org.hibernate.annotations.NamedQuery(name = "getComponenetProperties", query = "SELECT p FROM Property p "
			+ " JOIN p.propertyType pt WHERE p.implementation=? and (p.isApprove=1 or p.isApprove=4) AND (pt.type!='Villa' AND pt.type!='Empty Land'"
			+ " AND pt.type!='Shop')"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getOfferProperties", query = "SELECT p FROM Property p "
			+ " JOIN FETCH p.propertyType pt "
			+ " WHERE p.implementation=? and (p.isApprove=1 or p.isApprove=4)"),	
	
		@org.hibernate.annotations.NamedQuery(name = "getAllPeopleWithTypeOwner", query = "SELECT Distinct(p) FROM Person p "
				+ "LEFT JOIN FETCH p.personTypeAllocations type "
				+ "LEFT JOIN FETCH type.lookupDetail ld "
				+ "WHERE ld.dataId = 3 AND p.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getOwners", query = "SELECT po FROM PropertyOwnership po "
				+ "JOIN FETCH po.property parent "
				+ "WHERE parent.propertyId = ? AND po.details = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getOwnerCompanies", query = "SELECT Distinct(c) FROM Company c "
				+ "LEFT JOIN FETCH c.companyTypeAllocations type "
				+ "LEFT JOIN FETCH type.lookupDetail ld "
				+ "WHERE ld.dataId = 2 AND c.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaintenanceCompanies", query = "SELECT Distinct(c) FROM Company c "
			+ "LEFT JOIN FETCH c.companyTypeAllocations type "
			+ "LEFT JOIN FETCH type.lookupDetail ld "
			+ "WHERE ld.dataId = 10 AND c.implementation = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPropertyDetails", query = "SELECT pd FROM PropertyDetail pd "
				+ "JOIN FETCH pd.property parent "
				+ "LEFT JOIN FETCH pd.lookupDetail ld "
				+ "WHERE parent.propertyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getMaintenance", query = "SELECT m FROM Maintenance m "
				+ "JOIN FETCH m.property parent "
				+ "WHERE parent.propertyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnitFeatureInfo", query = "SELECT Distinct(ut) FROM UnitDetail ut  "
				+ "LEFT JOIN FETCH ut.lookupDetail ld "
				+ "JOIN FETCH ut.unit u " 
				+ "WHERE u.unitId = ? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getComponentDetailList", query = "SELECT Distinct(cd) FROM ComponentDetail cd "
				+ "LEFT JOIN FETCH cd.lookupDetail ld "	
				+ "JOIN FETCH cd.component c " 
				+ "WHERE c.componentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getComponentListPropertyBased", query = "SELECT c FROM Component c "
				+ "JOIN FETCH c.property p " + "WHERE p.propertyId = ? AND (c.isApprove=1 OR c.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getUnitListComponentBased", query = "SELECT u FROM Unit u "
				+ "JOIN FETCH u.component c " + "WHERE c.componentId = ? AND (u.isApprove=1 OR u.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getComponentsWithProperty", query = "SELECT c FROM Component c "
				+ "JOIN FETCH c.property p " + "WHERE c.componentId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getUnitComponentsWithProperty", query = "SELECT u FROM Unit u "
				+ " JOIN FETCH u.component c "
				+ " JOIN FETCH c.property p"
				+ " JOIN FETCH u.unitType ut "
				+ " WHERE u.unitId = ? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getUnitInformation", query = "SELECT Distinct(u) FROM Unit u "
			+ " JOIN FETCH u.component c "
			+ " JOIN FETCH c.property p"
			+ " JOIN FETCH u.unitType ut "
			+ " LEFT JOIN FETCH u.unitDetails ud "
			+ " LEFT JOIN FETCH u.assets ass "
			+ " LEFT JOIN FETCH u.offerDetails od "
			+ " WHERE u.unitId = ? "),

		@org.hibernate.annotations.NamedQuery(name = "getOfferPropertyDetails", query = "SELECT p FROM Property p "
				+ " LEFT JOIN FETCH p.components c"
				+ " LEFT JOIN FETCH c.componentType ct "
				+ " LEFT JOIN FETCH c.units u  LEFT JOIN FETCH u.unitType ut"
				+ " WHERE p.propertyId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getOffersAll", query = "SELECT o FROM Offer o "
				+ " where o.implementation=? and (o.isApprove=1 or o.isApprove=4)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getOfferNumber", query = "SELECT o FROM Offer o "
			+ " where o.implementation=?"),

		/*@org.hibernate.annotations.NamedQuery(name = "getOfferTenantDetails", query = "SELECT p FROM Person p " +
				" LEFT JOIN FETCH p.identities id  "
				+ " WHERE p.personType in (select pa from PersonType pa "
				+ " where pa.typeCode='TT') and p.implementation = ?"),	*/
				
		@org.hibernate.annotations.NamedQuery(name = "getOfferTenantDetails", query = "SELECT Distinct(p) FROM Person p " 
				+ " LEFT JOIN FETCH p.identities id  "
				+ " LEFT JOIN FETCH p.personTypeAllocations type "
				+ " LEFT JOIN FETCH type.lookupDetail ld "
				+ " WHERE ld.accessCode='TT' and p.implementation = ?"),		
		
		@org.hibernate.annotations.NamedQuery(name="getAllTenantCompanies", query = "SELECT Distinct(c) FROM Company c "
				+ " LEFT JOIN FETCH c.companyTypeAllocations type "
				+ " LEFT JOIN FETCH type.lookupDetail ld "
				+" WHERE c.implementation=? AND ld.dataId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getComponentsAll", query = "SELECT c FROM Component c "
				+ " JOIN FETCH c.property p where c.implementation=? and (c.isApprove=1 or c.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getComponents", query = "SELECT c FROM Component c "
				+ " JOIN FETCH c.property p" 
				+ " JOIN FETCH c.componentType ct" 
				+ " where c.property.propertyId=p.propertyId and c.componentType.componentTypeId=ct.componentTypeId and " 
				+ " c.implementation=? and (c.isApprove=1 or c.isApprove=4)"),		
				
		@org.hibernate.annotations.NamedQuery(name = "getTenantOfferInfo", query = "SELECT t FROM TenantOffer t "
				+ "LEFT JOIN FETCH t.tenantGroup tg " 
				+ "JOIN FETCH t.offer o " + "WHERE o.offerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getPersonInfo", query = "SELECT p FROM Person p, "
				+ " TenantOffer t "
				+ "WHERE t.person = ? AND t.person.personId=p.personId"),

		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentList", query = "SELECT cp FROM ContractPayment cp "
				+ "JOIN FETCH cp.contract c " + "WHERE c.contractId = ?"),	
				
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentListPaidOnly", query = "SELECT cp FROM ContractPayment cp "
				+ "JOIN FETCH cp.contract c " + "WHERE c.contractId = ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractByTenant", query = "SELECT c FROM Contract c "
			+" LEFT JOIN FETCH c.offer o LEFT JOIN FETCH o.tenantOffers to "
			+ " WHERE to.person.personId=?"),		

		@org.hibernate.annotations.NamedQuery(name = "getOfferDetailList", query = "SELECT ot FROM OfferDetail ot "
				+ "JOIN FETCH ot.offer o " + "WHERE o.offerId = ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllContracts", query = "SELECT DISTINCT(c) FROM Contract c "
				+ " JOIN FETCH c.offer o " 
				+ " JOIN FETCH o.offerDetails od "
				+ " JOIN FETCH o.tenantOffers to "
				+ " LEFT JOIN FETCH to.person p "
				+ " LEFT JOIN FETCH to.company cmp "
				+ " LEFT JOIN FETCH c.contractPayments cps "
 				+ " LEFT JOIN FETCH c.cancellations can "
				+ " LEFT JOIN FETCH c.releases rels "
				+ "WHERE c.implementation=? and (c.isApprove=1 or c.isApprove=4)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractInfoForTransaction", query = "SELECT DISTINCT(c) FROM Contract c "
				+ " LEFT JOIN FETCH c.offer o " 
				+ " LEFT JOIN FETCH o.offerDetails od "
				+ " LEFT JOIN FETCH o.tenantOffers to "
				+ " LEFT JOIN FETCH to.person p "
				+ " LEFT JOIN FETCH to.company cmp "
				+ " LEFT JOIN FETCH c.cancellations can "
				+ " LEFT JOIN FETCH c.releases res "
				+ " LEFT JOIN FETCH c.contractPayments cpms "
				+ "WHERE c.contractId=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractTransaction", query = "SELECT c FROM Contract c "
			+ " LEFT JOIN FETCH c.contractPayments cp " 
			+ " WHERE cp.contractPaymentId=? "),	
			
		@org.hibernate.annotations.NamedQuery(name = "getContractInfo", query = "SELECT c FROM Contract c "
			+ " JOIN FETCH c.contractPayments cp " 
			+ " JOIN c.offer o"
			+ " WHERE c.contractId=? "),	
		
		@org.hibernate.annotations.NamedQuery(name = "getContractOffer", query = "SELECT c FROM Contract c "
			+ " LEFT JOIN FETCH c.offer o " 
			+ " WHERE c.contractId=? "),	
			
		@org.hibernate.annotations.NamedQuery(name = "getOfferAllDetail", query = "SELECT o FROM Offer o " 
			+ " JOIN FETCH o.offerDetails od "
			+ " JOIN FETCH o.tenantOffers to "
			+ " WHERE o = ?"),		
				
		@org.hibernate.annotations.NamedQuery(name = "getFilterContracts", query = "SELECT c FROM Contract c "
			+ "JOIN FETCH c.offer o " +
			" JOIN FETCH o.offerDetails od " +
			" JOIN FETCH o.tenantOffers to "
			+ "WHERE c.implementation=? AND o.startDate>=? AND o.endDate<=? and (c.isApprove=1 or c.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getFilterContracts(4)", query = "SELECT c FROM Contract c "
			+ "JOIN FETCH c.offer o " +
			" JOIN FETCH o.offerDetails od " +
			" JOIN FETCH od.unit u " +
			" JOIN FETCH o.tenantOffers to "
			+ "WHERE c.implementation=? AND o.startDate>=? AND o.endDate<=? AND u.unitId=?  and (c.isApprove=1 or c.isApprove=4)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getFilterContracts(2)", query = "SELECT c FROM Contract c "
			+ "JOIN FETCH c.offer o " +
			" JOIN FETCH o.offerDetails od " +
			" JOIN FETCH od.unit u " +
			" JOIN FETCH o.tenantOffers to "
			+ "WHERE c.implementation=? AND u.unitId=?  and (c.isApprove=1 or c.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getFilterContractsMonths", query = "SELECT c FROM Contract c "
			+ "JOIN FETCH c.offer o " + "WHERE c.implementation=? AND o.startDate>=? AND o.startDate<=? and (c.isApprove=1 or c.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllApprovedContracts", query = "SELECT c FROM Contract c"
			+ " JOIN FETCH c.offer o " +
			" JOIN FETCH o.offerDetails od " +
			" JOIN FETCH o.tenantOffers to " +
			" WHERE c.implementation=? and (c.isApprove=1 or c.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getTenantContracts", query = "SELECT to FROM TenantOffer to " 
			+ " LEFT JOIN FETCH to.person pr"
			+ " LEFT JOIN FETCH to.company cp "
		    + " WHERE to.offer.offerId=?"),	
		    
	    @org.hibernate.annotations.NamedQuery(name = "getAllTenantOffers", query = "SELECT to FROM TenantOffer to "
			+ " LEFT JOIN FETCH to.person pr LEFT JOIN FETCH to.company cp "
		    + " WHERE to.offer.implementation = ?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getAllContractsExceptIsApproveValidation", query = "SELECT c FROM Contract c "
			+ "JOIN FETCH c.offer o " + "WHERE c.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllContractDetails", query = "SELECT c FROM Contract c "
				+ " JOIN FETCH c.offer o "
				+ " JOIN FETCH o.offerDetails od "
				+ " JOIN FETCH o.tenantOffers to "
				+ " LEFT JOIN FETCH c.contractPayments cp "
				+ " WHERE c.contractId = ? AND c.offer.offerId = o.offerId AND o.offerId = od.offer.offerId AND   "
				+ " to.offer.offerId = o.offerId "),

		@org.hibernate.annotations.NamedQuery(name = "getPropertyByCompId", query = "SELECT c FROM Component c "
				+ " JOIN FETCH c.property p "
				+ " JOIN FETCH p.propertyType pt "
				+ " WHERE  c.property.propertyId = p.propertyId AND "
				+ " pt.propertyTypeId = p.propertyType.propertyTypeId AND "
				+ " c.componentId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getPropertyByUnitId", query = "SELECT u FROM Unit u"
				+ " JOIN FETCH u.component c "
				+ " JOIN FETCH c.property p "
				+ " JOIN FETCH p.propertyType pt"
				+ " WHERE u.component.componentId = c.componentId AND"
				+ " pt.propertyTypeId = p.propertyType.propertyTypeId AND "
				+ " c.property.propertyId = p.propertyId " + " AND u.unitId=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getPropertyWithPropertyType", query = "SELECT p FROM Property p" +
				" JOIN FETCH p.propertyType pt " +
				" WHERE pt.propertyTypeId = p.propertyType.propertyTypeId AND" +
				" p.propertyId=?"),
		
		@org.hibernate.annotations.NamedQuery(name = "getAllContractsNOffers", query = "SELECT c FROM Contract c "
			+ " JOIN FETCH c.offer o "
			+ " JOIN FETCH o.offerDetails od "
			+ " JOIN FETCH o.tenantOffers to "
			+ " JOIN FETCH to.person p "
			+ " JOIN FETCH c.contractPayments cp "
			//+ " JOIN FETCH p.personType pt "
			+ " WHERE o.offerId = ? AND c.offer.offerId = o.offerId AND o.offerId = od.offer.offerId AND   "
			+ " to.offer.offerId = o.offerId AND to.person.personId = p.personId AND "
			+ " c.contractId = cp.contract.contractId "),

		@org.hibernate.annotations.NamedQuery(name = "getAllOffers", query = "SELECT o FROM Offer o "
				+ " WHERE o.implementation=? and (o.isApprove=1 or o.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllOfferedUnits", query = "SELECT DISTINCT u FROM Unit u "
			+ " JOIN  u.offerDetails od JOIN od.offer o"
		    + " WHERE u.implementation = ? ORDER BY u.unitId"),	
				
		@org.hibernate.annotations.NamedQuery(name = "getOffersWithinPeriod", query = "SELECT o FROM Offer o "
			+ " WHERE o.implementation=? and (o.isApprove=1 or o.isApprove=4)"
			+ " and o.startDate >= ? and o.endDate <= ?"),
											
		@org.hibernate.annotations.NamedQuery(name = "getUnOffered", query = "SELECT o FROM Offer o "
		+" LEFT JOIN FETCH o.contracts c WHERE c.offer.offerId IS NULL"
		+" AND o.implementation=? and (o.isApprove=1 or o.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getAllUnitList", query = "SELECT u FROM Unit u "
				+ " JOIN FETCH u.component c "
				+ " JOIN c.property p WHERE u.implementation=? and (u.isApprove=1 or u.isApprove=4)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllUnitListWithStatus", query = "SELECT u FROM Unit u "
				+ " JOIN FETCH u.component c "
				+ " JOIN c.property p WHERE u.implementation=? and u.status=? and (u.isApprove=1 or u.isApprove=4)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractUnits", query = "SELECT DISTINCT u FROM Unit u "
			+ " JOIN  u.offerDetails od JOIN od.offer o"
		    + " JOIN  o.contracts c WHERE u.implementation=? ORDER BY u.unitId"),		
		    
	    @org.hibernate.annotations.NamedQuery(name = "getContractByUnit", query = "SELECT c FROM Contract c "
			+ " JOIN c.offer o JOIN o.offerDetails od  WHERE od.unit.unitId=? AND o.endDate >= ?"),    
			
		@org.hibernate.annotations.NamedQuery(name = "getInactiveContractByUnit", query = "SELECT c FROM Contract c "
			+ " JOIN c.offer o JOIN o.offerDetails od  WHERE od.unit.unitId=? AND o.endDate < ?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllComponentList", query = "SELECT c FROM Component c "
				+ " JOIN FETCH c.property p " + " WHERE c.implementation=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllRenewContracts", query = "SELECT DISTINCT(c) FROM Contract c "
			+ " JOIN FETCH c.offer o " 
			+ " JOIN FETCH o.offerDetails od "
			+ " JOIN FETCH o.tenantOffers to "
			+ " LEFT JOIN FETCH to.person p "
			+ " LEFT JOIN FETCH to.company cmp "
			+ " LEFT JOIN FETCH c.releases re "
			+ " LEFT JOIN FETCH c.cancellations can "
			+ "WHERE c.implementation=? " +
			" AND ((o.endDate>=? AND o.endDate<=?) or(o.endDate<=?)) AND re is NULL  AND (c.isRenewal is null OR c.isRenewal = 0)"),
				
		@org.hibernate.annotations.NamedQuery(name = "getAllRenewContractsWOImp", query = "SELECT c FROM Contract c "
					+ "JOIN FETCH c.offer o "
					+ "WHERE ((o.endDate>=? AND o.endDate<?) or(o.endDate<=?)) AND c.isRenewal is null "),
					
		@org.hibernate.annotations.NamedQuery(name = "getContractPropertyInfoForLegal", query = "SELECT c FROM Contract c "
			+ " JOIN FETCH c.offer o " 
			+ " JOIN FETCH o.offerDetails od "
			+ " JOIN FETCH o.tenantOffers to "
			+ " LEFT JOIN FETCH od.property pt "
			+ " LEFT JOIN FETCH od.unit ut "
			+ " LEFT JOIN FETCH ut.component cmp "
			+ " LEFT JOIN FETCH cmp.property pro "
			+ " WHERE c.contractId=? "),

		@org.hibernate.annotations.NamedQuery(name = "getAllReleaseList", query = "SELECT r FROM Release r "
				+ "JOIN FETCH r.contract c "
				+ "LEFT JOIN FETCH c.cancellations can "
				+ "JOIN c.offer o "
				+ "WHERE r.implementation=? and (r.isApprove=1 or r.isApprove=4)"),

		@org.hibernate.annotations.NamedQuery(name = "getReleaseListWithAll", query = "SELECT r FROM Release r "
				+ " LEFT JOIN FETCH r.releaseDetails rd "
				+ " LEFT JOIN FETCH r.contract c "
				+ " LEFT JOIN FETCH c.offer o "
				+ " LEFT JOIN FETCH o.offerDetails ofd"
				+ " LEFT JOIN FETCH o.tenantOffers to "
				+ " LEFT JOIN FETCH to.person p "
				//+ " LEFT JOIN FETCH p.personType pt"
				+ " WHERE r.releaseId = ? "
				+ " AND  r.contract.contractId = c.contractId "
				+ " AND  c.offer.offerId = o.offerId"
				+ " AND to.offer.offerId = o.offerId"
				),

		@org.hibernate.annotations.NamedQuery(name = "getReleaseInfoForBankDeposit", query = "SELECT Distinct(r) FROM Release r "
			+ " LEFT JOIN FETCH r.releaseDetails rd " 
			+ " LEFT JOIN FETCH r.contract c "
			+ " LEFT JOIN FETCH c.contractPayments cp "
			+ " LEFT JOIN FETCH c.cancellations can "
			+ " LEFT JOIN FETCH c.offer o "
			+ " LEFT JOIN FETCH o.offerDetails ofd"
			+ " LEFT JOIN FETCH o.tenantOffers to "
			+ " LEFT JOIN FETCH to.person p "
			//+ " LEFT JOIN FETCH p.personType pt"
			+ " WHERE r.releaseId = ? "
			
			),
		
		@org.hibernate.annotations.NamedQuery(name = "getReleaseFullInformation", query = "SELECT Distinct(r) FROM Release r "
			+ " LEFT JOIN FETCH r.releaseDetails rd " 
			+ " LEFT JOIN FETCH r.contract c "
			+ " LEFT JOIN FETCH c.contractPayments cp "
			+ " LEFT JOIN FETCH c.cancellations can "
			+ " LEFT JOIN FETCH c.offer o "
			+ " LEFT JOIN FETCH o.offerDetails ofd"
			+ " LEFT JOIN FETCH o.tenantOffers to "
			+ " LEFT JOIN FETCH to.person p "
			+ " LEFT JOIN FETCH to.company cmp "
			+ " WHERE r.releaseId = ? "
		),
				
			
			
		@org.hibernate.annotations.NamedQuery(name = "getAllReleaseInfoForBankDeposit", query = "SELECT r FROM Release r "
			+ " LEFT JOIN FETCH r.releaseDetails rd " 
			+ " LEFT JOIN FETCH r.contract c "
			+ " LEFT JOIN FETCH c.contractPayments cp "
			+ " LEFT JOIN FETCH c.cancellations can "
			+ " LEFT JOIN FETCH c.offer o "
			+ " LEFT JOIN FETCH o.offerDetails ofd"
			+ " LEFT JOIN FETCH o.tenantOffers to "
			+ " LEFT JOIN FETCH to.person p "
			//+ " LEFT JOIN FETCH p.personType pt"
			+ " WHERE r.implementation = ? "
			+ " AND  r.contract.contractId = c.contractId "
			+ " AND  c.offer.offerId = o.offerId"
			+ " AND to.offer.offerId = o.offerId"
			),
					
		@org.hibernate.annotations.NamedQuery(name = "getAllReleaseDetailList", query = "SELECT rd FROM ReleaseDetail rd "
				+ "JOIN FETCH rd.release r WHERE r.releaseId=?"),
		
		@org.hibernate.annotations.NamedQuery(name = "getBankDepositReleaseDetailInfo", query = "SELECT rd FROM ReleaseDetail rd "
			+ " WHERE rd.releaseDetailsId = ?"),
	
		@org.hibernate.annotations.NamedQuery(name = "getReleaseInfo", query = "SELECT r FROM Release r"
			+ " JOIN FETCH r.releaseDetails rd"
			+ " JOIN FETCH r.contract c"
			+ " WHERE r.releaseId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getOfferDetailLines", query = "SELECT od FROM OfferDetail od "
				+ " JOIN FETCH od.offer o"
				+ " LEFT JOIN FETCH od.unit u"
				+ " LEFT JOIN FETCH u.component uc"
				+ " LEFT JOIN FETCH uc.property"
				+ " LEFT JOIN FETCH od.property p"
				+ " LEFT JOIN FETCH od.component c"
				+ " WHERE o.offerId=? AND od.offer.offerId=o.offerId"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractPayments", query = "SELECT cp FROM ContractPayment cp "
			+ " JOIN FETCH cp.contract co"
			+ " JOIN FETCH co.offer oe" 
 			+ " WHERE oe.endDate >= ?"
			+ " AND (co.isRenewal IS NULL OR co.isRenewal =1)"
			+ " AND cp.feeType = 3"
			+ " AND cp.isDeposited = 1"
			+ " AND cp.chequeDate IS NOT NULL"
			+ " AND cp.chequeDate <= ?"),		
			
		@org.hibernate.annotations.NamedQuery(name = "getContractRentPayment", query = "SELECT cp FROM ContractPayment cp "
			+ " WHERE cp.contract.contractId=? AND cp.feeType = 3 ORDER BY cp.chequeDate"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentByPaymentId", query = "SELECT cp FROM ContractPayment cp "
			+ " JOIN FETCH cp.contract co"
			+ " JOIN FETCH co.offer oe" 
			+ " WHERE cp.contractPaymentId = ?"),		
				
		@org.hibernate.annotations.NamedQuery(name = "getOfferProperty", query = "SELECT od FROM OfferDetail od "
			+ " WHERE od.property.propertyId=? AND od.property.status<>1"),		
			
		@org.hibernate.annotations.NamedQuery(name = "getOfferUnit", query = "SELECT od FROM OfferDetail od "
			+ " JOIN FETCH od.offer o"
			+ " JOIN FETCH od.unit u"
			+ " WHERE u.unitId = ? AND u.status <> 1"),	
			
			@org.hibernate.annotations.NamedQuery(name = "getOfferByUnitWithinPeriod", query = "SELECT od FROM OfferDetail od "
				+ " JOIN FETCH od.offer o"
				+ " JOIN FETCH od.unit u"
				+ " WHERE o.implementation = ? AND u.unitId = ? AND u.status <> 1"
				+ " AND o.startDate >= ? AND o.endDate <= ?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getOfferByPersonTenant", query = "SELECT to FROM TenantOffer to "
			+ " JOIN FETCH to.offer o"
			+ " JOIN FETCH to.person p"
			+ " WHERE o.implementation = ? AND p.personId = ?"),
			
			@org.hibernate.annotations.NamedQuery(name = "getOfferByPersonTenantWithinPeriod", query = "SELECT to FROM TenantOffer to "
				+ " JOIN FETCH to.offer o"
				+ " JOIN FETCH to.person p"
				+ " WHERE o.implementation = ? AND p.personId = ?"
				+ " AND o.startDate >= ? AND o.endDate <= ?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getOfferByCompanyTenant", query = "SELECT to FROM TenantOffer to "
			+ " JOIN FETCH to.offer o"
			+ " JOIN FETCH to.company c"
			+ " WHERE o.implementation = ? AND c.companyId = ?"),
			
			@org.hibernate.annotations.NamedQuery(name = "getOfferByCompanyTenantWithinPeriod", query = "SELECT to FROM TenantOffer to "
				+ " JOIN FETCH to.offer o"
				+ " JOIN FETCH to.company c"
				+ " WHERE o.implementation = ? AND c.companyId = ?"
				+ " AND o.startDate >= ? AND o.endDate <= ?"),

		@org.hibernate.annotations.NamedQuery(name = "getTenantOffer", query = "SELECT to FROM TenantOffer to "
				+ "LEFT JOIN FETCH to.tenantGroup "
				+ "JOIN FETCH to.offer o WHERE o.offerId=?"),  

		@org.hibernate.annotations.NamedQuery(name = "getOfferPersonDetails", query = "SELECT p FROM Person p "
				+ " LEFT JOIN FETCH p.identities id WHERE p.personId=? "),
				
		@org.hibernate.annotations.NamedQuery(name = "getOfferCompanyDetails", query = "SELECT c FROM Company c "
			+ "  WHERE c.companyId=?"),		

		@org.hibernate.annotations.NamedQuery(name = "getAllOfferComponents", query = "SELECT od FROM OfferDetail od "
				+ " LEFT JOIN FETCH od.component c LEFT JOIN c.componentType ct LEFT JOIN c.property p"
				+ " WHERE od.component.componentId=c.componentId AND od.offer.offerId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOfferProperty", query = "SELECT od FROM OfferDetail od "
				+ " JOIN FETCH od.property p LEFT JOIN FETCH p.propertyType pt WHERE od.property.propertyId=p.propertyId "
				+ " AND p.propertyType.propertyTypeId=pt.propertyTypeId"
				+ " AND od.offer.offerId=?"),

		@org.hibernate.annotations.NamedQuery(name = "getAllOfferUnits", query = "SELECT od FROM OfferDetail od "
				+ " JOIN FETCH od.unit u LEFT JOIN FETCH u.unitType ut WHERE od.unit.unitId=u.unitId AND od.offer.offerId=?"), 
				
		@org.hibernate.annotations.NamedQuery(name = "getComponentTypeDetails", query = "SELECT c FROM Component c "
			+ " LEFT JOIN FETCH c.property p LEFT JOIN FETCH c.componentType ct WHERE c.componentId=?"), 		
				
		@org.hibernate.annotations.NamedQuery(name = "getComponentById", query = "SELECT c FROM Component c"
				+ " JOIN FETCH c.property WHERE c.componentId = ?"), 	
				
		@org.hibernate.annotations.NamedQuery(name = "getAssetDetailsByUnit", query = "SELECT a FROM Asset a "
			+ " JOIN FETCH a.product p "
			+ " JOIN FETCH p.lookupDetailByProductUnit pu " 
			+ " WHERE a.unit.unitId=?"), 
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyOwners", query = "SELECT po FROM PropertyOwnership po"
			+ "  WHERE po.property = ?"), 
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyOwnersByPropertyId", query = "SELECT po FROM PropertyOwnership po"
			+ "  WHERE po.property.propertyId = ?"), 
		
		@org.hibernate.annotations.NamedQuery(name = "getAllTenantGroups", query = "SELECT t FROM TenantGroup t "
			+ " WHERE t.implementation=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllLegalListRecordBased", query = "SELECT l FROM Legal l "
			+ " WHERE l.recordId=?"),
		//Cancellation Process	
		@org.hibernate.annotations.NamedQuery(name = "getAllCancellation", query = "SELECT ca FROM Cancellation ca "
			+ "JOIN FETCH ca.contract c "
			+ "JOIN c.offer o "
			+ "WHERE ca.implementation=? and (ca.isApprove=1 or ca.isApprove=4)"),
			
		@org.hibernate.annotations.NamedQuery(name = "getCancellationPassingContract", query = "SELECT c FROM Cancellation c "
			+ " WHERE c.contract=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getReleasePassingContract", query = "SELECT r FROM Release r "
			+ " WHERE r.contract=?"),
				
		//Contract Payment getContractPaymentsChequeDeposit
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentsChequeDeposit", query = "SELECT cp FROM ContractPayment cp"
			+ " JOIN FETCH cp.contract c WHERE " 
			+ " cp.chequeDate>=? AND cp.chequeDate<=? AND cp.chequeNo is not null " 
			+ " AND (c.isApprove=1 or c.isApprove=4) AND cp.chequeDate is not null"),
			
		@org.hibernate.annotations.NamedQuery(name = "getOwnersBasedOnCompanyORPerson", query = "SELECT po FROM PropertyOwnership po "
			+ " WHERE po.personalId=? and po.details=? "),
						
		@org.hibernate.annotations.NamedQuery(name = "getTenantBasedOnCompany", query = "SELECT to FROM TenantOffer to "
			+ " WHERE to.company.companyId=?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getTenantBasedOnPerson", query = "SELECT to FROM TenantOffer to "
			+ " WHERE to.person.personId=?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentsBasedOnStatus", query = "SELECT cp FROM ContractPayment cp "
			+ "JOIN FETCH cp.contract c JOIN FETCH c.offer o " + "WHERE c.offer.offerId=o.offerId And c.implementation = ? and cp.paymentStatus=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentsAll", query = "SELECT cp FROM ContractPayment cp "
			+ "JOIN FETCH cp.contract c JOIN FETCH c.offer o " + "WHERE c.offer.offerId=o.offerId And c.implementation = ?"),
		
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentsBasedMoreCond", query = "SELECT cp FROM ContractPayment cp "
			+ "JOIN FETCH cp.contract c JOIN FETCH c.offer o " 
			+ "WHERE c.offer.offerId=o.offerId And c.implementation = ? and cp.paymentStatus=? " 
			+ " and c.createdDate>=? AND c.createdDate<=?"),
				
		@org.hibernate.annotations.NamedQuery(name = "getContractPaymentsAllMoreCond", query = "SELECT cp FROM ContractPayment cp "
			+ "JOIN FETCH cp.contract c JOIN FETCH c.offer o " + "WHERE c.offer.offerId=o.offerId " +
			" And c.implementation = ? and c.createdDate>=? AND c.createdDate<=?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyOfferDetailList", query = "SELECT od FROM OfferDetail od"
			+" JOIN FETCH od.offer o"
			+" JOIN FETCH o.tenantOffers to"
			+" JOIN FETCH o.contracts cn"
			+" LEFT JOIN FETCH od.unit u" 
			+" JOIN FETCH u.component c" 
			+" JOIN FETCH c.property p"
			+" LEFT JOIN FETCH  od.property p1"
			+" WHERE (p1.propertyId=? OR p.propertyId=?) AND o.startDate>=? AND o.endDate<=? order by u "	
		),
		
		@org.hibernate.annotations.NamedQuery(name = "getPropertyUnitRentalContractPayments", query = "SELECT cp FROM ContractPayment cp "
			+ "WHERE cp.paymentStatus=1 " +
			" And cp.contract.contractId = ? and cp.paidDate>=? AND cp.paidDate<=?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyRentalOfferDetailList", query = "SELECT od FROM OfferDetail od"
			+" JOIN FETCH od.offer o"
			+" JOIN FETCH o.tenantOffers to"
			+" JOIN FETCH o.contracts cn"
			+" LEFT JOIN FETCH od.unit u" 
			+" JOIN FETCH u.component c" 
			+" JOIN FETCH c.property p"
			+" LEFT JOIN FETCH  od.property p1"
			+" WHERE o.implementation=? AND o.startDate>=? AND o.endDate<=? order by p.addressOne,p1.addressOne,p.propertyId,p1.propertyId "	
		),
		
		@org.hibernate.annotations.NamedQuery(name = "getPropertyUnitVacancyList", query = "select u from Unit u"
				+" left join fetch u.component c " 
				+" left join fetch c.property p " 
				+" left join fetch u.offerDetails od " 
				+" left join fetch od.offer o " 
				+" left join fetch o.contracts cn " 
				+" left join fetch o.tenantOffers to " 
				+" where p.propertyId=?  and ( u.status=1  or ( " 
				+" ((u.status=1 or u.status is null) or o.endDate<? ) " 
				+" and  " 
				+" ( " 
				+" od.offerDetailId in " 
				+" ( " 
				+" select max(od.offerDetailId) from OfferDetail od " 
				+" join  od.unit u " 
				+" join  u.component c " 
				+" join  c.property p " 
				+" join  od.offer o " 
				+" join  o.contracts cn "
				+" where p.propertyId=?  " 
				+" group by u " 
				+" ))))"	
		),
		
		@org.hibernate.annotations.NamedQuery(name = "getTenantOfferDetailsOnlyCompany", query = "SELECT to FROM TenantOffer to "
			+ " JOIN FETCH to.company c WHERE to.offer.offerId=? AND c.companyId=? "),	
			
		@org.hibernate.annotations.NamedQuery(name = "getTenantOfferDetailsOnlyPerson", query = "SELECT to FROM TenantOffer to "
			+ " JOIN FETCH to.person p WHERE to.offer.offerId=? AND p.personId=?"),
		
		@org.hibernate.annotations.NamedQuery(name = "getOfferList", query = "SELECT o FROM Offer o "
			+" JOIN FETCH o.tenantOffers to"
			+" JOIN FETCH o.offerDetails od"
			+ " where o.implementation=? and (o.isApprove=1 or o.isApprove=4)"),		
			
		@org.hibernate.annotations.NamedQuery(name = "getAllPropertyExpense", query = "SELECT p FROM PropertyExpense p "
			+ " JOIN FETCH p.property pr" 
			+ " WHERE pr.implementation = ?"),	
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyExpenseById", query = "SELECT p FROM PropertyExpense p " 
			+" JOIN FETCH p.propertyExpenseDetails pd"
			+" JOIN FETCH p.property pr " 
			+" WHERE p.expenseId = ?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getPropertyExpenseDetail", query = "SELECT p FROM PropertyExpenseDetail p " 
			+ " WHERE p.propertyExpense.expenseId=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllPropertyDetail", query = "SELECT Distinct(pd) FROM PropertyDetail pd " 
			+ " LEFT JOIN FETCH pd.property p"
			+ " WHERE p.implementation=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllComponentDetail", query = "SELECT Distinct(cd) FROM ComponentDetail cd " 
			+ " LEFT JOIN FETCH cd.component c"
			+ " WHERE c.implementation=?"),
			
		@org.hibernate.annotations.NamedQuery(name = "getAllUnitDetail", query = "SELECT Distinct(ut) FROM UnitDetail ut  "
			+ "LEFT JOIN FETCH ut.unit u " + "WHERE u.implementation = ? "),
				
			
})				
package com.aiotech.aios.realestate.domain.query;