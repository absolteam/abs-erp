package com.aiotech.aios.realestate.service;


import java.util.HashSet;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ComponentService {

	private AIOTechGenericDAO<Component> componentDAO; 
	private AIOTechGenericDAO<ComponentDetail> componentDetailDAO;
	private AIOTechGenericDAO<ComponentType> componentTypeDAO;
	private AIOTechGenericDAO<Property> propertyDAO;
	
	//to get all components
	public List<Component> getComponentList(Implementation implementation) throws Exception {
		List<Component> components = componentDAO.findByNamedQuery("getComponents",implementation); 
		return components; 
	}
	
	//to get all components
	public List<Component> getAllComponents(Implementation implementation) throws Exception {
		List<Component> components = componentDAO.findByNamedQuery("getComponentsAll",implementation); 
		Property property=new Property();
		ComponentType componentType=new ComponentType();
		for(Component list:components){ 
			property = propertyDAO.findByExample(list.getProperty()).get(0);
			componentType=componentTypeDAO.findByExample(list.getComponentType()).get(0);
			list.setProperty(property);
			list.setComponentType(componentType);
		} 
		return components; 
	}

	//to get all properties 
	public List<Property>getAllProperties(Implementation implementation) throws Exception{
		List<Property> property=propertyDAO.findByNamedQuery("getPropertiesAll",implementation);
		return property;
	}

	//To Fetch all the component detail against implementation 
	public List<ComponentDetail>getAllComponentDetail(Implementation implementation) throws Exception{
		return componentDetailDAO.findByNamedQuery("getAllComponentDetail",implementation);
		 
	}
	
	//to get all components
	public List<ComponentType>getAllComponentTypes() throws Exception{
		List<ComponentType>component=componentTypeDAO.getAll();
		return component;
	}
	
	//to get component by id
	public Component getComponentById(Long componentId) throws Exception{
		return componentDAO.findById(componentId);  
	}
	
	public Component getComponentByIdWithProperty(Long componentId) throws Exception{
		try {
			return componentDAO.findByNamedQuery("getComponentById", componentId).get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//to get property 
	public Property getPropertyById(Long propertyId) throws Exception{
		Property property=propertyDAO.findById(propertyId);
		return property;
	}
	
	//to get component type by component id
	public Component getComponentProperty(Long componentId) throws Exception{
		return componentDAO.findByNamedQuery("getComponentTypeDetails", componentId).get(0); 
	}
	
	//to get component type
	public ComponentType getComponentTypeById(Integer compoentTypeId) throws Exception{
		ComponentType compoentType=componentTypeDAO.findById(compoentTypeId);
		return compoentType;
	}
	
	//to get component lines 
	public List<ComponentDetail>getComponentLines(Long compoentId)throws Exception{
		List<ComponentDetail> detailList=componentDetailDAO.findByNamedQuery("getComponentDetailList", compoentId);
		return detailList;
	}
	
	//to save property components
	public void savePropertyComponents(Component component, List<ComponentDetail> detailList)throws Exception{
		componentDAO.saveOrUpdate(component);
		if(detailList != null) {
			for(ComponentDetail list: detailList){
				list.setComponent(component);
			}
			component.setComponentDetails(new HashSet<ComponentDetail>(detailList));
			componentDetailDAO.saveOrUpdateAll(detailList);
		}
	}
	
	//to delete component details
	public void deletePropertyComponent(Component propertyComponent,List<ComponentDetail> componentDList) throws Exception{
		//List<Component> components = new ArrayList<Component>();
		//components.add(propertyComponent); 
		//List<ComponentDetail> componentDList = new ArrayList<ComponentDetail>();
		//componentDList=componentDetailDAO.findByNamedQuery("getComponentDetailList", propertyComponent.getComponentId()); 
		if(componentDList != null)
			componentDetailDAO.deleteAll(componentDList);
		componentDAO.delete(propertyComponent);
	}
	
	public void deletePropertyComponentDetail(ComponentDetail componentDetail) throws Exception{
		componentDetailDAO.delete(componentDetail);
	}
	
	public void deleteComponents(List<ComponentDetail> componentDetails) throws Exception{
		componentDetailDAO.deleteAll(componentDetails);
	}
	
	public List<Component> getComponentsForProperty(long propertyId) throws Exception{
		return componentDAO.findByNamedQuery("getComponentListPropertyBased", propertyId);
	}
		
	//Getters&setters
	public AIOTechGenericDAO<Component> getComponentDAO() {
		return componentDAO;
	}
	public void setComponentDAO(AIOTechGenericDAO<Component> componentDAO) {
		this.componentDAO = componentDAO;
	}
	public AIOTechGenericDAO<ComponentDetail> getComponentDetailDAO() {
		return componentDetailDAO;
	}
	public void setComponentDetailDAO(
			AIOTechGenericDAO<ComponentDetail> componentDetailDAO) {
		this.componentDetailDAO = componentDetailDAO;
	}
	public AIOTechGenericDAO<ComponentType> getComponentTypeDAO() {
		return componentTypeDAO;
	}
	public void setComponentTypeDAO(
			AIOTechGenericDAO<ComponentType> componentTypeDAO) {
		this.componentTypeDAO = componentTypeDAO;
	}

	public AIOTechGenericDAO<Property> getPropertyDAO() {
		return propertyDAO;
	}

	public void setPropertyDAO(AIOTechGenericDAO<Property> propertyDAO) {
		this.propertyDAO = propertyDAO;
	}
	
	public List<Component> getPropertyByComponentId (Component comp){
		return componentDAO.findByNamedQuery("getPropertyByCompId", comp.getComponentId());
	}
}
