package com.aiotech.aios.realestate.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.realestate.domain.entity.Legal;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.LegalService;
import com.aiotech.aios.realestate.to.LegalVO;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.to.DocumentVO;

public class LegalBL {
	private DirectoryBL directoryBL;
	private DocumentBL documentBL;
	private LegalService legalService;
	private ContractService contractService;
	public List<LegalVO> getLegalList(Implementation implementation) throws Exception{
		List<LegalVO> legalList=new ArrayList<LegalVO>();
		
		return legalList; 
	}
/*public List<LegalVO> getUseCaseBasedRecords(LegalVO legalVO,Implementation implementation)
 throws Exception {
		List<LegalVO> volist = new ArrayList<LegalVO>();
		List<Object> objects = legalService.getApproved(legalVO.getTableName(), Byte.parseByte("1"), implementation);
		List<Object> objects1 = legalService.getApproved(legalVO.getTableName(), Byte.parseByte("4"), implementation);
		objects.addAll(objects1);
		for (Object object : objects) {
			LegalVO vo = new LegalVO();
			vo.setObject(object);
			// Fetch Record Id
			String[] name = object.getClass().getName().split("entity.");
			String methodId = "get" + name[name.length - 1] + "Id";
			Method method = object.getClass().getMethod(methodId, null);
			vo.setRecordId((Long) method.invoke(object, null));
			String methodName = "get" + name[name.length - 1] + "Name";
			String methodNumber = "get" + name[name.length - 1] + "No";
			try {
				if (object.getClass().getMethod(methodName, null) != null) {
					Method method1 = object.getClass().getMethod(methodName,
							null);
					vo.setRecordName((String) method1.invoke(object, null));
				}
				
			} catch (NoSuchMethodException e) {
				vo.setRecordName("-NA-");
			}
			try {
				if (object.getClass().getMethod(methodNumber,
						null) != null) {
					Method number = object.getClass()
							.getMethod(methodNumber, null);
					vo.setRecordNumber(number.invoke(object,
							null).toString());
				}
			} catch (NoSuchMethodException e) {
				vo.setRecordNumber("-NA-");
			}
			volist.add(vo);

		}
		return volist;
	}*/

	public Object getUseCaseBasedRecords(Legal legalVO) throws Exception {
			Object object = legalService.getRecordObject(legalVO);
			return object;
	}
	public void saveLegal(Object object,Legal legal)throws Exception{
		addLegal(object,legal);
		if(legal.getLegalId()!=0){
			this.saveLegalDocuments(legal,true);
		}else{
			this.saveLegalDocuments(legal,false);

		}
	}
	public void addLegal(Object object,
			Legal legal) throws Exception{
		String[] name = object.getClass().getName().split("entity.");
		legal.setTableName(name[name.length-1]);
		String methodName = "get" +name[name.length-1]+"Id";
		Method method = object.getClass().getMethod(methodName, null);
		legal.setRecordId((Long)method.invoke(object, null));
		legalService.addLegal(legal);
	}
	public void saveLegalDocuments(Legal legal,boolean editFlag) throws Exception {

		DocumentVO docVO = documentBL.populateDocumentVO(legal, "legalDocs", editFlag);
		   Map<String, String> dirs  = docVO.getDirMap();   
		   
		   if (dirs != null){
		    Map<String, Directory> dirStucture = directoryBL.saveDirStructure(dirs,legal.getLegalId(),"Legal","legalDocs");
		    docVO.setDirStruct(dirStucture);
		   } 
		  
		   documentBL.saveUploadDocuments(docVO);
	}
	public List<Legal> getLegals(Object object)throws Exception{
		List<Legal> legals=null;
		Legal legal=new Legal();
		// Fetch Record Id
		String[] name = object.getClass().getName()
				.split("entity.");
		String methodId = "get" + name[name.length - 1]
				+ "Id";
		Method method = object.getClass().getMethod(
				methodId, null);
		legal.setRecordId((Long) method.invoke(object, null));
		legals=legalService.getAllLegal(legal);
		return legals;
	}
	public DocumentBL getDocumentBL() {
		return documentBL;
	}
	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}
	public LegalService getLegalService() {
		return legalService;
	}
	public void setLegalService(LegalService legalService) {
		this.legalService = legalService;
	}
	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}
	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}
}
