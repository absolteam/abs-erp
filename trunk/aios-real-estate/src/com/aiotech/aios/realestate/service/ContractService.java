package com.aiotech.aios.realestate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;


public class ContractService {
	private AIOTechGenericDAO<Contract> contractDAO;
	private AIOTechGenericDAO<ContractPayment> contractPaymentDAO;
	private AIOTechGenericDAO<Component> componentDAO;
	private AIOTechGenericDAO<Property> propertyDAO;
	private AIOTechGenericDAO<Offer> offerDAO;
	private AIOTechGenericDAO<TenantOffer> tenantOfferDAO;
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<OfferDetail> offerDetailDAO;
	private AIOTechGenericDAO<Unit> unitDAO;
	private AIOTechGenericDAO<Release> releaseDAO;
	
	//Contract List
	public List<Contract> getAllContractDetails(Implementation implementation) throws Exception {
		//return contractDAO.getAll();
		return contractDAO.findByNamedQuery("getAllContracts",implementation);
	}
	
	//Contract Filter List
	public List<Contract> getFilterContractDetails(Implementation implementation, Date fromDate, Date toDate) throws Exception {
			return contractDAO.findByNamedQuery("getFilterContracts",implementation, fromDate, toDate);
	}
	public List<Contract> getFilterContractDetails(Implementation implementation) throws Exception {
		return contractDAO.findByNamedQuery("getAllApprovedContracts",implementation);	
	}
	public List<Contract> getFilterContractDetails(Implementation implementation, Date fromDate, Date toDate,Long unitId) throws Exception {
			return contractDAO.findByNamedQuery("getFilterContracts(4)",implementation, fromDate, toDate,unitId);
	}
	public List<Contract> getFilterContractDetails(Implementation implementation,Long unitId) throws Exception {
		return contractDAO.findByNamedQuery("getFilterContracts(2)",implementation,unitId);
	}
	
	//Contract Filter List based on month
	public List<Contract> getFilterContractDetailsMonth(Implementation implementation, Date fromDate, Date toDate) throws Exception {
			return contractDAO.findByNamedQuery("getFilterContractsMonths",implementation, fromDate, toDate);
	}
	
	//Contract Payment getContractPaymentsChequeDeposit
	public List<ContractPayment> getContractPaymentsChequeDeposit(Date currentDate,Date date) throws Exception { 
		return contractPaymentDAO.findByNamedQuery("getContractPaymentsChequeDeposit", currentDate,  date);
	}
	
	//Contract Payment based on the status
	public List<ContractPayment> getAllContractPaymentDetails(Implementation implementation,Byte paymentStatus,Date startDate,Date endDate) throws Exception {
		if(paymentStatus!=null && paymentStatus!=-1 && startDate==null)
			return contractPaymentDAO.findByNamedQuery("getContractPaymentsBasedOnStatus", implementation,  paymentStatus);
		else if(paymentStatus==-1  && startDate==null )
			return contractPaymentDAO.findByNamedQuery("getContractPaymentsAll", implementation);
		else if(paymentStatus!=null && paymentStatus!=-1 && startDate!=null)
			return contractPaymentDAO.findByNamedQuery("getContractPaymentsBasedMoreCond", implementation,  paymentStatus,startDate,endDate);
		else if(paymentStatus==-1  && startDate!=null )
			return contractPaymentDAO.findByNamedQuery("getContractPaymentsAllMoreCond", implementation,startDate,endDate);
		else 
			return null;
	}
	
	public List<Contract> getContractFilterByTenant(Long tenantId) throws Exception{
		return contractDAO.findByNamedQuery("getContractByTenant",tenantId);
	}
	
	public List<Contract> getContractFilterByUnit(Long unitId, Date date, char activeFlag) throws Exception{
		if(activeFlag=='Y')
			return contractDAO.findByNamedQuery("getContractByUnit",unitId,date);
		else
			return contractDAO.findByNamedQuery("getInactiveContractByUnit",unitId,date);
	}
	
	//Contract Tenant List
	public List<TenantOffer> getTenantContractDetails(long offerId)throws Exception{
		return tenantOfferDAO.findByNamedQuery("getTenantContracts",offerId);
	}
	
	//Unit Contract List
	public List<Unit> getAllContractUnits(Implementation implementation) throws Exception{
		return unitDAO.findByNamedQuery("getContractUnits", implementation);
	}
	
	//Contract List
	public List<Contract> getAllContractsExceptIsApproveValidation(Implementation implementation) throws Exception {
		//return contractDAO.getAll();
		return contractDAO.findByNamedQuery("getAllContractsExceptIsApproveValidation",implementation);
	}
	
	//Contract List
	public List<Contract> getAllReleaseRenewContractDetails(Date currentDate,Date date,Implementation implementation) throws Exception {
		//return contractDAO.getAll();
		return contractDAO.findByNamedQuery("getAllRenewContracts",implementation,currentDate,date,currentDate);
	}
	
	//Offer Detail
	public Offer getOfferDetails(long offerid) throws Exception {
		return offerDAO.findById(offerid);
	}
	
	//Contract List
	public List<Offer> getAllOfferList(Implementation implementation) throws Exception { 
		return offerDAO.findByNamedQuery("getAllOffers",implementation); 
	}
	
	//Contract offer List
	public List<Offer> getUnOfferList(Implementation implementation) throws Exception { 
		return offerDAO.findByNamedQuery("getUnOffered",implementation); 
	}
	
	//Tenant Offer Detail
	public List<TenantOffer> getTenantOfferDetails(long offerid) throws Exception {
		return tenantOfferDAO.findByNamedQuery("getTenantOfferInfo",offerid);
	}
	
	//Tenant Offer Detail
	public List<Person> getPersonDetails(Person person) throws Exception {
		return personDAO.findByNamedQuery("getPersonInfo",person);
	}
	
	//Contract info
	public Contract getContractDetails(Long contractId) throws Exception {
		return contractDAO.findById(contractId);
	}
	
	public Contract getContractInfo(Long contractId) throws Exception {
		return contractDAO.findByNamedQuery("getContractInfo", contractId).get(
				0);
	}
	
	public Contract getContractOffer(Long contractId) throws Exception {
		return contractDAO.findByNamedQuery("getContractOffer", contractId).get(
				0);
	}
	
	public Contract getContractInfoForTransaction(Long contractId) throws Exception {
		return contractDAO.findByNamedQuery("getContractInfoForTransaction", contractId).get(
				0);
	}
	
	//Contract Payment List
	public List<ContractPayment> getContractPaymentList(long contractId) throws Exception {
		return contractPaymentDAO.findByNamedQuery("getContractPaymentList",contractId);
	}
	
	//Contract Payment
	public ContractPayment getContractPaymentByPaymentId(long contractPaymentId)
			throws Exception {
		return contractPaymentDAO.findByNamedQuery("getContractPaymentByPaymentId",
				contractPaymentId).get(0);
	}
	
	// Get Contract Rent Payments
	public List<ContractPayment> getContractRentPayment(Long contractId) {
		return contractPaymentDAO.findByNamedQuery("getContractRentPayment",
				contractId);
	}
	
	//Contract Payment List
	public List<ContractPayment> getContractPaymentListPaidOnly(long contractId) throws Exception {
		return contractPaymentDAO.findByNamedQuery("getContractPaymentListPaidOnly",contractId);
	}
	//delete List
	public void deletContract(Contract contract,List<ContractPayment> contractPayments) throws Exception {
		 contractPaymentDAO.deleteAll(contractPayments);
		 contractDAO.delete(contract);
	}
	
	//Offer Detail List
	public List<OfferDetail> getAllOfferDetailList(long offerId) throws Exception {
		return offerDetailDAO.findByNamedQuery("getOfferDetailList",offerId);
	}
	
	
	//Property info
	public Property getPropertyInfo(long propertyId) throws Exception {
		return propertyDAO.findById(propertyId);
	}
	
	//Component info
	public Component getComponentInfo(long componentId) throws Exception {
		List<Component> components=componentDAO.findByNamedQuery("getComponentsWithProperty",componentId);
		if(components!=null && components.size()>0)
			return components.get(0);
		else
			return null;
	}
	
	//Unit info
	public Unit getUnitInfo(long unitId) throws Exception {
		List<Unit> units=unitDAO.findByNamedQuery("getUnitInformation",unitId);
		if(units!=null && units.size()>0)
			return units.get(0);
		else
			return null;
		
	}
	
	//add contract 
	public void addContract(Contract contract,List<ContractPayment> contractPayments) throws Exception {
		contractDAO.saveOrUpdate(contract);
		if(contractPayments!=null){
			for(ContractPayment ul:contractPayments){
				ul.setContract(contract);
			}
		}
		contractPaymentDAO.saveOrUpdateAll(contractPayments);
	}
	
	public void updateContract(Contract contract) throws Exception {
		contractDAO.saveOrUpdate(contract);
	}
	
	public void createRelease(Release release) throws Exception {
		releaseDAO.saveOrUpdate(release);
	}
	
	//Contract details
	public Contract getContractTransaction(long contractId) throws Exception { 
		return contractDAO.findByNamedQuery("getContractTransaction",contractId).get(0); 
	}
	
	//Contract details
	public TenantOffer getTenantInfo(long tenantOfferId) throws Exception { 
		return tenantOfferDAO.findById(tenantOfferId);
	}
	//Property Unit Rental paid with specific period List
	public List<OfferDetail> getPropertyOfferDetailList(Long propertyId,Date fromDate,Date toDate) throws Exception {
		return offerDetailDAO.findByNamedQuery("getPropertyOfferDetailList",propertyId,propertyId,fromDate,toDate);
	}
	//Property Unit Rental paid with specific period List
	public List<ContractPayment> getPropertyUnitRentalContractPayments(Long contractId,Date fromDate,Date toDate) throws Exception {
		return contractPaymentDAO.findByNamedQuery("getPropertyUnitRentalContractPayments",contractId,fromDate,toDate);
	}
	
	//Property Rental paid with specific period List
	public List<OfferDetail> getPropertyRentalOfferDetailList(Implementation implementation,Date fromDate,Date toDate) throws Exception {
		return offerDetailDAO.findByNamedQuery("getPropertyRentalOfferDetailList",implementation,fromDate,toDate);
	}
	
	
	public Contract getContractPropertyInfoForLegal(Long contractId) throws Exception {
		List<Contract> contracts=new ArrayList<Contract>();
		contracts=contractDAO.findByNamedQuery("getContractPropertyInfoForLegal",contractId);
		if(contracts!=null && contracts.size()>0)
			return contracts.get(0);
		else
			return null;
	}
	
	//Getter & Setter
	public AIOTechGenericDAO<Contract> getContractDAO() {
		return contractDAO;
	}
	public void setContractDAO(AIOTechGenericDAO<Contract> contractDAO) {
		this.contractDAO = contractDAO;
	}
	public AIOTechGenericDAO<ContractPayment> getContractPaymentDAO() {
		return contractPaymentDAO;
	}
	public void setContractPaymentDAO(
			AIOTechGenericDAO<ContractPayment> contractPaymentDAO) {
		this.contractPaymentDAO = contractPaymentDAO;
	}
	public AIOTechGenericDAO<Component> getComponentDAO() {
		return componentDAO;
	}
	public void setComponentDAO(AIOTechGenericDAO<Component> componentDAO) {
		this.componentDAO = componentDAO;
	}
	public AIOTechGenericDAO<Property> getPropertyDAO() {
		return propertyDAO;
	}
	public void setPropertyDAO(AIOTechGenericDAO<Property> propertyDAO) {
		this.propertyDAO = propertyDAO;
	}

	public AIOTechGenericDAO<Offer> getOfferDAO() {
		return offerDAO;
	}

	public void setOfferDAO(AIOTechGenericDAO<Offer> offerDAO) {
		this.offerDAO = offerDAO;
	}

	public AIOTechGenericDAO<TenantOffer> getTenantOfferDAO() {
		return tenantOfferDAO;
	}

	public void setTenantOfferDAO(AIOTechGenericDAO<TenantOffer> tenantOfferDAO) {
		this.tenantOfferDAO = tenantOfferDAO;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<OfferDetail> getOfferDetailDAO() {
		return offerDetailDAO;
	}

	public void setOfferDetailDAO(AIOTechGenericDAO<OfferDetail> offerDetailDAO) {
		this.offerDetailDAO = offerDetailDAO;
	}

	public AIOTechGenericDAO<Unit> getUnitDAO() {
		return unitDAO;
	}

	public void setUnitDAO(AIOTechGenericDAO<Unit> unitDAO) {
		this.unitDAO = unitDAO;
	}

	public AIOTechGenericDAO<Release> getReleaseDAO() {
		return releaseDAO;
	}

	public void setReleaseDAO(AIOTechGenericDAO<Release> releaseDAO) {
		this.releaseDAO = releaseDAO;
	}
	
	public Contract getAllContractWithDetails (Contract contract){
		return contractDAO.findByNamedQuery("getAllContractDetails", contract.getContractId()).get(0);
	}
}
