package com.aiotech.aios.realestate.service;

import java.util.List;

import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ManagementDecisionService {

	AIOTechGenericDAO<ManagementDecision> managementDecisionDAO;
	
	public void addManagementDecision(ManagementDecision decision){
		managementDecisionDAO.saveOrUpdate(decision);
	}
	
	public ManagementDecision getManagementDecision(ManagementDecision decision){
		List<ManagementDecision> mDecision=managementDecisionDAO.findByExample(decision);
		if(mDecision!=null && mDecision.size()>0)
			return mDecision.get(0);
		else
			return null;
	}

	public AIOTechGenericDAO<ManagementDecision> getManagementDecisionDAO() {
		return managementDecisionDAO;
	}

	public void setManagementDecisionDAO(
			AIOTechGenericDAO<ManagementDecision> managementDecisionDAO) {
		this.managementDecisionDAO = managementDecisionDAO;
	}
	
	
	
}
