package com.aiotech.aios.realestate.service.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.hr.service.bl.PersonBankBL;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.vo.ComponentVO;
import com.aiotech.aios.realestate.domain.entity.vo.PropertyVO;
import com.aiotech.aios.realestate.domain.entity.vo.UnitVO;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.OfferService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.OfferManagementTO; 
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.converter.OfferTOConverter;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.system.domain.entity.Implementation;

public class OfferManagementBL {

	private OfferService offerService;
	private PersonBankBL personBankBL;

	public PropertyVO getComponentUnitDetails(Long propertyId) {
		List<Property> propertyList = offerService
				.getPropertyDetails(propertyId);
		PropertyVO propertyVO = converter(propertyList);
		return propertyVO;
	}

	public PropertyVO converter(List<Property> propertys) {
		PropertyVO propertyVO = new PropertyVO();
		for (Property property : propertys) {
			propertyVO.setPropertyId(property.getPropertyId());
			propertyVO.setPropertyName(property.getPropertyName());
			propertyVO.setCost(property.getCost());
			propertyVO.setFloors(property.getFloors());
			propertyVO.setRentYr(property.getRentYr());
			propertyVO.setStatus(property.getStatus());
			propertyVO.setSize(property.getSize());
			propertyVO.setSizeUnit(property.getSizeUnit());
			propertyVO.setAddressOne(property.getAddressOne());
			PropertyType propertyType = offerService
					.getPropertyTypeById(property.getPropertyType()
							.getPropertyTypeId());
			propertyVO.setPropertyType(propertyType);
			List<ComponentVO> componentVOs = new ArrayList<ComponentVO>();
			for (Component component : property.getComponents()) {
				ComponentVO componentVO = new ComponentVO();
				componentVO.setComponentId(component.getComponentId());
				componentVO.setComponentName(component.getComponentName());
				componentVO.setComponentNo(component.getComponentNo());
				componentVO.setRent(component.getRent());
				componentVO.setSize(component.getSize());
				componentVO.setSizeUnit(component.getSizeUnit());
				componentVO.setStatus(component.getStatus());
				componentVO.setComponentType(component.getComponentType());
				componentVO.setNoOfUnits(component.getNoOfUnits());
				List<UnitVO> unitVOs = new ArrayList<UnitVO>();
				for (Unit unit : component.getUnits()) {
					UnitVO unitVO = new UnitVO();
					unitVO.setUnitId(unit.getUnitId());
					unitVO.setUnitName(unit.getUnitName());
					unitVO.setUnitNo(unit.getUnitNo());
					unitVO.setRent(unit.getRent());
					unitVO.setRooms(unit.getRooms());
					unitVO.setStatus(unit.getStatus());
					unitVO.setUnitType(unit.getUnitType());
					unitVOs.add(unitVO);
					componentVO.setUnitVOs(unitVOs);
				}
				componentVOs.add(componentVO);
			}
			propertyVO.setComponentVOs(componentVOs);
		}
		return propertyVO;
	}

	public List<OfferManagementTO> getOfferedDetails(Long propertyId)
			throws Exception {
		List<OfferManagementTO> offeredlist = new ArrayList<OfferManagementTO>();
		OfferDetail offerDetail = offerService.getOfferProperty(propertyId);
		OfferManagementTO offerManagementTO = new OfferManagementTO();
		if (null != offerDetail && !offerDetail.equals("")) {
			Offer offer = offerService.getOffersById(offerDetail.getOffer()
					.getOfferId());
			offerManagementTO = getTenantDetails(offer);
			offerManagementTO.setPropertyId(offerDetail.getProperty()
					.getPropertyId().intValue());
			offeredlist.add(offerManagementTO);
		} else {
			List<Component> componentList = compService
					.getComponentsForProperty(propertyId);
			for (Component list : componentList) {
				List<Unit> unitList = unitService
						.getUnitListComponentBased(list.getComponentId());
				for (Unit unit : unitList) {
					OfferDetail offerUnit = offerService.getOfferedUnits(unit
							.getUnitId());
					offerManagementTO = getUnitOffered(offerUnit);
					if (null != offerManagementTO
							&& !offerManagementTO.equals(""))
						offeredlist.add(offerManagementTO);
				}
			}
		}
		return offeredlist;
	}

	public PropertyVO offeredForUnits(PropertyVO propertyVO,
			OfferManagementTO offer) {
		boolean flag = false;
		for (ComponentVO component : propertyVO.getComponentVOs()) {
			for (UnitVO unit : component.getUnitVOs()) {
				if (unit.getUnitId() == offer.getUnitId() && flag == false) {
					unit.setOfferNumber(offer.getOfferNumber());
					unit.setTenantName(offer.getTenantName());
					flag = true;
				}
				if (flag == true) {
					break;
				}
			}
			if (flag == true) {
				break;
			}
		}
		return propertyVO;
	}

	private OfferManagementTO getUnitOffered(OfferDetail offerUnit)
			throws Exception {
		if (offerUnit != null && !offerUnit.equals("")) {
			OfferManagementTO offerManagementTO = new OfferManagementTO();
			Offer offer = offerService.getOffersById(offerUnit.getOffer()
					.getOfferId());
			offerManagementTO = getTenantDetails(offer);
			offerManagementTO.setUnitId(offerUnit.getUnit().getUnitId()
					.intValue());
			return offerManagementTO;
		} else {
			return null;
		}
	}

	private OfferManagementTO getTenantDetails(Offer offer) throws Exception {
		OfferManagementTO offerManagementTO = new OfferManagementTO();
		TenantOffer tenantOffer = offerService
				.getTenantById(offer.getOfferId());
		if (tenantOffer.getPerson() != null) {
			Person person = offerService.getPersonDetails(tenantOffer
					.getPerson().getPersonId());
			offerManagementTO = OfferTOConverter.convertOfferEntityTo(offer,
					person, tenantOffer);
		} else {
			Company company = offerService.getCompanyDetails(tenantOffer
					.getCompany().getCompanyId());
			offerManagementTO = OfferTOConverter.convertOfferEntityTo(offer,
					company, tenantOffer);
		}
		return offerManagementTO;
	}

	public void updatePropertyStatus(String offerFor,
			List<OfferDetail> detailList, byte offerStatus) {
		if (offerFor.equalsIgnoreCase("UNITS")) {
			List<Unit> unitList = new ArrayList<Unit>();
			for (OfferDetail list : detailList) {
				Unit unit = offerService.getUnitsById(list.getUnit()
						.getUnitId());
				unit.setStatus(offerStatus);
				unitList.add(unit);
			}
			offerService.updateUnits(unitList);
		} else if (offerFor.equalsIgnoreCase("COMPONENT")) {
			List<Component> componentList = new ArrayList<Component>();
			for (OfferDetail list : detailList) {
				Component component = offerService.getComponentsById(list
						.getComponent().getComponentId());
				component.setStatus(offerStatus);
				componentList.add(component);
			}
			offerService.updateComponents(componentList);
		} else {
			Property property = offerService.getPropertyById(detailList.get(0)
					.getProperty().getPropertyId());
			property.setStatus(offerStatus);
			offerService.updateProperty(property);
		}
	}
	
	public List<OfferManagementTO> getAllTenantOffers(
			Implementation implementation) {

		List<TenantOffer> tenantOffers = offerService
				.getAllTenantOffers(implementation);
		List<OfferManagementTO> list = new ArrayList<OfferManagementTO>();
		OfferManagementTO offerManagementTO;

		if (tenantOffers != null && tenantOffers.size() > 0) {

			for (TenantOffer tenant : tenantOffers) {

				if (tenant.getPerson() != null
						&& !tenant.getPerson().equals("")) {

					offerManagementTO = new OfferManagementTO();
					offerManagementTO.setTenantName(tenant.getPerson().getFirstName()
							+ " " + tenant.getPerson().getMiddleName()
							+ " " + tenant.getPerson().getLastName());
					offerManagementTO.setTenantNumber(tenant.getPerson()
							.getPersonId() + "@P");

				} else {

					offerManagementTO = new OfferManagementTO();
					offerManagementTO.setTenantName(tenant.getCompany()
							.getCompanyName());
					offerManagementTO.setTenantNumber(tenant.getCompany()
							.getCompanyId() + "@C");
				}
				list.add(offerManagementTO);
			}
		}
		return list;
	}

	public List<Unit> getAllOfferedUnits(Implementation implementation) {
	
		return offerService.getAllOfferedUnits(implementation);
	}
	
	public OfferService getOfferService() {
		return offerService;
	}

	public void setOfferService(OfferService offerService) {
		this.offerService = offerService;
	}

	private PersonService personService;
	private UnitService unitService;
	private PropertyService propertyService;

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public ComponentService getCompService() {
		return compService;
	}

	public void setCompService(ComponentService compService) {
		this.compService = compService;
	}

	private ComponentService compService;

	public OfferManagementTO getOfferWithContract(Offer offer) throws Exception {
		OfferManagementTO to = new OfferManagementTO();
		Contract con = offerService.getOfferWithContract(offer);
		List<Offer> offerList = new ArrayList<Offer>();
		offerList.add(con.getOffer());
		to.setOfferList(OfferTOConverter.convertToTOList(offerList));
		to.setOfferDetails(con.getOffer().getOfferDetails());
		to.setTenantList(getTenants(con.getOffer().getTenantOffers()));
		to.setPropertyList(getProeprty(con.getOffer().getOfferDetails()));
		return to;
	}

	private List<HrPersonalDetailsTO> getTenants(Set<TenantOffer> tenantOffers)
			throws Exception {
		List<HrPersonalDetailsTO> persons = new ArrayList<HrPersonalDetailsTO>();
		for (TenantOffer offer : tenantOffers) {
			HrPersonalDetailsTO to = HrPersonalDetailsTOConverter
					.convertPersonToTO(offer.getPerson());
			/*to.setPersonTypes(personService.getPersonType(
					offer.getPerson().getPersonType().getPersonTypeId())
					.getTypeName());*/
			// to.setEmirateId(offer.getPerson().getEmiratesId());
			persons.add(to);
		}
		return persons;
	}

	private List<RePropertyInfoTO> getProeprty(Set<OfferDetail> offerDetails)
			throws Exception {
		List<RePropertyInfoTO> contractProp = new ArrayList<RePropertyInfoTO>();
		for (OfferDetail detail : offerDetails) {
			if (null != detail.getProperty()) {
				Property prop = propertyService
						.getPropertyWithPropertyType(detail.getProperty());
				contractProp.add(setPropertyAdd(prop));
			} else if (detail.getComponent() != null) {
				List<Component> comps = compService
						.getPropertyByComponentId(detail.getComponent());
				for (Component comp : comps) {
					contractProp.add(setPropertyAdd(comp.getProperty()));
				}
			} else if (detail.getUnit() != null) {
				List<Unit> units = unitService.getPropertyByUnitId(detail
						.getUnit());
				for (Unit u : units) {
					contractProp.add(setPropertyAdd(u.getComponent()
							.getProperty()));
				}
			}
		}
		return contractProp;
	}

	private RePropertyInfoTO setPropertyAdd(Property p) throws Exception {
		// if (p != null){
		RePropertyInfoTO to = RePropertyInfoTOConverter.convertToTO(p);
		to.setAddress(propertyService.getCityById(
				Long.parseLong("" + p.getCity())).getCityName()
				+ " , "
				+ propertyService.getStateById(
						Long.parseLong("" + p.getState())).getStateName()
				+ " , "
				+ propertyService.getCounryById(
						Long.parseLong("" + p.getCountry())).getCountryName());

		// contractProp.add(to);
		// }
		return to;
	}
	
	private List<Offer> getOffersWithinPeriod(Implementation implementation, String startDate,
			String endDate, String reportFilter) {

		List<Offer> offerEntityList = new ArrayList<Offer>();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyy");
		
		try {			
			if (startDate.equalsIgnoreCase("0")
					&& endDate.equalsIgnoreCase("0"))
				offerEntityList = offerService.getAllOffers(implementation);
			else if (startDate.equalsIgnoreCase("0")
					&& !endDate.equalsIgnoreCase("0"))
				offerEntityList = offerService.getOffersWithinPeriod(
						implementation, formatter.parse("01-Jan-1980"),
						formatter.parse(endDate));
			else if (!startDate.equalsIgnoreCase("0")
					&& endDate.equalsIgnoreCase("0"))
				offerEntityList = offerService.getOffersWithinPeriod(
						implementation, formatter.parse(startDate),
						formatter.parse("31-Dec-2099"));
			else if (!startDate.equalsIgnoreCase("0")
					&& !endDate.equalsIgnoreCase("0"))
				offerEntityList = offerService.getOffersWithinPeriod(
						implementation, formatter.parse(startDate),
						formatter.parse(endDate));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerEntityList;
	}

	public List<Offer> getFilteredOfferList(Implementation implementation, String startDate, String endDate,
			String reportFilter, int tenantId, int unitId) {

		String reportFilterTag = reportFilter; // reportFilter.split("_")[0];
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyy");
		List<Offer> offers = new ArrayList<Offer>();

		try {

			if (reportFilterTag.equalsIgnoreCase("none"))

				return offerService.getAllOffers(implementation);

			else if (reportFilterTag.equalsIgnoreCase("period"))

				return getOffersWithinPeriod(implementation, startDate, endDate, reportFilterTag);

			else if (reportFilterTag.equalsIgnoreCase("unit")) {

				offers.add(offerService.getOfferedUnits((long) unitId)
						.getOffer());
				return offers;
			}

			else if (reportFilterTag.equalsIgnoreCase("unitWithinPeriod")) {

				for (OfferDetail offerDetails : offerService
						.getOfferByUnitWithinPeriod(implementation,
								(long) unitId, formatter.parse(startDate),
								formatter.parse(endDate)))
					offers.add(offerDetails.getOffer());
				return offers;
			}

			else if (reportFilterTag.equalsIgnoreCase("tenantPerson")) {

				for (TenantOffer tenantOffer : offerService
						.getOfferByPersonTenant(implementation, tenantId))
					offers.add(tenantOffer.getOffer());
				return offers;
			}

			else if (reportFilterTag
					.equalsIgnoreCase("tenantPersonWithinPeriod")) {

				for (TenantOffer tenantOffer : offerService
						.getOfferByPersonTenantWithinPeriod(implementation,
								(long) tenantId, formatter.parse(startDate),
								formatter.parse(endDate)))
					offers.add(tenantOffer.getOffer());
				return offers;
			}

			else if (reportFilterTag.equalsIgnoreCase("tenantCompany")) {

				for (TenantOffer tenantOffer : offerService
						.getOfferByCompanyTenant(implementation, tenantId))
					offers.add(tenantOffer.getOffer());
				return offers;
			}

			else if (reportFilterTag
					.equalsIgnoreCase("tenantCompanyWithinPeriod")) {

				for (TenantOffer tenantOffer : offerService
						.getOfferByCompanyTenantWithinPeriod(implementation,
								(long) tenantId, formatter.parse(startDate),
								formatter.parse(endDate)))
					offers.add(tenantOffer.getOffer());
				return offers;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return offerService.getAllOffers(implementation);
	}
	
	public List<Offer> getOfferRantalRangeList(Implementation implementation, Double fromAmount,Double toAmount) {
		
		List<Offer> finalOffers=new ArrayList<Offer>();
		List<Offer> offers=offerService.getOfferList(implementation);
		 
		 if(fromAmount!=null && fromAmount>0 && toAmount!=null && toAmount>0){
				for(Offer offer:offers){
					Double offerAmount=0.0;
					for(OfferDetail offerDetail:offer.getOfferDetails()){
						offerAmount+=offerDetail.getOfferAmount();
					}
					if(offerAmount>=fromAmount && offerAmount<=toAmount){
						finalOffers.add(offer);
					}
				}
				
			}else if(fromAmount!=null && fromAmount>0 && (toAmount==null || toAmount==0.0)){
				for(Offer offer:offers){
					Double offerAmount=0.0;
					for(OfferDetail offerDetail:offer.getOfferDetails()){
						offerAmount+=offerDetail.getOfferAmount();
					}
					if(offerAmount>=fromAmount){
						finalOffers.add(offer);
					}
				}
			}else if((fromAmount==null || fromAmount==0.0) && toAmount!=null && toAmount>0){
				for(Offer offer:offers){
					Double offerAmount=0.0;
					for(OfferDetail offerDetail:offer.getOfferDetails()){
						offerAmount+=offerDetail.getOfferAmount();
					}
					if(offerAmount<=toAmount){
						finalOffers.add(offer);
					}
				}
			}else{
				finalOffers.addAll(offers);
			}
		 
		return finalOffers;
	}

	public PersonBankBL getPersonBankBL() {
		return personBankBL;
	}

	public void setPersonBankBL(PersonBankBL personBankBL) {
		this.personBankBL = personBankBL;
	}
}
