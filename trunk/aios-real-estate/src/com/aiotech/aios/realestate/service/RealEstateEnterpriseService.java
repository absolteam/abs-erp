package com.aiotech.aios.realestate.service;



public class RealEstateEnterpriseService {
	private PropertyService propertyService;
	private ComponentService componentService;
	private UnitService unitService;
	private ContractService contractService;
	private ReleaseService releaseService;
	private OfferService offerService;
	private CancellationService cancellationService;
	
	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}

	public ReleaseService getReleaseService() {
		return releaseService;
	}

	public void setReleaseService(ReleaseService releaseService) {
		this.releaseService = releaseService;
	}

	public OfferService getOfferService() {
		return offerService;
	}

	public void setOfferService(OfferService offerService) {
		this.offerService = offerService;
	}

	public CancellationService getCancellationService() {
		return cancellationService;
	}

	public void setCancellationService(CancellationService cancellationService) {
		this.cancellationService = cancellationService;
	}

	

	
	
	
}
