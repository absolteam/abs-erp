package com.aiotech.aios.realestate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Asset;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.UnitDetail;
import com.aiotech.aios.realestate.domain.entity.UnitType;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class UnitService {
	
	

	private AIOTechGenericDAO<Unit> unitDAO;
	private AIOTechGenericDAO<UnitType> unitTypeDAO;
	private AIOTechGenericDAO<Component> componentDAO;
	private AIOTechGenericDAO<UnitDetail> unitDetailDAO;
	private AIOTechGenericDAO<Property> propertyDAO;
	private AIOTechGenericDAO<Asset> assetDAO;
	
	//Unit List
	public List<Unit> getAllUnits(Implementation implementation) throws Exception {
		//return unitDAO.getAll();
		return unitDAO.findByNamedQuery("getAllUnitList",implementation);
	}
	//Unit List with status
	public List<Unit> getAllUnitsWithStatus(Implementation implementation,Byte status) throws Exception {
		//return unitDAO.getAll();
		return unitDAO.findByNamedQuery("getAllUnitListWithStatus",implementation,status);
	}
	
	//Component List
	public List<Component> getAllComponent(Implementation implementation) throws Exception {
		//return componentDAO.getAll();
		return componentDAO.findByNamedQuery("getAllComponentList",implementation);

	}
	//Unit Type List
	public List<UnitType> getAllUnitType() throws Exception {
		return unitTypeDAO.getAll();
	}
	
	//Unit Details
	public Unit getUnitDetails(Long id) throws Exception {
		return unitDAO.findById(id);
	}
	
	//Unit Details
	public Unit getUnitInformation(Long id) throws Exception {
		List<Unit> units=new ArrayList<Unit>();
		units=unitDAO.findByNamedQuery("getUnitInformation",id);
		if(units!=null && units.size()>0)
			return units.get(0);
		else
			return null;
	}
	
	//Component Details
	public Component getComponentDetails(Long id) throws Exception {
		return componentDAO.findByNamedQuery("getComponentsWithProperty", id).get(0);
		//return componentDAO.findById(id);
	}
	
	//Unit Type Details
	public UnitType getUnitTypeDetails(Integer id) throws Exception {
		return unitTypeDAO.findById(id);
	}
	
	//unit feature
	public List<UnitDetail> getUnitFeatureDetails(Long unitId) throws Exception {
		return unitDetailDAO.findByNamedQuery("getUnitFeatureInfo",unitId);
	}
	
	//unit feature related to implementation
	public List<UnitDetail> getAllUnitDetail(Implementation implementation) throws Exception {
		return unitDetailDAO.findByNamedQuery("getAllUnitDetail",implementation);
	}
	
	public List<Asset> getAssetDetails(Long unitId) throws Exception{
		return assetDAO.findByNamedQuery("getAssetDetailsByUnit",unitId);
	}
	
	public List<Unit> getUnitListComponentBased(Long componentId) throws Exception {
		return unitDAO.findByNamedQuery("getUnitListComponentBased", componentId);
	}
	
	public void addUnits(Unit unit,List<UnitDetail> unitdetails, List<Asset> assetDetailList) throws Exception {
		unitDAO.saveOrUpdate(unit);
		if(unitdetails!=null){
			for(UnitDetail ul:unitdetails){
				ul.setUnit(unit);
			}
			unit.setUnitDetails(new HashSet<UnitDetail>(unitdetails)); 
			unitDetailDAO.saveOrUpdateAll(unitdetails);
		}	
		if(assetDetailList!=null){
			for(Asset list: assetDetailList){
				list.setUnit(unit);
			}
			assetDAO.saveOrUpdateAll(new HashSet<Asset>(assetDetailList));
		}
	}
	
	public void deleteUnits(Unit unit,List<UnitDetail> unitFeature,List<Asset> assets) throws Exception {  
		//List<UnitDetail> unitFeature= new ArrayList<UnitDetail>();
		//unitFeature=unitDetailDAO.findByNamedQuery("getUnitFeatureInfo", unit.getUnitId());
		if(unitFeature != null)
			unitDetailDAO.deleteAll(unitFeature);
		if(assets !=null)
			assetDAO.deleteAll(assets);
		unitDAO.delete(unit);
	} 
	
	public void deleteUnitDetail(UnitDetail unitDetail) throws Exception {
		unitDetailDAO.delete(unitDetail);
	}
	
	public void deleteAssets(List<Asset> assets) throws Exception {
		assetDAO.deleteAll(assets);
	}
	
	public void updateUnitDetail(UnitDetail unitDetail) throws Exception {
		unitDetailDAO.saveOrUpdate(unitDetail);
	}
	
	//Component List property based
	public List<Component> getAllComponentPropertyBased(long propertyId) throws Exception {
		return componentDAO.findByNamedQuery("getComponentListPropertyBased",propertyId);
	}
	//property info
	public Property getPropertyInfo(Long propertyId) throws Exception {
		return propertyDAO.findById(propertyId);
	}
	
	public AIOTechGenericDAO<Unit> getUnitDAO() {
		return unitDAO;
	}
	public void setUnitDAO(AIOTechGenericDAO<Unit> unitDAO) {
		this.unitDAO = unitDAO;
	}
	public AIOTechGenericDAO<UnitType> getUnitTypeDAO() {
		return unitTypeDAO;
	}
	public void setUnitTypeDAO(AIOTechGenericDAO<UnitType> unitTypeDAO) {
		this.unitTypeDAO = unitTypeDAO;
	}
	public AIOTechGenericDAO<Component> getComponentDAO() {
		return componentDAO;
	}
	public void setComponentDAO(AIOTechGenericDAO<Component> componentDAO) {
		this.componentDAO = componentDAO;
	}
	public AIOTechGenericDAO<UnitDetail> getUnitDetailDAO() {
		return unitDetailDAO;
	}
	public void setUnitDetailDAO(AIOTechGenericDAO<UnitDetail> unitDetailDAO) {
		this.unitDetailDAO = unitDetailDAO;
	}
	public AIOTechGenericDAO<Property> getPropertyDAO() {
		return propertyDAO;
	}
	public void setPropertyDAO(AIOTechGenericDAO<Property> propertyDAO) {
		this.propertyDAO = propertyDAO;
	}
	
	public Unit getUnitCompsWithProperty (Unit unit){		
		return unitDAO.findByNamedQuery("getUnitComponentsWithProperty",unit.getUnitId()).get(0);
	}
	
	public List<Unit> getPropertyByUnitId(Unit unit){
		return unitDAO.findByNamedQuery("getPropertyByUnitId",unit.getUnitId());
	}
	
	//Property Unit Vacancy 
	public List<Unit> getPropertyUnitVacancy(long propertyId,Date fromDate,Date todate){
		return unitDAO.findByNamedQuery("getPropertyUnitVacancyList",propertyId,fromDate,propertyId);
	}
	
	public AIOTechGenericDAO<Asset> getAssetDAO() {
		return assetDAO;
	}
	public void setAssetDAO(AIOTechGenericDAO<Asset> assetDAO) {
		this.assetDAO = assetDAO;
	}

}
