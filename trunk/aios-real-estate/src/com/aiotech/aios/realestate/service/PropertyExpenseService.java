package com.aiotech.aios.realestate.service;

import java.util.List;

import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.realestate.domain.entity.PropertyExpenseDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PropertyExpenseService {
	
	private AIOTechGenericDAO<PropertyExpense> propertyExpenseDAO;
	private AIOTechGenericDAO<PropertyExpenseDetail> propertyExpenseDetailDAO;
	
	public List<PropertyExpense> getAllPropertyExpense(
			Implementation implementation) throws Exception {
		return this.getPropertyExpenseDAO().findByNamedQuery(
				"getAllPropertyExpense", implementation);
	}
	
	public PropertyExpense getPropertyExpenseById(long expenseId)
			throws Exception {
		return this.getPropertyExpenseDAO()
				.findByNamedQuery("getPropertyExpenseById", expenseId).get(0);
	}
	
	public PropertyExpense propertyExpenseById(Long expenseId)
			throws Exception {
		return this.getPropertyExpenseDAO()
				.findById(expenseId);
	}
	
	public List<PropertyExpenseDetail> getPropertyExpenseDetail(Long expenseId)
			throws Exception {
		return this.getPropertyExpenseDetailDAO().findByNamedQuery(
				"getPropertyExpenseDetail", expenseId);
	}
	
	public void deletePropertyExpense(PropertyExpense propertyExpense,
			List<PropertyExpenseDetail> propertyExpenseDetails)
			throws Exception {
		this.getPropertyExpenseDetailDAO().deleteAll(propertyExpenseDetails);
		this.getPropertyExpenseDAO().delete(propertyExpense);
	}
	
	public void savePropertyExpense(PropertyExpense propertyExpense,
			List<PropertyExpenseDetail> propertyExpenseDetails)
			throws Exception {
		this.getPropertyExpenseDAO().saveOrUpdate(propertyExpense);
		for (PropertyExpenseDetail list : propertyExpenseDetails) {
			list.setPropertyExpense(propertyExpense);
		}
		this.getPropertyExpenseDetailDAO().saveOrUpdateAll(
				propertyExpenseDetails);
	}
	
	public void updatePropertyExpense(PropertyExpense propertyExpense)
			throws Exception {
		this.getPropertyExpenseDAO().saveOrUpdate(propertyExpense);
	}
	
	public AIOTechGenericDAO<PropertyExpense> getPropertyExpenseDAO() {
		return propertyExpenseDAO;
	}

	public void setPropertyExpenseDAO(
			AIOTechGenericDAO<PropertyExpense> propertyExpenseDAO) {
		this.propertyExpenseDAO = propertyExpenseDAO;
	}

	public AIOTechGenericDAO<PropertyExpenseDetail> getPropertyExpenseDetailDAO() {
		return propertyExpenseDetailDAO;
	}

	public void setPropertyExpenseDetailDAO(
			AIOTechGenericDAO<PropertyExpenseDetail> propertyExpenseDetailDAO) {
		this.propertyExpenseDetailDAO = propertyExpenseDetailDAO;
	}
}
