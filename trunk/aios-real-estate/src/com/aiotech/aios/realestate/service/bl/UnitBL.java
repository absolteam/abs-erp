package com.aiotech.aios.realestate.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.service.ProductService;
import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.realestate.domain.entity.Asset;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.UnitDetail;
import com.aiotech.aios.realestate.domain.entity.UnitType;
import com.aiotech.aios.realestate.domain.entity.vo.UnitDetailVO;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.RePropertyUnitsTO;
import com.aiotech.aios.realestate.to.converter.RePropertyUnitsTOConverter;
import com.aiotech.aios.system.bl.CommentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.ImageService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.Comment;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.CommentVO;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class UnitBL {

	private DocumentService documentService;
	private ImageService imageService;
	private ImageBL imageBL;
	private CommentBL commentBL;
	private LookupMasterBL lookupMasterBL;
	List<Component> componentList = null;
	List<UnitType> unitTypeList = null;
	List<UnitDetail> unitFeatureList = null;
	Unit unitInfo = null;
	UnitType unitTyp = null;
	RePropertyUnitsTO unitTo = null;
	Component component = null;
	List<Product> productList = null;
	List<Asset> unitAssetList = null;
	private ProductService productService;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public Implementation getImplementation() {
		return (Implementation) ServletActionContext.getRequest().getSession()
				.getAttribute("THIS");
	}

	public void getAssetList() throws Exception {
		productList = productService.getProductsByItemType(
				getImplementation(), new Character('A'));
	}

	public void saveUnit(Unit unit, List<UnitDetail> unitFeature,
			List<Asset> assets, List<Asset> deletedAssets) throws Exception {
		if (deletedAssets != null && deletedAssets.size() > 0)
			unitService.deleteAssets(deletedAssets);

		unitService.addUnits(unit, unitFeature, assets);

		if (unit.getRelativeId() == null) {
			unit.setRelativeId(unit.getUnitId());
			unitService.addUnits(unit, null, null);
		}

		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
						.getCode());
		workflowEnterpriseService.generateNotificationsAgainstDataEntry("Unit",
				unit.getUnitId(), user, workflowDetailVo);

		// rejection case edit screen
		WorkflowDetailVO vo = null;
		if (sessionObj.containsKey("WORKFLOW_DETAILVO")) {
			vo = (WorkflowDetailVO) sessionObj.get("WORKFLOW_DETAILVO");

			sessionObj.remove("WORKFLOW_DETAILVO");
			workflowEnterpriseService.updateNotificationStatus(vo,
					vo.getRecordId(), (byte) 1, user);
		}

	}

	public Unit getUnitbyId(long unitId) {

		try {
			Unit unit = new Unit();
			unit.setUnitId(unitId);
			return unitService.getPropertyByUnitId(unit).get(0);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getUnit(long unitId, long recordId, Boolean isIdenticalEntry) {

		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		session.removeAttribute("REUNITFEATURE_" + sessionObj.get("jSessionId"));

		try {

			// Component List
			componentList = unitService.getAllComponent(getImplementation());

			// Unit Type List
			unitTypeList = unitService.getAllUnitType();
			// GET ASSET DETAILS
			getAssetList();
			Property propertyDetail = new Property();
			List<UnitDetailVO> unitDetailVO = new ArrayList<UnitDetailVO>();

			if (unitId != 0) {
				// Unit Details
				unitInfo = unitService.getUnitDetails(unitId);

				unitTyp = unitService.getUnitTypeDetails(unitInfo.getUnitType()
						.getUnitTypeId());
				unitInfo.setUnitType(unitTyp);

				component = unitService.getComponentDetails(unitInfo
						.getComponent().getComponentId());
				unitInfo.setComponent(component);

				unitTo = RePropertyUnitsTOConverter.convertToTO(unitInfo);

				propertyDetail = unitService.getPropertyInfo(component
						.getProperty().getPropertyId());
				// Set property information
				unitTo.setPropertyId(propertyDetail.getPropertyId());
				unitTo.setPropertyName(propertyDetail.getPropertyName());

				// Unit feature
				unitFeatureList = unitService.getUnitFeatureDetails(unitId);
				unitDetailVO = UnitDetailVO
						.convertToUnitDetailVO(unitFeatureList);
				session.setAttribute(
						"REUNITFEATURE_" + sessionObj.get("jSessionId"),
						unitFeatureList);

				// Asset Details
				unitAssetList = unitService.getAssetDetails(unitId);
				session.setAttribute(
						"REUNITASSET_DETAILS_" + sessionObj.get("jSessionId"),
						unitAssetList);

				if (isIdenticalEntry != null && isIdenticalEntry)
					unitTo.setUnitId(0);

				ServletActionContext.getRequest().setAttribute("UNITINFO",
						unitTo);
				ServletActionContext.getRequest().setAttribute(
						"FEATURE_DETAILS", unitDetailVO);
				ServletActionContext.getRequest().setAttribute("ASSET_INFO",
						unitAssetList);

				// Property Comment Information --Added by rafiq
				CommentVO comment = new CommentVO();
				if (recordId != 0) {
					comment.setRecordId(Long.parseLong(unitId + ""));
					comment.setUseCase("com.aiotech.aios.realestate.domain.entity.Unit");
					comment = commentBL.getCommentInfo(comment);
				}
				ServletActionContext.getRequest().setAttribute("COMMENT_IFNO",
						comment);
				// Comment process End
			}
			// unitFeatureList);
			ServletActionContext.getRequest().setAttribute("COMPONENTLIST",
					componentList);
			ServletActionContext.getRequest().setAttribute("UNITYPELIST",
					unitTypeList);
			ServletActionContext.getRequest().setAttribute("PRODUCT_INFO",
					productList);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

		return "success";
	}

	public void saveUploadedImages(Object object, boolean editFlag)
			throws Exception {
		DocumentVO docVO = new DocumentVO();
		docVO.setDisplayPane("uploadedImagesUnit");
		docVO.setEdit(editFlag);
		docVO.setObject(object);
		imageBL.saveUploadedImages(object, docVO);
	}

	private Image populateImage(Map.Entry<String, FileVO> entry, Object object)
			throws Exception {
		Image image = new Image();
		image.setName(entry.getKey());
		image.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		image.setTableName(name[name.length - 1]);

		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		image.setRecordId((Long) method.invoke(object, null));

		return image;
	}

	public List<RePropertyUnitsTO> getUnitListForReport(Byte status)
			throws Exception {

		List<RePropertyUnitsTO> unitList = new ArrayList<RePropertyUnitsTO>();
		Property propertyDetail = new Property();
		List<UnitDetailVO> unitDetailVO = new ArrayList<UnitDetailVO>();
		List<Unit> units = null;
		if (status == -1)
			units = unitService.getAllUnits(getImplementation());
		else
			units = unitService.getAllUnitsWithStatus(getImplementation(),
					status);
		for (Unit unitInfo : units) {
			// Unit Details
			unitTyp = unitService.getUnitTypeDetails(unitInfo.getUnitType()
					.getUnitTypeId());
			unitInfo.setUnitType(unitTyp);

			component = unitService.getComponentDetails(unitInfo.getComponent()
					.getComponentId());
			unitInfo.setComponent(component);

			unitTo = RePropertyUnitsTOConverter.convertToTO(unitInfo);

			getUnitFullInfo(unitTo);// Convert the unit specification into

			// Set property information
			propertyDetail = unitService.getPropertyInfo(component
					.getProperty().getPropertyId());
			unitTo.setPropertyId(propertyDetail.getPropertyId());
			unitTo.setPropertyName(propertyDetail.getPropertyName());

			/*
			 * // Unit feature unitFeatureList =
			 * unitService.getUnitFeatureDetails(unitInfo.getUnitId());
			 * unitDetailVO = UnitDetailVO
			 * .convertToUnitDetailVO(unitFeatureList);
			 * unitTo.setUnitDetailVO(unitDetailVO);
			 * 
			 * //Asset Details
			 * unitAssetList=unitService.getAssetDetails(unitInfo.getUnitId());
			 * if(unitAssetList!=null && unitAssetList.size()>0)
			 * unitTo.setAssetList
			 * (RePropertyUnitsTOConverter.assetListToTOConverter
			 * (unitAssetList));
			 */

			unitList.add(unitTo);

		}

		ServletActionContext.getRequest().setAttribute("UNITLIST", unitList);

		return unitList;
	}

	private RePropertyUnitsTO getUnitFullInfo(RePropertyUnitsTO to) {
		String unitfullstr = "Bed room : " + to.getBedroomSize() + ","
				+ " Bath room : " + to.getBathroomSize() + "," + " Hall : "
				+ to.getHallSize() + "," + " Kitchen : " + to.getKitchenSize();
		to.setUnitFullInfo(unitfullstr);
		return to;
	}

	private UnitService unitService;

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	/*
	 * public RePropertyUnitsTO getUnitDetails(Unit unit) throws Exception{ Unit
	 * propUnit = unitService.getUnitCompsWithProperty(unit); RePropertyUnitsTO
	 * reUnitTO = RePropertyUnitsTOConverter.convertToTO(propUnit);
	 * List<UnitDetail> unitDetails =
	 * unitService.getUnitFeatureDetails(propUnit.getUnitId());
	 * reUnitTO.setUnitDetailVO
	 * (UnitDetailVO.convertToUnitDetailVO(unitDetails)); return reUnitTO; }
	 */

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public List<Asset> getUnitAssetList() {
		return unitAssetList;
	}

	public void setUnitAssetList(List<Asset> unitAssetList) {
		this.unitAssetList = unitAssetList;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public CommentBL getCommentBL() {
		return commentBL;
	}

	public void setCommentBL(CommentBL commentBL) {
		this.commentBL = commentBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
