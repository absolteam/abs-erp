package com.aiotech.aios.realestate.service;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonType;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.domain.entity.UnitType;
import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class OfferService {

	private AIOTechGenericDAO<Offer> offerDAO;
	private AIOTechGenericDAO<OfferDetail> offerDetailDAO;
	private AIOTechGenericDAO<PropertyType> propertyTypeDAO;
	private AIOTechGenericDAO<Property> propertyDAO;
	private AIOTechGenericDAO<City> cityDAO;
	private AIOTechGenericDAO<State> stateDAO;
	private AIOTechGenericDAO<Country> countryDAO;
	private AIOTechGenericDAO<Component> componentDAO;
	private AIOTechGenericDAO<Unit> unitDAO;
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<PersonType> personTypeDAO;
	private AIOTechGenericDAO<TenantGroup> tenantGroupDAO;
	private AIOTechGenericDAO<TenantOffer> tenantOfferDAO;
	private AIOTechGenericDAO<UnitType> unitTypeDAO;
	private AIOTechGenericDAO<Company> companyDAO;
	private AIOTechGenericDAO<ComponentType> componentTypeDAO;

	// to get offer form listing
	public List<Offer> getAllOffers(Implementation implementation) {
		List<Offer> offerList = offerDAO.findByNamedQuery("getOffersAll",
				implementation);
		return offerList;
	}

	public List<Offer> getOffersWithinPeriod(Implementation implementation,
			Date startDate, Date endDate) {

		return offerDAO.findByNamedQuery("getOffersWithinPeriod",
				implementation, startDate, endDate);
	}

	public List<Offer> getOfferNumber(Implementation implementation) {
		List<Offer> offerList = offerDAO.findByNamedQuery("getOfferNumber",
				implementation);
		return offerList;
	}

	// to get all property types
	public List<PropertyType> getAllPropertyTypes() {
		List<PropertyType> typeList = propertyTypeDAO.getAll();
		return typeList;
	}

	// to get all properties
	public List<Property> getAllProperties(Implementation implementation) {
		List<Property> propertyList = propertyDAO.findByNamedQuery(
				"getAllProperties", implementation);
		return propertyList;
	}

	public List<Property> getOfferProperties(Implementation implementation) {
		List<Property> propertyList = propertyDAO.findByNamedQuery(
				"getOfferProperties", implementation);
		return propertyList;
	}

	// to get city name by id
	public City getCityById(Long cityId) {
		return cityDAO.findById(cityId);
	}

	// to get country by id
	public Country getCountryById(Long countryId) {
		return countryDAO.findById(countryId);
	}

	// to get state by id
	public State getStateById(Long stateId) {
		return stateDAO.findById(stateId);
	}
	
	public void updateOffer(Offer offer) throws Exception{
		this.getOfferDAO().saveOrUpdate(offer);
	}

	// to get component details
	public List<Component> getComponentsByProperty(Long buildingId) {
		List<Component> componentList = componentDAO.findByNamedQuery(
				"getComponentListPropertyBased", buildingId);
		return componentList;
	}

	// to get unit details
	public List<Unit> getUnitsByComponent(Long componentId) {
		List<Unit> unitList = unitDAO.findByNamedQuery(
				"getUnitListComponentBased", componentId);
		return unitList;
	}

	// TO get property details
	public List<Property> getPropertyDetails(Long propertyId) {
		List<Property> propertyList = propertyDAO.findByNamedQuery(
				"getOfferPropertyDetails", propertyId);
		return propertyList;
	}

	// To get property type details
	public PropertyType getPropertyTypeById(Integer propertyTypeId) {
		return propertyTypeDAO.findById(propertyTypeId);
	}

	// To get unit type by id
	public UnitType getUnitTypeById(Integer unitTypeId) {
		return unitTypeDAO.findById(unitTypeId);
	}

	// TO get tenant details
	public List<Person> getAllTenants(Implementation implementation) {
		List<Person> propertyList = personDAO.findByNamedQuery(
				"getOfferTenantDetails", implementation);
		return propertyList;
	}

	// To get all tenant companies
	public List<Company> getAllTenantCompanies(Implementation implementation,
			Integer companyType) {
		List<Company> companyList = companyDAO.findByNamedQuery(
				"getAllTenantCompanies", implementation, companyType);
		return companyList;
	}

	// to save offer details
	public void saveOfferForm(Offer offer, List<OfferDetail> detailList,
			TenantOffer tenant) throws Exception {
		offerDAO.saveOrUpdate(offer);
		tenant.setOffer(offer);
		tenantOfferDAO.saveOrUpdate(tenant);
		for (OfferDetail list : detailList) {
			list.setOffer(offer);
		}
		offerDetailDAO.saveOrUpdateAll(detailList);
	}

	public OfferDetail getOfferProperty(Long propertyId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getOfferProperty", propertyId);
		if (detailList != null && detailList.size() > 0)
			return detailList.get(0);
		else
			return null;
	}

	public OfferDetail getOfferedUnits(Long unitId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getOfferUnit", unitId);
		if (detailList != null && detailList.size() > 0)
			return detailList.get(0);
		else
			return null;
	}

	public List<OfferDetail> getOfferByUnitWithinPeriod(
			Implementation implementation, long unitId, Date startDate,
			Date endDate) {

		return offerDetailDAO.findByNamedQuery("getOfferByUnitWithinPeriod",
				implementation, unitId, startDate, endDate);
	}

	public List<TenantOffer> getOfferByPersonTenant(
			Implementation implementation, long personId) {

		return tenantOfferDAO.findByNamedQuery("getOfferByPersonTenant",
				implementation, personId);
	}

	public List<TenantOffer> getOfferByPersonTenantWithinPeriod(
			Implementation implementation, long personId, Date startDate,
			Date endDate) {

		return tenantOfferDAO.findByNamedQuery(
				"getOfferByPersonTenantWithinPeriod", implementation, personId,
				startDate, endDate);
	}

	public List<TenantOffer> getOfferByCompanyTenant(
			Implementation implementation, long companyId) {

		return tenantOfferDAO.findByNamedQuery("getOfferByCompanyTenant",
				implementation, companyId);
	}

	public List<TenantOffer> getOfferByCompanyTenantWithinPeriod(
			Implementation implementation, long companyId, Date startDate,
			Date endDate) {

		return tenantOfferDAO.findByNamedQuery(
				"getOfferByCompanyTenantWithinPeriod", implementation,
				companyId, startDate, endDate);
	}
	
	public List<TenantOffer> getAllTenantOffers(Implementation implementation) {

		return tenantOfferDAO.findByNamedQuery("getAllTenantOffers", implementation);
	}
	
	public List<Unit> getAllOfferedUnits(Implementation implementation) {

		return unitDAO.findByNamedQuery("getAllOfferedUnits", implementation);
	}

	public Unit getUnitsById(Long unitId) {
		return unitDAO.findById(unitId);
	}

	public Component getComponentsById(Long componentId) {
		return componentDAO.findById(componentId);
	}

	public Property getPropertyById(Long propertyId) {
		return propertyDAO.findById(propertyId);
	}

	// to update unit status
	public void updateUnits(List<Unit> unitList) {
		unitDAO.saveOrUpdateAll(unitList);
	}

	// to update component status
	public void updateComponents(List<Component> componentList) {
		componentDAO.saveOrUpdateAll(componentList);
	}

	// to update property status
	public void updateProperty(Property property) {
		propertyDAO.saveOrUpdate(property);
	}

	// to get tenant group
	public TenantGroup getTenantGroup(Integer tenantGroupId) {
		return tenantGroupDAO.findById(tenantGroupId);
	}

	// to get offer by id
	public Offer getOffersById(Long offerId) {
		return offerDAO.findById(offerId);
	}

	// to get tenant details by offer id
	public TenantOffer getTenantById(Long offerId) {
		List<TenantOffer> tenantList = tenantOfferDAO.findByNamedQuery(
				"getTenantOffer", offerId);
		return tenantList.get(0);
	}

	// to get offer details list
	public List<OfferDetail> getOfferDetailById(Long offerId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getOfferDetailLines", offerId);
		return detailList;
	}

	// to get components by offer component id
	public List<OfferDetail> getOfferComponentsById(Long offerId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getAllOfferComponents", offerId);
		return detailList;
	}

	public List<OfferDetail> getOfferUnitsById(Long offerId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getAllOfferUnits", offerId);
		return detailList;
	}

	// to get property by id
	public OfferDetail getOfferPropertyById(Long offerId) {
		List<OfferDetail> detailList = offerDetailDAO.findByNamedQuery(
				"getAllOfferProperty", offerId);
		return detailList.get(0);
	}

	// to delete offer details
	public void deleteOfferDetails(Offer offer, List<OfferDetail> detailList,
			TenantOffer tenant) {
		tenantOfferDAO.delete(tenant);
		offerDetailDAO.deleteAll(detailList);
		offerDAO.delete(offer);
	}

	// to get person list
	public Person getPersonDetails(Long tenantId) {
		List<Person> personList = personDAO.findByNamedQuery(
				"getOfferPersonDetails", tenantId);
		return personList.get(0);
	}

	// to get company
	public Company getCompanyDetails(Long companyId) {
		return companyDAO.findByNamedQuery("getOfferCompanyDetails", companyId)
				.get(0);
	}
	
	public List<TenantOffer> getTenantOfferDetailsOnlyPerson(Long offerId,Long personId) {
		List<TenantOffer> detailList = tenantOfferDAO.findByNamedQuery("getTenantOfferDetailsOnlyPerson", offerId,personId);
		return detailList;
	}
	public List<TenantOffer> getTenantOfferDetailsOnlyCompany(Long offerId,Long personId) {
		List<TenantOffer> detailList = tenantOfferDAO.findByNamedQuery("getTenantOfferDetailsOnlyCompany", offerId,personId);
		return detailList;
	}
	
	// to get offer form listing
	public List<Offer> getOfferList(Implementation implementation) {
		List<Offer> offerList = offerDAO.findByNamedQuery("getOfferList",
				implementation);
		return offerList;
	}
	// Getters&setters
	public AIOTechGenericDAO<Offer> getOfferDAO() {
		return offerDAO;
	}

	public void setOfferDAO(AIOTechGenericDAO<Offer> offerDAO) {
		this.offerDAO = offerDAO;
	}

	public AIOTechGenericDAO<OfferDetail> getOfferDetailDAO() {
		return offerDetailDAO;
	}

	public void setOfferDetailDAO(AIOTechGenericDAO<OfferDetail> offerDetailDAO) {
		this.offerDetailDAO = offerDetailDAO;
	}

	public AIOTechGenericDAO<PropertyType> getPropertyTypeDAO() {
		return propertyTypeDAO;
	}

	public void setPropertyTypeDAO(
			AIOTechGenericDAO<PropertyType> propertyTypeDAO) {
		this.propertyTypeDAO = propertyTypeDAO;
	}

	public AIOTechGenericDAO<Property> getPropertyDAO() {
		return propertyDAO;
	}

	public void setPropertyDAO(AIOTechGenericDAO<Property> propertyDAO) {
		this.propertyDAO = propertyDAO;
	}

	public AIOTechGenericDAO<City> getCityDAO() {
		return cityDAO;
	}

	public void setCityDAO(AIOTechGenericDAO<City> cityDAO) {
		this.cityDAO = cityDAO;
	}

	public AIOTechGenericDAO<State> getStateDAO() {
		return stateDAO;
	}

	public void setStateDAO(AIOTechGenericDAO<State> stateDAO) {
		this.stateDAO = stateDAO;
	}

	public AIOTechGenericDAO<Country> getCountryDAO() {
		return countryDAO;
	}

	public void setCountryDAO(AIOTechGenericDAO<Country> countryDAO) {
		this.countryDAO = countryDAO;
	}

	public AIOTechGenericDAO<Component> getComponentDAO() {
		return componentDAO;
	}

	public void setComponentDAO(AIOTechGenericDAO<Component> componentDAO) {
		this.componentDAO = componentDAO;
	}

	public AIOTechGenericDAO<Unit> getUnitDAO() {
		return unitDAO;
	}

	public void setUnitDAO(AIOTechGenericDAO<Unit> unitDAO) {
		this.unitDAO = unitDAO;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<PersonType> getPersonTypeDAO() {
		return personTypeDAO;
	}

	public void setPersonTypeDAO(AIOTechGenericDAO<PersonType> personTypeDAO) {
		this.personTypeDAO = personTypeDAO;
	}

	public AIOTechGenericDAO<TenantGroup> getTenantGroupDAO() {
		return tenantGroupDAO;
	}

	public void setTenantGroupDAO(AIOTechGenericDAO<TenantGroup> tenantGroupDAO) {
		this.tenantGroupDAO = tenantGroupDAO;
	}

	public AIOTechGenericDAO<TenantOffer> getTenantOfferDAO() {
		return tenantOfferDAO;
	}

	public void setTenantOfferDAO(AIOTechGenericDAO<TenantOffer> tenantOfferDAO) {
		this.tenantOfferDAO = tenantOfferDAO;
	}

	public AIOTechGenericDAO<UnitType> getUnitTypeDAO() {
		return unitTypeDAO;
	}

	public void setUnitTypeDAO(AIOTechGenericDAO<UnitType> unitTypeDAO) {
		this.unitTypeDAO = unitTypeDAO;
	}

	public Contract getOfferWithContract(Offer offer) {
		return contractDAO.findByNamedQuery("getAllContractsNOffers",
				offer.getOfferId()).get(0);
	}

	public Offer getOfferList(Offer offer) {
		return offerDAO.findByNamedQuery("getAllOffers", offer.getOfferId())
				.get(0);
	}

	public void setContractDAO(AIOTechGenericDAO<Contract> contractDAO) {
		this.contractDAO = contractDAO;
	}

	private AIOTechGenericDAO<Contract> contractDAO;

	public AIOTechGenericDAO<Contract> getContractDAO() {
		return contractDAO;
	}

	public AIOTechGenericDAO<Company> getCompanyDAO() {
		return companyDAO;
	}

	public void setCompanyDAO(AIOTechGenericDAO<Company> companyDAO) {
		this.companyDAO = companyDAO;
	}

	public AIOTechGenericDAO<ComponentType> getComponentTypeDAO() {
		return componentTypeDAO;
	}

	public void setComponentTypeDAO(
			AIOTechGenericDAO<ComponentType> componentTypeDAO) {
		this.componentTypeDAO = componentTypeDAO;
	}
}
