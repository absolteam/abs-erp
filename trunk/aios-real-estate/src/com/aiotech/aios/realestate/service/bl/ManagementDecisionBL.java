package com.aiotech.aios.realestate.service.bl;

import java.lang.reflect.Method;

import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.aios.realestate.service.ManagementDecisionService;

public class ManagementDecisionBL {

	private ManagementDecisionService managementDecisionService;

	public void addManagementDecision(Object object, ManagementDecision decision)
			throws Exception {
		String[] name = object.getClass().getName().split("entity.");
		decision.setTableName(name[name.length - 1]);
		String methodName = "get" + name[name.length - 1] + "Id";
		Method method = object.getClass().getMethod(methodName, null);
		decision.setRecordId((Long) method.invoke(object, null));
		managementDecisionService.addManagementDecision(decision);
	}

	public ManagementDecision getManagementDecision(Object object)
			throws Exception {
			ManagementDecision decision = new ManagementDecision();
			String[] name = object.getClass().getName().split("entity.");
			decision.setTableName(name[name.length - 1]);
			String methodName = "get" + name[name.length - 1] + "Id";
			Method method = object.getClass().getMethod(methodName, null);
			decision.setRecordId((Long) method.invoke(object, null));
			decision=managementDecisionService.getManagementDecision(decision);
			if(decision!=null && !decision.equals(""))
				return decision;
			else
				return null;
	}

	public ManagementDecisionService getManagementDecisionService() {
		return managementDecisionService;
	}

	public void setManagementDecisionService(
			ManagementDecisionService managementDecisionService) {
		this.managementDecisionService = managementDecisionService;
	}

}
