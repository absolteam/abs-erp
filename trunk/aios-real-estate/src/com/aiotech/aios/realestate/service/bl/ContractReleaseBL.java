package com.aiotech.aios.realestate.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.hr.to.HrPersonalDetailsTO;
import com.aiotech.aios.hr.to.converter.HrPersonalDetailsTOConverter;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.OfferService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.ReleaseService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.ContractReleaseTO;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.converter.ContractReleaseTOConverter;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Document;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.AlertRole;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class ContractReleaseBL {
	
	private ReleaseService releaseService;
	private ContractService contractService;
	private DocumentService documentService;
	private DocumentBL documentBL;
	private DirectoryBL directoryBL;
	private OfferManagementBL offerBL;
	private WorkflowEnterpriseService workflowEnterpriseService;
	
	private List<ContractReleaseTO> releaseTOList = null;
	private List<Release> releases=null;
	private Contract contract=null;
	private Offer offer=null; 
	private List<TenantOffer> tenentOffers=null;
	private List<Person> persons=null;
	//FOR ACCOUNTS INTEGRATION 
	private OfferService offerService; 
	private AlertBL alertBL;
	private SystemService systemService;
	public List<ContractReleaseTO> getReleaseListInformation(Implementation implementation) {

		try {
			releaseTOList=new ArrayList<ContractReleaseTO>();
			releases=this.getReleaseService().getAllRelease(implementation);
			List<Release> tempReleases=new ArrayList<Release>();
			tempReleases.addAll(releases);
			HttpSession session = ServletActionContext.getRequest().getSession();
			int releaseNoticePeriod = 0;
			if (session.getAttribute("release_notice_period") != null)
				releaseNoticePeriod = Integer.parseInt(session.getAttribute("release_notice_period").toString()) ;
			for(Release release:tempReleases){
				if(release.getContract().getCancellations()!=null 
						&& release.getContract().getCancellations().size()>0){
					List<Cancellation> cancellations=new ArrayList<Cancellation>(release.getContract().getCancellations());
					if(cancellations!=null && cancellations.size()>0
							&& cancellations.get(0).getIsPenalty()!=null
							&& cancellations.get(0).getIsPenalty()==false 
							&& cancellations.get(0).getCancellationEffectDate() !=null){ 

						Calendar cal = Calendar.getInstance(); 
						cal.add(Calendar.MONTH, releaseNoticePeriod);
						java.util.Date date=cal.getTime();
						if(DateFormat.compareDates(date,cancellations.get(0).getCancellationEffectDate())==true)
							releases.remove(release);
						
					}
				}
			}
			releaseTOList = ContractReleaseTOConverter.convertToTOList(releases);
			if(releaseTOList!=null && releaseTOList.size()>0){
				for (int i = 0; i <releaseTOList.size(); i++) {
				
					contract=new Contract();

					contract=this.contractService.getContractDetails(releaseTOList.get(i).getContractId());
					
					releaseTOList.get(i).setContractNumber(contract.getContractNo());
					releaseTOList.get(i).setContractDate(DateFormat.convertDateToString(contract.getContractDate().toString()));
					offer=new Offer(); 
					persons=new ArrayList<Person>();
					
					//Find offer
					offer=this.contractService.getOfferDetails(contract.getOffer().getOfferId());
					if(offer.getStartDate()!=null)
						releaseTOList.get(i).setFromDate(DateFormat.convertDateToString(offer.getStartDate().toString()));
					if(offer.getEndDate()!=null)
						releaseTOList.get(i).setToDate(DateFormat.convertDateToString(offer.getEndDate().toString()));
					
					//Find Tenent offer
					tenentOffers=this.contractService.getTenantOfferDetails(offer.getOfferId());
					if(tenentOffers!=null && tenentOffers.size()>0){
					//Find Person
						Company company=null;
						if(tenentOffers!=null && tenentOffers.size()>0 && tenentOffers.get(0).getPerson()!=null){
						//Find Person
							persons=this.contractService.getPersonDetails(tenentOffers.get(0).getPerson());
							releaseTOList.get(i).setTenantName(persons.get(0).getFirstName()+" "+persons.get(0).getLastName());
						}else{
							company=new Company();
							company=offerService.getCompanyDetails(tenentOffers.get(0).getCompany().getCompanyId());
							if(company!=null){
								releaseTOList.get(i).setTenantName(company.getCompanyName());
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return releaseTOList;

	}
	public void saveWaterDocuments(Release release,boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(release, "waterDocs", editFlag);
		   Map<String, String> dirs  = docVO.getDirMap();   
		   
		   if (dirs != null){
		    Map<String, Directory> dirStucture = directoryBL.saveDirStructure(dirs,release.getReleaseId(),"Release","waterDocs");
		    docVO.setDirStruct(dirStucture);
		   } 
		  
		   documentBL.saveUploadDocuments(docVO);
	}
	public void saveEnergyDocuments(Release release,boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(release, "electricityDocs", editFlag);
		   Map<String, String> dirs  = docVO.getDirMap();   
		   
		   if (dirs != null){
		    Map<String, Directory> dirStucture = directoryBL.saveDirStructure(dirs,release.getReleaseId(),"Release","electricityDocs");
		    docVO.setDirStruct(dirStucture);
		   } 
		  
		   documentBL.saveUploadDocuments(docVO);
	}
	public void saveAddcDocuments(Release release,boolean editFlag) throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(release, "addcDocs", editFlag);
		   Map<String, String> dirs  = docVO.getDirMap();   
		   
		   if (dirs != null){
		    Map<String, Directory> dirStucture = directoryBL.saveDirStructure(dirs,release.getReleaseId(),"Release","addcDocs");
		    docVO.setDirStruct(dirStucture);
		   } 
		  
		   documentBL.saveUploadDocuments(docVO);
	}
	 
	private Document populateDocument(Document document,
			Map.Entry<String, FileVO> entry, Object object) throws Exception{
		document.setName(entry.getKey());
		document.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		document.setTableName(name[name.length-1]);
		String methodName = "get" + name[name.length-1]+"Id";
		Method method = object.getClass().getMethod(methodName, null);
		document.setRecordId((Long)method.invoke(object, null));
		return document;
	} 
	
	public void saveRelease(Release release, List<ReleaseDetail> releaseDetails)
			throws Exception {
		this.getReleaseService().addRelease(release, releaseDetails); 
		
		/*
		if (release.getRelativeId() == null) {
			release.setRelativeId(release.getReleaseId());
			this.getReleaseService().addRelease(release, null); 
		}*/
		if(release.getRefund()!=null && release.getCheckAddcCert()==null){
			Map<String, Object> sessionObj = ActionContext.getContext().getSession(); 
			User user = (User) sessionObj.get("USER");
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Release", release.getReleaseId(), user, workflowDetailVo);
		}
	}
	
	//Release Print Update
	public void releasePrintUpdate(Release release) throws Exception{
		releaseService.updateRelease(release);
		
		
		List<OfferDetail> offerDetails=offerBL.getOfferService().getOfferDetailById(release.getContract().getOffer().getOfferId());

		String offerFor=null;
		if(offerDetails!=null && offerDetails.size()>0){
			if(offerDetails.get(0).getProperty()!=null && offerDetails.get(0).getProperty().getPropertyId()!=null && offerDetails.get(0).getProperty().getPropertyId()!=0){
				offerFor="PROPERTY";
			}else if(offerDetails.get(0).getComponent()!=null && offerDetails.get(0).getComponent().getComponentId()!=null && offerDetails.get(0).getComponent().getComponentId()!=0){
				offerFor="COMPONENT";
			}else if(offerDetails.get(0).getUnit()!=null && offerDetails.get(0).getUnit().getUnitId()!=null && offerDetails.get(0).getUnit().getUnitId()!=0){
				offerFor="UNITS";
			}
		}
		if(offerFor!=null){
			offerBL.updatePropertyStatus(offerFor, offerDetails, Byte.parseByte("1"));
		}
		
	
		List<Alert> resultList=new ArrayList<Alert>();
		Set<String> paymentList=new HashSet<String>();
		Set<String> alertList=new HashSet<String>();
		
			String contractMessage="Direct Payment need to be processed for Release Contract.";
			Alert alert=new Alert();
			alert.setMessage(contractMessage);
			alert.setRecordId(release.getReleaseId());
			alert.setTableName("Release");
			alert.setIsActive(true);
			Screen screen=new Screen(); 
			// Alert Role Access
			AlertRole alertRole = alertBL.getRoleIdByModuleProcess(Release.class
					.getName());
			if (alertRole.getRole() != null) {
				alert.setRoleId(alertRole.getRole().getRoleId());
			} else {
				alert.setPersonId(alertRole.getPersonId());
			}
			
			resultList.add(alert);
			screen=systemService.getScreenBasedOnScreenName("ReleaseDirectPayment");
			alert.setScreenId(screen.getScreenId());
			paymentList.add(alert.getRecordId()+"@"+alert.getTableName()+alert.getScreenId());
			
			alert=new Alert();
			contractMessage="Bank Receipt need to be processed for Release Contract.";
			alert.setMessage(contractMessage);
			alert.setRecordId(release.getReleaseId());
			alert.setTableName("Release");
			alert.setIsActive(true);
			// Alert Role Access
			alertRole = alertBL.getRoleIdByModuleProcess(Release.class
					.getName());
			if (alertRole.getRole() != null) {
				alert.setRoleId(alertRole.getRole().getRoleId());
			} else {
				alert.setPersonId(alertRole.getPersonId());
			}
			screen=systemService.getScreenBasedOnScreenName("ReleaseBankReceipt");
			alert.setScreenId(screen.getScreenId());
			paymentList.add(alert.getRecordId()+"@"+alert.getTableName()+alert.getScreenId());
			
			
		List<Alert>  alertExsit=alertBL.getAlertService().getAlerts();
		for (Alert alert2 : alertExsit) {
			if(alert2.getTableName().equals("Release"));
				alertList.add(alert2.getRecordId()+"@"+alert2.getTableName()+alert2.getScreenId());
		}	
		
		Set<String> pl = new HashSet<String>(paymentList);
		for (String string : paymentList) {
			for (String string1 : alertList) {
				 if(string1.equalsIgnoreCase(string)){
					 pl.remove(string);
				 }
			}
		}
		
		Set<Alert> resultset = new HashSet<Alert>();
		
		for (String string : pl) {
			String[] strAry=string.split("@(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
			for(Alert cp :resultList){
				if(cp.getRecordId()==Integer.parseInt(strAry[0])){
					resultset.add(cp);
				}
			}
			
		}
		if(resultset!=null && resultset.size()>0)
			alertBL.getAlertService().saveAlert(resultset);

		
	}
	
	//Getter & setter part
	public ReleaseService getReleaseService() {
		return releaseService;
	}
	public void setReleaseService(
			ReleaseService releaseService) {
		this.releaseService = releaseService;
	}
	public ContractService getContractService() {
		return contractService;
	}
	public void setContractService(
			ContractService contractService) {
		this.contractService = contractService;
	}
	public DocumentService getDocumentService() {
		return documentService;
	}
	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	ComponentService compService;
	PersonService personService;
	UnitService unitService;
	PropertyService propertyService;

	public ComponentService getCompService() {
		return compService;
	}
	public void setCompService(ComponentService compService) {
		this.compService = compService;
	}
	public PersonService getPersonService() {
		return personService;
	}
	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
	public UnitService getUnitService() {
		return unitService;
	}
	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}
	
	/*public ContractReleaseTO getReleaseListInfo(Release release) throws Exception {
		ContractReleaseTO to = new ContractReleaseTO();		
		Release rel =this.getReleaseService().getReleaseWithAll(release);
		to = ContractReleaseTOConverter.convertToTO(rel);
		Contract c = rel.getContract();
		List<Contract> contractList = new ArrayList<Contract>();
		contractList.add(c);			
		to.setReleaseDetails(rel.getReleaseDetails());
		to.setContractList(contractList);
		to.setPropertyList(getProeprty(c.getOffer().getOfferDetails()));
		to.setTenantList(getTenants(c.getOffer().getTenantOffers()));
		return to;
	}*/
	

	
	private List<HrPersonalDetailsTO> getTenants(Set<TenantOffer> tenantOffers ) throws Exception{
		List<HrPersonalDetailsTO> persons = new ArrayList<HrPersonalDetailsTO>();
		for (TenantOffer offer : tenantOffers){
			HrPersonalDetailsTO to = HrPersonalDetailsTOConverter.convertPersonToTO(offer.getPerson());			
			//to.setPersonTypes(personService.getPersonType(offer.getPerson().getPersonType().getPersonTypeId()).getTypeName());
			//to.setEmirateId(offer.getPerson().getEmiratesId());
			persons.add(to);			
		}
		return persons;
	}
	
	
	private List<RePropertyInfoTO> getProperty(Set<OfferDetail> offerDetails) throws Exception{
		List<RePropertyInfoTO> contractProp = new ArrayList<RePropertyInfoTO>();		
		for (OfferDetail detail : offerDetails){
			if (null != detail.getProperty()){	
				Property prop = propertyService.getPropertyWithPropertyType(detail.getProperty());
				contractProp.add(setPropertyAdd(prop));				
			} else if (detail.getComponent()!= null){
				List<Component> comps = compService.getPropertyByComponentId(detail.getComponent());
				for (Component comp : comps){					
					contractProp.add(setPropertyAdd(comp.getProperty()));
				}
			} else if (detail.getUnit() != null){
				List<Unit> units =  unitService.getPropertyByUnitId(detail.getUnit());
				for (Unit u : units){
					contractProp.add(setPropertyAdd(u.getComponent().getProperty()));
				}
			}
		}
		return contractProp;
	}
	
	private RePropertyInfoTO setPropertyAdd(Property p) throws Exception{
		//if (p != null){
		RePropertyInfoTO to  = RePropertyInfoTOConverter.convertToTO(p);
		to.setAddress(propertyService.getCityById(
				Long.parseLong("" + p.getCity())).getCityName()
				+ " , "
				+ propertyService.getStateById(
						Long.parseLong("" + p.getState())).getStateName()
				+ " , "
				+ propertyService.getCounryById(
						Long.parseLong("" + p.getCountry()))
						.getCountryName());
			
			//contractProp.add(to);
		//}
		return to;
	}
	 
	public OfferService getOfferService() {
		return offerService;
	}
	public void setOfferService(OfferService offerService) {
		this.offerService = offerService;
	}
	 
	public DocumentBL getDocumentBL() {
		return documentBL;
	}
	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}
	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}
	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}
	public AlertBL getAlertBL() {
		return alertBL;
	}
	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}
	public SystemService getSystemService() {
		return systemService;
	}
	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}
	public OfferManagementBL getOfferBL() {
		return offerBL;
	}
	public void setOfferBL(OfferManagementBL offerBL) {
		this.offerBL = offerBL;
	}
	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}
	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}
	
	
}
