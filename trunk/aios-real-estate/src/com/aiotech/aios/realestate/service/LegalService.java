package com.aiotech.aios.realestate.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Legal;
import com.aiotech.aios.realestate.to.LegalVO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class LegalService {

	private AIOTechGenericDAO<Legal> legalDAO;
	private RealEstateEnterpriseService realEstateEnterpriseService;
	private AIOTechGenericDAO<Object> dao;
	//Legal List
	public List<Legal> getAllLegal(Legal legal) throws Exception {
		return legalDAO.findByNamedQuery("getAllLegalListRecordBased",legal.getRecordId());
	}
	public void addLegal(Legal legal) throws Exception {
		 legalDAO.saveOrUpdate(legal);
	}

/*public List<Object> getApproved(String usecase,Byte flag,Implementation implementation)
	throws Exception {
		List<Object> objects = null;
		Object callingObject;
		Object usecaseObject;
		Object[] objectParm = { null };
		Boolean param = null;
		usecase="com.aiotech.aios.realestate.domain.entity."+usecase;
		String[] name = usecase.split("entity.");
		//String usecase = (name[name.length - 1]);
		String moduleMethod = "getRealEstateEnterpriseService";
		String usecaseService = "get" + (name[name.length - 1]) + "Service";
		String usecaseDAO = "get" + (name[name.length - 1]) + "DAO";
		
		Method method = this.getClass().getMethod(moduleMethod, null);
		callingObject = method.invoke(this);
		
		method = callingObject.getClass().getMethod(usecaseService, null);
		callingObject = method.invoke(callingObject);
		
		method = callingObject.getClass().getMethod(usecaseDAO, null);
		dao = (AIOTechGenericDAO<Object>) method.invoke(callingObject);
		
		Class cl = Class.forName(usecase);
		Constructor constructor = cl.getConstructor(null);
		usecaseObject = constructor.newInstance(null);
		
		Class[] argTypes = new Class[] { Byte.class };
		method = usecaseObject.getClass().getMethod("setIsApprove", argTypes);
		method.invoke(usecaseObject,flag);
		
		Class[] imple = new Class[] { Implementation.class };
		method = usecaseObject.getClass().getMethod("setImplementation", imple);
		method.invoke(usecaseObject,implementation);
		
		return dao.findByExample(usecaseObject);
	}

	*/
	public Object getRecordObject(Legal legalVO)
	throws Exception {
		List<Object> objects = null;
		Object callingObject;
		Object usecaseObject;
		Object[] objectParm = { null };
		Boolean param = null;
		String usecase="";
		usecase="com.aiotech.aios.realestate.domain.entity."+legalVO.getTableName();
		String[] name = usecase.split("entity.");
		//String usecase = (name[name.length - 1]);
		String moduleMethod = "getRealEstateEnterpriseService";
		String usecaseService = "get" + (name[name.length - 1]) + "Service";
		String usecaseDAO = "get" + (name[name.length - 1]) + "DAO";
		
		Method method = this.getClass().getMethod(moduleMethod, null);
		callingObject = method.invoke(this);
		
		method = callingObject.getClass().getMethod(usecaseService, null);
		callingObject = method.invoke(callingObject);
		
		method = callingObject.getClass().getMethod(usecaseDAO, null);
		dao = (AIOTechGenericDAO<Object>) method.invoke(callingObject);
		
		Class cl = Class.forName(usecase);
		Constructor constructor = cl.getConstructor(null);
		usecaseObject = constructor.newInstance(null);
		
		return dao.findById(legalVO.getRecordId());
	}
	
	public AIOTechGenericDAO<Legal> getLegalDAO() {
		return legalDAO;
	}

	public void setLegalDAO(AIOTechGenericDAO<Legal> legalDAO) {
		this.legalDAO = legalDAO;
	}

	public RealEstateEnterpriseService getRealEstateEnterpriseService() {
		return realEstateEnterpriseService;
	}

	public void setRealEstateEnterpriseService(
			RealEstateEnterpriseService realEstateEnterpriseService) {
		this.realEstateEnterpriseService = realEstateEnterpriseService;
	}

	public AIOTechGenericDAO<Object> getDao() {
		return dao;
	}

	public void setDao(AIOTechGenericDAO<Object> dao) {
		this.dao = dao;
	}
}
