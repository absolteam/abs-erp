package com.aiotech.aios.realestate.service.bl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.service.bl.DirectPaymentBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.realestate.domain.entity.PropertyExpenseDetail;
import com.aiotech.aios.realestate.service.PropertyExpenseService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.bl.AlertBL;

public class PropertyExpenseBL {
	private static final Logger LOGGER = LogManager
			.getLogger(PropertyExpenseBL.class);

	private List<PropertyExpense> propertyExpense;
	private PropertyExpenseService propertyExpenseService;
	private TransactionBL transactionBL;
	private PropertyService propertyService;
	private Implementation implementation;
	private DirectPaymentBL directPaymentBL;
	private AlertBL alertBL;

	public JSONObject getPropertyExpenseList() throws Exception {
		propertyExpense = this.getPropertyExpenseService()
				.getAllPropertyExpense(getImplementationId());
		LOGGER.info("PROPERTY EXPENSE LIST EMPTY " + propertyExpense.size());
		if (null != propertyExpense && !propertyExpense.equals("")) {
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("iTotalRecords", propertyExpense.size());
			jsonResponse.put("iTotalDisplayRecords", propertyExpense.size());
			JSONArray data = new JSONArray();
			JSONArray array = null;
			for (PropertyExpense list : propertyExpense) {
				array = new JSONArray();
				array.add(list.getExpenseId());
				array.add(list.getExpenseNumber());
				array.add(DateFormat.convertDateToString(list.getDate()
						.toString()));
				array.add(Constants.RealEstate.PropertyExpenseType.get(list
						.getExpenseType()));
				array.add(list.getProperty().getPropertyName());
				data.add(array);
			}
			jsonResponse.put("aaData", data);
			return jsonResponse;
		} else {
			LOGGER.info("PROPERTY EXPENSE LIST EMPTY");
			return null;
		}
	}

	// Save Property Expense
	public void savePropertyExpense(PropertyExpense propertyExpense,
			List<PropertyExpenseDetail> propertyExpenseDetailList)
			throws Exception {
		this.getPropertyExpenseService().savePropertyExpense(propertyExpense,
				propertyExpenseDetailList);
		Alert alert = alertBL.getAlertService().getAlertRecordInfo(
				propertyExpense.getExpenseId(), "PropertyExpense");
		if (null != alert && !("").equals(alert)) {
			alert.setIsActive(true);
			alertBL.commonSaveAlert(alert);
		} else {
			List<PropertyExpense> propertyExpenses = new ArrayList<PropertyExpense>();
			Set<PropertyExpenseDetail> propertyExpenseDetails = new HashSet<PropertyExpenseDetail>(
					propertyExpenseDetailList);
			propertyExpense.setPropertyExpenseDetails(propertyExpenseDetails);
			propertyExpenses.add(propertyExpense);
			alertBL.alertPropertyExpense(propertyExpenses);
		}
	}

	// Delete Property Expense, DirectPayment & Reverse the Transaction
	public boolean deletePropertyExpense(PropertyExpense propertyExpense)
			throws Exception {
		/*if (null != propertyExpense.getDirectPayments()) {
			DirectPayment directPayment = new ArrayList<DirectPayment>(
					propertyExpense.getDirectPayments()).get(0);
			if (null != directPayment.getVoidPayments()
					&& directPayment.getVoidPayments().size() > 0) {
				return false;
			}
			this.getDirectPaymentBL().deleteDirectPayment(directPayment);
		}
		this.getPropertyExpenseService().deletePropertyExpense(
				propertyExpense,
				new ArrayList<PropertyExpenseDetail>(propertyExpense
						.getPropertyExpenseDetails()));*/
		return true;
	}

	public Combination getBuildingExpenseAccount(PropertyExpense propertyExpense)
			throws Exception {
		Property property = this.getPropertyService().getPropertyById(
				propertyExpense.getProperty().getPropertyId());
		Account account = this
				.getTransactionBL()
				.getCombinationService()
				.getAccountDetail(
						Constants.RealEstate.PropertyExpenseType
								.get(propertyExpense.getExpenseType())
								.toString().concat(" ")
								.concat(property.getPropertyName()),
						property.getImplementation());
		return this.getTransactionBL().getCombinationService()
				.getCombinationByAccount(account.getAccountId());
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public PropertyExpenseService getPropertyExpenseService() {
		return propertyExpenseService;
	}

	public void setPropertyExpenseService(
			PropertyExpenseService propertyExpenseService) {
		this.propertyExpenseService = propertyExpenseService;
	}

	public List<PropertyExpense> getPropertyExpense() {
		return propertyExpense;
	}

	public void setPropertyExpense(List<PropertyExpense> propertyExpense) {
		this.propertyExpense = propertyExpense;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public DirectPaymentBL getDirectPaymentBL() {
		return directPaymentBL;
	}

	public void setDirectPaymentBL(DirectPaymentBL directPaymentBL) {
		this.directPaymentBL = directPaymentBL;
	}
}
