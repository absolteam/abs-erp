package com.aiotech.aios.realestate.service.bl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.CancellationService;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.ReleaseService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.CancellationTO;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;

public class CancellationBL {
	private CancellationService cancellationService;
	private ContractService contractService; 
	private ReleaseService releaseService; 
	private SystemService systemService;
	List<CancellationTO> cancelTOList=null;
	private OfferManagementBL offerBL;
	private PropertyService propertyService;
	private UnitService unitService;
	private AlertBL alertBL;
	private TransactionBL transactionBL;
	 
	public List<CancellationTO> getReleaseListInformation(Implementation implementation) {

		try {
			cancelTOList=new ArrayList<CancellationTO>();
			List<Cancellation> cancels=cancellationService.getAllCancellation(implementation);
			for (Cancellation cancellation : cancels) {
				CancellationTO to=new CancellationTO();
				to.setCancellationId(cancellation.getCancellationId());
				to.setContractId(cancellation.getContract().getContractId());
				cancelTOList.add(to);
			}
			//cancelTOList = ContractReleaseTOConverter.convertToTOList(cancels);
			if(cancels!=null && cancels.size()>0){
				for (int i = 0; i <cancels.size(); i++) {
				
					Contract contract=new Contract();

					contract=this.contractService.getContractDetails(cancelTOList.get(i).getContractId());
					
					cancelTOList.get(i).setContractNumber(contract.getContractNo());
					cancelTOList.get(i).setContractDate(DateFormat.convertDateToString(contract.getContractDate().toString()));
					Offer offer=new Offer();
					List<Person> persons=new ArrayList<Person>();
					
					//Find offer
					offer=this.contractService.getOfferDetails(contract.getOffer().getOfferId());
					if(offer.getStartDate()!=null)
						cancelTOList.get(i).setFromDate(DateFormat.convertDateToString(offer.getStartDate().toString()));
					if(offer.getEndDate()!=null)
						cancelTOList.get(i).setToDate(DateFormat.convertDateToString(offer.getEndDate().toString()));
					
					//Find Tenent offer
					List<TenantOffer> tenentOffers=this.contractService.getTenantOfferDetails(offer.getOfferId());
					if(tenentOffers!=null && tenentOffers.size()>0){
					//Find Person
						Company company=null;
						if(tenentOffers!=null && tenentOffers.size()>0 && tenentOffers.get(0).getPerson()!=null){
						//Find Person
							persons=this.contractService.getPersonDetails(tenentOffers.get(0).getPerson());
							cancelTOList.get(i).setTenantName(persons.get(0).getFirstName()+" "+persons.get(0).getLastName());
						}else{
							company=new Company();
							company=this.getOfferBL().getOfferService().getCompanyDetails(tenentOffers.get(0).getCompany().getCompanyId());
							if(company!=null){
								cancelTOList.get(i).setTenantName(company.getCompanyName());
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cancelTOList;

	}
	
	public Double getPenantyAmount(Cancellation cancel, Offer offer)
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		int penaltyMonths = 0;
		if (session.getAttribute("penalty_months") != null)
			penaltyMonths = Integer.parseInt(session.getAttribute("penalty_months").toString()) ;
		double penaltyAmount = 0; 
		Contract contract = this.getContractService().getContractInfo(
				cancel.getContract().getContractId());
		List<ContractPayment> contractPayments = new ArrayList<ContractPayment>(
				contract.getContractPayments());
		for (ContractPayment list : contractPayments) {
			if (null != list.getFeeType() && list.getFeeType() == 3) {
				penaltyAmount += list.getAmount();
			}
		}
		double totalOfferPeriod = Math.round(DateFormat.monthDifferenceUnRound(
				offer.getStartDate(), offer.getEndDate()));
		double monthlyRent = penaltyAmount / totalOfferPeriod;
		penaltyAmount = monthlyRent * penaltyMonths;
		return Double.valueOf(AIOSCommons.formatAmount(penaltyAmount).replaceAll(",", "")); 
	}
	
	public void cancelSave(Cancellation cancel, Contract contract) throws Exception {
		this.getCancellationService().cancelSave(cancel);
		/*if(cancel.getIsPenalty()){
			Role role = new Role();
			Alert alert = new Alert();
			role.setRoleId(cancel.getImplementation().getReceivableClerk());
			alert.setRole(role); 
			alert.setMessage("Contract Cancellation for Contract No: "
					+ contract.getContractNo());
			Screen screen = systemService
					.getScreenBasedOnScreenName("ContractCancelAlert");
			alert.setScreen(screen);
			alert.setIsActive(true);
			alertBL.saveAlerts(cancel, alert);
		}*/
	}
	
	public CancellationTO processContractView(Release relase)
			throws Exception {
		Contract contract = this.getContractService().getContractInfo(
				relase.getContract().getContractId());
		Cancellation cancellation = this.getCancellationService()
				.getCancellationUseContract(contract); 
		List<ContractPayment> contractPayments = new ArrayList<ContractPayment>(
				contract.getContractPayments());
		TenantOffer tenentOffer = this.getContractService()
				.getTenantContractDetails(contract.getOffer().getOfferId())
				.get(0);
		CancellationTO cancellationTO = new CancellationTO();
		List<CancellationTO> cancelList = new ArrayList<CancellationTO>();
		CancellationTO tempCancellationTO = null;
		if (null != tenentOffer) {
			if (tenentOffer.getPerson() != null
					&& !tenentOffer.getPerson().equals("")) {
				cancellationTO.setTenantName(tenentOffer.getPerson()
						.getFirstName().concat(" ")
						.concat(tenentOffer.getPerson().getLastName()));
				cancellationTO.setDescription(tenentOffer.getPerson()
						.getPersonId().toString().concat("@P"));
			} else {
				cancellationTO.setTenantName(tenentOffer.getCompany()
						.getCompanyName());
				cancellationTO.setDescription(tenentOffer.getCompany()
						.getCompanyId().toString().concat("@C"));
			}
		}
		Property property = new Property();
		Offer offer = this.getOfferBL().getOfferService()
				.getOffersById(tenentOffer.getOffer().getOfferId());
		if (null != tenentOffer.getOffer().getOfferId()) {
			OfferDetail offerDetail = this.getOfferBL().getOfferService()
					.getOfferDetailById(tenentOffer.getOffer().getOfferId())
					.get(0);
			if (null != offerDetail) {
				if (null != offerDetail.getProperty()
						&& !offerDetail.getProperty().equals("")) {
					property = this.getPropertyService().getPropertyById(
							offerDetail.getProperty().getPropertyId());
				} else {
					Unit units = this.getUnitService()
							.getUnitCompsWithProperty(offerDetail.getUnit());
					property.setPropertyId(units.getComponent().getProperty()
							.getPropertyId());
					property.setPropertyName(units.getComponent().getProperty()
							.getPropertyName());
				}
			}
		}
		cancellationTO.setPropertyName(property.getPropertyName());
		cancellationTO.setPropertyId(property.getPropertyId());
		Period period = null;
		Calendar calendar = this.getTransactionBL().getCalendarBL().getCalendarService()
				.getAtiveCalendar(contract.getImplementation());
		if (null != calendar)
			period = this.getTransactionBL().getCalendarBL().getCalendarService()
					.getActivePeriod(calendar.getCalendarId());
		cancelList = new ArrayList<CancellationTO>();
		for (ContractPayment list : contractPayments) {
			if (list.getFeeType() == 2 || list.getFeeType() == 3) { 
				tempCancellationTO = new CancellationTO();
				tempCancellationTO = addPayments(list, period.getStartTime(),
						offer, cancellation, property.getPropertyName());
				cancelList.add(tempCancellationTO);  
			}
		}
		if (cancellation.getIsPenalty()) {
			if (cancellation.getIsRequested()) {
				cancellationTO.setFeeDescription("Penalty(Tenant)");
				Account account = this
						.getTransactionBL()
						.getCombinationService()
						.getAccountDetail("Other Income ".concat(property.getPropertyName()),
								getImplementId());
				Combination combination = this.getTransactionBL()
						.getCombinationService()
						.getCombinationByAccount(account.getAccountId());
				cancellationTO.setIncomeAccountCode(combination
						.getAccountByNaturalAccountId()
						.getCode()
						.concat(".")
						.concat(combination.getAccountByAnalysisAccountId()
								.getCode())
						.concat(" [")
						.concat(combination.getAccountByNaturalAccountId()
								.getAccount())
						.concat(".")
						.concat(combination.getAccountByAnalysisAccountId()
								.getAccount()).concat("]"));
			} else {
				cancellationTO.setFeeDescription("Penalty(Management)");
				Account account = this
						.getTransactionBL()
						.getCombinationService()
						.getAccountDetail("Others ".concat(property.getPropertyName()),
								getImplementId());
				Combination combination = this.getTransactionBL()
						.getCombinationService()
						.getCombinationByAccount(account.getAccountId());
				cancellationTO.setExpenseAccountCode(combination
						.getAccountByNaturalAccountId()
						.getCode()
						.concat(".")
						.concat(combination.getAccountByAnalysisAccountId()
								.getCode())
						.concat(" [")
						.concat(combination.getAccountByNaturalAccountId()
								.getAccount())
						.concat(".")
						.concat(combination.getAccountByAnalysisAccountId()
								.getAccount()).concat("]"));
			} 
			cancellationTO.setPenaltyAmount(cancellation.getPenaltyAmount());
			cancelList.add(cancellationTO); 
		} 
		cancellationTO.setCancellationList(cancelList);
		return cancellationTO;
	}

	private CancellationTO addPayments(ContractPayment contractPayment,
			Date financialPeriod, Offer offer, Cancellation cancellation,
			String propertyName) throws Exception {
		CancellationTO infoTO = new CancellationTO();
		infoTO.setFeeDescription(Constants.RealEstate.ContractFeeType.get(
				contractPayment.getFeeType()).toString());
		infoTO.setFeeId(contractPayment.getFeeType());
		double paymentAmount = contractPayment.getAmount();
		double totalOfferPeriod = DateFormat.monthDifference(
				offer.getStartDate(), offer.getEndDate());
		double rentalReturn = 0;
		if (null == contractPayment.getChequeDate()
				&& contractPayment.getFeeType() == 3) {
			double monthDifference = DateFormat.monthDifference(
					financialPeriod, offer.getStartDate());
			if (totalOfferPeriod > 0) {
				double monthlyRental = paymentAmount / totalOfferPeriod;
				rentalReturn = monthlyRental * monthDifference;
				infoTO.setRentalReturn(AIOSCommons.formatAmount(rentalReturn));
			}
		} else if (null != contractPayment.getChequeDate()
				&& !contractPayment.getChequeDate().equals("")
				&& contractPayment.getFeeType() == 3) {
			double monthDifference = DateFormat.monthDifference(
					 financialPeriod, contractPayment.getChequeDate());
			if (monthDifference > 0) {
				double monthlyRental = paymentAmount / totalOfferPeriod;
				double rentIncome = monthlyRental * monthDifference;
				rentalReturn = contractPayment.getAmount() - rentIncome; 
				infoTO.setRentalReturn(AIOSCommons.formatAmount(rentalReturn));
				infoTO.setPdcAccount(false);
				infoTO.setUnEarnedAccountId(getImplementId()
						.getUnearnedRevenue());
				Combination combination = this
						.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(
								infoTO.getUnEarnedAccountId());
				infoTO.setUnEarnedAccountCode(combination
						.getAccountByNaturalAccountId()
						.getCode()
						.concat(" [")
						.concat(combination.getAccountByNaturalAccountId()
								.getAccount()).concat("]"));
			} else {
				infoTO.setRentalReturn(AIOSCommons.formatAmount(paymentAmount));
				infoTO.setPdcAccount(true);
				infoTO.setPdcAccountId(getImplementId().getPdcReceived());
				Combination combination = this.getTransactionBL()
						.getCombinationService()
						.getCombinationAccountDetail(infoTO.getPdcAccountId());
				infoTO.setPdcAccountCode(combination
						.getAccountByNaturalAccountId()
						.getCode()
						.concat(" [")
						.concat(combination.getAccountByNaturalAccountId()
								.getAccount()).concat("]"));
			}
		} else if (contractPayment.getFeeType() == 2) {
			infoTO.setPdcAccount(false);
			infoTO.setRentalReturn(AIOSCommons.formatAmount(paymentAmount));
			infoTO.setDepositAccountId(getImplementId().getUnearnedRevenue());
			Combination combination = this.getTransactionBL()
					.getCombinationService()
					.getCombinationAccountDetail(infoTO.getDepositAccountId());
			infoTO.setDepositAccountCode(combination
					.getAccountByNaturalAccountId()
					.getCode()
					.concat(" [")
					.concat(combination.getAccountByNaturalAccountId()
							.getAccount()).concat("]"));
		}
		return infoTO;
	}
	
	public Implementation getImplementId(){
		HttpSession session = ServletActionContext.getRequest()
		.getSession();
		Implementation implementation=new Implementation();
		if(session.getAttribute("THIS")!=null){
			implementation=(Implementation)session.getAttribute("THIS");
		}
		return implementation;
	}

	public CancellationService getCancellationService() {
		return cancellationService;
	}

	public void setCancellationService(CancellationService cancellationService) {
		this.cancellationService = cancellationService;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	} 

	public ReleaseService getReleaseService() {
		return releaseService;
	}

	public void setReleaseService(ReleaseService releaseService) {
		this.releaseService = releaseService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public OfferManagementBL getOfferBL() {
		return offerBL;
	}

	public void setOfferBL(OfferManagementBL offerBL) {
		this.offerBL = offerBL;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	} 
}
