package com.aiotech.aios.realestate.service;

import java.util.List;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class TenantGroupService {

	private AIOTechGenericDAO<TenantGroup> tenantGroupDAO;

	public AIOTechGenericDAO<TenantGroup> getTenantGroupDAO() {
		return tenantGroupDAO;
	}

	public void setTenantGroupDAO(AIOTechGenericDAO<TenantGroup> tenantGroupDAO) {
		this.tenantGroupDAO = tenantGroupDAO;
	}

	public List<TenantGroup> getAllGroups(Implementation implementation) {
		return tenantGroupDAO.findByNamedQuery("getAllTenantGroups",
				implementation);
	}
	
	public TenantGroup getGroupById(int id) {
		return tenantGroupDAO.findById(id);
	}

	public void addUpdateTenantGroup(TenantGroup tenantGroup) {
		tenantGroupDAO.saveOrUpdate(tenantGroup);
	}

	public void deleteTenantGroup(TenantGroup tenantGroup) {
		tenantGroupDAO.delete(tenantGroup);
	}

}
