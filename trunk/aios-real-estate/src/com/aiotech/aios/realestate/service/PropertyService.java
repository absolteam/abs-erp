package com.aiotech.aios.realestate.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;

import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.realestate.domain.entity.Maintenance;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.system.domain.entity.City;
import com.aiotech.aios.system.domain.entity.Country;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.State;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class PropertyService {

	private AIOTechGenericDAO<PropertyType> propertyTypeDAO;
	private AIOTechGenericDAO<Property> propertyDAO;
	private AIOTechGenericDAO<Company> companyDAO;
	private AIOTechGenericDAO<Person> personDAO;
	private AIOTechGenericDAO<PropertyOwnership> propertyOwnershipDAO;
	private AIOTechGenericDAO<Maintenance> maintenanceDAO;
	private AIOTechGenericDAO<PropertyDetail> propertyDetailDAO;
	private AIOTechGenericDAO<City> cityDAO;
	private AIOTechGenericDAO<State> stateDAO;

	public AIOTechGenericDAO<City> getCityDAO() {
		return cityDAO;
	}

	public void setCityDAO(AIOTechGenericDAO<City> cityDAO) {
		this.cityDAO = cityDAO;
	}

	public AIOTechGenericDAO<State> getStateDAO() {
		return stateDAO;
	}

	public void setStateDAO(AIOTechGenericDAO<State> stateDAO) {
		this.stateDAO = stateDAO;
	}

	public AIOTechGenericDAO<Country> getCountryDAO() {
		return countryDAO;
	}

	public void setCountryDAO(AIOTechGenericDAO<Country> countryDAO) {
		this.countryDAO = countryDAO;
	}

	private AIOTechGenericDAO<Country> countryDAO;

	public int getPropertiesCount() throws Exception {
		Query query = propertyDAO.getNamedQuery("getPropertiesCount");

		if (query.list().iterator().next() != null) {
			return ((Long) query.list().iterator().next()).intValue();
		} else {
			return 0;
		}
	}

	public List<PropertyOwnership> getOwnerCompaniesForProperty(
			Property property) throws Exception {
		return propertyOwnershipDAO.findByNamedQuery("getOwners",
				property.getPropertyId(), "COMPANY");
	}

	public List<PropertyOwnership> getOwnerPersonForProperty(Property property)
			throws Exception {
		return propertyOwnershipDAO.findByNamedQuery("getOwners",
				property.getPropertyId(), "PERSON");
	}

	public List<PropertyDetail> getPropertyDetails(Property property)
			throws Exception {
		try {
			return propertyDetailDAO.findByNamedQuery("getPropertyDetails",
					property.getPropertyId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Maintenance> getMaintenance(Property property) throws Exception {
		return maintenanceDAO.findByNamedQuery("getMaintenance",
				property.getPropertyId());
	}

	public List<Company> getAllCompanies() throws Exception {
		return companyDAO.getAll();
	}

	public Company getCompanyById(int companyId) throws Exception {
		return companyDAO.findById((long) companyId);
	}

	public List<Person> getAllPeopleWithTypeOwner(Implementation implementation)
			throws Exception {
		return personDAO.findByNamedQuery("getAllPeopleWithTypeOwner",
				implementation);
	}

	public List<Company> getAllOwnerCompanies(Implementation implementation) {
		return companyDAO.findByNamedQuery("getOwnerCompanies", implementation);
	}

	public List<Company> getAllMaintenanceCompanies(
			Implementation implementation) {
		return companyDAO.findByNamedQuery("getMaintenanceCompanies",
				implementation);
	}

	public void updateProperty(Property property) throws Exception {

		propertyDAO.saveOrUpdate(property);
	}

	public Property addProperty(Property property) throws Exception {

		propertyDAO.saveOrUpdate(property);
		List<PropertyOwnership> ownerships = new ArrayList<PropertyOwnership>();
		for (PropertyOwnership propertyOwnership : property
				.getPropertyOwnerships()) {
			propertyOwnership.setProperty(property);
			ownerships.add(propertyOwnership);
		}
		propertyOwnershipDAO.saveOrUpdateAll(ownerships);

		property.setPropertyOwnerships(new HashSet<PropertyOwnership>(
				ownerships));

		List<PropertyDetail> details = new ArrayList<PropertyDetail>();
		for (PropertyDetail propertyDetail : property.getPropertyDetails()) {
			propertyDetail.setProperty(property);
			details.add(propertyDetail);
		}
		propertyDetailDAO.saveOrUpdateAll(details);

		property.setPropertyDetails(new HashSet<PropertyDetail>(details));

		List<Maintenance> maintenance = new ArrayList<Maintenance>();
		for (Maintenance propertyMaintenance : property.getMaintenances()) {
			propertyMaintenance.setProperty(property);
			maintenance.add(propertyMaintenance);
			try {
				List<Maintenance> propertyMaintenances = getMaintenance(property);
				if (propertyMaintenances != null
						&& propertyMaintenances.size() > 0)
					maintenanceDAO.delete(propertyMaintenances.iterator()
							.next());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		maintenanceDAO.saveOrUpdateAll(maintenance);

		property.setMaintenances(new HashSet<Maintenance>(maintenance));
		return property;
	}

	public Person getPersonbyId(Long id) throws Exception {
		return personDAO.findById(id);
	}

	public List<PropertyType> getAllPropertyTypes() throws Exception {
		return propertyTypeDAO.findByNamedQuery("getAllPropertyTypes");
	}

	public PropertyType getPropertyTypeById(int propertyTypeId)
			throws Exception {
		return propertyTypeDAO.findById(propertyTypeId);
	}

	public List<Property> getAllProperties(Implementation implementation)
			throws Exception {
		return propertyDAO.findByNamedQuery("getAllProperties", implementation);
	}

	// Method is only for build changes update
	public List<PropertyDetail> getAllPropertyDetail(
			Implementation implementation) throws Exception {
		return propertyDetailDAO.findByNamedQuery("getAllPropertyDetail",
				implementation);
	}

	public List<Property> getAllPropertiesReport(Implementation implementation)
			throws Exception {
		return propertyDAO.findByNamedQuery("getAllPropertiesReport",
				implementation);
	}

	public List<Property> getAllPropertiesSizeReport(
			Implementation implementation, Byte status) throws Exception {
		return propertyDAO.findByNamedQuery("getAllPropertiesSizeReport",
				implementation, status);
	}

	public List<Property> getComponenetProperties(Implementation implementation)
			throws Exception {
		return propertyDAO.findByNamedQuery("getComponenetProperties",
				implementation);
	}

	public Property getPropertyById(long id) throws Exception {
		return propertyDAO.findById(id);
	}

	public void deleteOwnership(PropertyOwnership propertyOwnership)
			throws Exception {
		propertyOwnershipDAO.delete(propertyOwnership);
	}

	public void deleteOwnershipList(List<PropertyOwnership> propertyOwnership)
			throws Exception {
		propertyOwnershipDAO.deleteAll(propertyOwnership);
	}

	public void deletePropertyDetail(PropertyDetail propertyDetail)
			throws Exception {
		// // ALSO DELETE IMAGES RELATED TO THIS DETAIL OBJECT
		propertyDetailDAO.delete(propertyDetail);
	}

	public void updatePropertyOwner(PropertyOwnership ownership)
			throws Exception {
		propertyOwnershipDAO.saveOrUpdate(ownership);
	}

	public void updatePropertyDetail(PropertyDetail detail) throws Exception {
		propertyDetailDAO.saveOrUpdate(detail);
	}

	public void deletePropertyById(long id) throws Exception {
		Property property = this.getPropertyById(id);

		List<PropertyDetail> detail = (List<PropertyDetail>) this
				.getPropertyDetails(property);
		propertyDetailDAO.deleteAll(detail);

		List<PropertyOwnership> owners = (List<PropertyOwnership>) this
				.getOwnerCompaniesForProperty(property);
		List<PropertyOwnership> ownerPeople = (List<PropertyOwnership>) this
				.getOwnerPersonForProperty(property);
		if (ownerPeople != null)
			owners.addAll(ownerPeople);

		propertyOwnershipDAO.deleteAll(owners);

		List<Maintenance> maintenance = (List<Maintenance>) this
				.getMaintenance(property);
		maintenanceDAO.deleteAll(maintenance);

		propertyDAO.delete(property);
	}

	public AIOTechGenericDAO<PropertyType> getPropertyTypeDAO() {
		return propertyTypeDAO;
	}

	public void setPropertyTypeDAO(
			AIOTechGenericDAO<PropertyType> propertyTypeDAO) {
		this.propertyTypeDAO = propertyTypeDAO;
	}

	public AIOTechGenericDAO<Property> getPropertyDAO() {
		return propertyDAO;
	}

	public void setPropertyDAO(AIOTechGenericDAO<Property> propertyDAO) {
		this.propertyDAO = propertyDAO;
	}

	public AIOTechGenericDAO<Company> getCompanyDAO() {
		return companyDAO;
	}

	public void setCompanyDAO(AIOTechGenericDAO<Company> companyDAO) {
		this.companyDAO = companyDAO;
	}

	public AIOTechGenericDAO<Person> getPersonDAO() {
		return personDAO;
	}

	public void setPersonDAO(AIOTechGenericDAO<Person> personDAO) {
		this.personDAO = personDAO;
	}

	public AIOTechGenericDAO<PropertyOwnership> getPropertyOwnershipDAO() {
		return propertyOwnershipDAO;
	}

	public void setPropertyOwnershipDAO(
			AIOTechGenericDAO<PropertyOwnership> propertyOwnershipDAO) {
		this.propertyOwnershipDAO = propertyOwnershipDAO;
	}

	public AIOTechGenericDAO<Maintenance> getMaintenanceDAO() {
		return maintenanceDAO;
	}

	public void setMaintenanceDAO(AIOTechGenericDAO<Maintenance> maintenanceDAO) {
		this.maintenanceDAO = maintenanceDAO;
	}

	public AIOTechGenericDAO<PropertyDetail> getPropertyDetailDAO() {
		return propertyDetailDAO;
	}

	public void setPropertyDetailDAO(
			AIOTechGenericDAO<PropertyDetail> propertyDetailDAO) {
		this.propertyDetailDAO = propertyDetailDAO;
	}

	public List<PropertyOwnership> getPropertyOwners(PropertyOwnership ownership)
			throws Exception {
		List<PropertyOwnership> ownerList = getOwnerCompaniesForProperty(ownership
				.getProperty());
		ownerList.addAll(getOwnerPersonForProperty(ownership.getProperty()));
		return ownerList;
	}

	public List<Maintenance> getPropertyMaintenance(Maintenance maintenance) {
		return maintenanceDAO.findByExample(maintenance);
	}

	public City getCityById(Long id) {
		return cityDAO.findById(id);
	}

	public State getStateById(Long id) {
		return stateDAO.findById(id);
	}

	public Country getCounryById(Long id) {
		return countryDAO.findById(id);
	}

	public Property getPropertyWithPropertyType(Property prop) {
		return propertyDAO.findByNamedQuery("getPropertyWithPropertyType",
				prop.getPropertyId()).get(0);
	}

	public List<PropertyOwnership> getPropertyOwners(Property prop) {
		return propertyOwnershipDAO.findByNamedQuery("getPropertyOwners", prop);
	}

	public void deletePropertyDetailList(List<PropertyDetail> propertyDetailList)
			throws Exception {
		this.getPropertyDetailDAO().deleteAll(propertyDetailList);
	}

	public List<PropertyOwnership> getPropertyOwnersByPropertyId(Long propertyId)
			throws Exception {
		return propertyOwnershipDAO.findByNamedQuery(
				"getPropertyOwnersByPropertyId", propertyId);
	}

	public List<PropertyDetail> getPropertyDetailsById(Long propertyId)
			throws Exception {
		return propertyDetailDAO.findByNamedQuery("getPropertyDetails",
				propertyId);
	}
}
