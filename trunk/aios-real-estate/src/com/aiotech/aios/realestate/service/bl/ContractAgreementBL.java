package com.aiotech.aios.realestate.service.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Period;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.TransactionTempDetail;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.action.ReContractAgreementAction;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.ContractService;
import com.aiotech.aios.realestate.service.OfferService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.ReContractAgreementTO;
import com.aiotech.aios.realestate.to.converter.ReContractAgreementTOConverter;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.domain.entity.Alert;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.SystemService;
import com.aiotech.aios.system.service.bl.AlertBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.AlertRole;
import com.aiotech.aios.workflow.domain.entity.Screen;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.entity.Workflow;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetail;
import com.aiotech.aios.workflow.domain.entity.WorkflowDetailProcess;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;
import com.opensymphony.xwork2.ActionContext;

public class ContractAgreementBL {
	private static final Logger LOGGER = LogManager
			.getLogger(ReContractAgreementAction.class);

	ContractService contractService;
	OfferService offerService;
	OfferManagementBL offerBL;
	CancellationBL cancellationBL;
	DocumentBL documentBL;
	DirectoryBL directoryBL;
	private SystemService systemService;
	private ComponentService componentService;
	private AIOTechGenericDAO<Workflow> workflowDAO;
	private AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO;
	private AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO;

	private WorkflowEnterpriseService workflowEnterpriseService;

	private TransactionBL transactionBL;
	private LegalBL legalBL;
	private AlertBL alertBL;
	private ContractReleaseBL contractReleaseBL;
	List<ReContractAgreementTO> agreementTOList = null;
	ReContractAgreementTO agreementTO = null;
	Contract contract = null;
	List<Contract> contracts = null;
	ContractPayment contractPayment = null;
	List<ContractPayment> contractPayments = null;

	List<Offer> offers = null;
	Offer offer = null;
	TenantOffer tenentOffer = null;
	List<TenantOffer> tenentOffers = null;
	List<Person> persons = null;
	List<OfferDetail> offerDetails = null;
	Property property = null;
	Component component = null;
	Unit unit = null;
	private long propertyId;
	Implementation implementation;

	public List<ReContractAgreementTO> getContractTenantInformation(
			Implementation implementation) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO contractTO = null;
			boolean flag = false;
			contracts = contractService.getAllContractDetails(implementation);
			if (null != contracts && contracts.size() > 0) {
				for (Contract list : contracts) {
					tenentOffers = contractService
							.getTenantContractDetails(list.getOffer()
									.getOfferId());
					if (tenentOffers != null && tenentOffers.size() > 0) {
						for (TenantOffer tenant : tenentOffers) {
							if (tenant.getPerson() != null
									&& !tenant.getPerson().equals("")) {
								contractTO = new ReContractAgreementTO();
								contractTO.setTenantName(tenant.getPerson()
										.getFirstName()
										+ " "
										+ tenant.getPerson().getLastName());
								contractTO.setDescription(tenant.getPerson()
										.getPersonId() + "@P");
							} else {
								contractTO = new ReContractAgreementTO();
								contractTO.setTenantName(tenant.getCompany()
										.getCompanyName());
								contractTO.setDescription(tenant.getCompany()
										.getCompanyId() + "@C");
							}
							for (ReContractAgreementTO contractList : agreementTOList) {
								if (!contractList.getDescription()
										.equalsIgnoreCase(
												contractTO.getDescription())) {
									flag = true;
								} else {
									flag = false;
								}
							}
							if (flag == true || agreementTOList.size() == 0) {
								agreementTOList.add(contractTO);
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	public ReContractAgreementTO getContractDetails(long contractId)
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		session.removeAttribute("CONTRACT_PAYMENTS_"
				+ sessionObj.get("jSessionId"));
		agreementTO = new ReContractAgreementTO();
		contract = contractService.getContractTransaction(contractId);
		tenentOffer = contractService.getTenantContractDetails(
				contract.getOffer().getOfferId()).get(0);
		agreementTO = ReContractAgreementTOConverter.convertToTO(contract);
		if (null != tenentOffer) {
			if (tenentOffer.getPerson() != null
					&& !tenentOffer.getPerson().equals("")) {
				agreementTO.setTenantName(tenentOffer.getPerson()
						.getFirstName()
						+ " "
						+ tenentOffer.getPerson().getLastName());
				agreementTO.setDescription(tenentOffer.getPerson()
						.getPersonId() + "@P");
			} else {
				agreementTO.setTenantName(tenentOffer.getCompany()
						.getCompanyName());
				agreementTO.setDescription(tenentOffer.getCompany()
						.getCompanyId() + "@C");
			}
		}
		Property property = new Property();
		if (null != tenentOffer.getOffer().getOfferId()) {
			OfferDetail offerDetail = this.getOfferBL().getOfferService()
					.getOfferDetailById(tenentOffer.getOffer().getOfferId())
					.get(0);
			if (null != offerDetail) {
				if (null != offerDetail.getProperty()
						&& !offerDetail.getProperty().equals("")) {
					property = this.getPropertyService().getPropertyById(
							offerDetail.getProperty().getPropertyId());
				} else {
					Unit units = this.getUnitService()
							.getUnitCompsWithProperty(offerDetail.getUnit());
					property.setPropertyId(units.getComponent().getProperty()
							.getPropertyId());
					property.setPropertyName(units.getComponent().getProperty()
							.getPropertyName());
				}
			}
		}
		contractPayments = contractService.getContractPaymentList(contract
				.getContractId());
		com.aiotech.aios.accounts.domain.entity.Calendar calendar = this
				.getTransactionBL().getCalendarBL().getCalendarService()
				.getAtiveCalendar(contract.getImplementation());
		Period period = this.getTransactionBL().getCalendarBL()
				.getCalendarService().getActivePeriod(calendar.getCalendarId());
		agreementTO.setPropertyName(property.getPropertyName());
		agreementTO.setPropertyId(property.getPropertyId());
		agreementTO.setContractAgreementList(ReContractAgreementTOConverter
				.convertToPaymentList(contractPayments, period.getEndTime()));
		for (ReContractAgreementTO list : agreementTO
				.getContractAgreementList()) {
			if (null != list.getActionFlag()
					&& !list.getActionFlag()
					&& Constants.RealEstate.ContractFeeType
							.get((byte) list.getFeeId()).toString()
							.equalsIgnoreCase("Rent")) {
				Account account = this
						.getTransactionBL()
						.getCombinationService()
						.getAccountDetail(
								"Rent Income " + property.getPropertyName(),
								contract.getImplementation());
				list.setAccountCode(account.getCode());
				list.setAccountDescription(account.getAccount());
			}
			if (null != list.getActionFlag()
					&& list.getActionFlag()
					&& Constants.RealEstate.ContractFeeType
							.get((byte) list.getFeeId()).toString()
							.equalsIgnoreCase("Rent")) {
				Combination combination = null;
				if (contract.getImplementation().getPdcReceived() != null)
					combination = this
							.getTransactionBL()
							.getCombinationService()
							.getCombinationAccounts(
									contract.getImplementation()
											.getPdcReceived());
				list.setAccountCode(combination.getAccountByNaturalAccountId()
						.getCode());
				list.setAccountDescription(combination
						.getAccountByNaturalAccountId().getAccount());
			}
			SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateVal = inFormat.format(new Date());
			String dates2 = DateFormat.convertDateToString(dateVal);
			list.setCreatedDate(dates2);
		}
		// Set contract payment in session
		session.setAttribute(
				"CONTRACT_PAYMENTS_" + sessionObj.get("jSessionId"),
				contractPayments);
		return agreementTO;
	}

	public ReContractAgreementTO getContractDetail(int contractId)
			throws Exception {
		agreementTO = new ReContractAgreementTO();
		contract = contractService.getContractDetails(Long.valueOf(contractId
				+ ""));
		tenentOffer = contractService.getTenantContractDetails(
				contract.getOffer().getOfferId()).get(0);
		agreementTO = ReContractAgreementTOConverter.convertToTO(contract);
		if (null != tenentOffer) {
			if (tenentOffer.getPerson() != null
					&& !tenentOffer.getPerson().equals("")) {
				agreementTO.setTenantName(tenentOffer.getPerson()
						.getFirstName()
						+ " "
						+ tenentOffer.getPerson().getLastName());
				agreementTO.setDescription(tenentOffer.getPerson()
						.getPersonId() + "@P");
			} else {
				agreementTO.setTenantName(tenentOffer.getCompany()
						.getCompanyName());
				agreementTO.setDescription(tenentOffer.getCompany()
						.getCompanyId() + "@C");
			}
		}
		contractPayments = contractService.getContractPaymentList(contract
				.getContractId());
		agreementTO.setContractAgreementList(ReContractAgreementTOConverter
				.convertToPaymentTOList(contractPayments));
		return agreementTO;
	}

	public ReContractAgreementTO getContractDetailPaymentPaid(int contractId)
			throws Exception {
		agreementTO = new ReContractAgreementTO();
		contract = contractService.getContractDetails(Long.parseLong(contractId
				+ ""));
		tenentOffer = contractService.getTenantContractDetails(
				contract.getOffer().getOfferId()).get(0);
		agreementTO = ReContractAgreementTOConverter.convertToTO(contract);
		if (null != tenentOffer) {
			if (tenentOffer.getPerson() != null
					&& !tenentOffer.getPerson().equals("")) {
				agreementTO.setTenantName(tenentOffer.getPerson()
						.getFirstName()
						+ " "
						+ tenentOffer.getPerson().getLastName());
				agreementTO.setDescription(tenentOffer.getPerson()
						.getPersonId() + "@P");
			} else {
				agreementTO.setTenantName(tenentOffer.getCompany()
						.getCompanyName());
				agreementTO.setDescription(tenentOffer.getCompany()
						.getCompanyId() + "@C");
			}
		}
		contractPayments = contractService
				.getContractPaymentListPaidOnly(contract.getContractId());
		if (contractPayments != null && contractPayments.size() > 0) {

		}

		agreementTO.setContractAgreementList(ReContractAgreementTOConverter
				.convertToPaymentTOList(contractPayments));
		return agreementTO;
	}

	public List<ReContractAgreementTO> getContractUnitInformation(
			Implementation implementation) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			ReContractAgreementTO contractTO = null;
			List<Unit> unitList = contractService
					.getAllContractUnits(implementation);
			if (unitList != null && unitList.size() > 0) {
				for (Unit list : unitList) {
					contractTO = new ReContractAgreementTO();
					contractTO.setUnitName(list.getUnitName());
					contractTO.setUnitId(list.getUnitId());
					agreementTOList.add(contractTO);
				}
			} else {
				agreementTOList = null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	public List<ReContractAgreementTO> getContractListInformation(
			Implementation implementation) {
		try {
			contracts = new ArrayList<Contract>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();

			contracts = this.getContractService().getAllContractDetails(
					implementation);
			if (contracts != null && contracts.size() > 0) {

				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);

				for (int i = 0; i < contracts.size(); i++) {
					offer = new Offer();
					tenentOffer = new TenantOffer();

					// List<BankReceiptsDetail> bankReceiptsDetails = new
					// ArrayList<BankReceiptsDetail>();
					for (ContractPayment contractPayment : contracts.get(i)
							.getContractPayments()) {
						/*
						 * if (contractPayment.getBankReceiptsDetails() != null
						 * && contractPayment.getBankReceiptsDetails() .size() >
						 * 0) bankReceiptsDetails .addAll(new
						 * ArrayList<BankReceiptsDetail>( contractPayment
						 * .getBankReceiptsDetails()));
						 */

						// In Contract Payment Table Null is represending the
						// Not yet processed state, Zero(0) represents Receipt
						// has been done and
						// one(1) represents Deposited.

						if (contractPayment.getIsDeposited() != null
								&& !contractPayment.getIsDeposited()) {
							agreementTOList.get(i).setIsDeposited(true);
							break;
						}
					}

					/*
					 * agreementTOList.get(i).setBankReceiptsDetails(
					 * bankReceiptsDetails);
					 */
					// Find offer
					offer = contracts.get(i).getOffer();
					if (offer.getStartDate() != null)
						agreementTOList.get(i).setFromDate(
								DateFormat.convertDateToString(offer
										.getStartDate().toString()));
					if (offer.getEndDate() != null)
						agreementTOList.get(i).setToDate(
								DateFormat.convertDateToString(offer
										.getEndDate().toString()));

					// Find Tenent offer
					tenentOffers = new ArrayList<TenantOffer>(
							offer.getTenantOffers());
					Company company = null;
					if (tenentOffers != null && tenentOffers.size() > 0
							&& tenentOffers.get(0).getPerson() != null) {
						// Find Person
						Person person = new Person();
						person = tenentOffers.get(0).getPerson();
						agreementTOList.get(i).setTenantName(
								person.getFirstName() + " "
										+ person.getLastName());
					} else {
						company = new Company();
						company = tenentOffers.get(0).getCompany();
						if (company != null) {
							agreementTOList.get(i).setTenantName(
									company.getCompanyName());
						}
					}

					// Process For legal information
					agreementTOList.get(i).setLegallist(
							legalBL.getLegals(contracts.get(i)));

					// Condition for cancellation
					Cancellation cancellation = null;
					List<Cancellation> cancellations = new ArrayList<Cancellation>(
							contracts.get(i).getCancellations());
					if (cancellations != null && cancellations.size() > 0) {
						cancellation = new Cancellation();
						cancellation = cancellations.get(0);
					}
					if (cancellation != null) {
						Release relase = null;
						List<Release> releases = new ArrayList<Release>(
								contracts.get(i).getReleases());
						if (releases != null && releases.size() > 0) {
							relase = releases.get(0);
							relase = new Release();
						}
						if (relase != null
								&& relase.getRefund() != null
								&& relase.getRefund() > 0
								&& (relase.getIsApprove() == Constants.RealEstate.Status.Publish
										.getCode() || relase.getIsApprove() == Constants.RealEstate.Status.Published
										.getCode())
								&& (cancellation.getIsApprove() == Constants.RealEstate.Status.Publish
										.getCode() || cancellation
										.getIsApprove() == Constants.RealEstate.Status.Published
										.getCode())) {

							agreementTOList.get(i).setCancelled(true);
						} else {
							agreementTOList.get(i).setCancellationInitiated(
									true);
						}
					}
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	public List<ReContractAgreementTO> contractFilterByUnits(Long unitId,
			char activeFlag) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			contracts = contractService.getContractFilterByUnit(unitId,
					new Date(), activeFlag);
			agreementTOList = ReContractAgreementTOConverter
					.convertToTOList(contracts);

			for (int i = 0; i < contracts.size(); i++) {
				offer = new Offer();
				tenentOffer = new TenantOffer();
				persons = new ArrayList<Person>();

				offer = this.contractService.getOfferDetails(contracts.get(i)
						.getOffer().getOfferId());
				if (offer.getStartDate() != null)
					agreementTOList.get(i).setFromDate(
							DateFormat.convertDateToString(offer.getStartDate()
									.toString()));
				if (offer.getEndDate() != null)
					agreementTOList.get(i).setToDate(
							DateFormat.convertDateToString(offer.getEndDate()
									.toString()));

				tenentOffers = this.contractService.getTenantOfferDetails(offer
						.getOfferId());
				Company company = null;
				if (tenentOffers != null && tenentOffers.size() > 0
						&& tenentOffers.get(0).getPerson() != null) {

					persons = this.contractService
							.getPersonDetails(tenentOffers.get(0).getPerson());
					agreementTOList.get(i).setTenantName(
							persons.get(0).getFirstName() + " "
									+ persons.get(0).getLastName());
				} else {
					company = new Company();
					company = offerService.getCompanyDetails(tenentOffers
							.get(0).getCompany().getCompanyId());
					if (company != null) {
						agreementTOList.get(i).setTenantName(
								company.getCompanyName());
					}
				}
				// Process For legal information
				agreementTOList.get(i).setLegallist(
						legalBL.getLegals(contracts.get(i)));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	// Tenant Contract list for report
	public List<ReContractAgreementTO> contractFilterByUnitsReport(Long unitId,
			char activeFlag) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			contracts = contractService.getContractFilterByUnit(unitId,
					new Date(), activeFlag);
			tenentOffer = new TenantOffer();
			if (contracts != null && contracts.size() > 0) {
				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);
				for (int i = 0; i < contracts.size(); i++) {

					// Offer Info section
					offerInfoToContractTOConversion(contracts.get(i),
							agreementTOList.get(i));

					// Tenant Info section
					tenentOffers = this.contractService
							.getTenantOfferDetails(offer.getOfferId());
					if (tenentOffers != null) {
						agreementTOList.get(i).setTenantInfo(
								tenantOfferToContractTOConversion(
										tenentOffers.get(0),
										agreementTOList.get(i)));
					}
					// Property Info section
					propertyInfoToContractTOConversion(agreementTOList.get(i));

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	public List<ReContractAgreementTO> contractFilterByTenantId(Long tenantId) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			contracts = contractService.getContractFilterByTenant(tenantId);
			agreementTOList = ReContractAgreementTOConverter
					.convertToTOList(contracts);

			for (int i = 0; i < contracts.size(); i++) {
				offer = new Offer();
				tenentOffer = new TenantOffer();
				persons = new ArrayList<Person>();

				offer = this.contractService.getOfferDetails(contracts.get(i)
						.getOffer().getOfferId());
				if (offer.getStartDate() != null)
					agreementTOList.get(i).setFromDate(
							DateFormat.convertDateToString(offer.getStartDate()
									.toString()));
				if (offer.getEndDate() != null)
					agreementTOList.get(i).setToDate(
							DateFormat.convertDateToString(offer.getEndDate()
									.toString()));

				tenentOffers = this.contractService.getTenantOfferDetails(offer
						.getOfferId());
				Company company = null;
				if (tenentOffers != null && tenentOffers.size() > 0
						&& tenentOffers.get(0).getPerson() != null) {

					persons = this.contractService
							.getPersonDetails(tenentOffers.get(0).getPerson());
					agreementTOList.get(i).setTenantName(
							persons.get(0).getFirstName() + " "
									+ persons.get(0).getLastName());
				} else {
					company = new Company();
					company = offerService.getCompanyDetails(tenentOffers
							.get(0).getCompany().getCompanyId());
					if (company != null) {
						agreementTOList.get(i).setTenantName(
								company.getCompanyName());
					}
				}
				// Process For legal information
				agreementTOList.get(i).setLegallist(
						legalBL.getLegals(contracts.get(i)));
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	// Tenant Contract list for report
	public List<ReContractAgreementTO> contractReportFilterByTenantId(
			Long tenantId, boolean showArabic) {
		try {
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			contracts = contractService.getContractFilterByTenant(tenantId);
			tenentOffer = new TenantOffer();
			if (contracts != null && contracts.size() > 0) {
				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);
				for (int i = 0; i < contracts.size(); i++) {

					// Offer Info section
					offerInfoToContractTOConversion(contracts.get(i),
							agreementTOList.get(i));

					// Tenant Info section
					tenentOffers = this.contractService
							.getTenantOfferDetails(offer.getOfferId());
					if (tenentOffers != null) {
						agreementTOList.get(i).setTenantInfo(
								tenantOfferToContractTOConversion(
										tenentOffers.get(0),
										agreementTOList.get(i)));
					}
					// Property Info section
					propertyInfoToContractTOConversion(agreementTOList.get(i));

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return agreementTOList;
	}

	// Contract filter information getContractFilterInformation
	public List<ReContractAgreementTO> getContractFilterInformation(
			Implementation implementation, String fromDate, String toDate,
			Long tenantId, String tenantType, Long unitId) {

		try {
			contracts = new ArrayList<Contract>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();

			if (fromDate != null && !fromDate.trim().equals("")
					&& unitId != null && unitId != 0) {
				contracts = this.getContractService().getFilterContractDetails(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate), unitId);
			} else if (fromDate != null && !fromDate.trim().equals("")
					&& (unitId == null || unitId == 0)) {
				contracts = this.getContractService().getFilterContractDetails(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			} else if ((fromDate == null || fromDate.trim().equals(""))
					&& unitId != null && unitId != 0) {
				contracts = this.getContractService().getFilterContractDetails(
						implementation, unitId);
			} else {
				contracts = this.getContractService().getFilterContractDetails(
						implementation);
			}

			if (contracts != null && contracts.size() > 0) {

				for (Contract contract : contracts) {
					ReContractAgreementTO contTo = ReContractAgreementTOConverter
							.convertToTO(contract);
					contTo.setFromDate(DateFormat.convertDateToString(contract
							.getOffer().getStartDate().toString()));
					contTo.setToDate(DateFormat.convertDateToString(contract
							.getOffer().getEndDate().toString()));
					/*
					 * boolean flag=false; for(TenantOffer
					 * tenantOffer:contract.getOffer().getTenantOffers()){
					 * if(tenantOffer.getCompany()!=null &&
					 * tenantType.equalsIgnoreCase("C") ){
					 * System.out.println("tenantOffer company-----"
					 * +tenantOffer.getCompany().getCompanyId());
					 * if(tenantOffer.getCompany().getCompanyId()==tenantId){
					 * flag=true; }else{ flag=false; } } else
					 * if(tenantOffer.getPerson()!=null &&
					 * tenantType.equalsIgnoreCase("P")){
					 * System.out.println("tenantOffer person-----"
					 * +tenantOffer.getPerson().getPersonId());
					 * if(tenantOffer.getPerson().getPersonId()==tenantId){
					 * flag=true; }else{ flag=false; } }else
					 * if(!tenantType.equalsIgnoreCase("C") &&
					 * !tenantType.equalsIgnoreCase("P")){ flag=true; }
					 * tenantOfferToContractTOConversion(tenantOffer,contTo);
					 * 
					 * }
					 * 
					 * if(flag){
					 * offerInfoToContractTOConversion(contract,contTo);
					 * propertyInfoToContractTOConversion(contTo);
					 * agreementTOList.add(contTo); }
					 */
					List<TenantOffer> tenentOffers = new ArrayList<TenantOffer>();
					if (tenantType.equalsIgnoreCase("P")) {
						// Find Person
						tenentOffers = this.offerService
								.getTenantOfferDetailsOnlyPerson(contract
										.getOffer().getOfferId(), tenantId);
						for (TenantOffer tenantOffer : tenentOffers) {
							tenantOfferToContractTOConversionPassingPersonOrCompany(
									tenantOffer, contTo);
							offerInfoToContractTOConversion(contract, contTo);
							propertyInfoToContractTOConversion(contTo);
							agreementTOList.add(contTo);

						}

					} else if (tenantType.equalsIgnoreCase("C")) {
						tenentOffers = this.offerService
								.getTenantOfferDetailsOnlyCompany(contract
										.getOffer().getOfferId(), tenantId);
						for (TenantOffer tenantOffer : tenentOffers) {
							tenantOfferToContractTOConversionPassingPersonOrCompany(
									tenantOffer, contTo);
							offerInfoToContractTOConversion(contract, contTo);
							propertyInfoToContractTOConversion(contTo);
							agreementTOList.add(contTo);
						}
					} else {
						tenentOffers = this.contractService
								.getTenantOfferDetails(contract.getOffer()
										.getOfferId());
						for (TenantOffer tenantOffer : tenentOffers) {
							tenantOfferToContractTOConversion(tenantOffer,
									contTo);
							offerInfoToContractTOConversion(contract, contTo);
							propertyInfoToContractTOConversion(contTo);
							agreementTOList.add(contTo);
						}
					}

				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	// Contract report filter information getContractFilterInformation
	public List<ReContractAgreementTO> getContractReportFilterInformation(
			Implementation implementation, String fromDate, String toDate,
			String percentage) {

		try {
			contracts = new ArrayList<Contract>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();

			if (fromDate != null && !fromDate.trim().equals("")
					&& percentage != null && !percentage.trim().equals("")) {

				// Find the month end date
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(DateFormat.convertStringToDate(fromDate));
				calendar.add(Calendar.MONTH, 1);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.add(Calendar.DATE, -1);
				Date firstDayOfMonth = calendar.getTime();

				contracts = this.getContractService()
						.getFilterContractDetailsMonth(implementation,
								DateFormat.convertStringToDate(fromDate),
								firstDayOfMonth);
			} else {
				contracts = this.getContractService().getFilterContractDetails(
						implementation,
						DateFormat.convertStringToDate(fromDate),
						DateFormat.convertStringToDate(toDate));
			}
			tenentOffer = new TenantOffer();
			if (contracts != null && contracts.size() > 0) {
				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);
				for (int i = 0; i < contracts.size(); i++) {
					offer = new Offer();
					persons = new ArrayList<Person>();

					// Offer Info section
					offer = this.contractService.getOfferDetails(contracts
							.get(i).getOffer().getOfferId());
					if (offer.getStartDate() != null)
						agreementTOList.get(i).setFromDate(
								DateFormat.convertDateToString(offer
										.getStartDate().toString()));
					if (offer.getEndDate() != null)
						agreementTOList.get(i).setToDate(
								DateFormat.convertDateToString(offer
										.getEndDate().toString()));
					String cprd = "From : "
							+ agreementTOList.get(i).getFromDate() + " UpTo : "
							+ agreementTOList.get(i).getToDate();
					agreementTOList.get(i).setContractPeriod(cprd);

					// Offer Detail
					double offerAmountTotal = 0;
					List<OfferDetail> offerDetails = offerService
							.getOfferDetailById(offer.getOfferId());
					if (offerDetails != null && offerDetails.size() > 0) {
						for (OfferDetail offerDetail : offerDetails) {
							offerAmountTotal += offerDetail.getOfferAmount();
						}
						agreementTOList.get(i).setOfferAmount(offerAmountTotal);
						if (percentage != null && !percentage.trim().equals("")) {
							String str = ((agreementTOList.get(i)
									.getOfferAmount() * Integer
									.parseInt(percentage)) / 100)
									+ "";
							agreementTOList.get(i).setOfferAmountInPercentage(
									str);
							agreementTOList.get(i).setPercentageStr(
									percentage + "% Of Rent");
						}
						if (offerDetails.get(0).getProperty() != null
								&& offerDetails.get(0).getProperty()
										.getPropertyId() != null)
							agreementTOList.get(i).setPropertyId(
									offerDetails.get(0).getProperty()
											.getPropertyId());
						if (offerDetails.get(0).getComponent() != null
								&& offerDetails.get(0).getComponent()
										.getComponentId() != null)
							agreementTOList.get(i).setComponentId(
									offerDetails.get(0).getComponent()
											.getComponentId());
						if (offerDetails.get(0).getUnit() != null
								&& offerDetails.get(0).getUnit().getUnitId() != null)
							agreementTOList.get(i).setUnitId(
									offerDetails.get(0).getUnit().getUnitId());
					}

					// Tenant Info section
					tenentOffers = this.contractService
							.getTenantOfferDetails(offer.getOfferId());
					if (tenentOffers != null) {
						/*
						 * ReContractAgreementTO
						 * tempTenantInfo=tenantOfferToContractTOConversion
						 * (tenentOffers.get(0),agreementTOList.get(i));
						 * agreementTOList.get(i).setTenantInfo(tempTenantInfo);
						 */
						tenantOfferToContractTOConversion(tenentOffers.get(0),
								agreementTOList.get(i));
					}
					// Property Info section
					propertyInfoToContractTOConversion(agreementTOList.get(i));

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	public List<ReContractAgreementTO> getContractReleaseRenewInformation(
			Implementation implementation) {
		try {
			contracts = new ArrayList<Contract>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			
			HttpSession session = ServletActionContext.getRequest().getSession();
			int releaseNoticePeriod = 0;
			if (session.getAttribute("release_notice_period") != null)
				releaseNoticePeriod = Integer.parseInt(session.getAttribute("release_notice_period").toString()) ;
			
			Calendar cal = Calendar.getInstance();
			java.util.Date currentDate = cal.getTime();
			cal.add(Calendar.MONTH, releaseNoticePeriod);
			java.util.Date date = cal.getTime();

			LOGGER.info("current date--->" + currentDate);
			LOGGER.info("one month before date--->" + date);
			contracts = this.getContractService()
					.getAllReleaseRenewContractDetails(currentDate, date,
							implementation);

			if (contracts != null && contracts.size() > 0) {

				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);

				for (int i = 0; i < contracts.size(); i++) {

					if (contracts.get(i).getCancellations() != null
							&& contracts.get(i).getCancellations().size() > 0)
						continue;

					if (contracts.get(i).getReleases() != null
							&& contracts.get(i).getReleases().size() > 0)
						continue;

					offer = new Offer();
					tenentOffer = new TenantOffer();

					// Find offer
					offer = contracts.get(i).getOffer();
					if (offer.getStartDate() != null)
						agreementTOList.get(i).setFromDate(
								DateFormat.convertDateToString(offer
										.getStartDate().toString()));
					if (offer.getEndDate() != null)
						agreementTOList.get(i).setToDate(
								DateFormat.convertDateToString(offer
										.getEndDate().toString()));

					// Find Tenent offer
					tenentOffers = new ArrayList<TenantOffer>(
							offer.getTenantOffers());
					Company company = null;
					if (tenentOffers != null && tenentOffers.size() > 0
							&& tenentOffers.get(0).getPerson() != null) {
						// Find Person
						Person person = new Person();
						person = tenentOffers.get(0).getPerson();
						agreementTOList.get(i).setTenantName(
								person.getFirstName() + " "
										+ person.getLastName());
					} else {
						company = new Company();
						company = tenentOffers.get(0).getCompany();
						if (company != null) {
							agreementTOList.get(i).setTenantName(
									company.getCompanyName());
						}
					}

					// Process For legal information
					agreementTOList.get(i).setLegallist(
							legalBL.getLegals(contracts.get(i)));
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	public List<ReContractAgreementTO> getOfferList(
			Implementation implementation) {

		try {

			offers = new ArrayList<Offer>();
			offers = this.getContractService().getUnOfferList(implementation);
			if (offers != null && offers.size() > 0) {

				agreementTOList = ReContractAgreementTOConverter
						.convertToOfferTOList(offers);

				for (int i = 0; i < offers.size(); i++) {
					contracts = new ArrayList<Contract>();
					offer = new Offer();
					tenentOffers = new ArrayList<TenantOffer>();
					persons = new ArrayList<Person>();
					tenentOffers = this.contractService
							.getTenantOfferDetails(offers.get(i).getOfferId());
					Company company = null;
					if (tenentOffers != null && tenentOffers.size() > 0
							&& tenentOffers.get(0).getPerson() != null) {
						persons = this.contractService
								.getPersonDetails(tenentOffers.get(0)
										.getPerson());
						agreementTOList.get(i).setTenantName(
								persons.get(0).getFirstName() + " "
										+ persons.get(0).getLastName());
						agreementTOList.get(i).setTenantId(
								Integer.parseInt(persons.get(0).getPersonId()
										.toString()));
						agreementTOList.get(i).setPresentAddress(
								persons.get(0).getResidentialAddress());
						agreementTOList.get(i).setPermanantAddress(
								persons.get(0).getRegionBirth());
						agreementTOList.get(i).setTenantType("Person");
						agreementTOList.get(i).setTenantGroup(
								tenentOffers.get(0).getTenantGroup());
					} else {
						company = new Company();
						company = offerService.getCompanyDetails(tenentOffers
								.get(0).getCompany().getCompanyId());
						if (company != null) {
							agreementTOList.get(i).setTenantName(
									company.getCompanyName());
							agreementTOList.get(i).setTenantId(
									Integer.parseInt(company.getCompanyId()
											.toString()));
							agreementTOList.get(i).setPresentAddress(
									company.getCompanyAddress());
							agreementTOList.get(i).setTenantType("Company");
							agreementTOList.get(i).setTenantGroup(
									tenentOffers.get(0).getTenantGroup());
						}
					}
					offerDetails = this.getContractService()
							.getAllOfferDetailList(offers.get(i).getOfferId());
					StringBuilder unitName = new StringBuilder();
					for (int j = 0; j < offerDetails.size(); j++) {
						if (null != offerDetails.get(j).getProperty()
								&& !offerDetails.get(j).getProperty()
										.equals("")) {
							property = this.getPropertyService()
									.getPropertyById(
											offerDetails.get(j).getProperty()
													.getPropertyId());
							agreementTOList.get(i).setPropertyName(
									property.getPropertyName());
						} else if (null != offerDetails.get(j).getUnit()
								&& !offerDetails.get(j).getUnit().equals("")) {
							List<Unit> unitList = unitService
									.getPropertyByUnitId(offerDetails.get(j)
											.getUnit());
							Long componentId = unitList.get(0).getComponent()
									.getComponentId();
							component = componentService
									.getComponentByIdWithProperty(componentId);
							agreementTOList.get(i).setPropertyName(
									component.getProperty().getPropertyName());
							agreementTOList.get(i).setComponentName(
									component.getComponentName());
							for (Unit list : unitList) {
								unitName.append(list
										.getUnitName()
										.concat(",")
										.concat(component.getProperty()
												.getPropertyName()).concat("."));
							}
						}
					}
					if (null == offerDetails.get(0).getProperty()
							|| offerDetails.get(0).getProperty().equals(""))
						agreementTOList.get(i).setPropertyName(
								unitName.toString());
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	public List<ReContractAgreementTO> getOfferDeatilsList(int offerId) {

		try {

			offerDetails = new ArrayList<OfferDetail>();
			property = new Property();
			component = new Component();
			unit = new Unit();
			Boolean ownersFetched = false;
			String[] ownersEngArabic = new String[2];

			offerDetails = this.getContractService().getAllOfferDetailList(
					offerId);
			if (offerDetails != null && offerDetails.size() > 0) {

				agreementTOList = ReContractAgreementTOConverter
						.convertToOfferDetailTOList(offerDetails);

				for (int i = 0; i < offerDetails.size(); i++) {

					if (agreementTOList.get(i).getPropertyId() != 0) {
						property = this.getContractService().getPropertyInfo(
								agreementTOList.get(i).getPropertyId());
						agreementTOList.get(i).setFlatNumber(
								property.getPropertyName());
						agreementTOList.get(i).setRentAmount(
								property.getRentYr());
						agreementTOList.get(i).setBuildingName(
								property.getPropertyName());
						agreementTOList.get(i).setBuildingNameArabic(
								AIOSCommons.bytesToUTFString(property
										.getPropertyNameArabic()));
						agreementTOList.get(i).setPropertyNameArabic(
								AIOSCommons.bytesToUTFString(property
										.getPropertyNameArabic()));
						agreementTOList.get(i).setBuildingArea(
								property.getAddressOne());
						agreementTOList.get(i).setComponentName(null);
						try {
							agreementTOList.get(i).setBuildingCity(
									systemService.getCityById(
											property.getCity()).getCityName());
						} catch (Exception e) {
							agreementTOList.get(i).setBuildingCity("");
						}
						agreementTOList.get(i).setBuildingPlot(
								property.getPlotNo());
						agreementTOList.get(i).setBuildingStreet(
								property.getAddressTwo());
						agreementTOList.get(i).setServiceMeter(
								property.getServiceMeter() + "");
						if (!ownersFetched) {
							ownersEngArabic = getOwners("property",
									agreementTOList.get(i).getPropertyId());
							agreementTOList.get(i).setOfferedPropertyOwners(
									ownersEngArabic[0]);
							agreementTOList.get(i)
									.setOfferedPropertyOwners_arabic(
											ownersEngArabic[1]);
							ownersFetched = true;
						}

					} else if (agreementTOList.get(i).getComponentId() != 0) {
						component = this.getContractService().getComponentInfo(
								agreementTOList.get(i).getComponentId());
						agreementTOList.get(i).setFlatNumber(
								component.getComponentName());
						agreementTOList.get(i).setComponentNameArabic(
								AIOSCommons.bytesToUTFString(component
										.getComponentNameArabic()));
						agreementTOList.get(i).setRentAmount(
								component.getRent());
						agreementTOList.get(i).setComponentName(null);

						property = componentService
								.getComponentByIdWithProperty(
										agreementTOList.get(i).getComponentId())
								.getProperty();
						if (property != null) {

							agreementTOList.get(i).setBuildingName(
									property.getPropertyName());
							agreementTOList.get(i).setBuildingNameArabic(
									AIOSCommons.bytesToUTFString(property
											.getPropertyNameArabic()));
							agreementTOList.get(i).setBuildingArea(
									property.getAddressOne());
							agreementTOList.get(i).setServiceMeter(
									property.getServiceMeter() + "");
							try {
								agreementTOList.get(i).setBuildingCity(
										systemService.getCityById(
												property.getCity())
												.getCityName());
							} catch (Exception e) {
								agreementTOList.get(i).setBuildingCity("");
							}
							agreementTOList.get(i).setBuildingPlot(
									property.getPlotNo());
							agreementTOList.get(i).setBuildingStreet(
									property.getAddressTwo());
							if (!ownersFetched) {
								ownersEngArabic = getOwners("property",
										property.getPropertyId());
								agreementTOList.get(i)
										.setOfferedPropertyOwners(
												ownersEngArabic[0]);
								agreementTOList.get(i)
										.setOfferedPropertyOwners_arabic(
												ownersEngArabic[1]);
								ownersFetched = true;
							}
						}

					} else if (agreementTOList.get(i).getUnitId() != 0) {
						unit = this.getContractService().getUnitInfo(
								agreementTOList.get(i).getUnitId());
						agreementTOList.get(i)
								.setFlatNumber(unit.getUnitName());
						agreementTOList.get(i).setUnitNameArabic(
								AIOSCommons.bytesToUTFString(unit
										.getUnitNameArabic()));
						if (null != unit.getRent())
							agreementTOList.get(i)
									.setRentAmount(unit.getRent());
						else
							agreementTOList.get(i).setRentAmount(0);
						agreementTOList.get(i).setServiceMeter(
								unit.getMeter() + "");

						try {
							unit = unitService.getPropertyByUnitId(unit).get(0);
							agreementTOList.get(i).setComponentName(
									unit.getComponent().getComponentName());
							property = componentService
									.getComponentByIdWithProperty(
											unit.getComponent()
													.getComponentId())
									.getProperty();
							if (property != null) {

								agreementTOList.get(i).setBuildingName(
										property.getPropertyName());
								agreementTOList.get(i).setBuildingNameArabic(
										AIOSCommons.bytesToUTFString(property
												.getPropertyNameArabic()));
								agreementTOList.get(i).setBuildingArea(
										property.getAddressOne());
								agreementTOList.get(i).setServiceMeter(
										property.getServiceMeter() + "");
								try {
									agreementTOList.get(i).setBuildingCity(
											systemService.getCityById(
													property.getCity())
													.getCityName());
								} catch (Exception e) {
									agreementTOList.get(i).setBuildingCity("");
								}
								agreementTOList.get(i).setBuildingPlot(
										property.getPlotNo());
								agreementTOList.get(i).setBuildingStreet(
										property.getAddressTwo());
								if (!ownersFetched) {
									ownersEngArabic = getOwners("property",
											property.getPropertyId());
									agreementTOList.get(i)
											.setOfferedPropertyOwners(
													ownersEngArabic[0]);
									agreementTOList.get(i)
											.setOfferedPropertyOwners_arabic(
													ownersEngArabic[1]);
									ownersFetched = true;
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;
	}

	private String[] getOwners(String type, long id) throws Exception {

		String owners = "";
		String owners_arabic = "";
		String[] ownerEngArabic = new String[2];
		Property property = new Property();
		Person person;
		Company company;
		List<PropertyOwnership> ownerships = new ArrayList<PropertyOwnership>();
		property.setPropertyId(id);
		ownerships = propertyService.getPropertyOwners(property);

		for (PropertyOwnership ownership : ownerships) {

			if (ownership.getDetails().equals("PERSON")) {

				person = new Person();
				person.setPersonId(ownership.getPersonalId());
				owners = owners.concat(personService.getSelectedPersons(person)
						.getFirstName()
						+ " "
						+ personService.getSelectedPersons(person)
								.getMiddleName()
						+ " "
						+ personService.getSelectedPersons(person)
								.getLastName() + "" + ", ");
				owners_arabic = owners_arabic.concat(AIOSCommons
						.bytesToUTFString(personService.getSelectedPersons(
								person).getFirstNameArabic())
						+ " "
						+ AIOSCommons.bytesToUTFString(personService
								.getSelectedPersons(person)
								.getMiddleNameArabic())
						+ " "
						+ AIOSCommons
								.bytesToUTFString(personService
										.getSelectedPersons(person)
										.getLastNameArabic()) + "" + ", ");
			} else {

				company = companyService.getCompanyById(ownership
						.getPersonalId());
				owners = owners.concat(company.getCompanyName() + ", ");
				owners_arabic = owners_arabic.concat(AIOSCommons
						.bytesToUTFString(company.getCompanyNameArabic())
						+ ", ");
			}
		}
		ownerEngArabic[0] = AIOSCommons.removeTrailingSymbols(owners);
		ownerEngArabic[1] = AIOSCommons.removeTrailingSymbols(owners_arabic);
		return ownerEngArabic;
	}

	public void saveContractUploads(Contract contract, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(contract,
				"contractDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, contract.getContractId(), "Contract", "contractDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	@SuppressWarnings("unchecked")
	public void saveContractAgreement(Contract contract,
			Implementation implementation, boolean editFlag) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		if (contract != null && contract.getContractId() != null
				&& contract.getContractId() > 0) {

			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Modify
							.getCode());
		} else {
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
		}
		// contracts = new ArrayList<Contract>();
		contractPayment = new ContractPayment();
		contractPayments = new ArrayList<ContractPayment>();
		offer = new Offer();
		List<OfferDetail> offerDetails = new ArrayList<OfferDetail>();

		// implementationId=(Long) session.get("THIS");
		// contracts=this.getContractService().getAllContractDetails(implementation);
		/*
		 * if (contract.getContractNo() == null ||
		 * contract.getContractNo().trim().equals("0") ||
		 * contract.getContractNo().trim().equals("")) { if (contracts != null
		 * && contracts.size() > 0) { for (int i = 0; i < contracts.size(); i++)
		 * { intelist.add(Integer.parseInt(contracts.get(i).getContractNo())); }
		 * LOGGER.info("Person Number Max()--" + Collections.max(intelist));
		 * 
		 * int temp = Collections.max(intelist); temp += 1;
		 * contract.setContractNo(temp + ""); } else {
		 * contract.setContractNo("1000"); } }
		 */

		// contract.setIsRenewal(null);
		contractPayments = (ArrayList<ContractPayment>) session
				.getAttribute("CONTRACT_PAYMENTS_"
						+ sessionObj.get("jSessionId"));
		List<ContractPayment> tempContractPay = new ArrayList<ContractPayment>(
				contractPayments);
		if (null != contract.getContractId()
				&& !("").equals(contract.getContractId())
				&& null != contract.getPrintTaken() && contract.getPrintTaken()) {
			this.getContractService().addContract(contract, contractPayments);
			Contract tempContract = new Contract();
			tempContract = contract;
			boolean editPayment = false;
			List<ContractPayment> tempContractPayments = (ArrayList<ContractPayment>) session
					.getAttribute("CONTRACT_DBPAYMENTS_"
							+ sessionObj.get("jSessionId"));
			if (tempContractPayments.size() == tempContractPay.size()) {
				for (ContractPayment temp : tempContractPayments) {
					if (!editPayment) {
						for (ContractPayment list : tempContractPay) {
							if (temp.getContractPaymentId().equals(
									list.getContractPaymentId())
									&& !editPayment) {
								if (!temp.getAmount().equals(list.getAmount())) {
									editPayment = true;
								}
							}
						}
					}
				}
			} else {
				editPayment = true;
			}
			if (editPayment) {
				updateTransactionTemp(tempContract, tempContractPayments,
						implementation);
			}
		} else {
			this.getContractService().addContract(contract, contractPayments);
		}
		try {
			saveContractUploads(contract, editFlag);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("Contract Uploads Exception");
		}

		if (contract.getRelativeId() == null) {
			contract.setRelativeId(contract.getContractId());
			this.getContractService().addContract(contract, null);
		}

		User user = (User) sessionObj.get("USER");
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				"Contract", contract.getContractId(), user, workflowDetailVo);

		// rejection case edit screen
		WorkflowDetailVO vo = null;
		if (sessionObj.get("WORKFLOW_DETAILVO") != null) {

			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Contract", contract.getContractId(), user,
					workflowDetailVo);

			vo = (WorkflowDetailVO) sessionObj.get("WORKFLOW_DETAILVO");
			sessionObj.remove("WORKFLOW_DETAILVO");

			workflowEnterpriseService.updateNotificationStatus(vo,
					vo.getRecordId(), (byte) 1, user);

		}

		offerDetails = offerBL.getOfferService().getOfferDetailById(
				contract.getOffer().getOfferId());

		String offerFor = null;
		if (offerDetails != null && offerDetails.size() > 0) {
			if (offerDetails.get(0).getProperty() != null
					&& offerDetails.get(0).getProperty().getPropertyId() != null
					&& offerDetails.get(0).getProperty().getPropertyId() != 0) {
				offerFor = "PROPERTY";
			} else if (offerDetails.get(0).getComponent() != null
					&& offerDetails.get(0).getComponent().getComponentId() != null
					&& offerDetails.get(0).getComponent().getComponentId() != 0) {
				offerFor = "COMPONENT";
			} else if (offerDetails.get(0).getUnit() != null
					&& offerDetails.get(0).getUnit().getUnitId() != null
					&& offerDetails.get(0).getUnit().getUnitId() != 0) {
				offerFor = "UNITS";
			}
		}
		if (offerFor != null) {
			offerBL.updatePropertyStatus(offerFor, offerDetails,
					Byte.parseByte("3"));
		}

		// *****************Workflow Detail Process Remove for re do the
		// approval process*****************************************
		if (contract.getContractId() != null && contract.getContractId() > 0) {
			List<Workflow> workflows = null;
			workflows = workflowDAO.findByNamedQuery(
					"getWorkflowToEditProcess", getImplementation(),
					"com.aiotech.aios.realestate.domain.entity.Contract",
					contract.getContractId());
			if (workflows != null && workflows.size() > 0) {
				List<WorkflowDetailProcess> workflowDetailProcessList = new ArrayList<WorkflowDetailProcess>();
				List<WorkflowDetail> workflowDetails = new ArrayList<WorkflowDetail>(
						workflows.get(0).getWorkflowDetails());
				for (WorkflowDetail workflowDetail2 : workflowDetails) {
					workflowDetailProcessList.addAll(workflowDetail2
							.getWorkflowDetailProcesses());
				}
				workflowDetailProcessDAO.deleteAll(workflowDetailProcessList);
			}
		}
		LOGGER.info("Contract save success");
	}

	private void updateTransactionTemp(Contract contract,
			List<ContractPayment> contractPaymentList,
			Implementation implementation) throws Exception {
		if (checkPayments(contract) > 0) {
			this.getTransactionBL().deleteTranscationTempByJournalNO(
					"CT-".concat(
							contract.getContractId().toString().concat("-"))
							.concat(contract.getContractNo()),
					getImplementationId());
			List<Category> categoryList = this.getTransactionBL()
					.getCategoryBL().getCategoryService()
					.getAllActiveCategories(contract.getImplementation());
			Category categoryFee = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Contract Fee")) {
						categoryFee.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryFee || categoryFee.equals("")) {
				categoryFee = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Fee");
			} else if (null == categoryFee.getCategoryId()) {
				categoryFee = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Fee");
			}
			List<TransactionTempDetail> detailTempList = new ArrayList<TransactionTempDetail>();
			String description = "";
			Combination combination = getCombinationDetails(contract
					.getImplementation().getRevenueAccount());
			TransactionTempDetail transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(contract.getContractFee(),
							implementation.getRevenueAccount(), true,
							"Contract Fee Debited(Reverted).", null);
			detailTempList.add(transactionDetail);// ADD TRANSACTION DETAIL TO
													// LIST
			// FOR CUSTOMER
			Account account = new Account();
			account = getCustomerDetail(contract);
			combination = getCombinationByAccount(account.getAccountId());
			transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(contract.getContractFee(),
							combination.getCombinationId(), false,
							"Customer Credited(Reverted).", null);
			detailTempList.add(transactionDetail);// ADD TRANSACTION DETAIL TO
													// LIST
			TransactionTemp transaction = this
					.getTransactionBL()
					.createTransactionTemp(
							this.getTransactionBL().getCurrency()
									.getCurrencyId(),
							"CONTRACT FEE/CUSTOMER (REVERTED)",
							new Date(),
							categoryFee,
							"CT-".concat(
									contract.getContractId().toString()
											.concat("-")).concat(
									contract.getContractNo()),
							Byte.valueOf(String
									.valueOf(Constants.RealEstate.Status.Approved
											.getCode())));
			
			// Issue in jv
			/*this.getTransactionBL().saveTransactionTemp(transaction,
					detailTempList, null, 0l);// SAVE TRANSACTION CONTRACT FEES
												// CREDIT/CUSTOMER DEBIT*/
			// FOR CONTRACT DEPOSIT & CUSTOMER TRANSACTION
			Category categoryDeposit = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Contract Deposit")) {
						categoryDeposit.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryDeposit || categoryDeposit.equals("")) {
				categoryDeposit = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Deposit");
			} else if (null == categoryDeposit.getCategoryId()) {
				categoryDeposit = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Deposit");
			}
			detailTempList = new ArrayList<TransactionTempDetail>();
			transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(contract.getDeposit(),
							implementation.getUnearnedRevenue(), true,
							"Contract Deposit Debited(Reverted).", null);
			detailTempList.add(transactionDetail);// ADD TRANSACTION DETAIL TO
													// LIST
			// FOR CUSTOMER
			account = getCustomerDetail(contract);
			combination = getCombinationByAccount(account.getAccountId());
			transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(contract.getDeposit(),
							combination.getCombinationId(), false,
							"Customer Credited(Reverted).", null);
			detailTempList.add(transactionDetail);// ADD TRANSACTION DETAIL TO
													// LIST
			transaction = this.getTransactionBL().createTransactionTemp(
					this.getTransactionBL().getCurrency().getCurrencyId(),
					"CONTRACT DEPOSIT/CUSTOMER(REVERTED)",
					new Date(),
					categoryDeposit,
					"CT-".concat(
							contract.getContractId().toString().concat("-"))
							.concat(contract.getContractNo()),
					Byte.valueOf(String
							.valueOf(Constants.RealEstate.Status.Approved
									.getCode())));
			// Issue in jv
			/*this.getTransactionBL().saveTransactionTemp(transaction,
					detailTempList, null, 0l);*/// SAVE TRANSACTION CONTRACT
												// DEPOSIT CREDIT/ CUSTOMER
												// DEBIT
			// FOR CONTRACT PAYMENT MODES TRANSACTION
			offerDetails = offerService.getOfferDetailById(contract.getOffer()
					.getOfferId());// GET OFFER DETAILS

			if (offerDetails.get(0).getProperty() != null) { // CHECK CONTRACT
																// IS FOR
																// PROPERTY/COMPONENT/UNIT
																// AND SET TO
																// PROPERTY ID
				propertyId = offerDetails.get(0).getProperty().getPropertyId();
			} else if (offerDetails.get(0).getComponent() != null) {
				component = compService.getComponentById(offerDetails.get(0)
						.getComponent().getComponentId());
				propertyId = component.getProperty().getPropertyId();
			} else if (offerDetails.get(0).getUnit() != null) {
				unit = unitService.getUnitDetails(offerDetails.get(0).getUnit()
						.getUnitId());
				component = compService.getComponentById(unit.getComponent()
						.getComponentId());
				propertyId = component.getProperty().getPropertyId();
			}

			property = propertyService.getPropertyById(propertyId); // GET
																	// PROPERTY
																	// DETAILS
			contractPayments = getPaymentDetails(contract); // LIST OF PAYMENT
															// DETAILS
			double totalContractRent = 0;
			for (ContractPayment list : contractPayments) {
				totalContractRent += list.getAmount();
			}
			totalContractRent -= (contract.getContractFee() + contract
					.getDeposit());

			// FOR Unearned Revenue
			Category categoryUnearned = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Unearned Revenue")) {
						categoryUnearned.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryUnearned || categoryUnearned.equals("")) {
				categoryUnearned = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Unearned Revenue");
			} else if (null == categoryUnearned.getCategoryId()) {
				categoryUnearned = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Unearned Revenue");
			}
			detailTempList = new ArrayList<TransactionTempDetail>();
			transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(totalContractRent,
							implementation.getUnearnedRevenue(), true,
							"Unearned Revenue Debited(Reverted).", null);
			detailTempList.add(transactionDetail);
			// FOR CUSTOMER
			account = getCustomerDetail(contract);
			combination = getCombinationByAccount(account.getAccountId());
			transactionDetail = this.getTransactionBL()
					.createTransactionTempDetail(totalContractRent,
							combination.getCombinationId(), false,
							"Customer Credited(Reverted).", null);
			detailTempList.add(transactionDetail);
			// CREATE Unearned Revenue TRANSACTION
			transaction = new TransactionTemp();
			description = "Unearned Revenue/CUSTOMER(REVERTED)";
			transaction = this.transactionBL.createTransactionTemp(
					this.getTransactionBL().getCurrency().getCurrencyId(),
					description,
					new Date(),
					categoryUnearned,
					"CT-".concat(
							contract.getContractId().toString().concat("-"))
							.concat(contract.getContractNo()),
					Byte.valueOf(String
							.valueOf(Constants.RealEstate.Status.Approved
									.getCode())));
			/*this.transactionBL.saveTransactionTemp(transaction, null,
					detailTempList, 0l); */// FOR CONTRACT RENT INCOME
		} else {
			LOGGER.info("Contract Payments is Zero, No transaction for Zero Payments");
		}
	}

	public void deleteContract(Contract contract, Implementation implemenation)
			throws Exception {
		contractPayments = new ArrayList<ContractPayment>();
		List<OfferDetail> offerDetails = new ArrayList<OfferDetail>();
		contract = this.getContractService().getContractDetails(
				contract.getContractId());
		contractPayments = this.getContractService().getContractPaymentList(
				contract.getContractId());
		if (null != contract.getPrintTaken() && contract.getPrintTaken()) {
			updateTransactionTemp(contract, contractPayments, implemenation);
			revertDeleteContractTransaction(contract.getContractId());
		}
		this.getContractService().deletContract(contract, contractPayments);
		offerDetails = offerBL.getOfferService().getOfferDetailById(
				contract.getOffer().getOfferId());

		String offerFor = null;
		if (offerDetails != null && offerDetails.size() > 0) {
			if (offerDetails.get(0).getProperty() != null
					&& offerDetails.get(0).getProperty().getPropertyId() != null
					&& offerDetails.get(0).getProperty().getPropertyId() != 0) {
				offerFor = "PROPERTY";
			} else if (offerDetails.get(0).getComponent() != null
					&& offerDetails.get(0).getComponent().getComponentId() != null
					&& offerDetails.get(0).getComponent().getComponentId() != 0) {
				offerFor = "COMPONENT";
			} else if (offerDetails.get(0).getUnit() != null
					&& offerDetails.get(0).getUnit().getUnitId() != null
					&& offerDetails.get(0).getUnit().getUnitId() != 0) {
				offerFor = "UNITS";
			}
		}
		if (offerFor != null) {
			offerBL.updatePropertyStatus(offerFor, offerDetails,
					Byte.parseByte("6"));
		}
	}

	public void deleteContractTempTransaction(Contract contract)
			throws Exception {
		this.getTransactionBL().deleteTranscationTempByJournalNO(
				"CT-".concat(String.valueOf(contract.getContractId())
						.concat("-").concat(contract.getContractNo())),
				getImplementationId());
	}

	public List<ContractPayment> getPaymentDetails(Contract contract)
			throws Exception {
		contractPayments = contractService.getContractPaymentList(contract
				.getContractId());
		return contractPayments;
	}

	public boolean checkPropertyAccount(Implementation implementation)
			throws Exception {
		return true; 
	}

	public Account getCustomerDetail(Contract contract) throws Exception {
		Account account = new Account();
		tenentOffer = offerService.getTenantById(contract.getOffer()
				.getOfferId());
		Person person = null;
		Company company = null;
		StringBuilder accountCode = new StringBuilder();
		if (tenentOffer.getPerson() != null) {
			person = new Person();
			person = offerService.getPersonDetails(tenentOffer.getPerson()
					.getPersonId());
			accountCode.append(person.getFirstName().trim().concat(" ")
					.concat(person.getLastName().trim())
					+ "-TENANT");
		} else {
			company = new Company();
			company = offerService.getCompanyDetails(tenentOffer.getCompany()
					.getCompanyId());
			accountCode.append(company.getCompanyName().trim() + "-TENANT");
		}
		account = this
				.getTransactionBL()
				.getCombinationService()
				.getAccountDetail(accountCode.toString(),
						contract.getImplementation());
		return account;
	}

	/*
	 * public Account getPropertyDetail(Property property) throws Exception{
	 * Account account =new Account(); String
	 * accountCode="Rent Income "+property.getPropertyName(); account =
	 * accountService.getAccountDetails(accountCode.toString(),
	 * property.getImplementation()); return account; }
	 */

	public Combination getCombinationDetails(Long combinationId)
			throws Exception {
		Combination combination = this.getTransactionBL()
				.getCombinationService()
				.getCombination(combinationId.intValue());
		return combination;
	}

	public Combination getCombinationByAccount(Long accountId) throws Exception {
		Combination combination = this.getTransactionBL()
				.getCombinationService().getCombinationByAccount(accountId);
		return combination;
	}

	private double checkPayments(Contract contract) throws Exception {
		contractPayments = getPaymentDetails(contract);
		double amount = 0;
		for (ContractPayment list : contractPayments) {
			amount += list.getAmount();
		}
		return amount;
	}

	public void revertTransaction(long contractId) throws Exception {
		Contract contract = this.getContractService().getContractDetails(
				contractId);
		List<TransactionTemp> transactionTempList = this
				.getTransactionBL()
				.getTransactionTempService()
				.getTransactionTempJournalNo(
						"CT-".concat(String.valueOf(contractId).concat("-")
								.concat(contract.getContractNo())),
						getImplementationId());
		List<TransactionDetail> transactionDetailList = null;
		for (TransactionTemp list : transactionTempList) {
			Transaction transaction = this.getTransactionBL()
					.createTransaction(list.getCurrency().getCurrencyId(),
							list.getDescription(), list.getTransactionTime(),
							list.getCategory(), contract.getContractId(),
							Contract.class.getSimpleName());
			transactionDetailList = new ArrayList<TransactionDetail>();
			for (TransactionTempDetail detailList : new ArrayList<TransactionTempDetail>(
					list.getTransactionTempDetails())) {
				TransactionDetail detail = this.getTransactionBL()
						.createTransactionDetail(detailList.getAmount(),
								detailList.getCombination().getCombinationId(),
								detailList.getIsDebit(),
								detailList.getDescription(),
								detailList.getIsreceipt(),null);
				transactionDetailList.add(detail);
			}
			this.transactionBL.saveTransaction(transaction,
					transactionDetailList);
		}
		this.getTransactionBL().deleteTranscationTempByJournalNO(
				"CT-".concat(String.valueOf(contractId).concat("-")
						.concat(contract.getContractNo())),
				getImplementationId());
	}

	private void revertDeleteContractTransaction(long contractId)
			throws Exception {
		Contract contract = this.getContractService().getContractDetails(
				contractId);
		List<TransactionTemp> transactionTempList = this
				.getTransactionBL()
				.getTransactionTempService()
				.getTransactionTempJournalNo(
						"CT-".concat(String.valueOf(contractId).concat("-")
								.concat(contract.getContractNo())),
						getImplementationId());
		List<TransactionDetail> transactionDetailList = null;
		for (TransactionTemp list : transactionTempList) {
			Transaction transaction = this.getTransactionBL()
					.createTransaction(list.getCurrency().getCurrencyId(),
							list.getDescription(), list.getTransactionTime(),
							list.getCategory(), contract.getContractId(),
							Contract.class.getSimpleName());
			transactionDetailList = new ArrayList<TransactionDetail>();
			for (TransactionTempDetail detailList : new ArrayList<TransactionTempDetail>(
					list.getTransactionTempDetails())) {
				TransactionDetail detail = this.getTransactionBL()
						.createTransactionDetail(detailList.getAmount(),
								detailList.getCombination().getCombinationId(),
								detailList.getIsDebit(),
								detailList.getDescription(),
								detailList.getIsreceipt(),null);
				transactionDetailList.add(detail);
			}
			this.transactionBL.saveTransaction(transaction,
					transactionDetailList);
		}
	}

	public Property getPropertyDetails(long contractId) throws Exception {
		Contract contact = this.getContractService().getContractOffer(
				contractId);
		offer = contact.getOffer();
		List<OfferDetail> offerDetails = new ArrayList<OfferDetail>(
				offer.getOfferDetails());
		if (offerDetails.get(0).getProperty() != null) {
			return this.getPropertyService().getPropertyById(
					offerDetails.get(0).getProperty().getPropertyId());
		} else if (offerDetails.get(0).getUnit() != null) {
			Unit unit = this.getUnitService().getUnitInformation(
					offerDetails.get(0).getUnit().getUnitId());
			Component component = this.getComponentService()
					.getComponentByIdWithProperty(
							unit.getComponent().getComponentId());
			return this.getPropertyService().getPropertyById(
					component.getProperty().getPropertyId());
		}
		return null;
	}

	public void saveContractTransaction(Contract contract,
			Implementation implementation) throws Exception {
		if (checkPayments(contract) > 0) {
			List<Category> categoryList = this.getTransactionBL()
					.getCategoryBL().getCategoryService()
					.getAllActiveCategories(contract.getImplementation());
			Category categoryFee = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Contract Fee")) {
						categoryFee.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryFee || categoryFee.equals("")) {
				categoryFee = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Fee");
			} else if (null == categoryFee.getCategoryId()) {
				categoryFee = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Fee");
			}
			List<TransactionDetail> detailList = new ArrayList<TransactionDetail>();
			String description = "";
			Combination combination = getCombinationDetails(implementation
					.getRevenueAccount());
			TransactionDetail transactionDetail = this.getTransactionBL()
					.createTransactionDetail(contract.getContractFee(),
							implementation.getRevenueAccount(), false,
							"Contract Fee Credited.", null,null);
			detailList.add(transactionDetail);// ADD TRANSACTION DETAIL TO LIST
			// FOR CUSTOMER
			Account account = new Account();
			account = getCustomerDetail(contract);
			if (account != null) {
				combination = getCombinationByAccount(account.getAccountId());
				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(contract.getContractFee(),
								combination.getCombinationId(), true,
								"Customer Debited.", null,null);
				detailList.add(transactionDetail);// ADD TRANSACTION DETAIL TO
													// LIST
			}
			Transaction transaction = this.getTransactionBL()
					.createTransaction(
							this.getTransactionBL().getCurrency()
									.getCurrencyId(), "CONTRACT FEE/CUSTOMER",
							new Date(), categoryFee, contract.getContractId(),
							Contract.class.getSimpleName());
			this.getTransactionBL().saveTransaction(transaction, detailList);// SAVE
																				// TRANSACTION
																				// CONTRACT
																				// FEES
																				// CREDIT/
																				// CUSTOMER
																				// DEBIT

			// FOR CONTRACT DEPOSIT & CUSTOMER TRANSACTION
			Category categoryDeposit = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Contract Deposit")) {
						categoryDeposit.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryDeposit || categoryDeposit.equals("")) {
				categoryDeposit = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Deposit");
			} else if (null == categoryDeposit.getCategoryId()) {
				categoryDeposit = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Contract Deposit");
			}
			detailList = new ArrayList<TransactionDetail>();
			transactionDetail = this.getTransactionBL()
					.createTransactionDetail(contract.getDeposit(),
							implementation.getUnearnedRevenue(), false,
							"Contract Deposit Credited.", null,null);
			detailList.add(transactionDetail);// ADD TRANSACTION DETAIL TO LIST
			// FOR CUSTOMER
			if (account != null) {
				account = getCustomerDetail(contract);
				combination = getCombinationByAccount(account.getAccountId());
				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(contract.getDeposit(),
								combination.getCombinationId(), true,
								"Customer Debited.", null,null);
			}
			detailList.add(transactionDetail);// ADD TRANSACTION DETAIL TO LIST
			transaction = this.getTransactionBL().createTransaction(
					this.getTransactionBL().getCurrency().getCurrencyId(),
					"CONTRACT DEPOSIT/CUSTOMER", new Date(), categoryDeposit,
					contract.getContractId(), Contract.class.getSimpleName());
			this.getTransactionBL().saveTransaction(transaction, detailList);// SAVE
																				// TRANSACTION
																				// CONTRACT
																				// DEPOSIT
																				// CREDIT/
																				// CUSTOMER
																				// DEBIT

			// FOR CONTRACT PAYMENT MODES TRANSACTION
			offerDetails = offerService.getOfferDetailById(contract.getOffer()
					.getOfferId());// GET OFFER DETAILS

			if (offerDetails.get(0).getProperty() != null) { // CHECK CONTRACT
																// IS FOR
																// PROPERTY/COMPONENT/UNIT
																// AND SET TO
																// PROPERTY ID
				propertyId = offerDetails.get(0).getProperty().getPropertyId();
			} else if (offerDetails.get(0).getComponent() != null) {
				component = compService.getComponentById(offerDetails.get(0)
						.getComponent().getComponentId());
				propertyId = component.getProperty().getPropertyId();
			} else if (offerDetails.get(0).getUnit() != null) {
				unit = unitService.getUnitDetails(offerDetails.get(0).getUnit()
						.getUnitId());
				component = compService.getComponentById(unit.getComponent()
						.getComponentId());
				propertyId = component.getProperty().getPropertyId();
			}

			property = propertyService.getPropertyById(propertyId); // GET
																	// PROPERTY
																	// DETAILS
			contractPayments = getPaymentDetails(contract); // LIST OF PAYMENT
															// DETAILS
			double totalContractRent = 0;
			for (ContractPayment list : contractPayments) {
				totalContractRent += list.getAmount();
			}
			totalContractRent -= (contract.getContractFee() + contract
					.getDeposit());

			// FOR Unearned Revenue
			Category categoryUnearned = new Category();
			if (null != categoryList) {
				for (Category list : categoryList) {
					if (list.getName().equalsIgnoreCase("Unearned Revenue")) {
						categoryUnearned.setCategoryId(list.getCategoryId());
						break;
					}
				}
			}
			if (null == categoryUnearned || categoryUnearned.equals("")) {
				categoryUnearned = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Unearned Revenue");
			} else if (null == categoryUnearned.getCategoryId()) {
				categoryUnearned = this
						.getTransactionBL()
						.getCategoryBL()
						.createCategory(contract.getImplementation(),
								"Unearned Revenue");
			}
			detailList = new ArrayList<TransactionDetail>();
			transactionDetail = this.getTransactionBL()
					.createTransactionDetail(totalContractRent,
							implementation.getUnearnedRevenue(), false,
							"Unearned Revenue Credited.", null,null);
			detailList.add(transactionDetail);
			// FOR CUSTOMER
			account = getCustomerDetail(contract);
			if (account != null) {
				combination = getCombinationByAccount(account.getAccountId());
				transactionDetail = this.getTransactionBL()
						.createTransactionDetail(totalContractRent,
								combination.getCombinationId(), true,
								"Customer Debited", null,null);
			}
			detailList.add(transactionDetail);
			// CREATE Unearned Revenue TRANSACTION
			transaction = new Transaction();
			description = "Unearned Revenue/CUSTOMER";
			transaction = this.transactionBL.createTransaction(this
					.getTransactionBL().getCurrency().getCurrencyId(),
					description, new Date(), categoryUnearned,
					contract.getContractId(), Contract.class.getSimpleName());
			this.transactionBL.saveTransaction(transaction, detailList); // FOR
																			// CONTRACT
																			// RENT
																			// INCOME
		} else {
			LOGGER.info("Contract Payments is Zero, No transaction for Zero Payments");
		}
	}

	public void saveContractAlert(Contract contract,
			Implementation implementation) throws Exception {
		if (checkPayments(contract) > 0) {
			Alert alert = new Alert();
			// Alert Role Access
			AlertRole alertRole = alertBL.getRoleIdByModuleProcess(ContractPayment.class
					.getName());
			if (alertRole.getRole() != null) {
				alert.setRoleId(alertRole.getRole().getRoleId());
			} else { 
				alert.setPersonId(alertRole.getPersonId());
			}
			alert.setMessage("Contract Receipt for " + contract.getContractNo());
			Screen screen = systemService
					.getScreenBasedOnScreenName("ContractPaymentAlert");
			alert.setScreenId(screen.getScreenId());
			alert.setIsActive(true);
			alertBL.saveAlerts(contract, alert);
			/*
			 * ContractPayment contractPayment = this.getContractService()
			 * .getContractPaymentList(contract.getContractId()).get(0);
			 */
			alertBL.saveAlerts(contract, alert);
		} else {
			LOGGER.info("Contract Payments is Zero, No Alerts");
		}
	}

	// Tenant TO conversion
	public ReContractAgreementTO tenantOfferToContractTOConversionPassingPersonOrCompany(
			TenantOffer tenantOffer, ReContractAgreementTO offerAgreementTO)
			throws Exception {
		Company company = null;
		if (tenantOffer.getPerson() != null) {
			// Find Person
			persons = new ArrayList<Person>();
			persons.add(tenantOffer.getPerson());
			if (persons != null && persons.size() > 0) {
				offerAgreementTO.setTenantName(persons.get(0).getFirstName()
						+ " " + persons.get(0).getMiddleName() + " "
						+ persons.get(0).getLastName());
				offerAgreementTO.setTenantNameArabic(persons.get(0)
						.getFirstNameArabic()
						+ " "
						+ persons.get(0).getMiddleNameArabic()
						+ " "
						+ persons.get(0).getLastNameArabic());
				offerAgreementTO.setTenantId(Integer.parseInt(persons.get(0)
						.getPersonId().toString()));
				offerAgreementTO.setPresentAddress(persons.get(0)
						.getTownBirth());
				offerAgreementTO.setPermanantAddress(persons.get(0)
						.getRegionBirth());
				offerAgreementTO.setTenantIssueDate(persons.get(0)
						.getValidFromDate().toString());
				try {
					List<Identity> identity = personService
							.getAllIdentity(persons.get(0));
					offerAgreementTO.setTenantIdentity(getIdentityType(identity
							.get(0).getIdentityType())
							+ " - "
							+ identity.get(0).getIdentityNumber());
				} catch (Exception e) {
					e.printStackTrace();
					offerAgreementTO.setTenantIdentity("n/a");
				}
				String[] country = systemService
						.getCountryById(
								Long.parseLong(persons.get(0).getNationality()))
						.getCountryName().split("/");
				offerAgreementTO.setTenantNationality(country[0]);
				if (persons.get(0).getMobile() != null) {
					offerAgreementTO.setTenantMobile(persons.get(0).getMobile()
							.toString());
				}
				if (persons.get(0).getAlternateTelephone() != null)
					offerAgreementTO.setTenantFax(persons.get(0)
							.getAlternateTelephone().toString());
				if (persons.get(0).getPostboxNumber() != null)
					offerAgreementTO.setTenantPOBox(persons.get(0)
							.getPostboxNumber());
				offerAgreementTO.setTenantType("PERSON");
			}
		} else {
			company = new Company();
			company = tenantOffer.getCompany();
			if (company != null) {
				offerAgreementTO.setTenantName(company.getCompanyName());
				offerAgreementTO.setTenantNameArabic(AIOSCommons
						.bytesToUTFString(company.getCompanyNameArabic()));
				offerAgreementTO.setTenantId(Integer.parseInt(company
						.getCompanyId().toString()));
				offerAgreementTO.setPresentAddress(company.getCompanyAddress());
				offerAgreementTO.setTenantIssueDate("n/a");
				offerAgreementTO.setTenantIdentity(company.getTradeNo());
				offerAgreementTO.setTenantNationality("n/a");
				offerAgreementTO.setTenantFax(company.getCompanyFax());
				offerAgreementTO.setTenantMobile(company.getCompanyPhone());
				offerAgreementTO.setTenantPOBox(company.getCompanyPobox());
				offerAgreementTO.setTenantType("COMPANY");
			}
		}
		String tenantFullDet = " Identity : "
				+ offerAgreementTO.getTenantIdentity() + ", Nationality : "
				+ offerAgreementTO.getTenantNationality() + ", Mobile : "
				+ offerAgreementTO.getTenantMobile() + ", Street : "
				+ offerAgreementTO.getBuildingStreet() + ", Fax : "
				+ offerAgreementTO.getTenantFax();
		offerAgreementTO.setTenantFullInfo(tenantFullDet);
		return offerAgreementTO;
	}

	// Tenant TO conversion
	public ReContractAgreementTO tenantOfferToContractTOConversion(
			TenantOffer tenantOffer, ReContractAgreementTO offerAgreementTO)
			throws Exception {
		Company company = null;
		if (tenantOffer.getPerson() != null) {
			// Find Person
			persons = this.contractService.getPersonDetails(tenantOffer
					.getPerson());
			if (persons != null && persons.size() > 0) {
				offerAgreementTO.setTenantName(persons.get(0).getFirstName()
						+ " " + persons.get(0).getMiddleName() + " "
						+ persons.get(0).getLastName());
				offerAgreementTO.setTenantNameArabic(persons.get(0)
						.getFirstNameArabic()
						+ " "
						+ persons.get(0).getMiddleNameArabic()
						+ " "
						+ persons.get(0).getLastNameArabic());
				offerAgreementTO.setTenantId(Integer.parseInt(persons.get(0)
						.getPersonId().toString()));
				offerAgreementTO.setPresentAddress(persons.get(0)
						.getTownBirth());
				offerAgreementTO.setPermanantAddress(persons.get(0)
						.getRegionBirth());
				offerAgreementTO.setTenantIssueDate(persons.get(0)
						.getValidFromDate().toString());
				try {
					List<Identity> identity = personService
							.getAllIdentity(persons.get(0));
					offerAgreementTO.setTenantIdentity(getIdentityType(identity
							.get(0).getIdentityType())
							+ " - "
							+ identity.get(0).getIdentityNumber());
				} catch (Exception e) {
					e.printStackTrace();
					offerAgreementTO.setTenantIdentity("n/a");
				}
				String[] country = systemService
						.getCountryById(
								Long.parseLong(persons.get(0).getNationality()))
						.getCountryName().split("/");
				offerAgreementTO.setTenantNationality(country[0]);
				if (persons.get(0).getMobile() != null) {
					offerAgreementTO.setTenantMobile(persons.get(0).getMobile()
							.toString());
				}
				if (persons.get(0).getAlternateTelephone() != null)
					offerAgreementTO.setTenantFax(persons.get(0)
							.getAlternateTelephone().toString());
				if (persons.get(0).getPostboxNumber() != null)
					offerAgreementTO.setTenantPOBox(persons.get(0)
							.getPostboxNumber());
				offerAgreementTO.setTenantType("PERSON");
			}
		} else {
			company = new Company();
			company = offerService.getCompanyDetails(tenantOffer.getCompany()
					.getCompanyId());
			if (company != null) {
				offerAgreementTO.setTenantName(company.getCompanyName());
				offerAgreementTO.setTenantNameArabic(AIOSCommons
						.bytesToUTFString(company.getCompanyNameArabic()));
				offerAgreementTO.setTenantId(Integer.parseInt(company
						.getCompanyId().toString()));
				offerAgreementTO.setPresentAddress(company.getCompanyAddress());
				offerAgreementTO.setTenantIssueDate("n/a");
				offerAgreementTO.setTenantIdentity(company.getTradeNo());
				offerAgreementTO.setTenantNationality("n/a");
				offerAgreementTO.setTenantFax(company.getCompanyFax());
				offerAgreementTO.setTenantMobile(company.getCompanyPhone());
				offerAgreementTO.setTenantPOBox(company.getCompanyPobox());
				offerAgreementTO.setTenantType("COMPANY");
			}
		}
		String tenantFullDet = " Identity : "
				+ offerAgreementTO.getTenantIdentity() + ", Nationality : "
				+ offerAgreementTO.getTenantNationality() + ", Mobile : "
				+ offerAgreementTO.getTenantMobile() + ", Street : "
				+ offerAgreementTO.getBuildingStreet() + ", Fax : "
				+ offerAgreementTO.getTenantFax();
		offerAgreementTO.setTenantFullInfo(tenantFullDet);
		return offerAgreementTO;
	}

	public void propertyInfoToContractTOConversion(
			ReContractAgreementTO offerAgreementTO) throws Exception {
		boolean ownersFetched = false;
		String[] ownersEngArabic = new String[2];

		if (offerAgreementTO.getPropertyId() != 0) {
			property = this.getContractService().getPropertyInfo(
					offerAgreementTO.getPropertyId());
			offerAgreementTO.setFlatNumber(property.getPropertyName());
			offerAgreementTO.setRentAmount(property.getRentYr());
			offerAgreementTO.setBuildingName(property.getPropertyName());
			offerAgreementTO.setBuildingArea(property.getAddressOne());
			try {
				String[] city = systemService.getCityById(property.getCity())
						.getCityName().split("/");
				offerAgreementTO.setBuildingCity(city[0]);
			} catch (Exception e) {
				offerAgreementTO.setBuildingCity("");
			}
			offerAgreementTO.setBuildingPlot(property.getPlotNo());
			offerAgreementTO.setBuildingStreet(property.getAddressTwo());
			offerAgreementTO.setServiceMeter(property.getServiceMeter() + "");
			if (!ownersFetched) {
				ownersEngArabic = getOwners("property",
						offerAgreementTO.getPropertyId());
				offerAgreementTO.setOfferedPropertyOwners(ownersEngArabic[0]);
				offerAgreementTO
						.setOfferedPropertyOwners_arabic(ownersEngArabic[1]);
				ownersFetched = true;
			}

		} else if (offerAgreementTO.getComponentId() != 0) {
			component = this.getContractService().getComponentInfo(
					offerAgreementTO.getComponentId());
			offerAgreementTO.setFlatNumber(component.getComponentName());
			offerAgreementTO.setRentAmount(component.getRent());

			property = componentService.getComponentByIdWithProperty(
					offerAgreementTO.getComponentId()).getProperty();
			if (property != null) {

				offerAgreementTO.setBuildingName(property.getPropertyName());
				offerAgreementTO.setBuildingArea(property.getAddressOne());
				offerAgreementTO.setServiceMeter(property.getServiceMeter()
						+ "");
				try {
					String[] city = systemService
							.getCityById(property.getCity()).getCityName()
							.split("/");
					offerAgreementTO.setBuildingCity(city[0]);
				} catch (Exception e) {
					offerAgreementTO.setBuildingCity("");
				}
				offerAgreementTO.setBuildingPlot(property.getPlotNo());
				offerAgreementTO.setBuildingStreet(property.getAddressTwo());
				if (!ownersFetched) {
					ownersEngArabic = getOwners("property",
							property.getPropertyId());
					offerAgreementTO
							.setOfferedPropertyOwners(ownersEngArabic[0]);
					offerAgreementTO
							.setOfferedPropertyOwners_arabic(ownersEngArabic[1]);
					ownersFetched = true;
				}
			}

		} else if (offerAgreementTO.getUnitId() != 0) {
			unit = this.getContractService().getUnitInfo(
					offerAgreementTO.getUnitId());
			offerAgreementTO.setFlatNumber(unit.getUnitName());
			offerAgreementTO.setRentAmount(unit.getRent());
			offerAgreementTO.setServiceMeter(unit.getMeter() + "");

			try {
				unit = unitService.getPropertyByUnitId(unit).get(0);
				property = componentService.getComponentByIdWithProperty(
						unit.getComponent().getComponentId()).getProperty();
				if (property != null) {

					offerAgreementTO
							.setBuildingName(property.getPropertyName());
					offerAgreementTO.setBuildingArea(property.getAddressOne());
					offerAgreementTO.setServiceMeter(property.getServiceMeter()
							+ "");
					try {
						String[] city = systemService
								.getCityById(property.getCity()).getCityName()
								.split("/");
						offerAgreementTO.setBuildingCity(city[0]);
					} catch (Exception e) {
						offerAgreementTO.setBuildingCity("");
					}
					offerAgreementTO.setBuildingPlot(property.getPlotNo());
					offerAgreementTO
							.setBuildingStreet(property.getAddressTwo());
					if (!ownersFetched) {
						ownersEngArabic = getOwners("property",
								property.getPropertyId());
						offerAgreementTO
								.setOfferedPropertyOwners(ownersEngArabic[0]);
						offerAgreementTO
								.setOfferedPropertyOwners_arabic(ownersEngArabic[1]);
						ownersFetched = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			String propFullDet = " Flat : " + offerAgreementTO.getFlatNumber()
					+ ", Area : " + offerAgreementTO.getBuildingArea()
					+ ", Plot : " + offerAgreementTO.getBuildingPlot()
					+ ", Street : " + offerAgreementTO.getBuildingStreet()
					+ ", City : " + offerAgreementTO.getBuildingCity();
			offerAgreementTO.setPropertyFullInfo(propFullDet);
		}

	}

	public void offerInfoToContractTOConversion(Contract contract,
			ReContractAgreementTO offerAgreementTO) throws Exception {
		// Offer Info section
		offer = new Offer();
		offer = this.contractService.getOfferDetails(contract.getOffer()
				.getOfferId());
		if (offer.getStartDate() != null)
			offerAgreementTO.setFromDate(DateFormat.convertDateToString(offer
					.getStartDate().toString()));
		if (offer.getEndDate() != null)
			offerAgreementTO.setToDate(DateFormat.convertDateToString(offer
					.getEndDate().toString()));
		String cprd = "From : " + offerAgreementTO.getFromDate() + " UpTo : "
				+ offerAgreementTO.getToDate();
		offerAgreementTO.setContractPeriod(cprd);
		offerAgreementTO.setOfferNumber(offer.getOfferNo());
		// Offer Detail
		double offerAmountTotal = 0;
		List<OfferDetail> offerDetails = offerService.getOfferDetailById(offer
				.getOfferId());
		if (offerDetails != null && offerDetails.size() > 0) {
			for (OfferDetail offerDetail : offerDetails) {
				offerAmountTotal += offerDetail.getOfferAmount();
			}
			offerAgreementTO.setOfferAmount(offerAmountTotal);
			if (offerDetails.get(0).getProperty() != null
					&& offerDetails.get(0).getProperty().getPropertyId() != null)
				offerAgreementTO.setPropertyId(offerDetails.get(0)
						.getProperty().getPropertyId());
			if (offerDetails.get(0).getComponent() != null
					&& offerDetails.get(0).getComponent().getComponentId() != null)
				offerAgreementTO.setComponentId(offerDetails.get(0)
						.getComponent().getComponentId());
			if (offerDetails.get(0).getUnit() != null
					&& offerDetails.get(0).getUnit().getUnitId() != null)
				offerAgreementTO.setUnitId(offerDetails.get(0).getUnit()
						.getUnitId());
		}

	}

	public List<ReContractAgreementTO> getContractPaymentList(
			Implementation implementation, Byte paymentStatus, Date startDate,
			Date endDate) {
		try {
			List<ContractPayment> contractpayments = new ArrayList<ContractPayment>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();

			contractpayments = this.getContractService()
					.getAllContractPaymentDetails(implementation,
							paymentStatus, startDate, endDate);
			if (contractpayments != null && contractpayments.size() > 0) {

				agreementTOList = ReContractAgreementTOConverter
						.convertToPaymentTOList(contractpayments);
				int j = 0;
				long contractIdTemp = 0;

				for (int i = 0; i < contractpayments.size(); i++) {
					// Contract Info
					if (contractIdTemp == 0
							|| contractIdTemp != contractpayments.get(i)
									.getContract().getContractId())
						j = 0;
					else
						j++;

					contractIdTemp = contractpayments.get(i).getContract()
							.getContractId();

					agreementTOList.get(i).setContractNumber(
							contractpayments.get(i).getContract()
									.getContractNo());
					if (j == 0)
						agreementTOList.get(i).setPaymentType("Contract Fee");
					else if (j == 1)
						agreementTOList.get(i).setPaymentType(
								"Security Deposit");
					else if (j > 1)
						agreementTOList.get(i).setPaymentType("Rent");

					// Offer Info section
					offerInfoToContractTOConversion(contractpayments.get(i)
							.getContract(), agreementTOList.get(i));

					// Tenant Info section
					tenentOffers = this.contractService
							.getTenantOfferDetails(contractpayments.get(i)
									.getContract().getOffer().getOfferId());
					if (tenentOffers != null) {
						agreementTOList.get(i).setTenantInfo(
								tenantOfferToContractTOConversion(
										tenentOffers.get(0),
										agreementTOList.get(i)));
					}
					// Property Info section
					propertyInfoToContractTOConversion(agreementTOList.get(i));
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	// Property unit rental report
	public List<ReContractAgreementTO> getPropertyUnitRentalList(
			Long propertyId, Date startDate, Date endDate) {
		try {
			List<OfferDetail> offerDetials = new ArrayList<OfferDetail>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			offerDetials = this.getContractService()
					.getPropertyOfferDetailList(propertyId, startDate, endDate);
			for (OfferDetail offerDetail : offerDetials) {
				ReContractAgreementTO to = new ReContractAgreementTO();
				// Tenant offer info
				for (TenantOffer tenantoffer : offerDetail.getOffer()
						.getTenantOffers()) {
					tenantOfferToContractTOConversion(tenantoffer, to);
				}
				// building info
				if (offerDetail.getUnit() != null) {
					to.setBuildingName(offerDetail.getUnit().getUnitName());
					to.setBuildingId(Integer.parseInt(offerDetail.getUnit()
							.getUnitId() + ""));
				} else {
					to.setBuildingId(Integer.parseInt(offerDetail.getProperty()
							.getPropertyId() + ""));
					to.setBuildingName(offerDetail.getProperty()
							.getPropertyName());
				}
				// Offer Info
				to.setContractPeriod(DateFormat.convertDateToString(offerDetail
						.getOffer().getStartDate().toString())
						+ " to "
						+ DateFormat.convertDateToString(offerDetail.getOffer()
								.getEndDate().toString()));

				// Contract info
				Double amount = 0.0;
				for (Contract contract : offerDetail.getOffer().getContracts()) {
					List<ContractPayment> cp = this.getContractService()
							.getPropertyUnitRentalContractPayments(
									contract.getContractId(), startDate,
									endDate);
					for (ContractPayment contractPayment : cp) {
						amount += contractPayment.getAmount();
					}
				}
				to.setContractAmount(amount);
				agreementTOList.add(to);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	// Property rental report
	public List<ReContractAgreementTO> getPropertyRentalList(
			Implementation implementation, Date startDate, Date endDate) {
		try {
			List<OfferDetail> offerDetials = new ArrayList<OfferDetail>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			offerDetials = this.getContractService()
					.getPropertyRentalOfferDetailList(implementation,
							startDate, endDate);
			for (OfferDetail offerDetail : offerDetials) {
				ReContractAgreementTO to = new ReContractAgreementTO();

				// building info
				if (offerDetail.getUnit() != null) {
					to.setBuildingId(Integer.parseInt(offerDetail.getUnit()
							.getComponent().getProperty().getPropertyId()
							+ ""));
					to.setBuildingArea(offerDetail.getUnit().getComponent()
							.getProperty().getAddressOne());
					to.setBuildingName(offerDetail.getUnit().getComponent()
							.getProperty().getPropertyName());
				} else {
					to.setBuildingId(Integer.parseInt(offerDetail.getProperty()
							.getPropertyId() + ""));
					to.setBuildingArea(offerDetail.getProperty()
							.getAddressOne());
					to.setBuildingName(offerDetail.getProperty()
							.getPropertyName());

				}
				// Contract info
				Double amount = 0.0;
				for (Contract contract : offerDetail.getOffer().getContracts()) {
					List<ContractPayment> cp = this.getContractService()
							.getPropertyUnitRentalContractPayments(
									contract.getContractId(), startDate,
									endDate);
					for (ContractPayment contractPayment : cp) {
						amount += contractPayment.getAmount();
					}
				}
				to.setContractAmount(amount);
				agreementTOList.add(to);
			}

			Map<Integer, ReContractAgreementTO> mapResult = new HashMap<Integer, ReContractAgreementTO>();

			for (ReContractAgreementTO reContractAgreementTO : agreementTOList) {
				if (mapResult
						.containsKey(reContractAgreementTO.getBuildingId())) {
					ReContractAgreementTO tempTO = mapResult
							.get(reContractAgreementTO.getBuildingId());
					tempTO.setContractAmount(reContractAgreementTO
							.getContractAmount() + tempTO.getContractAmount());
					mapResult
							.put(reContractAgreementTO.getBuildingId(), tempTO);
				} else {
					mapResult.put(reContractAgreementTO.getBuildingId(),
							reContractAgreementTO);
				}
			}

			agreementTOList = new ArrayList<ReContractAgreementTO>(
					mapResult.values());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	// Contract report filter information getContractFilterInformation
	public List<ReContractAgreementTO> getContractRentalRangeReportFilterInformation(
			Implementation implementation, Double fromAmount, Double toAmount) {

		try {
			contracts = new ArrayList<Contract>();
			List<Contract> tempContracts = new ArrayList<Contract>();
			agreementTOList = new ArrayList<ReContractAgreementTO>();
			tempContracts = this.getContractService().getFilterContractDetails(
					implementation);

			if (fromAmount != null && fromAmount > 0 && toAmount != null
					&& toAmount > 0) {
				for (Contract contract : tempContracts) {
					Double offerAmount = 0.0;
					for (OfferDetail offerDetail : contract.getOffer()
							.getOfferDetails()) {
						offerAmount += offerDetail.getOfferAmount();
					}
					if (offerAmount >= fromAmount && offerAmount <= toAmount) {
						contracts.add(contract);
					}
				}

			} else if (fromAmount != null && fromAmount > 0
					&& (toAmount == null || toAmount == 0.0)) {
				for (Contract contract : tempContracts) {
					Double offerAmount = 0.0;
					for (OfferDetail offerDetail : contract.getOffer()
							.getOfferDetails()) {
						offerAmount += offerDetail.getOfferAmount();
					}
					if (offerAmount >= fromAmount) {
						contracts.add(contract);
					}
				}
			} else if ((fromAmount == null || fromAmount == 0.0)
					&& toAmount != null && toAmount > 0) {
				for (Contract contract : tempContracts) {
					Double offerAmount = 0.0;
					for (OfferDetail offerDetail : contract.getOffer()
							.getOfferDetails()) {
						offerAmount += offerDetail.getOfferAmount();
					}
					if (offerAmount <= toAmount) {
						contracts.add(contract);
					}
				}
			} else {
				contracts.addAll(tempContracts);
			}

			tenentOffer = new TenantOffer();
			if (contracts != null && contracts.size() > 0) {
				agreementTOList = ReContractAgreementTOConverter
						.convertToTOList(contracts);
				for (int i = 0; i < contracts.size(); i++) {

					offer = new Offer();
					persons = new ArrayList<Person>();

					// Offer Info section
					offer = this.contractService.getOfferDetails(contracts
							.get(i).getOffer().getOfferId());
					if (offer.getStartDate() != null)
						agreementTOList.get(i).setFromDate(
								DateFormat.convertDateToString(offer
										.getStartDate().toString()));
					if (offer.getEndDate() != null)
						agreementTOList.get(i).setToDate(
								DateFormat.convertDateToString(offer
										.getEndDate().toString()));
					String cprd = "From : "
							+ agreementTOList.get(i).getFromDate() + " UpTo : "
							+ agreementTOList.get(i).getToDate();
					agreementTOList.get(i).setContractPeriod(cprd);

					// Offer Detail
					double offerAmountTotal = 0;
					List<OfferDetail> offerDetails = offerService
							.getOfferDetailById(offer.getOfferId());
					if (offerDetails != null && offerDetails.size() > 0) {
						for (OfferDetail offerDetail : offerDetails) {
							offerAmountTotal += offerDetail.getOfferAmount();
						}
						agreementTOList.get(i).setOfferAmount(offerAmountTotal);
						if (offerDetails.get(0).getProperty() != null
								&& offerDetails.get(0).getProperty()
										.getPropertyId() != null)
							agreementTOList.get(i).setPropertyId(
									offerDetails.get(0).getProperty()
											.getPropertyId());
						if (offerDetails.get(0).getComponent() != null
								&& offerDetails.get(0).getComponent()
										.getComponentId() != null)
							agreementTOList.get(i).setComponentId(
									offerDetails.get(0).getComponent()
											.getComponentId());
						if (offerDetails.get(0).getUnit() != null
								&& offerDetails.get(0).getUnit().getUnitId() != null)
							agreementTOList.get(i).setUnitId(
									offerDetails.get(0).getUnit().getUnitId());
					}

					// Tenant Info section
					tenentOffers = this.contractService
							.getTenantOfferDetails(offer.getOfferId());
					if (tenentOffers != null) {
						/*
						 * ReContractAgreementTO
						 * tempTenantInfo=tenantOfferToContractTOConversion
						 * (tenentOffers.get(0),agreementTOList.get(i));
						 * agreementTOList.get(i).setTenantInfo(tempTenantInfo);
						 */
						tenantOfferToContractTOConversion(tenentOffers.get(0),
								agreementTOList.get(i));
					}
					// Property Info section
					propertyInfoToContractTOConversion(agreementTOList.get(i));

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agreementTOList;

	}

	@SuppressWarnings("unchecked")
	public void renewSaveContractAgreement(Contract contract,
			Implementation implementation, boolean editFlag,
			TenantOffer tenantOffer, Contract updateContract) throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();

		// contracts = new ArrayList<Contract>();
		contractPayment = new ContractPayment();
		contractPayments = new ArrayList<ContractPayment>();
		List<OfferDetail> offerDetails = new ArrayList<OfferDetail>(contract
				.getOffer().getOfferDetails());
		Offer offer = new Offer();
		offer = contract.getOffer();

		offer.setOfferId(null);
		offerService.saveOfferForm(offer, offerDetails, tenantOffer);

		contractPayments = (ArrayList<ContractPayment>) session
				.getAttribute("CONTRACT_PAYMENTS_"
						+ sessionObj.get("jSessionId"));
		updateContract.setIsRenewal(Byte.parseByte("1"));
		contractService.updateContract(updateContract);
		contract.setContractId(null);
		contract.setOffer(offer);
		contractService.addContract(contract, contractPayments);
		User user = (User) sessionObj.get("USER");
		WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
		workflowDetailVo
				.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
						.getCode());
		workflowEnterpriseService.generateNotificationsAgainstDataEntry(
				"Contract", contract.getContractId(), user, workflowDetailVo);

		try {
			saveContractUploads(contract, editFlag);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("Contract Uploads Exception");
		}

		LOGGER.info("Contract save success");

	}

	private String getIdentityType(int code) throws Exception {

		switch (code) {
		case 1:
			return "Emirates ID";
		case 2:
			return "Labour Card";
		case 3:
			return "Passport";
		default:
			return "ID";
		}
	}

	public Implementation getImplementationId() {
		implementation = new Implementation();
		HttpSession session = ServletActionContext.getRequest().getSession();
		if (session.getAttribute("THIS") != null) {
			implementation = (Implementation) session.getAttribute("THIS");
		}
		return implementation;
	}

	public ContractService getContractService() {
		return contractService;
	}

	public void setContractService(ContractService contractService) {
		this.contractService = contractService;
	}

	ComponentService compService;
	PersonService personService;
	PropertyService propertyService;
	UnitService unitService;
	CompanyService companyService;

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public ComponentService getCompService() {
		return compService;
	}

	public void setCompService(ComponentService compService) {
		this.compService = compService;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	/*
	 * public ReContractAgreementTO getContractDetail(Contract contract) throws
	 * Exception{ ReContractAgreementTO to = new ReContractAgreementTO();
	 * Contract con = contractService.getAllContractWithDetails(contract); to =
	 * ReContractAgreementTOConverter.convertToTO(con); Set<OfferDetail>
	 * offerDetails = con.getOffer().getOfferDetails();
	 * to.setProeprtyList(getProeprty(offerDetails));
	 * to.setContractPayments(con.getContractPayments());
	 * to.setTenantList(getTenants(con.getOffer().getTenantOffers())) ; return
	 * to;
	 * 
	 * }
	 * 
	 * private List<HrPersonalDetailsTO> getTenants(Set<TenantOffer>
	 * tenantOffers ) throws Exception{ List<HrPersonalDetailsTO> persons = new
	 * ArrayList<HrPersonalDetailsTO>(); for (TenantOffer offer : tenantOffers){
	 * HrPersonalDetailsTO to =
	 * HrPersonalDetailsTOConverter.convertPersonToTO(offer.getPerson());
	 * to.setPersonTypes
	 * (personService.getPersonType(offer.getPerson().getPersonType
	 * ().getPersonTypeId()).getTypeName());
	 * //to.setEmirateId(offer.getPerson().getEmiratesId()); persons.add(to); }
	 * return persons; }
	 * 
	 * 
	 * private List<RePropertyInfoTO> getProeprty(Set<OfferDetail> offerDetails)
	 * throws Exception{ List<RePropertyInfoTO> contractProp = new
	 * ArrayList<RePropertyInfoTO>(); for (OfferDetail detail : offerDetails){
	 * if (null != detail.getProperty()){
	 * contractProp.add(setPropertyAdd(detail.getProperty())); } else if
	 * (detail.getComponent()!= null){ List<Component> comps =
	 * compService.getPropertyByComponentId(detail.getComponent()); for
	 * (Component comp : comps){
	 * contractProp.add(setPropertyAdd(comp.getProperty())); } } else if
	 * (detail.getUnit() != null){ List<Unit> units =
	 * unitService.getPropertyByUnitId(detail.getUnit()); for (Unit u : units){
	 * contractProp.add(setPropertyAdd(u.getComponent().getProperty())); } } }
	 * return contractProp; }
	 * 
	 * private RePropertyInfoTO setPropertyAdd(Property p) throws Exception{
	 * //if (p != null){ RePropertyInfoTO to =
	 * RePropertyInfoTOConverter.convertToTO(p);
	 * to.setAddress(propertyService.getCityById( Long.parseLong("" +
	 * p.getCity())).getCityName() + " " + propertyService.getStateById(
	 * Long.parseLong("" + p.getState())).getStateName() + " " +
	 * propertyService.getCounryById( Long.parseLong("" + p.getCountry()))
	 * .getCountryName());
	 * 
	 * //contractProp.add(to); //} return to; }
	 */
	public OfferService getOfferService() {
		return offerService;
	}

	public void setOfferService(OfferService offerService) {
		this.offerService = offerService;
	}

	public OfferManagementBL getOfferBL() {
		return offerBL;
	}

	public void setOfferBL(OfferManagementBL offerBL) {
		this.offerBL = offerBL;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public LegalBL getLegalBL() {
		return legalBL;
	}

	public void setLegalBL(LegalBL legalBL) {
		this.legalBL = legalBL;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public AlertBL getAlertBL() {
		return alertBL;
	}

	public void setAlertBL(AlertBL alertBL) {
		this.alertBL = alertBL;
	}

	public CancellationBL getCancellationBL() {
		return cancellationBL;
	}

	public void setCancellationBL(CancellationBL cancellationBL) {
		this.cancellationBL = cancellationBL;
	}

	public ContractReleaseBL getContractReleaseBL() {
		return contractReleaseBL;
	}

	public void setContractReleaseBL(ContractReleaseBL contractReleaseBL) {
		this.contractReleaseBL = contractReleaseBL;
	}

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public Implementation getImplementation() {

		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public AIOTechGenericDAO<Workflow> getWorkflowDAO() {
		return workflowDAO;
	}

	public void setWorkflowDAO(AIOTechGenericDAO<Workflow> workflowDAO) {
		this.workflowDAO = workflowDAO;
	}

	public AIOTechGenericDAO<WorkflowDetail> getWorkflowDetailDAO() {
		return workflowDetailDAO;
	}

	public void setWorkflowDetailDAO(
			AIOTechGenericDAO<WorkflowDetail> workflowDetailDAO) {
		this.workflowDetailDAO = workflowDetailDAO;
	}

	public AIOTechGenericDAO<WorkflowDetailProcess> getWorkflowDetailProcessDAO() {
		return workflowDetailProcessDAO;
	}

	public void setWorkflowDetailProcessDAO(
			AIOTechGenericDAO<WorkflowDetailProcess> workflowDetailProcessDAO) {
		this.workflowDetailProcessDAO = workflowDetailProcessDAO;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
