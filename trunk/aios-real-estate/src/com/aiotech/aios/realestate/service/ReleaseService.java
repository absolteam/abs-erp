package com.aiotech.aios.realestate.service;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.ReleaseDetail;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class ReleaseService {
	
	private AIOTechGenericDAO<Release> releaseDAO;
	private AIOTechGenericDAO<ReleaseDetail> releaseDetailDAO;
	
	//Release List
	public List<Release> getAllRelease(Implementation implementation) throws Exception {
		return releaseDAO.findByNamedQuery("getAllReleaseList",implementation);
	}
	
	//Release Info
	public Release getReleaseInfo(long releaseId) throws Exception {
		return releaseDAO.findById(releaseId);
	}
	
	//Release Info
	public Release getReleaseFullInformation(long releaseId) throws Exception {
		List<Release> releases= releaseDAO.findByNamedQuery("getReleaseFullInformation",releaseId);
		if(releases!=null && releases.size()>0)
			return releases.get(0);
		else
			return null;
	}
	
	//Release Detail List
	public List<ReleaseDetail> getAllReleaseDetail(long releaseId) throws Exception {
		return releaseDetailDAO.findByNamedQuery("getAllReleaseDetailList",releaseId);
	}
	
	//Cancellation Info passing contract
	public Release getReleaseUseContract(Contract contract) throws Exception {
		Release release=null;
		List<Release> releases= releaseDAO.findByNamedQuery("getReleasePassingContract",contract);
		if(releases!=null && releases.size()>0){
			release=new Release();
			release=releases.get(0);
		}
		return release;
	}
	//add release 
	public void addRelease(Release release,List<ReleaseDetail> releaseDetails) throws Exception {
		releaseDAO.saveOrUpdate(release);
		if(releaseDetails!=null){
			for(ReleaseDetail rl:releaseDetails){
				rl.setRelease(release);
			}
		}
		releaseDetailDAO.saveOrUpdateAll(releaseDetails);
	}
	
	public void updateRelease(Release release) throws Exception {
		releaseDAO.saveOrUpdate(release);
	}
	
	public AIOTechGenericDAO<Release> getReleaseDAO() {
		return releaseDAO;
	}
	public void setReleaseDAO(AIOTechGenericDAO<Release> releaseDAO) {
		this.releaseDAO = releaseDAO;
	}
	public AIOTechGenericDAO<ReleaseDetail> getReleaseDetailDAO() {
		return releaseDetailDAO;
	}
	public void setReleaseDetailDAO(
			AIOTechGenericDAO<ReleaseDetail> releaseDetailDAO) {
		this.releaseDetailDAO = releaseDetailDAO;
	}
	
	public Release getReleaseWithAll (Release release){
		List<Release> releases=new ArrayList<Release>();
		releases=releaseDAO.findByNamedQuery("getReleaseListWithAll", release.getReleaseId());
		if(releases!=null && releases.size()>0)
			return releases.get(0);
		else
			return null;
	}

}
