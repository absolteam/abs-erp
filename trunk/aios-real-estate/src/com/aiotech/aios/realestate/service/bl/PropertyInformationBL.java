package com.aiotech.aios.realestate.service.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.Combination;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionDetail;
import com.aiotech.aios.accounts.service.bl.CombinationBL;
import com.aiotech.aios.accounts.service.bl.TransactionBL;
import com.aiotech.aios.common.to.Constants;
import com.aiotech.aios.common.util.AIOSCommons;
import com.aiotech.aios.common.util.DateFormat;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.service.CompanyService;
import com.aiotech.aios.hr.service.PersonService;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.ManagementDecision;
import com.aiotech.aios.realestate.domain.entity.OfferDetail;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyDetail;
import com.aiotech.aios.realestate.domain.entity.PropertyOwnership;
import com.aiotech.aios.realestate.domain.entity.PropertyType;
import com.aiotech.aios.realestate.domain.entity.TenantOffer;
import com.aiotech.aios.realestate.domain.entity.Unit;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.service.PropertyService;
import com.aiotech.aios.realestate.service.UnitService;
import com.aiotech.aios.realestate.to.RePropertyInfoTO;
import com.aiotech.aios.realestate.to.converter.RePropertyInfoTOConverter;
import com.aiotech.aios.system.bl.DirectoryBL;
import com.aiotech.aios.system.bl.DocumentBL;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Directory;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.ImageService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;
import com.aiotech.aios.workflow.domain.entity.User;
import com.aiotech.aios.workflow.domain.vo.WorkflowDetailVO;
import com.aiotech.aios.workflow.enterprise.WorkflowEnterpriseService;
import com.aiotech.aios.workflow.util.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

public class PropertyInformationBL {

	private PropertyService propertyService;
	private ComponentService componentService;
	private UnitService unitService;
	private DocumentService documentService;
	private PersonService personService;
	private CompanyService companyService;
	private ImageService imageService;
	private DocumentBL documentBL;
	private ImageBL imageBL;
	private DirectoryBL directoryBL;
	private TransactionBL transactionBL;
	private LegalBL legalBL;
	private ManagementDecisionBL managementDecisionBL;
	private CombinationBL combinationBL;
	private Implementation implementation;
	private LookupMasterBL lookupMasterBL;
	private WorkflowEnterpriseService workflowEnterpriseService;

	public DocumentBL getDocumentBL() {
		return documentBL;
	}

	public void setDocumentBL(DocumentBL documentBL) {
		this.documentBL = documentBL;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public DirectoryBL getDirectoryBL() {
		return directoryBL;
	}

	public void setDirectoryBL(DirectoryBL directoryBL) {
		this.directoryBL = directoryBL;
	}

	public ImageService getImageService() {
		return imageService;
	}

	public void setImageService(ImageService imageService) {
		this.imageService = imageService;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public CompanyService getCompanyService() {
		return companyService;
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public PropertyService getPropertyService() {
		return propertyService;
	}

	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	public void checkFeatureDeleteList(List<PropertyDetail> featureSessionList,
			Long buildingId) throws Exception {
		List<PropertyDetail> detailList = this.getPropertyService()
				.getPropertyDetailsById(buildingId);
		for (PropertyDetail lists1 : detailList) {
			for (PropertyDetail list2 : featureSessionList) {
				if (!lists1.equals(list2)) {
					list2.setPropertyDetailId(null);
				}
			}
		}
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, PropertyDetail> hashDetail = new HashMap<Long, PropertyDetail>();
		if (null != detailList && detailList.size() > 0) {
			for (PropertyDetail list : detailList) {
				listOne.add(list.getPropertyDetailId());
				hashDetail.put(list.getPropertyDetailId(), list);
			}
			if (null != hashDetail && hashDetail.size() > 0) {
				for (PropertyDetail list : featureSessionList) {
					listTwo.add(list.getPropertyDetailId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		if (null != different && different.size() > 0) {
			PropertyDetail tempDetail = null;
			List<PropertyDetail> tempDetailList = new ArrayList<PropertyDetail>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempDetail = new PropertyDetail();
					tempDetail = hashDetail.get(list);
					tempDetailList.add(tempDetail);
				}
			}
			this.getPropertyService().deletePropertyDetailList(tempDetailList);
		}
	}

	public void checkOwnersDeleteList(List<PropertyOwnership> ownersList,
			Long buildingId) throws Exception {
		List<PropertyOwnership> ownerList = this.getPropertyService()
				.getPropertyOwnersByPropertyId(buildingId);
		Collection<Long> listOne = new ArrayList<Long>();
		Collection<Long> listTwo = new ArrayList<Long>();
		Map<Long, PropertyOwnership> hashOwner = new HashMap<Long, PropertyOwnership>();
		if (null != ownerList && ownerList.size() > 0) {
			for (PropertyOwnership list : ownerList) {
				listOne.add(list.getPropertyOwnershipId());
				hashOwner.put(list.getPropertyOwnershipId(), list);
			}
			if (null != hashOwner && hashOwner.size() > 0) {
				for (PropertyOwnership list : ownersList) {
					listTwo.add(list.getPropertyOwnershipId());
				}
			}
		}
		Collection<Long> similar = new HashSet<Long>(listOne);
		Collection<Long> different = new HashSet<Long>();
		different.addAll(listOne);
		different.addAll(listTwo);
		similar.retainAll(listTwo);
		different.removeAll(similar);
		if (null != different && different.size() > 0) {
			PropertyOwnership tempOwner = null;
			List<PropertyOwnership> tempOwnerList = new ArrayList<PropertyOwnership>();
			for (Long list : different) {
				if (null != list && !list.equals(0)) {
					tempOwner = new PropertyOwnership();
					tempOwner = hashOwner.get(list);
					tempOwnerList.add(tempOwner);
				}
			}
			this.getPropertyService().deleteOwnershipList(tempOwnerList);
		}
	}

	public boolean deleteProperty(long propertyId, int propertyTypeId)
			throws Exception {
		boolean flag = false;
		if (propertyTypeId == Constants.RealEstate.PropertyType.Villa.getCode()
				|| propertyTypeId == Constants.RealEstate.PropertyType.Flat
						.getCode()
				|| propertyTypeId == Constants.RealEstate.PropertyType.Shop
						.getCode()) {

			try {
				Component component = componentService
						.getComponentsForProperty(propertyId).get(0);
				Unit unit = unitService.getUnitListComponentBased(
						component.getComponentId()).get(0);
				unitService.deleteUnits(unit,
						unitService.getUnitFeatureDetails(unit.getUnitId()),
						unitService.getAssetDetails(unit.getUnitId()));
				componentService.deletePropertyComponent(component,
						componentService.getComponentLines(component
								.getComponentId()));

				imageService.deleteImages(imageService.getImagesByRecordId(
						propertyId, "Property"));
				documentService.deleteDocuments(documentService
						.getChildDocumentsByRecordId(propertyId, "Property"));
				documentService.deleteDocuments(documentService
						.getParentDocumentsByRecordId(propertyId, "Property"));
				propertyService.deletePropertyById(propertyId);
				ServletActionContext.getRequest().setAttribute(
						"PROPERTY_CHILD_EXISTS", 0);
				flag = true;
			} catch (Exception e) {
				e.printStackTrace();
				ServletActionContext.getRequest().setAttribute(
						"PROPERTY_CHILD_EXISTS", 2);
			}
		} else if (componentService.getComponentsForProperty(propertyId).size() != 0) {
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_CHILD_EXISTS", 1);
		}
		try {
			imageService.deleteImages(imageService.getImagesByRecordId(
					propertyId, "Property"));
			documentService.deleteDocuments(documentService
					.getChildDocumentsByRecordId(propertyId, "Property"));
			documentService.deleteDocuments(documentService
					.getParentDocumentsByRecordId(propertyId, "Property"));
			propertyService.deletePropertyById(propertyId);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			ServletActionContext.getRequest().setAttribute(
					"PROPERTY_CHILD_EXISTS", 2);
		}
		return flag;

	}

	public void addProperty(Property property, Transaction transaction,
			ManagementDecision decision, Boolean editFlag) throws Exception {
		if (!editFlag)
			transaction = accountIntegration(property);

		propertyService.addProperty(property);
		if (property.getRelativeId() == null) {
			property.setRelativeId(property.getPropertyId());
			propertyService.addProperty(property);
		}

		if (transaction.getCurrency() != null) {
			List<TransactionDetail> transactionList = new ArrayList<TransactionDetail>(
					transaction.getTransactionDetails());
			transactionBL.saveTransaction(transaction, transactionList);
		}
		try {
			savePropertyDocuments(property, editFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			saveUploadedImages(property, "uploadedImages", editFlag);
			// Save Uploaded Images for Property Features
			for (PropertyDetail pd : property.getPropertyDetails())
				saveUploadedImages(pd, "uploadedFeatureImages", editFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (decision != null && !decision.equals("")) {
				managementDecisionBL.addManagementDecision(property, decision);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> sessionObj = ActionContext.getContext()
					.getSession();
			User user = (User) sessionObj.get("USER");
			WorkflowDetailVO workflowDetailVo = new WorkflowDetailVO();
			workflowDetailVo
					.setProcessType((byte) WorkflowConstants.ProcessMethod.Add
							.getCode());
			workflowEnterpriseService.generateNotificationsAgainstDataEntry(
					"Property", property.getPropertyId(), user,
					workflowDetailVo);

			WorkflowDetailVO vo = null;
			if (sessionObj.containsKey("WORKFLOW_DETAILVO")) {
				vo = (WorkflowDetailVO) sessionObj.get("WORKFLOW_DETAILVO");

				sessionObj.remove("WORKFLOW_DETAILVO");
				workflowEnterpriseService.updateNotificationStatus(vo,
						vo.getRecordId(), (byte) 1, user);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Transaction accountIntegration(Property propertyToAdd)
			throws Exception {
		HttpSession session = ServletActionContext.getRequest().getSession();
		Map<String, Object> sessionObj = ActionContext.getContext()
				.getSession();
		User user = (User) sessionObj.get("USER");
		Transaction transaction = null;
		List<PropertyType> propertyTypes = propertyService
				.getAllPropertyTypes();
		for (PropertyType propertyType : propertyTypes) {
			if (propertyToAdd.getPropertyType().getPropertyTypeId()
					.equals(propertyType.getPropertyTypeId())) {
				propertyToAdd.setPropertyType(propertyType);
			}

		}
		if (!(propertyToAdd.getPropertyType().getType())
				.equalsIgnoreCase("empty land")) {
			// Create analysis account(Revenue)

			Person person = new Person();
			person.setPersonId(user.getPersonId());

			propertyToAdd.setPerson(person);

			propertyToAdd.setCreatedDate(new Date());

			final Integer ASSET_ACCOUNT = 1;
			final Integer REVENUE_ACCOUNT = 3;
			final Integer EXPENSE_ACCOUNT = 4;

			Combination costCombination = combinationBL.createCostCenter(
					propertyToAdd.getPropertyName() + " ("
							+ propertyToAdd.getPropertyType().getType() + ")",
					true); // COST CENTER
			Combination assetCombination = combinationBL.createNaturalAccount(
					costCombination, propertyToAdd.getPropertyName(),
					ASSET_ACCOUNT, true); // NATURAL ACCOUNT(ASSET)

			Combination revenueCombination = combinationBL
					.createNaturalAccount(costCombination, "Rent Income "
							+ propertyToAdd.getPropertyName(), REVENUE_ACCOUNT,
							true); // NATURAL ACCOUNT(REVENUE)

			Combination expenseCombination = combinationBL
					.createNaturalAccount(costCombination, "Expense "
							+ propertyToAdd.getPropertyName(), EXPENSE_ACCOUNT,
							true); // NATURAL ACCOUNT(EXPENSE)

			Combination revenueAnalysis = new Combination();
			revenueAnalysis.setCombinationId(revenueCombination
					.getCombinationId());
			revenueAnalysis = combinationBL.createAnalysisCombination(
					revenueAnalysis,
					"Other Income " + propertyToAdd.getPropertyName(), true); // OTHER
			// INCOME(UNDER
			// REVENUE)

			Combination expenseAnalysisMaintainance = new Combination();
			expenseAnalysisMaintainance.setCombinationId(expenseCombination
					.getCombinationId());
			expenseAnalysisMaintainance = combinationBL
					.createAnalysisCombination(expenseAnalysisMaintainance,
							"Maintainance " + propertyToAdd.getPropertyName(),
							true); // MAINTAINANCE(UNDER
			// EXPENSE)

			Combination expenseAnalysisUtility = new Combination();
			expenseAnalysisUtility.setCombinationId(expenseCombination
					.getCombinationId());
			expenseAnalysisUtility = combinationBL.createAnalysisCombination(
					expenseAnalysisUtility,
					"Utility " + propertyToAdd.getPropertyName(), true); // UTILITY(UNDER
			// EXPENSE)

			Combination expenseAnalysisOther = new Combination();
			expenseAnalysisOther.setCombinationId(expenseCombination
					.getCombinationId());
			expenseAnalysisOther = combinationBL.createAnalysisCombination(
					expenseAnalysisOther,
					"Others " + propertyToAdd.getPropertyName(), true); // OTHER(UNDER
			// EXPENSE)

			transaction = createTransaction(getImplementation(),
					propertyToAdd.getCost() + "",
					propertyToAdd.getPropertyName(), propertyToAdd);

			TransactionDetail transactionDetail = new TransactionDetail();
			transactionDetail = createTransactionDetail(propertyToAdd.getCost()
					+ "", assetCombination, true,
					propertyToAdd.getPropertyName() + " Debited");
			Set<TransactionDetail> transactionList = new HashSet<TransactionDetail>();
			transactionList.add(transactionDetail);

			// CREDIT SIDE
			/*
			 * final long ACCOUNT_TYPE_ID = 5; Combination ownersEquity = this
			 * .getPropertyInfoBL().getTransactionBL().getJournalService(
			 * ).getFilterCombination( ACCOUNT_TYPE_ID, getImplementation()); //
			 * OWNERS EQUITY
			 */
			Combination ownersEquity = this
					.getTransactionBL()
					.getCombinationService()
					.getCombinationAccountDetail(
							this.getImplementation().getOwnersEquity());
			transactionDetail = createTransactionDetail(propertyToAdd.getCost()
					+ "", ownersEquity, false, ownersEquity
					.getAccountByNaturalAccountId().getAccount() + " Credited");
			transactionList.add(transactionDetail);
			transaction.setTransactionDetails(transactionList);
		}
		return transaction;
	}

	public void savePropertyDocuments(Property property, boolean editFlag)
			throws Exception {
		DocumentVO docVO = documentBL.populateDocumentVO(property,
				"uploadedDocs", editFlag);
		Map<String, String> dirs = docVO.getDirMap();

		if (dirs != null) {
			Map<String, Directory> dirStucture = directoryBL.saveDirStructure(
					dirs, property.getPropertyId(), "Property", "uploadedDocs");
			docVO.setDirStruct(dirStucture);
		}

		documentBL.saveUploadDocuments(docVO);
	}

	public void saveUploadedImages(Object object, String displayPane,
			Boolean editFlag) throws Exception {
		DocumentVO docVO = new DocumentVO();
		docVO.setDisplayPane(displayPane);
		docVO.setEdit(editFlag);
		docVO.setObject(object);
		imageBL.saveUploadedImages(object, docVO);
	}

	public TransactionDetail createTransactionDetail(String marketValue,
			Combination combination, Boolean flag, String description)
			throws Exception {
		TransactionDetail transactionDetail = this
				.getTransactionBL()
				.createTransactionDetail(
						Double.parseDouble(marketValue.replaceAll(",", "")),
						combination.getCombinationId(), flag, description, null,null);
		return transactionDetail;
	}

	public Transaction createTransaction(Implementation implementation,
			String marketValue, String buildingName, Property property)
			throws Exception {
		List<Category> categoryList = this.getTransactionBL().getCategoryBL()
				.getCategoryService().getAllActiveCategories(implementation);
		Category category = new Category();
		if (null != categoryList) {
			for (Category list : categoryList) {
				if (list.getName().equalsIgnoreCase("Asset Properties")) {
					category.setCategoryId(list.getCategoryId());
					break;
				}
			}
		}
		if (null == category || category.equals("")) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(implementation, "Asset Properties");
		} else if (null == category.getCategoryId()) {
			category = this.getTransactionBL().getCategoryBL()
					.createCategory(implementation, "Asset Properties");
		}
		Transaction transaction = this.getTransactionBL().createTransaction(
				this.getTransactionBL().getCurrency().getCurrencyId(),
				"Real Estate Building( " + buildingName + " ) Transaction.",
				new Date(), category, property.getPropertyId(),
				Property.class.getSimpleName());
		return transaction;
	}

	// Property Owner Detail process
	public List<RePropertyInfoTO> propertyOwnerDetailInfo(
			List<Property> propertys) throws Exception {
		List<RePropertyInfoTO> propInfoList = new ArrayList<RePropertyInfoTO>();
		for (Property property : propertys) {
			List<PropertyOwnership> proponwners = propertyService
					.getPropertyOwners(property);
			RePropertyInfoTO propertyInfo = new RePropertyInfoTO();
			String owners = "";
			String ownersArabic = "";
			for (PropertyOwnership propertyOwnership : proponwners) {
				if (propertyOwnership.getDetails().trim()
						.equalsIgnoreCase("PERSON")) {
					Person person = personService
							.getPersonDetails(propertyOwnership.getPersonalId());
					if (person != null) {

						owners = owners + person.getFirstName() + " "
								+ person.getMiddleName() + " "
								+ person.getLastName() + ", ";
						ownersArabic = ownersArabic
								+ AIOSCommons.bytesToUTFString(person
										.getFirstNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(person
										.getMiddleNameArabic())
								+ " "
								+ AIOSCommons.bytesToUTFString(person
										.getLastNameArabic()) + ", ";
					}
				} else if (propertyOwnership.getDetails().trim()
						.equalsIgnoreCase("COMPANY")) {
					Company company = companyService
							.getCompanyById(propertyOwnership.getPersonalId());
					if (company != null) {

						owners = owners + company.getCompanyName() + ", ";
						ownersArabic = ownersArabic
								+ AIOSCommons.bytesToUTFString(company
										.getCompanyNameArabic()) + ", ";
					}
				}
			}
			propertyInfo = RePropertyInfoTOConverter.convertToTO(property);

			// Process For legal information
			propertyInfo.setLegallist(legalBL.getLegals(property));
			propertyInfo.setManagementDecisions(managementDecisionBL
					.getManagementDecision(property));

			propertyInfo.setOwners(AIOSCommons.removeTrailingSymbols(owners));
			propertyInfo.setOwnersArabic(AIOSCommons
					.removeTrailingSymbols(ownersArabic));
			propInfoList.add(propertyInfo);
		}
		return propInfoList;
	}

	public List<RePropertyInfoTO> propertySizeReportInfo(
			Implementation implementation, Byte status) throws Exception {
		List<RePropertyInfoTO> propInfoList = new ArrayList<RePropertyInfoTO>();
		List<Property> propertys = null;
		RePropertyInfoTO propertyTo = null;
		if (status != -1)
			propertys = propertyService.getAllPropertiesSizeReport(
					implementation, status);
		else
			propertys = propertyService.getAllPropertiesReport(implementation);

		int i = 0;
		long propertyid = 0;
		for (Property property : propertys) {
			propertyTo = new RePropertyInfoTO();
			if (propertyid == 0 || property.getPropertyId() != propertyid) {
				i = 0;
			}
			propertyid = property.getPropertyId();

			int j = 0;
			propertyTo.setPropertyId(property.getPropertyId());
			propertyTo.setPropertyTypeId(property.getPropertyType()
					.getPropertyTypeId());
			propertyTo
					.setPropertyTypeName(property.getPropertyType().getType());
			propertyTo.setBuildingName(property.getPropertyName());
			propertyTo.setLandSize(property.getSize()
					+ " "
					+ RePropertyInfoTOConverter.measurment(property
							.getSizeUnit()));
			propertyTo.setBuildingSize(property.getBuildingSize()
					+ " "
					+ RePropertyInfoTOConverter.measurment(property
							.getBuildingSizeUnit()));
			propertyTo.setAddress(property.getAddressOne() + ","
					+ property.getAddressTwo());

			if (property.getComponents() != null)
				propertyTo.setComponentCount(property.getComponents().size()
						+ "");
			for (Component component : property.getComponents()) {
				if (i == j) {
					propertyTo.setComponentName(component.getComponentName());
					propertyTo.setComponentSize(component.getSize()
							+ " "
							+ RePropertyInfoTOConverter.measurment(component
									.getSizeUnit()));
				}
				j++;

			}
			propInfoList.add(propertyTo);
			i++;
		}

		return propInfoList;
	}

	public List<RePropertyInfoTO> getPropertyUnitVacancyList(long propertyId,
			Date fromDate, Date toDate) throws Exception {
		List<RePropertyInfoTO> vacancyList = new ArrayList<RePropertyInfoTO>();
		List<Unit> units = unitService.getPropertyUnitVacancy(propertyId,
				fromDate, toDate);
		if (units != null && units.size() > 0) {
			for (Unit unit : units) {
				RePropertyInfoTO infoTo = new RePropertyInfoTO();
				infoTo.setPropertyId(unit.getUnitId());
				infoTo.setUnitName(unit.getUnitName());
				infoTo.setComponentName(unit.getComponent().getComponentName());
				infoTo.setPropertyName(unit.getComponent().getProperty()
						.getPropertyName());
				if (unit.getStatus() != null)
					infoTo.setStatusStr(Constants.RealEstate.UnitStatus.get(
							unit.getStatus()).name());
				if (unit.getOfferDetails() != null
						&& unit.getOfferDetails().size() > 0
						&& unit.getStatus() != 1) {
					for (OfferDetail offerDetail : unit.getOfferDetails()) {
						infoTo.setFromDate(DateFormat
								.convertDateToString(offerDetail.getOffer()
										.getStartDate().toString()));
						infoTo.setToDate(DateFormat
								.convertDateToString(offerDetail.getOffer()
										.getEndDate().toString()));
						for (Contract contract : offerDetail.getOffer()
								.getContracts()) {
							infoTo.setContractNumber(contract.getContractNo());
						}
						for (TenantOffer tenantOffer : offerDetail.getOffer()
								.getTenantOffers()) {
							if (tenantOffer.getPerson() != null) {
								Person persons = this.personService
										.getPersonDetails(tenantOffer
												.getPerson().getPersonId());
								infoTo.setTenantName(persons.getFirstName()
										+ " " + persons.getMiddleName() + " "
										+ persons.getLastName());

							} else {
								Company company = this.companyService
										.getCompanyById(tenantOffer
												.getCompany().getCompanyId());
								infoTo.setTenantName(company.getCompanyName());
							}
						}
					}
				} else {
					infoTo.setFromDate("-NA-");
					infoTo.setToDate("-NA-");
					infoTo.setContractNumber("-NA-");
					infoTo.setTenantName("-NA-");
				}

				vacancyList.add(infoTo);
			}
		}
		return vacancyList;
	}

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public TransactionBL getTransactionBL() {
		return transactionBL;
	}

	public void setTransactionBL(TransactionBL transactionBL) {
		this.transactionBL = transactionBL;
	}

	public UnitService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitService unitService) {
		this.unitService = unitService;
	}

	public LegalBL getLegalBL() {
		return legalBL;
	}

	public void setLegalBL(LegalBL legalBL) {
		this.legalBL = legalBL;
	}

	public ManagementDecisionBL getManagementDecisionBL() {
		return managementDecisionBL;
	}

	public void setManagementDecisionBL(
			ManagementDecisionBL managementDecisionBL) {
		this.managementDecisionBL = managementDecisionBL;
	}

	public CombinationBL getCombinationBL() {
		return combinationBL;
	}

	public void setCombinationBL(CombinationBL combinationBL) {
		this.combinationBL = combinationBL;
	}

	public Implementation getImplementation() {
		return (Implementation) (ServletActionContext.getRequest().getSession()
				.getAttribute("THIS"));
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}

	public WorkflowEnterpriseService getWorkflowEnterpriseService() {
		return workflowEnterpriseService;
	}

	public void setWorkflowEnterpriseService(
			WorkflowEnterpriseService workflowEnterpriseService) {
		this.workflowEnterpriseService = workflowEnterpriseService;
	}

}
