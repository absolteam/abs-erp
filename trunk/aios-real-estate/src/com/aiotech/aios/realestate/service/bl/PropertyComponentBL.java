package com.aiotech.aios.realestate.service.bl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.aiotech.aios.common.to.FileVO;
import com.aiotech.aios.common.util.FileUtil;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.ComponentDetail;
import com.aiotech.aios.realestate.domain.entity.ComponentType;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.service.ComponentService;
import com.aiotech.aios.realestate.to.PropertyCompomentTO;
import com.aiotech.aios.system.bl.ImageBL;
import com.aiotech.aios.system.domain.entity.Image;
import com.aiotech.aios.system.service.DocumentService;
import com.aiotech.aios.system.service.bl.LookupMasterBL;
import com.aiotech.aios.system.to.DocumentVO;

public class PropertyComponentBL {

	private DocumentService documentService;
	private ImageBL imageBL;
	private LookupMasterBL lookupMasterBL;

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public void saveUploadedImages(Object object,boolean editFlag) throws Exception {
		DocumentVO docVO = new DocumentVO();
		docVO.setDisplayPane("uploadedImagesComponent");
		docVO.setEdit(editFlag);
		docVO.setObject(object);
		imageBL.saveUploadedImages(object, docVO);
	}

	private Image populateImage(Map.Entry<String, FileVO> entry,
			Object object) throws Exception {
		Image image = new Image();
		image.setName(entry.getKey());
		image.setHashedName(entry.getValue().getHashedName());
		String[] name = object.getClass().getName().split("entity.");
		image.setTableName(name[name.length-1]);
		
		String methodName = "get" +name[name.length-1]+"Id";
		Method method = object.getClass().getMethod(methodName, null);
		image.setRecordId((Long)method.invoke(object, null));

		return image;
	}
	
	/*public PropertyCompomentTO getComponentDetails(Component comp) throws Exception{
 		Component component=componentService.getComponentById(Long.parseLong(comp.getComponentId()+"")); 
		Property property=componentService.getPropertyById(component.getProperty().getPropertyId());
		ComponentType componentType=componentService.getComponentTypeById(component.getComponentType().getComponentTypeId());
		component.setProperty(property);
		component.setComponentType(componentType);
		List<ComponentDetail> componentDetailList = componentService.getComponentLines(Long.parseLong(""+comp.getComponentId()));
		
		component.setComponentDetails(new HashSet<ComponentDetail>(componentDetailList));
		PropertyCompomentTO to = new PropertyCompomentTO();
		to.setComponent(component);
		to.setComponentDetails(componentDetailList);
		
		return to;
	} */
	private ComponentService componentService;

	public ComponentService getComponentService() {
		return componentService;
	}

	public void setComponentService(ComponentService componentService) {
		this.componentService = componentService;
	}

	public ImageBL getImageBL() {
		return imageBL;
	}

	public void setImageBL(ImageBL imageBL) {
		this.imageBL = imageBL;
	}

	public LookupMasterBL getLookupMasterBL() {
		return lookupMasterBL;
	}

	public void setLookupMasterBL(LookupMasterBL lookupMasterBL) {
		this.lookupMasterBL = lookupMasterBL;
	}
}
