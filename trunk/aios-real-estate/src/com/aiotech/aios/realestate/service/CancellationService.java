package com.aiotech.aios.realestate.service;

import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.utils.spring.dao.AIOTechGenericDAO;

public class CancellationService {
	private AIOTechGenericDAO<Cancellation> cancellationDAO;
	
	
	//Cancellation List
	public List<Cancellation> getAllCancellation(Implementation implementation) throws Exception {
		return cancellationDAO.findByNamedQuery("getAllCancellation",implementation);
	}
	
	//get Contract Detail using cancellation id
	public Cancellation getCancellationInfo(long cancellationId)
			throws Exception {
		return cancellationDAO.findById(cancellationId);
	}

	public Cancellation getCancellationByContract(long contactId)
			throws Exception {
		return cancellationDAO.findByNamedQuery("getCancellationByContract",
				contactId).get(0);
	}
	
	//Cancellation Info passing contract
	public Cancellation getCancellationUseContract(Contract contract) throws Exception {
		Cancellation cancel=null;
		List<Cancellation> cancels= cancellationDAO.findByNamedQuery("getCancellationPassingContract",contract);
		if(cancels!=null && cancels.size()>0){
			cancel=new Cancellation();
			cancel=cancels.get(0);
		}
		return cancel;
	}
	public void cancelSave(Cancellation cancel) throws Exception {
		cancellationDAO.saveOrUpdate(cancel);
	}

	public AIOTechGenericDAO<Cancellation> getCancellationDAO() {
		return cancellationDAO;
	}

	public void setCancellationDAO(AIOTechGenericDAO<Cancellation> cancellationDAO) {
		this.cancellationDAO = cancellationDAO;
	}
	
}
