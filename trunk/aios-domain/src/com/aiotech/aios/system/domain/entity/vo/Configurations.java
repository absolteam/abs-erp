package com.aiotech.aios.system.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "configurations")
public class Configurations {
	 	@XmlElement(name = "configuration", type = Configuration.class)
	    private List<Configuration> configurations = new ArrayList<Configuration>();
	 
	    public Configurations() {}
	 
	    public Configurations(List<Configuration> configurations) {
	        this.configurations = configurations;
	    }
	 
	    public List<Configuration> getConfigurations() {
	        return configurations;
	    }
	 
	    public void setConfigurations(List<Configuration> configurations) {
	        this.configurations = configurations;
	    }
	   
}
