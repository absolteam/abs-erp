package com.aiotech.aios.system.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.workflow.domain.entity.User;

public class UserVO extends User {

	private static final long serialVersionUID = 1L;
	private String displayUsername;
	
	private String personName;
	private String designation;

	public String getDisplayUsername() {
		return displayUsername;
	}

	public void setDisplayUsername(String displayUsername) {
		this.displayUsername = displayUsername;
	}
	
	public static List<UserVO> convertToUserVO(List<User> users) {
		
		UserVO userVO = new UserVO();
		List<UserVO> vos = new ArrayList<UserVO>();
		
		for(User user : users) {
			
			userVO = new UserVO();
			userVO.setUsername(user.getUsername());
			userVO.setPassword(user.getPassword());
			userVO.setEnabled(user.getEnabled());
			//userVO.setImplementation(user.getImplementation());
			userVO.setUserRoles(user.getUserRoles());
			userVO.setDisplayUsername(user.getUsername().split("_")[0]);
			
			vos.add(userVO);
		}
		
		return vos;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
}
