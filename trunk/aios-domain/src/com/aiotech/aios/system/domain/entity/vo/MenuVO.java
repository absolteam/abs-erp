package com.aiotech.aios.system.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.workflow.domain.entity.Menu;
import com.aiotech.aios.workflow.domain.entity.MenuRight;
import com.aiotech.aios.workflow.domain.entity.Screen;

public class MenuVO implements Comparable<MenuVO>, java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Long menuItemId;
	private Screen screen;
	private MenuVO menu;
	private String itemTitle;
	private String itemTitleAr;
	private Byte itemOrder;
	private Boolean isReport;
	private String logo;
	private Boolean isQuickLink;
	private List<MenuRight> menuRights = new ArrayList<MenuRight>();
	private List<MenuVO> menus = new ArrayList<MenuVO>();

	static public MenuVO convertTOMenuVO(Menu menu) {

		MenuVO vo = new MenuVO();
		vo.setItemOrder(menu.getItemOrder());
		vo.setItemTitle(menu.getItemTitle());
		vo.setItemTitleAr(menu.getItemTitleAr());
		vo.setMenuItemId(menu.getMenuItemId());
		vo.setScreen(menu.getScreen());
		vo.setIsReport(menu.getIsReport());
		vo.setLogo(menu.getLogo());
		vo.setIsQuickLink(menu.getIsQuickLink());
		return vo;
	}

	public Long getMenuItemId() {
		return menuItemId;
	}

	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public MenuVO getMenu() {
		return menu;
	}

	public void setMenu(MenuVO menu) {
		this.menu = menu;
	}

	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	public Byte getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(Byte itemOrder) {
		this.itemOrder = itemOrder;
	}

	public List<MenuRight> getMenuRights() {
		return menuRights;
	}

	public void setMenuRights(List<MenuRight> menuRights) {
		this.menuRights = menuRights;
	}

	public List<MenuVO> getMenus() {
		return menus;
	}

	public void setMenus(List<MenuVO> menus) {
		this.menus = menus;
	}

	public Boolean getIsReport() {
		return isReport;
	}

	public void setIsReport(Boolean isReport) {
		this.isReport = isReport;
	}

	public String getItemTitleAr() {
		return itemTitleAr;
	}

	public void setItemTitleAr(String itemTitleAr) {
		this.itemTitleAr = itemTitleAr;
	}

	@Override
	public int compareTo(MenuVO o) {

		return this.itemOrder - o.itemOrder;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Boolean getIsQuickLink() {
		return isQuickLink;
	}

	public void setIsQuickLink(Boolean isQuickLink) {
		this.isQuickLink = isQuickLink;
	}
}
