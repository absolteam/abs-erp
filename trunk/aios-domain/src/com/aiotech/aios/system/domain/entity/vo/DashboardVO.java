package com.aiotech.aios.system.domain.entity.vo;

import java.util.Date;
import java.util.List;


public class DashboardVO {
	private String name;
	private String ticks;
	private String value;
	private Object object;
	private Double amount;
	private Long quantity;
	private String startDateView;
	private String endDateView;
	private Date startDate;
	private Date endDate;
	private List<DashboardVO> dashboardVOs;
	private Integer count;
	private Long id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTicks() {
		return ticks;
	}
	public void setTicks(String ticks) {
		this.ticks = ticks;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public String getStartDateView() {
		return startDateView;
	}
	public void setStartDateView(String startDateView) {
		this.startDateView = startDateView;
	}
	public String getEndDateView() {
		return endDateView;
	}
	public void setEndDateView(String endDateView) {
		this.endDateView = endDateView;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<DashboardVO> getDashboardVOs() {
		return dashboardVOs;
	}
	public void setDashboardVOs(List<DashboardVO> dashboardVOs) {
		this.dashboardVOs = dashboardVOs;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
}
