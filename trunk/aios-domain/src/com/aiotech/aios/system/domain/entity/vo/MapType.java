package com.aiotech.aios.system.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "map")
public class MapType {

	@XmlElement(name = "entry", type = MapEntryType.class)
    private List<MapEntryType> entry = new ArrayList<MapEntryType>();
	
	private String id;
	
	 public MapType() {}
	 
    public MapType(List<MapEntryType> entry) {
        this.entry = entry;
    }

	public List<MapEntryType> getEntry() {
		return entry;
	}

	public void setEntry(List<MapEntryType> entry) {
		this.entry = entry;
	}

   

}
