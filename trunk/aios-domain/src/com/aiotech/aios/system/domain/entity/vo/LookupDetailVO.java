package com.aiotech.aios.system.domain.entity.vo;

import com.aiotech.aios.system.domain.entity.LookupDetail;

public class LookupDetailVO extends LookupDetail{
	private String arabicDisplayName;

	public String getArabicDisplayName() {
		return arabicDisplayName;
	}

	public void setArabicDisplayName(String arabicDisplayName) {
		this.arabicDisplayName = arabicDisplayName;
	}
}
