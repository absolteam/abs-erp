package com.aiotech.aios.system.domain.entity.vo;

import com.aiotech.aios.workflow.domain.entity.AlertRole;

public class AlertRoleVO extends AlertRole{

	private String screenName;
	private String personName;
	private String roleName;
	private String statusView;
	private Long roleId;
	private Long moduleProcessId;
	private String createdDateView;
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getStatusView() {
		return statusView;
	}
	public void setStatusView(String statusView) {
		this.statusView = statusView;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getModuleProcessId() {
		return moduleProcessId;
	}
	public void setModuleProcessId(Long moduleProcessId) {
		this.moduleProcessId = moduleProcessId;
	}
	public String getCreatedDateView() {
		return createdDateView;
	}
	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}
}
