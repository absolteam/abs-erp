package com.aiotech.aios.system.domain.entity;

// Generated Aug 2, 2015 10:50:56 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.accounts.domain.entity.Account;
import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AssetClaimInsurance;
import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;
import com.aiotech.aios.accounts.domain.entity.Bank;
import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankReceipts;
import com.aiotech.aios.accounts.domain.entity.BankReconciliation;
import com.aiotech.aios.accounts.domain.entity.BankTransfer;
import com.aiotech.aios.accounts.domain.entity.Calendar;
import com.aiotech.aios.accounts.domain.entity.Category;
import com.aiotech.aios.accounts.domain.entity.CommissionRule;
import com.aiotech.aios.accounts.domain.entity.Coupon;
import com.aiotech.aios.accounts.domain.entity.Credit;
import com.aiotech.aios.accounts.domain.entity.CreditTerm;
import com.aiotech.aios.accounts.domain.entity.Currency;
import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;
import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;
import com.aiotech.aios.accounts.domain.entity.Debit;
import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.Discount;
import com.aiotech.aios.accounts.domain.entity.EODBalancing;
import com.aiotech.aios.accounts.domain.entity.Eibor;
import com.aiotech.aios.accounts.domain.entity.GoodsReturn;
import com.aiotech.aios.accounts.domain.entity.Invoice;
import com.aiotech.aios.accounts.domain.entity.IssueRequistion;
import com.aiotech.aios.accounts.domain.entity.IssueReturn;
import com.aiotech.aios.accounts.domain.entity.Loan;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;
import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;
import com.aiotech.aios.accounts.domain.entity.MaterialWastage;
import com.aiotech.aios.accounts.domain.entity.MemberCard;
import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;
import com.aiotech.aios.accounts.domain.entity.POSUserTill;
import com.aiotech.aios.accounts.domain.entity.Payment;
import com.aiotech.aios.accounts.domain.entity.PaymentRequest;
import com.aiotech.aios.accounts.domain.entity.PettyCash;
import com.aiotech.aios.accounts.domain.entity.PointOfSale;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.accounts.domain.entity.ProductCategory;
import com.aiotech.aios.accounts.domain.entity.ProductDefinition;
import com.aiotech.aios.accounts.domain.entity.ProductPackage;
import com.aiotech.aios.accounts.domain.entity.ProductPricing;
import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductionReceive;
import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;
import com.aiotech.aios.accounts.domain.entity.Promotion;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.accounts.domain.entity.Quotation;
import com.aiotech.aios.accounts.domain.entity.Receive;
import com.aiotech.aios.accounts.domain.entity.ReconciliationAdjustment;
import com.aiotech.aios.accounts.domain.entity.Requisition;
import com.aiotech.aios.accounts.domain.entity.RewardPolicy;
import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;
import com.aiotech.aios.accounts.domain.entity.SalesInvoice;
import com.aiotech.aios.accounts.domain.entity.SalesOrder;
import com.aiotech.aios.accounts.domain.entity.Stock;
import com.aiotech.aios.accounts.domain.entity.StockPeriodic;
import com.aiotech.aios.accounts.domain.entity.Store;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.accounts.domain.entity.Tender;
import com.aiotech.aios.accounts.domain.entity.Transaction;
import com.aiotech.aios.accounts.domain.entity.TransactionTemp;
import com.aiotech.aios.accounts.domain.entity.VoidPayment;
import com.aiotech.aios.accounts.domain.entity.WorkinProcess;
import com.aiotech.aios.hr.domain.entity.AcademicQualifications;
import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;
import com.aiotech.aios.hr.domain.entity.Allowance;
import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.hr.domain.entity.Attendance;
import com.aiotech.aios.hr.domain.entity.AttendancePolicy;
import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Contacts;
import com.aiotech.aios.hr.domain.entity.Department;
import com.aiotech.aios.hr.domain.entity.DependentAllowance;
import com.aiotech.aios.hr.domain.entity.Designation;
import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;
import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.FuelAllowance;
import com.aiotech.aios.hr.domain.entity.Grade;
import com.aiotech.aios.hr.domain.entity.HouseRentAllowance;
import com.aiotech.aios.hr.domain.entity.IdentityAvailability;
import com.aiotech.aios.hr.domain.entity.IdentityAvailabilityMaster;
import com.aiotech.aios.hr.domain.entity.InsuranceCompany;
import com.aiotech.aios.hr.domain.entity.Interview;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.hr.domain.entity.Job;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.Leave;
import com.aiotech.aios.hr.domain.entity.LeavePolicy;
import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;
import com.aiotech.aios.hr.domain.entity.Location;
import com.aiotech.aios.hr.domain.entity.MedicalAllowance;
import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.MileageLog;
import com.aiotech.aios.hr.domain.entity.OpenPosition;
import com.aiotech.aios.hr.domain.entity.OtRecoveryMethod;
import com.aiotech.aios.hr.domain.entity.ParkingAllowance;
import com.aiotech.aios.hr.domain.entity.PayByCount;
import com.aiotech.aios.hr.domain.entity.PayPeriod;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.hr.domain.entity.PersonBank;
import com.aiotech.aios.hr.domain.entity.PhoneAllowance;
import com.aiotech.aios.hr.domain.entity.PromotionDemotion;
import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.RecruitmentSource;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.hr.domain.entity.Skills;
import com.aiotech.aios.hr.domain.entity.SwipeInOut;
import com.aiotech.aios.hr.domain.entity.TicketAllowance;
import com.aiotech.aios.hr.domain.entity.WorkingShift;
import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectDiscussion;
import com.aiotech.aios.project.domain.entity.ProjectExpense;
import com.aiotech.aios.project.domain.entity.ProjectInventory;
import com.aiotech.aios.project.domain.entity.ProjectMileStone;
import com.aiotech.aios.project.domain.entity.ProjectNotes;
import com.aiotech.aios.project.domain.entity.ProjectPaymentDetail;
import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;
import com.aiotech.aios.project.domain.entity.ProjectResource;
import com.aiotech.aios.project.domain.entity.ProjectTask;
import com.aiotech.aios.realestate.domain.entity.Cancellation;
import com.aiotech.aios.realestate.domain.entity.Component;
import com.aiotech.aios.realestate.domain.entity.Contract;
import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.Property;
import com.aiotech.aios.realestate.domain.entity.PropertyExpense;
import com.aiotech.aios.realestate.domain.entity.Release;
import com.aiotech.aios.realestate.domain.entity.TenantGroup;
import com.aiotech.aios.realestate.domain.entity.Unit;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Implementation generated by hbm2java
 */
@Entity
@Table(name = "sys_implementation", catalog = "aios")
public class Implementation implements java.io.Serializable {

	private Long implementationId;
	private String companyName;
	private String companyKey;
	private String mainLogo;
	private String loginLogo;
	private String headerLogo;
	private String footerLogo;
	private String loginBack1;
	private String loginBack2;
	private String mainBack1;
	private String mainBack2;
	private Boolean isWorkflow;
	private Long receivableClerk;
	private Long unearnedRevenue;
	private Long accountPayable;
	private Long pdcReceived;
	private Long pdcIssued;
	private Long clearingAccount;
	private Boolean templatePrint;
	private Long ownersEquity;
	private String companyNameAr;
	private Long hrRoleToalert;
	private Long expenseAccount;
	private Long accountsReceivable;
	private Long revenueAccount;
	private Long accumulatedDepreciation;
	private Long serviceProduct;
	private Long accountStartingId;
	private Long creditCardAccount;
	private Long salesDiscount;
	private Long pettyCashAccount;
	private Long cashAccount;
	private Set<QuestionBank> questionBanks = new HashSet<QuestionBank>(0);
	private Set<CustomerQuotation> customerQuotations = new HashSet<CustomerQuotation>(
			0);
	private Set<Contract> contracts = new HashSet<Contract>(0);
	private Set<AccountGroup> accountGroups = new HashSet<AccountGroup>(0);
	private Set<PromotionDemotion> promotionDemotions = new HashSet<PromotionDemotion>(
			0);
	private Set<Skills> skillses = new HashSet<Skills>(0);
	private Set<Designation> designations = new HashSet<Designation>(0);
	private Set<SwipeInOut> swipeInOuts = new HashSet<SwipeInOut>(0);
	private Set<MemoWarning> memoWarnings = new HashSet<MemoWarning>(0);
	private Set<Tender> tenders = new HashSet<Tender>(0);
	private Set<ProductionRequisition> productionRequisitions = new HashSet<ProductionRequisition>(
			0);
	private Set<EODBalancing> EODBalancings = new HashSet<EODBalancing>(0);
	private Set<Leave> leaves = new HashSet<Leave>(0);
	private Set<Image> images = new HashSet<Image>(0);
	private Set<Project> projects = new HashSet<Project>(0);
	private Set<LeaveReconciliation> leaveReconciliations = new HashSet<LeaveReconciliation>(
			0);
	private Set<Allowance> allowances = new HashSet<Allowance>(0);
	private Set<Stock> stocks = new HashSet<Stock>(0);
	private Set<Cancellation> cancellations = new HashSet<Cancellation>(0);
	private Set<Invoice> invoices = new HashSet<Invoice>(0);
	private Set<Credit> credits = new HashSet<Credit>(0);
	private Set<PayByCount> payByCounts = new HashSet<PayByCount>(0);
	private Set<Attendance> attendances = new HashSet<Attendance>(0);
	private Set<InsuranceCompany> insuranceCompanies = new HashSet<InsuranceCompany>(
			0);
	private Set<MemberCard> memberCards = new HashSet<MemberCard>(0);
	private Set<WorkinProcess> workinProcesses = new HashSet<WorkinProcess>(0);
	private Set<DependentAllowance> dependentAllowances = new HashSet<DependentAllowance>(
			0);
	private Set<SalesInvoice> salesInvoices = new HashSet<SalesInvoice>(0);
	private Set<Customer> customers = new HashSet<Customer>(0);
	private Set<BankReconciliation> bankReconciliations = new HashSet<BankReconciliation>(
			0);
	private Set<AdvertisingPanel> advertisingPanels = new HashSet<AdvertisingPanel>(
			0);
	private Set<MaterialTransfer> materialTransfers = new HashSet<MaterialTransfer>(
			0);
	private Set<ProjectMileStone> projectMileStones = new HashSet<ProjectMileStone>(
			0);
	private Set<Discount> discounts = new HashSet<Discount>(0);
	private Set<FuelAllowance> fuelAllowances = new HashSet<FuelAllowance>(0);
	private Set<Company> companies = new HashSet<Company>(0);
	private Set<Supplier> suppliers = new HashSet<Supplier>(0);
	private Set<Coupon> coupons = new HashSet<Coupon>(0);
	private Set<ProjectDelivery> projectDeliveries = new HashSet<ProjectDelivery>(
			0);
	private Set<CurrencyConversion> currencyConversions = new HashSet<CurrencyConversion>(
			0);
	private Set<MileageLog> mileageLogs = new HashSet<MileageLog>(0);
	private Set<AssetClaimInsurance> assetClaimInsurances = new HashSet<AssetClaimInsurance>(
			0);
	private Set<LookupMaster> lookupMasters = new HashSet<LookupMaster>(0);
	private Set<OtRecoveryMethod> otRecoveryMethods = new HashSet<OtRecoveryMethod>(
			0);
	private Set<EmployeeLoan> employeeLoans = new HashSet<EmployeeLoan>(0);
	private Set<PayrollElement> payrollElements = new HashSet<PayrollElement>(0);
	private Set<PersonBank> personBanks = new HashSet<PersonBank>(0);
	private Set<SalesDeliveryNote> salesDeliveryNotes = new HashSet<SalesDeliveryNote>(
			0);
	private Set<IssueReturn> issueReturns = new HashSet<IssueReturn>(0);
	private Set<MedicalAllowance> medicalAllowances = new HashSet<MedicalAllowance>(
			0);
	private Set<Debit> debits = new HashSet<Debit>(0);
	private Set<ProjectInventory> projectInventories = new HashSet<ProjectInventory>(
			0);
	private Set<HouseRentAllowance> houseRentAllowances = new HashSet<HouseRentAllowance>(
			0);
	private Set<PettyCash> pettyCashs = new HashSet<PettyCash>(0);
	private Set<ProjectResource> projectResources = new HashSet<ProjectResource>(
			0);
	private Set<PaymentRequest> paymentRequests = new HashSet<PaymentRequest>(0);
	private Set<ParkingAllowance> parkingAllowances = new HashSet<ParkingAllowance>(
			0);
	private Set<Account> accounts = new HashSet<Account>(0);
	private Set<IdentityAvailability> identityAvailabilities = new HashSet<IdentityAvailability>(
			0);
	private Set<Department> departments = new HashSet<Department>(0);
	private Set<ProductionReceive> productionReceives = new HashSet<ProductionReceive>(
			0);
	private Set<AcademicQualifications> academicQualificationses = new HashSet<AcademicQualifications>(
			0);
	private Set<Offer> offers = new HashSet<Offer>(0);
	private Set<ProductPackage> productPackages = new HashSet<ProductPackage>(0);
	private Set<Unit> units = new HashSet<Unit>(0);
	private Set<VoidPayment> voidPayments = new HashSet<VoidPayment>(0);
	private Set<Loan> loans = new HashSet<Loan>(0);
	private Set<JobAssignment> jobAssignments = new HashSet<JobAssignment>(0);
	private Set<Location> locations = new HashSet<Location>(0);
	private Set<TenantGroup> tenantGroups = new HashSet<TenantGroup>(0);
	private Set<Calendar> calendars = new HashSet<Calendar>(0);
	private Set<Transaction> transactions = new HashSet<Transaction>(0);
	private Set<Country> countries = new HashSet<Country>(0);
	private Set<EosPolicy> eosPolicies = new HashSet<EosPolicy>(0);
	private Set<Reference> references = new HashSet<Reference>(0);
	private Set<Interview> interviews = new HashSet<Interview>(0);
	private Set<OpenPosition> openPositions = new HashSet<OpenPosition>(0);
	private Set<RecruitmentSource> recruitmentSources = new HashSet<RecruitmentSource>(
			0);
	private Set<ProductPricing> productPricings = new HashSet<ProductPricing>(0);
	private Set<BankDeposit> bankDeposits = new HashSet<BankDeposit>(0);
	private Set<AssetWarrantyClaim> assetWarrantyClaims = new HashSet<AssetWarrantyClaim>(
			0);
	private Set<Eibor> eibors = new HashSet<Eibor>(0);
	private Set<InterviewProcess> interviewProcesses = new HashSet<InterviewProcess>(
			0);
	private Set<SalesOrder> salesOrders = new HashSet<SalesOrder>(0);
	private Set<ProjectPaymentSchedule> projectPaymentSchedules = new HashSet<ProjectPaymentSchedule>(
			0);
	private Set<StockPeriodic> stockPeriodics = new HashSet<StockPeriodic>(0);
	private Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>(0);
	private Set<MaterialWastage> materialWastages = new HashSet<MaterialWastage>(
			0);
	private Set<PropertyExpense> propertyExpenses = new HashSet<PropertyExpense>(
			0);
	private Set<ProductCategory> productCategories = new HashSet<ProductCategory>(
			0);
	private Set<MaterialRequisition> materialRequisitions = new HashSet<MaterialRequisition>(
			0);
	private Set<BankTransfer> bankTransfers = new HashSet<BankTransfer>(0);
	private Set<TransactionTemp> transactionTemps = new HashSet<TransactionTemp>(
			0);
	private Set<MerchandiseExchange> merchandiseExchanges = new HashSet<MerchandiseExchange>(
			0);
	private Set<Requisition> requisitions = new HashSet<Requisition>(0);
	private Set<GoodsReturn> goodsReturns = new HashSet<GoodsReturn>(0);
	private Set<PayPeriod> payPeriods = new HashSet<PayPeriod>(0);
	private Set<CommissionRule> commissionRules = new HashSet<CommissionRule>(0);
	private Set<ProductDefinition> productDefinitions = new HashSet<ProductDefinition>(
			0);
	private Set<AssetUsage> assetUsages = new HashSet<AssetUsage>(0);
	private Set<ProjectTask> projectTasks = new HashSet<ProjectTask>(0);
	private Set<CreditTerm> creditTerms = new HashSet<CreditTerm>(0);
	private Set<ProjectExpense> projectExpenses = new HashSet<ProjectExpense>(0);
	private Set<ProjectPaymentDetail> projectPaymentDetails = new HashSet<ProjectPaymentDetail>(
			0);
	private Set<AssetCreation> assetCreations = new HashSet<AssetCreation>(0);
	private Set<ProjectNotes> projectNoteses = new HashSet<ProjectNotes>(0);
	private Set<TicketAllowance> ticketAllowances = new HashSet<TicketAllowance>(
			0);
	private Set<POSUserTill> POSUserTills = new HashSet<POSUserTill>(0);
	private Set<Property> properties = new HashSet<Property>(0);
	private Set<ResignationTermination> resignationTerminations = new HashSet<ResignationTermination>(
			0);
	private Set<CandidateOffer> candidateOffers = new HashSet<CandidateOffer>(0);
	private Set<WorkingShift> workingShifts = new HashSet<WorkingShift>(0);
	private Set<DirectPayment> directPayments = new HashSet<DirectPayment>(0);
	private Set<Category> categories = new HashSet<Category>(0);
	private Set<Candidate> candidates = new HashSet<Candidate>(0);
	private Set<LeavePolicy> leavePolicies = new HashSet<LeavePolicy>(0);
	private Set<Product> products = new HashSet<Product>(0);
	private Set<Promotion> promotions = new HashSet<Promotion>(0);
	private Set<Purchase> purchases = new HashSet<Purchase>(0);
	private Set<ProductSplit> productSplits = new HashSet<ProductSplit>(0);
	private Set<IdentityAvailabilityMaster> identityAvailabilityMasters = new HashSet<IdentityAvailabilityMaster>(
			0);
	private Set<Job> jobs = new HashSet<Job>(0);
	private Set<Grade> grades = new HashSet<Grade>(0);
	private Set<EmployeeCalendar> employeeCalendars = new HashSet<EmployeeCalendar>(
			0);
	private Set<Release> releases = new HashSet<Release>(0);
	private Set<PayrollTransaction> payrollTransactions = new HashSet<PayrollTransaction>(
			0);
	private Set<Payroll> payrolls = new HashSet<Payroll>(0);
	private Set<ReconciliationAdjustment> reconciliationAdjustments = new HashSet<ReconciliationAdjustment>(
			0);
	private Set<Quotation> quotations = new HashSet<Quotation>(0);
	private Set<Contacts> contactses = new HashSet<Contacts>(0);
	private Set<RewardPolicy> rewardPolicies = new HashSet<RewardPolicy>(0);
	private Set<IssueRequistion> issueRequistions = new HashSet<IssueRequistion>(
			0);
	private Set<AttendancePolicy> attendancePolicies = new HashSet<AttendancePolicy>(
			0);
	private Set<Currency> currencies = new HashSet<Currency>(0);
	private Set<Person> persons = new HashSet<Person>(0);
	private Set<MaterialIdleMix> materialIdleMixes = new HashSet<MaterialIdleMix>(
			0);
	private Set<PhoneAllowance> phoneAllowances = new HashSet<PhoneAllowance>(0);
	private Set<BankReceipts> bankReceiptses = new HashSet<BankReceipts>(0);
	private Set<Payment> payments = new HashSet<Payment>(0);
	private Set<Receive> receives = new HashSet<Receive>(0);
	private Set<ProjectDiscussion> projectDiscussions = new HashSet<ProjectDiscussion>(
			0);
	private Set<Bank> banks = new HashSet<Bank>(0);
	private Set<Component> components = new HashSet<Component>(0);
	private Set<Store> stores = new HashSet<Store>(0);
	private Set<LeaveProcess> leaveProcesses = new HashSet<LeaveProcess>(0);

	public Implementation() {
	}

	public Implementation(String companyName) {
		this.companyName = companyName;
	}

	public Implementation(String companyName, String companyKey,
			String mainLogo, String loginLogo, String headerLogo,
			String footerLogo, String loginBack1, String loginBack2,
			String mainBack1, String mainBack2, Boolean isWorkflow,
			Long receivableClerk, Long unearnedRevenue, Long accountPayable,
			Long pdcReceived, Long pdcIssued, Long clearingAccount, Long cashAccount,
			Boolean templatePrint, Long ownersEquity, String companyNameAr,
			Long hrRoleToalert, Long expenseAccount, Long accountsReceivable,
			Long revenueAccount, Long accumulatedDepreciation,
			Long serviceProduct, Long accountStartingId,
			Long creditCardAccount, Long salesDiscount, Long pettyCashAccount,
			Set<QuestionBank> questionBanks,
			Set<CustomerQuotation> customerQuotations, Set<Contract> contracts,
			Set<AccountGroup> accountGroups,
			Set<PromotionDemotion> promotionDemotions, Set<Skills> skillses,
			Set<Designation> designations, Set<SwipeInOut> swipeInOuts,
			Set<MemoWarning> memoWarnings, Set<Tender> tenders,
			Set<ProductionRequisition> productionRequisitions,
			Set<EODBalancing> EODBalancings, Set<Leave> leaves,
			Set<Image> images, Set<Project> projects,
			Set<LeaveReconciliation> leaveReconciliations,
			Set<Allowance> allowances, Set<Stock> stocks,
			Set<Cancellation> cancellations, Set<Invoice> invoices,
			Set<Credit> credits, Set<PayByCount> payByCounts,
			Set<Attendance> attendances,
			Set<InsuranceCompany> insuranceCompanies,
			Set<MemberCard> memberCards, Set<WorkinProcess> workinProcesses,
			Set<DependentAllowance> dependentAllowances,
			Set<SalesInvoice> salesInvoices, Set<Customer> customers,
			Set<BankReconciliation> bankReconciliations,
			Set<AdvertisingPanel> advertisingPanels,
			Set<MaterialTransfer> materialTransfers,
			Set<ProjectMileStone> projectMileStones, Set<Discount> discounts,
			Set<FuelAllowance> fuelAllowances, Set<Company> companies,
			Set<Supplier> suppliers, Set<Coupon> coupons,
			Set<ProjectDelivery> projectDeliveries,
			Set<CurrencyConversion> currencyConversions,
			Set<MileageLog> mileageLogs,
			Set<AssetClaimInsurance> assetClaimInsurances,
			Set<LookupMaster> lookupMasters,
			Set<OtRecoveryMethod> otRecoveryMethods,
			Set<EmployeeLoan> employeeLoans,
			Set<PayrollElement> payrollElements, Set<PersonBank> personBanks,
			Set<SalesDeliveryNote> salesDeliveryNotes,
			Set<IssueReturn> issueReturns,
			Set<MedicalAllowance> medicalAllowances, Set<Debit> debits,
			Set<ProjectInventory> projectInventories,
			Set<HouseRentAllowance> houseRentAllowances,
			Set<PettyCash> pettyCashs, Set<ProjectResource> projectResources,
			Set<PaymentRequest> paymentRequests,
			Set<ParkingAllowance> parkingAllowances, Set<Account> accounts,
			Set<IdentityAvailability> identityAvailabilities,
			Set<Department> departments,
			Set<ProductionReceive> productionReceives,
			Set<AcademicQualifications> academicQualificationses,
			Set<Offer> offers, Set<ProductPackage> productPackages,
			Set<Unit> units, Set<VoidPayment> voidPayments, Set<Loan> loans,
			Set<JobAssignment> jobAssignments, Set<Location> locations,
			Set<TenantGroup> tenantGroups, Set<Calendar> calendars,
			Set<Transaction> transactions, Set<Country> countries,
			Set<EosPolicy> eosPolicies, Set<Reference> references,
			Set<Interview> interviews, Set<OpenPosition> openPositions,
			Set<RecruitmentSource> recruitmentSources,
			Set<ProductPricing> productPricings, Set<BankDeposit> bankDeposits,
			Set<AssetWarrantyClaim> assetWarrantyClaims, Set<Eibor> eibors,
			Set<InterviewProcess> interviewProcesses,
			Set<SalesOrder> salesOrders,
			Set<ProjectPaymentSchedule> projectPaymentSchedules,
			Set<StockPeriodic> stockPeriodics, Set<PointOfSale> pointOfSales,
			Set<MaterialWastage> materialWastages,
			Set<PropertyExpense> propertyExpenses,
			Set<ProductCategory> productCategories,
			Set<MaterialRequisition> materialRequisitions,
			Set<BankTransfer> bankTransfers,
			Set<TransactionTemp> transactionTemps,
			Set<MerchandiseExchange> merchandiseExchanges,
			Set<Requisition> requisitions, Set<GoodsReturn> goodsReturns,
			Set<PayPeriod> payPeriods, Set<CommissionRule> commissionRules,
			Set<ProductDefinition> productDefinitions,
			Set<AssetUsage> assetUsages, Set<ProjectTask> projectTasks,
			Set<CreditTerm> creditTerms, Set<ProjectExpense> projectExpenses,
			Set<ProjectPaymentDetail> projectPaymentDetails,
			Set<AssetCreation> assetCreations,
			Set<ProjectNotes> projectNoteses,
			Set<TicketAllowance> ticketAllowances,
			Set<POSUserTill> POSUserTills, Set<Property> properties,
			Set<ResignationTermination> resignationTerminations,
			Set<CandidateOffer> candidateOffers,
			Set<WorkingShift> workingShifts, Set<DirectPayment> directPayments,
			Set<Category> categories, Set<Candidate> candidates,
			Set<LeavePolicy> leavePolicies, Set<Product> products,
			Set<Promotion> promotions, Set<Purchase> purchases,
			Set<ProductSplit> productSplits,
			Set<IdentityAvailabilityMaster> identityAvailabilityMasters,
			Set<Job> jobs, Set<Grade> grades,
			Set<EmployeeCalendar> employeeCalendars, Set<Release> releases,
			Set<PayrollTransaction> payrollTransactions, Set<Payroll> payrolls,
			Set<ReconciliationAdjustment> reconciliationAdjustments,
			Set<Quotation> quotations, Set<Contacts> contactses,
			Set<RewardPolicy> rewardPolicies,
			Set<IssueRequistion> issueRequistions,
			Set<AttendancePolicy> attendancePolicies, Set<Currency> currencies,
			Set<Person> persons, Set<MaterialIdleMix> materialIdleMixes,
			Set<PhoneAllowance> phoneAllowances,
			Set<BankReceipts> bankReceiptses, Set<Payment> payments,
			Set<Receive> receives, Set<ProjectDiscussion> projectDiscussions,
			Set<Bank> banks, Set<Component> components, Set<Store> stores,
			Set<LeaveProcess> leaveProcesses) {
		this.companyName = companyName;
		this.companyKey = companyKey;
		this.mainLogo = mainLogo;
		this.loginLogo = loginLogo;
		this.headerLogo = headerLogo;
		this.footerLogo = footerLogo;
		this.loginBack1 = loginBack1;
		this.loginBack2 = loginBack2;
		this.mainBack1 = mainBack1;
		this.mainBack2 = mainBack2;
		this.isWorkflow = isWorkflow;
		this.receivableClerk = receivableClerk;
		this.unearnedRevenue = unearnedRevenue;
		this.accountPayable = accountPayable;
		this.pdcReceived = pdcReceived;
		this.pdcIssued = pdcIssued;
		this.clearingAccount = clearingAccount;
		this.cashAccount = cashAccount;
		this.templatePrint = templatePrint;
		this.ownersEquity = ownersEquity;
		this.companyNameAr = companyNameAr;
		this.hrRoleToalert = hrRoleToalert;
		this.expenseAccount = expenseAccount;
		this.accountsReceivable = accountsReceivable;
		this.revenueAccount = revenueAccount;
		this.accumulatedDepreciation = accumulatedDepreciation;
		this.serviceProduct = serviceProduct;
		this.accountStartingId = accountStartingId;
		this.creditCardAccount = creditCardAccount;
		this.salesDiscount = salesDiscount;
		this.pettyCashAccount = pettyCashAccount;
		this.questionBanks = questionBanks;
		this.customerQuotations = customerQuotations;
		this.contracts = contracts;
		this.accountGroups = accountGroups;
		this.promotionDemotions = promotionDemotions;
		this.skillses = skillses;
		this.designations = designations;
		this.swipeInOuts = swipeInOuts;
		this.memoWarnings = memoWarnings;
		this.tenders = tenders;
		this.productionRequisitions = productionRequisitions;
		this.EODBalancings = EODBalancings;
		this.leaves = leaves;
		this.images = images;
		this.projects = projects;
		this.leaveReconciliations = leaveReconciliations;
		this.allowances = allowances;
		this.stocks = stocks;
		this.cancellations = cancellations;
		this.invoices = invoices;
		this.credits = credits;
		this.payByCounts = payByCounts;
		this.attendances = attendances;
		this.insuranceCompanies = insuranceCompanies;
		this.memberCards = memberCards;
		this.workinProcesses = workinProcesses;
		this.dependentAllowances = dependentAllowances;
		this.salesInvoices = salesInvoices;
		this.customers = customers;
		this.bankReconciliations = bankReconciliations;
		this.advertisingPanels = advertisingPanels;
		this.materialTransfers = materialTransfers;
		this.projectMileStones = projectMileStones;
		this.discounts = discounts;
		this.fuelAllowances = fuelAllowances;
		this.companies = companies;
		this.suppliers = suppliers;
		this.coupons = coupons;
		this.projectDeliveries = projectDeliveries;
		this.currencyConversions = currencyConversions;
		this.mileageLogs = mileageLogs;
		this.assetClaimInsurances = assetClaimInsurances;
		this.lookupMasters = lookupMasters;
		this.otRecoveryMethods = otRecoveryMethods;
		this.employeeLoans = employeeLoans;
		this.payrollElements = payrollElements;
		this.personBanks = personBanks;
		this.salesDeliveryNotes = salesDeliveryNotes;
		this.issueReturns = issueReturns;
		this.medicalAllowances = medicalAllowances;
		this.debits = debits;
		this.projectInventories = projectInventories;
		this.houseRentAllowances = houseRentAllowances;
		this.pettyCashs = pettyCashs;
		this.projectResources = projectResources;
		this.paymentRequests = paymentRequests;
		this.parkingAllowances = parkingAllowances;
		this.accounts = accounts;
		this.identityAvailabilities = identityAvailabilities;
		this.departments = departments;
		this.productionReceives = productionReceives;
		this.academicQualificationses = academicQualificationses;
		this.offers = offers;
		this.productPackages = productPackages;
		this.units = units;
		this.voidPayments = voidPayments;
		this.loans = loans;
		this.jobAssignments = jobAssignments;
		this.locations = locations;
		this.tenantGroups = tenantGroups;
		this.calendars = calendars;
		this.transactions = transactions;
		this.countries = countries;
		this.eosPolicies = eosPolicies;
		this.references = references;
		this.interviews = interviews;
		this.openPositions = openPositions;
		this.recruitmentSources = recruitmentSources;
		this.productPricings = productPricings;
		this.bankDeposits = bankDeposits;
		this.assetWarrantyClaims = assetWarrantyClaims;
		this.eibors = eibors;
		this.interviewProcesses = interviewProcesses;
		this.salesOrders = salesOrders;
		this.projectPaymentSchedules = projectPaymentSchedules;
		this.stockPeriodics = stockPeriodics;
		this.pointOfSales = pointOfSales;
		this.materialWastages = materialWastages;
		this.propertyExpenses = propertyExpenses;
		this.productCategories = productCategories;
		this.materialRequisitions = materialRequisitions;
		this.bankTransfers = bankTransfers;
		this.transactionTemps = transactionTemps;
		this.merchandiseExchanges = merchandiseExchanges;
		this.requisitions = requisitions;
		this.goodsReturns = goodsReturns;
		this.payPeriods = payPeriods;
		this.commissionRules = commissionRules;
		this.productDefinitions = productDefinitions;
		this.assetUsages = assetUsages;
		this.projectTasks = projectTasks;
		this.creditTerms = creditTerms;
		this.projectExpenses = projectExpenses;
		this.projectPaymentDetails = projectPaymentDetails;
		this.assetCreations = assetCreations;
		this.projectNoteses = projectNoteses;
		this.ticketAllowances = ticketAllowances;
		this.POSUserTills = POSUserTills;
		this.properties = properties;
		this.resignationTerminations = resignationTerminations;
		this.candidateOffers = candidateOffers;
		this.workingShifts = workingShifts;
		this.directPayments = directPayments;
		this.categories = categories;
		this.candidates = candidates;
		this.leavePolicies = leavePolicies;
		this.products = products;
		this.promotions = promotions;
		this.purchases = purchases;
		this.productSplits = productSplits;
		this.identityAvailabilityMasters = identityAvailabilityMasters;
		this.jobs = jobs;
		this.grades = grades;
		this.employeeCalendars = employeeCalendars;
		this.releases = releases;
		this.payrollTransactions = payrollTransactions;
		this.payrolls = payrolls;
		this.reconciliationAdjustments = reconciliationAdjustments;
		this.quotations = quotations;
		this.contactses = contactses;
		this.rewardPolicies = rewardPolicies;
		this.issueRequistions = issueRequistions;
		this.attendancePolicies = attendancePolicies;
		this.currencies = currencies;
		this.persons = persons;
		this.materialIdleMixes = materialIdleMixes;
		this.phoneAllowances = phoneAllowances;
		this.bankReceiptses = bankReceiptses;
		this.payments = payments;
		this.receives = receives;
		this.projectDiscussions = projectDiscussions;
		this.banks = banks;
		this.components = components;
		this.stores = stores;
		this.leaveProcesses = leaveProcesses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "implementation_id", unique = true, nullable = false)
	public Long getImplementationId() {
		return this.implementationId;
	}

	public void setImplementationId(Long implementationId) {
		this.implementationId = implementationId;
	}

	@Column(name = "company_name", nullable = false, length = 250)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "company_key", length = 50)
	public String getCompanyKey() {
		return this.companyKey;
	}

	public void setCompanyKey(String companyKey) {
		this.companyKey = companyKey;
	}

	@Column(name = "main_logo", length = 50)
	public String getMainLogo() {
		return this.mainLogo;
	}

	public void setMainLogo(String mainLogo) {
		this.mainLogo = mainLogo;
	}

	@Column(name = "login_logo", length = 50)
	public String getLoginLogo() {
		return this.loginLogo;
	}

	public void setLoginLogo(String loginLogo) {
		this.loginLogo = loginLogo;
	}

	@Column(name = "header_logo", length = 50)
	public String getHeaderLogo() {
		return this.headerLogo;
	}

	public void setHeaderLogo(String headerLogo) {
		this.headerLogo = headerLogo;
	}

	@Column(name = "footer_logo", length = 50)
	public String getFooterLogo() {
		return this.footerLogo;
	}

	public void setFooterLogo(String footerLogo) {
		this.footerLogo = footerLogo;
	}

	@Column(name = "login_back1", length = 50)
	public String getLoginBack1() {
		return this.loginBack1;
	}

	public void setLoginBack1(String loginBack1) {
		this.loginBack1 = loginBack1;
	}

	@Column(name = "login_back2", length = 50)
	public String getLoginBack2() {
		return this.loginBack2;
	}

	public void setLoginBack2(String loginBack2) {
		this.loginBack2 = loginBack2;
	}

	@Column(name = "main_back1", length = 50)
	public String getMainBack1() {
		return this.mainBack1;
	}

	public void setMainBack1(String mainBack1) {
		this.mainBack1 = mainBack1;
	}

	@Column(name = "main_back2", length = 50)
	public String getMainBack2() {
		return this.mainBack2;
	}

	public void setMainBack2(String mainBack2) {
		this.mainBack2 = mainBack2;
	}

	@Column(name = "is_workflow")
	public Boolean getIsWorkflow() {
		return this.isWorkflow;
	}

	public void setIsWorkflow(Boolean isWorkflow) {
		this.isWorkflow = isWorkflow;
	}

	@Column(name = "receivable_clerk")
	public Long getReceivableClerk() {
		return this.receivableClerk;
	}

	public void setReceivableClerk(Long receivableClerk) {
		this.receivableClerk = receivableClerk;
	}

	@Column(name = "unearned_revenue")
	public Long getUnearnedRevenue() {
		return this.unearnedRevenue;
	}

	public void setUnearnedRevenue(Long unearnedRevenue) {
		this.unearnedRevenue = unearnedRevenue;
	}

	@Column(name = "account_payable")
	public Long getAccountPayable() {
		return this.accountPayable;
	}

	public void setAccountPayable(Long accountPayable) {
		this.accountPayable = accountPayable;
	}

	@Column(name = "pdc_received")
	public Long getPdcReceived() {
		return this.pdcReceived;
	}

	public void setPdcReceived(Long pdcReceived) {
		this.pdcReceived = pdcReceived;
	}

	@Column(name = "pdc_issued")
	public Long getPdcIssued() {
		return this.pdcIssued;
	}

	public void setPdcIssued(Long pdcIssued) {
		this.pdcIssued = pdcIssued;
	}

	@Column(name = "clearing_account")
	public Long getClearingAccount() {
		return this.clearingAccount;
	}

	public void setClearingAccount(Long clearingAccount) {
		this.clearingAccount = clearingAccount;
	}

	@Column(name = "cash_account")
	public Long getCashAccount() {
		return cashAccount;
	}

	public void setCashAccount(Long cashAccount) {
		this.cashAccount = cashAccount;
	}
	
	@Column(name = "template_print")
	public Boolean getTemplatePrint() {
		return this.templatePrint;
	}

	public void setTemplatePrint(Boolean templatePrint) {
		this.templatePrint = templatePrint;
	}

	@Column(name = "owners_equity")
	public Long getOwnersEquity() {
		return this.ownersEquity;
	}

	public void setOwnersEquity(Long ownersEquity) {
		this.ownersEquity = ownersEquity;
	}

	@Column(name = "company_name_ar", length = 250)
	public String getCompanyNameAr() {
		return this.companyNameAr;
	}

	public void setCompanyNameAr(String companyNameAr) {
		this.companyNameAr = companyNameAr;
	}

	@Column(name = "hr_role_toalert")
	public Long getHrRoleToalert() {
		return this.hrRoleToalert;
	}

	public void setHrRoleToalert(Long hrRoleToalert) {
		this.hrRoleToalert = hrRoleToalert;
	}

	@Column(name = "expense_account")
	public Long getExpenseAccount() {
		return this.expenseAccount;
	}

	public void setExpenseAccount(Long expenseAccount) {
		this.expenseAccount = expenseAccount;
	}

	@Column(name = "accounts_receivable")
	public Long getAccountsReceivable() {
		return this.accountsReceivable;
	}

	public void setAccountsReceivable(Long accountsReceivable) {
		this.accountsReceivable = accountsReceivable;
	}

	@Column(name = "revenue_account")
	public Long getRevenueAccount() {
		return this.revenueAccount;
	}

	public void setRevenueAccount(Long revenueAccount) {
		this.revenueAccount = revenueAccount;
	}

	@Column(name = "accumulated_depreciation")
	public Long getAccumulatedDepreciation() {
		return this.accumulatedDepreciation;
	}

	public void setAccumulatedDepreciation(Long accumulatedDepreciation) {
		this.accumulatedDepreciation = accumulatedDepreciation;
	}

	@Column(name = "service_product")
	public Long getServiceProduct() {
		return this.serviceProduct;
	}

	public void setServiceProduct(Long serviceProduct) {
		this.serviceProduct = serviceProduct;
	}

	@Column(name = "account_starting_id")
	public Long getAccountStartingId() {
		return this.accountStartingId;
	}

	public void setAccountStartingId(Long accountStartingId) {
		this.accountStartingId = accountStartingId;
	}

	@Column(name = "credit_card_account")
	public Long getCreditCardAccount() {
		return this.creditCardAccount;
	}

	public void setCreditCardAccount(Long creditCardAccount) {
		this.creditCardAccount = creditCardAccount;
	}

	@Column(name = "sales_discount")
	public Long getSalesDiscount() {
		return this.salesDiscount;
	}

	public void setSalesDiscount(Long salesDiscount) {
		this.salesDiscount = salesDiscount;
	}

	@Column(name = "petty_cash_account")
	public Long getPettyCashAccount() {
		return this.pettyCashAccount;
	}

	public void setPettyCashAccount(Long pettyCashAccount) {
		this.pettyCashAccount = pettyCashAccount;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<QuestionBank> getQuestionBanks() {
		return this.questionBanks;
	}

	public void setQuestionBanks(Set<QuestionBank> questionBanks) {
		this.questionBanks = questionBanks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<CustomerQuotation> getCustomerQuotations() {
		return this.customerQuotations;
	}

	public void setCustomerQuotations(Set<CustomerQuotation> customerQuotations) {
		this.customerQuotations = customerQuotations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Contract> getContracts() {
		return this.contracts;
	}

	public void setContracts(Set<Contract> contracts) {
		this.contracts = contracts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AccountGroup> getAccountGroups() {
		return this.accountGroups;
	}

	public void setAccountGroups(Set<AccountGroup> accountGroups) {
		this.accountGroups = accountGroups;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PromotionDemotion> getPromotionDemotions() {
		return this.promotionDemotions;
	}

	public void setPromotionDemotions(Set<PromotionDemotion> promotionDemotions) {
		this.promotionDemotions = promotionDemotions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Skills> getSkillses() {
		return this.skillses;
	}

	public void setSkillses(Set<Skills> skillses) {
		this.skillses = skillses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Designation> getDesignations() {
		return this.designations;
	}

	public void setDesignations(Set<Designation> designations) {
		this.designations = designations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<SwipeInOut> getSwipeInOuts() {
		return this.swipeInOuts;
	}

	public void setSwipeInOuts(Set<SwipeInOut> swipeInOuts) {
		this.swipeInOuts = swipeInOuts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MemoWarning> getMemoWarnings() {
		return this.memoWarnings;
	}

	public void setMemoWarnings(Set<MemoWarning> memoWarnings) {
		this.memoWarnings = memoWarnings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Tender> getTenders() {
		return this.tenders;
	}

	public void setTenders(Set<Tender> tenders) {
		this.tenders = tenders;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductionRequisition> getProductionRequisitions() {
		return this.productionRequisitions;
	}

	public void setProductionRequisitions(
			Set<ProductionRequisition> productionRequisitions) {
		this.productionRequisitions = productionRequisitions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<EODBalancing> getEODBalancings() {
		return this.EODBalancings;
	}

	public void setEODBalancings(Set<EODBalancing> EODBalancings) {
		this.EODBalancings = EODBalancings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Leave> getLeaves() {
		return this.leaves;
	}

	public void setLeaves(Set<Leave> leaves) {
		this.leaves = leaves;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Image> getImages() {
		return this.images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<LeaveReconciliation> getLeaveReconciliations() {
		return this.leaveReconciliations;
	}

	public void setLeaveReconciliations(
			Set<LeaveReconciliation> leaveReconciliations) {
		this.leaveReconciliations = leaveReconciliations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Allowance> getAllowances() {
		return this.allowances;
	}

	public void setAllowances(Set<Allowance> allowances) {
		this.allowances = allowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(Set<Stock> stocks) {
		this.stocks = stocks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Cancellation> getCancellations() {
		return this.cancellations;
	}

	public void setCancellations(Set<Cancellation> cancellations) {
		this.cancellations = cancellations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Invoice> getInvoices() {
		return this.invoices;
	}

	public void setInvoices(Set<Invoice> invoices) {
		this.invoices = invoices;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Credit> getCredits() {
		return this.credits;
	}

	public void setCredits(Set<Credit> credits) {
		this.credits = credits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PayByCount> getPayByCounts() {
		return this.payByCounts;
	}

	public void setPayByCounts(Set<PayByCount> payByCounts) {
		this.payByCounts = payByCounts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Attendance> getAttendances() {
		return this.attendances;
	}

	public void setAttendances(Set<Attendance> attendances) {
		this.attendances = attendances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<InsuranceCompany> getInsuranceCompanies() {
		return this.insuranceCompanies;
	}

	public void setInsuranceCompanies(Set<InsuranceCompany> insuranceCompanies) {
		this.insuranceCompanies = insuranceCompanies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MemberCard> getMemberCards() {
		return this.memberCards;
	}

	public void setMemberCards(Set<MemberCard> memberCards) {
		this.memberCards = memberCards;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<WorkinProcess> getWorkinProcesses() {
		return this.workinProcesses;
	}

	public void setWorkinProcesses(Set<WorkinProcess> workinProcesses) {
		this.workinProcesses = workinProcesses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<DependentAllowance> getDependentAllowances() {
		return this.dependentAllowances;
	}

	public void setDependentAllowances(
			Set<DependentAllowance> dependentAllowances) {
		this.dependentAllowances = dependentAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<SalesInvoice> getSalesInvoices() {
		return this.salesInvoices;
	}

	public void setSalesInvoices(Set<SalesInvoice> salesInvoices) {
		this.salesInvoices = salesInvoices;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<BankReconciliation> getBankReconciliations() {
		return this.bankReconciliations;
	}

	public void setBankReconciliations(
			Set<BankReconciliation> bankReconciliations) {
		this.bankReconciliations = bankReconciliations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AdvertisingPanel> getAdvertisingPanels() {
		return this.advertisingPanels;
	}

	public void setAdvertisingPanels(Set<AdvertisingPanel> advertisingPanels) {
		this.advertisingPanels = advertisingPanels;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MaterialTransfer> getMaterialTransfers() {
		return this.materialTransfers;
	}

	public void setMaterialTransfers(Set<MaterialTransfer> materialTransfers) {
		this.materialTransfers = materialTransfers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectMileStone> getProjectMileStones() {
		return this.projectMileStones;
	}

	public void setProjectMileStones(Set<ProjectMileStone> projectMileStones) {
		this.projectMileStones = projectMileStones;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Discount> getDiscounts() {
		return this.discounts;
	}

	public void setDiscounts(Set<Discount> discounts) {
		this.discounts = discounts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<FuelAllowance> getFuelAllowances() {
		return this.fuelAllowances;
	}

	public void setFuelAllowances(Set<FuelAllowance> fuelAllowances) {
		this.fuelAllowances = fuelAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Company> getCompanies() {
		return this.companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Supplier> getSuppliers() {
		return this.suppliers;
	}

	public void setSuppliers(Set<Supplier> suppliers) {
		this.suppliers = suppliers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Coupon> getCoupons() {
		return this.coupons;
	}

	public void setCoupons(Set<Coupon> coupons) {
		this.coupons = coupons;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectDelivery> getProjectDeliveries() {
		return this.projectDeliveries;
	}

	public void setProjectDeliveries(Set<ProjectDelivery> projectDeliveries) {
		this.projectDeliveries = projectDeliveries;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<CurrencyConversion> getCurrencyConversions() {
		return this.currencyConversions;
	}

	public void setCurrencyConversions(
			Set<CurrencyConversion> currencyConversions) {
		this.currencyConversions = currencyConversions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MileageLog> getMileageLogs() {
		return this.mileageLogs;
	}

	public void setMileageLogs(Set<MileageLog> mileageLogs) {
		this.mileageLogs = mileageLogs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AssetClaimInsurance> getAssetClaimInsurances() {
		return this.assetClaimInsurances;
	}

	public void setAssetClaimInsurances(
			Set<AssetClaimInsurance> assetClaimInsurances) {
		this.assetClaimInsurances = assetClaimInsurances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<LookupMaster> getLookupMasters() {
		return this.lookupMasters;
	}

	public void setLookupMasters(Set<LookupMaster> lookupMasters) {
		this.lookupMasters = lookupMasters;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<OtRecoveryMethod> getOtRecoveryMethods() {
		return this.otRecoveryMethods;
	}

	public void setOtRecoveryMethods(Set<OtRecoveryMethod> otRecoveryMethods) {
		this.otRecoveryMethods = otRecoveryMethods;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<EmployeeLoan> getEmployeeLoans() {
		return this.employeeLoans;
	}

	public void setEmployeeLoans(Set<EmployeeLoan> employeeLoans) {
		this.employeeLoans = employeeLoans;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PayrollElement> getPayrollElements() {
		return this.payrollElements;
	}

	public void setPayrollElements(Set<PayrollElement> payrollElements) {
		this.payrollElements = payrollElements;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PersonBank> getPersonBanks() {
		return this.personBanks;
	}

	public void setPersonBanks(Set<PersonBank> personBanks) {
		this.personBanks = personBanks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<SalesDeliveryNote> getSalesDeliveryNotes() {
		return this.salesDeliveryNotes;
	}

	public void setSalesDeliveryNotes(Set<SalesDeliveryNote> salesDeliveryNotes) {
		this.salesDeliveryNotes = salesDeliveryNotes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<IssueReturn> getIssueReturns() {
		return this.issueReturns;
	}

	public void setIssueReturns(Set<IssueReturn> issueReturns) {
		this.issueReturns = issueReturns;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MedicalAllowance> getMedicalAllowances() {
		return this.medicalAllowances;
	}

	public void setMedicalAllowances(Set<MedicalAllowance> medicalAllowances) {
		this.medicalAllowances = medicalAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Debit> getDebits() {
		return this.debits;
	}

	public void setDebits(Set<Debit> debits) {
		this.debits = debits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectInventory> getProjectInventories() {
		return this.projectInventories;
	}

	public void setProjectInventories(Set<ProjectInventory> projectInventories) {
		this.projectInventories = projectInventories;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<HouseRentAllowance> getHouseRentAllowances() {
		return this.houseRentAllowances;
	}

	public void setHouseRentAllowances(
			Set<HouseRentAllowance> houseRentAllowances) {
		this.houseRentAllowances = houseRentAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PettyCash> getPettyCashs() {
		return this.pettyCashs;
	}

	public void setPettyCashs(Set<PettyCash> pettyCashs) {
		this.pettyCashs = pettyCashs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectResource> getProjectResources() {
		return this.projectResources;
	}

	public void setProjectResources(Set<ProjectResource> projectResources) {
		this.projectResources = projectResources;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PaymentRequest> getPaymentRequests() {
		return this.paymentRequests;
	}

	public void setPaymentRequests(Set<PaymentRequest> paymentRequests) {
		this.paymentRequests = paymentRequests;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ParkingAllowance> getParkingAllowances() {
		return this.parkingAllowances;
	}

	public void setParkingAllowances(Set<ParkingAllowance> parkingAllowances) {
		this.parkingAllowances = parkingAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Account> getAccounts() {
		return this.accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<IdentityAvailability> getIdentityAvailabilities() {
		return this.identityAvailabilities;
	}

	public void setIdentityAvailabilities(
			Set<IdentityAvailability> identityAvailabilities) {
		this.identityAvailabilities = identityAvailabilities;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(Set<Department> departments) {
		this.departments = departments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductionReceive> getProductionReceives() {
		return this.productionReceives;
	}

	public void setProductionReceives(Set<ProductionReceive> productionReceives) {
		this.productionReceives = productionReceives;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AcademicQualifications> getAcademicQualificationses() {
		return this.academicQualificationses;
	}

	public void setAcademicQualificationses(
			Set<AcademicQualifications> academicQualificationses) {
		this.academicQualificationses = academicQualificationses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Offer> getOffers() {
		return this.offers;
	}

	public void setOffers(Set<Offer> offers) {
		this.offers = offers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductPackage> getProductPackages() {
		return this.productPackages;
	}

	public void setProductPackages(Set<ProductPackage> productPackages) {
		this.productPackages = productPackages;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Unit> getUnits() {
		return this.units;
	}

	public void setUnits(Set<Unit> units) {
		this.units = units;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<VoidPayment> getVoidPayments() {
		return this.voidPayments;
	}

	public void setVoidPayments(Set<VoidPayment> voidPayments) {
		this.voidPayments = voidPayments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Loan> getLoans() {
		return this.loans;
	}

	public void setLoans(Set<Loan> loans) {
		this.loans = loans;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<JobAssignment> getJobAssignments() {
		return this.jobAssignments;
	}

	public void setJobAssignments(Set<JobAssignment> jobAssignments) {
		this.jobAssignments = jobAssignments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(Set<Location> locations) {
		this.locations = locations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<TenantGroup> getTenantGroups() {
		return this.tenantGroups;
	}

	public void setTenantGroups(Set<TenantGroup> tenantGroups) {
		this.tenantGroups = tenantGroups;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Calendar> getCalendars() {
		return this.calendars;
	}

	public void setCalendars(Set<Calendar> calendars) {
		this.calendars = calendars;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Transaction> getTransactions() {
		return this.transactions;
	}

	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Country> getCountries() {
		return this.countries;
	}

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<EosPolicy> getEosPolicies() {
		return this.eosPolicies;
	}

	public void setEosPolicies(Set<EosPolicy> eosPolicies) {
		this.eosPolicies = eosPolicies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Reference> getReferences() {
		return this.references;
	}

	public void setReferences(Set<Reference> references) {
		this.references = references;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Interview> getInterviews() {
		return this.interviews;
	}

	public void setInterviews(Set<Interview> interviews) {
		this.interviews = interviews;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<OpenPosition> getOpenPositions() {
		return this.openPositions;
	}

	public void setOpenPositions(Set<OpenPosition> openPositions) {
		this.openPositions = openPositions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<RecruitmentSource> getRecruitmentSources() {
		return this.recruitmentSources;
	}

	public void setRecruitmentSources(Set<RecruitmentSource> recruitmentSources) {
		this.recruitmentSources = recruitmentSources;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductPricing> getProductPricings() {
		return this.productPricings;
	}

	public void setProductPricings(Set<ProductPricing> productPricings) {
		this.productPricings = productPricings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<BankDeposit> getBankDeposits() {
		return this.bankDeposits;
	}

	public void setBankDeposits(Set<BankDeposit> bankDeposits) {
		this.bankDeposits = bankDeposits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AssetWarrantyClaim> getAssetWarrantyClaims() {
		return this.assetWarrantyClaims;
	}

	public void setAssetWarrantyClaims(
			Set<AssetWarrantyClaim> assetWarrantyClaims) {
		this.assetWarrantyClaims = assetWarrantyClaims;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Eibor> getEibors() {
		return this.eibors;
	}

	public void setEibors(Set<Eibor> eibors) {
		this.eibors = eibors;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<InterviewProcess> getInterviewProcesses() {
		return this.interviewProcesses;
	}

	public void setInterviewProcesses(Set<InterviewProcess> interviewProcesses) {
		this.interviewProcesses = interviewProcesses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<SalesOrder> getSalesOrders() {
		return this.salesOrders;
	}

	public void setSalesOrders(Set<SalesOrder> salesOrders) {
		this.salesOrders = salesOrders;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectPaymentSchedule> getProjectPaymentSchedules() {
		return this.projectPaymentSchedules;
	}

	public void setProjectPaymentSchedules(
			Set<ProjectPaymentSchedule> projectPaymentSchedules) {
		this.projectPaymentSchedules = projectPaymentSchedules;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<StockPeriodic> getStockPeriodics() {
		return this.stockPeriodics;
	}

	public void setStockPeriodics(Set<StockPeriodic> stockPeriodics) {
		this.stockPeriodics = stockPeriodics;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PointOfSale> getPointOfSales() {
		return this.pointOfSales;
	}

	public void setPointOfSales(Set<PointOfSale> pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MaterialWastage> getMaterialWastages() {
		return this.materialWastages;
	}

	public void setMaterialWastages(Set<MaterialWastage> materialWastages) {
		this.materialWastages = materialWastages;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PropertyExpense> getPropertyExpenses() {
		return this.propertyExpenses;
	}

	public void setPropertyExpenses(Set<PropertyExpense> propertyExpenses) {
		this.propertyExpenses = propertyExpenses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductCategory> getProductCategories() {
		return this.productCategories;
	}

	public void setProductCategories(Set<ProductCategory> productCategories) {
		this.productCategories = productCategories;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MaterialRequisition> getMaterialRequisitions() {
		return this.materialRequisitions;
	}

	public void setMaterialRequisitions(
			Set<MaterialRequisition> materialRequisitions) {
		this.materialRequisitions = materialRequisitions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<BankTransfer> getBankTransfers() {
		return this.bankTransfers;
	}

	public void setBankTransfers(Set<BankTransfer> bankTransfers) {
		this.bankTransfers = bankTransfers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<TransactionTemp> getTransactionTemps() {
		return this.transactionTemps;
	}

	public void setTransactionTemps(Set<TransactionTemp> transactionTemps) {
		this.transactionTemps = transactionTemps;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MerchandiseExchange> getMerchandiseExchanges() {
		return this.merchandiseExchanges;
	}

	public void setMerchandiseExchanges(
			Set<MerchandiseExchange> merchandiseExchanges) {
		this.merchandiseExchanges = merchandiseExchanges;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Requisition> getRequisitions() {
		return this.requisitions;
	}

	public void setRequisitions(Set<Requisition> requisitions) {
		this.requisitions = requisitions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<GoodsReturn> getGoodsReturns() {
		return this.goodsReturns;
	}

	public void setGoodsReturns(Set<GoodsReturn> goodsReturns) {
		this.goodsReturns = goodsReturns;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PayPeriod> getPayPeriods() {
		return this.payPeriods;
	}

	public void setPayPeriods(Set<PayPeriod> payPeriods) {
		this.payPeriods = payPeriods;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<CommissionRule> getCommissionRules() {
		return this.commissionRules;
	}

	public void setCommissionRules(Set<CommissionRule> commissionRules) {
		this.commissionRules = commissionRules;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductDefinition> getProductDefinitions() {
		return this.productDefinitions;
	}

	public void setProductDefinitions(Set<ProductDefinition> productDefinitions) {
		this.productDefinitions = productDefinitions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AssetUsage> getAssetUsages() {
		return this.assetUsages;
	}

	public void setAssetUsages(Set<AssetUsage> assetUsages) {
		this.assetUsages = assetUsages;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectTask> getProjectTasks() {
		return this.projectTasks;
	}

	public void setProjectTasks(Set<ProjectTask> projectTasks) {
		this.projectTasks = projectTasks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<CreditTerm> getCreditTerms() {
		return this.creditTerms;
	}

	public void setCreditTerms(Set<CreditTerm> creditTerms) {
		this.creditTerms = creditTerms;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectExpense> getProjectExpenses() {
		return this.projectExpenses;
	}

	public void setProjectExpenses(Set<ProjectExpense> projectExpenses) {
		this.projectExpenses = projectExpenses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectPaymentDetail> getProjectPaymentDetails() {
		return this.projectPaymentDetails;
	}

	public void setProjectPaymentDetails(
			Set<ProjectPaymentDetail> projectPaymentDetails) {
		this.projectPaymentDetails = projectPaymentDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AssetCreation> getAssetCreations() {
		return this.assetCreations;
	}

	public void setAssetCreations(Set<AssetCreation> assetCreations) {
		this.assetCreations = assetCreations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectNotes> getProjectNoteses() {
		return this.projectNoteses;
	}

	public void setProjectNoteses(Set<ProjectNotes> projectNoteses) {
		this.projectNoteses = projectNoteses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<TicketAllowance> getTicketAllowances() {
		return this.ticketAllowances;
	}

	public void setTicketAllowances(Set<TicketAllowance> ticketAllowances) {
		this.ticketAllowances = ticketAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<POSUserTill> getPOSUserTills() {
		return this.POSUserTills;
	}

	public void setPOSUserTills(Set<POSUserTill> POSUserTills) {
		this.POSUserTills = POSUserTills;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Property> getProperties() {
		return this.properties;
	}

	public void setProperties(Set<Property> properties) {
		this.properties = properties;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ResignationTermination> getResignationTerminations() {
		return this.resignationTerminations;
	}

	public void setResignationTerminations(
			Set<ResignationTermination> resignationTerminations) {
		this.resignationTerminations = resignationTerminations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<CandidateOffer> getCandidateOffers() {
		return this.candidateOffers;
	}

	public void setCandidateOffers(Set<CandidateOffer> candidateOffers) {
		this.candidateOffers = candidateOffers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<WorkingShift> getWorkingShifts() {
		return this.workingShifts;
	}

	public void setWorkingShifts(Set<WorkingShift> workingShifts) {
		this.workingShifts = workingShifts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<DirectPayment> getDirectPayments() {
		return this.directPayments;
	}

	public void setDirectPayments(Set<DirectPayment> directPayments) {
		this.directPayments = directPayments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Category> getCategories() {
		return this.categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Candidate> getCandidates() {
		return this.candidates;
	}

	public void setCandidates(Set<Candidate> candidates) {
		this.candidates = candidates;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<LeavePolicy> getLeavePolicies() {
		return this.leavePolicies;
	}

	public void setLeavePolicies(Set<LeavePolicy> leavePolicies) {
		this.leavePolicies = leavePolicies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Product> getProducts() {
		return this.products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Promotion> getPromotions() {
		return this.promotions;
	}

	public void setPromotions(Set<Promotion> promotions) {
		this.promotions = promotions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Purchase> getPurchases() {
		return this.purchases;
	}

	public void setPurchases(Set<Purchase> purchases) {
		this.purchases = purchases;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProductSplit> getProductSplits() {
		return this.productSplits;
	}

	public void setProductSplits(Set<ProductSplit> productSplits) {
		this.productSplits = productSplits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<IdentityAvailabilityMaster> getIdentityAvailabilityMasters() {
		return this.identityAvailabilityMasters;
	}

	public void setIdentityAvailabilityMasters(
			Set<IdentityAvailabilityMaster> identityAvailabilityMasters) {
		this.identityAvailabilityMasters = identityAvailabilityMasters;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Job> getJobs() {
		return this.jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Grade> getGrades() {
		return this.grades;
	}

	public void setGrades(Set<Grade> grades) {
		this.grades = grades;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<EmployeeCalendar> getEmployeeCalendars() {
		return this.employeeCalendars;
	}

	public void setEmployeeCalendars(Set<EmployeeCalendar> employeeCalendars) {
		this.employeeCalendars = employeeCalendars;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Release> getReleases() {
		return this.releases;
	}

	public void setReleases(Set<Release> releases) {
		this.releases = releases;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PayrollTransaction> getPayrollTransactions() {
		return this.payrollTransactions;
	}

	public void setPayrollTransactions(
			Set<PayrollTransaction> payrollTransactions) {
		this.payrollTransactions = payrollTransactions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Payroll> getPayrolls() {
		return this.payrolls;
	}

	public void setPayrolls(Set<Payroll> payrolls) {
		this.payrolls = payrolls;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ReconciliationAdjustment> getReconciliationAdjustments() {
		return this.reconciliationAdjustments;
	}

	public void setReconciliationAdjustments(
			Set<ReconciliationAdjustment> reconciliationAdjustments) {
		this.reconciliationAdjustments = reconciliationAdjustments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Quotation> getQuotations() {
		return this.quotations;
	}

	public void setQuotations(Set<Quotation> quotations) {
		this.quotations = quotations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Contacts> getContactses() {
		return this.contactses;
	}

	public void setContactses(Set<Contacts> contactses) {
		this.contactses = contactses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<RewardPolicy> getRewardPolicies() {
		return this.rewardPolicies;
	}

	public void setRewardPolicies(Set<RewardPolicy> rewardPolicies) {
		this.rewardPolicies = rewardPolicies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<IssueRequistion> getIssueRequistions() {
		return this.issueRequistions;
	}

	public void setIssueRequistions(Set<IssueRequistion> issueRequistions) {
		this.issueRequistions = issueRequistions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<AttendancePolicy> getAttendancePolicies() {
		return this.attendancePolicies;
	}

	public void setAttendancePolicies(Set<AttendancePolicy> attendancePolicies) {
		this.attendancePolicies = attendancePolicies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Currency> getCurrencies() {
		return this.currencies;
	}

	public void setCurrencies(Set<Currency> currencies) {
		this.currencies = currencies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(Set<Person> persons) {
		this.persons = persons;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<MaterialIdleMix> getMaterialIdleMixes() {
		return this.materialIdleMixes;
	}

	public void setMaterialIdleMixes(Set<MaterialIdleMix> materialIdleMixes) {
		this.materialIdleMixes = materialIdleMixes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<PhoneAllowance> getPhoneAllowances() {
		return this.phoneAllowances;
	}

	public void setPhoneAllowances(Set<PhoneAllowance> phoneAllowances) {
		this.phoneAllowances = phoneAllowances;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<BankReceipts> getBankReceiptses() {
		return this.bankReceiptses;
	}

	public void setBankReceiptses(Set<BankReceipts> bankReceiptses) {
		this.bankReceiptses = bankReceiptses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Payment> getPayments() {
		return this.payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Receive> getReceives() {
		return this.receives;
	}

	public void setReceives(Set<Receive> receives) {
		this.receives = receives;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<ProjectDiscussion> getProjectDiscussions() {
		return this.projectDiscussions;
	}

	public void setProjectDiscussions(Set<ProjectDiscussion> projectDiscussions) {
		this.projectDiscussions = projectDiscussions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Bank> getBanks() {
		return this.banks;
	}

	public void setBanks(Set<Bank> banks) {
		this.banks = banks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Component> getComponents() {
		return this.components;
	}

	public void setComponents(Set<Component> components) {
		this.components = components;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<Store> getStores() {
		return this.stores;
	}

	public void setStores(Set<Store> stores) {
		this.stores = stores;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "implementation")
	public Set<LeaveProcess> getLeaveProcesses() {
		return this.leaveProcesses;
	}

	public void setLeaveProcesses(Set<LeaveProcess> leaveProcesses) {
		this.leaveProcesses = leaveProcesses;
	} 
}
