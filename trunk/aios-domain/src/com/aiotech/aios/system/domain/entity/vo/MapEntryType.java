package com.aiotech.aios.system.domain.entity.vo;

import javax.xml.bind.annotation.XmlElement;

public class MapEntryType {
	
    private String key;
    private String value;

	@XmlElement
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

	@XmlElement
    public String getValue() {
        return value;
    }

    
    public void setValue(String value) {
        this.value = value;
    }
}
