package com.aiotech.aios.system.domain.entity.vo;

import java.util.Properties;

public class ConfigurationVO implements java.io.Serializable {

	private Integer alertDays;
	private Boolean smsEnabled;
	private Boolean emailEnabled;
	private Properties properties;

	public ConfigurationVO(Integer alertDays, Boolean smsEnabled,
			Boolean emailEnabled, Properties properties) {
		super();
		this.alertDays = alertDays;
		this.smsEnabled = smsEnabled;
		this.emailEnabled = emailEnabled;
		this.properties = properties;
	}

	public Integer getAlertDays() {
		return alertDays;
	}

	public void setAlertDays(Integer alertDays) {
		this.alertDays = alertDays;
	}

	public Boolean getSmsEnabled() {
		return smsEnabled;
	}

	public void setSmsEnabled(Boolean smsEnabled) {
		this.smsEnabled = smsEnabled;
	}

	public Boolean getEmailEnabled() {
		return emailEnabled;
	}

	public void setEmailEnabled(Boolean emailEnabled) {
		this.emailEnabled = emailEnabled;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
}
