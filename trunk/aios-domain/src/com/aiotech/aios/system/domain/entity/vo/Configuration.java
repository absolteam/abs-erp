package com.aiotech.aios.system.domain.entity.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "configuration")
public class Configuration {

	@XmlElement(name = "map", type = MapType.class)
	private MapType map = new MapType();
	
	@XmlElement(name = "accessMap", type = MapType.class)
	private MapType accessMap = new MapType();
	
	@XmlElement
	private Long implementation;
	
	public Long getImplementation() {
		return implementation;
	}

	public void setImplementation(Long implementation) {
		this.implementation = implementation;
	}

	public MapType getMap() {
		return map;
	}

	public void setMap(MapType map) {
		this.map = map;
	}

	public MapType getAccessMap() {
		return accessMap;
	}

	public void setAccessMap(MapType accessMap) {
		this.accessMap = accessMap;
	}

	
}
