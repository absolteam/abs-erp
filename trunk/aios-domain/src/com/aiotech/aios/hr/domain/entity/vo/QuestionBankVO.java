package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.QuestionBank;
import com.aiotech.aios.hr.domain.entity.QuestionInfo;

public class QuestionBankVO extends QuestionBank{

	private String questionPatternView;
	private String designationName;
	private String gradeName;
	private List<QuestionInfo> questionInfoList;

	public String getQuestionPatternView() {
		return questionPatternView;
	}

	public void setQuestionPatternView(String questionPatternView) {
		this.questionPatternView = questionPatternView;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public List<QuestionInfo> getQuestionInfoList() {
		return questionInfoList;
	}

	public void setQuestionInfoList(List<QuestionInfo> questionInfoList) {
		this.questionInfoList = questionInfoList;
	}

	
}
