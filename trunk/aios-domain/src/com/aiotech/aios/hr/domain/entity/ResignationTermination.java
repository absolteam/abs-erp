package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ResignationTermination generated by hbm2java
 */
@Entity
@Table(name = "hr_resignation_termination", catalog = "aios")
public class ResignationTermination implements java.io.Serializable {

	private Long resignationTerminationId;
	private JobAssignment jobAssignment;
	private Person personByRequestedBy;
	private Implementation implementation;
	private Person personByCreatedBy;
	private Byte type;
	private Date requestedDate;
	private Date effectiveDate;
	private Integer eligibleDays;
	private Integer givenDays;
	private Byte action;
	private Boolean positionAutoRelease;
	private String reason;
	private String description;
	private Date createdDate;
	private Integer noticeDays;
	private Byte status;
	private Byte isApprove;

	public ResignationTermination() {
	}

	public ResignationTermination(JobAssignment jobAssignment, Byte type,
			Date requestedDate, String reason) {
		this.jobAssignment = jobAssignment;
		this.type = type;
		this.requestedDate = requestedDate;
		this.reason = reason;
	}

	public ResignationTermination(JobAssignment jobAssignment,
			Person personByRequestedBy, Implementation implementation,
			Person personByCreatedBy, Byte type, Date requestedDate,
			Date effectiveDate, Integer eligibleDays, Integer givenDays,
			Byte action, Boolean positionAutoRelease, String reason,
			String description, Date createdDate, Integer noticeDays,
			Byte status, Byte isApprove) {
		this.jobAssignment = jobAssignment;
		this.personByRequestedBy = personByRequestedBy;
		this.implementation = implementation;
		this.personByCreatedBy = personByCreatedBy;
		this.type = type;
		this.requestedDate = requestedDate;
		this.effectiveDate = effectiveDate;
		this.eligibleDays = eligibleDays;
		this.givenDays = givenDays;
		this.action = action;
		this.positionAutoRelease = positionAutoRelease;
		this.reason = reason;
		this.description = description;
		this.createdDate = createdDate;
		this.noticeDays = noticeDays;
		this.status = status;
		this.isApprove = isApprove;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "resignation_termination_id", unique = true, nullable = false)
	public Long getResignationTerminationId() {
		return this.resignationTerminationId;
	}

	public void setResignationTerminationId(Long resignationTerminationId) {
		this.resignationTerminationId = resignationTerminationId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_assignment_id", nullable = false)
	public JobAssignment getJobAssignment() {
		return this.jobAssignment;
	}

	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requested_by")
	public Person getPersonByRequestedBy() {
		return this.personByRequestedBy;
	}

	public void setPersonByRequestedBy(Person personByRequestedBy) {
		this.personByRequestedBy = personByRequestedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id")
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public Person getPersonByCreatedBy() {
		return this.personByCreatedBy;
	}

	public void setPersonByCreatedBy(Person personByCreatedBy) {
		this.personByCreatedBy = personByCreatedBy;
	}

	@Column(name = "type", nullable = false)
	public Byte getType() {
		return this.type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "requested_date", nullable = false, length = 10)
	public Date getRequestedDate() {
		return this.requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "effective_date", length = 10)
	public Date getEffectiveDate() {
		return this.effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Column(name = "eligible_days")
	public Integer getEligibleDays() {
		return this.eligibleDays;
	}

	public void setEligibleDays(Integer eligibleDays) {
		this.eligibleDays = eligibleDays;
	}

	@Column(name = "given_days")
	public Integer getGivenDays() {
		return this.givenDays;
	}

	public void setGivenDays(Integer givenDays) {
		this.givenDays = givenDays;
	}

	@Column(name = "action")
	public Byte getAction() {
		return this.action;
	}

	public void setAction(Byte action) {
		this.action = action;
	}

	@Column(name = "position_auto_release")
	public Boolean getPositionAutoRelease() {
		return this.positionAutoRelease;
	}

	public void setPositionAutoRelease(Boolean positionAutoRelease) {
		this.positionAutoRelease = positionAutoRelease;
	}

	@Column(name = "reason", nullable = false, length = 65535)
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 10)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "notice_days")
	public Integer getNoticeDays() {
		return this.noticeDays;
	}

	public void setNoticeDays(Integer noticeDays) {
		this.noticeDays = noticeDays;
	}

	@Column(name = "status")
	public Byte getStatus() {
		return this.status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

}
