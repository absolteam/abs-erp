package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MemoWarning generated by hbm2java
 */
@Entity
@Table(name = "hr_memo_warning", catalog = "aios")
public class MemoWarning implements java.io.Serializable {

	private Long memoWarningId;
	private LookupDetail lookupDetail;
	private Implementation implementation;
	private Person person;
	private String title;
	private String subject;
	private String message;
	private String referenceNumber;
	private Byte isApprove;
	private Byte type;
	private String notes;
	private Date createdDate;
	private Byte status;
	private String department;
	private Byte processType;
	private Set<MemoWarningPerson> memoWarningPersons = new HashSet<MemoWarningPerson>(
			0);

	public MemoWarning() {
	}

	public MemoWarning(LookupDetail lookupDetail,
			Implementation implementation, Person person, String title,
			String subject, String message, String referenceNumber,
			Byte isApprove, Byte type, Date createdDate) {
		this.lookupDetail = lookupDetail;
		this.implementation = implementation;
		this.person = person;
		this.title = title;
		this.subject = subject;
		this.message = message;
		this.referenceNumber = referenceNumber;
		this.isApprove = isApprove;
		this.type = type;
		this.createdDate = createdDate;
	}

	public MemoWarning(LookupDetail lookupDetail,
			Implementation implementation, Person person, String title,
			String subject, String message, String referenceNumber,
			Byte isApprove, Byte type, String notes, Date createdDate,
			Byte status, String department, Byte processType,
			Set<MemoWarningPerson> memoWarningPersons) {
		this.lookupDetail = lookupDetail;
		this.implementation = implementation;
		this.person = person;
		this.title = title;
		this.subject = subject;
		this.message = message;
		this.referenceNumber = referenceNumber;
		this.isApprove = isApprove;
		this.type = type;
		this.notes = notes;
		this.createdDate = createdDate;
		this.status = status;
		this.department = department;
		this.processType = processType;
		this.memoWarningPersons = memoWarningPersons;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "memo_warning_id", unique = true, nullable = false)
	public Long getMemoWarningId() {
		return this.memoWarningId;
	}

	public void setMemoWarningId(Long memoWarningId) {
		this.memoWarningId = memoWarningId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_type", nullable = false)
	public LookupDetail getLookupDetail() {
		return this.lookupDetail;
	}

	public void setLookupDetail(LookupDetail lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by", nullable = false)
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Column(name = "title", nullable = false, length = 200)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "subject", nullable = false, length = 300)
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "message", nullable = false, length = 65535)
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "reference_number", nullable = false, length = 40)
	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Column(name = "is_approve", nullable = false)
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "type", nullable = false)
	public Byte getType() {
		return this.type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	@Column(name = "notes", length = 250)
	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", nullable = false, length = 10)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "status")
	public Byte getStatus() {
		return this.status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	@Column(name = "department", length = 500)
	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Column(name = "process_type")
	public Byte getProcessType() {
		return this.processType;
	}

	public void setProcessType(Byte processType) {
		this.processType = processType;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "memoWarning")
	public Set<MemoWarningPerson> getMemoWarningPersons() {
		return this.memoWarningPersons;
	}

	public void setMemoWarningPersons(Set<MemoWarningPerson> memoWarningPersons) {
		this.memoWarningPersons = memoWarningPersons;
	}

}
