package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;

import com.aiotech.aios.hr.domain.entity.PayPeriod;

public class PayPeriodVO extends PayPeriod{

	private String startDate;
	private String endDate;
	private String isDefaultView;
	private String isActiveView;
	private String payMonth;
	private Integer payWeek;
	private Long payPeriodTransactionId;
	private Date transactionDate;
	private Long jobAssignmentId;
	private boolean isMonthly;
	private boolean isWeekly;
	private boolean isYearly;
	private Byte payPolicy;
	private Integer order;
	private boolean isFreeze; 
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getIsDefaultView() {
		return isDefaultView;
	}
	public void setIsDefaultView(String isDefaultView) {
		this.isDefaultView = isDefaultView;
	}
	public String getIsActiveView() {
		return isActiveView;
	}
	public void setIsActiveView(String isActiveView) {
		this.isActiveView = isActiveView;
	}
	public String getPayMonth() {
		return payMonth;
	}
	public void setPayMonth(String payMonth) {
		this.payMonth = payMonth;
	}
	public Integer getPayWeek() {
		return payWeek;
	}
	public void setPayWeek(Integer payWeek) {
		this.payWeek = payWeek;
	}
	public Long getPayPeriodTransactionId() {
		return payPeriodTransactionId;
	}
	public void setPayPeriodTransactionId(Long payPeriodTransactionId) {
		this.payPeriodTransactionId = payPeriodTransactionId;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public boolean isMonthly() {
		return isMonthly;
	}
	public void setMonthly(boolean isMonthly) {
		this.isMonthly = isMonthly;
	}
	public boolean isWeekly() {
		return isWeekly;
	}
	public void setWeekly(boolean isWeekly) {
		this.isWeekly = isWeekly;
	}
	public Byte getPayPolicy() {
		return payPolicy;
	}
	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}
	public boolean isYearly() {
		return isYearly;
	}
	public void setYearly(boolean isYearly) {
		this.isYearly = isYearly;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public boolean isFreeze() {
		return isFreeze;
	}
	public void setFreeze(boolean isFreeze) {
		this.isFreeze = isFreeze;
	}
}
