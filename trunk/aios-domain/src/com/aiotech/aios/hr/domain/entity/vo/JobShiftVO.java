package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobShift;

public class JobShiftVO extends JobShift{
	private JobAssignment jobAssignment;
	private List<JobAssignment> jobAssignments;
	private JobShift jobShift;
	private String employeeName;
	private String locationName;
	private String dateView;
	private String startTime;
	private String endTime;
	private String shiftName;
	private String dayName;
	private Date date;
	private String shiftShortName;
	private boolean isOff;
	private String offType;
	public JobAssignment getJobAssignment() {
		return jobAssignment;
	}
	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}
	public List<JobAssignment> getJobAssignments() {
		return jobAssignments;
	}
	public void setJobAssignments(List<JobAssignment> jobAssignments) {
		this.jobAssignments = jobAssignments;
	}
	public JobShift getJobShift() {
		return jobShift;
	}
	public void setJobShift(JobShift jobShift) {
		this.jobShift = jobShift;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public String getDayName() {
		return dayName;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	public String getDateView() {
		return dateView;
	}
	public void setDateView(String dateView) {
		this.dateView = dateView;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getShiftShortName() {
		return shiftShortName;
	}
	public void setShiftShortName(String shiftShortName) {
		this.shiftShortName = shiftShortName;
	}
	public boolean isOff() {
		return isOff;
	}
	public void setOff(boolean isOff) {
		this.isOff = isOff;
	}
	public String getOffType() {
		return offType;
	}
	public void setOffType(String offType) {
		this.offType = offType;
	}
}
