package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.EosPolicy;

public class EosPolicyVO extends EosPolicy{
	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
