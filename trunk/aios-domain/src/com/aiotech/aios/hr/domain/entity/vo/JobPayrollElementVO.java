package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.JobPayrollElement;

public class JobPayrollElementVO extends JobPayrollElement{
	private String allowanceUrl;
	private Byte payPolicy;
	private Byte payType;
	private Long jobAssignmentId;
	private String payMonth;	
	private String payYear;
	private String allowedMaixmumAmount;
	private String availableAmount;
	private String periodStartDate;
	private String periodEndDate;
	private String elementName;
	private boolean useCalculationType;
	private Double consolidatedAmount;
	private Long payrollElementId;
	//view section
	private String calculationTypeView;
	public String getAllowanceUrl() {
		return allowanceUrl;
	}
	public void setAllowanceUrl(String allowanceUrl) {
		this.allowanceUrl = allowanceUrl;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public Byte getPayPolicy() {
		return payPolicy;
	}
	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}
	public Byte getPayType() {
		return payType;
	}
	public void setPayType(Byte payType) {
		this.payType = payType;
	}
	public String getPayMonth() {
		return payMonth;
	}
	public void setPayMonth(String payMonth) {
		this.payMonth = payMonth;
	}
	public String getPayYear() {
		return payYear;
	}
	public void setPayYear(String payYear) {
		this.payYear = payYear;
	}
	public String getAllowedMaixmumAmount() {
		return allowedMaixmumAmount;
	}
	public void setAllowedMaixmumAmount(String allowedMaixmumAmount) {
		this.allowedMaixmumAmount = allowedMaixmumAmount;
	}
	public String getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(String availableAmount) {
		this.availableAmount = availableAmount;
	}
	public String getPeriodStartDate() {
		return periodStartDate;
	}
	public void setPeriodStartDate(String periodStartDate) {
		this.periodStartDate = periodStartDate;
	}
	public String getPeriodEndDate() {
		return periodEndDate;
	}
	public void setPeriodEndDate(String periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public boolean isUseCalculationType() {
		return useCalculationType;
	}
	public void setUseCalculationType(boolean useCalculationType) {
		this.useCalculationType = useCalculationType;
	}
	public String getCalculationTypeView() {
		return calculationTypeView;
	}
	public void setCalculationTypeView(String calculationTypeView) {
		this.calculationTypeView = calculationTypeView;
	}
	public Double getConsolidatedAmount() {
		return consolidatedAmount;
	}
	public void setConsolidatedAmount(Double consolidatedAmount) {
		this.consolidatedAmount = consolidatedAmount;
	}
	public Long getPayrollElementId() {
		return payrollElementId;
	}
	public void setPayrollElementId(Long payrollElementId) {
		this.payrollElementId = payrollElementId;
	}
}
