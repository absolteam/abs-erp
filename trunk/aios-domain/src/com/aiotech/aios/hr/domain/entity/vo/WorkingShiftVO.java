package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.WorkingShift;

public class WorkingShiftVO extends WorkingShift{
	private String startTimeStr;
	private String endTimeStr;
	private String lateAttendanceStr;
	private String seviorLateAttendanceStr;
	private String bufferTimeStr;
	private String breakHoursStr;
	private String isActiveStr;
	private String isDefaultStr;
	private String breakStartStr;
	private String breakEndStr;
	public String getStartTimeStr() {
		return startTimeStr;
	}
	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}
	public String getEndTimeStr() {
		return endTimeStr;
	}
	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}
	public String getLateAttendanceStr() {
		return lateAttendanceStr;
	}
	public void setLateAttendanceStr(String lateAttendanceStr) {
		this.lateAttendanceStr = lateAttendanceStr;
	}
	public String getSeviorLateAttendanceStr() {
		return seviorLateAttendanceStr;
	}
	public void setSeviorLateAttendanceStr(String seviorLateAttendanceStr) {
		this.seviorLateAttendanceStr = seviorLateAttendanceStr;
	}
	public String getBufferTimeStr() {
		return bufferTimeStr;
	}
	public void setBufferTimeStr(String bufferTimeStr) {
		this.bufferTimeStr = bufferTimeStr;
	}
	public String getBreakHoursStr() {
		return breakHoursStr;
	}
	public void setBreakHoursStr(String breakHoursStr) {
		this.breakHoursStr = breakHoursStr;
	}
	public String getIsActiveStr() {
		return isActiveStr;
	}
	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}
	public String getIsDefaultStr() {
		return isDefaultStr;
	}
	public void setIsDefaultStr(String isDefaultStr) {
		this.isDefaultStr = isDefaultStr;
	}
	
	public String getBreakStartStr() {
		return breakStartStr;
	}
	public void setBreakStartStr(String breakStartStr) {
		this.breakStartStr = breakStartStr;
	}
	public String getBreakEndStr() {
		return breakEndStr;
	}
	public void setBreakEndStr(String breakEndStr) {
		this.breakEndStr = breakEndStr;
	}

}
