package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * QuestionBank generated by hbm2java
 */
@Entity
@Table(name = "hr_question_bank", catalog = "aios")
public class QuestionBank implements java.io.Serializable {

	private Long questionBankId;
	private Designation designation;
	private Implementation implementation;
	private String questionType;
	private Byte questionPattern;
	private String description;
	private Set<QuestionInfo> questionInfos = new HashSet<QuestionInfo>(0);
	private Set<Interview> interviews = new HashSet<Interview>(0);
	private Byte isApprove;
	private Long relativeId;
	
	public QuestionBank() {
	}

	public QuestionBank(String questionType) {
		this.questionType = questionType;
	}

	public QuestionBank(Designation designation, Implementation implementation,
			String questionType, Byte questionPattern, String description,
			Set<QuestionInfo> questionInfos, Set<Interview> interviews,
			Byte isApprove,Long relativeId) {
		this.designation = designation;
		this.implementation = implementation;
		this.questionType = questionType;
		this.questionPattern = questionPattern;
		this.description = description;
		this.questionInfos = questionInfos;
		this.interviews = interviews;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "question_bank_id", unique = true, nullable = false)
	public Long getQuestionBankId() {
		return this.questionBankId;
	}

	public void setQuestionBankId(Long questionBankId) {
		this.questionBankId = questionBankId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "designation_id")
	public Designation getDesignation() {
		return this.designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id")
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@Column(name = "question_type", nullable = false, length = 500)
	public String getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	@Column(name = "question_pattern")
	public Byte getQuestionPattern() {
		return this.questionPattern;
	}

	public void setQuestionPattern(Byte questionPattern) {
		this.questionPattern = questionPattern;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questionBank")
	public Set<QuestionInfo> getQuestionInfos() {
		return this.questionInfos;
	}

	public void setQuestionInfos(Set<QuestionInfo> questionInfos) {
		this.questionInfos = questionInfos;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "questionBank")
	public Set<Interview> getInterviews() {
		return this.interviews;
	}

	public void setInterviews(Set<Interview> interviews) {
		this.interviews = interviews;
	}
	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

}
