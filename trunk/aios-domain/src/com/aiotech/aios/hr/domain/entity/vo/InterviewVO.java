package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.Interview;
import com.aiotech.aios.hr.domain.entity.InterviewProcess;
import com.aiotech.aios.system.domain.entity.Alert;

public class InterviewVO extends Interview{

	private String questionType;
	private String room;
	private String address;
	private String interviewDateView;
	private String interviewTimeView;
	private String roundView;
	private String statusView;
	private String createdDateView;
	private String designationName;
	private String gradeName;
	private String companyName;
	private String departmentName;
	private String locationName;
	private String positionReferenceNumber;
	private OpenPositionVO openPositionVO;
	private CandidateVO candidateVO;
	private	PersonVO personVO;
	private QuestionBankVO questionBankVO;
	private String questionPatternView;
	private List<CandidateVO> candidates;
	private List<JobAssignmentVO> interviewers;
	private String candidatesArray;
	private String interviewersArray;
	private List<Alert> alerts;
	private InterviewProcess interviewProcess;
	private String averateRateing;
	private String totalScore;
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getRoundView() {
		return roundView;
	}
	public void setRoundView(String roundView) {
		this.roundView = roundView;
	}
	public String getStatusView() {
		return statusView;
	}
	public void setStatusView(String statusView) {
		this.statusView = statusView;
	}
	public String getCreatedDateView() {
		return createdDateView;
	}
	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getPositionReferenceNumber() {
		return positionReferenceNumber;
	}
	public void setPositionReferenceNumber(String positionReferenceNumber) {
		this.positionReferenceNumber = positionReferenceNumber;
	}
	public String getInterviewDateView() {
		return interviewDateView;
	}
	public void setInterviewDateView(String interviewDateView) {
		this.interviewDateView = interviewDateView;
	}
	public String getInterviewTimeView() {
		return interviewTimeView;
	}
	public void setInterviewTimeView(String interviewTimeView) {
		this.interviewTimeView = interviewTimeView;
	}
	public OpenPositionVO getOpenPositionVO() {
		return openPositionVO;
	}
	public void setOpenPositionVO(OpenPositionVO openPositionVO) {
		this.openPositionVO = openPositionVO;
	}
	public CandidateVO getCandidateVO() {
		return candidateVO;
	}
	public void setCandidateVO(CandidateVO candidateVO) {
		this.candidateVO = candidateVO;
	}
	public PersonVO getPersonVO() {
		return personVO;
	}
	public void setPersonVO(PersonVO personVO) {
		this.personVO = personVO;
	}
	public QuestionBankVO getQuestionBankVO() {
		return questionBankVO;
	}
	public void setQuestionBankVO(QuestionBankVO questionBankVO) {
		this.questionBankVO = questionBankVO;
	}
	public String getQuestionPatternView() {
		return questionPatternView;
	}
	public void setQuestionPatternView(String questionPatternView) {
		this.questionPatternView = questionPatternView;
	}
	public List<CandidateVO> getCandidates() {
		return candidates;
	}
	public void setCandidates(List<CandidateVO> candidates) {
		this.candidates = candidates;
	}
	public List<JobAssignmentVO> getInterviewers() {
		return interviewers;
	}
	public void setInterviewers(List<JobAssignmentVO> interviewers) {
		this.interviewers = interviewers;
	}
	public String getCandidatesArray() {
		return candidatesArray;
	}
	public void setCandidatesArray(String candidatesArray) {
		this.candidatesArray = candidatesArray;
	}
	public String getInterviewersArray() {
		return interviewersArray;
	}
	public void setInterviewersArray(String interviewersArray) {
		this.interviewersArray = interviewersArray;
	}
	public List<Alert> getAlerts() {
		return alerts;
	}
	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}
	public InterviewProcess getInterviewProcess() {
		return interviewProcess;
	}
	public void setInterviewProcess(InterviewProcess interviewProcess) {
		this.interviewProcess = interviewProcess;
	}
	public String getAverateRateing() {
		return averateRateing;
	}
	public void setAverateRateing(String averateRateing) {
		this.averateRateing = averateRateing;
	}
	public String getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}

}
