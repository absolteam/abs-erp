package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.Job;

public class JobVO extends Job{
	
	private String leaveList;
	private String payList;
	private String shiftList;
	private String assignmentList;
	public String getLeaveList() {
		return leaveList;
	}
	public void setLeaveList(String leaveList) {
		this.leaveList = leaveList;
	}
	public String getPayList() {
		return payList;
	}
	public void setPayList(String payList) {
		this.payList = payList;
	}
	public String getShiftList() {
		return shiftList;
	}
	public void setShiftList(String shiftList) {
		this.shiftList = shiftList;
	}
	public String getAssignmentList() {
		return assignmentList;
	}
	public void setAssignmentList(String assignmentList) {
		this.assignmentList = assignmentList;
	}
	

}
