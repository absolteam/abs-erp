package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.OpenPosition;

public class OpenPositionVO extends OpenPosition{
	private String designationName;
	private String gradeName;
	private String companyName;
	private String departmentName;
	private String locationName;
	private Integer numberOfCount;
	private String statusDisplay;
	private String createdDateDisplay;
	private String dateOpenedDisplay;
	private String targetDateDisplay;
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public Integer getNumberOfCount() {
		return numberOfCount;
	}
	public void setNumberOfCount(Integer numberOfCount) {
		this.numberOfCount = numberOfCount;
	}
	public String getStatusDisplay() {
		return statusDisplay;
	}
	public void setStatusDisplay(String statusDisplay) {
		this.statusDisplay = statusDisplay;
	}
	public String getCreatedDateDisplay() {
		return createdDateDisplay;
	}
	public void setCreatedDateDisplay(String createdDateDisplay) {
		this.createdDateDisplay = createdDateDisplay;
	}
	public String getDateOpenedDisplay() {
		return dateOpenedDisplay;
	}
	public void setDateOpenedDisplay(String dateOpenedDisplay) {
		this.dateOpenedDisplay = dateOpenedDisplay;
	}
	public String getTargetDateDisplay() {
		return targetDateDisplay;
	}
	public void setTargetDateDisplay(String targetDateDisplay) {
		this.targetDateDisplay = targetDateDisplay;
	}
	
}
