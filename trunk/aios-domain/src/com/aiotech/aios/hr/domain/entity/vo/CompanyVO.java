package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Customer;
import com.aiotech.aios.accounts.domain.entity.ShippingDetail;
import com.aiotech.aios.accounts.domain.entity.Supplier;
import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Company;

public class CompanyVO extends Company {

	private String companyNameArabicStr;
	private int companyTypeId;
	private String companyTypeName;
	private String description;
	private long tradeId;
	private String tradeName;
	private String id;
	private String companyOrigin;

	private String companyAddress;
	private String companyPobox;
	private String companyFax;
	private String companyPhone;
	private String accountNumber;
	private String bankAccountId;
	private String labourCardId;
	private Long tenantCombinationId;
	private Long supplierCombinationId;
	private Long customerCombinationId;
	private Long ownerCombinationId;
	private Supplier supplier;
	private Customer customer;
	private Long currencyId; 
	private List<ShippingDetail> shippingDetailList;
	private Company companyEdit;
	private CmpDeptLoc cmpDeptLocation;
	private String companyTypes;
	private long identityId;
	private int identityType;
	private String identityTypeName;
	private String identityNumber;
	private String issuedPlace;
	private String expireDate;
	private String daysLeft;
	private String identityCode;
	private String companyPersonId;
	
	public String getCompanyAddress() {
		return companyAddress;
	}
	public String getCompanyPobox() {
		return companyPobox;
	}
	public String getCompanyFax() {
		return companyFax;
	}
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public void setCompanyPobox(String companyPobox) {
		this.companyPobox = companyPobox;
	}
	public void setCompanyFax(String companyFax) {
		this.companyFax = companyFax;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	
	public int getCompanyTypeId() {
		return companyTypeId;
	}
	public void setCompanyTypeId(int companyTypeId) {
		this.companyTypeId = companyTypeId;
	}
	public String getCompanyTypeName() {
		return companyTypeName;
	}
	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getTradeId() {
		return tradeId;
	}
	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	

	public String getCompanyOrigin() {
		return companyOrigin;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(String bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getLabourCardId() {
		return labourCardId;
	}
	public void setLabourCardId(String labourCardId) {
		this.labourCardId = labourCardId;
	}
	public String getCompanyNameArabicStr() {
		return companyNameArabicStr;
	}
	public void setCompanyNameArabicStr(String companyNameArabicStr) {
		this.companyNameArabicStr = companyNameArabicStr;
	}
	public Long getTenantCombinationId() {
		return tenantCombinationId;
	}
	public void setTenantCombinationId(Long tenantCombinationId) {
		this.tenantCombinationId = tenantCombinationId;
	}
	public Long getSupplierCombinationId() {
		return supplierCombinationId;
	}
	public void setSupplierCombinationId(Long supplierCombinationId) {
		this.supplierCombinationId = supplierCombinationId;
	}
	public Long getCustomerCombinationId() {
		return customerCombinationId;
	}
	public void setCustomerCombinationId(Long customerCombinationId) {
		this.customerCombinationId = customerCombinationId;
	}
	public Long getOwnerCombinationId() {
		return ownerCombinationId;
	}
	public void setOwnerCombinationId(Long ownerCombinationId) {
		this.ownerCombinationId = ownerCombinationId;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}
	public List<ShippingDetail> getShippingDetailList() {
		return shippingDetailList;
	}
	public void setShippingDetailList(List<ShippingDetail> shippingDetailList) {
		this.shippingDetailList = shippingDetailList;
	}
	public void setCompanyOrigin(String companyOrigin) {
		this.companyOrigin = companyOrigin;
	}
	public Company getCompanyEdit() {
		return companyEdit;
	}
	public void setCompanyEdit(Company companyEdit) {
		this.companyEdit = companyEdit;
	}
	public CmpDeptLoc getCmpDeptLocation() {
		return cmpDeptLocation;
	}
	public void setCmpDeptLocation(CmpDeptLoc cmpDeptLocation) {
		this.cmpDeptLocation = cmpDeptLocation;
	}
	public String getCompanyTypes() {
		return companyTypes;
	}
	public void setCompanyTypes(String companyTypes) {
		this.companyTypes = companyTypes;
	}
	public long getIdentityId() {
		return identityId;
	}
	public void setIdentityId(long identityId) {
		this.identityId = identityId;
	}
	public int getIdentityType() {
		return identityType;
	}
	public void setIdentityType(int identityType) {
		this.identityType = identityType;
	}
	public String getIdentityTypeName() {
		return identityTypeName;
	}
	public void setIdentityTypeName(String identityTypeName) {
		this.identityTypeName = identityTypeName;
	}
	public String getIdentityNumber() {
		return identityNumber;
	}
	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}
	public String getIssuedPlace() {
		return issuedPlace;
	}
	public void setIssuedPlace(String issuedPlace) {
		this.issuedPlace = issuedPlace;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getDaysLeft() {
		return daysLeft;
	}
	public void setDaysLeft(String daysLeft) {
		this.daysLeft = daysLeft;
	}
	public String getIdentityCode() {
		return identityCode;
	}
	public void setIdentityCode(String identityCode) {
		this.identityCode = identityCode;
	}
	public String getCompanyPersonId() {
		return companyPersonId;
	}
	public void setCompanyPersonId(String companyPersonId) {
		this.companyPersonId = companyPersonId;
	}
	
}
