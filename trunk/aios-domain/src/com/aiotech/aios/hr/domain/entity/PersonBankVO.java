/**
 * 
 */
package com.aiotech.aios.hr.domain.entity;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

/**
 * @author Usman
 * 
 */
public class PersonBankVO extends PersonBank {

	private String accountHolderName;

	public PersonBankVO(PersonBank pb) {
		this.setAccountNumber(pb.getAccountNumber());
		this.setAccountTitle(pb.getAccountTitle());
		this.setBankCode(pb.getBankCode());
		this.setBankName(pb.getBankName());
		this.setBranchCity(pb.getBranchCity());
		this.setBranchName(pb.getBranchName());
		this.setCompany(pb.getCompany());
		this.setDescription(pb.getDescription());
		this.setIban(pb.getIban());
		this.setImplementation(pb.getImplementation());
		this.setIsActive(pb.getIsActive());
		this.setPerson(pb.getPerson());
		this.setPersonBankId(pb.getPersonBankId());
		this.setRoutingCode(pb.getRoutingCode());

		if (this.getPerson() != null) {
			this.accountHolderName = this.getPerson().getFirstName() + " "
					+ this.getPerson().getLastName();
		} else {
			this.accountHolderName = this.getCompany().getCompanyName();
		}

	}

	// =============Getters Setters=================
	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

}
