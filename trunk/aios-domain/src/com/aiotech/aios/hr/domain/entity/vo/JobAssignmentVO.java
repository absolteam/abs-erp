package com.aiotech.aios.hr.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.JobAssignment;

public class JobAssignmentVO extends JobAssignment {
	private String departmentLocationName;
	private String designationName;
	private String designationNameAR;
	private String jobName;
	private String bankNameAndNumber;
	private String personName;
	private String isActiveStr;
	private String payModeStr;
	private String isLocalPayStr;
	private String effectiveDateStr;
	private String endDateStr;
	private String isApproveStr;
	private String departmentName;
	private String companyName;
	private String locationName;
	private String isPrimaryStr;
	private String mobile;
	private String email;
	private String passportNumber;
	private Double crossSalary;
	private PersonVO personVO;
	private String accomodation;
	private String accomodationAR;
	private String salaryDetails;
	private String salaryDetailsAR;
	private String rejoiningDateStr;
	private List<LeaveReconciliationVO> leaveReconciliationVOs;
	private List<JobShiftVO> jobShiftVOs=new ArrayList<JobShiftVO>();
	private JobVO jobVO;

	public String getDepartmentLocationName() {
		return departmentLocationName;
	}

	public void setDepartmentLocationName(String departmentLocationName) {
		this.departmentLocationName = departmentLocationName;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getBankNameAndNumber() {
		return bankNameAndNumber;
	}

	public void setBankNameAndNumber(String bankNameAndNumber) {
		this.bankNameAndNumber = bankNameAndNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getIsActiveStr() {
		return isActiveStr;
	}

	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}

	public String getPayModeStr() {
		return payModeStr;
	}

	public void setPayModeStr(String payModeStr) {
		this.payModeStr = payModeStr;
	}

	public String getIsLocalPayStr() {
		return isLocalPayStr;
	}

	public void setIsLocalPayStr(String isLocalPayStr) {
		this.isLocalPayStr = isLocalPayStr;
	}

	public String getEffectiveDateStr() {
		return effectiveDateStr;
	}

	public void setEffectiveDateStr(String effectiveDateStr) {
		this.effectiveDateStr = effectiveDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getIsApproveStr() {
		return isApproveStr;
	}

	public void setIsApproveStr(String isApproveStr) {
		this.isApproveStr = isApproveStr;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<LeaveReconciliationVO> getLeaveReconciliationVOs() {
		return leaveReconciliationVOs;
	}

	public void setLeaveReconciliationVOs(
			List<LeaveReconciliationVO> leaveReconciliationVOs) {
		this.leaveReconciliationVOs = leaveReconciliationVOs;
	}

	public String getIsPrimaryStr() {
		return isPrimaryStr;
	}

	public void setIsPrimaryStr(String isPrimaryStr) {
		this.isPrimaryStr = isPrimaryStr;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Double getCrossSalary() {
		return crossSalary;
	}

	public void setCrossSalary(Double crossSalary) {
		this.crossSalary = crossSalary;
	}

	public PersonVO getPersonVO() {
		return personVO;
	}

	public void setPersonVO(PersonVO personVO) {
		this.personVO = personVO;
	}

	public String getDesignationNameAR() {
		return designationNameAR;
	}

	public void setDesignationNameAR(String designationNameAR) {
		this.designationNameAR = designationNameAR;
	}

	public String getAccomodation() {
		return accomodation;
	}

	public void setAccomodation(String accomodation) {
		this.accomodation = accomodation;
	}

	public String getAccomodationAR() {
		return accomodationAR;
	}

	public void setAccomodationAR(String accomodationAR) {
		this.accomodationAR = accomodationAR;
	}

	public String getSalaryDetails() {
		return salaryDetails;
	}

	public void setSalaryDetails(String salaryDetails) {
		this.salaryDetails = salaryDetails;
	}

	public String getSalaryDetailsAR() {
		return salaryDetailsAR;
	}

	public void setSalaryDetailsAR(String salaryDetailsAR) {
		this.salaryDetailsAR = salaryDetailsAR;
	}

	public JobVO getJobVO() {
		return jobVO;
	}

	public void setJobVO(JobVO jobVO) {
		this.jobVO = jobVO;
	}

	public String getRejoiningDateStr() {
		return rejoiningDateStr;
	}

	public void setRejoiningDateStr(String rejoiningDateStr) {
		this.rejoiningDateStr = rejoiningDateStr;
	}

	public List<JobShiftVO> getJobShiftVOs() {
		return jobShiftVOs;
	}

	public void setJobShiftVOs(List<JobShiftVO> jobShiftVOs) {
		this.jobShiftVOs = jobShiftVOs;
	}
}
