package com.aiotech.aios.hr.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Candidate;
import com.aiotech.aios.hr.domain.entity.CandidateOffer;

public class CandidateVO extends Candidate{

	private String createdDateDisplay;
	private String statusDisplay;
	private String expectedJoiningDateDisplay;
	private String availability;
	private String providerName;
	private String positionReferenceNumber;
	private String candidateName;
	private String sourceType;
	private String advertisingReference;
	private PersonVO personVO; 
	private String doShortList;
	private String rating;
	private String score;
	private Integer ratingPoints;
	private InterviewVO interviewVO;
	private Long candidateOfferId; 
	private String remarks;
	private CandidateOffer candidateOffer;
	private JobVO jobVO;
	
	//Offer letter Salary & Leave pojo methods
	private String elementName;
	private String amount;
	private String leaveType;
	private String days;
	private String term;
	private String netAmount;
	private String salaryPeriod;
	private String address;
	private String startTime;
	private String endTime;
	private String irregularDay;
	private List<CandidateVO> salaryElements=new ArrayList<CandidateVO>();
	private List<CandidateVO> leaves=new ArrayList<CandidateVO>();
	private List<CandidateVO> shifts=new ArrayList<CandidateVO>();
	public String getCreatedDateDisplay() {
		return createdDateDisplay;
	}
	public void setCreatedDateDisplay(String createdDateDisplay) {
		this.createdDateDisplay = createdDateDisplay;
	}
	public String getStatusDisplay() {
		return statusDisplay;
	}
	public void setStatusDisplay(String statusDisplay) {
		this.statusDisplay = statusDisplay;
	}
	public String getExpectedJoiningDateDisplay() {
		return expectedJoiningDateDisplay;
	}
	public void setExpectedJoiningDateDisplay(String expectedJoiningDateDisplay) {
		this.expectedJoiningDateDisplay = expectedJoiningDateDisplay;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public String getPositionReferenceNumber() {
		return positionReferenceNumber;
	}
	public void setPositionReferenceNumber(String positionReferenceNumber) {
		this.positionReferenceNumber = positionReferenceNumber;
	}
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	public String getAdvertisingReference() {
		return advertisingReference;
	}
	public void setAdvertisingReference(String advertisingReference) {
		this.advertisingReference = advertisingReference;
	}
	public PersonVO getPersonVO() {
		return personVO;
	}
	public void setPersonVO(PersonVO personVO) {
		this.personVO = personVO;
	}
	public String getDoShortList() {
		return doShortList;
	}
	public void setDoShortList(String doShortList) {
		this.doShortList = doShortList;
	}
	public InterviewVO getInterviewVO() {
		return interviewVO;
	}
	public void setInterviewVO(InterviewVO interviewVO) {
		this.interviewVO = interviewVO;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public Integer getRatingPoints() {
		return ratingPoints;
	}
	public void setRatingPoints(Integer ratingPoints) {
		this.ratingPoints = ratingPoints;
	}
	public Long getCandidateOfferId() {
		return candidateOfferId;
	}
	public void setCandidateOfferId(Long candidateOfferId) {
		this.candidateOfferId = candidateOfferId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public CandidateOffer getCandidateOffer() {
		return candidateOffer;
	}
	public void setCandidateOffer(CandidateOffer candidateOffer) {
		this.candidateOffer = candidateOffer;
	}
	public JobVO getJobVO() {
		return jobVO;
	}
	public void setJobVO(JobVO jobVO) {
		this.jobVO = jobVO;
	}
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}
	public String getSalaryPeriod() {
		return salaryPeriod;
	}
	public void setSalaryPeriod(String salaryPeriod) {
		this.salaryPeriod = salaryPeriod;
	}
	public List<CandidateVO> getSalaryElements() {
		return salaryElements;
	}
	public void setSalaryElements(List<CandidateVO> salaryElements) {
		this.salaryElements = salaryElements;
	}
	public List<CandidateVO> getLeaves() {
		return leaves;
	}
	public void setLeaves(List<CandidateVO> leaves) {
		this.leaves = leaves;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<CandidateVO> getShifts() {
		return shifts;
	}
	public void setShifts(List<CandidateVO> shifts) {
		this.shifts = shifts;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getIrregularDay() {
		return irregularDay;
	}
	public void setIrregularDay(String irregularDay) {
		this.irregularDay = irregularDay;
	}
	
}
