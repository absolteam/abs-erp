package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;

import com.aiotech.aios.hr.domain.entity.EosPolicy;
import com.aiotech.aios.hr.domain.entity.Payroll;
import com.aiotech.aios.hr.domain.entity.ResignationTermination;
import com.aiotech.aios.system.domain.entity.Implementation;

public class ResignationTerminationVO extends ResignationTermination {

	private String effectiveDateView;
	private String createdDateView;
	private String requestedDateView;
	private String fromDateView;
	private String toDateView;
	private String updatedDateView;
	private String currentPosition;
	private String typeView;
	private String requestedBy;
	private String employeeName;
	private String jobNumber;
	private String resignationStatus;
	private String actionView;
	private Long jobAssignmentId;
	private Date fromDate;
	private Date toDate;
	private int totalMonths;
	private Double totalYears;
	private int totalDays;
	private EosPolicy eosPolicy;
	private Long gradeId;
	private int gratuityDays;
	private Double perDayBasic;
	private Double totalAmount;
	private String totalAmountView;
	private Double basicAmount;
	private Implementation implementation;
	private String designationName;
	private String personNumber;
	private String companyName;
	private String departmentName;
	private String personName;
	private String locationName;
	private Long previousPayPeriod;
	private Payroll payroll;
	
	public String getEffectiveDateView() {
		return effectiveDateView;
	}

	public void setEffectiveDateView(String effectiveDateView) {
		this.effectiveDateView = effectiveDateView;
	}

	public String getCreatedDateView() {
		return createdDateView;
	}

	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}

	public String getRequestedDateView() {
		return requestedDateView;
	}

	public void setRequestedDateView(String requestedDateView) {
		this.requestedDateView = requestedDateView;
	}

	public String getFromDateView() {
		return fromDateView;
	}

	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}

	public String getToDateView() {
		return toDateView;
	}

	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}

	public String getUpdatedDateView() {
		return updatedDateView;
	}

	public void setUpdatedDateView(String updatedDateView) {
		this.updatedDateView = updatedDateView;
	}

	public String getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(String currentPosition) {
		this.currentPosition = currentPosition;
	}

	public String getTypeView() {
		return typeView;
	}

	public void setTypeView(String typeView) {
		this.typeView = typeView;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getResignationStatus() {
		return resignationStatus;
	}

	public void setResignationStatus(String resignationStatus) {
		this.resignationStatus = resignationStatus;
	}

	public String getActionView() {
		return actionView;
	}

	public void setActionView(String actionView) {
		this.actionView = actionView;
	}

	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}

	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public int getTotalMonths() {
		return totalMonths;
	}

	public void setTotalMonths(int totalMonths) {
		this.totalMonths = totalMonths;
	}

	public EosPolicy getEosPolicy() {
		return eosPolicy;
	}

	public void setEosPolicy(EosPolicy eosPolicy) {
		this.eosPolicy = eosPolicy;
	}

	public Long getGradeId() {
		return gradeId;
	}

	public void setGradeId(Long gradeId) {
		this.gradeId = gradeId;
	}


	public Implementation getImplementation() {
		return implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	public Double getTotalYears() {
		return totalYears;
	}

	public void setTotalYears(Double totalYears) {
		this.totalYears = totalYears;
	}

	public int getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(int totalDays) {
		this.totalDays = totalDays;
	}

	public int getGratuityDays() {
		return gratuityDays;
	}

	public void setGratuityDays(int gratuityDays) {
		this.gratuityDays = gratuityDays;
	}

	public Double getPerDayBasic() {
		return perDayBasic;
	}

	public void setPerDayBasic(Double perDayBasic) {
		this.perDayBasic = perDayBasic;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getBasicAmount() {
		return basicAmount;
	}

	public void setBasicAmount(Double basicAmount) {
		this.basicAmount = basicAmount;
	}

	public String getTotalAmountView() {
		return totalAmountView;
	}

	public void setTotalAmountView(String totalAmountView) {
		this.totalAmountView = totalAmountView;
	}

	public String getDesignationName() {
		return designationName;
	}

	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}

	public String getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Long getPreviousPayPeriod() {
		return previousPayPeriod;
	}

	public void setPreviousPayPeriod(Long previousPayPeriod) {
		this.previousPayPeriod = previousPayPeriod;
	}

	public Payroll getPayroll() {
		return payroll;
	}

	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}
}
