package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.Leave;

public class LeaveVO extends Leave{
	private String isMonthlyStr;
	private String isYearlyStr;
	private String isServiceStr;
	private String isPaidStr;
	private String isCarryForwardStr;
	private String canEncashStr;
	private String isActiveStr;
	private String createdPerson;
	private String createdDateStr;
	private String updatedPerson;
	private String updatedDateStr;
	private String leaveCode;
	private String leaveType;
	private Long leaveTypeId;
	public String getIsMonthlyStr() {
		return isMonthlyStr;
	}
	public void setIsMonthlyStr(String isMonthlyStr) {
		this.isMonthlyStr = isMonthlyStr;
	}
	public String getIsYearlyStr() {
		return isYearlyStr;
	}
	public void setIsYearlyStr(String isYearlyStr) {
		this.isYearlyStr = isYearlyStr;
	}
	public String getIsServiceStr() {
		return isServiceStr;
	}
	public void setIsServiceStr(String isServiceStr) {
		this.isServiceStr = isServiceStr;
	}
	public String getIsPaidStr() {
		return isPaidStr;
	}
	public void setIsPaidStr(String isPaidStr) {
		this.isPaidStr = isPaidStr;
	}
	public String getIsCarryForwardStr() {
		return isCarryForwardStr;
	}
	public void setIsCarryForwardStr(String isCarryForwardStr) {
		this.isCarryForwardStr = isCarryForwardStr;
	}
	public String getCanEncashStr() {
		return canEncashStr;
	}
	public void setCanEncashStr(String canEncashStr) {
		this.canEncashStr = canEncashStr;
	}
	public String getIsActiveStr() {
		return isActiveStr;
	}
	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}
	public String getCreatedPerson() {
		return createdPerson;
	}
	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getUpdatedPerson() {
		return updatedPerson;
	}
	public void setUpdatedPerson(String updatedPerson) {
		this.updatedPerson = updatedPerson;
	}
	public String getUpdatedDateStr() {
		return updatedDateStr;
	}
	public void setUpdatedDateStr(String updatedDateStr) {
		this.updatedDateStr = updatedDateStr;
	}
	public String getLeaveCode() {
		return leaveCode;
	}
	public void setLeaveCode(String leaveCode) {
		this.leaveCode = leaveCode;
	}
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public Long getLeaveTypeId() {
		return leaveTypeId;
	}
	public void setLeaveTypeId(Long leaveTypeId) {
		this.leaveTypeId = leaveTypeId;
	}



}
