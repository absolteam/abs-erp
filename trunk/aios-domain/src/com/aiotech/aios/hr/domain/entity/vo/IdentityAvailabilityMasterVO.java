package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.IdentityAvailabilityMaster;

public class IdentityAvailabilityMasterVO extends IdentityAvailabilityMaster{

	private List<IdentityAvailabilityVO> identityAvailabilityVOs;
	private String handoverDateStr;
	private String expectedReturnDateStr;
	private String actualReturnDateStr;
	private String createdDateStr;
	private String statusStr;
	private String createdPerson;
	private String personName;

	public List<IdentityAvailabilityVO> getIdentityAvailabilityVOs() {
		return identityAvailabilityVOs;
	}

	public void setIdentityAvailabilityVOs(
			List<IdentityAvailabilityVO> identityAvailabilityVOs) {
		this.identityAvailabilityVOs = identityAvailabilityVOs;
	}

	public String getHandoverDateStr() {
		return handoverDateStr;
	}

	public void setHandoverDateStr(String handoverDateStr) {
		this.handoverDateStr = handoverDateStr;
	}

	public String getExpectedReturnDateStr() {
		return expectedReturnDateStr;
	}

	public void setExpectedReturnDateStr(String expectedReturnDateStr) {
		this.expectedReturnDateStr = expectedReturnDateStr;
	}

	public String getActualReturnDateStr() {
		return actualReturnDateStr;
	}

	public void setActualReturnDateStr(String actualReturnDateStr) {
		this.actualReturnDateStr = actualReturnDateStr;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getCreatedPerson() {
		return createdPerson;
	}

	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}
}
