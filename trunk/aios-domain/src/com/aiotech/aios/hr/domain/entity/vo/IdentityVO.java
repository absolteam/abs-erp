/**
 * 
 */
package com.aiotech.aios.hr.domain.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.Identity;
import com.aiotech.utils.common.DateUtil;

/**
 * @author Usman Anwar
 * 
 */
public class IdentityVO extends Identity {

	private String serialNo;
	private String documentType;
	private String documentFrom;
	private String documentOf;

	private String expiryDate;
	private String daysLeft;
	private String documentNumber;
	private String issuedPlace;

	public static List<IdentityVO> convertToVOList(List<Identity> identities) {

		List<IdentityVO> vos = new ArrayList<IdentityVO>();
		try {
			Integer i = 1;
			for (Identity identity : identities) {

				IdentityVO vo = convertToVO(identity);
				vo.setSerialNo(i.toString());
				vos.add(vo);

				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vos;

	}

	public static IdentityVO convertToVO(Identity identity) {

		IdentityVO vo = new IdentityVO();
		try {

			vo.setDocumentType(identity.getLookupDetail().getDisplayName());
			vo.setDocumentFrom(identity.getPerson() != null ? "Person"
					: "Company");
			vo.setDocumentOf(identity.getPerson() != null ? identity
					.getPerson().getFirstName()
					+ " "
					+ identity.getPerson().getLastName() : identity
					.getCompany().getCompanyName());

			vo.setExpiryDate(DateUtil.convertSystemDateToString(identity
					.getExpireDate()));

			if (identity.getExpireDate() != null) {
				vo.setDaysLeft(DateUtil.dayVariance(new Date(),
						identity.getExpireDate()) > 0 ? Long.valueOf(
						DateUtil.dayVariance(new Date(),
								identity.getExpireDate())).toString() : "0");
			}

			vo.setDocumentNumber(identity.getIdentityNumber());
			vo.setIssuedPlace(identity.getIssuedPlace());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}

	// =========Getters Setters=============
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentFrom() {
		return documentFrom;
	}

	public void setDocumentFrom(String documentFrom) {
		this.documentFrom = documentFrom;
	}

	public String getDocumentOf() {
		return documentOf;
	}

	public void setDocumentOf(String documentOf) {
		this.documentOf = documentOf;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDaysLeft() {
		return daysLeft;
	}

	public void setDaysLeft(String daysLeft) {
		this.daysLeft = daysLeft;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getIssuedPlace() {
		return issuedPlace;
	}

	public void setIssuedPlace(String issuedPlace) {
		this.issuedPlace = issuedPlace;
	}

}
