package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * InsuranceCompany generated by hbm2java
 */
@Entity
@Table(name = "hr_insurance_company", catalog = "aios")
public class InsuranceCompany implements java.io.Serializable {

	private Long insuranceCompanyId;
	private Implementation implementation;
	private Person person;
	private LookupDetail lookupDetail;
	private String companyName;
	private String policyNumber;
	private Date periodStart;
	private Date periodEnd;
	private String coPay;
	private String coverage;
	private String exclusion;
	private String agent;
	private String contactDetails;
	private Date createdDate;

	public InsuranceCompany() {
	}

	public InsuranceCompany(Implementation implementation,
			LookupDetail lookupDetail, String companyName, String policyNumber) {
		this.implementation = implementation;
		this.lookupDetail = lookupDetail;
		this.companyName = companyName;
		this.policyNumber = policyNumber;
	}

	public InsuranceCompany(Implementation implementation, Person person,
			LookupDetail lookupDetail, String companyName, String policyNumber,
			Date periodStart, Date periodEnd, String coPay, String coverage,
			String exclusion, String agent, String contactDetails,
			Date createdDate) {
		this.implementation = implementation;
		this.person = person;
		this.lookupDetail = lookupDetail;
		this.companyName = companyName;
		this.policyNumber = policyNumber;
		this.periodStart = periodStart;
		this.periodEnd = periodEnd;
		this.coPay = coPay;
		this.coverage = coverage;
		this.exclusion = exclusion;
		this.agent = agent;
		this.contactDetails = contactDetails;
		this.createdDate = createdDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "insurance_company_id", unique = true, nullable = false)
	public Long getInsuranceCompanyId() {
		return this.insuranceCompanyId;
	}

	public void setInsuranceCompanyId(Long insuranceCompanyId) {
		this.insuranceCompanyId = insuranceCompanyId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "policy_type", nullable = false)
	public LookupDetail getLookupDetail() {
		return this.lookupDetail;
	}

	public void setLookupDetail(LookupDetail lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

	@Column(name = "company_name", nullable = false, length = 150)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "policy_number", nullable = false, length = 65535)
	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "period_start", length = 10)
	public Date getPeriodStart() {
		return this.periodStart;
	}

	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "period_end", length = 10)
	public Date getPeriodEnd() {
		return this.periodEnd;
	}

	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	@Column(name = "co_pay", length = 100)
	public String getCoPay() {
		return this.coPay;
	}

	public void setCoPay(String coPay) {
		this.coPay = coPay;
	}

	@Column(name = "coverage", length = 65535)
	public String getCoverage() {
		return this.coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	@Column(name = "exclusion", length = 65535)
	public String getExclusion() {
		return this.exclusion;
	}

	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}

	@Column(name = "agent", length = 250)
	public String getAgent() {
		return this.agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	@Column(name = "contact_details", length = 65535)
	public String getContactDetails() {
		return this.contactDetails;
	}

	public void setContactDetails(String contactDetails) {
		this.contactDetails = contactDetails;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
