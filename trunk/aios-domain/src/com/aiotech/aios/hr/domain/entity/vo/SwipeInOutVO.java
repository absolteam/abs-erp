package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.SwipeInOut;

/**
 * @author Hasham
 *
 */
public class SwipeInOutVO extends SwipeInOut {

	private List<SwipeInOut> swipeInOuts;
	private JobAssignmentVO jobAssignmentVO;
	private String employeeName;
	private String designation;
	private String strSwipeId;

	public List<SwipeInOut> getSwipeInOuts() {
		return swipeInOuts;
	}

	public void setSwipeInOuts(List<SwipeInOut> swipeInOuts) {
		this.swipeInOuts = swipeInOuts;
	}

	public JobAssignmentVO getJobAssignmentVO() {
		return jobAssignmentVO;
	}

	public void setJobAssignmentVO(JobAssignmentVO jobAssignmentVO) {
		this.jobAssignmentVO = jobAssignmentVO;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getStrSwipeId() {
		return strSwipeId;
	}

	public void setStrSwipeId(String strSwipeId) {
		this.strSwipeId = strSwipeId;
	}
	
}
