package com.aiotech.aios.hr.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.vo.AssetVO;
import com.aiotech.aios.hr.domain.entity.AssetUsage;

public class AssetUsageVO extends AssetUsage{
	private String allocationDate;
	private String returnDate;
	private String employeeName;
	private String companyNDepartmentName;
	private String strAssetStatus;
	private String assetName;
	private String purchaseDateView;
	private String assetTypeView;
	private String assetColorView;
	private AssetVO assetVO;

	public String getAllocationDate() {
		return allocationDate;
	}

	public void setAllocationDate(String allocationDate) {
		this.allocationDate = allocationDate;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCompanyNDepartmentName() {
		return companyNDepartmentName;
	}

	public void setCompanyNDepartmentName(String companyNDepartmentName) {
		this.companyNDepartmentName = companyNDepartmentName;
	}

	public String getStrAssetStatus() {
		return strAssetStatus;
	}

	public void setStrAssetStatus(String strAssetStatus) {
		this.strAssetStatus = strAssetStatus;
	}
	
	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public static AssetUsageVO convertToVO(AssetUsage assetUsage)
	{
		AssetUsageVO assetUsageVO = new AssetUsageVO();
		
		try {
			assetUsageVO.setAllocatedDate(assetUsage.getAllocatedDate());
			assetUsageVO.setAllocationDate(assetUsage.getAllocatedDate().toString());
			assetUsageVO.setAssetStatus(assetUsage.getAssetStatus());
			assetUsageVO.setAssetUsageId(assetUsage.getAssetUsageId());
			assetUsageVO.setCmpDeptLocation(assetUsage.getCmpDeptLocation());
			assetUsageVO.setCreatedDate(assetUsage.getCreatedDate());
			assetUsageVO.setDescription(assetUsage.getDescription());
			assetUsageVO.setPersonByCreatedBy(assetUsage.getPersonByCreatedBy());
			assetUsageVO.setPersonByPersonId(assetUsage.getPersonByPersonId());
			assetUsageVO.setProduct(assetUsage.getProduct());
			assetUsageVO.setReturedDate(assetUsage.getReturedDate());
			if(assetUsage.getReturedDate() != null) {
				assetUsageVO.setReturnDate(assetUsage.getReturedDate().toString());
			} else {
				assetUsageVO.setReturnDate("");
			}
			
			//assetUsageVO.setEmployeeName(employeeName)
			if(assetUsage.getPersonByPersonId()!=null)
				assetUsageVO.setEmployeeName(assetUsage.getPersonByPersonId().getFirstName()+" "+assetUsage.getPersonByPersonId().getLastName()+"");
			else
				assetUsageVO.setEmployeeName("-NA-");
			
			
			if(assetUsage.getCmpDeptLocation()!=null && assetUsage.getCmpDeptLocation().getDepartment()!=null){
				assetUsageVO.setCompanyNDepartmentName(assetUsage.getCmpDeptLocation().getCompany().getCompanyName()
					+"||"+assetUsage.getCmpDeptLocation().getDepartment().getDepartmentName()
					+"||"+assetUsage.getCmpDeptLocation().getLocation().getLocationName());
			}else{
				assetUsageVO.setCompanyNDepartmentName(assetUsage.getCmpDeptLocation().getCompany().getCompanyName()
						+"||"+assetUsage.getCmpDeptLocation().getLocation().getLocationName());
			}
			
			
			
			if(assetUsage.getAssetStatus()!=null && assetUsage.getAssetStatus()==1){
				assetUsageVO.setStrAssetStatus("ALLOCATED");
			}else if(assetUsage.getAssetStatus()!=null && assetUsage.getAssetStatus()==2){
				assetUsageVO.setStrAssetStatus("RETURNED");
			}else if(assetUsage.getAssetStatus()!=null && assetUsage.getAssetStatus()==3){
				assetUsageVO.setStrAssetStatus("DAMAGED");
			}else if(assetUsage.getAssetStatus()!=null && assetUsage.getAssetStatus()==4){
				assetUsageVO.setStrAssetStatus("LOST");
			}else{
				assetUsageVO.setStrAssetStatus("-NA-");
			}
			
			assetUsageVO.setAssetName(assetUsage.getProduct().getProductName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assetUsageVO;
		
	}
	
	public static List<AssetUsageVO> convertToVOList(List<AssetUsage> assetUsages)
	{
		ArrayList<AssetUsageVO> assetUsageVOs = new ArrayList<AssetUsageVO>();
		
		for(AssetUsage asset: assetUsages)
		{
			AssetUsageVO assetVO = convertToVO(asset);
			assetUsageVOs.add(assetVO);
		}
		
		return assetUsageVOs;
	}

	public AssetVO getAssetVO() {
		return assetVO;
	}

	public void setAssetVO(AssetVO assetVO) {
		this.assetVO = assetVO;
	}

	public String getPurchaseDateView() {
		return purchaseDateView;
	}

	public void setPurchaseDateView(String purchaseDateView) {
		this.purchaseDateView = purchaseDateView;
	}

	public String getAssetTypeView() {
		return assetTypeView;
	}

	public void setAssetTypeView(String assetTypeView) {
		this.assetTypeView = assetTypeView;
	}

	public String getAssetColorView() {
		return assetColorView;
	}

	public void setAssetColorView(String assetColorView) {
		this.assetColorView = assetColorView;
	}

}
