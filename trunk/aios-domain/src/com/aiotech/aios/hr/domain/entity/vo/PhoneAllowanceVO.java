package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.PhoneAllowance;

public class PhoneAllowanceVO extends PhoneAllowance{

	private String fromDateView;
	private String toDateView;
	private String isFinanceView;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private String transactionDate;
	private PayPeriodTransaction payPeriodTransaction;
	
	private String networkProvider;
	private String subscriptionType;

	public String getFromDateView() {
		return fromDateView;
	}
	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}
	public String getToDateView() {
		return toDateView;
	}
	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}
	public String getIsFinanceView() {
		return isFinanceView;
	}
	public void setIsFinanceView(String isFinanceView) {
		this.isFinanceView = isFinanceView;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}
	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public PayPeriodTransaction getPayPeriodTransaction() {
		return payPeriodTransaction;
	}
	public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
		this.payPeriodTransaction = payPeriodTransaction;
	}
	public String getNetworkProvider() {
		return networkProvider;
	}
	public void setNetworkProvider(String networkProvider) {
		this.networkProvider = networkProvider;
	}
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
}
