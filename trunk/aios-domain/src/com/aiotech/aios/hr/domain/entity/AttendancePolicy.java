package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AttendancePolicy generated by hbm2java
 */
@Entity
@Table(name = "hr_attendance_policy", catalog = "aios")
public class AttendancePolicy implements java.io.Serializable {

	private Long attendancePolicyId;
	private Implementation implementation;
	private Person personByUpdatedBy;
	private Person personByCreatedBy;
	private Boolean allowOt;
	private Boolean allowCompoff;
	private Date otHoursForHalfday;
	private Date otHoursForFullday;
	private Date compoffHoursForHalfday;
	private Date compoffHoursForFullday;
	private Date lopHoursForHalfday;
	private Date lopHoursForFullday;
	private String processType;
	private String policyName;
	private Boolean isDefalut;
	private Byte attendanceType;
	private Double attendanceDays;
	private Double attendanceHours;
	private Date createdDate;
	private Date updatedDate;
	private Boolean isActive;
	private Boolean hourlyProcess;
	private Boolean allowLop;
	private Set<Grade> grades = new HashSet<Grade>(0);
	private Byte isApprove;
	private Long relativeId;

	public AttendancePolicy() {
	}

	public AttendancePolicy(Implementation implementation, String processType) {
		this.implementation = implementation;
		this.processType = processType;
	}

	public AttendancePolicy(Implementation implementation,
			Person personByUpdatedBy, Person personByCreatedBy,
			Boolean allowOt, Boolean allowCompoff, Date otHoursForHalfday,
			Date otHoursForFullday, Date compoffHoursForHalfday,
			Date compoffHoursForFullday, Date lopHoursForHalfday,
			Date lopHoursForFullday, String processType, String policyName,
			Boolean isDefalut, Byte attendanceType, Double attendanceDays,
			Double attendanceHours, Date createdDate, Date updatedDate,
			Boolean isActive, Boolean hourlyProcess, Boolean allowLop,
			Set<Grade> grades,Byte isApprove,Long relativeId) {
		this.implementation = implementation;
		this.personByUpdatedBy = personByUpdatedBy;
		this.personByCreatedBy = personByCreatedBy;
		this.allowOt = allowOt;
		this.allowCompoff = allowCompoff;
		this.otHoursForHalfday = otHoursForHalfday;
		this.otHoursForFullday = otHoursForFullday;
		this.compoffHoursForHalfday = compoffHoursForHalfday;
		this.compoffHoursForFullday = compoffHoursForFullday;
		this.lopHoursForHalfday = lopHoursForHalfday;
		this.lopHoursForFullday = lopHoursForFullday;
		this.processType = processType;
		this.policyName = policyName;
		this.isDefalut = isDefalut;
		this.attendanceType = attendanceType;
		this.attendanceDays = attendanceDays;
		this.attendanceHours = attendanceHours;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.isActive = isActive;
		this.hourlyProcess = hourlyProcess;
		this.allowLop = allowLop;
		this.grades = grades;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "attendance_policy_id", unique = true, nullable = false)
	public Long getAttendancePolicyId() {
		return this.attendancePolicyId;
	}

	public void setAttendancePolicyId(Long attendancePolicyId) {
		this.attendancePolicyId = attendancePolicyId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_by")
	public Person getPersonByUpdatedBy() {
		return this.personByUpdatedBy;
	}

	public void setPersonByUpdatedBy(Person personByUpdatedBy) {
		this.personByUpdatedBy = personByUpdatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public Person getPersonByCreatedBy() {
		return this.personByCreatedBy;
	}

	public void setPersonByCreatedBy(Person personByCreatedBy) {
		this.personByCreatedBy = personByCreatedBy;
	}

	@Column(name = "allow_ot")
	public Boolean getAllowOt() {
		return this.allowOt;
	}

	public void setAllowOt(Boolean allowOt) {
		this.allowOt = allowOt;
	}

	@Column(name = "allow_compoff")
	public Boolean getAllowCompoff() {
		return this.allowCompoff;
	}

	public void setAllowCompoff(Boolean allowCompoff) {
		this.allowCompoff = allowCompoff;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "ot_hours_for_halfday", length = 8)
	public Date getOtHoursForHalfday() {
		return this.otHoursForHalfday;
	}

	public void setOtHoursForHalfday(Date otHoursForHalfday) {
		this.otHoursForHalfday = otHoursForHalfday;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "ot_hours_for_fullday", length = 8)
	public Date getOtHoursForFullday() {
		return this.otHoursForFullday;
	}

	public void setOtHoursForFullday(Date otHoursForFullday) {
		this.otHoursForFullday = otHoursForFullday;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "compoff_hours_for_halfday", length = 8)
	public Date getCompoffHoursForHalfday() {
		return this.compoffHoursForHalfday;
	}

	public void setCompoffHoursForHalfday(Date compoffHoursForHalfday) {
		this.compoffHoursForHalfday = compoffHoursForHalfday;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "compoff_hours_for_fullday", length = 8)
	public Date getCompoffHoursForFullday() {
		return this.compoffHoursForFullday;
	}

	public void setCompoffHoursForFullday(Date compoffHoursForFullday) {
		this.compoffHoursForFullday = compoffHoursForFullday;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "lop_hours_for_halfday", length = 8)
	public Date getLopHoursForHalfday() {
		return this.lopHoursForHalfday;
	}

	public void setLopHoursForHalfday(Date lopHoursForHalfday) {
		this.lopHoursForHalfday = lopHoursForHalfday;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "lop_hours_for_fullday", length = 8)
	public Date getLopHoursForFullday() {
		return this.lopHoursForFullday;
	}

	public void setLopHoursForFullday(Date lopHoursForFullday) {
		this.lopHoursForFullday = lopHoursForFullday;
	}

	@Column(name = "process_type", nullable = false, length = 5)
	public String getProcessType() {
		return this.processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	@Column(name = "policy_name", length = 150)
	public String getPolicyName() {
		return this.policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	@Column(name = "is_defalut")
	public Boolean getIsDefalut() {
		return this.isDefalut;
	}

	public void setIsDefalut(Boolean isDefalut) {
		this.isDefalut = isDefalut;
	}

	@Column(name = "attendance_type")
	public Byte getAttendanceType() {
		return this.attendanceType;
	}

	public void setAttendanceType(Byte attendanceType) {
		this.attendanceType = attendanceType;
	}

	@Column(name = "attendance_days", precision = 4, scale = 1)
	public Double getAttendanceDays() {
		return this.attendanceDays;
	}

	public void setAttendanceDays(Double attendanceDays) {
		this.attendanceDays = attendanceDays;
	}

	@Column(name = "attendance_hours", precision = 3, scale = 1)
	public Double getAttendanceHours() {
		return this.attendanceHours;
	}

	public void setAttendanceHours(Double attendanceHours) {
		this.attendanceHours = attendanceHours;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 10)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 10)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "is_active")
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "hourly_process")
	public Boolean getHourlyProcess() {
		return this.hourlyProcess;
	}

	public void setHourlyProcess(Boolean hourlyProcess) {
		this.hourlyProcess = hourlyProcess;
	}

	@Column(name = "allow_lop")
	public Boolean getAllowLop() {
		return this.allowLop;
	}

	public void setAllowLop(Boolean allowLop) {
		this.allowLop = allowLop;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "attendancePolicy")
	public Set<Grade> getGrades() {
		return this.grades;
	}

	public void setGrades(Set<Grade> grades) {
		this.grades = grades;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

}
