package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;
import java.util.Map;

import org.joda.time.LocalDate;

import com.aiotech.aios.hr.domain.entity.LeaveProcess;
import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;

public class LeaveProcessVO extends LeaveProcess{
	private String excludedWeekendDays;
	private String excludedHolidays;
	private String totalDays;
	private Date fromDate;
	private Date toDate;
	private LeaveReconciliationVO oldLeaveReconciliationVO;
	private LeaveReconciliation newLeaveReconciliation;
	private Map<LocalDate,LocalDate> weekendMap;
	private Map<LocalDate,LocalDate> holidayMap;
	private Map<LocalDate,LocalDate> leaeveProcessMap;
	private LeaveProcess leaveProcess;
	private Date cutOffDate;
	private Double leaveDaysDeduction;
	
	public String getExcludedWeekendDays() {
		return excludedWeekendDays;
	}

	public void setExcludedWeekendDays(String excludedWeekendDays) {
		this.excludedWeekendDays = excludedWeekendDays;
	}

	public String getExcludedHolidays() {
		return excludedHolidays;
	}

	public void setExcludedHolidays(String excludedHolidays) {
		this.excludedHolidays = excludedHolidays;
	}

	public String getTotalDays() {
		return totalDays;
	}

	public void setTotalDays(String totalDays) {
		this.totalDays = totalDays;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}


	public LeaveReconciliation getNewLeaveReconciliation() {
		return newLeaveReconciliation;
	}

	public void setNewLeaveReconciliation(LeaveReconciliation newLeaveReconciliation) {
		this.newLeaveReconciliation = newLeaveReconciliation;
	}

	public Map<LocalDate, LocalDate> getWeekendMap() {
		return weekendMap;
	}

	public void setWeekendMap(Map<LocalDate, LocalDate> weekendMap) {
		this.weekendMap = weekendMap;
	}

	public Map<LocalDate, LocalDate> getHolidayMap() {
		return holidayMap;
	}

	public void setHolidayMap(Map<LocalDate, LocalDate> holidayMap) {
		this.holidayMap = holidayMap;
	}

	public LeaveProcess getLeaveProcess() {
		return leaveProcess;
	}

	public void setLeaveProcess(LeaveProcess leaveProcess) {
		this.leaveProcess = leaveProcess;
	}

	public Map<LocalDate, LocalDate> getLeaeveProcessMap() {
		return leaeveProcessMap;
	}

	public void setLeaeveProcessMap(Map<LocalDate, LocalDate> leaeveProcessMap) {
		this.leaeveProcessMap = leaeveProcessMap;
	}

	public LeaveReconciliationVO getOldLeaveReconciliationVO() {
		return oldLeaveReconciliationVO;
	}

	public void setOldLeaveReconciliationVO(
			LeaveReconciliationVO oldLeaveReconciliationVO) {
		this.oldLeaveReconciliationVO = oldLeaveReconciliationVO;
	}

	public Date getCutOffDate() {
		return cutOffDate;
	}

	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}

	public Double getLeaveDaysDeduction() {
		return leaveDaysDeduction;
	}

	public void setLeaveDaysDeduction(Double leaveDaysDeduction) {
		this.leaveDaysDeduction = leaveDaysDeduction;
	}

	
}
