package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Department;

public class DepartmentVO extends Department{
	
	private String companyName;
	private String locationName;
	private CmpDeptLoc cmpDeptLocation;
	private String address;
	private String accountName;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public CmpDeptLoc getCmpDeptLocation() {
		return cmpDeptLocation;
	}
	public void setCmpDeptLocation(CmpDeptLoc cmpDeptLocation) {
		this.cmpDeptLocation = cmpDeptLocation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
}
