package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.HouseRentAllowance;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;

public class HouseRentAllowanceVO extends HouseRentAllowance{

	private String fromDateView;
	private String toDateView;
	private String isFinanceView;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private String transactionDate;
	private PayPeriodTransaction payPeriodTransaction;
	private String propertyName;
	
	public String getFromDateView() {
		return fromDateView;
	}
	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}
	public String getToDateView() {
		return toDateView;
	}
	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}
	
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}
	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public PayPeriodTransaction getPayPeriodTransaction() {
		return payPeriodTransaction;
	}
	public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
		this.payPeriodTransaction = payPeriodTransaction;
	}
	public String getIsFinanceView() {
		return isFinanceView;
	}
	public void setIsFinanceView(String isFinanceView) {
		this.isFinanceView = isFinanceView;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
}
