package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.PayrollElement;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Alert;

public class EmployeePaymentVO {

	private Long recordId;
	private String useCase;
	private Object object;
	private Long accountId;
	private String accountCategory;
	private Long combinationId;
	private PayrollElement payrollElement;
	private Double amount;
	private Alert alert;
	private boolean deleteFlag;
	private boolean editFlag;
	private Double oldAmount;
	private List<DirectPaymentDetail> directPayments;
	private Person person;
	private JobAssignment jobAssignment;
	private Long messageId;
	private int processType;
	public String getUseCase() {
		return useCase;
	}
	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public Long getRecordId() {
		return recordId;
	}
	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getAccountCategory() {
		return accountCategory;
	}
	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}
	public Long getCombinationId() {
		return combinationId;
	}
	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
	public PayrollElement getPayrollElement() {
		return payrollElement;
	}
	public void setPayrollElement(PayrollElement payrollElement) {
		this.payrollElement = payrollElement;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Alert getAlert() {
		return alert;
	}
	public void setAlert(Alert alert) {
		this.alert = alert;
	}
	public boolean isDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public boolean isEditFlag() {
		return editFlag;
	}
	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}
	public Double getOldAmount() {
		return oldAmount;
	}
	public void setOldAmount(Double oldAmount) {
		this.oldAmount = oldAmount;
	}
	public List<DirectPaymentDetail> getDirectPayments() {
		return directPayments;
	}
	public void setDirectPayments(List<DirectPaymentDetail> directPayments) {
		this.directPayments = directPayments;
	}
	public JobAssignment getJobAssignment() {
		return jobAssignment;
	}
	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public int getProcessType() {
		return processType;
	}
	public void setProcessType(int processType) {
		this.processType = processType;
	}
	
}
