package com.aiotech.aios.hr.domain.entity;

// Generated Sep 7, 2015 10:55:25 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.accounts.domain.entity.AssetCreation;
import com.aiotech.aios.accounts.domain.entity.Product;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AssetUsage generated by hbm2java
 */
@Entity
@Table(name = "hr_asset_usage", catalog = "aios")
public class AssetUsage implements java.io.Serializable {

	private Long assetUsageId;
	private LookupDetail lookupDetailByAssetColor;
	private LookupDetail lookupDetailByAssetType;
	private AssetCreation assetCreation;
	private CmpDeptLoc cmpDeptLocation;
	private Product product;
	private Implementation implementation;
	private Person personByCreatedBy;
	private Person personByPersonId;
	private Date allocatedDate;
	private Date returedDate;
	private Byte assetStatus;
	private Boolean isActive;
	private String description;
	private Date createdDate;
	private Byte isApprove;
	private String assetName;
	private String assetModel;
	private String brand;
	private String serialNumber;
	private String manufacturer;
	private Integer usefulLife;
	private Date purchaseDate;

	public AssetUsage() {
	}

	public AssetUsage(Implementation implementation, Person personByCreatedBy,
			Boolean isActive, Date createdDate, Byte isApprove) {
		this.implementation = implementation;
		this.personByCreatedBy = personByCreatedBy;
		this.isActive = isActive;
		this.createdDate = createdDate;
		this.isApprove = isApprove;
	}

	public AssetUsage(LookupDetail lookupDetailByAssetColor,
			LookupDetail lookupDetailByAssetType, AssetCreation assetCreation,
			CmpDeptLoc cmpDeptLocation, Product product,
			Implementation implementation, Person personByCreatedBy,
			Person personByPersonId, Date allocatedDate, Date returedDate,
			Byte assetStatus, Boolean isActive, String description,
			Date createdDate, Byte isApprove, String assetName,
			String assetModel, String brand, String serialNumber,
			String manufacturer, Integer usefulLife, Date purchaseDate) {
		this.lookupDetailByAssetColor = lookupDetailByAssetColor;
		this.lookupDetailByAssetType = lookupDetailByAssetType;
		this.assetCreation = assetCreation;
		this.cmpDeptLocation = cmpDeptLocation;
		this.product = product;
		this.implementation = implementation;
		this.personByCreatedBy = personByCreatedBy;
		this.personByPersonId = personByPersonId;
		this.allocatedDate = allocatedDate;
		this.returedDate = returedDate;
		this.assetStatus = assetStatus;
		this.isActive = isActive;
		this.description = description;
		this.createdDate = createdDate;
		this.isApprove = isApprove;
		this.assetName = assetName;
		this.assetModel = assetModel;
		this.brand = brand;
		this.serialNumber = serialNumber;
		this.manufacturer = manufacturer;
		this.usefulLife = usefulLife;
		this.purchaseDate = purchaseDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "asset_usage_id", unique = true, nullable = false)
	public Long getAssetUsageId() {
		return this.assetUsageId;
	}

	public void setAssetUsageId(Long assetUsageId) {
		this.assetUsageId = assetUsageId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_color")
	public LookupDetail getLookupDetailByAssetColor() {
		return this.lookupDetailByAssetColor;
	}

	public void setLookupDetailByAssetColor(
			LookupDetail lookupDetailByAssetColor) {
		this.lookupDetailByAssetColor = lookupDetailByAssetColor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_type")
	public LookupDetail getLookupDetailByAssetType() {
		return this.lookupDetailByAssetType;
	}

	public void setLookupDetailByAssetType(LookupDetail lookupDetailByAssetType) {
		this.lookupDetailByAssetType = lookupDetailByAssetType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_id")
	public AssetCreation getAssetCreation() {
		return this.assetCreation;
	}

	public void setAssetCreation(AssetCreation assetCreation) {
		this.assetCreation = assetCreation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_branch_id")
	public CmpDeptLoc getCmpDeptLocation() {
		return this.cmpDeptLocation;
	}

	public void setCmpDeptLocation(CmpDeptLoc cmpDeptLocation) {
		this.cmpDeptLocation = cmpDeptLocation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id")
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by", nullable = false)
	public Person getPersonByCreatedBy() {
		return this.personByCreatedBy;
	}

	public void setPersonByCreatedBy(Person personByCreatedBy) {
		this.personByCreatedBy = personByCreatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id")
	public Person getPersonByPersonId() {
		return this.personByPersonId;
	}

	public void setPersonByPersonId(Person personByPersonId) {
		this.personByPersonId = personByPersonId;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "allocated_date", length = 10)
	public Date getAllocatedDate() {
		return this.allocatedDate;
	}

	public void setAllocatedDate(Date allocatedDate) {
		this.allocatedDate = allocatedDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "retured_date", length = 10)
	public Date getReturedDate() {
		return this.returedDate;
	}

	public void setReturedDate(Date returedDate) {
		this.returedDate = returedDate;
	}

	@Column(name = "asset_status")
	public Byte getAssetStatus() {
		return this.assetStatus;
	}

	public void setAssetStatus(Byte assetStatus) {
		this.assetStatus = assetStatus;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "is_approve", nullable = false)
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "asset_name", length = 200)
	public String getAssetName() {
		return this.assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	@Column(name = "asset_model", length = 100)
	public String getAssetModel() {
		return this.assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	@Column(name = "brand", length = 250)
	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Column(name = "serial_number", length = 100)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Column(name = "manufacturer", length = 100)
	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@Column(name = "useful_life")
	public Integer getUsefulLife() {
		return this.usefulLife;
	}

	public void setUsefulLife(Integer usefulLife) {
		this.usefulLife = usefulLife;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "purchase_date", length = 10)
	public Date getPurchaseDate() {
		return this.purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

}
