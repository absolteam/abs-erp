package com.aiotech.aios.hr.domain.entity.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.JobPayrollElement;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.Payroll;

public class PayrollVO extends Payroll{

	private String recordType;
	private String labourCardId;
	private String routingCode;
	private String ibanNumber;
	private String periodStart;
	private String periodEnd;
	private String lopDays;
	private String createdDateStr;
	private String createdTime;
	private String currency;
	private String variableSalary;
	private Integer numberOfRecord;
	private String documentryField;
	private String employeeName;
	private String netAmountStr;
	private String payStatus;
	private Byte payStatusId;
	private Payroll payroll;
	private String designation;
	private String basic;
	private String otherEarning;
	private String otherDeduction;
	private String grossPay;
	private String paymentMode;
	private String totalBasic;
	private String totalGross;
	private String totalNet;
	private String totalEarning;
	private String totalDeduction;
	private String companyName;
	private String netPay;
	private List<JobAssignment> jobAssignmentList;
	private JobAssignment jobAssignment;
	private PayPeriodTransaction payPeriodTransaction;
	private PayPeriodTransaction previousPeriod;
	private Byte payPolicy;
	private List<PayPeriodTransaction> payPeriodTransactions;
	private Payroll previousPayroll;
	private Double perDaySalary;
	private Double perHourSalary;
	private List<JobPayrollElement> jobPayrollElements;
	private Map<String,JobPayrollElement> jobPayrollElementsMap=new HashMap<String, JobPayrollElement>();
	private String useCase;
	private Integer loanFinanceType;
	private EmployeeLoan employeeLoan;
	private PayPeriodVO payPeriodVO;
	private List<PayrollTransactionVO> payrollTransactionVOs;
	private Long payPeriodId;
	private Long jobAssignmentId;
	private boolean cutOffFromEnd;
	private Map<String,String> payrollCodeMaps=new HashMap<String,String>();
	//Direct payment transaction and params
	private String overTime;
	private String annualLeave;
	private String departmentName;
	private String description;
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getLabourCardId() {
		return labourCardId;
	}
	public void setLabourCardId(String labourCardId) {
		this.labourCardId = labourCardId;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getIbanNumber() {
		return ibanNumber;
	}
	public void setIbanNumber(String ibanNumber) {
		this.ibanNumber = ibanNumber;
	}
	public String getPeriodStart() {
		return periodStart;
	}
	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}
	public String getPeriodEnd() {
		return periodEnd;
	}
	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}
	public String getLopDays() {
		return lopDays;
	}
	public void setLopDays(String lopDays) {
		this.lopDays = lopDays;
	}
	
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	
	public Integer getNumberOfRecord() {
		return numberOfRecord;
	}
	public void setNumberOfRecord(Integer numberOfRecord) {
		this.numberOfRecord = numberOfRecord;
	}
	public String getDocumentryField() {
		return documentryField;
	}
	public void setDocumentryField(String documentryField) {
		this.documentryField = documentryField;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getNetAmountStr() {
		return netAmountStr;
	}
	public void setNetAmountStr(String netAmountStr) {
		this.netAmountStr = netAmountStr;
	}
	public String getVariableSalary() {
		return variableSalary;
	}
	public void setVariableSalary(String variableSalary) {
		this.variableSalary = variableSalary;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	
	public Payroll getPayroll() {
		return payroll;
	}
	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}
	public Byte getPayStatusId() {
		return payStatusId;
	}
	public void setPayStatusId(Byte payStatusId) {
		this.payStatusId = payStatusId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getBasic() {
		return basic;
	}
	public void setBasic(String basic) {
		this.basic = basic;
	}
	public String getOtherEarning() {
		return otherEarning;
	}
	public void setOtherEarning(String otherEarning) {
		this.otherEarning = otherEarning;
	}
	public String getOtherDeduction() {
		return otherDeduction;
	}
	public void setOtherDeduction(String otherDeduction) {
		this.otherDeduction = otherDeduction;
	}
	public String getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(String grossPay) {
		this.grossPay = grossPay;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getTotalBasic() {
		return totalBasic;
	}
	public void setTotalBasic(String totalBasic) {
		this.totalBasic = totalBasic;
	}
	public String getTotalGross() {
		return totalGross;
	}
	public void setTotalGross(String totalGross) {
		this.totalGross = totalGross;
	}
	public String getTotalNet() {
		return totalNet;
	}
	public void setTotalNet(String totalNet) {
		this.totalNet = totalNet;
	}
	public String getTotalEarning() {
		return totalEarning;
	}
	public void setTotalEarning(String totalEarning) {
		this.totalEarning = totalEarning;
	}
	public String getTotalDeduction() {
		return totalDeduction;
	}
	public void setTotalDeduction(String totalDeduction) {
		this.totalDeduction = totalDeduction;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getNetPay() {
		return netPay;
	}
	public void setNetPay(String netPay) {
		this.netPay = netPay;
	}
	public List<JobAssignment> getJobAssignmentList() {
		return jobAssignmentList;
	}
	public void setJobAssignmentList(List<JobAssignment> jobAssignmentList) {
		this.jobAssignmentList = jobAssignmentList;
	}
	public PayPeriodTransaction getPayPeriodTransaction() {
		return payPeriodTransaction;
	}
	public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
		this.payPeriodTransaction = payPeriodTransaction;
	}
	public Byte getPayPolicy() {
		return payPolicy;
	}
	public void setPayPolicy(Byte payPolicy) {
		this.payPolicy = payPolicy;
	}
	public JobAssignment getJobAssignment() {
		return jobAssignment;
	}
	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}
	public PayPeriodTransaction getPreviousPeriod() {
		return previousPeriod;
	}
	public void setPreviousPeriod(PayPeriodTransaction previousPeriod) {
		this.previousPeriod = previousPeriod;
	}
	public List<PayPeriodTransaction> getPayPeriodTransactions() {
		return payPeriodTransactions;
	}
	public void setPayPeriodTransactions(
			List<PayPeriodTransaction> payPeriodTransactions) {
		this.payPeriodTransactions = payPeriodTransactions;
	}
	public Payroll getPreviousPayroll() {
		return previousPayroll;
	}
	public void setPreviousPayroll(Payroll previousPayroll) {
		this.previousPayroll = previousPayroll;
	}
	public Double getPerDaySalary() {
		return perDaySalary;
	}
	public void setPerDaySalary(Double perDaySalary) {
		this.perDaySalary = perDaySalary;
	}
	public List<JobPayrollElement> getJobPayrollElements() {
		return jobPayrollElements;
	}
	public void setJobPayrollElements(List<JobPayrollElement> jobPayrollElements) {
		this.jobPayrollElements = jobPayrollElements;
	}
	public Map<String, JobPayrollElement> getJobPayrollElementsMap() {
		return jobPayrollElementsMap;
	}
	public void setJobPayrollElementsMap(
			Map<String, JobPayrollElement> jobPayrollElementsMap) {
		this.jobPayrollElementsMap = jobPayrollElementsMap;
	}
	public String getUseCase() {
		return useCase;
	}
	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}
	public Integer getLoanFinanceType() {
		return loanFinanceType;
	}
	public void setLoanFinanceType(Integer loanFinanceType) {
		this.loanFinanceType = loanFinanceType;
	}
	public EmployeeLoan getEmployeeLoan() {
		return employeeLoan;
	}
	public void setEmployeeLoan(EmployeeLoan employeeLoan) {
		this.employeeLoan = employeeLoan;
	}
	public Double getPerHourSalary() {
		return perHourSalary;
	}
	public void setPerHourSalary(Double perHourSalary) {
		this.perHourSalary = perHourSalary;
	}
	public PayPeriodVO getPayPeriodVO() {
		return payPeriodVO;
	}
	public void setPayPeriodVO(PayPeriodVO payPeriodVO) {
		this.payPeriodVO = payPeriodVO;
	}
	public List<PayrollTransactionVO> getPayrollTransactionVOs() {
		return payrollTransactionVOs;
	}
	public void setPayrollTransactionVOs(
			List<PayrollTransactionVO> payrollTransactionVOs) {
		this.payrollTransactionVOs = payrollTransactionVOs;
	}
	public Long getPayPeriodId() {
		return payPeriodId;
	}
	public void setPayPeriodId(Long payPeriodId) {
		this.payPeriodId = payPeriodId;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public boolean isCutOffFromEnd() {
		return cutOffFromEnd;
	}
	public void setCutOffFromEnd(boolean cutOffFromEnd) {
		this.cutOffFromEnd = cutOffFromEnd;
	}
	public Map<String, String> getPayrollCodeMaps() {
		return payrollCodeMaps;
	}
	public void setPayrollCodeMaps(Map<String, String> payrollCodeMaps) {
		this.payrollCodeMaps = payrollCodeMaps;
	}
	public String getOverTime() {
		return overTime;
	}
	public void setOverTime(String overTime) {
		this.overTime = overTime;
	}
	public String getAnnualLeave() {
		return annualLeave;
	}
	public void setAnnualLeave(String annualLeave) {
		this.annualLeave = annualLeave;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
