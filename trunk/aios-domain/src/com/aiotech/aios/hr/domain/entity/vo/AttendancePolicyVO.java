package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.AttendancePolicy;

public class AttendancePolicyVO extends AttendancePolicy{

	private String otHoursForHalfdayStr;
	private String otHoursForFulldayStr;
	private String compoffHoursForHalfdayStr;
	private String compoffHoursForFulldayStr;
	private String lopHoursForHalfdayStr;
	private String lopHoursForFulldayStr;
	private AttendancePolicy attendancePolicy;
	private String allowOtStr;
	private String allowCompoffStr;
	private String isDefalutStr;
	public String getOtHoursForHalfdayStr() {
		return otHoursForHalfdayStr;
	}
	public void setOtHoursForHalfdayStr(String otHoursForHalfdayStr) {
		this.otHoursForHalfdayStr = otHoursForHalfdayStr;
	}
	public String getOtHoursForFulldayStr() {
		return otHoursForFulldayStr;
	}
	public void setOtHoursForFulldayStr(String otHoursForFulldayStr) {
		this.otHoursForFulldayStr = otHoursForFulldayStr;
	}
	public String getCompoffHoursForHalfdayStr() {
		return compoffHoursForHalfdayStr;
	}
	public void setCompoffHoursForHalfdayStr(String compoffHoursForHalfdayStr) {
		this.compoffHoursForHalfdayStr = compoffHoursForHalfdayStr;
	}
	public String getCompoffHoursForFulldayStr() {
		return compoffHoursForFulldayStr;
	}
	public void setCompoffHoursForFulldayStr(String compoffHoursForFulldayStr) {
		this.compoffHoursForFulldayStr = compoffHoursForFulldayStr;
	}
	public String getLopHoursForHalfdayStr() {
		return lopHoursForHalfdayStr;
	}
	public void setLopHoursForHalfdayStr(String lopHoursForHalfdayStr) {
		this.lopHoursForHalfdayStr = lopHoursForHalfdayStr;
	}
	public String getLopHoursForFulldayStr() {
		return lopHoursForFulldayStr;
	}
	public void setLopHoursForFulldayStr(String lopHoursForFulldayStr) {
		this.lopHoursForFulldayStr = lopHoursForFulldayStr;
	}
	public AttendancePolicy getAttendancePolicy() {
		return attendancePolicy;
	}
	public void setAttendancePolicy(AttendancePolicy attendancePolicy) {
		this.attendancePolicy = attendancePolicy;
	}
	public String getAllowOtStr() {
		return allowOtStr;
	}
	public void setAllowOtStr(String allowOtStr) {
		this.allowOtStr = allowOtStr;
	}
	public String getAllowCompoffStr() {
		return allowCompoffStr;
	}
	public void setAllowCompoffStr(String allowCompoffStr) {
		this.allowCompoffStr = allowCompoffStr;
	}
	public String getIsDefalutStr() {
		return isDefalutStr;
	}
	public void setIsDefalutStr(String isDefalutStr) {
		this.isDefalutStr = isDefalutStr;
	}
	
}
