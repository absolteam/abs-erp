package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.PromotionDemotion;

public class PromotionDemotionVO extends PromotionDemotion{

	private String fromDateView;
	private String toDateView;
	private String effectiveDateView;
	private String createdDateView;
	private String updatedDateView;
	private String currentPosition;
	private String nextPosition;
	private String typeView;
	private String actionView;
	private String requestedBy;
	private String employeeName;
	private String jobNumber;
	private String jobTemplate;
	private String promotionStatus;
	private String departmentName;
	private String designationName;
	private String personNumber;
	private String companyName;
	private String personName;
	private String locationName;
	private String requestedDateView;
	public String getFromDateView() {
		return fromDateView;
	}
	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}
	public String getToDateView() {
		return toDateView;
	}
	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}
	public String getEffectiveDateView() {
		return effectiveDateView;
	}
	public void setEffectiveDateView(String effectiveDateView) {
		this.effectiveDateView = effectiveDateView;
	}
	public String getCreatedDateView() {
		return createdDateView;
	}
	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}
	public String getUpdatedDateView() {
		return updatedDateView;
	}
	public void setUpdatedDateView(String updatedDateView) {
		this.updatedDateView = updatedDateView;
	}
	public String getCurrentPosition() {
		return currentPosition;
	}
	public void setCurrentPosition(String currentPosition) {
		this.currentPosition = currentPosition;
	}
	public String getNextPosition() {
		return nextPosition;
	}
	public void setNextPosition(String nextPosition) {
		this.nextPosition = nextPosition;
	}
	public String getTypeView() {
		return typeView;
	}
	public void setTypeView(String typeView) {
		this.typeView = typeView;
	}
	public String getRequestedBy() {
		return requestedBy;
	}
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	public String getJobTemplate() {
		return jobTemplate;
	}
	public void setJobTemplate(String jobTemplate) {
		this.jobTemplate = jobTemplate;
	}
	public String getPromotionStatus() {
		return promotionStatus;
	}
	public void setPromotionStatus(String promotionStatus) {
		this.promotionStatus = promotionStatus;
	}
	public String getActionView() {
		return actionView;
	}
	public void setActionView(String actionView) {
		this.actionView = actionView;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getPersonNumber() {
		return personNumber;
	}
	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getRequestedDateView() {
		return requestedDateView;
	}
	public void setRequestedDateView(String requestedDateView) {
		this.requestedDateView = requestedDateView;
	}
}
