package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * JobLeave generated by hbm2java
 */
@Entity
@Table(name = "hr_job_leave", catalog = "aios")
public class JobLeave implements java.io.Serializable {

	private Long jobLeaveId;
	private Leave leave;
	private Job job;
	private BigDecimal days;
	private Boolean isActive;
	private Set<LeaveProcess> leaveProcesses = new HashSet<LeaveProcess>(0);
	private Set<LeaveReconciliation> leaveReconciliations = new HashSet<LeaveReconciliation>(
			0);

	public JobLeave() {
	}

	public JobLeave(Leave leave, Job job, BigDecimal days, Boolean isActive) {
		this.leave = leave;
		this.job = job;
		this.days = days;
		this.isActive = isActive;
	}

	public JobLeave(Leave leave, Job job, BigDecimal days, Boolean isActive,
			Set<LeaveProcess> leaveProcesses,
			Set<LeaveReconciliation> leaveReconciliations) {
		this.leave = leave;
		this.job = job;
		this.days = days;
		this.isActive = isActive;
		this.leaveProcesses = leaveProcesses;
		this.leaveReconciliations = leaveReconciliations;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "job_leave_id", unique = true, nullable = false)
	public Long getJobLeaveId() {
		return this.jobLeaveId;
	}

	public void setJobLeaveId(Long jobLeaveId) {
		this.jobLeaveId = jobLeaveId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "leave_id", nullable = false)
	public Leave getLeave() {
		return this.leave;
	}

	public void setLeave(Leave leave) {
		this.leave = leave;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "job_id", nullable = false)
	public Job getJob() {
		return this.job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	@Column(name = "days", nullable = false, precision = 4, scale = 1)
	public BigDecimal getDays() {
		return this.days;
	}

	public void setDays(BigDecimal days) {
		this.days = days;
	}

	@Column(name = "is_active", nullable = false)
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "jobLeave")
	public Set<LeaveProcess> getLeaveProcesses() {
		return this.leaveProcesses;
	}

	public void setLeaveProcesses(Set<LeaveProcess> leaveProcesses) {
		this.leaveProcesses = leaveProcesses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "jobLeave")
	public Set<LeaveReconciliation> getLeaveReconciliations() {
		return this.leaveReconciliations;
	}

	public void setLeaveReconciliations(
			Set<LeaveReconciliation> leaveReconciliations) {
		this.leaveReconciliations = leaveReconciliations;
	}

}
