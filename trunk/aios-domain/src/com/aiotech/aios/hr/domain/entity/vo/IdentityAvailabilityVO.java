package com.aiotech.aios.hr.domain.entity.vo;


import com.aiotech.aios.hr.domain.entity.IdentityAvailability;

public class IdentityAvailabilityVO extends IdentityAvailability{

	private String handoverDateStr;
	private String expectedReturnDateStr;
	private String actualReturnDateStr;
	private String createdDateStr;
	private String statusStr;
	private String isActiveStr;
	private String createdPerson;
	private String identityName;
	private String identityType;
	private String identityNumber;
	private Long alertId;
	private String personName;
	private Long personId;
	
	
	public String getHandoverDateStr() {
		return handoverDateStr;
	}
	public void setHandoverDateStr(String handoverDateStr) {
		this.handoverDateStr = handoverDateStr;
	}
	public String getExpectedReturnDateStr() {
		return expectedReturnDateStr;
	}
	public void setExpectedReturnDateStr(String expectedReturnDateStr) {
		this.expectedReturnDateStr = expectedReturnDateStr;
	}
	public String getActualReturnDateStr() {
		return actualReturnDateStr;
	}
	public void setActualReturnDateStr(String actualReturnDateStr) {
		this.actualReturnDateStr = actualReturnDateStr;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getIsActiveStr() {
		return isActiveStr;
	}
	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}
	public String getCreatedPerson() {
		return createdPerson;
	}
	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}
	public String getIdentityName() {
		return identityName;
	}
	public void setIdentityName(String identityName) {
		this.identityName = identityName;
	}
	public String getIdentityType() {
		return identityType;
	}
	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}
	public String getIdentityNumber() {
		return identityNumber;
	}
	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}
	public Long getAlertId() {
		return alertId;
	}
	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
}
