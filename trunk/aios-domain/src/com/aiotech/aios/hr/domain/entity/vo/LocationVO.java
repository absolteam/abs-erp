package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.Location;

public class LocationVO extends Location{

	private String fullAddress;

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}
}
