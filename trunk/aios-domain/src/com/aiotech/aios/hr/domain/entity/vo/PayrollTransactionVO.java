/**
 * 
 */
package com.aiotech.aios.hr.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.utils.common.AIOSCommonUtil;
import com.aiotech.aios.hr.domain.entity.PayrollTransaction;

/**
 * @author Usman Anwar
 * 
 */
public class PayrollTransactionVO extends PayrollTransaction {

	private String employeeName;
	private String designation;
	private String elementType;
	private String elementNature;
	private String payMonth;
	private String transAmount;

	private String createdDateView;
	private String anchorExpression;
	private String linkLabel;
	private String elementName;
	private String leaveType;
	private Double leaveAvailableDays;
	private String fromDate;
	private String toDate;

	private List<PayrollTransactionVO> transactions;

	public static PayrollTransactionVO convertToVO(
			PayrollTransaction payrollTransaction) {
		PayrollTransactionVO vo = new PayrollTransactionVO();

		try {

			vo.setTransAmount(AIOSCommonUtil.formatAmount(payrollTransaction
					.getAmount()));
			vo.setEmployeeName(payrollTransaction.getPayroll()
					.getJobAssignment().getPerson().getFirstName()
					+ " "
					+ payrollTransaction.getPayroll().getJobAssignment()
							.getPerson().getLastName());

			vo.setDesignation(payrollTransaction.getPayroll()
					.getJobAssignment().getDesignation().getDesignationName());
			vo.setElementNature(payrollTransaction.getPayrollElement()
					.getElementNature());
			vo.setElementType(payrollTransaction.getPayrollElement()
					.getElementType());
			vo.setNote(payrollTransaction.getNote());
			vo.setPayMonth(payrollTransaction.getPayroll().getPayMonth());
			vo.setElementName(payrollTransaction.getPayrollElement()
					.getElementName());
			/*
			 * vo.setAnchorExpression(payrollTransaction.getAnchorExpression());
			 * vo.setLinkLabel(payrollTransaction.getLinkLabel());
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
		return vo;
	}

	public static List<PayrollTransactionVO> convertToVOList(
			List<PayrollTransaction> vos) {

		List<PayrollTransactionVO> newVOS = new ArrayList<PayrollTransactionVO>();
		try {
			for (PayrollTransaction payrollTransaction : vos) {
				newVOS.add(convertToVO(payrollTransaction));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newVOS;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getElementType() {
		return elementType;
	}

	public void setElementType(String elementType) {
		this.elementType = elementType;
	}

	public String getElementNature() {
		return elementNature;
	}

	public void setElementNature(String elementNature) {
		this.elementNature = elementNature;
	}

	public String getPayMonth() {
		return payMonth;
	}

	public void setPayMonth(String payMonth) {
		this.payMonth = payMonth;
	}

	public String getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(String transAmount) {
		this.transAmount = transAmount;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public List<PayrollTransactionVO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<PayrollTransactionVO> transactions) {
		this.transactions = transactions;
	}

	public String getCreatedDateView() {
		return createdDateView;
	}

	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}

	public String getElementName() {
		return elementName;
	}

	public void setElementName(String elementName) {
		this.elementName = elementName;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public Double getLeaveAvailableDays() {
		return leaveAvailableDays;
	}

	public void setLeaveAvailableDays(Double leaveAvailableDays) {
		this.leaveAvailableDays = leaveAvailableDays;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
