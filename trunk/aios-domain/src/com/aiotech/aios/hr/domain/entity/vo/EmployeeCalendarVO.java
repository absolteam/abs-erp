package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.EmployeeCalendar;

public class EmployeeCalendarVO extends EmployeeCalendar{
	private int holidayTypeId;
	private String holidayType;
	
	public int getHolidayTypeId() {
		return holidayTypeId;
	}
	public void setHolidayTypeId(int holidayTypeId) {
		this.holidayTypeId = holidayTypeId;
	}
	public String getHolidayType() {
		return holidayType;
	}
	public void setHolidayType(String holidayType) {
		this.holidayType = holidayType;
	}
}
