package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.AdvertisingPanel;

public class AdvertisingPanelVO extends AdvertisingPanel{
	private String providerName;
	private String positionReferenceNumber;
	private String postedBy;
	private String postedDateDisplay;
	private String sourceType;
	private String taskDisplay;
	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getPositionReferenceNumber() {
		return positionReferenceNumber;
	}

	public void setPositionReferenceNumber(String positionReferenceNumber) {
		this.positionReferenceNumber = positionReferenceNumber;
	}

	public String getPostedBy() {
		return postedBy;
	}

	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}

	public String getPostedDateDisplay() {
		return postedDateDisplay;
	}

	public void setPostedDateDisplay(String postedDateDisplay) {
		this.postedDateDisplay = postedDateDisplay;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getTaskDisplay() {
		return taskDisplay;
	}

	public void setTaskDisplay(String taskDisplay) {
		this.taskDisplay = taskDisplay;
	}

	

}
