package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.LeaveReconciliation;

public class LeaveReconciliationVO extends LeaveReconciliation{
	private Long leaveReconciliationId;
	private String leaveType;
	private double avaiableDays;

	public Long getLeaveReconciliationId() {
		return leaveReconciliationId;
	}

	public void setLeaveReconciliationId(Long leaveReconciliationId) {
		this.leaveReconciliationId = leaveReconciliationId;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public double getAvaiableDays() {
		return avaiableDays;
	}

	public void setAvaiableDays(double avaiableDays) {
		this.avaiableDays = avaiableDays;
	}
	
}
