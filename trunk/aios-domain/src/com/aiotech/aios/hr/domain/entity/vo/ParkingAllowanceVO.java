package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.ParkingAllowance;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;

public class ParkingAllowanceVO extends ParkingAllowance{

	private String fromDateView;
	private String toDateView;
	private String isFinanceView;
	private Long jobAssignmentId;
	private Long jobPayrollElementId;
	private String transactionDate;
	private PayPeriodTransaction payPeriodTransaction;
	
	private String cardCompany;
	private String cardType;
	private String parkingType;
	private String purchaseDateView;
	public String getFromDateView() {
		return fromDateView;
	}
	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}
	public String getToDateView() {
		return toDateView;
	}
	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}
	public String getIsFinanceView() {
		return isFinanceView;
	}
	public void setIsFinanceView(String isFinanceView) {
		this.isFinanceView = isFinanceView;
	}
	public Long getJobAssignmentId() {
		return jobAssignmentId;
	}
	public void setJobAssignmentId(Long jobAssignmentId) {
		this.jobAssignmentId = jobAssignmentId;
	}
	public Long getJobPayrollElementId() {
		return jobPayrollElementId;
	}
	public void setJobPayrollElementId(Long jobPayrollElementId) {
		this.jobPayrollElementId = jobPayrollElementId;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public PayPeriodTransaction getPayPeriodTransaction() {
		return payPeriodTransaction;
	}
	public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
		this.payPeriodTransaction = payPeriodTransaction;
	}
	
	public String getCardCompany() {
		return cardCompany;
	}
	public void setCardCompany(String cardCompany) {
		this.cardCompany = cardCompany;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getParkingType() {
		return parkingType;
	}
	public void setParkingType(String parkingType) {
		this.parkingType = parkingType;
	}
	public String getPurchaseDateView() {
		return purchaseDateView;
	}
	public void setPurchaseDateView(String purchaseDateView) {
		this.purchaseDateView = purchaseDateView;
	}
}
