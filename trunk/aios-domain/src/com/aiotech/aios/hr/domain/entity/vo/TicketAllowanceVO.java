package com.aiotech.aios.hr.domain.entity.vo;

import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;
import com.aiotech.aios.hr.domain.entity.TicketAllowance;

public class TicketAllowanceVO extends TicketAllowance{
private String fromDateView;
private String toDateView;
private String isFinanceView;
private Long jobAssignmentId;
private Long jobPayrollElementId;
private String transactionDate;
private PayPeriodTransaction payPeriodTransaction;

private String to;
private String from;
private String airline;
private String agency;
private String ticketClass;
private String purchaseDateView;
public String getFromDateView() {
	return fromDateView;
}
public void setFromDateView(String fromDateView) {
	this.fromDateView = fromDateView;
}
public String getToDateView() {
	return toDateView;
}
public void setToDateView(String toDateView) {
	this.toDateView = toDateView;
}
public String getIsFinanceView() {
	return isFinanceView;
}
public void setIsFinanceView(String isFinanceView) {
	this.isFinanceView = isFinanceView;
}
public Long getJobAssignmentId() {
	return jobAssignmentId;
}
public void setJobAssignmentId(Long jobAssignmentId) {
	this.jobAssignmentId = jobAssignmentId;
}
public Long getJobPayrollElementId() {
	return jobPayrollElementId;
}
public void setJobPayrollElementId(Long jobPayrollElementId) {
	this.jobPayrollElementId = jobPayrollElementId;
}
public String getTransactionDate() {
	return transactionDate;
}
public void setTransactionDate(String transactionDate) {
	this.transactionDate = transactionDate;
}
public PayPeriodTransaction getPayPeriodTransaction() {
	return payPeriodTransaction;
}
public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
	this.payPeriodTransaction = payPeriodTransaction;
}

public String getTo() {
	return to;
}
public void setTo(String to) {
	this.to = to;
}
public String getFrom() {
	return from;
}
public void setFrom(String from) {
	this.from = from;
}
public String getAirline() {
	return airline;
}
public void setAirline(String airline) {
	this.airline = airline;
}
public String getAgency() {
	return agency;
}
public void setAgency(String agency) {
	this.agency = agency;
}

public String getTicketClass() {
	return ticketClass;
}
public void setTicketClass(String ticketClass) {
	this.ticketClass = ticketClass;
}
public String getPurchaseDateView() {
	return purchaseDateView;
}
public void setPurchaseDateView(String purchaseDateView) {
	this.purchaseDateView = purchaseDateView;
}
}
