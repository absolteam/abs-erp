package com.aiotech.aios.hr.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.hr.domain.entity.JobAssignment;
import com.aiotech.aios.hr.domain.entity.MemoWarning;
import com.aiotech.aios.hr.domain.entity.MemoWarningPerson;

public class MemoWarningVO extends MemoWarning{
	
	private String memoPersonIds;
	private String memoPersonNames;
	private String ccPersonIds;
	private String ccPersonNames;
	private String departmentIds;
	private String createdDateStr;
	private String createdPerson;
	private MemoWarning memoWarning;
	private String companyId;
	private String employeeName;
	private String employeeLocation;
	private String employeeDepartment;
	private String employeeCompany;
	private String employeeCode;
	private JobAssignment jobAssignment;
	private MemoWarningPerson memoWarningPerson;
	List<MemoWarningVO> memoWarningPersonVOs;
	List<MemoWarningVO> memoWarningPersonCCVOs;
	private String employeeDesignation;
	private String memoType;
	private String memoSubType;
	private String processTypeName;
	private String toDetails;
	private String dearDetails;
	private String departmentNames;
	private String messageHtml;
	public String getMemoPersonIds() {
		return memoPersonIds;
	}
	public void setMemoPersonIds(String memoPersonIds) {
		this.memoPersonIds = memoPersonIds;
	}
	public String getMemoPersonNames() {
		return memoPersonNames;
	}
	public void setMemoPersonNames(String memoPersonNames) {
		this.memoPersonNames = memoPersonNames;
	}
	public String getCcPersonIds() {
		return ccPersonIds;
	}
	public void setCcPersonIds(String ccPersonIds) {
		this.ccPersonIds = ccPersonIds;
	}
	public String getCcPersonNames() {
		return ccPersonNames;
	}
	public void setCcPersonNames(String ccPersonNames) {
		this.ccPersonNames = ccPersonNames;
	}
	public String getDepartmentIds() {
		return departmentIds;
	}
	public void setDepartmentIds(String departmentIds) {
		this.departmentIds = departmentIds;
	}
	public String getCreatedDateStr() {
		return createdDateStr;
	}
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	public String getCreatedPerson() {
		return createdPerson;
	}
	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}
	public MemoWarning getMemoWarning() {
		return memoWarning;
	}
	public void setMemoWarning(MemoWarning memoWarning) {
		this.memoWarning = memoWarning;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeLocation() {
		return employeeLocation;
	}
	public void setEmployeeLocation(String employeeLocation) {
		this.employeeLocation = employeeLocation;
	}
	public String getEmployeeDepartment() {
		return employeeDepartment;
	}
	public void setEmployeeDepartment(String employeeDepartment) {
		this.employeeDepartment = employeeDepartment;
	}
	public String getEmployeeCompany() {
		return employeeCompany;
	}
	public void setEmployeeCompany(String employeeCompany) {
		this.employeeCompany = employeeCompany;
	}
	public JobAssignment getJobAssignment() {
		return jobAssignment;
	}
	public void setJobAssignment(JobAssignment jobAssignment) {
		this.jobAssignment = jobAssignment;
	}
	public List<MemoWarningVO> getMemoWarningPersonVOs() {
		return memoWarningPersonVOs;
	}
	public void setMemoWarningPersonVOs(List<MemoWarningVO> memoWarningPersonVOs) {
		this.memoWarningPersonVOs = memoWarningPersonVOs;
	}
	public MemoWarningPerson getMemoWarningPerson() {
		return memoWarningPerson;
	}
	public void setMemoWarningPerson(MemoWarningPerson memoWarningPerson) {
		this.memoWarningPerson = memoWarningPerson;
	}
	public String getEmployeeDesignation() {
		return employeeDesignation;
	}
	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public List<MemoWarningVO> getMemoWarningPersonCCVOs() {
		return memoWarningPersonCCVOs;
	}
	public void setMemoWarningPersonCCVOs(List<MemoWarningVO> memoWarningPersonCCVOs) {
		this.memoWarningPersonCCVOs = memoWarningPersonCCVOs;
	}
	public String getMemoType() {
		return memoType;
	}
	public void setMemoType(String memoType) {
		this.memoType = memoType;
	}
	public String getMemoSubType() {
		return memoSubType;
	}
	public void setMemoSubType(String memoSubType) {
		this.memoSubType = memoSubType;
	}
	public String getProcessTypeName() {
		return processTypeName;
	}
	public void setProcessTypeName(String processTypeName) {
		this.processTypeName = processTypeName;
	}
	public String getDearDetails() {
		return dearDetails;
	}
	public void setDearDetails(String dearDetails) {
		this.dearDetails = dearDetails;
	}
	public String getToDetails() {
		return toDetails;
	}
	public void setToDetails(String toDetails) {
		this.toDetails = toDetails;
	}
	public String getDepartmentNames() {
		return departmentNames;
	}
	public void setDepartmentNames(String departmentNames) {
		this.departmentNames = departmentNames;
	}
	public String getMessageHtml() {
		return messageHtml;
	}
	public void setMessageHtml(String messageHtml) {
		this.messageHtml = messageHtml;
	}

}
