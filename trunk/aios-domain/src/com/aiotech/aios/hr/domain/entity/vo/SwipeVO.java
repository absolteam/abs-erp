package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class SwipeVO {
	private Integer userReference;
	private Date date;
	private Date time;
	private Long deviceRegistrationId;
	private Integer implementationId;
	private String inOutType;
	private List<SwipeVO> vos;
	private String doorNumber;
	public String getInOutType() {
		return inOutType;
	}
	public void setInOutType(String inOutType) {
		this.inOutType = inOutType;
	}
	public Integer getUserReference() {
		return userReference;
	}
	public void setUserReference(Integer userReference) {
		this.userReference = userReference;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Long getDeviceRegistrationId() {
		return deviceRegistrationId;
	}
	public void setDeviceRegistrationId(Long deviceRegistrationId) {
		this.deviceRegistrationId = deviceRegistrationId;
	}
	public Integer getImplementationId() {
		return implementationId;
	}
	public void setImplementationId(Integer implementationId) {
		this.implementationId = implementationId;
	}
	public List<SwipeVO> getVos() {
		return vos;
	}
	public void setVos(List<SwipeVO> vos) {
		this.vos = vos;
	}
	public String getDoorNumber() {
		return doorNumber;
	}
	public void setDoorNumber(String doorNumber) {
		this.doorNumber = doorNumber;
	}
	
}
