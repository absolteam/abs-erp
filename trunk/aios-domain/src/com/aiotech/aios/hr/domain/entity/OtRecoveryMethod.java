package com.aiotech.aios.hr.domain.entity;

// Generated Jun 9, 2014 1:44:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * OtRecoveryMethod generated by hbm2java
 */
@Entity
@Table(name = "hr_ot_recovery_method", catalog = "aios")
public class OtRecoveryMethod implements java.io.Serializable {

	private Long otRecoveryMethodId;
	private Implementation implementation;
	private Person person;
	private String methodName;
	private String methodType;
	private String methodValue;
	private Boolean isCompensationOff;
	private Boolean isLop;
	private Boolean isOt;
	private Date createdDate;

	public OtRecoveryMethod() {
	}

	public OtRecoveryMethod(Implementation implementation, String methodName,
			String methodValue) {
		this.implementation = implementation;
		this.methodName = methodName;
		this.methodValue = methodValue;
	}

	public OtRecoveryMethod(Implementation implementation, Person person,
			String methodName, String methodType, String methodValue,
			Boolean isCompensationOff, Boolean isLop, Boolean isOt,
			Date createdDate) {
		this.implementation = implementation;
		this.person = person;
		this.methodName = methodName;
		this.methodType = methodType;
		this.methodValue = methodValue;
		this.isCompensationOff = isCompensationOff;
		this.isLop = isLop;
		this.isOt = isOt;
		this.createdDate = createdDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ot_recovery_method_id", unique = true, nullable = false)
	public Long getOtRecoveryMethodId() {
		return this.otRecoveryMethodId;
	}

	public void setOtRecoveryMethodId(Long otRecoveryMethodId) {
		this.otRecoveryMethodId = otRecoveryMethodId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Column(name = "method_name", nullable = false, length = 150)
	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Column(name = "method_type", length = 50)
	public String getMethodType() {
		return this.methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	@Column(name = "method_value", nullable = false, length = 20)
	public String getMethodValue() {
		return this.methodValue;
	}

	public void setMethodValue(String methodValue) {
		this.methodValue = methodValue;
	}

	@Column(name = "is_compensation_off")
	public Boolean getIsCompensationOff() {
		return this.isCompensationOff;
	}

	public void setIsCompensationOff(Boolean isCompensationOff) {
		this.isCompensationOff = isCompensationOff;
	}

	@Column(name = "is_lop")
	public Boolean getIsLop() {
		return this.isLop;
	}

	public void setIsLop(Boolean isLop) {
		this.isLop = isLop;
	}

	@Column(name = "is_ot")
	public Boolean getIsOt() {
		return this.isOt;
	}

	public void setIsOt(Boolean isOt) {
		this.isOt = isOt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
