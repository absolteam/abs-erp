package com.aiotech.aios.hr.domain.entity.vo;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.hr.domain.entity.EmployeeLoan;
import com.aiotech.aios.hr.domain.entity.EmployeeLoanRepayment;
import com.aiotech.aios.hr.domain.entity.PayPeriodTransaction;

public class EmployeeLoanVO extends EmployeeLoan{
	private String dueDate;
	private String pricipalBegining;
	private String installment;
	private String intrestRate;
	private String principalEnd;
	private String principal;
	private String employeeName;
	private String financeTypeView;
	private String repaymentTypeView;
	private String requestedDateView;
	private String paymentFrequencyView;
	private String dueStartDateView;
	private String intrestView;
	private Long employeeLoanRepaymentId;
	private Date dueStartDate;//For shorting
	private List<EmployeeLoanRepayment> repaymentList;
	private Date actualDueDate;
	private PayPeriodTransaction payPeriodTransaction;
	private boolean deleteFlag;
	private boolean editFlag;
	private Double oldAmount;
	private Long messageId;
	private String designationName;
	private String personNumber;
	private String companyName;
	private String departmentName;
	private String personName;
	private String locationName;
	private List<EmployeeLoanVO> loanRepayments;
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getPricipalBegining() {
		return pricipalBegining;
	}
	public void setPricipalBegining(String pricipalBegining) {
		this.pricipalBegining = pricipalBegining;
	}
	public String getInstallment() {
		return installment;
	}
	public void setInstallment(String installment) {
		this.installment = installment;
	}
	
	public String getPrincipalEnd() {
		return principalEnd;
	}
	public void setPrincipalEnd(String principalEnd) {
		this.principalEnd = principalEnd;
	}
	public String getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(String intrestRate) {
		this.intrestRate = intrestRate;
	}
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getFinanceTypeView() {
		return financeTypeView;
	}
	public void setFinanceTypeView(String financeTypeView) {
		this.financeTypeView = financeTypeView;
	}
	public String getRepaymentTypeView() {
		return repaymentTypeView;
	}
	public void setRepaymentTypeView(String repaymentTypeView) {
		this.repaymentTypeView = repaymentTypeView;
	}
	public String getRequestedDateView() {
		return requestedDateView;
	}
	public void setRequestedDateView(String requestedDateView) {
		this.requestedDateView = requestedDateView;
	}
	public String getPaymentFrequencyView() {
		return paymentFrequencyView;
	}
	public void setPaymentFrequencyView(String paymentFrequencyView) {
		this.paymentFrequencyView = paymentFrequencyView;
	}
	public String getDueStartDateView() {
		return dueStartDateView;
	}
	public void setDueStartDateView(String dueStartDateView) {
		this.dueStartDateView = dueStartDateView;
	}
	public String getIntrestView() {
		return intrestView;
	}
	public void setIntrestView(String intrestView) {
		this.intrestView = intrestView;
	}
	public Long getEmployeeLoanRepaymentId() {
		return employeeLoanRepaymentId;
	}
	public void setEmployeeLoanRepaymentId(Long employeeLoanRepaymentId) {
		this.employeeLoanRepaymentId = employeeLoanRepaymentId;
	}
	public Date getDueStartDate() {
		return dueStartDate;
	}
	public void setDueStartDate(Date dueStartDate) {
		this.dueStartDate = dueStartDate;
	}
	public List<EmployeeLoanRepayment> getRepaymentList() {
		return repaymentList;
	}
	public void setRepaymentList(List<EmployeeLoanRepayment> repaymentList) {
		this.repaymentList = repaymentList;
	}
	public Date getActualDueDate() {
		return actualDueDate;
	}
	public void setActualDueDate(Date actualDueDate) {
		this.actualDueDate = actualDueDate;
	}
	public PayPeriodTransaction getPayPeriodTransaction() {
		return payPeriodTransaction;
	}
	public void setPayPeriodTransaction(PayPeriodTransaction payPeriodTransaction) {
		this.payPeriodTransaction = payPeriodTransaction;
	}
	public boolean isDeleteFlag() {
		return deleteFlag;
	}
	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
	public boolean isEditFlag() {
		return editFlag;
	}
	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}
	public Double getOldAmount() {
		return oldAmount;
	}
	public void setOldAmount(Double oldAmount) {
		this.oldAmount = oldAmount;
	}
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getPersonNumber() {
		return personNumber;
	}
	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public List<EmployeeLoanVO> getLoanRepayments() {
		return loanRepayments;
	}
	public void setLoanRepayments(List<EmployeeLoanVO> loanRepayments) {
		this.loanRepayments = loanRepayments;
	}
	
	
}
