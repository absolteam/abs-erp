package com.aiotech.aios.project.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.project.domain.entity.ProjectTask;

public class ProjectTaskVO extends ProjectTask {

	private String fromDate;
	private String toDate;
	private String actualFromDate;
	private String actualToDate;
	private String customerName;
	private String projectType;
	private String coordinator;
	private String currentStatus;
	private String paymentTerm;
	private String projectAccessRights;
	private String projectTitle;
	private String supplierName;
	private String mileStoneTitle;
	private String parentTaskTitle;
	private Double pettyCashAmount;
	private Long recordId;
	private String useCase;
	private Object object;
	private Long alertId;
	private List<DirectPaymentDetail> directPayments;
	private boolean allowModification;
	private boolean editFlag;

	
	private List<ProjectResourceVO> projectResourceVOs = new ArrayList<ProjectResourceVO>();
	private List<ProjectMileStoneVO> projectMileStoneVOs = new ArrayList<ProjectMileStoneVO>();
	private List<ProjectPaymentScheduleVO> projectPaymentScheduleVOs = new ArrayList<ProjectPaymentScheduleVO>();
	private List<ProjectExpenseVO> projectExpenseVOs = new ArrayList<ProjectExpenseVO>();
	private List<ProjectInventoryVO> projectInventoryVOs = new ArrayList<ProjectInventoryVO>();
	private List<ProjectTaskVO> subTasks=new ArrayList<ProjectTaskVO>();
	public ProjectTaskVO() {
		
	}
	
	public ProjectTaskVO(ProjectTask projecttTask) {
		this.setProjectTaskId(projecttTask.getProjectTaskId());
		
	}
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public List<ProjectResourceVO> getProjectResourceVOs() {
		return projectResourceVOs;
	}
	public void setProjectResourceVOs(List<ProjectResourceVO> projectResourceVOs) {
		this.projectResourceVOs = projectResourceVOs;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public List<DirectPaymentDetail> getDirectPayments() {
		return directPayments;
	}

	public void setDirectPayments(List<DirectPaymentDetail> directPayments) {
		this.directPayments = directPayments;
	}

	public List<ProjectMileStoneVO> getProjectMileStoneVOs() {
		return projectMileStoneVOs;
	}

	public void setProjectMileStoneVOs(List<ProjectMileStoneVO> projectMileStoneVOs) {
		this.projectMileStoneVOs = projectMileStoneVOs;
	}

	public List<ProjectPaymentScheduleVO> getProjectPaymentScheduleVOs() {
		return projectPaymentScheduleVOs;
	}

	public void setProjectPaymentScheduleVOs(
			List<ProjectPaymentScheduleVO> projectPaymentScheduleVOs) {
		this.projectPaymentScheduleVOs = projectPaymentScheduleVOs;
	}

	public List<ProjectExpenseVO> getProjectExpenseVOs() {
		return projectExpenseVOs;
	}

	public void setProjectExpenseVOs(List<ProjectExpenseVO> projectExpenseVOs) {
		this.projectExpenseVOs = projectExpenseVOs;
	}

	public String getProjectAccessRights() {
		return projectAccessRights;
	}

	public void setProjectAccessRights(String projectAccessRights) {
		this.projectAccessRights = projectAccessRights;
	}

	public String getActualFromDate() {
		return actualFromDate;
	}

	public void setActualFromDate(String actualFromDate) {
		this.actualFromDate = actualFromDate;
	}

	public String getActualToDate() {
		return actualToDate;
	}

	public void setActualToDate(String actualToDate) {
		this.actualToDate = actualToDate;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getMileStoneTitle() {
		return mileStoneTitle;
	}

	public void setMileStoneTitle(String mileStoneTitle) {
		this.mileStoneTitle = mileStoneTitle;
	}

	public String getParentTaskTitle() {
		return parentTaskTitle;
	}

	public void setParentTaskTitle(String parentTaskTitle) {
		this.parentTaskTitle = parentTaskTitle;
	}

	public List<ProjectInventoryVO> getProjectInventoryVOs() {
		return projectInventoryVOs;
	}

	public void setProjectInventoryVOs(List<ProjectInventoryVO> projectInventoryVOs) {
		this.projectInventoryVOs = projectInventoryVOs;
	}

	public boolean isAllowModification() {
		return allowModification;
	}

	public void setAllowModification(boolean allowModification) {
		this.allowModification = allowModification;
	}

	public List<ProjectTaskVO> getSubTasks() {
		return subTasks;
	}

	public void setSubTasks(List<ProjectTaskVO> subTasks) {
		this.subTasks = subTasks;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public Double getPettyCashAmount() {
		return pettyCashAmount;
	}

	public void setPettyCashAmount(Double pettyCashAmount) {
		this.pettyCashAmount = pettyCashAmount;
	}
	
}
