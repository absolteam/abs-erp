package com.aiotech.aios.project.domain.entity;

// Generated 5 Nov, 2014 10:07:26 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ProjectPaymentSchedule generated by hbm2java
 */
@Entity
@Table(name = "pm_project_payment_schedule", catalog = "aios")
public class ProjectPaymentSchedule implements java.io.Serializable {

	private Long projectPaymentScheduleId;
	private ProjectTaskSurrogate projectTaskSurrogate;
	private Implementation implementation;
	private LookupDetail lookupDetail;
	private Double amount;
	private Date paymentDate;
	private Byte paymentMode;
	private String description;
	private Byte status;
	private String receiptReference;
	private Set<ProjectPaymentDetail> projectPaymentDetails = new HashSet<ProjectPaymentDetail>(
			0);

	public ProjectPaymentSchedule() {
	}

	public ProjectPaymentSchedule(ProjectTaskSurrogate projectTaskSurrogate,
			Double amount, Date paymentDate) {
		this.projectTaskSurrogate = projectTaskSurrogate;
		this.amount = amount;
		this.paymentDate = paymentDate;
	}

	public ProjectPaymentSchedule(ProjectTaskSurrogate projectTaskSurrogate,
			Implementation implementation, LookupDetail lookupDetail,
			Double amount, Date paymentDate, Byte paymentMode,
			String description, Byte status,
			Set<ProjectPaymentDetail> projectPaymentDetails,String receiptReference) {
		this.projectTaskSurrogate = projectTaskSurrogate;
		this.implementation = implementation;
		this.lookupDetail = lookupDetail;
		this.amount = amount;
		this.paymentDate = paymentDate;
		this.paymentMode = paymentMode;
		this.description = description;
		this.status = status;
		this.receiptReference=receiptReference;
		this.projectPaymentDetails = projectPaymentDetails;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "project_payment_schedule_id", unique = true, nullable = false)
	public Long getProjectPaymentScheduleId() {
		return this.projectPaymentScheduleId;
	}

	public void setProjectPaymentScheduleId(Long projectPaymentScheduleId) {
		this.projectPaymentScheduleId = projectPaymentScheduleId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_task_surrogate_id", nullable = false)
	public ProjectTaskSurrogate getProjectTaskSurrogate() {
		return this.projectTaskSurrogate;
	}

	public void setProjectTaskSurrogate(
			ProjectTaskSurrogate projectTaskSurrogate) {
		this.projectTaskSurrogate = projectTaskSurrogate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id")
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "payment_of")
	public LookupDetail getLookupDetail() {
		return this.lookupDetail;
	}

	public void setLookupDetail(LookupDetail lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

	@Column(name = "amount", nullable = false, precision = 22, scale = 0)
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date", nullable = false, length = 10)
	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Column(name = "payment_mode")
	public Byte getPaymentMode() {
		return this.paymentMode;
	}

	public void setPaymentMode(Byte paymentMode) {
		this.paymentMode = paymentMode;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "status")
	public Byte getStatus() {
		return this.status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "projectPaymentSchedule")
	public Set<ProjectPaymentDetail> getProjectPaymentDetails() {
		return this.projectPaymentDetails;
	}

	public void setProjectPaymentDetails(
			Set<ProjectPaymentDetail> projectPaymentDetails) {
		this.projectPaymentDetails = projectPaymentDetails;
	}

	@Column(name = "receipt_reference", length = 50)
	public String getReceiptReference() {
		return receiptReference;
	}

	public void setReceiptReference(String receiptReference) {
		this.receiptReference = receiptReference;
	}

}
