package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectResource;

public class ProjectResourceVO extends ProjectResource{
	
	private String resourceDetail;
	private String resourceTypeName;
	private Double total;
	
	public ProjectResourceVO() {
		
	}
	
	public ProjectResourceVO(ProjectResource projectResource) {
		this.setProjectResourceId(projectResource.getProjectResourceId());
/*		this.setProject(projectResource.getProject());
*/		this.setLookupDetail(projectResource.getLookupDetail());
		this.setResourceType(projectResource.getResourceType());
		this.setCount(projectResource.getCount());
		this.setUnitPrice(projectResource.getUnitPrice());
		this.setDescription(projectResource.getDescription());
		this.setProjectResourceDetails(projectResource.getProjectResourceDetails());
		
	}
	
	public String getResourceDetail() {
		return resourceDetail;
	}
	public void setResourceDetail(String resourceDetail) {
		this.resourceDetail = resourceDetail;
	}
	public String getResourceTypeName() {
		return resourceTypeName;
	}
	public void setResourceTypeName(String resourceTypeName) {
		this.resourceTypeName = resourceTypeName;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}

}
