package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectExpense;

public class ProjectExpenseVO extends ProjectExpense{

	private String paymentDateView;

	public String getPaymentDateView() {
		return paymentDateView;
	}

	public void setPaymentDateView(String paymentDateView) {
		this.paymentDateView = paymentDateView;
	}
}
