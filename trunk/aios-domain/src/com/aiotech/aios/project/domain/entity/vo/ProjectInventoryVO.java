package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectInventory;

public class ProjectInventoryVO extends ProjectInventory{
	private Double totalAmount;
	private String storeName;
	private Double suggestedRetailPrice;
	private String statusName;
	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Double getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}

	public void setSuggestedRetailPrice(Double suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
