package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectPaymentDetail;


public class ProjectPaymentDetailVO extends ProjectPaymentDetail implements java.io.Serializable {

	private String receiptDate;
	private String receiptStatus;
	private String receiptAmount;
	private String clientName;
	private String projectTitle;
	private String milestoneTitle;
	
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getReceiptStatus() {
		return receiptStatus;
	}
	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}
	public String getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getProjectTitle() {
		return projectTitle;
	}
	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}
	public String getMilestoneTitle() {
		return milestoneTitle;
	}
	public void setMilestoneTitle(String milestoneTitle) {
		this.milestoneTitle = milestoneTitle;
	}
	
}
