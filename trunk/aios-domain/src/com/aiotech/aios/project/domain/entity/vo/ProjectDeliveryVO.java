package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.aios.project.domain.entity.ProjectDelivery;
import com.aiotech.aios.project.domain.entity.ProjectTask;

public class ProjectDeliveryVO extends ProjectDelivery {

	private String date;
	private String projectTitle;
	private String projectReference;
	private String orderDate;
	private String customerMobile;
	private String customerAddress;
	private String mileStoneTitle;
	private String viewType;
	private String currentStatus;
	private String taskTitle;
	private String projectMileStone;
	private String customerName;
	private String paymentAmount;
	private ProjectTask projectTask;
	private Project project;

	public ProjectDeliveryVO() {

	}

	public ProjectDeliveryVO(ProjectDelivery projectDelivery) {
		this.setProjectDeliveryId(projectDelivery.getProjectDeliveryId());
		this.setImplementation(projectDelivery.getImplementation());

		this.setDeliveryTitle(projectDelivery.getDeliveryTitle());
		this.setReferenceNumber(projectDelivery.getReferenceNumber());
		this.setDeliveryDate(projectDelivery.getDeliveryDate());
		this.setDescription(projectDelivery.getDescription());
		this.setDeliveryStatus(projectDelivery.getDeliveryStatus());
		this.setActualDeliveryDate(projectDelivery.getActualDeliveryDate());
		if (projectDelivery.getProjectTaskSurrogate().getProject() != null) {
			this.setProjectTitle(projectDelivery.getProjectTaskSurrogate()
					.getProject().getProjectTitle());
			this.setProject(projectDelivery.getProjectTaskSurrogate()
					.getProject());
		} else if (projectDelivery.getProjectTaskSurrogate().getProjectTask() != null) {
			this.setProjectTitle(projectDelivery.getProjectTaskSurrogate()
					.getProjectTask().getProject().getProjectTitle());
			this.setProject(projectDelivery.getProjectTaskSurrogate()
					.getProjectTask().getProject());
			if (projectDelivery.getProjectTaskSurrogate().getProjectTask() != null
					&& projectDelivery.getProjectTaskSurrogate()
							.getProjectTask().getProjectMileStone() != null)
				this.setProjectMileStone(projectDelivery
						.getProjectTaskSurrogate().getProjectTask()
						.getProjectMileStone().getTitle());
		}

	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getMileStoneTitle() {
		return mileStoneTitle;
	}

	public void setMileStoneTitle(String mileStoneTitle) {
		this.mileStoneTitle = mileStoneTitle;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getTaskTitle() {
		return taskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}

	public ProjectTask getProjectTask() {
		return projectTask;
	}

	public void setProjectTask(ProjectTask projectTask) {
		this.projectTask = projectTask;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getProjectMileStone() {
		return projectMileStone;
	}

	public void setProjectMileStone(String projectMileStone) {
		this.projectMileStone = projectMileStone;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getProjectReference() {
		return projectReference;
	}

	public void setProjectReference(String projectReference) {
		this.projectReference = projectReference;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
}
