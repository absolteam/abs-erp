package com.aiotech.aios.project.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.project.domain.entity.ProjectMileStone;

public class ProjectMileStoneVO extends ProjectMileStone implements java.io.Serializable{

	private String projectTitle;
	private String fromDate;
	private String toDate;
	private String actualFromDate;
	private String actualToDate;
	private String currentStatus;
	private String clientName;
	private String actualFormatCost;
	private String estimationFormatCost;
	private String mileStoneTitle;
	private String referenceNumber;
	private String taskName;
	private String taskReference;
	private List<ProjectPaymentDetailVO> projectPaymentDetailVOs = new ArrayList<ProjectPaymentDetailVO>();
	
	public String getProjectTitle() {
		return projectTitle;
	}
	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getActualFromDate() {
		return actualFromDate;
	}
	public void setActualFromDate(String actualFromDate) {
		this.actualFromDate = actualFromDate;
	}
	public String getActualToDate() {
		return actualToDate;
	}
	public void setActualToDate(String actualToDate) {
		this.actualToDate = actualToDate;
	}
	public List<ProjectPaymentDetailVO> getProjectPaymentDetailVOs() {
		return projectPaymentDetailVOs;
	}
	public void setProjectPaymentDetailVOs(
			List<ProjectPaymentDetailVO> projectPaymentDetailVOs) {
		this.projectPaymentDetailVOs = projectPaymentDetailVOs;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getActualFormatCost() {
		return actualFormatCost;
	}
	public void setActualFormatCost(String actualFormatCost) {
		this.actualFormatCost = actualFormatCost;
	}
	public String getEstimationFormatCost() {
		return estimationFormatCost;
	}
	public void setEstimationFormatCost(String estimationFormatCost) {
		this.estimationFormatCost = estimationFormatCost;
	}
	public String getMileStoneTitle() {
		return mileStoneTitle;
	}
	public void setMileStoneTitle(String mileStoneTitle) {
		this.mileStoneTitle = mileStoneTitle;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskReference() {
		return taskReference;
	}
	public void setTaskReference(String taskReference) {
		this.taskReference = taskReference;
	}
}
