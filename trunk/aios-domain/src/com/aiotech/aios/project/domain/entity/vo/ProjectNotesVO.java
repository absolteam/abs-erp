package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectNotes;

public class ProjectNotesVO extends ProjectNotes {
	private boolean signedUser;
	private String time;
	private String date;
	public boolean isSignedUser() {
		return signedUser;
	}

	public void setSignedUser(boolean signedUser) {
		this.signedUser = signedUser;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
