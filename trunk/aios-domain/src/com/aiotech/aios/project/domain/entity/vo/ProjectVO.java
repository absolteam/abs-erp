package com.aiotech.aios.project.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;

import com.aiotech.aios.project.domain.entity.Project;
import com.aiotech.utils.common.ConstantsUtil;
import com.aiotech.utils.common.DateUtil;

public class ProjectVO extends Project {

	private String fromDate;
	private String toDate;
	private String customerName;
	private String projectType;
	private String coordinator;
	private String currentStatus;
	private String paymentTerm;
	private String projectAccessRights;
	private String supplierName;
	private Double pettyCashAmount;
	private Long recordId;
	private String useCase;
	private Object object;
	private Long alertId;
	private List<DirectPaymentDetail> directPayments;
	private boolean allowModification;
	private String projectLead;
	private String projectHelper;
	private String salesPerson;
	private String projectDecorator;
	private String projectManager;
	private String priorityView;
	private String statusView;
	private String createdByView;
	private String createdDateView;
	private String expenseAmount;
	private String customerMobile;
	private String customerAddress;
	private List<ProjectResourceVO> projectResourceVOs = new ArrayList<ProjectResourceVO>();
	private List<ProjectMileStoneVO> projectMileStoneVOs = new ArrayList<ProjectMileStoneVO>();
	private List<ProjectPaymentScheduleVO> projectPaymentScheduleVOs = new ArrayList<ProjectPaymentScheduleVO>();
	private List<ProjectExpenseVO> projectExpenseVOs = new ArrayList<ProjectExpenseVO>();
	private List<ProjectInventoryVO> projectInventoryVOs = new ArrayList<ProjectInventoryVO>();

	private List<ProjectVO> projectHistoryVOs = new ArrayList<ProjectVO>();

	private Project editProject;
	private boolean editFlag;

	public ProjectVO() {

	}

	public ProjectVO(Project project) {
		this.setProjectId(project.getProjectId());
		this.setPersonByProjectLead(project.getPersonByProjectLead());
		this.setLookupDetail(project.getLookupDetail());
		this.setImplementation(project.getImplementation());
		this.setPersonByCreatedBy(project.getPersonByCreatedBy());
		this.setCreditTerm(project.getCreditTerm());
		this.setCustomer(project.getCustomer());
		this.setReferenceNumber(project.getReferenceNumber());
		this.setProjectTitle(project.getProjectTitle());
		this.setStartDate(project.getStartDate());
		if (project.getStartDate() != null)
			this.setFromDate(DateUtil.convertDateToString(project
					.getStartDate() + ""));
		this.setEndDate(project.getEndDate());
		if (project.getEndDate() != null)
			this.setToDate(DateUtil.convertDateToString(project.getEndDate()
					+ ""));
		this.setProjectValue(project.getProjectValue());
		this.setSecurityDeposit(project.getSecurityDeposit());
		this.setDepositReleaseDate(project.getDepositReleaseDate());
		this.setClientCoordinator(project.getClientCoordinator());
		this.setCoordinatorContact(project.getCoordinatorContact());
		this.setContractTerms(project.getContractTerms());
		this.setPenalityDescriptive(project.getPenalityDescriptive());
		this.setProjectStatus(project.getProjectStatus());
		this.setBudget(project.getBudget());
		this.setDescription(project.getDescription());
		this.setIsApprove(project.getIsApprove());
		this.setCreatedDate(project.getCreatedDate());
		if (project.getCreatedDate() != null)
			this.setCreatedDateView(DateUtil.convertDateToString(project
					.getCreatedDate() + "")
					+ " "
					+ DateUtil.convertDateToTimeOnly(project.getCreatedDate()));
		this.setRelativeId(project.getRelativeId());
		if (project.getPersonByProjectLead() != null)
			this.setProjectLead(project.getPersonByProjectLead().getFirstName()
					+ " " + project.getPersonByProjectLead().getLastName());
		if (project.getCustomer().getPersonByPersonId() != null) {
			this.setCustomerName(project.getCustomer().getPersonByPersonId()
					.getFirstName()
					+ " "
					+ project.getCustomer().getPersonByPersonId().getLastName());
		} else {
			this.setCustomerName(project.getCustomer().getCompany()
					.getCompanyName());
		}

		this.setPriorityView(ConstantsUtil.Accounts.Priority.get(
				project.getPriority()).name());
		this.setStatusView(ConstantsUtil.Accounts.ProjectStatus.get(
				project.getProjectStatus()).name());

		if (project.getPersonByCreatedBy() != null)
			this.setCreatedByView(project.getPersonByCreatedBy().getFirstName()
					+ " " + project.getPersonByCreatedBy().getLastName());
		/*
		 * this.setProjectResources(project.getProjectResources());
		 * this.setProjectMileStones(project.getProjectMileStones());
		 * this.setProjectDeliveries(project.getProjectDeliveries());
		 */
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public List<ProjectResourceVO> getProjectResourceVOs() {
		return projectResourceVOs;
	}

	public void setProjectResourceVOs(List<ProjectResourceVO> projectResourceVOs) {
		this.projectResourceVOs = projectResourceVOs;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public List<DirectPaymentDetail> getDirectPayments() {
		return directPayments;
	}

	public void setDirectPayments(List<DirectPaymentDetail> directPayments) {
		this.directPayments = directPayments;
	}

	public List<ProjectMileStoneVO> getProjectMileStoneVOs() {
		return projectMileStoneVOs;
	}

	public void setProjectMileStoneVOs(
			List<ProjectMileStoneVO> projectMileStoneVOs) {
		this.projectMileStoneVOs = projectMileStoneVOs;
	}

	public List<ProjectPaymentScheduleVO> getProjectPaymentScheduleVOs() {
		return projectPaymentScheduleVOs;
	}

	public void setProjectPaymentScheduleVOs(
			List<ProjectPaymentScheduleVO> projectPaymentScheduleVOs) {
		this.projectPaymentScheduleVOs = projectPaymentScheduleVOs;
	}

	public List<ProjectExpenseVO> getProjectExpenseVOs() {
		return projectExpenseVOs;
	}

	public void setProjectExpenseVOs(List<ProjectExpenseVO> projectExpenseVOs) {
		this.projectExpenseVOs = projectExpenseVOs;
	}

	public String getProjectAccessRights() {
		return projectAccessRights;
	}

	public void setProjectAccessRights(String projectAccessRights) {
		this.projectAccessRights = projectAccessRights;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public boolean isAllowModification() {
		return allowModification;
	}

	public void setAllowModification(boolean allowModification) {
		this.allowModification = allowModification;
	}

	public String getProjectLead() {
		return projectLead;
	}

	public void setProjectLead(String projectLead) {
		this.projectLead = projectLead;
	}

	public String getPriorityView() {
		return priorityView;
	}

	public void setPriorityView(String priorityView) {
		this.priorityView = priorityView;
	}

	public String getStatusView() {
		return statusView;
	}

	public void setStatusView(String statusView) {
		this.statusView = statusView;
	}

	public String getCreatedByView() {
		return createdByView;
	}

	public void setCreatedByView(String createdByView) {
		this.createdByView = createdByView;
	}

	public String getCreatedDateView() {
		return createdDateView;
	}

	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}

	public List<ProjectInventoryVO> getProjectInventoryVOs() {
		return projectInventoryVOs;
	}

	public void setProjectInventoryVOs(
			List<ProjectInventoryVO> projectInventoryVOs) {
		this.projectInventoryVOs = projectInventoryVOs;
	}

	public Project getEditProject() {
		return editProject;
	}

	public void setEditProject(Project editProject) {
		this.editProject = editProject;
	}

	public List<ProjectVO> getProjectHistoryVOs() {
		return projectHistoryVOs;
	}

	public void setProjectHistoryVOs(List<ProjectVO> projectHistoryVOs) {
		this.projectHistoryVOs = projectHistoryVOs;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public String getProjectHelper() {
		return projectHelper;
	}

	public void setProjectHelper(String projectHelper) {
		this.projectHelper = projectHelper;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getProjectDecorator() {
		return projectDecorator;
	}

	public void setProjectDecorator(String projectDecorator) {
		this.projectDecorator = projectDecorator;
	}

	public String getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public Double getPettyCashAmount() {
		return pettyCashAmount;
	}

	public void setPettyCashAmount(Double pettyCashAmount) {
		this.pettyCashAmount = pettyCashAmount;
	}

}
