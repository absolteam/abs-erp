package com.aiotech.aios.project.domain.entity.vo;

import com.aiotech.aios.project.domain.entity.ProjectPaymentSchedule;

public class ProjectPaymentScheduleVO extends ProjectPaymentSchedule{

	private String paymentDateView;

	public String getPaymentDateView() {
		return paymentDateView;
	}

	public void setPaymentDateView(String paymentDateView) {
		this.paymentDateView = paymentDateView;
	}
}
