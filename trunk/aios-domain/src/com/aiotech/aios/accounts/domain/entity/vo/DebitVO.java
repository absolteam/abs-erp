package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Debit;

public class DebitVO extends Debit{

	
	private String supplierName;
	private String supplierNameAndNumber;
	private String strDate;
	private List<DebitDetailVO> debitDetailVOs;
	
	
	public DebitVO() {
		
	}
	
	public DebitVO(Debit debit) {
		this.setDebitId(debit.getDebitId());
		this.setImplementation(debit.getImplementation());
		this.setGoodsReturn(debit.getGoodsReturn());
		this.setDebitNumber(debit.getDebitNumber());
		this.setDate(debit.getDate());
		this.setDescription(debit.getDescription());
		this.setDebitDetails(debit.getDebitDetails());
	}
	
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierNameAndNumber() {
		return supplierNameAndNumber;
	}
	public void setSupplierNameAndNumber(String supplierNameAndNumber) {
		this.supplierNameAndNumber = supplierNameAndNumber;
	}
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public List<DebitDetailVO> getDebitDetailVOs() {
		return debitDetailVOs;
	}

	public void setDebitDetailVOs(List<DebitDetailVO> debitDetailVOs) {
		this.debitDetailVOs = debitDetailVOs;
	}
	
}
