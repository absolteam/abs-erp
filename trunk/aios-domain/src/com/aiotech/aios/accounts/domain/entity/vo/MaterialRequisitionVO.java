package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisition;

public class MaterialRequisitionVO extends MaterialRequisition implements
		java.io.Serializable {

	private String date;
	private String status;
	private String storeName;
	private String personName;
	private String transferReference;
	private Double totalRequisitionQty;
	private boolean editStatus;
	private List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs = new ArrayList<MaterialRequisitionDetailVO>();

	public MaterialRequisitionVO() {
	}

	public MaterialRequisitionVO(MaterialRequisition materialRequisition) {
		this.setMaterialRequisitionId(materialRequisition
				.getMaterialRequisitionId());
		this.setImplementation(materialRequisition.getImplementation());
		this.setStore(materialRequisition.getStore());
		this.setReferenceNumber(materialRequisition.getReferenceNumber());
		this.setRequisitionDate(materialRequisition.getRequisitionDate());
		this.setRequisitionStatus(materialRequisition.getRequisitionStatus());
		this.setDescription(materialRequisition.getDescription());
		this.setMaterialRequisitionDetails(materialRequisition
				.getMaterialRequisitionDetails());
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public List<MaterialRequisitionDetailVO> getMaterialRequisitionDetailVOs() {
		return materialRequisitionDetailVOs;
	}

	public void setMaterialRequisitionDetailVOs(
			List<MaterialRequisitionDetailVO> materialRequisitionDetailVOs) {
		this.materialRequisitionDetailVOs = materialRequisitionDetailVOs;
	}

	public Double getTotalRequisitionQty() {
		return totalRequisitionQty;
	}

	public void setTotalRequisitionQty(Double totalRequisitionQty) {
		this.totalRequisitionQty = totalRequisitionQty;
	}

	public boolean isEditStatus() {
		return editStatus;
	}

	public void setEditStatus(boolean editStatus) {
		this.editStatus = editStatus;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getTransferReference() {
		return transferReference;
	}

	public void setTransferReference(String transferReference) {
		this.transferReference = transferReference;
	}
}
