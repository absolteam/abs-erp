package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Aisle;

public class AisleVO extends Aisle {

	private String sectionType;
	private List<ShelfVO> shelfVOs = new ArrayList<ShelfVO>();
	
	public AisleVO() {
	}
	
	public AisleVO(Aisle aisle) {
		this.setAisleId(aisle.getAisleId());
		this.setLookupDetail(aisle.getLookupDetail());
		this.setStore(aisle.getStore());
		this.setAisleNumber(aisle.getAisleNumber());
		this.setDimension(aisle.getDimension());
		this.setShelfs(aisle.getShelfs());
	}
	
	public String getSectionType() {
		return sectionType;
	}

	public void setSectionType(String sectionType) {
		this.sectionType = sectionType;
	}

	public List<ShelfVO> getShelfVOs() {
		return shelfVOs;
	}

	public void setShelfVOs(List<ShelfVO> shelfVOs) {
		this.shelfVOs = shelfVOs;
	}
	
}
