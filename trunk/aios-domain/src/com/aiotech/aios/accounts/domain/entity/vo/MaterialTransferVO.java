package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialTransfer;

public class MaterialTransferVO extends MaterialTransfer implements
		java.io.Serializable {

	private String date;
	private String transferPerson;
	private Double totalTransferedQty;
	private Double totalTransferAmount;
	private List<MaterialTransferDetailVO> materialTransferDetailVOs;
	private WorkinProcessVO workingProcessVO;

	public MaterialTransferVO() {
	}

	public MaterialTransferVO(MaterialTransfer materialTransfer) {
		this.setMaterialTransferId(materialTransfer.getMaterialTransferId());
		this.setImplementation(materialTransfer.getImplementation());
		this.setPersonByCreatedBy(materialTransfer.getPersonByCreatedBy());
		this.setPersonByTransferPerson(materialTransfer
				.getPersonByTransferPerson());
		this.setReferenceNumber(materialTransfer.getReferenceNumber());
		this.setTransferDate(materialTransfer.getTransferDate());
		this.setDescription(materialTransfer.getDescription());
		this.setCreatedDate(materialTransfer.getCreatedDate());
		this.setMaterialTransferDetails(materialTransfer
				.getMaterialTransferDetails());
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTransferPerson() {
		return transferPerson;
	}

	public void setTransferPerson(String transferPerson) {
		this.transferPerson = transferPerson;
	}

	public List<MaterialTransferDetailVO> getMaterialTransferDetailVOs() {
		return materialTransferDetailVOs;
	}

	public void setMaterialTransferDetailVOs(
			List<MaterialTransferDetailVO> materialTransferDetailVOs) {
		this.materialTransferDetailVOs = materialTransferDetailVOs;
	}

	public WorkinProcessVO getWorkingProcessVO() {
		return workingProcessVO;
	}

	public void setWorkingProcessVO(WorkinProcessVO workingProcessVO) {
		this.workingProcessVO = workingProcessVO;
	}

	public Double getTotalTransferedQty() {
		return totalTransferedQty;
	}

	public void setTotalTransferedQty(Double totalTransferedQty) {
		this.totalTransferedQty = totalTransferedQty;
	}

	public Double getTotalTransferAmount() {
		return totalTransferAmount;
	}

	public void setTotalTransferAmount(Double totalTransferAmount) {
		this.totalTransferAmount = totalTransferAmount;
	}

}
