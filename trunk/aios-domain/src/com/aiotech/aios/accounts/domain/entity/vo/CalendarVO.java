package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Calendar;

public class CalendarVO extends Calendar implements java.io.Serializable {

	private String statusStr;
	private String start;
	private String end;
	private List<PeriodVO> periodVOs = new ArrayList<PeriodVO>();
	
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public List<PeriodVO> getPeriodVOs() {
		return periodVOs;
	}
	public void setPeriodVOs(List<PeriodVO> periodVOs) {
		this.periodVOs = periodVOs;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
}
