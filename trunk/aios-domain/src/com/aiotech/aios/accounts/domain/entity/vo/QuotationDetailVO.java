package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.QuotationDetail;

public class QuotationDetailVO extends QuotationDetail implements
		java.io.Serializable {

	private String itemTypeStr;
	private QuotationVO quotationVO;
	private Double totalRate;
	private Double packageUnit;
	private String displayQuantity;
	private String displayUnitRate;
	private String displayTotalAmount;
	List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public String getItemTypeStr() {
		return itemTypeStr;
	}

	public void setItemTypeStr(String itemTypeStr) {
		this.itemTypeStr = itemTypeStr;
	}

	public QuotationVO getQuotationVO() {
		return quotationVO;
	}

	public void setQuotationVO(QuotationVO quotationVO) {
		this.quotationVO = quotationVO;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public String getDisplayUnitRate() {
		return displayUnitRate;
	}

	public void setDisplayUnitRate(String displayUnitRate) {
		this.displayUnitRate = displayUnitRate;
	}

	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}

	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}

	public String getDisplayQuantity() {
		return displayQuantity;
	}

	public void setDisplayQuantity(String displayQuantity) {
		this.displayQuantity = displayQuantity;
	}

	public Double getPackageUnit() {
		return packageUnit;
	}

	public void setPackageUnit(Double packageUnit) {
		this.packageUnit = packageUnit;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	} 
}
