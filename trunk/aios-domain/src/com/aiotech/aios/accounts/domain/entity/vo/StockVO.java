package com.aiotech.aios.accounts.domain.entity.vo;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Stock;

public class StockVO extends Stock implements java.io.Serializable {

	private Double availableQuantity;
	private Double issuedQuantity;
 	private Double receivedQuantity;
	private Double unitRate;
 	private Double totalAmount; 
	private Double totalReceivedQuantity;
	private Double totalIssuedQuantity;
	private double costPrice;
	private double standardPrice;
	private Double reorderQuantity;
 	private Long productId;
	private long productPricingCalcId;
	private Long storeId;
	private long referenceRecordId;
	private String productName;
	private String productUnit;
	private String costingType;
	private String storeReference;
	private String storeName;
	private String productCode;
	private String storePerson;
	private String storeType;
	private String productPic;
	private String aisleName;
	private String rackName;
	private String shelfName;
	private String expiryDate;
	private String productType;
	private InputStream barcode;
	private BufferedImage image;
	private String categoryName;
	private String subCategoryName;
	private Integer shelfId;
	private Double suggestedRetailPrice;
	private Double sellingPrice;
	private String unitCode;
	
	private List<StockVO> barCodeDetails = new ArrayList<StockVO>();
	
 	public StockVO() {
		
	}
	
	public StockVO(Stock stock) {
		this.setStockId(stock.getStockId());
		this.setProduct(stock.getProduct());
		this.setImplementation(stock.getImplementation());
		this.setStore(stock.getStore());
		this.setUnitRate(stock.getUnitRate());
		this.setQuantity(stock.getQuantity());
	}
	
	public Double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Double getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(Double issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public Double getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Double receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalReceivedQuantity() {
		return totalReceivedQuantity;
	}

	public void setTotalReceivedQuantity(Double totalReceivedQuantity) {
		this.totalReceivedQuantity = totalReceivedQuantity;
	}

	public Double getTotalIssuedQuantity() {
		return totalIssuedQuantity;
	}

	public void setTotalIssuedQuantity(Double totalIssuedQuantity) {
		this.totalIssuedQuantity = totalIssuedQuantity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductUnit() {
		return productUnit;
	}

	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}

	public String getCostingType() {
		return costingType;
	}

	public void setCostingType(String costingType) {
		this.costingType = costingType;
	}

	public String getStoreReference() {
		return storeReference;
	}

	public void setStoreReference(String storeReference) {
		this.storeReference = storeReference;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	} 

	public long getReferenceRecordId() {
		return referenceRecordId;
	}

	public void setReferenceRecordId(long referenceRecordId) {
		this.referenceRecordId = referenceRecordId;
	}

	public String getStorePerson() {
		return storePerson;
	}

	public void setStorePerson(String storePerson) {
		this.storePerson = storePerson;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public String getProductPic() {
		return productPic;
	}

	public void setProductPic(String productPic) {
		this.productPic = productPic;
	}

	public List<StockVO> getBarCodeDetails() {
		return barCodeDetails;
	}

	public void setBarCodeDetails(List<StockVO> barCodeDetails) {
		this.barCodeDetails = barCodeDetails;
	}

	public InputStream getBarcode() {
		return barcode;
	}

	public void setBarcode(InputStream barcode) {
		this.barcode = barcode;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getAisleName() {
		return aisleName;
	}

	public void setAisleName(String aisleName) {
		this.aisleName = aisleName;
	}

	public String getRackName() {
		return rackName;
	}

	public void setRackName(String rackName) {
		this.rackName = rackName;
	}

	public String getShelfName() {
		return shelfName;
	}

	public void setShelfName(String shelfName) {
		this.shelfName = shelfName;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public long getProductPricingCalcId() {
		return productPricingCalcId;
	}

	public void setProductPricingCalcId(long productPricingCalcId) {
		this.productPricingCalcId = productPricingCalcId;
	}

	public Double getReorderQuantity() {
		return reorderQuantity;
	}

	public void setReorderQuantity(Double reorderQuantity) {
		this.reorderQuantity = reorderQuantity;

	}

	public Double getSuggestedRetailPrice() {
		return suggestedRetailPrice;
	}

	public void setSuggestedRetailPrice(Double suggestedRetailPrice) {
		this.suggestedRetailPrice = suggestedRetailPrice;

	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	} 
}
