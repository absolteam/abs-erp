package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.IssueRequistion;

public class IssueRequistionVO extends IssueRequistion implements java.io.Serializable{

	private Double totalIssueQty;
	private Double totalReceiveAmount;
	private String strIssueDate;
	private String shippingSource;
	private String departmentName;
	private String locationName;
	private String requisitionPersonName;
	private List<IssueRequistionDetailVO> issueRequistionDetailVO = new ArrayList<IssueRequistionDetailVO>();
	
	public IssueRequistionVO() {
		
	}
	
	public IssueRequistionVO(IssueRequistion issueRequistion) {
		this.setIssueRequistionId(issueRequistion.getIssueRequistionId());
		this.setCmpDeptLocation(issueRequistion.getCmpDeptLocation());
		this.setLookupDetail(issueRequistion.getLookupDetail());
		this.setImplementation(issueRequistion.getImplementation());
		this.setPerson(issueRequistion.getPerson());
 		this.setReferenceNumber(issueRequistion.getReferenceNumber()); 
		this.setIssueDate(issueRequistion.getIssueDate()); 
		this.setDescription(issueRequistion.getDescription());
		this.setIssueRequistionDetails(issueRequistion.getIssueRequistionDetails());
	}
	
	public Double getTotalIssueQty() {
		return totalIssueQty;
	}
	public void setTotalIssueQty(Double totalIssueQty) {
		this.totalIssueQty = totalIssueQty;
	}
	public Double getTotalReceiveAmount() {
		return totalReceiveAmount;
	}
	public void setTotalReceiveAmount(Double totalReceiveAmount) {
		this.totalReceiveAmount = totalReceiveAmount;
	}
	public List<IssueRequistionDetailVO> getIssueRequistionDetailVO() {
		return issueRequistionDetailVO;
	}
	public void setIssueRequistionDetailVO(
			List<IssueRequistionDetailVO> issueRequistionDetailVO) {
		this.issueRequistionDetailVO = issueRequistionDetailVO;
	} 

	public String getStrIssueDate() {
		return strIssueDate;
	}

	public void setStrIssueDate(String strIssueDate) {
		this.strIssueDate = strIssueDate;
	}

	public String getShippingSource() {
		return shippingSource;
	}

	public void setShippingSource(String shippingSource) {
		this.shippingSource = shippingSource;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getRequisitionPersonName() {
		return requisitionPersonName;
	}

	public void setRequisitionPersonName(String requisitionPersonName) {
		this.requisitionPersonName = requisitionPersonName;
	}
	 
}
