package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Invoice;

public class InvoiceVO extends Invoice implements java.io.Serializable{

	private Double totalInvoiceQty;
	private Double totalInvoiceAmount;
	private String supplierName;
	private String supplierNumber;
	private String invoiceDate;
 	private Double pendingInvoice;
	private Long messageId;
	private Object object;
	private Long recordId;
	private String useCase;
  	private String parentInvoice;
  	private Integer paymentTermDays;
 	private List<InvoiceDetailVO> invoiceDetailVOList = new ArrayList<InvoiceDetailVO>();
	private List<ReceiveVO> receiveVOList = new ArrayList<ReceiveVO>();
	private List<InvoiceVO> invoiceVOs;
	private Date formDate;
	private Date toDate;
	private Long supplierId;
	private String receiveNumber;
	private String receiveDate;
	private Byte paymentStatus;
	private Long combinationId;
	private String combinationStr;
	private String createdPerson;
	private String invoiceAmountStr;
	private String totalInvoiceStr;
	private String pendingInvoiceStr;
	private String invoiceReference;
	private String paymentTermStr;
	private String dueDate;
	private String balanceAmount;
	private Date invoiceDatedt;
	
	private boolean group0;
	private boolean group1;
	private boolean group2;
	private boolean group3;
	private boolean group4;
	
	private String groupAmount0;
	private String groupAmount1;
	private String groupAmount2;
	private String groupAmount3;
	private String groupAmount4;
	
	public InvoiceVO() {
		
	}
	
	public InvoiceVO(Invoice invoice) {
		this.setInvoiceId(invoice.getInvoiceId());
		this.setImplementation(invoice.getImplementation());
		this.setReceive(invoice.getReceive());
		this.setSupplier(invoice.getSupplier());
		this.setInvoiceNumber(invoice.getInvoiceNumber());
		this.setDate(invoice.getDate());
		this.setIsApprove(invoice.getIsApprove());
		this.setStatus(invoice.getStatus());
		this.setDescription(invoice.getDescription());
		this.setRelativeId(invoice.getRelativeId());
		this.setInvoiceDetails(invoice.getInvoiceDetails());
	}
	
	public List<InvoiceDetailVO> getInvoiceDetailVOList() {
		return invoiceDetailVOList;
	}

	public void setInvoiceDetailVOList(List<InvoiceDetailVO> invoiceDetailVOList) {
		this.invoiceDetailVOList = invoiceDetailVOList;
	}

	public List<ReceiveVO> getReceiveVOList() {
		return receiveVOList;
	}

	public void setReceiveVOList(List<ReceiveVO> receiveVOList) {
		this.receiveVOList = receiveVOList;
	}

	public Double getTotalInvoiceQty() {
		return totalInvoiceQty;
	}

	public void setTotalInvoiceQty(Double totalInvoiceQty) {
		this.totalInvoiceQty = totalInvoiceQty;
	}

	public Double getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(Double totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(String supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	 

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	} 

	public String getParentInvoice() {
		return parentInvoice;
	}

	public void setParentInvoice(String parentInvoice) {
		this.parentInvoice = parentInvoice;
	}

	public List<InvoiceVO> getInvoiceVOs() {
		return invoiceVOs;
	}

	public void setInvoiceVOs(List<InvoiceVO> invoiceVOs) {
		this.invoiceVOs = invoiceVOs;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getReceiveNumber() {
		return receiveNumber;
	}

	public void setReceiveNumber(String receiveNumber) {
		this.receiveNumber = receiveNumber;
	}

	public Double getPendingInvoice() {
		return pendingInvoice;
	}

	public void setPendingInvoice(Double pendingInvoice) {
		this.pendingInvoice = pendingInvoice;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public Byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public String getCombinationStr() {
		return combinationStr;
	}

	public void setCombinationStr(String combinationStr) {
		this.combinationStr = combinationStr;
	}

	public String getCreatedPerson() {
		return createdPerson;
	}

	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}

	public String getInvoiceAmountStr() {
		return invoiceAmountStr;
	}

	public void setInvoiceAmountStr(String invoiceAmountStr) {
		this.invoiceAmountStr = invoiceAmountStr;
	}

	public String getTotalInvoiceStr() {
		return totalInvoiceStr;
	}

	public void setTotalInvoiceStr(String totalInvoiceStr) {
		this.totalInvoiceStr = totalInvoiceStr;
	}

	public String getPendingInvoiceStr() {
		return pendingInvoiceStr;
	}

	public void setPendingInvoiceStr(String pendingInvoiceStr) {
		this.pendingInvoiceStr = pendingInvoiceStr;
	}

	public String getInvoiceReference() {
		return invoiceReference;
	}

	public void setInvoiceReference(String invoiceReference) {
		this.invoiceReference = invoiceReference;
	}

	public String getPaymentTermStr() {
		return paymentTermStr;
	}

	public void setPaymentTermStr(String paymentTermStr) {
		this.paymentTermStr = paymentTermStr;
	}

	public Integer getPaymentTermDays() {
		return paymentTermDays;
	}

	public void setPaymentTermDays(Integer paymentTermDays) {
		this.paymentTermDays = paymentTermDays;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Date getInvoiceDatedt() {
		return invoiceDatedt;
	}

	public void setInvoiceDatedt(Date invoiceDatedt) {
		this.invoiceDatedt = invoiceDatedt;
	}

	public String getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(String balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public boolean isGroup0() {
		return group0;
	}

	public void setGroup0(boolean group0) {
		this.group0 = group0;
	}

	public boolean isGroup1() {
		return group1;
	}

	public void setGroup1(boolean group1) {
		this.group1 = group1;
	}

	public boolean isGroup2() {
		return group2;
	}

	public void setGroup2(boolean group2) {
		this.group2 = group2;
	}

	public boolean isGroup3() {
		return group3;
	}

	public void setGroup3(boolean group3) {
		this.group3 = group3;
	}

	public boolean isGroup4() {
		return group4;
	}

	public void setGroup4(boolean group4) {
		this.group4 = group4;
	}

	public String getGroupAmount0() {
		return groupAmount0;
	}

	public void setGroupAmount0(String groupAmount0) {
		this.groupAmount0 = groupAmount0;
	}

	public String getGroupAmount1() {
		return groupAmount1;
	}

	public void setGroupAmount1(String groupAmount1) {
		this.groupAmount1 = groupAmount1;
	}

	public String getGroupAmount2() {
		return groupAmount2;
	}

	public void setGroupAmount2(String groupAmount2) {
		this.groupAmount2 = groupAmount2;
	}

	public String getGroupAmount3() {
		return groupAmount3;
	}

	public void setGroupAmount3(String groupAmount3) {
		this.groupAmount3 = groupAmount3;
	}

	public String getGroupAmount4() {
		return groupAmount4;
	}

	public void setGroupAmount4(String groupAmount4) {
		this.groupAmount4 = groupAmount4;
	} 
}
