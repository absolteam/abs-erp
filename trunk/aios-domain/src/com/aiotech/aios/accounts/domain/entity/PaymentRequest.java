package com.aiotech.aios.accounts.domain.entity;

// Generated Jun 8, 2015 9:58:21 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PaymentRequest generated by hbm2java
 */
@Entity
@Table(name = "ac_payment_request", catalog = "aios")
public class PaymentRequest implements java.io.Serializable {

	private Long paymentRequestId;
	private Implementation implementation;
	private Person personByCreatedBy;
	private Person personByPersonId;
	private String paymentReqNo;
	private String personName;
	private Date requestDate;
	private Byte status;
	private Byte isApprove;
	private Date createdDate;
	private String description;
	private Long relativeId;
	private Set<PaymentRequestDetail> paymentRequestDetails = new HashSet<PaymentRequestDetail>(
			0);
	private Set<DirectPayment> directPayments = new HashSet<DirectPayment>(0);

	public PaymentRequest() {
	}

	public PaymentRequest(Implementation implementation,
			Person personByCreatedBy, String paymentReqNo, Date requestDate,
			Date createdDate) {
		this.implementation = implementation;
		this.personByCreatedBy = personByCreatedBy;
		this.paymentReqNo = paymentReqNo;
		this.requestDate = requestDate;
		this.createdDate = createdDate;
	}

	public PaymentRequest(Implementation implementation,
			Person personByCreatedBy, Person personByPersonId,
			String paymentReqNo, Date requestDate, Byte status, Byte isApprove,
			Date createdDate, String description, Long relativeId,
			Set<PaymentRequestDetail> paymentRequestDetails,
			Set<DirectPayment> directPayments) {
		this.implementation = implementation;
		this.personByCreatedBy = personByCreatedBy;
		this.personByPersonId = personByPersonId;
		this.paymentReqNo = paymentReqNo;
		this.requestDate = requestDate;
		this.status = status;
		this.isApprove = isApprove;
		this.createdDate = createdDate;
		this.description = description;
		this.relativeId = relativeId;
		this.paymentRequestDetails = paymentRequestDetails;
		this.directPayments = directPayments;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "payment_request_id", unique = true, nullable = false)
	public Long getPaymentRequestId() {
		return this.paymentRequestId;
	}

	public void setPaymentRequestId(Long paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by", nullable = false)
	public Person getPersonByCreatedBy() {
		return this.personByCreatedBy;
	}

	public void setPersonByCreatedBy(Person personByCreatedBy) {
		this.personByCreatedBy = personByCreatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id")
	public Person getPersonByPersonId() {
		return this.personByPersonId;
	}

	public void setPersonByPersonId(Person personByPersonId) {
		this.personByPersonId = personByPersonId;
	}

	@Column(name = "payment_req_no", nullable = false, length = 20)
	public String getPaymentReqNo() {
		return this.paymentReqNo;
	}

	public void setPaymentReqNo(String paymentReqNo) {
		this.paymentReqNo = paymentReqNo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "request_date", nullable = false, length = 10)
	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	@Column(name = "status")
	public Byte getStatus() {
		return this.status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paymentRequest")
	public Set<PaymentRequestDetail> getPaymentRequestDetails() {
		return this.paymentRequestDetails;
	}

	public void setPaymentRequestDetails(
			Set<PaymentRequestDetail> paymentRequestDetails) {
		this.paymentRequestDetails = paymentRequestDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "paymentRequest")
	public Set<DirectPayment> getDirectPayments() {
		return this.directPayments;
	}

	public void setDirectPayments(Set<DirectPayment> directPayments) {
		this.directPayments = directPayments;
	}

	@Column(name = "person_name", length = 250)
	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

}
