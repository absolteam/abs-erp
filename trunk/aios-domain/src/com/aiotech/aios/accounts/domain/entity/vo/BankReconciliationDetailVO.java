package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.BankReconciliationDetail;

public class BankReconciliationDetailVO extends BankReconciliationDetail implements java.io.Serializable {

	private String transactionDateStr;
	private String receiptAmount;
	private String paymentAmount; 
	private String chequeDateStr;
	private String chequeNumber;
	private String useCaseName;
	private Long flowId;
	private Boolean receiptSide;
	private boolean reconsoleStatus;
	private Boolean chequeEntry;
	
	public String getTransactionDateStr() {
		return transactionDateStr;
	}

	public void setTransactionDateStr(String transactionDateStr) {
		this.transactionDateStr = transactionDateStr;
	}

	public String getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	} 
	 
	public Long getFlowId() {
		return flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Boolean getReceiptSide() {
		return receiptSide;
	}

	public void setReceiptSide(Boolean receiptSide) {
		this.receiptSide = receiptSide;
	}

	public boolean isReconsoleStatus() {
		return reconsoleStatus;
	}

	public void setReconsoleStatus(boolean reconsoleStatus) {
		this.reconsoleStatus = reconsoleStatus;
	}

	public String getChequeDateStr() {
		return chequeDateStr;
	}

	public void setChequeDateStr(String chequeDateStr) {
		this.chequeDateStr = chequeDateStr;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public Boolean getChequeEntry() {
		return chequeEntry;
	}

	public void setChequeEntry(Boolean chequeEntry) {
		this.chequeEntry = chequeEntry;
	} 

	public String getUseCaseName() {
		return useCaseName;
	}

	public void setUseCaseName(String useCaseName) {
		this.useCaseName = useCaseName;
	} 
}
