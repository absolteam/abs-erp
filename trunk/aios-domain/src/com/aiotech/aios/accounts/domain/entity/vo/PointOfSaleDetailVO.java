package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PointOfSaleDetail;

public class PointOfSaleDetailVO extends PointOfSaleDetail implements java.io.Serializable{

	private Boolean nonExchanged;
	private String totalPrice;
	private String productUnitPrice;
	private String productName;
	private double returnQuantity;
	private Double totalAmount;
	private String discountMethodName;
	private String discount;
	private Double totalDetailAmount;
	private String totalDetailAmountStr;
	private Double discountDetailAmount;
	private Long recordId;
	private Integer shelfId;
	private Double productCostPrice;
	private Double totalGain;
	private Double totalCost;
	private String storeName;

	public PointOfSaleDetailVO() {
	}

	public PointOfSaleDetailVO(PointOfSaleDetail pointOfSaleDetail) {
		this.setPointOfSaleDetailId(pointOfSaleDetail.getPointOfSaleDetailId());
		this.setDiscountMethod(pointOfSaleDetail.getDiscountMethod());
		this.setProductByProductId(pointOfSaleDetail.getProductByProductId());
		this.setPromotionMethod(pointOfSaleDetail.getPromotionMethod());
		this.setPointOfSale(pointOfSaleDetail.getPointOfSale());
		this.setProductPricingCalc(pointOfSaleDetail.getProductPricingCalc());
		this.setQuantity(pointOfSaleDetail.getQuantity());
		this.setIsPercentageDiscount(pointOfSaleDetail
				.getIsPercentageDiscount());
		this.setDiscountValue(pointOfSaleDetail.getDiscountValue());
		this.setActualPoints(pointOfSaleDetail.getActualPoints());
		this.setPromotionPoints(pointOfSaleDetail.getPromotionPoints());
		this.setMerchandiseExchangeDetails(pointOfSaleDetail
				.getMerchandiseExchangeDetails());
	}

	public Boolean getNonExchanged() {
		return nonExchanged;
	}

	public void setNonExchanged(Boolean nonExchanged) {
		this.nonExchanged = nonExchanged;
	}

	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getProductUnitPrice() {
		return productUnitPrice;
	}

	public void setProductUnitPrice(String productUnitPrice) {
		this.productUnitPrice = productUnitPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(double returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getDiscountMethodName() {
		return discountMethodName;
	}

	public void setDiscountMethodName(String discountMethodName) {
		this.discountMethodName = discountMethodName;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public Double getTotalDetailAmount() {
		return totalDetailAmount;
	}

	public void setTotalDetailAmount(Double totalDetailAmount) {
		this.totalDetailAmount = totalDetailAmount;
	}

	public String getTotalDetailAmountStr() {
		return totalDetailAmountStr;
	}

	public void setTotalDetailAmountStr(String totalDetailAmountStr) {
		this.totalDetailAmountStr = totalDetailAmountStr;
	}

	public Double getDiscountDetailAmount() {
		return discountDetailAmount;
	}

	public void setDiscountDetailAmount(Double discountDetailAmount) {
		this.discountDetailAmount = discountDetailAmount;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public Double getProductCostPrice() {
		return productCostPrice;
	}

	public void setProductCostPrice(Double productCostPrice) {
		this.productCostPrice = productCostPrice;
	}

	public Double getTotalGain() {
		return totalGain;
	}

	public void setTotalGain(Double totalGain) {
		this.totalGain = totalGain;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
}
