package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.MerchandiseExchange;

public class MerchandiseExchangeVO extends MerchandiseExchange {

	private String exchangeDate;

	public String getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(String exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	 
}
