package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.LedgerFiscal;

public class LedgerFiscalVO extends LedgerFiscal implements java.io.Serializable{

	private String periodName;
	private String accountName;
	private String debitBalance;
	private String creditBalance;
	private String accountType;
	
	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getDebitBalance() {
		return debitBalance;
	}

	public void setDebitBalance(String debitBalance) {
		this.debitBalance = debitBalance;
	}

	public String getCreditBalance() {
		return creditBalance;
	}

	public void setCreditBalance(String creditBalance) {
		this.creditBalance = creditBalance;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	} 
}
