package com.aiotech.aios.accounts.domain.entity;

// Generated Jul 16, 2014 11:00:49 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Bank generated by hbm2java
 */
@Entity
@Table(name = "ac_bank", catalog = "aios")
public class Bank implements java.io.Serializable {

	private Long bankId;
	private Implementation implementation;
	private Person person;
	private Combination combination;
	private String bankName;
	private Byte institutionType;
	private Date createdDate;
	private Byte isApprove;
	private Long relativeId;
	private Set<BankAccount> bankAccounts = new HashSet<BankAccount>(0);

	public Bank() {
	}

	public Bank(Implementation implementation, Person person, String bankName,
			Byte institutionType, Date createdDate) {
		this.implementation = implementation;
		this.person = person;
		this.bankName = bankName;
		this.institutionType = institutionType;
		this.createdDate = createdDate;
	}

	public Bank(Implementation implementation, Person person,
			Combination combination, String bankName, Byte institutionType,
			Date createdDate, Set<BankAccount> bankAccounts,
			Byte isApprove,Long relativeId) {
		this.implementation = implementation;
		this.person = person;
		this.combination = combination;
		this.bankName = bankName;
		this.institutionType = institutionType;
		this.createdDate = createdDate;
		this.bankAccounts = bankAccounts;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "bank_id", unique = true, nullable = false)
	public Long getBankId() {
		return this.bankId;
	}

	public void setBankId(Long bankId) {
		this.bankId = bankId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by", nullable = false)
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combination_id")
	public Combination getCombination() {
		return this.combination;
	}

	public void setCombination(Combination combination) {
		this.combination = combination;
	}

	@Column(name = "bank_name", nullable = false, length = 100)
	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name = "institution_type", nullable = false)
	public Byte getInstitutionType() {
		return this.institutionType;
	}

	public void setInstitutionType(Byte institutionType) {
		this.institutionType = institutionType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", nullable = false, length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bank")
	public Set<BankAccount> getBankAccounts() {
		return this.bankAccounts;
	}

	public void setBankAccounts(Set<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

}
