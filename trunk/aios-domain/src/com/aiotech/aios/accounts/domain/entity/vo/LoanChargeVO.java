package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.LoanCharge;

public class LoanChargeVO extends LoanCharge{
	private List<LoanCharge> loanCharges=new ArrayList<LoanCharge>();

	public List<LoanCharge> getLoanCharges() {
		return loanCharges;
	}

	public void setLoanCharges(List<LoanCharge> loanCharges) {
		this.loanCharges = loanCharges;
	}
}
