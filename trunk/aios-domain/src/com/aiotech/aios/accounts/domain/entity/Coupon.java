package com.aiotech.aios.accounts.domain.entity;

// Generated Jan 18, 2015 3:53:34 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Coupon generated by hbm2java
 */
@Entity
@Table(name = "ac_coupon", catalog = "aios")
public class Coupon implements java.io.Serializable {

	private Long couponId;
	private Implementation implementation;
	private String couponName;
	private Byte couponType;
	private Integer startDigit;
	private Integer endDigit;
	private Date fromDate;
	private Date toDate;
	private String description;
	private Boolean status;
	private Set<PromotionMethod> promotionMethods = new HashSet<PromotionMethod>(
			0);
	private Set<RewardPolicy> rewardPolicies = new HashSet<RewardPolicy>(0);
	private Set<PointOfSaleOffer> pointOfSaleOffers = new HashSet<PointOfSaleOffer>(
			0);

	public Coupon() {
	}

	public Coupon(Implementation implementation, String couponName,
			Byte couponType, Integer startDigit, Integer endDigit,
			Date fromDate, Boolean status) {
		this.implementation = implementation;
		this.couponName = couponName;
		this.couponType = couponType;
		this.startDigit = startDigit;
		this.endDigit = endDigit;
		this.fromDate = fromDate;
		this.status = status;
	}

	public Coupon(Implementation implementation, String couponName,
			Byte couponType, Integer startDigit, Integer endDigit,
			Date fromDate, Date toDate, String description, Boolean status,
			Set<PromotionMethod> promotionMethods,
			Set<RewardPolicy> rewardPolicies,
			Set<PointOfSaleOffer> pointOfSaleOffers) {
		this.implementation = implementation;
		this.couponName = couponName;
		this.couponType = couponType;
		this.startDigit = startDigit;
		this.endDigit = endDigit;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.description = description;
		this.status = status;
		this.promotionMethods = promotionMethods;
		this.rewardPolicies = rewardPolicies;
		this.pointOfSaleOffers = pointOfSaleOffers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "coupon_id", unique = true, nullable = false)
	public Long getCouponId() {
		return this.couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@Column(name = "coupon_name", nullable = false, length = 40)
	public String getCouponName() {
		return this.couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	@Column(name = "coupon_type", nullable = false)
	public Byte getCouponType() {
		return this.couponType;
	}

	public void setCouponType(Byte couponType) {
		this.couponType = couponType;
	}

	@Column(name = "start_digit", nullable = false)
	public Integer getStartDigit() {
		return this.startDigit;
	}

	public void setStartDigit(Integer startDigit) {
		this.startDigit = startDigit;
	}

	@Column(name = "end_digit", nullable = false)
	public Integer getEndDigit() {
		return this.endDigit;
	}

	public void setEndDigit(Integer endDigit) {
		this.endDigit = endDigit;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "from_date", nullable = false, length = 10)
	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "to_date", length = 10)
	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "status", nullable = false)
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "coupon")
	public Set<PromotionMethod> getPromotionMethods() {
		return this.promotionMethods;
	}

	public void setPromotionMethods(Set<PromotionMethod> promotionMethods) {
		this.promotionMethods = promotionMethods;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "coupon")
	public Set<RewardPolicy> getRewardPolicies() {
		return this.rewardPolicies;
	}

	public void setRewardPolicies(Set<RewardPolicy> rewardPolicies) {
		this.rewardPolicies = rewardPolicies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "coupon")
	public Set<PointOfSaleOffer> getPointOfSaleOffers() {
		return this.pointOfSaleOffers;
	}

	public void setPointOfSaleOffers(Set<PointOfSaleOffer> pointOfSaleOffers) {
		this.pointOfSaleOffers = pointOfSaleOffers;
	}

	private Byte isApprove; 
	private Long relativeId;

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}
}
