package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Credit;

public class CreditNoteVO extends Credit {

	private String creditDate;
	private String customerName;
	private String customerReference;
	private String currencyName;
	private String customerNameAndNo;
	private String statusString;
	private String processType;
	private String salesReference;
	private Double totalDeduction;
	private List<CreditDetailVO> creditDetailVOs;
	private Boolean paymentFlag;
	
	public CreditNoteVO() {
		
	}
	
	public CreditNoteVO(Credit credit) {
		this.setCreditId(credit.getCreditId());
		this.setCurrency(credit.getCurrency());
		this.setImplementation(credit.getImplementation());
		this.setCustomer(credit.getCustomer());
		this.setCreditNumber(credit.getCreditNumber());
		this.setDate(credit.getDate());
		this.setDescription(credit.getDescription());
		this.setCreditDetails(credit.getCreditDetails());
	}
	
	public String getCreditDate() {
		return creditDate;
	}
	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerReference() {
		return customerReference;
	}
	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCustomerNameAndNo() {
		return customerNameAndNo;
	}

	public void setCustomerNameAndNo(String customerNameAndNo) {
		this.customerNameAndNo = customerNameAndNo;
	}

	public List<CreditDetailVO> getCreditDetailVOs() {
		return creditDetailVOs;
	}

	public void setCreditDetailVOs(List<CreditDetailVO> creditDetailVOs) {
		this.creditDetailVOs = creditDetailVOs;
	}

	public String getStatusString() {
		return statusString;
	}

	public void setStatusString(String statusString) {
		this.statusString = statusString;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getSalesReference() {
		return salesReference;
	}

	public void setSalesReference(String salesReference) {
		this.salesReference = salesReference;
	}

	public Double getTotalDeduction() {
		return totalDeduction;
	}

	public void setTotalDeduction(Double totalDeduction) {
		this.totalDeduction = totalDeduction;
	}

	public Boolean getPaymentFlag() {
		return paymentFlag;
	}

	public void setPaymentFlag(Boolean paymentFlag) {
		this.paymentFlag = paymentFlag;
	}
	
}
