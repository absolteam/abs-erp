package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetRevaluation;

public class AssetRevaluationVO extends AssetRevaluation {

	private String date;
	private String valuationTypeName;
	private String revaluationTypeName;
	private String sourceName;
	private String sourceDetailName;
	private String methodName;
	private String assetName;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getValuationTypeName() {
		return valuationTypeName;
	}
	public void setValuationTypeName(String valuationTypeName) {
		this.valuationTypeName = valuationTypeName;
	}
	public String getRevaluationTypeName() {
		return revaluationTypeName;
	}
	public void setRevaluationTypeName(String revaluationTypeName) {
		this.revaluationTypeName = revaluationTypeName;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getSourceDetailName() {
		return sourceDetailName;
	}
	public void setSourceDetailName(String sourceDetailName) {
		this.sourceDetailName = sourceDetailName;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
}
