package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.MaterialRequisitionDetail;

public class MaterialRequisitionDetailVO extends MaterialRequisitionDetail implements java.io.Serializable {

	private Double requisitionQty;
 	private Double transferQty;
	private Double transferedQty;
	private Double pendingQty;
	private Double unitRate;
	private Double totalAmount;
	private Long transferReason;
	private Long transferType;
	private Long materialTransferDetailId;
	private String storeName;
	private Integer storeId;
 	private String productName;
	private String unitName;
	private String transferStoreName;
	private Integer shelfFromId;
	private Integer shelfToId; 
	public MaterialRequisitionDetailVO() {
	}
	
	public MaterialRequisitionDetailVO(MaterialRequisitionDetail materialRequisitionDetail) {
		this.setMaterialRequisitionDetailId(materialRequisitionDetail.getMaterialRequisitionDetailId());
		this.setProduct(materialRequisitionDetail.getProduct());
		this.setMaterialRequisition(materialRequisitionDetail.getMaterialRequisition());
		this.setQuantity(materialRequisitionDetail.getQuantity());
		this.setDescription(materialRequisitionDetail.getDescription());
		this.setMaterialTransferDetails(materialRequisitionDetail.getMaterialTransferDetails());
	}
	
	public Double getRequisitionQty() {
		return requisitionQty;
	}

	public void setRequisitionQty(Double requisitionQty) {
		this.requisitionQty = requisitionQty;
	}

	public Double getTransferQty() {
		return transferQty;
	}

	public void setTransferQty(Double transferQty) {
		this.transferQty = transferQty;
	}

	public Long getMaterialTransferDetailId() {
		return materialTransferDetailId;
	}

	public void setMaterialTransferDetailId(Long materialTransferDetailId) {
		this.materialTransferDetailId = materialTransferDetailId;
	}

	public Double getTransferedQty() {
		return transferedQty;
	}

	public void setTransferedQty(Double transferedQty) {
		this.transferedQty = transferedQty;
	}

	public Long getTransferReason() {
		return transferReason;
	}

	public void setTransferReason(Long transferReason) {
		this.transferReason = transferReason;
	}

	public Long getTransferType() {
		return transferType;
	}

	public void setTransferType(Long transferType) {
		this.transferType = transferType;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Integer getShelfFromId() {
		return shelfFromId;
	}

	public void setShelfFromId(Integer shelfFromId) {
		this.shelfFromId = shelfFromId;
	}

	public Integer getShelfToId() {
		return shelfToId;
	}

	public void setShelfToId(Integer shelfToId) {
		this.shelfToId = shelfToId;
	}

	public String getTransferStoreName() {
		return transferStoreName;
	}

	public void setTransferStoreName(String transferStoreName) {
		this.transferStoreName = transferStoreName;
	}

	public Double getPendingQty() {
		return pendingQty;
	}

	public void setPendingQty(Double pendingQty) {
		this.pendingQty = pendingQty;
	} 
}
