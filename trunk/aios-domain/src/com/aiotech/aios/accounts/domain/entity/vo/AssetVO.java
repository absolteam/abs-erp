package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetCreation;

public class AssetVO extends AssetCreation {

	private String ownerName;
	private String assetClassCategory;
	private String assetSubClass;
	private String assetType;
	private String condition;
	private Character ownerType;
	private Long ownerId;
	private String depreciationDate;
	private Double assetCost;
 	private Double depreciationExpenses;
 	private Double bookValue;
 	private Double surplusValue;
 	private Double revaluationReserve;
 	private Double accumulatedSurplus;
 	private String showAssetCost;
 	private String showBookValue;
 	private String showAccumulatedDepreciation; 
 	private String showAccumulatedSurplus;
  	private int conventionValue;
  	
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getAssetSubClass() {
		return assetSubClass;
	}
	public void setAssetSubClass(String assetSubClass) {
		this.assetSubClass = assetSubClass;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getAssetClassCategory() {
		return assetClassCategory;
	}
	public void setAssetClassCategory(String assetClassCategory) {
		this.assetClassCategory = assetClassCategory;
	}
	public Character getOwnerType() {
		return ownerType;
	}
	public void setOwnerType(Character ownerType) {
		this.ownerType = ownerType;
	}
	public Long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	public String getDepreciationDate() {
		return depreciationDate;
	}
	public void setDepreciationDate(String depreciationDate) {
		this.depreciationDate = depreciationDate;
	} 
	public Double getAssetCost() {
		return assetCost;
	}
	public void setAssetCost(Double assetCost) {
		this.assetCost = assetCost;
	}
	public Double getDepreciationExpenses() {
		return depreciationExpenses;
	}
	public void setDepreciationExpenses(Double depreciationExpenses) {
		this.depreciationExpenses = depreciationExpenses;
	}
	public int getConventionValue() {
		return conventionValue;
	}
	public void setConventionValue(int conventionValue) {
		this.conventionValue = conventionValue;
	}
	public String getShowAssetCost() {
		return showAssetCost;
	}
	public void setShowAssetCost(String showAssetCost) {
		this.showAssetCost = showAssetCost;
	}
	public String getShowBookValue() {
		return showBookValue;
	}
	public void setShowBookValue(String showBookValue) {
		this.showBookValue = showBookValue;
	}
	public String getShowAccumulatedDepreciation() {
		return showAccumulatedDepreciation;
	}
	public void setShowAccumulatedDepreciation(String showAccumulatedDepreciation) {
		this.showAccumulatedDepreciation = showAccumulatedDepreciation;
	}
	public Double getBookValue() {
		return bookValue;
	}
	public void setBookValue(Double bookValue) {
		this.bookValue = bookValue;
	}
	public Double getSurplusValue() {
		return surplusValue;
	}
	public void setSurplusValue(Double surplusValue) {
		this.surplusValue = surplusValue;
	}
	public Double getRevaluationReserve() {
		return revaluationReserve;
	}
	public void setRevaluationReserve(Double revaluationReserve) {
		this.revaluationReserve = revaluationReserve;
	}
	public Double getAccumulatedSurplus() {
		return accumulatedSurplus;
	}
	public void setAccumulatedSurplus(Double accumulatedSurplus) {
		this.accumulatedSurplus = accumulatedSurplus;
	}
	public String getShowAccumulatedSurplus() {
		return showAccumulatedSurplus;
	}
	public void setShowAccumulatedSurplus(String showAccumulatedSurplus) {
		this.showAccumulatedSurplus = showAccumulatedSurplus;
	}
	 
}
