package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.IssueReturnDetail;

public class IssueReturnDetailVO extends IssueReturnDetail {

	private String productName;
	private String productCode;
	private String issueReference;
	private Double issueQty;
	private String materialCondition;
	
	public IssueReturnDetailVO() {
		
	}
	
	public IssueReturnDetailVO(IssueReturnDetail issueReturnDetail) {
		this.setIssueReturnDetailId(issueReturnDetail.getIssueReturnDetailId());
		this.setIssueReturn(issueReturnDetail.getIssueReturn());
		this.setProduct(issueReturnDetail.getProduct());
		this.setIssueRequistionDetail(issueReturnDetail.getIssueRequistionDetail());
		this.setLookupDetail(issueReturnDetail.getLookupDetail());
		this.setReturnQuantity(issueReturnDetail.getReturnQuantity());
		this.setDescription(issueReturnDetail.getDescription());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getIssueReference() {
		return issueReference;
	}

	public void setIssueReference(String issueReference) {
		this.issueReference = issueReference;
	}

	public Double getIssueQty() {
		return issueQty;
	}

	public void setIssueQty(Double issueQty) {
		this.issueQty = issueQty;
	}

	public String getMaterialCondition() {
		return materialCondition;
	}

	public void setMaterialCondition(String materialCondition) {
		this.materialCondition = materialCondition;
	}
	
}
