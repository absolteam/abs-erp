package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.Date;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankDeposit;
import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;
import com.aiotech.aios.realestate.domain.entity.ContractPayment;

public class BankDepositVO extends BankDeposit implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BankDepositDetail bankDepositDetail;
	private String useCase;
	private Long recordId;
	private String recordDate;
	private String recordAmount;
	private String bankDepositDetailStr;
	private ContractPayment contractPayment;
	private String name;
	private Long bankAccountCombinationId;
	private String accountNumber;
	private List<BankDepositVO> bankDepositDetailList;
	private String linkLabel;
	private String anchorExpression;
	private String chargesType;
	private Boolean paymentType;
	private Boolean cancelContract;
	private String depositAmount;
	private String otherAmount;
	private String mangtAmount;
	private String tenantAmount;
	private Date offerMontly;
	private Date rentMonthly;
	private long contractPaymentId;
	private double monthlyRental;
	private String rentPDCAmount;
	private String depositor;
	private String createdByPerson;
	private String bankName;
	private String bankAccountNumber;
	private List<BankDepositDetailVO> bankDepositDetailVOs;
	
	public BankDepositVO() {
		
	}
	
	public BankDepositVO(BankDeposit bankDeposit) {
		this.setBankDepositId(bankDeposit.getBankDepositId());
		this.setPersonByDepositerId(bankDeposit.getPersonByDepositerId());
		this.setImplementation(bankDeposit.getImplementation());
		this.setPersonByCreatedBy(bankDeposit.getPersonByCreatedBy());
		this.setBankDepositNumber(bankDeposit.getBankDepositNumber());
		this.setDate(bankDeposit.getDate());
		this.setDescription(bankDeposit.getDescription());
		this.setCreatedDate(bankDeposit.getCreatedDate());
		this.setBankDepositDetails(bankDeposit.getBankDepositDetails());
	}
	
	public BankDepositDetail getBankDepositDetail() {
		return bankDepositDetail;
	}

	public void setBankDepositDetail(BankDepositDetail bankDepositDetail) {
		this.bankDepositDetail = bankDepositDetail;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	
	public String getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}

	public String getRecordAmount() {
		return recordAmount;
	}

	public void setRecordAmount(String recordAmount) {
		this.recordAmount = recordAmount;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getBankDepositDetailStr() {
		return bankDepositDetailStr;
	}

	public void setBankDepositDetailStr(String bankDepositDetailStr) {
		this.bankDepositDetailStr = bankDepositDetailStr;
	}

	public ContractPayment getContractPayment() {
		return contractPayment;
	}

	public void setContractPayment(ContractPayment contractPayment) {
		this.contractPayment = contractPayment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getBankAccountCombinationId() {
		return bankAccountCombinationId;
	}

	public void setBankAccountCombinationId(Long bankAccountCombinationId) {
		this.bankAccountCombinationId = bankAccountCombinationId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public List<BankDepositVO> getBankDepositDetailList() {
		return bankDepositDetailList;
	}

	public void setBankDepositDetailList(List<BankDepositVO> bankDepositDetailList) {
		this.bankDepositDetailList = bankDepositDetailList;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public String getChargesType() {
		return chargesType;
	}

	public void setChargesType(String chargesType) {
		this.chargesType = chargesType;
	}

	public Boolean getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Boolean paymentType) {
		this.paymentType = paymentType;
	} 

	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	public Boolean getCancelContract() {
		return cancelContract;
	}

	public void setCancelContract(Boolean cancelContract) {
		this.cancelContract = cancelContract;
	}

	public String getOtherAmount() {
		return otherAmount;
	}

	public void setOtherAmount(String otherAmount) {
		this.otherAmount = otherAmount;
	}

	public String getMangtAmount() {
		return mangtAmount;
	}

	public void setMangtAmount(String mangtAmount) {
		this.mangtAmount = mangtAmount;
	}

	public String getTenantAmount() {
		return tenantAmount;
	}

	public void setTenantAmount(String tenantAmount) {
		this.tenantAmount = tenantAmount;
	}

	public Date getOfferMontly() {
		return offerMontly;
	}

	public void setOfferMontly(Date offerMontly) {
		this.offerMontly = offerMontly;
	}

	public Date getRentMonthly() {
		return rentMonthly;
	}

	public void setRentMonthly(Date rentMonthly) {
		this.rentMonthly = rentMonthly;
	}

	public long getContractPaymentId() {
		return contractPaymentId;
	}

	public void setContractPaymentId(long contractPaymentId) {
		this.contractPaymentId = contractPaymentId;
	}

	public double getMonthlyRental() {
		return monthlyRental;
	}

	public void setMonthlyRental(double monthlyRental) {
		this.monthlyRental = monthlyRental;
	}

	public String getRentPDCAmount() {
		return rentPDCAmount;
	}

	public void setRentPDCAmount(String rentPDCAmount) {
		this.rentPDCAmount = rentPDCAmount;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public String getCreatedByPerson() {
		return createdByPerson;
	}

	public void setCreatedByPerson(String createdByPerson) {
		this.createdByPerson = createdByPerson;
	}

	public List<BankDepositDetailVO> getBankDepositDetailVOs() {
		return bankDepositDetailVOs;
	}

	public void setBankDepositDetailVOs(
			List<BankDepositDetailVO> bankDepositDetailVOs) {
		this.bankDepositDetailVOs = bankDepositDetailVOs;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	} 
			
}
