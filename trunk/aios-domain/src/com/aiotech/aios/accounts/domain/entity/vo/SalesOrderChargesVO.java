package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.SalesOrderCharge;

public class SalesOrderChargesVO extends SalesOrderCharge{
	
	private String chargeType;
	private String chargeMode;
	
	public SalesOrderChargesVO() {
		
	}
	
	public SalesOrderChargesVO(SalesOrderCharge salesOrderCharge) {
		this.setSalesOrderChargeId(salesOrderCharge.getSalesOrderChargeId());
		this.setLookupDetail(salesOrderCharge.getLookupDetail());
		this.setSalesOrder(salesOrderCharge.getSalesOrder());
		this.setIsPercentage(salesOrderCharge.getIsPercentage());
		this.setCharges(salesOrderCharge.getCharges());
		this.setDescription(salesOrderCharge.getDescription());
	}
	
	public String getChargeType() {
		return chargeType;
	}
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	public String getChargeMode() {
		return chargeMode;
	}
	public void setChargeMode(String chargeMode) {
		this.chargeMode = chargeMode;
	}
	
	

}
