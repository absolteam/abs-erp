package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.CreditTerm;

public class CreditTermVO extends CreditTerm implements java.io.Serializable{

	private String customerSupplierName;
	private String strDueDay;
	private String strDiscountMode;
	private String strPenaltyMode;
	private String strPenaltyType;
	
	public CreditTermVO() {
		
	}
	
	public CreditTermVO(CreditTerm creditTerm) {
		this.setCreditTermId(creditTerm.getCreditTermId()); 
		this.setImplementation(creditTerm.getImplementation()); 
		this.setName(creditTerm.getName());
		this.setDueDay(creditTerm.getDueDay());
		this.setDiscountDay(creditTerm.getDiscountDay());
		this.setDiscountMode(creditTerm.getDiscountMode());
		this.setDiscount(creditTerm.getDiscount());
		this.setPenaltyType(creditTerm.getPenaltyType());
		this.setPenaltyMode(creditTerm.getPenaltyMode());
		this.setPenalty(creditTerm.getPenalty());
		this.setIsSupplier(creditTerm.getIsSupplier());
		this.setSuppliers(creditTerm.getSuppliers());
		this.setCustomers(creditTerm.getCustomers());
		this.setPurchases(creditTerm.getPurchases());
		this.setSalesInvoices(creditTerm.getSalesInvoices());
	}
	
	public String getCustomerSupplierName() {
		return customerSupplierName;
	}
	public void setCustomerSupplierName(String customerSupplierName) {
		this.customerSupplierName = customerSupplierName;
	}
	public String getStrDueDay() {
		return strDueDay;
	}
	public void setStrDueDay(String strDueDay) {
		this.strDueDay = strDueDay;
	}
	public String getStrDiscountMode() {
		return strDiscountMode;
	}
	public void setStrDiscountMode(String strDiscountMode) {
		this.strDiscountMode = strDiscountMode;
	}
	public String getStrPenaltyMode() {
		return strPenaltyMode;
	}
	public void setStrPenaltyMode(String strPenaltyMode) {
		this.strPenaltyMode = strPenaltyMode;
	}

	public String getStrPenaltyType() {
		return strPenaltyType;
	}

	public void setStrPenaltyType(String strPenaltyType) {
		this.strPenaltyType = strPenaltyType;
	}
}
