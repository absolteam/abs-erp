package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.Supplier;

public class SupplierVO extends Supplier {

	private String type;
	private String supplierTypeName;
	private String supplierName;
	private long combinationId;
	private String accountCode;
	private Long creditTermId;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getSupplierTypeName() {
		return supplierTypeName;
	}

	public void setSupplierTypeName(String supplierTypeName) {
		this.supplierTypeName = supplierTypeName;
	}

	public Long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(Long creditTermId) {
		this.creditTermId = creditTermId;
	}
}
