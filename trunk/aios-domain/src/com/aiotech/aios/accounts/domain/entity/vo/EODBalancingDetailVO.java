package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.EODBalancingDetail;

public class EODBalancingDetailVO extends EODBalancingDetail implements java.io.Serializable {
	
	private String strReceiptType;
	private Double varianceAmount;
	private Integer quantity;
	
	private List<EODProductionDetailVO> eodProductionDetailVOs = new ArrayList<EODProductionDetailVO>();
	
	public EODBalancingDetailVO() {
	}
	
	public EODBalancingDetailVO (EODBalancingDetail eodBalancingDetail) {
		this.setEodBalancingDetailId(eodBalancingDetail.getEodBalancingDetailId());
		this.setEODBalancing(eodBalancingDetail.getEODBalancing());
		this.setProduct(eodBalancingDetail.getProduct());
		this.setProductQuantity(eodBalancingDetail.getProductQuantity()); 
	}

	public String getStrReceiptType() {
		return strReceiptType;
	}

	public void setStrReceiptType(String strReceiptType) {
		this.strReceiptType = strReceiptType;
	}

	public Double getVarianceAmount() {
		return varianceAmount;
	}

	public void setVarianceAmount(Double varianceAmount) {
		this.varianceAmount = varianceAmount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public List<EODProductionDetailVO> getEodProductionDetailVOs() {
		return eodProductionDetailVOs;
	}

	public void setEodProductionDetailVOs(
			List<EODProductionDetailVO> eodProductionDetailVOs) {
		this.eodProductionDetailVOs = eodProductionDetailVOs;
	}
	
}
