package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.RequisitionDetail;

public class RequisitionDetailVO extends RequisitionDetail implements java.io.Serializable {

	private Double unitRate;
	private Double totalAmount;
	private Double availableQty;
	private Double packageUnit;
	private String requisitionQty;
	private String quotationQty;
	private String purchaseQty;
	private String receiveQty;
	private String returnQty;
	private String balanceQty;
	private String issuedQty;
	private String issueReturnQty; 
	private String productName;
	private String productCode;
	private String storeName;
	private String batchNumber;
	private String expiryDate;
	private String itemType;
	private long storeId;
	private int shelfId;
	private Long packageDetailId;
	private RequisitionVO requisitionVO;
	private List<ProductPackageVO> productPackageVOs;
	
	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Double availableQty) {
		this.availableQty = availableQty;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public RequisitionVO getRequisitionVO() {
		return requisitionVO;
	}

	public void setRequisitionVO(RequisitionVO requisitionVO) {
		this.requisitionVO = requisitionVO;
	}

	public String getRequisitionQty() {
		return requisitionQty;
	}

	public void setRequisitionQty(String requisitionQty) {
		this.requisitionQty = requisitionQty;
	}

	public String getQuotationQty() {
		return quotationQty;
	}

	public void setQuotationQty(String quotationQty) {
		this.quotationQty = quotationQty;
	}

	public String getPurchaseQty() {
		return purchaseQty;
	}

	public void setPurchaseQty(String purchaseQty) {
		this.purchaseQty = purchaseQty;
	}

	public String getReceiveQty() {
		return receiveQty;
	}

	public void setReceiveQty(String receiveQty) {
		this.receiveQty = receiveQty;
	}

	public String getReturnQty() {
		return returnQty;
	}

	public void setReturnQty(String returnQty) {
		this.returnQty = returnQty;
	}

	public String getBalanceQty() {
		return balanceQty;
	}

	public void setBalanceQty(String balanceQty) {
		this.balanceQty = balanceQty;
	}

	public String getIssuedQty() {
		return issuedQty;
	}

	public void setIssuedQty(String issuedQty) {
		this.issuedQty = issuedQty;
	}

	public String getIssueReturnQty() {
		return issueReturnQty;
	}

	public void setIssueReturnQty(String issueReturnQty) {
		this.issueReturnQty = issueReturnQty;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public Double getPackageUnit() {
		return packageUnit;
	}

	public void setPackageUnit(Double packageUnit) {
		this.packageUnit = packageUnit;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	} 
}
