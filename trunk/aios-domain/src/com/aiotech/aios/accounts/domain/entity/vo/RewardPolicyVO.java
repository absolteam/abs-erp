package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.RewardPolicy;

public class RewardPolicyVO extends RewardPolicy {

	private String rewardTypeName;
	private String rewardPercentageOrAmount; 
	private CouponVO couponVO;

	public CouponVO getCouponVO() {
		return couponVO;
	}

	public void setCouponVO(CouponVO couponVO) {
		this.couponVO = couponVO;
	}

	public String getRewardTypeName() {
		return rewardTypeName;
	}

	public void setRewardTypeName(String rewardTypeName) {
		this.rewardTypeName = rewardTypeName;
	}

	public String getRewardPercentageOrAmount() {
		return rewardPercentageOrAmount;
	}

	public void setRewardPercentageOrAmount(String rewardPercentageOrAmount) {
		this.rewardPercentageOrAmount = rewardPercentageOrAmount;
	} 
}
