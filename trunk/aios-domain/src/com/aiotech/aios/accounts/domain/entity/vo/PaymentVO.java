package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Payment;

public class PaymentVO extends Payment implements java.io.Serializable{
	
	private String supplierName;
	private String supplierNameAndNumber;
	private String referenceNumber;
	private String strDate;
	private String paymentAccess;
	private String paymentCode;
	private List<PaymentDetailVO> paymentDetailVOs;
	
	public PaymentVO() {
		
	}
	
	public PaymentVO(Payment payment) {
		this.setPaymentId(payment.getPaymentId());
		this.setEmi(payment.getEmi());
		this.setCurrency(payment.getCurrency());
		this.setChequeBook(payment.getChequeBook());
		this.setImplementation(payment.getImplementation());
		this.setPerson(payment.getPerson());
		this.setReceive(payment.getReceive());
		this.setLoan(payment.getLoan());
		this.setCombination(payment.getCombination());
		this.setSupplier(payment.getSupplier());
		this.setPaymentNumber(payment.getPaymentNumber());
		this.setDate(payment.getDate());
		this.setInvoiceNumber(payment.getInvoiceNumber());
		this.setDescription(payment.getDescription());
		this.setCreatedDate(payment.getCreatedDate());
		this.setIsApprove(payment.getIsApprove());
		this.setPaymentMode(payment.getPaymentMode());
		this.setChequeDate(payment.getChequeDate());
		this.setChequeNumber(payment.getChequeNumber());
		this.setPrintTaken(payment.getPrintTaken());
		this.setDiscountReceived(payment.getDiscountReceived());
		this.setRelativeId(payment.getRelativeId());
		this.setPaymentDetails(payment.getPaymentDetails());
		this.setVoidPayments(payment.getVoidPayments());
	}
	
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierNameAndNumber() {
		return supplierNameAndNumber;
	}
	public void setSupplierNameAndNumber(String supplierNameAndNumber) {
		this.supplierNameAndNumber = supplierNameAndNumber;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	public List<PaymentDetailVO> getPaymentDetailVOs() {
		return paymentDetailVOs;
	}

	public void setPaymentDetailVOs(List<PaymentDetailVO> paymentDetailVOs) {
		this.paymentDetailVOs = paymentDetailVOs;
	}

	public String getPaymentAccess() {
		return paymentAccess;
	}

	public void setPaymentAccess(String paymentAccess) {
		this.paymentAccess = paymentAccess;
	}

	public String getPaymentCode() {
		return paymentCode;
	}

	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	
}
