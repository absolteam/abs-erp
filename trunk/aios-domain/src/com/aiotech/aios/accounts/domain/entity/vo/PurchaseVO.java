package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;
import com.aiotech.aios.accounts.domain.entity.Purchase;
import com.aiotech.aios.system.domain.entity.Alert;

public class PurchaseVO extends Purchase implements java.io.Serializable{

	private Double totalPurchaseQty;
	private Double totalPurchaseAmount;
	private String totalPurchaseAmountStr;
	private String amountInWords;
	private Long recordId;
	private String useCase;
	private Object object;
	private Long accountId;
	private String accountCategory;
	private Long combinationId;
	private Double amount;
	private Alert alert;
	private boolean deleteFlag;
	private boolean editFlag;
	private Double oldAmount;
	private Long messageId;
	private boolean batchProduct;
	private List<DirectPaymentDetail> directPayments;
	private String receiveNumber;
	private String requisitionNumber;
	private String createdBy;
	private Double totalPurchaseAmountView;
	private String fromDate;
	private String toDate;
	private String supplierName;
	private String creditTermValue;
	private Long supplierId;
	private Long implementationId;
	private List<PurchaseDetailVO> purchaseDetailVO = new ArrayList<PurchaseDetailVO>();
	private List<PurchaseDetailVO> purchaseDetailBatchVO = new ArrayList<PurchaseDetailVO>();
	private PurchaseDetailVO purchaseDetVo=new PurchaseDetailVO();
	private Long productId;
	private Long departmentId;
	private String departmentName;
	public List<PurchaseDetailVO> getPurchaseDetailVO() {
		return purchaseDetailVO;
	}

	public void setPurchaseDetailVO(List<PurchaseDetailVO> purchaseDetailVO) {
		this.purchaseDetailVO = purchaseDetailVO;
	}

	public Double getTotalPurchaseQty() {
		return totalPurchaseQty;
	}

	public void setTotalPurchaseQty(Double totalPurchaseQty) {
		this.totalPurchaseQty = totalPurchaseQty;
	}

	public Double getTotalPurchaseAmount() {
		return totalPurchaseAmount;
	}

	public void setTotalPurchaseAmount(Double totalPurchaseAmount) {
		this.totalPurchaseAmount = totalPurchaseAmount;
	}

	public Long getRecordId() {
		return recordId;
	}

	public void setRecordId(Long recordId) {
		this.recordId = recordId;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccountCategory() {
		return accountCategory;
	}

	public void setAccountCategory(String accountCategory) {
		this.accountCategory = accountCategory;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	public boolean isDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public Double getOldAmount() {
		return oldAmount;
	}

	public void setOldAmount(Double oldAmount) {
		this.oldAmount = oldAmount;
	}

	public List<DirectPaymentDetail> getDirectPayments() {
		return directPayments;
	}

	public void setDirectPayments(List<DirectPaymentDetail> directPayments) {
		this.directPayments = directPayments;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	} 

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Double getTotalPurchaseAmountView() {
		return totalPurchaseAmountView;
	}

	public void setTotalPurchaseAmountView(Double totalPurchaseAmountView) {
		this.totalPurchaseAmountView = totalPurchaseAmountView;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCreditTermValue() {
		return creditTermValue;
	}

	public void setCreditTermValue(String creditTermValue) {
		this.creditTermValue = creditTermValue;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Long getImplementationId() {
		return implementationId;
	}

	public void setImplementationId(Long implementationId) {
		this.implementationId = implementationId;
	}

	public PurchaseDetailVO getPurchaseDetVo() {
		return purchaseDetVo;
	}

	public void setPurchaseDetVo(PurchaseDetailVO purchaseDetVo) {
		this.purchaseDetVo = purchaseDetVo;
	}

	public boolean isBatchProduct() {
		return batchProduct;
	}

	public void setBatchProduct(boolean batchProduct) {
		this.batchProduct = batchProduct;
	}

	public String getReceiveNumber() {
		return receiveNumber;
	}

	public void setReceiveNumber(String receiveNumber) {
		this.receiveNumber = receiveNumber;
	}

	public List<PurchaseDetailVO> getPurchaseDetailBatchVO() {
		return purchaseDetailBatchVO;
	}

	public void setPurchaseDetailBatchVO(
			List<PurchaseDetailVO> purchaseDetailBatchVO) {
		this.purchaseDetailBatchVO = purchaseDetailBatchVO;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getTotalPurchaseAmountStr() {
		return totalPurchaseAmountStr;
	}

	public void setTotalPurchaseAmountStr(String totalPurchaseAmountStr) {
		this.totalPurchaseAmountStr = totalPurchaseAmountStr;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	} 
}
