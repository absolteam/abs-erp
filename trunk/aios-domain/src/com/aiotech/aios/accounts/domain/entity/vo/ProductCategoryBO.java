package com.aiotech.aios.accounts.domain.entity.vo;

public class ProductCategoryBO {

	private String parentCatgoryName;
	private String categoryName;
	private String description;
	private long productCategoryId;
	
	public String getParentCatgoryName() {
		return parentCatgoryName;
	}
	public void setParentCatgoryName(String parentCatgoryName) {
		this.parentCatgoryName = parentCatgoryName;
	}
	 
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	} 
}
