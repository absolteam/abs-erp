package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.WorkinProcess;

public class WorkinProcessVO extends WorkinProcess implements
		java.io.Serializable {

	private String date;
	private String statusStr;
	private String productName;
	private String personName;
	private String storeName;
	private String requisitionReference;
	private List<WorkinProcessProductionVO> workinProcessProductionVOs = new ArrayList<WorkinProcessProductionVO>();

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public List<WorkinProcessProductionVO> getWorkinProcessProductionVOs() {
		return workinProcessProductionVOs;
	}

	public void setWorkinProcessProductionVOs(
			List<WorkinProcessProductionVO> workinProcessProductionVOs) {
		this.workinProcessProductionVOs = workinProcessProductionVOs;
	}

	public String getRequisitionReference() {
		return requisitionReference;
	}

	public void setRequisitionReference(String requisitionReference) {
		this.requisitionReference = requisitionReference;
	} 
}
