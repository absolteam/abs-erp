package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.Quotation;

public class QuotationVO extends Quotation implements java.io.Serializable{

	private String supplierName;
	private String quotationDateStr;
	private String displayQuantity;
	private String displayUnitRate;
	private String displayTotalAmount;
	
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getQuotationDateStr() {
		return quotationDateStr;
	}
	public void setQuotationDateStr(String quotationDateStr) {
		this.quotationDateStr = quotationDateStr;
	}
	public String getDisplayQuantity() {
		return displayQuantity;
	}
	public void setDisplayQuantity(String displayQuantity) {
		this.displayQuantity = displayQuantity;
	}
	public String getDisplayUnitRate() {
		return displayUnitRate;
	}
	public void setDisplayUnitRate(String displayUnitRate) {
		this.displayUnitRate = displayUnitRate;
	}
	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}
	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}
}
