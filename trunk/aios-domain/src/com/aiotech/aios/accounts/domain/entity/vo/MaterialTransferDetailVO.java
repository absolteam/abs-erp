package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.MaterialTransferDetail;

public class MaterialTransferDetailVO extends MaterialTransferDetail implements java.io.Serializable{
	
	private String productName;
	private String storeFrom;
	private String storeTo;
	private String transferType;
	private String transferReason;
	private Double total;
	private Double returnQty;
	private Double transferedQty;
	private Double totalRate;
	private Double returnedQty;
	private Long returnDetailId; 
	private Long materialCondition;
	private MaterialTransferVO materilaTransferVO;
	
	public MaterialTransferDetailVO(){
	}
	
	public MaterialTransferDetailVO(MaterialTransferDetail materialTransferDetail) {
		this.setMaterialTransferDetailId(materialTransferDetail.getMaterialTransferDetailId());
		this.setProduct(materialTransferDetail.getProduct());
		this.setShelfByFromShelfId(materialTransferDetail.getShelfByFromShelfId());
		this.setShelfByShelfId(materialTransferDetail.getShelfByShelfId());
		this.setMaterialTransfer(materialTransferDetail.getMaterialTransfer());
		this.setLookupDetailByTransferReason(materialTransferDetail.getLookupDetailByTransferReason());
		this.setLookupDetailByTransferType(materialTransferDetail.getLookupDetailByTransferType());
		this.setMaterialRequisitionDetail(materialTransferDetail.getMaterialRequisitionDetail());
		this.setQuantity(materialTransferDetail.getQuantity());
		this.setUnitRate(materialTransferDetail.getUnitRate());
		this.setDescription(materialTransferDetail.getDescription());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreFrom() {
		return storeFrom;
	}

	public void setStoreFrom(String storeFrom) {
		this.storeFrom = storeFrom;
	}

	public String getStoreTo() {
		return storeTo;
	}

	public void setStoreTo(String storeTo) {
		this.storeTo = storeTo;
	}

	public String getTransferType() {
		return transferType;
	}

	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}

	public String getTransferReason() {
		return transferReason;
	}

	public void setTransferReason(String transferReason) {
		this.transferReason = transferReason;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	public MaterialTransferVO getMaterilaTransferVO() {
		return materilaTransferVO;
	}

	public void setMaterilaTransferVO(MaterialTransferVO materilaTransferVO) {
		this.materilaTransferVO = materilaTransferVO;
	}

	public Double getReturnQty() {
		return returnQty;
	}

	public void setReturnQty(Double returnQty) {
		this.returnQty = returnQty;
	}

	public Double getTransferedQty() {
		return transferedQty;
	}

	public void setTransferedQty(Double transferedQty) {
		this.transferedQty = transferedQty;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public Long getReturnDetailId() {
		return returnDetailId;
	}

	public void setReturnDetailId(Long returnDetailId) {
		this.returnDetailId = returnDetailId;
	}

	public Double getReturnedQty() {
		return returnedQty;
	}

	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public Long getMaterialCondition() {
		return materialCondition;
	}

	public void setMaterialCondition(Long materialCondition) {
		this.materialCondition = materialCondition;
	}

}
