package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetWarranty;

public class AssetWarrantyVO extends AssetWarranty implements java.io.Serializable {

	private String warrantyType;
	private String fromDate;
	private String toDate;
	private String assetName;
	private long assetId;
	private long warrantyTypeId;
	
	public String getWarrantyType() {
		return warrantyType;
	}
	public void setWarrantyType(String warrantyType) {
		this.warrantyType = warrantyType;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public long getAssetId() {
		return assetId;
	}
	public void setAssetId(long assetId) {
		this.assetId = assetId;
	}
	public long getWarrantyTypeId() {
		return warrantyTypeId;
	}
	public void setWarrantyTypeId(long warrantyTypeId) {
		this.warrantyTypeId = warrantyTypeId;
	}
	
}
