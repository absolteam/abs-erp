package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.CommissionRule;

public class CommissionRuleVO extends CommissionRule {

	private String baseName;
	private String creditNote;
	private String debitNote;
	private String methodName;
	private String fromDate;
	private String toDate;
	
	public String getBaseName() {
		return baseName;
	}
	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}
	public String getCreditNote() {
		return creditNote;
	}
	public void setCreditNote(String creditNote) {
		this.creditNote = creditNote;
	}
	public String getDebitNote() {
		return debitNote;
	}
	public void setDebitNote(String debitNote) {
		this.debitNote = debitNote;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}
