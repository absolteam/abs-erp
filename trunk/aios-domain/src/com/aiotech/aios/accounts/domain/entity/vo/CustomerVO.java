package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Customer;

public class CustomerVO extends Customer implements java.io.Serializable {

	private String customerName;
	private String type;
	private String accountCode;
	private String mobileNumber;
	private String residentialAddress;
	private long combinationId;
	private long creditTermId;
	private byte customerNature;
	private String strCustomerType;
	private String strCreditTerm;
	private String strRefferalSource;
	private String strMemberType;
	private String emailAddress;
	private boolean isPerson;
	private Boolean status;
	private String poBox;
	private String address;
	private String telephone;
	private String fax;
	private String mobile;
	private List<Customer> customers;
	public CustomerVO() {
		
	}
	
	public CustomerVO(Customer customer) {
		this.setCustomerId(customer.getCustomerId());
		this.setCmpDeptLocation(customer.getCmpDeptLocation());
		this.setCompany(customer.getCompany());
		this.setCreditTerm(customer.getCreditTerm());
		this.setImplementation(customer.getImplementation());
		this.setCombination(customer.getCombination());
		this.setPersonBySaleRepresentative(customer.getPersonBySaleRepresentative());
		this.setPersonByPersonId(customer.getPersonByPersonId());
		this.setCustomerNumber(customer.getCustomerNumber());
		this.setCustomerType(customer.getCustomerType());
		this.setLookupDetailByShippingMethod(customer.getLookupDetailByShippingMethod());
		this.setCreditLimit(customer.getCreditLimit());
		this.setLookupDetailByRefferalSource(customer.getLookupDetailByRefferalSource());
		this.setOpeningBalance(customer.getOpeningBalance());
		this.setBillingAddress(customer.getBillingAddress());
		this.setDescription(customer.getDescription());
		this.setMemberType(customer.getMemberType());
		this.setMemberCardNumber(customer.getMemberCardNumber());
		this.setDirectPayments(customer.getDirectPayments());
		this.setCustomerQuotations(customer.getCustomerQuotations());
		this.setProductPricingCalcs(customer.getProductPricingCalcs());
		this.setShippingDetails(customer.getShippingDetails());
		this.setDiscountOptions(customer.getDiscountOptions());
		this.setSalesOrders(customer.getSalesOrders());
		this.setSalesDeliveryNotes(customer.getSalesDeliveryNotes());
		this.setPointOfSales(customer.getPointOfSales());
		this.setPromotionOptions(customer.getPromotionOptions());
		this.setBankReceiptses(customer.getBankReceiptses());
		this.setSalesInvoices(customer.getSalesInvoices());
		this.setCredits(customer.getCredits());
		
	}
	
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	public long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(long creditTermId) {
		this.creditTermId = creditTermId;
	}

	public String getStrCustomerType() {
		return strCustomerType;
	}

	public void setStrCustomerType(String strCustomerType) {
		this.strCustomerType = strCustomerType;
	}

	public String getStrCreditTerm() {
		return strCreditTerm;
	}

	public void setStrCreditTerm(String strCreditTerm) {
		this.strCreditTerm = strCreditTerm;
	}

	public String getStrRefferalSource() {
		return strRefferalSource;
	}

	public void setStrRefferalSource(String strRefferalSource) {
		this.strRefferalSource = strRefferalSource;
	}

	public String getStrMemberType() {
		return strMemberType;
	}

	public void setStrMemberType(String strMemberType) {
		this.strMemberType = strMemberType;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getResidentialAddress() {
		return residentialAddress;
	}

	public void setResidentialAddress(String residentialAddress) {
		this.residentialAddress = residentialAddress;
	}

	public byte getCustomerNature() {
		return customerNature;
	}

	public void setCustomerNature(byte customerNature) {
		this.customerNature = customerNature;
	}


	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isPerson() {
		return isPerson;
	}

	public void setPerson(boolean isPerson) {
		this.isPerson = isPerson;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	} 
}
