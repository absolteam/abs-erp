package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.Tender;

public class TenderVO extends Tender {

	private Double totalAmount;
	
	public TenderVO() {
		
	}
	
	public TenderVO(Tender tender) {
		this.setTenderId(tender.getTenderId());
		this.setCurrency(tender.getCurrency());
		this.setImplementation(tender.getImplementation());
		this.setTenderNumber(tender.getTenderNumber());
		this.setTenderDate(tender.getTenderDate());
		this.setTenderExpiry(tender.getTenderExpiry());
		this.setDescription(tender.getDescription());
		this.setTenderDetails(tender.getTenderDetails());
		this.setQuotations(tender.getQuotations());
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
