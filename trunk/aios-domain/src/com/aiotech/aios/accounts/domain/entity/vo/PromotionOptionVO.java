package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PromotionOption;

public class PromotionOptionVO extends PromotionOption {

	private String productCode;
	private String productName;
	private String productCategoryName;
	private String productSubCategory;
	private String customerReference;
	private String customerName;
	private String memberCardType;
	private String memberCardNo;
	
	public PromotionOptionVO() {
	}
	
	public PromotionOptionVO(PromotionOption promotionOption) {
		this.setPromotionOptionId(promotionOption.getPromotionOptionId());
		this.setPromotion(promotionOption.getPromotion());
		this.setProductCategory(promotionOption.getProductCategory());
		this.setProduct(promotionOption.getProduct());
		this.setCustomer(promotionOption.getCustomer());
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductSubCategory() {
		return productSubCategory;
	}

	public void setProductSubCategory(String productSubCategory) {
		this.productSubCategory = productSubCategory;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMemberCardType() {
		return memberCardType;
	}

	public void setMemberCardType(String memberCardType) {
		this.memberCardType = memberCardType;
	}

	public String getMemberCardNo() {
		return memberCardNo;
	}

	public void setMemberCardNo(String memberCardNo) {
		this.memberCardNo = memberCardNo;
	}

	public String getProductCategoryName() {
		return productCategoryName;
	}

	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}
	
}
