package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PointOfSaleReceipt;

public class PointOfSaleReceiptVO extends PointOfSaleReceipt implements java.io.Serializable{

	private String receiptTypeStr;
	private String receiptAmountStr;
	private Double receiptAmount;
	
	public String getReceiptTypeStr() {
		return receiptTypeStr;
	}
	public void setReceiptTypeStr(String receiptTypeStr) {
		this.receiptTypeStr = receiptTypeStr;
	}
	public String getReceiptAmountStr() {
		return receiptAmountStr;
	}
	public void setReceiptAmountStr(String receiptAmountStr) {
		this.receiptAmountStr = receiptAmountStr;
	}
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	
	
}
