package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductionReceive;

public class ProductionReceiveVO extends ProductionReceive {

	private String date;
	private String receivePerson;
	private String source;
	private List<ProductionReceiveDetailVO> productionReceiveDetailVOs;
	
	public ProductionReceiveVO() {
	}
	
	public ProductionReceiveVO(ProductionReceive productionReceive) {
		this.setProductionReceiveId(productionReceive.getProductionReceiveId());
		this.setLookupDetail(productionReceive.getLookupDetail());
		this.setImplementation(productionReceive.getImplementation());
		this.setPersonByCreatedBy(productionReceive.getPersonByCreatedBy());
		this.setPersonByReceivePerson(productionReceive.getPersonByReceivePerson());
		this.setReferenceNumber(productionReceive.getReferenceNumber());
		this.setReceiveDate(productionReceive.getReceiveDate());
		this.setDescription(productionReceive.getDescription());
		this.setCreatedDate(productionReceive.getCreatedDate());
		this.setProductionReceiveDetails(productionReceive.getProductionReceiveDetails());
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReceivePerson() {
		return receivePerson;
	}

	public void setReceivePerson(String receivePerson) {
		this.receivePerson = receivePerson;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<ProductionReceiveDetailVO> getProductionReceiveDetailVOs() {
		return productionReceiveDetailVOs;
	}

	public void setProductionReceiveDetailVOs(
			List<ProductionReceiveDetailVO> productionReceiveDetailVOs) {
		this.productionReceiveDetailVOs = productionReceiveDetailVOs;
	}
	
}
