package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.DebitDetail;

public class DebitDetailVO extends DebitDetail implements java.io.Serializable{

	private String productCode;
	private String strItemType;
	private String productName;
	private String returnNumber;
	private Double returnQty;
	private Double noteQty;
	private Double unitRate;
	private Double total;
	
	public DebitDetailVO() {
		
	}
	
	public DebitDetailVO(DebitDetail debitDetail) {
		this.setDebitDetailId(debitDetail.getDebitDetailId()); 
		this.setProduct(debitDetail.getProduct());
		this.setDebit(debitDetail.getDebit());
		this.setReturnQty(debitDetail.getReturnQty());
		this.setAmount(debitDetail.getAmount());
		this.setDescription(debitDetail.getDescription());
	}
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStrItemType() {
		return strItemType;
	}
	public void setStrItemType(String strItemType) {
		this.strItemType = strItemType;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getReturnNumber() {
		return returnNumber;
	}
	public void setReturnNumber(String returnNumber) {
		this.returnNumber = returnNumber;
	}
	public Double getReturnQty() {
		return returnQty;
	}
	public void setReturnQty(Double returnQty) {
		this.returnQty = returnQty;
	}
	public Double getNoteQty() {
		return noteQty;
	}
	public void setNoteQty(Double noteQty) {
		this.noteQty = noteQty;
	}
	public Double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
}
