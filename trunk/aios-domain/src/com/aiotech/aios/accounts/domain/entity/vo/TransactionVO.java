package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Transaction;

public class TransactionVO extends Transaction implements java.io.Serializable {

	private String accountDescription;
	private String debitAmount;
	private String creditAmount;
	private String closingBalance;
	private String fromDate;
	private String toDate;
	private String expenseAmount;
	private String revenueAmount;
	private String assetAmount;
	private String liabilityAmount;
	private String label;
	private String netAmount; 
	private String transactionDate;
	private LedgerVO ledgerVO;
	private List<TransactionVO> transactionVOs = new ArrayList<TransactionVO>(); 
	private List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>(); 
	private List<TransactionDetailVO> transactionDetailVOCs = new ArrayList<TransactionDetailVO>(); 
	
	public List<TransactionDetailVO> getTransactionDetailVOs() {
		return transactionDetailVOs;
	}

	public void setTransactionDetailVOs(
			List<TransactionDetailVO> transactionDetailVOs) {
		this.transactionDetailVOs = transactionDetailVOs;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public List<TransactionVO> getTransactionVOs() {
		return transactionVOs;
	}

	public void setTransactionVOs(List<TransactionVO> transactionVOs) {
		this.transactionVOs = transactionVOs;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(String closingBalance) {
		this.closingBalance = closingBalance;
	}

	public LedgerVO getLedgerVO() {
		return ledgerVO;
	}

	public void setLedgerVO(LedgerVO ledgerVO) {
		this.ledgerVO = ledgerVO;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getRevenueAmount() {
		return revenueAmount;
	}

	public void setRevenueAmount(String revenueAmount) {
		this.revenueAmount = revenueAmount;
	}

	public List<TransactionDetailVO> getTransactionDetailVOCs() {
		return transactionDetailVOCs;
	}

	public void setTransactionDetailVOCs(
			List<TransactionDetailVO> transactionDetailVOCs) {
		this.transactionDetailVOCs = transactionDetailVOCs;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getAssetAmount() {
		return assetAmount;
	}

	public void setAssetAmount(String assetAmount) {
		this.assetAmount = assetAmount;
	}

	public String getLiabilityAmount() {
		return liabilityAmount;
	}

	public void setLiabilityAmount(String liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	} 
}
