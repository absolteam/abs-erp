package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.CombinationTemplate;

public class CombinationTemplateVO extends CombinationTemplate implements java.io.Serializable{
	
	private CombinationTemplateVO vo;
	private List<CombinationTemplateVO> companyVos=new ArrayList<CombinationTemplateVO>();
	private List<CombinationTemplateVO> costVos=new ArrayList<CombinationTemplateVO>();
	private List<CombinationTemplateVO> naturalVos=new ArrayList<CombinationTemplateVO>();
	private List<CombinationTemplateVO> analysisVos=new ArrayList<CombinationTemplateVO>();
	private List<CombinationTemplateVO> buffer1Vos=new ArrayList<CombinationTemplateVO>();
	private List<CombinationTemplateVO> buffer2Vos=new ArrayList<CombinationTemplateVO>(); 
	private CombinationTemplate combinationTemplate;  
	private String accountType; 
	
	public static List<CombinationTemplateVO> convertTOCombinationTemplateVO(
			List<CombinationTemplate> combinationTemplateList) throws Exception {
		List<CombinationTemplateVO> vos = new ArrayList<CombinationTemplateVO>();
		CombinationTemplateVO vo = null;
		for (CombinationTemplate combinationTemplate : combinationTemplateList) {
			vo = new CombinationTemplateVO();
			vo.setCombinationId(combinationTemplate.getCombinationId());
			vo.setCOATemplateByAnalysisAccountId(combinationTemplate
					.getCOATemplateByAnalysisAccountId());
			vo.setCOATemplateByBuffer2AccountId(combinationTemplate
					.getCOATemplateByBuffer2AccountId());
			vo.setCOATemplateByBuffer1AccountId(combinationTemplate
					.getCOATemplateByBuffer1AccountId());
			vo.setCOATemplateByCompanyAccountId(combinationTemplate
					.getCOATemplateByCompanyAccountId());
			vo.setCOATemplateByCostcenterAccountId(combinationTemplate
					.getCOATemplateByCostcenterAccountId());
			vo.setCOATemplateByNaturalAccountId(combinationTemplate
					.getCOATemplateByNaturalAccountId());
			vos.add(vo);
		}
		return vos;
	}

	public CombinationTemplateVO getVo() {
		return vo;
	}

	public void setVo(CombinationTemplateVO vo) {
		this.vo = vo;
	}

	public List<CombinationTemplateVO> getCompanyVos() {
		return companyVos;
	}

	public void setCompanyVos(List<CombinationTemplateVO> companyVos) {
		this.companyVos = companyVos;
	}

	public List<CombinationTemplateVO> getCostVos() {
		return costVos;
	}

	public void setCostVos(List<CombinationTemplateVO> costVos) {
		this.costVos = costVos;
	}

	public List<CombinationTemplateVO> getNaturalVos() {
		return naturalVos;
	}

	public void setNaturalVos(List<CombinationTemplateVO> naturalVos) {
		this.naturalVos = naturalVos;
	}

	public List<CombinationTemplateVO> getAnalysisVos() {
		return analysisVos;
	}

	public void setAnalysisVos(List<CombinationTemplateVO> analysisVos) {
		this.analysisVos = analysisVos;
	}

	public List<CombinationTemplateVO> getBuffer1Vos() {
		return buffer1Vos;
	}

	public void setBuffer1Vos(List<CombinationTemplateVO> buffer1Vos) {
		this.buffer1Vos = buffer1Vos;
	}

	public List<CombinationTemplateVO> getBuffer2Vos() {
		return buffer2Vos;
	}

	public void setBuffer2Vos(List<CombinationTemplateVO> buffer2Vos) {
		this.buffer2Vos = buffer2Vos;
	}

	public CombinationTemplate getCombinationTemplate() {
		return combinationTemplate;
	}

	public void setCombinationTemplate(CombinationTemplate combinationTemplate) {
		this.combinationTemplate = combinationTemplate;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	} 
	 
}
