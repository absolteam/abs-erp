package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AccountGroup;
import com.aiotech.aios.accounts.domain.entity.AccountGroupDetail;

public class AccountGroupVO extends AccountGroup{
	private String accountTypeName;
	private String combinationName;
	
	private AccountGroupDetail accountGroupDetail;
	public String getAccountTypeName() {
		return accountTypeName;
	}

	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}

	public String getCombinationName() {
		return combinationName;
	}

	public void setCombinationName(String combinationName) {
		this.combinationName = combinationName;
	}

	public AccountGroupDetail getAccountGroupDetail() {
		return accountGroupDetail;
	}

	public void setAccountGroupDetail(AccountGroupDetail accountGroupDetail) {
		this.accountGroupDetail = accountGroupDetail;
	}
}
