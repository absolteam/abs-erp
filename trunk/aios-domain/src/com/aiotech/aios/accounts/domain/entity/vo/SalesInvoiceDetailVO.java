package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.SalesInvoiceDetail;

public class SalesInvoiceDetailVO extends SalesInvoiceDetail implements java.io.Serializable {

	private String productName;
	private String productCode;
	private Double total;
	private Double productCostPrice;
	private Double saleGain;
	private Double discountValue;
	private String discountType;
	private String customerName;
	private String unitCostPrice;
	private String totalCostPrice;
	private String totalInvoiceAmount;
	
	public SalesInvoiceDetailVO() {

	}

	public SalesInvoiceDetailVO(SalesInvoiceDetail salesInvoiceDetail) {
		this.setSalesInvoiceDetailId(salesInvoiceDetail
				.getSalesInvoiceDetailId());
		this.setSalesDeliveryDetail(salesInvoiceDetail.getSalesDeliveryDetail());
		this.setProduct(salesInvoiceDetail.getProduct());
		this.setSalesInvoice(salesInvoiceDetail.getSalesInvoice());
		this.setInvoiceQuantity(salesInvoiceDetail.getInvoiceQuantity());
		this.setUnitRate(salesInvoiceDetail.getUnitRate());
		this.setDescription(salesInvoiceDetail.getDescription());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getProductCostPrice() {
		return productCostPrice;
	}

	public void setProductCostPrice(Double productCostPrice) {
		this.productCostPrice = productCostPrice;
	}

	public Double getSaleGain() {
		return saleGain;
	}

	public void setSaleGain(Double saleGain) {
		this.saleGain = saleGain;
	}

	public Double getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUnitCostPrice() {
		return unitCostPrice;
	}

	public void setUnitCostPrice(String unitCostPrice) {
		this.unitCostPrice = unitCostPrice;
	}

	public String getTotalCostPrice() {
		return totalCostPrice;
	}

	public void setTotalCostPrice(String totalCostPrice) {
		this.totalCostPrice = totalCostPrice;
	}

	public String getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(String totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}
}
