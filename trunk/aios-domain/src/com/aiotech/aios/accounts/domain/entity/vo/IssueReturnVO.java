package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.IssueReturn;

public class IssueReturnVO extends IssueReturn{
	
	
	private String strReturnDate;
	private String locationName;
	private String departmentName;
	private String shippingAddress;
	private List<IssueReturnDetailVO> issueReturnDetailVOs;
	
	public IssueReturnVO() {
		
	}
	
	public IssueReturnVO(IssueReturn issueReturn) {
		this.setIssueReturnId(issueReturn.getIssueReturnId());
		this.setImplementation(issueReturn.getImplementation());
		this.setCmpDeptLocation(issueReturn.getCmpDeptLocation());
		this.setReferenceNumber(issueReturn.getReferenceNumber());
		this.setReturnDate(issueReturn.getReturnDate());
		this.setDescription(issueReturn.getDescription());
		this.setIssueReturnDetails(issueReturn.getIssueReturnDetails());
	}

	public List<IssueReturnDetailVO> getIssueReturnDetailVOs() {
		return issueReturnDetailVOs;
	}

	public void setIssueReturnDetailVOs(
			List<IssueReturnDetailVO> issueReturnDetailVOs) {
		this.issueReturnDetailVOs = issueReturnDetailVOs;
	}

	public String getStrReturnDate() {
		return strReturnDate;
	}

	public void setStrReturnDate(String strReturnDate) {
		this.strReturnDate = strReturnDate;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

}
