package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Shelf;

public class ShelfVO extends Shelf {

	private String shelfTypeName;
	private boolean confirmSave;
	private Integer referenceNumber;
	private List<ShelfVO> rackDetails = new ArrayList<ShelfVO>();
	private String rackTypeName;
	private String rackDimension;
	
	public ShelfVO() {
	}
	
	public ShelfVO(Shelf shelf) {
		this.setShelfId(shelf.getShelfId());
		this.setAisle(shelf.getAisle());
		this.setShelf(shelf.getShelf());
		this.setShelfType(shelf.getShelfType());
		this.setDimension(shelf.getDimension());
		this.setReceiveDetails(shelf.getReceiveDetails());
		this.setShelfs(shelf.getShelfs());
	}
	
	public boolean isConfirmSave() {
		return confirmSave;
	}

	public void setConfirmSave(boolean confirmSave) {
		this.confirmSave = confirmSave;
	}

	public Integer getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(Integer referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public List<ShelfVO> getRackDetails() {
		return rackDetails;
	}

	public void setRackDetails(List<ShelfVO> rackDetails) {
		this.rackDetails = rackDetails;
	}

	public String getShelfTypeName() {
		return shelfTypeName;
	}

	public void setShelfTypeName(String shelfTypeName) {
		this.shelfTypeName = shelfTypeName;
	}

	public String getRackTypeName() {
		return rackTypeName;
	}

	public void setRackTypeName(String rackTypeName) {
		this.rackTypeName = rackTypeName;
	}

	public String getRackDimension() {
		return rackDimension;
	}

	public void setRackDimension(String rackDimension) {
		this.rackDimension = rackDimension;
	}
	
}
