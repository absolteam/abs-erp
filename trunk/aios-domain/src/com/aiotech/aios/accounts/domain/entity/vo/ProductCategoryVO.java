package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;
import java.util.Set;

import com.aiotech.aios.accounts.domain.entity.ProductCategory;

public class ProductCategoryVO extends ProductCategory implements java.io.Serializable{
	
	private List<ProductCategoryVO> subCategories;
	private List<ProductVO> productVOs;
	private String parentCatgoryName;
	private boolean updateFlag;
	private Long parentCategoryId;
	private Double productAmount;
	private Double productQuantity;
	private String categoryPic;
	private boolean specialProduct;
	private Double totalProductAmount;
	private String totalProductAmountStr;
	private Double discountProductAmount;
	private Set<ProductCategoryVO> subCategorySet;
	
	public List<ProductCategoryVO> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<ProductCategoryVO> subCategories) {
		this.subCategories = subCategories;
	}

	public String getParentCatgoryName() {
		return parentCatgoryName;
	}

	public void setParentCatgoryName(String parentCatgoryName) {
		this.parentCatgoryName = parentCatgoryName;
	}

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public Double getProductAmount() {
		return productAmount;
	}

	public void setProductAmount(Double productAmount) {
		this.productAmount = productAmount;
	}

	public Double getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(Double productQuantity) {
		this.productQuantity = productQuantity;
	}

	public String getCategoryPic() {
		return categoryPic;
	}

	public void setCategoryPic(String categoryPic) {
		this.categoryPic = categoryPic;
	}

	public boolean isSpecialProduct() {
		return specialProduct;
	}

	public void setSpecialProduct(boolean specialProduct) {
		this.specialProduct = specialProduct;
	}

	public Double getTotalProductAmount() {
		return totalProductAmount;
	}

	public void setTotalProductAmount(Double totalProductAmount) {
		this.totalProductAmount = totalProductAmount;
	}

	public String getTotalProductAmountStr() {
		return totalProductAmountStr;
	}

	public void setTotalProductAmountStr(String totalProductAmountStr) {
		this.totalProductAmountStr = totalProductAmountStr;
	}

	public Double getDiscountProductAmount() {
		return discountProductAmount;
	}

	public void setDiscountProductAmount(Double discountProductAmount) {
		this.discountProductAmount = discountProductAmount;
	}

	public List<ProductVO> getProductVOs() {
		return productVOs;
	}

	public void setProductVOs(List<ProductVO> productVOs) {
		this.productVOs = productVOs;
	}

	public Set<ProductCategoryVO> getSubCategorySet() {
		return subCategorySet;
	}

	public void setSubCategorySet(Set<ProductCategoryVO> subCategorySet) {
		this.subCategorySet = subCategorySet;
	}
}
