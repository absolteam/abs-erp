package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.Product;

public class ProductVO extends Product implements java.io.Serializable,
		Comparable<ProductVO> {

	private double quantity;
	private double finalUnitPrice;
	private double totalPrice;
	private double basePrice;
	private double standardPrice;
	private double suggestedPrice;
	private double sellingPrice;
	private String displayPrice;
	private String displayUnitPrice;
	private String productPic;
	private String subCategory;
	private String category;
	private String unitCode;
	private long storeId;
	private String storeName;
	private long productPricingCalcId;
	private long comboProductId;
	private boolean promotionFlag;
	private boolean discountFlag;
	private boolean discountMode;
	private boolean customerPromotion;
	private Boolean comboType;
	private int comboItemOrder;
	private double promotionPoints;
	private double productPurchasePoints;
	private double totalPoints;
	private double discount;
	private long promotionMethodId;
	private long discountMethodId;
	private long personId;
	private String unitName;
	private String costingMethod;
	private int numberOfCoupons;
	private double customerDiscount;
	private double customerDiscountPercentage;
	private double availableQuantity;
	private int customerCoupons;
	private int shelfId;
	private boolean orderPrinted;
	private long pointOfSaleDetailId;
	private Integer itemOrder;
	private boolean barPrinter;
	private boolean kitchenPrinter;
	private double discountedValue;

	private String itemTypeName;
	private String subItemTypeName;
	private String shelfName;
	private String itemNatureName;
	private String statusStr;
	private Double currentStock;

 	private Map<Long, ProductVO> productComboVOs = new HashMap<Long, ProductVO>();
	private Map<String, ProductVO> productVOsMap = new HashMap<String, ProductVO>();
	private List<PurchaseDetailVO> productPurchaseVos;

	// Receive Vs Issuance
	private String receiveDates;
	private String receiveNumbers;
	private String issuanceNumbers;
	private String issuanceDates;
	private Double unitRate;
	private Double issuedQuantity;
	private Double receivedQuantity;
	private Double receivedTotalAmount;
	private Double issuedTotalAmount;
	private String goodsReturnNumbers;
	private String goodsReturnDates;
	private Double returnQuantity;
	private String issueReturnNumbers;
	private String issueReturnDates;
	private Double issueReturnQuantity;
	private Double balanceQuantity;
	private String requisitionNumbers;
	private String requisitionDates;
	private Map<String, ArrayList<ReceiveDetailVO>> receiveDetailVOMap = new HashMap<String, ArrayList<ReceiveDetailVO>>();
	private Map<String, ArrayList<GoodsReturnDetailVO>> goodsReturnDetailVOMap = new HashMap<String, ArrayList<GoodsReturnDetailVO>>();
	private Map<String, ArrayList<IssueRequistionDetailVO>> issueRequistionDetailVOMap = new HashMap<String, ArrayList<IssueRequistionDetailVO>>();
	private Map<String, ArrayList<IssueReturnDetailVO>> issueReturnDetailVOMap = new HashMap<String, ArrayList<IssueReturnDetailVO>>();

	private Map<String, ReceiveDetailVO> receiveDetailVOMapWithoutList = new HashMap<String, ReceiveDetailVO>();
	private Map<String, GoodsReturnDetailVO> goodsReturnDetailVOWithoutList = new HashMap<String, GoodsReturnDetailVO>();
	private Map<String, IssueRequistionDetailVO> issueRequistionDetailVOWithoutList = new HashMap<String, IssueRequistionDetailVO>();
	private Map<String, IssueReturnDetailVO> issueReturnDetailVOWithoutList = new HashMap<String, IssueReturnDetailVO>();

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getFinalUnitPrice() {
		return finalUnitPrice;
	}

	public void setFinalUnitPrice(double finalUnitPrice) {
		this.finalUnitPrice = finalUnitPrice;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getDisplayPrice() {
		return displayPrice;
	}

	public void setDisplayPrice(String displayPrice) {
		this.displayPrice = displayPrice;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public String getDisplayUnitPrice() {
		return displayUnitPrice;
	}

	public void setDisplayUnitPrice(String displayUnitPrice) {
		this.displayUnitPrice = displayUnitPrice;
	}

	public long getProductPricingCalcId() {
		return productPricingCalcId;
	}

	public void setProductPricingCalcId(long productPricingCalcId) {
		this.productPricingCalcId = productPricingCalcId;
	}

	public boolean isPromotionFlag() {
		return promotionFlag;
	}

	public void setPromotionFlag(boolean promotionFlag) {
		this.promotionFlag = promotionFlag;
	}

	public double getPromotionPoints() {
		return promotionPoints;
	}

	public void setPromotionPoints(double promotionPoints) {
		this.promotionPoints = promotionPoints;
	}

	public long getPromotionMethodId() {
		return promotionMethodId;
	}

	public void setPromotionMethodId(long promotionMethodId) {
		this.promotionMethodId = promotionMethodId;
	}

	public int getNumberOfCoupons() {
		return numberOfCoupons;
	}

	public void setNumberOfCoupons(int numberOfCoupons) {
		this.numberOfCoupons = numberOfCoupons;
	}

	public boolean isDiscountMode() {
		return discountMode;
	}

	public void setDiscountMode(boolean discountMode) {
		this.discountMode = discountMode;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getProductPurchasePoints() {
		return productPurchasePoints;
	}

	public void setProductPurchasePoints(double productPurchasePoints) {
		this.productPurchasePoints = productPurchasePoints;
	}

	public double getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(double totalPoints) {
		this.totalPoints = totalPoints;
	}

	public boolean isDiscountFlag() {
		return discountFlag;
	}

	public void setDiscountFlag(boolean discountFlag) {
		this.discountFlag = discountFlag;
	}

	public long getDiscountMethodId() {
		return discountMethodId;
	}

	public void setDiscountMethodId(long discountMethodId) {
		this.discountMethodId = discountMethodId;
	}

	public double getCustomerDiscount() {
		return customerDiscount;
	}

	public void setCustomerDiscount(double customerDiscount) {
		this.customerDiscount = customerDiscount;
	}

	public double getCustomerDiscountPercentage() {
		return customerDiscountPercentage;
	}

	public void setCustomerDiscountPercentage(double customerDiscountPercentage) {
		this.customerDiscountPercentage = customerDiscountPercentage;
	}

	public int getCustomerCoupons() {
		return customerCoupons;
	}

	public void setCustomerCoupons(int customerCoupons) {
		this.customerCoupons = customerCoupons;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getCostingMethod() {
		return costingMethod;
	}

	public void setCostingMethod(String costingMethod) {
		this.costingMethod = costingMethod;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public boolean isOrderPrinted() {
		return orderPrinted;
	}

	public void setOrderPrinted(boolean orderPrinted) {
		this.orderPrinted = orderPrinted;
	}

	public long getPointOfSaleDetailId() {
		return pointOfSaleDetailId;
	}

	public void setPointOfSaleDetailId(long pointOfSaleDetailId) {
		this.pointOfSaleDetailId = pointOfSaleDetailId;
	}

	public String getProductPic() {
		return productPic;
	}

	public void setProductPic(String productPic) {
		this.productPic = productPic;
	}

	public Boolean getComboType() {
		return comboType;
	}

	public void setComboType(Boolean comboType) {
		this.comboType = comboType;
	}

	public Integer getItemOrder() {
		return itemOrder;
	}

	public void setItemOrder(Integer itemOrder) {
		this.itemOrder = itemOrder;
	}

	public Map<Long, ProductVO> getProductComboVOs() {
		return productComboVOs;
	}

	public void setProductComboVOs(Map<Long, ProductVO> productComboVOs) {
		this.productComboVOs = productComboVOs;
	}

	public long getComboProductId() {
		return comboProductId;
	}

	public void setComboProductId(long comboProductId) {
		this.comboProductId = comboProductId;
	}

	public int getComboItemOrder() {
		return comboItemOrder;
	}

	public void setComboItemOrder(int comboItemOrder) {
		this.comboItemOrder = comboItemOrder;
	}

	public boolean isCustomerPromotion() {
		return customerPromotion;
	}

	public void setCustomerPromotion(boolean customerPromotion) {
		this.customerPromotion = customerPromotion;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public boolean isBarPrinter() {
		return barPrinter;
	}

	public void setBarPrinter(boolean barPrinter) {
		this.barPrinter = barPrinter;
	}

	public boolean isKitchenPrinter() {
		return kitchenPrinter;
	}

	public void setKitchenPrinter(boolean kitchenPrinter) {
		this.kitchenPrinter = kitchenPrinter;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public double getSuggestedPrice() {
		return suggestedPrice;
	}

	public void setSuggestedPrice(double suggestedPrice) {
		this.suggestedPrice = suggestedPrice;
	}

	public double getDiscountedValue() {
		return discountedValue;
	}

	public void setDiscountedValue(double discountedValue) {
		this.discountedValue = discountedValue;
	}

	@Override
	public int compareTo(ProductVO o) {
		// TODO Auto-generated method stub
		return this.itemOrder - o.itemOrder;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getItemTypeName() {
		return itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

	public String getSubItemTypeName() {
		return subItemTypeName;
	}

	public void setSubItemTypeName(String subItemTypeName) {
		this.subItemTypeName = subItemTypeName;
	}

	public String getShelfName() {
		return shelfName;
	}

	public void setShelfName(String shelfName) {
		this.shelfName = shelfName;
	}

	public String getItemNatureName() {
		return itemNatureName;
	}

	public void setItemNatureName(String itemNatureName) {
		this.itemNatureName = itemNatureName;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}

	public Double getCurrentStock() {
		return currentStock;
	}

	public void setCurrentStock(Double currentStock) {
		this.currentStock = currentStock;
	}

	public List<PurchaseDetailVO> getProductPurchaseVos() {
		return productPurchaseVos;
	}

	public void setProductPurchaseVos(List<PurchaseDetailVO> productPurchaseVos) {
		this.productPurchaseVos = productPurchaseVos;
	}

	public String getReceiveDates() {
		return receiveDates;
	}

	public void setReceiveDates(String receiveDates) {
		this.receiveDates = receiveDates;
	}

	public String getReceiveNumbers() {
		return receiveNumbers;
	}

	public void setReceiveNumbers(String receiveNumbers) {
		this.receiveNumbers = receiveNumbers;
	}

	public String getIssuanceNumbers() {
		return issuanceNumbers;
	}

	public void setIssuanceNumbers(String issuanceNumbers) {
		this.issuanceNumbers = issuanceNumbers;
	}

	public String getIssuanceDates() {
		return issuanceDates;
	}

	public void setIssuanceDates(String issuanceDates) {
		this.issuanceDates = issuanceDates;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(Double issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public Double getReceivedQuantity() {
		return receivedQuantity;
	}

	public void setReceivedQuantity(Double receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

	public Double getReceivedTotalAmount() {
		return receivedTotalAmount;
	}

	public void setReceivedTotalAmount(Double receivedTotalAmount) {
		this.receivedTotalAmount = receivedTotalAmount;
	} 

	public String getGoodsReturnDates() {
		return goodsReturnDates;
	}

	public void setGoodsReturnDates(String goodsReturnDates) {
		this.goodsReturnDates = goodsReturnDates;
	}

	public Double getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(Double returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public String getIssueReturnNumbers() {
		return issueReturnNumbers;
	}

	public void setIssueReturnNumbers(String issueReturnNumbers) {
		this.issueReturnNumbers = issueReturnNumbers;
	}

	public String getIssueReturnDates() {
		return issueReturnDates;
	}

	public void setIssueReturnDates(String issueReturnDates) {
		this.issueReturnDates = issueReturnDates;
	}

	public Double getIssueReturnQuantity() {
		return issueReturnQuantity;
	}

	public void setIssueReturnQuantity(Double issueReturnQuantity) {
		this.issueReturnQuantity = issueReturnQuantity;
	}

	

	public Double getBalanceQuantity() {
		return balanceQuantity;
	}

	public void setBalanceQuantity(Double balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	public String getRequisitionNumbers() {
		return requisitionNumbers;
	}

	public void setRequisitionNumbers(String requisitionNumbers) {
		this.requisitionNumbers = requisitionNumbers;
	}

	public String getRequisitionDates() {
		return requisitionDates;
	}

	public void setRequisitionDates(String requisitionDates) {
		this.requisitionDates = requisitionDates;
	}

	public Map<String, ReceiveDetailVO> getReceiveDetailVOMapWithoutList() {
		return receiveDetailVOMapWithoutList;
	}

	public void setReceiveDetailVOMapWithoutList(
			Map<String, ReceiveDetailVO> receiveDetailVOMapWithoutList) {
		this.receiveDetailVOMapWithoutList = receiveDetailVOMapWithoutList;
	}

	public Map<String, GoodsReturnDetailVO> getGoodsReturnDetailVOWithoutList() {
		return goodsReturnDetailVOWithoutList;
	}

	public void setGoodsReturnDetailVOWithoutList(
			Map<String, GoodsReturnDetailVO> goodsReturnDetailVOWithoutList) {
		this.goodsReturnDetailVOWithoutList = goodsReturnDetailVOWithoutList;
	}

	public Map<String, IssueRequistionDetailVO> getIssueRequistionDetailVOWithoutList() {
		return issueRequistionDetailVOWithoutList;
	}

	public void setIssueRequistionDetailVOWithoutList(
			Map<String, IssueRequistionDetailVO> issueRequistionDetailVOWithoutList) {
		this.issueRequistionDetailVOWithoutList = issueRequistionDetailVOWithoutList;
	}

	public Map<String, IssueReturnDetailVO> getIssueReturnDetailVOWithoutList() {
		return issueReturnDetailVOWithoutList;
	}

	public void setIssueReturnDetailVOWithoutList(
			Map<String, IssueReturnDetailVO> issueReturnDetailVOWithoutList) {
		this.issueReturnDetailVOWithoutList = issueReturnDetailVOWithoutList;
	}

	public Map<String, ProductVO> getProductVOsMap() {
		return productVOsMap;
	}

	public void setProductVOsMap(Map<String, ProductVO> productVOsMap) {
		this.productVOsMap = productVOsMap;
	}

	public Double getIssuedTotalAmount() {
		return issuedTotalAmount;
	}

	public void setIssuedTotalAmount(Double issuedTotalAmount) {
		this.issuedTotalAmount = issuedTotalAmount;
	}

	public String getGoodsReturnNumbers() {
		return goodsReturnNumbers;
	}

	public void setGoodsReturnNumbers(String goodsReturnNumbers) {
		this.goodsReturnNumbers = goodsReturnNumbers;
	}

	public Map<String, ArrayList<ReceiveDetailVO>> getReceiveDetailVOMap() {
		return receiveDetailVOMap;
	}

	public void setReceiveDetailVOMap(
			Map<String, ArrayList<ReceiveDetailVO>> receiveDetailVOMap) {
		this.receiveDetailVOMap = receiveDetailVOMap;
	}

	public Map<String, ArrayList<GoodsReturnDetailVO>> getGoodsReturnDetailVOMap() {
		return goodsReturnDetailVOMap;
	}

	public void setGoodsReturnDetailVOMap(
			Map<String, ArrayList<GoodsReturnDetailVO>> goodsReturnDetailVOMap) {
		this.goodsReturnDetailVOMap = goodsReturnDetailVOMap;
	}

	public Map<String, ArrayList<IssueRequistionDetailVO>> getIssueRequistionDetailVOMap() {
		return issueRequistionDetailVOMap;
	}

	public void setIssueRequistionDetailVOMap(
			Map<String, ArrayList<IssueRequistionDetailVO>> issueRequistionDetailVOMap) {
		this.issueRequistionDetailVOMap = issueRequistionDetailVOMap;
	}

	public Map<String, ArrayList<IssueReturnDetailVO>> getIssueReturnDetailVOMap() {
		return issueReturnDetailVOMap;
	}

	public void setIssueReturnDetailVOMap(
			Map<String, ArrayList<IssueReturnDetailVO>> issueReturnDetailVOMap) {
		this.issueReturnDetailVOMap = issueReturnDetailVOMap;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

}
