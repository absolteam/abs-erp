package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.CustomerQuotationDetail;

public class CustomerQuotationDetailVO extends CustomerQuotationDetail
		implements java.io.Serializable {

	private String productCode;
	private String productName;
	private String storeName;
	private String strDiscountMode;
	private String baseUnitName;
	private String conversionUnitName;
	private String baseQuantity;
	private String unitCode;
	private Double total;
	private Long packageDetailId;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	
	public CustomerQuotationDetailVO() {

	}

	public CustomerQuotationDetailVO(
			CustomerQuotationDetail customerQuotationDetail) {
		this.setCustomerQuotationDetailId(customerQuotationDetail
				.getCustomerQuotationDetailId());
		this.setCustomerQuotation(customerQuotationDetail
				.getCustomerQuotation());
		this.setProduct(customerQuotationDetail.getProduct());
		this.setStore(customerQuotationDetail.getStore());
		this.setQuantity(customerQuotationDetail.getQuantity());
		this.setUnitRate(customerQuotationDetail.getUnitRate());
		this.setIsPercentage(customerQuotationDetail.getIsPercentage());
		this.setDiscount(customerQuotationDetail.getDiscount());
		this.setDescription(customerQuotationDetail.getDescription()); 
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStrDiscountMode() {
		return strDiscountMode;
	}

	public void setStrDiscountMode(String strDiscountMode) {
		this.strDiscountMode = strDiscountMode;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

}
