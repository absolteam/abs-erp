package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetCheckIn;

public class AssetCheckInVO extends AssetCheckIn {

	private String outDate;
	private String inDate;
	private String checkOutNumber;
	private String assetName;
	private String checkOutTo;
	private String checkOutReference;
	private String checkInBy;
	
	public AssetCheckInVO() {
		
	}
	
	public AssetCheckInVO(AssetCheckIn assetCheckIn) {
		this.setAssetCheckInId(assetCheckIn.getAssetCheckInId());
		this.setPerson(assetCheckIn.getPerson());
		this.setAssetCheckOut(assetCheckIn.getAssetCheckOut());
		this.setCheckInNumber(assetCheckIn.getCheckInNumber());
		this.setCheckInDate(assetCheckIn.getCheckInDate());
		this.setDescription(assetCheckIn.getDescription());
	}
	
	public String getOutDate() {
		return outDate;
	}
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	public String getInDate() {
		return inDate;
	}
	public void setInDate(String inDate) {
		this.inDate = inDate;
	}
	public String getCheckOutNumber() {
		return checkOutNumber;
	}
	public void setCheckOutNumber(String checkOutNumber) {
		this.checkOutNumber = checkOutNumber;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getCheckOutTo() {
		return checkOutTo;
	}
	public void setCheckOutTo(String checkOutTo) {
		this.checkOutTo = checkOutTo;
	}
	public String getCheckOutReference() {
		return checkOutReference;
	}
	public void setCheckOutReference(String checkOutReference) {
		this.checkOutReference = checkOutReference;
	}
	public String getCheckInBy() {
		return checkInBy;
	}
	public void setCheckInBy(String checkInBy) {
		this.checkInBy = checkInBy;
	}
}
