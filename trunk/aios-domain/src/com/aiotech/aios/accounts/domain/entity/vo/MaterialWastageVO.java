package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialWastage;

public class MaterialWastageVO extends MaterialWastage implements java.io.Serializable {

	private String date;
	private String personName;
	private String wastageTypeStr;
	private String statusStr;
	private List<MaterialWastageDetailVO> materialWastageDetailVOs = new ArrayList<MaterialWastageDetailVO>();
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public List<MaterialWastageDetailVO> getMaterialWastageDetailVOs() {
		return materialWastageDetailVOs;
	}

	public void setMaterialWastageDetailVOs(
			List<MaterialWastageDetailVO> materialWastageDetailVOs) {
		this.materialWastageDetailVOs = materialWastageDetailVOs;
	}

	public String getWastageTypeStr() {
		return wastageTypeStr;
	}

	public void setWastageTypeStr(String wastageTypeStr) {
		this.wastageTypeStr = wastageTypeStr;
	}

	public String getStatusStr() {
		return statusStr;
	}

	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
}
