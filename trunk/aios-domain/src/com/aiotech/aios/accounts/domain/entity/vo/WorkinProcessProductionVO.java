package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.WorkinProcessProduction;

public class WorkinProcessProductionVO extends WorkinProcessProduction implements java.io.Serializable {

	private String storeName;
	private String productName;
	private Double total;
	List<WorkinProcessDetailVO> workinProcessDetailVOs = new ArrayList<WorkinProcessDetailVO>();
	private WorkinProcessVO workinProcessVO;
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public List<WorkinProcessDetailVO> getWorkinProcessDetailVOs() {
		return workinProcessDetailVOs;
	}

	public void setWorkinProcessDetailVOs(
			List<WorkinProcessDetailVO> workinProcessDetailVOs) {
		this.workinProcessDetailVOs = workinProcessDetailVOs;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public WorkinProcessVO getWorkinProcessVO() {
		return workinProcessVO;
	}

	public void setWorkinProcessVO(WorkinProcessVO workinProcessVO) {
		this.workinProcessVO = workinProcessVO;
	}
}
