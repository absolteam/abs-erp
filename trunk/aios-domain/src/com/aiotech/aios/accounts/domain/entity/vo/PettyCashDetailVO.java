package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PettyCashDetail;

public class PettyCashDetailVO extends PettyCashDetail implements java.io.Serializable {

	private String personName;
	private String combinationStr;
	private String expenseAmountStr;
	private String costCenterStr;
	
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getCombinationStr() {
		return combinationStr;
	}
	public void setCombinationStr(String combinationStr) {
		this.combinationStr = combinationStr;
	}
	public String getExpenseAmountStr() {
		return expenseAmountStr;
	}
	public void setExpenseAmountStr(String expenseAmountStr) {
		this.expenseAmountStr = expenseAmountStr;
	}
	public String getCostCenterStr() {
		return costCenterStr;
	}
	public void setCostCenterStr(String costCenterStr) {
		this.costCenterStr = costCenterStr;
	}
}
