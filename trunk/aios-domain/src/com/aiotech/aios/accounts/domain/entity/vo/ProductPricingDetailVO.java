package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductPricingDetail;

public class ProductPricingDetailVO extends ProductPricingDetail implements java.io.Serializable {

	private Double finalPrice;
	private String productCode;
	private String productName;
	private String storeName;
	private String pricingTitle;
	private String currencyCode;
	private String costPrice;
	private String salePrice;
	private long productPricingId;
	private long productId;
	private List<ProductPricingCalcVO> productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
	
	public ProductPricingDetailVO() {
		
	}
	
	public ProductPricingDetailVO(ProductPricingDetail productPricingDetail) {
		this.setProductPricingDetailId(productPricingDetail.getProductPricingDetailId());
		this.setProduct(productPricingDetail.getProduct());
		this.setProductPricing(productPricingDetail.getProductPricing());
		this.setBasicCostPrice(productPricingDetail.getBasicCostPrice());
		this.setStandardPrice(productPricingDetail.getStandardPrice());
		this.setSuggestedRetailPrice(productPricingDetail.getSuggestedRetailPrice());
		this.setDescription(productPricingDetail.getDescription());
		this.setProductPricingCalcs(productPricingDetail.getProductPricingCalcs());
	}

	public List<ProductPricingCalcVO> getProductPricingCalcVOs() {
		return productPricingCalcVOs;
	}

	public void setProductPricingCalcVOs(
			List<ProductPricingCalcVO> productPricingCalcVOs) {
		this.productPricingCalcVOs = productPricingCalcVOs;
	}

	public Double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPricingTitle() {
		return pricingTitle;
	}

	public void setPricingTitle(String pricingTitle) {
		this.pricingTitle = pricingTitle;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public long getProductPricingId() {
		return productPricingId;
	}

	public void setProductPricingId(long productPricingId) {
		this.productPricingId = productPricingId;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
}
