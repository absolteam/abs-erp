package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.aiotech.aios.accounts.domain.entity.SalesInvoice;

@XmlRootElement
public class SalesInvoiceVO extends SalesInvoice implements java.io.Serializable{

	private String salesInvoiceDate;
 	private String paymentMode;
 	private String customerName;
	private String creditTermName;
	private String invoiceTypeName;
	private String strCurrency;
	private String shippingAddress;
	private String statusName;
	private String customerNameAndNo;
	private Double pendingInvoice;
	private String customerNumber;
	private String parentInvoice;
	private List<SalesInvoiceDetailVO> salesInvoiceDetailVOs;
	private String customerMobile;
	private String customerAddress;
	private List<SalesInvoiceVO> salesInvoiceVOs;
	private List<SalesInvoiceVO> projectSalesInvoiceVOs;
	private List<SalesInvoiceVO> pointOfSaleInvoiceVOs;
	
	private Date formDate;
	private Date toDate;
	private Long customerId;
	private String salesDeliveryNumber;
	private String salesDate;
	private String salesFlag;
	private Byte paymentStatus;
	private Boolean deliveryFlag;
	private Double paidInvoice;
	private Boolean editFlag;
	private String poBox;
	private String address;
	private String telephone;
	private String fax;
	private String mobile;
	private String amountInWords;
	private String pendingInvoiceStr;
	private String totalInvoiceStr;
	private String invoiceAmountStr;
	private String useCase;
	private SalesDeliveryNoteVO salesDeliveryNoteVO;
	private String totalInvoiceAmount;
	public SalesInvoiceVO() {
		
	}
	
	public SalesInvoiceVO(SalesInvoice salesInvoice) {
		this.setSalesInvoiceId(salesInvoice.getSalesInvoiceId());
		this.setCurrency(salesInvoice.getCurrency());
		this.setImplementation(salesInvoice.getImplementation());
		this.setCustomer(salesInvoice.getCustomer());
		this.setPerson(salesInvoice.getPerson());
		this.setCreditTerm(salesInvoice.getCreditTerm());
		this.setReferenceNumber(salesInvoice.getReferenceNumber());
		this.setInvoiceDate(salesInvoice.getInvoiceDate());
		this.setDescription(salesInvoice.getDescription());
		this.setCreateDate(salesInvoice.getCreateDate());
		this.setInvoiceType(salesInvoice.getInvoiceType());
		this.setSalesInvoiceDetails(salesInvoice.getSalesInvoiceDetails());
	}
	
	public String getSalesInvoiceDate() {
		return salesInvoiceDate;
	}

	public void setSalesInvoiceDate(String salesInvoiceDate) {
		this.salesInvoiceDate = salesInvoiceDate;
	}  

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCreditTermName() {
		return creditTermName;
	}

	public void setCreditTermName(String creditTermName) {
		this.creditTermName = creditTermName;
	}

	public String getInvoiceTypeName() {
		return invoiceTypeName;
	}

	public void setInvoiceTypeName(String invoiceTypeName) {
		this.invoiceTypeName = invoiceTypeName;
	}

	public String getStrCurrency() {
		return strCurrency;
	}

	public void setStrCurrency(String strCurrency) {
		this.strCurrency = strCurrency;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCustomerNameAndNo() {
		return customerNameAndNo;
	}

	public void setCustomerNameAndNo(String customerNameAndNo) {
		this.customerNameAndNo = customerNameAndNo;
	}

	public List<SalesInvoiceDetailVO> getSalesInvoiceDetailVOs() {
		return salesInvoiceDetailVOs;
	}

	public void setSalesInvoiceDetailVOs(
			List<SalesInvoiceDetailVO> salesInvoiceDetailVOs) {
		this.salesInvoiceDetailVOs = salesInvoiceDetailVOs;
	}

	public Double getPendingInvoice() {
		return pendingInvoice;
	}

	public void setPendingInvoice(Double pendingInvoice) {
		this.pendingInvoice = pendingInvoice;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public List<SalesInvoiceVO> getSalesInvoiceVOs() {
		return salesInvoiceVOs;
	}

	public void setSalesInvoiceVOs(List<SalesInvoiceVO> salesInvoiceVOs) {
		this.salesInvoiceVOs = salesInvoiceVOs;
	}

	public String getParentInvoice() {
		return parentInvoice;
	}

	public void setParentInvoice(String parentInvoice) {
		this.parentInvoice = parentInvoice;
	}

	public String getSalesDeliveryNumber() {
		return salesDeliveryNumber;
	}

	public void setSalesDeliveryNumber(String salesDeliveryNumber) {
		this.salesDeliveryNumber = salesDeliveryNumber;
	}

	public String getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}

	public List<SalesInvoiceVO> getProjectSalesInvoiceVOs() {
		return projectSalesInvoiceVOs;
	}

	public void setProjectSalesInvoiceVOs(
			List<SalesInvoiceVO> projectSalesInvoiceVOs) {
		this.projectSalesInvoiceVOs = projectSalesInvoiceVOs;
	}

	public List<SalesInvoiceVO> getPointOfSaleInvoiceVOs() {
		return pointOfSaleInvoiceVOs;
	}

	public void setPointOfSaleInvoiceVOs(List<SalesInvoiceVO> pointOfSaleInvoiceVOs) {
		this.pointOfSaleInvoiceVOs = pointOfSaleInvoiceVOs;
	}

	public String getSalesFlag() {
		return salesFlag;
	}

	public void setSalesFlag(String salesFlag) {
		this.salesFlag = salesFlag;
	}

	public Byte getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(Byte paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Boolean getDeliveryFlag() {
		return deliveryFlag;
	}

	public void setDeliveryFlag(Boolean deliveryFlag) {
		this.deliveryFlag = deliveryFlag;
	}

	public Double getPaidInvoice() {
		return paidInvoice;
	}

	public void setPaidInvoice(Double paidInvoice) {
		this.paidInvoice = paidInvoice;
	}

	public Boolean getEditFlag() {
		return editFlag;
	}

	public void setEditFlag(Boolean editFlag) {
		this.editFlag = editFlag;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public SalesDeliveryNoteVO getSalesDeliveryNoteVO() {
		return salesDeliveryNoteVO;
	}

	public void setSalesDeliveryNoteVO(SalesDeliveryNoteVO salesDeliveryNoteVO) {
		this.salesDeliveryNoteVO = salesDeliveryNoteVO;
	}

	public String getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(String totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}

	public String getPendingInvoiceStr() {
		return pendingInvoiceStr;
	}

	public void setPendingInvoiceStr(String pendingInvoiceStr) {
		this.pendingInvoiceStr = pendingInvoiceStr;
	}

	public String getTotalInvoiceStr() {
		return totalInvoiceStr;
	}

	public void setTotalInvoiceStr(String totalInvoiceStr) {
		this.totalInvoiceStr = totalInvoiceStr;
	}

	public String getInvoiceAmountStr() {
		return invoiceAmountStr;
	}

	public void setInvoiceAmountStr(String invoiceAmountStr) {
		this.invoiceAmountStr = invoiceAmountStr;
	}

	public String getUseCase() {
		return useCase;
	}

	public void setUseCase(String useCase) {
		this.useCase = useCase;
	}
	
	
}
