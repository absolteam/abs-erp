package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.CurrencyConversion;

public class CurrencyConversionVO extends CurrencyConversion implements java.io.Serializable{

	private String conversionCurrency;
	private String functionalCurrency;
	private String countryName;
	
	public String getConversionCurrency() {
		return conversionCurrency;
	}
	public void setConversionCurrency(String conversionCurrency) {
		this.conversionCurrency = conversionCurrency;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getFunctionalCurrency() {
		return functionalCurrency;
	}
	public void setFunctionalCurrency(String functionalCurrency) {
		this.functionalCurrency = functionalCurrency;
	}
}
