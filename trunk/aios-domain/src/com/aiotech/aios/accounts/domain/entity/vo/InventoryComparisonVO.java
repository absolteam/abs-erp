package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

public class InventoryComparisonVO implements java.io.Serializable {

	private String productCode;
	private String productName;
	private String unitPrice;
	private Double totalQty;
	private Double totalUnitPrice;
	private Double unitRate;
	private Long supplierId;
	private Long productId;
	private Long customerId;
	private String priceLevel;
	private List<InventoryComparisonVO> inventoryComparisonVOs = new ArrayList<InventoryComparisonVO>();
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}
	public Double getTotalUnitPrice() {
		return totalUnitPrice;
	}
	public void setTotalUnitPrice(Double totalUnitPrice) {
		this.totalUnitPrice = totalUnitPrice;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public List<InventoryComparisonVO> getInventoryComparisonVOs() {
		return inventoryComparisonVOs;
	}
	public void setInventoryComparisonVOs(
			List<InventoryComparisonVO> inventoryComparisonVOs) {
		this.inventoryComparisonVOs = inventoryComparisonVOs;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}
	public String getPriceLevel() {
		return priceLevel;
	}
	public void setPriceLevel(String priceLevel) {
		this.priceLevel = priceLevel;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
}
