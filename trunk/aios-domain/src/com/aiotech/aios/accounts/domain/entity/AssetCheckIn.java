package com.aiotech.aios.accounts.domain.entity;

// Generated Feb 22, 2015 9:08:14 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.Person;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AssetCheckIn generated by hbm2java
 */
@Entity
@Table(name = "ac_asset_check_in", catalog = "aios")
public class AssetCheckIn implements java.io.Serializable {

	private Long assetCheckInId;
	private Person person;
	private AssetCheckOut assetCheckOut;
	private String checkInNumber;
	private Date checkInDate;
	private String description;
	private Byte isApprove;
	private Long relativeId;

	public AssetCheckIn() {
	}

	public AssetCheckIn(Person person, AssetCheckOut assetCheckOut,
			String checkInNumber, Date checkInDate) {
		this.person = person;
		this.assetCheckOut = assetCheckOut;
		this.checkInNumber = checkInNumber;
		this.checkInDate = checkInDate;
	}

	public AssetCheckIn(Person person, AssetCheckOut assetCheckOut,
			String checkInNumber, Date checkInDate, String description,
			Byte isApprove, Long relativeId) {
		this.person = person;
		this.assetCheckOut = assetCheckOut;
		this.checkInNumber = checkInNumber;
		this.checkInDate = checkInDate;
		this.description = description;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "asset_check_in_id", unique = true, nullable = false)
	public Long getAssetCheckInId() {
		return this.assetCheckInId;
	}

	public void setAssetCheckInId(Long assetCheckInId) {
		this.assetCheckInId = assetCheckInId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "check_in_by", nullable = false)
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_check_out_id", nullable = false)
	public AssetCheckOut getAssetCheckOut() {
		return this.assetCheckOut;
	}

	public void setAssetCheckOut(AssetCheckOut assetCheckOut) {
		this.assetCheckOut = assetCheckOut;
	}

	@Column(name = "check_in_number", nullable = false, length = 30)
	public String getCheckInNumber() {
		return this.checkInNumber;
	}

	public void setCheckInNumber(String checkInNumber) {
		this.checkInNumber = checkInNumber;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "check_in_date", nullable = false, length = 10)
	public Date getCheckInDate() {
		return this.checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

}
