package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.AssetServiceCharge;

public class AssetServiceChargeVO extends AssetServiceCharge {

	private String assetName;
	private String serviceNumber;
	private String serviceDate;
	private String costType;
	private long assetServiceDetailId;
	private List<AssetServiceCharge> assetServiceCharges;
	
	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public String getServiceDate() {
		return serviceDate;
	}

	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}

	public String getCostType() {
		return costType;
	}

	public void setCostType(String costType) {
		this.costType = costType;
	}

	public long getAssetServiceDetailId() {
		return assetServiceDetailId;
	}

	public void setAssetServiceDetailId(long assetServiceDetailId) {
		this.assetServiceDetailId = assetServiceDetailId;
	}

	public List<AssetServiceCharge> getAssetServiceCharges() {
		return assetServiceCharges;
	}

	public void setAssetServiceCharges(List<AssetServiceCharge> assetServiceCharges) {
		this.assetServiceCharges = assetServiceCharges;
	}
}
