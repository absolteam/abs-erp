package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductionRequisitionDetail;

public class ProductionRequisitionDetailVO extends ProductionRequisitionDetail
		implements java.io.Serializable {

	private String productName;
	private String storeName;
	private String processMode;
	private double actualQuantity;
	private Long packageDetailId;
	private String baseUnitName;
	private String baseQuantity;
	private Double baseConversionUnit;
	private ProductPackageDetailVO productPackageDetailVO;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	private List<MaterialIdleMixDetailVO> materialIdleMixDetailVOs = new ArrayList<MaterialIdleMixDetailVO>();

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getProcessMode() {
		return processMode;
	}

	public void setProcessMode(String processMode) {
		this.processMode = processMode;
	}

	public List<MaterialIdleMixDetailVO> getMaterialIdleMixDetailVOs() {
		return materialIdleMixDetailVOs;
	}

	public void setMaterialIdleMixDetailVOs(
			List<MaterialIdleMixDetailVO> materialIdleMixDetailVOs) {
		this.materialIdleMixDetailVOs = materialIdleMixDetailVOs;
	}

	public double getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(double actualQuantity) {
		this.actualQuantity = actualQuantity;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}
}
