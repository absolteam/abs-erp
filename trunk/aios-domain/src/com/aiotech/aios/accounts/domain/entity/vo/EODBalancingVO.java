package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.EODBalancing;

public class EODBalancingVO extends EODBalancing {

	private Integer currencySettingId;
	private String currencyMode;
	private String currencyType;
	private String denomination;
	private Double equiavalent;
	private double actualAmount;
	private double tillAmount;
	private Double varianceAmount;
	private String eodDate;
	private String startDate;
	private String endDate;
	private String storeName;
	private Date fromDate;
  	private Date toDate;
  	private Integer storeId;
  	private Long tillId;
  	private Long personId;
  	private String tillNumber;
  	private String fromDateView;
  	private String toDateView;
	private String posUserName;
	private List<EODBalancingDetailVO> eodBalancingDetailVOs = new ArrayList<EODBalancingDetailVO>(); 
	private List<EODBalancingVO> eodBalanceVOList=new ArrayList<EODBalancingVO>();
	Map<String,EODBalancingDetailVO> eodDetailMap=new HashMap<String,EODBalancingDetailVO>();

	public EODBalancingVO() {
	}
	
	public EODBalancingVO(EODBalancing eodBalancing) {
		this.setEodBalancingId(eodBalancing.getEodBalancingId());
		this.setImplementation(eodBalancing.getImplementation());
		this.setReferenceNumber(eodBalancing.getReferenceNumber());
		this.setDate(eodBalancing.getDate());
		this.setEODBalancingDetails(eodBalancing.getEODBalancingDetails());
	}
	
	public Integer getCurrencySettingId() {
		return currencySettingId;
	}
	public void setCurrencySettingId(Integer currencySettingId) {
		this.currencySettingId = currencySettingId;
	}
	public String getCurrencyMode() {
		return currencyMode;
	}
	public void setCurrencyMode(String currencyMode) {
		this.currencyMode = currencyMode;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public String getDenomination() {
		return denomination;
	}
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
	public Double getEquiavalent() {
		return equiavalent;
	}
	public void setEquiavalent(Double equiavalent) {
		this.equiavalent = equiavalent;
	}
	public String getEodDate() {
		return eodDate;
	}
	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}
	public double getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}
	public double getTillAmount() {
		return tillAmount;
	}
	public void setTillAmount(double tillAmount) {
		this.tillAmount = tillAmount;
	}
	public Double getVarianceAmount() {
		return varianceAmount;
	}
	public void setVarianceAmount(Double varianceAmount) {
		this.varianceAmount = varianceAmount;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public List<EODBalancingDetailVO> getEodBalancingDetailVOs() {
		return eodBalancingDetailVOs;
	}
	public void setEodBalancingDetailVOs(
			List<EODBalancingDetailVO> eodBalancingDetailVOs) {
		this.eodBalancingDetailVOs = eodBalancingDetailVOs;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Long getTillId() {
		return tillId;
	}

	public void setTillId(Long tillId) {
		this.tillId = tillId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getTillNumber() {
		return tillNumber;
	}

	public void setTillNumber(String tillNumber) {
		this.tillNumber = tillNumber;
	}

	public String getFromDateView() {
		return fromDateView;
	}

	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}

	public String getToDateView() {
		return toDateView;
	}

	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}

	public String getPosUserName() {
		return posUserName;
	}

	public void setPosUserName(String posUserName) {
		this.posUserName = posUserName;
	}

	public List<EODBalancingVO> getEodBalanceVOList() {
		return eodBalanceVOList;
	}

	public void setEodBalanceVOList(List<EODBalancingVO> eodBalanceVOList) {
		this.eodBalanceVOList = eodBalanceVOList;
	}

	public Map<String, EODBalancingDetailVO> getEodDetailMap() {
		return eodDetailMap;
	}

	public void setEodDetailMap(Map<String, EODBalancingDetailVO> eodDetailMap) {
		this.eodDetailMap = eodDetailMap;
	}
	
}
