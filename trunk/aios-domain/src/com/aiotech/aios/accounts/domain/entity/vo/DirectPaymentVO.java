package com.aiotech.aios.accounts.domain.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.DirectPayment;
import com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail;

public class DirectPaymentVO extends DirectPayment implements Serializable {

	private long directPaymentDetailId;
	private String invoiceNumber;
	private String directPaymentNumber;
	private String directPaymentDate;
	private String directPaymentMode;
	private String amount;
	private String payableType;
	private String payable;
	private String bankName;
	private String accoutNumber;
	private String paymentChequeNumber;
	private String paymentChequeDate;
	
	//Workflow variable
	private Long messageId;
	private String workflowReturnMessage;
	private int processType;
	private Object object;
	private List<DirectPaymentDetail> directPayments;
	private Long alertId;
	
	public String getDirectPaymentNumber() {
		return directPaymentNumber;
	}
	public void setDirectPaymentNumber(String directPaymentNumber) {
		this.directPaymentNumber = directPaymentNumber;
	}
	public String getDirectPaymentDate() {
		return directPaymentDate;
	}
	public void setDirectPaymentDate(String directPaymentDate) {
		this.directPaymentDate = directPaymentDate;
	}
	public String getDirectPaymentMode() {
		return directPaymentMode;
	}
	public void setDirectPaymentMode(String directPaymentMode) {
		this.directPaymentMode = directPaymentMode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayable() {
		return payable;
	}
	public void setPayable(String payable) {
		this.payable = payable;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccoutNumber() {
		return accoutNumber;
	}
	public void setAccoutNumber(String accoutNumber) {
		this.accoutNumber = accoutNumber;
	}
	public String getPaymentChequeNumber() {
		return paymentChequeNumber;
	}
	public void setPaymentChequeNumber(String paymentChequeNumber) {
		this.paymentChequeNumber = paymentChequeNumber;
	}
	public String getPaymentChequeDate() {
		return paymentChequeDate;
	}
	public void setPaymentChequeDate(String paymentChequeDate) {
		this.paymentChequeDate = paymentChequeDate;
	}
	
	public DirectPaymentVO() {
		
	}
	
	public DirectPaymentVO(DirectPayment directPayment) {
		
		this.setDirectPaymentId(directPayment.getDirectPaymentId());
		this.setCurrency(directPayment.getCurrency());
		this.setBankAccount(directPayment.getBankAccount());
		this.setUseCase(directPayment.getUseCase());
		this.setPersonByCreatedBy(directPayment.getPersonByCreatedBy());
		this.setCombination(directPayment.getCombination());
		this.setPersonByPersonId(directPayment.getPersonByPersonId());
		this.setPaymentMode(directPayment.getPaymentMode());
 		this.setChequeBook(directPayment.getChequeBook());
		this.setRecordId(directPayment.getRecordId());
		this.setPaymentRequest(directPayment.getPaymentRequest());
		this.setCustomer(directPayment.getCustomer());
		this.setImplementation(directPayment.getImplementation());
		this.setSupplier(directPayment.getSupplier());
		this.setPaymentNumber(directPayment.getPaymentNumber());
		this.setPaymentDate(directPayment.getPaymentDate());
		this.setChequeNumber(directPayment.getChequeNumber());
		this.setDescription(directPayment.getDescription());
		this.setCreatedDate(directPayment.getCreatedDate());
		this.setChequeDate(directPayment.getChequeDate());
		this.setIsPdc(directPayment.getIsPdc()); 
		this.setOthers(directPayment.getOthers());
		this.setDirectPaymentDetails(directPayment.getDirectPaymentDetails());
		this.setVoidPayments(directPayment.getVoidPayments());
	}
	public long getDirectPaymentDetailId() {
		return directPaymentDetailId;
	}
	public void setDirectPaymentDetailId(long directPaymentDetailId) {
		this.directPaymentDetailId = directPaymentDetailId;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getPayableType() {
		return payableType;
	}
	public void setPayableType(String payableType) {
		this.payableType = payableType;
	}
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public String getWorkflowReturnMessage() {
		return workflowReturnMessage;
	}
	public void setWorkflowReturnMessage(String workflowReturnMessage) {
		this.workflowReturnMessage = workflowReturnMessage;
	}
	public int getProcessType() {
		return processType;
	}
	public void setProcessType(int processType) {
		this.processType = processType;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	public List<DirectPaymentDetail> getDirectPayments() {
		return directPayments;
	}
	public void setDirectPayments(List<DirectPaymentDetail> directPayments) {
		this.directPayments = directPayments;
	}
	public Long getAlertId() {
		return alertId;
	}
	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}
	
}
