package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.SalesOrderDetail;

public class SalesOrderDetailVO extends SalesOrderDetail implements
		java.io.Serializable {

	private String productName;
	private String productCode;
	private String unitCode;
	private String storeName;
	private String strDiscountMode;
	private String conversionUnitName;
	private String baseQuantity;
	private Double packageUnit;
	private Double total;
	private double availableQty;
	private double standardPrice;
	private double basePrice;
	private double deliveredQty;
	private double remainingQtyQty;
	private long storeId;
	private Long packageDetailId;
	private int shelfId;
	private String productType;
	private List<SalesDeliveryDetailVO> salesDeliveryDetailVOs;
	private String baseUnitName;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public SalesOrderDetailVO() {

	}

	public SalesOrderDetailVO(SalesOrderDetail salesOrderDetail) {
		this.setSalesOrderDetailId(salesOrderDetail.getSalesOrderDetailId());
		this.setProduct(salesOrderDetail.getProduct());
		this.setStore(salesOrderDetail.getStore());
		this.setSalesOrder(salesOrderDetail.getSalesOrder());
		this.setOrderQuantity(salesOrderDetail.getOrderQuantity());
		this.setUnitRate(salesOrderDetail.getUnitRate());
		this.setIsPercentage(salesOrderDetail.getIsPercentage());
		this.setDiscount(salesOrderDetail.getDiscount());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStrDiscountMode() {
		return strDiscountMode;
	}

	public void setStrDiscountMode(String strDiscountMode) {
		this.strDiscountMode = strDiscountMode;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public double getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(double availableQty) {
		this.availableQty = availableQty;
	}

	public double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public long getStoreId() {
		return storeId;
	}

	public void setStoreId(long storeId) {
		this.storeId = storeId;
	}

	public int getShelfId() {
		return shelfId;
	}

	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}

	public double getDeliveredQty() {
		return deliveredQty;
	}

	public void setDeliveredQty(double deliveredQty) {
		this.deliveredQty = deliveredQty;
	}

	public double getRemainingQtyQty() {
		return remainingQtyQty;
	}

	public void setRemainingQtyQty(double remainingQtyQty) {
		this.remainingQtyQty = remainingQtyQty;
	}

	public List<SalesDeliveryDetailVO> getSalesDeliveryDetailVOs() {
		return salesDeliveryDetailVOs;
	}

	public void setSalesDeliveryDetailVOs(
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs) {
		this.salesDeliveryDetailVOs = salesDeliveryDetailVOs;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public Double getPackageUnit() {
		return packageUnit;
	}

	public void setPackageUnit(Double packageUnit) {
		this.packageUnit = packageUnit;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}
}
