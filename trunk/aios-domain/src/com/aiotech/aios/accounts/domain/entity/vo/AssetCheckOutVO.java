package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetCheckOut;

public class AssetCheckOutVO extends AssetCheckOut implements java.io.Serializable {

	private String assetName;
	private String outDue;
	private String outDate;
	private String checkOutTo;
	private String departmentName;
	private String locationName;
	private String assetNumber;
	private long personId;
	
	public AssetCheckOutVO(AssetCheckOut assetCheckOut) {
		this.setAssetCheckOutId(assetCheckOut.getAssetCheckOutId());
		this.setCmpDeptLocation(assetCheckOut.getCmpDeptLocation());
		this.setPersonByCheckOutBy(assetCheckOut.getPersonByCheckOutBy());
		this.setPersonByCheckOutTo(assetCheckOut.getPersonByCheckOutTo());
		this.setAssetCreation(assetCheckOut.getAssetCreation());
		this.setCheckOutNumber(assetCheckOut.getCheckOutNumber());
		this.setCheckOutDate(assetCheckOut.getCheckOutDate());
		this.setCheckOutDue(assetCheckOut.getCheckOutDue());
		this.setDescription(assetCheckOut.getDescription());
		this.setAssetCheckIns(assetCheckOut.getAssetCheckIns());
	}
	
	public AssetCheckOutVO() {
		
	}
	
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getOutDue() {
		return outDue;
	}
	public void setOutDue(String outDue) {
		this.outDue = outDue;
	}
	public String getOutDate() {
		return outDate;
	}
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	public String getCheckOutTo() {
		return checkOutTo;
	}
	public void setCheckOutTo(String checkOutTo) {
		this.checkOutTo = checkOutTo;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getAssetNumber() {
		return assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}
	
}
