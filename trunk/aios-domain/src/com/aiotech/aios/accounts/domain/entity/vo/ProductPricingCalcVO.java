package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.ProductPricingCalc;

public class ProductPricingCalcVO extends ProductPricingCalc implements java.io.Serializable {

	private String calculationSubTypeCode;
	private String calculationTypeCode;
	private String pricingType;
	private Double salesAmount;
	private String storeName;
	private String customerName;
	public ProductPricingCalcVO() {
	}
	
	public ProductPricingCalcVO(ProductPricingCalc productPricingCalc) {
		this.setProductPricingCalcId(productPricingCalc.getProductPricingCalcId());
		this.setProductPricingDetail(productPricingCalc.getProductPricingDetail());
		this.setCustomer(productPricingCalc.getCustomer());
		this.setLookupDetail(productPricingCalc.getLookupDetail());
		this.setCalculationType(productPricingCalc.getCalculationType());
		this.setCalculationSubType(productPricingCalc.getCalculationSubType());
		this.setSalePrice(productPricingCalc.getSalePrice());
		this.setSalePriceAmount(productPricingCalc.getSalePriceAmount());
		this.setIsDefault(productPricingCalc.getIsDefault());
		this.setDescription(productPricingCalc.getDescription());
		this.setPointOfSaleDetails(productPricingCalc.getPointOfSaleDetails());
 	}
	
	public String getCalculationSubTypeCode() {
		return calculationSubTypeCode;
	}
	public void setCalculationSubTypeCode(String calculationSubTypeCode) {
		this.calculationSubTypeCode = calculationSubTypeCode;
	}
	public String getCalculationTypeCode() {
		return calculationTypeCode;
	}
	public void setCalculationTypeCode(String calculationTypeCode) {
		this.calculationTypeCode = calculationTypeCode;
	}
	public String getPricingType() {
		return pricingType;
	}
	public void setPricingType(String pricingType) {
		this.pricingType = pricingType;
	}
	public Double getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(Double salesAmount) {
		this.salesAmount = salesAmount;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
}
