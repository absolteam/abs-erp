package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.PurchaseDetail;

public class PurchaseDetailVO extends PurchaseDetail implements
		java.io.Serializable {

	private Double returnQty;
	private Double receiveQty;
	private Double purchaseQty;
	private Double totalRate;
	private Double outStandingQty;
	private Double receivedQty;
	private Double returnedQty;
	private Double batchReceiveQty;
	private Double batchReturnQty;
	private Double totalPurchaseProduct;
	private Double currentReceivedQty;
	private Double currentReturnQty;
	private Double baseConversionUnit;
	private String baseUnitName;
	private Long storeId;
	private Long supplierCombinationId;
	private Long productCombinationId;
	private Long requisitionDetailId;
	private Long receiveDetailId;
	private Long purchaseId;
	private Long packageDetailId;
	private Integer rackId;
	private Integer shelfId;
	private Integer currencyId;
	private String storeName;
	private String rackType;
	private String purchaseOrderReferenceNumber;
	private String supplierNameAndNumber;
	private String description;
	private Double unitRate;
	private String unitRateStr;
	private String totalRateStr;
	private String deliveryDate;
	private String receiveDate;
	private String expiryDate;
	private String referenceNumber;
	private String supplierName;
	private String purchaseDate;
	private String productName;
	private String baseQuantity;
	private String convertionUnitName;
	private String unitCode;
	private Integer flowId;
	private PurchaseVO purchaseVO;
	private ProductPackageDetailVO productPackageDetailVO;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public Double getOutStandingQty() {
		return outStandingQty;
	}

	public void setOutStandingQty(Double outStandingQty) {
		this.outStandingQty = outStandingQty;
	}

	public Double getReturnQty() {
		return returnQty;
	}

	public void setReturnQty(Double returnQty) {
		this.returnQty = returnQty;
	}

	public Double getReceiveQty() {
		return receiveQty;
	}

	public void setReceiveQty(Double receiveQty) {
		this.receiveQty = receiveQty;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public Double getPurchaseQty() {
		return purchaseQty;
	}

	public void setPurchaseQty(Double purchaseQty) {
		this.purchaseQty = purchaseQty;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Double getReturnedQty() {
		return returnedQty;
	}

	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public Double getTotalPurchaseProduct() {
		return totalPurchaseProduct;
	}

	public void setTotalPurchaseProduct(Double totalPurchaseProduct) {
		this.totalPurchaseProduct = totalPurchaseProduct;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Long getProductCombinationId() {
		return productCombinationId;
	}

	public void setProductCombinationId(Long productCombinationId) {
		this.productCombinationId = productCombinationId;
	}

	public Long getSupplierCombinationId() {
		return supplierCombinationId;
	}

	public void setSupplierCombinationId(Long supplierCombinationId) {
		this.supplierCombinationId = supplierCombinationId;
	}

	public Integer getRackId() {
		return rackId;
	}

	public void setRackId(Integer rackId) {
		this.rackId = rackId;
	}

	public String getRackType() {
		return rackType;
	}

	public void setRackType(String rackType) {
		this.rackType = rackType;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Double getBatchReceiveQty() {
		return batchReceiveQty;
	}

	public void setBatchReceiveQty(Double batchReceiveQty) {
		this.batchReceiveQty = batchReceiveQty;
	}

	public Double getBatchReturnQty() {
		return batchReturnQty;
	}

	public void setBatchReturnQty(Double batchReturnQty) {
		this.batchReturnQty = batchReturnQty;
	}

	public Double getCurrentReceivedQty() {
		return currentReceivedQty;
	}

	public void setCurrentReceivedQty(Double currentReceivedQty) {
		this.currentReceivedQty = currentReceivedQty;
	}

	public Double getCurrentReturnQty() {
		return currentReturnQty;
	}

	public void setCurrentReturnQty(Double currentReturnQty) {
		this.currentReturnQty = currentReturnQty;
	}

	public String getPurchaseOrderReferenceNumber() {
		return purchaseOrderReferenceNumber;
	}

	public void setPurchaseOrderReferenceNumber(
			String purchaseOrderReferenceNumber) {
		this.purchaseOrderReferenceNumber = purchaseOrderReferenceNumber;
	}

	public String getSupplierNameAndNumber() {
		return supplierNameAndNumber;
	}

	public void setSupplierNameAndNumber(String supplierNameAndNumber) {
		this.supplierNameAndNumber = supplierNameAndNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getRequisitionDetailId() {
		return requisitionDetailId;
	}

	public void setRequisitionDetailId(Long requisitionDetailId) {
		this.requisitionDetailId = requisitionDetailId;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Long getReceiveDetailId() {
		return receiveDetailId;
	}

	public void setReceiveDetailId(Long receiveDetailId) {
		this.receiveDetailId = receiveDetailId;
	}

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}

	public Integer getFlowId() {
		return flowId;
	}

	public void setFlowId(Integer flowId) {
		this.flowId = flowId;
	}

	public PurchaseVO getPurchaseVO() {
		return purchaseVO;
	}

	public void setPurchaseVO(PurchaseVO purchaseVO) {
		this.purchaseVO = purchaseVO;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getConvertionUnitName() {
		return convertionUnitName;
	}

	public void setConvertionUnitName(String convertionUnitName) {
		this.convertionUnitName = convertionUnitName;
	}

	public String getUnitRateStr() {
		return unitRateStr;
	}

	public void setUnitRateStr(String unitRateStr) {
		this.unitRateStr = unitRateStr;
	}

	public String getTotalRateStr() {
		return totalRateStr;
	}

	public void setTotalRateStr(String totalRateStr) {
		this.totalRateStr = totalRateStr;
	}
}
