package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.VoidPayment;

public class VoidChequeVO extends VoidPayment implements java.io.Serializable{ 
	
	private String paymentNumber;
	private String chequeDate;
	private Integer chequeNo;
	private Integer chequeBookNo;
	private String payeeName;
	private String paymentType;
	private String bankName;
	private String accountNumber;
	private String voidDateStr;
	private BankAccount bankAccount; 
	private long directPaymentId;
	private long paymentId;
	
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public Integer getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(Integer chequeNo) {
		this.chequeNo = chequeNo;
	} 
	public BankAccount getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}
	public Integer getChequeBookNo() {
		return chequeBookNo;
	}
	public void setChequeBookNo(Integer chequeBookNo) {
		this.chequeBookNo = chequeBookNo;
	}
	public String getPaymentNumber() {
		return paymentNumber;
	}
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}
	public String getPayeeName() {
		return payeeName;
	}
	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	} 
	public long getDirectPaymentId() {
		return directPaymentId;
	}
	public void setDirectPaymentId(long directPaymentId) {
		this.directPaymentId = directPaymentId;
	}
	public long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getVoidDateStr() {
		return voidDateStr;
	}
	public void setVoidDateStr(String voidDateStr) {
		this.voidDateStr = voidDateStr;
	}
	 
}
