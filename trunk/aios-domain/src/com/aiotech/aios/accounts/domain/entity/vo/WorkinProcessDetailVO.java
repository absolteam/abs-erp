package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.WorkinProcessDetail;

public class WorkinProcessDetailVO extends WorkinProcessDetail implements java.io.Serializable {

	private String storeName; 
	private Long packageDetailId;
	private String baseUnitName;
	private String conversionUnitName;
	private String baseQuantity;
	private String wastageQuantityStr;
	private Double baseConversionUnit;
	private ProductPackageDetailVO productPackageDetailVO;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getWastageQuantityStr() {
		return wastageQuantityStr;
	}

	public void setWastageQuantityStr(String wastageQuantityStr) {
		this.wastageQuantityStr = wastageQuantityStr;
	}
}
