package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.CustomerQuotationCharge;

public class CustomerQuotationChargeVO extends CustomerQuotationCharge{
	
	private String chargesType;
	private String chargesMode;
	
	public CustomerQuotationChargeVO() {
		
	}
	
	public CustomerQuotationChargeVO(CustomerQuotationCharge customerQuotationCharge) {
		this.setCustomerQuotationChargeId(customerQuotationCharge.getCustomerQuotationChargeId());
		this.setCustomerQuotation(customerQuotationCharge.getCustomerQuotation());
		this.setLookupDetail(customerQuotationCharge.getLookupDetail());
		this.setIsPercentage(customerQuotationCharge.getIsPercentage());
		this.setCharges(customerQuotationCharge.getCharges());
		this.setDescription(customerQuotationCharge.getDescription());
	}
	
	public String getChargesType() {
		return chargesType;
	}
	public void setChargesType(String chargesType) {
		this.chargesType = chargesType;
	}
	public String getChargesMode() {
		return chargesMode;
	}
	public void setChargesMode(String chargesMode) {
		this.chargesMode = chargesMode;
	}
	
	
	
	

}
