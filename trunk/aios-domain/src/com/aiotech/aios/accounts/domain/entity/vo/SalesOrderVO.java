package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.SalesOrder;

public class SalesOrderVO extends SalesOrder implements java.io.Serializable {

	private String salesDate;
	private String shipDate;
	private String expirySales;
	private String customerName;
	private String salesReperesentive;
	private String orderTypeDetail;
	private String salesStatus; 
	private Long customerId;
	private Long shippingDetailId;
	private Long salesReperesentiveId;
	private Long shippingMethodId;
	private Long shippingTermsId;
 	private Long currencyId;
 	private String customerNameAndNo;
	private String shippingMethod;
	private String shippingTerm;
	private String strStatus;
	private Boolean deliveryFlag;
	private List<SalesOrderDetailVO> salesOrderDetailVOs;
	private List<SalesOrderChargesVO> salesOrderChargesVOs; 
	private Long creditTermId;
 	
 	public SalesOrderVO() {
 		
 	}
 	
 	public SalesOrderVO(SalesOrder salesOrder) {
 		this.setSalesOrderId(salesOrder.getSalesOrderId());
 		this.setLookupDetailByOrderType(salesOrder.getLookupDetailByOrderType());
 		this.setCurrency(salesOrder.getCurrency());
 		this.setCustomerQuotation(salesOrder.getCustomerQuotation());
 		this.setImplementation(salesOrder.getImplementation());
 		this.setCustomer(salesOrder.getCustomer());
 		this.setLookupDetailByShippingTerm(salesOrder.getLookupDetailByShippingTerm());
 		this.setPerson(salesOrder.getPerson());
 		this.setShippingDetail(salesOrder.getShippingDetail());
 		this.setLookupDetailByShippingMethod(salesOrder.getLookupDetailByShippingMethod());
 		this.setReferenceNumber(salesOrder.getReferenceNumber());
 		this.setOrderDate(salesOrder.getOrderDate());
 		this.setShippingDate(salesOrder.getShippingDate());
 		this.setExpiryDate(salesOrder.getExpiryDate());
 		this.setOrderStatus(salesOrder.getOrderStatus());
 		this.setModeOfPayment(salesOrder.getModeOfPayment());
 		this.setDescription(salesOrder.getDescription());
 		this.setSalesOrderCharges(salesOrder.getSalesOrderCharges());
 		this.setSalesDeliveryNotes(salesOrder.getSalesDeliveryNotes());
 		this.setSalesOrderDetails(salesOrder.getSalesOrderDetails());
 		
 	}
	
	public String getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(String salesDate) {
		this.salesDate = salesDate;
	}

	public String getShipDate() {
		return shipDate;
	}

	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}

	public String getExpirySales() {
		return expirySales;
	}

	public void setExpirySales(String expirySales) {
		this.expirySales = expirySales;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSalesReperesentive() {
		return salesReperesentive;
	}

	public void setSalesReperesentive(String salesReperesentive) {
		this.salesReperesentive = salesReperesentive;
	}

	public String getOrderTypeDetail() {
		return orderTypeDetail;
	}

	public void setOrderTypeDetail(String orderTypeDetail) {
		this.orderTypeDetail = orderTypeDetail;
	}

	public String getSalesStatus() {
		return salesStatus;
	}

	public void setSalesStatus(String salesStatus) {
		this.salesStatus = salesStatus;
	}
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getSalesReperesentiveId() {
		return salesReperesentiveId;
	}

	public void setSalesReperesentiveId(Long salesReperesentiveId) {
		this.salesReperesentiveId = salesReperesentiveId;
	}

	public Long getShippingMethodId() {
		return shippingMethodId;
	}

	public void setShippingMethodId(Long shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}

	public Long getShippingTermsId() {
		return shippingTermsId;
	}

	public void setShippingTermsId(Long shippingTermsId) {
		this.shippingTermsId = shippingTermsId;
	}

	public String getCustomerNameAndNo() {
		return customerNameAndNo;
	}

	public void setCustomerNameAndNo(String customerNameAndNo) {
		this.customerNameAndNo = customerNameAndNo;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(String shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

	public List<SalesOrderDetailVO> getSalesOrderDetailVOs() {
		return salesOrderDetailVOs;
	}

	public void setSalesOrderDetailVOs(List<SalesOrderDetailVO> salesOrderDetailVOs) {
		this.salesOrderDetailVOs = salesOrderDetailVOs;
	}

	public List<SalesOrderChargesVO> getSalesOrderChargesVOs() {
		return salesOrderChargesVOs;
	}

	public void setSalesOrderChargesVOs(
			List<SalesOrderChargesVO> salesOrderChargesVOs) {
		this.salesOrderChargesVOs = salesOrderChargesVOs;
	}

	public Long getShippingDetailId() {
		return shippingDetailId;
	}

	public void setShippingDetailId(Long shippingDetailId) {
		this.shippingDetailId = shippingDetailId;
	}

	public Boolean getDeliveryFlag() {
		return deliveryFlag;
	}

	public void setDeliveryFlag(Boolean deliveryFlag) {
		this.deliveryFlag = deliveryFlag;
	}

	public Long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(Long creditTermId) {
		this.creditTermId = creditTermId;
	} 
}
