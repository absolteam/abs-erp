package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetWarrantyClaim;

public class AssetWarrantyClaimVO extends AssetWarrantyClaim {

	private String date;
	private String assetName;
	private String warrantyNumber;
	private String statusName;
	private String payOutNature;
	private String paymentMode;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public String getWarrantyNumber() {
		return warrantyNumber;
	}
	public void setWarrantyNumber(String warrantyNumber) {
		this.warrantyNumber = warrantyNumber;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getPayOutNature() {
		return payOutNature;
	}
	public void setPayOutNature(String payOutNature) {
		this.payOutNature = payOutNature;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
}
