package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryDetail;

public class SalesDeliveryDetailVO extends SalesDeliveryDetail implements
		java.io.Serializable {

	private Double invoiceQuantity;
	private Double invoicedQty;
	private Double deliveredQuantity;
	private Double totalRate;
	private Double invoiceUnitRate;
	private Double inwardedQuantity;
	private Double inwardQuantity;
	private Double remainingQuantity;
	private Double stockAvailableQuantity;
	private Long salesInvoiceDetailId;
	private Long productId;
	private Long packageDetailId;
	private Integer shelfId;
	private String storeName;
	private String productCode;
	private String productName;
	private String unitName;
	private String strDiscountMode;
	private String expiryDate;
	private Double total;
	private double basePrice;
	private double standardPrice;
	private String salesReferenceNumber;
	private String customerNameAndNumber;
	private String description;
	private String salesPersonName;
	private String baseUnitName;
	private String productType;
	private String conversionUnitName;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	private String status;
	private String isInvoiced;
	private String saleModule;
	private Double unitRate;
	private String baseQuantity;
	private String unitCode;
	private String displayAmount;
	private String displayDiscount;
	
	public SalesDeliveryDetailVO() {

	}

	public SalesDeliveryDetailVO(SalesDeliveryDetail salesDeliveryDetail) {
		this.setSalesDeliveryDetailId(salesDeliveryDetail
				.getSalesDeliveryDetailId());
		this.setProduct(salesDeliveryDetail.getProduct());
		this.setSalesDeliveryNote(salesDeliveryDetail.getSalesDeliveryNote());
		this.setShelf(salesDeliveryDetail.getShelf());
		this.setOrderQuantity(salesDeliveryDetail.getOrderQuantity());
		this.setDeliveryQuantity(salesDeliveryDetail.getDeliveryQuantity());
		this.setUnitRate(salesDeliveryDetail.getUnitRate());
		this.setIsPercentage(salesDeliveryDetail.getIsPercentage());
		this.setDiscount(salesDeliveryDetail.getDiscount());
		this.setCreditDetails(salesDeliveryDetail.getCreditDetails());
		this.setSalesInvoiceDetails(salesDeliveryDetail
				.getSalesInvoiceDetails());
	}

	public Double getInvoiceQuantity() {
		return invoiceQuantity;
	}

	public void setInvoiceQuantity(Double invoiceQuantity) {
		this.invoiceQuantity = invoiceQuantity;
	}

	public Long getSalesInvoiceDetailId() {
		return salesInvoiceDetailId;
	}

	public void setSalesInvoiceDetailId(Long salesInvoiceDetailId) {
		this.salesInvoiceDetailId = salesInvoiceDetailId;
	}

	public Double getInvoicedQty() {
		return invoicedQty;
	}

	public void setInvoicedQty(Double invoicedQty) {
		this.invoicedQty = invoicedQty;
	}

	public Double getDeliveredQuantity() {
		return deliveredQuantity;
	}

	public void setDeliveredQuantity(Double deliveredQuantity) {
		this.deliveredQuantity = deliveredQuantity;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public Double getInvoiceUnitRate() {
		return invoiceUnitRate;
	}

	public void setInvoiceUnitRate(Double invoiceUnitRate) {
		this.invoiceUnitRate = invoiceUnitRate;
	}

	public Double getInwardedQuantity() {
		return inwardedQuantity;
	}

	public void setInwardedQuantity(Double inwardedQuantity) {
		this.inwardedQuantity = inwardedQuantity;
	}

	public Double getInwardQuantity() {
		return inwardQuantity;
	}

	public void setInwardQuantity(Double inwardQuantity) {
		this.inwardQuantity = inwardQuantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStrDiscountMode() {
		return strDiscountMode;
	}

	public void setStrDiscountMode(String strDiscountMode) {
		this.strDiscountMode = strDiscountMode;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public Double getRemainingQuantity() {
		return remainingQuantity;
	}

	public void setRemainingQuantity(Double remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}

	public String getSalesReferenceNumber() {
		return salesReferenceNumber;
	}

	public void setSalesReferenceNumber(String salesReferenceNumber) {
		this.salesReferenceNumber = salesReferenceNumber;
	}

	public String getCustomerNameAndNumber() {
		return customerNameAndNumber;
	}

	public void setCustomerNameAndNumber(String customerNameAndNumber) {
		this.customerNameAndNumber = customerNameAndNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSalesPersonName() {
		return salesPersonName;
	}

	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsInvoiced() {
		return isInvoiced;
	}

	public void setIsInvoiced(String isInvoiced) {
		this.isInvoiced = isInvoiced;
	}

	public String getSaleModule() {
		return saleModule;
	}

	public void setSaleModule(String saleModule) {
		this.saleModule = saleModule;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public Double getStockAvailableQuantity() {
		return stockAvailableQuantity;
	}

	public void setStockAvailableQuantity(Double stockAvailableQuantity) {
		this.stockAvailableQuantity = stockAvailableQuantity;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitCode() {
		return unitCode;
	}

	public void setUnitCode(String unitCode) {
		this.unitCode = unitCode;
	}

	public String getDisplayAmount() {
		return displayAmount;
	}

	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}

	public String getDisplayDiscount() {
		return displayDiscount;
	}

	public void setDisplayDiscount(String displayDiscount) {
		this.displayDiscount = displayDiscount;
	} 
}
