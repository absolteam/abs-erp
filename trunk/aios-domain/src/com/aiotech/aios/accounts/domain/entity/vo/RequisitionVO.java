package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.aiotech.aios.accounts.domain.entity.Requisition;

public class RequisitionVO extends Requisition implements java.io.Serializable {

	private String date;
	private String personName;
	private String statusStr;
	private String locationName;
	private String totalRequisitionQty;
	private String totalQuotationQty;
	private String totalPurchaseQty;
	private String totalReceiveQty;
	private String totalReturnQty;
	private String totalIssuedQty;
	private String totalIssueReturnQty;
	private String totalReceivePendingQty;
	private long locationId;
	private String purchaseNumber;
	private String receiveNumber;
	
	private List<RequisitionDetailVO> requisitionDetailVOs;
	private Map<Long, RequisitionDetailVO> requisitionDetailMap;
	private Map<Long, PurchaseVO> purchaseMap;
	private Set<Long> receiveVOSet;
	private List<PurchaseDetailVO> purchaseDetailVOs;
	private List<ReceiveDetailVO> receiveDetailVOs;
	private List<IssueRequistionDetailVO> issueRequistionDetailVOs;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public List<RequisitionDetailVO> getRequisitionDetailVOs() {
		return requisitionDetailVOs;
	}
	public void setRequisitionDetailVOs(
			List<RequisitionDetailVO> requisitionDetailVOs) {
		this.requisitionDetailVOs = requisitionDetailVOs;
	}
	public long getLocationId() {
		return locationId;
	}
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getTotalQuotationQty() {
		return totalQuotationQty;
	}
	public void setTotalQuotationQty(String totalQuotationQty) {
		this.totalQuotationQty = totalQuotationQty;
	}
	public String getTotalRequisitionQty() {
		return totalRequisitionQty;
	}
	public void setTotalRequisitionQty(String totalRequisitionQty) {
		this.totalRequisitionQty = totalRequisitionQty;
	}
	public String getTotalPurchaseQty() {
		return totalPurchaseQty;
	}
	public void setTotalPurchaseQty(String totalPurchaseQty) {
		this.totalPurchaseQty = totalPurchaseQty;
	}
	public Map<Long, RequisitionDetailVO> getRequisitionDetailMap() {
		return requisitionDetailMap;
	}
	public void setRequisitionDetailMap(
			Map<Long, RequisitionDetailVO> requisitionDetailMap) {
		this.requisitionDetailMap = requisitionDetailMap;
	}
	public String getTotalReceiveQty() {
		return totalReceiveQty;
	}
	public void setTotalReceiveQty(String totalReceiveQty) {
		this.totalReceiveQty = totalReceiveQty;
	}
	public String getTotalReturnQty() {
		return totalReturnQty;
	}
	public void setTotalReturnQty(String totalReturnQty) {
		this.totalReturnQty = totalReturnQty;
	}
	public String getTotalIssuedQty() {
		return totalIssuedQty;
	}
	public void setTotalIssuedQty(String totalIssuedQty) {
		this.totalIssuedQty = totalIssuedQty;
	}
	public String getTotalIssueReturnQty() {
		return totalIssueReturnQty;
	}
	public void setTotalIssueReturnQty(String totalIssueReturnQty) {
		this.totalIssueReturnQty = totalIssueReturnQty;
	}
	public String getTotalReceivePendingQty() {
		return totalReceivePendingQty;
	}
	public void setTotalReceivePendingQty(String totalReceivePendingQty) {
		this.totalReceivePendingQty = totalReceivePendingQty;
	}
	public Map<Long, PurchaseVO> getPurchaseMap() {
		return purchaseMap;
	}
	public void setPurchaseMap(Map<Long, PurchaseVO> purchaseMap) {
		this.purchaseMap = purchaseMap;
	}
	public List<PurchaseDetailVO> getPurchaseDetailVOs() {
		return purchaseDetailVOs;
	}
	public void setPurchaseDetailVOs(List<PurchaseDetailVO> purchaseDetailVOs) {
		this.purchaseDetailVOs = purchaseDetailVOs;
	}
	public List<ReceiveDetailVO> getReceiveDetailVOs() {
		return receiveDetailVOs;
	}
	public void setReceiveDetailVOs(List<ReceiveDetailVO> receiveDetailVOs) {
		this.receiveDetailVOs = receiveDetailVOs;
	}
	public List<IssueRequistionDetailVO> getIssueRequistionDetailVOs() {
		return issueRequistionDetailVOs;
	}
	public void setIssueRequistionDetailVOs(
			List<IssueRequistionDetailVO> issueRequistionDetailVOs) {
		this.issueRequistionDetailVOs = issueRequistionDetailVOs;
	}  
	public String getPurchaseNumber() {
		return purchaseNumber;
	}
	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}
	public String getReceiveNumber() {
		return receiveNumber;
	}
	public void setReceiveNumber(String receiveNumber) {
		this.receiveNumber = receiveNumber;
	}
	public Set<Long> getReceiveVOSet() {
		return receiveVOSet;
	}
	public void setReceiveVOSet(Set<Long> receiveVOSet) {
		this.receiveVOSet = receiveVOSet;
	} 
}
