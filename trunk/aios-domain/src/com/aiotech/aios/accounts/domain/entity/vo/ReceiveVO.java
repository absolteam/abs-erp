package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Receive;

public class ReceiveVO extends Receive implements java.io.Serializable{

	private Double totalReceiveQty;
	private Double totalReceiveAmount;
	private String strReceiveDate;
	private String supplierName;
	private boolean invoiceStatus;
	private String grnDate;
	private long supplierId;
	private List<ReceiveDetailVO> receiveDetailsVO = new ArrayList<ReceiveDetailVO>();
	private boolean editFlag;
	private String displayQuantity;
	private String displayUnitRate;
	private String displayTotalAmount;
	private String displayTotalAmountInWords;
	private String purchaseNumber;
	private String requisitionNumber;
	public ReceiveVO() {
		
	}
	
	public ReceiveVO(Receive receive) {
		this.setReceiveId(receive.getReceiveId());
		this.setImplementation(receive.getImplementation());
 		this.setSupplier(receive.getSupplier());
		this.setReceiveNumber(receive.getReceiveNumber());
		this.setReceiveDate(receive.getReceiveDate());
		this.setDescription(receive.getDescription());
		this.setReferenceNo(receive.getReferenceNo());
		this.setStatus(receive.getStatus());
		this.setInvoices(receive.getInvoices());
		this.setPayments(receive.getPayments());
		this.setReceiveDetails(receive.getReceiveDetails());
		
	}

	public List<ReceiveDetailVO> getReceiveDetailsVO() {
		return receiveDetailsVO;
	}

	public void setReceiveDetailsVO(List<ReceiveDetailVO> receiveDetailsVO) {
		this.receiveDetailsVO = receiveDetailsVO;
	}

	public Double getTotalReceiveQty() {
		return totalReceiveQty;
	}

	public void setTotalReceiveQty(Double totalReceiveQty) {
		this.totalReceiveQty = totalReceiveQty;
	}

	public Double getTotalReceiveAmount() {
		return totalReceiveAmount;
	}

	public void setTotalReceiveAmount(Double totalReceiveAmount) {
		this.totalReceiveAmount = totalReceiveAmount;
	}

	public String getStrReceiveDate() {
		return strReceiveDate;
	}

	public void setStrReceiveDate(String strReceiveDate) {
		this.strReceiveDate = strReceiveDate;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public boolean isInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(boolean invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getGrnDate() {
		return grnDate;
	}

	public void setGrnDate(String grnDate) {
		this.grnDate = grnDate;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public String getDisplayQuantity() {
		return displayQuantity;
	}

	public void setDisplayQuantity(String displayQuantity) {
		this.displayQuantity = displayQuantity;
	}

	public String getDisplayUnitRate() {
		return displayUnitRate;
	}

	public void setDisplayUnitRate(String displayUnitRate) {
		this.displayUnitRate = displayUnitRate;
	}

	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}

	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}

	public String getDisplayTotalAmountInWords() {
		return displayTotalAmountInWords;
	}

	public void setDisplayTotalAmountInWords(String displayTotalAmountInWords) {
		this.displayTotalAmountInWords = displayTotalAmountInWords;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public String getRequisitionNumber() {
		return requisitionNumber;
	}

	public void setRequisitionNumber(String requisitionNumber) {
		this.requisitionNumber = requisitionNumber;
	}

}
