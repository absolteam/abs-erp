package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.IssueRequistionDetail;

public class IssueRequistionDetailVO extends IssueRequistionDetail implements java.io.Serializable{

	private Double returnedQty;
	private Double returnQty;
	private Double totalRate;
	private Double issuedQty;
	private Double selectedReturnQty;
	private Double availableQuantity;
	private Long returnDetailId;
	private Long materialCondition;
	private String productCode;
	private String productName;
	private String storeName;
	private IssueRequistionVO issueRequistionVO;
	private String section;
	private String displayTotalAmount;
	private String issueDate;
	private String referenceNumber;
	private String personName;
	private Long packageDetailId;
	private String baseUnitName;
	private Double baseConversionUnit;
	private String expiryDate;
	private String productType;
	private String baseQuantity;
	private String unitNameStr;
	private ProductPackageDetailVO productPackageDetailVO;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	public IssueRequistionDetailVO() {
		
	}
	
	public IssueRequistionDetailVO(IssueRequistionDetail issueRequistionDetail) {
		this.setIssueRequistionDetailId(issueRequistionDetail.getIssueRequistionDetailId());
		this.setProduct(issueRequistionDetail.getProduct());
		this.setIssueRequistion(issueRequistionDetail.getIssueRequistion());
		this.setShelf(issueRequistionDetail.getShelf());
		this.setQuantity(issueRequistionDetail.getQuantity());
		this.setUnitRate(issueRequistionDetail.getUnitRate());
		this.setDescription(issueRequistionDetail.getDescription());
		this.setIssueReturnDetails(issueRequistionDetail.getIssueReturnDetails());
	}
	
	
	public Double getReturnedQty() {
		return returnedQty;
	}
	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}
	public Double getReturnQty() {
		return returnQty;
	}
	public void setReturnQty(Double returnQty) {
		this.returnQty = returnQty;
	}
	public Double getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}
	public Double getIssuedQty() {
		return issuedQty;
	}
	public void setIssuedQty(Double issuedQty) {
		this.issuedQty = issuedQty;
	}
	public Double getSelectedReturnQty() {
		return selectedReturnQty;
	}
	public void setSelectedReturnQty(Double selectedReturnQty) {
		this.selectedReturnQty = selectedReturnQty;
	}
	public Long getReturnDetailId() {
		return returnDetailId;
	}
	public void setReturnDetailId(Long returnDetailId) {
		this.returnDetailId = returnDetailId;
	}
	public Long getMaterialCondition() {
		return materialCondition;
	}
	public void setMaterialCondition(Long materialCondition) {
		this.materialCondition = materialCondition;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(Double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public IssueRequistionVO getIssueRequistionVO() {
		return issueRequistionVO;
	}

	public void setIssueRequistionVO(IssueRequistionVO issueRequistionVO) {
		this.issueRequistionVO = issueRequistionVO;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}

	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitNameStr() {
		return unitNameStr;
	}

	public void setUnitNameStr(String unitNameStr) {
		this.unitNameStr = unitNameStr;
	}
	
}
