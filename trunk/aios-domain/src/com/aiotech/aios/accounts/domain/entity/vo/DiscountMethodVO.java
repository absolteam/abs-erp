package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.DiscountMethod;

public class DiscountMethodVO extends DiscountMethod {
	
	private String flat;
	private String discountFormat;
	
	public DiscountMethodVO() {
	}
	
	public DiscountMethodVO(DiscountMethod discountMethod) {
		this.setDiscountMethodId(discountMethod.getDiscountMethodId());
		this.setDiscount(discountMethod.getDiscount());
		this.setFlatPercentage(discountMethod.getFlatPercentage());
		this.setFlatAmount(discountMethod.getFlatAmount());
		this.setSalesAmount(discountMethod.getSalesAmount());
		this.setDiscountAmount(discountMethod.getDiscountAmount());
		this.setDiscountPercentage(discountMethod.getDiscountPercentage());
		this.setDescription(discountMethod.getDescription());
	}

	public String getFlat() {
		return flat;
	}

	public void setFlat(String flat) {
		this.flat = flat;
	}

	public String getDiscountFormat() {
		return discountFormat;
	}

	public void setDiscountFormat(String discountFormat) {
		this.discountFormat = discountFormat;
	}
	
}
