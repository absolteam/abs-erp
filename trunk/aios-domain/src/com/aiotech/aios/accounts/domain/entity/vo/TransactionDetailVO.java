package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aiotech.aios.accounts.domain.entity.TransactionDetail;

public class TransactionDetailVO extends TransactionDetail implements
		java.io.Serializable {

	private String accountCode;
	private String accountDescription;
	private String transactionRef;
	private String debitAmount;
	private String creditAmount;
	private String journalNumber;
	private String closingBalance;
	private String accountSubType;
	private String expenseAmount;
	private String revenueAmount;
	private String assetAmount;
	private String liabilityAmount;
	private String transactionDate;
 	private Long costCenterAccountId;
	private Integer accountType;
	private Double retainEarnings;
	private Boolean ledgerSide;
	private List<TransactionDetailVO> transactionDetailVOs = new ArrayList<TransactionDetailVO>();
	private Map<Long, TransactionDetailVO> transactionDetailVOMap = new HashMap<Long, TransactionDetailVO>();
	private Map<String, List<TransactionDetailVO>> transactionDetailVOGroup = new HashMap<String, List<TransactionDetailVO>>();

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountDescription() {
		return accountDescription;
	}

	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getJournalNumber() {
		return journalNumber;
	}

	public void setJournalNumber(String journalNumber) {
		this.journalNumber = journalNumber;
	}

	public String getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(String closingBalance) {
		this.closingBalance = closingBalance;
	}

	public Boolean getLedgerSide() {
		return ledgerSide;
	}

	public void setLedgerSide(Boolean ledgerSide) {
		this.ledgerSide = ledgerSide;
	}

	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public Long getCostCenterAccountId() {
		return costCenterAccountId;
	}

	public void setCostCenterAccountId(Long costCenterAccountId) {
		this.costCenterAccountId = costCenterAccountId;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public String getExpenseAmount() {
		return expenseAmount;
	}

	public void setExpenseAmount(String expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public String getRevenueAmount() {
		return revenueAmount;
	}

	public void setRevenueAmount(String revenueAmount) {
		this.revenueAmount = revenueAmount;
	}

	public String getAssetAmount() {
		return assetAmount;
	}

	public void setAssetAmount(String assetAmount) {
		this.assetAmount = assetAmount;
	}

	public String getLiabilityAmount() {
		return liabilityAmount;
	}

	public void setLiabilityAmount(String liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	public Double getRetainEarnings() {
		return retainEarnings;
	}

	public void setRetainEarnings(Double retainEarnings) {
		this.retainEarnings = retainEarnings;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Map<Long, TransactionDetailVO> getTransactionDetailVOMap() {
		return transactionDetailVOMap;
	}

	public void setTransactionDetailVOMap(
			Map<Long, TransactionDetailVO> transactionDetailVOMap) {
		this.transactionDetailVOMap = transactionDetailVOMap;
	}

	public Map<String, List<TransactionDetailVO>> getTransactionDetailVOGroup() {
		return transactionDetailVOGroup;
	}

	public void setTransactionDetailVOGroup(
			Map<String, List<TransactionDetailVO>> transactionDetailVOGroup) {
		this.transactionDetailVOGroup = transactionDetailVOGroup;
	}

	public List<TransactionDetailVO> getTransactionDetailVOs() {
		return transactionDetailVOs;
	}

	public void setTransactionDetailVOs(
			List<TransactionDetailVO> transactionDetailVOs) {
		this.transactionDetailVOs = transactionDetailVOs;
	} 
}
