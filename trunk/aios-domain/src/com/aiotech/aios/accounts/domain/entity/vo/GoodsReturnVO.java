package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.GoodsReturn;

public class GoodsReturnVO extends GoodsReturn implements java.io.Serializable {
	
	private String supplierName;
	private String supplierNumber;
	private String goodsReturnDate;
	private String grnNumber;
 	private long supplierId;
	private List<GoodsReturnDetailVO> goodsReturnDetailVOs;
	private boolean receiveFlag;
	private String purchaseNumber;
	private String purchaseDate;
	private String displayTotalAmount;
	private String displayTotalAmountInWords;

	public GoodsReturnVO() {
		
	}
	
	public GoodsReturnVO(GoodsReturn goodsReturn) {
		this.setGoodsReturnId(goodsReturn.getGoodsReturnId());
 		this.setSupplier(goodsReturn.getSupplier());
		this.setReturnNumber(goodsReturn.getReturnNumber());
		this.setDate(goodsReturn.getDate());
		this.setDebits(goodsReturn.getDebits());
		this.setGoodsReturnDetails(goodsReturn.getGoodsReturnDetails());
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(String supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public List<GoodsReturnDetailVO> getGoodsReturnDetailVOs() {
		return goodsReturnDetailVOs;
	}

	public void setGoodsReturnDetailVOs(
			List<GoodsReturnDetailVO> goodsReturnDetailVOs) {
		this.goodsReturnDetailVOs = goodsReturnDetailVOs;
	}

	public String getGoodsReturnDate() {
		return goodsReturnDate;
	}

	public void setGoodsReturnDate(String goodsReturnDate) {
		this.goodsReturnDate = goodsReturnDate;
	}

	public String getGrnNumber() {
		return grnNumber;
	}

	public void setGrnNumber(String grnNumber) {
		this.grnNumber = grnNumber;
	} 

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	public boolean isReceiveFlag() {
		return receiveFlag;
	}

	public void setReceiveFlag(boolean receiveFlag) {
		this.receiveFlag = receiveFlag;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}

	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}

	public String getDisplayTotalAmountInWords() {
		return displayTotalAmountInWords;
	}

	public void setDisplayTotalAmountInWords(String displayTotalAmountInWords) {
		this.displayTotalAmountInWords = displayTotalAmountInWords;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

}
