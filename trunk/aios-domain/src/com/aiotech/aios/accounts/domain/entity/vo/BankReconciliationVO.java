package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankReconciliation;

public class BankReconciliationVO extends BankReconciliation implements
		java.io.Serializable {

	private String accountBalanceStr;
	private String statementBalanceStr;
	private boolean accountSide;
	private String receiptTotal;
	private String paymentTotal;
	private String differenceTotal;
	private String startDate;
	private String endDate;
	private String transactionDateStr;
	private String bankName;
	private String bankAccountNumber;
	private Long bankAccountId;
	private List<BankReconciliationDetailVO> bankReconciliationDetailVOs = new ArrayList<BankReconciliationDetailVO>();

	public String getAccountBalanceStr() {
		return accountBalanceStr;
	}

	public void setAccountBalanceStr(String accountBalanceStr) {
		this.accountBalanceStr = accountBalanceStr;
	}

	public boolean isAccountSide() {
		return accountSide;
	}

	public void setAccountSide(boolean accountSide) {
		this.accountSide = accountSide;
	}

	public List<BankReconciliationDetailVO> getBankReconciliationDetailVOs() {
		return bankReconciliationDetailVOs;
	}

	public void setBankReconciliationDetailVOs(
			List<BankReconciliationDetailVO> bankReconciliationDetailVOs) {
		this.bankReconciliationDetailVOs = bankReconciliationDetailVOs;
	}

	public String getStatementBalanceStr() {
		return statementBalanceStr;
	}

	public void setStatementBalanceStr(String statementBalanceStr) {
		this.statementBalanceStr = statementBalanceStr;
	}

	public String getReceiptTotal() {
		return receiptTotal;
	}

	public void setReceiptTotal(String receiptTotal) {
		this.receiptTotal = receiptTotal;
	}

	public String getPaymentTotal() {
		return paymentTotal;
	}

	public void setPaymentTotal(String paymentTotal) {
		this.paymentTotal = paymentTotal;
	}

	public String getDifferenceTotal() {
		return differenceTotal;
	}

	public void setDifferenceTotal(String differenceTotal) {
		this.differenceTotal = differenceTotal;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTransactionDateStr() {
		return transactionDateStr;
	}

	public void setTransactionDateStr(String transactionDateStr) {
		this.transactionDateStr = transactionDateStr;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public Long getBankAccountId() {
		return bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
}
