package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryNote;


public class SalesDeliveryNoteVO extends SalesDeliveryNote implements java.io.Serializable {

	private String customerName;
	private String salesPerson;
	private String unitName;
	private String deliveryStatus;
	private String dispatchDate;
	private Double totalDeliveryAmount;
	private Double totalDeliveryQty;
	private Double addtionalCharges;
	private Long customerId;
	private Long currencyId;
	private Long productId;
	private String currencyCode;
	private String customerNameAndNo;
	private String shippingMethod;
	private String shippingTerm;
	private String strStatus;
	private String expiryDateFormat;
	private String shippingDateFormat;
	private Boolean deliveryFlag;
	private List<SalesDeliveryDetailVO>  salesDeliveryDetailVOs = new ArrayList<SalesDeliveryDetailVO>();
	private List<SalesDeliveryChargeVO> salesDeliveryChargeVOs = new ArrayList<SalesDeliveryChargeVO>();
	private List<SalesDeliveryPackVO> salesDeliveryPackVOs = new ArrayList<SalesDeliveryPackVO>();
	private boolean editFlag;
	private Long alertId;
	private String poBox;
	private String address;
	private String telephone;
	private String fax;
	private String mobile;
	private CustomerVO customerVO;
	private String amountInWords;
	private String creditTermName;
	private String productName;
	private String storeName;
	private String displayAmount;
	private Boolean salesCharge;
	private String date;
	private Double totalQuantity;
	private String salesAmount;
	private String customerDiscount;
	private String totalDue;
	
	public SalesDeliveryNoteVO() {
		
	}
	
	public SalesDeliveryNoteVO(SalesDeliveryNote salesDeliveryNote) {
		this.setSalesDeliveryNoteId(salesDeliveryNote.getSalesDeliveryNoteId());
		this.setCurrency(salesDeliveryNote.getCurrency());
		this.setPersonBySalesPersonId(salesDeliveryNote.getPersonBySalesPersonId());
		this.setImplementation(salesDeliveryNote.getImplementation());
		this.setCustomer(salesDeliveryNote.getCustomer());
		this.setLookupDetailByShippingTerm(salesDeliveryNote.getLookupDetailByShippingTerm());
		this.setPersonByCreatedBy(salesDeliveryNote.getPersonByCreatedBy());
		this.setSalesOrder(salesDeliveryNote.getSalesOrder());
		this.setShippingDetail(salesDeliveryNote.getShippingDetail());
		this.setLookupDetailByShippingMethod(salesDeliveryNote.getLookupDetailByShippingMethod());
		this.setReferenceNumber(salesDeliveryNote.getReferenceNumber());
		this.setDeliveryDate(salesDeliveryNote.getDeliveryDate());
		this.setModeOfPayment(salesDeliveryNote.getModeOfPayment());
		this.setStatus(salesDeliveryNote.getStatus());
		this.setDescription(salesDeliveryNote.getDescription());
		this.setStatusNote(salesDeliveryNote.getStatusNote());
		this.setSalesDeliveryCharges(salesDeliveryNote.getSalesDeliveryCharges());
		this.setSalesDeliveryDetails(salesDeliveryNote.getSalesDeliveryDetails());
		this.setSalesDeliveryPacks(salesDeliveryNote.getSalesDeliveryPacks());
	}
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getDispatchDate() {
		return dispatchDate;
	}

	public void setDispatchDate(String dispatchDate) {
		this.dispatchDate = dispatchDate;
	}

	public List<SalesDeliveryDetailVO> getSalesDeliveryDetailVOs() {
		return salesDeliveryDetailVOs;
	}

	public void setSalesDeliveryDetailVOs(
			List<SalesDeliveryDetailVO> salesDeliveryDetailVOs) {
		this.salesDeliveryDetailVOs = salesDeliveryDetailVOs;
	}

	public Double getTotalDeliveryAmount() {
		return totalDeliveryAmount;
	}

	public void setTotalDeliveryAmount(Double totalDeliveryAmount) {
		this.totalDeliveryAmount = totalDeliveryAmount;
	}

	public Double getTotalDeliveryQty() {
		return totalDeliveryQty;
	}

	public void setTotalDeliveryQty(Double totalDeliveryQty) {
		this.totalDeliveryQty = totalDeliveryQty;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCustomerNameAndNo() {
		return customerNameAndNo;
	}

	public void setCustomerNameAndNo(String customerNameAndNo) {
		this.customerNameAndNo = customerNameAndNo;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(String shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

	public String getExpiryDateFormat() {
		return expiryDateFormat;
	}

	public void setExpiryDateFormat(String expiryDateFormat) {
		this.expiryDateFormat = expiryDateFormat;
	}

	public String getShippingDateFormat() {
		return shippingDateFormat;
	}

	public void setShippingDateFormat(String shippingDateFormat) {
		this.shippingDateFormat = shippingDateFormat;
	}

	public List<SalesDeliveryChargeVO> getSalesDeliveryChargeVOs() {
		return salesDeliveryChargeVOs;
	}

	public void setSalesDeliveryChargeVOs(
			List<SalesDeliveryChargeVO> salesDeliveryChargeVOs) {
		this.salesDeliveryChargeVOs = salesDeliveryChargeVOs;
	}

	public List<SalesDeliveryPackVO> getSalesDeliveryPackVOs() {
		return salesDeliveryPackVOs;
	}

	public void setSalesDeliveryPackVOs(
			List<SalesDeliveryPackVO> salesDeliveryPackVOs) {
		this.salesDeliveryPackVOs = salesDeliveryPackVOs;
	}

	public Double getAddtionalCharges() {
		return addtionalCharges;
	}

	public void setAddtionalCharges(Double addtionalCharges) {
		this.addtionalCharges = addtionalCharges;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Boolean getDeliveryFlag() {
		return deliveryFlag;
	}

	public void setDeliveryFlag(Boolean deliveryFlag) {
		this.deliveryFlag = deliveryFlag;
	}

	public boolean isEditFlag() {
		return editFlag;
	}

	public void setEditFlag(boolean editFlag) {
		this.editFlag = editFlag;
	}

	public Long getAlertId() {
		return alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public CustomerVO getCustomerVO() {
		return customerVO;
	}

	public void setCustomerVO(CustomerVO customerVO) {
		this.customerVO = customerVO;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getAmountInWords() {
		return amountInWords;
	}

	public void setAmountInWords(String amountInWords) {
		this.amountInWords = amountInWords;
	}

	public String getCreditTermName() {
		return creditTermName;
	}

	public void setCreditTermName(String creditTermName) {
		this.creditTermName = creditTermName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getDisplayAmount() {
		return displayAmount;
	}

	public void setDisplayAmount(String displayAmount) {
		this.displayAmount = displayAmount;
	}

	public Boolean getSalesCharge() {
		return salesCharge;
	}

	public void setSalesCharge(Boolean salesCharge) {
		this.salesCharge = salesCharge;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(String salesAmount) {
		this.salesAmount = salesAmount;
	}

	public String getCustomerDiscount() {
		return customerDiscount;
	}

	public void setCustomerDiscount(String customerDiscount) {
		this.customerDiscount = customerDiscount;
	}

	public String getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(String totalDue) {
		this.totalDue = totalDue;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
}
