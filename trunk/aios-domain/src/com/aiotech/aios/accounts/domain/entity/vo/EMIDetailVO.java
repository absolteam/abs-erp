package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.EmiDetail;

public class EMIDetailVO extends EmiDetail{
	private String emiDueDate;
	private String paymentFor;
	public String getEmiDueDate() {
		return emiDueDate;
	} 
	public void setEmiDueDate(String emiDueDate) {
		this.emiDueDate = emiDueDate;
	}

	public String getPaymentFor() {
		return paymentFor;
	}

	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}
}
