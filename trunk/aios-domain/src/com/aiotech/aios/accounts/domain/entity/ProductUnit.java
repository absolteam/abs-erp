package com.aiotech.aios.accounts.domain.entity;

// Generated Feb 22, 2015 9:08:14 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ProductUnit generated by hbm2java
 */
@Entity
@Table(name = "ac_product_unit", catalog = "aios")
public class ProductUnit implements java.io.Serializable {

	private Integer unitId;
 	private String unitName;
	 

	public ProductUnit() {
	}

	public ProductUnit(String unitName) {
		this.unitName = unitName;
	} 

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "unit_id", unique = true, nullable = false)
	public Integer getUnitId() {
		return this.unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	} 

	@Column(name = "unit_name", nullable = false, length = 50)
	public String getUnitName() {
		return this.unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	 
}
