package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetService;

public class AssetServiceVO extends AssetService {

	private String assetName;
	private String assetServiceType;
	private String assetServiceLevel;
	private String serviceInCharge;
	
	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	public String getAssetServiceType() {
		return assetServiceType;
	}

	public void setAssetServiceType(String assetServiceType) {
		this.assetServiceType = assetServiceType;
	}

	public String getAssetServiceLevel() {
		return assetServiceLevel;
	}

	public void setAssetServiceLevel(String assetServiceLevel) {
		this.assetServiceLevel = assetServiceLevel;
	}

	public String getServiceInCharge() {
		return serviceInCharge;
	}

	public void setServiceInCharge(String serviceInCharge) {
		this.serviceInCharge = serviceInCharge;
	}
}
