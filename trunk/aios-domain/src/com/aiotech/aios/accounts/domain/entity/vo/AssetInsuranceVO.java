package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.AssetInsurance;

public class AssetInsuranceVO extends AssetInsurance {

	private String insurancePeriodName;
	private String fromDate;
	private String toDate;
	private String insuranceType;
	private String provider;
	private String policyType;
	private String assetName;
	
	public String getInsurancePeriodName() {
		return insurancePeriodName;
	}

	public void setInsurancePeriodName(String insurancePeriodName) {
		this.insurancePeriodName = insurancePeriodName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getAssetName() {
		return assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
}
