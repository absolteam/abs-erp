package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.ProductPackageDetail;

public class ProductPackageDetailVO extends ProductPackageDetail implements java.io.Serializable {

	private String conversionUnitName;
	private String conversionUnitMeasure;
	private String conversionQuantity;
	private Double basePrice;
	
	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getConversionQuantity() {
		return conversionQuantity;
	}

	public void setConversionQuantity(String conversionQuantity) {
		this.conversionQuantity = conversionQuantity;
	}

	public Double getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(Double basePrice) {
		this.basePrice = basePrice;
	}

	public String getConversionUnitMeasure() {
		return conversionUnitMeasure;
	}

	public void setConversionUnitMeasure(String conversionUnitMeasure) {
		this.conversionUnitMeasure = conversionUnitMeasure;
	}
}
