package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.BankDepositDetail;

public class BankDepositDetailVO extends BankDepositDetail {
	
	private String receiptTypeStr;
	private String bankName;
	private String accountNo;
	private String receiptSourceType;
	private String receiptSourceName;
	
	public BankDepositDetailVO() {
		
	}
	
	public BankDepositDetailVO(BankDepositDetail bankDepositDetail) {
		this.setBankDepositDetailId(bankDepositDetail.getBankDepositDetailId());
		this.setBankDeposit(bankDepositDetail.getBankDeposit());
 		this.setBankReceiptsDetail(bankDepositDetail.getBankReceiptsDetail());
		this.setAmount(bankDepositDetail.getAmount());
		this.setDescription(bankDepositDetail.getDescription());
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getReceiptSourceType() {
		return receiptSourceType;
	}

	public void setReceiptSourceType(String receiptSourceType) {
		this.receiptSourceType = receiptSourceType;
	}

	public String getReceiptSourceName() {
		return receiptSourceName;
	}

	public void setReceiptSourceName(String receiptSourceName) {
		this.receiptSourceName = receiptSourceName;
	}

	public String getReceiptTypeStr() {
		return receiptTypeStr;
	}

	public void setReceiptTypeStr(String receiptTypeStr) {
		this.receiptTypeStr = receiptTypeStr;
	}
	
}
