package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.PaymentRequest;

public class PaymentRequestVO extends PaymentRequest implements java.io.Serializable {

	private String date;
	private String createdBy;
	private String requestedBy;
	private List<PaymentRequestDetailVO> paymentRequestDetailVOs = new ArrayList<PaymentRequestDetailVO>();
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<PaymentRequestDetailVO> getPaymentRequestDetailVOs() {
		return paymentRequestDetailVOs;
	}

	public void setPaymentRequestDetailVOs(
			List<PaymentRequestDetailVO> paymentRequestDetailVOs) {
		this.paymentRequestDetailVOs = paymentRequestDetailVOs;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}
}
