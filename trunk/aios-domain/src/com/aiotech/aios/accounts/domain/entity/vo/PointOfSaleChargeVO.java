package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PointOfSaleCharge;

public class PointOfSaleChargeVO extends PointOfSaleCharge {

	private String chargeType;
	
	public PointOfSaleChargeVO(){
	}
	
	public PointOfSaleChargeVO(PointOfSaleCharge pointOfSaleCharge) {
		this.setPointOfSaleChargeId(pointOfSaleCharge.getPointOfSaleChargeId());
		this.setPointOfSale(pointOfSaleCharge.getPointOfSale());
		this.setCharges(pointOfSaleCharge.getCharges());
		this.setLookupDetail(pointOfSaleCharge.getLookupDetail());
	}

	public String getChargeType() {
		return chargeType;
	}

	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	
}
