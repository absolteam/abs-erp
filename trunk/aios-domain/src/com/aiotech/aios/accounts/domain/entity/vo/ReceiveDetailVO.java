package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ReceiveDetail;

public class ReceiveDetailVO extends ReceiveDetail implements
		java.io.Serializable {

	private Double invoicedQty;
	private Double invoiceQty;
	private Double totalRate;
	private Double receivedQty;
	private Double selectedInvQty;
	private Double returnedQty;
	private Double editReceiveQty;
	private Double editReturnQty;
	private Long invoiceDetailId;
	private Long packageDetailId;
	private Long purchaseId;
	private String unitCodeStr;
	private String conversionUnitName;
	private String baseUnitName;
	private String productCode;
	private String productName;
	private String itemType;
	private String storeName;
	private String referenceNumber;
	private String supplierName;
	private String receiveDate;
	private boolean invoicedFlag;
	private ReceiveVO receiveVO;
	private String displayUnitRate;
	private String displayTotalAmount;
	private String displayQuantity;
	private Double orderedQty;
	private String purchaseNumber;
	private String section;
	private String baseQuantity;
	private Double pendingQuantity;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public ReceiveDetailVO() {

	}

	public ReceiveDetailVO(ReceiveDetail receiveDetail) {
		this.setReceiveDetailId(receiveDetail.getReceiveDetailId());
		this.setShelf(receiveDetail.getShelf());
		this.setProduct(receiveDetail.getProduct());
		this.setReceive(receiveDetail.getReceive());
		this.setPurchase(receiveDetail.getPurchase());
		this.setReceiveQty(receiveDetail.getReceiveQty());
		this.setUnitRate(receiveDetail.getUnitRate());
		this.setDescription(receiveDetail.getDescription());
		this.setInvoiceDetails(receiveDetail.getInvoiceDetails());
	}

	public Double getInvoicedQty() {
		return invoicedQty;
	}

	public void setInvoicedQty(Double invoicedQty) {
		this.invoicedQty = invoicedQty;
	}

	public Double getInvoiceQty() {
		return invoiceQty;
	}

	public void setInvoiceQty(Double invoiceQty) {
		this.invoiceQty = invoiceQty;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Long getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(Long invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public Double getSelectedInvQty() {
		return selectedInvQty;
	}

	public void setSelectedInvQty(Double selectedInvQty) {
		this.selectedInvQty = selectedInvQty;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Double getReturnedQty() {
		return returnedQty;
	}

	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public boolean isInvoicedFlag() {
		return invoicedFlag;
	}

	public void setInvoicedFlag(boolean invoicedFlag) {
		this.invoicedFlag = invoicedFlag;
	}

	public ReceiveVO getReceiveVO() {
		return receiveVO;
	}

	public void setReceiveVO(ReceiveVO receiveVO) {
		this.receiveVO = receiveVO;
	}

	public String getDisplayUnitRate() {
		return displayUnitRate;
	}

	public void setDisplayUnitRate(String displayUnitRate) {
		this.displayUnitRate = displayUnitRate;
	}

	public String getDisplayTotalAmount() {
		return displayTotalAmount;
	}

	public void setDisplayTotalAmount(String displayTotalAmount) {
		this.displayTotalAmount = displayTotalAmount;
	}

	public String getDisplayQuantity() {
		return displayQuantity;
	}

	public void setDisplayQuantity(String displayQuantity) {
		this.displayQuantity = displayQuantity;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public Double getOrderedQty() {
		return orderedQty;
	}

	public void setOrderedQty(Double orderedQty) {
		this.orderedQty = orderedQty;
	}

	public String getPurchaseNumber() {
		return purchaseNumber;
	}

	public void setPurchaseNumber(String purchaseNumber) {
		this.purchaseNumber = purchaseNumber;
	}

	public Double getEditReceiveQty() {
		return editReceiveQty;
	}

	public void setEditReceiveQty(Double editReceiveQty) {
		this.editReceiveQty = editReceiveQty;
	}

	public Double getEditReturnQty() {
		return editReturnQty;
	}

	public void setEditReturnQty(Double editReturnQty) {
		this.editReturnQty = editReturnQty;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public Long getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(Long purchaseId) {
		this.purchaseId = purchaseId;
	}


	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}


	public Double getPendingQuantity() {
		return pendingQuantity;
	}

	public void setPendingQuantity(Double pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}

	public String getUnitCodeStr() {
		return unitCodeStr;
	}

	public void setUnitCodeStr(String unitCodeStr) {
		this.unitCodeStr = unitCodeStr;
	}
}
