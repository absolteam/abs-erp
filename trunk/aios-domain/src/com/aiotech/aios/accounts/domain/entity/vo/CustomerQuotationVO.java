package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.CustomerQuotation;

public class CustomerQuotationVO extends CustomerQuotation {

	private String orderDateFormat;
	private String expiryDateFormat;
	private String shippingDateFormat;
	private String customerName;
	private String salesRep;
	private String customerNameAndNo;
	private String shippingMethod;
	private String shippingTerm;
	private String strStatus;
	private Long creditTermId;
	
	private List<CustomerQuotationDetailVO> customerQuotationDetailVOs;
	private List<CustomerQuotationChargeVO> customerQuotationChargeVOs;
	
	public CustomerQuotationVO() {
		
	}
	
	public CustomerQuotationVO(CustomerQuotation customerQuotation) {
		this.setCustomerQuotationId(customerQuotation.getCustomerQuotationId());
		this.setImplementation(customerQuotation.getImplementation());
		this.setCustomer(customerQuotation.getCustomer());
		this.setLookupDetailByShippingTerm(customerQuotation.getLookupDetailByShippingTerm());
		this.setPerson(customerQuotation.getPerson());
		this.setLookupDetailByShippingMethod(customerQuotation.getLookupDetailByShippingMethod());
		this.setReferenceNo(customerQuotation.getReferenceNo());
		this.setDate(customerQuotation.getDate());
		this.setExpiryDate(customerQuotation.getExpiryDate());
		this.setShippingDate(customerQuotation.getShippingDate());
		this.setModeOfPayment(customerQuotation.getModeOfPayment());
		this.setCustomerMessage(customerQuotation.getCustomerMessage());
		this.setStatus(customerQuotation.getStatus());
		this.setDescription(customerQuotation.getDescription());
		this.setCustomerQuotationDetails(customerQuotation.getCustomerQuotationDetails());
		this.setSalesOrders(customerQuotation.getSalesOrders());
		this.setCustomerQuotationCharges(customerQuotation.getCustomerQuotationCharges());
		
	}
	
	public String getOrderDateFormat() {
		return orderDateFormat;
	}

	public void setOrderDateFormat(String orderDateFormat) {
		this.orderDateFormat = orderDateFormat;
	}

	public String getExpiryDateFormat() {
		return expiryDateFormat;
	}

	public void setExpiryDateFormat(String expiryDateFormat) {
		this.expiryDateFormat = expiryDateFormat;
	}

	public String getShippingDateFormat() {
		return shippingDateFormat;
	}

	public void setShippingDateFormat(String shippingDateFormat) {
		this.shippingDateFormat = shippingDateFormat;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSalesRep() {
		return salesRep;
	}

	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}

	public String getCustomerNameAndNo() {
		return customerNameAndNo;
	}

	public void setCustomerNameAndNo(String customerNameAndNo) {
		this.customerNameAndNo = customerNameAndNo;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingTerm() {
		return shippingTerm;
	}

	public void setShippingTerm(String shippingTerm) {
		this.shippingTerm = shippingTerm;
	}

	public String getStrStatus() {
		return strStatus;
	}

	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

	public List<CustomerQuotationDetailVO> getCustomerQuotationDetailVOs() {
		return customerQuotationDetailVOs;
	}

	public void setCustomerQuotationDetailVOs(
			List<CustomerQuotationDetailVO> customerQuotationDetailVOs) {
		this.customerQuotationDetailVOs = customerQuotationDetailVOs;
	}

	public List<CustomerQuotationChargeVO> getCustomerQuotationChargeVOs() {
		return customerQuotationChargeVOs;
	}

	public void setCustomerQuotationChargeVOs(
			List<CustomerQuotationChargeVO> customerQuotationChargeVOs) {
		this.customerQuotationChargeVOs = customerQuotationChargeVOs;
	}

	public Long getCreditTermId() {
		return creditTermId;
	}

	public void setCreditTermId(Long creditTermId) {
		this.creditTermId = creditTermId;
	}
	
}
