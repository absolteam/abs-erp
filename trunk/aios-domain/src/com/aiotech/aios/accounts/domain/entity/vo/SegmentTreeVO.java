package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

public class SegmentTreeVO {

	private String title;
	private Long key;
	private List<SegmentTreeVO> children;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getKey() {
		return key;
	}
	public void setKey(Long key) {
		this.key = key;
	}
	public List<SegmentTreeVO> getChildren() {
		return children;
	}
	public void setChildren(List<SegmentTreeVO> children) {
		this.children = children;
	}
}
