package com.aiotech.aios.accounts.domain.entity;

// Generated Jul 16, 2014 11:00:49 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * PromotionMethod generated by hbm2java
 */
@Entity
@Table(name = "ac_promotion_method", catalog = "aios")
public class PromotionMethod implements java.io.Serializable {

	private Long promotionMethodId;
	private Promotion promotion;
	private Coupon coupon;
	private Byte pacType;
	private Character calculationType;
	private Double promotionAmount;
	private Double productPoint;
	private String description;
	private Set<PointOfSaleDetail> pointOfSaleDetails = new HashSet<PointOfSaleDetail>(
			0);
	private Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>(0);

	public PromotionMethod() {
	}

	public PromotionMethod(Promotion promotion) {
		this.promotion = promotion;
	}

	public PromotionMethod(Promotion promotion, Coupon coupon, Byte pacType,
			Character calculationType, Double promotionAmount,
			Double productPoint, String description,
			Set<PointOfSaleDetail> pointOfSaleDetails,
			Set<PointOfSale> pointOfSales) {
		this.promotion = promotion;
		this.coupon = coupon;
		this.pacType = pacType;
		this.calculationType = calculationType;
		this.promotionAmount = promotionAmount;
		this.productPoint = productPoint;
		this.description = description;
		this.pointOfSaleDetails = pointOfSaleDetails;
		this.pointOfSales = pointOfSales;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "promotion_method_id", unique = true, nullable = false)
	public Long getPromotionMethodId() {
		return this.promotionMethodId;
	}

	public void setPromotionMethodId(Long promotionMethodId) {
		this.promotionMethodId = promotionMethodId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "promotion_id", nullable = false)
	public Promotion getPromotion() {
		return this.promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coupon_id")
	public Coupon getCoupon() {
		return this.coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	@Column(name = "pac_type")
	public Byte getPacType() {
		return this.pacType;
	}

	public void setPacType(Byte pacType) {
		this.pacType = pacType;
	}

	@Column(name = "calculation_type", length = 1)
	public Character getCalculationType() {
		return this.calculationType;
	}

	public void setCalculationType(Character calculationType) {
		this.calculationType = calculationType;
	}

	@Column(name = "promotion_amount", precision = 22, scale = 0)
	public Double getPromotionAmount() {
		return this.promotionAmount;
	}

	public void setPromotionAmount(Double promotionAmount) {
		this.promotionAmount = promotionAmount;
	}

	@Column(name = "product_point", precision = 22, scale = 0)
	public Double getProductPoint() {
		return this.productPoint;
	}

	public void setProductPoint(Double productPoint) {
		this.productPoint = productPoint;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "promotionMethod")
	public Set<PointOfSaleDetail> getPointOfSaleDetails() {
		return this.pointOfSaleDetails;
	}

	public void setPointOfSaleDetails(Set<PointOfSaleDetail> pointOfSaleDetails) {
		this.pointOfSaleDetails = pointOfSaleDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "promotionMethod")
	public Set<PointOfSale> getPointOfSales() {
		return this.pointOfSales;
	}

	public void setPointOfSales(Set<PointOfSale> pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

}
