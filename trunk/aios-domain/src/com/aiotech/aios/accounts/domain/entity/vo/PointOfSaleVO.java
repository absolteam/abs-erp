package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.aiotech.aios.accounts.domain.entity.PointOfSale;
@XmlRootElement
public class PointOfSaleVO extends PointOfSale implements java.io.Serializable {

	private String salesAmount;
	private String additionalCost;
	private String totalDue;
	private String cashReceived;
	private String cardReceived;
	private String chequeReceived;
	private String couponReceived;
	private String exchangeReceived;
	private String receivedTotal;
	private String balanceDue;
  	private String receiptType;
  	private int receiptQuantity;
  	private byte currencyMode;
  	private long couponId;
  	private long rewardPolicyId;
  	private int promotionMethodId;
  	private int discountMethodId;
  	private int couponNumber;
  	private long customerId;
   	private double offerAmount;
  	private String outStanding;
  	private String customerDiscount;
  	private String customerName;
  	private String employeeName;
  	private String salesType;
  	private String date;
  	private String transactionNumber;
  	private Double totalQuantity;
  	private Double totalAmount;
  	private Double grandTotal;
  	private Double consolidatedSales;
  	private String dayAnalysis;
  	private String weekAnalysis;
  	private String monthAnalysis;
  	private String posUserName;
  	private String storeName;
  	private String customerNumber;
  	private String customerMobile;
  	private String customerLocation;
  	private String useCaseId;
  	private String posSalesDate;
  	private long customerRecordId;
  	private String customerType;
  	private double deliveryCharges;
  	private List<PointOfSaleDetailVO> pointOfSaleDetailVOs = new ArrayList<PointOfSaleDetailVO>();
  	private List<PointOfSaleChargeVO> pointOfSaleChargeVOs = new ArrayList<PointOfSaleChargeVO>();
  	private List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs = new ArrayList<PointOfSaleReceiptVO>();
	private List<ProductVO> productVOs = new ArrayList<ProductVO>();
	private List<ProductCategoryVO> productCategoryVOs = new ArrayList<ProductCategoryVO>();
	private List<CreditDetailVO> creditDetailVOs = new ArrayList<CreditDetailVO>();
  	private Long deliveryOption;
   	private Long shippingId;
  	private String shippingAddress;
   	private String tillNumber;
  	private String fromDateView;
  	private String toDateView;
  	private String categoryName;
  	private String printerGroup;
  	private String viewType;
  	private Date fromDate;
  	private Date toDate;
  	private Integer storeId;
  	private Long tillId;
  	private Long personId;
  	private String quotation;
  	private String salesReceiptNumber;
  	private String productName;
  	private Long productId;
  	private Double discount;
  	private boolean posDetail;
  	private boolean posCharge;
  	private boolean posReceipt;
   	private List<PointOfSale> pointOfSales;
  	public PointOfSaleVO() {
  	}
  	
  	public PointOfSaleVO(PointOfSale pointOfSale) {
  		this.setPointOfSaleId(pointOfSale.getPointOfSaleId());
  		this.setPOSUserTill(pointOfSale.getPOSUserTill());
  		this.setImplementation(pointOfSale.getImplementation());
  		this.setCustomer(pointOfSale.getCustomer()); 
  		this.setReferenceNumber(pointOfSale.getReferenceNumber());
  		this.setSalesDate(pointOfSale.getSalesDate());
  		this.setIsEodBalanced(pointOfSale.getIsEodBalanced());
  		this.setPointOfSaleReceipts(pointOfSale.getPointOfSaleReceipts());
  		this.setPointOfSaleDetails(pointOfSale.getPointOfSaleDetails());
  		this.setPointOfSaleOffers(pointOfSale.getPointOfSaleOffers());
  		this.setPointOfSaleCharges(pointOfSale.getPointOfSaleCharges());
  	}
  	
	public String getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(String salesAmount) {
		this.salesAmount = salesAmount;
	}
	public String getAdditionalCost() {
		return additionalCost;
	}
	public void setAdditionalCost(String additionalCost) {
		this.additionalCost = additionalCost;
	}
	public String getTotalDue() {
		return totalDue;
	}
	public void setTotalDue(String totalDue) {
		this.totalDue = totalDue;
	}
	public String getCashReceived() {
		return cashReceived;
	}
	public void setCashReceived(String cashReceived) {
		this.cashReceived = cashReceived;
	}
	public String getCardReceived() {
		return cardReceived;
	}
	public void setCardReceived(String cardReceived) {
		this.cardReceived = cardReceived;
	}
	public String getChequeReceived() {
		return chequeReceived;
	}
	public void setChequeReceived(String chequeReceived) {
		this.chequeReceived = chequeReceived;
	}
	public String getCouponReceived() {
		return couponReceived;
	}
	public void setCouponReceived(String couponReceived) {
		this.couponReceived = couponReceived;
	}
	public String getReceivedTotal() {
		return receivedTotal;
	}
	public void setReceivedTotal(String receivedTotal) {
		this.receivedTotal = receivedTotal;
	}
	public String getBalanceDue() {
		return balanceDue;
	}
	public void setBalanceDue(String balanceDue) {
		this.balanceDue = balanceDue;
	}
	public String getReceiptType() {
		return receiptType;
	}
	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}
	public int getReceiptQuantity() {
		return receiptQuantity;
	}
	public void setReceiptQuantity(int receiptQuantity) {
		this.receiptQuantity = receiptQuantity;
	}
	public byte getCurrencyMode() {
		return currencyMode;
	}
	public void setCurrencyMode(byte currencyMode) {
		this.currencyMode = currencyMode;
	} 
	public int getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(int couponNumber) {
		this.couponNumber = couponNumber;
	}
	public double getOfferAmount() {
		return offerAmount;
	}
	public void setOfferAmount(double offerAmount) {
		this.offerAmount = offerAmount;
	}
	public int getPromotionMethodId() {
		return promotionMethodId;
	}
	public void setPromotionMethodId(int promotionMethodId) {
		this.promotionMethodId = promotionMethodId;
	}
	public int getDiscountMethodId() {
		return discountMethodId;
	}
	public void setDiscountMethodId(int discountMethodId) {
		this.discountMethodId = discountMethodId;
	}
	public String getCustomerDiscount() {
		return customerDiscount;
	}
	public void setCustomerDiscount(String customerDiscount) {
		this.customerDiscount = customerDiscount;
	}
	public String getOutStanding() {
		return outStanding;
	}
	public void setOutStanding(String outStanding) {
		this.outStanding = outStanding;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getSalesType() {
		return salesType;
	}
	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<PointOfSaleDetailVO> getPointOfSaleDetailVOs() {
		return pointOfSaleDetailVOs;
	}
	public void setPointOfSaleDetailVOs(
			List<PointOfSaleDetailVO> pointOfSaleDetailVOs) {
		this.pointOfSaleDetailVOs = pointOfSaleDetailVOs;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getExchangeReceived() {
		return exchangeReceived;
	}
	public void setExchangeReceived(String exchangeReceived) {
		this.exchangeReceived = exchangeReceived;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getPosUserName() {
		return posUserName;
	}

	public void setPosUserName(String posUserName) {
		this.posUserName = posUserName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public List<PointOfSaleChargeVO> getPointOfSaleChargeVOs() {
		return pointOfSaleChargeVOs;
	}

	public void setPointOfSaleChargeVOs(
			List<PointOfSaleChargeVO> pointOfSaleChargeVOs) {
		this.pointOfSaleChargeVOs = pointOfSaleChargeVOs;
	}

	

	public Long getShippingId() {
		return shippingId;
	}

	public void setShippingId(Long shippingId) {
		this.shippingId = shippingId;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}


	public Long getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(Long deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getTillNumber() {
		return tillNumber;
	}

	public void setTillNumber(String tillNumber) {
		this.tillNumber = tillNumber;
	}

	public String getFromDateView() {
		return fromDateView;
	}

	public void setFromDateView(String fromDateView) {
		this.fromDateView = fromDateView;
	}

	public String getToDateView() {
		return toDateView;
	}

	public void setToDateView(String toDateView) {
		this.toDateView = toDateView;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getPrinterGroup() {
		return printerGroup;
	}

	public void setPrinterGroup(String printerGroup) {
		this.printerGroup = printerGroup;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public List<ProductVO> getProductVOs() {
		return productVOs;
	}

	public void setProductVOs(List<ProductVO> productVOs) {
		this.productVOs = productVOs;
	}

	public long getCustomerRecordId() {
		return customerRecordId;
	}

	public void setCustomerRecordId(long customerRecordId) {
		this.customerRecordId = customerRecordId;
	} 

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public double getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(double deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	public List<ProductCategoryVO> getProductCategoryVOs() {
		return productCategoryVOs;
	}

	public void setProductCategoryVOs(List<ProductCategoryVO> productCategoryVOs) {
		this.productCategoryVOs = productCategoryVOs;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getCustomerLocation() {
		return customerLocation;
	}

	public void setCustomerLocation(String customerLocation) {
		this.customerLocation = customerLocation;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Long getTillId() {
		return tillId;
	}

	public void setTillId(Long tillId) {
		this.tillId = tillId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getQuotation() {
		return quotation;
	}

	public void setQuotation(String quotation) {
		this.quotation = quotation;
	}

	public String getSalesReceiptNumber() {
		return salesReceiptNumber;
	}

	public void setSalesReceiptNumber(String salesReceiptNumber) {
		this.salesReceiptNumber = salesReceiptNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public boolean isPosDetail() {
		return posDetail;
	}

	public void setPosDetail(boolean posDetail) {
		this.posDetail = posDetail;
	}

	public List<PointOfSaleReceiptVO> getPointOfSaleReceiptVOs() {
		return pointOfSaleReceiptVOs;
	}

	public void setPointOfSaleReceiptVOs(
			List<PointOfSaleReceiptVO> pointOfSaleReceiptVOs) {
		this.pointOfSaleReceiptVOs = pointOfSaleReceiptVOs;
	}

	public boolean isPosCharge() {
		return posCharge;
	}

	public void setPosCharge(boolean posCharge) {
		this.posCharge = posCharge;
	}

	public String getUseCaseId() {
		return useCaseId;
	}

	public void setUseCaseId(String useCaseId) {
		this.useCaseId = useCaseId;
	}

	public boolean isPosReceipt() {
		return posReceipt;
	}

	public void setPosReceipt(boolean posReceipt) {
		this.posReceipt = posReceipt;
	}

	public List<CreditDetailVO> getCreditDetailVOs() {
		return creditDetailVOs;
	}

	public void setCreditDetailVOs(List<CreditDetailVO> creditDetailVOs) {
		this.creditDetailVOs = creditDetailVOs;
	}

	public List<PointOfSale> getPointOfSales() {
		return pointOfSales;
	}

	public void setPointOfSales(List<PointOfSale> pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	public String getPosSalesDate() {
		return posSalesDate;
	}

	public void setPosSalesDate(String posSalesDate) {
		this.posSalesDate = posSalesDate;
	}

	public long getCouponId() {
		return couponId;
	}

	public void setCouponId(long couponId) {
		this.couponId = couponId;
	}

	public long getRewardPolicyId() {
		return rewardPolicyId;
	}

	public void setRewardPolicyId(long rewardPolicyId) {
		this.rewardPolicyId = rewardPolicyId;
	}

	public Double getConsolidatedSales() {
		return consolidatedSales;
	}

	public void setConsolidatedSales(Double consolidatedSales) {
		this.consolidatedSales = consolidatedSales;
	}

	public String getDayAnalysis() {
		return dayAnalysis;
	}

	public void setDayAnalysis(String dayAnalysis) {
		this.dayAnalysis = dayAnalysis;
	}

	public String getWeekAnalysis() {
		return weekAnalysis;
	}

	public void setWeekAnalysis(String weekAnalysis) {
		this.weekAnalysis = weekAnalysis;
	}

	public String getMonthAnalysis() {
		return monthAnalysis;
	}

	public void setMonthAnalysis(String monthAnalysis) {
		this.monthAnalysis = monthAnalysis;
	} 
}
