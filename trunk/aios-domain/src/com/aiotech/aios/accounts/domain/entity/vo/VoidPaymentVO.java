package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.VoidPayment;

public class VoidPaymentVO extends VoidPayment implements java.io.Serializable {

	private String voidDateStr;

	public String getVoidDateStr() {
		return voidDateStr;
	}

	public void setVoidDateStr(String voidDateStr) {
		this.voidDateStr = voidDateStr;
	}
}
