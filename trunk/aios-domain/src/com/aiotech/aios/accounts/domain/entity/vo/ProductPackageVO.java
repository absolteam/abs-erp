package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductPackage;

public class ProductPackageVO extends ProductPackage implements java.io.Serializable {

	private String productName;
	private String demension;
	private String unitName;
	private List<ProductPackageDetailVO> productPackageDetailVOs = new ArrayList<ProductPackageDetailVO>();
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public List<ProductPackageDetailVO> getProductPackageDetailVOs() {
		return productPackageDetailVOs;
	}
	public void setProductPackageDetailVOs(
			List<ProductPackageDetailVO> productPackageDetailVOs) {
		this.productPackageDetailVOs = productPackageDetailVOs;
	}
	public String getDemension() {
		return demension;
	}
	public void setDemension(String demension) {
		this.demension = demension;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
}
