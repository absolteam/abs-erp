package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankReceipts;

public class BankReceiptsVO extends BankReceipts implements java.io.Serializable{
	
	private String strReceiptsDate;
	private String receiptAmount;
	private String currencyName;
	private String receiptType;
	private String receiptSourceType;
	private String receiptSourceName; 
	private String createdPerson;
	private Long combinationId;
	private String combinationAccount;
	private List<BankReceiptsDetailVO> bankReceiptsDetailVOs; 

	public BankReceiptsVO() {
		
	}
	
	public BankReceiptsVO(BankReceipts bankReceipts) {
		this.setBankReceiptsId(bankReceipts.getBankReceiptsId());
		this.setLookupDetail(bankReceipts.getLookupDetail());
		this.setCurrency(bankReceipts.getCurrency());
		this.setImplementation(bankReceipts.getImplementation());
		this.setCustomer(bankReceipts.getCustomer());
		this.setCombination(bankReceipts.getCombination());
		this.setPersonByPersonId(bankReceipts.getPersonByPersonId());
		this.setSupplier(bankReceipts.getSupplier());
		this.setReceiptsNo(bankReceipts.getReceiptsNo());
		this.setReceiptsDate(bankReceipts.getReceiptsDate());
		this.setReferenceNo(bankReceipts.getReferenceNo());
		this.setOthers(bankReceipts.getOthers());
		this.setDescription(bankReceipts.getDescription());
		this.setPersonByCreatedBy(bankReceipts.getPersonByCreatedBy());
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public String getStrReceiptsDate() {
		return strReceiptsDate;
	}

	public void setStrReceiptsDate(String strReceiptsDate) {
		this.strReceiptsDate = strReceiptsDate;
	}

	public String getReceiptSourceType() {
		return receiptSourceType;
	}

	public void setReceiptSourceType(String receiptSourceType) {
		this.receiptSourceType = receiptSourceType;
	}

	public String getReceiptSourceName() {
		return receiptSourceName;
	}

	public void setReceiptSourceName(String receiptSourceName) {
		this.receiptSourceName = receiptSourceName;
	}

	public List<BankReceiptsDetailVO> getBankReceiptsDetailVOs() {
		return bankReceiptsDetailVOs;
	}

	public void setBankReceiptsDetailVOs(
			List<BankReceiptsDetailVO> bankReceiptsDetailVOs) {
		this.bankReceiptsDetailVOs = bankReceiptsDetailVOs;
	}

	public String getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getCreatedPerson() {
		return createdPerson;
	}

	public void setCreatedPerson(String createdPerson) {
		this.createdPerson = createdPerson;
	}

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public String getCombinationAccount() {
		return combinationAccount;
	}

	public void setCombinationAccount(String combinationAccount) {
		this.combinationAccount = combinationAccount;
	}
	
}
