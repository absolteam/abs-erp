package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PaymentDetail;

public class PaymentDetailVO extends PaymentDetail {
	
	private String productName;
	private String productCode;
	private String strItemType;
	private String invoiceReference;
	private Double invoiceQty;
	private Double receiceQty;
	private Double amount;
	private Double total;
	
	public PaymentDetailVO() {
		
	}
	
	public PaymentDetailVO(PaymentDetail paymentDetail) {
		this.setPaymentDetailId(paymentDetail.getPaymentDetailId());
		this.setProduct(paymentDetail.getProduct());
		this.setInvoiceDetail(paymentDetail.getInvoiceDetail());
		this.setPayment(paymentDetail.getPayment());
		this.setLoanCharge(paymentDetail.getLoanCharge());
		this.setCombination(paymentDetail.getCombination());
		this.setQuantity(paymentDetail.getQuantity());
		this.setUnitRate(paymentDetail.getUnitRate());
		this.setDescription(paymentDetail.getDescription());
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStrItemType() {
		return strItemType;
	}
	public void setStrItemType(String strItemType) {
		this.strItemType = strItemType;
	}
	public String getInvoiceReference() {
		return invoiceReference;
	}
	public void setInvoiceReference(String invoiceReference) {
		this.invoiceReference = invoiceReference;
	}

	public Double getInvoiceQty() {
		return invoiceQty;
	}

	public void setInvoiceQty(Double invoiceQty) {
		this.invoiceQty = invoiceQty;
	}

	public Double getReceiceQty() {
		return receiceQty;
	}

	public void setReceiceQty(Double receiceQty) {
		this.receiceQty = receiceQty;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
}
