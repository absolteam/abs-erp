package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PromotionMethod;

public class PromotionMethodVO extends PromotionMethod {
	
	private String promotionTypeName;
	private String calculationTypeName;
	private String couponName;
	

	public PromotionMethodVO() {
	}
	
	public PromotionMethodVO(PromotionMethod promotionMethod) {
		this.setPromotionMethodId(promotionMethod.getPromotionMethodId());
		this.setPromotion(promotionMethod.getPromotion());
		this.setCoupon(promotionMethod.getCoupon());
		this.setPacType(promotionMethod.getPacType());
		this.setCalculationType(promotionMethod.getCalculationType());
		this.setPromotionAmount(promotionMethod.getPromotionAmount());
		this.setProductPoint(promotionMethod.getProductPoint());
		this.setDescription(promotionMethod.getDescription());
		this.setPointOfSaleDetails(promotionMethod.getPointOfSaleDetails());
	}

	public String getPromotionTypeName() {
		return promotionTypeName;
	}

	public void setPromotionTypeName(String promotionTypeName) {
		this.promotionTypeName = promotionTypeName;
	}

	public String getCalculationTypeName() {
		return calculationTypeName;
	}

	public void setCalculationTypeName(String calculationTypeName) {
		this.calculationTypeName = calculationTypeName;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

}
