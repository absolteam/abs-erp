package com.aiotech.aios.accounts.domain.entity;

// Generated Aug 18, 2015 12:10:38 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Account generated by hbm2java
 */
@Entity
@Table(name = "ac_account", catalog = "aios")
public class Account implements java.io.Serializable {

	private Long accountId;
	private LookupDetail lookupDetail;
	private Implementation implementation;
	private Segment segment;
	private Integer accountType;
	private String account;
	private String code;
	private Boolean isSystem;
	private Byte isApprove;
	private Long relativeId;
	private Set<Combination> combinationsForAnalysisAccountId = new HashSet<Combination>(
			0);
	private Set<Combination> combinationsForBuffer1AccountId = new HashSet<Combination>(
			0);
	private Set<Combination> combinationsForBuffer2AccountId = new HashSet<Combination>(
			0);
	private Set<Combination> combinationsForCostcenterAccountId = new HashSet<Combination>(
			0);
	private Set<Combination> combinationsForCompanyAccountId = new HashSet<Combination>(
			0);
	private Set<Combination> combinationsForNaturalAccountId = new HashSet<Combination>(
			0);

	public Account() {
	}

	public Account(Long accountId, Segment segment) {
		this.accountId = accountId;
		this.segment = segment;
	}

	public Account(Long accountId, LookupDetail lookupDetail,
			Implementation implementation, Segment segment,
			Integer accountType, String account, String code, Boolean isSystem,
			Byte isApprove, Long relativeId,
			Set<Combination> combinationsForAnalysisAccountId,
			Set<Combination> combinationsForBuffer1AccountId,
			Set<Combination> combinationsForBuffer2AccountId,
			Set<Combination> combinationsForCostcenterAccountId,
			Set<Combination> combinationsForCompanyAccountId,
			Set<Combination> combinationsForNaturalAccountId) {
		this.accountId = accountId;
		this.lookupDetail = lookupDetail;
		this.implementation = implementation;
		this.segment = segment;
		this.accountType = accountType;
		this.account = account;
		this.code = code;
		this.isSystem = isSystem;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
		this.combinationsForAnalysisAccountId = combinationsForAnalysisAccountId;
		this.combinationsForBuffer1AccountId = combinationsForBuffer1AccountId;
		this.combinationsForBuffer2AccountId = combinationsForBuffer2AccountId;
		this.combinationsForCostcenterAccountId = combinationsForCostcenterAccountId;
		this.combinationsForCompanyAccountId = combinationsForCompanyAccountId;
		this.combinationsForNaturalAccountId = combinationsForNaturalAccountId;
	}

	@Id
	@Column(name = "account_id", unique = true, nullable = false)
	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_type")
	public LookupDetail getLookupDetail() {
		return this.lookupDetail;
	}

	public void setLookupDetail(LookupDetail lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id")
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "segment_id", nullable = false)
	public Segment getSegment() {
		return this.segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	@Column(name = "account_type")
	public Integer getAccountType() {
		return this.accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	@Column(name = "account", length = 500)
	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	@Column(name = "code", length = 20)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "is_system")
	public Boolean getIsSystem() {
		return this.isSystem;
	}

	public void setIsSystem(Boolean isSystem) {
		this.isSystem = isSystem;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByAnalysisAccountId")
	public Set<Combination> getCombinationsForAnalysisAccountId() {
		return this.combinationsForAnalysisAccountId;
	}

	public void setCombinationsForAnalysisAccountId(
			Set<Combination> combinationsForAnalysisAccountId) {
		this.combinationsForAnalysisAccountId = combinationsForAnalysisAccountId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByBuffer1AccountId")
	public Set<Combination> getCombinationsForBuffer1AccountId() {
		return this.combinationsForBuffer1AccountId;
	}

	public void setCombinationsForBuffer1AccountId(
			Set<Combination> combinationsForBuffer1AccountId) {
		this.combinationsForBuffer1AccountId = combinationsForBuffer1AccountId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByBuffer2AccountId")
	public Set<Combination> getCombinationsForBuffer2AccountId() {
		return this.combinationsForBuffer2AccountId;
	}

	public void setCombinationsForBuffer2AccountId(
			Set<Combination> combinationsForBuffer2AccountId) {
		this.combinationsForBuffer2AccountId = combinationsForBuffer2AccountId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByCostcenterAccountId")
	public Set<Combination> getCombinationsForCostcenterAccountId() {
		return this.combinationsForCostcenterAccountId;
	}

	public void setCombinationsForCostcenterAccountId(
			Set<Combination> combinationsForCostcenterAccountId) {
		this.combinationsForCostcenterAccountId = combinationsForCostcenterAccountId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByCompanyAccountId")
	public Set<Combination> getCombinationsForCompanyAccountId() {
		return this.combinationsForCompanyAccountId;
	}

	public void setCombinationsForCompanyAccountId(
			Set<Combination> combinationsForCompanyAccountId) {
		this.combinationsForCompanyAccountId = combinationsForCompanyAccountId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "accountByNaturalAccountId")
	public Set<Combination> getCombinationsForNaturalAccountId() {
		return this.combinationsForNaturalAccountId;
	}

	public void setCombinationsForNaturalAccountId(
			Set<Combination> combinationsForNaturalAccountId) {
		this.combinationsForNaturalAccountId = combinationsForNaturalAccountId;
	}

}
