package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Combination;

public class CombinationVO extends Combination implements java.io.Serializable{
	
	private CombinationVO vo;
	private List<CombinationVO> companyVos=new ArrayList<CombinationVO>();
	private List<CombinationVO> costVos=new ArrayList<CombinationVO>();
	private List<CombinationVO> naturalVos=new ArrayList<CombinationVO>();
	private List<CombinationVO> analysisVos=new ArrayList<CombinationVO>();
	private List<CombinationVO> buffer1Vos=new ArrayList<CombinationVO>();
	private List<CombinationVO> buffer2Vos=new ArrayList<CombinationVO>();
	private Combination combination;  
	private String accountType; 

	public static List<CombinationVO> convertTOCombinationVO(List<Combination> combinations) { 
		List<CombinationVO> vos = new ArrayList<CombinationVO>();
		CombinationVO vo = null;
		for (Combination combination : combinations) { 
			vo = new CombinationVO();
			vo.setCombinationId(combination.getCombinationId());
			vo.setAccountByAnalysisAccountId(combination
					.getAccountByAnalysisAccountId());
			vo.setAccountByBuffer1AccountId(combination
					.getAccountByBuffer1AccountId());
			vo.setAccountByBuffer2AccountId(combination
					.getAccountByBuffer2AccountId());
			vo.setAccountByCompanyAccountId(combination
					.getAccountByCompanyAccountId());
			vo.setAccountByCostcenterAccountId(combination
					.getAccountByCostcenterAccountId());
			vo.setAccountByNaturalAccountId(combination
					.getAccountByNaturalAccountId());
			vos.add(vo);
		}
		return vos;
	}

	public List<CombinationVO> getCompanyVos() {
		return companyVos;
	}

	public void setCompanyVos(List<CombinationVO> companyVos) {
		this.companyVos = companyVos;
	}

	public List<CombinationVO> getCostVos() {
		return costVos;
	}

	public void setCostVos(List<CombinationVO> costVos) {
		this.costVos = costVos;
	}

	public List<CombinationVO> getNaturalVos() {
		return naturalVos;
	}

	public void setNaturalVos(List<CombinationVO> naturalVos) {
		this.naturalVos = naturalVos;
	}

	public List<CombinationVO> getAnalysisVos() {
		return analysisVos;
	}

	public void setAnalysisVos(List<CombinationVO> analysisVos) {
		this.analysisVos = analysisVos;
	}

	public List<CombinationVO> getBuffer1Vos() {
		return buffer1Vos;
	}

	public void setBuffer1Vos(List<CombinationVO> buffer1Vos) {
		this.buffer1Vos = buffer1Vos;
	}

	public List<CombinationVO> getBuffer2Vos() {
		return buffer2Vos;
	}

	public void setBuffer2Vos(List<CombinationVO> buffer2Vos) {
		this.buffer2Vos = buffer2Vos;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Combination getCombination() {
		return combination;
	}

	public void setCombination(Combination combination) {
		this.combination = combination;
	} 
	public CombinationVO getVo() {
		return vo;
	}

	public void setVo(CombinationVO vo) {
		this.vo = vo;
	}
}
