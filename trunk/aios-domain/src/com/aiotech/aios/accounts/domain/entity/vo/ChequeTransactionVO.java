package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

public class ChequeTransactionVO implements java.io.Serializable {

	private String referenceNumber;
	private String payableName;
	private String receivableName;
	private String chequeNumber;
	private String chequeDate;
	private String transactionDate;
	private String passedDateCheque;
	private String chequePayment;
	private String chequeReceive;
	private String paymentTotal;
	private String receiveTotal;
	private String transactionType;
 	private String bankName;
 	private String accountNumber;
 	private Boolean paymentCheque;
 	private List<ChequeTransactionVO> chequeTransactionVOs;
 	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	} 
	public String getChequePayment() {
		return chequePayment;
	}
	public void setChequePayment(String chequePayment) {
		this.chequePayment = chequePayment;
	}
	public String getChequeReceive() {
		return chequeReceive;
	}
	public void setChequeReceive(String chequeReceive) {
		this.chequeReceive = chequeReceive;
	}
	public String getPaymentTotal() {
		return paymentTotal;
	}
	public void setPaymentTotal(String paymentTotal) {
		this.paymentTotal = paymentTotal;
	}
	public String getReceiveTotal() {
		return receiveTotal;
	}
	public void setReceiveTotal(String receiveTotal) {
		this.receiveTotal = receiveTotal;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getPayableName() {
		return payableName;
	}
	public void setPayableName(String payableName) {
		this.payableName = payableName;
	}
	public String getReceivableName() {
		return receivableName;
	}
	public void setReceivableName(String receivableName) {
		this.receivableName = receivableName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Boolean getPaymentCheque() {
		return paymentCheque;
	}
	public void setPaymentCheque(Boolean paymentCheque) {
		this.paymentCheque = paymentCheque;
	}
	public List<ChequeTransactionVO> getChequeTransactionVOs() {
		return chequeTransactionVOs;
	}
	public void setChequeTransactionVOs(
			List<ChequeTransactionVO> chequeTransactionVOs) {
		this.chequeTransactionVOs = chequeTransactionVOs;
	}
	public String getPassedDateCheque() {
		return passedDateCheque;
	}
	public void setPassedDateCheque(String passedDateCheque) {
		this.passedDateCheque = passedDateCheque;
	} 
}
