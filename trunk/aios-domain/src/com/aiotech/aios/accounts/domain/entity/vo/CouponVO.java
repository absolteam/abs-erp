package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.Coupon;

public class CouponVO extends Coupon {

	private String couponTypeName;
	private String validFrom;
	private String validTo;
	private String statusValue;
	
	public String getCouponTypeName() {
		return couponTypeName;
	}
	public void setCouponTypeName(String couponTypeName) {
		this.couponTypeName = couponTypeName;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	public String getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	} 
}
