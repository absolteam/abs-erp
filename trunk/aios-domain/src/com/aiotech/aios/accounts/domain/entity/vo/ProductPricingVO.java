package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductPricing;

public class ProductPricingVO extends ProductPricing {

	private String pricingLevel;
	private String effictiveStartDate;
	private String effictiveEndDate;
	private String currencyCode;
	private String customers;
	private List<ProductPricingDetailVO> productPricingDetailVOs = new ArrayList<ProductPricingDetailVO>();
	private List<ProductPricingCalcVO> productPricingCalcVOs = new ArrayList<ProductPricingCalcVO>();
	
	public ProductPricingVO() {
		
	}
	
	public ProductPricingVO(ProductPricing productPricing) {
		this.setProductPricingId(productPricing.getProductPricingId());
		this.setPricingTitle(productPricing.getPricingTitle());
		this.setCurrency(productPricing.getCurrency());
		this.setImplementation(productPricing.getImplementation());
		this.setStartDate(productPricing.getStartDate());
		this.setEndDate(productPricing.getEndDate());
		this.setDescription(productPricing.getDescription());
		this.setProductPricingDetails(productPricing.getProductPricingDetails());
	}
	
	public String getPricingLevel() {
		return pricingLevel;
	}
	public void setPricingLevel(String pricingLevel) {
		this.pricingLevel = pricingLevel;
	}
	public String getEffictiveStartDate() {
		return effictiveStartDate;
	}
	public void setEffictiveStartDate(String effictiveStartDate) {
		this.effictiveStartDate = effictiveStartDate;
	}
	public String getEffictiveEndDate() {
		return effictiveEndDate;
	}
	public void setEffictiveEndDate(String effictiveEndDate) {
		this.effictiveEndDate = effictiveEndDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getCustomers() {
		return customers;
	}
	public void setCustomers(String customers) {
		this.customers = customers;
	}
	public List<ProductPricingDetailVO> getProductPricingDetailVOs() {
		return productPricingDetailVOs;
	}
	public void setProductPricingDetailVOs(
			List<ProductPricingDetailVO> productPricingDetailVOs) {
		this.productPricingDetailVOs = productPricingDetailVOs;
	}
	public List<ProductPricingCalcVO> getProductPricingCalcVOs() {
		return productPricingCalcVOs;
	}
	public void setProductPricingCalcVOs(
			List<ProductPricingCalcVO> productPricingCalcVOs) {
		this.productPricingCalcVOs = productPricingCalcVOs;
	}
}
