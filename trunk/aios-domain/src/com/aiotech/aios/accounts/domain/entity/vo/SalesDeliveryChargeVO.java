package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryCharge;

public class SalesDeliveryChargeVO extends SalesDeliveryCharge{

	private String strChargeType;
	private String strMode;
	
	public SalesDeliveryChargeVO() {
		
	}
	
	public SalesDeliveryChargeVO(SalesDeliveryCharge salesDeliveryCharge) {
		this.setSalesDeliveryChargeId(salesDeliveryCharge.getSalesDeliveryChargeId());
		this.setSalesDeliveryNote(salesDeliveryCharge.getSalesDeliveryNote());
		this.setLookupDetail(salesDeliveryCharge.getLookupDetail());
		this.setIsPercentage(salesDeliveryCharge.getIsPercentage());
		this.setCharges(salesDeliveryCharge.getCharges());
		this.setDescription(salesDeliveryCharge.getDescription());
	}
	
	public String getStrChargeType() {
		return strChargeType;
	}
	public void setStrChargeType(String strChargeType) {
		this.strChargeType = strChargeType;
	}
	public String getStrMode() {
		return strMode;
	}
	public void setStrMode(String strMode) {
		this.strMode = strMode;
	}
	
	
}
