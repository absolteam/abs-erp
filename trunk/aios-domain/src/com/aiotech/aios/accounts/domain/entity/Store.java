package com.aiotech.aios.accounts.domain.entity;

// Generated Jun 14, 2015 11:34:31 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Store generated by hbm2java
 */
@Entity
@Table(name = "ac_store", catalog = "aios")
public class Store implements java.io.Serializable {

	private Long storeId;
	private Implementation implementation;
	private Person person;
	private CmpDeptLoc cmpDeptLocation;
	private LookupDetail lookupDetail;
	private String storeNumber;
	private String storeName;
	private String address;
	private Byte isApprove;
	private Long relativeId;
	private Set<SalesOrderDetail> salesOrderDetails = new HashSet<SalesOrderDetail>(
			0);
	private Set<Aisle> aisles = new HashSet<Aisle>(0);
	private Set<POSUserTill> POSUserTills = new HashSet<POSUserTill>(0);
	private Set<IssueReturn> issueReturns = new HashSet<IssueReturn>(0);
	private Set<ProductSplitDetail> productSplitDetails = new HashSet<ProductSplitDetail>(
			0);
	private Set<StockPeriodic> stockPeriodics = new HashSet<StockPeriodic>(0);
	private Set<Stock> stocks = new HashSet<Stock>(0);
	private Set<ProductPricingCalc> productPricingCalcs = new HashSet<ProductPricingCalc>(
			0);
	private Set<ProductPricingDetail> productPricingDetails = new HashSet<ProductPricingDetail>(
			0);
	private Set<MaterialWastageDetail> materialWastageDetails = new HashSet<MaterialWastageDetail>(
			0);
	private Set<EODBalancing> EODBalancings = new HashSet<EODBalancing>(0);
	private Set<CustomerQuotationDetail> customerQuotationDetails = new HashSet<CustomerQuotationDetail>(
			0);
	private Set<MaterialRequisition> materialRequisitions = new HashSet<MaterialRequisition>(
			0);

	public Store() {
	}

	public Store(Implementation implementation, Person person,
			String storeNumber) {
		this.implementation = implementation;
		this.person = person;
		this.storeNumber = storeNumber;
	}

	public Store(Implementation implementation, Person person,
			CmpDeptLoc cmpDeptLocation, LookupDetail lookupDetail,
			String storeNumber, String storeName, String address,
			Byte isApprove, Long relativeId,
			Set<SalesOrderDetail> salesOrderDetails, Set<Aisle> aisles,
			Set<POSUserTill> POSUserTills, Set<IssueReturn> issueReturns,
			Set<ProductSplitDetail> productSplitDetails,
			Set<StockPeriodic> stockPeriodics, Set<Stock> stocks,
			Set<ProductPricingCalc> productPricingCalcs,
			Set<ProductPricingDetail> productPricingDetails,
			Set<MaterialWastageDetail> materialWastageDetails,
			Set<EODBalancing> EODBalancings,
			Set<CustomerQuotationDetail> customerQuotationDetails,
			Set<MaterialRequisition> materialRequisitions) {
		this.implementation = implementation;
		this.person = person;
		this.cmpDeptLocation = cmpDeptLocation;
		this.lookupDetail = lookupDetail;
		this.storeNumber = storeNumber;
		this.storeName = storeName;
		this.address = address;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
		this.salesOrderDetails = salesOrderDetails;
		this.aisles = aisles;
		this.POSUserTills = POSUserTills;
		this.issueReturns = issueReturns;
		this.productSplitDetails = productSplitDetails;
		this.stockPeriodics = stockPeriodics;
		this.stocks = stocks;
		this.productPricingCalcs = productPricingCalcs;
		this.productPricingDetails = productPricingDetails;
		this.materialWastageDetails = materialWastageDetails;
		this.EODBalancings = EODBalancings;
		this.customerQuotationDetails = customerQuotationDetails;
		this.materialRequisitions = materialRequisitions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "store_id", unique = true, nullable = false)
	public Long getStoreId() {
		return this.storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "store_person", nullable = false)
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	} 

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "storage_type")
	public LookupDetail getLookupDetail() {
		return this.lookupDetail;
	}

	public void setLookupDetail(LookupDetail lookupDetail) {
		this.lookupDetail = lookupDetail;
	}

	@Column(name = "store_number", nullable = false, length = 30)
	public String getStoreNumber() {
		return this.storeNumber;
	}

	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}

	@Column(name = "store_name", length = 30)
	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	@Column(name = "address", length = 50)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<SalesOrderDetail> getSalesOrderDetails() {
		return this.salesOrderDetails;
	}

	public void setSalesOrderDetails(Set<SalesOrderDetail> salesOrderDetails) {
		this.salesOrderDetails = salesOrderDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<Aisle> getAisles() {
		return this.aisles;
	}

	public void setAisles(Set<Aisle> aisles) {
		this.aisles = aisles;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<POSUserTill> getPOSUserTills() {
		return this.POSUserTills;
	}

	public void setPOSUserTills(Set<POSUserTill> POSUserTills) {
		this.POSUserTills = POSUserTills;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<IssueReturn> getIssueReturns() {
		return this.issueReturns;
	}

	public void setIssueReturns(Set<IssueReturn> issueReturns) {
		this.issueReturns = issueReturns;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<ProductSplitDetail> getProductSplitDetails() {
		return this.productSplitDetails;
	}

	public void setProductSplitDetails(
			Set<ProductSplitDetail> productSplitDetails) {
		this.productSplitDetails = productSplitDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<StockPeriodic> getStockPeriodics() {
		return this.stockPeriodics;
	}

	public void setStockPeriodics(Set<StockPeriodic> stockPeriodics) {
		this.stockPeriodics = stockPeriodics;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<Stock> getStocks() {
		return this.stocks;
	}

	public void setStocks(Set<Stock> stocks) {
		this.stocks = stocks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<ProductPricingCalc> getProductPricingCalcs() {
		return this.productPricingCalcs;
	}

	public void setProductPricingCalcs(
			Set<ProductPricingCalc> productPricingCalcs) {
		this.productPricingCalcs = productPricingCalcs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<ProductPricingDetail> getProductPricingDetails() {
		return this.productPricingDetails;
	}

	public void setProductPricingDetails(
			Set<ProductPricingDetail> productPricingDetails) {
		this.productPricingDetails = productPricingDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<MaterialWastageDetail> getMaterialWastageDetails() {
		return this.materialWastageDetails;
	}

	public void setMaterialWastageDetails(
			Set<MaterialWastageDetail> materialWastageDetails) {
		this.materialWastageDetails = materialWastageDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<EODBalancing> getEODBalancings() {
		return this.EODBalancings;
	}

	public void setEODBalancings(Set<EODBalancing> EODBalancings) {
		this.EODBalancings = EODBalancings;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<CustomerQuotationDetail> getCustomerQuotationDetails() {
		return this.customerQuotationDetails;
	}

	public void setCustomerQuotationDetails(
			Set<CustomerQuotationDetail> customerQuotationDetails) {
		this.customerQuotationDetails = customerQuotationDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "store")
	public Set<MaterialRequisition> getMaterialRequisitions() {
		return this.materialRequisitions;
	}

	public void setMaterialRequisitions(
			Set<MaterialRequisition> materialRequisitions) {
		this.materialRequisitions = materialRequisitions;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	public CmpDeptLoc getCmpDeptLocation() {
		return cmpDeptLocation;
	}

	public void setCmpDeptLocation(CmpDeptLoc cmpDeptLocation) {
		this.cmpDeptLocation = cmpDeptLocation;
	} 
}
