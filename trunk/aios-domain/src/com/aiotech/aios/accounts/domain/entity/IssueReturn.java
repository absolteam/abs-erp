package com.aiotech.aios.accounts.domain.entity;

// Generated Jun 18, 2015 12:25:40 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.CmpDeptLoc;
import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * IssueReturn generated by hbm2java
 */
@Entity
@Table(name = "ac_issue_return", catalog = "aios")
public class IssueReturn implements java.io.Serializable {

	private Long issueReturnId;
	private Implementation implementation;
	private CmpDeptLoc cmpDeptLocation;
	private Store store;
	private String referenceNumber;
	private Date returnDate;
	private String description;
	private Byte isApprove;
	private Long relativeId;
	private Set<IssueReturnDetail> issueReturnDetails = new HashSet<IssueReturnDetail>(
			0);

	public IssueReturn() {
	}

	public IssueReturn(Implementation implementation, String referenceNumber,
			Date returnDate) {
		this.implementation = implementation;
		this.referenceNumber = referenceNumber;
		this.returnDate = returnDate;
	}

	public IssueReturn(Implementation implementation, CmpDeptLoc cmpDeptLocation,
			Store store, String referenceNumber, Date returnDate,
			String description, Byte isApprove, Long relativeId,
			Set<IssueReturnDetail> issueReturnDetails) {
		this.implementation = implementation;
		this.cmpDeptLocation = cmpDeptLocation;
		this.store = store;
		this.referenceNumber = referenceNumber;
		this.returnDate = returnDate;
		this.description = description;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
		this.issueReturnDetails = issueReturnDetails;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "issue_return_id", unique = true, nullable = false)
	public Long getIssueReturnId() {
		return this.issueReturnId;
	}

	public void setIssueReturnId(Long issueReturnId) {
		this.issueReturnId = issueReturnId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cmp_dep_loc")
	public CmpDeptLoc getCmpDeptLocation() {
		return cmpDeptLocation;
	}

	public void setCmpDeptLocation(CmpDeptLoc cmpDeptLocation) {
		this.cmpDeptLocation = cmpDeptLocation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "store_id")
	public Store getStore() {
		return this.store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	@Column(name = "reference_number", nullable = false, length = 30)
	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "return_date", nullable = false, length = 10)
	public Date getReturnDate() {
		return this.returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "issueReturn")
	public Set<IssueReturnDetail> getIssueReturnDetails() {
		return this.issueReturnDetails;
	}

	public void setIssueReturnDetails(Set<IssueReturnDetail> issueReturnDetails) {
		this.issueReturnDetails = issueReturnDetails;
	} 
}
