package com.aiotech.aios.accounts.domain.entity;

// Generated Feb 22, 2015 9:08:14 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.LookupDetail;

/**
 * BankAccount generated by hbm2java
 */
@Entity
@Table(name = "ac_bank_account", catalog = "aios")
public class BankAccount implements java.io.Serializable {

	private Long bankAccountId;
	private LookupDetail lookupDetailBySignatory2LookupId;
	private LookupDetail lookupDetailBySignatory1LookupId;
	private LookupDetail lookupDetailByCardIssuingEntity;
	private Bank bank;
	private String bankAccountHolder;
	private Combination combinationByClearingAccount;
	private Combination combination;
	private Currency currency;
	private LookupDetail lookupDetailByOperationMode;
	private String accountNumber;
	private String cardNumber;
	private Byte cardType;
	private Date validFrom;
	private Date validTo;
	private Integer csvNumber;
	private Integer pinNumber;
	private Double cardLimit;
	private Double creditLimit;
	private Byte accountType;
	private String accountPurpose;
	private String rountingNumber;
	private String swiftCode;
	private String iban;
	private Long branchContact;
	private String contactPerson;
	private String branchName;
	private String address;
	private String description;
	private Set<ReconciliationAdjustment> reconciliationAdjustments = new HashSet<ReconciliationAdjustment>(
			0); 
	private Set<BankReconciliation> bankReconciliations = new HashSet<BankReconciliation>(
			0);
	private Set<DirectPayment> directPayments = new HashSet<DirectPayment>(0);
	private Set<Loan> loans = new HashSet<Loan>(0);
	private Set<BankTransfer> bankTransfersForDebitBankAccount = new HashSet<BankTransfer>(
			0);
	private Set<BankTransfer> bankTransfersForCreditBankAccount = new HashSet<BankTransfer>(
			0);
	private Set<ChequeBook> chequeBooks = new HashSet<ChequeBook>(0);
	private Set<Payment> payments = new HashSet<Payment>(0);
	private Set<Company> companies = new HashSet<Company>(0);

	public BankAccount() {
	}

	public BankAccount(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BankAccount(LookupDetail lookupDetailBySignatory2LookupId,
			LookupDetail lookupDetailBySignatory1LookupId,
			LookupDetail lookupDetailByCardIssuingEntity, Company company,
			Bank bank, Combination combinationByClearingAccount, Person person,
			Combination combination, Currency currency,
			LookupDetail lookupDetailByOperationMode, String accountNumber,
			String cardNumber, String bankAccountHolder, Byte cardType, Date validFrom, Date validTo,
			Integer csvNumber, Integer pinNumber, Double cardLimit, Double creditLimit,
			Byte accountType, String accountPurpose, String rountingNumber,
			String swiftCode, String iban, Long branchContact,
			String contactPerson, String branchName, String address,
			String description,
			Set<ReconciliationAdjustment> reconciliationAdjustments, 
			Set<BankReconciliation> bankReconciliations,
			Set<DirectPayment> directPayments, Set<Loan> loans,
			Set<BankTransfer> bankTransfersForDebitBankAccount,
			Set<BankTransfer> bankTransfersForCreditBankAccount,
			Set<ChequeBook> chequeBooks, Set<Payment> payments,
			Set<Company> companies) {
		this.lookupDetailBySignatory2LookupId = lookupDetailBySignatory2LookupId;
		this.lookupDetailBySignatory1LookupId = lookupDetailBySignatory1LookupId;
		this.lookupDetailByCardIssuingEntity = lookupDetailByCardIssuingEntity;
		this.bank = bank;
		this.combinationByClearingAccount = combinationByClearingAccount;
		this.combination = combination;
		this.currency = currency;
		this.lookupDetailByOperationMode = lookupDetailByOperationMode;
		this.accountNumber = accountNumber;
		this.cardNumber = cardNumber;
		this.bankAccountHolder = bankAccountHolder;
		this.cardType = cardType;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.csvNumber = csvNumber;
		this.pinNumber = pinNumber;
		this.cardLimit = cardLimit;
		this.creditLimit = creditLimit;
		this.accountType = accountType;
		this.accountPurpose = accountPurpose;
		this.rountingNumber = rountingNumber;
		this.swiftCode = swiftCode;
		this.iban = iban;
		this.branchContact = branchContact;
		this.contactPerson = contactPerson;
		this.branchName = branchName;
		this.address = address;
		this.description = description;
		this.reconciliationAdjustments = reconciliationAdjustments;
 		this.bankReconciliations = bankReconciliations;
		this.directPayments = directPayments;
		this.loans = loans;
		this.bankTransfersForDebitBankAccount = bankTransfersForDebitBankAccount;
		this.bankTransfersForCreditBankAccount = bankTransfersForCreditBankAccount;
		this.chequeBooks = chequeBooks;
		this.payments = payments;
		this.companies = companies;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "bank_account_id", unique = true, nullable = false)
	public Long getBankAccountId() {
		return this.bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "signatory2_lookup_id")
	public LookupDetail getLookupDetailBySignatory2LookupId() {
		return this.lookupDetailBySignatory2LookupId;
	}

	public void setLookupDetailBySignatory2LookupId(
			LookupDetail lookupDetailBySignatory2LookupId) {
		this.lookupDetailBySignatory2LookupId = lookupDetailBySignatory2LookupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "signatory1_lookup_id")
	public LookupDetail getLookupDetailBySignatory1LookupId() {
		return this.lookupDetailBySignatory1LookupId;
	}

	public void setLookupDetailBySignatory1LookupId(
			LookupDetail lookupDetailBySignatory1LookupId) {
		this.lookupDetailBySignatory1LookupId = lookupDetailBySignatory1LookupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "card_issuing_entity")
	public LookupDetail getLookupDetailByCardIssuingEntity() {
		return this.lookupDetailByCardIssuingEntity;
	}

	public void setLookupDetailByCardIssuingEntity(
			LookupDetail lookupDetailByCardIssuingEntity) {
		this.lookupDetailByCardIssuingEntity = lookupDetailByCardIssuingEntity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bank_id")
	public Bank getBank() {
		return this.bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clearing_account")
	public Combination getCombinationByClearingAccount() {
		return this.combinationByClearingAccount;
	}

	public void setCombinationByClearingAccount(
			Combination combinationByClearingAccount) {
		this.combinationByClearingAccount = combinationByClearingAccount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "combination_id")
	public Combination getCombination() {
		return this.combination;
	}

	public void setCombination(
			Combination combination) {
		this.combination = combination;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency_id")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "operation_mode")
	public LookupDetail getLookupDetailByOperationMode() {
		return this.lookupDetailByOperationMode;
	}

	public void setLookupDetailByOperationMode(
			LookupDetail lookupDetailByOperationMode) {
		this.lookupDetailByOperationMode = lookupDetailByOperationMode;
	}

	@Column(name = "account_number", nullable = false, length = 30)
	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column(name = "card_number", length = 30)
	public String getCardNumber() {
		return this.cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	@Column(name = "card_type")
	public Byte getCardType() {
		return this.cardType;
	}

	public void setCardType(Byte cardType) {
		this.cardType = cardType;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_from", length = 10)
 	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "valid_to", length = 10)
 	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	@Column(name = "csv_number")
	public Integer getCsvNumber() {
		return this.csvNumber;
	}

	public void setCsvNumber(Integer csvNumber) {
		this.csvNumber = csvNumber;
	}

	@Column(name = "pin_number")
	public Integer getPinNumber() {
		return this.pinNumber;
	}

	public void setPinNumber(Integer pinNumber) {
		this.pinNumber = pinNumber;
	}

	@Column(name = "card_limit", precision = 22, scale = 0)
	public Double getCardLimit() {
		return this.cardLimit;
	}

	public void setCardLimit(Double cardLimit) {
		this.cardLimit = cardLimit;
	}

	@Column(name = "account_type")
	public Byte getAccountType() {
		return this.accountType;
	}

	public void setAccountType(Byte accountType) {
		this.accountType = accountType;
	}

	@Column(name = "account_purpose", length = 250)
	public String getAccountPurpose() {
		return this.accountPurpose;
	}

	public void setAccountPurpose(String accountPurpose) {
		this.accountPurpose = accountPurpose;
	}

	@Column(name = "rounting_number", length = 30)
	public String getRountingNumber() {
		return this.rountingNumber;
	}

	public void setRountingNumber(String rountingNumber) {
		this.rountingNumber = rountingNumber;
	}

	@Column(name = "swift_code", length = 20)
	public String getSwiftCode() {
		return this.swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	@Column(name = "iban", length = 50)
	public String getIban() {
		return this.iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	@Column(name = "branch_contact")
	public Long getBranchContact() {
		return this.branchContact;
	}

	public void setBranchContact(Long branchContact) {
		this.branchContact = branchContact;
	}

	@Column(name = "contact_person", length = 150)
	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Column(name = "branch_name", length = 250)
	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@Column(name = "address", length = 500)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<ReconciliationAdjustment> getReconciliationAdjustments() {
		return this.reconciliationAdjustments;
	}

	public void setReconciliationAdjustments(
			Set<ReconciliationAdjustment> reconciliationAdjustments) {
		this.reconciliationAdjustments = reconciliationAdjustments;
	} 

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<BankReconciliation> getBankReconciliations() {
		return this.bankReconciliations;
	}

	public void setBankReconciliations(
			Set<BankReconciliation> bankReconciliations) {
		this.bankReconciliations = bankReconciliations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<DirectPayment> getDirectPayments() {
		return this.directPayments;
	}

	public void setDirectPayments(Set<DirectPayment> directPayments) {
		this.directPayments = directPayments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<Loan> getLoans() {
		return this.loans;
	}

	public void setLoans(Set<Loan> loans) {
		this.loans = loans;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccountByDebitBankAccount")
	public Set<BankTransfer> getBankTransfersForDebitBankAccount() {
		return this.bankTransfersForDebitBankAccount;
	}

	public void setBankTransfersForDebitBankAccount(
			Set<BankTransfer> bankTransfersForDebitBankAccount) {
		this.bankTransfersForDebitBankAccount = bankTransfersForDebitBankAccount;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccountByCreditBankAccount")
	public Set<BankTransfer> getBankTransfersForCreditBankAccount() {
		return this.bankTransfersForCreditBankAccount;
	}

	public void setBankTransfersForCreditBankAccount(
			Set<BankTransfer> bankTransfersForCreditBankAccount) {
		this.bankTransfersForCreditBankAccount = bankTransfersForCreditBankAccount;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<ChequeBook> getChequeBooks() {
		return this.chequeBooks;
	}

	public void setChequeBooks(Set<ChequeBook> chequeBooks) {
		this.chequeBooks = chequeBooks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<Payment> getPayments() {
		return this.payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankAccount")
	public Set<Company> getCompanies() {
		return this.companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

	@Column(name = "account_holder")
	public String getBankAccountHolder() {
		return bankAccountHolder;
	}

	public void setBankAccountHolder(String bankAccountHolder) {
		this.bankAccountHolder = bankAccountHolder;
	}

	@Column(name = "credit_limit")
	public Double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}

}
