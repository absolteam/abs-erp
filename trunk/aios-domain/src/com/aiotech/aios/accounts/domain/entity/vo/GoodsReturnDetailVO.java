package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.GoodsReturnDetail;

public class GoodsReturnDetailVO extends GoodsReturnDetail implements
		java.io.Serializable {

	private String productCode;
	private String strItemType;
	private String productName;
	private String purchaseReference;
	private String purchaseQty;
	private double receiveQty;
	private long packageDetailId;
	private String conversionUnitName;
	private String baseUnitName;
	private String unitRate;
	private String unitRateStr;
	private String totalAmount;
	private String receiveReference;
	private String baseQuantity;
	private String unitNameStr;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public GoodsReturnDetailVO() {

	}

	public GoodsReturnDetailVO(GoodsReturnDetail goodsReturnDetail) {
		this.setReturnDetailId(goodsReturnDetail.getReturnDetailId());
		this.setProduct(goodsReturnDetail.getProduct());
		this.setPurchase(goodsReturnDetail.getPurchase());
		this.setGoodsReturn(goodsReturnDetail.getGoodsReturn());
		this.setReturnQty(goodsReturnDetail.getReturnQty());
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStrItemType() {
		return strItemType;
	}

	public void setStrItemType(String strItemType) {
		this.strItemType = strItemType;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPurchaseReference() {
		return purchaseReference;
	}

	public void setPurchaseReference(String purchaseReference) {
		this.purchaseReference = purchaseReference;
	}

	public String getPurchaseQty() {
		return purchaseQty;
	}

	public void setPurchaseQty(String purchaseQty) {
		this.purchaseQty = purchaseQty;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}

	public double getReceiveQty() {
		return receiveQty;
	}

	public void setReceiveQty(double receiveQty) {
		this.receiveQty = receiveQty;
	}

	public String getReceiveReference() {
		return receiveReference;
	}

	public void setReceiveReference(String receiveReference) {
		this.receiveReference = receiveReference;
	}

	public long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getConversionUnitName() {
		return conversionUnitName;
	}

	public void setConversionUnitName(String conversionUnitName) {
		this.conversionUnitName = conversionUnitName;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}

	public String getUnitNameStr() {
		return unitNameStr;
	}

	public void setUnitNameStr(String unitNameStr) {
		this.unitNameStr = unitNameStr;
	}

	public String getUnitRateStr() {
		return unitRateStr;
	}

	public void setUnitRateStr(String unitRateStr) {
		this.unitRateStr = unitRateStr;
	}
}
