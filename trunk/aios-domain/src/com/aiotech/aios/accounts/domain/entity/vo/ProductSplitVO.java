package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductSplit;
import com.aiotech.aios.accounts.domain.entity.ProductSplitDetail;

public class ProductSplitVO extends ProductSplit{
	private String createdDateView;
	private String productName;
	private String storeName;
	private String splitedPerson;
	private String sectionName;
	private String rackName;
	private String shelfName;
	private String productCode;
	private List<ProductSplitVO> splitDetailVO=new ArrayList<ProductSplitVO>();
	private ProductSplitDetail productSplitDetail;
	public String getCreatedDateView() {
		return createdDateView;
	}
	public void setCreatedDateView(String createdDateView) {
		this.createdDateView = createdDateView;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getSplitedPerson() {
		return splitedPerson;
	}
	public void setSplitedPerson(String splitedPerson) {
		this.splitedPerson = splitedPerson;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getRackName() {
		return rackName;
	}
	public void setRackName(String rackName) {
		this.rackName = rackName;
	}
	public String getShelfName() {
		return shelfName;
	}
	public void setShelfName(String shelfName) {
		this.shelfName = shelfName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public List<ProductSplitVO> getSplitDetailVO() {
		return splitDetailVO;
	}
	public void setSplitDetailVO(List<ProductSplitVO> splitDetailVO) {
		this.splitDetailVO = splitDetailVO;
	}
	public ProductSplitDetail getProductSplitDetail() {
		return productSplitDetail;
	}
	public void setProductSplitDetail(ProductSplitDetail productSplitDetail) {
		this.productSplitDetail = productSplitDetail;
	}
}
