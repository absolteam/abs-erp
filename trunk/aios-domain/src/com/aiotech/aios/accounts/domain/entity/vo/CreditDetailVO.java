package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.CreditDetail;

public class CreditDetailVO extends CreditDetail implements java.io.Serializable {

	private Double deliveryQuantity;
	private Double invoiceQuantity;
	private Double inwardedQuantity;
	private Double unitRate;
	private Long productId;
	private Long salesDeliveryDetailId;
	private Integer shelfId;
	private String shelfName;
	private String productName;
	private String productCode;
	private String referenceNumber;
	private String returnDate;
	private String returnType;
	private Double total;
	
	public CreditDetailVO() {
		
	}
	
	public CreditDetailVO(CreditDetail creditDetail) {
		this.setCreditDetailId(creditDetail.getCreditDetailId());
		this.setSalesDeliveryDetail(creditDetail.getSalesDeliveryDetail());
		this.setCredit(creditDetail.getCredit());
		this.setReturnQuantity(creditDetail.getReturnQuantity());
		this.setDescription(creditDetail.getDescription());
	}
	
	public Double getDeliveryQuantity() {
		return deliveryQuantity;
	}

	public void setDeliveryQuantity(Double deliveryQuantity) {
		this.deliveryQuantity = deliveryQuantity;
	}

	public Double getInvoiceQuantity() {
		return invoiceQuantity;
	}

	public void setInvoiceQuantity(Double invoiceQuantity) {
		this.invoiceQuantity = invoiceQuantity;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getSalesDeliveryDetailId() {
		return salesDeliveryDetailId;
	}

	public void setSalesDeliveryDetailId(Long salesDeliveryDetailId) {
		this.salesDeliveryDetailId = salesDeliveryDetailId;
	}

	public Double getInwardedQuantity() {
		return inwardedQuantity;
	}

	public void setInwardedQuantity(Double inwardedQuantity) {
		this.inwardedQuantity = inwardedQuantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public String getShelfName() {
		return shelfName;
	}

	public void setShelfName(String shelfName) {
		this.shelfName = shelfName;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
}
