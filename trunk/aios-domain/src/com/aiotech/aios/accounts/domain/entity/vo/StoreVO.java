package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Store;

public class StoreVO extends Store implements java.io.Serializable {

	private String personNumber;
	private String personName;
	private long personId;
	private String storageType;
	private String storageSection;
	private String binType;
	private String linkLabel;
	private String anchorExpression;
	private List<AisleVO> aisleVOs = new ArrayList<AisleVO>();
	private List<StockVO> stockVOs = new ArrayList<StockVO>();

	public StoreVO() {
	}

	public StoreVO(Store store) {
		this.setStoreId(store.getStoreId());
		this.setImplementation(store.getImplementation());
		this.setPerson(store.getPerson());
		this.setLookupDetail(store.getLookupDetail());
		this.setStoreNumber(store.getStoreNumber());
		this.setStoreName(store.getStoreName());
		this.setAddress(store.getAddress());
		this.setSalesOrderDetails(store.getSalesOrderDetails());
		this.setAisles(store.getAisles());
		this.setCustomerQuotationDetails(store.getCustomerQuotationDetails());
	}

	public String getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getStorageSection() {
		return storageSection;
	}

	public void setStorageSection(String storageSection) {
		this.storageSection = storageSection;
	}

	public String getBinType() {
		return binType;
	}

	public void setBinType(String binType) {
		this.binType = binType;
	}

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getLinkLabel() {
		return linkLabel;
	}

	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	public String getAnchorExpression() {
		return anchorExpression;
	}

	public void setAnchorExpression(String anchorExpression) {
		this.anchorExpression = anchorExpression;
	}

	public List<AisleVO> getAisleVOs() {
		return aisleVOs;
	}

	public void setAisleVOs(List<AisleVO> aisleVOs) {
		this.aisleVOs = aisleVOs;
	}

	public List<StockVO> getStockVOs() {
		return stockVOs;
	}

	public void setStockVOs(List<StockVO> stockVOs) {
		this.stockVOs = stockVOs;
	}

}
