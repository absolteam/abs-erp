package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductDefinition;

public class ProductDefinitionVO extends ProductDefinition implements
		java.io.Serializable {

	private String productName;
	private String definitionLabel;
	private String specialProductName;
	private String pricingType;
	private List<ProductDefinitionVO> productDefinitionVOs = new ArrayList<ProductDefinitionVO>();

	static public ProductDefinitionVO convertTOProductDefinitionVO(
			ProductDefinition definition) {
		ProductDefinitionVO vo = new ProductDefinitionVO();
		vo.setDefinitionLabel(definition.getDefinitionLabel());
		vo.setProductDefinitionId(definition.getProductDefinitionId());
		vo.setProductName(definition.getProductByProductId().getProductName());
		vo.setSpecialProductName(null != definition
				.getProductBySpecialProductId() ? definition
				.getProductBySpecialProductId().getProductName() : null);
		return vo;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDefinitionLabel() {
		return definitionLabel;
	}

	public void setDefinitionLabel(String definitionLabel) {
		this.definitionLabel = definitionLabel;
	}

	public List<ProductDefinitionVO> getProductDefinitionVOs() {
		return productDefinitionVOs;
	}

	public void setProductDefinitionVOs(
			List<ProductDefinitionVO> productDefinitionVOs) {
		this.productDefinitionVOs = productDefinitionVOs;
	}

	public String getSpecialProductName() {
		return specialProductName;
	}

	public void setSpecialProductName(String specialProductName) {
		this.specialProductName = specialProductName;
	}

	public String getPricingType() {
		return pricingType;
	}

	public void setPricingType(String pricingType) {
		this.pricingType = pricingType;
	}
}
