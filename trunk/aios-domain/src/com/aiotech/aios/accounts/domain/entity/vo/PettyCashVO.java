package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.PettyCash;

public class PettyCashVO extends PettyCash implements java.io.Serializable{
	
	private String pettyCashDate;
	private Double remainingBalance;
	private Double totalExpense;
	private String pettyCashTypeName;
	private String employeeName;
	private String cashInFormat;
	private String cashOutFormat;
	private String remainingBalanceFormat;
	private String expenseAmountFormat;
	private String receiptVoucher;
	private String createdBy;
	private String receivedBy;
	private String expenseAccount;
	private String settledWith;
	private String key;
	private String value;
	private String code;
	private String receiptReference;
	private Long receiptVoucherId;
	private Long parentPettyCashId;
	private String parentReference;
	private String combinationStr;
	private String voucherDateView;
	private String cashForwaredStr;
	private List<PettyCashVO> pettyCashes = new ArrayList<PettyCashVO>();
	
	public String getPettyCashDate() {
		return pettyCashDate;
	}
	public void setPettyCashDate(String pettyCashDate) {
		this.pettyCashDate = pettyCashDate;
	}
	public Double getRemainingBalance() {
		return remainingBalance;
	}
	public void setRemainingBalance(Double remainingBalance) {
		this.remainingBalance = remainingBalance;
	}
	public Double getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(Double totalExpense) {
		this.totalExpense = totalExpense;
	}
	public String getPettyCashTypeName() {
		return pettyCashTypeName;
	}
	public void setPettyCashTypeName(String pettyCashTypeName) {
		this.pettyCashTypeName = pettyCashTypeName;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getCashInFormat() {
		return cashInFormat;
	}
	public void setCashInFormat(String cashInFormat) {
		this.cashInFormat = cashInFormat;
	}
	public String getCashOutFormat() {
		return cashOutFormat;
	}
	public void setCashOutFormat(String cashOutFormat) {
		this.cashOutFormat = cashOutFormat;
	}
	public String getRemainingBalanceFormat() {
		return remainingBalanceFormat;
	}
	public void setRemainingBalanceFormat(String remainingBalanceFormat) {
		this.remainingBalanceFormat = remainingBalanceFormat;
	}
	public String getExpenseAmountFormat() {
		return expenseAmountFormat;
	}
	public void setExpenseAmountFormat(String expenseAmountFormat) {
		this.expenseAmountFormat = expenseAmountFormat;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getExpenseAccount() {
		return expenseAccount;
	}
	public void setExpenseAccount(String expenseAccount) {
		this.expenseAccount = expenseAccount;
	}
	public String getSettledWith() {
		return settledWith;
	}
	public void setSettledWith(String settledWith) {
		this.settledWith = settledWith;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getReceiptVoucher() {
		return receiptVoucher;
	}
	public void setReceiptVoucher(String receiptVoucher) {
		this.receiptVoucher = receiptVoucher;
	}
	public Long getReceiptVoucherId() {
		return receiptVoucherId;
	}
	public void setReceiptVoucherId(Long receiptVoucherId) {
		this.receiptVoucherId = receiptVoucherId;
	}
	public String getReceiptReference() {
		return receiptReference;
	}
	public void setReceiptReference(String receiptReference) {
		this.receiptReference = receiptReference;
	}
	public Long getParentPettyCashId() {
		return parentPettyCashId;
	}
	public void setParentPettyCashId(Long parentPettyCashId) {
		this.parentPettyCashId = parentPettyCashId;
	}
	public String getParentReference() {
		return parentReference;
	}
	public void setParentReference(String parentReference) {
		this.parentReference = parentReference;
	}
	public String getCombinationStr() {
		return combinationStr;
	}
	public void setCombinationStr(String combinationStr) {
		this.combinationStr = combinationStr;
	}
	public String getVoucherDateView() {
		return voucherDateView;
	}
	public void setVoucherDateView(String voucherDateView) {
		this.voucherDateView = voucherDateView;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	public List<PettyCashVO> getPettyCashes() {
		return pettyCashes;
	}
	public void setPettyCashes(List<PettyCashVO> pettyCashes) {
		this.pettyCashes = pettyCashes;
	}
	public String getCashForwaredStr() {
		return cashForwaredStr;
	}
	public void setCashForwaredStr(String cashForwaredStr) {
		this.cashForwaredStr = cashForwaredStr;
	} 
}
