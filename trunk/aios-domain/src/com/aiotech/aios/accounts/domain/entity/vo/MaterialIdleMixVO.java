package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMix;
import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;

public class MaterialIdleMixVO extends MaterialIdleMix implements
		java.io.Serializable {

	private String productCode;
	private String productName;
	private String personName;
	private String date;
	List<MaterialIdleMixDetail> materialMixDetails = new ArrayList<MaterialIdleMixDetail>();
	List<MaterialIdleMixDetailVO> materialMixDetailVOs = new ArrayList<MaterialIdleMixDetailVO>();

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<MaterialIdleMixDetail> getMaterialMixDetails() {
		return materialMixDetails;
	}

	public void setMaterialMixDetails(List<MaterialIdleMixDetail> materialMixDetails) {
		this.materialMixDetails = materialMixDetails;
	}

	public List<MaterialIdleMixDetailVO> getMaterialMixDetailVOs() {
		return materialMixDetailVOs;
	}

	public void setMaterialMixDetailVOs(
			List<MaterialIdleMixDetailVO> materialMixDetailVOs) {
		this.materialMixDetailVOs = materialMixDetailVOs;
	}
}
