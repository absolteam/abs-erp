package com.aiotech.aios.accounts.domain.entity;

// Generated Jan 18, 2015 3:53:34 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ProductPoint generated by hbm2java
 */
@Entity
@Table(name = "ac_product_point", catalog = "aios")
public class ProductPoint implements java.io.Serializable {

	private Long productPointId;
	private Product product;
	private Double purchaseAmount;
	private Double statusPoints;
	private String description;
	private Date fromDate;
	private Date toDate;
	private Boolean status;

	public ProductPoint() {
	}

	public ProductPoint(Product product, Double purchaseAmount,
			Double statusPoints, Date fromDate, Boolean status) {
		this.product = product;
		this.purchaseAmount = purchaseAmount;
		this.statusPoints = statusPoints;
		this.fromDate = fromDate;
		this.status = status;
	}

	public ProductPoint(Product product, Double purchaseAmount,
			Double statusPoints, String description, Date fromDate,
			Date toDate, Boolean status) {
		this.product = product;
		this.purchaseAmount = purchaseAmount;
		this.statusPoints = statusPoints;
		this.description = description;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "product_point_id", unique = true, nullable = false)
	public Long getProductPointId() {
		return this.productPointId;
	}

	public void setProductPointId(Long productPointId) {
		this.productPointId = productPointId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "purchase_amount", nullable = false, precision = 22, scale = 0)
	public Double getPurchaseAmount() {
		return this.purchaseAmount;
	}

	public void setPurchaseAmount(Double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	@Column(name = "status_points", nullable = false, precision = 22, scale = 0)
	public Double getStatusPoints() {
		return this.statusPoints;
	}

	public void setStatusPoints(Double statusPoints) {
		this.statusPoints = statusPoints;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "from_date", nullable = false, length = 10)
	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "to_date", length = 10)
	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	@Column(name = "status", nullable = false)
	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	private Byte isApprove;
	private Long relativeId;

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

}
