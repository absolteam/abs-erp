package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.BankReceiptsDetail;

public class BankReceiptsDetailVO extends BankReceiptsDetail implements java.io.Serializable{
	
	private String paymentModeName;
	private String strChequeDate;
	private String paymentStatusName;
	private String referenceNo;
	
	
	public BankReceiptsDetailVO() {
		
	}
	
	public BankReceiptsDetailVO(BankReceiptsDetail bankReceiptsDetail) {
		this.setBankReceiptsDetailId(bankReceiptsDetail.getBankReceiptsDetailId());
		this.setBankReceipts(bankReceiptsDetail.getBankReceipts());
		this.setCombination(bankReceiptsDetail.getCombination());
		this.setPaymentMode(bankReceiptsDetail.getPaymentMode());
		this.setInstitutionName(bankReceiptsDetail.getInstitutionName());
		this.setBankRefNo(bankReceiptsDetail.getBankRefNo());
		this.setChequeDate(bankReceiptsDetail.getChequeDate());
		this.setAmount(bankReceiptsDetail.getAmount());
		this.setLookupDetail(bankReceiptsDetail.getLookupDetail());
		this.setPaymentStatus(bankReceiptsDetail.getPaymentStatus());
		this.setRecordId(bankReceiptsDetail.getRecordId());
		this.setUseCase(bankReceiptsDetail.getUseCase());
		this.setIsPdc(bankReceiptsDetail.getIsPdc());
		this.setDescription(bankReceiptsDetail.getDescription());
		this.setBankDepositDetails(bankReceiptsDetail.getBankDepositDetails());
		this.setPettyCashs(bankReceiptsDetail.getPettyCashs());
	}

	public String getPaymentModeName() {
		return paymentModeName;
	}

	public void setPaymentModeName(String paymentModeName) {
		this.paymentModeName = paymentModeName;
	}

	public String getStrChequeDate() {
		return strChequeDate;
	}

	public void setStrChequeDate(String strChequeDate) {
		this.strChequeDate = strChequeDate;
	}

	public String getPaymentStatusName() {
		return paymentStatusName;
	}

	public void setPaymentStatusName(String paymentStatusName) {
		this.paymentStatusName = paymentStatusName;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
}

