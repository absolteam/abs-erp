package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.MemberCard;

public class MemberCardVO extends MemberCard {

	private String cardTypeName;
	private Integer updatedDigit;
	
	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public Integer getUpdatedDigit() {
		return updatedDigit;
	}

	public void setUpdatedDigit(Integer updatedDigit) {
		this.updatedDigit = updatedDigit;
	}
}
