package com.aiotech.aios.accounts.domain.entity;

// Generated Dec 10, 2014 3:46:11 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * InvoiceDetail generated by hbm2java
 */
@Entity
@Table(name = "ac_invoice_detail", catalog = "aios")
public class InvoiceDetail implements java.io.Serializable {

	private Long invoiceDetailId;
	private Product product;
	private Invoice invoice;
	private ReceiveDetail receiveDetail;
	private Double unitQty;
	private Double invoiceQty;
	private Double amount;
	private String description;
	private Set<PaymentDetail> paymentDetails = new HashSet<PaymentDetail>(0);

	public InvoiceDetail() {
	}

	public InvoiceDetail(Product product, Invoice invoice,
			ReceiveDetail receiveDetail) {
		this.product = product;
		this.invoice = invoice;
		this.receiveDetail = receiveDetail;
	}

	public InvoiceDetail(Product product, Invoice invoice,
			ReceiveDetail receiveDetail, Double unitQty, Double invoiceQty,
			Double amount, String description, Set<PaymentDetail> paymentDetails) {
		this.product = product;
		this.invoice = invoice;
		this.receiveDetail = receiveDetail;
		this.unitQty = unitQty;
		this.invoiceQty = invoiceQty;
		this.amount = amount;
		this.description = description;
		this.paymentDetails = paymentDetails;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "invoice_detail_id", unique = true, nullable = false)
	public Long getInvoiceDetailId() {
		return this.invoiceDetailId;
	}

	public void setInvoiceDetailId(Long invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id", nullable = false)
	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "receive_detail_id", nullable = false)
	public ReceiveDetail getReceiveDetail() {
		return this.receiveDetail;
	}

	public void setReceiveDetail(ReceiveDetail receiveDetail) {
		this.receiveDetail = receiveDetail;
	}

	@Column(name = "unit_qty", precision = 22, scale = 0)
	public Double getUnitQty() {
		return this.unitQty;
	}

	public void setUnitQty(Double unitQty) {
		this.unitQty = unitQty;
	}

	@Column(name = "invoice_qty", precision = 22, scale = 0)
	public Double getInvoiceQty() {
		return this.invoiceQty;
	}

	public void setInvoiceQty(Double invoiceQty) {
		this.invoiceQty = invoiceQty;
	}

	@Column(name = "amount", precision = 22, scale = 0)
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoiceDetail")
	public Set<PaymentDetail> getPaymentDetails() {
		return this.paymentDetails;
	}

	public void setPaymentDetails(Set<PaymentDetail> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

}
