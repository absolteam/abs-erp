package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.SalesDeliveryPack;

public class SalesDeliveryPackVO extends SalesDeliveryPack {

	private String unit;

	public SalesDeliveryPackVO() {
		
	}
	
	public SalesDeliveryPackVO(SalesDeliveryPack salesDeliveryPack) {
		this.setSalesDeliveryPackId(salesDeliveryPack.getSalesDeliveryPackId());
		this.setSalesDeliveryNote(salesDeliveryPack.getSalesDeliveryNote());
		this.setLookupDetail(salesDeliveryPack.getLookupDetail());
		this.setReferenceNumber(salesDeliveryPack.getReferenceNumber());
		this.setGrossWeight(salesDeliveryPack.getGrossWeight());
		this.setNoOfPacks(salesDeliveryPack.getNoOfPacks());
		this.setDispatcher(salesDeliveryPack.getDispatcher());
		this.setDescription(salesDeliveryPack.getDescription());
	}
	
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
}
