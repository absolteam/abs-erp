package com.aiotech.aios.accounts.domain.entity;

// Generated Nov 22, 2014 1:18:42 PM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ProductSplit generated by hbm2java
 */
@Entity
@Table(name = "ac_product_split", catalog = "aios")
public class ProductSplit implements java.io.Serializable {

	private Long productSplitId;
	private Product product;
	private Implementation implementation;
	private Person person;
	private Shelf shelf;
	private String description;
	private Date createdDate;
	private String referenceNumber;
	private Set<ProductSplitDetail> productSplitDetails = new HashSet<ProductSplitDetail>(
			0);

	public ProductSplit() {
	}

	public ProductSplit(Product product, Implementation implementation) {
		this.product = product;
		this.implementation = implementation;
	}

	public ProductSplit(Product product, Implementation implementation,
			Person person, Shelf shelf, String description, Date createdDate,
			String referenceNumber, Set<ProductSplitDetail> productSplitDetails) {
		this.product = product;
		this.implementation = implementation;
		this.person = person;
		this.shelf = shelf;
		this.description = description;
		this.createdDate = createdDate;
		this.referenceNumber = referenceNumber;
		this.productSplitDetails = productSplitDetails;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "product_split_id", unique = true, nullable = false)
	public Long getProductSplitId() {
		return this.productSplitId;
	}

	public void setProductSplitId(Long productSplitId) {
		this.productSplitId = productSplitId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by")
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shelf_id")
	public Shelf getShelf() {
		return this.shelf;
	}

	public void setShelf(Shelf shelf) {
		this.shelf = shelf;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "reference_number", length = 100)
	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productSplit")
	public Set<ProductSplitDetail> getProductSplitDetails() {
		return this.productSplitDetails;
	}

	public void setProductSplitDetails(
			Set<ProductSplitDetail> productSplitDetails) {
		this.productSplitDetails = productSplitDetails;
	}

}
