package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.ProductionRequisition;

public class ProductionRequisitionVO extends ProductionRequisition implements
		java.io.Serializable {

	private String requisitionDateStr;
	private String requisitionStatusStr;
	private String personName;
	private boolean editStatus;
	private List<ProductionRequisitionDetailVO> productionRequisitionDetailVOs = new ArrayList<ProductionRequisitionDetailVO>();

	public String getRequisitionDateStr() {
		return requisitionDateStr;
	}

	public void setRequisitionDateStr(String requisitionDateStr) {
		this.requisitionDateStr = requisitionDateStr;
	}

	public String getRequisitionStatusStr() {
		return requisitionStatusStr;
	}

	public void setRequisitionStatusStr(String requisitionStatusStr) {
		this.requisitionStatusStr = requisitionStatusStr;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public List<ProductionRequisitionDetailVO> getProductionRequisitionDetailVOs() {
		return productionRequisitionDetailVOs;
	}

	public void setProductionRequisitionDetailVOs(
			List<ProductionRequisitionDetailVO> productionRequisitionDetailVOs) {
		this.productionRequisitionDetailVOs = productionRequisitionDetailVOs;
	}

	public boolean isEditStatus() {
		return editStatus;
	}

	public void setEditStatus(boolean editStatus) {
		this.editStatus = editStatus;
	}
}
