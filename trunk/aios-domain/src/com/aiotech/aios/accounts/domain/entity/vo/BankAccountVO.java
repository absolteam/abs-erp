package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.BankAccount;
import com.aiotech.aios.accounts.domain.entity.Transaction;

public class BankAccountVO extends BankAccount implements java.io.Serializable{
	
	private String bankName;
	private String accountTypeName;
	private String accountHolder;
	private List<Transaction> transactions;
	private List<AccountsDetailVO> accountsDetailVOs;
	
	public BankAccountVO() {
		
	}
	
	public BankAccountVO(BankAccount bankAccount) {
		this.setBankAccountId(bankAccount.getBankAccountId());
		this.setCurrency(bankAccount.getCurrency());
		this.setLookupDetailByCardIssuingEntity(bankAccount.getLookupDetailByCardIssuingEntity());
		this.setBank(bankAccount.getBank());
		this.setLookupDetailByOperationMode(bankAccount.getLookupDetailByOperationMode());
		this.setLookupDetailBySignatory1LookupId(bankAccount.getLookupDetailBySignatory1LookupId());
 		this.setLookupDetailBySignatory2LookupId(bankAccount.getLookupDetailBySignatory2LookupId());
		this.setCombination(bankAccount.getCombination());
		this.setCombinationByClearingAccount(bankAccount.getCombinationByClearingAccount());
 		this.setAccountNumber(bankAccount.getAccountNumber());
		this.setCardNumber(bankAccount.getCardNumber());
		this.setCardType(bankAccount.getCardType());
		this.setValidFrom(bankAccount.getValidFrom());
		this.setValidTo(bankAccount.getValidTo());
		this.setCsvNumber(bankAccount.getCsvNumber());
		this.setPinNumber(bankAccount.getPinNumber());
		this.setCardLimit(bankAccount.getCardLimit());
		this.setAccountType(bankAccount.getAccountType());
		this.setAccountPurpose(bankAccount.getAccountPurpose());
		this.setRountingNumber(bankAccount.getRountingNumber());
		this.setSwiftCode(bankAccount.getSwiftCode());
		this.setIban(bankAccount.getIban());
		this.setBranchContact(bankAccount.getBranchContact());
		this.setContactPerson(bankAccount.getContactPerson());
		this.setBranchName(bankAccount.getBranchName());
		this.setAddress(bankAccount.getAddress());
		this.setDescription(bankAccount.getDescription());
		this.setBankReconciliations(bankAccount.getBankReconciliations());
		this.setReconciliationAdjustments(bankAccount.getReconciliationAdjustments());
		this.setChequeBooks(bankAccount.getChequeBooks());
 		this.setCompanies(bankAccount.getCompanies());
		this.setDirectPayments(bankAccount.getDirectPayments());
		this.setPayments(bankAccount.getPayments());
		this.setBankTransfersForCreditBankAccount(bankAccount.getBankTransfersForCreditBankAccount());
		this.setBankTransfersForDebitBankAccount(bankAccount.getBankTransfersForDebitBankAccount());
		this.setLoans(bankAccount.getLoans());
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountTypeName() {
		return accountTypeName;
	}

	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public List<AccountsDetailVO> getAccountsDetailVOs() {
		return accountsDetailVOs;
	}

	public void setAccountsDetailVOs(List<AccountsDetailVO> accountsDetailVOs) {
		this.accountsDetailVOs = accountsDetailVOs;
	}
}
