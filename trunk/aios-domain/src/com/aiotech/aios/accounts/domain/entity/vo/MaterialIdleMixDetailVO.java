package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialIdleMixDetail;

public class MaterialIdleMixDetailVO extends MaterialIdleMixDetail implements
		java.io.Serializable {

	private double productionQuantity;
	private String storeName;
	private Integer shelfId;
	private Long packageDetailId;
	private boolean insufficientBalance;
	private Double baseConversionUnit;
	private String baseUnitName;
	private ProductPackageDetailVO productPackageDetailVO;
	private String baseQuantity;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();

	public double getProductionQuantity() {
		return productionQuantity;
	}

	public void setProductionQuantity(double productionQuantity) {
		this.productionQuantity = productionQuantity;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getShelfId() {
		return shelfId;
	}

	public void setShelfId(Integer shelfId) {
		this.shelfId = shelfId;
	}

	public boolean isInsufficientBalance() {
		return insufficientBalance;
	}

	public void setInsufficientBalance(boolean insufficientBalance) {
		this.insufficientBalance = insufficientBalance;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public String getBaseQuantity() {
		return baseQuantity;
	}

	public void setBaseQuantity(String baseQuantity) {
		this.baseQuantity = baseQuantity;
	}
}
