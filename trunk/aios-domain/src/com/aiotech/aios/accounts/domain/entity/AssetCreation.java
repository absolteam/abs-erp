package com.aiotech.aios.accounts.domain.entity;

// Generated Sep 7, 2015 10:55:25 AM by Hibernate Tools 3.4.0.CR1

import com.aiotech.aios.hr.domain.entity.AssetUsage;
import com.aiotech.aios.hr.domain.entity.Company;
import com.aiotech.aios.hr.domain.entity.Person;
import com.aiotech.aios.system.domain.entity.Implementation;
import com.aiotech.aios.system.domain.entity.LookupDetail;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AssetCreation generated by hbm2java
 */
@Entity
@Table(name = "ac_asset_creation", catalog = "aios")
public class AssetCreation implements java.io.Serializable {

	private Long assetCreationId;
	private LookupDetail lookupDetailByAssetType;
	private LookupDetail lookupDetailByCategory;
	private Person person;
	private Product product;
	private Company company;
	private Implementation implementation;
	private LookupDetail lookupDetailByAssetSubclass;
	private String assetName;
	private String assetNumber;
	private Byte assetClass;
	private String assetMake;
	private String assetModel;
	private Byte assetColor;
	private Byte assetCondition;
	private String yearMake;
	private String assetVin;
	private String plateNumber;
	private String tcNumber;
	private Byte plateColor;
	private Byte plateType;
	private String brand;
	private String serialNumber;
	private String manufacturer;
	private Integer usefulLife;
	private Double decliningVariance;
	private String description;
	private Date commenceDate;
	private Byte depreciationMethod;
	private Byte convention;
	private Double scrapValue;
	private Byte isApprove;
	private Long relativeId;
	private Set<AssetDisposal> assetDisposalsForAssetId = new HashSet<AssetDisposal>(
			0);
	private Set<AssetDepreciation> assetDepreciations = new HashSet<AssetDepreciation>(
			0);
	private Set<AssetCheckOut> assetCheckOuts = new HashSet<AssetCheckOut>(0);
	private Set<AssetService> assetServices = new HashSet<AssetService>(0);
	private Set<AssetDetail> assetDetails = new HashSet<AssetDetail>(0);
	private Set<AssetWarranty> assetWarranties = new HashSet<AssetWarranty>(0);
	private Set<AssetRevaluation> assetRevaluations = new HashSet<AssetRevaluation>(
			0);
	private Set<AssetDisposal> assetDisposalsForExchangeAssetId = new HashSet<AssetDisposal>(
			0);
	private Set<AssetUsage> assetUsages = new HashSet<AssetUsage>(0);
	private Set<AssetInsurance> assetInsurances = new HashSet<AssetInsurance>(0);

	public AssetCreation() {
	}

	public AssetCreation(LookupDetail lookupDetailByAssetType,
			Implementation implementation,
			LookupDetail lookupDetailByAssetSubclass, String assetName,
			String assetNumber, Byte assetClass, Integer usefulLife,
			Date commenceDate, Byte depreciationMethod, Byte convention) {
		this.lookupDetailByAssetType = lookupDetailByAssetType;
		this.implementation = implementation;
		this.lookupDetailByAssetSubclass = lookupDetailByAssetSubclass;
		this.assetName = assetName;
		this.assetNumber = assetNumber;
		this.assetClass = assetClass;
		this.usefulLife = usefulLife;
		this.commenceDate = commenceDate;
		this.depreciationMethod = depreciationMethod;
		this.convention = convention;
	}

	public AssetCreation(LookupDetail lookupDetailByAssetType,
			LookupDetail lookupDetailByCategory, Person person,
			Product product, Company company, Implementation implementation,
			LookupDetail lookupDetailByAssetSubclass, String assetName,
			String assetNumber, Byte assetClass, String assetMake,
			String assetModel, Byte assetColor, Byte assetCondition,
			String yearMake, String assetVin, String plateNumber,
			String tcNumber, Byte plateColor, Byte plateType, String brand,
			String serialNumber, String manufacturer, Integer usefulLife,
			Double decliningVariance, String description, Date commenceDate,
			Byte depreciationMethod, Byte convention, Double scrapValue,
			Byte isApprove, Long relativeId,
			Set<AssetDisposal> assetDisposalsForAssetId,
			Set<AssetDepreciation> assetDepreciations,
			Set<AssetCheckOut> assetCheckOuts, Set<AssetService> assetServices,
			Set<AssetDetail> assetDetails, Set<AssetWarranty> assetWarranties,
			Set<AssetRevaluation> assetRevaluations,
			Set<AssetDisposal> assetDisposalsForExchangeAssetId,
			Set<AssetUsage> assetUsages, Set<AssetInsurance> assetInsurances) {
		this.lookupDetailByAssetType = lookupDetailByAssetType;
		this.lookupDetailByCategory = lookupDetailByCategory;
		this.person = person;
		this.product = product;
		this.company = company;
		this.implementation = implementation;
		this.lookupDetailByAssetSubclass = lookupDetailByAssetSubclass;
		this.assetName = assetName;
		this.assetNumber = assetNumber;
		this.assetClass = assetClass;
		this.assetMake = assetMake;
		this.assetModel = assetModel;
		this.assetColor = assetColor;
		this.assetCondition = assetCondition;
		this.yearMake = yearMake;
		this.assetVin = assetVin;
		this.plateNumber = plateNumber;
		this.tcNumber = tcNumber;
		this.plateColor = plateColor;
		this.plateType = plateType;
		this.brand = brand;
		this.serialNumber = serialNumber;
		this.manufacturer = manufacturer;
		this.usefulLife = usefulLife;
		this.decliningVariance = decliningVariance;
		this.description = description;
		this.commenceDate = commenceDate;
		this.depreciationMethod = depreciationMethod;
		this.convention = convention;
		this.scrapValue = scrapValue;
		this.isApprove = isApprove;
		this.relativeId = relativeId;
		this.assetDisposalsForAssetId = assetDisposalsForAssetId;
		this.assetDepreciations = assetDepreciations;
		this.assetCheckOuts = assetCheckOuts;
		this.assetServices = assetServices;
		this.assetDetails = assetDetails;
		this.assetWarranties = assetWarranties;
		this.assetRevaluations = assetRevaluations;
		this.assetDisposalsForExchangeAssetId = assetDisposalsForExchangeAssetId;
		this.assetUsages = assetUsages;
		this.assetInsurances = assetInsurances;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "asset_creation_id", unique = true, nullable = false)
	public Long getAssetCreationId() {
		return this.assetCreationId;
	}

	public void setAssetCreationId(Long assetCreationId) {
		this.assetCreationId = assetCreationId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_type", nullable = false)
	public LookupDetail getLookupDetailByAssetType() {
		return this.lookupDetailByAssetType;
	}

	public void setLookupDetailByAssetType(LookupDetail lookupDetailByAssetType) {
		this.lookupDetailByAssetType = lookupDetailByAssetType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category")
	public LookupDetail getLookupDetailByCategory() {
		return this.lookupDetailByCategory;
	}

	public void setLookupDetailByCategory(LookupDetail lookupDetailByCategory) {
		this.lookupDetailByCategory = lookupDetailByCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_person_id")
	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id")
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_company_id")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "implementation_id", nullable = false)
	public Implementation getImplementation() {
		return this.implementation;
	}

	public void setImplementation(Implementation implementation) {
		this.implementation = implementation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asset_subclass", nullable = false)
	public LookupDetail getLookupDetailByAssetSubclass() {
		return this.lookupDetailByAssetSubclass;
	}

	public void setLookupDetailByAssetSubclass(
			LookupDetail lookupDetailByAssetSubclass) {
		this.lookupDetailByAssetSubclass = lookupDetailByAssetSubclass;
	}

	@Column(name = "asset_name", nullable = false, length = 100)
	public String getAssetName() {
		return this.assetName;
	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}

	@Column(name = "asset_number", nullable = false, length = 40)
	public String getAssetNumber() {
		return this.assetNumber;
	}

	public void setAssetNumber(String assetNumber) {
		this.assetNumber = assetNumber;
	}

	@Column(name = "asset_class", nullable = false)
	public Byte getAssetClass() {
		return this.assetClass;
	}

	public void setAssetClass(Byte assetClass) {
		this.assetClass = assetClass;
	}

	@Column(name = "asset_make", length = 100)
	public String getAssetMake() {
		return this.assetMake;
	}

	public void setAssetMake(String assetMake) {
		this.assetMake = assetMake;
	}

	@Column(name = "asset_model", length = 100)
	public String getAssetModel() {
		return this.assetModel;
	}

	public void setAssetModel(String assetModel) {
		this.assetModel = assetModel;
	}

	@Column(name = "asset_color")
	public Byte getAssetColor() {
		return this.assetColor;
	}

	public void setAssetColor(Byte assetColor) {
		this.assetColor = assetColor;
	}

	@Column(name = "asset_condition")
	public Byte getAssetCondition() {
		return this.assetCondition;
	}

	public void setAssetCondition(Byte assetCondition) {
		this.assetCondition = assetCondition;
	}

	@Column(name = "year_make", length = 10)
	public String getYearMake() {
		return this.yearMake;
	}

	public void setYearMake(String yearMake) {
		this.yearMake = yearMake;
	}

	@Column(name = "asset_vin", length = 100)
	public String getAssetVin() {
		return this.assetVin;
	}

	public void setAssetVin(String assetVin) {
		this.assetVin = assetVin;
	}

	@Column(name = "plate_number")
	public String getPlateNumber() {
		return this.plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	@Column(name = "tc_number", length = 100)
	public String getTcNumber() {
		return this.tcNumber;
	}

	public void setTcNumber(String tcNumber) {
		this.tcNumber = tcNumber;
	}

	@Column(name = "plate_color")
	public Byte getPlateColor() {
		return this.plateColor;
	}

	public void setPlateColor(Byte plateColor) {
		this.plateColor = plateColor;
	}

	@Column(name = "plate_type")
	public Byte getPlateType() {
		return this.plateType;
	}

	public void setPlateType(Byte plateType) {
		this.plateType = plateType;
	}

	@Column(name = "brand", length = 100)
	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Column(name = "serial_number", length = 40)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Column(name = "manufacturer", length = 40)
	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@Column(name = "useful_life", nullable = false)
	public Integer getUsefulLife() {
		return this.usefulLife;
	}

	public void setUsefulLife(Integer usefulLife) {
		this.usefulLife = usefulLife;
	}

	@Column(name = "declining_variance", precision = 22, scale = 0)
	public Double getDecliningVariance() {
		return this.decliningVariance;
	}

	public void setDecliningVariance(Double decliningVariance) {
		this.decliningVariance = decliningVariance;
	}

	@Column(name = "description", length = 65535)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "commence_date", nullable = false, length = 10)
	public Date getCommenceDate() {
		return this.commenceDate;
	}

	public void setCommenceDate(Date commenceDate) {
		this.commenceDate = commenceDate;
	}

	@Column(name = "depreciation_method", nullable = false)
	public Byte getDepreciationMethod() {
		return this.depreciationMethod;
	}

	public void setDepreciationMethod(Byte depreciationMethod) {
		this.depreciationMethod = depreciationMethod;
	}

	@Column(name = "convention", nullable = false)
	public Byte getConvention() {
		return this.convention;
	}

	public void setConvention(Byte convention) {
		this.convention = convention;
	}

	@Column(name = "scrap_value", precision = 22, scale = 0)
	public Double getScrapValue() {
		return this.scrapValue;
	}

	public void setScrapValue(Double scrapValue) {
		this.scrapValue = scrapValue;
	}

	@Column(name = "is_approve")
	public Byte getIsApprove() {
		return this.isApprove;
	}

	public void setIsApprove(Byte isApprove) {
		this.isApprove = isApprove;
	}

	@Column(name = "relative_id")
	public Long getRelativeId() {
		return this.relativeId;
	}

	public void setRelativeId(Long relativeId) {
		this.relativeId = relativeId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreationByAssetId")
	public Set<AssetDisposal> getAssetDisposalsForAssetId() {
		return this.assetDisposalsForAssetId;
	}

	public void setAssetDisposalsForAssetId(
			Set<AssetDisposal> assetDisposalsForAssetId) {
		this.assetDisposalsForAssetId = assetDisposalsForAssetId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetDepreciation> getAssetDepreciations() {
		return this.assetDepreciations;
	}

	public void setAssetDepreciations(Set<AssetDepreciation> assetDepreciations) {
		this.assetDepreciations = assetDepreciations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetCheckOut> getAssetCheckOuts() {
		return this.assetCheckOuts;
	}

	public void setAssetCheckOuts(Set<AssetCheckOut> assetCheckOuts) {
		this.assetCheckOuts = assetCheckOuts;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetService> getAssetServices() {
		return this.assetServices;
	}

	public void setAssetServices(Set<AssetService> assetServices) {
		this.assetServices = assetServices;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetDetail> getAssetDetails() {
		return this.assetDetails;
	}

	public void setAssetDetails(Set<AssetDetail> assetDetails) {
		this.assetDetails = assetDetails;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetWarranty> getAssetWarranties() {
		return this.assetWarranties;
	}

	public void setAssetWarranties(Set<AssetWarranty> assetWarranties) {
		this.assetWarranties = assetWarranties;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetRevaluation> getAssetRevaluations() {
		return this.assetRevaluations;
	}

	public void setAssetRevaluations(Set<AssetRevaluation> assetRevaluations) {
		this.assetRevaluations = assetRevaluations;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreationByExchangeAssetId")
	public Set<AssetDisposal> getAssetDisposalsForExchangeAssetId() {
		return this.assetDisposalsForExchangeAssetId;
	}

	public void setAssetDisposalsForExchangeAssetId(
			Set<AssetDisposal> assetDisposalsForExchangeAssetId) {
		this.assetDisposalsForExchangeAssetId = assetDisposalsForExchangeAssetId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetUsage> getAssetUsages() {
		return this.assetUsages;
	}

	public void setAssetUsages(Set<AssetUsage> assetUsages) {
		this.assetUsages = assetUsages;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assetCreation")
	public Set<AssetInsurance> getAssetInsurances() {
		return this.assetInsurances;
	}

	public void setAssetInsurances(Set<AssetInsurance> assetInsurances) {
		this.assetInsurances = assetInsurances;
	}

}
