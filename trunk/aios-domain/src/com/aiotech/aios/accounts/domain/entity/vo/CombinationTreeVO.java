package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

public class CombinationTreeVO implements java.io.Serializable {

	private long key; 
	private String refKey;
 	private String title;
	private String extraClasses;
	private boolean icon;
	private boolean expanded;
	private boolean unselectable;
	private List<CombinationTreeVO> children = new ArrayList<CombinationTreeVO>();
	
	// Getters & Setters
	public long getKey() {
		return key;
	}
	public void setKey(long key) {
		this.key = key;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<CombinationTreeVO> getChildren() {
		return children;
	}
	public void setChildren(List<CombinationTreeVO> children) {
		this.children = children;
	}
	public boolean isIcon() {
		return icon;
	}
	public void setIcon(boolean icon) {
		this.icon = icon;
	}
	public boolean isExpanded() {
		return expanded;
	}
	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}
	public String getRefKey() {
		return refKey;
	}
	public void setRefKey(String refKey) {
		this.refKey = refKey;
	}
	public boolean isUnselectable() {
		return unselectable;
	}
	public void setUnselectable(boolean unselectable) {
		this.unselectable = unselectable;
	}
	public String getExtraClasses() {
		return extraClasses;
	}
	public void setExtraClasses(String extraClasses) {
		this.extraClasses = extraClasses;
	} 
}
