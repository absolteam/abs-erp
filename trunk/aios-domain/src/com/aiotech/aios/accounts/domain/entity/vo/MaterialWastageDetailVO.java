package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.MaterialWastageDetail;

public class MaterialWastageDetailVO extends
		MaterialWastageDetail implements java.io.Serializable {

	private double availableQuantity;
	private Long packageDetailId;
	private String baseUnitName;
	private Double baseConversionUnit;
	private String productType;
	private ProductPackageDetailVO productPackageDetailVO;
	private List<ProductPackageVO> productPackageVOs = new ArrayList<ProductPackageVO>();
	
	public double getAvailableQuantity() {
		return availableQuantity;
	}

	public void setAvailableQuantity(double availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	public Long getPackageDetailId() {
		return packageDetailId;
	}

	public void setPackageDetailId(Long packageDetailId) {
		this.packageDetailId = packageDetailId;
	}

	public String getBaseUnitName() {
		return baseUnitName;
	}

	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	public Double getBaseConversionUnit() {
		return baseConversionUnit;
	}

	public void setBaseConversionUnit(Double baseConversionUnit) {
		this.baseConversionUnit = baseConversionUnit;
	}

	public ProductPackageDetailVO getProductPackageDetailVO() {
		return productPackageDetailVO;
	}

	public void setProductPackageDetailVO(
			ProductPackageDetailVO productPackageDetailVO) {
		this.productPackageDetailVO = productPackageDetailVO;
	}

	public List<ProductPackageVO> getProductPackageVOs() {
		return productPackageVOs;
	}

	public void setProductPackageVOs(List<ProductPackageVO> productPackageVOs) {
		this.productPackageVOs = productPackageVOs;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	} 
}
