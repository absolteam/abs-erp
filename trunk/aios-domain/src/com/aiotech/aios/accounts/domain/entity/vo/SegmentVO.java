package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Segment;

public class SegmentVO extends Segment implements java.io.Serializable {
 
	private String accessCode;
	private String parentName;
	private Long parentSegmentId;
	private Integer parentAccessId;
	private Long combinationId;
	private boolean showSubAccountBtn;
	
	private List<SegmentVO> segmentVOs = new ArrayList<SegmentVO>(); 
	
	public List<SegmentVO> getSegmentVOs() {
		return segmentVOs;
	}

	public void setSegmentVOs(List<SegmentVO> segmentVOs) {
		this.segmentVOs = segmentVOs;
	}

	public String getAccessCode() {
		return accessCode;
	}

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	} 

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public Long getParentSegmentId() {
		return parentSegmentId;
	}

	public void setParentSegmentId(Long parentSegmentId) {
		this.parentSegmentId = parentSegmentId;
	}

	public boolean isShowSubAccountBtn() {
		return showSubAccountBtn;
	}

	public void setShowSubAccountBtn(boolean showSubAccountBtn) {
		this.showSubAccountBtn = showSubAccountBtn;
	}

	public Integer getParentAccessId() {
		return parentAccessId;
	}

	public void setParentAccessId(Integer parentAccessId) {
		this.parentAccessId = parentAccessId;
	} 
}
