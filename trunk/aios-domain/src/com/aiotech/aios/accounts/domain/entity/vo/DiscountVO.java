package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;

import com.aiotech.aios.accounts.domain.entity.Discount;

public class DiscountVO extends Discount implements java.io.Serializable {

 	private String discountTypeName;
	private String validFrom;
	private String validTo;
	private String statusValue;
	private String priceListName;
	private String discountOptionName;
	private String discountTo;
	private ArrayList<DiscountOptionVO> discountOptionVOs;
	private ArrayList<DiscountMethodVO> discountMethodVOs;
	
	public DiscountVO() {
	}
	
	public DiscountVO(Discount discount) {
		this.setDiscountId(discount.getDiscountId());
		this.setLookupDetail(discount.getLookupDetail());
 		this.setImplementation(discount.getImplementation());
		this.setDiscountType(discount.getDiscountType());
		this.setFromDate(discount.getFromDate());
		this.setToDate(discount.getToDate());
		this.setMinimumSales(discount.getMinimumSales());
		this.setMinimumValue(discount.getMinimumValue());
		this.setMaxDiscountLimit(discount.getMaxDiscountLimit());
		this.setStatus(discount.getStatus());
		this.setDiscountOption(discount.getDiscountOption());
		this.setDescription(discount.getDescription());
		this.setDiscountMethods(discount.getDiscountMethods());
		this.setDiscountOptions(discount.getDiscountOptions());
	}
	
	public String getDiscountTypeName() {
		return discountTypeName;
	}
	public void setDiscountTypeName(String discountTypeName) {
		this.discountTypeName = discountTypeName;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	public String getStatusValue() {
		return statusValue;
	}
	public void setStatusValue(String statusValue) {
		this.statusValue = statusValue;
	}
	public String getDiscountOptionName() {
		return discountOptionName;
	}
	public void setDiscountOptionName(String discountOptionName) {
		this.discountOptionName = discountOptionName;
	}
	public String getPriceListName() {
		return priceListName;
	}
	public void setPriceListName(String priceListName) {
		this.priceListName = priceListName;
	}
	public ArrayList<DiscountOptionVO> getDiscountOptionVOs() {
		return discountOptionVOs;
	}
	public void setDiscountOptionVOs(ArrayList<DiscountOptionVO> discountOptionVOs) {
		this.discountOptionVOs = discountOptionVOs;
	}
	public ArrayList<DiscountMethodVO> getDiscountMethodVOs() {
		return discountMethodVOs;
	}
	public void setDiscountMethodVOs(ArrayList<DiscountMethodVO> discountMethodVOs) {
		this.discountMethodVOs = discountMethodVOs;
	}

	public String getDiscountTo() {
		return discountTo;
	}

	public void setDiscountTo(String discountTo) {
		this.discountTo = discountTo;
	} 
}
