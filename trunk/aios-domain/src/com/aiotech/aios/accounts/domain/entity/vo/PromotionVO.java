package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.ArrayList;

import com.aiotech.aios.accounts.domain.entity.Promotion;

public class PromotionVO extends Promotion {

	private String rewardTypeName;
	private String statusName;
	private String promotionOptionName;
	private String mininumSalesName;
	private String priceListName;
	private String strStartDate;
	private String strEndDate;
	private ArrayList<PromotionMethodVO> promotionMethodVOs;
	private ArrayList<PromotionOptionVO> promotionOptionVOs;
	
	public PromotionVO() {
	}
	
	public PromotionVO(Promotion promotion) {
		this.setPromotionId(promotion.getPromotionId());
		this.setLookupDetail(promotion.getLookupDetail());
		this.setImplementation(promotion.getImplementation());
		this.setPromotionName(promotion.getPromotionName());
		this.setRewardType(promotion.getRewardType());
		this.setToDate(promotion.getToDate());
		this.setFromDate(promotion.getFromDate());
		this.setMiniumSales(promotion.getMiniumSales());
		this.setSalesValue(promotion.getSalesValue());
		this.setStatus(promotion.getStatus());
		this.setPromotionOption(promotion.getPromotionOption());
		this.setDescription(promotion.getDescription());
		this.setPromotionOptions(promotion.getPromotionOptions());
		this.setPromotionMethods(promotion.getPromotionMethods());
	}
	
	public String getRewardTypeName() {
		return rewardTypeName;
	}
	public void setRewardTypeName(String rewardTypeName) {
		this.rewardTypeName = rewardTypeName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getPromotionOptionName() {
		return promotionOptionName;
	}
	public void setPromotionOptionName(String promotionOptionName) {
		this.promotionOptionName = promotionOptionName;
	}
	public String getMininumSalesName() {
		return mininumSalesName;
	}
	public void setMininumSalesName(String mininumSalesName) {
		this.mininumSalesName = mininumSalesName;
	}
	public String getPriceListName() {
		return priceListName;
	}
	public void setPriceListName(String priceListName) {
		this.priceListName = priceListName;
	}

	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}

	public ArrayList<PromotionMethodVO> getPromotionMethodVOs() {
		return promotionMethodVOs;
	}

	public void setPromotionMethodVOs(
			ArrayList<PromotionMethodVO> promotionMethodVOs) {
		this.promotionMethodVOs = promotionMethodVOs;
	}

	public ArrayList<PromotionOptionVO> getPromotionOptionVOs() {
		return promotionOptionVOs;
	}

	public void setPromotionOptionVOs(
			ArrayList<PromotionOptionVO> promotionOptionVOs) {
		this.promotionOptionVOs = promotionOptionVOs;
	}
	
}
