package com.aiotech.aios.accounts.domain.entity.vo;

import java.util.List;

import com.aiotech.aios.accounts.domain.entity.Period;

public class PeriodVO extends Period implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private long rentRevenueCombinationId;
	private long unEarnedRevenueCombinationId;
	private String rentRevenueCombination;
	private String unEarnedRevenueCombination;
	private String propertyName;
	private String contractNumber;
	private long contractId;
	private String chequesDate;
	private String rentAmount;
	private long alertRecordId;
	private String start;
	private String end;
	private String statusStr;
	private List<PeriodVO> periodVOList;
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public long getRentRevenueCombinationId() {
		return rentRevenueCombinationId;
	}
	public void setRentRevenueCombinationId(long rentRevenueCombinationId) {
		this.rentRevenueCombinationId = rentRevenueCombinationId;
	}
	public long getUnEarnedRevenueCombinationId() {
		return unEarnedRevenueCombinationId;
	}
	public void setUnEarnedRevenueCombinationId(long unEarnedRevenueCombinationId) {
		this.unEarnedRevenueCombinationId = unEarnedRevenueCombinationId;
	}
	public String getRentRevenueCombination() {
		return rentRevenueCombination;
	}
	public void setRentRevenueCombination(String rentRevenueCombination) {
		this.rentRevenueCombination = rentRevenueCombination;
	}
	public String getUnEarnedRevenueCombination() {
		return unEarnedRevenueCombination;
	}
	public void setUnEarnedRevenueCombination(String unEarnedRevenueCombination) {
		this.unEarnedRevenueCombination = unEarnedRevenueCombination;
	}
	public List<PeriodVO> getPeriodVOList() {
		return periodVOList;
	}
	public void setPeriodVOList(List<PeriodVO> periodVOList) {
		this.periodVOList = periodVOList;
	}
	public long getContractId() {
		return contractId;
	}
	public void setContractId(long contractId) {
		this.contractId = contractId;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getChequesDate() {
		return chequesDate;
	}
	public void setChequesDate(String chequesDate) {
		this.chequesDate = chequesDate;
	}
	public String getRentAmount() {
		return rentAmount;
	}
	public void setRentAmount(String rentAmount) {
		this.rentAmount = rentAmount;
	}
	public long getAlertRecordId() {
		return alertRecordId;
	}
	public void setAlertRecordId(long alertRecordId) {
		this.alertRecordId = alertRecordId;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	} 
}
