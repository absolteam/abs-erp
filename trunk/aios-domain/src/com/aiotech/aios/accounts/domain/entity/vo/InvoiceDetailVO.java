package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.InvoiceDetail;

public class InvoiceDetailVO extends InvoiceDetail implements java.io.Serializable{

	private Double totalReceivedQty;
	private Double totalReceivedAmount;
	private Double invoicedQty;
	private Double receivedQty;
	private Double totalRate;
	private Double selectedInvQty;
	private Double unitRate;
	private String productCode;
	private String productName;
	private String itemType;
	private String receiveReference;
	private ReceiveDetailVO receiveDetailVO;
	
	public InvoiceDetailVO() {
		
	}
	
	public InvoiceDetailVO(InvoiceDetail invoiceDetail) {
		this.setInvoiceDetailId(invoiceDetail.getInvoiceDetailId());
		this.setProduct(invoiceDetail.getProduct());
		this.setInvoice(invoiceDetail.getInvoice());
		this.setReceiveDetail(invoiceDetail.getReceiveDetail());
		this.setUnitQty(invoiceDetail.getUnitQty());
		this.setInvoiceQty(invoiceDetail.getInvoiceQty());
		this.setAmount(invoiceDetail.getAmount());
		this.setDescription(invoiceDetail.getDescription());
		this.setPaymentDetails(invoiceDetail.getPaymentDetails());
	}
	
	public Double getTotalReceivedQty() {
		return totalReceivedQty;
	}

	public void setTotalReceivedQty(Double totalReceivedQty) {
		this.totalReceivedQty = totalReceivedQty;
	}

	public Double getTotalReceivedAmount() {
		return totalReceivedAmount;
	}

	public void setTotalReceivedAmount(Double totalReceivedAmount) {
		this.totalReceivedAmount = totalReceivedAmount;
	}

	public Double getReceivedQty() {
		return receivedQty;
	}

	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}

	public Double getInvoicedQty() {
		return invoicedQty;
	}

	public void setInvoicedQty(Double invoicedQty) {
		this.invoicedQty = invoicedQty;
	}

	public Double getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(Double totalRate) {
		this.totalRate = totalRate;
	}

	public ReceiveDetailVO getReceiveDetailVO() {
		return receiveDetailVO;
	}

	public void setReceiveDetailVO(ReceiveDetailVO receiveDetailVO) {
		this.receiveDetailVO = receiveDetailVO;
	}

	public Double getSelectedInvQty() {
		return selectedInvQty;
	}

	public void setSelectedInvQty(Double selectedInvQty) {
		this.selectedInvQty = selectedInvQty;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getReceiveReference() {
		return receiveReference;
	}

	public void setReceiveReference(String receiveReference) {
		this.receiveReference = receiveReference;
	}
}
