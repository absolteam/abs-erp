package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.PaymentRequestDetail;

public class PaymentRequestDetailVO extends PaymentRequestDetail implements java.io.Serializable {

	private String amountStr;
	private String paymentModeStr;
	private String categoryName;
	
	public String getAmountStr() {
		return amountStr;
	}

	public void setAmountStr(String amountStr) {
		this.amountStr = amountStr;
	}

	public String getPaymentModeStr() {
		return paymentModeStr;
	}

	public void setPaymentModeStr(String paymentModeStr) {
		this.paymentModeStr = paymentModeStr;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	} 
}
