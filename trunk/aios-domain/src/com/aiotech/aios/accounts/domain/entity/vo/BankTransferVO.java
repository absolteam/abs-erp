package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.BankTransfer;

public class BankTransferVO extends BankTransfer {
	
	private String transferDate;
	private String currencyName;
	private String transferNature;
	private String transferFrom;
	private String transferTo;
	
	
	public BankTransferVO() {
		
	}
	
	public BankTransferVO(BankTransfer bankTransfer) {
		this.setBankTransferId(bankTransfer.getBankTransferId());
		this.setBankAccountByCreditBankAccount(bankTransfer.getBankAccountByCreditBankAccount());
		this.setBankAccountByDebitBankAccount(bankTransfer.getBankAccountByDebitBankAccount());
		this.setCurrency(bankTransfer.getCurrency());
		this.setImplementation(bankTransfer.getImplementation());
		this.setLookupDetail(bankTransfer.getLookupDetail());
		this.setCombination(bankTransfer.getCombination());
		this.setTransferNo(bankTransfer.getTransferNo());
		this.setDate(bankTransfer.getDate());
		this.setTransferAmount(bankTransfer.getTransferAmount());
		this.setTransferCharges(bankTransfer.getTransferCharges());
		this.setDescription(bankTransfer.getDescription());
	}

	public String getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(String transferDate) {
		this.transferDate = transferDate;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getTransferNature() {
		return transferNature;
	}

	public void setTransferNature(String transferNature) {
		this.transferNature = transferNature;
	}

	public String getTransferFrom() {
		return transferFrom;
	}

	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	
}
