package com.aiotech.aios.accounts.domain.entity.vo;

import com.aiotech.aios.accounts.domain.entity.ProductionReceiveDetail;

public class ProductionReceiveDetailVO extends ProductionReceiveDetail {

	private String productName;
	private String unitName;
	private String storeName;
	private Double total;
	
	public ProductionReceiveDetailVO() {
	}
	
	public ProductionReceiveDetailVO(ProductionReceiveDetail productionReceiveDetail) {
		this.setProductionReceiveDetailId(productionReceiveDetail.getProductionReceiveDetailId());
		this.setProductionReceive(productionReceiveDetail.getProductionReceive());
		this.setShelf(productionReceiveDetail.getShelf());
		this.setProduct(productionReceiveDetail.getProduct());
		this.setQuantity(productionReceiveDetail.getQuantity());
		this.setUnitRate(productionReceiveDetail.getUnitRate());
		this.setDescription(productionReceiveDetail.getDescription());
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
}
