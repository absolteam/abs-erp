package com.aiotech.aios.realestate.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Maintenance;

public class MaintenanceVO extends Maintenance {

	public String maintenanceCompanyName;
	public String maintenanceCompanyTradeId;

	public static List<MaintenanceVO> convertToMaintenanceVO(
			List<Maintenance> maintenances) {

		List<MaintenanceVO> maintenanceVOList = new ArrayList<MaintenanceVO>();
		MaintenanceVO maintenanceVO;
		for (Maintenance maintenance : maintenances) {

			maintenanceVO = new MaintenanceVO();
			maintenanceVO.setCompanyId(maintenance.getCompanyId());
			maintenanceVO.setDetails(maintenance.getDetails());
			maintenanceVO.setMaintenanceCompanyName("");
			maintenanceVO.setMaintenanceId(maintenance.getMaintenanceId());
			maintenanceVO.setProperty(maintenance.getProperty());

			maintenanceVOList.add(maintenanceVO);
		}

		return maintenanceVOList;
	}

	public static MaintenanceVO convertToMaintenanceVO(Maintenance maintenances) {

		MaintenanceVO maintenanceVO = new MaintenanceVO();
		maintenanceVO.setCompanyId(maintenances.getCompanyId());
		maintenanceVO.setDetails(maintenances.getDetails());
		maintenanceVO.setMaintenanceCompanyName("");
		maintenanceVO.setMaintenanceId(maintenances.getMaintenanceId());
		maintenanceVO.setProperty(maintenances.getProperty());

		return maintenanceVO;
	}

	public String getMaintenanceCompanyName() {
		return this.maintenanceCompanyName;
	}

	public void setMaintenanceCompanyName(String maintenanceCompanyName) {
		this.maintenanceCompanyName = maintenanceCompanyName;
	}
	
	public String getMaintenanceCompanyTradeId() {
		return maintenanceCompanyTradeId;
	}

	public void setMaintenanceCompanyTradeId(String maintenanceCompanyTradeId) {
		this.maintenanceCompanyTradeId = maintenanceCompanyTradeId;
	}
}
