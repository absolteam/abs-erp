package com.aiotech.aios.realestate.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Component;

public class ComponentVO extends Component implements Comparable<ComponentVO> {
	
	List<UnitVO> unitVOs = new ArrayList<UnitVO>();

	
	
	public List<UnitVO> getUnitVOs() {
		return unitVOs;
	}



	public void setUnitVOs(List<UnitVO> unitVOs) {
		this.unitVOs = unitVOs;
	}



	@Override
	public int compareTo(ComponentVO o) {
		
		return o.getComponentNo() - this.getComponentNo() ;
	}
	
	

}
