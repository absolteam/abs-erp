package com.aiotech.aios.realestate.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.Offer;
import com.aiotech.aios.realestate.domain.entity.Property;

public class PropertyVO extends Property implements Comparable<PropertyVO> {
	
	List<ComponentVO> componentVOs = new ArrayList<ComponentVO>();   

	public List<ComponentVO> getComponentVOs() {
		return componentVOs;
	}

	private String tenantName;
	private long offerNumber;




	public void setComponentVOs(List<ComponentVO> componentVOs) {
		this.componentVOs = componentVOs;
	}






	@Override
	public int compareTo(PropertyVO o) {
		// TODO Auto-generated method stub
		return 0;
	}






	public String getTenantName() {
		return tenantName;
	}






	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}






	public long getOfferNumber() {
		return offerNumber;
	}






	public void setOfferNumber(long offerNumber) {
		this.offerNumber = offerNumber;
	}
 

}
