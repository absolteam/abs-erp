package com.aiotech.aios.realestate.domain.entity;

// Generated Jul 31, 2013 11:05:03 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Maintenance generated by hbm2java
 */
@Entity
@Table(name = "re_maintenance", catalog = "aios")
public class Maintenance implements java.io.Serializable {

	private Long maintenanceId;
	private Property property;
	private Long companyId;
	private String details;

	public Maintenance() {
	}

	public Maintenance(Property property, Long companyId) {
		this.property = property;
		this.companyId = companyId;
	}

	public Maintenance(Property property, Long companyId, String details) {
		this.property = property;
		this.companyId = companyId;
		this.details = details;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "maintenance_id", unique = true, nullable = false)
	public Long getMaintenanceId() {
		return this.maintenanceId;
	}

	public void setMaintenanceId(Long maintenanceId) {
		this.maintenanceId = maintenanceId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "property_id", nullable = false)
	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	@Column(name = "company_id", nullable = false)
	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "details", length = 65535)
	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
