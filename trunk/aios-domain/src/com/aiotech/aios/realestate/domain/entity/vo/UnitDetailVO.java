package com.aiotech.aios.realestate.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.UnitDetail;

public class UnitDetailVO extends UnitDetail {

	String unitFeatureCode;
	String unitFeatureDetails;
	private Long lookupDetailId;

	public static List<UnitDetailVO> convertToUnitDetailVO(
			List<UnitDetail> unitDetail) {
		
		List<UnitDetailVO> unitDetailVOs = new ArrayList<UnitDetailVO>();
		UnitDetailVO unitDetailVO = null;

		for (UnitDetail detail : unitDetail) {
			
			unitDetailVO = new UnitDetailVO();
			unitDetailVO.setUnitDetailId(detail.getUnitDetailId());			
			unitDetailVO.setUnit(detail.getUnit());
			unitDetailVO.setUnitFeatureCode(detail.getLookupDetail().getAccessCode());
			unitDetailVO.setDetails(detail.getLookupDetail().getDisplayName());
			unitDetailVO.setUnitFeatureDetails(detail.getDetails());
			unitDetailVO.setLookupDetailId(detail.getLookupDetail().getLookupDetailId());
			/*unitFeatures = detail.getDetails().split("#");
			try {
				if(unitFeatures.length != 0)
				{
					unitDetailVO.setUnitFeatureCode(unitFeatures[0]);
					unitDetailVO.setDetails(unitFeatures[1]);
					if(unitFeatures.length==3)
					unitDetailVO.setUnitFeatureDetails(unitFeatures[2]);
				} else {
					unitDetailVO.setUnitFeatureCode(detail.getDetails());
					unitDetailVO.setDetails(detail.getDetails());
					unitDetailVO.setUnitFeatureDetails(detail.getDetails());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			unitDetailVOs.add(unitDetailVO);
		}
		return unitDetailVOs;
	}

	public String getUnitFeatureCode() {
		return unitFeatureCode;
	}

	public void setUnitFeatureCode(String unitFeatureCode) {
		this.unitFeatureCode = unitFeatureCode;
	}

	public String getUnitFeatureDetails() {
		return unitFeatureDetails;
	}

	public void setUnitFeatureDetails(String unitFeatureDetails) {
		this.unitFeatureDetails = unitFeatureDetails;
	}

	public Long getLookupDetailId() {
		return lookupDetailId;
	}

	public void setLookupDetailId(Long lookupDetailId) {
		this.lookupDetailId = lookupDetailId;
	}
}
