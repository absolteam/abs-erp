package com.aiotech.aios.realestate.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aiotech.aios.realestate.domain.entity.ComponentDetail;

public class ComponentDetailVO extends ComponentDetail {
	
	String componentFeatureCode;
	String componentFeatureDetails;
	
	public static List<ComponentDetailVO> convertToComponentDetailVO(List<ComponentDetail> componentDetails) {
		
		List<ComponentDetailVO> componentDetailVO = new ArrayList<ComponentDetailVO>();
		ComponentDetailVO vo = null;
		String[] arrayList = new String[3];
		
		for(ComponentDetail detail : componentDetails) {
			
			vo = new ComponentDetailVO();
			arrayList = detail.getFeature().split("#");
			vo.setComponentDetailId(detail.getComponentDetailId());
			vo.setComponent(detail.getComponent());
			vo.setFeature(arrayList[1]);
			vo.setComponentFeatureCode(arrayList[0]);
			vo.setComponentFeatureDetails(arrayList[2]);
			componentDetailVO.add(vo);
		}
		return componentDetailVO;
	}
	
	public String getComponentFeatureCode() {
		return componentFeatureCode;
	}
	public void setComponentFeatureCode(String componentFeatureCode) {
		this.componentFeatureCode = componentFeatureCode;
	}
	public String getComponentFeatureDetails() {
		return componentFeatureDetails;
	}
	public void setComponentFeatureDetails(String componentFeatureDetails) {
		this.componentFeatureDetails = componentFeatureDetails;
	}
	
}
