package com.aiotech.aios.realestate.domain.entity.vo;

import com.aiotech.aios.realestate.domain.entity.Unit;

public class UnitVO extends Unit implements Comparable<UnitVO>{

	@Override
	public int compareTo(UnitVO o) {
		return o.getUnitNo()- this.getUnitNo();
	}
	
	private String tenantName;
	private long offerNumber;
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public long getOfferNumber() {
		return offerNumber;
	}
	public void setOfferNumber(long offerNumber) {
		this.offerNumber = offerNumber;
	}

}
