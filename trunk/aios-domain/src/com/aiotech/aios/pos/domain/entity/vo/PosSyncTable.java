package com.aiotech.aios.pos.domain.entity.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "pos-sync-table")
public class PosSyncTable {

	@XmlElement(name = "sync-table-structure", type = SyncTableStructure.class)
	private SyncTableStructure syncTableStructure = new SyncTableStructure();

	@XmlElement
	private String tableName;

	public SyncTableStructure getSyncTableStructure() {
		return syncTableStructure;
	}

	public void setSyncTableStructure(SyncTableStructure syncTableStructure) {
		this.syncTableStructure = syncTableStructure;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
}
