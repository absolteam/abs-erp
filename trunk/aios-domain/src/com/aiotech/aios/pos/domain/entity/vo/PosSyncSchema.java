package com.aiotech.aios.pos.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "pos-sync-schema")
public class PosSyncSchema {

	public PosSyncSchema() {}

	@XmlElement(name = "pos-sync-table", type = PosSyncTable.class)
	private List<PosSyncTable> posSyncTables = new ArrayList<PosSyncTable>();

	public List<PosSyncTable> getPosSyncTables() {
		return posSyncTables;
	}

	public void setPosSyncTables(List<PosSyncTable> posSyncTables) {
		this.posSyncTables = posSyncTables;
	}
}
