package com.aiotech.aios.pos.domain.entity.vo;

import javax.xml.bind.annotation.XmlElement;

public class SyncTableColumnEntry {

	private String column;
	private String options;

	@XmlElement
	public void setColumn(String column) {
		this.column = column;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	@XmlElement
	public String getOptions() {
		return options;
	}

	public String getColumn() {
		return column;
	}
}
