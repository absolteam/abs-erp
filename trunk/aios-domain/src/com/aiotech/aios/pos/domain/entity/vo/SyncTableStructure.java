package com.aiotech.aios.pos.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "sync-table-structure")
public class SyncTableStructure {

	@XmlElement(name = "entry", type = SyncTableColumnEntry.class)
	private List<SyncTableColumnEntry> syncTableColumnEntries = new ArrayList<SyncTableColumnEntry>();

	public SyncTableStructure() {
	}

	public List<SyncTableColumnEntry> getSyncTableColumnEntries() {
		return syncTableColumnEntries;
	}

	public void setSyncTableColumnEntries(
			List<SyncTableColumnEntry> syncTableColumnEntries) {
		this.syncTableColumnEntries = syncTableColumnEntries;
	}
}
