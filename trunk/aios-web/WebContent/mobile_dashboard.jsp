<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <%if (session.getAttribute("lang").equals("ar")) {%> dir="rtl"
	<%}%>>
<head>
<meta http-equiv="Content-Type" charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<title>${THIS.companyName} | ${USER.username}</title>

<%-- <link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	rel="stylesheet" type="text/css" media="all" />  
<link
	href="${pageContext.request.contextPath}/css/styles/default/ui.css"
	rel="stylesheet" type="text/css" media="all" />  --%>
	
<link href="${pageContext.request.contextPath}/css/forms.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/HRM.css" type="text/css"
	media="all" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery.datepick.css">
<%-- <link href="${pageContext.request.contextPath}/css/jquery.notice.css"
	rel="stylesheet" type="text/css" media="all" /> --%>

<%-- <link
	href="${pageContext.request.contextPath}/css/styles/erp_new/erp_new_mobile.css"
	rel="stylesheet" type="text/css" media="all" /> --%>
	
<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" /> 
	<script type="text/javascript" src="${pageContext.request.contextPath}/mobile/jquery-1.10.2.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/mobile/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/mobile/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/cookie.js"></script>
<%-- <link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/menu/pro_menu.css" /> --%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.datepick.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.browser.js"></script>
	
<!-- FOR AUTOCOMPLETE -->
<script
	src="${pageContext.request.contextPath}/js/jquery-autocomplete/jquery.ui.autocompletescreen-mobile.js"></script>

<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.button.css"
	rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript">var contextPath = "${pageContext.request.contextPath}";</script>
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/menu/orange_mobile.css"
	type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/css/general_mobile.css"
	rel="stylesheet" type="text/css" media="all" />
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.notice.css"
	type="text/css" media="all" /> --%>

	<script type="text/javascript">
	
		var datatableSearch = ('${lang}' == 'ar') ? "تقص: " : "SEARCH: ";
		var datatableFirst = ('${lang}' == 'ar') ? "الأول" : "First";
		var datatableLast = ('${lang}' == 'ar') ? "آخر" : "Last";
		var datatableNext = ('${lang}' == 'ar') ? "التالي" : "Next";
		var datatablePrevious = ('${lang}' == 'ar') ? "سابق" : "Previous";
		var datatableEmpty = ('${lang}' == 'ar') ? "لا توجد بيانات متاحة في الجدول" : "No data available in table";
		var datatableShowing = ('${lang}' == 'ar') ? "أداء" : "Showing";
		var datatableTo = ('${lang}' == 'ar') ? "إلى" : "to";
		var datatableOf = ('${lang}' == 'ar') ? "من" : "of";
		var datatableEntries = ('${lang}' == 'ar') ? "مقالات" : "Entries";
		var datatableInfoEmpty = ('${lang}' == 'ar') ? "إظهار 0 إلى 0 من 0 مقالات" : "Showing 0 to 0 of 0 entries";
		var datatableShow = ('${lang}' == 'ar') ? "عرض" : "Show";
		var datatableLoading = ('${lang}' == 'ar') ? "تحميل ..." : "Loading...";
		var datatableProcessing = ('${lang}' == 'ar') ? "تجهيز ..." : "Processing...";
		var datatableNoMatch = ('${lang}' == 'ar') ? "لا توجد سجلات مطابقة" : "No matching records found";
		
	</script>

<link rel="stylesheet"
	href="//cdn.datatables.net/1.10.2/css/jquery.dataTables.css"
	type="text/css" />
<script type="text/javascript" language="javascript"
	src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>

<%-- <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/DateTime.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/date.js"></script>  
<script
	src="${pageContext.request.contextPath}/js/menu/jquery.dcmegamenu.1.3.3.js"></script> --%>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
<style>
.main_oe_overlay{
	background:#000;
	opacity:0;
	position:fixed;
	top:0px;
	left:0px;
	width:100%;
}
.contact_container{
	width:100%!important;
}
.expanded{
	width:99%!important;
}
.content_left{
width:99%!important;
}
</style>
</head>

<body>
	<div class="wrapper">

		<div>
			
			<!-- <div id="page-wrapper"> -->

				<div id="right_side_container">
					<%@ include file="inc/mobile_dashboard_header.jsp"%>
					<%@ include file="inc/mobile_dashboard_body.jsp"%>
				</div>
			
			<!-- </div> -->
			<div id="main_oe_overlay" class="main_oe_overlay"></div>
			<div id="ajaxLoader"
				style="width: 100px;position: absolute; z-index: 9999; display: none;">
				<div style="height: 50px; width: 100px; border: solid thin grey; background-color: white;" title="Loading...">			
					<span style="text-align: center; color: grey; width: 100px; margin: 5px; display: block;">Loading...</span>
					<img
						src="${pageContext.request.contextPath}/images/ajaxloadingbar.gif"
						title="Loading...">
				</div>
			</div>
		</div>
	</div>

	<%-- <div class="clearfix"></div>
	<%@ include file="inc/Footer.jsp"%> --%>

	<div id="message" style="display: none;">
		<!-- <a id="top-link" href="#top"><img height="53" width="53" alt="top" src="/images/top.png"></a> -->
	</div>

	<div
		style="display: none; position: absolute; overflow: auto; z-index: 10000; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">

		<div id="password-update-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="password-popup-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>

</body>
</html>