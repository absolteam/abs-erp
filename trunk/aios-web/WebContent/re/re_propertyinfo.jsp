<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
 	
.legal_icon_view{
   display: block;
    height: 20px;
    width: 20px;
    /* background: url(images/icon_legal_lg.png) no-repeat;
   	background-size:23px 26px;
   	background-position:center; */
    background: url(images/icon_legal.png) no-repeat;
    float: right;
    text-align: center;
    top:4px;
}
.decision_icon_view{
    display: block;
    height: 20px;
    width: 20px;
    /* background: url(images/mdcn-view.jpg) no-repeat;
   	background-size:23px 26px;
   	background-position:center; */
   	background: url(images/mdcn-view.png) no-repeat;
    float: right;
    text-align: center;
    top:4px;
    margin-right:2px;
}
 </style>
<script type="text/javascript">
var id;
var buildingName="";
var floorNo="";
var plotNo="";
var addressLine1="";
var fromDate="";
var toDate="";
var yearOfBuild="";
var approvedStatus="";
var siteplanRecipient = "";
var releaseType = "";
var transferType = "";
var accountNo = "";
var generalServiceType = "";

var oTable; var selectRow=""; var aData ="";
var propertyId=0; propertyType=""; propertyOwners=""; var aSelected = [];
var propertyOwnersArabic = "";

$(document).ready(function (){
		 
	$('.formError').remove();
	propertyGridLoad();
	if(typeof($('#codecombination-popup')!="undefined")){ 
		$('#codecombination-popup').dialog('destroy');		
  		$('#codecombination-popup').remove(); 
	}
	
	if(typeof($('#common-popup')!="undefined")){ 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
	}
		
	if(typeof($('#management_cn')!="undefined")){ 
		$('#management_cn').dialog('destroy');		
		$('#management_cn').remove(); 
	}
	if(typeof($('#management_cnview')!="undefined")){ 
		$('#management_cnview').dialog('destroy');		
  		$('#management_cnview').remove();
	}
	if(typeof($('#maintenance-popup')!="undefined")){ 
		$('#maintenance-popup').remove();
		$('#maintenance-popup').dialog('destroy');	
	} 
	
	$('#property tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          propertyId=aData[0]; 
	          propertyType = aData[2];
	          propertyOwners = aData[4];
	          propertyOwnersArabic = aData[5];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          propertyId=aData[0];
	          propertyType = aData[2];
	          propertyOwners = aData[4];
	          propertyOwnersArabic = aData[5];
	      }
	});
	
	//Added by rafiq for Legal Listing control view
	if($('#legalProcess').val()==1){
		$('#property tbody tr').live('dblclick', function () {
			//$('#loading').fadeIn();
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          $("#legalRecordId").val(aData[0]);
		          $("#propertyId").val(aData[0]);
			      useCaseViewCall();
			       $('#DOMWindowOverlay').trigger('click');
			       return false;
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          $("#legalRecordId").val(aData[0]);
		          $("#propertyId").val(aData[0]);
			      useCaseViewCall();
			      $('#DOMWindowOverlay').trigger('click');
			      return false;
		      }
			  //$('#loading').fadeOut();
		});
		
		$('.vmenu').html("");
		$('#action_buttons').hide();
		
	}
	//Legal script end
	
 	$("#add").click( function() { 
 		$('#loading').fadeIn();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/property_info_add_redirect.action", 
			data: {recordId:0}, 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#main-wrapper").html(result); 
			} 		
		}); 
		$('#loading').fadeOut();
		return true; 
	});

 	$("#edit").click( function() { 
 		$('.tempresultfinal').fadeOut();
	    var selectedBuildingId;
	    selectedBuildingId = propertyId;	    
	    if(selectedBuildingId == null || selectedBuildingId =='null' || selectedBuildingId == 0)
		{
			$('.commonErr').hide().html("Please select one record to edit...").slideDown();
			return false; 
		}
		else
		{			
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_property_info.action",  
		     	data: {buildingId: selectedBuildingId,recordId:0}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper;
		     	}
			});
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});

 	$("#view").click( function() {  

 		if(propertyId == null || propertyId == '' || propertyId == 0)
		{
			$('.commonErr').hide().html("Please select one record to view...").slideDown();
			return false; 
		}
			 	
 		$('#loading').fadeIn();
 		window.open('getPropertyDetal.action?format=HTML&approvalFlag=N&recordId='
 		 		+ propertyId ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true; 
	});
 	
 	var printPlan = function () { 	
 		 		
 		$('#loading').fadeIn(); 
 		
 		window.open('print_siteplan.action?buildingId=' + propertyId
	  				+ '&ownerNameArabic=' + propertyOwnersArabic + '&siteplanReceiver=' + siteplanRecipient,
	  					'','width=800,height=700,scrollbars=yes,left=100px');
 		$('#loading').fadeOut();
		return false;
 		 		
 		 $.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/print_siteplan_template.action", 
 		 	async: false, 
 		    dataType: "html",
 		    data: {
 		    	buildingId: propertyId,
 		    	ownerName: propertyOwnersArabic,
 		    	siteplanReceiver: siteplanRecipient
 		    },
 		    cache: false,
 			success:function(result){
 				
 				window.open('<%=request.getContextPath()%>/printing/applet/re_print_siteplan.html',
 						'','width=400,height=400,scrollbars=yes,left=100px');
 			},
 			error:function(result){
 				 alert(result);
 			}
 		}); 
	
  		$('#loading').fadeOut();
		return true; 
	};
 	
	$('#recipient-popup-screen').dialog({
		minwidth: 'auto',
		width:300,
		height:150,  
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() { 				 
				if(siteplanRecipient == "") {
					$("#error_plan").fadeIn(); 
					return false;	
				}
				$("#error_plan").hide();
				printPlan();
				$(this).dialog("close");	
			}
		} 	
	});
	
	$('#print-siteplan').live('click',function(){ 
	 	
 		if(propertyId == null || propertyId == '' || propertyId == 0) {
			$('.commonErr').hide().html("Please select one record to print the document...").slideDown();
			return false; 
		}
 	 	 		
	 	$('#recipient-popup-screen').dialog('open');
		$('#ui-dialog-title-recipient-popup-screen').text("Select Recipient");
		siteplanRecipient = "";
		$('.commonErr').hide();
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_recipient_info.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.recipient-popup-screen-result').html(result); 
			},
			error:function(result){  
				 $('.recipient-popup-screen-result').html(result); 
			}
		}); 
	 	return false;
	});
	
	var proceedToPrintClearance = function () {
			
		$('#loading').fadeIn(); 
		
		window.open('print_clearance.action?buildingId=' + propertyId
 				+ '&clearanceType=' + releaseType + '&accountNo=' + accountNo + '&isUnitClearance=0'
 				+ '&generalServiceType=' + generalServiceType,
 				'','width=800,height=700,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
		
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/print_clearance_template.action", 
			 	async: false, 
			    dataType: "html",
			    data: {
			    	buildingId: propertyId,
			    	clearanceType: releaseType,
			    	accountNo: accountNo
			    },
			    cache: false,
				success:function(result) {   
					
					window.open('<%=request.getContextPath()%>/printing/applet/re_print_clearance.html',
								'','width=400,height=400,scrollbars=no,left=100px'); 
				},
				error:function(result){  
					 alert(result);
				}
			});
		
 		$('#loading').fadeOut();
	};
	
	$('#release-popup-screen').dialog({
		minwidth: 'auto',
		width:300,
		height:300,  
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() {
				accountNo = $('#accountNo').val();
				generalServiceType = $('#generalServiceType').val();
				proceedToPrintClearance();
				$(this).dialog("close"); 
			}
		} 	
	});
	
	$('#clearance-general-service').live('click',function(){ 
	 	
 		if(propertyId == null || propertyId == '' || propertyId == 0) {
			$('.commonErr').hide().html("Please select one record to print the document...").slideDown();
			return false; 
		}
 		
 		$('#release-popup-screen').dialog('open');
		$('#ui-dialog-title-release-popup-screen').text("Select Clearance Type");
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/release_options.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.release-result').html(result); 
			},
			error:function(result){  
				 $('.release-result').html(result); 
			}
		});	
	 	return false;
	});
 	
	var proceedToPrintTransfer = function () {
		
		$('#loading').fadeIn(); 
		
		window.open('print_transfer.action?buildingId=' + propertyId
 				+ '&clearanceType=' + transferType + '&accountNo=' + accountNo + '&isUnitClearance=0'
 				+ '&generalServiceType=' + generalServiceType,
 				'','width=800,height=700,scrollbars=yes,left=100px');
		return false;
		
		<%--  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/print_transfer_template.action", 
			 	async: false, 
			    dataType: "html",
			    data: {
			    	buildingId: propertyId,
			    	clearanceType: releaseType,
			    	accountNo: accountNo
			    },
			    cache: false,
				success:function(result) {   
					
					window.open('<%=request.getContextPath()%>/printing/applet/re_print_transfer.html',
								'','width=400,height=400,scrollbars=no,left=100px'); 
				},
				error:function(result){  
					 alert(result);
				}
			}); --%>
		
 		$('#loading').fadeOut();
	};
	
	$('#contract-popup-screen').dialog({
		minwidth: 'auto',
		width:366,
		height:330, 
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() {
				accountNo = $('#transferAccount').val();
				generalServiceType = "";
				proceedToPrintTransfer();
				$(this).dialog("close"); 
			}
		} 	
	});
	
	$('#transfer-general-service').live('click',function(){ 
	 	
 		if(propertyId == null || propertyId == '' || propertyId == 0) {
			$('.commonErr').hide().html("Please select one record to print the document...").slideDown();
			return false; 
		}
 		
 		$('#contract-popup-screen').dialog('open');
		$('#ui-dialog-title-contract-popup-screen').text("Transfer Options");
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/transfer_options.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.contract-result').html(result); 
			},
			error:function(result){  
				 $('.contract-result').html(result); 
			}
		});	
	 	return false;
	});
	
 	$("#report").click( function() {  	 	
 		$('#loading').fadeIn();
 		window.open('getAllProperties.action?format=HTML&recordId='
 		 		+ "0" ,'','width=1000,height=600,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true; 
	});

	$("#delete").click(function(){  
		var selectedBuildingId;
		selectedBuildingId = propertyId;
		var type = 0;
		if(propertyType.toLowerCase() == "villa") { 
			type = 1;
		} else if(propertyType.toLowerCase() == "shop") { 
			type = 6;
		} else if(propertyType.toLowerCase() == "flat") { 
			type = 7;
		} 
		if(selectedBuildingId == null || selectedBuildingId == '' || selectedBuildingId == 0){
			$('.commonErr').hide().html("Please select one record to delete...").slideDown();
			return false; 
		}
		else{	
			var cnfrm = confirm("Selected property will be permanently deleted.");
			if(!cnfrm) {
				return false;
			}
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_property_info.action",  
		     	data: {buildingId: selectedBuildingId, propertyTypeId: type}, 
		     	async: false,
				dataType: "html",
				cache: false, 
		     	success: function(result){
		     		$(".tempresults").html(result); 
		     		if($(".tempresults").html().trim()=="SUCCESS"){
			     		$('.success').hide().html("Property Deleted Successfully.").slideDown();
			     		$('.success').delay(2000).slideUp(1000);
			     		checkDataTableExsist();
			     	}else{
			     		$('.commonErr').hide().html(result).slideDown(); 
			     		$('.commonErr').delay(2000).slideUp(2000);
				    }
					$('#loading').fadeOut(); 
		     	}
			}); 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	});

	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

	function checkDataTableExsist(){
		oTable = $('#property').dataTable();
		oTable.fnDestroy();
		$('#property').remove();
		$('#gridDiv').html("<table class='display' id='property'></table>");
		propertyGridLoad();
	}
	function propertyGridLoad(){
		$('#property').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/json-propertyinfo.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "ID","bVisible": false},
				{ "sTitle": '<fmt:message key="re.property.info.propertyName" />',"sWidth": "300px"},
				{ "sTitle": '<fmt:message key="re.property.info.propertytype" />',"sWidth": "100px"},			
				{ "sTitle": '<fmt:message key="re.property.info.annualRent" />',"sWidth": "100px"},
				{ "sTitle": '<fmt:message key="re.property.info.owners" />',"sWidth": "400px"},
				{ "sTitle": "OwnersArabic","bVisible": false},
				{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "100px"},
			],
			"sScrollY": $("#main-content").height() - 235,
			//"bPaginate": false,
			"aaSorting": [[1, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	

		oTable = $('#property').dataTable();
	}

	setTimeout("$('#nortify').fadeOut()", 5000);
</script>

<body>
	<div id="main-content">	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="max-height: 99%; min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				<fmt:message key="re.property.info.propertyinformation" />
			</div>
			<input type="hidden" id="legalProcess" value="${LEGAL_PROCESS}" />
			<div class="success-message success response-msg ui-corner-all" style="display: none;"></div>
			<div class="response-msg error commonErr ui-corner-all" style="display: none;">
			
			</div>
			<div style="display: none;" class="tempresults"></div>
			<div class="portlet-content">
				<div style="display: none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
				</c:if>
				</div>
				<div id="nortify">
					<c:choose>
						<c:when test="${PROPERTY_CHILD_EXISTS eq 1 && PROPERTY_CHILD_EXISTS ne 0}">
							<div class="error response-msg ui-corner-all">Please delete components first</div>
						</c:when>
						<c:when test="${PROPERTY_CHILD_EXISTS eq 0 && PROPERTY_CHILD_EXISTS ne 1}">
								<div class="success response-msg ui-corner-all">Property deleted</div> 
						</c:when>
						<c:when test="${PROPERTY_CHILD_EXISTS eq 2 && PROPERTY_CHILD_EXISTS ne 1 && PROPERTY_CHILD_EXISTS ne 0}">
								<div class="error response-msg ui-corner-all">Property deletion failed</div> 
						</c:when>
					</c:choose>
				</div>
				<div>
				<c:choose>
					<c:when test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">
							<fmt:message key="${requestScope.succMsg}" />
						</div>
					</c:when> 
					<c:when test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">
							<fmt:message key="${requestScope.errMsg}" />
						</div>
					</c:when>	
				</c:choose>	
				<div class="error response-msg ui-corner-all" id="return-error-message" style="display:none;">${requestScope.RETURN_ERROR_MESSAGE}</div>
				</div>
				
				<div id="rightclickarea">
					<div id="gridDiv" style="max-height: 98%;">
						 <table id="property" class="display"></table>	
					</div>
				</div>		
				<div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>View</span></div>					
					<div class="first_li"><span>Delete</span></div>
					<div class="sep_li"></div>
					<div class="first_li"><span>Print-Siteplan</span></div>
					<div class="first_li"><span>Clearance-General-Service</span></div>
					<!-- <div class="first_li"><span>Transfer-General-Service</span></div> -->
					<!--
					<div class="first_li"><span>Report</span></div>
				--></div>
				
				<div style="position: relative;" id="action_buttons" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div class="portlet-header ui-widget-header float-right" id="view" style="display: none;">
						<fmt:message key="re.property.info.view" />
					</div>		
					<div style="display: none;" class="portlet-header ui-widget-header float-right" id="print-siteplan">
						Print-Siteplan
					</div>
					<div style="display: none;" class="portlet-header ui-widget-header float-right" id="clearance-general-service">
						Clearance-General-Service
					</div>		
					<!-- <div style="display: none;" class="portlet-header ui-widget-header float-right" id="transfer-general-service">
						Transfer-General-Service
					</div>	 -->
					<div class="property_action_btn portlet-header ui-widget-header float-right"  id="delete">
						<fmt:message key="re.property.info.delete" />
					</div>					
					<div class="property_action_btn portlet-header ui-widget-header float-right"  id="view">
						<fmt:message key="re.property.info.view" />
					</div>
					<div class="property_action_btn portlet-header ui-widget-header float-right"  id="edit">
						<fmt:message key="re.property.info.edit" />
					</div>
					<div class="property_action_btn portlet-header ui-widget-header float-right"  id="add">
						<fmt:message key="re.property.info.add" />
					</div>
				</div>
				
			</div>
		</div>
		<div style="display: none; position: absolute; overflow: auto; 
			z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" 
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" 
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	
		<div id="recipient-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="recipient-popup-screen-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
		<div id="release-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="release-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div>
		</div>
		</div>
		<div id="contract-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="contract-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div>
		</div>
</body>