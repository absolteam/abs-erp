<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">

	var oTable; var selectRow=""; var aData ="";
	var groupId = 0; var aSelected = []; var groupName = "";

	$(function() {
		 
		$('.formError').remove();

		
		$jquery("#tenantGroupAdd").validationEngine('attach');

		
		$('#example').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/getTenantGroups.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "bServerSide": true,
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "tenant_grp_id",  "bVisible": false},		
				{ "sTitle": "Group Title"},
				{ "sTitle": "Deposit"},			
				{ "sTitle": "Contract Fee"},
				{ "sTitle": "Rent"},	
				{ "sTitle": "Penalty"},	
				{ "sTitle": "Details"},		
			],
			"sScrollY": $("#main-content").height() - 450,
			"aaSorting": [[1, 'asc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	

		oTable = $('#example').dataTable();		 
		$('#example tbody tr').live('click', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          groupId=aData[0]; 
		          groupName = aData[1]; 
		          editGroup(aData);
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          groupId=aData[0];
		          groupName = aData[1]; 
		          editGroup(aData);
		      }
		});
	});

	$("#save").click(function () {

		if($jquery('#tenantGroupAdd').validationEngine('validate')) {
		 
			$('.errorMessage').hide();
			if(groupId == "" || groupId == null)
				groupId = 0;
			tenantGrpId = groupId;
			tenantGrpTitle = $("#tenantGrpTitle").val();
			deposit = $("#deposit").val();
			contractFee = $("#contractFee").val();
			rent = $("#rent").val();
			penalty = $("#penalty").val();
			details = $("#details").val();
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/addEditTenantGroup.action",
			 	async: false, 
			 	data:{ tenantGrpId : tenantGrpId, tenantGrpTitle : tenantGrpTitle, deposit : deposit,
					contractFee : contractFee, rent : rent, penalty : penalty, details : details},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					oTable.fnDraw();
					groupId = 0;
				},
				error:function(result){ 
					$('.errorMessage').hide().html("Group already exists").slideDown();
				}
			}); 
		} else 
			return false; 
	});

	$("#clear").click(function () {

		groupId = 0;
		$('.errorMessage').hide();
		$("#tenantGrpId").val("");
		$("#tenantGrpTitle").val("");
		$("#deposit").val("");
		$("#contractFee").val("");
		$("#rent").val("");
		$("#penalty").val("");
		$("#details").val("");
	});

	$("#delete").click(function () {

		if(groupId == '' || groupId == 0) {
			$('.errorMessage').hide().html("Please select group to delete...").slideDown();
			return false; 
		}

		$('.errorMessage').hide();
		var confirmDeletion = confirm("'" + groupName + "' will be permanently deleted.");
		if (!confirmDeletion) {
			return false;
		}

		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/deleteTenantGroup.action",
		 	async: false, 
		 	data:{tenantGrpId: groupId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#clear").trigger('click');
		 		oTable.fnDeleteRow( aData );
			},
			error:function(result){ 
				$("#clear").trigger('click');
				$('.errorMessage').hide().html("Deletion failed...").slideDown();
			}
		}); 
	});
	
	function editGroup(record) {

		$("#tenantGrpId").val(record[0]);
		$("#tenantGrpTitle").val(record[1]);
		$("#deposit").val(record[2]);
		$("#contractFee").val(record[3]);
		$("#rent").val(record[4]);
		$("#penalty").val(record[5]);
		$("#details").val(record[6]);
	}

	function getId(parentTR){ 
		if ($(parentTR).hasClass('row_selected') ) { 
			$(parentTR).addClass('row_selected');
	        aData = oTable.fnGetData( parentTR );
	        return aData[0]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(parentTR).addClass('row_selected');
	        aData = oTable.fnGetData( parentTR );
	        return aData[0];
	    }
	}
</script>

<style type="text/css">
	label {
		display: inline-block;
	}
	textarea {
		max-height: 125px;
		height: 125px; 
		min-height: 125px; 
		max-width: 80%; 
		width: 80%;
		min-width: 80%; 
	}
</style>

<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Groups</div>	 
			<div class="portlet-content" style="padding-bottom: 5%;">
				<input type="hidden" id="tenantGrpId" value="">
				<div class="errorMessage response-msg error ui-corner-all" style="width: 450px; display: none;">
				</div>
				<form id="tenantGroupAdd"  style="position: relative;">
						<div>
						<fieldset style="height: 180px; width: 48%; margin-left: 10px; float: left; 
								border: 1px solid #DDDDDD; padding: 1%;">
							<legend>Tenant Group</legend>
							<div>
								<label class="width30 tooltip">Group Title
								<span class="mandatory">*</span></label>
								<input type="text" name="tenantGrpTitle" 
									id="tenantGrpTitle" class="width60 validate[required]" TABINDEX=1>
							</div>	
							<div>
								<label class="width30 tooltip">Deposit Amount
								<span class="mandatory">*</span></label>
								<input type="text" name="deposit" 
									id="deposit" class="width60 validate[required]" TABINDEX=2>
							</div>	
							<div>
								<label class="width30 tooltip">Contract Fee
								<span class="mandatory">*</span></label>
								<input type="text" name="contractFee" 
									id="contractFee" class="width60 validate[required]" TABINDEX=3>
							</div>	
							<div>
								<label class="width30 tooltip">Rent Amount
								<span class="mandatory">*</span></label>
								<input type="text" name="rent" 
									id="rent" class="width60 validate[required]" TABINDEX=4>
							</div>	
							<div>
								<label class="width30 tooltip">Penalty
								<span class="mandatory">*</span></label>
								<input type="text" name="penalty" 
									id="penalty" class="width60 validate[required]" TABINDEX=5>
							</div>
						</fieldset>	
					</div>
					<div>
						<fieldset style="height: 180px; width: 45%; float: right; 
								border: 1px solid #DDDDDD; padding: 1%;">
							<legend>Details</legend>
							<div>
								<textarea id="details" rows="4" cols="4" TABINDEX=6></textarea>
							</div>	
						</fieldset>
					</div>	
				</form>
			</div>
			
			<div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix
				 ui-corner-all form-container float-right buttons">							
				<div class="portlet-header ui-widget-header float-right" id="clear">
					Clear
				</div>
				<div class="portlet-header ui-widget-header float-right" id="delete">
					Delete
				</div>
				<div class="portlet-header ui-widget-header float-right" id="save">
					Save
				</div>
			</div>
			<div id="rightclickarea">
				<div id="gridDiv" style="max-height: 50%;">
				<table id="example" class="display"></table>					
				</div>
			</div>
			<div class="vmenu">				
				<div class="first_li"><span>Delete</span></div>	
			</div>
	</div>
</body>