<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var caseIdCourt="";
var description="";
var buildingId="";
var contractId="";
var approvedStatus="";

$(document).ready(function (){
	$('.formError').remove();

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/legal_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#legalsearchcontent').html(result);  
     		$('#temp1').html($('#temp').html());  
     		$('#temp').html("");
	 	},  
	 	error:function(result){ 
	 		 $('#legalsearchcontent').html(result); 
	 	} 
	});
	
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_legal_info_list.action", 
		 datatype: "json", 
		 colNames:['Case Id',
		  		 	'<fmt:message key="re.property.info.caseidincourt"/>',
		  		 	'<fmt:message key="re.property.info.description"/>',
		  		 	'Building ID',
		  		 	'<fmt:message key="re.property.info.buidingname"/>',
		  		 	'Contract ID',
		  		 	'<fmt:message key="re.property.info.contractnumber"/>',
		  		 	'<fmt:message key="re.property.info.fromdate"/>',
			  		 '<fmt:message key="re.property.info.todate"/>',
			  		'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'caseId',index:'CASE_ID', width:50,  sortable:true},
				{name:'caseInCourt',index:'CASE_ID_COURT', width:100,  sortable:true},
		  		{name:'description',index:'CASE_DESCRIPTION', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'buildingId',index:'BUILDING_ID', width:150,sortable:true, sorttype:"data"},
		  		{name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
		  		 {name:'contractNumber',index:'CONTRACT_NO', width:100,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:100,sortable:true, sorttype:"data"} ,
		  		{name:'toDate',index:'TO_DATE', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'CASE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.legalinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["caseId","contractId","buildingId"]);

 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/legal_info_add.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		}); 
		return true; 
	});
	
	$("#edit").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.caseId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/legal_info_edit.action",  
		     	data: {caseId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.caseId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/legal_info_view.action",  
		     	data: {caseId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null'){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.caseId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/legal_info_delete_confirm.action",  
		     	data: {caseId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#search").click(function(){
		$('.commonErr').hide();
		buildingId=Number($('#buildingId option:selected').val());
		contractId=Number($('#contractId option:selected').val());
		caseIdCourt=$('#caseIdCourt').val();
		description=$('#description').val();
		approvedStatus=$('#approvedStatus').val();
		if( (buildingId!=0 || contractId!=0 || caseIdCourt != null && caseIdCourt.trim().length > 0 
				|| description != null && description.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0)){
		var tempExp= /%/;
		 var tempMatch=caseIdCourt.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 caseIdCourt=caseIdCourt.replace('%','$');
				 tempMatch=caseIdCourt.search(tempExp);
			 }
		 }else{
			 caseIdCourt=caseIdCourt+'$';
		 }
		 tempMatch!= -1;
		 tempMatch=description.search(tempExp);
			 if(tempMatch!= -1 ){
				 while(tempMatch!= -1){
					 description=description.replace('%','$');
					 tempMatch=description.search(tempExp);
				 }
			 }else{
				 description=description+'$';
			 }
			 
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_legal_info.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
					$('.commonErr').hide();
					$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#gridDiv").html(data);  //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
		}else{
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/legal_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.legalinformation"/></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width80"> 
					 <div class="">
					    <form name="propertyInfo" id="propertyInfo">
						   <div id="hrm" class="width50 float-left"> 
						   		<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.caseidincourt"/></label> 
									<input type="text" id="caseIdCourt" class=""/>
								</div>
								<div id="legalsearchcontent">
			   				  </div>
			   				</div>
							<div id="hrm" class="width50 float-right">
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.description"/></label> 
									<input type="text" id="description">
								</div>
								<div id="temp1">
								</div>
						 </div>
						   <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="accounts.materialdespatch.button.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="accounts.materialdespatch.button.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="accounts.materialdespatch.button.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>