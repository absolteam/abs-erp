 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
</style>
<script type="text/javascript"> 
var expenseId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove();
	if(typeof($('#component-popup')!="undefined")){ 
		$('#component-popup').dialog('destroy');		
		$('#component-popup').remove();   
	 }
	$('#example').dataTable({ 
		"sAjaxSource": "show_json_property_expense.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Expense", "bVisible": false},
			{ "sTitle": '<fmt:message key="sys.common.number"/>'},
			{ "sTitle": '<fmt:message key="sys.common.date"/>'}, 
			{ "sTitle": '<fmt:message key="sys.common.type"/>'}, 
			{ "sTitle": '<fmt:message key="re.property.info.propertyName" />'}, 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){   
		expenseId=Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_property_expense_entry.action", 
	     	async: false, 
	     	data:{expenseId:expenseId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	});  

	$('#edit').click(function(){  
		if(expenseId > 0){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_property_expense_entry.action", 
		     	async: false, 
		     	data:{expenseId:expenseId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
					$.scrollTo(0,300);
				} 		
			}); 
		}
		else{
			$('#error_message').hide().html("Please select a record to edit").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		} 
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(expenseId > 0){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/property_expense_delete.action", 
		     	async: false,
		     	data:{expenseId:expenseId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);  
					var message=$('.tempresult').html().toLowerCase().trim(); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/get_property_expense.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html("Property Expense deleted..").slideDown(1000); 
								$.scrollTo(0,300);
							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
						return false;
					} 
				} 		
			}); 
		}
		else{
			$('#error_message').hide().html("Please select a record to delete").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		} 
	}); 

	//init datatable
	oTable = $('#example').dataTable();

	$('#print').click(function(){   
   		if(expenseId > 0){ 
    		window.open('property_expense_payment_printview.action?expenseId='+ expenseId +'&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');  
   		} else{
			$('#error_message').hide().html("Please select a record to print").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		} 
	});
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          expenseId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          expenseId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.propertyExpenseInfo"/></div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="purchase_list">
						<table class="display" id="example"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="re.property.info.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="re.property.info.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="re.property.info.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="re.property.info.add"/></div>	 
			</div>
		</div>
	</div>
</div>