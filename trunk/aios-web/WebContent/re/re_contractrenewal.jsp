<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var contractNumber="";
var buildingId="";
var tenantId="";
var contractDate="";
var fromDate="";
var toDate="";
var grandTotal="";
$(document).ready(function (){

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/contract_renewal_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#contractsearchcontent').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#contractsearchcontent').html(result); 
	 	} 
	});
	
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_contract_renewal_list.action", 
		 datatype: "json", 
		 colNames:['Contract ID','Contract Number','Offer ID','Building ID','Building Name','Tenant ID','Tenant Name','Contract Date','From Date','To Date','Amount'], 
		 colModel:[ 
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
				{name:'contractNumber',index:'CONTRACT_NO', width:50,  sortable:true},
		  		{name:'offerId',index:'OFFER_ID', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		 {name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'tenantId',index:'CUSTOMER_ID', width:150,sortable:true, sorttype:"data"},
		  		{name:'tenantName',index:'TENANT_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:100,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:100,sortable:true, sorttype:"data"},
		  		{name:'toDate',index:'TO_DATE', width:100,sortable:true, sorttype:"data"},
		  		{name:'grandTotal',index:'AMOUNT', width:50,sortable:true, sorttype:"data"}
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'CONTRACT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"Contract Renewal"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["offerId","contractId","buildingId","tenantId"]);
	
	$("#renew").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to renew !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.contractId; 
			
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/contract_renewal_edit.action",  
		     	data: {contractId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#search").click(function(){
		buildingId=Number($('#buildingId option:selected').val());
		contractNumber=Number($('#contractNumber').val());
		tenantId=Number($('#tenantId option:selected').val());
		contractDate=$('#defaultPopup').val();
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		grandTotal=Number($('#grandTotal').val());
		if((buildingId != 0  || contractNumber != 0 || grandTotal != 0 || tenantId != 0 || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 || contractDate != null && contractDate.trim().length > 0)){
		$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_contract_renewal.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_renewal_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Renewal</div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>

				<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					 <div class="">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm" class="width50 float-left"> 
						   		<div id="contractsearchcontent">
			   					</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.contract.contractno"/></label> <input type="text" id="contractNumber" />
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.contractdate"/></label> 
									<input type="text" id="defaultPopup" class=""/>
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.fromdate"/></label> 
									<input type="text" id="startPicker" readonly="readonly"  class=""/>
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.todate"/></label> 
									<input type="text" id="endPicker" readonly="readonly" class=""/>
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.amount"/></label> 
									<input type="text" id="grandTotal" class=""/>
								</div>
						   </div>
						   <div class="clearfix"></div>
						   <div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.contract.search"/></div>
						</form>
				 	</div>
		 		 </div>

			

				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete" style="display:none;"><fmt:message key="accounts.materialdespatch.button.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="renew">Renew</div>
					<div class="portlet-header ui-widget-header float-right"  id="add" style="display:none;"><fmt:message key="accounts.materialdespatch.button.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>