<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var addressLine1="";
var approvedStatus="";
$(document).ready(function (){
		 
	$('.formError').remove();

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/maintenance_contract_search_load.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#maintenancesearchcontent').html(result);  
     		$('#temp1').html($('#temp').html()); 
     		$('#temp').html("");
	 	},  
	 	error:function(result){ 
	 		 $('#maintenancesearchcontent').html(result); 
	 	} 
	});
	
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_maintainence_contract_info_list.action", 
		 datatype: "json", 
		 colNames:['MAINTENANCE CONTRACT ID',
		  		 	'PURCHASE ORDER ID',
		  		 	'<fmt:message key="re.contract.purchaseorderno"/>',
		  		 	'<fmt:message key="re.contract.contractdate"/>',
		  		 	'<fmt:message key="re.contract.securitydeposit"/>',
		  		 	'<fmt:message key="re.contract.addressline1"/>',
		  		 	'<fmt:message key="re.contract.fromdate"/>',
		  		 	'<fmt:message key="re.contract.todate"/>',
		  		 	'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'maintenanceId',index:'MAINTENANCE_CONTRACT_ID', width:100,  sortable:true},
				{name:'purchaseOrderId',index:'PURCHASE_ORDER_ID', width:100,  sortable:true},
				{name:'purchaseOrderNo',index:'PO_PO_NUMBER', width:100,  sortable:true},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'securityDeposit',index:'SECURITY_DEPOSIT', width:100,  sortable:true},
		  		 {name:'addressLine1',index:'ADDRESS_LINE1', width:150,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'toDate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'MAINTENANCE_CONTRACT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.contract.maintenancecontract"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
	$("#list2").jqGrid('hideCol',["maintenanceId","purchaseOrderId"]);

 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_maintenance_contract_redirect.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		}); 
		return true; 
	});
	
	$("#edit").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.maintenanceId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_maintenance_contract_redirect.action",  
		     	data: {maintenanceId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.maintenanceId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_maintenance_contract_redirect.action",  
		     	data: {maintenanceId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null'){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.maintenanceId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_maintenance_contract_redirect.action",  
		     	data: {maintenanceId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#search").click(function(){
		$('.commonErr').hide();
		addressLine1=$('#addressLine1').val();
		purchaseOrderId=Number($('#purchaseOrderId option:selected').val());
		contractDate=$('#defaultPopup').val();
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		approvedStatus=$('#approvedStatus').val();
		if( (purchaseOrderId!=0 || addressLine1 != null || addressLine1.trim().length > 0 || contractDate != null || contractDate.trim().length > 0 
				|| fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0)){
		var tempExp= /%/;
		 var tempMatch=addressLine1.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 addressLine1=addressLine1.replace('%','$');
				 tempMatch=addressLine1.search(tempExp);
			 }
		 }else{
			 addressLine1=addressLine1+'$';
		 }
		$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_maintainence_contract.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
					$('.commonErr').hide();
					$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#gridDiv").html(data);  //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
		}else{
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_contract_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 

	//Default Date Picker
	$('#defaultPopup').datepick();

	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }

	 
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.maintenancecontract"/></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					 <div class="">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm" class="width50 float-left"> 
						   <div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.addressline1"/></label> 
									<input type="text" id="addressLine1" class=""/>
								</div>
							<div id="maintenancesearchcontent">
			   					</div>
			   					<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.contractdate"/></label> 
									<input type="text" id="defaultPopup" class=""/>
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.fromdate"/></label> 
									<input type="text" id="startPicker" readonly="readonly"  class=""/>
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.contract.todate"/></label> 
									<input type="text" id="endPicker" readonly="readonly" class=""/>
								</div>
								<div id="temp1">
								</div>
							 </div>
						   <div class="clearfix"></div>
						   <div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.contract.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="accounts.materialdespatch.button.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="accounts.materialdespatch.button.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="accounts.materialdespatch.button.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>