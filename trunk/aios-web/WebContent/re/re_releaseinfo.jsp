<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var contractId="";
var buildingId="";
var tenantId="";
var paidAmount="";
var contractDate="";
$(document).ready(function (){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/release_search_load.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#releasesearchcontent').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#releasesearchcontent').html(result); 
	 	} 
	});
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_release_info_list.action", 
		 datatype: "json", 
		 colNames:['Release ID','Contract ID','<fmt:message key="re.property.info.contractno"/>','Building ID','<fmt:message key="re.property.info.buildingname"/>','<fmt:message key="re.property.info.buildingno"/>','Tenant ID','<fmt:message key="re.property.info.tenantname"/>','<fmt:message key="re.property.info.contractdate"/>','<fmt:message key="re.property.info.amount"/>'], 
		 colModel:[ 
				{name:'releaseId',index:'RELEASE_ID', width:100,  sortable:true},
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
				{name:'contractNumber',index:'CONTRACT_NO', width:100,  sortable:true},
		  		{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		{name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'buildingNumber',index:'BUILDING_NUMBER', width:100,sortable:true, sorttype:"data"},
		  		{name:'tenantId',index:'CUSTOMER_ID', width:150,sortable:true, sorttype:"data"},
		  		{name:'tenantName',index:'TENANT_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:100,sortable:true, sorttype:"data"},
		  		{name:'paidAmount',index:'PAID_AMOUNT', width:100,sortable:true, sorttype:"data"}
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'RELEASE_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:'<fmt:message key="re.property.info.releaseinfo"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["releaseId","contractId","buildingId",'"buildingNumber"',"customerId","tenantId","paidAmount"]);

 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_release_info.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		}); 
		return true; 
	});
	
	$("#edit").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.releaseId; 
			contractId=s2.contractId;
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_release_info.action", 
		     	data: {releaseId: id,contractId: contractId }, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	},
		     	error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.releaseId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_release_info.action", 
		     	data: {releaseId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	},
		     	error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null'){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.releaseId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/release_info_delete_confirm.action",  
		     	data: {releaseId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#search").click(function(){
		buildingId=Number($('#buildingId option:selected').val());
		buildingsId=Number($('#buildingsId option:selected').val());
		contractId=Number($('#contractId option:selected').val());
		tenantId=Number($('#tenantId option:selected').val());
		contractDate=$('#contractDate').val();
		paidAmount=Number($('#paidAmount').val());
		if((buildingsId!=0 || buildingId != 0  ||  contractId != 0  || tenantId != 0  ||  paidAmount!=0 || contractDate != null && contractDate.trim().length > 0)){ 
		$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_release_info.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});
	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/release_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 

	//Default Date Picker
	$('#contractDate').datepick();

});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.releaseinfo"/></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width60"> 
					 <div class="width:60%">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm"> 
							   <div id="releasesearchcontent">
				   				</div>
							  <div class="float-left" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.contractdate"/></label> 
									<input type="text" id="contractDate" readonly="readonly">
								</div>
								<div class="float-left" style="display:none;">
									<label style="font-weight:bold;"><fmt:message key="re.property.info.amount"/></label> <input type="text" id="paidAmount" />
								</div>
						   </div>
						  <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
		 		<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="accounts.materialdespatch.button.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="accounts.materialdespatch.button.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="accounts.materialdespatch.button.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>