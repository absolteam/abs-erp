<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}

.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:60%}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
.ui-autocomplete{
	width:194px!important;
}
.vmenu .pdfdownload span {
    cursor: pointer;
    display: block;
    padding: 5px 10px;
    width: 100px;
}
.vmenu .exportexcel span {
    cursor: pointer;
    display: block;
    padding: 5px 10px;
    width: 100px;
} 
</style>

</head>
<script type="text/javascript">
var oTable; var selectRow=""; var aData="";var aSelected = []; var s1=""; 
$(function(){
		$('.formError').remove(); 
	 	$('#example').dataTable({ 
	 		"sAjaxSource": "default_jsongeneral_report.action",
	 	    "sPaginationType": "full_numbers",
	 	    "bJQueryUI": true, 
	 		"aoColumns": [
	 			{ "sTitle": 'N/A', "bVisible": false},
	 			{ "sTitle": 'N/A',"sWidth": "300px"},
	 			{ "sTitle": 'N/A',"sWidth": "150px"},
	 			{ "sTitle": 'N/A',"sWidth": "100px"},
	 			{ "sTitle": 'N/A',"sWidth": "200px"},
	 			{ "sTitle": 'N/A',"sWidth": "100px"},
	 		],
	 		"sScrollY": (Number($("#main-content").height() - 50) / 2 ) - 55,
	 		"aaSorting": [[1, 'desc']], 
	 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
	 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
	 				$(nRow).addClass('row_selected');
	 			}
	 		}
	 	});	 
	 	oTable = $('#example').dataTable(); 
	 	
	 	/* Click event handler */
	 	$('#example tbody tr').live('click', function () {  
	 		  if ( $(this).hasClass('row_selected') ) {
	 			  $(this).addClass('row_selected');
	 	        aData =oTable.fnGetData( this );
	 	        s1=aData[0]; 
	 	    }
	 	    else {
	 	        oTable.$('tr.row_selected').removeClass('row_selected');
	 	        $(this).addClass('row_selected');
	 	        aData =oTable.fnGetData( this );
	 	        s1=aData[0];
	 	    }
	 		return false;
	 	}); 
	 	
	 	
	 	$('#downloadAllPDF').click(function(){   
			$('a').attr('href',"download_defaultreport.action?format=PDF");  
		}); 
	 	
	 	$('#exportAllExcel').click(function(){   
			$('a').attr('href',"default_excel_report.action?format=XLS");  
		});
	 	
	 	$('.pdfdownload').click(function(){  
	 		$('.vmenu').hide();
			$('.overlay').hide();
			if(s1 == null || s1 == 0) {
				$('.commonErr').hide().html("Please select one record to downlaod!!!").slideDown();
				return false; 
			}else{ 
				$('a').attr('href',"download_defaultreport.action?format=PDF"); 
			} 
		}); 
	 	
	 	$('.exportexcel').click(function(){ 
	 		$('.vmenu').hide();
			$('.overlay').hide();
			if(s1 == null || s1 == 0) {
				$('.commonErr').hide().html("Please select one record to export !!!").slideDown();
				return false; 
			}else{
				$('a').attr('href',"export_defaultreport.action?format=XLS"); 
			} 
		}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:93%;">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>General Reporting</div>	 
		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
				</c:if>    
			</div>	
			<div>
				<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
				</c:if>
			 </div> 
			 <div class="width100 float-left" id="hrm" style="padding:5px; margin-bottom: 5px;">  
				 <div class="width30 float-left">
				 	 <label class="width30" style="margin-top: 5px;">Search Name</label>
				 	 <input type="text" class="width50" id="" name="">
				 </div> 
			 </div> 
			 <div id="rightclickarea"> 
					<div id="gridDiv">
						<table class="display" id="example"></table>
					</div>  
			</div>	
			 <div class="vmenu">
		 	       	<div class="pdfdownload"><span><a href="#">Download as PDF</a></span></div>
					<div class="sep_li"></div>		 
					<div class="exportexcel"><span><a href="#">Export to Excel</a></span></div> 
			</div>
			<div class="clearfix"></div>  
			 <div class="width100 float-left" id="hrm" style="padding:5px; margin-top:10px;">  
				 <div class="width30 float-left">
				 	 <label class="width30" style="margin-top: 5px;">Search Name</label>
				 	 <input type="text" class="width50" id="" name="">
				 </div> 
			 </div> 
			 <div class="clearfix"></div>   
			 <div style="position: relative;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="downloadAllPDF"><a href="#">Download as PDF</a></div>
				<div class="portlet-header ui-widget-header float-right" id="exportAllExcel"><a href="#">Export to Excel</a></div>
			</div>
		</div>
	</div> 
</div>