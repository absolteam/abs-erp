  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var id;
var startDate; 
var endDate; 
var agreementNumber; 
var agreementDate; 
var firstRentDate; 

var amount;
var functionalAmount;
var tenantName;
var leadDays;
$(function(){ 
	 $("#mastersearchValidate").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});
	$('.formError').remove();
	$("#list2").jqGrid({
		url: '<%=request.getContextPath()%>/ta_tenantjsonlisting.action', 
		 datatype: "json",
		 colNames:['agreementId','Agreement Number', 'Agreement Date', 'From Date', 'To Date', 'First Rent Date', 'Lead Days', 'Amount', 'Exchange Rate', 'Functional Amount', 'customerId', 'Customer Name'],  
		 colModel:[
                     {name:'agreementId',index:'A.RE_AGREEMENT_ID', sortable:false},
                     {name:'agreementNumber',index:'A.RE_AGREEMENT_NUMBER', width:80, sortable:true,sortable:"data"},
                     {name:'agreementDate',index:'A.AGREEMENT_DATE', width:80, sortable:true,sortable:"date"},
                     {name:'startDate',index:'A.EFFECTIVE_DATE_FROM', width:80, sortable:true,sortable:"date"},
                     {name:'endDate',index:'A.EFFECTIVE_DATE_TO', width:80, sortable:true, sorttype:"date"} ,
                     {name:'firstRentDate',index:'A.FIRST_RENT_DATE', width:80, sortable:true, sorttype:"date"} , 
                     {name:'leadDays',index:'A.LEAD_DAYS', width:50, sortable:true, sorttype:"data"} , 
                     {name:'amount',index:'A.AMOUNT', width:100, sortable:true, sorttype:"data"} , 
                     {name:'exchangeRate',index:'A.EXCHANGE_RATE', width:100, sortable:true, sorttype:"data"} , 
                     {name:'functionalAmount',index:'A.FUNCTIONAL_AMOUNT', width:100, sortable:true, sorttype:"data"} , 
                     {name:'customerId',index:'A.CUSTOMER_ID', width:100, sortable:false} ,   
                     {name:'customerName',index:'B.CUSTOMER_NAME', width:100, sortable:true, sorttype:"data"} ,                                                    
    	 ],			  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'A.RE_AGREEMENT_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:"Tenant Agreement"
	});
	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false}); 
	$("#list2").jqGrid('hideCol',["agreementId","customerId"]);   
	  
	
 	$("#add").click( function() { 
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/ta_tenantagreement_addmaster.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
			},
			error: function(result){ 
				$("#main-wrapper").html(result); 
			}
		
		}); 
		return true; 
	});
	
 	$('#defaultPopup').datepick();  
	
	$("#edit").click( function(){ 
	 
	    var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null){
			alert("Please select one row to edit");
		}
		else{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.agreementId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/ta_tenantagreement_editmaster.action",  
		     	data: {agreementId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function() {
				},
				success:function(result){ 
					$('.formError').hide();  
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html();
				     if(tempeMsg!="" && tempeMsg!=null){
				    	$('.success').hide(); 
				    	$('.error').show();}
				    	else{ 
					    	$('.success').show(); 
					    	$('.error').hide();
				    	}   
			 		}
			});
			return true;	
		} 
		return false;
	});
	$("#del").click(function(){
		 
		 var s = $("#list2").jqGrid('getGridParam','selrow');
			if(s == null){
				alert("Please select one row to delete");
			}
			else{			
				var s2 = $("#list2").jqGrid('getRowData',s);  
				id = s2.agreementId; 
			var flag=false;
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/ta_tenantagreement_deletemaster.action",  
		     	data: {agreementId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				success:function(result){ 
		     		$('#main-wrapper').html(result);  
		     		if($('.sucess').html()==null || $('.sucess').html()=="") 
		     		{  
		     			$('.success').hide();
		     			$('.error').show();
		     		}
					else
					{  
						$('.success').show();
		     			$('.error').hide();
					}	
			 	},  
			 	error:function(result){ 
			 		 $('#main-wrapper').html(result); 
			 		 $('.error').show();
			 		 $('.success').hide();
			 	     
			 	    return false;
			 	} 
        	});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
	  	startDate=$('#startPicker').val(); 
	  	endDate=$('#endPicker').val(); 
		agreementNumber=$('#agreementNumber').val(); 
		agreementDate=$('#agreementDate').val(); 
		firstRentDate=$('#firstRentDate').val(); 

		amount=$('#amount').val();
		functionalAmount=$('#functionalAmount').val();
		tenantName=$('#tenantName').val();
		leadDays=$('#leadDays').val();

		if((agreementNumber != null && agreementNumber.trim().length > 0)
			|| (agreementDate != null && agreementDate.trim().length > 0)
			|| (firstRentDate != null && agreementDate.trim().length > 0)
			|| (amount != null && amount.trim().length > 0)
			|| (functionalAmount != null && functionalAmount.trim().length > 0)
			|| (tenantName != null && tenantName.trim().length > 0)
			|| (leadDays != null && leadDays.trim().length > 0)){

		
				$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/ta_tenantagreement_searchmaster.action",   
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) { 
		     		$("#gridDiv").html(data); 
				},
		     	success: function(data){
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else{ 
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown(); 
			return false; 
		}		
	});

	$("#cancel").click(function(){  
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/ta_tenantagreement_retrieve.action",   
	     	async: false, 
			dataType: "html",
			cache: false,
			error: function(data) {
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	$('#agreementDate').datepick({showTrigger: '#calImg'});
	$('#firstRentDate').datepick({showTrigger: '#calImg'});
	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
		$('#themeRollerSelect option:not(:selected)').remove();
	}

	$('#startPicker,#endPicker').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});	
});


//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
                $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
if (this.id == 'startPicker') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Agreement</div>	 
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div  class="response-msg success ui-corner-all" style="width:80%; display:none;" id="sMsg">${requestScope.succMsg}</div>
			<div  class="response-msg error ui-corner-all" style="width:80%; display:none;" id="eMsg">${requestScope.errMsg}</div>  
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<form name="tenantmaster" id="mastersearchValidate"> 
				<div class="width48 float-right" id="hrm"> 
	 				<fieldset>
						<legend>Tenant Details</legend>
							<div>
								<div class="width90 float-left"><label class="width30" for="amount">Rent Amount</label><input type="text" class="width40" name="amount" id="amount"></div>
								<div class="width90 float-left"><label class="width30" for="functionalAmount">Functional Amount</label><input type="text" class="width40" name="functionalAmount" id="functionalAmount"></div>
								<div class="width90 float-left"><label class="width30" for="tenantName">Customer Name</label><input type="text" class="width40" name="tenantName" id="tenantName"></div> 
								 								 
							</div> 		
					</fieldset>  
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend>Tenant Details</legend>
							<div>
								<!--<div class="width90 float-left"><label class="width30" for="startPicker">From Date</label><input type="text" class="width40" name="startDate" id="startPicker"></div>
								<div class="width90 float-left"><label class="width30" for="endPicker">To Date</label><input type="text" class="width40" name="enDate" id="endPicker"></div>  -->
								<div class="width90 float-left"><label class="width30" for="agreementNumber">Agreement Number</label><input type="text" class="width40" name="agreementNumber" id="agreementNumber"></div>
								<div class="width90 float-left"><label class="width30" for="agreementDate">Agreement Date</label><input type="text" class="width40" name="agreementDate" id="agreementDate"></div> 
								<div class="width90 float-left"><label class="width30" for="firstRentDate">First Rent Date</label><input type="text" class="width40" name="firstRentDate" id="firstRentDate"></div>
								<div class="width90 float-left"><label class="width30" for="leadDays">Lead Days</label><input type="text" class="width40" name="leadDays" id="leadDays"></div> 
							</div>  				
					</fieldset> 
				</div>
				
			<div class="clearfix"></div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin: 10px 0px 10px 10px;"> 				 
				<div class="portlet-header ui-widget-header float-left" id="search" style="cursor:pointer;">Search</div>
				<div class="portlet-header ui-widget-header float-left" id="cancel" style="cursor:pointer;">Cancel</div> 
			</div>
		</form>	
						
		<div class="clearfix"></div> 
 			<div id="gridDiv">
				<table id="list2"></table>  
				<div id="pager2"> </div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >
				<div class="portlet-header ui-widget-header float-right" id="del" >Delete</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">Edit</div>
				<div class="portlet-header ui-widget-header float-right" id="add" >Add</div>
			</div>
		</div>
	</div>
</div>