 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id = 0;var oTable; var selectRow=""; var aData ="";
var offerId=0;var aSelected = []; var bankAccountId=0;
$(function(){  
	$('.formError').remove();  
	if(typeof($('#offer-popup')!="undefined")){ 
		$('#offer-popup').dialog('destroy');		
		$('#offer-popup').remove(); 
	}
	$('#example').dataTable({ 
		"sAjaxSource": "offer_form_getjsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "OFFER_ID", "bVisible": false},
			{ "sTitle": '<fmt:message key="re.offer.offerno" />',"sWidth": "100px"},
			{ "sTitle": '<fmt:message key="re.offer.tenant" />',"sWidth": "250px"},
			{ "sTitle": '<fmt:message key="re.offer.offerdate" />',"sWidth": "125px"},
			{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "250px"},
			{ "sTitle": '<fmt:message key="re.contract.createdOn"/>',"sWidth": "125px"},
			{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "150px"},
			{ "sTitle": '<fmt:message key="re.contract.options"/>',"sWidth": "100px"},  
		],
		"sScrollY": $("#main-content").height() - 235,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
		
	
	$('#add').click(function(){ 
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/offer_form_getcanvas.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#temp-result").html(result);  
				$.scrollTo(0,300);
			} 		
		}); 
	});
	
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('#edit').click(function(){
		if(offerId>0 && offerId!=""){
			var showPage="edit"; 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/offer_editentry.action", 
		     	async: false,
		     	data:{showPage:showPage, offerId:offerId, recordId:0},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
					$.scrollTo(0,300);
				} 		
			}); 
		}else{
			alert("Select any one record to Edit.");
		}
	}); 

	$('#view').click(function(){  
		if(offerId == null || offerId == 0) {
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}	
		
		$('#loading').fadeIn();
		window.open('getPropertyOfferDetail.action?recordId='
				+ offerId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;
	}); 
		
	$('#delete').click(function(){ 
		if(offerId>0){
			var cnfrm = confirm("Selected offer will be permanently deleted.");
			if(!cnfrm) {
				return false;
			}
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/offer_deleteentry.action", 
		     	async: false,
		     	data:{offerId:offerId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
					$.scrollTo(0,300);
				} 		
			}); 
		}else{
			alert("Select any one record to Delete.");
		}
	}); 

	//init datatable
	oTable = $('#example').dataTable();
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if($(this).hasClass('row_selected')){ 
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          offerId=aData[0];  
	      }
	      else{ 
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          offerId=aData[0]; 
	      }
	});
	
	$('.offer_print').live('click',function(){
		var parentTR=$(this).parent().parent().get(0);
		var offerId=getId(parentTR); 
	 	$('#offer-popup-screen').dialog('open');
		$('#ui-dialog-title-offer-popup-screen').text("Print Options");
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/offer_print_options.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.offer-result').html(result); 
			},
			error:function(result){  
				 $('.offer-result').html(result); 
			}
		});		
	 	return false;
		
	});
	$('#offer-popup-screen').dialog({
		minwidth: 'auto',
		width:500,
		height:280,  
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() {
				$('#err_label').hide();
				if($('#bankAccount').val() == null || $('#bankAccount').val() == 0) {
					$('#err_label').html("Please select bank account");
					$('#err_label').fadeIn();
					return false;
				}			
				bankAccountId = Number($('#bankAccount').val());
				proceedToPrint();
				$(this).dialog("close"); 
			}
		}
	});

	//$('.offer_print').live('click',function(){
	var proceedToPrint = function () {
		
		offerTemplate = $('#offerTemplate').val();
		offerPrintInfo = $('#offerPrintInfo').val();
		//alert(offerId+"--offerTemplate--"+offerTemplate+"--bankAccountId--"+bankAccountId);
		// HTML PRINT
		 if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false" || '${THIS.templatePrint}' == '') {
			window.open('<%=request.getContextPath()%>/offer_printout.action?offerId=' + offerId+ '&personBankId=' + bankAccountId,'_blank', 
				'width=800,height=700,scrollbars=yes,left=100px,top=0px'); 	
			return false;
		}
		// TEMPLATE PRINT
		else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") {
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/offer_printout.action",
		     	async: false,
		     	data:{offerId: offerId,personBankId:bankAccountId,offerPrintOption1:offerPrintInfo},
				dataType: "html",
				cache: false,
				error: function(data) {
					$('#loading').fadeOut();
				},
		     	success: function(data){
					$(".tempresultfinal").html(data);  //gridDiv main-wrapper
					$('#loading').fadeOut();
					var w=window.open('<%=request.getContextPath()%>/printing/applet/re_print_offer.html?offerTemplate='+offerTemplate,'_blank', 
						'width=450,height=150,scrollbars=yes,left=200px,top=200px'); 
					//w.offerTemplate=offerTemplate;
		     	}
			});
			return false;
		} 
	};
	
	});

	function getId(parentTR){ 
		if ($(parentTR).hasClass('row_selected') ) { 
			$(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        offerId=aData[0]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        offerId=aData[0];
	    }
		return offerId;
	}
 
</script> 

<div id="main-content">
		<div id="temp-result" style="display:none;"></div>
			<div id="" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerform" /></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div> 
				 <div id="rightclickarea">
					 <div id="offer_management">
						<table class="display" id="example"></table>
					 </div>  
				 </div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Delete</span></div>
				 </div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit" /></div>
					<div class="portlet-header ui-widget-header float-right callJq"  id="add"><fmt:message key="re.property.info.add" /></div>
				</div>
			</div>
		</div>
		<div id="offer-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="offer-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
</div>