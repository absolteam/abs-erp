<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var id;
var personId="";
var contractId="";
var flatDescription="";
var approvedStatus="";
var oTable; var selectRow=""; var aData="";var aSelectedRelease = [];var s="";var s1=""; var s2="";

$(document).ready(function (){
	$('#cancellation').dataTable({ 
 		"sAjaxSource": "cancel_contract_list_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Release ID', "bVisible": false},
 			{ "sTitle": 'Contract ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "125px"},
 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "250px"},
 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "100px"},
 			
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRowRelease, aDataRelease, iDisplayIndex ) {
 			if (jQuery.inArray(aDataRelease.DT_RowId, aSelectedRelease) !== -1) {
 				$(nRowRelease).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#cancellation').dataTable();

 	/* Click event handler */
 	$('#cancellation tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			$(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
 	
 	$('#cancellation tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        s=aData[0];
	 	    s1=aData[1];
	 	   	s2=aData[7];
	 	   	//alert(s2);
	 	  /*  	if(s2=="Release Inspection")
	      	  	$.fn.editCancellationInfo();
	 	   	else 
	 	   		$.fn.editCancellationInfoWOConfirm();
	 	  */
	 	   	$.fn.editCancellationInfo();
	        return false;
	    }
	     
	});

 	$('#view').click(function(){ 
 	 	var releaseId = s; 
		if(releaseId == null || releaseId == 0) {
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}	
		
		$('#loading').fadeIn();
		window.open('getPropertyReleaseDetail.action?releaseId='
				+ releaseId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;
	}); 
 	
	<%-- $('#cancellation-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		buttons: {
			
			"No": function() { 
				$(this).dialog("close"); 
			}, 
			"Yes": function() { 
				var cancellationId=Number(s);
				var contractId=Number(s1);
				var initializationFlag=Number(s2);
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/cancellation_contract_info_edit.action",  
			     	data: {cancellationId:cancellationId,contractId:contractId}, 
			     	async: false,
					dataType: "html",
					cache: false,
			     	success: function(data){
						$("#main-wrapper").html(data); //gridDiv main-wrapper
			     	}
				});
				$(this).dialog("close"); 
			} 
		}
	}); --%>
 	$.fn.editCancellationInfo = function() {
 		var cancellationId=Number(s);
		var contractId=Number(s1);
		var initializationFlag=Number(s2);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/cancellation_contract_info_edit.action",  
	     	data: {cancellationId:cancellationId,contractId:contractId,recordId:0}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
				$("#main-wrapper").html(data); //gridDiv main-wrapper
	     	}
		}); 
 	};

 	$.fn.editCancellationInfoWOConfirm = function() {
 		var cancellationId=Number(s);
		var contractId=Number(s1);
		var initializationFlag=Number(s2);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/cancellation_contract_info_edit.action",  
	     	data: {cancellationId:cancellationId,contractId:contractId,recordId:0}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
				$("#main-wrapper").html(data); //gridDiv main-wrapper
	     	}
		});
 	}
 	
 	$('.cancellation_print').live('click',function(){
 		var parentTR=$(this).parent().parent().get(0);
		var cancellationId=getId(parentTR); 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/release_print_update.action",  
	     	data: {releaseId:releaseId}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
	     		window.open('<%=request.getContextPath()%>/cancellation_contract_printout.action?cancellationId=' + cancellationId,'_blank', 
	    		'width=800,height=600,scrollbars=yes,left=100px');
	     	}
		});
		return false;
	});
});
function getId(parentTR){ 
	if ($(parentTR).hasClass('row_selected') ) { 
		$(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0]; 
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0];
    }
	return s1;
}	
</script>
<body>
	<div id="main-content">
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		 <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Cancellation Information</div>
			<div class="portlet-content">
			  <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
				 </div>
				<div id="gridDiv">
					<table class="display" id="cancellation"></table>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
				<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<!--<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				--></div>
				
			</div>
		</div>
		
	</div>
</body>