<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}

.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:60%}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
.ui-autocomplete{
	width:194px!important;
}
</style>

</head>
<script type="text/javascript">
var contractDate="";
var fromDate="";
var toDate="";
var tenantId="";
var unitId=0;
$(function (){
	$('.formError').remove();   
	//Date configuration 
	 $(".tenantName").combobox({ 
	       selected: function(event, ui){
		      	tenantName=$(this).val(); 
		      	tenantTypeName=$('.tenantName option:selected').text(); 
		      	var splitArray=tenantName.split('@');
		      	tenantId=splitArray[0]; 
		      	$("#tenantId").val(tenantId);
		      	$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/contract_detail_report.action",  
					data:{tenantName:tenantName,tenantId:tenantId,tenantTypeName:tenantTypeName},
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result){  
						$("#temp-result").html(result);  
						tenantId=0;
					} 
				});  
	       }
	    }); 
	
	 $(".print-call").click(function(){ 
			tenantId=$("#tenantId").val();
			if(tenantId>0){
				window.open('<%=request.getContextPath()%>/tenant_contract_printout.action?tenantId='
						+ tenantId+'&tenantName='+tenantName+'&tenantTypeName='+tenantTypeName,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
				return false;
			}else{
				alert("Please select tenant to take a report");
				return false;
			}
			return false;
		});
	  $(".pdf-download-call").click(function(){ 
		  tenantId=$("#tenantId").val();
			if(tenantId>0){
				window.open('<%=request.getContextPath()%>/downloadTenantContractList.action?recordId='
						+ tenantId+'&tempString='+tenantName+'&tempString1='+tenantTypeName+ '&format=PDF','_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
				return false;
			}else{
				alert("Please select tenant to take a report");
				return false;
			}
			return false;
		}); 
	  $(".xls-download-call").click(function(){
		  tenantId=$("#tenantId").val();
			if(tenantId>0){
				window.open('<%=request.getContextPath()%>/tenant_contract_printout_xls.action?tenantId='
						+ tenantId+'&tenantName='+tenantName+'&tenantTypeName='+tenantTypeName,'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
				return false;
			}else{
				alert("Please select tenant to take a report");
				return false;
			}
			return false;
		}); 
});  
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:93%;">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Contract Detail</div>	 
		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
				</c:if>    
			</div>	
			<div>
				<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
				</c:if>
			 </div> 
			 <div class="width100 float-left" id="hrm" style="padding:5px;">  
				 <div class="width30 float-left">
				 	<label class="width30" style="margin-top: 5px;">Tenant Name</label>
				 	<input type="hidden" id="tenantId"  />
				 	<select id="tenantName" class="tenantName" name="tenantName">
						<option value="">Select</option>
						<c:forEach items="${TENANT_INFO}" var="bean">
							<option value="${bean.description}">${bean.tenantName}</option>
						</c:forEach>
					</select>
				 </div> 
			 </div>
			 <div id="temp-result" class="width100 float-left"></div>
			 
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 