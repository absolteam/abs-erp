<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var id;
var personId="";
var contractId="";
var flatDescription="";
var approvedStatus="";
var releaseType = "";
var clearanceAccountNo = "";
var oTable; var selectRow=""; var aData="";var aSelectedRelease = [];var s="";var s1=""; var s2="";

$(document).ready(function (){
	if(typeof($('#contract-popup')!="undefined")){ 
		$('#contract-popup').dialog('destroy');		
		$('#contract-popup').remove(); 
	}
	$('#release').dataTable({ 
 		"sAjaxSource": "release_contract_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Release ID', "bVisible": false},
 			{ "sTitle": 'Contract ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "125px"},
 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "250px"},
 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "200px"},
 			{ "sTitle": '<fmt:message key="re.contract.options"/>',"sWidth": "100px"},
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRowRelease, aDataRelease, iDisplayIndex ) {
 			if (jQuery.inArray(aDataRelease.DT_RowId, aSelectedRelease) !== -1) {
 				$(nRowRelease).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#release').dataTable();

 	/* Click event handler */
 	$('#release tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			$(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
 	
 	$('#release tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        s=aData[0];
	 	    s1=aData[1];
	 	   	s2=aData[7];
	 	   	//alert(s2);
	 	/*    	if(s2=="Release Inspection")
	      	  	$.fn.editReleaseInfo();
	 	   	else */
	 	   		$.fn.editReleaseInfoWOConfirm();
	        return false;
	    }
	     
	});

 	$('#view').click(function(){ 
 	 	var releaseId = s; 
		if(releaseId == null || releaseId == 0) {
			$('.commonErr').hide().html('<fmt:message key="re.property.releaseMessage"/>').slideDown();
			return false; 
		}	
		
		$('#loading').fadeIn();
		window.open('getPropertyReleaseDetail.action?releaseId='
				+ releaseId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;
	}); 
 	
	$('#release-popup').dialog({
		autoOpen: false,
		width: 300,
		bgiframe: false,
		modal: true,
		buttons: { 
			"No": function() { 
				$(this).dialog("close"); 
			}, 
			"Yes": function() { 
				var releaseId=Number(s);
				var contractId=Number(s1);
				var initializationFlag=Number(s2);
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/release_contract_info_edit.action",  
			     	data: {releaseId:releaseId,contractId:contractId,initializationFlag:initializationFlag,recordId:0}, 
			     	async: false,
					dataType: "html",
					cache: false,
			     	success: function(data){
						$("#main-wrapper").html(data); //gridDiv main-wrapper
			     	}
				});
				$(this).dialog("close"); 
			} 
		}
	});
 	$.fn.editReleaseInfo = function() {
 		$('.ui-dialog-titlebar').remove(); 
		$('#release-popup').dialog('open'); 
 	};

 	$.fn.editReleaseInfoWOConfirm = function() {
 		var releaseId=Number(s);
		var contractId=Number(s1);
		var initializationFlag=Number(s2);
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/release_contract_info_edit.action",  
	     	data: {releaseId:releaseId,contractId:contractId,initializationFlag:initializationFlag,recordId:0}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
				$("#main-wrapper").html(data); //gridDiv main-wrapper
	     	}
		});
 	};
 	
 	//$('.release_print').live('click',function(){
	var proceedToPrint = function () {
		
		var releaseId = s1; 
		if(releaseId == null || releaseId == 0) {
			$('.commonErr').hide().html('<fmt:message key="re.property.contractPrintError"/>').slideDown();
			return false; 
		}
		
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/release_print_update.action",  
	     	data: {releaseId:releaseId}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
	     		
	     		// HTML PRINT
	    		if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false") {
	    			
		     		window.open('<%=request.getContextPath()%>/release_contract_printout.action?releaseId=' 
		     				+ releaseId + '&releaseType=' + releaseType + '&transferAccount=' + clearanceAccountNo
		     				,'_blank', 'width=800,height=700,scrollbars=yes,left=100px'); 
		     		return false;
	    		}
	     		
	    		// TEMPLATE PRINT
	    		else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") {
	    			
		     		$.ajax({
		    			type: "POST",  
		    			url: "<%=request.getContextPath()%>/release_contract_template_printout.action",  
		    	     	data: {
		    	     		releaseId:releaseId,
		    	     		releaseType: releaseType,
		    	     		transferAccount: clearanceAccountNo
		    	     		}, 
		    	     	async: false,
		    			dataType: "html",
		    			cache: false,
		    	     	success: function(data){
		    	     		window.open('<%=request.getContextPath()%>/printing/applet/re_print_release.html' 
		    	     				,'_blank', 'width=450,height=150,scrollbars=yes,left=200px,top=200px'); 
		    	     	},
		    	     	error: function(data){
		    	     		alert(data);
		    	     	}
		    		});
		     		return false;
	     		}
	     	},
	     	error: function(data){
	     		alert(data + " - update failed");
	     	}
		});
		return false;
	};
	
	$('#release-popup-screen').dialog({
			minwidth: 'auto',
			width:300,
			height:300,  
			bgiframe: false,
			modal: true,
			autoOpen: false,
			buttons: { 
				"Print": function() {
					clearanceAccountNo = $("#accountNo").val();
					proceedToPrint();
					$(this).dialog("close"); 
				}
			} 	
	});
	
	 $('.release_print').live('click',function(){ 
		 			 
		 	var parentTR=$(this).parent().parent().get(0);
			var idval=getId(parentTR); 
			
		 	$('#release-popup-screen').dialog('open');
			$('#ui-dialog-title-release-popup-screen').text("Select Clearance Type");
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/release_options.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.release-result-print').html(result); 
				},
				error:function(result){  
					 $('.release-result-print').html(result); 
				}
			});	
		 	return false;
		});
});
function getId(parentTR){ 
	if ($(parentTR).hasClass('row_selected') ) { 
		$(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0]; 
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0];
    }
	return s1;
}	
</script>
<body>
	<div id="main-content">
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		 <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.contractReleaseInfo"/></div>
			<div class="portlet-content">
			  <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
				 </div>
				<div id="gridDiv">
					<table class="display" id="release"></table>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
				<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<!--<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				--></div>
				
			</div>
		</div>
		<div id="release-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
			<div class="release-result">Are you sure to release this contract?</div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Yes</button>
				<button type="button" class="ui-state-default ui-corner-all">No</button>
			</div>
		</div>
		
		<div style="display: none; position: absolute; overflow: auto; 
			z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" 
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" 
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	
		<div id="release-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="release-result-print width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
	</div>
</body>