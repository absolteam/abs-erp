<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var buildingName="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var unitReportId=0;var s=""; 
var redirectedUnitId=0;
var unitStatus;
$(document).ready(function (){
	$('.formError').remove();
	
	unitStatus="-1";

	unitListJsonCall();
	
	$('#unitStatus').live('change', function () {
		unitReportAlreadyExsist();
	});
	
 	
 	
 	/* Click event handler */
 	$('#unitreport tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	});

 	/* Click event handler */
 	$('#unitreport tbody tr').live('dblclick', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	      callReportView();
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	      callReportView();
 	    }
 	});
 	
	$("#view").click( function() { 
		callReportView();
		return true;	
	});
	$("#ContractUnit").click( function() { 
		redirectedUnitId=unitReportId;
		$.ajax({
			type:"POST",
			url:'<%=request.getContextPath()%>/getunit_contract.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#reporting-tree").html(result);
				$("#page-wrapper #main-wrapper #main-content").css("margin-right", 0);
			}
			
		});	
	});
	
	 $(".print-call").click(function(){ 
		 	unitStatus=$('#unitStatus').val();
			window.open('<%=request.getContextPath()%>/unit_report_printout.action?status='+unitStatus,'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		});
	  $(".pdf-download-call").click(function(){ 
		  	unitStatus=$('#unitStatus').val();
			window.open('<%=request.getContextPath()%>/downloadUnitReport.action?format=PDF&tempVar='+unitStatus,'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		}); 
	  $(".xls-download-call").click(function(){
		  	unitStatus=$('#unitStatus').val();
			window.open('<%=request.getContextPath()%>/unit_report_download_xls.action?status='+unitStatus,'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			
			return false;
		}); 
	
});
function unitReportAlreadyExsist(){
	oTable = $('#unitreport').dataTable();
	oTable.fnDestroy();
	$('#unitreport').remove();
	$('#gridDiv').html("<table class='display' id='unitreport'></table>");
	unitListJsonCall();
}
function unitListJsonCall(){
	unitStatus=$('#unitStatus').val();
	
	$('#unitreport').dataTable({ 
 		"sAjaxSource": "unit_report_json.action?status="+unitStatus,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Unit ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.property.info.unitName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.type" />',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.property.info.componentName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.propertyName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.size" />',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "125px"},
 		],
 		"sScrollY": $("#main-content").height() - 260,
		//"bPaginate": false,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}	 
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#unitreport').dataTable();
}
function callReportView(){
	$('#loading').fadeIn();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getPropertyUnitDetail.action", 
		data:{format:"HTML",recordId:unitReportId,approvalFlag:"N"},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$("#reporting-tree").html(result); 
			$("table:first").addClass("table-custom-design-report");
			$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
			$("#reporting-tree").append("<div style='margin-top:-40px;' class='portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons'>"+
			"<div class='portlet-header ui-widget-header float-left'><a class='' style='cursor: pointer;' onclick='reportAjaxCallFunction(this);' id='unit_report_redirect'>Back</a></div></div>");
			$('#loading').fadeOut();
		}
 	});
}
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyUnit" /></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				  <div class="width40 float-left" id="hrm" style="padding:5px;">  
					 <div class="width100 float-left">
					 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.property.info.status" /></label>
					 	<select class="width50" id="unitStatus">
					 		<option value="-1">All</option>
					 		<option value="1">AVAILABLE</option>
					 		<option value="2">PENDING</option>
					 		<option value="3">RENTED</option>
					 		<option value="4">SOLD</option>
					 		<option value="5">RENOVATION</option>
					 		<option value="6">OFFERED</option>
					 	</select>
					 </div>
					
				 </div>
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="unitreport"></table>
					 </div>
				 </div>		
				 <div class="vmenu">
					<div class="first_li"><span id="view">View</span></div>
					<div class="first_li"><span id="ContractUnit">Contract Info</span></div>
					<div class="sep_li"></div>		 
		 	       	<div class="first_li"><span class="print-call">Print</span></div>
					<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
					<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
				 </div>
			</div>
		</div>
	</div>
	<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 
</body>