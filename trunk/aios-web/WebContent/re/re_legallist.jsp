<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var buildingName="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(document).ready(function (){
	$('.formError').remove();
	
 	$('#legal').dataTable({ 
 		"sAjaxSource": "json_propertyunits.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Legal ID', "bVisible": false},
 			{ "sTitle": 'Legal Number'},
 			{ "sTitle": 'Screen',"sWidth": "100px"},
 			{ "sTitle": 'Effective Date',"sWidth": "100px"},
 			{ "sTitle": 'Created Date'},
 			{ "sTitle": 'Created By',"sWidth": "150px"},
 			{ "sTitle": 'Details'},
 		],
 		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}	 
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#legal').dataTable();

 	/* Click event handler */
 	$('#example tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
 		
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/legal_add_get.action",  
			data: {legalId: 0},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

	$("#edit").click( function() { 
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			legalId = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/legal_add_get.action",  
		     	data: {legalId: legalId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		if(s == null ) {
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}		
			
		unitId = s; 
		$('#loading').fadeIn();
		window.open('.action?recordId='
				+ unitId + '&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;	
	});
	
	$("#delete").click(function(){
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{	
			var cnfrm = confirm("Selected legal will be permanently deleted.");
			if(!cnfrm) {
				return false;
			}
				
			unitId = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_legal.action",  
		     	data:{unitId: unitId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_component_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property Units</div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="legal"></table>
					 </div>
				 </div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Delete</span></div>
				 </div>
				
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add" /></div>
				 </div>
			</div>
		</div>
	</div>
</body>