<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var ownerName="";
var Proffession="";
var fromDate="";
var toDate="";
var uaeIdNo="";
var address1="";
var passportNo="";
var mobileNo="";

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-ownerinformation.action", 
		 datatype: "json", 
		 colNames:['Owner Id','<fmt:message key="re.owner.info.ownername"/>','<fmt:message key="re.owner.info.proffession"/>','<fmt:message key="re.owner.info.fromdate"/>','<fmt:message key="re.owner.info.todate"/>','<fmt:message key="re.owner.info.birthdate"/>','<fmt:message key="re.owner.info.civilidno"/>','<fmt:message key="re.owner.info.address1"/>','<fmt:message key="re.owner.info.passportno"/>','<fmt:message key="re.owner.info.mobileno"/>','<fmt:message key="re.owner.info.landlineno"/>','<fmt:message key="re.owner.info.emailid"/>'], 
		 colModel:[ 
				{name:'ownerId',index:'OWNER_ID', width:100,  sortable:true},
				{name:'ownerName',index:'NAME', width:100,  sortable:true},
		  		{name:'Proffession',index:'PROFESSION', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'fromDate',index:'FROM_DATE', width:100,  sortable:true},
		  		{name:'toDate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'birthDate',index:'BIRTH_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'uaeIdNo',index:'UAE_ID_NO', width:150,sortable:true, sorttype:"data"},
		  		{name:'address1',index:'ADDRESS_LINE_1', width:150,sortable:true, sorttype:"data"}, 
		  		{name:'passportNo',index:'PASSPORT_NO', width:150,sortable:true, sorttype:"data"},
		  		{name:'mobileNo',index:'MOBILE_NO', width:150,sortable:true, sorttype:"data"},
		  		{name:'landlineNo',index:'LANDLINE_NO', width:150,sortable:true, sorttype:"data"},
		  		{name:'emailId',index:'EMAIL_ID', width:150,sortable:true, sorttype:"data"},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'OWNER_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.owner.info.ownerinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["ownerId"]);

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/owner_information_add.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	
	
	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.ownerId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/owner_information_edit.action",  
		     	data: {ownerId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to View");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.ownerId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/owner_information_view.action",  
		     	data: {ownerId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.ownerId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/owner_information_delete_req.action",  
		     	data:{ownerId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		ownerName=$('#ownerName').val();
		Proffession=$('#Proffession').val();
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		uaeIdNo=$('#uaeIdNo').val();
		address1=$('#address1').val();
		passportNo=$('#passportNo').val();
		mobileNo=$('#mobileNo').val();
		if((ownerName != null && ownerName.trim().length > 0 || Proffession != null && Proffession.trim().length > 0 || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 || uaeIdNo != null && uaeIdNo.trim().length > 0 || address1 != null && address1.trim().length > 0  || passportNo != null && passportNo.trim().length > 0 || mobileNo != null && mobileNo.trim().length > 0)){
		var tempExp= /%/;
		var tempMatch=ownerName.search(tempExp);
		if(tempMatch!= -1 ){
		 while(tempMatch!= -1){
			 ownerName=ownerName.replace('%','$');
			 tempMatch=ownerName.search(tempExp);
		 }
		}else{
			ownerName=ownerName+'$';
		}
		tempMatch!= -1;
		tempMatch=Proffession.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 Proffession=Proffession.replace('%','$');
				 tempMatch=Proffession.search(tempExp);
			 }
		 }else{
			 Proffession=Proffession+'$';
		 }
		  tempMatch!= -1;
		 tempMatch=uaeIdNo.search(tempExp);
			 if(tempMatch!= -1 ){
				 while(tempMatch!= -1){
					 uaeIdNo=uaeIdNo.replace('%','$');
					 tempMatch=uaeIdNo.search(tempExp);
				 }
			 }else{
				 uaeIdNo=uaeIdNo+'$';
			 }
			 tempMatch!= -1;
			 tempMatch=address1.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 address1=address1.replace('%','$');
						 tempMatch=address1.search(tempExp);
					 }
				 }else{
					 address1=address1+'$';
				 }
				 tempMatch!= -1;
				 tempMatch=passportNo.search(tempExp);
					 if(tempMatch!= -1 ){
						 while(tempMatch!= -1){
							 passportNo=passportNo.replace('%','$');
							 tempMatch=passportNo.search(tempExp);
						 }
					 }else{
						 passportNo=passportNo+'$';
					 }
					 tempMatch!= -1;
					 tempMatch=mobileNo.search(tempExp);
						 if(tempMatch!= -1 ){
							 while(tempMatch!= -1){
								 mobileNo=mobileNo.replace('%','$');
								 tempMatch=mobileNo.search(tempExp);
							 }
						 }else{
							 mobileNo=mobileNo+'$';
						 }
				 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_owner_information.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
		});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/owner_information_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.ownerinformation"/></div>
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					 <div class="">
					    <form name="propertyFlat" id="propertyFlat">
					     <div id="hrm" class="width50 float-left"> 
						    <div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.ownername"/></label> <input type="text" id="ownerName" />
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.proffession"/></label> <input type="text" id="Proffession" />
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.fromdate"/></label> 
									<input type="text" id="startPicker" readonly="readonly">
								</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.todate"/></label> 
									<input type="text" id="endPicker" readonly="readonly">
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.civilidno"/></label> <input type="text" id="uaeIdNo" />
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.address1"/></label> <input type="text" id="address1" />
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.passportno"/></label> <input type="text" id="passportNo" />
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.mobileno"/></label> <input type="text" id="mobileNo" />
								</div>
							</div>
						 <div class="clearfix"></div>
						   <div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.owner.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.owner.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.owner.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.owner.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.owner.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>