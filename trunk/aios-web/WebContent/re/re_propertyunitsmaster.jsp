<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var buildingName = "";
var accountNo = "";
var generalServiceType = "";
var transferType = "";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 

$(document).ready(function (){
	$('.formError').remove();
	if(typeof($('#component-popup')!="undefined")){ 
		$('#component-popup').dialog('destroy');		
		$('#component-popup').remove(); 
	}
	if(typeof($('#common-popup')!="undefined")){ 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
	}
 	$('#unit').dataTable({ 
 		"sAjaxSource": "json_propertyunits.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Unit ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.property.info.unitName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.type" />',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.property.info.componentName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.propertyName" />'},
 			{ "sTitle": '<fmt:message key="re.property.info.size" />',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "125px"},
 		],
 		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}	 
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#unit').dataTable();

 	/* Click event handler */
 	$('#unit tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
 		
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_unit_get.action",  
			data: {unitId: 0,recordId:0},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return false; 
	});

	$("#edit").click( function() { 
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to edit...").slideDown();
			return false; 
		}
		else
		{			
			unitId = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/add_unit_get.action",  
		     	data: {unitId: unitId,recordId:0, isIdenticalEntry: false}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to view...").slideDown();
			return false; 
		}
			
		unitId = s; 
		$('#loading').fadeIn();
		window.open('getPropertyUnitDetail.action?recordId='
				+ unitId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;	
	});
	
	$("#delete").click(function(){
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to delete...").slideDown();
			return false; 
		}
		else
		{	
			var cnfrm = confirm("Selected unit will be permanently deleted.");
			if(!cnfrm) {
				return false;
			}
				
			unitId = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_unit.action",  
		     	data:{unitId: unitId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_component_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data); //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
	$('#create-identical').click(function() {
		
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to edit...").slideDown();
			return false; 
		}
		else
		{			
			unitId = s; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/add_unit_get.action",  
		     	data: {unitId: unitId, recordId: 0, isIdenticalEntry: true}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	var proceedToPrintClearance = function () {
		
		$('#loading').fadeIn(); 
		
		window.open('print_clearance.action?buildingId=' + unitId
 				+ '&clearanceType=' + releaseType + '&accountNo=' + accountNo + '&isUnitClearance=1'
 				+ '&generalServiceType=' + generalServiceType,
 				'','width=800,height=700,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
		
		 <%-- $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/print_clearance_template.action", 
			 	async: false, 
			    dataType: "html",
			    data: {
			    	buildingId: propertyId,
			    	clearanceType: releaseType,
			    	accountNo: accountNo
			    },
			    cache: false,
				success:function(result) {   
					
					window.open('<%=request.getContextPath()%>/printing/applet/re_print_clearance.html',
								'','width=400,height=400,scrollbars=no,left=100px'); 
				},
				error:function(result){  
					 alert(result);
				}
			}); --%>
		
 		$('#loading').fadeOut();
	};
	
	$('#release-popup-screen').dialog({
		minwidth: 'auto',
		width:300,
		height:300,  
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() {
				accountNo = $('#accountNo').val();
				generalServiceType = $('#generalServiceType').val();
				proceedToPrintClearance();
				$(this).dialog("close"); 
			}
		} 	
	});
	
	$('#print-clearance').live('click',function(){ 
	 	
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to print...").slideDown();
			return false; 
		}
			
		unitId = s; 
 		
 		$('#release-popup-screen').dialog('open');
		$('#ui-dialog-title-release-popup-screen').text("Select Clearance Type");
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/release_options.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.release-result').html(result); 
			},
			error:function(result){  
				 $('.release-result').html(result); 
			}
		});	
	 	return false;
	});
	
	var proceedToPrintTransfer = function () {
		
		$('#loading').fadeIn(); 
		
		window.open('print_transfer.action?buildingId=' + unitId
 				+ '&clearanceType=' + transferType + '&accountNo=' + accountNo + '&isUnitClearance=1'
 				+ '&generalServiceType=' + generalServiceType,
 				'','width=800,height=700,scrollbars=yes,left=100px');
		
		$('#loading').fadeOut();
		return false;
		
		<%--  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/print_transfer_template.action", 
			 	async: false, 
			    dataType: "html",
			    data: {
			    	buildingId: propertyId,
			    	clearanceType: releaseType,
			    	accountNo: accountNo
			    },
			    cache: false,
				success:function(result) {   
					
					window.open('<%=request.getContextPath()%>/printing/applet/re_print_transfer.html',
								'','width=400,height=400,scrollbars=no,left=100px'); 
				},
				error:function(result){  
					 alert(result);
				}
			}); --%>
		
 		$('#loading').fadeOut();
	};
	
	$('#contract-popup-screen').dialog({
		minwidth: 'auto',
		width:366,
		height:330, 
		bgiframe: false,
		modal: true,
		autoOpen: false,
		buttons: { 
			"Print": function() {
				accountNo = $('#transferAccount').val();
				generalServiceType = "";
				proceedToPrintTransfer();
				$(this).dialog("close"); 
			}
		} 	
	});
	
	$('#print-transfer-letter').live('click',function(){ 
	 	
		if(s == null || s == 0 || s == "") {
			$('.commonErr').hide().html("Please select one record to print...").slideDown();
			return false; 
		}
			
		unitId = s;
 		
 		$('#contract-popup-screen').dialog('open');
		$('#ui-dialog-title-contract-popup-screen').text("Transfer Options");
	 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/transfer_options.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.contract-result').html(result); 
			},
			error:function(result){  
				 $('.contract-result').html(result); 
			}
		});	
	 	return false;
	});
});
</script>

<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyUnit" /></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all" id="unit-success">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all" id="unit-error">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="unit"></table>
					 </div>
				 </div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
		 	       	<div class="first_li"><span>Create-Identical</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Delete</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Print-Clearance</span></div>
					<!-- <div class="first_li"><span>Print-Transfer-Letter</span></div> -->
				 </div>
				
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add" /></div>
					<div class="portlet-header ui-widget-header float-right" style="display: none;"  id="print-clearance">Print-Clearance</div>
					<div class="portlet-header ui-widget-header float-right" style="display: none;"  id="print-transfer-letter">Print-Transfer-Letter</div>
					<div class="portlet-header ui-widget-header float-right" style="display: none;"  id="create-identical">Create-Identical</div>
				 </div>
			</div>
		</div>
		<div id="release-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="release-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">OK</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div>
		</div>
		<div id="contract-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="contract-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div>
		</div>
	</div>
</body>