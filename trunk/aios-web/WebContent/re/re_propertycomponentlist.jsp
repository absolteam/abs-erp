 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;var oTable; var selectRow=""; var aData ="";
var componentId=0;var aSelected = []; var componentName="";
$(function(){ 
	$('.formError').remove();  
	if(typeof($('#component-popup')!="undefined")){ 
		$('#component-popup').dialog('destroy');		
		$('#component-popup').remove(); 
	}
	
	if(typeof($('#common-popup')!="undefined")){ 
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
	}
	
	$('#component').dataTable({ 
		"sAjaxSource": "property_component_getjsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "COMPONENT_ID", "bVisible": false},
			{ "sTitle": "PROPERTY_ID",  "bVisible": false},
			{ "sTitle": "COMPONENT_TYPE_ID", "bVisible": false},
			{ "sTitle": '<fmt:message key="re.property.info.componentName" />',"sWidth": "200px"},
			{ "sTitle": '<fmt:message key="re.property.info.propertytype" />',"sWidth": "125px"},
			{ "sTitle": '<fmt:message key="re.property.info.propertyName" />'},
			{ "sTitle": '<fmt:message key="re.property.info.landsize" />'},
			{ "sTitle": '<fmt:message key="re.property.info.status" />',"sWidth": "125px"},
			
		],
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	//init datatable
	oTable = $('#component').dataTable();
	 
	/* Click event handler */
	$('#component tbody tr').live('click', function () {  
		  if($(this).hasClass('row_selected')){ 
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          componentId=aData[0]; 
	          componentName=aData[3];
	      }
	      else{ 
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          componentId=aData[0];
	          componentName=aData[3]; 
	      }
	});
	
$('#add').click(function(){  
	var showPage="add";
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/property_component_entry.action", 
     	async: false,
     	data:{showPage:showPage,recordId:0,componentId:0},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result);  
			$.scrollTo(0,300);
		} 		
	}); 
}); 

$('#edit').click(function(){ 

	if(componentId == null || componentId == 0 ) {
		$('.commonErr').hide().html("Please select one record to edit...").slideDown();
		return false; 
	}
	
	var showPage="edit"; 
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/property_component_editentry.action", 
     	async: false,
     	data:{showPage:showPage, componentId:componentId,recordId:0},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result);  
			$.scrollTo(0,300);
		} 		
	}); 
}); 

$("#view").click( function() { 
		if(componentId == null || componentId == 0 ) {
			$('.commonErr').hide().html("Please select one record to view...").slideDown();
			return false; 
		}		
			
		$('#loading').fadeIn();
		window.open('getPropertyCompDetail.action?recordId='
				+ componentId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return true;
	});


$('#delete').click(function(){  
	$('.success,.error').hide();
	if(componentId == null || componentId == 0 ) {
		$('.commonErr').hide().html("Please select one record to delete...").slideDown();
		return false; 
	}	
	
	var cnfrm = confirm("Selected component will be permanently deleted.");
	if(!cnfrm) {
		return false;
	}
	
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/property_component_deleteentry.action", 
     	async: false,
     	data:{componentId:componentId, componentName:componentName},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result);  
			$.scrollTo(0,300);
		} 		
	}); 
}); 


});

setTimeout("$('#nortify').fadeOut()", 5000);

</script> 
<div id="main-content">
		<div id="" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyComponent" /></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="display:none;"></div>
					<div  class="success response-msg commonSuccess ui-corner-all" style="display:none;"></div>
				 </div> 
				 
				 <div id="nortify">
					<c:choose>
						<c:when test="${COMPONENT_CHILD_EXISTS eq 1 && COMPONENT_CHILD_EXISTS ne 0}">
							<div class="error response-msg ui-corner-all">Please delete units first</div>
						</c:when>
						<c:when test="${COMPONENT_CHILD_EXISTS eq 0 && COMPONENT_CHILD_EXISTS ne 1}">
								<div class="success response-msg ui-corner-all">Component deleted</div> 
						</c:when>
					</c:choose>
				</div>
				 
				 <div id="rightclickarea">
					 <div id="property_component">
						<table class="display" id="component"></table>
					 </div>  
				 </div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Delete</span></div>
				 </div>
				 
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add" /></div>
				 </div>
			</div>
		</div>
</div>