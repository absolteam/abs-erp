<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var personId="";
var contractId="";
var flatDescription="";
var approvedStatus="";

$(document).ready(function (){

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/maintenance_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#maintenancesearchcontent').html(result);  
     		$('#temp1').html($('#temp').html()); 
     		$('#temp').html("");
	 	},  
	 	error:function(result){ 
	 		 $('#maintenancesearchcontent').html(result); 
	 	} 
	});
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_flat_maintainence_list.action", 
		 datatype: "json", 
		 colNames:['flatId','contract ID','<fmt:message key="re.property.info.contractnumber"/>','Person ID','<fmt:message key="re.property.info.engineername"/>','<fmt:message key="re.property.info.description"/>','Work Flow Status'], 
		 colModel:[ 
				{name:'flatId',index:'FLAT_ID', width:100,  sortable:true},
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
		  		{name:'contractNo',index:'CONTRACT_NO', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'personId',index:'PERSON_ID', width:100,  sortable:true},
		  		 {name:'personName',index:'PERSON_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'flatDescription',index:'DESCRIPTION', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true, sorttype:"data"},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'FLAT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.flatmaintenancerelease"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["flatId","contractId","personId"]);

 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_flat_maintainence.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		}); 
		return true; 
	});
	
	$("#edit").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.flatId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_flat_maintainence_redirect.action",  
		     	data: {flatId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.flatId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_flat_maintainence_redirect.action",  
		     	data: {flatId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null'){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.flatId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_confirm_flat_maintainence.action",  
		     	data: {flatId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		$('.commonErr').hide();
		personId=Number($('#personId option:selected').val());
		contractId=Number($('#contractId option:selected').val());
		flatDescription=$('#flatDescription').val();
		approvedStatus=$('#approvedStatus').val();
		if((personId != 0  || contractId != 0  || flatDescription != null && flatDescription.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0)){
		var tempExp= /%/;
		 var tempMatch=flatDescription.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 flatDescription=flatDescription.replace('%','$');
				 tempMatch=flatDescription.search(tempExp);
			 }
		 }else{
			 flatDescription=flatDescription+'$';
		 }
		$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_flat_maintainence.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
					$('.commonErr').hide();
					$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#gridDiv").html(data);  //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
		}else{
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/flat_maintainence_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		 <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.flatmaintenancerelease"/></div>
			<div class="portlet-content">
			  <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width60"> 
					 <div class="width:60%">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm"> 
						   <div id="maintenancesearchcontent">
			   				</div>
			   				<div>
								<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.description"/></label> 
								<input type="text" id="flatDescription" style="width: 29%;">
							</div>
			   				 <div id="temp1">
			   				</div>
							 </div>
						  <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>