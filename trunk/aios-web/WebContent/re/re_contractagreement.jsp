<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">

.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.legal_icon_view{
   display: block;
    height: 20px;
    width: 20px;
    background: url(images/icon_legal_lg.png) no-repeat;
   	background-size:23px 26px;
   	background-position:center;
    float: right;
    text-align: center;
    top:4px;
  
}
.tableRowBackgroud{
	background-color: #F5B5CF!important;
}
</style>
</head>
<script type="text/javascript">
var id;
var contractNumber="";
var buildingId="";
var tenantId="";
var contractDate="";
var fromDate="";
var toDate="";
var grandTotal="";
var approvedStatus="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s1=0; 
var transferType;
var transferText;
var transferAccount;
var contractPrintStyle = "";
var cancelFlag = "";
var bankReceiptsId="";var contractTemplate="";
$(document).ready(function (){
	if(typeof($('#contract-popup')!="undefined")){ 
		$('#contract-popup').dialog('destroy');		
		$('#contract-popup').remove(); 
	}
	
	$('.formError').remove();
		
 	//Added by rafiq for Legal Listing control view
	if(Number($('#legalProcess').val())==1){
		$('#contract').dataTable({ 
	 		"sAjaxSource": "json_contract_agreement_list.action",
	 	    "sPaginationType": "full_numbers",
	 	    "bJQueryUI": true, 
	 		"aoColumns": [
	 			{ "sTitle": 'Contract ID', "bVisible": false},
	 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
	 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
	 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"}, 
	 			{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.createdOn"/>',"sWidth": "150px"},
	 			{ "sTitle": 'Options',"sWidth": "100px","bVisible": false},
	 			{ "sTitle": 'Cancel Flag', "bVisible": false},
	 			{ "sTitle": 'Property Id', "bVisible": false},
	 			{ "sTitle": 'Bank Receipt Id', "bVisible": false},
	 			
	 		],
	 		"sScrollY": $("#main-content").height() - 235,
	 		"aaSorting": [[1, 'desc']], 
	 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
	 			if (jQuery.inArray(aData1.DT_RowId, aSelected) !== -1) {
	 				$(nRow).addClass('row_selected');
	 			}
	 			if ( aData1[9] == "Y" )
		      	{
		        	$(nRow).addClass( 'tableRowBackgroud' );
		     	 }
	 			var len=$('#contract tbody tr').size();
	 		}
	 	});	
	 	
	 	oTable = $('#contract').dataTable();
		
		$('#contract tbody tr').live('dblclick', function () {
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          $("#legalRecordId").val(aData[0]);
			      useCaseViewCall();
			       $('#DOMWindowOverlay').trigger('click');
			       return false;
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          $("#legalRecordId").val(aData[0]);
			      useCaseViewCall();
			      $('#DOMWindowOverlay').trigger('click');
			      return false;
		      }
		});
		
		$('.vmenu').html("");
		$('#action_buttons').hide();
		
	}else{
		$('#contract').dataTable({ 
	 		"sAjaxSource": "json_contract_agreement_list.action",
	 	    "sPaginationType": "full_numbers",
	 	    "bJQueryUI": true, 
	 		"aoColumns": [
	 			{ "sTitle": 'Contract ID', "bVisible": false},
	 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
	 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
	 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.createdOn"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.options"/>',"sWidth": "100px"},
	 			{ "sTitle": 'Cancel Flag', "bVisible": false},
	 			{ "sTitle": 'Property Id', "bVisible": false},
	 			{ "sTitle": 'Bank Receipt Id', "bVisible": false},
	 		],
	 		"sScrollY": (Number($("#main-content").height() - 200) / 2 ) - 55,
	 		"aaSorting": [[1, 'desc']], 
	 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
	 			if (jQuery.inArray(aData1.DT_RowId, aSelected) !== -1) {
	 				$(nRow).addClass('row_selected');
	 			}
	 			if ( aData1[9] == "Y" )
			      {
			        $(nRow).addClass( 'tableRowBackgroud' );
			      }
	 		},
			"fnInitComplete": function (oSettings, json) {
				getRenewList();
		  	}
	 	});	
	 	
	 	oTable = $('#contract').dataTable();
		
		//Legal script end	 
	}
 	

 	/* Click event handler */
 	$('#contract tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s1=aData[0];
 	        cancelFlag=aData[9];
 	       bankReceiptsId=aData[11];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s1=aData[0];
 	       cancelFlag=aData[9];
 	       bankReceiptsId=aData[11];
 	    }
 	});
 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_contract_agreement.action", 
			data: {contractId: 0,recordId:0},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		}); 
		return false; 
	});
	
	$("#edit").click( function() { 
		$('.success').fadeOut();
	  
		if(s1 == null || s1 == 0 ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{
			if(cancelFlag=='N'){
				$('#loading').fadeIn();		
				id = s1; 
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/add_contract_agreement.action",  
			     	data: {contractId: id,recordId:0}, 
			     	async: false,
					dataType: "html",
					cache: false,
			     	success: function(data){
						$("#main-wrapper").html(data); //gridDiv main-wrapper
						$('#loading').fadeOut();
			     	}
				});
				return true;	
			}else{
				$('.commonErr').hide().html("This contract is in cancel process!!!").slideDown();
				return false; 
			}

		} 
		$.scrollTo(0,300);
		return false;
	});

	$('#view').click(function(){ 
 	 	var contractId = s1; 
		if(contractId == null || contractId == 0) {
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}	
		
		$('#loading').fadeIn();
		window.open('getPropertyContractDetail.action?recordId='
				+ contractId + '&approvalFlag=N&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
	}); 
	
	$('#receipt-print').click(function(){ 
 	 	var contractId = s1; 
		if(contractId == null || contractId == 0) {
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}	
		
		if(bankReceiptsId==null || bankReceiptsId=='' || bankReceiptsId==0){
			$('.commonErr').hide().html("Receipt is not ready to print !!!").slideDown();
			return false; 
		}
		
		$('#loading').fadeIn();
		 window.open('contractreceipt_printview.action?contractId='
					+ contractId +'','_blank','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
	}); 
	
	
	
	$("#delete").click(function(){
		$('.success').fadeOut();
		
		if(s1 == null || s1==0){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			var cnfrm = confirm("Selected contract will be permanently deleted.");
			if(!cnfrm) {
				return false;
			}
			
			$('#loading').fadeIn();		
			id = s1;
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_confirm_contract_agreement.action",  
		     	data: {contractId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		}
		return false; 
	});

	$('#cancellation').click(function(){ 
 	 	var contractId = s1;
 	 	if(s1 == null || s1 == 0 ){
			$('.commonErr').hide().html("Please select one record to cancel !!!").slideDown();
			return false; 
		} else {
	 	 	if(cancelFlag=='N'){
	 	 	var answer = confirm ("You want to 'Cancel' this Contract?");
				if (answer){
					$.ajax({
						type: "POST",  
						url: "<%=request.getContextPath()%>/contract_cancellation_initiation.action",  
				     	data: {contractId: contractId}, 
				     	async: false,
						dataType: "html",
						cache: false,
				     	success: function(data){
							$("#main-wrapper").html(data); //gridDiv main-wrapper
				     	}
					});
				}else{
		
					return false;
				}
	 	 	}
			else{
				$('.commonErr').hide().html("This contract is already in cancel process!!!").slideDown();
				return false; 
			}
		}
	}); 

	//Default Date Picker
	$('#defaultPopup').datepick();

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_agreement_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
	var proceedToPrint = function () {
		
		if(s1 == null || s1=='null' || s1 == 0){
			$('.commonErr').hide().html("Please select one record to print...").slideDown();
			return false; 
		}
		var idval = s1;
		// HTML PRINT
		callContractTransaction(idval); 
		if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false" || '${THIS.templatePrint}' == "") {
			
			window.open('<%=request.getContextPath()%>/contract_printout.action?contractId='
					+ idval + '&contractTransferType=' + transferType
					+ '&contractTransferText=' + transferText + '&contractTransferAccountNumber=' + transferAccount
					+ '&contractPrintStyle=' + contractPrintStyle,
					'_blank', 
					'width=800,height=700,scrollbars=yes,left=100px,top=2px');
			return false;
		}
		
		// TEMPLATE PRINT
		else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") { 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/contract_template_printout.action", 
				async: false, 
				dataType: "html",
				data: {
			   		contractId: idval,
			   		contractTransferType: transferType,
			   		contractTransferText: transferText,
			   		contractTransferAccountNumber: transferAccount
			   		
		   		},
				cache: false,
				success:function(result){   
					window.open('<%=request.getContextPath()%>/printing/applet/re_print_contract.html?contractTemplate='+contractTemplate,'_blank', 
								'width=450,height=150,scrollbars=yes,left=200px,top=200px'); 
				},
				error:function(result){  
					alert(result);
				}
			}); 
			return false;
		}
	};
	
	$('#contract-popup-screen').dialog({
			minwidth: 'auto',
			width:400,
			height:400,  
			bgiframe: false,
			modal: true,
			autoOpen: false,
			buttons: { 
				"Print": function() {
					$('#err_label').hide();
					transferAccount = $('#transferAccount').val();
					contractTemplate= $('#contracTemplate').val();
					if(transferAccount == "") {
						 $('#err_label').fadeIn();
						return false;
					}						
					proceedToPrint();
					$(this).dialog("close"); 
				}
			}
	});
	
	
	$('.contract_print').click(function(){  
		
	}); 
		
	 $('.transfer-popup').live('click',function(){  
		 	
			var parentTR=$(this).parent().parent().get(0);
			var idval=getId(parentTR); 
		 	$('#contract-popup-screen').dialog('open');
			$('#ui-dialog-title-contract-popup-screen').text("Print Options");
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/transfer_options.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.contract-result').html(result); 
				},
				error:function(result){  
					 $('.contract-result').html(result); 
				}
			});		
		 	return false;
		});

	
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	

	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function getId(parentTR){ 
		if ($(parentTR).hasClass('row_selected') ) { 
			$(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        s1=aData[0]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        s1=aData[0];
	    }
		return s1;
	}
	function callContractTransaction(contractId){  
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_agreement_transaction.action",   
	     	async: false,
	     	data:{contractId:contractId},
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){ 
				$('#loading').fadeOut();
				return false; 
	     	}  
		}); 
		return false;
	} 
	function getRenewList(){
		//Renew/Relase content fetch
	 	$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/contract_release_renewal_json_redirect.action", 
				data: {contractId: 0},
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#gridDiv1").html(result); 
				} 
		}); 
	}
</script>

	<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractagreement"/></div>	 
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<input type="hidden" id="legalProcess" value="${LEGAL_PROCESS}" />
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
				 </div>
				 
				 <div id="rightclickarea">
					<div id="gridDiv">
						<table class="display dataTable" id="contract"></table>
					</div>
				</div>		
				 <div class="vmenu">
		 	       	<div class="first_li"><span>Add</span></div>
					<div class="sep_li"></div>		 
					<div class="first_li"><span>Edit</span></div>
					<div class="first_li"><span>Receipt Print</span></div>
					<div class="first_li"><span>View</span></div>
					<div class="first_li"><span>Delete</span></div>
					<div class="first_li"><span>Cancellation</span></div>
				 </div>
				 
				<div id="action_buttons" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="accounts.materialdespatch.button.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="cancellation"><fmt:message key="re.property.info.cancellation" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="receipt-print">Receipt Print</div>
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view" /></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="accounts.materialdespatch.button.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="accounts.materialdespatch.button.add"/></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div style="min-height:5px;"></div>
		<div id="gridDiv1">
		</div>
		
		<div style="display: none; position: absolute; overflow: auto; 
			z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" 
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" 
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	
		<div id="contract-popup-screen" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="contract-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
		
	</div> 
</body>