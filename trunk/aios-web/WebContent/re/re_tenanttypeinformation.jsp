<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var descriptions="";
var noOfDaysAllowLate="";
var offerValidityDays="";
var approvedStatus="";
$(document).ready(function (){

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/tenant_type_search_load.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#approvedStatus').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#approvedStatus').html(result); 
	 	} 
	});
	
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-tenanttypeinformation.action", 
		 datatype: "json", 
		 colNames:['Tenant TypeId','<fmt:message key="re.property.info.tenanttypedescription"/>','<fmt:message key="re.property.info.noofdaysallowlate"/>','<fmt:message key="re.property.info.offervaliditydays"/>','<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'tenantTypeId',index:'TENANT_TYPE_ID', width:100,  sortable:true},
				{name:'descriptions',index:'DESCRIPTION', width:100,  sortable:true},
		  		{name:'noOfDaysAllowLate',index:'NO_OF_DAYS_ALLOW_LATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'offerValidityDays',index:'OFFER_VALIDITY_DAYS', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,sortable:true, sorttype:"data"},
		  ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'TENANT_TYPE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.tenanttypeinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["tenantTypeId"]);

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/tenant_type_informationadd.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	
	
	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.tenantTypeId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/tenant_type_informationedit.action",  
		     	data: {tenantTypeId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.tenantTypeId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/tenant_type_informationview.action",  
		     	data: {tenantTypeId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.tenantTypeId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/tenant_type_information_delete_req.action",  
		     	data:{tenantTypeId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		$('.commonErr').hide();
		descriptions=$('#descriptions').val();
		noOfDaysAllowLate=Number($('#noOfDaysAllowLate').val());
		offerValidityDays=Number($('#offerValidityDays').val());
		approvedStatus=$('#approvedStatus').val();
		if( (descriptions != null && descriptions.trim().length > 0 || noOfDaysAllowLate!=0 || offerValidityDays!=0 || approvedStatus != null && approvedStatus.trim().length > 0 )){
		var tempExp= /%/;
		 var tempMatch=descriptions.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 descriptions=descriptions.replace('%','$');
				 tempMatch=descriptions.search(tempExp);
			 }
		 }else{
			 description=descriptions+'$';
		 }
		$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_tenant_type_information.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.tenanttypeinformation"/></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					 <div class="width:60%">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm"> 
						   <div class="float-left" style="">
									<label style="font-weight:bold;"><fmt:message key="re.property.info.tenanttypedescription"/></label>
									<input type="text" id="descriptions" />
								</div>
								<div class="float-left" style="">
									<label style="font-weight:bold;"><fmt:message key="re.property.info.noofdaysallowlate"/></label>
									<input type="text" id="noOfDaysAllowLate" />
								</div>
								<div class="float-left" style="">
									<label style="font-weight:bold;"><fmt:message key="re.property.info.offervaliditydays"/></label>
									<input type="text" id="offerValidityDays" />
								</div>
								<div id="approvedstatussearch">
									<label style="font-weight:bold;"><fmt:message key="re.common.approvedstatus"/></label>
									<select id="approvedStatus" style="width: 20%;">
										<option value="">-Select-</option>
									</select>
			   					</div>
							 </div>
							 <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>