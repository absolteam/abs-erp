<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var className="";
var relativeClassId="";
$(document).ready(function (){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/maintenance_classes_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
			
     		$('#relativeClassId').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#relativeClassId').html(result); 
	 	} 
	});
	
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-maintenance_classes.action", 
		 datatype: "json", 
		 colNames:['CLASS_ID','<fmt:message key="re.property.info.classname"/>', 'RELATED CLASS ID', '<fmt:message key="re.property.info.relatedclass"/>'],
		 colModel:[ 
				{name:'classId',index:'CLASS_ID', width:100,  sortable:true},
				{name:'className',index:'CLASS_NAME', width:100,  sortable:true},
		  		{name:'relativeClassId',index:'RELATED_CLASS_ID', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'relativeClass',index:'CLASS_NAME', width:100,  sortable:true},
		  		
		 ], 		  				  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'classes.CLASS_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:'<fmt:message key="re.property.info.maintenanceclasses"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["classId","relativeClassId"]);  

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/maintenance_classes_add.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	
 	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.classId; 
			//alert("feeId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintenance_classes_edit.action",  
		     	data: {classId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
			     
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	},
		     	error: function(data) 
				{
		     		
		     		$("#main-wrapper").html(data);
				}
			});
			return true;	
		} 
		return false;
	});

 	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to view");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.classId; 
			//alert("feeId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintenance_classes_view.action",  
		     	data: {classId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
			     $("#main-wrapper").html(data); //gridDiv main-wrapper
		     	},
		     	error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.classId; 
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintenance_classes_delete_req.action",  
		     	data: {classId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut(); 
					
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		className=$('#className').val();
		
		relativeClassId=Number($('#relativeClassId option:selected').val());
		if( (className != null && className.trim().length > 0 || relativeClassId != 0 )){
		var tempExp= /%/;
		 var tempMatch=className.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 className=className.replace('%','$');
				 tempMatch=className.search(tempExp);
			 }
		 }else{
			 className=className+'$';
		 }
		$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_mainatenance_classes.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_classes_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	});
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	} 
	

</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.maintenanceclasses"/></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width60"> 
					 <div class="width:60%">
					    <form name="propertyFlat" id="propertyFlat">
						   <div id="hrm"> 
					   		  	<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.classname"/></label> 
									<input type="text" id="className"  class="width30 validate[required]" />
								</div> 
								<div ><label><fmt:message key="re.property.info.relatedclass"/></label>
									<select  id="relativeClassId" class="width30" >
										<option value="">-Select-</option>
									</select>
								</div>
						   </div>
						  <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.owner.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.owner.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.owner.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.owner.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>