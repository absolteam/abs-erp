<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var buildingId="";
var componentType="";
var fromDate="";
var toDate="";
var flatNumber="";
var approvedStatus="";
$(document).ready(function (){
	$('.formError').remove();

	//To Load Building & Component list 
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/flat_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#flatsearchcontent').html(result);  
     		$('#temp1').html($('#tempresult').html());  
     		$('#tempresult').html("");
     	},  
	 	error:function(result){ 
	 		 $('#flatsearchcontent').html(); 
	 	} 
	});
	
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-propertyflat.action", 
		 datatype: "json", 
		 colNames:['propertyFlatId','buildingId','<fmt:message key="re.property.info.componenttype"/>','<fmt:message key="re.property.info.buildingname"/>','<fmt:message key="re.property.info.flatnumber"/>','<fmt:message key="re.property.info.componenttypename"/>','<fmt:message key="re.property.info.fromdate"/>','<fmt:message key="re.property.info.todate"/>',
		  		 '<fmt:message key="re.property.info.totalarea"/>','Work Flow Status'], 
		 colModel:[ 
				{name:'propertyFlatId',index:'PROP_FLAT_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'componentType',index:'COMPONENT_TYPE', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME', width:100,  sortable:true},
				{name:'flatNumber',index:'FLAT_NUMBER', width:100,  sortable:true},
				{name:'componentTypeName',index:'A.LOV_CODE', width:150,sortable:true, sorttype:"data"},
				{name:'fromDate',index:'FROM_DATE', width:100,  sortable:true},
		  		{name:'todate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'totalArea',index:'TOTAL_AREA', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,  sortable:true},
		  		 
		  	 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'PROP_FLAT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.propertyflat"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["propertyFlatId","buildingId","componentType"]);

	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_property_flat.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			propertyFlatId = s2.propertyFlatId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_property_flat.action",  
		     	data: {propertyFlatId: propertyFlatId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false;
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			propertyFlatId = s2.propertyFlatId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_property_flat.action",  
		     	data: {propertyFlatId: propertyFlatId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false;
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			propertyFlatId = s2.propertyFlatId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_confirm_property_flat.action",  
		     	data:{propertyFlatId: propertyFlatId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});

	$("#search").click(function(){
		buildingId=Number($('#buildingId option:selected').val());
		componentType=Number($('#componentType option:selected').val());
		fromDate=$('#startPicker').val();
	    toDate=$('#endPicker').val();
		//componentTypeName=$('#componentTypeName').val();
		flatNumber=$('#flatNumber').val();
		approvedStatus=$('#approvedStatus').val();
	if((buildingId != 0  || componentType != 0  || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 
				|| flatNumber != null && flatNumber.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0 )){
			$('.commonErr').fadeOut();
			 var tempExp= /%/;
			 var tempMatch=flatNumber.search(tempExp);
			 tempMatch!= -1;
			 if(flatNumber != ""){
				 tempMatch=flatNumber.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 flatNumber=flatNumber.replace('%','$');
						 tempMatch=flatNumber.search(tempExp);
					 }
				 }else{
					 flatNumber=flatNumber+'$';
				 }
			 }
			$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/search_property_flat.action",  
			     	async: false,
					dataType: "html",
					cache: false,
					error: function(data) 
					{
						$('.commonErr').hide();
			     		alert("error = " + $(data));
					},
			     	success: function(data)
			     	{
						$("#gridDiv").html(data);  //gridDiv main-wrapper
			     	}
			});
		}else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_flat_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	

</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		  <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyflat"/></div>	 
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width80"> 
					 <div class="">
					    <form name="propertyInfo" id="propertyInfo">
						   <div id="hrm" class="width50 float-left"> 
								<div id="flatsearchcontent">
			   					</div>
			   					<div>
									<label style="font-weight:bold;"><fmt:message key="re.property.info.flatnumber"/></label> <input type="text" id="flatNumber" />
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.fromdate"/></label> 
									<input type="text" id="startPicker" readonly="readonly">
								</div>
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.todate"/></label> 
									<input type="text" id="endPicker" readonly="readonly">
								</div>
								<div id="temp1">
								<label class=""><fmt:message key="re.common.approvedstatus"/></label>
									<select id="approvedStatus" style="width: 35.5%;">
									<option value="">-Select-</option>
									</select>
								
								</div>
						 </div>
						   <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
			 <div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>