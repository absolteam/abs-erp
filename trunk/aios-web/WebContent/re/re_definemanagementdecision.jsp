<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var buildingId="";
var flatId="";
var fromDate="";
var toDate="";
var approvedStatus="";

$(document).ready(function (){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/define_search_get.action",  
     	async: false,
     	dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#definesearchcontent').html(result);  
     		$('#temp1').html($('#temp').html());  
     		$('#temp').html("");
	 	},  
	 	error:function(result){ 
	 		 $('#definesearchcontent').html(result); 
	 	} 
	});
	
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-managementdecision.action", 
		 datatype: "json", 
		 colNames:['decisionId','BUILDING ID','<fmt:message key="re.property.info.buildingname"/>',
		  		 'flatId','<fmt:message key="re.property.info.flatnumber"/>','<fmt:message key="re.property.info.rentalamount"/>',
		  		 '<fmt:message key="re.property.info.fromdate"/>','<fmt:message key="re.property.info.todate"/>','<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'decisionId',index:'DIFINE_DECISION_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME ', width:150,sortable:true, sorttype:"data"},
				{name:'flatId',index:'FLAT_ID ', width:150,sortable:true, sorttype:"data"},
		  		 {name:'flatNumber',index:'FLAT_NUMBER', width:150,sortable:true, sorttype:"data"},
		  		 {name:'rentalAmount',index:'RENTAL_AMOUNT', width:150,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'toDate',index:'TO_DATE', width:100,  sortable:true,sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,  sortable:true,sorttype:"data"},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'DIFINE_DECISION_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.definemanagementdecision"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["decisionId","buildingId","flatId",'rentalAmount',"workflowStatus"]);

 	
 	$("#add").click( function() {
 		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/management_decision_add.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		$('#loading').fadeOut();
		return true; 
	});

 	
	
	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.decisionId; 
			id1=s2.buildingId;
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/management_decision_edit.action",  
		     	data: {decisionId: id, buildingId: id1}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.decisionId; 
			id1=s2.buildingId;
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/management_decision_view.action",  
		     	data: {decisionId: id, buildingId: id1}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.decisionId; 
			id1=s2.buildingId;
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/management_decision_delete_req.action",  
		     	data: {decisionId: id, buildingId: id1}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true; 
		} 
		return false; 
	});

	$("#search").click(function(){
		$('.commonErr').hide();
		buildingId=Number($('#buildingId option:selected').val());
		flatId=Number($('#flatId option:selected').val());
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		approvedStatus=$('#approvedStatus').val();
		if(( buildingId!=0 || flatId!=0 || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0
				|| approvedStatus != null && approvedStatus.trim().length > 0 )){
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_management_decision.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/management_decision_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 

	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definemanagementdecision"/></div>	 
			<div class="portlet-content">
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				</div>
				<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					<div class="">
				    <form name="propertyInfo" id="propertyInfo">
						   <div id="hrm" class="width50 float-left"> 
						   		 <div id="definesearchcontent">
				   				</div>
				   			</div>
							<div id="hrm" class="width50 float-right">
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.fromdate"/></label> 
									<input type="text" id="startPicker" readonly="readonly" class=""/>
								</div>
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.todate"/></label> 
									<input type="text" id="endPicker" readonly="readonly" class=""/>
								</div>
								<div id="temp1" style="display:none;">
								</div>
						 </div>
						   <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
