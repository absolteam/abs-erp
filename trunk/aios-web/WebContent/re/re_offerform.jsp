<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var customerId="";
var buildingId="";
var offerNumber="";
var offerDate="";
var approvedStatus="";

$(document).ready(function (){

	$('.formError').remove();

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/offer_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#offersearchcontent').html(result);  
     		$('#temp1').html($('#temp').html()); 
     		$('#temp').html("");
	 	},  
	 	error:function(result){ 
	 		 $('#offersearchcontent').html(result); 
	 	} 
	});
	
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json_offer_form_list.action", 
		 datatype: "json", 
		 colNames:['Offer ID','Building ID','<fmt:message key="re.offer.buildingname"/>','Customer ID','<fmt:message key="re.offer.customername"/>','<fmt:message key="re.offer.offerno"/>','<fmt:message key="re.offer.offerdate"/>','Work Flow Status'], 
		 colModel:[ 
				{name:'offerId',index:'OFFER_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		{name:'buildingName',index:'BUILDING_NAME', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'customerId',index:'CUSTOMER_ID', width:100,  sortable:true},
		  		 {name:'customerName',index:'TENANT_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'offerNumber',index:'OFFER_NUMBER', width:150,sortable:true, sorttype:"data"},
		  		{name:'offerDate',index:'OFFER_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true, sorttype:"data"},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'OFFER_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.offer.offerform"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["offerId","buildingId","customerId"]);

 	
 	$("#add").click( function() {
 		var empcode='${emp_CODE}';
		if(empcode != ""){
	 		$('#loading').fadeIn(); 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/add_offer_form.action", 
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result); 
					$('#loading').fadeOut();
				} 
			}); 
			return true; 
		}else{
			$('.error').fadeOut();
			$('.success').fadeOut();
			$('.commonErr').show();
			$('.commonErr').hide().html("Session Expired !!!").slideDown();
			return false; 
		}
	});
	
	$("#edit").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.offerId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_offer_form_redirect.action",  
		     	data: {offerId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	//Default Date Picker
	$('#defaultPopup').datepick();

	$("#view").click( function() { 
		$('.commonErr').hide();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null ){
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.offerId; 
			//alert("headerId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_offer_form_redirect.action",  
		     	data: {offerId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null'){
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}else{	
			$('#loading').fadeIn();		
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.offerId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_confirm_offer_form.action",  
		     	data: {offerId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) {
		     		$("#main-wrapper").html(data);
		     		$('#loading').fadeOut();
				},
		     	success: function(data){
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		$('.commonErr').hide();
		customerId=Number($('#customerId option:selected').val());
		buildingId=Number($('#buildingId option:selected').val());
		offerNumber=Number($('#offerNumber').val());
		offerDate=$('#defaultPopup').val();
		approvedStatus=$('#approvedStatus').val();
		var empcode='${emp_CODE}';
		if(empcode != ""){
			if((customerId != 0 || buildingId != 0 || offerNumber !=0 || offerDate != null && offerDate.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0)){
			$('#loading').fadeIn();
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/search_offer_form.action",  
			     	async: false,
					dataType: "html",
					cache: false,
					error: function(data) {
						$('.commonErr').hide();
						$('#loading').fadeOut();
					},
			     	success: function(data){
						$("#gridDiv").html(data);  //gridDiv main-wrapper
						$('#loading').fadeOut();
			     	}
				});
			}else{
				$('.error').fadeOut();
				$('.success').fadeOut();
				$('.commonErr').show();
				$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
				return false; 
			}
		}else{
			$('.error').fadeOut();
			$('.success').fadeOut();
			$('.commonErr').show();
			$('.commonErr').hide().html("Session Expired !!!").slideDown();
			return false; 
		}
		});

	$("#cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/offer_form_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerform"/></div>	 
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	 
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				 <div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width80"> 
					 <div class="">
					    <form name="propertyInfo" id="propertyInfo">
						   <div id="hrm" class="width50 float-left"> 
								<div id="offersearchcontent">
			   					</div>
			   					<div>
									<label style="font-weight:bold;"><fmt:message key="re.offer.offernumber"/></label> <input type="text" id="offerNumber" />
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div>
									<label class="" style="font-weight:bold;"><fmt:message key="re.offer.offerdate"/></label> 
									<input type="text" id="defaultPopup" readonly="readonly" class=""/>
								</div>
								<div id="temp1">
								</div>
						 </div>
						   <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.offer.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.offer.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.offer.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.offer.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>