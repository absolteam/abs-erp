<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var descriptions="";
var calculatedAs="";
var fromDate="";
var toDate="";
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-definefee.action", 
		 datatype: "json", 
		 colNames:['FEE_ID','<fmt:message key="re.property.info.feedescription"/>','<fmt:message key="re.property.info.calculatedas"/>','<fmt:message key="re.property.info.feeamount"/>','<fmt:message key="re.property.info.deposit"/>','<fmt:message key="re.property.info.fromdate"/>','<fmt:message key="re.property.info.todate"/>'], 
		 colModel:[ 
				{name:'feeId',index:'FEE_ID', width:100,  sortable:true},
				{name:'descriptions',index:'DESCRIPTION', width:100,  sortable:true},
		  		{name:'calculatedAs',index:'CALCULATED_AS', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'feeAmount',index:'FEE_AMOUNT', width:100,  sortable:true},
		  		{name:'deposit',index:'DEPOSIT', width:100,  sortable:true},
		  		{name:'FROM_DATE',index:'fromDate', width:100,  sortable:true},
		  		{name:'TO_DATE',index:'toDate', width:100,  sortable:true},
		  		
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'FEE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.definefee"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["feeId","deposit"]);

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/define_fee_add.action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/fee_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#calculatedAs').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#calculatedAs').html(result); 
	 	} 
	});

 	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.feeId; 
			//alert("feeId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/define_fee_edit.action",  
		     	data: {feeId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
			     
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	},
		     	error: function(data) 
				{
		     		
		     		$("#main-wrapper").html(data);
				}
			});
			return true;	
		} 
		return false;
	});

 	$("#view").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.feeId; 
			//alert("feeId : "+id);
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/define_fee_view.action",  
		     	data: {feeId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
			     $("#main-wrapper").html(data); //gridDiv main-wrapper
		     	},
		     	error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				}
			});
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.feeId; 
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/define_fee_delete_req.action",  
		     	data: {feeId: id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
					$('#loading').fadeOut(); 
					
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		descriptions=$('#descriptions').val();
		calculatedAs=$('#calculatedAs option:selected').val();
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		if( (descriptions != null && descriptions.trim().length > 0 || calculatedAs != null && calculatedAs.trim().length > 0 || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 )){
		var tempExp= /%/;
		 var tempMatch=descriptions.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 descriptions=descriptions.replace('%','$');
				 tempMatch=descriptions.search(tempExp);
			 }
		 }else{
			 descriptions=descriptions+'$';
		 }
		 tempMatch!= -1;
		 tempMatch=calculatedAs.search(tempExp);
			 if(tempMatch!= -1 ){
				 while(tempMatch!= -1){
					 calculatedAs=calculatedAs.replace('%','$');
					 tempMatch=calculatedAs.search(tempExp);
				 }
			 }else{
				 calculatedAs=calculatedAs+'$';
			 }
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_define_fee.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_fee_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	});
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	} 
	

</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definefee"/></div>	 
			<div class="portlet-content">
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width60"> 
					 <div class="width:60%">
					    <form name="propertyFla" id="propertyFlat">
						   <div id="hrm"> 
					   		  <div><label style="font-weight:bold;"><fmt:message key="re.property.info.calculatedas"/></label>
									<select id="calculatedAs" class="float-left"  style="width: 23%;">
										<option value="">-Select-</option>
									</select>
								</div>	
						   		<div class="float-left" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.fromdate"/></label> 
									<input type="text" id="startPicker" class="" readonly="readonly">
								</div>
								<div class="float-left" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.todate"/></label> 
									<input type="text" id="endPicker" class=""  readonly="readonly">
								</div>
								<div class="float-left" style="">
									<label style="font-weight:bold;"><fmt:message key="re.property.info.feedescription"/></label> <input type="text" id="descriptions" class="">
								</div>
						   </div>
						  <div class="clearfix"></div>
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.owner.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.owner.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.owner.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.owner.info.add"/></div>
				</div>
		 </div>
	</div>
</div>
