<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var buidingId="";
var brandId="";
var modelNumber="";
var noOfPieces="";
var fromDate="";
var toDate="";


$(document).ready(function (){

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/maintenance_asset_search_get.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#maintenanceassetsearchcontent').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#maintenanceassetsearchcontent').html(result); 
	 	} 
	});
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-maintenanceasset.action", 
		 datatype: "json", 
		 colNames:['buildingAssetId','<fmt:message key="re.property.info.classname"/>','<fmt:message key="re.property.info.subclassname"/>',
		  		 '<fmt:message key="re.owner.info.brandid"/>','<fmt:message key="re.owner.info.brand"/>','<fmt:message key="re.owner.info.modelnumber"/>',
		  		 '<fmt:message key="re.owner.info.noofpieces"/>','<fmt:message key="re.owner.info.fromdate"/>','<fmt:message key="re.owner.info.todate"/>'], 
		 colModel:[ 
				{name:'buildingAssetId',index:'BUILDING_MAINTENANCE_ASSET_ID', width:100,  sortable:true},
				{name:'className',index:'CLASS_NAME', width:150,sortable:true, sorttype:"data"},
				{name:'subClassName',index:'SUB_CLASS_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'brandId',index:'BRAND', width:100,  sortable:true},
		  		{name:'brand',index:'LOV_MEANING', width:150,sortable:true, sorttype:"data"},
		  		{name:'modelNumber',index:'MODEL_NUMBER', width:150,sortable:true, sorttype:"data"},
		  		{name:'noOfPieces',index:'NO_OF_PIECES', width:150,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'BEGIN_DATE', width:150,sortable:true, sorttype:"data"}, 
		  		{name:'toDate',index:'END_DATE', width:150,sortable:true, sorttype:"data"},
		  		
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'BUILDING_MAINTENANCE_ASSET_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.owner.info.buildingmaintenanceasset"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["buildingAssetId","brandId"]);

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/maintainence_asset_add.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	
	
	$("#edit").click( function() { 
		$('.success').fadeOut();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.buildingAssetId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintainence_asset_edit.action",  
		     	data: {buildingAssetId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});

	$("#view").click( function() { 
		$('.success').fadeOut();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.buildingAssetId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintainence_asset_view.action",  
		     	data: {buildingAssetId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	
	$("#delete").click(function(){
		$('.success').fadeOut();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.buildingAssetId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintainence_asset_delete_req.action",  
		     	data:{buildingAssetId:id}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		brandId=$('#brandId').val();
		modelNumber=$('#modelNumber').val();
		noOfPieces=Number($('#noOfPieces').val());
		fromDate=$('#startPicker').val();
		toDate=$('#endPicker').val();
		if((modelNumber != null && modelNumber.trim().length > 0 || fromDate != null && fromDate.trim().length > 0 || toDate != null && toDate.trim().length > 0 || brandId != null && brandId.trim().length > 0 || noOfPieces != 0 )){
		var tempExp= /%/;
		var tempMatch=modelNumber.search(tempExp);
		if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 modelNumber=modelNumber.replace('%','$');
				 tempMatch=modelNumber.search(tempExp);
			 }
			}else{
				modelNumber=modelNumber+'$';
			}
			$('.commonErr').fadeOut();
			$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/search_maintainence_asset.action",  
			     	async: false,
					dataType: "html",
					cache: false,
					error: function(data) 
					{
						$('.commonErr').hide();
			     		alert("error = " + $(data));
					},
			     	success: function(data)
			     	{
						$("#gridDiv").html(data);  //gridDiv main-wrapper
			     	}
				});
		}else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
		});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_asset_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
</script>
<body>
 <div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.buildingmaintenanceasset"/></div>
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>	 
			<div class="portlet-content">
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
				 .<div  style="margin-bottom:10px;"class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width70"> 
					 <div class="">
					    <form name="propertyFlat" id="propertyFlat">
					     <div id="hrm" class="width50 float-left"> 
						     <div id="maintenanceassetsearchcontent">
			   					</div>
								<div class="" style="">
									<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.modelnumber"/></label> 
									<input type="text" id="modelNumber">
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.noofpieces"/></label> <input type="text" id="noOfPieces" />
								</div>
							</div>
							<div id="hrm" class="width50 float-right">
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.fromdate"/></label> <input type="text" readonly="readonly" id="startPicker" />
								</div>
								<div class="" style="">
									<label style="font-weight:bold;"><fmt:message key="re.owner.info.todate"/></label> <input type="text" readonly="readonly" id="endPicker" />
								</div>
							</div>
						 <div class="clearfix"></div>
						   <div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
							<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.owner.info.search"/></div>
						</form>
				 	</div>
		 		 </div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.owner.info.view"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.owner.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.owner.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.owner.info.add"/></div>
				</div>
			</div>
		</div>
	</div>
</body>