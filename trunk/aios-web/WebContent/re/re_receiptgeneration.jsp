<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
$(document).ready(function (){
		 
	$('.formError').remove();

	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/property_info_search_load.action",  
     	async: false,
		dataType: "html",
		cache: false,
		success:function(result){ 
     		$('#approvedStatus').html(result);  
	 	},  
	 	error:function(result){ 
	 		 $('#approvedStatus').html(result); 
	 	} 
	});

	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-list_generation_list.action", 
		 datatype: "json", 
		 colNames:['Invoice Id',
		  		 'Invoice Number',
		  		 'Invoice Date',
		  		 'Currency',
		  		 'Rate Type',
		  		 'Exchange Rate',
		  		 'Transaction Type',
		  		 'Contract Id',
		  		 'Contract Number',
		  		 'Due Date',
		  		 'Amount ',
		  		 'Description',
		  		 'Work Flow Status'], 
		 colModel:[ 
				{name:'invoiceId',index:'INVOICE_ID', width:100,  sortable:true},
				{name:'invoiceNumber',index:'INVOICE_NUMBER', width:100,  sortable:true},
		  		{name:'invoiceDate',index:'INVOICE_DATE', width:100,sortable:true, sorttype:"data"} ,
		  		{name:'currencyCode',index:'CURRENCY', width:100,  sortable:true},
		  		{name:'rateType',index:'RATE_TYPE', width:100,  sortable:true},
		  		{name:'exchangeRate',index:'EXCHANGE_RATE', width:100,  sortable:true},
		  		{name:'transactionType',index:'TRANSACTION_TYPE', width:100,  sortable:true},
		  		{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
		  		{name:'contractNumber',index:'CONTRACT_NO', width:100,  sortable:true},
		  		{name:'dueDate',index:'DUE_DATE', width:100,  sortable:true},
		  		{name:'amount',index:'AMOUNT', width:100,  sortable:true},
		  		{name:'description',index:'DESCRIPTION', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,  sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'INVOICE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'Receipt Generation'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["invoiceId","rateType","exchangeRate","transactionType","contractId","workflowStatus"]);

 	$("#add").click( function() {
 	 	
 		$('#loading').fadeIn(); 
 		companyId=$('#headerCompanyId').val();
 		ledgerId=$('#headerLedgerId').val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/re_receipt_generation_add.action", 
	     	async: false,
			dataType: "html",
			data:{companyId: companyId,ledgerId: ledgerId},
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
				$('#loading').fadeOut();
			} 
		
		}); $.scrollTo(0,300); 
		return true; 
	});

 	$("#edit").click( function() { 
 		$('.success').fadeOut();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to edit !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.invoiceId; 
			companyId=$('#headerCompanyId').val();
	 		ledgerId=$('#headerLedgerId').val();
			//alert("headerId : "+id);
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/re_receipt_generation_edit.action",  
		     	data: {invoiceId: id,companyId: companyId,ledgerId: ledgerId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});

 	$("#view").click( function() { 
 		$('.success').fadeOut();
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			$('.commonErr').hide().html("Please select one record to view !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			id = s2.invoiceId; 
			companyId=$('#headerCompanyId').val();
	 		ledgerId=$('#headerLedgerId').val();
			//alert("headerId : "+id);
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/re_receipt_generation_view.action",  
		     	data: {invoiceId: id,companyId: companyId,ledgerId: ledgerId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true;	
		} 
		return false;
	});

	$("#delete").click(function(){
		$('.success').fadeOut();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			$('.commonErr').hide().html("Please select one record to delete !!!").slideDown();
			return false; 
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.invoiceId; 
			companyId=$('#headerCompanyId').val();
	 		ledgerId=$('#headerLedgerId').val();
			$('#loading').fadeIn();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/re_receipt_generation_delete_redirect.action",  
		     	data: {invoiceId: id,companyId: companyId,ledgerId: ledgerId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			$('#loading').fadeOut();
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		$('.success').fadeOut();
		tenantName=$('#tenantName').val();
		tenantType=Number($('#tenantType option:selected').val());
		uaeIdNo=$('#uaeIdNo').val();
		mobileNo=$('#mobileNo').val();
		birthDate=$('#birthDate').val();
		presentAddress=$('#presentAddress').val();
		profession=$('#profession').val();
		approvedStatus=$('#approvedStatus').val();
		if((tenantType!=0 || tenantName != null && tenantName.trim().length > 0 ||uaeIdNo != null && uaeIdNo.trim().length > 0 || profession != null && profession.trim().length > 0 || mobileNo != null && mobileNo.trim().length > 0 || birthDate != null && birthDate.trim().length > 0 
				|| presentAddress != null && presentAddress.trim().length > 0 || approvedStatus != null && approvedStatus.trim().length > 0)){
			$('.commonErr').fadeOut();
			var tempExp= /%/;
			 var tempMatch=tenantName.search(tempExp);
			 if(tempMatch!= -1 ){
				 while(tempMatch!= -1){
					 tenantName=tenantName.replace('%','$');
					 tempMatch=tenantName.search(tempExp);
				 }
			 }else{
				 tenantName=tenantName+'$';
			 }
			 tempMatch!= -1;
			 if(uaeIdNo != ""){
				 tempMatch=uaeIdNo.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 uaeIdNo=uaeIdNo.replace('%','$');
						 tempMatch=uaeIdNo.search(tempExp);
					 }
				 }else{
					 uaeIdNo=uaeIdNo+'$';
				 }
			 }
			 tempMatch!= -1;
			 if(mobileNo != ""){
				 tempMatch=mobileNo.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 mobileNo=mobileNo.replace('%','$');
						 tempMatch=mobileNo.search(tempExp);
					 }
				 }else{
					 mobileNo=mobileNo+'$';
				 }
			 }
			 tempMatch!= -1;
			 if(presentAddress != ""){
				 tempMatch=presentAddress.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 presentAddress=presentAddress.replace('%','$');
						 tempMatch=presentAddress.search(tempExp);
					 }
				 }else{
					 presentAddress=presentAddress+'$';
				 }
			 }
			 tempMatch!= -1;
			 if(profession != ""){
				 tempMatch=profession.search(tempExp);
				 if(tempMatch!= -1 ){
					 while(tempMatch!= -1){
						 profession=profession.replace('%','$');
						 tempMatch=profession.search(tempExp);
					 }
				 }else{
					 profession=profession+'$';
				 }
			 }
			 $('#loading').fadeIn();
				$.ajax({
						type: "POST",  
						url: "<%=request.getContextPath()%>/search_tenant_information.action",  
				     	async: false,
						dataType: "html",
						cache: false,
						error: function(data) 
						{
							$('.commonErr').hide();
				     		alert("error = " + $(data));
						},
				     	success: function(data)
				     	{
							$("#gridDiv").html(data);  //gridDiv main-wrapper
				     	}
				});
				$('#loading').fadeOut();
		}else{
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/re_receipt_generation_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	//Default Date Picker
	$('#birthDate').datepick();

}); 	
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Receipt Generation</div>	 
			<div class="portlet-content">
			 <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
					</c:if>
				 </div>
					<div>
						<div class="">
							 <fieldset>
							 	 <form name="propertyFlat" id="propertyFlat">
							 	 	<div id="hrm" class="width50 float-left"> 
							 	 		<fieldset>
								 	 	 	 <legend>Search Parameter</legend>
										   		<div class="" style="">
													<label style="font-weight:bold;"><fmt:message key="re.owner.info.tenantname"/></label> 
													<input type="text" id="tenantName" />
												</div>
												<div class="" style="">
														<label style="font-weight:bold;"><fmt:message key="re.owner.info.civilidno"/></label> <input type="text" id="uaeIdNo" />
													</div>
												<div id="approvedstatussearch">
			   									</div>
			   									<div class="" style="">
														<label style="font-weight:bold;"><fmt:message key="re.owner.info.mobileno"/></label> 
														<input type="text" id="mobileNo" />
													</div>
											 </fieldset>
										</div>
										<div id="hrm" class="width50 float-right" style="margin-top: 8px;">
											<fieldset>
												<div class="" style="">
													<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.birthdate"/></label> 
													<input type="text" id="birthDate" readonly="readonly">
												</div>
												<div class="" style="">
													<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.presentaddress"/></label> 
													<input type="text" id="presentAddress" class=""/>
												</div>
												<div class="" style="">
													<label class="" style="font-weight:bold;"><fmt:message key="re.owner.info.proffession"/></label> 
													<input type="text" id="profession" class=""/>
												</div>
									
													<div id="temp1">
			   										</div>
											 </fieldset>
									   </div>
									   <div class="clearfix"></div>
									   <div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
										<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.owner.info.search"/></div>
									</form>
							 	</fieldset>
							 </div>
						</div>
						<div id="gridDiv">		
							<table id="list2"></table>  
							<div id="pager2"> </div> 
						</div>
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
							<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.owner.info.view"/></div>
							<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.owner.info.delete"/></div>
							<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.owner.info.edit"/></div>
							<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.owner.info.add"/></div>
						</div>
					</div>
				</div>
			</div>
	</body>