<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width: 50%;
	}
	
	.ui-combobox-button{height: 32px;}
	
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}
</style>
</head>
<script type="text/javascript">
var contractDate="";
var fromDate="";
var toDate="";
var tenantId=0;
var unitId=0;
var tenantName="";
var tenantTypeName="";
var unitName="";
$(function (){
	$('.formError').remove();   
	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	 
	 //Onload Call
	 contractListCall();
	 
	
	 $('.fromDate').change(function(){  
		 if($('.toDate').val()!=null && $('.toDate').val()!='')
			 contractListCall();
		 
	 }); 
	 
	 $('.toDate').change(function(){
		 
		 if($('.fromDate').val()!=null && $('.fromDate').val()!='')
			 contractListCall();
	 });
	 
	 $(".print-call").click(function(){ 
		 fromDate=$('.fromDate').val();
		 toDate=$('.toDate').val(); 
		 tenantId=$('#tenantId').val();
			window.open('<%=request.getContextPath()%>/contract_report_printout.action?fromDate='
					+ fromDate+'&toDate='+toDate+'&tenantName='+tenantName+'&unitId='+unitId+'&tenantTypeName='+tenantTypeName+'&unitName='+unitName,
					+'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		});
	  $(".pdf-download-call").click(function(){ 
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			 percentage=$('#percentage').val();

			window.open('<%=request.getContextPath()%>/downloadContractList.action?fromDate='
					+ fromDate+'&toDate='+toDate+'&format=PDF'+'&tempString='+tenantName
					+'&tempLong='+unitId+'&tempString1='+tenantTypeName+'&tempString1='+unitName,
					+'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		}); 
	  $(".xls-download-call").click(function(){
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			window.open('<%=request.getContextPath()%>/contract_report_xls.action?fromDate='
					+ fromDate+'&toDate='+toDate+'&tenantName='+tenantName+'&unitId='+unitId+'&tenantTypeName='+tenantTypeName+'&unitName='+unitName,
					+'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			
			return false;
		}); 
	  
	  $(".tenantName").combobox({ 
	      selected: function(event, ui) {
	    	  	tenantName = $(this).val(); 
	    	  	tenantTypeName=$('.tenantName option:selected').text(); 
	    	  	contractListCall();
	      }
	}); 
	
	$(".unitName").combobox({ 
	       selected: function(event, ui){
	    	    unitId = $(this).val(); 
	    	    unitName=$('.unitName option:selected').text(); 
	    	    alert(unitId);
	    	    contractListCall();
	    	   
	       }
    }); 
	
	$("#reset").click(function () {
		$("#getcontract_reports").trigger('click');
	});
}); 
function contractListCall(){
	 fromDate=$('.fromDate').val();
	 toDate=$('.toDate').val(); 
	 //alert("fromDate-->"+fromDate+"--toDate-->"+toDate+"--tenantName-->"+tenantName+"--unitId-->"+unitId);
	 $.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/contract_detail_report.action",
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result){  
			$("#temp-result").html(result);  
		} 
	});  

}
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
   var daysInMonth =$.datepick.daysInMonth(
   $('#selectedYear').val(), $('#selectedMonth').val());
   $('#selectedDay option:gt(27)').attr('disabled', false);
   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
   if ($('#selectedDay').val() > daysInMonth) {
       $('#selectedDay').val(daysInMonth);
   }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}  
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:93%;">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractdetailreport"/></div>	 
		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
				</c:if>    
			</div>	
			<div>
				<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
				</c:if>
			 </div> 
			<div class="width45 float-left" style="padding:5px;">  
				 <div>
				 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDatefrom" /></label>
				 	<input type="text" name="fromDate" class="width60 fromDate" id="startPicker" readonly="readonly" onblur="triggerDateFilter()">
				 </div>
				 <div>
				 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDateTo" /></label>
				 	<input type="text" name="toDate" class="width60 toDate" id="endPicker" readonly="readonly" onblur="triggerDateFilter()">
				 </div>
			 </div>
			 <div class="width45 float-right" style="padding:5px;">  
				 <div>
				 	<label class="width30"><fmt:message key="re.offer.tenant" /></label>
				 	<input type="hidden" id="tenantId"  />
				 	<select id="tenantName" class="tenantName" name="tenantName">
						<option value="">Select</option>
						<c:forEach items="${TENANT_INFO_OFFER}" var="bean">
							<option value="${bean.tenantNumber}">${bean.tenantName}</option>
						</c:forEach>
					</select>
				 </div> 
				 <div>
				 	<label class="width30"><fmt:message key="re.offer.propertyUnit" /></label>
				 	<select id="unitName" class="unitName" name="unitName">
						<option value="">Select</option>
						<c:forEach items="${UNIT_INFO_OFFER}" var="bean">
							<option value="${bean.unitId}">${bean.unitName}</option>
						</c:forEach>
					</select>
				 </div> 
			 </div>
			 <div class="float-right">
				<div class="portlet-header ui-widget-header float-right" id="reset" style="cursor: pointer;">
					<fmt:message key="sys.common.resetSelection" />
				</div>
				<!-- <div class="portlet-header ui-widget-header float-right" id="apply_filter" style="cursor: pointer;">
					Apply Filter
				</div> -->
			 </div>
			 <div id="temp-result" class="width100 float-left"></div>
			 
		</div>
		
	</div> 
</div> 
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div>