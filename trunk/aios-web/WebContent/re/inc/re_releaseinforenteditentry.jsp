<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="releaseInfoRentAddEdit" id="releaseInfoRentAddEdit">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend></legend>
		<div>
			<label>Paid Amount<span style="color:red">*</span></label>
			<input type="text" name="paidAmount" id="paidAmount" class="width50 balanceCheck validate[required,custom[onlyFloat]]">
		</div>
		<div>
			<label>Balance Amount</label>
			<input type="text" name="balanceAmount" id="balanceAmount" class="width50" disabled="disabled" >
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend>Lines Details</legend> 
			<div>
				<label>Line No<span style="color:red">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50 validate[required,custom[onlyNumber]]" />
			</div>
			<div>
				<label>Flat Number<span style="color:red">*</span></label>
				<input type="text" name="flatNumber" id="flatNumber" class="width50 validate[required]" />
			</div>
			<div>
				<label>Rent Amount<span style="color:red">*</span></label>
				<input type="text" name="rentAmount" id="rentAmount" class="width50 balanceCheck validate[required,custom[onlyFloat]]" />
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;">Cancel</div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;">Save</div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#releaseInfoRentAddEdit").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(6)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing button 		
		 });
		 $('#add').click(function(){
			  if($('#releaseInfoRentAddEdit').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);	
					var lineNo=$('#lineNo').val();				
					var flatNumber=$('#flatNumber').val();
					var rentAmount=$('#rentAmount').val();
					var paidAmount=$('#paidAmount').val();
					var balanceAmount=$('#balanceAmount').val();
									 
					var trnVal =$('#transURN').val(); 

					var releaseId=$('#releaseId').val();
					var actualLineId=$($($(slidetab).children().get(5)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(5)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_edit_release_rent_update.action", 
					 	async: false,
					 	data: {lineNumber: lineNo,flatNumber: flatNumber,rentAmount: rentAmount,paidAmount: paidAmount,releaseId:releaseId,  
							 trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(flatNumber);
		        		$($(slidetab).children().get(2)).text(rentAmount); 
		        		$($(slidetab).children().get(3)).text(paidAmount); 
		        		$($(slidetab).children().get(4)).text(balanceAmount);  

		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button
	
						$($($(slidetab).children().get(5)).children().get(0)).val($('#objLineVal').html());
				  		$($($(slidetab).children().get(5)).children().get(2)).val($('#objTempId').html());
				  		$("#transURN").val($('#objTrnVal').html());
				   } 
			  }else{
				  return false;
			  }
		});

		 $('.balanceCheck').blur(function(){
			rentAmount=$('#rentAmount').val();
			paidAmount=$('#paidAmount').val();
			if(rentAmount != '' && paidAmount !='' ){
				balanceAmount=Number(rentAmount) - Number(paidAmount);
				$('#balanceAmount').val(balanceAmount);
			}
		});
		
});
 </script>	