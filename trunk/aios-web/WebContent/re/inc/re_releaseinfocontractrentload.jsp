<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <c:forEach items="${requestScope.itemList}" var="result" >
 	<tr class="even"> 
		<td class="width10 lineno">${result.lineNumber}</td>
		<td class="width20 flatno">${result.flatNumber}</td>	
		<td class="width20 rentAmountCalc">${result.rentAmount}</td> 
		<td class="width20 message" style="display:none;">${result.message}</td>
		<td class="width20 balanceAmountCalc" style="">${result.balanceAmount }</td>
		<td style="display:none"> 
			<input type="text" value="${result.contractRentId}" name="actualLineId" id="actualLineId"/>
		</td> 
		<td style=" width:10%;display:none;"> 
					<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
		   	</a>	
		   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
				<span class="processing"></span>
			</a>
		</td>  
		<td style="display:none;" class="session_status"></td>
		<td style="display:none;"></td>	
	</tr>
</c:forEach>
<input type="hidden" id="count" value="${count}"/>