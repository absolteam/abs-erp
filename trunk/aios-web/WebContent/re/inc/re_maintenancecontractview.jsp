<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var actualID;
$(function (){  
	$('#maintenanceTypeCode').val($('#maintenanceTypeCodeTemp').val().trim());
	$('#supplierId').val($('#supplierIdTemp').val());
	$('#purchaseOrderId').val($('#purchaseOrderIdTemp').val());

	var amcAmountTemp=Number($('#amcAmount').val());
	$('.installmentAmountCalc').each(function(){ 		// Calculation for Balance Amount in Payment TAB
		if($(this).html() != null || $(this).html() != ''){
			balance=amcAmountTemp-Number($(this).html());
			$($(this).siblings().get(2)).html(balance);
			amcAmountTemp=balance;
		}
	});
	

	$('.formError').remove(); 

	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_contract_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
}); 
	 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.maintenancecontract"/></div>
		<div class="portlet-content">
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 	  
	 		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
	 		<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 
			<form id="MaintainenceContractEdit">
				<fieldset>	
					<div class="width50 float-right" id="hrm"> 
		 				<fieldset>
							<legend><fmt:message key="re.contract.info"/></legend>	
								 <div>
								 <label><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>
								 	<input type="text" disabled="disabled" name="contractDate" class="width30" id="contractDate" readonly="readonly" value="${bean.contractDate}"/>
								 </div>				
	                            <div>
	                            <input type="hidden" id="maintenanceId" value="${bean.maintenanceId}"/>
									<label><fmt:message key="re.contract.contractno"/><span style="color:red">*</span></label>
									<input type="hidden" name="contractId" id="contractId" class="width50" value="${bean.contractId}"/>
									<input type="text" disabled="disabled" name="contractNumber" id="contractNumber" class="width30 validate[required,custom[onlyNumber]]" value="${bean.contractId}"/>
									<span id="hit7" class="button" style="width: 40px ! important;display:none;">
										<a class="btn ui-state-default ui-corner-all contractno-popup"> 
											<span class="ui-icon ui-icon-newwin"> </span> 
										</a>
									</span>
								</div>	
	                         	<div>
	                         		<label><fmt:message key="re.contract.maintenancetype"/></label>
	                         		<input type="hidden" name="maintenanceTypeCodeTemp" id="maintenanceTypeCodeTemp" value="${bean.maintenanceTypeCode}"/>
									<select id="maintenanceTypeCode" disabled="disabled" class="width30 validate[required]">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.maintenanceTypeList}" var="typelist" varStatus="status">
											<option value="${typelist.maintenanceTypeCode}">${typelist.maintenanceTypeName}</option>
										</c:forEach>
									</select>
								</div>	
								<div>
	                            	<label>Maintenance SubType</label>
	                            	<input type="text" disabled="disabled" name="maintenanceSubType" class="width30" value="${bean.maintenanceSubType }" id="maintenanceSubType">
	                            </div>										
								<div>
									<label><fmt:message key="re.contract.suppliervendorname"/></label>
									<input type="hidden" name="supplierIdTemp" id="supplierIdTemp" value="${bean.supplierId }"/>
									<input type="hidden" name="supplierId" class="width30" id="supplierId" value="${bean.supplierId }">
									<input type="text" disabled="disabled" name="supplierName" class="width30" id="supplierName" value="${bean.supplierName }" disabled="disabled">
								</div>	
								 <div>
	                            	<label><fmt:message key="re.contract.addressline1"/></label>
	                            	<input type="text" disabled="disabled" name="addressLine1" class="width30" disabled="disabled" id="addressLine1" value="${bean.addressLine1}"/>
	                            </div>		
	                            <div>
	                            	<label><fmt:message key="re.contract.addressline2"/></label>
	                            	<input type="text"  disabled="disabled" name="addressLine2" class="width30" disabled="disabled" id="addressLine2" value="${bean.addressLine2 }"/>
	                            </div>										
								<div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								<div id="UploadDmsDiv" class="width100">
									<input type="hidden" id="URNdms" value="${bean.dmsURN}" disabled="disabled" class="width30">
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			                            <div  id="dms_document_information" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
			                        </div>
								</div>
				    		</div>	
						</fieldset>  																		
					</div> 
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="re.contract.maintenanceinfo"/></legend>
							<div><label><fmt:message key="re.contract.purchaseorderno"/><span style="color:red">*</span></label>
								<input type="hidden" name="purchaseOrderIdTemp" id="purchaseOrderIdTemp" value="${bean.purchaseOrderId }"/>
								<input type="hidden" name="purchaseOrderId" id="purchaseOrderId" class="width30 validate[required]">
								<input type="text" disabled="disabled" name="purchaseOrderNo" id="purchaseOrderNo" disabled="disabled" value="${bean.purchaseOrderNo }" class="width30 validate[required]">
								<span id="hit7" class="button" style="width: 40px ! important;">
									<a class="btn ui-state-default ui-corner-all purchaseorder-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
							</div>	
							<div>
								<label><fmt:message key="re.contract.contractamount"/><span style="color:red">*</span></label>
								<input type="text" disabled="disabled" name="amcAmount" id="amcAmount" class="width30 validate[required,custom[onlyFloat]]" value="${bean.amcAmount}"/>
							</div>
							<div>
								<label><fmt:message key="re.contract.securitydeposit"/></label>
								<input type="text" disabled="disabled" name="securityDeposit" id="securityDeposit" class="width30 validate[optional,custom[onlyFloat]]" value="${bean.securityDeposit}"/>
							</div>						
						</fieldset> 
						<fieldset>
							<legend><fmt:message key="re.contract.contractperiod"/></legend>
							<div>
								<label class="width30"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
								<input type="text" disabled="disabled" name="fromDate" id="startPicker" class="width30 validate[required]" readonly="readonly" value="${bean.fromDate }"/>
							</div>		
						    <div>
						    	<label class="width30"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
						    	<input type="text" disabled="disabled" name="toDate" id="endPicker" class="width30 validate[required]" readonly="readonly" value="${bean.toDate }"/>
						    </div>
						</fieldset>
					</div>
				</fieldset>	
			</form>
			<div class="clearfix"></div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.assetinformation"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th style=" width:5%;"><fmt:message key="re.contract.lineno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.buildingno"/></th>
				            					<th style=" width:5%;"><fmt:message key="re.property.info.componenttype"/></th>
				            					<th style=" width:5%;"><fmt:message key="re.property.info.flatno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.additionaldetails"/></th>
				            					<th class="width10"><fmt:message key="re.contract.assetno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.assetname"/></th>
				            					<th class="width10"><fmt:message key="re.contract.brand"/></th>
				            					<th class="width10"><fmt:message key="re.contract.modelno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.age"/></th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabAsset" style="">	
											<c:forEach items="${requestScope.result1}" var="result" >
												<tr class="rowid">  
													<td style=" width:5%;">${result.lineNumber}</td>	 
													<td class="width10">${result.buildingName}</td>
													<td style=" width:5%;">${result.componentTypeName}</td>
													<td style=" width:5%;">${result.flatNumber}</td>
													<td class="width10">${result.additionalDetails}</td>
													<td class="width10">${result.assetNo}</td> 
													<td class="width10">${result.assetName}</td>
													<td class="width10">${result.brand}</td>
													<td class="width10">${result.modelNo}</td>
													<td class="width10"><c:if test="${result.age ne 0}">${result.age}</c:if><c:if test="${result.age eq 0}"></c:if></td>
													<td style="display:none"> 
														<input type="hidden" value="${result.maintainenceAssetId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAsset" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAsset"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAsset" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="building_id">${result.buildingId}</td>	
													<td style="display:none;" class="asset_id">${result.assetId}</td>
													<td style="display:none;" class="componant_id">${result.buildingComponantId}</td>
													<td style="display:none;" class="flat_id">${result.flatId}</td>		 
												</tr>  
											</c:forEach>
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<div style="display:none" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addassetrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.paymentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCountPayment" id="childCountPayment"  value="${countSize2}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr >  
				            					<th class="width10"><fmt:message key="re.contract.lineno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.date"/></th>
				            					<th class="width10"><fmt:message key="re.contract.installmentamount"/></th>
				            					<th class="width10"><fmt:message key="re.contract.balanceamount"/></th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabPayment" style="">
										<c:forEach items="${requestScope.result2}" var="result2" >
												<tr class="asset">  
													<td class="width10">${result2.lineNumber}</td>	 
													<td class="width20">${result2.date }</td>
													<td class="width20 installmentAmountCalc">${result2.instalmentAmount }</td>	
													<td class="width10 balanceAmountCalc"></td> 
													<td style="display:none"> 
														<input type="hidden" value="${result2.maintainencePayementId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>	
													<td style="display:none;"></td>	 
												</tr>  
											</c:forEach>	
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<fieldset>
						<div class="float-right width45" style="display:none;">
							<label class="width30"><fmt:message key="re.contract.total"/></label><input type="text" name="sno" class="width30 tooltip" id="total">
						</div>
					</fieldset>
							
					<div style="display:none" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addpayementrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Visit Details</div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCountVisit" id="childCountVisit"  value="${countSize3}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th class="width10">Visit Number</th>
				            					<th class="width10">Person Name</th>
				            					<th class="width10">Purpose</th>
				            					<th class="width10">Remarks</th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabVisit" style="">
										<c:forEach items="${requestScope.result3}" var="result3" >
												<tr class="visit">  
													<td class="width10">${result3.visitNo}</td>	 
													<td class="width20">${result3.personName }</td>
													<td class="width20">${result3.purpose }</td>	
													<td class="width10">${result3.remarks }</td> 
													<td style="display:none"> 
														<input type="hidden" value="${result3.visitId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addVisit" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editVisit"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delVisit" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>	
													<td style="display:none;"></td>	 
												</tr>  
											</c:forEach>	
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<fieldset>
						<div class="float-right width45" style="display:none;">
							<label class="width30"><fmt:message key="re.contract.total"/></label><input type="text" name="sno" class="width30 tooltip" id="total">
						</div>
					</fieldset>
							
					<div style="display:none" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addvisitrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>		
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
				<div class="portlet-header ui-widget-header float-right headerData" id="close" style="cursor:pointer;"><fmt:message key="re.property.info.close"/></div>
			</div>
		</div>
	</div>
</div>

