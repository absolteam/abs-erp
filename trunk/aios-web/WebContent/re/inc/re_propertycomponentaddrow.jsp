<tr class="rowid" id="fieldrow_${rowId}">
	<td style="width:5%; display: none;" class="width10" id="lineId_${rowId}">${rowId}</td>
	<td id="componentfeatureCode_${rowId}"></td>
	<td id="componentfeature_${rowId}"></td>
	<td id="componentfeatureDetails_${rowId}"></td> 
	<td style="display:none">
		<input type="hidden" name="componentlineid" id="componentlineid_${rowId}"/> 
		<input type="hidden" name="componentfeatureid" id="componentfeatureid_${rowId}"/> 
		<input type="hidden" name="lookupDetailId" id="lookupDetailId_${rowId}"/>
	</td> 
	 <td style="width:1%;" class="opn_td" id="option_${i}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataRow" id="AddImage_${rowId}" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataRow" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowRow" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>
<script type="text/javascript">
var slidetab="";
$(function(){
	 $('.addDataRow').click(function(){
			if($("#-form").validationEngine({returnIsValid:true})){   
				 $('.error').hide(); 
				 slidetab=$(this).parent().parent().get(0);   
				 var tempvar=$(slidetab).attr("id");  
				 var idarray = tempvar.split('_');
				 var rowid=Number(idarray[1]);  
				 var showPage="";
				 if(componentId !=null && componentId!="" && componentId>0)
					 showPage="editadd";
				 else
					 showPage="addadd";
				 $("#AddImage_"+rowid).hide();
				 $("#EditImage_"+rowid).hide();
				 $("#DeleteImage_"+rowid).hide();
				 $("#WorkingImage_"+rowid).show(); 
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_compoment_addlineentry.action",
					data:{id:rowid, showPage:showPage},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 	$(result).insertAfter(slidetab); 
					 	$('#lineNumber').val($('#lineId_'+rowid).text());
					}
				 });
			}
			else{
				return false;
			}		
		 });

		 $('.editDataRow').click(function(){
				if($("#-form").validationEngine({returnIsValid:true})){   
					 $('.error').hide(); 
					 slidetab=$(this).parent().parent().get(0);   
					 var tempvar=$(slidetab).attr("id");  
					 var idarray = tempvar.split('_');
					 var rowid=Number(idarray[1]);  
					 var componentId=$('#componentId').val();
					 var showPage="";
					 if(componentId !=null && componentId!="" && componentId>0)
						 showPage="editedit";
					 else
						 showPage="addedit";
					 $("#AddImage_"+rowid).hide();
					 $("#EditImage_"+rowid).hide();
					 $("#DeleteImage_"+rowid).hide();
					 $("#WorkingImage_"+rowid).show(); 
					 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/property_compoment_addlineentry.action",
						data:{id:rowid,showPage:showPage},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 	$(result).insertAfter(slidetab); 
						 	$('#lineNumber').val($('#lineId_'+rowid).text());  
			        		$('#componentFeature').val($('#componentfeature_'+rowid).text());  
			        		$('#componentFeatureId').val($('#componentfeatureid_'+rowid).val());  
			        		$('#lookupDetailId').val($('#lookupDetailId_'+rowid).val());
						}
					 });
					}
					else{
						return false;
					}		
		  });

		 $(".delrowRow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var lineId=$('#lineId_'+rowid).text();
			 var componentfeature=$('#componentfeature_'+rowid).text().trim();
			 if(componentfeature!=null && componentfeature!=""){
				 var flag=false; 
				 $.ajax({
						 type : "POST",
						 url:"<%=request.getContextPath()%>/property_compoment_adddeleteline.action", 
						 data:{id:lineId},
						 async: false,
					  dataType: "html",
						 cache: false,
					   success:function(result){ 
							 $('.formError').hide();  
							 $('.tempresult').html(result);    
							 if(result!=null){
							    	if($('#returnMsg').html() != '' || $('#returnMsg').html() == "Success")
		                              flag = true;
							    	else{	
								    	flag = false; 
								    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
							    	} 
							     }  
						},
						error:function(result){
							$("#page-error").hide().html($('#returnMsg').html().slideDown()); 
							$('.tempresult').html(result);   
							return false;
						}
							
					 }); 
					 if(flag==true){ 
			        		 var childCount=Number($('#childCount').val());
			        		 if(childCount > 0){
								childCount=childCount-1;
								$('#childCount').val(childCount); 
			        		 } 
			        		 $(this).parent().parent('tr').remove(); 
			        		//To reset sequence 
			        		var i=0;
			     			$('.rowid').each(function(){  
								i=i+1;
								$($(this).children().get(0)).text(i); 
		   					 });  
					}
			 }
			 else{
				 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
				 return false;
			 }  
		 });
});
</script>