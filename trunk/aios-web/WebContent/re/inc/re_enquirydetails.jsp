<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript">
var buildingId="";
var flatId="";
var componentType="";
var featureId="";
var timeCode="";
var rent="";
var totalArea="";
var noOfRooms="";
var toNoOfRooms;
var toRent;
var toTotalArea;
$(function (){  

	var curDate=new Date().toString();
	var splitDate=curDate.split(" ");
	var day=splitDate[2];
	var mon=splitDate[1];
	var year=splitDate[3];
	fromDate=day+"-"+mon+"-"+year;
	$('#enquiryDate').val(fromDate);
	
	$('.formError').remove();
	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	$('#enquiryDate').datepick({onSelect: customRange, showTrigger: '#calImg'});

	$("#search").click(function(){
		 buildingId=Number($('#buildingId option:selected').val());
		 flatId=Number($('#flatId option:selected').val());
		 componentType=Number($('#componentType option:selected').val());
 		 featureCode=String($('#featureCode').val());
 		 if(featureCode == 'null'){featureCode="";}
		 timeCode=$('#timeCode').val();	
		 rent=Number($('#rent').val());
		 totalArea=$('#totalArea').val();
		 noOfRooms=Number($('#noOfRooms').val());
		 toNoOfRooms=Number($('#toNoOfRooms').val());
		 toRent=Number($('#toRent').val());
		 toTotalArea=Number($('#toTotalArea').val());
		 
	    if( (buildingId != 0  || flatId!=0 || componentType != 0 || featureCode != null && featureCode.length > 0 || 
	    	    rent!=0 || noOfRooms!=0 || toNoOfRooms!=0 || toRent!=0 || 
	    	    timeCode != null && timeCode.length > 0 || totalArea != null && totalArea.length > 0  )) {
			var tempExp= /%/;
			var tempMatch=totalArea.search(tempExp);
			if(tempMatch!= -1 ){
				while(tempMatch!= -1){
					totalArea=totalArea.replace('%','$');
					tempMatch=totalArea.search(tempExp);
				}
			}else{
				 totalArea=totalArea+'$';  
			}
			$('.commonErr').fadeOut();
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_enquiry_details.action",  
			    	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
			    		alert("error = " + $(data));
				},
			    	success: function(data)
			    	{
			     	$("#gridDiv").html(data);  //gridDiv main-wrapper
			    	}
			});
		}else{
			$('.error').fadeOut();
			$('.success').fadeOut();
			$('.commonErr').hide().html("Please give a valid input to search !!!").slideDown();
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/enquiry_details_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 


	$('#buildingId').change(function(){
		var buildingId=$('#buildingId').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/enquiry_flat_retrive_action.action",
			data:{buildingId:buildingId},
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#flatId").html(result);
			}
		});
	});

	$('#print').click(function(){
		$('.commonErr').hide();
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null ){
			$('.error').fadeOut();
			$('.success').fadeOut();
			$('.commonErr').hide().html("Please search and select one record to print !!!").slideDown();
			return false; 
		}else{
			var s2 = $("#list2").jqGrid('getRowData',s);  
			id = s2.buildingId; 
			flatId=s2.flatId; 
			urllocation='enquiry_details_report.action?format1=PDF&buildingId='+id+'&flatId ='+flatId;
			window.open(urllocation);
			return true;
		}
		
	}); 

	$('#flatId').change(function(){
		var buildingId1=$('#buildingId').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/enquiry_floor_retrive_action.action",
			data:{buildingId:buildingId1},
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#floorNo").html(result);
			}
		});
	});
});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
  var daysInMonth =$.datepick.daysInMonth(
  $('#selectedYear').val(), $('#selectedMonth').val());
  $('#selectedDay option:gt(27)').attr('disabled', false);
  $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
  if ($('#selectedDay').val() > daysInMonth) {
      $('#selectedDay').val(daysInMonth);
  }
} 
function customRange(dates) {
if (this.id == 'birthdate') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#endPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.enquiry"/></div>
			<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
				</div>
				<form name="enquiryForm" id="enquiryForm">
					<fieldset>
						<div class="width50 float-right" id="hrm" style="display:none;"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.enquirydate"/></legend>						
		                            <div><label>Date</label><input type="text" name="birthdate" class="width30" id="enquiryDate" readonly="readonly"></div>		
									
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm" style="display:none;">
							<fieldset>
								<legend><fmt:message key="re.property.info.enquiryinformation"/></legend>
									<div><label><fmt:message key="re.property.info.customername"/></label><input type="text" name="customerName" class="width30" id="customerName"></div>
									<div><label><fmt:message key="re.owner.info.emailid"/></label><input type="text" name="email" class="width30" id="email"></div>
									<div><label><fmt:message key="re.owner.info.mobileno"/></label><input type="text" name="mobileNo" class="width30" id="mobileNo"></div>
							</fieldset> 
						</div>
						<div class="width100 float-right" id="hrm">
							<fieldset>
				            	<legend><fmt:message key="re.property.info.searchparameter"/></legend>
				                <div class="float-right width50">
									<div>
										<label class="width10"><fmt:message key="re.property.info.rent"/></label><input type="text" name="rent" class="width30 float-left" id="rent">
										<label class="width10"><fmt:message key="re.property.info.to"/></label><input type="text" name="toRent" class="width30" id="toRent">
									</div>
									<div  style="display:none;">
										<label class="width10"><fmt:message key="re.property.info.area"/></label><input type="text" name="area" class="width30 float-left" id="totalArea" > 
										<label class="width10"><fmt:message key="re.property.info.to"/></label><input type="text" name="toTotalArea" class="width30 tooltip" id="toTotalArea">
									</div>
									<div>
										<label class="width10"><fmt:message key="re.property.info.noofrooms"/></label><input type="text" name="noOfRooms" class="width30 tooltip float-left" id="noOfRooms"> 
										<label class="width10"><fmt:message key="re.property.info.to"/></label><input type="text" name="toNoOfRooms" class="width30 tooltip" id="toNoOfRooms">
									</div>
								</div>
								<div class="float-right width50">
				                	<div style="display:none;">
				                		<label class="width30"><fmt:message key="re.property.info.city"/></label>
										<select class="width60" id="city">
											<option value="">-Select-</option>
											<option value="slm">Salem</option>
											<option value="cmb">Coimbatore</option>
											<option value="erd">Erode</option>
										</select>
									</div>	
									<div>
										<label class="width30"><fmt:message key="re.property.info.buildingname"/></label>
										<select id="buildingId" class="width30 validate[required]" >
											<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
													<option value="${building.buildingId }">${building.buildingName }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
									<div>
										<label class="width30"><fmt:message key="re.property.info.flatno"/></label>	 
										<select id="flatId" class="width30 validate[required]">
											<option value="">-Select-</option>
										</select>
									</div>
									<div style="display:none;">
										<label class="width30"><fmt:message key="re.property.info.floorno"/></label>	 
										<select id="floorNo" class="width30">
											<option value="">-Select-</option>
										</select>
									</div>
									<div>
										<label class="width30"><fmt:message key="re.property.info.componenttype"/></label>
										<select id="componentType" class="width30">
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookupList ne null}">
											<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
												<option value="${compType.componentType }">${compType.componentTypeName }</option>
											</c:forEach>
											</c:if>	
										</select>
									</div>
									<div>
										<label class="width30"><fmt:message key="re.property.info.features"/></label>
										<select class="width60" multiple="multiple" size="4" id="featureCode">
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookup ne null}">
											<c:forEach items="${requestScope.lookup}" var="feautType" varStatus="status">
												<option value="${feautType.featureCode }">${feautType.features }</option>
											</c:forEach>
											</c:if>	
										</select>
									</div>
									<div style="display:none;"><label class="width30"><fmt:message key="re.property.info.timevacant"/></label>
										<select id="timeVacant" class="width30">
											<option value="">-Select-</option>
											<c:if test="${requestScope.bean ne null}">
											<c:forEach items="${requestScope.bean}" var="timeType" varStatus="status">
												<option value="${timeType.timeCode }">${timeType.timeVacant }</option>
											</c:forEach>
											</c:if>	
										</select>
									</div>
				   				</div>
				
								<div class="float-right buttons ui-widget-content ui-corner-all"> 			
									<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
									<div class="portlet-header ui-widget-header float-right mou-cursor" id="search"><fmt:message key="re.property.info.search"/></div>
								</div>
							</fieldset>
						</div>
					</fieldset>
				</form>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>     						
				<div class="float-right buttons ui-widget-content ui-corner-all"> 			
					<div class="portlet-header ui-widget-header float-right" id="print"><fmt:message key="re.property.info.print"/></div>
					<div class="portlet-header ui-widget-header float-right" id="save" style="display:none;"><fmt:message key="re.property.info.save"/></div> 
				</div>
		</div>
	</div>
</div>
</body>