<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoOwnerEditAddEntry" id="propertyInfoOwnerEditAddEntry" style="position: relative;">	
<div  class="response-msg error availErr ui-corner-all" style="width:80%; display:none;"></div> 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.percentageinfo"/></legend>
		<div>
			<label><fmt:message key="re.property.info.percentage"/><span class="mandatory">*</span></label>
			<input type="text" name="percentage" id="percentage" class="width50  validate[required,custom[number]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.ownerinformation"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.ownername"/><span class="mandatory">*</span></label>
				<input type="text" name="ownerName" id="ownerName" disabled="disabled" class="width50  validate[required]" />
				<input type="hidden" name="ownerId" id=ownerId class="width50" />
				<span id="hit7" class="button" style="width: 40px ! important;">
					<a class="btn ui-state-default ui-corner-all ownerInfo-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
			<div>
				<label><fmt:message key="re.property.info.ownertype"/></label>
				<input type="text" name="ownerType" id="ownerType" disabled="disabled" class="width50  validate[required]" />
				<input type="hidden" name="ownerTypeId" id=ownerTypeId class="width50" />
			</div>
			<div>
				<label><fmt:message key="re.owner.info.civilidno"/></label>
				<input name="uaeNo" id="uaeNo" disabled="disabled" class="width50">
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		
		$jquery("#propertyInfoOwnerEditAddEntry").validationEngine('attach');

		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button	 	
			openFlag=0;	
		 });

	/*	$('#percentage').blur(function(){
			$.fn.percentCheck();
				
		});*/

		/*	$.fn.percentCheck = function() { 	// Checking Percentage
			var percentage=$('#percentage').val();
			$('#percentageTotal').val(Number($('#percentage').val())+Number($('#percentageTotal').val()));
		 	percentageTotal=$('#percentageTotal').val();
			if(percentage>100 || percentageTotal>100){
		 		$('.availErr').slideDown();
		 		$('.availErr').html("Sum of Percentage Should not greater than 100");
				percentage=$('#percentage').val();
				$('#percentageTotal').val(Number($('#percentageTotal').val())-Number($('#percentage').val()));
		 		$('#percentage').val("");
		 		return false;
			}else{
				$('.availErr').hide();
				return true;
			}	
		}*/
		
		 $('#add').click(function(){
			 if($jquery('#propertyInfoOwnerEditAddEntry').validationEngine('validate')){
				 percentage=Number($('#percentage').val());
				  	total=0;
					$('.percentage').each(function(){ 
						total+=Number($(this).html());
					});
					  	
					percentageTotal=total;
					total=percentageTotal+percentage;
					$('#percentageTotal').val(total);
			if(total <= 100){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var ownerId=$('#ownerId').val();
					var ownerName=$('#ownerName').val();
					var ownerType=$('#ownerType').val();
					var uaeNo=$('#uaeNo').val();
					var percentage=$('#percentage').val();
									 
					var trnVal =$('#transURN').val(); 

					var buildingId=$('#buildingId').val();  
					var actionFlag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_property_owner_save.action", 
					 	async: false,
					 	data: {buildingId: buildingId, ownerId: ownerId,ownerName: ownerName,ownerType: ownerType,uaeNo: uaeNo, 
							percentage: percentage,trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
							
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(ownerName);
		        		$($(slidetab).children().get(1)).text(ownerType);
		        		$($(slidetab).children().get(2)).text(uaeNo); 
		        		$($(slidetab).children().get(3)).text(percentage); 

		        		$($(slidetab).children().get(6)).text(ownerId);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
				  		$.fn.globeTotal();  //For Total Percentage
				  		
				  		var childCount=Number($('#childCount').val());
						childCount=childCount+1;
						$('#childCount').val(childCount);
				   } 
				 }else{
					  $('.availErr').slideDown();
						$('.availErr').html("Sum of Percentage Should not greater than 100");
					     percentage=$('#percentage').val();
						$('#percentageTotal').val(Number($('#percentageTotal').val())-Number($('#percentage').val()));
				 		$('#percentage').val("");
				 		return false;
				  }
			  }else{
				  return false;
			  }
		});

		 $('.ownerInfo-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#property-popup').dialog('open');
				//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_info_owner_details.action",
				 	async: false, 
				 	data:{},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			});
		 
		$('#property-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			width: '35%',
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$('#ownerName').val("");
					$('#ownerType').val("");
					 $('#uaeNo').val("");
					 
					$(this).dialog("close"); 
				} 
			}
		});
		
});
 </script>	