<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  
	$('.formError').remove();

	$("#tenantInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	    success :  false,
	    failure : function() { callFailFunction()}
		});
	
	$('#close').click(function(){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
	     	async: false, 
			dataType: "html",
			cache: false,
			error: function(data) 
			{  
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
 	});  
});
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.tenanttypeinformation"/></div>
					 <div class="portlet-content">
						<div style="display:none;" class="tempresultfinal">
							<c:if test="${requestScope.bean != null}">
								<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
								 <c:choose>
									 <c:when test="${bean.sqlReturnStatus == 1}">
										<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
									</c:when>
									<c:when test="${bean.sqlReturnStatus != 1}">
										<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
									</c:when>
								</c:choose>
							</c:if>    
						</div>
						<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
		    		<input type="hidden" id="notificationType" name="notificationType" value="${requestScope.notificationType}"/>
					<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
					<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
					<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
					<input class="width40" type="hidden" name="sessionpersonId" value="${bean.sessionPersonId }" id="sessionPersonId">
					  <form name="tenantInformationAdd" id="tenantInformationAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.tenanttypeinformation2"/></legend>
								<div>
									<c:choose>
										<c:when test="${bean.payChequeFees eq 1 }">
											<input type="checkbox" checked="checked" name="payChequeFees" disabled="disabled" id="payChequeFees"/>
											<input type="text" name="chequeAmount" id="chequeAmount" value="${bean.chequeAmount }" disabled="disabled" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payChequeFees" id="payChequeFees" >
											<input type="text" name="chequeAmount" id="chequeAmount"  class="width30" disabled="disabled" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paychequefees"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.contractFees eq 1 }">
											<input type="checkbox" checked="checked" name="contractFees" disabled="disabled" id="contractFees"/>
											<input type="text" name="contractAmount" id="contractAmount" disabled="disabled" value="${bean.contractAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="contractFees" id="contractFees" >
											<input type="text" name="contractAmount" id="contractAmount"  disabled="disabled" class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.contractfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.payDeposit eq 1 }">
											<input type="checkbox" checked="checked" name="payDeposit" disabled="disabled" id="payDeposit"/>
											<input type="text" name="depositAmount" id="depositAmount" disabled="disabled" value="${bean.depositAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payDeposit" id="payDeposit" >
											<input type="text" name="depositAmount" id="depositAmount" disabled="disabled" class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paydeposit"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.rentFees eq 1 }">
											<input type="checkbox" checked="checked" name="rentFees" disabled="disabled" id="rentFees"/>
											<input type="text" name="rentAmount" id="rentAmount" disabled="disabled" value="${bean.rentAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="rentFees" id="rentFees" disabled="disabled">
											<input type="text" name="rentAmount" id="rentAmount" disabled="disabled" class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.rentfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.maintenanceFees eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="maintenanceFees" id="maintenanceFees"/>
											<input type="text" name="maintenanceAmount" disabled="disabled" id="maintenanceAmount" value="${bean.maintenanceAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="maintenanceFees" id="maintenanceFees" disabled="disabled">
											<input type="text" name="maintenanceAmount" disabled="disabled" id="maintenanceAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.maintenancefees"/></label>
								</div>		
								<div>
									<c:choose>
										<c:when test="${bean.payPenalities eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="payPenalities" id="payPenalities"/>
											<input type="text" name="penalitiesAmount" disabled="disabled" id="penalitiesAmount" value="${bean.penalitiesAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payPenalities" disabled="disabled" id="payPenalities" >
											<input type="text" name="penalitiesAmount" disabled="disabled" id="penalitiesAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paypenalities"/></label>
								</div>
								<div>
									<c:choose>
										<c:when test="${bean.otherFees eq 1 }">
											<input type="checkbox" checked="checked" name="otherFees" disabled="disabled" id="otherFees"/>
											<input type="text" name="otherAmount" id="otherAmount" disabled="disabled" value="${bean.otherAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="otherFees" id="otherFees" disabled="disabled">
											<input type="text" name="otherAmount" id="otherAmount" disabled="disabled" class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.otherfees"/></label>
								</div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.property.info.tenanttypeinformation1"/></legend>	
			   						<div style="display:none;"><label>Tenant Type Id</label>
			   							<input type="text" name="tenantTypeId" class="width30" id="tenantTypeId" readonly="readonly" value="${bean.tenantTypeId}"/>
			   						</div>             		
			   						<div><label><fmt:message key="re.property.info.tenanttypedescription"/></label>
			   							<input type="text" name="descriptions" class="width50" disabled="disabled" id="descriptions" value="${bean.descriptions}"/>
			   						</div> 
			   						<div>
										<c:choose>
											<c:when test="${bean.payAfterContract eq 1 }">
												<input type="checkbox" checked="checked" disabled="disabled" name="payAfterContract" id="payAfterContract"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="payAfterContract" disabled="disabled" id="payAfterContract" >
												<input type="text" name="chequeAmount" id="chequeAmount" disabled="disabled" class="width30" style="display:none;">
											</c:otherwise>
										</c:choose>
										<label><fmt:message key="re.property.info.payaftercontract"/></label>
									</div>	            		
									<div>
										<c:choose>
											<c:when test="${bean.needReleaseLetter eq 1 }">
												<input type="checkbox" checked="checked" name="needReleaseLetter" disabled="disabled" id="needReleaseLetter"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="needReleaseLetter" disabled="disabled" id="needReleaseLetter" >
											</c:otherwise>
										</c:choose>
										<label><fmt:message key="re.property.info.needreleaseletter"/></label>
									</div>
									<div>
										<label><fmt:message key="re.property.info.offervaliditydays"/></label>
										<input type="text" name="offerValidityDays" class="width30" disabled="disabled" id="offerValidityDays" value="${bean.offerValidityDays}"/>
									</div>	
									<div><label><fmt:message key="re.property.info.noofdaysallowlate"/></label>
		                            	<input type="text" name="noOfDaysAllowLate" class="width30" disabled="disabled" id="noOfDaysAllowLate" value="${bean.noOfDaysAllowLate}"/>
		                            </div>	
		                    </fieldset> 
		               </div>
		               <div class="clearfix"></div>
								 	<div class="width50 float-left" style="margin:5px;" id="hrm">
									<fieldset style="margin-top: -12px;">
										<legend>Supervisor Comments</legend>
										<textarea id="comments" class="width100" disabled="disabled">${bean.comments}</textarea>
									</fieldset>
								</div>
						<div class="clearfix"></div>
						<div class="float-right buttons ui-widget-content ui-corner-all">
		 					<div  id="close" class="portlet-header ui-widget-header float-right"><fmt:message key="hr.common.button.close"/></div> 
						</div>
				   </form>
		       </div>
		</div>
  </div>