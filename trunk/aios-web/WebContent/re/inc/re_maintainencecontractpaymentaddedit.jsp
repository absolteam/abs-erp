<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementFeeAddEdit" id="contractAgreementFeeAddEdit">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.contract.paymentdetails"/></legend>
		<div>
			<label><fmt:message key="re.contract.installmentamount"/><span class="mandatory">*</span></label>
			<input type="text" name="instalmentAmount" id="instalmentAmount" class="width50  validate[required,custom[onlyNumber]]">
			<input type="hidden" name="instalmentAmountTemp" id="instalmentAmountTemp" class="width50">
		</div>
		<div>
			<label><fmt:message key="re.contract.balanceamount"/><span class="mandatory">*</span></label>
			<input type="text" name="balanceAmount" id="balanceAmount" class="width50  validate[required,custom[onlyNumber]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.contract.linedetails"/></legend> 
			<div>
				<label><fmt:message key="re.contract.lineno"/><span class="mandatory">*</span></label>
				<input type="text" name="lineNo" id="lineNo" disabled="disabled" class="width50 validate[required,custom[onlyNumber]]" />
			</div>
			<div>
				<label><fmt:message key="re.contract.date"/><span class="mandatory">*</span></label>
				<input type="text" name="date" readonly="readonly" id="defaultActualPicker" class="width50 validate[required]" />
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';
     var balanceFlag=false;	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#contractAgreementFeeAddEdit").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		 $('#defaultActualPicker').datepick();	
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing button 
	        openFlag=0;		
		 });
		 $('#add').click(function(){
			  if($('#contractAgreementFeeAddEdit').validationEngine({returnIsValid:true})){
				  if(balanceFlag){
					  	$('#loading').fadeIn();
						var tempObject=$(this);					
						var lineNo=$('#lineNo').val();
						var instalmentAmount=$('#instalmentAmount').val();
						var balanceAmount=$('#balanceAmount').val();
						var defaultActualPicker=$('#defaultActualPicker').val();
										 
						var trnVal =$('#transURN').val(); 
						
						var flag=false;
			        	$.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/add_edit_contract_payement_update.action", 
						 	async: false,
						 	data: {lineNo: lineNo,instalmentAmount: instalmentAmount,balanceAmount: balanceAmount,date: defaultActualPicker,  
								trnValue:trnVal, actualLineId: actualID},
						    dataType: "html",
						    cache: false,
							success:function(result){ 
						 		$('.formError').hide();
		                           $('#othererror').hide(); 
								$('.tempresult').html(result); 
							     if(result!=null){
							    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                                    flag = true;
							    	else{	
								    	flag = false;
								    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
							    	} 
							     }
							     $('#loading').fadeOut();
						 	},  
						 	error:function(result){
							 	alert("err "+result);
						 		 $('.tempresult').html(result);
		                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
		                            $('#loading').fadeOut();
		                            return false;
						 	} 
			        	});
			        	if(flag==true){
			        		$($(slidetab).children().get(0)).text(lineNo);
			        		$($(slidetab).children().get(1)).text(defaultActualPicker); 
			        		$($(slidetab).children().get(2)).text(instalmentAmount);
			        		$($(slidetab).children().get(3)).text(balanceAmount); 
			        		
			        		
			        		//$($(slidetab).children().get(4)).text(remarks);
						   
						  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
						  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
							$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
							$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button
	
							 openFlag=0;
		
					  		$($($(slidetab).children().get(4)).children().get(0)).val($('#objLineVal').html());
					  		$("#transURN").val($('#objTrnVal').html());
					   } 
				  }else{
					  $("#othererror").hide().html("Balance Amount Should not below the Zero .!!!").slideDown(); 
					  return false;
				  }
			  }else{
				  return false;
			  }
		});

		 $('#instalmentAmount').keyup(function(){
			instalmentAmountTemp=Number($('#instalmentAmountTemp').val());
			total=Number($('#instalmentAmount').val());
			amcAmount=Number($('#amcAmount').val());
			$('.installmentAmountCalc').each(function(){
				if($(this).html() != null || $(this).html() != ''){
					total+=Number($(this).html());
				}
			});
			balanceAmount=amcAmount-(total-instalmentAmountTemp);
			if(balanceAmount < 0){
				$('#balanceAmount').val(balanceAmount);
				balanceFlag=false;
				$("#othererror").hide().html("Balance Amount Should not below the Zero .!!!").slideDown(); 
				  return false;
			}else{
				$('#balanceAmount').val(balanceAmount);
				$("#othererror").hide();
				balanceFlag=true;
			}
		});

		
});
 </script>	