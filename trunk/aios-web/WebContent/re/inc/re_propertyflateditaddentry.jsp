<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoFeatureEditAddEntry" id="propertyInfoFeatureEditAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.featuredetails"/></legend>
		<div>
			<label><fmt:message key="re.property.info.featuredetail1"/></label>
			<input type="text" name="measurement" id="measurement" class="width50">
		</div>
		<div>
			<label><fmt:message key="re.property.info.featuredetail2"/></label>
			<input type="text" name="remarks" id="remarks" class="width50">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.featuredetails"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.lineno"/></label>
				<input type="text" name="lineNo" id="lineNo" disabled="disabled" class="width50" />
			</div>
			<div>
			<label><fmt:message key="re.property.info.featurecode"/><span class="mandatory">*</span></label>
			<input type="text" name="featureCode" id="featureCode" class="width50 validate[required]" disabled="disabled"/>
			<input type="hidden" name="featureId" id="featureId" class="width50" />
			<span id="hit7" class="button" style="width: 40px ! important;">
				<a class="btn ui-state-default ui-corner-all feature-popup"> 
					<span class="ui-icon ui-icon-newwin"> </span> 
				</a>
			</span>
		</div>
		<div>
			<label><fmt:message key="re.property.info.features"/></label>
			<input name="features" id="features" class="width50" disabled="disabled">
		</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#propertyInfoFeatureEditAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			 $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(6)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button	 	
			openFlag=0;
		 });
		 $('#add').click(function(){
			  if($('#propertyInfoFeatureEditAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var featureCode=$('#featureCode').val();
					var features=$('#features').val();
					var measurement=$('#measurement').val();
					var remarks=$('#remarks').val();
									 
					var trnVal =$('#transURN').val(); 
					var propertyFlatId=$('#propertyFlatId').val();  	// Master Primary Key
					var actionFlag=$($($(slidetab).children().get(5)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_property_flat_feature_save.action", 
					 	async: false,
					 	data: {propertyFlatId: propertyFlatId,lineNumber: lineNo,featureCode: featureCode,features: features,measurements: measurement, 
							remarks: remarks,trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(featureCode);
		        		$($(slidetab).children().get(2)).text(features); 
		        		$($(slidetab).children().get(3)).text(measurement); 

		        		$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(5)).children().get(2)).val($('#objTempVal').html()); 
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCount=Number($('#childCount').val());
				  		childCount=childCount+1;
						$('#childCount').val(childCount);
				   } 
			  }else{
				  return false;
			  }
		});
		 $('.feature-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#property-popup').dialog('open');
				//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_flat_feature_details.action",
				 	async: false, 
				 	data:{},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			});
		 
		$('#property-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$("#featureCode").val("");
					$("#features").val("");  
					$(this).dialog("close"); 
				} 
			}
		});
		
});
</script>