<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';

$(document).ready(function (){ 
	$('#classId').val($('#tempclassId').val());
	$('#brandId').val($('#tempbrandId').val());
	if($('#endPicker').val() == '31-Dec-9999'){
		$('#endPicker').val("")
	}

	 $("#ownerInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

		$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_asset_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

		$('#startPicker').blur(function(){
			$('.error').hide();
		});

		$('#save').click(function(){ 
			$('.tempresultfinal').fadeOut();
			if($("#ownerInformationAdd").validationEngine({returnIsValid:true})){
				var buildingId=$('#buildingId').val();
				var classId=$('#classId').val();
				var brandId=$('#brandId').val();
				var modelNumber=$('#modelNumber').val();
				var noOfPieces=$('#noOfPieces').val();
				var fromDate=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
		        var toDate=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
				var buildingAssetId=$('#buildingAssetId').val();
				var subClassName=$('#subClassName').val();
				var specification=$('#specification').val();
				var detailDescription=$('#detailDescription').val();
				if(true){
					fromDate=$('#startPicker').val();
					toDate=$('#endPicker').val();
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/maintainence_asset_update.action",   
			     	async: false,
			     	data: {buildingAssetId:buildingAssetId,classId:classId,brandId: brandId,modelNumber: modelNumber,detailDescription: detailDescription,
						noOfPieces: noOfPieces,fromDate: fromDate,toDate: toDate, subClassName: subClassName,specification: specification},
					dataType: "html",
					cache: false,
					success:function(result){	
				 		$('.formError').hide(); 
				 		$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
				 	},  
				 	error:function(result){  
				 		$('.formError').hide(); 
				 		$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
                       return false;
				 	} 
				});
				}
				 else
				 {
					 $('.error').hide().html("To Date' should be greater than 'From Date' !!!").slideDown();
					 return false;
				 } 


				}else{return false;}
				return true;
			}); 

		if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'
		});

	
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.buildingmaintenanceasset"/></div>
		<div  class="response-msg error searcherror ui-corner-all" style="width:80%; display:none;"></div>
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
			<form name="ownerInformationAdd" id="ownerInformationAdd"> 
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.owner.info.validityinformation"/></legend>
						<div>
							<label class="width30"><fmt:message key="re.owner.info.fromdate"/><span style="color:red">*</span></label>
							<input type="text" name="startPicker" class="width30 validate[required]" id="startPicker" readonly="readonly"  value="${bean.fromDate }" />
						</div>
						<div>
							<label class="width30"><fmt:message key="re.owner.info.todate"/></label>
							<input type="text" name="endPicker" class="width30 tooltip" id="endPicker" readonly="readonly" value="${bean.toDate }" />
						</div>
				 	</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.owner.info.assetinformation"/></legend>
						<div>
							<label><fmt:message key="re.property.info.classname"/><span style="color:red">*</span></label>
							<input type="hidden" name="classId" class="width30" id="tempclassId" value="${bean.classId}"/>
							<select id="classId" class="width30 validate[required]" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.classList ne null}">
									<c:forEach items="${requestScope.classList}" var="classes" varStatus="status">
										<option value="${classes.classId }">${classes.className }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label><fmt:message key="re.property.info.subclassname"/></label>
							<input name="subClassName" class="width30"  id="subClassName" value="${bean.subClassName}"/>
						</div>
						<div>
							<label><fmt:message key="re.property.info.itemname"/></label>
							<input name="detailDescription" class="width30"  id="detailDescription" value="${bean.detailDescription}"/>
						</div>
						<div>
							<label><fmt:message key="re.property.info.detailspecification"/></label>
							<input name=specification class="width30"  id="specification" value="${bean.specification}"/>
						</div>
						<div>
							<input type="hidden" name="brandId" class="width30" id="tempbrandId" value="${bean.brandId}"/>
							<label><fmt:message key="re.owner.info.brand"/></label>
							<select class="width30" id="brandId" >
								<option value="">-Select-</option>
								<c:forEach items="${requestScope.lookupList}" var="typelist" varStatus="status">
									<option value="${typelist.brandId}">${typelist.brand}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label><fmt:message key="re.owner.info.modelnumber"/></label>
							<input type="text" name="modelNumber" class="width30" id="modelNumber" value="${bean.modelNumber}"/>
						</div>
						<div>
							<label><fmt:message key="re.owner.info.noofpieces"/><span class="mandatory">*</span></label>
							<input type="text" name="uaeIdNo" class="width30 validate[required,custom[onlyNumber]]" id="noOfPieces" value="${bean.noOfPieces}"/>
						</div>
					</fieldset> 
				</div>
			</form>
			<input type="hidden" id="buildingAssetId" value="${bean.buildingAssetId}"/>
			<div class="clearfix"></div>					
			<div class="float-right buttons ui-widget-content ui-corner-all"> 			
				<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.owner.info.cancel"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="save"><fmt:message key="re.owner.info.update"/></div>
			</div>
	  </div>
</div>