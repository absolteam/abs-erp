<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-management_decision_search.action?buildingId="+buildingId+'&flatId ='+flatId+'& fromDate ='+fromDate+'& toDate ='+toDate+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['decisionId','BUILDING ID','<fmt:message key="re.property.info.buildingname"/>',
			  		 'flatId','<fmt:message key="re.property.info.flatnumber"/>','<fmt:message key="re.property.info.rentalamount"/>',
			  		 '<fmt:message key="re.property.info.fromdate"/>','<fmt:message key="re.property.info.todate"/>','<fmt:message key="cs.common.button.workflowstatus"/>'], 
			 colModel:[ 
					{name:'decisionId',index:'DIFINE_DECISION_ID', width:100,  sortable:true},
					{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
					{name:'buildingName',index:'BUILDING_NAME ', width:150,sortable:true, sorttype:"data"},
					{name:'flatId',index:'FLAT_ID ', width:150,sortable:true, sorttype:"data"},
			  		 {name:'flatNumber',index:'FLAT_NUMBER', width:150,sortable:true, sorttype:"data"},
			  		 {name:'rentalAmount',index:'RENTAL_AMOUNT', width:150,sortable:true, sorttype:"data"},
			  		{name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"} ,
			  		{name:'toDate',index:'TO_DATE', width:100,  sortable:true,sorttype:"data"},
			  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,  sortable:true,sorttype:"data"},
			 ], 		  				  		  
			 rowNum:10, 		 
			 rowList:[5,10,20,30,35], 
			 pager: '#pager2', 
			 sortname: 'DIFINE_DECISION_ID', 
			 viewrecords: true, 
			 sortorder: "desc", 
			 caption:'<fmt:message key="re.property.info.definemanagementdecision"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["decisionId","buildingId","flatId","rentalAmount","workflowStatus"]);
	});  
 	</script>
	 <table id="list2"></table>  
	<div id="pager2"> </div> 
