<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${cityList != null }">
		<c:forEach items="${cityList}" var="cityBean" varStatus="status" >
		<option value="${cityBean.cityId}">${cityBean.cityName}</option>
	</c:forEach> 
	</c:when>
</c:choose>