<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">-Select-</option>
	<c:forEach items="${requestScope.approvedStatus}" var="approvedStatus1" varStatus="status">
	<option value="${approvedStatus1.approvedStatus }">${approvedStatus1.approvedStatusName}</option>
</c:forEach>
