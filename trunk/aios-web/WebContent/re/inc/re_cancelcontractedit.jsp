<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>

</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" /> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var actualID; var slidetab="";
var detailRowAdded = 0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){ 
	getCurrentDate();
	var effectiveTemp=$('#effectiveDateTemp').val().trim();
	$('.isPenalty').click(function() {
		 var isPenalty=$('input[name=isPenalty]:checked').val();
		 if(isPenalty==0){
			 addPeriods(); 
			 //$('#effectiveDate').datepick({onSelect: addPeriods}).datepick('setDate', new Date());
		 }else{
			 if(effectiveTemp==""){
				 getCurrentDate();
			 }else{
				 $('#effectiveDate').val($('#effectiveDateTemp').val());
			 }
		 }
			
	});
	
	//Contract AMount total	
		var	total=0;
		$('.contractAmountCalc').each(function(){
			total+=convertToDouble($(this).html());
			$(this).html(convertToAmount($(this).html()));
		});
		$('#contractAmountTotal').val(convertToAmount(total));
		$('#rentAmountTotalAll').val(convertToAmount(total));
	
		//Date Check blur
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}

		//select the radio - is penalty
		if($("#isPenaltyTemp").val()=='true')
			$("[name=isPenalty]").filter("[value=1]").attr("checked","checked");
		else if($("#isPenaltyTemp").val()=='false')
			$("[name=isPenalty]").filter("[value=0]").attr("checked","checked");
		//Select the radio - is requested
		if($("#isRequestedTemp").val()=='true')
			$("[name=isRequested]").filter("[value=1]").attr("checked","checked");
		else if($("#isRequestedTemp").val()=='false')
			$("[name=isRequested]").filter("[value=0]").attr("checked","checked");

		
	 $('.save_detail').click(function(){

		 var cancellationId=$("#cancellationId").val(); 
		 var contractId=$("#contractId").val();
		 var penaltyAmount=$("#penaltyAmount").val();
		 var isPenalty=$('input[name=isPenalty]:checked').val();
		 var isRequested=$('input[name=isRequested]:checked').val();
		 var effectiveDate=$("#effectiveDate").val();
		 if(isPenalty==1)
			 isPenalty=true;
		 else
			 isPenalty=false;
		 
		 if(isRequested==1)
			 isRequested=true;
		 else
			 isRequested=false;
		
		 if($jquery("#CancellationForm").validationEngine('validate')){
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/save_contract_cancellation_detail.action", 
				 data:{
					 	cancellationId:cancellationId,
					 	contractId:contractId,
					 	isPenalty:isPenalty,
						isRequested:isRequested,
						penaltyAmount:penaltyAmount,
						effectiveDate:effectiveDate
					 },
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
				   $("#main-wrapper").html(result);
			   },
		       error:function(result){  
				  $("#main-wrapper").html(result);
				  $(".success").hide();
				  $('.error').show(); 
				 $("#transURN").val(""); 
			  }
		   }); 
		 }
		 else{
			 return false;
		 }		  
	 });
      
	 $(".cancel_detail").click(function(){
		 slidetab=$(this).parent().parent().get(0);
		 var trnVal=$("#transURN").val(); 
		 $.ajax({
			 type:"POST",
			 url:"<%=request.getContextPath()%>/cancel_contract_list.action", 
			 //data:{trnValue:trnVal},
			 async: false,
			  dataType: "html",
			   cache: false,
			   success:function(result){  
				 $('.formError').hide(); 
				 	$('#transURN').val("");  
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html(); 
				    if(tempsMsg!=""){
				       $('#sMsg').show(); 
				       $('#eMsg').hide();
				     }
				     else{ 
				    	$('#eMsg').show(); 
				    	$('#sMsg').hide();
				    }  
			 },
		     error:function(result){
				 $("#main-wrapper").html(result);
				 $(".success").hide();
				 $('.error').show(); 
				 $("#transURN").val(""); 
				 $("#trnValue").val("");
			 }
		 });
			 
	});
});	

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
function addPeriods() { 
	 var date = new Date($('#effectiveDate').datepick('getDate')[0].getTime()); 
	    $.datepick.add(date, parseInt($('#noticeMonth').val(), 10), 'm'); 
	    $('#effectiveDate').val($.datepick.formatDate(date)); 
}
function getCurrentDate(){
	
		var effectDat=$('#effectiveDate').val().trim();
	//Cancellation Date
	if(effectDat==""){
		var curDate=new Date().toString();
		var splitDate=curDate.split(" ");
		var day=splitDate[2];
		var mon=splitDate[1];
		var year=splitDate[3];
		fromDate=day+"-"+mon+"-"+year;
		$('#effectiveDate').val(fromDate);
		$('#effectiveDate').datepick();
	}
}
</script>

<div id="main-content">
<div style="display:none;" class="tempresult"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Contract Cancellation Information
		</div>
	<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
	 		<fieldset style="padding-left: 10px;">
	 			<legend>Offer & Contract Details</legend>
	 			<div class="width33 float-left" id="hrm">
				  <fieldset style="height:130px;">
						<input type="hidden" name="cancellationId" value="${CANCELLATION_INFO.cancellationId }"  id="cancellationId" >
						
						
						<div >
							<label class="width30"><fmt:message key="re.contract.contractno"/></label>	 
							<input class="width60" type="text" name="contractNumber" value="${bean.contractNumber }"  id="contractNumber" disabled="disabled" >
							<input class="width50" type="hidden" name="contractId" value="${bean.contractId }"  id="contractId" disabled="disabled" >
							
						</div>
						<div >
							<label class="width30"><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>	 
							<input class="width60 contractDate validate[required]" type="text" name="contractDate" value="${bean.contractDate }"  id="defaultPopup" readonly="readonly" disabled="disabled">
						</div>
						<div>
							<label class="width30">Installments<span style="color:red">*</span></label>	 
							<input class="width60 validate[required,custom[onlyNumber]]" type="text" name="noOfCheques" value="${bean.noOfCheques }"  id="noOfCheques" disabled="disabled">
						</div>
						<div><label class="width30">Contract Fee<span style="color:red">*</span></label>
						<input type="text" class="width60 validate[required]" id="contractAmount" value="${bean.contractAmount }" name="contractAmount" tabindex="7" disabled="disabled">
						</div>
						
						<div><label class="width30">Deposit<span style="color:red">*</span></label>
						<input type="text" class="width60 validate[required]" id="depositAmount" value="${bean.depositAmount }" name="depositAmount" tabindex="8" disabled="disabled">
						</div>
					</fieldset>	
				</div>
	 			<div class="width33 float-left" id="hrm">
	 				<fieldset style="height:130px;">
						<div>
							<label class="width30"><fmt:message key="re.contract.offerno"/><span style="color:red">*</span></label>	 
							<input class="validate[required] width60" type="text" name="offerNumber" value="${OFFER_INFO.offerNumber }" id="offerNumber" disabled="disabled" >
							<input class="width30" type="hidden" name="offerId" value="${OFFER_INFO.offerId }"  id="offerId" disabled="disabled" >
						</div>
						
						<div>
							<label class="width30"><fmt:message key="re.contract.tenantname"/></label>	 
							<input class="width60" type="text" name="tenantName" value="${OFFER_INFO.tenantName }"  id="tenantName" disabled="disabled" >
							<input class="width40" type="hidden" name="tenantId" value="${OFFER_INFO.tenantId }"  id="tenantId" >
							<input class="width30 " id="rentFees" type="hidden" name="rentFees" value="${OFFER_INFO.rentFees }"  disabled="disabled">
							<input class="width30 " id="rentFeesAmount" type="hidden" name="rentFeesAmount" value="${OFFER_INFO.rentAmount }"  disabled="disabled">
						</div>
						<div>
							<label class="width30"><fmt:message key="re.contract.address"/></label>	 
							<input class="width60" type="text" name="presentAddress" value="${OFFER_INFO.presentAddress }"  id="presentAddress" disabled="disabled" >
						</div>
						<div>
							<label class="width30"><fmt:message key="re.contract.address1"/></label>	 
							<input class="width60" type="text" name="permanantAddress" value="${OFFER_INFO.permanantAddress }"  id="permanantAddress" disabled="disabled" >
						</div>
					</fieldset>	
				</div>
				<div class="width33 float-left" id="hrm">
					<fieldset style="height:130px;">
						<div><label class="width30" id="fromDateCheck"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
						<input type="text" class="width60 dateCheck validate[required]" id="startPicker" value="${OFFER_INFO.fromDate }" name="employerFromDate" tabindex="7" readonly="readonly"  disabled="disabled">
						</div>
						
						<div><label class="width30"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
						<input type="text" class="width60 employerToDate dateCheck validate[required]" id="endPicker" value="${OFFER_INFO.toDate }" name="employerToDate" tabindex="8" readonly="readonly"  disabled="disabled">
						</div>
						<div><label class="width30"><fmt:message key="re.contract.years"/></label><input type="text" class="width60" name="years" id="years" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.months"/></label><input type="text" class="width60" name="months" id="months" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.days"/></label><input type="text" class="width60" name="days" id="days"  disabled="disabled"></div>
					</fieldset>	
				</div>
		</fieldset>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			 <div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.rentdetails"/></div>
				<div id="hrm" class="hastable width100"> 
					<table id="hastab" class="width100">	
							<thead class="chrome_tab">									
							<tr>
								<th class="width20">Property/Component/Unit Name</th>
								<th class="width20" style=" width:5%;display:none;"><fmt:message key="re.contract.rentamount"/></th>
								<th class="width20"><fmt:message key="re.contract.contractamount"/></th>
							</tr> 
						</thead> 
						<tbody class="tab tabRent">
							<c:forEach items="${requestScope.OFFER_DETAIL_LIST}" var="result" varStatus="status" >
							 	<tr class="even"> 
									<td class="width20">${result.flatNumber }</td>	
									<td class="width20 rentAmountCalc" style=" width:5%;display:none;">${result.rentAmount }</td> 
									<td class="width20 contractAmountCalc">${result.contractAmount }</td>
									<td style="display:none"> 
										<input type="hidden" value="${result.contractRentId }" name="actualLineId" id="actualLineId"/>	
										<input type="hidden" name="actionFlag" value="U"/>
		              					<input type="hidden" name="tempLineId" value=""/>
									</td> 
									
									<td style="display:none;" class=""></td>
									<td style="display:none;"></td>	
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="float-right " style="width:50%">
				<label><fmt:message key="re.contract.total"/></label>
			 	<input name="rentAmountTotal" id="rentAmountTotal" class="width40" disabled="disabled" style="display:none">
			 	<input name="contractAmountTotal" id="contractAmountTotal" class="width70" disabled="disabled" >
			</div>
		</div>
		<div class="clearfix"></div>
				<div class="width100 float-left" id="hrm">
					<fieldset style="height:100px;">
						 <legend>Cancellation Information</legend>
						
						
					<div class="width30 float-left" id="hrm">
						<fieldset style="height:40px;">
						<input type="hidden" name="isRequestedTemp" id="isRequestedTemp" value="${CANCELLATION_INFO.isRequested}" >
							 <legend>Cancellation Initiated By<span style="color:red">*</span></legend>
							<div>
								<input type="radio" class="width10 float-left" value="1" name="isRequested" id="isRequested">
								<label class="width20 float-left">Tenant</label>
							</div>
							<div>
								<input type="radio" class="width10 float-left"  value="0" name="isRequested" id="isRequested">
								<label class="width20 float-left">Management</label>
							</div>
							
						</fieldset>	
					</div>
					<form name="CancellationForm" id="CancellationForm">
						<div class="width60 float-left cancellation-process" id="hrm">
							<fieldset style="height:40px;">
								<input type="hidden" name="isPenaltyTemp" id="isPenaltyTemp" value="${CANCELLATION_INFO.isPenalty}" >
								<input type="hidden" name="penaltyAmount" id="penaltyAmount" value="${CANCELLATION_INFO.penaltyAmount}" >
								 <legend>Process Of Cancellation<span style="color:red">*</span></legend>
								 <div>
									<input type="radio" class="width5 float-left isPenalty"  value="1" name="isPenalty" id="isPenalty">
									<label class="width10 float-left">Penalty</label>
								</div>
								<div>
									<input type="radio" class="width5 float-left isPenalty" value="0" name="isPenalty" id="isPenalty">
									<label class="width20 float-left">Notice Period</label>
								</div>
								<div id="noticediv">
									<label class="width20">Notice Month</label>	 
									<input class="width20 validate[required]" type="text" id="noticeMonth" name="noticeMonth" value="2" disabled="disabled">
								</div>
								
							</fieldset>	
						</div>
						<div class="width100 float-left">
							<label class="width10 float-left">Effective Date<span style="color:red">*</span></label>
							<input type="text" class="width20 float-left" readonly="readonly"  value="${CANCELLATION_INFO.effectiveDate}" name="effectiveDate" id="effectiveDate">
							<input type="hidden" class="width20 float-left" readonly="readonly"  value="${CANCELLATION_INFO.effectiveDate}" name="effectiveDateTemp" id="effectiveDateTemp">
						</div>
					</form>
					
					</fieldset>	
				</div>
			
	<div class="clearfix"></div> 
	<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>  
	
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " id="action_buttons" style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right cancel_detail" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
	<div class="portlet-header ui-widget-header float-right save_detail" style="cursor:pointer;">Save</div>
</div>

<input type="hidden" name="availflag" id="availflag" value="1"/>
</div>
