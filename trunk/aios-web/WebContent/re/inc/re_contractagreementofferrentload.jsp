<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:forEach items="${requestScope.OFFER_DETAIL_LIST}" var="result" varStatus="status">
 	<tr>  	 
		<td class="width20">${status.index+1}</td>
		<td class="width20">${result.flatNumber}</td>	
		<td class="width20 rentAmountCalc" style="display:none">${result.rentAmount}</td> 
		<td class="width20 contractAmountCalc">${result.contractAmount}</td>
		<td style="display:none"> 
			<input type="hidden" value="${result.offerDetailId}" name="actualLineId" id="actualLineId"/>   	
		</td> 
		
	<td style="display:none;" class=""></td>
	<td style="display:none;"></td>		 
	</tr>  
</c:forEach>	
