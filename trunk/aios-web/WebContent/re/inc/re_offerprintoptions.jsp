<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div>
	<div style="margin-bottom:20px;">
		<label class="width30">Template</label>
		 <select tabindex="1" name="offerTemplate" class="width70 float-right" id="offerTemplate">
			<option value="OfferLetter">Default</option>
			<option value="OfferLetterGovernment">Government</option>
			<option value="OfferLetterMilitary">Military</option>
		 </select> 
	</div>
	<div style="margin-bottom:20px;">
		<label class="width30">Bank Account</label>
		 <select  tabindex="2" name="bankAccount" class="width70 float-right" id="bankAccount">
			<c:if test="${requestScope.BANK_ACCOUNTS!=null}">
				<c:forEach items="${requestScope.BANK_ACCOUNTS}" var="bankAcc">
					<option value="${bankAcc.personBankId}">${bankAcc.accountTitle} </option>
				</c:forEach>
			</c:if>	
		 </select> 
	</div>
	<div style="margin-bottom:50px;">
		<label class="width30">Offer Title / Subject</label>
		<textarea tabindex="3" name="offerPrintInfo" class="width70 float-right" id="offerPrintInfo" rows="2"></textarea>
	</div>
	<div class="error response-msg ui-corner-all" style="display: none;" id="err_label">
		
	</div>
</div>

<script>
$(function () { 
	
	
});
</script>