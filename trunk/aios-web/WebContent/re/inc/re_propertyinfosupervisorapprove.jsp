<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
 
  <script type="text/javascript" src="/js/thickbox-compressed.js"></script>
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Property Info Approval</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var openFlag=0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){  
	$('#district').val($('#districtTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#country').val($('#countryTemp').val());
	$('#measurementCode').val($('#measurementCodeTemp').val());
	$('#propertyType').val($('#propertyTypeTemp').val());
	$('#status').val($('#statusTemp').val());
	
	if($('#endPicker').val() =='31-Dec-9999'){
		$('#endPicker').val("")
	}

	approvedStatus=$('#approvedStatus').val();
	if(approvedStatus == 'A' ){
		$('#approve').remove();	
		$('#reject').remove();	
		$('#cancel').remove();
		$('#close').fadeIn();
		$('.childCountErr').hide().html("Workflow is already Approved!!!").slideDown();
	}
	
	$('.formError').remove(); 
	 $("#propertyInfoEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	$("#propertyInfoComments").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

 	var dmsUserId='${emp_CODE}';
	var dmsPersonId='${PERSON_ID}';
	var companyId=Number($('#headerCompanyId').val());
	var applicationId=Number($('#headerApplicationId').val());
	var functionId=Number($('#headerCategoryId').val());
	var functionType="list,upload";//edit,delete,list,upload
	//$('#dmstabs-1, #dmstabs-2').tabs();
	//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
	$('#dms_create_document').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
			loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
			//$('#tabs').tabs({selected:0});// switch to first tab
	});
	$('#dms_document_information').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
		loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
		//$('#tabs').tabs({selected:1});// switch to second tab
	});

	//Approve - Save Property Information
	$("#approve").click(function(){
		$('.tempresultfinal').fadeOut();
		if($('#propertyInfoComments').validationEngine({returnIsValid:true})){  
			var childCount = $('#childCount').val();
			var  childCountFeature=$('#childCountFeature').val();
			var  childCountComponent=$('#childCountComponent').val();
			if(childCount!="" && childCount > 0 && childCountFeature!="" && childCountFeature>0 && childCountComponent!="" && childCountComponent>0 ){
				buildingId= $('#buildingId').val();

				var headerFlag=$("#headerFlag").val();

				comments=$('#supervisorComments').val();

				//Work Flow Variables
				sessionPersonId = $('#sessionPersonId').val();
				companyId=Number($('#headerCompanyId').val());
				applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Property Info Approved by Supervisor";
				functionId=Number($('#functionId').val());
				functionType="approve";
				notificationId=$('#notificationId').val();
				workflowId=$('#workflowId').val();
				mappingId=$('#mappingId').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				
				total=0;
				$('.percentage').each(function(){ 
					total+=Number($(this).html());
				});
				
				if(total == 100){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/property_info_supervisor_approval.action",
						data: {buildingId: buildingId, headerFlag: headerFlag, trnValue: transURN,
							comments: comments, sessionPersonId: sessionPersonId,companyId: companyId,applicationId: applicationId,mappingId: mappingId,
							workflowStatus: workflowStatus,functionId: functionId,functionType: functionType,notificationId: notificationId,workflowId: workflowId},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.formError').hide();
								$('.childCountErr').hide()
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#approve').remove();	
									$('#reject').remove();	
									$('#cancel').remove();
									$('#close').fadeIn();
								} else{
									$('.tempresultfinal').fadeIn();
								}
								// Scroll to top of the page
								$.scrollTo(0,300);
						},  
						error:function(result){
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							// Scroll to top of the page
							$.scrollTo(0,300);
					 	} 
					});
					$('#loading').fadeOut();
				}else{
					$('.childCountErr').hide().html("Owner details Percentage Sum should be equal to 100.!!!").slideDown();
					// Scroll to top of the page
					$.scrollTo(0,300);
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Child Record").slideDown();
				// Scroll to top of the page
				$.scrollTo(0,300);
				return false; 
			}
			
		 }else{
			  return false;
		  }
	});

	//Reject - Save Property Information
	$("#reject").click(function(){
		$('.tempresultfinal').fadeOut();
		if($('#propertyInfoComments').validationEngine({returnIsValid:true})){  
			var childCount = $('#childCount').val();
			var  childCountFeature=$('#childCountFeature').val();
			var  childCountComponent=$('#childCountComponent').val();
			if(childCount!="" && childCount > 0 && childCountFeature!="" && childCountFeature>0 && childCountComponent!="" && childCountComponent>0 ){
				buildingId= $('#buildingId').val();

				var headerFlag=$("#headerFlag").val();

				comments=$('#supervisorComments').val();

				//Work Flow Variables
				sessionPersonId = $('#sessionPersonId').val();
				companyId=Number($('#headerCompanyId').val());
				applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Property Info Rejected by Supervisor";
				functionId=Number($('#functionId').val());
				functionType="reject";
				notificationId=$('#notificationId').val();
				workflowId=$('#workflowId').val();
				mappingId=$('#mappingId').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				
				total=0;
				$('.percentage').each(function(){ 
					total+=Number($(this).html());
				});
				
				if(total == 100){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/property_info_supervisor_approval.action",
						data: {buildingId: buildingId,  headerFlag: headerFlag, trnValue: transURN,
							comments: comments, sessionPersonId: sessionPersonId,companyId: companyId,applicationId: applicationId,mappingId: mappingId,
							workflowStatus: workflowStatus,functionId: functionId,functionType: functionType,notificationId: notificationId,workflowId: workflowId},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.formError').hide();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#approve').remove();	
									$('#reject').remove();	
									$('#cancel').remove();
									$('#close').fadeIn();
								} else{
									$('.tempresultfinal').fadeIn();
								}
								// Scroll to top of the page
								$.scrollTo(0,300);
						},  
						error:function(result){
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							// Scroll to top of the page
							$.scrollTo(0,300);
					 	} 
					});
					$('#loading').fadeOut();
				}else{
					$('.childCountErr').hide().html("Owner details Percentage Sum should be equal to 100.!!!").slideDown();
					// Scroll to top of the page
					$.scrollTo(0,300);
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Child Record").slideDown();
				// Scroll to top of the page
				$.scrollTo(0,300);
				return false; 
			}
			
		 }else{
			  return false;
		  }
	});
	
	
	$("#cancel").click(function(){ 
		$('.backtodashboard').trigger('click');
		// Scroll to top of the page
		$.scrollTo(0,300);
	}); 

	$('#close').click(function(){
		$('.backtodashboard').trigger('click');
		// Scroll to top of the page
		$.scrollTo(0,300);
	  });

	$("#supervisorComments").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#supervisorComments").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#supervisorComments").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    }); 

    

	//Get State List On Change Country List
	$('#country').change(function(){
		countryId=$('#country').val();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/property_info_state_load.action", 
	     	async: false,
	     	data:{countryId: countryId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#state").html(result); 
			}, 
			error: function() {
			}
		}); 		
	});

	//Get City List On Change State List
	$('#state').change(function(){
		stateId=$('#state').val();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/property_info_city_load.action", 
	     	async: false,
	     	data:{stateId: stateId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#city").html(result); 
			}, 
			error: function() {
			}
		}); 		
	});

	$.fn.globeTotal = function() { 	// Total for Percentage Calculation
		total=0;
		$('.percentage').each(function(){ 
			total+=Number($(this).html());
		});
		$('#percentageTotal').val(total)
		//var percentageTotal=$('#percentageTotal').val();
		
	}

	 $('#birthdate').datepick({onSelect: customRange, showTrigger: '#calImg'});
	 $("#text_area_present_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_present_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 

	    $("#text_area_permanent_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar1").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar1').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar1").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_permanent_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_permanent_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    });  

	    if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
	 	$('#startPicker,#endPicker').datepick({
	 		onSelect: customRange, showTrigger: '#calImg'
		});

	 	$('.tooltip').tooltip({
	        track: true,
	        delay: 0,
	        showURL: false,
	        showBody: " - ",
	        fade: 250
	    });
		
});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<form id="propertyInfoEdit">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyinformation"/></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
			<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 
		
			<fieldset>
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.property.info.propertyinformation2"/></legend>						
			                <div>
			                	<label class="width30"><fmt:message key="re.property.info.fromdate"/><span class="mandatory">*</span></label>
			                	<input type="text" disabled="disabled" name="fromDate" id="startPicker" value="${bean.fromDate }" class="width30 validate[required]" TABINDEX=10  title="Select Birth Date" readonly="readonly">
			                </div>		
		                    <div>
		                    	<label class="width30"><fmt:message key="re.property.info.todate"/></label>
		                    	<input type="text" disabled="disabled" name="toDate" id="endPicker" value="${bean.toDate }" class="width30" TABINDEX=11 title="Enter Designation" readonly="readonly">
		                    </div>
							<div><label class="width30"><fmt:message key="re.property.info.status"/><span class="mandatory">*</span></label>
								<input type="hidden" id="statusTemp" name="statusTemp" value="${bean.propStatus }" class="width30">
								<select id="status" disabled="disabled" class="tooltip" TABINDEX=12  style="width:31.5%;">
										<c:if test="${requestScope.propertyStatus ne null}">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.propertyStatus}" var="propertyStatusList" varStatus="status">
											<option value="${propertyStatusList.propStatusCode }">${propertyStatusList.propStatus }</option>
										</c:forEach>
										</c:if>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.landsize"/><span class="mandatory">*</span></label>
								<input type="text" name="landSize" disabled="disabled" id="landSize" value="${bean.landSize }" class="width10 validate[required]" TABINDEX=13 >
								<input type="hidden" id="measurementCodeTemp" name="measurementCodeTemp" value="${bean.measurementCode}" class="width30">
								<select id="measurementCode" disabled="disabled" class="width20  validate[required]" TABINDEX=14 >
									<option value="">-Select-</option>
									<c:if test="${requestScope.beanMeasurement ne null}">
									<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementList" varStatus="status">
										<option value="${beanMeasurementList.measurementCode }">${beanMeasurementList.measurementValue }</option>
									</c:forEach>
									</c:if>
								</select>
							</div>
					        <div>
					        	<label class="width30"><fmt:message key="re.property.info.propertytax"/></label>
					        	<input type="text" name="propertyTax" disabled="disabled" id="propertyTax" value="${bean.propertyTax }" TABINDEX=15 class="width30">
					        </div>
	                       	<div>
	                        	<label class="width30"><fmt:message key="re.property.info.yearofbuild"/><span class="mandatory">*</span></label>
	                        	<input type="text" disabled="disabled" name="yearOfBuild" id="yearOfBuild" value="${bean.yearOfBuild }" TABINDEX=16 class="width30 validate[required,custom[onlyNumber,singlelength[4,4]]]" maxlength="4">
	                       	</div>
	                      	<div>
	                      		<label class="width30"><fmt:message key="re.property.info.buildingcost"/><span class="mandatory">*</span></label>
	                      		<input type="text" disabled="disabled" name="marketValue" id="marketValue" value="${bean.marketValue }" TABINDEX=17 class="width30 validate[required,custom[onlyFloat]]">
	                      	</div>
	                       	<div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								<div id="UploadDmsDiv" class="width100">
									<label class="width30"><fmt:message key="re.property.info.imageupload"/></label>
									<input type="text" id="URNdms" value="${bean.image }" disabled="disabled" class="width30">
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			                            <div  id="dms_document_information" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
			                        </div>
								</div>
				    		</div>		                            
					</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.property.info.propertyinformation1"/></legend>
							<div style="display:none;">
								<label>Person Id</label><input type="text" name="sessionPersonId" value="${bean.sessionPersonId}" class="width40" id="sessionPersonId" disabled="disabled">
								<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
								<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
								<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
								<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
								<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
							</div>
							<div style="display:none;">
								<label class="width30">Id No.</label>
								<input type="hidden" name="buildingNumber" id="buildingNumber" value="${bean.buildingNumber }" class="width30" disabled="disabled">
								<input type="hidden" name="buildingId" id="buildingId" value="${bean.buildingId }" class="width30" disabled="disabled">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.propertytype"/><span class="mandatory">*</span></label>
								<input type="hidden" id="propertyTypeTemp" name="propertyTypeTemp" value="${bean.propertyTypeName }" class="width30 validate[required]">
								<select id="propertyType" disabled="disabled" class="validate[required]" TABINDEX=1  style="width:31.5%;">
									<option value="">-Select-</option>
									<c:if test="${requestScope.propertyList ne null}">
									<c:forEach items="${requestScope.propertyList}" var="propertyTypeList" varStatus="status">
											<option value="${propertyTypeList.propertyTypeCode }">${propertyTypeList.propertyTypeName }</option>
										</c:forEach>
										</c:if>
								</select>
							</div>	
							<div>
								<label class="width30 tooltip" title="Enter Building Name" ><fmt:message key="re.property.info.buildingname"/><span class="mandatory">*</span></label>
								<input type="text" disabled="disabled" name="buildingName" id="buildingName" value="${bean.buildingName }" class="width30 validate[required]" TABINDEX=2>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.plotno"/><span class="mandatory">*</span></label>
								<input type="text" disabled="disabled" id="plotNo" name="plotNo" value="${bean.plotNo }" class="width30 validate[required]" TABINDEX=3>
							</div>			
							<div>
								<label class="width30"><fmt:message key="re.property.info.nooffloors"/><span class="mandatory">*</span></label>
								<input type="text" disabled="disabled" id="floorNo" name="floorNo" value="${bean.floorNo }" class="width30 validate[required]" TABINDEX=4>
							</div>
							
							<div>
								<label class="width30"><fmt:message key="re.property.info.addressLine1"/></label>
								<input type="text" disabled="disabled" id="addressLine1" name="addressLine1" value="${bean.addressLine1 }" class="width30" TABINDEX=5>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.addressLine2"/></label>
								<input type="text" disabled="disabled" id="addressLine2" name="addressLine2" value="${bean.addressLine2 }" class="width30" TABINDEX=6>
							</div>
							
							<div>
								<label class="width30"><fmt:message key="re.property.info.country"/><span class="mandatory">*</span></label>
								<input type="hidden" id="countryTemp" name="countryTemp" value="${bean.countryId }" class="width30">
								<select id="country" disabled="disabled" class="validate[required]" TABINDEX=7  style="width:31.5%;">
									<option value=""> - Select - </option>
									<c:if test="${requestScope.countryList ne null}">
									<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
										<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
									</c:forEach>
									</c:if>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.state"/><span class="mandatory">*</span></label>
								<input type="hidden" id="stateTemp" name="stateTemp" value="${bean.stateId }" class="width30">
								<select id="state" disabled="disabled" class="validate[required]" TABINDEX=8  style="width:31.5%;">
									<option>-Select-</option>
									<c:if test="${requestScope.stateList ne null}">
									<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
										<option value="${statelist.stateId }">${statelist.stateName}</option>
									</c:forEach>
									</c:if>
								</select>
							</div>	
							<div>
								<label class="width30"><fmt:message key="re.property.info.city"/><span class="mandatory">*</span></label>
								<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" class="width30 validate[required]">
								<select id="city" disabled="disabled" class="validate[required]" TABINDEX=9  style="width:31.5%;">
									<option value="">-Select-</option>
									<c:if test="${requestScope.cityList ne null}">
									<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
										<option value="${citylist.cityId }">${citylist.cityName}</option>
									</c:forEach>
									</c:if>
								</select>
							</div>	
							<div style="display:none;">
								<label class="width30"><fmt:message key="re.property.info.district"/><span class="mandatory">*</span></label>
								<input type="hidden" id="districtTemp" name="districtTemp" value="${bean.districtName }" class="width30">
								<select id="district" disabled="disabled" class=""  style="width:31.5%;" >
									<option value="">-Select-</option>
									<option value="MAS">Chennai</option>
									<option value="TPJ">Trichy</option>
									<option value="MDU">Madurai</option>
									<option value="TEN">Tirunelveli</option>
								</select>
							</div>	
					</fieldset> 
				</div> 
			</fieldset>
		</form>
			<div id="main-content"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.ownerdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
					 		<input type="hidden" name="percentageTotal" id="percentageTotal" style="width:45%!important;" disabled="disabled" >
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.property.info.ownername"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.ownertype"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.civilidno"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.percentage"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabOwner" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
												<tr>  	 
													<td class="width20">${result.ownerName }</td>
													<td class="width20">${result.ownerType }</td>	
													<td class="width20">${result.uaeNo }</td> 
													<td class="width20 percentage">${result.percentage }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.propertyInfoOwnerId }" name="actualLineId" id="actualLineId"/>	
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/> 
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwner" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwner"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwner" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="owner_id">${result.ownerId }</td>
													<td style="display:none;"></td>		 
												</tr>  
											</c:forEach>	
										 	<%for(int i=0;i<=2;i++) { %>
											 	<tr>  	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20 percentage"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwner" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwner"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwner" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="owner_id"></td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	<%}%>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsowner" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>	
			<div id="main-content"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
					<form name="newFields1">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuredetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCount" id="childCountFeature"  value="${countSize2}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.featurecode"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.features"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.featuredetail1"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.featuredetail2"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabFeature" style="">	
											<c:forEach items="${requestScope.result2}" var="result2" >
												<tr class="row">  
													<td class="width10">${result2.lineNumber }</td>	 
													<td class="width20">${result2.featureCode }</td>
													<td class="width20">${result2.features }</td>	
													<td class="width10">${result2.measurements }</td> 
													<td class="width10">${result2.remarks }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result2.propertyInfoFeatureId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="feature_id">${result2.featureId }</td>	
													<td style="display:none;"></td>	 
												</tr>  
											</c:forEach>
										 	<c:forEach var="i" begin="${countSize2+1}" end="${countSize2+2}">
												 <tr class="row">  
													<td class="width10">${i}</td>	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width10"></td> 
													<td class="width10"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>	
													<td style="display:none;"></td>	 
												</tr>  
								  		 	</c:forEach>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</form>
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div id="main-content"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
					<form name="newFields4">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.componentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCount" id="childCountComponent"  value="${countSize3}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.componenttype"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.noofcomponent"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.noofflatincomponent"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.fromdate"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.todate"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.totalarea"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabComponent" style="">	
											<c:forEach items="${requestScope.result3}" var="result3" >
												<tr class="component"> 
													<td class="width10">${result3.lineNumber }</td>
													<td class="width20">${result3.componentTypeName }</td>	
													<td class="width10">${result3.noOfComponent }</td> 
													<td class="width10">${result3.noOfFlatInComponent }</td>
													<td class="width10">${result3.fromDate }</td>
													<c:choose>
														<c:when test="${result3.toDate ne '31-Dec-9999' }">
															<td class="width10">${result3.toDate }</td>
														</c:when>
														<c:otherwise>
															<td class="width10"></td>
														</c:otherwise>
													</c:choose>
													<td class="width10">${result3.totalArea } ${result3.measurementValue }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result3.propertyComponentId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addComponent" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editComponent"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delComponent" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="feature_id">${result3.componentType }</td>	
													<td style="display:none;" class="measurment_code">${result3.measurementCode }</td>	 
													<td style="display:none;" class="total_area">${result3.totalArea }</td> 
												</tr>  
											</c:forEach>
										 	<c:forEach var="i" begin="${countSize3+1}" end="${countSize3+2}">
												 <tr class="component">  
													<td class="width10">${i}</td>	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width10"></td> 
													<td class="width10"></td>
													<td class="width10"></td>
													<td class="width10"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addComponent" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editComponent"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delComponent" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"  class="feature_id"></td>	
													<td style="display:none;" class="measurment_code"></td>	 
													<td style="display:none;" class="total_area"></td> 
												</tr>  
								  		 	</c:forEach>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</form>
					</div>
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowscomponent" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
				<div class="clearfix"></div>
				<form id="propertyInfoComments">
					<div class="width50 float-right" id="hrm">
						<fieldset>
					    	<legend><fmt:message key="cs.scanandupload.label.supervisorcomments"/><span style="color:red">*</span></legend>
					    	<div style="height:25px" class="width60 float-left">
					            <div id="barbox">
					                  <div id="progressbar"></div>
					            </div>
					            <div id="count">500</div>
					      	</div>
					      	<p>
					        	<textarea class="width60 float-left validate[required]" id="supervisorComments"></textarea>
					      	</p>
						 </fieldset>
					 </div>
				 </form>
				 <div class="clearfix"></div>
			</div>	
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
				<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
				<div class="portlet-header ui-widget-header float-right cancelrec" id="cancel" style=" cursor:pointer;"><fmt:message key="cs.common.button.cancel"/></div>
				<div class="portlet-header ui-widget-header float-right" id="reject" style="cursor:pointer;"><fmt:message key="re.propertyinfo.button.reject"/></div> 
				<div class="portlet-header ui-widget-header float-right headerData" id="approve" style="cursor:pointer;"><fmt:message key="re.propertyinfo.button.approve"/></div>
			</div>	
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="property-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>
