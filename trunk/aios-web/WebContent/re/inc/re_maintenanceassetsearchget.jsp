<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
<div>
	<label class=""><fmt:message key="re.owner.info.brand"/></label>
		<select class="width40 validate[required]" id="brandId">
			<option value="">-Select-</option>
				<c:forEach items="${requestScope.lookupList}" var="typelist" varStatus="status">
					<option value="${typelist.brandId}">${typelist.brand}</option>
				</c:forEach>
		</select>
</div>