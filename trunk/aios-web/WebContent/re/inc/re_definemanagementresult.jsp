<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %> 
<option value="">Select</option>
<c:if test="${item != null}">
	<c:forEach items="${requestScope.item}" var="flatList">
		<option value="${flatList.flatId}">${flatList.flatNumber} </option>
	</c:forEach>
</c:if> 