<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:'<%=request.getContextPath()%>/json-miantenance_classes_search.action?className='+className+'&relativeClassId='+relativeClassId,
		 datatype: "json", 
		 colNames:['CLASS_ID','<fmt:message key="re.property.info.classname"/>', 'RELATED CLASS ID', '<fmt:message key="re.property.info.relatedclass"/>'],
		 colModel:[ 
				{name:'classId',index:'CLASS_ID', width:100,  sortable:true},
				{name:'className',index:'CLASS_NAME', width:100,  sortable:true},
		  		{name:'relativeClassId',index:'RELATED_CLASS_ID', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'relativeClass',index:'CLASS_NAME', width:100,  sortable:true},
		  		
		 ], 		  				  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'classes.CLASS_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:'<fmt:message key="re.property.info.maintenanceclasses"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["classId","relativeClassId"]);
 	});
</script>
<table id="list2"></table>  
<div id="pager2"> </div> 
