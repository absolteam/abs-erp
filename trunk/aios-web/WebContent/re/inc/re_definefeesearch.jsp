<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-define_fee_search.action?descriptions="+descriptions+'&calculatedAs='+calculatedAs+'&fromDate='+fromDate+'&toDate='+toDate,
		 datatype: "json", 
		 colNames:['FEE_ID','<fmt:message key="re.property.info.feedescription"/>','<fmt:message key="re.property.info.calculatedas"/>','<fmt:message key="re.property.info.feeamount"/>','<fmt:message key="re.property.info.deposit"/>','<fmt:message key="re.property.info.fromdate"/>','<fmt:message key="re.property.info.todate"/>'], 
		 colModel:[ 
				{name:'feeId',index:'FEE_ID', width:100,  sortable:true},
				{name:'descriptions',index:'DESCRIPTION', width:100,  sortable:true},
		  		{name:'calculatedAs',index:'CALCULATED_AS', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'feeAmount',index:'FEE_AMOUNT', width:100,  sortable:true},
		  		{name:'deposit',index:'DEPOSIT', width:100,  sortable:true},
		  		{name:'FROM_DATE',index:'fromDate', width:100,  sortable:true},
		  		{name:'TO_DATE',index:'toDate', width:100,  sortable:true},
		  		
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'FEE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.definefee"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["feeId","deposit"]);
 	});
	</script>
<table id="list2"></table>  
<div id="pager2"> </div> 

 	