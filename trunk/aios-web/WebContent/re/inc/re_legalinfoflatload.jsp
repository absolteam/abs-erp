<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<option value="">Select</option>
<c:if test="${flatList != null}">
	<c:forEach items="${requestScope.flatList}" var="flat">
		<option value="${flat.flatId}">${flat.flatNumber} </option>
	</c:forEach>
</c:if> 
