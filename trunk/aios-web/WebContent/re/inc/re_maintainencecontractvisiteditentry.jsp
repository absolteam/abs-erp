<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementFeeAddEntry" id="contractAgreementFeeAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend>Visit Details 1</legend>
		<div>
			<label>Purpose<span style="color:red">*</span></label>
			<input type="text" name="purpose" id="purpose" class="width50  validate[required]">
		</div>
		<div>
			<label>Remarks</label>
			<input type="text" name="remarks" id="remarks" class="width50">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend>Visit Details</legend> 
			<div>
				<label>Visit Number<span style="color:red">*</span></label>
				<input type="text" name="visitNo" id="visitNo" disabled="disabled" class="width50" />
			</div>
			<div>
				<label>Person Name<span style="color:red">*</span></label>
				<input type="text" name="personName" id="personName"  class="width50 validate[required]" />
			</div>
		</fieldset>
</div>  
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#contractAgreementFeeAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing button	  	
	        openFlag=0;	
		 });
		 $('#defaultActualPicker').datepick();
		 $('#add').click(function(){
			  if($('#contractAgreementFeeAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var visitNo=$('#visitNo').val();
					var personName=$('#personName').val();
					var purpose=$('#purpose').val();
					var remarks=$('#remarks').val();
					var trnVal =$('#transURN').val(); 
					var maintenanceId=$('#maintenanceId').val();

					var actualLineId=$($($(slidetab).children().get(4)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					//alert("actualLineId : "+actualLineId+" uflag : "+uflag+" tempLineId : "+tempLineId);
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_edit_contract_visit_update.action", 
					 	async: false,
						data: {visitNo: visitNo,personName: personName,purpose: purpose,remarks: remarks, 
							  trnValue:trnVal,maintenanceId:maintenanceId,trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(visitNo);
		        		$($(slidetab).children().get(1)).text(personName);
		        		$($(slidetab).children().get(2)).text(purpose);
		        		$($(slidetab).children().get(3)).text(remarks); 
		        		

		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						 openFlag=0;
	
						$($($(slidetab).children().get(4)).children().get(0)).val($('#objLineVal').html());
				  		$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempId').html());
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCountPayment=Number($('#childCountPayment').val());
				  		childCountPayment=childCountPayment+1;
						$('#childCountPayment').val(childCountPayment);
				   } 
			  }else{
				  return false;
			  }
		});

});
 </script>	