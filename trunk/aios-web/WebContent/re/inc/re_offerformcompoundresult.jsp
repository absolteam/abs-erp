<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.Property"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.vo.PropertyVO"%>
<%@page
	import="com.aiotech.aios.realestate.domain.entity.vo.ComponentVO"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.vo.UnitVO"%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%
	List<ComponentVO> componentVos=new ArrayList<ComponentVO>();
	List<UnitVO> unitVos=new ArrayList<UnitVO>();
	PropertyVO propertyVO=(PropertyVO)ServletActionContext.getRequest().getAttribute("OFFER_PROPERTY_INFO");
	componentVos=propertyVO.getComponentVOs();
	Collections.sort(componentVos);
	int i=1; 
	out.println("<div id=\"tableWrap\" class=\"wrap_in\">"
					+"<div class=\"reference\" align=\"center\">"
						+" <table  width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"
							+"<tr>"+
							"<td title=\"Legend\" style=\"font-weight:bold; position: relative; float:left; top:7px; right:50px;\">Legend</td>"+
							"<td title=\"Available\"><img alt=\"Available\" src=\"./images/property/offer/leg_available.png\"></td>"+
							"<td style=\"font-weight:bold\">Available</td>"+
							"<td title=\"Mouse-over\"><img alt=\"Mouse-over\" src=\"./images/property/offer/leg_mouse_over.png\"></td>"+
							"<td style=\"font-weight:bold\">Mouse-over</td>"+
							"<td title=\"Selected\"><img alt=\"Selected\" src=\"./images/property/offer/leg_selected.png\"></td>"+
							"<td style=\"font-weight:bold\">Selected</td>"+
							"<td title=\"Offered\"><img alt=\"Offered\" src=\"./images/property/offer/leg_offered.png\"></td>"+
							"<td style=\"font-weight:bold\">Offered</td>"+ 
							"<td title=\"Rented\"><img alt=\"Rented\" src=\"./images/property/offer/leg_rented.png\"></td>"+
							"<td style=\"font-weight:bold\">Rented</td>"+
							"<td title=\"Sold\"><img alt=\"Sold\" src=\"./images/property/offer/leg_sold.png\"></td>"+
							"<td style=\"font-weight:bold\">Sold</td>"+
							"<td title=\"Pending\"><img alt=\"Pending\" src=\"./images/property/offer/leg_pending.png\"></td>"+
							"<td style=\"font-weight:bold\">Pending</td>"+  
							"<td title=\"Renovation\"><img alt=\"Renovation\" src=\"./images/property/offer/leg_renovation.png\"></td>"+
							"<td style=\"font-weight:bold\">Renovation</td>"+ 
							"</tr></table></div>"
					+" <div style='display:none'>"
						+" <span id=\"propertyName\">"+propertyVO.getPropertyName()+"</span>"
						+" <input type=\"hidden\" id=\"propertyId\" value="+propertyVO.getPropertyId()+">"
						+" <input type=\"hidden\" id=\"propertyRent\" value="+propertyVO.getRentYr()+">"
						+" <input type=\"hidden\" id=\"propertyStatus\" value="+propertyVO.getStatus()+">"
						+" <input type=\"hidden\" id=\"propertyType\" value="+propertyVO.getPropertyType().getType()+">"
						+" <input type=\"hidden\" id=\"propertyFloors\" value="+propertyVO.getFloors()+">"
					+" </div>"); 
					out.println("<div class=\"tablediv compoundDiv\" style=left:25%;top:53px;>");
					for(ComponentVO compvo:componentVos ){
						out.println(" <table cellspacing=0 id=tableid_"+i+" class=\"popup float-left tableclass width20\">");
						unitVos=new ArrayList<UnitVO>();
						unitVos=compvo.getUnitVOs();
						Collections.sort(unitVos); 
						int j=1; 
						for(UnitVO univo: unitVos){ 
							out.println(" <tr id=component_"+i+""+j+" class='componentTR'>"
								+" <td  class='showTD showPopupTD tabletdid width100 tooltip-csss' id=\'units_"+i+""+j+"'>"
										+ "<input type='hidden' name='unitStatus' id=unitStatus_"+i+""+j+" value="+univo.getStatus()+">"
										+ "<span class='unitname' id=popupUnitName_"+i+""+j+" style=\"display:none;\">"+univo.getUnitName()+"</span>"
										+ "<span class='unitRent' id=unitRent_"+i+""+j+" style=\"display:none;\">"+univo.getRent()+"</span>"
										+ "<span class='unitid' id=unitId_"+i+""+j+" style=\"display:none;\">"+univo.getUnitId()+"</span>" 
										+ "<img id=unitType_"+i+""+j+"  alt="+univo.getUnitType().getType()+" class=\"imgSize\">");
										if(univo.getOfferNumber()>0){
											out.println("<span class=\"classic\">"+univo.getUnitName()+
												"<br/>"+univo.getOfferNumber()+
												"<br/>"+univo.getTenantName()+"</span>");	 
										} 
										else{
											out.println("<span class=\"classic\">"+univo.getUnitName()+"</span>");	
										}
								out.println(" </td>");
							
								j++;
						}
						out.println(" </tr>");
						i++;
						out.println(" </table>"); 
					} 
	out.println("</div>");
%>
<style>
.selected{
	cursor:cell;
	background-color: #10FF10;
}
</style>
<script type="text/javascript">
var slidetab="";var css = {};  var unitStatus="";
var propertyId=""; var propertyRent=""; var propertyName="";
//for getting selections
var componentArray=new Array(2);
var unitArray= new Array();
var componentFlag="";
var unitFlag=""; 
var componentList="";
var unitList=""; var counter=0;
var offerId=0;
$(function(){
	 propertyId=$('#propertyId').val();
	setImageForUnit(); 
	checkPropertyStatus();  
	$('.showTD').bind('mouseenter',function(){
		var currentTD=$(this);
		$(currentTD).css('cursor','-moz-spinning');
		var rowId=getRowId($(currentTD).attr('id'));  
		var unitstatus=$('#unitStatus_'+rowId).val(); 
		if(unitstatus==1 || unitstatus==6){
			if($('#unitType_'+rowId).hasClass('selected')){
				$('#unitType_'+rowId).removeClass().addClass('selected');
			}else{
				$('#unitType_'+rowId).removeClass().addClass('mouseover');
			}
		}else{
			$('#units_'+rowId).css("cursor","not-allowed"); 
		}
		var tempvar=$(currentTD).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);
		popupUnitName=$('#popupUnitName_'+rowid).html();  
	}).bind('mouseleave', function(){
		var currentTD=$(this); 
		var rowId=getRowId($(currentTD).attr('id'));  
		var unitstatus=$('#unitStatus_'+rowId).val(); 
		if(unitstatus==1 || unitstatus==6){
			if($('#unitType_'+rowId).hasClass('selected')){
				$('#unitType_'+rowId).removeClass().addClass('selected');
			}
			else{ 
				if(unitstatus==6){
					$('#unitType_'+rowId).removeClass().addClass('offered');
				}else{
					$('#unitType_'+rowId).removeClass().addClass('bigroom');
				}
			}
		}  
		popupUnitName="";
	}); 
	
	$('.showTD').click(function(){  
		var currentTD=$(this);   
		var tempvar=$(currentTD).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);
		var unitStatus=$('#unitStatus_'+rowid).val();  
		var rowId=getRowId($(currentTD).attr('id'));  
		if(unitStatus==1){ 
			if($('#unitType_'+rowId).hasClass('selected')){ 
				$('#unitType_'+rowId).removeClass().addClass('bigroom');
			}else{
				$('#unitType_'+rowId).removeClass().addClass('selected');
			}
		}else if(unitStatus==6){
			if($('#unitType_'+rowId).hasClass('selected')){ 
				$('#unitType_'+rowId).removeClass().addClass('offered');
			}else{
				$('#unitType_'+rowId).removeClass().addClass('selected');
			}
		}else{
			$('#units_'+rowId).css("cursor","not-allowed"); 
		} 
	});
	
	$('#createOffer_'+propertyId).live('click',function(){ 
		 propertyId=$('#propertyId').val();
		 propertyRent=$('#propertyRent').val();
		 propertyName=$('#propertyName').html();  
		 var propertyType=$('#propertyType').val();  
		 componentFlag=0; 
		 unitList="";
		 unitArray= new Array();
		$('.selected').each(function(){   
			 var currentTD=$(this).attr("id");  
			 var idarray = currentTD.split('_');
			 var rowid=Number(idarray[1]);
			 var unitId=$('#unitId_'+rowid).html();   
			 var unitName=$('#popupUnitName_'+rowid).html();
			 var unitRent=$('#unitRent_'+rowid).html(); 
			 var unitTypeName=$('#unitType_'+rowid).attr('alt'); 
			 unitArray.push(unitId+","+unitName+","+unitTypeName+","+unitRent+",");   
		});
		 for(var j=0;j<unitArray.length;j++){ 
			unitList+=unitArray[j]+"@"; 
		}  
		if(unitList!=null && unitList!=""){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/getoffer_entryscreen.action", 
				data:{	componentList:componentList,unitList:unitList, propertyId:propertyId, componentFlag:componentFlag, 
						propertyRent:propertyRent, propertyName:propertyName, propertyType:propertyType, offerId:offerId},
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove(); 
					$("#main-wrapper").html("");  
					$("#main-wrapper").html(result); 
				},
				error: function(result){ 
					 $('#DOMWindow').remove();
					 $("#main-wrapper").html(""); 
					 $('#DOMWindowOverlay').remove();
					 $("#main-wrapper").html(result);  
					
				}
			});
			return false;
		 }
		 else{  
			$('#page-error').hide().html("Please select compound unit(s) to process").slideDown(400);
			$('#page-error').delay(2000).slideUp(1000);
			return false;
		 } 
	});
});
function setImageForUnit(){  
	$('.componentTR').each(function(){   
		slidetab = $($(this).parent()).parent().get(0); 
		var tableID=$(slidetab).attr('id');
		var idarray = tableID.split('_');
		var rowid=Number(idarray[1]);  
		var tdID=$($($(this).children()).get(0)).attr('id');
		idarray = tdID.split('_');
		rowid=Number(idarray[1]); 
		var unitType=$('#unitType_'+rowid).attr('alt').toLowerCase(); 
		css = {};
		css['width']=50+"%";
		css['height']=20;
		css['padding-left']=15+"%";
		css['padding-right']=15+"%";
		if(unitType=="flat"){  
			$('#unitType_'+rowid).css(css);
			$('#unitType_'+rowid).attr('src', 'images/property/offer/flat.png'); 
		}
		else if(unitType=="shop"){
			$('#unitType_'+rowid).css(css);
			$('#unitType_'+rowid).attr('src', 'images/property/offer/shop.png');
		}
		else if(unitType=="villa"){
			$('#unitType_'+rowid).css(css);
			$('#unitType_'+rowid).attr('src', 'images/property/offer/flat.png'); 
		} 
		else if(unitType=="office"){
			$('#unitType_'+rowid).css(css);
			$('#unitType_'+rowid).attr('src', 'images/property/units/cafe.png');
		}  
	});  
}

function checkPropertyStatus(){
	var propertyStatus=$('#propertyStatus').val(); 
	if(propertyStatus!=1){ 
		$('.tabletdid').val(propertyStatus);   
	} 
	checkUnitStatus();
}
function checkUnitStatus(){
	$('.componentTR').each(function(){
		slidetab = $(this);
		var componentTR=$($(slidetab).children().get(0)); 
		var tempvar=$(componentTR).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		unitStatus=$('#unitStatus_'+rowid).val(); 
		if(unitStatus==1){
			$('#unitType_'+rowid).addClass('bigroom');   
		}
		else if(unitStatus==2){
			$('#unitType_'+rowid).addClass('pending');   
		}
		else if(unitStatus==3){
			$('#unitType_'+rowid).addClass('rented');  
		}
		else if(unitStatus==4){
			$('#unitType_'+rowid).addClass('sold');  
		}
		else if(unitStatus==5){ 
			$('#unitType_'+rowid).addClass('renovation');  
		} 
		else if(unitStatus==6){
			$('#unitType_'+rowid).addClass('offered');    
		} 
	});
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>