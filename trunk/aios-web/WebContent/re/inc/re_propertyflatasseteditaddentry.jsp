<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="flatMaintainenceReleaseEditEntry" id="flatMaintainenceReleaseEditEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>	
	<legend><fmt:message key="re.property.info.assetdetails"/></legend> 				 
		<div>
			<label><fmt:message key="re.owner.info.brand"/></label>
			<input type="text" name="brand" id="brand" class="width50" disabled="disabled">
		</div>
		<div>
			<label><fmt:message key="re.owner.info.modelnumber"/></label>
			<input type="text" name="modelNumber" id="modelNumber" class="width50" disabled="disabled">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.linesdetails"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.lineno"/><span style="color:red">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50" disabled="disabled">
			</div>
			<div>
				<label><fmt:message key="re.property.info.maintenancecode"/><span class="mandatory">*</span></label>
				<input type="text" name="maintainenceCode" id="maintainenceCode" disabled="disabled" class="width50  validate[required]">
				<span id="hit7" class="button" style="width: 40px ! important;">
					<a class="btn ui-state-default ui-corner-all maintenance-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
			<div style="display:none">
				<label>Building Name></label>
				<input type="hidden" name="buildingName" id="buildingName" disabled="disabled" class="width50">
				<input type="hidden" name="buildingId" id="buildingId" class="width50" />
			</div>
			<div style="display:none">
				<label>No OF piecies</label>
				<input type="hidden" name="noOfPieces" id="noOfPieces" disabled="disabled" class="width50">
			</div>
			<div style="display:none">
				<label>From Date</label>
				<input type="hidden" name="fromDate" id="fromDate" disabled="disabled" class="width50">
			</div>
			<div style="display:none">
				<label>TO Date</label>
				<input type="hidden" name="toDate" id="toDate" disabled="disabled" class="width50">
			</div>
		</fieldset>
</div>
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#flatMaintainenceReleaseEditEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button	 	
			openFlag=0;	
		 });
		 $('#add').click(function(){
			  if($('#flatMaintainenceReleaseEditEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var maintainenceCode=$('#maintainenceCode').val();
					var brand=$('#brand').val();
					var modelNumber=$('#modelNumber').val();
					var propertyFlatId=$('#propertyFlatId').val();
					var trnVal =$('#transURN').val();  
					var actionFlag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
    				//alert("lineType : "+lineType+" trnVal : "+trnVal+" actionFlag : "+actionFlag+" tempLineId : "+tempLineId); 
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_flat_asset_save.action", 
					 	async: false,
					 	data: {lineNumber: lineNo,maintainenceCode: maintainenceCode,propertyFlatId: propertyFlatId, 
							trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(maintainenceCode);
		        		$($(slidetab).children().get(2)).text(brand); 
		        		$($(slidetab).children().get(3)).text(modelNumber); 

		        		$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempVal').html()); 
				  		$("#transURN").val($('#objTrnVal').html());
				  		$.fn.globeTotal();  //For Total Amount of Amount
	
				  		var childCount=Number($('#childCount').val());
				  		childCount=childCount+1;
						$('#childCount').val(childCount);
				   } 
			  }else{
				  return false;
			  }
		});

		 $('.maintenance-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#property-popup').dialog('open');
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/retrieve_flat_asset_details.action",
				 	async: false, 
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			});
		 
		$('#property-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			width:'50%',
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});		

});
 </script>	