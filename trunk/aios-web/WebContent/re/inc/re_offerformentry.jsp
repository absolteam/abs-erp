<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.TenantGroup"%><script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.aiotech.aios.realestate.to.OfferManagementTO"%>
<%@page import="com.aiotech.aios.realestate.*"%>
<style>
#offer-popup{
	overflow:hidden; 
}
</style>
<script type="text/javascript">
var tempid=""; var offerId=0;
var tenantArray=new Array();
var selectedTenantGroup = "";
$(function(){
	var offerId=$('#offerId').val();

	if("${tenantGroupId}" != null && "${tenantGroupId}" != '') { 
		
		$("#tenantGroupSelect").val("${tenantGroupId}");
		selectedTenantGroup = $("#tenantGroupSelect").val();
	}
	

	
	 $jquery("#offerEntryValidation").validationEngine('attach');
	 
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, minDate:0, showTrigger: '#calImg'}); 
	
	 $('.offer-popup').click(function(){ 
	       tempid=$(this).parent().get(0);   
	       $('.ui-dialog-titlebar').remove(); 
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/offer_redirect_tenant.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.offer-result').html(result);  
				},
				error:function(result){  
					 $('.offer-result').html(result); 
				}
			});  
		});
		 $('#offer-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$('.addData').click(function(){
				if($jquery("#offerEntryValidation").validationEngine('validate')){   
					 $('.error').hide(); 
					 slidetab=$(this).parent().parent().get(0);   
					 var tempvar=$(slidetab).attr("id");  
					 var idarray = tempvar.split('_');
					 var rowid=Number(idarray[1]);  
					 var showPage="";
					 if(offerId !=null && offerId!="" && offerId>0)
						 showPage="editadd";
					 else
						 showPage="addadd";
					 $("#AddImage_"+rowid).hide();
					 $("#EditImage_"+rowid).hide();
					 $("#DeleteImage_"+rowid).hide();
					 $("#WorkingImage_"+rowid).show(); 
					 var offerFor=$($($(slidetab).children().get(4)).children().get(1)).val();
					 
					 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/offer_addlineentry.action",
						data:{id:rowid, showPage:showPage, offerFor:offerFor},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 	$(result).insertAfter(slidetab); 
						 	$('#lineNumber').val($('#lineId_'+rowid).text());
						 	$('#offerFor').val($('#name_'+rowid).text());
						 	$('#rent').val($('#rent_'+rowid).text());
						 	$('#offerAmount').val($('#offeramount_'+rowid).text());
						 	$('#offerForId').val($('#commonid_'+rowid).val());
						 	$('#offer').val(offerFor);
						}
					 });
				}
				else{
					return false;
				}		
	  });
		
		$('.editData').click(function(){
			if($jquery("#offerEntryValidation").validationEngine('validate')){   
				 $('.error').hide(); 
				 slidetab=$(this).parent().parent().get(0);   
				 var tempvar=$(slidetab).attr("id");  
				 var idarray = tempvar.split('_');
				 var rowid=Number(idarray[1]);  
				 var showPage="";
				 if(offerId !=null && offerId!="" && offerId>0)
					 showPage="editedit";
				 else
					 showPage="addedit";
				 $("#AddImage_"+rowid).hide();
				 $("#EditImage_"+rowid).hide();
				 $("#DeleteImage_"+rowid).hide();
				 $("#WorkingImage_"+rowid).show(); 
				 var offerFor=$($($(slidetab).children().get(4)).children().get(1)).val();
				 
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/offer_addlineentry.action",
					data:{id:rowid, showPage:showPage, offerFor:offerFor},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 	$(result).insertAfter(slidetab); 
					 	$('#lineNumber').val($('#lineId_'+rowid).text());
					 	$('#offerFor').val($('#name_'+rowid).text());
					 	$('#rent').val($('#rent_'+rowid).text());
					 	$('#offerAmount').val($('#offeramount_'+rowid).text());
					 	$('#offerForId').val($('#commonid_'+rowid).val());
					 	$('#offer').val(offerFor);
					}
				 });
			}
			else{
				return false;
			}		
  	  });
		
		 $(".delrow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var lineId=$('#lineId_'+rowid).text();
			 var offeramount=$('#offeramount_'+rowid).text().trim();
			 if(offeramount!=null && offeramount!=""){
				 var flag=false; 
				 $.ajax({
						 type : "POST",
						 url:"<%=request.getContextPath()%>/offer_adddeleteline.action", 
						 data:{id:lineId},
						 async: false,
					  dataType: "html",
						 cache: false,
					   success:function(result){ 
							 $('.formError').hide();  
							 $('.tempresult').html(result);    
							 if(result!=null){
							    	if($('#returnMsg').html() != '' || $('#returnMsg').html() == "Success")
		                              flag = true;
							    	else{	
								    	flag = false; 
								    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
							    	} 
							     }  
						},
						error:function(result){
							$("#page-error").hide().html($('#returnMsg').html().slideDown()); 
							$('.tempresult').html(result);   
							return false;
						}
							
					 }); 
					 if(flag==true){ 
			        		 var childCount=Number($('#childCount').val());
			        		 if(childCount > 0){
								childCount=childCount-1;
								$('#childCount').val(childCount); 
			        		 } 
			        		 $(this).parent().parent('tr').remove(); 
			        		//To reset sequence 
			        		var i=0;
			     			$('.rowid').each(function(){  
								i=i+1;
								$($(this).children().get(0)).text(i); 
		   					 });  
					}
			 }
			 else{
				 $('#warning_message').hide().html("Please insert this record to delete...").slideDown(1000);
				 return false;
			 }  
		 });
		 
		 $('.discard').click(function(){
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/offer_discard.action", 
				 	async: false, 
				    dataType: "html",
				    cache: false,
					success:function(result){
						$('#offer-popup').dialog('destroy');		
	  					$('#offer-popup').remove(); 
				 		$("#main-wrapper").html(result); 
					}
				});
		 });
		 
		 $('.save').click(function(){ 
				var id=$('.save').attr('id'); 
				var offerId=$('#offerId').val();
				//var offerNumber=Number($('#offerNumber').val()); 
				var tenantId=$('#tenantId').val();
				tenantArray=tenantId.split("@#");
				tenantId=tenantArray[0];
				var offerTenantFor=tenantArray[1];
				var offerDate=$('#offerDate').val();
				var validityDays=$('#validityDays').val();
				var extensionDays=$('#extensionDays').val();
				var contractFrom=$('#startPicker').val();
				var contractTo=$('#endPicker').val(); 
				var offerNumber=0;
				if(selectedTenantGroup == "" || selectedTenantGroup == undefined) {
					selectedTenantGroup = 1;
				}
				var securityDeposit = $("#depositAmount").val();
				var tenantGroupId = selectedTenantGroup;
				var tenantOfferId=Number($('#tenantOfferId').val());
				if(offerId!=null && offerId!="" && offerId>0){ 
					offerId=offerId;
					url_action="offer_update";
					offerNumber=$('#offerNumber').val();
				}
				else{
					offerId=0;
					offerNumber=0;
					url_action="offer_save";  
				} 
				var offerFor=$('#offerFor').val();
				var offerStatus=6;
				var offerStringAmount= new Array();  
				$('.rowid').each(function() {
					var currentTD=$(this);
					var tempvar=$(currentTD).attr("id");  
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]);  
				 	offerStringAmount.push(convertToDouble($('#offerAmount_'+rowid).val()));
				}); 
				 if($jquery("#offerEntryValidation").validationEngine('validate')){   
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/"+url_action+".action?offerStringAmount="+offerStringAmount, 
					 	async: false,
					 	data:{	offerId:offerId, offerNumber:offerNumber, tenantId:tenantId, 
					 			offerDate:offerDate, validityDays:validityDays, extensionDays:extensionDays,
					 			contractFrom:contractFrom, contractTo:contractTo, offerFor:offerFor, 
					 			tenantOfferId:tenantOfferId, offerStatus:offerStatus, offerTenantFor:offerTenantFor, 
					 			tenantGroupId: tenantGroupId, securityDeposit: securityDeposit},
					    dataType: "html",
					    cache: false,
						success:function(result){
							 $(".tempresult").html(result);
							 var message=$('#returnMsg').html(); 
							 if(message.trim()=="Success"){
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/offer_details_list.action", 
									 	async: false,
									    dataType: "html",
									    cache: false,
										success:function(result){
											$('#offer-popup').dialog('destroy');		
						  					$('#offer-popup').remove(); 
											$("#main-wrapper").html(result); 
											$('#success_message').hide().html(message).slideDown(1000); 									
										}
								 });
							 }
							 else{
								 $('#journal-error').hide().html(message).slideDown(1000);
								 return false;
							 }
						}
					 }); 
					}
					else{	
						 return false;
					} 
			 });

		 $('.propertyrent').each(function(){
			 var rent=convertToAmount($(this).text());
			 $(this).text(rent);
			if(Number($('#offerId').val()>0)){ 
			}
			else{
				 var tempvar=$(this).attr('id');
				 var splitval=tempvar.split("_"); 
				 var idval=splitval[1]; 
				 $('#offerAmount_'+idval).val(rent);
			}
		 }); 

		$("#tenantGroupSelect").change( function () {

			$("#depositAmount").val($(this).children(":selected").attr("id").split("_")[1]);
			selectedTenantGroup = $("#tenantGroupSelect").val();
		});
		 
		 if(Number($('#offerId').val()>0)) { 
			 $('.offerrent').each(function(){
				 var tempvar=$(this).attr('id');
				 var splitval=tempvar.split("_"); 
				 var idval=splitval[1];  
				 var rent=convertToAmount($('#offerAmount_'+idval).val().trim()); 
				 $('#offerAmount_'+idval).val(rent);
			 });
		  }
		 
		 if(Number($('#offerId').val()) > 0) {
			 
				populateUploadsPane("doc","offerDocs","Offer", Number($('#offerId').val()));				
				$('#dms_document_information').click(function(){
					AIOS_Uploader.openFileUploader("doc","offerDocs","Offer",Number($('#offerId').val()),"OfferDocuments");
				});
			}
			else {
				$('#dms_document_information').click(function(){
					AIOS_Uploader.openFileUploader("doc","offerDocs","Offer","-1","OfferDocuments");
				});
			}
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth){
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		var offerId=$('#offerId').val();
		//if(offerId==null || offerId==0 || offerId=="")
			addPeriods(); 
	}
	else{
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);  
	} 
}
function addPeriods() {
    var date = new Date($('#startPicker').datepick('getDate')[0].getTime()); 
    if($('#periodSelection_Year').attr('checked') == true)
    	$.datepick.add(date, parseInt(364, 10), 'd'); 
    else if($('#periodSelection_HalfYear').attr('checked') == true)
    	$.datepick.add(date, parseInt(182, 10), 'd'); 
    else return;
    $('#endPicker').val($.datepick.formatDate(date)); 
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerform" /></div>	
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Property Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		  <form name="offerEntry" id="offerEntryValidation" style="position: relative;"> 
			<div class="portlet-content">  
				<fieldset style="border:1px solid #DDDDDD;">
					<div class="form_head"><fmt:message key="re.offer.offerDetails" /></div>
					<div id="journal-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  		<div class="tempresult" style="display:none;"></div>
			  		<input type="hidden" id="offerId" name="offerId" value="${OFFER_DETAILS_INFO.offerId}"/>
					<div class="width100 float-left" id="hrm">  
						<div class="float-right  width48">  
							<fieldset> 
								<div>
									<label><fmt:message key="re.offer.offerdate" /><span style="color: red;">*</span></label>   
									<c:choose>
										<c:when test="${OFFER_DETAILS_INFO.offerId ne null && OFFER_DETAILS_INFO.offerId ne ''}">
											<input type="text" id="offerDate" readonly="readonly" name="offerDate" value="${OFFER_DETAILS_INFO.offerDate}" class="width40 validate[required]"/>
										</c:when>
										<c:otherwise>
											<input type="text" id="offerDate" readonly="readonly" name="offerDate" value="${OFFER_CANVAS_DETAILS.offerDate}" class="width40 validate[required]"/>
										</c:otherwise>
									</c:choose>
								</div>	
								<div>							
									<label><fmt:message key="re.offer.offervaliditydays" /><span style="color: red;">*</span></label> 
									<c:choose>
										<c:when test="${OFFER_DETAILS_INFO.offerId ne null && OFFER_DETAILS_INFO.offerId ne ''}">
											<input type="text" id="validityDays" name="validityDays" value="${OFFER_DETAILS_INFO.validityDays}" class="validate[required] width40"/>
										</c:when>
										<c:otherwise>
											<input type="text" id="validityDays" name="validityDays" value="5" class="validate[required] width40"/>
										</c:otherwise>
									</c:choose>
									
								</div> 
								<div>
									<label><fmt:message key="re.offer.offerExtension" /><span style="color:red">*</span></label>
									<input type="text" id="extensionDays" name="extensionDays" value="${OFFER_DETAILS_INFO.extensionDays}"  class="validate[required] width40"/> 
								</div>
								<fieldset> 
							  		<legend><fmt:message key="re.offer.contractPeriod" /></legend>
							  		<div style="height: 24px;">
							  			<label><fmt:message key="re.offer.period" /></label>
							  			<input type="radio" value="Year" id="periodSelection_Year" name="periodSelection" checked="checked"> 1 Year
							  			<input type="radio" value="HalfYear" id="periodSelection_HalfYear" name="periodSelection"> Half Year
							  			<input type="radio" value="Other" id="periodSelection_Other" name="periodSelection"> Other
							  		</div>
							  		<div>
										<label><fmt:message key="re.offer.offerDatefrom" /><span style="color:red">*</span></label>
										<input type="text" id="startPicker" name="contractFrom" value="${OFFER_DETAILS_INFO.startDate}"  class="validate[required] width40"/> 
									</div>  
									<div>
										<label><fmt:message key="re.offer.offerDateTo" /><span style="color:red">*</span></label>
										<input type="text" id="endPicker" name="contractTo" value="${OFFER_DETAILS_INFO.endDate}"  class="validate[required] width40"/> 
									</div>  
							  </fieldset>   
						  </fieldset> 
						</div>
						<div class="float-left width50">
							<fieldset style="height: 166px;">
								<div>
									<label><fmt:message key="re.offer.offerno" /></label> 
									<c:choose>
										<c:when test="${OFFER_DETAILS_INFO.offerId ne null && OFFER_DETAILS_INFO.offerId ne ''}">
											<input type="text" id="offerNumber" disabled="disabled" name="offerNumber" value="${OFFER_DETAILS_INFO.offerNumber}" class="width40"/>
										</c:when>
										<c:otherwise>
											<input type="text" id="offerNumber" disabled="disabled" name="offerNumber" value="${offerNumber}" class="width40"/>
										</c:otherwise>
									</c:choose>  						
								</div>	
								<div>
									<label><fmt:message key="re.offer.tenant" /><span style="color: red;">*</span></label>
									<input name="tenantName" type="text" readonly="readonly" id="tenantName" value="${OFFER_DETAILS_INFO.tenantName}" class="validate[required] width40">
									<input type="hidden" id="tenantId" value="${OFFER_DETAILS_INFO.tenantId}@#${OFFER_DETAILS_INFO.commonId}"/>  
									<span class="button">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all offer-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
									<input type="hidden" id="tenantOfferId" value="${OFFER_DETAILS_INFO.tenantOfferId}"/>
								</div> 
								<div>
									<label> 
									<fmt:message key="re.offer.emiratesIdTrade" /></label>
									<input name="addressDetails" type="text" id="addressDetails" value="${OFFER_DETAILS_INFO.addressDetails}" class="width40">
								</div>  
								<div style="display: none;">
									<label><fmt:message key="re.property.info.country" /></label> 
									<input type="text" id="countryName" name="countryName" value="" class="width40"/>
									<input type="hidden" id="countryId" value=""/>
								</div>
								<div>	
								<%	List<TenantGroup> tenantGroup = (List<TenantGroup>) ServletActionContext
																.getRequest().getAttribute("TENANT_GROUPS_LIST");
								
									if(tenantGroup != null) { %> 
										<label><fmt:message key="re.offer.tenantGroup" /></label> 
										<select id="tenantGroupSelect" class="float-left" style="width: 41%;">
											<% for(TenantGroup group : tenantGroup) { %>
											
												<option id="deposit_<%= group.getDeposit() %>"	value="<%= group.getTenantGrpId() %>">
													<%= group.getTenantGrpTitle().split("_")[0] %>
												</option>
												
											<% } %> 
										</select> 
									<% } %>
								</div>	
								<div>
									<label><fmt:message key="re.offer.securityDeposit" /><span class="mandatory">*</span></label>
									<input name="depositAmount" class="validate[required,custom[number]] width40" type="text" id="depositAmount" 
										   value="${OFFER_DETAILS_INFO.securityDeposit}">
								</div> 										 							
							</fieldset>
						</div> 	
						<div style="display: block;">
							<fieldset style="height: 55px; float: left; width: 46%;">
								<div id="dms_document_information" style="cursor: pointer; color: blue;">
									<u><fmt:message key="re.property.info.uploadDocsHere" /></u>
								</div>
								<div style="padding-top: 10px; margin-top:3px; height: 38px; overflow: auto;">
									<span id="offerDocs"></span>
								</div>	
							</fieldset>
						</div>			
				 	</div>
			</fieldset> 
			
			<div class="clearfix"></div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="margin-top:15px;">  
				<div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerFeatures" /></div>
	 			<div class="portlet-content"> 
	 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100"  > 
						 <input type="hidden" readonly="readonly" name="trnValue" id="trnValue"/> 
						 <input type="hidden" name="childCount" id="childCount" value="${OFFER_INFO_SIZE}"/>  
						 <input type="hidden" name="offerFor" id="offerFor" value="${OFFER_CANVAS_DETAILS.offerFor}"/>  
						<table id="hastab" class="width100"> 
							<thead>
							   <tr> 
							   		<th style="width:5%;"><fmt:message key="re.offer.lineNo" /></th>
									<th><fmt:message key="re.offer.propertyUnit" /></th>
									<th><fmt:message key="re.offer.type" /></th>
									<th><fmt:message key="re.offer.rent" /></th>  
									<th><fmt:message key="re.offer.offerAmount" /></th>
									<th style="width:7%;display:none;"><fmt:message key="re.contract.options"/></th> 
							  </tr>
							</thead> 
							<tbody class="tab">  
								<c:choose>
									<c:when test="${OFFER_CANVAS_DETAILS.componentFlag ne 0 && OFFER_CANVAS_DETAILS.componentFlag eq 1 || OFFER_CANVAS_DETAILS.propertyType eq 'villa' }"> 
										<c:set var="i" value="1"/>
										<tr class="rowid" id="fieldrow_${i}">
											<td style="width:5%;" id="lineId_${i}">${i}</td> 
											<td id="name_${i}">${OFFER_CANVAS_DETAILS.propertyName}</td> 	
											<td id="type_${i}">${OFFER_CANVAS_DETAILS.propertyTypes}</td> 										
											<td id="rent_${i}" class="propertyrent">${OFFER_CANVAS_DETAILS.propertyRent}</td> 																						
											<td id="offeramount_${i}" class="offerrent">
												<input type="text" name="offeramount" class="width30 validate[required]" id="offerAmount_${i}" value="${OFFER_CANVAS_DETAILS.offerAmount}">
											</td> 
											<td style="display:none">
												<input type="hidden" name="propertyid" id="commonid_${i}"/> 
												<input type="hidden" name="propertyname" value="Property" id="propertyname_${i}"/>  
											</td> 
											 <td style="width:1%;display:none;" class="opn_td" id="option_${i}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>   
										</tr>
									</c:when> 
									<c:otherwise>
										<div>
										<%
											OfferManagementTO offerTO=(OfferManagementTO)ServletActionContext.getRequest().getAttribute("OFFER_CANVAS_DETAILS");
											int i=0;	
											List<OfferManagementTO> componentList= new ArrayList<OfferManagementTO>();
											List<OfferManagementTO> unitList= new ArrayList<OfferManagementTO>();
											componentList=offerTO.getComponentList(); 
											unitList=offerTO.getUnitList();
											if(componentList!=null && !componentList.equals("")){
												for(OfferManagementTO component: componentList){
													out.println("<tr style=\"background:#FFFBFC;\" class=\"rowid\" id=\"fieldrow_"+ ++i+"\">"+
																"<td id=\"lineId_"+i+"\">"+i+"</td>"+
																"<td id=\"name_"+i+"\">"+component.getComponentName()+"</td>"+
																"<td id=\"type_"+i+"\">"+component.getComponentType()+"</td>"+
																"<td id=\"rent_"+i+"\">"+AIOSCommons.formatAmount(component.getComponentRent())+"</td>"+ 
																"<td id=\"offeramount_"+i+"\">"+
																	"<input type=\"text\" class=\"width30 validate[required]\" name=\"offeramount\" id=\"offerAmount_"+i+"\" value="+AIOSCommons.formatAmount(component.getOfferAmount())+">"+
																"</td>"+
																"<td style=\"display:none;\">"+
																	"<input type=\"hidden\" name=\"componentid\" id=\"commonid_"+i+"\" value="+component.getComponentId()+">"+
																	"<input type=\"hidden\" name=\"componentname\" id=\"componentname_"+i+"\" value=\"Component\">"+
																	"<input type=\"hidden\" name=\"offerdetailid\" id=\"offerDetailId_"+i+"\" value="+component.getOfferLineId()+">"+
																"</td>"+
																"<td style=\"width:1%;display:none;\" class=\"opn_td\" id=\"option_"+i+"\">"+
																	  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip addData\" id=\"AddImage_"+i+"\" style=\"cursor:pointer;\" title=\"Add Record\">"+
											 								"<span class=\"ui-icon ui-icon-plus\"></span>"+
																	  "</a>"+	
																	  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip editData\" id=\"EditImage_"+i+"\" style=\"display:none; cursor:pointer;\" title=\"Edit Record\">"+
																			"<span class=\"ui-icon ui-icon-wrench\"></span>"+
																	  "</a>"+
																	  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow\" id=\"DeleteImage_"+i+"\" style=\"cursor:pointer;\" title=\"Delete Record\">"+
																			"<span class=\"ui-icon ui-icon-circle-close\"></span>"+
																	  "</a>"+
																	  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip\" id=\"WorkingImage_"+i+"\" style=\"display:none;\" title=\"Working\">"+
																			"<span class=\"processing\"></span>"+
																	  "</a>"+
															"</td>"+  
															"</tr>");
												}
											}
											else if(unitList!=null && !unitList.equals("")){
												for(OfferManagementTO units: unitList){
													out.println("<tr style=\"background:#FFFEFB;\" class=\"rowid\" id=\"fieldrow_"+ ++i+"\">"+
															"<td id=\"lineId_"+i+"\">"+i+"</td>"+ 
															"<td id=\"name_"+i+"\">"+units.getUnitName()+"</td>"+
															"<td id=\"type_"+i+"\">"+units.getUnitType()+"</td>"+
															"<td id=\"rent_"+i+"\">"+AIOSCommons.formatAmount(units.getUnitRent())+"</td>"+ 
															"<td id=\"offeramount_"+i+"\">"+
																"<input type=\"text\" name=\"offeramount\" class=\"width30 validate[required]\" id=\"offerAmount_"+i+"\" value="+AIOSCommons.formatAmount(units.getOfferAmount())+">"+
															"</td>"+
															"<td style=\"display:none;\">"+
																"<input type=\"hidden\" name=\"unitid\" id=\"commonid_"+i+"\" value="+units.getUnitId()+">"+
																"<input type=\"hidden\" name=\"unitname\" id=\"unitname_"+i+"\" value=\"Unit\">"+
															"</td>"+
															"<td style=\"width:1%;display:none;\" class=\"opn_td\" id=\"option_"+i+"\">"+
																  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip addData\" id=\"AddImage_"+i+"\" style=\"cursor:pointer;\" title=\"Add Record\">"+
										 								"<span class=\"ui-icon ui-icon-plus\"></span>"+
																  "</a>"+	
																  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip editData\" id=\"EditImage_"+i+"\" style=\"display:none; cursor:pointer;\" title=\"Edit Record\">"+
																		"<span class=\"ui-icon ui-icon-wrench\"></span>"+
																  "</a>"+
																  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow\" id=\"DeleteImage_"+i+"\" style=\"cursor:pointer;\" title=\"Delete Record\">"+
																		"<span class=\"ui-icon ui-icon-circle-close\"></span>"+
																  "</a>"+
																  "<a class=\"btn_no_text del_btn ui-state-default ui-corner-all tooltip\" id=\"WorkingImage_"+i+"\" style=\"display:none;\" title=\"Working\">"+
																		"<span class=\"processing\"></span>"+
																  "</a>"+
														"</td>"+  
														"</tr>");
												}
											}
										%> 
										</div>
									</c:otherwise>
								</c:choose> 
					 </tbody>
				</table>
			</div> 
		</div> 
	</div> 
	</div>  
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div>  
		<div class="portlet-header ui-widget-header float-right save" id="${showPage}" style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	 </div>  
	 <div class="clearfix"></div>
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="offer-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="offer-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
  </form>
  </div> 
</div> 