<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-search_enquiry_details.action?buildingId="+buildingId+'&flatId ='+flatId+'& componentType ='+componentType+'& featureCode ='+featureCode+'&rent ='+rent+'&totalArea ='+totalArea+'& noOfRooms='+noOfRooms+'& toNoOfRooms='+toNoOfRooms+'& toRent='+toRent+'& toTotalArea='+toTotalArea, 
		 datatype: "json", 
		 colNames:['buildingId','<fmt:message key="re.property.info.buildingname"/>','flatId','<fmt:message key="re.property.info.flat"/>','componentType','<fmt:message key="re.property.info.componenttypename"/>','featureId','<fmt:message key="re.property.info.feautures"/>',
					'<fmt:message key="re.property.info.rent"/>',
					'<fmt:message key="re.property.info.totalarea"/>',
					'<fmt:message key="re.property.info.noofrooms"/>',
					 ], 
		 colModel:[ 
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME', width:100,  sortable:true},
				{name:'flatId',index:'PROP_FLAT_ID', width:100,  sortable:true},
				{name:'flatNumber',index:'FLAT_NUMBER', width:100,  sortable:true},
				{name:'componentType',index:'LOV_CODE_ID', width:100,  sortable:true},
				{name:'componentTypeName',index:'LOV_CODE', width:100,  sortable:true},
				{name:'featureCode',index:'FEATURE_CODE', width:100,  sortable:true},
				{name:'features',index:'LOV_MEANING', width:100,  sortable:true},
				{name:'rent',index:'RENT', width:75,  sortable:true},
				{name:'totalArea',index:'TOTAL_AREA', width:75,  sortable:true},
				{name:'noOfRooms',index:'NO_OF_ROOMS', width:75,  sortable:true},
			 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'flat.PROP_FLAT_ID',
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.enquiryinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["buildingId","componentType","flatId","featureCode",]);
});
 	</script>
 	 <body>
 	<input type="hidden" id="bankName"/> 
 	<table id="list2"></table>  
 	<div id="pager2"> </div> 
 	</body>
