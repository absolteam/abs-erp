<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
	<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  

	$('.addLines').click(function(){ 
		slidetab=$(this).parent().parent().get(0);  
		//slidetabrev=($($(slidetab).siblings().get(0)).children().children().children().get(0).tagName);
		
		//alert(slidetabrev);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_getproperty_component.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 $(result).insertAfter(slidetab);
			 $($($(slidetab).children().get(7)).children().get(0)).hide();	//Add Button
	         $($($(slidetab).children().get(7)).children().get(2)).hide(); //Delete Button
	         $($($(slidetab).children().get(7)).children().get(3)).show();	//Processing Button
			}
		});
	});

	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	 $('#birthdate').datepick({onSelect: customRange, showTrigger: '#calImg'});
	 $("#text_area_present_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_present_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 

	    $("#text_area_permanent_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar1").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar1').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar1").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_permanent_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_permanent_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    });  
		
});


//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
  var daysInMonth =$.datepick.daysInMonth(
  $('#selectedYear').val(), $('#selectedMonth').val());
  $('#selectedDay option:gt(27)').attr('disabled', false);
  $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
  if ($('#selectedDay').val() > daysInMonth) {
      $('#selectedDay').val(daysInMonth);
  }
} 
function customRange(dates) {
if (this.id == 'birthdate') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#endPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
  



<div id="main-content">
				 
				 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property Component</div>
					
					<fieldset>

						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend>Property Component 2</legend>						
		                            <div><label>From Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
									<div><label>To Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>																
									<div><label>Deposit<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter UAE Id No"></div>
									<div><label>Rent<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Email Id"></div>
									<div><label>Rent Status<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Email Id" readonly="readonly"></div>
									<div><label>Total Area<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Email Id"></div>
							
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend> Property Component 1</legend>
									<div><label>Id No.</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Building Id No.<span style="color:red">*</span></label>
									<select id="buildingId" class="width30 validate[required]" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.itemList ne null}">
										<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
											<option value="${building.buildingId }">${building.buildingName }</option>
										</c:forEach>
									</c:if>
								</select>
							</div>	
									<div><label>Floor No.<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>123</option>
											<option>420</option>
											</select>
									</div>	
									<div><label>No. of Rooms<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter UAE Id No"></div>					
								<div>
								<label>Component Type<span style="color:red">*</span></label>
								<select id="componentType" class="width30 validate[required]" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.lookupList ne null}">
										<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
											<option value="${compType.componentType }">${compType.componentTypeName }</option>
										</c:forEach>
									</c:if>
								</select>
								</div>
									<div><label>Status<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>Active</option>
											<option>Inactive</option>
											</select>
									</div>				
										<div id="hrm" class=" float-left" Style="*float:none!important;margin-bottom:10px;">
										<div><label class="width35">Image Upload</label><input type="file" class="width30" name="imageUpload" value="${bean.imageUpload}"></div>
						 			</div>							
						 </fieldset> 
							
							
						</div>
				
				</fieldset>
					<div id="main-content"> 
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:1063px;"> 
					<form name="newFields">
	 				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Feature Details</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
					<div style="display:none;" class="tempresult"></div>
			 		<input type="hidden" name="trnValue" id="trnValue" readonly="readonly"/> 
			 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
		 							<thead class="chrome_tab">
										<tr>  
			            					<th class="width10">Line No.</th>
			            					<th class="width20">Feature Code</th>
			            					<th class="width20">Features</th>
			            					<th class="width10">Measurement</th>
			            					<th class="width10">Remarks</th>
			            					<th style="width:5%;"><fmt:message key="accounts.currencymaster.label.options"/></th>
										</tr>
									</thead>  
									<tbody class="tab" style="">	
										 <%for(int i=0;i<=2;i++) { %>
											 <tr>  
												<td class="width10"></td>	 
												<td class="width20"></td>
												<td class="width20"></td>	
												<td class="width10"></td> 
												<td class="width10"></td>
												<td style="display:none"> 
													<input type="hidden" name="tempId" value="" id="tempId"/>
													<input type="hidden" name="curIdVal" value="" id="curIdVal"/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addLines" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>		 
											</tr>  
								  		 <%}%>
									</tbody>
		 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    	</div>
							
						</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
		<div class="portlet-header ui-widget-header float-right cancelrec" style=" cursor:pointer;"><fmt:message key="accounts.currencymaster.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;"><fmt:message key="accounts.currencymaster.button.save"/></div>
	
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;">Add Row</div> 
	</div>
	</div>
	
</div>
			
				
