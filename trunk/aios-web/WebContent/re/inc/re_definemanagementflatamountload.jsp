<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
 <fieldset>
	<div>
		<label class="width30"><fmt:message key="re.offer.rentamount"/></label>
		<input type="text" name="rent" class="width30" id="rent" value="${item.rent }" disabled="disabled">
	</div>		
	<div>
		<label class="width30"><fmt:message key="re.property.info.depositamount"/></label>
		<input type="text" name="deposit" class="width30" id="deposit" value="${item.deposit }" disabled="disabled">
	</div>
</fieldset> 
 