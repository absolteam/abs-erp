<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="releaseInfoPaymentAddEdit" id="releaseInfoPaymentAddEdit">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.chequedetails"/></legend>
		<div>
			<label><fmt:message key="re.property.info.chequeno"/><span style="color:red">*</span></label>
			<input type="text" name="chequeNumber" id="chequeNumber" class="width50  validate[required]">
		</div>
		<div>
			<label><fmt:message key="re.property.info.chequeamount"/><span style="color:red">*</span></label>
			<input type="text" name="chequeAmount" id="chequeAmount" class="width50  validate[required,custom[onlyFloat]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
	<fieldset>
		<legend><fmt:message key="re.property.info.linesdetails"/></legend> 
		<div>
				<label><fmt:message key="re.property.info.bankname"/><span style="color:red">*</span></label>
				<input type="text" name="bankName" id="bankName" class="width50 validate[required]" />
			</div>
			<div>
				<label><fmt:message key="re.property.info.chequedate"/><span style="color:red">*</span></label>
				<input type="text" name="chequeDate" id="defaultActualPicker" class="width50 validate[required]" />
			</div>
	</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#contractAgreementPaymentAddEdit").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing button 		
		 });
		 $('#add').click(function(){
			  if($('#releaseInfoPaymentAddEdit').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var bankName=$('#bankName').val();
					var chequeDate=$('#defaultActualPicker').val();
					var chequeNumber=$('#chequeNumber').val();
					var chequeAmount=$('#chequeAmount').val();
									 
					var trnVal =$('#transURN').val(); 

					var releaseId=$('#releaseId').val();
					var actualLineId=$($($(slidetab).children().get(4)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_edit_release_payment_update.action", 
					 	async: false,
					 	data: {releaseId: releaseId, bankName: bankName,chequeDate: chequeDate,chequeNumber: chequeNumber,chequeAmount: chequeAmount, 
							trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(bankName);
		        		$($(slidetab).children().get(1)).text(chequeDate);
		        		$($(slidetab).children().get(2)).text(chequeNumber); 
		        		$($(slidetab).children().get(3)).text(chequeAmount); 
		        		
		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button
	
						$($($(slidetab).children().get(4)).children().get(0)).val($('#objLineVal').html());
				  		$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempId').html());
				  		$("#transURN").val($('#objTrnVal').html());

				  		$.fn.globeTotalCheque();  //For Total Amount of Payment
				   } 
			  }else{
				  return false;
			  }
		});

		// Default Date
			$('#defaultActualPicker').datepick();
		
});
 </script>	