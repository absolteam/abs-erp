<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:forEach items="${requestScope.itemList}" var="result" varStatus="status">
	<tr class="rowid">  	 
		<td class="width10">${status.index+1 }</td>
		<td class="width10">${result.maintainenceCode}</td>	
		<td class="width20"></td>
		<td class="width10 amountcalc"></td> 
		<td class="width20"></td>
		<td style="display:none"> 
			<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
		</td> 
		<td style=" width:5%;"> 
					<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addflat" style="cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
		   	</a>	
		   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFlatNew"  style="display:none;cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a  style="display:none;cursor:pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFlat" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
				<span class="processing"></span>
			</a>
		</td>  
		<td style="display:none;" class="release_type"></td>
		<td style="display:none;"></td>	 
	</tr>  
</c:forEach>	
<script type="text/javascript">
//Adding Feature Details
	$('.addflat').click(function(){ 
		if(openFlag==0){
		if($('#flatMaintainenceReleaseAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	       //alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_flat_maintainence_redirect.action", 
				async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				
				 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				 
				 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
		         openFlag=1;
		         $('#loading').fadeOut();
				}
			});
		}else{
			$('#warningMsg').hide().html("Please insert record ").slideDown();
		}
		}	
});

$(".editFlatNew").click(function(){
	if(openFlag==0){
		$('#loading').fadeIn();
		slidetab=$(this).parent().parent().get(0); 
		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();

	    actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
		if(actualID != null && actualID !='' && actualID!=undefined){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_edit_flat_maintainence_redirect.action",  
			async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 $(result).insertAfter(slidetab);
			 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
	         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
	         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button

	         openFlag=1;
				
			 
	         var temp1= $($(slidetab).children().get(0)).text();
			 var temp2= $($(slidetab).children().get(1)).text();
			 var temp3= $($(slidetab).children().get(2)).text();
			 var temp4= $($(slidetab).children().get(3)).text();
			 //var temp4= $($(slidetab).children().get(4)).text();
			 var temp5= $($(slidetab).children().get(4)).text();

			 //var temp7= $($(slidetab).children().get(8)).text();
			 var temp6= $($(slidetab).children().get(8)).text();

			 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');

			$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
			$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
			$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp6);
			
		  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
		  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
		  	//$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp6);
		
		  	$('#loading').fadeOut();
		
		  		}
			});
		}else{
			$('#warningMsg').hide().html("Please insert record ").slideDown();
		}
	}	
});

$.fn.globeTotal = function() { 	// Total for Amount Calculation
	var total=0;
	$('.amountcalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#amountTotal').val(total);
 }	

	$('.delFlat').click(function(){
		if(openFlag==0){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(6)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_flat_maintainence_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
		}	
	});
</script>