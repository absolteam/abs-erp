 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <script type="text/javascript"> 
 var tempid;
 var slidetab;
 var shockaction="";
 
 $(function(){
	 $('#assetNumber').attr("disabled",true);
	 $('#assetAddress').attr("disabled",true);
	 $('#assetDesc').attr("disabled",true);

	 $('#tenantName').attr("disabled",true);
	 $('#addA').attr("disabled",true);
	 $('#addB').attr("disabled",true);
	 $('#addC').attr("disabled",true);
	 $('#addD').attr("disabled",true);
	 $('#addE').attr("disabled",true);
	 
	 $("#addTenantValidate").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 });

	 if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
			$('#themeRollerSelect option:not(:selected)').remove();
		}

		$('#startPicker,#endPicker').datepick({
			 onSelect: customRange, showTrigger: '#calImg'});

	 $('.asset-popup').click(function(){ 
	       tempid=$(this).parent().get(0);  
			$('#asset-popup').dialog('open'); 
			shockaction=$($(tempid).siblings().get(1)).attr('id');
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/ta_tenantagreement_"+shockaction+".action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.asset-result').html(result); 
				},
				error:function(result){ 
					 $('.asset-result').html(result); 
				}
			}); 
	});
		
	 $('#asset-popup').dialog({ 
			autoOpen: false,
			minwidth: 'auto',
			width:500,
			bgiframe: false,
			modal: false,
			buttons: {
				"Ok": function() {
		 			$(this).dialog("close"); 
				}, 
				"Cancel": function() {  
					$(this).dialog("close"); 
				} 
			}	
		}); 

	 $('#agreementDate').datepick({showTrigger: '#calImg'});
	 $('#firstRentDate').datepick({showTrigger: '#calImg'});

	 $('#save').click(function(){
		 var assetId=$('#assetId').val();
		 var startDate =$('.startDate ').val();
		 var endDate =$('.endDate ').val();
		 var agreementDate=$('#agreementDate').val();
		 var agreementNumber=$('#agreementNumber').val();

		 var leadDays=$('#leadDay').val();
		 var firstRentDate=$('#firstRentDate').val();
		 var payementFrequency=$('#payementFrequency').val();

		 var currencyName=$('#currencyCode :selected').text();
		 var amount=$('#rentAmount').val();
		 var rentType=$('#rentType').val();
		 var exchangeRate=$('#exchangeRate').val();
		 var functionalAmount=$('#fnAmount').val();
		 var ledgerId=$('#ledgerId').val();

		 var tenantId=$('#tenantId').val();
		 var tenantBillingId=1;


		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/ta_tenantagreement_addsave.action", 
			 	async: false,
			 	data: {assetId:assetId, startDate:startDate, endDate:endDate, agreementDate:agreementDate, agreementNumber:agreementNumber,
					   leadDays:leadDays, firstRentDate:firstRentDate, payementFrequency:payementFrequency, currencyName:currencyName, 
					   amount:amount, rentType:rentType, exchangeRate:exchangeRate, functionalAmount:functionalAmount, tenantId:tenantId, tenantBillingId:tenantBillingId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide(); 
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html();
					 
				     if(tempsMsg!=""){
				    	$('#sMsg').show(); 
				    	$('#eMsg').hide();}
				    	else{ 
					    	$('#eMsg').show(); 
					    	$('#sMsg').hide();
				    	}  
			 	},  
			 	error:function(result){  
                 $("#main-wrapper").html($('#eMsg').html()); 
                 return false;
			 	} 
      	}); 
		
	 });
	 
 });
//Prevent selection of invalid dates through the select controls
 function checkLinkedDays() {
     var daysInMonth =$.datepick.daysInMonth(
                 $('#selectedYear').val(), $('#selectedMonth').val());
     $('#selectedDay option:gt(27)').attr('disabled', false);
     $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
     if ($('#selectedDay').val() > daysInMonth) {
         $('#selectedDay').val(daysInMonth);
     }
 } 
 function customRange(dates) {
 if (this.id == 'startPicker') {
 $('#endPicker').datepick('option', 'minDate', dates[0] || null);
 }
 else {
 	$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
 	}
 }
 </script>

<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Agreement</div>
             
        <div class="portlet-content">
        	<form name="addtenant" id="addTenantValidate">
        		<div class="tempresult" style="display:none"></div>
        		<div class="width100 float-left" id="hrm">  
					<div class="width30 float-right">
						<label class="width45" for="agreementNumber">Agreement Number</label>
						<input type="text" id="agreementNumber" class="width40" name="agreementNumber"/ >
					</div>
					<div class="width60 float-left"> 
						<label class="width20" for="assetNumber">Asset Number</label>
						<input type="hidden" id="assetId" name="assetId"> 
						<input type="text" id="assetNumber" class="width20" name="assetNumber"> 
						<span id="" class="button" style="margin-top:3px;">
							<a href="#" class="btn ui-state-default ui-corner-all asset-popup width100"> 
								<span class="ui-icon ui-icon-newwin"></span> 
							</a>
						</span>
				</div>  
				</div>
				
				<div class="width100 float-left" id="hrm">  
					<div class="width30 float-right">
						<label class="width45">Agreement Date</label>
						<input type="text" id="agreementDate" class="width40" name="agreementDate">
					</div>  
					<div class="width60 float-left">
							<label class="width20" for="assetAddress">Asset Address</label>
							<input type="text" id="assetAddress" class="width70" name="assetAddress">
				   </div> 
				</div>
				
				<div class="width100 float-left" id="hrm"> 
						<div class="width30 float-right">
							<label class="width45" for="agreementStatus">Agreement Status</label>
							<select name="agreementStatus" id="agreementStatus" class="40">
								<option value="">--Select--</option>
								<option value="">Approved</option>
								<option value="">Awaiting</option>
								<option value="">Canceled</option>
							</select>
						</div> 
						
						<div class="width60 float-left">
							<label class="width20" for="assetDesc">Asset Description</label>
							<input type="text" id="assetDesc" class="width70" name="assetDesc">
						</div> 
				</div>
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
						<div class="width30 float-left">
							<label class="width40" for="startPicker">Agreement Valid From</label>
							<input type="text" id="startPicker" class="width40 startDate" name="startDate">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="endDate">Agreement Valid To</label>
							<input type="text" id="endPicker" class="width40 endDate" name="endDate">
						</div>  
				</div>
				
				<div style="margin-bottom:10px;" id="hrm" class="width100 float-left"> 
					<fieldset>
						<legend>Rent Information</legend> 
							<div class="width30 float-left">
								<label class="width50" for="firstRentDate">First Rent Date</label>
								<input type="text" id="firstRentDate" class="width30" name="firstRentDate">
							</div>
							
							<div class="width30 float-left">
								<label class="width60" for="payementFrequency">Payment Frequency</label>
								<select name="payementFrequency" class="width30" id="payementFrequency">
									<option value="">--Select--</option>
									<c:if test="${pyList ne null }">
										<c:forEach items="${pyList}" var="bean" >
											<option value="${bean.paymentId}">${bean.paymentCode}</option>
										</c:forEach>
									</c:if>		 
								</select>
							</div>
							
							<div class="width30 float-left">
								<label class="width60" for="leadDay">Invoice Creation lead day</label>
								<input type="text" id="leadDay" class="width30" name="leadDay">
							</div> 
					</fieldset>
				</div> 
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
					<fieldset>
					<legend>Currency Information</legend>
					<div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="currencyCode">Currency</label>
								<select name="currencyCode" class="width30" id="currencyCode">
									<option value="">--Select--</option>
									<c:if test="${curList ne null }">
										<c:forEach items="${curList}" var="bean" >
											<option value="${bean.currencyId}">${bean.currencyName}</option>
										</c:forEach>
									</c:if>		 
								</select>
						</div>
						<div class="width30 float-left">
							<label class="width40" for="rentAmount">Rent</label>
							<input type="text" id="rentAmount" class="width30" name="rentAmount">
						</div> 
						
						<div class="width30 float-left">
							<label class="width40 float-left" for="">Rate Type</label>
							 <select name="rentType" class="width30" id="rentType">
									<option value="">--Select--</option>
									<option value="T">Table</option>
									<option value="U">User</option> 
								</select>
						</div>
				     </div>	 
				     <div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="exchangeRate">Conversion Rate</label>
							<input type="text" id="exchangeRate" class="width30" name="exchangeRate">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="fnAmount">Functional Amount</label>
							<input type="text" id="fnAmount" class="width30" name="fnAmount">
						</div>
					</div>	 
					</fieldset>
				</div>
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
					<fieldset>
					<legend>Tenant Information</legend>
					<div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="tenantName">Tenant Name</label>
							<input type="hidden" id="tenantId" name="tenantId"> 
							<input type="text" id="tenantName" class="width40" name="tenantName"> 
							<span id="" class="button" style="margin-top:3px;">
								<a href="#" class="btn ui-state-default ui-corner-all asset-popup width100"> 
									<span class="ui-icon ui-icon-newwin"></span> 
								</a>
							</span>
						</div> 
						<div class="width30 float-left">
							<label class="width40">Tenant Address</label>
							<input type="text" id="addA" class="width50" name="">
						</div> 
						
						 <div class="width30 float-left">
							<label class="width40">Address1</label>
							<input type="text" id="addB" class="width50" name="">
						</div> 
				     </div>	 
				     <div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40">Place</label>
							<input type="text" id="addC" class="width30" name="">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="">Country</label>
							<input type="text" id="addD" class="width30" name="">
						</div>
						
						<div class="width30 float-left">
							<label class="width40">Pin</label>
							<input type="text" id="addE" class="width30" name="">
						</div>
					</div> 
					</fieldset>
				</div> 
				<input type="hidden" name="ledgerId" id="ledgerId" value="10"/>
				<div class="clearfix"></div>  
				 
	       		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
	       		    <div class="portlet-header ui-widget-header float-right" id="generateSchedule">GENERATE SCHEDULE</div> 
					<div class="portlet-header ui-widget-header float-right" id="cancel">CANCEL</div> 
					<div class="portlet-header ui-widget-header float-right" id="approve">APPROVE</div> 
				 	<div class="portlet-header ui-widget-header float-right" id="save">SAVE</div> 
				</div>
            </form>
        </div>
    </div>
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="asset-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
<div class="asset-result width100"></div>
<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
