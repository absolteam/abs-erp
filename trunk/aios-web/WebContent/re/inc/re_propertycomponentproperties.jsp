 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
 <style type="text/css">
#component-popup{
	overflow: hidden;
}
#example1{
	cursor:pointer;
}
</style>
<script type="text/javascript">
var id;var oTable; var selectRow=""; var aData ="";
var aSelected = []; var componentName="";
var propertyName=""; var propertyId="";
$(function(){ 
	$('.formError').remove();   
	$('#example1').dataTable({ 
		"sAjaxSource": "component_propertyjsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [ 
			{ "sTitle": "PROPERTY_ID",  "bVisible": false}, 
			{ "sTitle": "Property Name"}, 
		], 
		"sScrollY": $("#component-popup").height() - 160,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#example1').dataTable(); 
	
	$('#example1 tbody tr').live('dblclick', function(){  
		 if($(this).hasClass('row_selected')){ 
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          propertyId=aData[0]; 
	          propertyName=aData[1]; 
	      }
	      else{ 
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          propertyId=aData[0]; 
	          propertyName=aData[1]; 
	      } 
		 $('#propertyId').val(propertyId);
		 $('#propertyName').val(propertyName); 
		 $('#component-popup').dialog("close"); 
	});
});
</script> 
<div id="main-content">
		<div id="" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property</div>	 
			<div class="portlet-content"> 
				 <div id="component_type">
					<table class="display" id="example1"></table>
				 </div>  
			</div>
		</div>
</div>