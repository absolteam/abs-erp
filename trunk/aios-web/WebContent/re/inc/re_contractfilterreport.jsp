<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
</style>
</head>
<script type="text/javascript">
var id;
var contractNumber="";
var buildingId="";
var contractDate="";
var grandTotal="";
var approvedStatus="";
var oTable; var selectRow=""; var aData="";var aSelected = [];var s1=0; 

$(function(){ 
	
	$('.formError').remove(); 
 	$('#contract').dataTable({ 
 		"sAjaxSource": "json_contract_detail_list.action?fromDate="+fromDate+"&toDate="+toDate+"&tenantName="+tenantName+"&unitId="+unitId,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 		"aoColumns": [
 			{ "sTitle": 'Contract ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
 			{ "sTitle": '<fmt:message key="re.property.unit"/>',"sWidth": "200px"},
 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
 			{ "sTitle": 'Options',"sWidth": "100px", "bVisible": false},
 			
 		],
 		"sScrollY": $("#main-content").height() - 315,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#contract').dataTable();

 	/* Click event handler */
 	$('#contract tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s1=aData[0]; 
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s1=aData[0];
 	    }
 	}); 
 	
 	
 	$('#contract tbody tr').live('dblclick', function () {
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData =oTable.fnGetData( this );
	 	        s1=aData[0];
	 	       callReportView();
		       return false;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	 	      s1=aData[0];
	 	      callReportView();
		      return false;
	      }
	});
 	

	$('#view').click(function(){ 
		callReportView();
		return false;
	}); 
	
	
	

	

	//Default Date Picker
	$('#defaultPopup').datepick();

	
	
	$('.contract_print').live('click',function(){
		var parentTR=$(this).parent().parent().get(0);
		var contractId=getId(parentTR); 
		$('#loading').fadeIn();
		window.open('getPropertyContractDetail.action?recordId='
				+ contractId + '&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
	});

	//Date configuration
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	

	});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	   var daysInMonth =$.datepick.daysInMonth(
	   $('#selectedYear').val(), $('#selectedMonth').val());
	   $('#selectedDay option:gt(27)').attr('disabled', false);
	   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   if ($('#selectedDay').val() > daysInMonth) {
	       $('#selectedDay').val(daysInMonth);
	   }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function getId(parentTR){ 
		if ($(parentTR).hasClass('row_selected') ) { 
			$(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        s1=aData[0]; 
	    }
	    else {
	        oTable.$('tr.row_selected').removeClass('row_selected');
	        $(parentTR).addClass('row_selected');
	        aData =oTable.fnGetData( parentTR );
	        s1=aData[0];
	    }
		return s1;
	}	
	function callReportView(){
		$('#loading').fadeIn();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getPropertyContractDetail.action", 
			data:{format:"HTML",recordId:s1,approvalFlag:"N"},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#reporting-tree").html(result); 
				$("table:first").addClass("table-custom-design-report");
				$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
				$("#reporting-tree").append("<div style='margin-top:-40px;' class='portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons'>"+
				"<div class='portlet-header ui-widget-header float-left'><a class='' style='cursor: pointer;' onclick='reportAjaxCallFunction(this);' id='getcontract_reports'>Back</a></div></div>");
				$('#loading').fadeOut();
				return false;
			}
	 	});
	}
</script>
<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.searchresult"/></div>	 
<div id="rightclickarea">
	<div id="gridDiv">
		<table class="display dataTable" id="contract"></table>
	</div>
</div>	
<div class="vmenu">
	<div class="first_li"><span id="view">View</span></div>
	<div class="sep_li"></div>		 
       	<div class="first_li"><span class="print-call">Print</span></div>
	<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
	<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
 </div> 