<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <c:if test="${flatList ne null}">
	 <c:choose>
		<c:when test="${requestScope.flatList ne null}">
			<c:forEach items="${requestScope.flatList}" var="contract" varStatus="status">
				<option value="${contract.contractId }">${contract.contractNumber }</option>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<option value="">--NIL--</option>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${flatList eq null}">
	<option value="">--NIL--</option>
</c:if>