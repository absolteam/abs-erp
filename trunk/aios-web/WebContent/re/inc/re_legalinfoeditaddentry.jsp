<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="12" class="tdidentity">
<div id="errMsg"></div>
<form name="legalInfoFeatureAddEntry" id="legalInfoFeatureAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.sessionstatus"/></legend>
		<div>
			<label><fmt:message key="re.property.info.expenses"/><span class="mandatory">*</span></label>
			<input type="text" name="expenses" id="expenses" class="width50  validate[required,custom[onlyFloat]]">
		</div>
		<div>
			<label><fmt:message key="re.property.info.sessionresult"/><span class="mandatory">*</span></label>
			<input type="text" name="sessionResult" id="sessionResult" class="width50  validate[required]">
		</div>
		<div>
			<label><fmt:message key="re.property.info.status"/><span class="mandatory">*</span></label>
			<select id="sessionStatus" class="width50  validate[required]">
				<option value="">--Select--</option>
				<c:forEach items="${requestScope.decisionList}" var="decisionstatus" varStatus="status">
					<option value="${decisionstatus.sessionStatusCode }">${decisionstatus.sessionStatus}</option>
				</c:forEach>
			</select>
		</div>
		<div>
			<label><fmt:message key="re.property.info.todate"/></label>
			<input type="text" name="sessionToDate" id="sessionToDate" readonly="readonly" class="width50">
		</div>
		<div>
			<label><fmt:message key="re.property.info.description"/></label>
			<input type="text" name="statusDescription" id="statusDescription" class="width50">
		</div>
		<div style="display:none;">
			<label><fmt:message key="re.property.info.description"/></label>
			<input type="hidden" id="URNdms" value="0" disabled="disabled"/>
		</div>
		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			<div  id="dms_document_information" class="portlet-header ui-widget-header float-right"><fmt:message key="cs.scan.upload.upload"/></div>
        </div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.featuredetails"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.sessionno"/><span class="mandatory">*</span></label>
				<input type="text" name="sessionNumber" id="sessionNumber" class="width50 validate[required]" />
			</div>
			<div>
				<label><fmt:message key="re.property.info.sessiondate"/><span class="mandatory">*</span></label>
				<input type="text" name="sessionDate" id="sessionDate" readonly="readonly" class="width50 validate[required]" />
			</div>
			<div>
				<label><fmt:message key="re.property.info.courtname"/><span class="mandatory">*</span></label>
				<select id="courtName" class="width50  validate[required]">
					<option value="">--Select--</option>
					<c:forEach items="${requestScope.courtList}" var="court" varStatus="status">
						<option value="${court.courtNameCode }">${court.courtName}</option>
					</c:forEach>
				</select>
			</div>
			<div>
				<label><fmt:message key="re.property.info.lawyername"/><span class="mandatory">*</span></label>
				<input name="lawyerName" id="lawyerName" class="width50 validate[required]">
			</div>
			<div>
				<label><fmt:message key="re.property.info.submitteddocument"/></label>
				<input type="text" name="submittedDoc" id="submittedDoc" class="width50" />
			</div>
			<div>
				<label><fmt:message key="re.property.info.receiveddocument"/></label>
				<input name="receivedDoc" id="receivedDoc" class="width50">
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#legalInfoFeatureAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			  $($(this).parent().parent().parent().get(0)).remove();
				$($($(slidetab).children().get(12)).children().get(0)).show();	//Add Button
				$($($(slidetab).children().get(12)).children().get(2)).show(); 	//Delete Button
				$($($(slidetab).children().get(12)).children().get(3)).hide();	//Processing Button		
				openFlag=0;
		 });
		 $('#add').click(function(){
			  if($('#legalInfoFeatureAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var sessionNumber=$('#sessionNumber').val();
					var sessionDate=$('#sessionDate').val();
					var lawyerName=$('#lawyerName').val();
					var submittedDoc=$('#submittedDoc').val();
					var remarks=$('#remarks').val();
					var receivedDoc=$('#receivedDoc').val();
					var expenses=$('#expenses').val();
					var sessionResult=$('#sessionResult').val();
					var sessionStatus=$('#sessionStatus').val();
					var sessionStatusName=$('#sessionStatus :selected').text();
					var statusDescription=$('#statusDescription').val();
					var courtNameCode=$('#courtName').val();
					var courtName=$('#courtName :selected').text();
					var sessionToDate=$('#sessionToDate').val();
					var URNdms=$('#URNdms').val();
					
					var trnVal =$('#transURN').val(); 
					var caseId=$('#caseId').val();  	// Master Primary Key
					var actionFlag=$($($(slidetab).children().get(11)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(11)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_legal_info_save.action", 
					 	async: false,
					 	data: {sessionNumber: sessionNumber,sessionDate: sessionDate,lawyerName: lawyerName,submittedDoc: submittedDoc, 
							receivedDoc: receivedDoc,expenses: expenses,sessionResult: sessionResult,sessionStatus:sessionStatus,
							statusDescription: statusDescription,courtNameCode: courtNameCode,sessionToDate: sessionToDate,dmsURN: URNdms,
							trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag,caseId:caseId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(sessionNumber);
		        		$($(slidetab).children().get(1)).text(sessionDate);
		        		$($(slidetab).children().get(2)).text(courtName);
		        		$($(slidetab).children().get(3)).text(lawyerName); 
		        		$($(slidetab).children().get(4)).text(submittedDoc); 
		        		$($(slidetab).children().get(5)).text(receivedDoc);
		        		$($(slidetab).children().get(6)).text(expenses); 
		        		$($(slidetab).children().get(7)).text(sessionResult);
		        		$($(slidetab).children().get(8)).text(sessionStatusName);
		        		$($(slidetab).children().get(9)).text(sessionToDate);
		        		$($(slidetab).children().get(10)).text(statusDescription);

		        		$($(slidetab).children().get(13)).text(sessionStatus);
		        		$($(slidetab).children().get(14)).text(courtNameCode);
		        		$($(slidetab).children().get(15)).text(URNdms);
		        		
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(12)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(12)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(12)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(11)).children().get(2)).val($('#objTempVal').html()); 
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCount=Number($('#childCount').val());
				  		childCount=childCount+1;
						$('#childCount').val(childCount);
						$.fn.globeTotalExpenses();	//Total Expenses calculation
				   } 
			  }else{
				  return false;
			  }
		});

		 $('#sessionStatus').change(function(){
				sessionStatus=$('#sessionStatus').val();
				if(sessionStatus == '3' || sessionStatus == '4'){
					$('#sessionToDate').val("");
					$('#sessionToDate').attr('disabled','disabled');
				}else{
					$('#sessionToDate').removeAttr('disabled');
				}
			});
			
		$('#sessionDate').datepick();
		$('#sessionToDate').datepick();

		var dmsUserId='${emp_CODE}';
		var dmsPersonId='${personId}';
		var companyId=Number($('#headerCompanyId').val());
		var applicationId=Number($('#headerApplicationId').val());
		var functionId=Number($('#headerCategoryId').val());
		var functionType="list,upload";//edit,delete,list,upload
		//$('#dmstabs-1, #dmstabs-2').tabs();
		//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
		$('#dms_create_document').click(function(){
			var dmsURN=$('#URNdms').val();
				loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
				//$('#tabs').tabs({selected:0});// switch to first tab
		});
		$('#dms_document_information').click(function(){
			var dmsURN=$('#URNdms').val();
			loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
			//$('#tabs').tabs({selected:1});// switch to second tab
		});

});
function loadTB(title,url,imgPath){
	tb_show(title,url,imgPath);
}
 </script>	