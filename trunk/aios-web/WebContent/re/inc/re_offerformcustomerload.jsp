<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <div class="selectable width100" id="hrm"> 
     <div class="float-left width100" style="background-color:#dfdfdf;padding:5px;">
     	<div class="width50 float-left"><span>Customer Name</span></div> 
     	<div class="width10 float-left" style="display:none;"><span>Mobile No</span></div>
     	<div class="width50 float-left"><span>Email ID</span></div>
     	<div class="width20 float-left" style="display:none;"><span>County</span></div>
     	<div class="width20 float-left" style="display:none;"><span>State Name</span></div>
     	<div class="width20 float-left" style="display:none;"><span>City Name</span></div>
     </div> 
     <c:choose>
	     <c:when test="${itemList ne null}">
	 		<c:forEach items="${itemList}" var="item" varStatus="status"> 
			 	 <div class="selectlist float-left width100">   
			 	 	 <input type="hidden" value="${item.customerId}"/>
			 	 	 <input type="hidden" value="${item.presentAddress}"/>
			 	 	 <input type="hidden" value="${item.landLineNo}"/>
			 	 	 <div class="width50 float-left" style="padding:1% 0  1% 0 ;"><span>${item.customerName}</span></div>
			 	 	 <div class="width10 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.mobileNo}</span></div>
			 	 	 <div class="width50 float-left" style="padding:1% 0  1% 0;"><span>${item.emailId}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.countryName}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.stateNames}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.cityNames}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.customerTypeId}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.offerValidityDays}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.customerTypeName}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.rentFees}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.rentAmount}</span></div>
		         </div>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<div class="width50 float-left" style="padding:1% 0  1% 0;"><span>No Records Available</span></div>
		</c:otherwise>
	</c:choose>
</div>
<script type="text/javascript">
  $(function(){
	  $(".ui-dialog-titlebar").remove();
	   $(".selectlist").click(function(){
            $(temp).css('background-color','#fff');
            $(this).css('background-color','#F39814');
            temp=this;
        });
	   $(".selectlist").mousedown(function(){
            $(this).css('background-color','#FECA40');
        });
	    $('.selectlist').click(function(){
	       $('#customerId').val($($(this).children().get(0)).val());
	       $('#address').val($($(this).children().get(1)).val());
	       $('#landLineNo').val($($(this).children().get(2)).val());
	       $('#customerName').val($($($(this).children().get(3)).children().get(0)).html());
	       $('#mobileNo').val($($($(this).children().get(4)).children().get(0)).html());
	       $('#emailId').val($($($(this).children().get(5)).children().get(0)).html());
	       $('#country').val($($($(this).children().get(6)).children().get(0)).html());
	       $('#state').val($($($(this).children().get(7)).children().get(0)).html());
	       $('#city').val($($($(this).children().get(8)).children().get(0)).html());
	       $('#customerType').val($($($(this).children().get(11)).children().get(0)).html());
	       $('#customerTypeId').val($($($(this).children().get(9)).children().get(0)).html());
	       $('#offerValidityDays').val($($($(this).children().get(10)).children().get(0)).html());
	       $('#rentFees').val($($($(this).children().get(12)).children().get(0)).html());
	       $('#rentFeesAmount').val($($($(this).children().get(13)).children().get(0)).html());
	       
           // $($(tempid).siblings().get(2)).val($($(this).children().get(1)).html());
           
    });  
}); 
</script>