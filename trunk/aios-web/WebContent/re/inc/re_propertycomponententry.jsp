<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>  
<style>
.opn_td{
	width:6%;
}
.ui-button{
right:42px;
}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var featureRowsAdded = 0;
var compRent;
$(function(){

	if($('#status').val()=="true"){
		$('#status').attr('checked',true);
		$('#statusCombo').val(1);
	}
	else{
		$('#status').attr('checked',false);
		$('#statusCombo').val(0);
	}
	var tempvar=$('.tab>tr:last').attr('id'); 
	var idval=tempvar.split("_");
	var rowid=Number(idval[1]); 
	$('#DeleteImage_'+rowid).hide(); 
	
	$('#unitSize').val($('#tempunitSize').val());
	
	
	 $jquery("#componentEntryValidation").validationEngine('attach');


	$('#statusCombo').val($('#status').val());

	compRent = convertToAmount("${COMPONENT_INFO.rent}");
	$("#rent").val(compRent);
	
	$('.component-popup').click(function(){ 
       tempid=$(this).parent().get(0);  
	   $('.ui-dialog-titlebar').remove();
	  	var urlaction=$($(tempid).siblings().get(1)).attr('id').trim().toLowerCase();  
	  	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/property_component_"+urlaction+".action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.component-result').html(result);  
			},
			error:function(result){  
				 $('.component-result').html(result); 
			}
		});  
	});
	 $('#component-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:500, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true
	});
	 
	 
	 $('#feature-type-add').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
         var accessCode="COMPONENT_FEATURE"; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:600,
		height:350,
		bgiframe: false,
		modal: true 
	});
	 
	$('#componentSize').blur(function(){
		var propSize = $('#propertySize').val();
		var compSize = $('#componentSize').val();
		if (propSize != null && propSize != "") {
			if (parseFloat(compSize) > parseFloat(propSize)) {
				$('#componentSizeError').val('1');
				$('#compSizeErrorMsg').html('Component size is greator than Property size, please correct it or it will not be saved.').show();
			} else {
				$('#componentSizeError').val('0');
				$('#compSizeErrorMsg').hide();
			}
		} else {
			$('#componentSizeError').val('0');
			$('#compSizeErrorMsg').hide();
		}
	});

	 $('.addData').click(function(){
		if($jquery("#componentEntryValidation").validationEngine('validate') && $('#componentSizeError').val() == '0'){ 
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var showPage="";
			 if(componentId !=null && componentId!="" && componentId>0)
				 showPage="editadd";
			 else
				 showPage="addadd";
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).hide();
			 $("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_compoment_addlineentry.action",
				data:{id:rowid, showPage:showPage},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
				 	$('#lineNumber').val($('#lineId_'+rowid).text());
				}
			 });
		}
		else{
			return false;
		}
		 return false;
	 });

	 $('.editData').click(function(){
			if($jquery("#componentEntryValidation").validationEngine('validate') && $('#componentSizeError').val() == '0'){   
				 $('.error').hide(); 
				 slidetab=$(this).parent().parent().get(0);   
				 var tempvar=$(slidetab).attr("id");  
				 var idarray = tempvar.split('_');
				 var rowid=Number(idarray[1]);  
				 var componentId=$('#componentId').val();
				 var showPage="";
				 if(componentId !=null && componentId!="" && componentId>0)
					 showPage="editedit";
				 else
					 showPage="addedit";
				 $("#AddImage_"+rowid).hide();
				 $("#EditImage_"+rowid).hide();
				 $("#DeleteImage_"+rowid).hide();
				 $("#WorkingImage_"+rowid).show(); 
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_compoment_addlineentry.action",
					data:{id:rowid,showPage:showPage},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 	$(result).insertAfter(slidetab); 
					 	$('#lineNumber').val($('#lineId_'+rowid).text());  
		        		$('#componentFeature').val($('#componentfeatureCode_'+rowid).text());  
		        		$('#componentFeatureId').val($('#componentfeatureid_'+rowid).val()); 
		        		$('#componentFeatures').val($('#componentfeature_'+rowid).text());  
		        		$('#componentFeatureDetail').val($('#componentfeatureDetails_'+rowid).text());
		        		$('#lookupDetailId').val($('#lookupDetailId_'+rowid).val());
					}
				 });
				}
				else{
					return false;
				}
			 return false;
	  });

	 $(".delrow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var componentfeature=$('#componentfeature_'+rowid).text().trim();
		 if(componentfeature!=null && componentfeature!=""){
			 var flag=false; 
			 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/property_compoment_adddeleteline.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result);    
						 if(result!=null){
						    	if($('#returnMsg').html() != '' || $('#returnMsg').html() == "Success")
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#page-error").hide().html($('#returnMsg').html().slideDown()); 
						$('.tempresult').html(result);   
						return false;
					}
						
				 }); 
				 if(flag==true){ 
		        		 var childCount=Number($('#childCount').val());
		        		 if(childCount > 0){
							childCount=childCount-1;
							$('#childCount').val(childCount); 
		        		 } 
		        		 $(this).parent().parent('tr').remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowid').each(function(){  
							i=i+1;
							$($(this).children().get(0)).text(i); 
	   					 });  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }
		 return false;
	 });

	 $('.discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_component_discard.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#component-popup').dialog('destroy');		
					$('#component-popup').remove(); 
			 		$("#main-wrapper").html(result); 
				}
			});
		 return false;
	 });

	 $('.save').click(function(){
			 $('.success,.error').hide();
		 	$('#componentSize').trigger('blur');
			var id=$('.save').attr('id'); 
			var succesMessage="Successfully Created";
			var propertyId=$('#propertyId').val();
			var componentNumber=Number($('#componentNumber').val());
			var componentName=$('#componentName').val();
			var componentNameArabic=$('#componentNameArabic').val();
			var componentTypeId=$('#componentTypeId').val();
			var rent=$('#rent').val().replace(/,/g, '');
			var componentSize=$('#componentSize').val();
			var unitSize=$('#unitSize').val();
			var status=$('#statusCombo').val(); 
			var componentId=$('#componentId').val();
			var numberOfSize=$('#numberOfSize').val();
			if(componentId!=null && componentId!="" && componentId>0){
				url_action="property_component_save";
				url_action="property_component_update";
				succesMessage="Successfully Updated";
			}else{
				componentId=0;
				url_action="property_component_save";
			}
			 if($jquery("#componentEntryValidation").validationEngine('validate')  && $('#componentSizeError').val() == '0'){   
				var childCount=Number($('#childCount').val());
				if(1==1){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false,
				 	data:{propertyId:propertyId, componentNumber:componentNumber, componentName:componentName, componentTypeId:componentTypeId,
				 		  componentSize:componentSize,sizeUnit:unitSize, rent:rent, status:status, componentId:componentId, numberOfSize:numberOfSize,
				 		 componentNameArabic:componentNameArabic},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('#returnMsg').html(); 
						 if(message.trim()=="Success"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/property_component_list.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#component-popup').dialog('destroy');		
										$('#component-popup').remove(); 
										$("#main-wrapper").html(result); 
										$('.commonSuccess').html(succesMessage);
										$('.commonSuccess').show();
									}
							 });
						 }
						 else{
							 $('#journal-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					}
				 }); 
				}
				else{
					 $('#journal-error').hide().html("Please insert lines records to process").slideDown(1000);
					 return false;
				}
			 }
			 else{
				 return false;
			 }
			 return false;
		 });

	 $('.addrows').click(function(){
   	  var id=Number(1);
		  var i=Number(0);
		  $('.rowid').each(function(){
				id=id+1; 
		  });  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_component_addrow.action", 
			 	async: false,
			 	data:{id:id},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tab").append(result);
					//if($(".tab").height()<255)
						// $(".tab").animate({height:'+=20',maxHeight:'55'},0);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
					 i=0;
					 $('.rowid').each(function(){
							i=i+1;
							$($(this).children().get(0)).text(i); 
						}); 
					 var tempvar=$('.tab>tr:last').attr('id'); 
					 var idval=tempvar.split("_");
					 var rowid=Number(idval[1]); 
					 $('#DeleteImage_'+rowid).show(); 
				}
			});
		  return false;
     });
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.ComponentDetails" /></div>	
		  <c:choose>
			<c:when test="${requestScope.COMMENT_IFNO.commentId ne null && requestScope.COMMENT_IFNO.commentId ne ''
								&& requestScope.COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		  <form name="componentEntry" id="componentEntryValidation" style="margin-left: 18px;"> 
			<div class="portlet-content">  
				<fieldset>
				<div class="form_head"><fmt:message key="re.property.info.ComponentDetails" /></div>  
				<div id="journal-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>
				<div id="compSizeErrorMsg" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="componentId" name="componentId" value="${COMPONENT_INFO.componentId}"/>
				<div class="width100 float-left" id="hrm">  
					<div class="float-right  width48">  
						<fieldset style="height: 130px; margin-right: 20px;"> 
						<div style="margin-top:3px;">
							<label><fmt:message key="re.property.info.componentType" /><span style="color: red;">*</span></label> 
							<input type="text" id="componentType" name="componentType" value="${COMPONENT_INFO.componentType}" class="validate[required] width40"/>
							<input type="hidden" id="componentTypeId" value="${COMPONENT_INFO.componentTypeId}"/>
							<span class="button">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all component-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div>	
						<div>
							<label><fmt:message key="re.property.info.annualRent" /><span style="color: red;">*</span></label> 
							<input type="text" id="rent" name="rent" class="validate[required] width40"/>
						</div>	
						<div>							
							<label><fmt:message key="re.property.info.componentSize" /></label> 
							<input type="text" id="componentSize" name="componentSize" value="${COMPONENT_INFO.componentSize}" class="width15"/>
							<input type="hidden" id="componentSizeError" value="0"/>
							<select name="unitSize" id="unitSize" style="width: 23.5%" class="">
								<option value="">--<fmt:message key="re.property.info.sizeUnit" />--</option>
								<option value="1">Square Feet</option>
								<option value="2">Square Meter</option>
								<option value="3">Square Yard</option>
							</select>
							<input type="hidden" id="tempunitSize" name="tempunitSize" style="display: none;" value="${COMPONENT_INFO.unitSize}"/> 
						</div> 
						<div style="margin-top:5px;">
							<label><fmt:message key="re.property.info.status" /></label>
							<input type="hidden" id="status" name="status" value="${COMPONENT_INFO.status}" title="Status" style="width:5%"/> 
							<select style="width: 41.5%;" id="statusCombo" name="statusCombo">
								<option value="1"><fmt:message
									key="re.property.info.available" /></option>
								<option value="2"><fmt:message
									key="re.property.info.pending" /></option>
								<option value="5"><fmt:message
									key="re.property.info.renovation" /></option>
							</select>
						</div>  
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="height: 130px;">
								<div>
									<label > 
									<fmt:message key="re.property.info.propertyName" /><span style="color: red;">*</span></label>
									<input name="propertyName" type="text" readonly="readonly" id="propertyName" value="${COMPONENT_INFO.propertyName}" class="validate[required] width40">
									<input type="hidden" id="propertyId" value="${COMPONENT_INFO.propertyId}"/>
									<input type="hidden" id="propertySize" value="${COMPONENT_INFO.propertySize}"/>
									<span class="button">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all component-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
								</div> 
								<div>
									<label><fmt:message key="re.property.info.noOfUnits" /><span style="color: red;">*</span></label>
									<input type="text" id="numberOfSize" name="numberOfSize" value="${COMPONENT_INFO.numberOfSize}" 
										class="width40 validate[required,custom[onlyNumber]]"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.componentNumber" /></label> 
									<input type="text" id="componentNumber" name="componentNumber" value="${COMPONENT_INFO.componentNumber}" class="width40 validate[optional,custom[onlyNumber]]"/>
								</div>	
								<div style="margin-top:3px;">
									<label> 
									<fmt:message key="re.property.info.componentName" /><span style="color: red;">*</span></label>
									<input name="componentName" type="text" id="componentName" value="${COMPONENT_INFO.componentName}" class="validate[required] width40">
								</div>  
								<div style="margin-top:3px;">
									<label> 
									<fmt:message key="re.property.info.componentName" />(Arabic)</label>
									<input name="componentNameArabic" type="text" id="componentNameArabic" value="${COMPONENT_INFO.componentNameArabic}" class="width40">
								</div>  
								
						</fieldset>
					</div> 
				 </div>
				 
			<div class="clearfix"></div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="margin-top:10px; width: 98%;">  
			<div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.componentFeatures" />
				<!-- Add Feature Link -->
						<a id="feature-type-add" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" 
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "  
							title="Add Feature Type" style="float: right; 
													cursor: pointer; 
													left: 49px; 
													border-bottom-width: 1px; 
													margin-bottom: 0px; 
													margin-top: -5px; 
													height: 4px; 
													margin-right: 10px; 
													padding-top: 5px; 
													padding-bottom: 10px;"> 
							
							<span class="ui-icon ui-icon-plus" style="float: left;"></span>
							<label>Add Feature Type</label> 
						</a>
			</div>
 			<div class="portlet-content"> 
 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
 				
 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
				<div id="hrm" class="hastable" > 
					 <input type="hidden" readonly="readonly" name="trnValue" id="trnValue"/> 
					 <input type="hidden" name="childCount" id="childCount" value="${COMPONENT_INFO_SIZE}"/>  
					<table id="hastab" class="width100"> 
						<thead>
						   <tr> 
						   		<th style="width:5%; display: none;">Line Number</th>
								<th><fmt:message key="re.property.info.featureCode" /></th>
								<th><fmt:message key="re.property.info.features" /></th>  
								<th><fmt:message key="re.property.info.featuresDetail" /></th>
								<th style="width:7%;"><fmt:message key="re.contract.options"/></th> 
						  </tr>
						</thead> 
						<tbody class="tab"> 
						<c:choose>
							<c:when test="${requestScope.COMPONENT_LINE_INFO ne null && requestScope.COMPONENT_LINE_INFO ne ''}">
								<c:forEach var="bean" items="${COMPONENT_LINE_INFO}" varStatus ="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td style="width:5%; display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
										<td id="componentfeatureCode_${status.index+1}">${bean.componentFeatureCode}</td>  
										<td id="componentfeature_${status.index+1}">${bean.componentFeature}</td>  
										<td id="componentfeatureDetails_${status.index+1}">${bean.componentFeatureDetails}</td>  
										<td style="display:none">
											<input type="hidden" name="componentlineid" value="${bean.componentLineId}" id="componentlineid_${status.index+1}"/> 
											<input type="hidden" name="componentfeatureid" value="${bean.componentFeatureId}" id="componentfeatureid_${status.index+1}"/> 
											<input type="hidden" name="lookupDetailId" value="${bean.lookupDetailId}" id="lookupDetailId_${status.index+1}"/> 
										</td> 
										 <td style="width:5%;" class="opn_td" id="option_${status.index+1}">
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="cursor:pointer;display:none; " title="Add Record">
					 								<span class="ui-icon ui-icon-plus"></span>
											  </a>	
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											  </a> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											  </a>
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
													<span class="processing"></span>
											  </a>
										</td>   
									</tr>
								</c:forEach>
							</c:when> 
						</c:choose> 
						<c:forEach var="i" begin="${requestScope.COMPONENT_INFO_SIZE+1}" end="${requestScope.COMPONENT_INFO_SIZE+2}" step="1" varStatus ="status"> 
							<tr class="rowid" id="fieldrow_${i}">
								<td style="width:5%; ; display: none;" class="width10" id="lineId_${i}">${i}</td>
								<td id="componentfeatureCode_${i}"></td> 
								<td id="componentfeature_${i}"></td> 
								<td id="componentfeatureDetails_${i}"></td> 
								<td style="display:none">
									<input type="hidden" name="componentlineid" id="componentlineid_${i}"/> 
									<input type="hidden" name="componentfeatureid" id="componentfeatureid_${i}"/> 
									<input type="hidden" name="lookupDetailId" id="lookupDetailId_${i}"/> 
								</td> 
								 <td style="width:1%;" class="opn_td" id="option_${i}">
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;" title="Add Record">
			 								<span class="ui-icon ui-icon-plus"></span>
									  </a>	
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span>
									  </a> 
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="cursor:pointer;" title="Delete Record">
											<span class="ui-icon ui-icon-circle-close"></span>
									  </a>
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
											<span class="processing"></span>
									  </a>
								</td>   
							</tr>
						</c:forEach>
				 </tbody>
			</table>
		</div> 
	</div>
	<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
	</div> 
	</div>
	</fieldset>
	</div>  
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:25px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div>  
		<div class="portlet-header ui-widget-header float-right save" id="${showPage}" style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	 </div>  
	 <div class="clearfix"></div>
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"><div id="component-popup" class="ui-dialog-content ui-widget-content">
			<div class="component-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix" ><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
	<!-- Added for Lookup Information -->
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
  </form>
  </div> 
</div>