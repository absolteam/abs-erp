<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <c:if test="${requestScope.bean != null && requestScope.bean !=''}">
 <c:choose>
 	<c:when test="${bean.sqlReturnStatus==1}">
 		<div id="flag" style="display:none">${bean.sqlReturnStatus}</div>
 		 <div id="objTrnVal" style="display:none;">${bean.trnValue}</div>
 		  <div id="objTempVal" style="display:none;">${bean.tempLineId}</div>
		 <div id="objLineVal" style="display:none;">${bean.actualLineId}</div> 
 	</c:when>
 	<c:when test="${bean.sqlReturnStatus==0}"> 
 		<div id="returnMsg" style="display:none;">${bean.sqlReturnMsg}</div>
 	</c:when> 
 	<c:otherwise>
 		<div id="returnMsg" style="display:none;">${bean.sqlReturnMsg}</div>
 	</c:otherwise>
 </c:choose> 
 </c:if>