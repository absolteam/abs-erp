 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;" id="childRowId_${requestScope.rowid}">
<td colspan="4" class="tdidentity" id="childRowTD_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="periodEditAddValidate" style="padding-left: 20px;">
<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
<input type="hidden" name="lookupDetailId" id="lookupDetailId"/>
<div class="width36 float-left" id="hrm" >
		<fieldset style="height: 100px;">
			<legend>Selection</legend> 
			<div><label class="width30">Feature Code</label><input type="text" tabindex="1" readonly="readonly" name="unitFeatures" id="unitFeatures" class="width45 validate[required]">
			<span class="button">
				<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all componet_featurepop width100"> 
					<span class="ui-icon ui-icon-newwin"> 
					</span> 
				</a>
			</span>
			</div>
			<div><label class="width30">Features</label><input type="text" tabindex="1" readonly="readonly" 
				name="unitDetails" id="unitDetails" class="width45 validate[required]">
			</div>
		</fieldset> 
</div> 

<div class="width30 float-left" id="hrm">
	<fieldset style="height: 100px;">
		<legend>Details</legend> 
		<div>
			<label class="width30">Feature Details</label>
			<input type="text" tabindex="1" 
				name="unitFeatureDetails" id="unitFeatureDetails" class="width50">			
		</div>
	</fieldset>	
</div> 

<div class="width30 float-left" id="hrm">
	<fieldset style="height: 100px;">
		<legend>Uploads</legend> 
		<div id="hrm" Style="margin-bottom: 10px;">
			<div id="UploadDmsDiv" class="width100">
				
				<div style="height: 90px;">									
					
					<div id="dms_image_information_unit_feature" style="cursor: pointer; color: blue;">
						<u>Upload image here</u>
					</div>
					<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
						<span id="uploadedImagesUnit"></span>
					</div>
					
				</div>					
			</div>
		</div>
	</fieldset>	
</div> 

<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_feature" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_feature" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
 <div class="clearfix"></div> 
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="componet_featurepop" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="componet_featurepop-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
</form>
	</td>  
	
 </tr>
	 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	
	 $(function(){  

		// for uploader
		if($('#addEditFlag').val()=='E') {

			var tempvar=$('.tdidentity').attr('id');
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			var unitFeatureId = $("#unitDetailId_"+rowid).val();

			populateUploadsPane("img","uploadedImagesUnit", "UnitDetail", unitFeatureId);
			
			$('#dms_image_information_unit_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedImagesUnit", "UnitDetail", unitFeatureId);
			});
		}
		else {
			$('#dms_image_information_unit_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedImagesUnit", "UnitDetail", "-1");
			});
		}

		 
		  $('.cancel_feature').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				$('#componet_featurepop').dialog('destroy');		
				$('#componet_featurepop').remove();
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				
				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
  					$("#AddImage_"+rowid).show();
  				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
  				 	$("#EditImage_"+rowid).show();
				}
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 $("#childRowId_"+rowid).remove();	
				 tempvar=$('.tabFeature>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#DeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
		  });
		  
		  $('.add_feature').click(function(){ 
			  
		 // slidetab=$(this).parent().parent().get(0);			 
		 //Find the Id for fetch the value from Id
			var tempvar=$('.tdidentity').attr('id');
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			 //var tempObject=$(this);
			 var unitDetails=$('#unitDetails').val();
			 var unitFeatures=$('#unitFeatures').val();
			 var unitFeatureDetails=$('#unitFeatureDetails').val();
			 var lookupDetailId=$('#lookupDetailId').val();
			 var addEditFlag=$('#addEditFlag').val();
			 var lineId=$('#lineId_'+rowid).val();
			
		        //alert(fromdate+"--"+todate+"--"+usrfromdate+"--"+usrtodate);
		        if($jquery("#periodEditAddValidate").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						url:"<%=request.getContextPath()%>/unit_feature_add_save.action",
					 	async: false, 
					 	data:{unitDetails:unitDetails,addEditFlag:addEditFlag,id:rowid,
					 		unitFeatures:unitFeatures, unitFeatureDetails:unitFeatureDetails,
					 		lookupDetailId:lookupDetailId},
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                     flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                             $("#othererror").html($('#returnMsg').html()); 
                             return false;
					 	} 
		        	}); 
		        	if(flag==true){ 
						
			        	//Bussiness parameter
			        	$('#featureCode_'+rowid).text(unitFeatures); 
			        	$('#featureDetails_'+rowid).text(unitDetails); 
		        		$('#featureUnitDetails_'+rowid).text(unitFeatureDetails); 
		        		$('#lookupDetailId_'+rowid).text(lookupDetailId); 
		        		$('#componet_featurepop').dialog('destroy');		
						$('#componet_featurepop').remove();
						//Button(action) hide & show 
		    				 $("#AddImage_"+rowid).hide();
		    				 $("#EditImage_"+rowid).show();
		    				 $("#DeleteImage_"+rowid).show();
		    				 $("#WorkingImage_"+rowid).hide(); 
		        			 $("#childRowId_"+rowid).remove();	
		        			 
		        			 featureRowAddedUnit = featureRowAddedUnit + 1;
	         					if(featureRowAddedUnit == 2) {
	         						tempvar=$('.tabFeature>tr:last').attr('id'); 
	         						idval=tempvar.split("_");
	         						rowid=Number(idval[1]); 
	         						$('#DeleteImage_'+rowid).show();
	         						$('.addrowsfeature').trigger('click');
	         						featureRowAddedUnit = 1;
	         					}
			        			
		        		}		  	 
		      } 
		      else{return false;}
		  });
			$('.startPicker,.endPicker').datepick({onSelect: customRange, showTrigger: '#calImg'});
			$('.startPicker').click(function(){
				startpick=($(this).attr('class'));
				//$($($($(this).parent().parent()).children().get(3)).children().get(0)).val("");
			});
			$('.endPicker').click(function(){
				endpick=($(this).attr('class'));  
			});

			//Popup
			$('.componet_featurepop').click(function(){ 
			      tempid=$(this).parent().get(0);  
			      $('.ui-dialog-titlebar').remove(); 
				 	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/unit_feature_list_get.action", 
					 	async: false, 
					    dataType: "html",
					    cache: false,
						success:function(result){   
							 $('.componet_featurepop-result').html(result);  
						},
						error:function(result){ 
							 $('.componet_featurepop-result').html(result); 
						}
					});  
			});
			
			 $('#componet_featurepop').dialog({
				 autoOpen: false,
				 minwidth: 'auto',
				 width:500, 
				 bgiframe: false,
				 overflow:'hidden',
				 modal: true 
			});
	 });
 </script>	