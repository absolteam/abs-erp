<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(document).ready(function (){ 
	$('#buildingId').val($('#tempbuildingId').val());
	$('#classId').val($('#tempclassId').val());
	$('#brandId').val($('#tempbrandId').val());

	 $("#ownerInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

	 $("#close").click(function(){ 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/maintenance_asset_list.action",   
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
		     	}
			});
			return true;
		}); 
	}); 		 	
</script>
  

<script type="text/javascript">

</script>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.buildingmaintenanceasset"/></div>
					<div>
						<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
							<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
						</c:if>
						<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
							<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
						</c:if>
			 	   </div>
					<form name="ownerInformationAdd" id="ownerInformationAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.owner.info.validityinformation"/></legend>
									<div><label class="width30"><fmt:message key="re.owner.info.fromdate"/></label><input type="text" name="startPicker" class="width30" id="startPicker" disabled="disabled"  value="${bean.fromDate }" /></div>
									<div><label class="width30"><fmt:message key="re.owner.info.todate"/></label><input type="text" name="endPicker" class="width30 tooltip" id="endPicker" disabled="disabled" value="${bean.toDate }" /></div>
							 </fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.owner.info.assetinformation"/></legend>
								<div><label><fmt:message key="re.owner.info.classid"/></label>
									<input type="hidden" name="classId" class="width30" id="tempclassId" value="${bean.classId}"/>
									<select id="classId" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.classList ne null}">
											<c:forEach items="${requestScope.classList}" var="classes" varStatus="status">
												<option value="${classes.classId }">${classes.className }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.subclassname"/></label>
									<input name="subClassName" class="width30" disabled="disabled" id="subClassName" value="${bean.subClassName}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.itemname"/></label>
									<input name="detailDescription" class="width30" disabled="disabled"  id="detailDescription" value="${bean.detailDescription}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.detailspecification"/></label>
									<input name=specification class="width30" disabled="disabled"  id="specification" value="${bean.specification}"/>
								</div>
								 <div>
									<input type="hidden" name="brandId" class="width30" id="tempbrandId" value="${bean.brandId}"/>
									<label><fmt:message key="re.owner.info.brand"/></label>
										<select class="width30 validate[required]" id="brandId" disabled="disabled">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.lookupList}" var="typelist" varStatus="status">
											<option value="${typelist.brandId}">${typelist.brand}</option>
										</c:forEach>
										</select>
									</div>
									<div><label><fmt:message key="re.owner.info.modelnumber"/></label><input type="text" name="Proffession" class="width30 tooltip" id="modelNumber" disabled="disabled" value="${bean.modelNumber}"/></div>
									<div><label><fmt:message key="re.owner.info.noofpieces"/></label><input type="text" name="uaeIdNo" class="width30 validate[required]" id="noOfPieces" disabled="disabled" value="${bean.noOfPieces}"/></div>
							</fieldset> 
						</div>
					</form>
					<input type="hidden" id="buildingAssetId" value="${bean.buildingAssetId}"/>
					<div class="clearfix"></div>					
					<div class="float-right buttons ui-widget-content ui-corner-all"> 			
						<div class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
					</div>
			 </div>
		</div>