<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  
	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	 $('#birthdate').datepick({onSelect: customRange, showTrigger: '#calImg'});
	 

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
  var daysInMonth =$.datepick.daysInMonth(
  $('#selectedYear').val(), $('#selectedMonth').val());
  $('#selectedDay option:gt(27)').attr('disabled', false);
  $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
  if ($('#selectedDay').val() > daysInMonth) {
      $('#selectedDay').val(daysInMonth);
  }
} 
function customRange(dates) {
if (this.id == 'birthdate') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#endPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
  


<fieldset>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Maintenance Contract</div>
			
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend>Info</legend>	
							 <div><label>Contract Date.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>				
                            <div><label>Contract No.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
                         	<div><label>Contract Type</label>
								<select class="width30">
									<option>-Select-</option>
									<option>Elevator</option>
									<option>A/C</option>
								</select>
								</div>										<div><label>Payment Method</label>
								<select class="width30">
									<option>-Select-</option>
									<option>DD</option>
									<option>Cash</option>
								</select>
							</div>	
							<div><label>Supplier/Vendor No.</label>
								<select class="width30">
									<option>-Select-</option>
									<option>123</option>
									<option>456</option>
								</select>
                            <div><label>Address1<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
                            <div><label>Address2<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>										
							</div>	
					</fieldset>  																		
				</div> 
		<div class="width50 float-left" id="hrm">
			<fieldset>
				<legend>Maintenance Info.</legend>
					<div><label>Purchase Order No.</label>
						<select class="width30">
							<option>-Select-</option>
							<option>123</option>
							<option>420</option>
						</select>
					</div>					
					<div><label>AMC Amount<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name" readonly="readonly"></div>
					<div><label>Security Deposit<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name"></div>					
					<div><label>Visit No.</label>
						<select class="width30">
							<option>-Select-</option>
							<option>123</option>
							<option>420</option>
						</select>
					</div>	
			</fieldset> 
			<fieldset>
				<legend>Contract Period</legend>
					<div><label class="width30">From Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip"  title="Select Birth Date" readonly="readonly"></div>		
				    <div><label class="width30">To Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>
			</fieldset>
		</div>
	</div>
</div>
</fieldset>	
	
					<div id="main-content"> 
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:1063px;"> 
					<form name="newFields">
	 				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset Information</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
					<div style="display:none;" class="tempresult"></div>
			 		<input type="hidden" name="trnValue" id="trnValue" readonly="readonly"/> 
			 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
		 							<thead class="chrome_tab">
										<tr>  
			            					<th class="width10">SL No.</th>
			            					<th class="width10">Building No.</th>
			            					<th class="width10">Building Component</th>
			            					<th class="width10">Asset No.</th>
			            					<th class="width10">Asset Name</th>
			            					<th class="width10">Brand</th>
			            					<th class="width10">Model No.</th>
			            					<th class="width10">Age</th>
			            					<th style="width:5%;"><fmt:message key="accounts.currencymaster.label.options"/></th>
										</tr>
									</thead>  
									<tbody class="tab" style="">	
										 <%for(int i=0;i<=2;i++) { %>
											 <tr>  
												<td class="width10"></td>	 
												<td class="width10"></td>
												<td class="width10"></td>	
												<td class="width10"></td> 
												<td class="width10"></td>
												<td class="width10"></td>
												<td class="width10"></td>
												<td class="width10"></td>
												<td style="display:none"> 
													<input type="hidden" name="tempId" value="" id="tempId"/>
													<input type="hidden" name="curIdVal" value="" id="curIdVal"/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>		 
											</tr>  
								  		 <%}%>
									</tbody>
		 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    	</div>
							
						</div> 
				</form>
	
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;">Add Row</div> 
		
	</div>
	</div>
	
		<fieldset>
			<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:1063px;"> 
			<form name="newFields">
				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payment Information</div>
			<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
			<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
			<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
			<div class="portlet-content"> 
			<div style="display:none;" class="tempresult"></div>
	 		<input type="hidden" name="trnValue" id="trnValue" readonly="readonly"/> 
	 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
				<div id="hrm"> 
					<div id="hrm" class="hastable width100"> 
						<table id="hastab" class="width100">	
 							<thead class="chrome_tab">
								<tr>  
	            					<th class="width10">SL No.</th>
	            					<th class="width10">Date</th>
	            					<th class="width10">Installment Amount</th>
	            					<th class="width10">Balance Amount</th>
	            					<th style="width:5%;"><fmt:message key="accounts.currencymaster.label.options"/></th>
								</tr>
							</thead>  
							<tbody class="tab" style="">	
								 <%for(int i=0;i<=2;i++) { %>
									 <tr>  
										<td class="width10"></td>	 
										<td class="width20"></td>
										<td class="width20"></td>	
										<td class="width10"></td> 
										<td style="display:none"> 
											<input type="hidden" name="tempId" value="" id="tempId"/>
											<input type="hidden" name="curIdVal" value="" id="curIdVal"/>	
										</td> 
										<td style=" width:5%;"> 
		  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" style="cursor:pointer;" title="Add Record">
		 	 									<span class="ui-icon ui-icon-plus"></span>
										   	</a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  style="display:none;cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
											</a> 
											<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" style="cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
												<span class="processing"></span>
											</a>
										</td>  
										<td style="display:none;"></td>		 
									</tr>  
						  		 <%}%>
							</tbody>
 						</table>
							<div class="iDiv" style="display: none;">
							</div>
					</div>
						<div class="vGrip">
							<span></span>
						</div>
		    	</div>
					
			</div> 
		</form>
				<fieldset>
					<div class="float-right width45">
						<label class="width30">Total</label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name">
					</div>
				</fieldset>
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
							<div class="portlet-header ui-widget-header float-right cancelrec" style=" cursor:pointer;"><fmt:message key="accounts.currencymaster.button.cancel"/></div> 
							<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;"><fmt:message key="accounts.currencymaster.button.save"/></div>
						</div>
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
							<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;">Add Row</div> 
						</div>
			</div>
			</div>	
</fieldset>	
