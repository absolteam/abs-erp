<tr class="offer">  	 
	<td class="width20">${requestScope.lineNumber}</td>
	<td class="width20"></td>	
	<td class="width20"></td> 
	<td class="width20"></td>
	<td style="display:none"> 
		<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
		<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
		<input type="hidden" value="" name="tempLineId" id="tempLineId"/>  
	</td> 
	<td style=" width:5%;"> 
				<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeatureNew" style="cursor:pointer;" title="Add this row" >
		 	 <span class="ui-icon ui-icon-plus"></span>
		</a>
		<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeatureNew" style="cursor:pointer;" title="Edit this row" >
			<span class="ui-icon ui-icon-wrench"></span>
		</a>
		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeatureNew" style="cursor:pointer;" title="Delete this Row" >
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td>  
	<td style="display:none;" class="owner_id"></td>
	<td style="display:none;"></td>		 
</tr>  
<script type="text/javascript">
$(function() {
	
$('.tooltip').tooltip({
		track: true,
		delay: 0,
		showURL: false,
		showBody: " - ",
		fade: 250
	});	
//Adding Feature Details
$('.addFeatureNew').click(function(){ 
	if(openFlag==0){
		if($('#offerFormEdit').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	       //alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/edit_add_offer_form_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button

		         openFlag=1;
		         
		         $('#loading').fadeOut();
				}
			});
		}else{
			$('#warningMsg').hide().html("Please insert record ").slideDown();
		}
	}
});

$(".editFeatureNew").click(function(){
	if(openFlag==0){
		$('#loading').fadeIn();
		slidetab=$(this).parent().parent().get(0); 
		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
        
		//actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_edit_offer_form_redirect.action",  
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 $(result).insertAfter(slidetab);
			 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
	         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
	         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
				
			 
			 var temp1= $($(slidetab).children().get(0)).text();
			 var temp2= $($(slidetab).children().get(1)).text();
			 var temp3= $($(slidetab).children().get(2)).text();
			 var temp4= $($(slidetab).children().get(3)).text();

			 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');

			$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
			$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
			
		  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
		  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);

		  	openFlag=1;
		
		  	$('#loading').fadeOut();

			}
		});
	}	
});

$('.delFeatureNew').click(function(){
	if(openFlag==0){
		slidetab=$(this).parent().parent().get(0); 
		$('.error').hide();
	    $('.childCountErr').hide();
	    $('#warningMsg').hide();
	    actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		 var actionflag="D";
		 var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
		  var trnValue=$('#transURN').val();
		 var offerId=$("#offerId").val();
		 var flag = false;
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
		if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/edit_delete_offer_form_update.action", 
			 	async: false,
			 	data: {actualLineId: actualID, actionFlag:actionflag, offerId: offerId,trnValue: trnValue,tempLineId: tempLineId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
	}	
});
});
</script>