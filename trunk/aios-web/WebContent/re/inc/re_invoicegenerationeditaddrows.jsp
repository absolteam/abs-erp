<tr class="invoicelines"> 
<td class="width10">${requestScope.lineNumber}</td>		 
	<td class="width20"></td>	 
	<td class="width20 amountCalc"></td>   
	<td class="width20 functionalAmountCalc"></td>	 
	<td class="width20 "></td> 
	<td style="display:none;">
		<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
		<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
		<input type="hidden" value="" name="tempLineId" id="tempLineId"/> 
	 </td> 
	<td style="width:5%;">
		<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataNew" title="Add this row" >
		 	 <span class="ui-icon ui-icon-plus"></span>
		</a>
		<a style="display:none;cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataNew" title="Edit this row" >
			<span class="ui-icon ui-icon-wrench"></span>
		</a>
		<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowNew" title="Delete this Row" >
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td>
	<td style="display:none;"></td>
	<td style="display:none;"></td>
	<td style="display:none;"></td> 
	<td style="display:none;"></td>
	<td style="display:none;"></td> 
</tr>
<script type="text/javascript">
$(function(){

	$('.addDataNew').click(function(){
		if($('#invoiceGenerationEdit').validationEngine({returnIsValid:true})){
		 	if($('#openFlag').val() == 0) { 
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        availFlag=$('#availFlag').val();
		        if(true){
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/re_invoice_generation_edit_add.action", 
				 		async: false,
				    	dataType: "html",
				   		cache: false,
						success:function(result){
							 $(result).insertAfter(slidetab);
							 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
					         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
					         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button

					         var temp1= $($(slidetab).children().get(0)).text();

					         $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					         
					         $('#loading').fadeOut();
					         $('#openFlag').val(1);
						}
					});
		        }
		        else{
		        	$('#loading').fadeOut();  
		        	$('#temperror').hide().html("Invoice date doesnot belongs to this period.").slideDown(1000);
		        	$.scrollTo(0,300);	//Scroll to top 
					return false; 
		        }
		 	}	
		}else{
			return false;
		}
	});
	$(".editDataNew").click(function(){
		if($('#invoiceGenerationEdit').validationEngine({returnIsValid:true})){
		 	if($('#openFlag').val() == 0) {
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/re_invoice_generation_edit_edit.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
					 var temp5= $($(slidetab).children().get(4)).text();
					 
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
		
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
		
				  	$('#loading').fadeOut();
				  	$('#openFlag').val(1);
					}
				});
		 	}
		}else{
			return false;
		}
	});

   
 	$('.delrowNew').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
 		tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
 		var ledgerId=$('#headerLedgerId').val();	
 		invoiceId=$('#invoiceId').val();			
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/re_invoice_generation_edit_delete.action", 
			 	async: false,
			 	data: {invoiceId: invoiceId,trnValue: trnValue, actualLineId: actualID, ledgerId: ledgerId,tempLineId: tempLineId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$('#temperror').show();
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
					childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
		 	$('#warningMsg').show();
			$('#warningMsg').html("Please insert record to delete");
			return false; 
		}
 	});	

});
</script>