 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
 <style type="text/css">
#component-popup{
	overflow: hidden;
}
#example2{
	cursor:pointer;
}
</style>
<script type="text/javascript">
var id;var oTable; var selectRow=""; var aData ="";
var aSelected = [];
var componentTypeId=""; var componentTypeName="";
$(function(){ 
	$('.formError').remove();   
	$('#example2').dataTable({ 
		"sAjaxSource": "component_componenttype_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [ 
			{ "sTitle": "Type_ID",  "bVisible": false}, 
			{ "sTitle": "Component Type"}, 
		], 
		"sScrollY": $("#component-popup").height() - 160,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#example2').dataTable(); 
	
	$('#example2 tbody tr').live('dblclick', function (){  
		 if($(this).hasClass('row_selected')){ 
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          componentTypeId=aData[0]; 
	          componentTypeName=aData[1]; 
	      }
	      else{ 
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          componentTypeId=aData[0]; 
	          componentTypeName=aData[1]; 
	      }  
		 $('#componentTypeId').val(componentTypeId);
		 $('#componentType').val(componentTypeName);
		 $('#component-popup').dialog("close"); 
	});
});
</script> 
<div id="main-content">
		<div id="" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Component Type</div>	 
			<div class="portlet-content"> 
				 <div id="component_type">
					<table class="display" id="example2"></table>
				 </div>  
			</div>
		</div>
</div>