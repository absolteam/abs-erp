<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<c:forEach items="${requestScope.rentFeeList}" var="result" varStatus="status">
 	<tr class="even"> 
		<td class="width20 indexForFee">${status.index+1}</td>
		<td class="width20">${result.feeId }</td>	
		<td class="width20 indexFeeDesc">${result.feeDescription }</td> 
		<td class="width20 feeAmountCalc">${result.feeAmount }</td>
		<td style="display:none"> 
			<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
		</td> 
		<td style=" width:5%;display:none;"> 
					<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFee" style="cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
		   	</a>	
		   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFee"  style="display:none;cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFee" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
				<span class="processing"></span>
			</a>
		</td>  
		<td style="display:none;" class=""></td>
		<td style="display:none;"></td>	
	</tr>
</c:forEach>