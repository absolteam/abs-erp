<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>


<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.opensymphony.xwork2.ActionContext"%>  
<style>
.opn_td{
	width:6%;
}
.table-custom-design{
	width:100% !important;
	padding-top:10px;
}
.tbody-custom-design{
	float:left;
	height:250px;
	overflow-x:hidden;
	overflow-y:scroll;
	border: solid 2px gray;
}
</style>
<script type="text/javascript">
var slidetab="";
var featureRowsAdded = 0;
var compRent;
$(function(){

	if($('#status').val()=="true"){
		$('#status').attr('checked',true);
		$('#statusCombo').val(1);
	}
	else{
		$('#status').attr('checked',false);
		$('#statusCombo').val(0);
	}
	$('#unitSize').val($('#tempunitSize').val());
	

	$jquery("#componentEntryValidation").validationEngine('attach');

	$('#statusCombo').val($('#status').val());

	compRent = convertToAmount("${COMPONENT_INFO.rent}");
	$("#rent").val(compRent);
	
	$('.component-popup').click(function(){ 
       tempid=$(this).parent().get(0);  
       var tableName=$('#tableName').val();
       if(tableName!=""){
    	   $('#general-error').hide();
		   $('#component-popup').dialog('open');
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/legal_usecase_record_details.action", 
				data:{tableName:tableName},
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.component-result').html(result);  
				},
				error:function(result){  
					 $('.component-result').html(result); 
				}
			});  
       }else{
    	   
    	   $("#general-error").hide().html("Select Legal group information").slideDown(1000);
       }
	});
	 $('#component-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:600,
			bgiframe: false,
			modal: false,
			buttons: { 
				"Close": function() {  
					$(this).dialog("close"); 
				}		
			} 	
	});
	 
	 
	 //Document upload section
	 var legalId=$('#legalId').val();
	 $('#legal_document_information').click(function(){
			if(legalId>0){
				AIOS_Uploader.openFileUploader("doc","legalDocs","Legal",legalId,"LegalDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","legalDocs","Legal","-1","LegalDocuments");
			}
		});
	 
	 $('#tableName1').change(function(){
		 var tableName=$('#tableName1').val();
		 if(tableName!=null && tableName!=""){
			 
			 url=null;
				if(tableName=="Property")
					url="property_info_list.action";
				else if(tableName=="Contract")
					url="contract_agreement_list.action";
				else
					return false;
			  
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+url,
				data:{				 		
			 		legalProcess:Number(1)
			 	},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#UseCase_View_Temp").html(result); 

				}
		 	});
			 $('#popup_open_trigger').trigger('click');
		 }
	 });
	 $('#tableName1').live('dblclick', function (){
		 var tableName=$('#tableName1').val();
			 if(tableName!=null && tableName!=""){
				 
				 url=null;
					if(tableName=="Property")
						url="property_info_list.action";
					else if(tableName=="Contract")
						url="contract_agreement_list.action";
					else
						return false;
				  
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/"+url,
					data:{				 		
				 		legalProcess:Number(1)
				 	},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#UseCase_View_Temp").html(result); 
	
					}
			 	});
				 $('#popup_open_trigger').trigger('click');
			 }
	 });
	 
	 
	 $('#popup_open_trigger').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#UseCase_View_Temp', 
			loaderHeight:16, 
			loaderWidth:17 
		}); 

	 $('.discard').click(function(){
		 window.location.reload();
	 });

	 $('.save').click(function(){ 
			 if($jquery("#componentEntryValidation").validationEngine('validate')){   
				 var id=$('.save').attr('id'); 
					var legalId=$('#legalId').val();
					var tableName=$('#tableName1').val();
					var legalRecordId=$('#legalRecordId').val();
					var createdDate=$('#createdDate').val();
					var effectiveDate=$('#effectiveDate').val();
					var legalDetail=$('#details').val();
					var propertyId=$('#propertyId').val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/legal_save.action", 
				 	async: false,
				 	data:{
				 		
				 		tableName:tableName,
				 		recordId:legalRecordId,
				 		createdDate:createdDate,
				 		effectiveDate:effectiveDate,
				 		legalDetail:legalDetail,
				 		propertyId:propertyId
				 		},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$("#main-wrapper").html(result); 
					}
				 }); 
				
			 }
			 else{
				 return false;
			 }
		 });

	 $('#effectiveDate').datepick();
	 $('#createdDate').datepick();
	 	var curDate=new Date().toString();
		var splitDate=curDate.split(" ");
		var day=splitDate[2];
		var mon=splitDate[1];
		var year=splitDate[3];
		fromDate=day+"-"+mon+"-"+year;
		$('#createdDate').val(fromDate);
});
function usecasePopupOpencall(){
	
	var tableName=$('#tableName1').val();
	var recordId=$('#recordId').val();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/legal_get_recordview.action", 
		data:{tableName:tableName,recordId:recordId},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			//alert(result);
			$("#UseCase_View_Temp").html(result); 
		}
 	});
	//$('#popup_open_trigger').trigger('click');

}
function useCaseViewCall(){
	tableName=$('#tableName1').val();
	legalRecordId=$('#legalRecordId').val();
	//alert("tableName"+tableName+"--recordId--"+recordId);
	url=null;
	if(tableName=="Property")
		url="getPropertyDetal.action";
	else if(tableName=="Contract")
		url="getPropertyContractDetail.action";
	else
		return false;
	if(url!=null && url!=""){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+url, 
			data:{format:"HTML",recordId:legalRecordId,approvalFlag:"N"},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#UseCase_View_Top_Result").html(result); 
				$("#UseCase_View_Top").slideDown("slow");
				$("table:first").addClass("table-custom-design");
				$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design");
				//$("table:first").find("tr:first").find("td:eq("+0+")").remove();
				//$("table:first").find("tr:first").find("td:eq("+2+")").remove();
			}
	 	});
	
	}else{
		alert("No Process Defined.");
	} 
	
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div>
			<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
			</c:if>
			<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
			</c:if>
		 </div>
		<div id="UseCase_View_Top" style="display:none;">
			<fieldset id="UseCase_View_Top_Result">
			</fieldset>
		</div>
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create Legal</div>	
		  <form name="componentEntry" id="componentEntryValidation" style="margin-left: 18px;position: relative;"> 
			<div class="portlet-content">  
				<fieldset>
				<div class="form_head">Legal Details</div>
				<div id="general-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="legalId" name="legalId" value="${LEGAL_INFO.legalId}"/>
				<div class="width100 float-left" id="hrm">  
					<div class="float-right  width48">  
						<fieldset style="height: 125px; margin-right: 20px;">
							
							<div>
								<label class="width30">Details</label> 
								<textarea id="details" name="details" class="width60"></textarea>
							</div>
							<div id="legal_document_information" style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
							</div>
							<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
								<span id="legalDocs"></span>
							</div>
							
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="height: 125px;">
								<div>
									<label class="width30"> 
									Legal Group<span style="color: red;">*</span></label>
									<select name="tableName1" id="tableName1" class="width50 validate[required]">
										<option value="">--Select--</option>
										<option value="Property">Property</option>
										<option value="Contract">Contract</option>
									</select>
									<input type="hidden" id="legalRecordId" name="legalRecordId"  class="width50 validate[required]"/> 
									<input type="hidden" id="propertyId" name="propertyId"  class="width50 validate[required]"/>
								</div> 
								<div style="display:none">
									<label class="width30">Number</label>
									<input type="text" id="recordNumber" name="recordNumber"  class="width50"/>
									<span class="button">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all component-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>
								</div>
								<div style="display:none">
									<label class="width30"> 
									 Name</label>
									<input name="recordName" type="text" id="recordName" class="width50">
								</div>
								<div><label class="width30">Effective Date<span style="color: red;">*</span></label>
									<input type="text" class="width50 dateCheck validate[required]" id="effectiveDate" value="${LEGAL_INFO.effectiveDate }" name="effectiveDate"  readonly="readonly" >
								</div>
								<div><label class="width30">Created Date</label>
									<input type="text" class="width50 dateCheck validate[required]" id="createdDate" value="${LEGAL_INFO.createdDate }" name="createdDate"  readonly="readonly"  disabled="disabled">
								</div>	
									 	
						</fieldset>
					</div> 
				 </div>
				 
			<input type="hidden" class="docflag" value="0"/>
			<div class="clearfix"></div> 
			
	</fieldset>
	
	<div id="UseCase_View_Temp" style="display:none;">
	</div>
	</div>
	<div id="popup_open_trigger"></div>  
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin-right:15px; margin-bottom: 5px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div>  
		<div class="portlet-header ui-widget-header float-right save" id="${showPage}" style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	 </div>  
	 <div class="clearfix"></div>
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="component-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="component-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix" ><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
  </form>
  </div> 
</div>