<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tr>
	<td colspan="5" class="tdidentity" id="childRowId_${rowid}"> 
		 <div class="width48 float-right" id="hrm">
				<fieldset>
					<legend>Offer Lines</legend> 
					<div>
						<label class="width30">${offerFor} Rent<span style="color: red;">*</span></label>
						<input type="text" name="rent" id="rent" class="width40 validate[required,custom[onlyFloat]]" readonly="readonly">
					</div>
					<div>
						<label class="width30">Offer Amount<span style="color: red;">*</span></label>
						<input type="text" name="offerAmount" id="offerAmount" class="width40 validate[required,custom[onlyFloat]] ">
						 
					</div>  
				</fieldset> 
		 </div>  
		  <div class="width50 float-left" id="hrm">
			<fieldset>
				<legend>Offer Lines</legend> 
				<div>
					<label class="width30">Line Number</label> 
					<input type="text" readonly="readonly"  class="width20" name="lineNumber" id="lineNumber">						
				</div> 
				<div>
					<label class="width30">${offerFor} Name</label> 
					<input type="text" readonly="readonly"  class="width50" name="offerFor" id="offerFor">	
					<input type="hidden" name="offerForId" id="offerForId">	
					<input type="hidden" name="offer" id="offer">					
				</div> 
			</fieldset>	
		</div>  
		 <div class="clearfix"></div> 
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"> 
		 	<input type="hidden" id="showPage" value="${showPage}"/>
			<div class="portlet-header ui-widget-header float-right rowcancel" id="cancel_${rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right rowsave" id="rowsave_${rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div> 
	</td>  
</tr>
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0); 
var componentId=0;
var unitId=0;
var propertyId=0;
$(function(){  	
	
	$('.rowcancel').click(function(){ 
		$('.formError').remove();			 
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if(showPage=="editedit"){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			$("#DeleteImage_"+rowid).show();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 } 
	});

	$('.rowsave').click(function(){
		 //Find the Id for fetch the value from Id
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	
		 
		 /*var offerLineId=$('#offerLineId').val();
		 var offerId=$('#offerId').val(); 
		 if(offerId!=null && offerId!="" && offerId>0){
		 }
		 else{
			 offerId=0;
		 }
		 offerLineId=$('#offerlineId_'+rowid).val(); 
		 if(offerLineId!=null && offerLineId!="" && offerLineId>0){
		 }
		 else{
			 offerLineId=0;
		 }*/
		 
		 var offer=$('#offer').val();
		 if(offer=="Component"){
			 componentId=$('#offerForId').val();
		 }
		 else if(offer=="Unit"){
			 unitId=$('#offerForId').val();
		 }
		 else if(offer=="Property"){
			 propertyId=$('#offerForId').val();
		 }
		 var offerAmount=$('#offerAmount').val();   
		 var showPage=$('#showPage').val();   
		
		 var url_action=""; 
		 if(showPage=="editedit")
			 url_action="offer_editedit_linesave";
		 else if(showPage=="editadd")
			 url_action="offer_editadd_linesave";
		 else if(showPage=="addedit")
			 url_action="offer_addedit_linesave";
		 else if(showPage=="addadd")
			 url_action="offer_addadd_linesave";
		 else{
			 $('#othererror').hide().html("There is no Action mapped.").slideDown(1000);
			 return false;
		 } 
		 if($("#offerEntryValidation").validationEngine({returnIsValid:true})){      
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false, 
				 	data:{	componentId:componentId, unitId:unitId, propertyId:propertyId, offerAmount:offerAmount, id:rowid, showPage:showPage 
					 	 },
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tempresult').html(result); 
					     if(result!=null) {
                             flag = true; 
					     } 
				 	},  
				 	error:function(result){
				 	  $('.tempresult').html(result);
                     $("#othererror").html($('#returnMsg').html()); 
                     return false;
				 	} 
	          });
			 if(flag==true){   
		        	//Bussiness parameter 
	        		$('#offeramount_'+rowid).text(offerAmount);   
					//Button(action) hide & show 
        			 $("#AddImage_"+rowid).hide();
        			 $("#EditImage_"+rowid).show();
        			 $("#DeleteImage_"+rowid).show();
        			 $("#WorkingImage_"+rowid).hide(); 
	        		 $("#childRowId_"+rowid).remove();
					//Row count+1
					if(showPage=="addadd" || showPage=="editadd"){
						var childCount=Number($('#childCount').val());
						childCount=childCount+1;
						$('#childCount').val(childCount);
					}
	        	}
		 }
		 else{
			 return false;
		 } 
	}); 
});	
</script>