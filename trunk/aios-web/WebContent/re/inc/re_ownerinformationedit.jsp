 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <style>
 
   td input[type='checkbox']
 {
 	width:20%;
 	border: 0px none;
 	margin-left:5px;
 }
 </style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';
var slidetab="";
var accountType="";
var rowidentity;
var actionVal="";

$(document).ready(function (){  

	$('#country').val($('#countryTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#ownerType').val($('#tempowner').val());
	$('#companyPersonId').val($('#companyPersonIdTemp').val());
	$('#existingCompanyId').val($('#companyPersonIdTemp').val());
	if($('#endPicker').val() == '31-Dec-9999'){
		$('#endPicker').val("")
	}
	ownerType=$('#ownerType').val();
	if(ownerType == '1'){
		$('#passportNoDiv').fadeOut();
	}else{
		$('#passportNoDiv').fadeIn();
	}
	

	$("#ownerInformationEdit").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 });

	$(".cancel").click(function(){
		$('#loading').fadeIn(); 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/owner_information_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
				$('#common-popup').dialog('destroy');		
				 $('#common-popup').remove();
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#common-popup').dialog('destroy');		
				 $('#common-popup').remove();
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 

	$('.update').click(function(){ 
		$('.tempresultfinal').fadeOut();
		if($("#ownerInformationEdit").validationEngine({returnIsValid:true})){
			var fromdate=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
	        var birthDate=Number(formatDate(new Date(getDateFromFormat($(".birthDate").val(),fromFormat)),toFormat));
			if(birthDate < fromdate){
				var ownerId=$('#ownerId').val();
				var ownerName=$('#companyPersonId :selected').text();
				var ownerType=Number($('#ownerType').val());
				var proffession=$('#proffession').val();
				var uaeIdNo=$('#uaeIdNo').val();
				var passportNo=$('#passportNo').val();
				birthDate=$('.birthDate').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var address1=$('#address1').val();
				var address2=$('#address2').val();
				var country=$('#country').val();
				var state=$('#state').val();
				var city=$('#city').val();
				var emailId=$('#emailId').val();
				var mobileNo=$('#mobileNo').val();
				var landlineNo=$('#landlineNo').val();
				var companyId=$('#companyPersonId').val();
				var website=$('#website').val();
				var fromdateformat=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
				var todateformat=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));

				//Accounting
				var liabilityAccountCode = $('#liabilityAccountCode').val();
				var advanceAccountCode = $('#advanceAccountCode').val();
				var clearningAccountCode = $('#clearningAccountCode').val();

				var codeArray=new Array();
				codeArray=liabilityAccountCode.split('---');
				liabilityAccountCode=codeArray[0];  
				codeArray="";
				codeArray=advanceAccountCode.split('---');
				advanceAccountCode=codeArray[0];	

				codeArray="";
				codeArray=clearningAccountCode.split('---');
				clearningAccountCode=codeArray[0];
				
		       if(true){
		    	   $('#loading').fadeIn();
					$.ajax({
						type: "POST",  
						url: "<%=request.getContextPath()%>/owner_information_update.action",   
				     	async: false,
				     	data: {ownerId: ownerId,ownerName: ownerName,ownerType:ownerType,proffession: proffession,uaeIdNo: uaeIdNo,
							passportNo: passportNo,birthDate: birthDate,fromDate: fromDate,
							toDate: toDate,address1: address1,  address2: address2, country: country,  state: state,
							city: city,emailId: emailId, mobileNo: mobileNo,landlineNo: landlineNo,website: website,companyId: companyId,
							liabilityAccountCode: liabilityAccountCode,advanceAccountCode: advanceAccountCode, clearningAccountCode: clearningAccountCode},
					    dataType: "html",
						cache: false,
						success:function(result){	
					 		$('.formError').hide(); 
					 		$('.error').hide();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#common-popup').dialog('destroy');		
							 $('#common-popup').remove();
					 	},  
					 	error:function(result){  
					 		$('.formError').hide(); 
					 		$('.error').hide();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#common-popup').dialog('destroy');		
							 $('#common-popup').remove();
					 	} 
					});
					$('#loading').fadeOut();
				}else{
						$('.error').show();
						$('.error').html("'To Date' should be greater than 'From Date' !!!").slideDown();
						return false;
				}
			}else{ 
				$('.error').html("From Date should be greater than Birth Date !!!").slideDown();
				return false;
			}
		}else{ 
			return false;
		}
		return true;
	}); 

	$('#ownerType').change(function(){
		ownerType=$('#ownerType').val();
		if(ownerType == '1'){
			$('#passportNoDiv').fadeOut();
		}else{
			$('#passportNoDiv').fadeIn();
		}
		$('#loading').fadeIn();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/owner_information_load_company.action", 
	     	async: false,
	     	data: {ownerType: ownerType},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#companyPersonDiv').fadeIn();
				$("#companyPersonId").html(result); 
			}, 
			error: function() {
			}
		}); 
		$('#loading').fadeOut();
	});

	$('#companyPersonId').change(function(){ 
		 $('#status').hide();
		 $('#availErr').hide(); 
		 $('#temperror').hide();
		 var companyPersonId = $("#companyPersonId").val();
		 var existingCompanyId = $("#existingCompanyId").val();
		 ownerType = $('#ownerType').val();
		 $('#checking').show();  
		 $("#checking").html('<img align="absmiddle" src="../images/loader.gif" /> Checking availability...');
		var msg="";
		 $.ajax({
			 type: "POST",
			 url: "<%=request.getContextPath()%>/owner_information_checking_available_edit.action", 
			 data: {companyId: companyPersonId,ownerType: ownerType, existingCompanyId: existingCompanyId },
			  dataType: "html",
			 success: function(result){  
				 $('.tempresult').html(result); 
				msg=$('#resultMessage').html().trim().toUpperCase(); 
				 if(msg == 'OK'){  
					 $('#status').show(); 
					 $('#checking').hide();  
					 $('#status').html(' <img align="absmiddle" src="../images/accepted.png" /> ');
					 $('#availflag').val(1);
				 } 
				 else{ 
					 $('#status').hide();
					 $('#checking').hide();  
					 $('#availErr').hide().html(msg).slideDown(1000); 
					 $('#availflag').val(0);
				 }
			}
		});	 
	 });

	//Get State List On Change Country List
	$('#country').change(function(){
		countryId=$('#country').val();
		$('#loading').fadeIn();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/property_info_state_load.action", 
	     	async: false,
	     	data:{countryId: countryId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#state").html(result); 
			}, 
			error: function() {
			}
		}); 
		$('#loading').fadeOut();		
	});

	//Get City List On Change State List
	$('#state').change(function(){
		stateId=$('#state').val();
		$('#loading').fadeIn();
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/property_info_city_load.action", 
	     	async: false,
	     	data:{stateId: stateId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#city").html(result); 
			}, 
			error: function() {
			}
		}); 	
		$('#loading').fadeOut();	
	});

	$('.common-popup').click(function(){	
		$('.formError').remove();	 
	    tempid=$(this).parent().get(0);  
	    accountType=$(this).attr('id'); 
	   	var commonaction="";
		actionVal="";
		var ledgerId=Number($('#headerLedgerId').val()); 
		commonaction=$($(tempid).siblings().get(2)).attr('id');
	      
		if(commonaction=="reterive_liability_account_action"){
   	    		commonaction="common_retrieveaccount";   
   	    		actionVal="vendor";
		}
		else if(commonaction=="reterive_advance_account_action"){
   	    		commonaction="common_retrieveaccount"; 
   	    		actionVal="advance";
		}else if(commonaction=="reterive_clearning_account_action"){
   	    		commonaction="common_retrieveaccount";
   	    		actionVal="clearing";
   	  	}
   	      
		$('#common-popup').dialog('open');
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+commonaction+".action", 
			 	async: false, 
			 	data:{ledgerId:ledgerId, accountType:accountType},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.common_result').html(result); 
				},
				error:function(result){ 
					 $('.common_result').html(result); 
				}
		}); 
	});	 
			
	$('#common-popup').dialog({ 
		autoOpen: false,
		minwidth: 'auto',
		width:700,
		bgiframe: false,
		modal: false,
		buttons: {
			"Cancel": function() {  
					$(this).dialog("close");  
			}, 
			"Ok": function(){
				if(actionVal!=null && actionVal!=""){ 
					var s = $("#list2").jqGrid('getGridParam','selrow');
					if(s == null){
						alert("Please select one account code");
						return false;
					}
					else{			
						var s2 = $("#list2").jqGrid('getRowData',s);   
						var commonCodes=s2.commonCode;
						var commonDescs=s2.commonDesc;  
						
						var joinCode= commonCodes+"---"+commonDescs;
						 
						if(actionVal=="vendor")
							$('#liabilityAccountCode').val(joinCode);
						else if(actionVal=="advance") 
							$('#advanceAccountCode').val(joinCode);
						else if(actionVal=="clearing") 
							$('#clearningAccountCode').val(joinCode);
					} 
				}
				$(this).dialog("close");  
			}				
		}	
	});  
				
	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
		 $('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#startPicker,#endPicker').datepick({
		 onSelect: customRange, showTrigger: '#calImg'
	});

	// Default Date
	$('#defaultActualPicker').datepick({
		maxDate: -1, showTrigger: '#calImg'
	});
});

	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	    var daysInMonth =$.datepick.daysInMonth(
	    $('#selectedYear').val(), $('#selectedMonth').val());
	    $('#selectedDay option:gt(27)').attr('disabled', false);
	    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	    if ($('#selectedDay').val() > daysInMonth) {
	        $('#selectedDay').val(daysInMonth);
	    }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.ownerinformation"/></div>
			<div  class="response-msg error searcherror ui-corner-all" style="width:80%; display:none;"></div>
			<div class="portlet-content">
					<div style="display:none;"  class="tempresult"></div>
					<div style="display:none;" class="tempresultfinal">
						<c:if test="${requestScope.bean != null}">
							<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
							 <c:choose>
								 <c:when test="${bean.sqlReturnStatus == 1}">
									<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
								<c:when test="${bean.sqlReturnStatus != 1}">
									<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
							</c:choose>
						</c:if>    
					</div>	 
				  <form name="ownerInformationEdit" method="post" id="ownerInformationEdit"> 
					<div class="width50 float-right" id="hrm"> 
						<fieldset>
							<legend><fmt:message key="re.owner.info.ownerinformation2"/></legend>					
								<div>
									<label class="width30"><fmt:message key="re.owner.info.address1"/><span style="color:red">*</span></label>
									<input type="text" name="address1" class="width30 validate[required] tooltip" TABINDEX=10 id="address1" value="${bean.address1}">
								</div>
								 <div>
									<label class="width30"><fmt:message key="re.owner.info.address2"/></label>
									<input type="text" name="sno" class="width30 tooltip" id="address2" TABINDEX=11 value="${bean.address2}">
								</div>
						 		<div>
								 	<label class="width30"><fmt:message key="re.owner.info.country"/><span style="color:red">*</span></label>
								 	<input type="hidden" id="countryTemp" name="cityTemp" value="${bean.countryId}" class="width30">
										<select id="country" class="width30 validate[required]" TABINDEX=12>
											<option value="">-Select-</option>
											<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
												<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
											</c:forEach>
										</select>
								</div>
								<div>
									<label class="width30"><fmt:message key="re.owner.info.state"/><span style="color:red">*</span></label>
									<input type="hidden" id="stateTemp" name="cityTemp" value="${bean.stateId }" class="width30">
										<select class="width30 validate[required]" id="state" TABINDEX=13>
											<option value="">-Select-</option>
											<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
												<option value="${statelist.stateId }">${statelist.stateName}</option>
											</c:forEach>
										</select>
								</div>	
								<div>
									<label class="width30"><fmt:message key="re.owner.info.city"/><span style="color:red">*</span></label>
									<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" class="width30">
										<select  class="width30 validate[required]" id="city" TABINDEX=14>
											<option value="">-Select-</option>
											<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
												<option value="${citylist.cityId }">${citylist.cityName}</option>
											</c:forEach>
										</select>
								</div>														
								<div>
									<label class="width30"><fmt:message key="re.owner.info.emailid"/><span style="color:red">*</span></label>
									<input type="text" name="sno" class="width30 validate[required,custom[email]]" id="emailId" TABINDEX=15 value="${bean.emailId}">
								</div>
					            <div>
					            	<label class="width30"><fmt:message key="re.owner.info.mobileno"/><span style="color:red">*</span></label>
					            	<input type="text" name="sno" class="width30 validate[required,custom[onlyNumber]]" TABINDEX=16 id="mobileNo" value="${bean.mobileNo}">
					            </div>
					            <div>
					            	<label class="width30"><fmt:message key="re.owner.info.landlineno"/></label>
					            	<input type="text" name="sno" class="width30" id="landlineNo" TABINDEX=17 value="${bean.landlineNo}">
					            </div>
					             <div>
	                            	<label class="width30"><fmt:message key="re.owner.info.website"/></label>
	                            	<input type="text" name="website" class="width30" id="website" value="${bean.website}" TABINDEX=18>
	                            </div>
						</fieldset>  																		
					</div> 
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="re.owner.info.ownerinformation1"/></legend>
								<div style="display:none;"><label class="width30">Owner Id No.</label><input type="text" name="drno" class="width30 tooltip" id="ownerId" disabled="disabled" value="${bean.ownerId}"></div>
								<div  style="display:none;">
									<label class="width30"><fmt:message key="re.owner.info.ownername"/><span style="color:red">*</span></label>
									<input type="text" name="sno" class="width30 validate[required,custom[onlyLetter]]" TABINDEX=1 id="ownerName" value="${bean.ownerName}">
								</div>
								<div><label class="width30"><fmt:message key="re.owner.info.ownertype"/><span style="color:red">*</span></label>
								<input type="hidden" id="tempowner" name="cityTemp" value="${bean.ownerType}" class="width30">
										<select class="width30 validate[required]" id="ownerType" TABINDEX=2>
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.ownerTypeList}" var="typelist" varStatus="status">
											<option value="${typelist.ownerType}">${typelist.ownerTypeName}</option>
										</c:forEach>
										</select>
								</div>	
								<div id="companyPersonDiv" style="">
									<label class="width30"><fmt:message key="re.owner.info.companyperson"/><span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${bean.ownerType eq 1}">
											<input type="hidden" id="companyPersonIdTemp" value="${bean.companyId}" class="width30">
										</c:when>
										<c:when test="${bean.ownerType eq 2}">
											<input type="hidden" id="companyPersonIdTemp" value="${bean.personId}" class="width30">
										</c:when>
										<c:otherwise>
											<input type="hidden" id="companyPersonIdTemp" value="${bean.personId}" class="width30">
										</c:otherwise>
									</c:choose>
									<input type="hidden" id="existingCompanyId" class="width30">
									<select class="width30 validate[required]" id="companyPersonId" TABINDEX=3 >
										<option value="">-Select-</option>
										<c:choose>
									 		<c:when test="${beanCom != null }">
									 			<c:forEach items="${beanCom}" var="companyBean" varStatus="status" >
													<option value="${companyBean.companyId}">${companyBean.companyName}</option>
												</c:forEach> 
									 		</c:when>
									 	</c:choose> 
									</select>
									<div id="checking"></div>
									<div id="status" class="width5 float-right"></div>
									<div id="availErr" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>
								</div>			
								<div>
									<label class="width30"><fmt:message key="re.owner.info.proffession"/></label>
									<input type="text" name="Proffession" class="width30" id="proffession" value="${bean.proffession}" TABINDEX=4/>
								</div>
								<div><label class="width30" style="font-family: Arial,Verdan,Sans-Serif;font-size: 87%;"><fmt:message key="re.owner.info.civilidno"/></label>
									<input type="text" name="uaeIdNo" class="width30" id="uaeIdNo" value="${bean.uaeIdNo}" TABINDEX=5>
								</div>
								<div id="passportNoDiv">
									<label class="width30"><fmt:message key="re.owner.info.passportno"/><span style="color:red">*</span></label>
									<input type="text" name="passportNo" class="width30" id="passportNo" value="${bean.passportNo}" TABINDEX=6>
								</div>
								<div><label class="width30" style="font-family: Arial,Verdan,Sans-Serif;font-size: 87%;"><fmt:message key="re.owner.info.birthdate"/><span style="color:red">*</span></label>
									<input type="text" name="birthDate" class="width30 birthDate validate[required]" readonly="readonly" TABINDEX=7 id="defaultActualPicker" value="${bean.birthDate}">
								</div>
						 		<div><label class="width30"><fmt:message key="re.owner.info.fromdate"/><span style="color:red">*</span></label>
						 			<input type="text" name="startPicker" class="width30 validate[required]" id="startPicker" TABINDEX=8 readonly="readonly" value="${bean.fromDate}">
						 		</div>		
			                    <div><label class="width30"><fmt:message key="re.owner.info.todate"/></label>
			                    	<input type="text" name="endPicker" class="width30" id="endPicker" TABINDEX=9 readonly="readonly" value="${bean.toDate}">
			                    </div>
						</fieldset> 
					</div>
					<div class="portlet-content">
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="accounts.suppliersite.label.accountinginformation"/></legend>
									<div class="width90 float-left"><label class="width30">Owner Account Code<span style="color:red;">*</span></label>
										<input type="text" class="width50 validate[required]" value="${bean.liabilityAccountCode}" name="liabilityAccountCode" id="liabilityAccountCode" readonly="readonly">
										<input type="hidden" id="reterive_liability_account_action"/>
										<span class="button" style="margin-top:3px;">
											<a  id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"></span> 
											</a>
										</span>
									</div>
									<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.advanceaccountcode"/><span style="color:red;">*</span></label>
										<input type="text" class="width50 validate[required]" value="${bean.advanceAccountCode}" name="advanceAccountCode" id="advanceAccountCode" readonly="readonly">
										<input type="hidden" id="reterive_advance_account_action"/>
										<span  class="button" style="margin-top:3px;">
											<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"></span> 
											</a>
										</span>	
									</div>	
									<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.clearningacccount"/><span style="color:red;">*</span></label>
										<input type="text" class="width50 validate[required]" value="${bean.clearningAccountCode}" name="clearningAccountCode" id="clearningAccountCode" readonly="readonly">
										<input type="hidden" id="reterive_clearning_account_action"/>
										<span class="button" style="margin-top:3px;">
											<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"></span> 
											</a>
										</span>	
									</div>																
							</fieldset> 
						</div>	
					</div>
					<div class="clearfix"></div>	
					<div class="width100 float-left buttons" Style="float: none !important;">
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div class="portlet-header ui-widget-header float-right cancel"><fmt:message key="re.owner.info.discard"/></div>
						<div class="portlet-header ui-widget-header float-right update"><fmt:message key="re.owner.info.update"/></div>
					</div>
				</div>
				<input type="hidden" name="availflag" id="availflag" value="0"/>	
			</form>
		</div>
	</div>
</div>
			
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="common_result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div> 			