<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style>
table.display td{
	padding:3px;
}
.ui-widget-header{
	padding:4px;
}
</style> 

<script type="text/javascript">
var id;
var oTable; var selectRow=""; var aData="";var aSelected = [];var inactiveId=""; 
$(document).ready(function (){ 
 	$('#unitcontractinactive').dataTable({ 
 		"sAjaxSource": "contract_jsoninactiveunitdetail.action?unitId="+unitId,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "aoColumns": [
   			{ "sTitle": 'Contract ID', "bVisible": false},
   			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
   			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
   			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
   			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
   			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
   			{ "sTitle": 'Options',"sWidth": "100px", "bVisible": false},
   			
   		],
 		"sScrollY": (Number($("#main-content").height() - 220) / 2 ) - 75,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#unitcontractinactive').dataTable();
 	/* Click event handler */
 	 	
 	$('#unitcontractinactive tbody tr').live('dblclick', function () {
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData =oTable.fnGetData( this );
			  inactiveId=aData[0];
	 	       callReportView();
		       return false;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          inactiveId=aData[0];
	 	      callReportView();
		      return false;
	      }
	});
 	
 	$('.contract_print').live('click',function(){
		var parentTR=$(this).parent().parent().get(0);
		var contractId=getId(parentTR); 

		$('#loading').fadeIn();
		window.open('getPropertyContractDetail.action?recordId='
				+ contractId + '&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
	}); 

});
function callReportView(){
	$('#loading').fadeIn();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getPropertyContractDetail.action", 
		data:{format:"HTML",recordId:inactiveId,approvalFlag:"N"},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$("#reporting-tree").html(result); 
			$("table:first").addClass("table-custom-design-report");
			$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
			$('#loading').fadeOut();
		}
 	});
}
</script>
<body>
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>InActive Contract Search Result(s)</div>	 
		<div class="portlet-content">
			<div id="gridResult">
				<table class="display dataTable" id="unitcontractinactive"></table>
			</div>
		</div>
</div>
</body>