<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
 <c:choose>
 	<c:when  test="${availSatus==1}"> 
 		  <div id="resultMessage">${requestScope.errMsg}</div>
 	</c:when>
 	<c:otherwise> 
 		<div id="resultMessage">${requestScope.succMsg}</div>
 	</c:otherwise> 
 </c:choose> 
 