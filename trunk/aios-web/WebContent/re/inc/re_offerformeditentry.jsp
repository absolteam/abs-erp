<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="offerFormAddEdit" id="offerFormAddEdit">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.offer.amountdetails"/></legend>
		<div>
			<label><fmt:message key="re.offer.rentamount"/><span style="color:red">*</span></label>
			<input type="text" name="rentAmount" id="rentAmount" class="width50  validate[required,custom[onlyFloat]]" disabled="disabled">
		</div>
		<div>
			<label><fmt:message key="re.offer.contractamount"/><span style="color:red">*</span></label>
			<input type="text" name="contractAmount" id="contractAmount" class="width50  validate[required,custom[onlyFloat]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
	<fieldset>
		<legend><fmt:message key="re.offer.linesdetails"/></legend> 
		<div>
				<label><fmt:message key="re.offer.lineno"/><span style="color:red">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50" disabled="disabled">
			</div>
			<div>
				<label><fmt:message key="re.offer.flatnumber"/><span style="color:red">*</span></label>	
				<input type="text" id="flatNumber" class="width30 validate[required]" disabled="disabled" />
				<input type="hidden" id="propertyFlatId" class="width30" disabled="disabled"/>
				<span id="hit7" class="button" style="width: 40px ! important;">
					<a class="btn ui-state-default ui-corner-all flat-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
	</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.offer.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.offer.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#offerFormAddEdit").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing button 
	        openFlag=0;			
		 });
		 $('#add').click(function(){
			  if($('#offerFormAddEdit').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var propertyFlatId=$('#propertyFlatId').val();
					var flatNumber=$('#flatNumber').val();
					var rentAmount=$('#rentAmount').val();
					var contractAmount=$('#contractAmount').val();

					var offerId=$('#offerId').val();				 
					var trnVal =$('#transURN').val(); 

					var actualLineId=$($($(slidetab).children().get(4)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					//alert("actualLineId : "+actualLineId+" uflag : "+uflag+" tempLineId : "+tempLineId);
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_edit_offer_form_update.action", 
					 	async: false,
					 	data: {offerId: offerId, lineNumber: lineNo,propertyFlatId: propertyFlatId,rentAmount: rentAmount,contractAmount: contractAmount, 
							trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(flatNumber);
		        		$($(slidetab).children().get(2)).text(rentAmount); 
		        		$($(slidetab).children().get(3)).text(contractAmount); 

		        		//$($(slidetab).children().get(4)).text(remarks);
		        		$($(slidetab).children().get(6)).text(propertyFlatId);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						openFlag=0;	
	
						$($($(slidetab).children().get(4)).children().get(0)).val($('#objLineVal').html());
				  		$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempId').html());
				  		$("#transURN").val($('#objTrnVal').html());
				   } 
			  }else{
				  return false;
			  }
		});

		 $('.flat-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
		       var buildingId=$('#buildingId').val();
		       var 	customerTypeId=$('#customerTypeId').val();		
				$('#offerform-popup').dialog('open');
				//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/offer_form_load_flat.action",
				 	async: false, 
				 	data: {buildingId: buildingId,customerTypeId: customerTypeId},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.offerform-result').html(result); 
					},
					error:function(result){ 
						 $('.offerform-result').html(result); 
					}
				}); 
			});
		$('#offerform-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			width:'50%',
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$('#flatNumber').val("");
					$('#rentAmount').val("");
					$('#contractAmount').val("");
					$(this).dialog("close"); 
				} 
			}
		});

		
});
 </script>	