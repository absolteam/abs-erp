<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			component name
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="components" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${COMPONENTLIST}" var="bean" varStatus="status">
	     		<option value="${bean.componentId}">${bean.componentName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 component_list" style="padding:3px;"> 
	     	<ul>
	     		<c:forEach items="${COMPONENTLIST}" var="bean" varStatus="status">
		     		<li id="component_${status.index+1}" class="componentlist_li" style="height: 20px;">
		     			<input type="hidden" value="${bean.componentId}" id="componentbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.componentName}" id="componentbynameid_${status.index+1}"> 
		     			<span id="componentbyname_${status.index+1}" style="cursor: pointer; position: absolute;" class="float-left">${bean.componentName}
		     				<span id="componentselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){
	$($($('#component-popup').parent()).get(0)).css('width',500);
	$($($('#component-popup').parent()).get(0)).css('height',250);
	$($($('#component-popup').parent()).get(0)).css('padding',0);
	$($($('#component-popup').parent()).get(0)).css('left',0);
	$($($('#component-popup').parent()).get(0)).css('top',100);
	$($($('#component-popup').parent()).get(0)).css('overflow','hidden');
	$('#component-popup').dialog('open');
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){ 
    	   $('#componentId').val($(this).val());
	       $('#componentName').val($(".autoComplete option:selected").text());
	       $('#component-popup').dialog("close"); 
	   } 
	}); 
	$('.componentlist_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.componentlist_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#componentselect_'+rowid).hasClass('selected')){ 
				 $('#componentselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#componentselect_'+rowid).addClass('selected');
	});
	
	$('.componentlist_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#componentName').val($('#componentbynameid_'+rowid).val());
		 $('#componentId').val($('#componentbyid_'+rowid).val()); 
		 $('#component-popup').dialog("close"); 
	});
	$('#close').click(function(){
		$('#component-popup').dialog("close");
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.component_list ul li{
		padding:3px;
	}
	.component_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat;  
	}
</style>