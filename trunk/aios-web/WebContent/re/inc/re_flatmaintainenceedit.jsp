<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var openFlag=0;
var slidetab;
var actualID;
$(function(){
	$('#contractId').val($('#contractIdTemp').val());
	$('#personId').val($('#personIdTemp').val());
	$('#ebBillStatusCode').val($('#ebBillStatusCodeTemp').val());
	$('#waterBillStatusCode').val($('#waterBillStatusCodeTemp').val());
	$('#releaseType').val($('#releaseTypeTemp').val());
	if($('#waterCertiDate').val() =='01-Jan-1900'){
		$('#waterCertiDate').val("");
	}
	if($('#defaultPopup').val() =='01-Jan-1900'){
		$('#defaultPopup').val("");
	}

	 $("#flatMaintainenceReleaseEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
    });

	 var td=$("#flatDescription").val(); 
		var max_numb = 500;//Set the Maximum Number of words
		var mn = td.length*100;//Multiply the lenght on words x 100

		var val= (mn / max_numb);//Divide it by the Max numb of words previously declared
		var cunt= max_numb - td.length;//Get Count of remaining characters

		if(td.length <= max_numb){
			$("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
			$('#count').html(cunt);//Output the count variable previously calculated into the div with id= count
			$('#progressbar').animate({//Increase the width of the css property "width"
			"width": val+'%',
			}, 1);//Increase the progress bar
		}
		else{
		    $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
		    $("#flatDescription").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
		}

	 $("#flatDescription").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#flatDescription").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#flatDescription").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 	

	    approvedStatus=$('#approvedStatus').val(); 
		if( approvedStatus == 'E'){
			$('#save').remove();
			$('#request').fadeOut();	
			$('#discard').fadeOut();	
			$('#close').fadeIn();
			$('#contractId').attr("disabled",true);
			$('#personId').attr("disabled",true);
			$('#flatDescription').attr("disabled",true);
			$('#waterCertiNo').attr("disabled",true);
			$('#waterCertiDate').attr("disabled",true);
			$('#electricCertiNo').attr("disabled",true);
			$('#defaultPopup').attr("disabled",true);
			$('#waterBillStatusCode').attr("disabled",true);
			$('#ebBillStatusCode').attr("disabled",true);
			$('.editFlat').fadeOut();	
			$('.addflat').fadeOut();	
			$('.delFlat').fadeOut();	
			$('.commonErr').show();
			$('.commonErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
		}
		if(approvedStatus == 'A' ){
			$('#save').remove();
			$('#request').fadeOut();	
			$('#discard').fadeOut();	
			$('#close').fadeIn();
			$('#contractId').attr("disabled",true);
			$('#personId').attr("disabled",true);
			$('#flatDescription').attr("disabled",true);
			$('#waterCertiNo').attr("disabled",true);
			$('#waterCertiDate').attr("disabled",true);
			$('#electricCertiNo').attr("disabled",true);
			$('#defaultPopup').attr("disabled",true);
			$('#waterBillStatusCode').attr("disabled",true);
			$('#ebBillStatusCode').attr("disabled",true);
			$('.editFlat').fadeOut();	
			$('.addflat').fadeOut();	
			$('.delFlat').fadeOut();
			$('.commonErr').hide().html("Workflow is already Approved, Cannot edit this screen.!!!").slideDown();
		}
		if(approvedStatus == 'R' ){
			$('#request').fadeIn();	
			$('#discard').fadeIn();	
			$('#save').fadeIn();
			$('.commonErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
		}

		$('#personId').focus(function(){
			$('.commonErr').fadeOut();
		});
		
	
	$('.formError').remove();
	//Save Material Despatch details
	$("#save").click(function(){
		if($('#flatMaintainenceReleaseEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var flatId = $('#flatId').val();
				var contractId = $('#contractId').val();
				var personId = $('#personId').val();
				var flatDescription = $('#flatDescription').val();
				var workflowStatus="Not Yet Approved";
				var companyId=Number($('#headerCompanyId').val());

				waterCertiNo=$('#waterCertiNo').val();
				waterCertiDate=$('#waterCertiDate').val();
				electricCertiNo=$('#electricCertiNo').val();
				electricCertiDate=$('#defaultPopup').val();
				releaseType=$('#releaseType').val();

				waterBillStatusCode=$('#waterBillStatusCode').val();
				ebBillStatusCode=$('#ebBillStatusCode').val();

				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if (true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_final_save_flat_maintainence.action",
						data: {contractId: contractId, personId: personId,flatDescription: flatDescription,flatId:flatId,
							headerFlag: headerFlag, waterCertiNo: waterCertiNo,electricCertiDate: electricCertiDate,ebBillStatusCode: ebBillStatusCode,
							waterCertiDate: waterCertiDate,electricCertiNo: electricCertiNo,waterBillStatusCode: waterBillStatusCode, trnValue: transURN,
							companyId:companyId,workflowStatus:workflowStatus,releaseType:releaseType}, 
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#loading').fadeOut(); 
						} 
					});
					 
				}else{
					$('.childCountErr').hide().html("Please Enter  Description").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Save Material Despatch details
	$("#request").click(function(){
		if($('#flatMaintainenceReleaseEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var contractId = $('#contractId').val();
				var personId = $('#personId').val();
				var flatDescription = $('#flatDescription').val();
				var flatId = $('#flatId').val();
				

				waterCertiNo=$('#waterCertiNo').val();
				waterCertiDate=$('#waterCertiDate').val();
				electricCertiNo=$('#electricCertiNo').val();
				electricCertiDate=$('#defaultPopup').val();
				releaseType=$('#releaseType').val();

				waterBillStatusCode=$('#waterBillStatusCode').val();
				ebBillStatusCode=$('#ebBillStatusCode').val();

				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var applicationId=Number($('#headerApplicationId').val());
				var workflowStatus="Request For Flat Maintenance Release";
				var wfCategoryId=Number($('#headerCategoryId').val());
				var functionId=Number($('#headerCategoryId').val());
				var functionType="request";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){					//if( (flatDescription != null && flatDescription.trim().length > 0)){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_request_flat_maintainence.action",
						data: {contractId: contractId, personId: personId,flatDescription: flatDescription,
							waterCertiNo: waterCertiNo,electricCertiDate: electricCertiDate,ebBillStatusCode: ebBillStatusCode,
							waterCertiDate: waterCertiDate,electricCertiNo: electricCertiNo,waterBillStatusCode: waterBillStatusCode,trnValue: transURN,
							companyId:companyId,applicationId:applicationId,workflowStatus:workflowStatus,wfCategoryId:wfCategoryId,
							functionId:functionId,functionType:functionType,sessionpersonId: sessionpersonId,flatId:flatId,releaseType:releaseType},  
						async: false,
						dataType: "html",
						cache: false,
						success:function(result){
								$('.formError').hide();
								$('.commonErr').hide();
								$('.tempresultfinal').fadeOut();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#request').remove();	
									$('#discard').remove();	
									$('#save').remove();
									$('#close').fadeIn();
									$('.commonErr').hide();
								} else{
									$('.tempresultfinal').fadeIn();
								}
							},  
						error:function(result){
								$('.tempresultfinal').fadeOut();
								$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$("#main-wrapper").html(result); 
								} else{
									$('.tempresultfinal').fadeIn();
								};
						 	}
					});
					 
				}else{
					$('.childCountErr').hide().html("Please Enter Description").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Amount Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Feature Details
 	$('.addflat').click(function(){ 
 		if(openFlag==0){
			if($('#flatMaintainenceReleaseEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		       //alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_flat_maintainence_redirect.action", 
					async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
	
					 $($($(slidetab).children().get(7)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(7)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(7)).children().get(3)).show();	//Processing Button

			         openFlag=1;
			         
			         $('#loading').fadeOut();
					}
				});
			}else{
				$('#warningMsg').hide().html("Please insert record ").slideDown();
			}
 		}	
	});
	
	$(".editFlat").click(function(){
		if(openFlag==0){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
		   	$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	       //actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
	       $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/edit_edit_flat_maintainence_redirect.action", 
				async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button

		         openFlag=1;
				 
		         var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
				 //var temp5= $($(slidetab).children().get(4)).text();
				 var temp5= $($(slidetab).children().get(4)).text();

				 //var temp7= $($(slidetab).children().get(8)).text();
				 var temp6= $($(slidetab).children().get(8)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp6);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
			  	//$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp6);
			
			  	$('#loading').fadeOut();
	
				},
				error:function(result){
					alert("Inelsepart"+result);
				}
			});
			
		}	
	});


   
 	$('.delFlat').click(function(){
 		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(6)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(6)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			 var flatId=$("#flatId").val();
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_flat_maintainence_update.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, flatId: flatId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
		       			childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
 		}	
 	});

 	$.fn.globeTotal = function() { 	// Total for Amount Calculation
		var total=0;
		$('.amountcalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#amountTotal').val(total);
	 }

	$("#discard").click(function(){
		$('#loading').fadeIn();
		var trnValue=$("#transURN").val();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_discard_flat_maintainence.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
   		});
	});

	$('#close').click(function(){
	   	 $('.formError').remove();
	   		$('#loading').fadeIn();
			 $.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/flat_maintainence_list.action",   
			     	async: false, 
					dataType: "html",
					cache: false,
					error: function(data) 
					{  
					},
			     	success: function(data)
			     	{
						$("#main-wrapper").html(data);  //gridDiv main-wrapper
						$('#loading').fadeOut();
			     	}
				});
			return true;
	 });
	
	$('.addrowsfeature').click(function(){
		var count=Number(0);
		$('.rowid').each(function(){
			count=count+1;
		});  
		var lineNumber=count;
 		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/flat_maintainence_editrow.action", 
		 	async: false,
			data:{lineNumber:lineNumber},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tab").append(result);
				if($(".tab").height()<255)
					 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tab").height()>255)
					 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	// Default Date
	$('#tenantReleaseLetterDate').datepick();

	$('#waterCertiDate').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});
});	 
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}	
	 
	
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.flatmaintenancerelease"/></div>
			<div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	
		
	 		<div class="portlet-content">
	 			<form id="flatMaintainenceReleaseEdit">
					<fieldset>
			 			<div class="width50 float-left" id="hrm">
							<fieldset style="min-height:100px;">
							<div style="display:none">
								<label>Person Id</label><input type="text" name="sessionpersonId" value="${PERSON_ID}" class="width40" id="sessionpersonId" disabled="disabled">
								<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
							</div>
								<div>
									<label><fmt:message key="re.property.info.contractnumber"/><span style="color:red">*</span></label>	
									 <input type="hidden" name="contractIdTemp" id="contractIdTemp" value="${bean.contractId }"/>
										<select id="contractId" class="width30 " disabled="disabled" >
											<option value="">-Select-</option>
											<c:if test="${requestScope.item ne null}">
												<c:forEach items="${requestScope.item}" var="contract" varStatus="status">
													<option value="${contract.contractId }">${contract.contractNo }--${contract.buildingName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
							 	<div>
									<label><fmt:message key="re.property.info.engineername"/><span style="color:red">*</span></label>	
									 <input type="hidden" name="personIdTemp" id="personIdTemp" value="${bean.personId }" />
										<select id="personId" class="width30 validate[required]">
											<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="person" varStatus="status">
													<option value="${person.personId }">${person.personName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<fieldset>
				                    <legend><fmt:message key="re.property.info.description"/></legend>
				                    <div style="height:25px" class="width60 float-left">
				                            <div id="barbox">
				                                  <div id="progressbar"></div>
				                            </div>
				                            <div id="count">500</div>
				                      </div>
				                      <p>
				                        <textarea class="width60 float-left tooltip" id="flatDescription" title="PLease Enter Description">${bean.flatDescription }</textarea>
				                      </p>
				                 </fieldset>
							</fieldset>
					 	</div>
					 	<div class="width50 float-right" id="hrm">
							<fieldset style="min-height:100px;">
								<div>
									<label><fmt:message key="re.property.info.watercertifino"/><span style="color:red">*</span></label>
									<input type="text" name="waterCertiNo" id="waterCertiNo" class="width30 validate[required]" value="${bean.waterCertiNo }" >
								</div>
								<div>
									<label><fmt:message key="re.property.info.watercertifidate"/></label>
									<input type="text" name="waterCertiDate" class="width30" id="waterCertiDate" readonly="readonly" value="${bean.waterCertiDate }">
								</div>
								<div>
									<label>Water Bill Status<span style="color:red">*</span></label>	
										<input type="hidden" name="waterBillStatusCodeTemp" id="waterBillStatusCodeTemp" value="${bean.waterBillStatusCode }" />
										<select id="waterBillStatusCode" class="width30 validate[required]">
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanStatus ne null}">
												<c:forEach items="${requestScope.beanStatus}" var="beanstatus" varStatus="status">
													<option value="${beanstatus.statusCode }">${beanstatus.statusName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.eleccertifino"/><span style="color:red">*</span></label>
									<input type="text" name="electricCertiNo" id="electricCertiNo" class="width30 validate[required]" value="${bean.electricCertiNo }">
								</div>
								<div>
									<label><fmt:message key="re.property.info.eleccertifidate"/></label>
									<input type="text" name="electricCertiDate" id="defaultPopup" class="width30" readonly="readonly" value="${bean.electricCertiDate }">
								</div>	
								<div>
									<label>EB Bill Status<span style="color:red">*</span></label>	
										<input type="hidden" name="ebBillStatusCodeTemp" id="ebBillStatusCodeTemp" value="${bean.ebBillStatusCode }" />
										<select id="ebBillStatusCode" class="width30 validate[required]">
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanStatus ne null}">
												<c:forEach items="${requestScope.beanStatus}" var="beanstatus1" varStatus="status">
													<option value="${beanstatus1.statusCode }">${beanstatus1.statusName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.releasetype"/><span class="mandatory">*</span></label>
									<input type="hidden" name="releaseTypeTemp" id="releaseTypeTemp" value="${bean.releaseType }" />
									<select id="releaseType" class="width30 validate[required]">
											<option value="">-Select-</option>
											<c:if test="${requestScope.releaseStatus ne null}">
												<c:forEach items="${requestScope.releaseStatus}" var="beanstatus1" varStatus="status">
													<option value="${beanstatus1.releaseType }">${beanstatus1.releaseTypeName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
							</fieldset>
					 	</div>
	 			</fieldset>
			</form>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.amountdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width10"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20">Condition/Status</th>
				            					<th class="width10"><fmt:message key="re.property.info.amount"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.remarks"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="rowid">  	 
													<td class="width10">${result.lineNumber}</td>
													<td class="width10">${result.maintainenceCode}</td>	
													<td class="width20">${result.conditionStatusName }</td>
													<td class="width10 amountcalc">${result.amount}</td> 
													<td class="width20">${result.remarks}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.flatChildId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addflat" style="cursor:pointer;"  title="Add this row" >
														 	 <span class="ui-icon ui-icon-plus"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFlat" style="cursor:pointer;" title="Edit this row" >
															<span class="ui-icon ui-icon-wrench"></span>
														</a>
														<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFlat" style="cursor:pointer;" title="Delete this Row" >
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="release_type"></td>
													<td style="display:none;">${result.conditionStatusCode}</td>		 
												</tr>  
								  		 	</c:forEach>	
										    <c:forEach var="i" begin="${countSize+1}" end="${countSize+2}">
											 	<tr class="rowid" style="display:none;">
											 		<td class="width10">${i}</td>
													<td class="width10"></td>	
													<td class="width20"></td>	
													<td class="width10 amountcalc"></td> 
													<td class="width20"></td>
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>  
													</td> 
													<td style=" width:5%;"> 
					  									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addflat" style="cursor:pointer;" title="Add this row" >
														 	 <span class="ui-icon ui-icon-plus"></span>
														</a>
														<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFlat" style="cursor:pointer;" title="Edit this row" >
															<span class="ui-icon ui-icon-wrench"></span>
														</a>
														<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFlat" style="cursor:pointer;" title="Delete this Row" >
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="release_type"></td>
													<td style="display:none;"></td>	 
												</tr>  
								  		 	</c:forEach>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
				    		<!--  <div class="float-right " style="display:none;">
							<label>Total</label>
						 	<input name="amountTotal" id="amountTotal" style="width:45%!important;" disabled="disabled" >
						 </div>-->
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" style="display:none;"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<input class="width40" type="hidden" name="offerId" id="flatId" disabled="disabled" value="${bean.flatId}" />
			<input class="width40" type="hidden" name="workflowStatus" id="workflowStatus" disabled="disabled" value="${bean.workflowStatus}" />
		 	<div class="float-right buttons ui-widget-content ui-corner-all">
		 			<div class="portlet-header ui-widget-header float-right discard" style="display:none;">Contract</div> 
		 			<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel"/></div> 
					<div class="portlet-header ui-widget-header float-right" id="request">Request For Verification</div>
					<div class="portlet-header ui-widget-header float-right save-all" id="save">Update</div> 
					<div style="display:none;" class="portlet-header ui-widget-header float-right" id="close">Close</div>
			</div>
 		</div>
 	</div>
 </div>
 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="flat-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="flaat-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>


	 	
