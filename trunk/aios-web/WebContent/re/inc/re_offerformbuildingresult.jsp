<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<%@page import="com.aiotech.aios.realestate.*"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.Property"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.vo.PropertyVO"%>
<%@page
	import="com.aiotech.aios.realestate.domain.entity.vo.ComponentVO"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.vo.UnitVO"%>

<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<%
List<ComponentVO> componentVos=new ArrayList<ComponentVO>();
	List<UnitVO> unitVos=new ArrayList<UnitVO>();
	PropertyVO propertyVO=(PropertyVO)ServletActionContext.getRequest().getAttribute("OFFER_PROPERTY_INFO");
		componentVos=propertyVO.getComponentVOs();
		Collections.sort(componentVos);
		int i=1; int j=1; 
		
		out.println("<div id=\"tableWrap\" class=\"wrap_in\">"); 
		out.println("<div class=\"reference\" align=\"center\">"+
						"<table  width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">"+
						"<tr>"+
						"<td title=\"Legend\" style=\"font-weight:bold; position: relative; float:left; top:7px; right:50px;\">Legend</td>"+
						"<td title=\"Available\"><img alt=\"Available\" src=\"./images/property/offer/leg_available.png\"></td>"+
						"<td style=\"font-weight:bold\">Available</td>"+
						"<td title=\"Mouse-over\"><img alt=\"Mouse-over\" src=\"./images/property/offer/leg_mouse_over.png\"></td>"+
						"<td style=\"font-weight:bold\">Mouse-over</td>"+
						"<td title=\"Selected\"><img alt=\"Selected\" src=\"./images/property/offer/leg_selected.png\"></td>"+
						"<td style=\"font-weight:bold\">Selected</td>"+
						"<td title=\"Offered\"><img alt=\"Offered\" src=\"./images/property/offer/leg_offered.png\"></td>"+
						"<td style=\"font-weight:bold\">Offered</td>"+ 
						"<td title=\"Rented\"><img alt=\"Rented\" src=\"./images/property/offer/leg_rented.png\"></td>"+
						"<td style=\"font-weight:bold\">Rented</td>"+
						"<td title=\"Sold\"><img alt=\"Sold\" src=\"./images/property/offer/leg_sold.png\"></td>"+
						"<td style=\"font-weight:bold\">Sold</td>"+
						"<td title=\"Pending\"><img alt=\"Pending\" src=\"./images/property/offer/leg_pending.png\"></td>"+
						"<td style=\"font-weight:bold\">Pending</td>"+  
						"<td title=\"Renovation\"><img alt=\"Renovation\" src=\"./images/property/offer/leg_renovation.png\"></td>"+
						"<td style=\"font-weight:bold\">Renovation</td>"+ 
						"</tr></table></div>");
		out.println("<div style='display:none'>");
		out.println("<span id=\"propertyName\">"+propertyVO.getPropertyName()+"</span>");
		out.println("<input type=\"hidden\" id=\"propertyId\" value="+propertyVO.getPropertyId()+">");
		out.println("<input type=\"hidden\" id=\"propertyRent\" value="+propertyVO.getRentYr()+">");
		out.println("<input type=\"hidden\" id=\"propertyStatus\" value="+propertyVO.getStatus()+">");
		out.println("<input type=\"hidden\" id=\"propertyType\" value="+propertyVO.getPropertyType().getType()+">");
		out.println("<input type=\"hidden\" id=\"propertyFloors\" value="+propertyVO.getFloors()+">");
		out.println("<input type=\"hidden\" id=\"offeredNumber\" value="+propertyVO.getOfferNumber()+">");
		out.println("<input type=\"hidden\" id=\"tenantName\" value="+propertyVO.getTenantName()+">");
		out.println("</div><div class='mainarea' align=\"center\">");
		out.println("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"margin:10px;!important\" id=\"tableid\" class=\"popup\">");
		if(propertyVO.getPropertyType().getType().equalsIgnoreCase("villa")){
			out.println("<tr id=component_"+i+" class='componentTR'>"+
							"<td class=trtd style=\"width:90%;height:100px; cursor:pointer;\" id=trtd_"+i+">"+
								"<span id=\"popupComponentName_"+i+"\" style=\"display:none;\">"+propertyVO.getPropertyName()+"</span>"+
							"</td>"+
						"</tr>");  
		}
		else{
			for(ComponentVO compvo:componentVos ){ 
				out.println("<tr id=component_"+i+" class='componentTR'>"); 
				unitVos=new ArrayList<UnitVO>();
				unitVos=compvo.getUnitVOs();
				Collections.sort(unitVos); 
				if(unitVos!=null && unitVos.size()>0){
					out.println("<td title="+compvo.getComponentName()+" class='trtd tooltip' style=\"width:5%;height:20px;\" id=trtd_"+i+">"+
									"<span id=\"componentStatus_"+i+"\" style=\"display:none;\">"+compvo.getStatus()+"</span>"+
									"<span id=\"popupComponentName_"+i+"\" style=\"display:none;\">"+compvo.getComponentName()+"</span>"+
									"<span class='componentrent' id=componentRent_"+i+" style=\"display:none;\">"+compvo.getRent()+"</span>"+
									"<span class='componentid' id=componentId_"+i+" style=\"display:none;\">"+compvo.getComponentId()+"</span>"+
									"<span class='componentUnitSize' id=componentUnitSize_"+i+" style=\"display:none;\">"+compvo.getNoOfUnits()+"</span>"+
									"<span class='componenttype' id=componentType_"+i+" style=\"display:none;\">"+compvo.getComponentType().getType()+"</span>"+
									"<img src=\"images/property/offer/compt_available.png\" class=\"componentimg\" id=\"componentimg_"+i+"\" alt='Coponent'/>"+
								"</td>");
					for(UnitVO univo: unitVos){ 
						out.println("<td class='showTD showPopupTD tabletdid tooltip-csss' id=\'units_"+j+"'>"+
										"<input type='hidden' name='unitStatus' id=unitStatus_"+j+" value="+univo.getStatus()+">"+
										"<span class='unitname' id=popupUnitName_"+j+" style=\"display:none;\">"+univo.getUnitName()+"</span>"+
										"<span class='unitRent' id=unitRent_"+j+" style=\"display:none;\">"+univo.getRent()+"</span>"+
										"<span class='unitid' id=unitId_"+j+" style=\"display:none;\">"+univo.getUnitId()+"</span>");   
										if(univo.getOfferNumber()>0){
											out.println("<span class=\"classic\">"+univo.getUnitName()+
												"<br/>"+univo.getOfferNumber()+
												"<br/>"+univo.getTenantName()+"</span>");	 
										} 
										else{
											out.println("<span class=\"classic\">"+univo.getUnitName()+"</span>");	
										}
										out.println("<div align=\"center\" id=\"div_unit_img_"+j+"\"><img id=\"unitType_"+j+"\" alt="+univo.getUnitType().getType()+" class=\"imgSize\"></div>"+  
									"</td>");
						j++;
					}
				}else{
					out.println("<td class='trtd float-left' style=\"width:5%; cursor:pointer; 	height:20px;\" id=trtd_"+i+">"+
									"<span id=\"componentStatus_"+i+"\" style=\"display:none;\">"+compvo.getStatus()+"</span>"+
									"<span id=\"popupComponentName_"+i+"\" style=\"display:none;\">"+compvo.getComponentName()+"</span>"+
									"<span class='componentrent' id=componentRent_"+i+" style=\"display:none;\">"+compvo.getRent()+"</span>"+
									"<span class='componentid' id=componentId_"+i+" style=\"display:none;\">"+compvo.getComponentId()+"</span>"+	
									"<span class='componentUnitSize' id=componentUnitSize_"+i+" style=\"display:none;\">"+compvo.getNoOfUnits()+"</span>"+
									"<span class='componenttype' id=componentType_"+i+" style=\"display:none;\">"+compvo.getComponentType().getType()+"</span>"+
							   "</td>");
				} 
				out.println("</tr>");
				i++; 
			}
		}
		out.println("</table></div>"); 
		out.println("<div class=\"main_index\">"+
						" <div class=\"reference_keys\"><img src='images/property/offer/room.png' align=\"left\" /><p align=\"right\">Appartments</p></div>"+
						" <div class=\"reference_keys\"><img src='images/property/offer/ofice.png' align=\"left\" /><p align=\"right\">Offices</p></div>"+
						" <div class=\"reference_keys\"><img src='images/property/offer/cafe.png' align=\"left\" /><p align=\"right\">Shops/Resturants</p></div><br />"+
						"<p style=\"font-weight:bold;\">"+
							"Property Name : "+	propertyVO.getPropertyName()+"<br/>"+
							"Component Size :  <span class=\"componentsize\"></span><br/>"+
							"Unit Size : <span class=\"unitsize\"></span></p>"+
					"</div>");
		out.println("</div>");

%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript"> 
var slidetab = "";
var childslidetab = ""; var currentTD=""; var currentTR="";
var tdArray = new Array();
var greatestTRTD = 0; var unitStatus="";
var currentTRTD = 0; var flag=false;
var returnMessage="";
//popup vars
var popupUnitName="";
var popupComponentName="";

//for getting selections
var componentArray=new Array(2);
var unitArray= new Array();
var componentFlag="";
var unitFlag=""; 
var componentList="";
var unitList=""; var counter=0; 
$(function(){    
	componentList="";
	unitList=""; 
	//calScreenResolution(); 
	checkPropertyStatus(); 
	if($('#propertyType').val()!="Villa"){  
		$('.componentTR').each(function(){
			slidetab = $(this);
			checkTRStatus(slidetab);
			var thisTDSize = $(slidetab).children().size()-1; 
			for ( var i = 0; i<thisTDSize; i++) {
				tdArray[i] = thisTDSize; 
			} 
			greatestTRTD = Number(Math.max.apply(Math, tdArray)); 
			++counter;
		}); 
		$('.componentsize').html(counter);
		unitColSpan(greatestTRTD);  
	}
	else{
		var propertyStatus=$('#propertyStatus').val();
		var rowId=getRowId($(this).attr('id'));   
		if(propertyStatus==2){ 
			$('#unitType_'+rowId).removeClass().addClass('pending');
		}
		else if(propertyStatus==3){
			$('#unitType_'+rowId).removeClass().addClass('rented');
		}
		else if(propertyStatus==4){
			$('#unitType_'+rowId).removeClass().addClass('sold');
		}
		else if(propertyStatus==5){
			$('#unitType_'+rowId).removeClass().addClass('renovation');
		}
		else if(propertyStatus==6){
			$('#unitType_'+rowId).removeClass().addClass('offered');
		}else{
			$('#unitType_'+rowId).removeClass().addClass('bigroom');
		}
	}
	 
	$('#tableid').dblclick(function(){ 
		if($('#propertyType').val()!="Villa"){
			//if($('#tableid').hasClass('complete')){
				$('.componentTR').each(function(){ 
					slidetab = $(this); 
					var currentTD="";
					var unitTDSize= $(slidetab).children().size();  
					for(var m=1;m<unitTDSize;m++){
						currentTD=$(slidetab).children().get(m); 
						var tempvar=$(currentTD).attr("id");  
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]);  
						
						var tempvar0=$($($(slidetab).children()).get(0)).attr("id");  
						var idarray0 = tempvar0.split('_');
						var rowid0=Number(idarray0[1]);  
						
						unitStatus=$('#unitStatus_'+rowid).val(); 
						var componentStatus=$('#componentStatus_'+rowid0).html();  
						
						if((unitStatus==1 || unitStatus==6) || (componentStatus==1 || componentStatus==6)){ 
							var rowId=getRowId($(currentTD).attr('id')); 
							if(	$('#div_unit_img_'+rowId).hasClass('selected')){  
								flag=true;  
							}
							else{  
								flag=false;  
								break;
							}
						} 
					} 
				});  
				callAllSelected(flag);
			/* }
			else{ 
				$('#tableid').css("cursor","not-allowed"); 
			} */
		}
		else{ 
			var propertyStatus=$('#propertyStatus').val();
			var villaTD=$($('.componentTR').children()).get(0);  
			if(propertyStatus==1 || propertyStatus==6){  
				$('#tableid').css("cursor","pointer");
				 if($(villaTD).hasClass('selected')){ 
					 $($(villaTD)).removeClass('selected'); 
					 $($(villaTD)).css('background-color', '#DDECFB'); 
				 }
				 else{  
					$($(villaTD)).css('background-color', '#10FF10'); 
					$($(villaTD)).addClass('selected');  
				 } 
			}
			else{
				$($(villaTD)).css('cursor','not-allowed');  
			}
		}
	});
	
	/* $('#tableid').live('mouseenter', function(){  
		if($('#tableid').hasClass('incomplete')){
			$('#tableid').css("cursor","not-allowed"); 
		} 
	}); */
	
	$('.trtd').bind('mouseenter', function() { 
		//if($('#tableid').hasClass('complete')){
			slidetab=$($(this).parent()).get(0); 
			var currentTD=$($(slidetab).children()).get(0); 
			var unitTDSize= $(slidetab).children().size();  
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);  
			popupComponentName=$('#popupComponentName_'+rowid).html();  
			var componentStatus=$('#componentStatus_'+rowid).html();  
			 
			for(var m=1;m<=unitTDSize;m++){
				var currentTD=$(slidetab).children().get(m);
				var tempvar=$(currentTD).attr("id");  
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]);  
				unitStatus=$('#unitStatus_'+rowid).val();
				if(componentStatus==1 || componentStatus==6){ 
					$($(slidetab).children().get(0)).css('cursor','pointer'); 
					if((unitStatus==1 || unitStatus==6)){ 
						var rowId=getRowId($(currentTD).attr('id')); 
						if(	$('#div_unit_img_'+rowId).hasClass('selected')){ 
							$('#div_unit_img_'+rowId).removeClass().addClass('selected'); 
						} 
						else{  
							$('#div_unit_img_'+rowId).removeClass().addClass('mouseover'); 
						}
					} 
				}
				else{ 
					$($(slidetab).children().get(0)).css('cursor','not-allowed');  
				}
			}
		/* }
		else{
			$('.trtd').css("title","Incomplete");  
		} */
	}).bind('mouseleave', function(){  
		slidetab=$($(this).parent()).get(0); 
		var unitTDSize= $(slidetab).children().size();  
		var currentTD="";
		for(var n=1;n<=unitTDSize;n++){  
			currentTD=$(slidetab).children().get(n);
			var componentTR=$(slidetab).children().get(0);
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);  
			unitStatus=$('#unitStatus_'+rowid).val();  
			
			var tempvar0=$(componentTR).attr("id");  
			var idarray0 = tempvar0.split('_');
			var rowid0=Number(idarray0[1]);  
			var componentStatus=$('#componentStatus_'+rowid0).html(); 
			if(componentStatus==1){
				var rowId=getRowId($(currentTD).attr('id')); 
				if(	$('#div_unit_img_'+rowId).hasClass('selected')){  
					$('#div_unit_img_'+rowId).removeClass().addClass('selected');  
				}
				else if(unitStatus==1){  
					$('#div_unit_img_'+rowId).removeClass().addClass('bigroom');  
				}
				else if(unitStatus==6){
					$('#div_unit_img_'+rowId).removeClass().addClass('offered');   
				}
			}
			else if(componentStatus==6){
				var rowId=getRowId($(currentTD).attr('id')); 
				if(	$('#div_unit_img_'+rowId).hasClass('selected')){  
					$('#div_unit_img_'+rowId).removeClass().addClass('selected');  
				} 
				else if(unitStatus==6 || unitStatus==1){
					$('#div_unit_img_'+rowId).removeClass().addClass('offered');  
				}
			}
		}
		/* currentTD=$($(slidetab).children().get(0));
		if(currentTD.IsBubblePopupOpen()){
			currentTD.RemoveBubblePopup(); 
		}
		popupComponentName="";
		currentTD=""; */
	}); 
	
	$('.showTD').bind('mouseenter',function(){
		//if($('#tableid').hasClass('complete')){
			var currentTD=$(this);
			$(currentTD).css('cursor','-moz-spinning');
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);
			unitStatus=$('#unitStatus_'+rowid).val(); 
			popupUnitName=$('#popupUnitName_'+rowid).html();   
			var componentTD=$($($($(currentTD).parent()).get(0)).children()).get(0);
			var tempvar0=$(componentTD).attr("id");  
			var idarray0 = tempvar0.split('_');
			var rowid0=Number(idarray0[1]);
			var componentStatus=$('#componentStatus_'+rowid0).html(); 
			
			if((unitStatus==1 || unitStatus==6)){
				if((componentStatus==1 || componentStatus==6)){
					$(currentTD).css('cursor','cell');
					var rowId=getRowId($(currentTD).attr('id')); 
					if(	$('#div_unit_img_'+rowId).hasClass('selected')){ 
						$('#div_unit_img_'+rowId).removeClass().addClass('selected'); 
					}
					else{
						var rowId=getRowId($(currentTD).attr('id')); 
						$('#div_unit_img_'+rowId).removeClass().addClass('mouseover'); 
					}
				}
				else{
					$(currentTD).css('cursor','not-allowed');  
				} 
			}
			else{
				$(currentTD).css('cursor','not-allowed');  
			} 
		/* }
		else{
			$('.showTD').css("cursor","not-allowed");  
		}  */
	}).bind('mouseleave', function(){
		var currentTD=$(this);  
		var tempvar=$(currentTD).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		unitStatus=$('#unitStatus_'+rowid).val(); 
		
		var componentTD=$($($($(currentTD).parent()).get(0)).children()).get(0);
		var tempvar0=$(componentTD).attr("id");  
		var idarray0 = tempvar0.split('_');
		var rowid0=Number(idarray0[1]);
		var componentStatus=$('#componentStatus_'+rowid0).html();  
	    if(unitStatus==1){
			if(componentStatus==6){
				var rowId=getRowId($(currentTD).attr('id')); 
				if(	$('#div_unit_img_'+rowId).hasClass('selected')){ 
					$('#div_unit_img_'+rowId).removeClass().addClass('selected'); 
				}
				else
					$('#div_unit_img_'+rowId).removeClass().addClass('offered');  
			}
			else{
				var rowId=getRowId($(currentTD).attr('id')); 
				if(	$('#div_unit_img_'+rowId).hasClass('selected')){ 
					$('#div_unit_img_'+rowId).removeClass().addClass('selected'); 
				}
				else if(componentStatus==1){ 
					var rowId=getRowId($(currentTD).attr('id')); 
					$('#div_unit_img_'+rowId).removeClass().addClass('bigroom'); 
				}
			}
	    }	
		 else if(unitStatus==6){
			 var rowId=getRowId($(currentTD).attr('id'));  
			 if($('#div_unit_img_'+rowId).hasClass('selected') ) { 
				 $('#div_unit_img_'+rowId).removeClass().addClass('selected');  
			}
			else{ 
				var rowId=getRowId($(currentTD).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass().addClass('offered'); 
			}
		} 
		popupUnitName="";
	}); 
	
	$('.showTD').click(function(){ 
		/* if($('#tableid').hasClass('complete')){ */
			var currentTD=$(this);   
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);
			unitStatus=$('#unitStatus_'+rowid).val(); 
			 
			var componentTD=$($($($(currentTD).parent()).get(0)).children()).get(0);
			var tempvar0=$(componentTD).attr("id");  
			var idarray0 = tempvar0.split('_');
			var rowid0=Number(idarray0[1]);
			var componentStatus=$('#componentStatus_'+rowid0).html(); 
			if((unitStatus==1 || unitStatus==6) && (componentStatus==1 || componentStatus==6)){ 
				$(currentTD).css('cursor','cell');
				var rowId=getRowId($(currentTD).attr('id'));  
				if ($('#div_unit_img_'+rowId).hasClass('selected') ) {  
				 	$('#div_unit_img_'+rowId).removeClass('selected');  
				 	$(componentTD).removeClass('selectedTR');
				 	if(unitStatus==1){ 
				 		var rowId=getRowId($(currentTD).attr('id')); 
						$('#div_unit_img_'+rowId).removeClass().addClass('bigroom'); 
					}
				 	else if(unitStatus==6 || componentStatus==6){ 
				 		var rowId=getRowId($(currentTD).attr('id')); 
						$('#div_unit_img_'+rowId).removeClass().addClass('offered'); 
					}
				}
				else{
					var rowId=getRowId($(currentTD).attr('id')); 
					$('#div_unit_img_'+rowId).removeClass().addClass('selected');  
				 	checkAllSelected(componentTD);
				} 
			} 
			else{
				$(currentTD).css('cursor','not-allowed');
			}
		/* }
		else{
			$('.showTD').css("cursor","not-allowed"); 
			$('.showTD').css("title","Incomplete"); 
		}  */
	});
	
	$('.trtd').click(function(){
		//if($('#tableid').hasClass('complete')){
			currentTR=$(this);
			var trFlag=false;
			slidetab=$($(currentTR).parent()).get(0);  
			var unitTDSize= $(slidetab).children().size()-1; 
			var tempvar0=$(currentTR).attr("id");  
			var idarray0 = tempvar0.split('_');
			var rowid0=Number(idarray0[1]);  
			var componentStatus=$('#componentStatus_'+rowid0).html();
			if(componentStatus==1 || componentStatus==6){
				for(var m=1;m<=unitTDSize;m++){
					var currentTD=$(slidetab).children().get(m); 
					var tempvar1=$(currentTD).attr('id');  
					var idarray = tempvar1.split('_');
					var rowid=Number(idarray[1]);  
					unitStatus=$('#unitStatus_'+rowid).val();    
					if(unitStatus==1 || unitStatus==6){  
						var rowId=getRowId($(currentTD).attr('id'));  
						if($('#div_unit_img_'+rowId).hasClass('selected')){   
							flag=true;  
						}
						else{  
							flag=false;  
							break;
						}
					}
				} 
				if(flag==false){
					var unitTDSize= $(slidetab).children().size()-1; 
					trFlag=true;
					for(var n=1;n<=unitTDSize;n++){ 
						var currentTD=$(slidetab).children().get(n);
						var tempvar1=$(currentTD).attr('id');  
						var idarray = tempvar1.split('_');
						var rowid=Number(idarray[1]);
						unitStatus=$('#unitStatus_'+rowid).val();   
						if(unitStatus==1 || unitStatus==6){ 
							var rowId=getRowId($(currentTD).attr('id'));  
							$('#div_unit_img_'+rowId).removeClass().addClass('selected'); 
						}
						else{ 
							trFlag=false;
						}
						$($(slidetab).children().get(0)).addClass('selectedTR');
					} 
				}
				else{  
					var unitTDSize= $(slidetab).children().size()-1; 
					for(var n=1;n<=unitTDSize;n++){ 
						var currentTD=$(slidetab).children().get(n);
						var tempvar1=$(currentTD).attr("id");  
						var idarray = tempvar1.split('_');
						var rowid=Number(idarray[1]);
						unitStatus=$('#unitStatus_'+rowid).val();   
						trFlag=true;
						if(componentStatus==1){
							if(unitStatus==1){ 
								var rowId=getRowId($(currentTD).attr('id'));  
								$('#div_unit_img_'+rowId).removeClass().addClass('bigroom');  
							}
							else if(unitStatus==6){
								var rowId=getRowId($(currentTD).attr('id'));  
								$('#div_unit_img_'+rowId).removeClass().addClass('offered');    
							}
						}
						else if(componentStatus==6){
							var rowId=getRowId($(currentTD).attr('id'));  
							$('#div_unit_img_'+rowId).removeClass().addClass('offered');    
						}
						trFlag=false;
					}
				}
			} 
			if(trFlag==false){
				$($(slidetab).children().get(0)).removeClass('selectedTR');
			}
		/* }
		else{
			$('.trtd').css("cursor","not-allowed"); 
			$('.trtd').css("title","Incomplete"); 
		} */
	});  
	propertyId=$('#propertyId').val();
	$('#createOffer_'+propertyId).live('click',function(){
		componentArray=new Array();
		unitArray= new Array();
		componentFlag=1;
		var propertyId="";
		var propertyRent="";
		var propertyName="";
		var controlFlag=true; 
		var propertyType=$('#propertyType').val();  
		var offerId=0;
		if($('#propertyType').val()!="Villa"){
			propertyId=$('#propertyId').val();
			propertyRent=$('#propertyRent').val();
			propertyName=$('#propertyName').html();  
			
			if(controlFlag==true){
				$('.componentTR').each(function() {
					slidetab = $(this);
					controlFlag=callComponentUnits(slidetab);
					if(controlFlag==false){
						 return false;
					}
				});
			}
			
			if(controlFlag==true){
				//setTOComponentArray();
				setTOUnitArray();
			} 
			else{
				setTOUnitArray();
			} 
			 
			for(var i=0;i<componentArray.length;i++){ 
				componentList+=componentArray[i]+"@";
			}
			for(var j=0;j<unitArray.length;j++){ 
				unitList+=unitArray[j]+"@";
			}
		}
		else{
			 var villaTD=$($('.componentTR').children()).get(0); 
			 if($(villaTD).hasClass('selected')){
				 propertyId=$('#propertyId').val();
				 propertyRent=$('#propertyRent').val();
				 propertyName=$('#propertyName').html();
			 }
			 else{  
				$('#page-error').hide().html("Please select this villa to process").slideDown(400);
				$('#page-error').delay(2000).slideUp(1000);
				return false;
			 } 
		} 
		if($('#propertyType').val()!="Villa"){
			$('.componentTR').each(function() {
				slidetab = $(this);
				if($($(slidetab).children().get(0)).hasClass('selectedTR')){
					componentFlag=1;
				}
				else{
					componentFlag=0;
					return false;
				}
			});
		}
		else{
			componentFlag=1;
		} 
		 if((componentList!=null && componentList!="")  || (unitList!=null && unitList!="") || ($('#propertyType').val()=="Villa")){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/getoffer_entryscreen.action", 
				data:{	componentList:componentList,unitList:unitList, propertyId:propertyId, componentFlag:componentFlag, 
						propertyRent:propertyRent, propertyName:propertyName, propertyType:propertyType, offerId:offerId},
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove(); 
					$("#main-wrapper").html("");  
					$("#main-wrapper").html(result); 
				},
				error: function(result){ 
					 $('#DOMWindow').remove();
					 $("#main-wrapper").html(""); 
					 $('#DOMWindowOverlay').remove();
					 $("#main-wrapper").html(result);  
					
				}
			});
			return false;
		}
		else{  
			$('#page-error').hide().html("Please select this property to process").slideDown(400);
			$('#page-error').delay(2000).slideUp(1000);
			return false;
		 }  
		return false;
	}); 
	
	//Check for property process
//	checkPropertyProcess(counter);
});

 
function callComponentUnits(slidetab){
	var controlFlag=false;
	var thisTDSize = Number($(slidetab).children().size())-1;
	if($($(slidetab).children().get(0)).hasClass('selectedTR')){
		controlFlag=true;
	}
	else{
		for(var i=1;i<=thisTDSize;i++){
			var rowId=getRowId($($(slidetab).children().get(i)).attr('id'));  
			if($('#div_unit_img_'+rowId).hasClass('selected')){ 
				controlFlag=false; 
				break;
			}
			else{
				controlFlag=true;
			}
		}
	}  
	return controlFlag;
}

function setTOComponentArray(){
	$('.componentTR').each(function(){
		slidetab=$(this);  
		var currentTD=$(slidetab).children().get(0);
		if($(currentTD).hasClass('selectedTR')){
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);
			var componentId=$('#componentId_'+rowid).html();  
			var componentName=$('#popupComponentName_'+rowid).html();
			var componentRent=$('#componentRent_'+rowid).html();
			var componentType=$('#componentType_'+rowid).html();
			componentArray.push(componentId+","+componentName+","+componentType+","+componentRent);  
		}
	}); 
}

function setTOUnitArray(){
	$('.componentTR').each(function(){
		slidetab=$(this);  
		var thisTDSize = Number($(slidetab).children().size());    
		console.debug(thisTDSize);
		for(var i=1;i<thisTDSize;i++){ 
			console.debug(i);
			console.debug($($(slidetab).children().get(i)).attr('id'));
			var rowId=getRowId($($(slidetab).children().get(i)).attr('id'));  
			if($('#div_unit_img_'+rowId).hasClass('selected')){ 
				var currentTD=$(slidetab).children().get(i);
				var tempvar=$(currentTD).attr('id');  
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]);
				unitId=0; 
				var unitId=$('#unitId_'+rowid).html(); 
				var unitName=$('#popupUnitName_'+rowid).html();
				var unitRent=$('#unitRent_'+rowid).html(); 
				var unitTypeName=$('#unitType_'+rowid).attr('alt');
				unitArray.push(unitId+","+unitName+","+unitTypeName+","+unitRent+","); 
			} 
		} 
	}); 
}	
function unitColSpan(largestNumber) {
	counter=0;
	$('.componentTR').each(function() {
		slidetab = $(this);
		var thisTDSize = Number($(slidetab).children().size());
		thisTDSize--;
		var difference = 0; 
		var divideTD = Number(largestNumber / thisTDSize);  
		if (largestNumber != thisTDSize) {
			if (divideTD > 1 && divideTD <= 2) {
				difference = Number(largestNumber - thisTDSize);
				for ( var m = 1; m <=difference; m++) {
					$($(slidetab).children().get(m)).attr('colspan', 2);  
				}
			}
			if (divideTD > 2) {
				for ( var n = 1; n<=thisTDSize; n++) {
					$($(slidetab).children().get(n)).attr('colspan', divideTD); 
				}
			}
		}
		var tdwidth = 0;  
		tdwidth = Number(100 / thisTDSize);   
		if(thisTDSize>2 && largestNumber!=thisTDSize){ 
			tdwidth=(tdwidth-largestNumber)+1.7;
		}
		else if(largestNumber==thisTDSize && thisTDSize>1){  
			tdwidth=tdwidth-3.5;
		}
		else if(thisTDSize>1 && thisTDSize<=2){  
			//$($(slidetab).children().get(0)).css('width','3%'); 
			tdwidth=tdwidth-10;
		}
		else if(thisTDSize==1){ 
			tdwidth=93;
		} 
		for ( var n = 1; n<=thisTDSize; n++) { 
			$($(slidetab).children().get(n)).css('width',tdwidth + "%");   
			var currentTD=$(slidetab).children().get(n);
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);  
			unitStatus=$('#unitStatus_'+rowid).val();  
			var altvalue=$('#unitType_'+rowid).attr('alt').toLowerCase();
			 
			var imgWidth =45; //$(currentTD).css('width'); 
			var imgHeight=20;//$(currentTD).css('height');
			
			if(altvalue=="flat"){
				$('#unitType_'+rowid).css('width',imgWidth+"%");
				$('#unitType_'+rowid).css('height',imgHeight);
				$('#div_unit_img_'+rowid).addClass('bigroom');
				$('#unitType_'+rowid).attr('src', 'images/property/offer/flat.png');
			}
			else if(altvalue=="shop"){
				$('#unitType_'+rowid).css('width',imgWidth+"%");
				$('#unitType_'+rowid).css('height',imgHeight);
				$('#div_unit_img_'+rowid).addClass('bigroom ofice');
				$('#unitType_'+rowid).attr('src', 'images/property/offer/shop.png');
			}
			else if(altvalue=="office"){
				$('#unitType_'+rowid).css('width',imgWidth+"%");
				$('#unitType_'+rowid).css('height',imgHeight);
				$('#div_unit_img_'+rowid).addClass('bigroom ofice');
				$('#unitType_'+rowid).attr('src', 'images/property/offer/ofice.png');
			} 
			
			if(unitStatus==2){ 
				var rowId=getRowId($($(slidetab).children().get(n)).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass('bigroom').addClass('pending'); 
			}
			else if(unitStatus==3){ 
				var rowId=getRowId($($(slidetab).children().get(n)).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass('bigroom').addClass('rented');    
			}
			else if(unitStatus==4){ 
				var rowId=getRowId($($(slidetab).children().get(n)).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass('bigroom').addClass('sold');   
			}
			else if(unitStatus==5){  
				var rowId=getRowId($($(slidetab).children().get(n)).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass('bigroom').addClass('renovation');  
			} 
			else if(unitStatus==6){ 
				var rowId=getRowId($($(slidetab).children().get(n)).attr('id')); 
				$('#div_unit_img_'+rowId).removeClass('bigroom').addClass('offered');  
			}
			++counter;
		}
		var currentTD=$(slidetab).children().get(0);
		var unitFlag=callCheckAllUnits($($(currentTD).parent()).get(0));  
		var tempvar=$(currentTD).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);
		var componentStatus=$('#componentStatus_'+rowid).html();  
		if(componentStatus==1){
			if(unitFlag==2){    
				var rowId=getRowId($($(slidetab).children().get(0)).attr('id')); 
				$('#componentimg_'+rowId).attr('src','images/property/offer/compt_pending.png'); 
				$('#componentStatus_'+rowid).html(unitFlag); 
			}
			else if(unitFlag==3){  
				var rowId=getRowId($($(slidetab).children().get(0)).attr('id')); 
				$('#componentimg_'+rowId).attr('src','images/property/offer/compt_rented.png');  
				$('#componentStatus_'+rowid).html(unitFlag); 
			}
			else if(unitFlag==4){  
				var rowId=getRowId($($(slidetab).children().get(0)).attr('id')); 
				$('#componentimg_'+rowId).attr('src','images/property/offer/compt_sold.png'); 
				$('#componentStatus_'+rowid).html(unitFlag); 
			}
			else if(unitFlag==5){  
				var rowId=getRowId($($(slidetab).children().get(0)).attr('id')); 
				$('#componentimg_'+rowId).attr('src','images/property/offer/compt_renovation.png'); 
				$('#componentStatus_'+rowid).html(unitFlag); 
			} 
		}
	});
	$('.unitsize').html(counter);
}

function callCheckAllUnits(slidetab){
	var thisTDSize = Number($(slidetab).children().size())-1;
	var unitFlag=false;
	for (var n = 1; n<=thisTDSize; n++){ 
		var currentTD=$(slidetab).children().get(n);
		var tempvar=$(currentTD).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		unitStatus=$('#unitStatus_'+rowid).val();  
		if(unitStatus==1 || unitStatus==6){ 
			return unitStatus;
		}
		else{
			unitFlag=unitStatus;
		} 
	}  
	return unitFlag;
}
function callAllSelected(flag){ 
	$('.componentTR').each(function(){
		slidetab = $(this);
		var unitTDSize= $(slidetab).children().size(); 
		for(var n=1;n<unitTDSize;n++){ 
			var currentTD=$(slidetab).children().get(n);
			var tempvar=$(currentTD).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);
			unitStatus=$('#unitStatus_'+rowid).val();   
			
			
			var tempvar0=$($($(slidetab).children()).get(0)).attr("id");
			var idarray0 = tempvar0.split('_');
			var rowid0=Number(idarray0[1]);
			var componentStatus=$('#componentStatus_'+rowid0).html();
			if((unitStatus==1 || unitStatus==6) && (componentStatus==1 || componentStatus==6)){ 
				if(flag==false){
					var rowId=getRowId($(currentTD).attr('id'));  
					$('#div_unit_img_'+rowId).removeClass().addClass('selected');   
				}
				else{
					var rowId=getRowId($(currentTD).attr('id'));   
					if(unitStatus==6 || componentStatus==6){
						$('#div_unit_img_'+rowId).removeClass().addClass('offered');   
					}
					else if(unitStatus==1){
						$('#div_unit_img_'+rowId).removeClass().addClass('bigroom');   
					}
				}
			}
		}
		var cflag=false;  
		for(var x=1;x<unitTDSize;x++){
			var currentTD=$(slidetab).children().get(x);
			var rowId=getRowId($(currentTD).attr('id'));   
			if($('#div_unit_img_'+rowId).hasClass('selected')){   
				cflag=true;  
			}
			else{  
				cflag=false;  
				break;
			} 
		}
		if(cflag==true){ 
			$($(slidetab).children().get(0)).addClass('selectedTR');
		}
		else{
			$($(slidetab).children().get(0)).removeClass('selectedTR');
		}
	});
} 
function checkTRStatus(slidetab){
	var valueTD=$($(slidetab).children().get(0)); 
	var tempvar=$(valueTD).attr("id");  
	var idarray = tempvar.split('_');
	var rowid=Number(idarray[1]);
	var componentStatus=$('#componentStatus_'+rowid).html(); 
	if(componentStatus!=1){
		var unitTDSize= $(slidetab).children().size(); 
		for(var n=0;n<unitTDSize;n++){ 
			if(componentStatus==2){ 
				$($(slidetab).children().get(n)).attr('src', 'images/property/offer/leg_pending.png'); 
				$('#unitStatus_'+n).val(componentStatus); 
			}
			else if(componentStatus==3){ 
				$($(slidetab).children().get(n)).attr('src', 'images/property/offer/leg_rented.png'); 
				$('#unitStatus_'+n).val(componentStatus); 
			}
			else if(componentStatus==4){ 
				$($(slidetab).children().get(n)).attr('src', 'images/property/offer/leg_sold.png'); 
				$('#unitStatus_'+n).val(componentStatus); 
			}
			else if(componentStatus==5){ 
				$($(slidetab).children().get(n)).attr('src', 'images/property/offer/leg_renovation.png'); 
				$('#unitStatus_'+n).val(componentStatus); 
			} 
			else if(componentStatus==6){ 
				$($(slidetab).children().get(n)).attr('src', 'images/property/offer/leg_offered.png'); 
				$('#unitStatus_'+n).val(componentStatus); 
			}
		} 
	}
}
function checkPropertyStatus(){
	var propertyStatus=$('#propertyStatus').val(); 
	if(propertyStatus!=1){ 
		$('.componentTR').each(function(){
			slidetab=$(this);
			var componentTR=$($(slidetab).children().get(0)); 
			var tempvar=$(componentTR).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			$('#componentStatus_'+rowid).html(propertyStatus); 
		}); 
	}

	$('.componentTR').each(function(){
		slidetab = $(this);
		var unitTDSize= $(slidetab).children().size(); 
		for(var n=1;n<unitTDSize;n++){ 
			var currentTD=$(slidetab).children().get(n);  
			if(propertyStatus==2){
				var rowId=getRowId($(currentTD).attr('id'));  
				$('#div_unit_img_'+rowId).removeClass().addClass('pending');  
			}else if(propertyStatus==3){
				var rowId=getRowId($(currentTD).attr('id'));  
				$('#div_unit_img_'+rowId).removeClass().addClass('rented');  
			}else if(propertyStatus==4){
				var rowId=getRowId($(currentTD).attr('id'));  
				$('#div_unit_img_'+rowId).removeClass().addClass('sold');  
			}else if(propertyStatus==5){
				var rowId=getRowId($(currentTD).attr('id'));  
				$('#div_unit_img_'+rowId).removeClass().addClass('renovation');  
			}else if(propertyStatus==6){
				var rowId=getRowId($(currentTD).attr('id'));  
				$('#div_unit_img_'+rowId).removeClass().addClass('offered');  
			} 
		}
	}); 
}
function checkAllSelected(componentTD){
	slidetab=$($(componentTD).parent()).get(0);  
	var unitTDSize= $(slidetab).children().size()-1;  
	var currentTD="";
	for(var m=1;m<=unitTDSize;m++){
		currentTD=$(slidetab).children().get(m);  
		var rowId=getRowId($(currentTD).attr('id')); 
		//$('#div_unit_img_'+rowId).hasClass('selected');  
		if($('#div_unit_img_'+rowId).hasClass('selected')) { 
			flag=true;
		}
		else{
			flag=false;
			break;
			$(componentTD).removeClass('selectedTR');
		}
	}
	if(flag==true){
		$(componentTD).addClass('selectedTR');
	}else{
		$(componentTD).removeClass('selectedTR');
	}
}
function calScreenResolution(){
	if(screen.width<=1024){  
		$('#tableid').css("width",20+'%'); 
	}
	else{
		$('#tableid').css("width",15+'%'); 
	}
}
function checkPropertyProcess(){ 
	var propertyFloors=Number($('#propertyFloors').val());
	var totalComponent=Number($('#tableid tr').size()); 
	if(propertyFloors==totalComponent){  
		checkUnitProcess(counter);
	}
	else{
		$('#tableid').addClass('incomplete tooltip');
		$('#tableid').attr('title', 'Incomplete');
		return false;
	}
}
function checkUnitProcess(totalUnit){  
	var componentUnitSize=0; 
	$('.componentTR').each(function(){
		var tempvar=$(this).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		var tempSize=Number($('#componentUnitSize_'+rowid).html());
		componentUnitSize+=tempSize;
	});  
	if(componentUnitSize==totalUnit){
		$('#tableid').addClass('complete');
	}
	else{
		$('#tableid').addClass('incomplete tooltip');
		$('#tableid').attr('title', 'Incomplete');
		return false;
	}
} 
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>