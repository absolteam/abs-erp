<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<tr class="assetrowid" id="fieldrow_${rowid}">
	<td style="display:none;" id="lineId_${rowid}">${rowid}</td> 
	<td id="productcode_${rowid}" style="width:2%;">
		<select id="productCode_${rowid}" class="autocompletecode productcodeclass_${rowid}" name="productCode">
			<option value="">Select</option>
			<c:forEach items="${PRODUCT_INFO}" var="bean">
				<option value="${bean.productId}">${bean.code}</option>
			</c:forEach>
		</select>
	</td>
	<td id="productname_${rowid}">
		<select id="productName_${rowid}" class="autocomplete productnameclass_${rowid}" name="productName">
			<option value="">Select</option>
			<c:forEach items="${PRODUCT_INFO}" var="bean">
				<option value="${bean.productId}@@${bean.lookupDetailByProductUnit.displayName}">${bean.productName}</option>
			</c:forEach>
		</select>
	</td>
	<td id="uom_${rowid}"></td> 
	<td style="display:none">
		<input type="hidden" name="assetlineid" id="assetlineid_${rowid}"/>
		<input type="hidden" name="productid" id="productid_${rowid}"/> 
	</td> 
	<td id="assetdescription_${rowid}">
		<input type="text" id="assetDescription_${rowid}" style="width:97%!important;" name="assetDescription"/>
	</td> 
	 <td style="width:0.5%;" class="opn_td" id="option_${rowid}"> 
	 		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${rowid}" style="cursor:pointer;display:none;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${rowid}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip assetdelrow" id="DeleteAImage_${rowid}" style="cursor:pointer;display:none;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowid}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a> 
	</td>   
</tr>
<script type="text/javascript">
$(function(){
	/* $(".assetdelrowN").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var uom=$('#uom_'+rowid).text().trim();
		 alert(uom);
		 if(uom!=null && uom!=""){
			 var flag=true;  
			 if(flag==true){ 
       		 var childCount=Number($('#assetchildCount').val());
       		 if(childCount > 0){
					childCount=childCount-1;
					$('#assetchildCount').val(childCount); 
       		 } 
       		 $(slidetab).remove(); 
       		//To reset sequence 
       		var i=0;
    			$('.assetrowid').each(function(){  
					i=i+1; 
				});  
			}
		 }
		 else{
			 $('#warning_message').hide().html("Empty/Last Asset information can't delete...").slideDown(1000);
			 return false;
		 }  
		 return false;
	 }); */
$('.autocompletecode').combobox({ 
			 selected: function(event, ui){ 
				 var elVal=$(this).val(); 
				 slidetab=$(this).attr('id'); 
			  	 var idarray =slidetab.split('_');
			  	 var rowid=Number(idarray[1]);  
			  	 var nexttab=slidetab=$(this).parent().parent().get(0);  
			  	 nexttab=$(nexttab).next(); 
			  	 $('#productName_'+rowid+' option').each(function(){
			 		 var productId=$(this).val(); 
			 		 var splitval=productId.split("@@");
			 		 productId=splitval[0]; 
			 		 if(elVal==productId){
			 			var productName=$(this).text();  
			 			$('.productnameclass_'+rowid).combobox('autocomplete', productName); 
			 			$('#uom_'+rowid).text(splitval[1]);
			 			$('#productid_'+rowid).val(productId);
			 		 }
			 	 }); 
			  	var unitVal=$('#uom_'+rowid).text();
			  	triggerAddRow(unitVal,nexttab,rowid); 
	         } 
		 });
	 	
	 	$(".autocomplete").combobox({ 
	        selected: function(event, ui){
	 	      	var productVal=$(this).val();
	 	      	proArray=productVal.split('@@');
	 	        var unitName=proArray[1];
	 	        slidetab=$(this).attr('id'); 
	 	  		var idarray =slidetab.split('_');
	 	  		var rowid=Number(idarray[1]);  
	 	  		$('#uom_'+rowid).text(unitName); 
	 	  		var nexttab=slidetab=$(this).parent().parent().get(0);  
	 		  	nexttab=$(nexttab).next();
	 	  		triggerAddRow(unitName,nexttab,rowid);  
	 	  		//change product code
	 	  		var productName=$(this).val(); 
	 	  		$('#productCode_'+rowid+' option').each(function(){
	 		 		 var productId=$(this).val(); 
	 		 		 var splitval=productName.split("@@");
	 		 		 productName=splitval[0]; 
	 		 		 if(productId==productName){
	 		 			productName=$(this).text();   
	 		 			$('.productcodeclass_'+rowid).combobox('autocomplete', productName);
	 		 			$('#productid_'+rowid).val(productId);
	 		 		 }
	 		 	 });
	        }
	     }); 
});
</script>