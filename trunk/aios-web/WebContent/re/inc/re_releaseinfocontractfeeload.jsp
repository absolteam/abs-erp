<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <c:forEach items="${requestScope.itemList}" var="result" varStatus="status">
 	<tr class="even"> 
		<td class="width10 indexForFee">${status.index+1}</td>
		<td class="width20 feeId">${result.feeId}</td>	
		<td class="width20 feeDescription">${result.feeDescription}</td> 
		<td class="width20 feeAmountCalc">${result.feeAmount}</td>
		<td class="width20" style="display:none;"></td> 
		<td class="width10" style="display:none;"></td>
		<td style="display:none"> 
			<input type="text" value="${result.releaseFeeId}" name="actualLineId" id="actualLineId"/>
		</td> 
		<td style=" width:10%;display:none;"> 
					<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
		   	</a>	
		   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
				<span class="processing"></span>
			</a>
		</td>  
		<td style="display:none;" class="session_status"></td>
		<td style="display:none;"></td>	
	</tr>
</c:forEach>
<input type="hidden" id="count" value="${count}"/>