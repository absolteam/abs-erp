<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<div id="fromDateComp">${bean.fromDate}</div>
<div id="toDateComp"><c:if test="${bean.toDate ne '31-Dec-9999'}">${bean.toDate}</c:if></div>
<div id="totalAreaComp">${bean.totalArea}</div>
<div id="measurementCodeComp">${bean.measurementCode}</div>