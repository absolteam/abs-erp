<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementPaymentAddEntry" id="contractAgreementPaymentAddEntry" style="position: relative;">	

<div class="width45 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.contract.chequedetails"/></legend>
			<div>
				<label>Payment Mode<span style="color:red">*</span></label>
				<select id="paymentMode"  class="width50 validate[required]" >
					<option value="">--Select--</option>
					<option value="1">Cash</option>
					<option value="2">Cheque</option>
					<option value="3">Direct Transfer</option>
				</select>
			</div>	 
			<div id="bankNameDiv">
				<label><fmt:message key="re.contract.bankname"/><span style="color:red">*</span></label>
				<input type="text" name="bankName" id="bankName" class="width50 validate[required]">
			</div>
			<div id="defaultActualPickerDiv">
				<label>Date<span style="color:red">*</span></label>
				<input type="text" name="chequeDate" id="defaultActualPicker" class="width50 validate[required]" readonly="readonly">
			</div>
			
		</fieldset>
</div>
<div class="width45 view float-left" id="hrm">
	<fieldset>					 
		<legend><fmt:message key="re.contract.chequedetails"/></legend>
			
		<div id="chequeNumberDiv">
			<label><fmt:message key="re.contract.chequeno"/><span style="color:red">*</span></label>
			<input type="text" name="chequeNumber" id="chequeNumber" class="width50  validate[required]">
		</div>
		<div>
			<label>Amount<span style="color:red">*</span></label>
			<input type="text" name="chequeAmount" id="chequeAmount" class="width50  validate[required]">
			<input type="hidden" name="chequeAmountTemp" id="chequeAmountTemp" value="0" class="width50">
		</div>
		<div>
			<label>Fee Type<span style="color:red">*</span></label>
			<select id="feeType"  class="width50 validate[required]" >
				<option value="">--Select--</option>
				<c:forEach items="${FEETYPES}" var="feeType" varStatus="status">
					<option value="${feeType.code}">${feeType}</option>
				</c:forEach>
			</select>
		</div>	
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
	<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $('#defaultActualPicker').datepick();
		
		 
		 $jquery("#contractAgreementPaymentAddEntry").validationEngine('attach');

		$('.cancel').click(function(){ 
			  $('.formError').remove();
			
			var addEditFlag=$('#addEditFlag').val();	
		  	if(addEditFlag=="A"){
				
				$($($(slidetab).children().get(6)).children().get(0)).show();	//Add Button
				$($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
				
		  	}else{
				$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
				$($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
			  	
		  	}
			//$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button	 	
			openFlag=0;	
		  	$($(this).parent().parent().parent().get(0)).remove();
			
		 });
		 $('#add').click(function(){
			  if($jquery('#contractAgreementPaymentAddEntry').validationEngine('validate')){
				 // if($.fn.globePaymentValidation()){
					 $('#loading').fadeIn();

						var tempObject=$(this);
						var temp=$('#bankName').val();					
						var bankName=$('#bankName').val();
						var chequeDate=$('#defaultActualPicker').val();
						var chequeNumber=$('#chequeNumber').val();
						var chequeAmount=convertToDouble($('#chequeAmount').val());
						var addEditFlag=$('#addEditFlag').val();			 
						var trnVal =$('#transURN').val(); 
						var feeType=$('#feeType').val();
						var feeTypeText=$('#feeType option:selected').text();
						if(temp=='Cash')
							temp='-NA-';
						var flag=false;
						var actualLineId=Number($($($(slidetab).children().get(5)).children().get(0)).val());
						var id=Number($($($(slidetab).children().get(5)).children().get(1)).val());
			        	$.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/add_add_contract_payment_save.action", 
						 	async: false,
						 	data: {bankName: bankName,chequeDate: chequeDate,chequeNumber: chequeNumber,chequeAmount: chequeAmount, 
								trnValue:trnVal,addEditFlag:addEditFlag,contractPaymentId:actualLineId,feeType:feeType,id:id},
						    dataType: "html",
						    cache: false,
							success:function(result){ 
						 		$('.formError').hide();
		                           $('#othererror').hide(); 
								$('.tempresult').html(result); 
							     if(result!=null){
							    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                                    flag = true;
							    	else{	
								    	flag = false;
								    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
							    	} 
							     }
							     $('#loading').fadeOut();
						 	},  
						 	error:function(result){
							 	//alert("err "+result);
						 		 $('.tempresult').html(result);
		                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
		                            $('#loading').fadeOut();
		                            return false;
						 	} 
			        	});
			        	if(flag==true){
			        		if(bankName=="-NA-")
			        			bankName="Cash";
			        		$($(slidetab).children().get(0)).text(bankName);
			        		$($(slidetab).children().get(1)).text(chequeDate);
			        		$($(slidetab).children().get(2)).text(chequeNumber); 
			        		$($(slidetab).children().get(3)).text(chequeAmount); 
			        		$($(slidetab).children().get(4)).text(feeTypeText);
			        		$($($(slidetab).children().get(5)).children().get(2)).val(feeType);
			        		
	
			        		//$($(slidetab).children().get(4)).text(remarks);
						   
						  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
						  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
							//$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
							$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button
							$($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
							

							openFlag=0;
		
					  		$("#transURN").val($('#objTrnVal').html());
		
					  		var childCountPayment=Number($('#childCountPayment').val());
					  		childCountPayment=childCountPayment+1;
							$('#childCountPayment').val(childCountPayment);
	
							$.fn.globeTotalCheque();  //For Total Amount of Payment
							$.fn.globeTotalFee();	// For Grand Total
							contractPaymentAddVisibility();
					   } 
				  }else{
					  $("#othererror").hide().html("Check the missing fields").slideDown(); 
                      return false;
				  }
			  /*}else{
				  return false;
			  }*/
		});

		// Default Date
			//$('#defaultActualPicker').datepick();

			//Payment Method 
			$("#paymentMode").change(function() {
				
				var paymentMode=$('#paymentMode').val();
				//alert($('#paymentModeCheque').attr('checked'));
				//alert(paymentMode);
				
				if(paymentMode==1){

					$('#bankName').removeClass('validate[required]');
					$('#defaultActualPicker').removeClass('validate[required]');
					$('#chequeNumber').removeClass('validate[required]');
					
					$('#bankName').attr('readonly','readonly');
					$('#defaultActualPicker').attr('readonly','readonly');
					$('#chequeNumber').attr('readonly','readonly');

					$('#bankName').val("-NA-");
					$('#defaultActualPicker').val("-NA-");
					$('#chequeNumber').val("-NA-");
					
					$('#bankNameDiv').css("display","none");
					$('#defaultActualPickerDiv').css("display","none");
					$('#chequeNumberDiv').css("display","none");
					
				}else if(paymentMode==2) {
					$('#bankName').val("");
					$('#defaultActualPicker').val("");
					$('#chequeNumber').val("");
	
					$('#bankNameDiv').css("display","block");
					$('#defaultActualPickerDiv').css("display","block");
					$('#chequeNumberDiv').css("display","block");
					
					$('#bankName').removeAttr('readonly');
					$('#defaultActualPicker').removeAttr('readonly');
					$('#chequeNumber').removeAttr('readonly');
					
					$('#bankName').addClass('validate[required]');
					$('#defaultActualPicker').addClass('validate[required]');
					$('#chequeNumber').addClass('validate[required]');
				}else if(paymentMode==3){
					$('#bankName').val("");
					$('#defaultActualPicker').val("");
					$('#chequeNumber').val("");
	
					$('#bankNameDiv').css("display","block");
					$('#defaultActualPickerDiv').css("display","block");
					$('#chequeNumberDiv').css("display","none");
					
					$('#chequeNumber').val("-NA-");

					
					$('#bankName').addClass('validate[required]');
					$('#defaultActualPicker').addClass('validate[required]');
					$('#chequeNumber').removeClass('validate[required]');
					
				}else{
					return false;
				}
			});	
});
 </script>	