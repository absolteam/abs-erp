 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;" id="childRowId_${requestScope.rowid}">
<td colspan="4" class="tdidentity" id="childRowTD_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="periodform" id="periodEditAddValidate" style="position: relative;">
<input type="hidden" name="addEditFlag" id="addEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="float-left width50" id="hrm">
	<fieldset style="height:60px;">
		<div><label>Asset Name</label><input type="text" tabindex="1" name="assetName" id="assetName" class="width40 validate[required]"></div>
		<div><label>Damage</label><input type="text" tabindex="2" name="damage" id="damage" class="width40"></div> 
	</fieldset> 
</div> 
<div class="float-left width50" id="hrm">
	<fieldset style="height:60px;">
		<div><label>Amount</label><input type="text" tabindex="3" name="balanceAmount" id="balanceAmount" class="width40 validate[required ,custom[onlyFloat]]"></div> 
	</fieldset>
</div> 

<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:20px;">  
		<div class="portlet-header ui-widget-header float-right cancel_period" id="cancel_${requestScope.rowid}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right add_period" id="update_${requestScope.rowid}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
	 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){  
		  $('.cancel_period').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
  					$("#AddImage_"+rowid).show();
  				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
  				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 
			     var childCount=Number($('#childCount').val()); 
		          $('#childCount').val(childCount);
					if(childCount<1){
						$('#calendarName').attr("disabled",false);
						$('#sfrom').attr("disabled",false);
						$('#sto').attr("disabled",false);  
					}
					else{
						$('#calendarName').attr("disabled",true);
						$('#sfrom').attr("disabled",true);
						$('#sto').attr("disabled",true);  
					} 
					$('#openFlag').val(0);
		  });
		  $('.add_period').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);

			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var releaseDetailId=$('#releaseDetailId_'+rowid).val();	
			 var releaseId=$('#releaseId').val();
			 var assetName=$('#assetName').val();
			 var damage=$('#damage').val();
			 var balanceAmount=convertToDouble($('#balanceAmount').val());
			 var lineId=$('#lineId_'+rowid).val();
			 var addEditFlag=$('#addEditFlag').val(); 
	        if($jquery("#periodEditAddValidate").validationEngine('validate')){   
	        	$.ajax({
					type:"POST", 
					url:"<%=request.getContextPath()%>/save_release_maintain_detail.action", 
				 	async: false, 
				 	data:{releaseDetailId:releaseDetailId,
				 	releaseId:releaseId,
				 	assetName:assetName,
				 	damage:damage,
				 	balanceAmount:balanceAmount,
				 	addEditFlag:addEditFlag,
				 	id:lineId},
				    dataType: "html",
				    cache: false,
					success:function(result){
				 		//Bussiness parameter
	        			$('#assetName_'+rowid).text(assetName); 
	        			$('#damage_'+rowid).text(damage); 
	        			$('#balanceAmount_'+rowid).text(balanceAmount); 

	        			
	        			//Maintenance Total Calculation
	    				$.fn.calmaintenanceTotal(); 
	    				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
	    					$('.addrows_detail').trigger('click'); 
	    				
	    				
					//Button(action) hide & show 
	    				 $("#AddImage_"+rowid).hide();
	    				 $("#EditImage_"+rowid).show();
	    				 $("#DeleteImage_"+rowid).show();
	    				 $("#WorkingImage_"+rowid).hide();  
	        			 $("#childRowId_"+rowid).remove();	
	        			 $('#openFlag').val(0);
				 	} 
	        	}); 
	        	 
	        }
	        else{
	        	return false;
	        }
		   
		  });
			
	 });
 </script>	