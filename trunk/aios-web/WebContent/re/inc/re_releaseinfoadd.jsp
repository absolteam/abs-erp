<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
  <script type="text/javascript" src="/js/dateDifference.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<title>Tenant Details</title>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
.extrapad{
padding:10px!important;
}
</style>

<script type="text/javascript">
var actualID;
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';
$(function (){  

	$('.formError').remove(); 
	 $("#releaseInfoAdd").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	//Save Material Despatch details
	$("#save").click(function(){
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			//alert(childCount);
			if(true){
				contractId = $('#contractId').val();
				buildingId=$('#buildingId').val();
				tenantReleaseLetterNo=$('#tenantReleaseLetterNo').val();
				tenantReleaseLetterDate=$('#tenantReleaseLetterDate').val();
				paidAmount=$('#paidAmount').val();
				var paymentMethod = $("#paymentMethod option:selected").map(function(){
					   return $(this).val();
					}).get().join(",");
				noOfCheques=$('#noOfCheques').val();
				cashAmount=$('#cashAmount').val();
				grandTotal=$('#grandTotal').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/add_final_save_release.action",
						data: {contractId: contractId, buildingId: buildingId,tenantReleaseLetterNo: tenantReleaseLetterNo,tenantReleaseLetterDate: tenantReleaseLetterDate,paymentMethod: paymentMethod,
							noOfCheques: noOfCheques,paidAmount: paidAmount, trnValue: transURN},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#loading').fadeOut(); 
						},  
					 	error:function(result){
							$('.tempresultfinal').fadeOut();
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#loading').fadeOut(); 
					 	}  
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of contract Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});
	
	//Adding Rent Details
	$('.addRent').click(function(){ 
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_release_rent_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			return false;
		}
		
	});

	//Adding Fee Details
	$('.addFee').click(function(){
		if($('#count').val()!=0){
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_release_fee_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(7)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(7)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(7)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			return false;
		}
		}
		else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please renew the Contract and then release !!!").slideDown();
			return false; 
		}
	});

	//Edit Rent Details
	$(".editRent").click(function(){
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_release_rent_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
				 var temp5= $($(slidetab).children().get(4)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Edit Fee Details
	$(".editFee").click(function(){
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(6)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_release_fee_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(7)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(7)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(7)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
				 var temp5= $($(slidetab).children().get(4)).text();
				 var temp6= $($(slidetab).children().get(5)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp6);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Delete Rent Details
	$('.delRent').click(function(){
		slidetab=$(this).parent().parent().get(0); 
		$('.error').hide();
       $('.childCountErr').hide();
       $('#warningMsg').hide();
		var trnValue=$("#transURN").val(); 
		actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_release_rent_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
	});

	//Delete Fee Details
	$('.delFee').click(function(){
		slidetab=$(this).parent().parent().get(0); 
		$('.error').hide();
       $('.childCountErr').hide();
       $('#warningMsg').hide();
		var trnValue=$("#transURN").val(); 
		actualID=$($($(slidetab).children().get(6)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_release_fee_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCountFee=Number($('#childCountFee').val());
	       		 if(childCountFee > 0){
	       			childCountFee=childCountFee-1;
					$('#childCountFee').val(childCountFee);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
	});

	//Adding Payment Details
 	$('.addPayment').click(function(){
 		if($('#coun').val()!=0){ 
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_release_payment_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}
		else{
			return false; 
		}
 		}
		else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please renew the Contract and then release !!!").slideDown();
			return false; 
		}
	});

 	//Edit Payment Details
	$(".editPayment").click(function(){
		if($('#releaseInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_release_payment_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Delete Payment Details
 	$('.delPayment').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_release_payment_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCountPayment=Number($('#childCountPayment').val());
	       		 if(childCountPayment > 0){
	       			childCountPayment=childCountPayment-1;
					$('#childCountPayment').val(childCountPayment);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
 	});

	$("#discard").click(function(){
		$('#loading').fadeIn();
		var trnValue=$("#transURN").val();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_discard_release.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
   	});
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_agreement_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	//Payment Method 
	$('#paymentMethod').change(function() {
		var payment=$('#paymentMethod').val();
		//alert("Inside select : "+payment);
		//var paymentFormat=payment.replace(",", "/");
		//var paymentSplit=paymentFormat.split('/');
		//alert("Inside select , payment : "+payment+" paymentSplit : "+paymentSplit);
		if(payment == 'Q'){
			$('#cashAmount').attr('disabled','disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else if(payment == "C,Q"){ 
			$('#cashAmount').removeAttr('disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else {
			$('.PaymentTAB').fadeOut();
			$('#cashAmount').removeAttr('disabled');
			$('#noOfCheques').attr('disabled','disabled');
			$('#noOfCheques').removeClass('validate[required,custom[onlyNumber]]');
		}
	});

	$('#noOfCheques').blur(function() { //Payment Details Add Row by checking Numbers
		$('.PaymentTAB').fadeIn();
		var rowCount=$('#noOfCheques').val();
		$.ajax({
	   		type:"POST",
	   		url:"<%=request.getContextPath()%>/release_info_load_cheque_fee.action", 
	   	 	async: false,
	   	 	data: {noOfCheques: rowCount},
	   	    dataType: "html",
	   	    cache: false,
	   		success:function(result){
		   		$.fn.globeFeeDesc();	//Remove the Existing Entry
	   			$(".tabFee").append(result);
	   			/* if($(".tabFee").height()<255)
					 $(".tabFee").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabFee").height()>255)
					 $(".tabFee").css({"overflow-x":"hidden","overflow-y":"auto"}); */
	   			$.fn.globeTotalFee();  //Total for Fee Amount
	   			$.fn.globeIndexFee();  //Set Index Fee Amount
	   		},
	   		error:function(result){
	   			$(".tabFee").append(result);
	   		}
   		});
		$(".tabPayment").html("");
		$(".tabPayment").animate({height:'0',maxHeight:'255'},0);
		for(i=1;i<=rowCount;i++){
			$.fn.addRowPayment();
		}
	});

	$.fn.addRowPayment = function() { 	// Addrow for Payment Details
		$.ajax({
				 type:"POST",
			      url:"<%=request.getContextPath()%>/add_release_info_paymentrows.action", 
		 	  	async: false,
		     dataType: "html",
		        cache: false,
			success:function(result){
				$(".tabPayment").append(result);
				if($(".tabPayment").height()<255)
					 $(".tabPayment").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabPayment").height()>255)
					 $(".tabPayment").css({"overflow-x":"hidden","overflow-y":"auto"});
				}
			});
	}

	
	
	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/release_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
	// Add Rows for Owner
	$('.addrowsowner').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/property_info_owner_addrow.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tabOwner").append(result);
				if($(".tabOwner").height()<255)
					 $(".tabOwner").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabOwner").height()>255)
					 $(".tabOwner").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});

	// Add Rows for Feature
	$('.addrowsrent').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/releas_info_rent_addrow.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tabRent").append(result);
				if($(".tabRent").height()<255)
					 $(".tabRent").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabRent").height()>255)
					 $(".tabRent").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});

	// Add Rows for Feature
	$('.addrowsfee').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/release_info_addrow.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tabFee").append(result);
				if($(".tabFee").height()<255)
					 $(".tabFee").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabFee").height()>255)
					 $(".tabFee").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});

	

	$('.contract-popup').click(function(){
		var releaseDate=$('#tenantReleaseLetterDate').val();
		if(releaseDate != '' && releaseDate != null ){ 
		tempid=$(this).parent().get(0);  
	       //var orderId=$('#orderId').val();
			$('#release-popup').dialog('open');
			//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/contract_load_release_info.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.release-result').html(result); 
				},
				error:function(result){ 
					 $('.release-result').html(result); 
				}
			}); 
		}else{
			$('.commonErr').show();
			$('.commonErr').hide().html("Please Enter Release Date").slideDown();
			return false; 
		}
		});

	

	$('#tenantReleaseLetterDate').blur(function(){
		$('.commonErr').hide();
	});

	$('#release-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
					
				$(this).dialog("close"); 
			}, 
			"Cancel": function() {
				$(this).dialog("close"); 
			} 
		}
	});
	


	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	// Default Date
	$('#tenantReleaseLetterDate').datepick();

	$('#waterCertiDate').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});

 	$.fn.globeIndexFee = function() { 	// Index For Fee Details
		var index=1;
		$('.indexForFee').each(function(){ 
			$(this).html(index);
			index=index+1;
		});
	 }

	$.fn.globeFeeDesc = function() { 	// Remove the Exisiting Cheque Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Cheque'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }


	$.fn.globeTotalFee = function() { 	// Total for Fee Amount
		var total=0;
		$('.feeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#feeAmountTotal').val(total);
		$('#feeAmountTotalAll').val(total);
	 }

	$.fn.globeTotalCheque = function() { 	// Total Payment Amount
		var total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#chequeAmountTotal').val(total)
		 $('#balance').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val()));
	
	 }

	 $('#cashAmount').blur(function(){
		 $('#balance').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val()));
	 });

 	$.fn.globeTotalAsset = function() { 	// Total Asset Amount
		var total=0;
		$('.assetAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#assetTotalAmount').val(total);
		$('#assetTotalAmountAll').val(total);
	 }

 	$.fn.globeTotalRent = function() { 	// Total Rent Amount
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total);
		$('#rentAmountTotalAll').val(total);
		var total=0;
		$('.balanceAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#balanceAmountTotal').val(total);
	 }
	
});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.releaseinfo"/></div>
		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
		<form id="releaseInfoAdd">
			<fieldset>
				<div class="portlet-content"> 
					<div class="width50 float-right" id="hrm"> 
		 				<fieldset>
							<legend><fmt:message key="re.property.info.releaseinfo2"/></legend>	
								<div>
									<label><fmt:message key="re.property.info.contractdate"/></label>
									<input type="text" name="date" id="contractDate" class="width30" disabled="disabled">
								</div>					
								<div>
									<label><fmt:message key="re.property.info.tenantno"/></label>
									<input type="text" name="tenantId" id="tenantId"class="width30" disabled="disabled">
								</div>
								<div>
									<label><fmt:message key="re.property.info.tenantname"/></label>
									<input type="text" name="tenantName" id="tenantName" class="width30" disabled="disabled">
								</div>
								<div>
									<label><fmt:message key="re.property.info.fromdate"/></label>
									<input type="text" name="fromDate" class="width30" id="fromDate" disabled="disabled">
								</div>		
								<div>
									<label><fmt:message key="re.property.info.todate"/></label>
									<input type="text" name="toDate" id="toDate" class="width30" disabled="disabled">
								</div>																
								
								<div style="display:none"; ><label>Pay After Contract<span style="color:red">*</span></label>
										<select id="payAfterContract" class="width30" >
											<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
													<option value="${building.tenantId }">${building.payAfterContract }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>	
							</fieldset>  																		
						</div> 
					<div class="width50 float-left" id="hrm">
						<fieldset style="">
							<legend><fmt:message key="re.property.info.releaseinfo1"/></legend>
								<div>
									<label><fmt:message key="re.property.info.releaseletterno"/><span style="color:red">*</span></label>
									<input type="text" name="tenantReleaseLetterNo" id="tenantReleaseLetterNo" class="width30 validate[required]" >
								</div>
								<div style="">
									<label style=""><fmt:message key="re.property.info.releaseletterdate"/><span style="color:red">*</span></label>
									<input type="text" name="tenantReleaseLetterDate" id="tenantReleaseLetterDate"  readonly="readonly" class="width30 validate[required]" >
								</div>
								
								<div>
									<label><fmt:message key="re.property.info.contractno"/><span style="color:red">*</span></label>
									<input type="hidden" id="contractId" class="width30 validate[required]" disabled="disabled" />
									<input type="text" id="contractNumber" class="width30 validate[required]" disabled="disabled" />
									<span id="hit7" class="button" style="width: 40px ! important;">
										<a class="btn ui-state-default ui-corner-all contract-popup"> 
											<span class="ui-icon ui-icon-newwin"> </span> 
										</a>
									</span>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.buildingno"/></label>
									<input type="text" name="buildingNumber" class="width30" id="buildingNumber"  disabled="disabled">
									<input type="hidden" name="buildingId" class="width30" id="buildingId"  disabled="disabled">
								</div>
								<div>
									<label><fmt:message key="re.property.info.buildingname"/></label>
									<input type="text" name="buildingName" id="buildingName" class="width30" disabled="disabled">
								</div>
								<div style="display:none";>
									<label class=""><fmt:message key="re.contract.paymentmethod"/><span style="color:red">*</span></label>	 
									<select id="paymentMethod" class="width30" multiple="multiple" >
										<option value="C">Cash</option>
										<option value="Q">Cheque</option>
									</select>
								</div>
								<div style="display:none";>
									<label class=""><fmt:message key="re.contract.noofcheque"/><span style="color:red">*</span></label>	 
									<input class="width30 " type="text" name="noOfCheques"  id="noOfCheques" disabled="disabled" >
								</div>
						</fieldset> 
					</div>
				</div>
			</fieldset>
		</form>
		<div class="clearfix"></div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.assetdetails"/></div>
					<div class="portlet-content"> 
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.maintenancecode"/></th>
			            					<th class="width20">Condition/Status</th>
			            					<th class="width20"><fmt:message key="re.property.info.releasetype"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.remarks"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.amount"/></th>
			            					<th style="width:10%;display:none;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabAsset" style="">	
										<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
										 	<tr class="even"> 
												<td class="width10 lineno"></td>
												<td class="width10"></td>	
												<td class="width20"  style="height: 20px;"></td>	
												<td class="width20"></td>
												<td class="width20"></td>
												<td class="width10 assetAmountCalc"></td> 
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
												</td> 
												<td style=" width:10%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAsset" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAsset"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAsset" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="session_status"></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div style="margin-bottom:20px;float:right;width: 14%;" >
							<label class="width10"><fmt:message key="re.property.info.total"/></label>	
							<input class="width70" type="text" name="assetTotalAmount" disabled="disabled" id="assetTotalAmount" > 
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div style="display:none;" class="portlet-header ui-widget-header float-left addrowsren" style="cursor:pointer;"><label class="width10"><fmt:message key="re.property.info.addrow"/></label>	</div> 
				</div>
			</div>
		</div>	
		<div class="clearfix"></div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.rentdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
				 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
				 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.flatno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.rent"/></th>
			            					<th class="width20" style="display:none;"><fmt:message key="re.property.info.paidamount"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.balance"/></th>
			            					<th style="width:10%;display:none;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabRent" style="">	
										<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
										 	<tr class="even"> 
												<td class="width10 lineno"></td>
												<td class="width20 flatno"></td>	
												<td class="width20 rentAmountCalc" style="height: 20px;"></td> 
												<td class="width20 message" style="display:none;"></td>
												<td class="width20 balanceAmountCalc"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
												</td> 
												<td style=" width:10%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="session_status"></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width: 60%;">
							<label>Total</label>
						 	<input name="rentAmountTotal" id="rentAmountTotal" style="width:45%!important;" disabled="disabled" >
						 	<input name="contractAmountTotal" id="balanceAmountTotal" style="width:45%!important;" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div style="display:none;" class="portlet-header ui-widget-header float-left addrowsren" style="cursor:pointer;"><label class="width10"><fmt:message key="re.property.info.addrow"/></label>	</div> 
				</div>
			</div>
		</div>	
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container "> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.feedetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsgFee" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
				 		<input type="hidden" name="childCountFee" id="childCountFee"  value="0"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.feeid"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.description"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.feeamount"/></th>
			            					<th class="width20" style="display:none;">Paid Amount</th>
			            					<th class="width10" style="display:none;"><fmt:message key="re.property.info.balance"/></th>			            					
											<th style="width:10%;display:none;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabFee" style="">	
										<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
										 	<tr class="even"> 
												<td class="width10"></td>
												<td class="width20"></td>	
												<td class="width20" style="height: 20px;"></td> 
												<td class="width20 feeAmountCalc"></td>
												<td class="width20" style="display:none;"></td> 
												<td class="width10" style="display:none;"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
												</td> 
												<td style="width:10%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFee" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFee"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFee" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:39%; margin-right:-7%;">
							<label class="width10"><fmt:message key="re.property.info.total"/></label>
						 	<input name="feeAmountTotal" id="feeAmountTotal" class="width70" disabled="disabled">
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div style="display:none;" class="portlet-header ui-widget-header float-left addrowsfee" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container PaymentTAB" style="display:none;"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.paymentdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content">  
				 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value="0"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100" > 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width20"><fmt:message key="re.property.info.bankname"/></th> 
											<th class="width20"><fmt:message key="re.property.info.chequedate"/></th>
											<th class="width20"><fmt:message key="re.property.info.chequeno"/></th>
											<th class="width20"><fmt:message key="re.property.info.chequeamount"/></th>
											<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabPayment" style="">	
										<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
										 	<tr class="even"> 
												<td class="width20"></td>
												<td class="width20"></td>	
												<td class="width20"></td> 
												<td class="width20 chequeAmountCalc"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment1" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment1"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment1" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:33%">
							<label><fmt:message key="re.property.info.total"/></label>
						 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
					<div style="display:none;" class="portlet-header ui-widget-header float-left addrowspaymen"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>
		<div id="hrm" class="width100"> 
			<fieldset class="width50" style="margin:0 auto">
				<legend><fmt:message key="re.property.info.finalcalculation"/></legend>	
				<div>
					<label>Total Asset</label>
					<input type="text" name="assetTotalAmountAll" id="assetTotalAmountAll" class="width30" disabled="disabled">
				</div>
				<div>
					<label>Total Rent</label>
					<input type="text" name="rentAmountTotalAll" id="rentAmountTotalAll" class="width30" disabled="disabled">
				</div>
				<div>
					<label>Total Fee's</label>
					<input type="text" name="feeAmountTotalAll" id="feeAmountTotalAll" class="width30" disabled="disabled" >
				</div>						
				<div  style="display:none;">
					<label><fmt:message key="re.property.info.dueamount"/></label>
					<input type="text" name="dueAmount" id="dueAmount" class="width30" disabled="disabled">
				</div>
				<div  style="display:none;">
					<label><fmt:message key="re.property.info.depositamount"/></label>
					<input type="text" name="depositAmount" id="amount" class="width30" disabled="disabled">
				</div>
				<div  style="display:none;">
					<label><fmt:message key="re.contract.cashamount"/></label>
					<input type="text" name="payableAmount" id="cashAmount" class="width30" >
				</div>																
				<div style="display:none;"><label><fmt:message key="re.property.info.paidamount"/><span style="color:red">*</span></label><input type="text" name="paidAmount" id="paidAmount" class="width30" ></div>
				<div  style="display:none;">
					<label><fmt:message key="re.property.info.balanceamount"/></label>
					<input type="text" name="balanceAmount" id="balanceAmount" class="width30" disabled="disabled">
				</div>
				<div  style="display:none;">
					<label><fmt:message key="re.property.info.balance"/></label>
					<input type="text" name="balance" id="balance" class="width30" disabled="disabled">
				</div>																
			</fieldset> 
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:0 auto" style="margin:10px ;"> 
			<div class="portlet-header ui-widget-header float-right cancelrec" id="discard" style=" cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;display:none;">Print</div>
			<div class="portlet-header ui-widget-header float-right headerData" id="save" style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
		</div>
	</div>
</div>


<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="release-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="release-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>