<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style>
table.display td{
	padding:3px;
}
.ui-widget-header{
	padding:4px;
}
</style> 

<script type="text/javascript">
var id;
var oTable2; var selectRow=""; var aData="";var aSelected1 = [];var s=""; 
$(document).ready(function (){
		 
 	$('#example1').dataTable({ 
 		"sAjaxSource": "contract_release_renewal_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 		"aoColumns": [
 			{ "sTitle": 'Contract ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "125px"},
 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "250px"},
 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.createdOn"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.decision"/>',"sWidth": "200px"},
 		],
 		"sScrollY": (Number($("#main-content").height() - 220) / 2 ) - 75,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow1, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData1.DT_RowId, aSelected1) !== -1) {
 				$(nRow1).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable2 = $('#example1').dataTable();
 	/* Click event handler */
 	$('#example1 tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable2.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable2.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable2.fnGetData( this );
 	        s=aData[0];
 	    }
 	});

});
function getRenew(contractId) {
	var answer = confirm ("Please confirm to renew the contract");
	if (answer){
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/renew_contract_agreement.action", 
		data: {contractId: contractId,renewalFlag:"1"},
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#main-wrapper").html(result); 
		}, 
		error: function(result){ 
			$("#main-wrapper").html(result); 
		}
	}); 
	}else{

		return false;
	}
}
function getRelease(contractId) {
	var answer = confirm ("You want to 'Release' this Contract?");
	if (answer){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_release_renew_update.action",  
	     	data: {contractId: contractId,renewalFlag:0}, 
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data){
				$("#main-wrapper").html(data); //gridDiv main-wrapper
	     	}
		});
	}else{

		return false;
	}
}
</script>
<body>
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.renewRelease"/></div>	 
		<div class="portlet-content">
			<div id="gridResult">
				<table class="display dataTable" id="example1"></table>
			</div>
		</div>
</div>
</body>