<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-flat_maintainence_search.action?personId="+personId+'&contractId ='+contractId+'& flatDescription ='+flatDescription+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['flatId','contract ID','<fmt:message key="re.property.info.contractnumber"/>','Person ID','<fmt:message key="re.property.info.engineername"/>','<fmt:message key="re.property.info.description"/>','Work Flow Status'], 
		 colModel:[ 
				{name:'flatId',index:'FLAT_ID', width:100,  sortable:true},
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
		  		{name:'contractNo',index:'CONTRACT_NO', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'personId',index:'PERSON_ID', width:100,  sortable:true},
		  		 {name:'personName',index:'PERSON_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'flatDescription',index:'DESCRIPTION', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true, sorttype:"data"},
		  		
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'FLAT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.flatmaintenancerelease"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["flatId","contractId","personId"]);
 	});
	</script>
 <body>
<table id="list2"></table>  
<div id="pager2"> </div> 
</body>
 	