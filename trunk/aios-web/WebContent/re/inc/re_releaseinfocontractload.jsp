<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <div class="selectable width100" id="hrm"> 
     <div class="float-left width100" style="background-color:#dfdfdf;padding:5px;">
     	<div class="width20 float-left"><span>Contract Number</span></div> 
     	<div class="width40 float-left" style=""><span>Building Name</span></div>
     	<div class="width40 float-left"><span>Tenant Name</span></div>
     	<div class="width20 float-left" style="display:none;"><span>Contract Date</span></div>
     	<div class="width20 float-left" style="display:none;"><span>From Date</span></div>
     	<div class="width20 float-left" style="display:none;"><span>To Date</span></div>
     	<div class="width20 float-left" style="display:none;"><span>Deposit Amount</span></div>
     </div> 
  <c:choose>
		<c:when test="${requestScope.itemList ne null}">	 
			 <c:forEach items="${itemList}" var="item" varStatus="status"> 
			 	 <div class="selectlist float-left width100">   
			 	 	 <input type="hidden" value="${item.contractId}"/>
			 	 	 <input type="hidden" value="${item.buildingId}"/>
			 	 	 <input type="hidden" value="${item.buildingNumber}"/>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0 ;"><span>${item.contractNumber}</span></div>
			 	 	 <div class="width40 float-left" style="padding:1% 0  1% 0;"><span>${item.buildingName}</span></div>
			 	 	 <div class="width40 float-left" style="padding:1% 0  1% 0;"><span>${item.tenantName}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.contractDate}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.fromDate}</span></div>
			 	 	  <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.toDate}</span></div>
			 	 	  <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.tenantId}</span></div>
			 	 	  <div class="width20 float-left" style="padding:1% 0  1% 0;display:none;"><span>${item.amount}</span></div>
		         </div>
			</c:forEach>
		 </c:when>
		<c:otherwise>
		   <div class="width50 float-right" style="padding:1% 0  1% 0;"><span>No Records Available</span></div>
		</c:otherwise>
	</c:choose>
 </div>
<script type="text/javascript">
  $(function(){
	  $(".ui-dialog-titlebar").remove();
	   $(".selectlist").click(function(){
            $(temp).css('background-color','#fff');
            $(this).css('background-color','#F39814');
            temp=this;
        });
	   $(".selectlist").mousedown(function(){
            $(this).css('background-color','#FECA40');
        });
	    $('.selectlist').click(function(){
	       $('#contractId').val($($(this).children().get(0)).val());
	       $('#buildingId').val($($(this).children().get(1)).val());
	       $('#buildingNumber').val($($(this).children().get(2)).val());
	       $('#contractNumber').val($($($(this).children().get(3)).children().get(0)).html());
	       $('#buildingName').val($($($(this).children().get(4)).children().get(0)).html());
	       $('#tenantName').val($($($(this).children().get(5)).children().get(0)).html());
	       $('#contractDate').val($($($(this).children().get(6)).children().get(0)).html());
	       $('#fromDate').val($($($(this).children().get(7)).children().get(0)).html());
	       $('#toDate').val($($($(this).children().get(8)).children().get(0)).html());
	       $('#tenantId').val($($($(this).children().get(9)).children().get(0)).html());
	       $('#amount').val($($($(this).children().get(10)).children().get(0)).html());
           // $($(tempid).siblings().get(2)).val($($(this).children().get(1)).html());
			tenantReleaseLetterDates=$('#tenantReleaseLetterDate').val();
			contractId=$($(this).children().get(0)).val().trim();
	       $.ajax({
		   		type:"POST",
		   		url:"<%=request.getContextPath()%>/contract_load_rent_release_info.action", 
		   	 	async: false,
		   	 	data: {tenantReleaseLetterDate: tenantReleaseLetterDates,contractId: contractId},
		   	    dataType: "html",
		   	    cache: false,
		   		success:function(result){
		   			$(".tabRent").html(result);
		   			//alert(result);
		   			$.fn.globeTotalRent();  //For Total Amount of Rent
		   			//if($(".tabRent").height()<255)
		   			//	 $(".tabRent").animate({height:'+=20',maxHeight:'255'},0);
		   			// if($(".tabRent").height()>255)
		   			//	 $(".tabRent").css({"overflow-x":"hidden","overflow-y":"auto"});
		   		}
	   		});
	       
	       
		   $.ajax({
		   		type:"POST",
		   		url:"<%=request.getContextPath()%>/contract_load_fee_release_info.action", 
		   	 	async: false,
		   	 	data: {contractId: contractId},
		   	    dataType: "html",
		   	    cache: false,
		   		success:function(result){
		   			$(".tabFee").html(result);
		   			$.fn.globeTotalFee();  //For Total Amount of Fee
		   			//if($(".tabRent").height()<255)
		   			//	 $(".tabRent").animate({height:'+=20',maxHeight:'255'},0);
		   			// if($(".tabRent").height()>255)
		   			//	 $(".tabRent").css({"overflow-x":"hidden","overflow-y":"auto"});
		   		}
	   		});

		   $.ajax({
		   		type:"POST",
		   		url:"<%=request.getContextPath()%>/contract_load_asset_release_info.action", 
		   	 	async: false,
		   	 	data: {contractId: contractId},
		   	    dataType: "html",
		   	    cache: false,
		   		success:function(result){
		   			$(".tabAsset").html(result);
		   			$.fn. globeTotalAsset();  //For Total Amount of Asset
		   			//if($(".tabRent").height()<255)
		   			//	 $(".tabRent").animate({height:'+=20',maxHeight:'255'},0);
		   			// if($(".tabRent").height()>255)
		   			//	 $(".tabRent").css({"overflow-x":"hidden","overflow-y":"auto"});
		   		}
	   		});

		  
	   		
           
    });  
}); 
</script>