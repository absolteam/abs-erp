<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width: 50%;
	}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}
</style>
</head>
<script type="text/javascript">
var contractDate="";
var fromDate="";
var toDate="";
var tenantId=0;
var unitId=0;
var tenantName="";
var tenantTypeName="";
var unitName="";
var oTable=null; var selectRow=""; var aData="";var aSelected = [];var unitReportId=0;var s=""; var s1=""; 

$(function (){
	$('.formError').remove();   
		
	 
	 //Onload Call
	 paymentListJsonCall();
	 
	
	 $('.fromDate').change(function(){  
			 contractListCall();
		 
	 }); 
	 
	 $('.toDate').change(function(){
			 contractListCall();
	 });
	 
	 $(".print-call").click(function(){ 
		 fromDate=$('.fromDate').val();
		 toDate=$('.toDate').val(); 
		 tenantId=$('#tenantId').val();
			window.open('<%=request.getContextPath()%>/contract_rental_range_report_printout.action?fromDate='
					+ fromDate+'&toDate='+toDate,
					+'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		});
	  $(".pdf-download-call").click(function(){ 
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			 percentage=$('#percentage').val();

			window.open('<%=request.getContextPath()%>/downloadContractRentalRangeRoport.action?fromDate='
					+ fromDate+'&toDate='+toDate+'&format=PDF',
					+'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		}); 
	  $(".xls-download-call").click(function(){
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			window.open('<%=request.getContextPath()%>/contract_rental_range_report_download_xls.action?fromDate='
					+ fromDate+'&toDate='+toDate,
					+'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			
			return false;
		}); 
	  $('#view').click(function(){ 
			callReportView();
			return false;
		}); 
		
	  /* Click event handler */
	 	$('#ContractRentRange tbody tr').live('click', function () {  
	 		  if ( $(this).hasClass('row_selected') ) {
	 			  $(this).addClass('row_selected');
	 	        aData =oTable.fnGetData( this );
	 	        s1=aData[0]; 
	 	    }
	 	    else {
	 	        oTable.$('tr.row_selected').removeClass('row_selected');
	 	        $(this).addClass('row_selected');
	 	        aData =oTable.fnGetData( this );
	 	        s1=aData[0];
	 	    }
	 	}); 
	 	
	 	
	 	$('#ContractRentRange tbody tr').live('dblclick', function () {
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
				  aData =oTable.fnGetData( this );
		 	        s1=aData[0];
		 	       callReportView();
			       return false;
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		 	      s1=aData[0];
		 	      callReportView();
			      return false;
		      }
		});
}); 
function contractListCall(){
	oTable = $('#ContractRentRange').dataTable();
	oTable.fnDestroy();
	$('#ContractRentRange').remove();
	$('#gridDiv').html("<table class='display' id='ContractRentRange'></table>");
	paymentListJsonCall();
}
function paymentListJsonCall(){
	 fromDate=$('.fromDate').val();
	 toDate=$('.toDate').val(); 
	 $('#ContractRentRange').dataTable({ 
	 		"sAjaxSource": "contract_rental_range_report_json.action?fromDate="+fromDate+"&toDate="+toDate,
	 	    "sPaginationType": "full_numbers",
	 	    "bJQueryUI": true, 
	 		"aoColumns": [
	 			{ "sTitle": 'Contract ID', "bVisible": false},
	 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
	 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
	 			{ "sTitle": 'Unit',"sWidth": "300px"},
	 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
	 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
	 			{ "sTitle": 'Amount',"sWidth": "100px"},
	 			
	 		],
	 		"sScrollY": $("#main-content").height() - 315,
	 		"aaSorting": [[1, 'desc']], 
	 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
	 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
	 				$(nRow).addClass('row_selected');
	 			}
	 		}
	 	});	
	 	//Json Grid
	 	//init datatable
	 	oTable = $('#ContractRentRange').dataTable();

}
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
   var daysInMonth =$.datepick.daysInMonth(
   $('#selectedYear').val(), $('#selectedMonth').val());
   $('#selectedDay option:gt(27)').attr('disabled', false);
   $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
   if ($('#selectedDay').val() > daysInMonth) {
       $('#selectedDay').val(daysInMonth);
   }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
} 
function getId(parentTR){ 
	if ($(parentTR).hasClass('row_selected') ) { 
		$(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0]; 
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(parentTR).addClass('row_selected');
        aData =oTable.fnGetData( parentTR );
        s1=aData[0];
    }
	return s1;
}	
function callReportView(){
	$('#loading').fadeIn();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getPropertyContractDetail.action", 
		data:{format:"HTML",recordId:s1,approvalFlag:"N"},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$("#reporting-tree").html(result); 
			$("table:first").addClass("table-custom-design-report");
			$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
			$("#reporting-tree").append("<div style='margin-top:-40px;' class='portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons'>"+
			"<div class='portlet-header ui-widget-header float-left'><a class='' style='cursor: pointer;' onclick='reportAjaxCallFunction(this);' id='contract_rental_range_report'>Back</a></div></div>");
			$('#loading').fadeOut();
			return false;
		}
 	});
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:93%;">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Detail Report</div>	 
		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
				</c:if>    
			</div>	
			<div>
				<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
				</c:if>
			 </div> 
			<div class="width45 float-left" style="padding:5px;">  
				 <div>
				 	<label class="width30" style="margin-top: 5px;">Greater then Rent</label>
				 	<input type="text" name="fromDate" class="width60 fromDate" id="fromDate">
				 </div>
			 </div>
			<div class="width45 float-left" style="padding:5px;">  
				 <div>
				 	<label class="width30" style="margin-top: 5px;">Less then Rent</label>
				 	<input type="text" name="toDate" class="width60 toDate" id="toDate">
				 </div>
			 </div>
			<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="ContractRentRange"></table>
				</div>
			</div>	
			<div class="vmenu">
				<div class="first_li"><span id="view">View</span></div>
				<div class="sep_li"></div>		 
			       	<div class="first_li"><span class="print-call">Print</span></div>
				<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
				<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
			 </div> 
			 
		</div>
		
	</div> 
</div> 
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div>