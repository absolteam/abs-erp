 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>

<style>
	.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width: 50%;
	}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}
</style>

<script type="text/javascript">

var id;var oTable; var selectRow=""; var aData ="";
var offerId = 0; var aSelected = []; 
var tenantId = 0; var unitId = 0;
var tenantType = "";
var filterURI = "get_filtered_offer_report.action?reportFilter=none&startDate=&endDate=";
var filterKey = "reportFilter=none&startDate=&endDate=";

$(function() {  
	
	 $('.formError').remove();  
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});	
	 
		$('#example').dataTable({ 
			"sAjaxSource": filterURI,
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "OFFER_ID", "bVisible": false},
				{ "sTitle": '<fmt:message key="re.offer.offerno" />',"sWidth": "100px"},
				{ "sTitle": '<fmt:message key="re.offer.tenant" />',"sWidth": "250px"},
				{ "sTitle": '<fmt:message key="re.offer.offerdate" />',"sWidth": "125px"},
				{ "sTitle": '<fmt:message key="re.contract.offerPeriod"/>',"sWidth": "150px"}, 
				{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "250px"},
			],
			"sScrollY": $("#main-content").height() - 335,
			"aaSorting": [[1, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	 

		oTable = $('#example').dataTable();
	 
		$('#example tbody tr').live('click', function () {  
			  if($(this).hasClass('row_selected')){ 
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];  
		      }
		      else{ 
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0]; 
		      }
		});
		$('#example tbody tr').live('dblclick', function () {  
			  if($(this).hasClass('row_selected')){ 
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];  
		          callReportView();
		      }
		      else{ 
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];
		          callReportView();
		      }
		});
		$(".print-call").click(function(){ 
			
			window.open('<%=request.getContextPath()%>/offer_report_printout.action?' + filterKey,
					'_blank',
					'width=800,height=700,scrollbars=yes,left=100px,top=2px');
			
			return false;
		});	
		
		$(".xls-download-call").click(function(){ 
			
			window.open('<%=request.getContextPath()%>/offer_report_printout_xls.action?' + filterKey,
					'_blank',
					'width=200,height=200,scrollbars=yes,left=100px,top=2px');
			
			return false;
		});	
		
		$(".pdf-download-call").click(function(){ 
			
			window.open('<%=request.getContextPath()%>/offer_report_printout_pdf.action?' + filterKey,
					'_blank',
					'width=800,height=700,scrollbars=yes,left=100px,top=2px');
			
			return false;
		});	
		$(".view").click(function(){ 
			callReportView();
		});
		$(".tenantName").combobox({ 
		      selected: function(event, ui) {
		      	
		    	  	tenantId = $(this).val();  
		      		var splitArray = tenantId.split('@');
		      		tenantId = splitArray[0]; 
		      		tenantType = (splitArray[1] == "C") ? "tenantCompany" : "tenantPerson";
		      		$("#tenantId").val(tenantId);
		      				      		
		      		filterKey = "reportFilter=" + tenantType + "&tenantId=" + tenantId;
		      		filterURI = "redirect_to_filtered_offer_report.action?" + filterKey;
		      		triggerRedirect();
		      }
		}); 
		
		$(".unitName").combobox({ 
		       selected: function(event, ui){
		    	   
		    	    $('.fromDate').val("");
		    	    fromDate = $('.fromDate').val();
		    	    $('.toDate').val("");
		    	    toDate = $('.toDate').val();
		    	    $(".tenantName").val("");
		    	    tenantId = 0;
		    	    tenantType = "";
		    	    
		    	    unitId = $(this).val();   
		    	    filterKey = "reportFilter=unit&unitId=" + unitId + "&startDate=&endDate=";
		    	    filterURI = "redirect_to_filtered_offer_report.action?" + filterKey;
		    	    triggerRedirect();
		       }
	    }); 
		
		$("#reset").click(function () {
			$("#offer_report_redirect").trigger('click');
		});
	}); 
	
	function checkLinkedDays() {
	   
		var daysInMonth =$.datepick.daysInMonth(
	   	$('#selectedYear').val(), $('#selectedMonth').val());
	   	$('#selectedDay option:gt(27)').attr('disabled', false);
	   	$('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	   	if ($('#selectedDay').val() > daysInMonth) {
	    	$('#selectedDay').val(daysInMonth);
	   	}
	} 
	
	function customRange(dates) {
		
		if (this.id == 'startPicker') {
			//$('.fromDate').trigger('change');
			$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			//$('.toDate').trigger('change');
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
	function triggerDateFilter() {
		
		var tenantTypeFilter = "";
		fromDate = ($('.fromDate').val() == "") ? "0" : $('.fromDate').val();
		toDate = ($('.toDate').val() == "") ? "0" : $('.toDate').val();
		
		if($(".tenantName").val() != "" && tenantId != 0 && tenantType != "") {
			
			if(tenantType == "tenantCompany")		
				tenantTypeFilter = "tenantCompanyWithinPeriod";				
			else
				tenantTypeFilter = "tenantPersonWithinPeriod";
				
			filterKey = "reportFilter=" + tenantTypeFilter
			+ "&startDate=" + fromDate 
			+ "&endDate=" + toDate 
			+ "&tenantId=" + tenantId;				
		} else {
			
			filterKey = "reportFilter=period&startDate=" + fromDate + "&endDate=" + toDate;	
		}
		
		filterURI = "redirect_to_filtered_offer_report.action?" + filterKey;		
		triggerRedirect();
	}
	
	function triggerRedirect() {
		
		$.ajax({
 			type: "POST", 
 			url: filterURI, 
 	     	async: false,
 			dataType: "html",
 			cache: false,
 			success: function(result) { 
 				$("#offer-result-div").html(result); 
 			},
 			error: function(result) { 
 				alert("error: " + result);
 			} 
 		}); 
	}
	function callReportView(){
		$('#loading').fadeIn();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getPropertyOfferDetail.action", 
			data:{format:"HTML",recordId:offerId,approvalFlag:"N"},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#reporting-tree").html(result); 
				$("table:first").addClass("table-custom-design-report");
				$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
				$("#reporting-tree").append("<div style='margin-top:-40px;' class='portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons'>"+
				"<div class='portlet-header ui-widget-header float-left'><a class='' style='cursor: pointer;' onclick='reportAjaxCallFunction(this);' id='offer_report_redirect'>Back</a></div></div>");
				$('#loading').fadeOut();
				return false;
			}
	 	});
	}
</script> 

<div id="main-content">
	<div id="" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerReport" /></div>	 
		<div class="portlet-content">
			<div class="width45 float-left" style="padding:5px;">  
				 <div>
				 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDatefrom" /></label>
				 	<input type="text" name="fromDate" class="width60 fromDate" id="startPicker" readonly="readonly" onblur="triggerDateFilter()">
				 </div>
				 <div>
				 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDateTo" /></label>
				 	<input type="text" name="toDate" class="width60 toDate" id="endPicker" readonly="readonly" onblur="triggerDateFilter()">
				 </div>
			 </div>
			 <div class="width45 float-right" style="padding:5px;">  
				 <div>
				 	<label class="width30"><fmt:message key="re.offer.tenant" /></label>
				 	<input type="hidden" id="tenantId"  />
				 	<select id="tenantName" class="tenantName" name="tenantName">
						<option value="">Select</option>
						<c:forEach items="${TENANT_INFO_OFFER}" var="bean">
							<option value="${bean.tenantNumber}">${bean.tenantName}</option>
						</c:forEach>
					</select>
				 </div> 
				 <div>
				 	<label class="width30"><fmt:message key="re.offer.propertyUnit" /></label>
				 	<select id="unitName" class="unitName" name="unitName">
						<option value="">Select</option>
						<c:forEach items="${UNIT_INFO_OFFER}" var="bean">
							<option value="${bean.unitId}">${bean.unitName}</option>
						</c:forEach>
					</select>
				 </div> 
			 </div>
			 <div class="float-right">
				<div class="portlet-header ui-widget-header float-right" id="reset" style="cursor: pointer;">
					<fmt:message key="sys.common.resetSelection" />
				</div>
				<!-- <div class="portlet-header ui-widget-header float-right" id="apply_filter" style="cursor: pointer;">
					Apply Filter
				</div> -->
			 </div>
		 	 <div id="offer-result-div" class="width100 float-left">
		 	 	<div id="rightclickarea">
			 	 	<div id="gridDiv">
						<table class="display" id="example"></table>
				 	</div> 
				 </div>
			 	<div class="vmenu">
				 	<div class="first_li"><span class="view">View</span></div>
				 	<div class="sep_li"></div>		 
				       	<div class="first_li"><span class="print-call">Print</span></div>
					<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
					<div class="first_li"><span class="pdf-download-call">Download s PDF</span></div>					
				</div>
		 	 </div>
		</div>
	</div>
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div>