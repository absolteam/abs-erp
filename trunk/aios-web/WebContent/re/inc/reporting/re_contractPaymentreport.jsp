<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var buildingName="";
var oTable=null; var selectRow=""; var aData="";var aSelected = [];var unitReportId=0;var s=""; 
var redirectedUnitId=0;
var paymentStatus;
var fromDate="";
var toDate="";
$(document).ready(function (){
	$('.formError').remove();

	paymentStatus="-1";

	paymentListJsonCall();
	
	$('#paymentStatus').live('change', function () {
			checkDataTableExsist();
	});
	
 	/* Click event handler */
 	$('#ContractPayment tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	});

	$("#view").click( function() { 
		callReportView();
		return true;	
	});
	$("#ContractUnit").click( function() { 
		redirectedUnitId=unitReportId;
		$.ajax({
			type:"POST",
			url:'<%=request.getContextPath()%>/getunit_contract.action',
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
				$("#reporting-tree").html(result);
				$("#page-wrapper #main-wrapper #main-content").css("margin-right", 0);
			}
			
		});	
	});
	
	 $(".print-call").click(function(){ 
		 
			window.open('<%=request.getContextPath()%>/contract_payment_report_printout.action?paymentStatusStr='+paymentStatus+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		});
	  $(".pdf-download-call").click(function(){ 
			
			window.open('<%=request.getContextPath()%>/downloadContractPaymentList.action?format=PDF&approvalFlag='+paymentStatus+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		}); 
	  $(".xls-download-call").click(function(){
			
			window.open('<%=request.getContextPath()%>/contracct_payment_report_download_xls.action?paymentStatusStr='+paymentStatus+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			
			return false;
		}); 
	  
	 	 $('#startPicker,#endPicker').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});	
		 
		 
		 $('.fromDate').change(function(){  
			 if($('.toDate').val()!=null && $('.toDate').val()!='')
				 checkDataTableExsist();
		 }); 
		 
		 $('.toDate').change(function(){
			 if($('.fromDate').val()!=null && $('.fromDate').val()!='')
				 checkDataTableExsist();
		 });
	
});
function checkDataTableExsist(){
	oTable = $('#ContractPayment').dataTable();
	oTable.fnDestroy();
	$('#ContractPayment').remove();
	$('#gridDiv').html("<table class='display' id='ContractPayment'></table>");
	paymentListJsonCall();
}
function paymentListJsonCall(){
	
	paymentStatus=$('#paymentStatus').val();
	fromdate=$('#startPicker').val();
	toDate=$('#endPicker').val();
	
	$('#ContractPayment').dataTable({ 
 		"sAjaxSource": "contract_payment_report_json.action?paymentStatusStr="+paymentStatus+"&fromDate="+fromdate+"&toDate="+toDate,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Contract Payment ID', "bVisible": false},
 			{ "sTitle": 'Contract Number'},
 			{ "sTitle": 'Offer Number', "bVisible": false},
 			{ "sTitle": 'Tenant Name'},
 			{ "sTitle": 'Property Name'},
 			{ "sTitle": 'Unit', "bVisible": false},
 			{ "sTitle": 'Payment Type'},
 			{ "sTitle": 'Payment Mode'},
 			{ "sTitle": 'Amount'},
 			{ "sTitle": 'Bank Name'},
 			{ "sTitle": 'Cheque Date'},
 			{ "sTitle": 'Cheque No.'},
 			{ "sTitle": 'Payment Status'},
 		],
 		"sScrollY": $("#main-content").height() - 300,
		//"bPaginate": false,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}	 
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#ContractPayment').dataTable();
}
function callReportView(){
	$('#loading').fadeIn();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getPropertyUnitDetail.action", 
		data:{format:"HTML",recordId:unitReportId,approvalFlag:"N"},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$("#reporting-tree").html(result); 
			$("table:first").addClass("table-custom-design-report");
			$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
			$('#loading').fadeOut();
		}
 	});
}
function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}  
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Payments</div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				 <div class="width50 float-left" id="hrm" style="padding:5px;">  
					 <div class="width50 float-left">
					 	<label class="width30" style="margin-top: 5px;">From Date</label>
					 	<input type="text" name="fromDate" class="width40 fromDate" id="startPicker" readonly="readonly">
					 </div>
					 <div class="width50 float-left">
					 	<label class="width30" style="margin-top: 5px;">To Date</label>
					 	<input type="text" name="toDate" class="width40 toDate" id="endPicker" readonly="readonly">
					 </div>
			 	</div>
			 	<div class="width40 float-left" id="hrm" style="padding:5px;">  
					 <div class="width100 float-left">
					 	<label class="width30" style="margin-top: 5px;">Payment Status</label>
					 	<select class="width40" id="paymentStatus">
					 		<option value="-1">All</option>
					 		<option value="1">Paid</option>
					 		<option value="2">Pending</option>
					 		<option value="3">Canceled</option>
					 	</select>
					 </div>
					
				 </div>
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="ContractPayment"></table>
					 </div>
				 </div>	
				 <div class="vmenu">
		 	       	<div class="first_li"><span class="print-call">Print</span></div>
					<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
					<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
				 </div>	
			</div>
		</div>
	</div>
	<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 
</body>