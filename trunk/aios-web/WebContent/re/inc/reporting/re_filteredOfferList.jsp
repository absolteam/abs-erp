 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">

$(function() {  
		 
		$('#tenantGrid').dataTable({ 
			"sAjaxSource": "get_filtered_offer_report.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "OFFER_ID", "bVisible": false},
				{ "sTitle": '<fmt:message key="re.offer.offerno" />',"sWidth": "100px"},
				{ "sTitle": '<fmt:message key="re.offer.tenant" />',"sWidth": "250px"},
				{ "sTitle": '<fmt:message key="re.offer.offerdate" />',"sWidth": "125px"},
				{ "sTitle": '<fmt:message key="re.contract.offerPeriod"/>',"sWidth": "150px"}, 
				{ "sTitle": '<fmt:message key="re.contract.createdBy"/>',"sWidth": "250px"},
			],
			"sScrollY": $("#main-content").height() - 335,
			"aaSorting": [[1, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
		
		oTable = $('#tenantGrid').dataTable();
		 
		$('#tenantGrid tbody tr').live('click', function () {  
			  if($(this).hasClass('row_selected')){ 
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];  
		      }
		      else{ 
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0]; 
		      }
		});
		
		$('#tenantGrid tbody tr').live('dblclick', function () {  
			  if($(this).hasClass('row_selected')){ 
				  $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];  
		          callReportView();
		      }
		      else{ 
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData =oTable.fnGetData( this );
		          offerId=aData[0];
		          callReportView();
		      }
		});

	}); 
</script> 
<div id="rightclickarea">
<div id="gridDiv">
	<table class="display" id="tenantGrid"></table>
</div> 
</div>
 <div class="vmenu">
 	<div class="first_li"><span class="view">View</span></div>
 	<div class="sep_li"></div>		 
       	<div class="first_li"><span class="print-call">Print</span></div>
	<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
	<div class="first_li"><span class="pdf-download-call">Download s PDF</span></div>					
</div>				 	
		 	 