<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}

.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:60%}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
.ui-autocomplete{
	width:250px!important;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var id;
var buildingName="";
var oTable=null; var selectRow=""; var aData="";var aSelected = [];var unitReportId=0;var s=""; 
var redirectedUnitId=0;
var fromDate="";
var toDate="";
$(document).ready(function (){
	$('.formError').remove();
	paymentListJsonCall();
	 $(".propertyName").combobox({ 
	       selected: function(event, ui){
	    	   propertyId=$(this).val();  
	    	   buildingName=$('.propertyName option:selected').text(); 
		      	$("#propertyId").val(propertyId);
		      	if($('.toDate').val()!=null && $('.toDate').val()!='' && $('.fromDate').val()!=null && $('.fromDate').val()!='')
		      		checkDataTableExsist();
	       }
	  }); 

	
 	/* Click event handler */
 	$('#PropertyUnitRent tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       unitReportId=aData[0];
 	    }
 	});


	 $(".print-call").click(function(){ 
		 	propertyId=Number($('#propertyId').val());
			fromdate=$('#startPicker').val();
			toDate=$('#endPicker').val();
			window.open('<%=request.getContextPath()%>/property_unit_rental_printout.action?buildingId='+propertyId+'&buildingName='+buildingName+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		});
	  $(".pdf-download-call").click(function(){ 
		 	propertyId=Number($('#propertyId').val());
			fromdate=$('#startPicker').val();
			toDate=$('#endPicker').val();
			window.open('<%=request.getContextPath()%>/downloadPropertyUnitRentalReport.action?format=PDF&recordId='+propertyId+'&tempString='+buildingName+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			return false;
			
		}); 
	  $(".xls-download-call").click(function(){
		  	propertyId=Number($('#propertyId').val());
			fromdate=$('#startPicker').val();
			toDate=$('#endPicker').val();
			window.open('<%=request.getContextPath()%>/property_unit_rental_report_download_xls.action?buildingId='+propertyId+'&buildingName='+buildingName+'&fromDate='+fromdate+'&toDate='+toDate,
					'_blank','width=500,height=400,scrollbars=yes,left=100px,top=2px');	
			
			return false;
		}); 
	  
	 	 $('#startPicker,#endPicker').datepick({
		 onSelect: customRange, showTrigger: '#calImg'});	
		 
		 
		 $('.fromDate').change(function(){  
			 if($('.toDate').val()!=null && $('.toDate').val()!='' && $('#propertyId').val()!=null && $('#propertyId').val()!='')
				 checkDataTableExsist();
		 }); 
		 
		 $('.toDate').change(function(){
			 if($('.fromDate').val()!=null && $('.fromDate').val()!='' && $('#propertyId').val()!=null && $('#propertyId').val()!='')
				 checkDataTableExsist();
		 });
	
});
function checkDataTableExsist(){
	oTable = $('#PropertyUnitRent').dataTable();
	oTable.fnDestroy();
	$('#PropertyUnitRent').remove();
	$('#gridDiv').html("<table class='display' id='PropertyUnitRent'></table>");
	paymentListJsonCall();
}
function paymentListJsonCall(){
	
	propertyId=Number($('#propertyId').val());
	fromdate=$('#startPicker').val();
	toDate=$('#endPicker').val();
	
	$('#PropertyUnitRent').dataTable({ 
 		"sAjaxSource": "property_unit_rental_json.action?buildingId="+propertyId+"&fromDate="+fromdate+"&toDate="+toDate,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Property ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.property.unit"/>'},
 			{ "sTitle": '<fmt:message key="re.offer.tenant" />'},
 			{ "sTitle": '<fmt:message key="re.offer.period" />'},
 			{ "sTitle": '<fmt:message key="sys.common.amount" />'},
 			
 		],
 		"sScrollY": $("#main-content").height() - 300,
		//"bPaginate": false,
 		"aaSorting": [[1, 'asc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}	 
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#PropertyUnitRent').dataTable();
}

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}  
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.propertyrentallist"/></div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				
			 	<div class="width40 float-left" id="hrm" style="padding:5px;">  
				 	<div class="width100 float-left">
				 	<label class="width20" style="margin-top: 5px;"><fmt:message key="re.property.property"/></label>
				 	<input type="hidden" id="propertyId"  />
				 	<select id="propertyName" class="propertyName" name="propertyName">
						<option value="">Select</option>
						<c:forEach items="${PROPERTY_LIST}" var="bean">
							<option value="${bean.buildingId}">${bean.buildingName}</option>
						</c:forEach>
					</select>
				 	</div> 
				 </div>
				 <div class="width45 float-left" id="hrm" style="padding:5px;">  
					 <div class="width50 float-left">
					 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDatefrom" /></label>
					 	<input type="text" name="fromDate" class="width60 fromDate" id="startPicker" readonly="readonly">
					 </div>
					 <div class="width50 float-left">
					 	<label class="width30" style="margin-top: 5px;"><fmt:message key="re.offer.offerDateTo" /></label>
					 	<input type="text" name="toDate" class="width60 toDate" id="endPicker" readonly="readonly">
					 </div>
			 	</div>
				 <div id="rightclickarea">
					 <div id="gridDiv">
						<table class="display" id="PropertyUnitRent"></table>
					 </div>
				 </div>	
				  <div class="vmenu">
		 	       	<div class="first_li"><span class="print-call">Print</span></div>
					<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
					<div class="first_li"><span class="pdf-download-call">Download as PDF</span></div>
				 </div>		
			</div>
		</div>
	</div>
	<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 
</body>