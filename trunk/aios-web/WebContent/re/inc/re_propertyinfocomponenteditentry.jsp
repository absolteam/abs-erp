<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="8" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoComponentAddEntry" id="propertyInfoComponentAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.validitydetails"/></legend>
		<div>
          	<label><fmt:message key="re.property.info.fromdate"/><span style="color:red">*</span></label>
          	<input type="text" name="fromDate" class="width30 validate[required]" id="startPick"  readonly="readonly">
         </div>		
		<div>
			<label><fmt:message key="re.property.info.todate"/></label>
			<input type="text" name="toDate" class="width30" id="endPick" readonly="readonly">
		</div>			
		<div>
			<label><fmt:message key="re.property.info.totalarea"/><span style="color:red">*</span></label>
			<input type="text" name="totalArea" id="totalArea" class="width30 validate[required]">
			<select id="totalAreaMeasurementCode" class="width20" >
				<option value="">Select</option>
				<c:if test="${requestScope.beanMeasurement ne null}">
				<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementLists" varStatus="status">
					<option value="${beanMeasurementLists.measurementCode }">${beanMeasurementLists.measurementValue }</option>
				</c:forEach>
				</c:if>
			</select>
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.componentdetails"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.lineno"/><span style="color:red">*</span></label>
				<input type="text" name="lineNumber" id="lineNumber" class="width50" disabled="disabled"/>
			</div>
			<div>
				<label><fmt:message key="re.property.info.componenttype"/><span style="color:red">*</span></label>
				<select id="componentType" class="width30 validate[required]" >
					<option value="">-Select-</option>
					<c:if test="${requestScope.lookupList ne null}">
						<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
							<option value="${compType.componentType }">${compType.componentTypeName }</option>
						</c:forEach>
					</c:if>
				</select>
			</div>
			<div>
				<label><fmt:message key="re.property.info.noofcomponent"/><span style="color:red">*</span></label>
				<input name="noOfComponent" id="noOfComponent" class="width50 validate[required,custom[onlyNumber]]">
			</div>
			<div>
				<label><fmt:message key="re.property.info.noofflatincomponent"/><span style="color:red">*</span></label>
				<input type="text" name="noOfFlatInComponent" id="noOfFlatInComponent" class="width50 validate[required,custom[onlyNumber]]" />
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  	
 </tr>
  <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#legalInfoFeatureAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		 $('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(8)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(8)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(8)).children().get(3)).hide();	//Processing button 
	        openFlag=0;	
		 });
		 $('#add').click(function(){
			  if($('#propertyInfoComponentAddEntry').validationEngine({returnIsValid:true})){
				  
					var tempObject=$(this);					
					var lineNumber=$('#lineNumber').val();
					var componentType=$('#componentType').val();
					var componentTypeName=$('#componentTypeName').val();
					var noOfComponent=$('#noOfComponent').val();
					var noOfFlatInComponent=$('#noOfFlatInComponent').val();
					var startPick=$('#startPick').val();
					var endPick=$('#endPick').val();
					var totalArea=$('#totalArea').val();
					measurementCode = $('#totalAreaMeasurementCode').val();
					measurementValue = $('#totalAreaMeasurementCode option:selected').text();
					totalAreaMeasure=""+totalArea+" "+measurementValue;

					var trnVal =$('#transURN').val(); 				 
					var buildingId=$('#buildingId').val();
					var actualLineId=$($($(slidetab).children().get(7)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(7)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(7)).children().get(2)).val(); 
					
					fromDateHead=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),usrtoFormat));
					toDateHead=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),usrtoFormat));
					fromDateChild=Number(formatDate(new Date(getDateFromFormat($("#startPick").val(),fromFormat)),toFormat));
					toDateChild=Number(formatDate(new Date(getDateFromFormat($("#endPick").val(),fromFormat)),toFormat));
					if(toDateHead == 19700101){toDateHead='99991231';}
					if(toDateChild == 19700101){toDateChild='99991231';}
					
					//alert(" fromDateHead : "+fromDateHead+" toDateHead : "+toDateHead+" fromDateChild : "+fromDateChild+" toDateChild : "+toDateChild);
					
					if((fromDateHead < fromDateChild || fromDateHead == fromDateChild) && (toDateHead > toDateChild || toDateHead == toDateChild)){
						$('#loading').fadeIn();
						var flag=false;
			        	$.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/edit_edit_property_component_update.action", 
						 	async: false,
						 	data: {fromDate: startPick,toDate: endPick,totalArea: totalArea,lineNumber: lineNumber, measurementCode: measurementCode,
								componentType: componentType,noOfComponent: noOfComponent,noOfFlatInComponent: noOfFlatInComponent,
								trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId,buildingId:buildingId},
						    dataType: "html",
						    cache: false,
							success:function(result){ 
						 		$('.formError').hide();
		                           $('#othererror').hide(); 
								$('.tempresult').html(result); 
							     if(result!=null){
							    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                                    flag = true;
							    	else{	
								    	flag = false;
								    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
							    	} 
							     }
							     $('#loading').fadeOut();
						 	},  
						 	error:function(result){
							 	alert("err "+result);
						 		 $('.tempresult').html(result);
		                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
		                            $('#loading').fadeOut();
		                            return false;
						 	} 
			        	});
			        	if(flag==true){
			        		$($(slidetab).children().get(0)).text(lineNumber);
			        		$($(slidetab).children().get(1)).text(componentTypeName);
			        		$($(slidetab).children().get(2)).text(noOfComponent); 
			        		$($(slidetab).children().get(3)).text(noOfFlatInComponent); 
			        		$($(slidetab).children().get(4)).text(startPick);
			        		$($(slidetab).children().get(5)).text(endPick); 
			        		$($(slidetab).children().get(6)).text(totalAreaMeasure);
			        		
	
			        		$($(slidetab).children().get(9)).text(componentType);
			        		$($(slidetab).children().get(10)).text(measurementCode);
			        		$($(slidetab).children().get(11)).text(totalArea);
			        		
			        		$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
						  	$($($(slidetab).children().get(8)).children().get(1)).show();	//Edit Button
							$($($(slidetab).children().get(8)).children().get(2)).show(); 	//Delete Button
							$($($(slidetab).children().get(8)).children().get(3)).hide();	//Processing Button
	
							openFlag=0;
		
							$($($(slidetab).children().get(7)).children().get(0)).val($('#objLineVal').html());
					  		$($($(slidetab).children().get(7)).children().get(2)).val($('#objTempId').html());
					  		$("#transURN").val($('#objTrnVal').html());
					   } 
			  	}else{
					$("#othererror").hide().html("From Date & To Date Should not Excede the Header From Date & To Date").slideDown(); 
					return false;
				}
			 }else{
				  return false;
			 }
		});

		 if (!$.browser.msie) {
				$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
			}
			 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
			 $('#selectedMonth,#linkedMonth').change();
			 $('#l10nLanguage,#rtlLanguage').change();
			 if ($.browser.msie) {
			        $('#themeRollerSelect option:not(:selected)').remove();
			 }
		 	$('#startPick,#endPick').datepick({
		 		onSelect: customRangeSecond, showTrigger: '#calImg'
			});

			
	});

	//Prevent selection of invalid dates through the select controls
	 function checkLinkedDays() {
	     var daysInMonth =$.datepick.daysInMonth(
	     $('#selectedYear').val(), $('#selectedMonth').val());
	     $('#selectedDay option:gt(27)').attr('disabled', false);
	     $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	     if ($('#selectedDay').val() > daysInMonth) {
	         $('#selectedDay').val(daysInMonth);
	     }
	 } 
	 function customRangeSecond(dates) {
			if (this.id == 'startPick') {
			$('#endPick').datepick('option', 'minDate', dates[0] || null);
			}
			else {
				$('#startPick').datepick('option', 'maxDate', dates[0] || null);
			}
		}


 </script>	