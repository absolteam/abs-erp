<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 
 <script type="text/javascript">
 var startpick;
 var slidetab;
 var actualID;
 var exratetype;
 var msg="";
 var currencyCode="";
 var actionName=""; var codeId="";
 $(function(){
	 $("#invoiceGenerationAdd").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	$('.showClass').hide();

	var ccode="";  
	var myArray = new Array(); 
	var codeSpilt="";
	var codeSpiltedVal=""
	var exrate="";
	var fnamt=""; 
 	$("#currencyCode option").each(function(){ 
		ccode = $(this).val().trim(); 
		codeSpiltedVal=$(this).val().trim(); 
		myArray=ccode.split("@@");
		codeSpilt=myArray[0];
		codeSpiltedVal=myArray[1];	  
		ccode=codeSpilt.toUpperCase();
		ccode=ccode.toString();  
		if(ccode=="AED"){  
			$("#currencyCode option[value='AED@@"+codeSpiltedVal+"']").attr("selected", "selected"); 
			$("#rateType option[value='T']").attr("selected", "selected");  
			$('#rateType').attr('disabled',true);
			exrate=Number(1).toFixed(codeSpiltedVal); 
			$('#exchangeRate').val(exrate);
			$('#curPrecision').val(codeSpiltedVal);
		}  
	});

 	//select CurCode and CurPrec
 	var selectCurCode=$('#currencyCode :selected').val();
 	myArray=selectCurCode.split("@@");
 	selectCurCode=myArray[0];
 	$('#selectCurCode').val(selectCurCode);
 	var selectCurPrec=myArray[1];
 	$('#selectCurPrec').val(selectCurPrec);
 	 
 	$('#currencyCode').change(function() { 
        $("#currencyCode option:selected").each(function () {
       	 currencyCode = $(this).text();
       	
           });
      })
     .trigger('change');

	 $('#currencyCode').change(function(){ 
	 		//select CurCode and CurPrec
 	 	selectCurCode=$('#currencyCode :selected').val();
 	 	myArray=selectCurCode.split("@@");
 	 	selectCurCode=myArray[0];
 	 	$('#selectCurCode').val(selectCurCode);
 	 	selectCurPrec=myArray[1];
 	 	$('#selectCurPrec').val(selectCurPrec); 

		var currencyCode=$('#currencyCode :selected').val();
		var res=$('#fnCurrencyCode').val()+"@@"+$('#fnCurPrec').val();

		if(currencyCode == res){
			$('#rateType option[value="T"]').attr("selected","selected");
			var thisPrec=Number($('#fnCurPrec').val());
			var exRate=Number(1);
			var exRateVal=exRate.toFixed(thisPrec);
			$('#exchangeRate').val(exRateVal);
			$('#rateType').attr("disabled",true);
			$('#exchangeRate').attr("disabled",true);
		}else{
			currencyCode=$('#selectCurCode').val();
			$.ajax({
			      type:"GET",
			      url:"<%=request.getContextPath()%>/exchange_rate_retreive_action.action",
			      data:{currencyCode:currencyCode},
			      async: false,
				  dataType: "html",
				  cache: false,
				  success:function(result){  
					  //alert(result);
			    	  $('.tempresult').html(result); 
				    	  if($('#objExRate').html() != 0 && $('#objRvRate').html() != 0){
			    		  $('#rateType option[value="T"]').attr("selected","selected");
				    	  $('#rateType').attr("disabled",false);
				    	  $('#exchangeRate').attr("disabled",true);
 				    	  $('#exchangeRate').val(Number($('#objExRate').html()).toFixed(Number($('#objPercision').html())));
 				    	  $('#tableExRate').val(Number($('#objExRate').html()).toFixed(Number($('#objPercision').html())));
  			    	  }else{
  			    		  $('#rateType option[value="U"]').attr("selected","selected");
  			    		  $('#rateType').attr("disabled",true);
   			    		  $('#exchangeRate').val("");
  			    		  $('#exchangeRate').attr("disabled",false);
  			    		  $('#tableExRate').val("");
  			    		  $('#exchangeRate').focus();
			    	  }
			      },
			      error:function(result){
			    	  $('.tempresult').html(result); 
			      }
			});
  		}
	});

	$('#rateType').change(function(){
		var exratetype=$('#rateType :selected').val();
		if(exratetype == 'T'){
   			 $('#exchangeRate').val($('#tableExRate').val());
  			 $('#exchangeRate').attr("disabled",true);
		}
  		else{
  			$('#exchangeRate').val("");
  			$('#exchangeRate').focus();
			$('#exchangeRate').attr("disabled",false);
  		}
	});

	$('#chequeAmount').blur(function(){
		var chqamt=Number($('#chequeAmount').val());
		var exRate=$('#exchangeRate').val();
		var curPrec=$('#selectCurPrec').val();
		chqamt=chqamt.toFixed(curPrec);
		var chqFnamt=(chqamt*exRate).toFixed(curPrec); 
		$('#chequeAmount').val(chqamt); 
		$('#functionalCurrencyValue').val(chqFnamt);
	});
		
	$('#exchangeRate').blur(function(){
		var exRate=Number($('#exchangeRate').val());
		var tempPrec=Number($('#selectCurPrec').val());
		var finalVal=(exRate).toFixed(tempPrec);
		$('#exchangeRate').val(finalVal);

		var chqamt=Number($('#amount').val());
		var fnCurrVal=(exRate*chqamt).toFixed(tempPrec);
		$('#functionalAmount').val(fnCurrVal);
	});
				
	$('#amount').blur(function(){ 
		var lmt=$('#amount').val();
		var loanamt="";
		ccode=$('#currencyCode :selected').text(); 
		codeSpiltedVal=$('#currencyCode :selected').val();  
		myArray=codeSpiltedVal.split("@@");   
		codeSpiltedVal=myArray[1];  
		if(lmt!=null && lmt!='' && ccode=="AED"){ 
			exrate=Number(1); 
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			exrate=Number(exrate).toFixed(codeSpiltedVal);  
			
			fnamt=(loanamt*exrate).toFixed(codeSpiltedVal); 
			$('#exchangeRateVal').val(exrate);
			$('#amount').val(loanamt);  
			$('#functionalAmount').val(fnamt);
		}
		else{
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			$('#exratetype').attr('disabled',false);
			exrate=$('#exchangeRateVal').val(); 
		}
	});
		
	$('.calc').change(function(){
		if($('#chequeAmount').val() != null && $('#chequeAmount').val() != "" && $('#exchangeRate').val() != null && $('#exchangeRate').val() != "" ){
			var unit=parseFloat($('#exchangeRate').val());
			var item=parseFloat($('#chequeAmount').val());
			var total=unit * item;
			$('#functionalCurrencyValue').val(total);
		}else{
			$('#functionalCurrencyValue').val("");
		}
	});
	$.fn.globeTotal = function() { 
		var total=0;
		var curPre=$('#curPrecision').val();
		$('.amountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#amountTotal').val(total)

		total=0;
		$('.functionalAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#functionalAmountTotal').val(total); 
 
	 }

	 //OnChange Mode Of Payment
	 $('#modeOfPaymentCode').change(function(){
		 modeOfPaymentCode=$('#modeOfPaymentCode').val();
		 if(modeOfPaymentCode == '1'){
			 $('.bankDetailsDiv').fadeOut();
			 $('.cashDetailsDiv').fadeIn();
		 }else if(modeOfPaymentCode == '2'){
			 $('.bankDetailsDiv').fadeIn();
			 $('.cashDetailsDiv').fadeOut();
		 }else{
			 $('.bankDetailsDiv').fadeIn();
			 $('.cashDetailsDiv').fadeIn();
		 }
	 });
	  
	$(".save").click(function(){
		$('.tempresultfinal').fadeOut();
		if($('#invoiceGenerationAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				//invoiceNumber = $('#invoiceNumber').val();
				invoiceDate=$('#glDate').val();
				currencyCode=$('#currencyCode').val();
				exchangeRate=$('#exchangeRate').val();
				status=$('#status').val();
				rateType=$('#rateType').val();
				transactionType=$('#transactionType').val();
				
				contractId=$('#contractId').val();
				rentScheduleNumber=$('#rentScheduleNumber').val();
				scheduleLineNumber=$('#scheduleLineNumber').val();
				functionalCurrencyValue=$('#functionalCurrencyValue').val();
				dueDate=$('#dueDate').val();
				amount=Number($('#amount').val());
				functionalAmount=Number($('#functionalAmount').val());

				description=$('#description').val();

				//Ledger & Company Details
				var headerLedgerId = $('#headerLedgerId').val();
				var companyId=Number($('#headerCompanyId').val());
				var applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Not Yet Approved";

				var transURN = $('#transURN').val();
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/re_invoice_generation_save.action",
						data: { invoiceDate: invoiceDate, 
							currencyCode: currencyCode,exchangeRate: exchangeRate,status: status,rateType: rateType,transactionType: transactionType,
							contractId: contractId,rentScheduleNumber: rentScheduleNumber,scheduleLineNumber: scheduleLineNumber,dueDate: dueDate,
							amount: amount,functionalAmount: functionalAmount, description: description,
							companyId: companyId,workflowStatus: workflowStatus,headerLedgerId: headerLedgerId,applicationId: applicationId,
							trnValue: transURN },  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$("#main-wrapper").html(result); 
								} else{
									$('.tempresultfinal').fadeIn();
								}
							},
						error: function(result) {
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
						}
						 
					});
					 $.scrollTo(0,300);
					 $('#loading').fadeOut();
				}else{
					$('.childCountErr').show();
					$('.childCountErr').html("Internal Error, Please contact Administrator");
					return false; 
				}
			}else{
				$('.childCountErr').show();
				$('.childCountErr').html("Please insert data of Invoice Child");
				return false; 
			}
		 }else{
			  return false;
		  }
	});
	
	$('.addData').click(function(){
		if($('#invoiceGenerationAdd').validationEngine({returnIsValid:true})){
		 	if($('#openFlag').val() == 0) { 
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        availFlag=$('#availFlag').val();
		        if(availFlag==1){
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/re_invoice_generation_add_add.action", 
				 		async: false,
				    	dataType: "html",
				   		cache: false,
						success:function(result){
							 $(result).insertAfter(slidetab);
							 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
					         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
					         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button

					         var temp1= $($(slidetab).children().get(0)).text();

					         $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					         
					         $('#loading').fadeOut();
					         $('#openFlag').val(1);
						}
					});
		        }
		        else{
		        	$('#loading').fadeOut();  
		        	$('#temperror').hide().html("Invoice date doesnot belongs to this period.").slideDown(1000);
		        	$.scrollTo(0,300);	//Scroll to top 
					return false; 
		        }
		 	}	
		}else{
			return false;
		}
	});
	$(".editData").click(function(){
		if($('#invoiceGenerationAdd').validationEngine({returnIsValid:true})){
		 	if($('#openFlag').val() == 0) {
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/re_invoice_generation_add_edit.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
					 var temp5= $($(slidetab).children().get(4)).text();
					 
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
		
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
		
				  	$('#loading').fadeOut();
				  	$('#openFlag').val(1);
					}
				});
		 	}
		}else{
			return false;
		}
	});

   
 	$('.delrow').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
 		var ledgerId=$('#headerLedgerId').val();				
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/re_invoice_generation_add_delete.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID, ledgerId:ledgerId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$('#temperror').show();
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
					childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
		 	$('#warningMsg').show();
			$('#warningMsg').html("Please insert record to delete");
			return false; 
		}
 	});	

 	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		//slidetab=$(this).parent().parent().get(0); 
 		var trnValue=$("#transURN").val();  
 		//actualID=$($($(slidetab).children().get(3)).children().get(1)).val(); 
 		//alert("Cancel Entry - trnValue : "+trnValue+"  actualID :"+actualID);
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/re_invoice_generation_add_discard.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();
		 		$("#main-wrapper").html(result);
				$("#transURN").val(""); 
				$('#loading').fadeOut(); 
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();
				$('#loading').fadeOut();
			} 
    	});
 	});

 	$('#cancel').click(function(){
   	 	$('.formError').remove();
   		$('#loading').fadeIn();
		 $.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/re_receipt_generation_list.action",   
		     	async: false, 
				dataType: "html",
				cache: false,
				error: function(data) 
				{  
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
					$.scrollTo(0,300);
					$('#loading').fadeOut();
		     	}
			});
		return true;
 	});

 	$('.addrows').click(function(){
		var count=Number(0);
		$('.invoicelines').each(function(){
			count=count+1;
		});
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/re_invoice_generation_add_add_rows.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});
		  
	$('.startPicker').datepick({
		onSelect: customRange, showTrigger: '#calImg'});

	// System Date Greater than
	$('#dueDate').datepick();

	//Extra
    var res=$('#fnCurrencyCode').val()+"@@"+$('#fnCurPrec').val();
 	$('#currencyCode').val(res);
	currencyCode=$('#currencyCode :selected').val();
	if(currencyCode == res){
 		$('#exratetype').attr("disabled",true);
 		$('#exchangeRate').attr("disabled",true);
	}else{
		$('#exratetype').attr("disabled",false);
 		$('#exchangeRate').attr("disabled",false);
	}
});
function customRange(dates) { 
	$('.glDateformError').remove();
	 var glDate ="";
	 glDate =$('#glDate').val().trim();
	// var calPeriodId=$('#calPeriodId').val();
	 var ledgerId=$('#headerLedgerId').val();
	 var categoryId=$('#categoryId').val();
	 if(glDate!="" && glDate!=null){
		 $('#status').hide();
		 $('#availErr').hide(); 
		 $('#temperror').hide(); 
		 $('#checking').show();  
		 $("#checking").html('<img align="absmiddle" src="../images/loader.gif" /> Checking availability...');
		
		 $.ajax({
			 type: "POST",
			 url: "<%=request.getContextPath()%>/gl_journal_dateavail.action", 
			 data: {glDate:glDate, ledgerId:ledgerId, categoryId:categoryId},
			  dataType: "html",
			 success: function(result){  
				 $('.tempresult').html(result); 
				msg=$('#resultMessage').html().trim().toUpperCase(); 
				 if(msg == 'OK'){  
					 $('#status').show(); 
					 $('#glCategoryId').val($('#objGlCategoryId').html());
			 		 $('#glCategoryName').val($('#objGlCategoryName').html());
			 		 $('#glLedgerId').val($('#objGlLedgerId').html());
			 		 $('#glPeriodId').val($('#objGlPeriodId').html());
					 $('#status').html(' <img align="absmiddle" src="../images/accepted.png" /> ');
					 $('#status').attr("title", "Validation Success"); 
					 $('#availFlag').val("1");
				 } 
				 else{ 
					 $('#status').show();  
					 var css = {};  
					 css['display'] = 'inline';
					 css['position'] = 'absolute';
					 css['margin-top'] ='5px';
					 css['cursor']='pointer';
					 $('#status').css(css); 
					 $('#status').html(' <img align="absmiddle" src="../images/cancel.png" /> ');
					 $('#status').attr("title", "GL Details missing, please contact administrator.");  
					 $('#availFlag').val("0");
				 }
			} 	
		}); 
		 $('#checking').hide();
	}else{  
		var el=$(this).attr('class');
	
	}
} 
</script> 
<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Receipt Generation</div>
         <div style="display:none;" class="tempresult"></div>
         <div id="ajaxresult" style="display:none;"></div>    
        <div class="portlet-content">
        	<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	
			<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
			<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>
			<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>
        	<form name="invoiceGenerationAdd" id="invoiceGenerationAdd">
        		<div class="width100 float-left" id="hrm" style="margin-bottom:10px;">
					<fieldset>
						<legend>Invoice Details</legend>
						<div class="width30 float-left">
							<label class="width30">Transaction Number</label>
							<input type="text" value=""class="width50 disabledClass" name="invoiceNumber" id="invoiceNumber" disabled="disabled"  >
						</div>
						<div class="width30 float-left">
							<label class="width30">Transaction Date<span style="color: red;">*</span></label>
							<input name="glDate" type="text" readonly="readonly" id="glDate"  class="startPicker validate[required] width50">
							 <div id="checking"></div>
							<div id="status" class="width5 float-left tooltip" style="position:absolute;display: inline;"></div> 
						</div>
						<div class="width30 float-left">
							<label class="width30">Transaction Type<span class="mandatory">*</span></label>
							<select style="width: 52%;" class="disabledClass validate[required]" name="transactionType" id="transactionType">
								<option value="">--Select--</option>
								<option value="1">Rent Receivable</option>
								<option value="2">Fee Receivable</option>
							</select>
						</div>
						<div class="clearfix"></div><br/>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.currency"/><span class="mandatory">*</span></label>
							<select  style="width: 52%;" class="disabledClass validate[required]" name="currencyCode" id="currencyCode">
								<option value="">--Select--</option>
								<c:choose>
							 		<c:when test="${selectCurrency != null }">
							 			<c:forEach items="${selectCurrency}" var="currencyBean" varStatus="status" >
											<option value="${currencyBean.currencyCode}@@${currencyBean.curPrecsion}">${currencyBean.currencyCode}</option>
										</c:forEach> 
							 		</c:when>
							 	</c:choose>  
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.ratetype"/><span class="mandatory">*</span></label>
							<select style="width: 52%;" class="disabledClass validate[required]" name="rateType" id="rateType">
								<option value="">--Select--</option>
								<option value="U">User</option>
								<option value="T">Table</option>
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.exchangerate"/><span class="mandatory">*</span></label>
							<input type="text" class="width50 calc disabledClass validate[required,custom[onlyFloat]]" name="exchangeRate" disabled="disabled"  id="exchangeRate">
						</div>
						<div class="clearfix"></div><br/>
						<div class="width30 float-left">
							<label class="width30">Amount<span class="mandatory">*</span></label>
							<input type="text" value="" class="width50 disabledClass validate[required,custom[onlyFloat]]" name="amount" id="amount">
						</div>
						<div class="width30 float-left">
							<label class="width30">Functional Amount<span class="mandatory">*</span></label>
							<input type="text" value=""class="width50 disabledClass" name="functionalAmount" id="functionalAmount" disabled="disabled"  >
						</div>
						<div class="width30 float-left">
							<label class="width30">Status<span class="mandatory">*</span></label>
							<input type="text" value=""class="width50 disabledClass" name="functionalAmount" id="functionalAmount" disabled="disabled"  >
						</div>
						<div class="clearfix"></div><br/>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.modeofpayment"/><span class="mandatory">*</span></label>
							<select style="width: 52%;" class="disabledClass validate[required]" id="modeOfPaymentCode" name="modeOfPaymentCode">
								<option value="">--Select--</option>
								<c:choose>
							 		<c:when test="${modeOfPaymentList != null}">
										<c:forEach items="${modeOfPaymentList}" var="transBean" varStatus="status" >
											<option value="${transBean.modeOfPaymentCode }">${transBean.modeOfPayment}</option>
										</c:forEach>
									</c:when>
							 	</c:choose>  
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30">Customer Name</label>
							<input type="text" value=""class="width50 disabledClass" name="scheduleLineNumber" id="scheduleLineNumber" >
						</div>
						<div class="width30 float-left">
							<label class="width30">Customer Number</label>
							<input type="text" value="" class="width50 disabledClass" name="buildingName" id="buildingName" disabled="disabled"  >
							<input type="hidden" value="" class="width50 disabledClass" name="buildingId" id="buildingId" disabled="disabled"  >
						</div>
						<div class="clearfix"></div>
						
					</fieldset>
				</div>
				<div class="clearfix"></div>
				<div class="width100 bankDetailsDiv" id="hrm" style="margin-bottom:10px;">
					<fieldset>
						<legend>Bank Details</legend>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.bankname"/><span class="mandatory">*</span></label>
							<select style="width:52%;" class="disabledClass validate[required]" id="bankId" name="bankId">
								<option value="">--Select--</option>
								<c:choose>
							 		<c:when test="${beanBank != null}">
										<c:forEach items="${beanBank}" var="bean" varStatus="status" >
											<option value="${bean.bankId }">${bean.bankName} </option>
										</c:forEach>
									</c:when>
							 	</c:choose>  
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30">Branch Name</label>
							<select style="width:52%;" class="disabledClass" name="documentName" id="documentName">
								<option value="">--Select--</option>
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.bankaccountno"/><span class="mandatory">*</span></label>
							<select style="width:52%;" class="disabledClass validate[required]" name="bankAccountDocId" id="bankAccountDocId">
								<option value="">--Select--</option>
							</select>
						</div>
						<div class="clearfix"></div><br/>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.chequenumber"/></label>
							<input type="text" value="" style="width:52%;" class="width50 disabledClass" name="chequeNumber" id="chequeNumber">
						</div>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.chequeamount"/><span class="mandatory">*</span></label>
							<input type="text" class="width50 calc disabledClass validate[required,custom[onlyFloat]]" name="chequeAmount" id="chequeAmount">
						</div>
						<div class="width30 float-left">
								<label class="width30"><fmt:message key="accounts.bankacinfo.label.checkdate"/><span id="tempMandatory" class="mandatory">*</span></label>
								<input type="text" value="" class="width50 disabledClass validate[required] startPicker" name="checkDate" id="checkDate" readonly="readonly" >
								<div id="status2" style="display:none;" title="Check Date does not belong to this period." class="tooltip"></div>
							</div>
					</fieldset>
				</div>
				<div class="clearfix"></div> 
				<div class="width100 cashDetailsDiv" id="hrm" style="margin-bottom:10px;">
					<fieldset>
						<legend>Cash Details</legend>
						<div class="width30 float-left">
							<label class="width30">Cash Transaction Type</label>
							<select style="width:52%;" class="disabledClass" name="documentName" id="documentName">
								<option value="">--Select--</option>
							</select>
						</div>
						<div class="width30 float-left">
							<label class="width30">Cash Trans Name</label>
							<input type="text" value="" class="width50 disabledClass" name="chequeNumber" id="chequeNumber">
						</div>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.category.label.ledgername"/><span style="color: red;">*</span></label>  
							<select name="ledgerId" style="width:52%;"  class="validate[required]" id="ledgerName">
								<option value="">--Select--</option>
									<c:forEach items="${ledgerVal}" var="bean" varStatus="index">
										<option value="${bean.ledgerId}">${bean.ledgerName}</option>
									</c:forEach>
				 			</select>
						</div>
						<div class="clearfix"></div>
						<div class="width30 float-left">
							<label class="width30">Cash Account</label>
							<input type="text" value="" class="width50 disabledClass" name="chequeNumber" id="chequeNumber">
						</div>
						<div class="width30 float-left">
							<label class="width30">Amount</label>
							<input type="text" value="" class="width50 disabledClass" name="chequeNumber" id="chequeNumber">
						</div>
						<div class="width30 float-left">
							<label class="width30">Functional Amount</label>
							<input type="text" value="" class="width50 disabledClass" name="chequeNumber" id="chequeNumber">
						</div>
					</fieldset>
				</div>
				<div class="clearfix"></div> 
				<div style="margin-top:10px;" class="width100" id="hrm">
					<fieldset>
						<legend><fmt:message key="accounts.journal.label.desc"/><span style="color: red;">*</span></legend>
						<input type="text" name="description" id="description" class="width90 validate[required]" style="margin:5px;" />
					</fieldset>
			 	</div>
				<div class="clearfix"></div>
				<div class="portlet-content"> 
					<div class="childContent">
					<input type="hidden" name="childCount" id="childCount"  value="0"/>
					<input type="hidden" name="trnValue" value="" id="transURN" readonly="readonly"/>
					 <input type="hidden" name="openFlag" id="openFlag"  value="0"/> 
					<div id="hrm" style="margin-bottom:10px;">
						<div class="hastable">
							<table class="hastab"> 
								<thead>										
								<tr>
									<th class="width10">Line Number</th> 
									<th class="width20">Revenue Code</th>
									<th class="width20">Amount</th>
									<th class="width20">Functional Amount</th>
									<th class="width20">Note</th>
									<th  style="width:5%;"><fmt:message key="accounts.banktransaction.label.options"/></th>
								</tr> 
								</thead> 
							 
								<tbody class="tab">
								<%for(int i=1;i<4;i++) { %>
									<tr class="invoicelines"> 
										<td class="width10"><%=i%></td>		 
										<td class="width20"></td>	 
										<td class="width20 amountCalc"></td>   
										<td class="width20 functionalAmountCalc"></td>	 
										<td class="width20 "></td> 
										<td style="display:none;">
											<input type="hidden" value="" name="actualLineId" class="actualLineId"/>
										 </td> 
										<td style="width:5%;">
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" title="Add this row" >
											 	 <span class="ui-icon ui-icon-plus"></span>
											</a>
											<a style="display:none;cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" title="Edit this row" >
												<span class="ui-icon ui-icon-wrench"></span>
											</a>
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" title="Delete this Row" >
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td>
										<td style="display:none;"></td>
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
									</tr>
								<%}%>
								</tbody>
							</table>
						</div>	
					</div>					
				
					<div class="clearfix"></div>
					<div class="float-right " style="width:72%">
						<label><fmt:message key="re.contract.total"/></label>
					 	<input name="amountTotal" id="amountTotal"  style="width:29%;" disabled="disabled" > 
					 	<input name="functionalAmountTotal" id="functionalAmountTotal"  style="width:29%;" disabled="disabled" >
					</div>
					
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
							<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.banktransaction.button.addrow"/></div> 
					</div> 
				</div>
				</div>
				
				<div class="clearfix"></div>
	       		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
					<div class="portlet-header ui-widget-header float-right" id="cancel"><fmt:message key="accounts.banktransaction.button.discard"/></div>
				 	<div class="portlet-header ui-widget-header float-right save" id="save"><fmt:message key="accounts.banktransaction.button.save"/></div> 
				</div>
				
				
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="curPrecision"/>
<input type="hidden" id="personId" value="${requestScope.personId}"/>
<input type="hidden" id="approverId" value=""/>
<input type="hidden" id="workflowStatus" value="approvesave"/>	
<input type="hidden" id="workflowFnType" value="approvesave"/>	
<input type="hidden" id="codeCombinationHidden" value=""/>
<input type="hidden" id="coaHeaderIdHidden" value=""/>
<input type="hidden" name="fnCurrencyCode" id="fnCurrencyCode" value="${fnCurrency.currencyCode}"/> 
<input type="hidden" name="fnCurPrec" id="fnCurPrec" value="${fnCurrency.precision}"/>
<input type="hidden" id="tableExRate" /> 
<input type="hidden" id="selectCurCode" />
<input type="hidden" id="selectCurPrec" />

<input type="hidden" id="availFlag" value="0"/>

	