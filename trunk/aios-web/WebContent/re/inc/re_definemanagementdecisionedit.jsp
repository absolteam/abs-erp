<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';


$(document).ready(function (){  
	$('#buildingId').val($('#buildingIdTemp').val());
	$('#flatId').val($('#flatIdTemp').val());
	$('#calculatedAsDeposit').val($('#calculatedAsDepositTemp').val());
	$('#calculatedAsRental').val($('#calculatedAsRentalTemp').val());
	$('#calculatedAsOther').val($('#calculatedAsOtherTemp').val());

	var depositAmount;
	if($('#depositAmount').is(':checked')){$('#depositAmountDiv').fadeIn();}else{$('#depositAmountDiv').fadeOut();}
	var rentalAmount;
	if($('#rentalAmount').is(':checked')){$('#rentalAmountDiv').fadeIn();}else{$('#rentalAmountDiv').fadeOut();}
	var otherAmount;
	if($('#otherAmount').is(':checked')){$('#otherAmountDiv').fadeIn();}else{$('#otherAmountDiv').fadeOut();}
	
	
	$("#defineManagementAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	$(".cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_management_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 

	$("#close").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_management_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 

	$('#startPicker').blur(function(){
		$('.error').hide();
	});

	$('#percentage').blur(function(){
		var percentage=$('#percentage').val();
		if(percentage>100){
			$('#percentage').val("");		
			$('.availErr').show();
			$('.availErr').html("Percentage Should not greater than 100");
			
		}else{
			$('.availErr').hide();
		}	
	});

	approvedStatus=$('#approvedStatus').val(); 
	if( approvedStatus == 'E'){
		$('#request').remove();	
		$('.cancel').remove();	
		$('.update').remove();
		$('#close').fadeIn();
		$('#buildingId').attr("disabled",true);
		$('#flatId').attr("disabled",true);
		$('#depositAmount').attr("disabled",true);
		$('#depositInc').attr("disabled",true);
		$('#depositIncDec').attr("disabled",true);
		$('#calculatedAsDeposit').attr("disabled",true);
		$('#percentageDeposit').attr("disabled",true);
		$('#rentalAmount').attr("disabled",true);
		$('#rentalInc').attr("disabled",true);
		$('#rentalDec').attr("disabled",true);
		$('#calculatedAsRental').attr("disabled",true);
		$('#percentageRental').attr("disabled",true);
		$('#otherAmount').attr("disabled",true);
		$('#otherInc').attr("disabled",true);
		$('#otherDec').attr("disabled",true);
		$('#calculatedAsOther').attr("disabled",true);
		$('#percentageOther').attr("disabled",true);
		$('#holdingFlat').attr("disabled",true);
		$('#holdingRent').attr("disabled",true);
		$('#holdingRent').attr("disabled",true);
		$('#rent').attr("disabled",true);
		$('#deposit').attr("disabled",true);
		$('#startPicker').attr("disabled",true);
		$('#endPicker').attr("disabled",true);
		$('#remarks').attr("disabled",true);
		$('.commonErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#request').remove();	
		$('.cancel').remove();	
		$('.update').remove();
		$('#close').fadeIn();
		$('#buildingId').attr("disabled",true);
		$('#flatId').attr("disabled",true);
		$('#depositAmount').attr("disabled",true);
		$('#depositInc').attr("disabled",true);
		$('#depositIncDec').attr("disabled",true);
		$('#calculatedAsDeposit').attr("disabled",true);
		$('#percentageDeposit').attr("disabled",true);
		$('#rentalAmount').attr("disabled",true);
		$('#rentalInc').attr("disabled",true);
		$('#rentalDec').attr("disabled",true);
		$('#calculatedAsRental').attr("disabled",true);
		$('#percentageRental').attr("disabled",true);
		$('#otherAmount').attr("disabled",true);
		$('#otherInc').attr("disabled",true);
		$('#otherDec').attr("disabled",true);
		$('#calculatedAsOther').attr("disabled",true);
		$('#percentageOther').attr("disabled",true);
		$('#holdingFlat').attr("disabled",true);
		$('#holdingRent').attr("disabled",true);
		$('#holdingRent').attr("disabled",true);
		$('#rent').attr("disabled",true);
		$('#deposit').attr("disabled",true);
		$('#startPicker').attr("disabled",true);
		$('#endPicker').attr("disabled",true);
		$('#remarks').attr("disabled",true);
		$('.commonErr').hide().html("Workflow is already Approved, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#request').fadeIn();	
		$('.cancel').fadeIn();	
		$('.update').fadeIn();
		$('.commonErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
	}

	$('.update').click(function(){ 
		if($("#defineManagementAdd").validationEngine({returnIsValid:true})){
			var decisionId=$('#decisionId').val();
			var buildingId=$('#buildingId').val();
			depositAmount;
			if($('#depositAmount').is(':checked')){depositAmount=1;}else{depositAmount=0;}
			rentalAmount;
			if($('#rentalAmount').is(':checked')){rentalAmount=1;}else{rentalAmount=0;}
			otherAmount;
			if($('#otherAmount').is(':checked')){otherAmount=1;}else{otherAmount=0;}
			var holdingRent;
			if($('#holdingRent').is(':checked')){holdingRent=1;}else{holdingRent=0;}
			if($('#depositInc').is(':checked')){depositIncDec=1;}else{depositIncDec=0;}
			var rentalIncDec;
			if($('#rentalInc').is(':checked')){rentalIncDec=1;}else{rentalIncDec=0;}
			var otherIncDec;
			if($('#otherInc').is(':checked')){otherIncDec=1;}else{otherIncDec=0;}
			var holdingFlat;
			if($('#holdingFlat').is(':checked')){holdingFlat=1;}else{holdingFlat=0;}
			var holdingFlat;
			if($('#holdingFlat').is(':checked')){holdingFlat=1;}else{holdingFlat=0;}
			var fromDate=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
	        var toDate=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));

	        var percentageDeposit=Number($('#percentageDeposit').val());
			var percentageRental=Number($('#percentageRental').val());
			var percentageOther=Number($('#percentageOther').val());
			var remarks=$('#remarks').val();

			var calculatedAsDeposit=$('#calculatedAsDeposit').val();
			var calculatedAsRental=$('#calculatedAsRental').val();
			var calculatedAsOther=$('#calculatedAsOther').val();
			workflowStatus="Not Yet Approved";
			var companyId=Number($('#headerCompanyId').val());
			var applicationId=Number($('#headerApplicationId').val());
	        
			var flatId=$('#flatId').val();
			if(fromDate<=toDate){
				$('#loading').fadeIn();
				fromDate=$("#startPicker").val();
				toDate=$("#endPicker").val();
				$('#loading').fadeIn();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/define_management_update.action",
					data: {buildingId: buildingId,decisionId:decisionId, depositAmount: depositAmount,rentalAmount: rentalAmount,
						holdingRent: holdingRent,depositIncDec: depositIncDec,rentalIncDec: rentalIncDec,otherIncDec: otherIncDec,
						holdingFlat: holdingFlat,fromDate: fromDate,toDate: toDate,percentageDeposit: percentageDeposit,percentageRental: percentageRental,
						percentageOther: percentageOther,remarks: remarks, otherAmount: otherAmount,companyId:companyId,applicationId:applicationId,
						calculatedAsDeposit: calculatedAsDeposit,calculatedAsRental: calculatedAsRental,calculatedAsOther: calculatedAsOther,workflowStatus:workflowStatus,
						flatId: flatId},  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('.tempresultfinal').fadeOut();
			 		 	$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
						$('#loading').fadeOut(); 
					} ,  
				 	error:function(result){
						$('.tempresultfinal').fadeOut();
						$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
						$('#loading').fadeOut(); 
				 	} 
				});
				$('#loading').fadeOut();
			}else{
				$('.error').show();
				$('.error').html("'To Date' should be greater than 'From Date' !!!");
				return false;
			} 
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});

	$('#request').click(function(){ 
		if($("#defineManagementAdd").validationEngine({returnIsValid:true})){
			var buildingId=$('#buildingId').val();
			var depositAmount;
			if($('#depositAmount').is(':checked')){depositAmount=1;}else{depositAmount=0;}
			var rentalAmount;
			if($('#rentalAmount').is(':checked')){rentalAmount=1;}else{rentalAmount=0;}
			var otherAmount;
			if($('#otherAmount').is(':checked')){otherAmount=1;}else{otherAmount=0;}
			var holdingRent;
			if($('#holdingRent').is(':checked')){holdingRent=1;}else{holdingRent=0;}
			var depositIncDec;
			if($('#depositInc').is(':checked')){depositIncDec=1;}else{depositIncDec=0;}
			var rentalIncDec;
			if($('#rentalInc').is(':checked')){rentalIncDec=1;}else{rentalIncDec=0;}
			var otherIncDec;
			if($('#otherInc').is(':checked')){otherIncDec=1;}else{otherIncDec=0;}
			var holdingFlat;
			if($('#holdingFlat').is(':checked')){holdingFlat=1;}else{holdingFlat=0;}
			var fromDate=$('#startPicker').val();
			var toDate=$('#endPicker').val();
			var flatId=$('#flatId').val();
			var calculatedAsDeposit=$('#calculatedAsDeposit').val();
			var calculatedAsRental=$('#calculatedAsRental').val();
			var calculatedAsOther=$('#calculatedAsOther').val();
			var percentageDeposit=$('#percentageDeposit').val();
			var percentageRental=$('#percentageRental').val();
			var percentageOther=Number($('#percentageOther').val());
			var remarks=$('#remarks').val();
			var decisionId=$('#decisionId').val();
			
			//Work Flow Variables
			var sessionPersonId = $('#sessionPersonId').val();
			var companyId=Number($('#headerCompanyId').val());
			var applicationId=Number($('#headerApplicationId').val());
			var workflowStatus="Request For Verification";
			var wfCategoryId=Number($('#headerCategoryId').val());
			var functionId=Number($('#headerCategoryId').val());
			var functionType="request";
			
			
			$('#loading').fadeIn();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/define_management_edit_request.action",
				data: {buildingId: buildingId, depositAmount: depositAmount,rentalAmount: rentalAmount,holdingRent: holdingRent,
					holdingFlat: holdingFlat,fromDate: fromDate,toDate: toDate,depositIncDec: depositIncDec,rentalIncDec: rentalIncDec,
					otherIncDec: otherIncDec,flatId: flatId,percentageDeposit: percentageDeposit,percentageRental: percentageRental,
					percentageOther: percentageOther,remarks: remarks, otherAmount: otherAmount,
					calculatedAsDeposit: calculatedAsDeposit,calculatedAsRental: calculatedAsRental,calculatedAsOther: calculatedAsOther,
					companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus,wfCategoryId: wfCategoryId,decisionId:decisionId,
					functionId: functionId,functionType: functionType,sessionPersonId: sessionPersonId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				success:function(result){
						$('.formError').hide();
						$('.commonErr').hide();
			 		 	$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$('.tempresultfinal').fadeIn();
							$('#loading').fadeOut(); 
							$('#request').remove();	
							$('.cancel').remove();	
							$('.update').remove();
							$('#close').fadeIn();
							$('.commonErr').hide();
						} else{
							$('.tempresultfinal').fadeIn();
						}
				},  
			 	error:function(result){
					$('.tempresultfinal').html(result);
					if($("#sqlReturnStatus").val()==1){
						$("#main-wrapper").html(result); 
					} else{
						$('.tempresultfinal').fadeIn();
					}
			 	}  
			});
			$('#loading').fadeOut();
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});
	
	$('#buildingId').change(function(){
		var buildingId=$('#buildingId').val();
		$('#loading').fadeIn();
		$.ajax({
    		type:"POST",
    		url:"<%=request.getContextPath()%>/flat_retrive_edit_action.action",
    		data:{buildingId:buildingId},
    		async:false,
    		dataType:"html",
    		cache:false,
    		success:function(result){
    			$("#flatId").html(result);
    			$('.FlatAmountDetails').hide();
    		}
    	});
		$('#loading').fadeOut();
	});

	$('#flatId').change(function(){
		var buildingId=$('#buildingId').val();
		var flatId=Number($('#flatId option:selected').val());
		if(flatId != 0){
			$('#loading').fadeIn();
			$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/flat_check_edit_action.action",
	    		data:{buildingId:buildingId,flatId:flatId},
	    		async:false,
	    		dataType:"html",
	    		cache:false,
	    		success:function(result){
	    			$('.tempresult').html(result); 
	    			msg=$('#resultMessage').html().trim().toUpperCase(); 
	    			if(msg == 'OK'){
	    				$.ajax({
		    				type:"POST",
		    				url:"<%=request.getContextPath()%>/flat_retrieve_amount.action" ,
		    				data:{buildingId:buildingId,flatId:flatId},
		    				async:false,
		    				dataType:"html",
		    				cache:false,
		    				success:function(result){
			    				$('.FlatAmountDetails').html(result);
			    				$('.FlatAmountDetails').show();
		    				}
	    				});
	    			}
	    			else{
	    				$('.availErr').show();
	    				$('.availErr').html("Building and Flat Combination Already Exists!!!");
	    				$('#flatId').val("");
	    				$('.FlatAmountDetails').hide();
	    			}
	    		}
	    	});
			$('#loading').fadeOut();
		}else{
			$('.FlatAmountDetails').hide();
		}
	});

	$("#depositAmount").click(function() {
		$('#depositAmountDiv').slideToggle();
	});
	$("#rentalAmount").click(function() {
		$('#rentalAmountDiv').slideToggle();
	});
	$("#otherAmount").click(function() {
		$('#otherAmountDiv').slideToggle();
	});
		
	if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
		$('#startPicker,#endPicker').datepick({
		 	onSelect: customRange, showTrigger: '#calImg'
		});

});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
  

<script type="text/javascript">

</script>

<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definemanagementdecision"/></div>
        <div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
        <div class="portlet-content">
        <div  class="response-msg error availErr ui-corner-all" style="width:80%; display:none;"></div>
       		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
			<form name="defineManagementAdd" id="defineManagementAdd">
				<div class="width50 float-left" id="hrm">
					<fieldset>
					<div style="display:none;">
							<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40" id="sessionPersonId" disabled="disabled">
								<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
						</div>
				 		<div>
				 			<input type="hidden" name="decisionId" id="decisionId" value="${bean.decisionId }" />
							<label class="width30"><fmt:message key="re.property.info.buildingno"/><span style="color:red">*</span></label>	
						 	<input type="hidden" name="buildingIdTemp" id="buildingIdTemp" value="${bean.buildingId }" />
						 	<select id="buildingId" class="width30 validate[required]" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemList ne null}">
									<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
										<option value="${building.buildingId }">${building.buildingName }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.flatno"/><span style="color:red">*</span></label>	 
						 <input type="hidden" name="flatIdTemp" id="flatIdTemp" value="${bean.flatId }" />
							<select id="flatId" class="width30 validate[required]">
								<option value="">-Select-</option>
								<c:if test="${requestScope.item ne null}">
									<c:forEach items="${requestScope.item}" var="flat" varStatus="status">
										<option value="${flat.flatId }">${flat.flatNumber }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<fieldset>
							<div>
							 	<label class="width30"><fmt:message key="re.property.info.depositamount"/></label>
							 	<c:choose>
									<c:when test="${bean.depositAmount eq 1 }">
										<input class="width40" type="checkbox" checked="checked" name="depositAmount" id="depositAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" name="depositAmount" id="depositAmount" >
									</c:otherwise>
								</c:choose>
							 </div>
							 <div id="depositAmountDiv">
								<div>
								 	<c:choose>
							 			<c:when test="${bean.depositIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="depositIncDec"  id="depositInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="depositIncDec"  id="depositDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="depositIncDec"  id="depositInc">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="depositIncDec"  id="depositDec" checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsDepositTemp" value="${bean.calculatedAsDeposit}">
									<select id="calculatedAsDeposit" class="width40" >
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate" varStatus="status">
												<option value="${calculate.calculatedAs }">${calculate.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" type="text" value="${bean.percentageDeposit }" name="percentageDeposit" id="percentageDeposit">
							 	</div>
							 </div>
					 	</fieldset>
					 	<fieldset>
						 	<div>
							 	<label class="width30"><fmt:message key="re.property.info.rentalamount"/></label>
							 	<c:choose>
									<c:when test="${bean.rentalAmount eq 1 }">
										<input class="width40" type="checkbox" checked="checked" name="rentalAmount" id="rentalAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" name="rentalAmount" id="rentalAmount" >
									</c:otherwise>
								</c:choose>
						 	</div>
						 	<div id="rentalAmountDiv">
							 	<div>
							 		<c:choose>
							 			<c:when test="${bean.rentalIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="rentalIncDec"  id="rentalInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="rentalIncDec"  id="rentalDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="rentalIncDec"  id="rentalInc" >
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="rentalIncDec"  id="rentalDec" checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsRentalTemp" value="${bean.calculatedAsRental}">
									<select id="calculatedAsRental" class="width40" >
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate1" varStatus="status">
												<option value="${calculate1.calculatedAs }">${calculate1.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" type="text" value="${bean.percentageRental }" name="percentageRental" id="percentageRental">
							 	</div>
							 </div>
					 	</fieldset>
					 	<fieldset>
						 	<div>
							 	<label class="width30">Other Amount</label>
							 	<c:choose>
									<c:when test="${bean.otherAmount eq 1 }">
										<input class="width40" type="checkbox" checked="checked" name="otherAmount" id="otherAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" name="otherAmount" id="otherAmount" >
									</c:otherwise>
								</c:choose>
						 	</div>
						 	<div id="otherAmountDiv">
							 	<div>
								 	<c:choose>
							 			<c:when test="${bean.otherIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="otherIncDec"  id="otherInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="otherIncDec"  id="otherDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="otherIncDec"  id="otherInc">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="otherIncDec"  id="otherDec"  checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsOtherTemp" value="${bean.calculatedAsOther}">
									<select id="calculatedAsOther" class="width40" >
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate2" varStatus="status">
												<option value="${calculate2.calculatedAs }">${calculate2.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" type="text" value="${bean.percentageOther }" name="percentageOther" id="percentageOther">
							 	</div>
							 </div>
					 	</fieldset>
						<div>
						 	<c:choose>
								<c:when test="${bean.holdingFlat eq 1 }">
									<input class="width40" type="checkbox" checked="checked" name="holdingFlat" id="holdingFlat"/>
								</c:when>
								<c:otherwise>
									<input class="width40" type="checkbox" name="holdingFlat" id="holdingFlat" >
								</c:otherwise>
							</c:choose>
						 	<label class="width30"><fmt:message key="re.property.info.holdingflat"/></label>
						 	
						 </div>
						 <div>
						 	<c:choose>
								<c:when test="${bean.holdingRent eq 1 }">
									<input class="width40" type="checkbox" checked="checked" name="holdingRent" id="holdingRent"/>
								</c:when>
								<c:otherwise>
									<input class="width40" type="checkbox" name="holdingRent" id="holdingRent" >
								</c:otherwise>
							</c:choose>
						 	<label class="width30"><fmt:message key="re.property.info.holdingcontract"/></label>
						 </div>
					</fieldset>
				</div>
				<div class="width50 float-left FlatAmountDetails" id="hrm">
					<fieldset>
						<div>
							<label class="width30"><fmt:message key="re.offer.rentamount"/></label>
							<input type="text" name="rent" class="width30" value="${beanFlatAmount.rent }" id="rent" disabled="disabled">
						</div>		
						<div>
							<label class="width30"><fmt:message key="re.property.info.depositamount"/></label>
							<input type="text" name="deposit" class="width30" value="${beanFlatAmount.deposit }" id="deposit" disabled="disabled">
						</div>
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset style="">
						<div>
							<label class="width30"><fmt:message key="re.property.info.fromdate"/><span style="color:red">*</span></label>
							<input type="text" name="startPicker" class="width30 validate[required]" readonly="readonly" id="startPicker" value="${bean.fromDate }" />
						</div>		
						<div>
							<label class="width30"><fmt:message key="re.property.info.todate"/><span style="color:red">*</span></label>
							<input type="text" name="endPicker" class="width30 validate[required]" readonly="readonly" id="endPicker" value="${bean.toDate }" />
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.remarks"/></label>
							<input type="text" name="remarks" class="width30" value="${bean.remarks }" id="remarks">
						</div>
					</fieldset>
				</div>
			</form>
			<div style="display:none;" class="tempresult"></div>
			<div class="clearfix"></div>			
			<div class="float-right buttons ui-widget-content ui-corner-all">
				<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
	 			<div class="portlet-header ui-widget-header float-right cancel"><fmt:message key="re.property.info.discard"/></div> 
	 			<div class="portlet-header ui-widget-header float-right" id="request" style="display:none;cursor:pointer;"><fmt:message key="cs.common.button.requestforverification"/></div>
				<div class="portlet-header ui-widget-header float-right update"><fmt:message key="re.owner.info.update"/></div>
			</div>
		</div>
	</div>
</div>
							
							
		
		
		
		