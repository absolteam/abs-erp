<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var slidetab;
var actualID;
var openFlag=0;
$(function(){

	$('#buildingId').val($('#buildingIdTemp').val());
	 $("#offerFormEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	
	$('.formError').remove();

	approvedStatus=$('#approvedStatus').val(); 
	if( approvedStatus == 'E'){
		$('#request').remove();	
		$('#discard').remove();	
		$('#save').remove();
		$('#close').fadeIn();
		$('#buildingId').attr("disabled",true);
		$('#defaultPopup').attr("disabled",true);
		$('#startPicker').attr("disabled",true);
		$('#endPicker').attr("disabled",true);
		$('#extensionDays').attr("disabled",true);
		$('.addFeature').fadeOut();	
		$('.editFeature').fadeOut();	
		$('.delFeature').fadeOut();	
		$('.commonErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#request').remove();	
		$('#discard').remove();	
		$('#save').remove();
		$('#close').fadeIn();
		$('#buildingId').attr("disabled",true);
		$('#defaultPopup').attr("disabled",true);
		$('#startPicker').attr("disabled",true);
		$('#endPicker').attr("disabled",true);
		$('#extensionDays').attr("disabled",true);
		$('.addFeature').fadeOut();	
		$('.editFeature').fadeOut();	
		$('.delFeature').fadeOut();
		$('.commonErr').hide().html("Workflow is already Approved, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#request').fadeIn();	
		$('#discard').fadeIn();	
		$('#save').fadeIn();
		$('.commonErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
	}

	$('#buildingId').focus(function(){
		$('.commonErr').fadeOut();
	});
	
    
	//Save Material Despatch details
	$("#save").click(function(){
		if($('#offerFormEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var offerId=$('#offerId').val();
				var buildingId=$('#buildingId').val();
				var customerId=$('#customerId').val();
				var address= $('#address').val();
				var offerDate=$('#defaultPopup').val();
				var offerValidityDays=$('#offerValidityDays').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var extensionDays=$('#extensionDays').val();

				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_final_save_offer_form.action",
						data: {offerId: offerId, buildingId: buildingId, customerId: customerId,presentAddress: address,offerDate: offerDate,
							offerValidityDays: offerValidityDays,fromDate: fromDate,toDate: toDate,extensionDays: extensionDays,
							headerFlag: headerFlag, trnValue: transURN},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}							
							$('#loading').fadeOut(); 
						} 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Request Material Despatch details
	$("#request").click(function(){
		if($('#offerFormEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var offerId=$('#offerId').val();
				var buildingId=$('#buildingId').val();
				var customerId=$('#customerId').val();
				var address= $('#address').val();
				var offerDate=$('#defaultPopup').val();
				var offerStatus=$('#offerStatus').val();
				var offerValidityDays=$('#offerValidityDays').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var extensionDays=$('#extensionDays').val();
				var headerLedgerId = $('#headerLedgerId').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);

				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var applicationId=Number($('#headerApplicationId').val());
				var workflowStatus="Request For Offer Form";
				var wfCategoryId=Number($('#headerCategoryId').val());
				var functionId=Number($('#headerCategoryId').val());
				var functionType="request";
				
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_final_request_offer_form.action",
						data: {offerId: offerId,buildingId: buildingId, customerId: customerId,presentAddress: address,offerDate: offerDate,offerStatus:offerStatus,
							offerValidityDays: offerValidityDays,fromDate: fromDate,toDate: toDate,extensionDays: extensionDays, trnValue: transURN,
							companyId:companyId,applicationId:applicationId,workflowStatus:workflowStatus,wfCategoryId:wfCategoryId,
							functionId:functionId,functionType:functionType,sessionpersonId: sessionpersonId},
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
								$('.formError').hide();
								$('.commonErr').hide();
								$('.tempresultfinal').fadeOut();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#request').remove();	
									$('#save').remove();
									$('#discard').fadeOut();
									$('#close').fadeIn();
									$('.commonErr').hide();
								} else{
									$('.tempresultfinal').fadeIn();
								}
							},  
						error:function(result){
								$('.tempresultfinal').fadeOut();
								$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$("#main-wrapper").html(result); 
								} else{
									$('.tempresultfinal').fadeIn();
								};
						 	} 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Feature Details
 	$('.addFeature').click(function(){ 
 		if(openFlag==0){
			if($('#offerFormEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        var buildingId=$('#buildingId').val();
		       //alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_offer_form_redirect.action", 
					data: {buildingId: buildingId},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 
					 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				$('#warningMsg').hide().html("Please insert record ").slideDown();
			}
 		}
	});
	
	$(".editFeature").click(function(){
		if(openFlag==0){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        var buildingId=$('#buildingId').val();
			//actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/edit_edit_offer_form_redirect.action", 
				data: {buildingId: buildingId}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();

				 var temp5= $($(slidetab).children().get(6)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(2)).val(temp5);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
			  	openFlag=1;
			  	$('#loading').fadeOut();
	
				}
			});
		}
	});
   
 	$('.delFeature').click(function(){
 		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			 var offerId=$("#offerId").val();
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_offer_form_update.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, offerId: offerId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
		       			childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
 		}
 	});

	$("#discard").click(function(){
		$('#loading').fadeIn();
		var trnValue=$("#transURN").val();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_discard_offer_form.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
   		});
	});
	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/offer_form_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	$('.customer-popup').click(function(){ 
	       tempid=$(this).parent().get(0);  
	       //var orderId=$('#orderId').val();
			$('#customerform-popup').dialog('open');
			//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/offer_form_load_customer.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.customerform-result').html(result); 
				},
				error:function(result){ 
					 $('.customerform-result').html(result); 
				}
			}); 
		});
	$('.addrowsfeature').click(function(){
		var count=Number(0);
		$('.offer').each(function(){
			count=count+1;
		});
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/offer_form_editrow.action", 
	 	async: false,
		data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
	});
});

	$('#customerform-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$('#customerName').val("");
				$('#address').val("");
				$('#mobileNo').val("");
				$('#landLineNo').val(""); 
				$('#emailId').val("");
				$('#country').val("");
				$('#state').val("");
				$('#city').val("");
				$('#customerType').val("");
				$('#offerValidityDays').val("");
				$(this).dialog("close"); 
			} 
		}
	});
	$('#offerform-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$('#flatNumber').val("");
				$('#rentAmount').val("");
				$('#contractAmount').val("");
				$(this).dialog("close"); 
			} 
		}
	});
	 
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerform"/></div>
		<div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
		<div class="portlet-content">
			<form id="offerFormEdit">
				<fieldset>
	 				<div style="display:none;" class="tempresultfinal">
						<c:if test="${requestScope.bean != null}">
							<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
							 <c:choose>
								 <c:when test="${bean.sqlReturnStatus == 1}">
									<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
								<c:when test="${bean.sqlReturnStatus != 1}">
									<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
							</c:choose>
						</c:if>    
					</div>	 
		 			<div style="float:left;width:48%;" id="hrm" class="">
					 	<fieldset>
					 	<div style="display:none;">
								<label>Person Id</label><input type="text" name="sessionpersonId" value="${PERSON_ID}" class="width40" id="sessionpersonId" disabled="disabled">
									<input type="hidden" name="workflowStatus" value="${bean.workflowStatus }" class="width40" id="workflowStatus">
									<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.offer.offerno"/></label>	 
								<input class="width40" type="text" name="offerNumber" value="${bean.offerNumber }" id="offerNumber" disabled="disabled" >
								<input class="width40" type="hidden" name="offerId" value="${bean.offerId }" id="offerId" disabled="disabled" >
							</div>
						 	<div>
						 		<label class="width30"><fmt:message key="re.offer.buildingname"/><span style="color:red">*</span></label>
						 		<input class="width40" type="hidden" name="buildingIdTemp" value="${bean.buildingId }" id="buildingIdTemp" disabled="disabled" >
								<select id="buildingId" class="width40 validate[required]" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.itemList ne null}">
										<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
											<option value="${building.buildingId }">${building.buildingName }</option>
										</c:forEach>
									</c:if>
								</select>
							</div>	
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.customername"/><span style="color:red">*</span></label>
							 	<input class="width40 validate[required]" type="text" name="customerName" id="customerName" value="${bean.customerName }" disabled="disabled" >
							 	<input class="width40" type="hidden" name="customerId" id="customerId" value="${bean.customerId }" disabled="disabled" >
							 	<span id="hit7" class="button" style="width: 40px ! important;">
									<a class="btn ui-state-default ui-corner-all customer-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.address"/></label>
							 	<input class="width40" type="text" name="address" value="${bean.presentAddress }" id="address" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.mobileno"/></label>
							 	<input class="width40" type="text" name="mobileNo" value="${bean.mobileNo }" id="mobileNo" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.landLineNo"/></label>
							 	<input class="width40" type="text" name="landLineNo" value="${bean.landLineNo }" id="landLineNo" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.email"/></label>
							 	<input class="width40" type="text" name="emailId" value="${bean.emailId }" id="emailId" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.country"/></label>
							 	<input class="width40" type="text" name="country" value="${bean.countryName }" id="country" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.state"/></label>
							 	<input class="width40" type="text" name="state" value="${bean.stateName }" id="state" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.city"/></label>
							 	<input class="width40" type="text" name="city" value="${bean.cityName }" id="city" disabled="disabled" >
						 	</div>
					 	</fieldset>
		 			</div>
	 				<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:100px;">
							<div>
							   <label class="width30"><fmt:message key="re.offer.offerdate"/><span style="color:red">*</span></label>
						 	  <input class="width30 validate[required]" id="defaultPopup" type="text" name="offerDate" value="${bean.offerDate }" readonly="readonly"/>
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.customertype"/></label>
								<input class="width30 taskDueDate" id="customerType" type="text" name="customerType" value="${bean.customerTypeId }"  disabled="disabled">
								<input class="width30 " id="customerTypeId" type="hidden" name="customerTypeId" value=""  disabled="disabled">
								<input class="width30 " id="rentFees" type="hidden" name="rentFees" value=""  disabled="disabled">
								<input class="width30 " id="rentFeesAmount" type="hidden" name="rentFeesAmount" value=""  disabled="disabled">
							</div> 
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.offervaliditydays"/></label>
								<input class="width10" id="offerValidityDays" type="text"  name="offerValidityDays" disabled="disabled" value="${bean.offerValidityDays }">
							</div> 
							<div>
							 	<label class="width30"><fmt:message key="re.offer.extensiondays"/></label>
								<input class="width10 " id="extensionDays" type="text" name="extensionDays" value="${bean.extensionDays }">
							</div> 
				 		</fieldset>
				 	</div>
	 				<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:140px;">
							<legend><fmt:message key="re.offer.expectedcontractperiod"/></legend>
							<div>
							 	<label class="width30"><fmt:message key="re.offer.from"/><span style="color:red">*</span></label>
								<input class="width30 validate[required]" id="startPicker" type="text" name="fromDate" value="${bean.fromDate }">
							</div> 
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.to"/><span style="color:red">*</span></label>
								<input class="width30 validate[required]" id="endPicker" type="text" name="toDate" value="${bean.toDate }">
							</div> 
						</fieldset>
					</div>
				</fieldset>
			</form>

			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.amountdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important;  display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important;  display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.offer.lineno"/></th>
				            					<th class="width20"><fmt:message key="re.offer.flatnumber"/></th>
				            					<th class="width20"><fmt:message key="re.offer.rentamount"/></th>
				            					<th class="width20"><fmt:message key="re.offer.contractamount"/></th>
				            					<th style="width:5%;"><fmt:message key="re.offer.options"/></th>
											</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="offer">  	 
													<td class="width20">${result.lineNumber}</td>
													<td class="width20">${result.flatNumber}</td>	
													<td class="width20">${result.rentAmount}</td> 
													<td class="width20">${result.contractAmount}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.offerDetailId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;"  title="Add this row" >
														 	 <span class="ui-icon ui-icon-plus"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature" style="cursor:pointer;" title="Edit this row" >
															<span class="ui-icon ui-icon-wrench"></span>
														</a>
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete this Row" >
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="owner_id">${result.propertyFlatId}</td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	</c:forEach>	
										 	<c:forEach var="i" begin="${countSize+1}" end="${countSize+2}">
											 	<tr class="offer">  	 
													<td class="width20">${i}</td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>  
													</td> 
													<td style=" width:5%;"> 
					  									<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;" title="Add this row" >
														 	 <span class="ui-icon ui-icon-plus"></span>
														</a>
														<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature" style="cursor:pointer;" title="Edit this row" >
															<span class="ui-icon ui-icon-wrench"></span>
														</a>
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete this Row" >
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="flat_id"></td>
													<td style="display:none;"></td>	 
												</tr>  
								  		 	</c:forEach>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
		</div>
	 	<div class="float-right buttons ui-widget-content ui-corner-all">
	 			<div class="portlet-header ui-widget-header float-right discard" style="display:none;">Contract</div> 
				<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="request">Request For Verification</div>
				<div class="portlet-header ui-widget-header float-right save-all" id="save"><fmt:message key="re.owner.info.update"/></div> 
				<div style="display:none;" class="portlet-header ui-widget-header float-right" id="close">Close</div>
		</div>
 	</div>
 </div>


	 	
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="offerform-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="offerform-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>	 	
	
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="customerform-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="customerform-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>	
	 	 	
