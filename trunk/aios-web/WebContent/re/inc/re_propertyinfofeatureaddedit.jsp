<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoFeatureAddEdit" id="propertyInfoFeatureAddEdit" style="position: relative;"> 
<div class="width36 view float-left" id="hrm" style="height: 100px; padding-left: 20px;">
		<fieldset>
			<legend>Selection</legend>
			<div style="display: none;">
				<label><fmt:message key="re.property.info.lineno"/><span class="mandatory">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50" disabled="disabled" />
			</div>
			<div>
				<label class="width30"><fmt:message key="re.property.info.featurecode"/><span class="mandatory">*</span></label>
				<input type="text" name="featureCode" id="featureCode" class="width45 validate[required]" disabled="disabled"/>
				<input type="hidden" name="featureId" id="featureId" class="width50" />
				<input type="hidden" name="featureLookupId" id="featureLookupId" class="width50" />
				<span class="button" style="width: 40px ! important; cursor: pointer;">
					<a class="btn ui-state-default ui-corner-all feature-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
			<div>
				<label class="width30"><fmt:message key="re.property.info.features"/></label>
				<input name="features" id="features" class="width45 " disabled="disabled">
			</div>
		</fieldset>
</div> 

<div class="width30 view float-left" id="hrm" style="height: 100px;">
	<fieldset>					 
		<legend>Details</legend>		
		<div>
			<label class="width30">Feature Details</label>
			<input type="text" name="measurement" id="measurement" class="width50 ">
		</div>
		<div style="display: none;">
			<label><fmt:message key="re.property.info.featuredetail2"/></label>
			<input type="text" name="remarks" id="remarks" class="width50">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>
	
<div class="width30 view float-left" id="hrm" style="height: 100px;">
	<fieldset>
		<legend>Uploads</legend>
		<div id="hrm" Style="margin-bottom: 10px;">
			<div id="UploadDmsDiv" class="width100">
				
				<div style="height: 80px;">									
					
					<div id="dms_image_information_feature" style="cursor: pointer; color: blue;">
						<u>Upload image here</u>
					</div>
					<div id="uploadedFeatureImages" style="padding-top: 10px; margin-top:3px; height: 30px; overflow: auto;"></div>
					
				</div>
									
			</div>
		</div>
	</fieldset>
</div> 

<div class="clearfix" style="padding-top: 10px; padding-right: 20px;"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div> 
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;"
	 class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable"
	 tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		
	<div id="property-featurepopup" class="ui-dialog-content ui-widget-content"
		style="height: 200px !important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
	</div>
</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
     editFeatureId = $($($(slidetab).children().get(5)).children().get(0)).val(); 
	 $(function(){ 
		 $('.formError').remove(); 
		
		 
		 $jquery("#propertyInfoFeatureAddEdit").validationEngine('attach');


		// for uploader
		if((editFeatureId != undefined) && (editFeatureId != null) && (editFeatureId != "")) {
			
			populateUploadsPane("img","uploadedFeatureImages", "PropertyDetail", editFeatureId);
			
			$('#dms_image_information_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedFeatureImages", "PropertyDetail", editFeatureId, "");
			});
		}
		else {
			
			$('#dms_image_information_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedFeatureImages", "PropertyDetail", "-1");
			});
		}
		
		$('.cancel').click(function(){ 
			$('.formError').remove();
			$('#property-featurepopup').dialog('destroy');		
			$('#property-featurepopup').remove(); 
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(6)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing button	
	        openFlag=0;		
		 });
		 $('#add').click(function(){
			  if($jquery('#propertyInfoFeatureAddEdit').validationEngine('validate')){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var featureId=$('#featureId').val();
					var featureCode=$('#featureCode').val();
					var features=$('#features').val();
					var measurement=$('#measurement').val();
					var remarks=$('#remarks').val();
					var trnVal =$('#transURN').val(); 
					var lookupDetailId=$('#featureLookupId').val();
					var flag=false;

		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_edit_property_feature_update.action", 
					 	async: false,
					 	data: {featureId: featureId,featureCode: featureCode,features: features,measurements: measurement, 
							remarks: remarks,trnValue:trnVal, actualLineId: actualID,
							propertyInfoOwnerId: editFeatureId,lookupDetailId:lookupDetailId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
							},  
						 	error:function(result){
							 	alert("err "+result);
						 		 $('.tempresult').html(result);
		                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
		                            $('#loading').fadeOut();
		                            return false;
						 	} 
			        	});
			        	if(flag==true){
			        		$('#property-featurepopup').dialog('destroy');		
			    			$('#property-featurepopup').remove(); 
			        		$($(slidetab).children().get(0)).text(lineNo);
			        		$($(slidetab).children().get(1)).text(featureCode);
			        		$($(slidetab).children().get(2)).text(features); 
			        		$($(slidetab).children().get(3)).text(measurement); 

			        		$($(slidetab).children().get(4)).text(remarks);

			        		$($(slidetab).children().get(7)).text(featureId);
			        		$($(slidetab).children().get(9)).text(lookupDetailId);
						   
						  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
						  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
							$($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete Button
							$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button
							openFlag=0;	
					  		$($($(slidetab).children().get(5)).children().get(0)).val($('#objLineVal').html());
					  		$("#transURN").val($('#objTrnVal').html());
					   } 
				  }else{
				  return false;
			  }
		});

		 $('.feature-popup').click(function(){ 
		       tempid=$(this).parent().get(0);   
		       $('.ui-dialog-titlebar').remove();   
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_info_feature_details.action",
				 	async: false,  
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			}); 
		 $('#property-featurepopup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:500, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
		
});
 </script>	