 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <script type="text/javascript"> 
 var tempid;
 var slidetab;
 var shockaction="";
 
 $(function(){ 
	 $('#assetNumber').attr("disabled",true);
	 $('#assetAddress').attr("disabled",true);
	 $('#assetDesc').attr("disabled",true);

	 $('#agreementNumber').attr("disabled",true);
	 $('#agreementDate').attr("disabled",true);
	 $('#startPicker').attr("disabled",true);
	 $('#endPicker').attr("disabled",true);
	 $('#firstRentDate').attr("disabled",true);

	 $('#firstRentDate').attr("disabled",true);
	 $('#payementFrequency').attr("disabled",true);
	 $('#leadDay').attr("disabled",true);
	 $('#currencyCode').attr("disabled",true);
	 $('#rentAmount').attr("disabled",true);

	 $('#rentType').attr("disabled",true);
	 $('#exchangeRate').attr("disabled",true);
	 $('#fnAmount').attr("disabled",true); 
	 $('#tenantName').attr("disabled",true);  
	 
	 $('#addA').attr("disabled",true);
	 $('#addB').attr("disabled",true);
	 $('#addC').attr("disabled",true);
	 $('#addD').attr("disabled",true);
	 $('#addE').attr("disabled",true); 
	 
	 (document.edittenant.payementFrequency.value=(document.getElementById('pyFreq').value)).selected='true';  
	 (document.edittenant.currencyCode.value=(document.getElementById('curCode').value)).selected='true';   

	 $('#cancel').click(function(){
		 $.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/ta_tenantagreement_retrieve.action",   
	     	async: false, 
			dataType: "html",
			cache: false,
			error: function(result){
				$("#main-wrapper").html(result);  //gridDiv main-wrapper
			},
	     	success: function(result){
				$("#main-wrapper").html(result);  //gridDiv main-wrapper
				$('.success').show();
				$('.success').html("Discarded Successfully");
	     	}
		});
	 });

	 $('#confirm').click(function(){
		 var agreementId=$('#agreementId').val(); 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/ta_tenantagreement_deleteconfirm.action", 
			 	async: false,
			 	data: {agreementId:agreementId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide(); 
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html();
					 
				     if(tempsMsg!=""){
				    	$('#sMsg').show(); 
				    	$('#eMsg').hide();}
				    	else{ 
					    	$('#eMsg').show(); 
					    	$('#sMsg').hide();
				    	}  
			 	},  
			 	error:function(result){  
                 $("#main-wrapper").html($('#eMsg').html()); 
                 return false;
			 	} 
      	}); 
		
	 });
	 
 });
 </script>

<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Agreement</div>
             
        <div class="portlet-content">
        	<form name="edittenant" id="editTenantValidate">
        		<div class="tempresult" style="display:none"></div>
        		<div class="width100 float-left" id="hrm">  
					<div class="width30 float-right">
						<input type="hidden" name="agreementId" id="agreementId" value="${tenantTo.agreementId}"/>
						<label class="width45" for="agreementNumber">Agreement Number</label>
						<input type="text" id="agreementNumber" class="width40" name="agreementNumber" value="${tenantTo.agreementNumber}"/ >
					</div>
					<div class="width60 float-left"> 
						<label class="width20" for="assetNumber">Asset Number</label>
						<input type="hidden" id="assetId" name="assetId" value="${tenantTo.assetId}"> 
						<input type="text" id="assetNumber" class="width20" name="assetNumber" value="${tenantTo.assetNumber}">  
				</div>  
				</div>
				
				<div class="width100 float-left" id="hrm">  
					<div class="width30 float-right">
						<label class="width45">Agreement Date</label>
						<input type="text" id="agreementDate" class="width40" name="agreementDate" value="${tenantTo.edDate}">
					</div>  
					<div class="width60 float-left">
							<label class="width20" for="assetAddress">Asset Address</label>
							<input type="text" id="assetAddress" class="width70" name="assetAddress">
				   </div> 
				</div>
				
				<div class="width100 float-left" id="hrm"> 
						<div class="width30 float-right">
							<label class="width45" for="agreementStatus">Agreement Status</label>
							<select name="agreementStatus" id="agreementStatus" class="40">
								<option value="">--Select--</option>
								<option value="">Approved</option>
								<option value="">Awaiting</option>
								<option value="">Canceled</option>
							</select>
						</div> 
						
						<div class="width60 float-left">
							<label class="width20" for="assetDesc">Asset Description</label>
							<input type="text" id="assetDesc" class="width70" name="assetDesc" value="${tenantTo.assetDesc}">
						</div> 
				</div>
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
						<div class="width30 float-left">
							<label class="width40" for="startPicker">Agreement Valid From</label>
							<input type="text" id="startPicker" class="width40 startDate" name="startDate"  value="${tenantTo.stDate}">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="endDate">Agreement Valid To</label>
							<input type="text" id="endPicker" class="width40 endDate" name="endDate" value="${tenantTo.edDate}">
						</div>  
				</div>
				
				<div style="margin-bottom:10px;" id="hrm" class="width100 float-left"> 
					<fieldset>
						<legend>Rent Information</legend> 
							<div class="width30 float-left">
								<label class="width50" for="firstRentDate">First Rent Date</label>
								<input type="text" id="firstRentDate" class="width30" name="firstRentDate" value="${tenantTo.frDate}">
							</div>
							
							<div class="width30 float-left">
								<label class="width60" for="payementFrequency">Payment Frequency</label>
								<select name="payementFrequency" class="width30" id="payementFrequency">
									<option value="">--Select--</option>
									<c:if test="${pyList ne null }">
										<c:forEach items="${pyList}" var="bean" >
											<option value="${bean.paymentId}">${bean.paymentCode}</option>
										</c:forEach>
									</c:if>		 
								</select>
								<input type="hidden" id="pyFreq" name="pyFreq" value="${tenantTo.pyFreq}">
							</div>
							
							<div class="width30 float-left">
								<label class="width60" for="leadDay">Invoice Creation lead day</label>
								<input type="text" id="leadDay" class="width30" name="leadDay" value="${tenantTo.leadDays}">
							</div> 
					</fieldset>
				</div> 
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
					<fieldset>
					<legend>Currency Information</legend>
					<div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="currencyCode">Currency</label>
								<select name="currencyCode" class="width30" id="currencyCode">
									<option value="">--Select--</option>
									<c:if test="${curList ne null }">
										<c:forEach items="${curList}" var="bean" >
											<option value="${bean.currencyName}">${bean.currencyName}</option>
										</c:forEach>
									</c:if>		 
								</select>
								<input type="hidden" id="curCode" name="curCode" value="${tenantTo.curCode}">
						</div>
						<div class="width30 float-left">
							<label class="width40" for="rentAmount">Rent</label>
							<input type="text" id="rentAmount" class="width30" name="rentAmount" value="${tenantTo.amount}">
						</div> 
						
						<div class="width30 float-left">
							<label class="width40 float-left" for="">Rate Type</label>
							 <select name="rentType" class="width30" id="rentType">
									<option value="">--Select--</option>
									<option value="T">Table</option>
									<option value="U">User</option> 
								</select>
						</div>
				     </div>	 
				     <div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="exchangeRate">Conversion Rate</label>
							<input type="text" id="exchangeRate" class="width30" name="exchangeRate" value="${tenantTo.exchangeRate}">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="fnAmount">Functional Amount</label>
							<input type="text" id="fnAmount" class="width30" name="fnAmount" value="${tenantTo.functionalAmount}">
						</div>
					</div>	 
					</fieldset>
				</div>
				
				<div class="width100 float-left" id="hrm" style="margin-bottom:10px;"> 
					<fieldset>
					<legend>Tenant Information</legend>
					<div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40" for="tenantName">Tenant Name</label>
							<input type="hidden" id="tenantId" name="tenantId" value="${tenantTo.tenantId}">
							<input type="text" id="tenantName" class="width40" name="tenantName" value="${tenantTo.tenantName}"> 
						</div> 
						<div class="width30 float-left">
							<label class="width40">Tenant Address</label>
							<input type="text" id="addA" class="width50" name="tenantAddress" value="${tenantTo.tenantAddress}">
						</div> 
						
						 <div class="width30 float-left">
							<label class="width40">Address1</label>
							<input type="text" id="addB" class="width50" name="tenantAddress1" value="${tenantTo.tenantAddress1}">
						</div> 
				     </div>	 
				     <div class="width100 float-left"> 
						<div class="width30 float-left">
							<label class="width40">Place</label>
							<input type="text" id="addC" class="width30" name="tenantPlace"  value="${tenantTo.tenantPlace}">
						</div>
						
						<div class="width30 float-left">
							<label class="width40" for="">Country</label>
							<input type="text" id="addD" class="width30" name="tenantCountry" value="${tenantTo.tenantCountry}">
						</div>
						
						<div class="width30 float-left">
							<label class="width40">Pin</label>
							<input type="text" id="addE" class="width30" name="" >
						</div>
					</div> 
					</fieldset>
				</div> 
				<input type="hidden" name="ledgerId" id="ledgerId" value="${tenantTo.ledgerId}"/>
				<div class="clearfix"></div>  
				 
	       		<div class="float-right buttons ui-widget-content ui-corner-all" style=""> 
					<div class="portlet-header ui-widget-header float-right" id="cancel">Discard</div>  
				 	<div class="portlet-header ui-widget-header float-right" id="confirm">Confirm</div> 
				</div>
            </form>
        </div>
    </div>
</div> 