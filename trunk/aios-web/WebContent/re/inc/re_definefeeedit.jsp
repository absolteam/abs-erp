<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';

$(function (){
	$('#calculatedAs').val($('#tempcalculatedAs').val());
	if($('#endPicker').val() =='31-Dec-9999'){
		$('#endPicker').val("")
	}

	 $("#defineFeeAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

		$("#cancel").click(function(){
		$('#loading').fadeIn();		 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_fee_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut(); 
				 
	     	}
		});
		return true;
	}); 

		$('#update').click(function(){ 
			if($("#defineFeeAdd").validationEngine({returnIsValid:true})){
			var feeId=$('#feeId').val();	
			var descriptions=$('#descriptions').val();
			var calculatedAs=$('#calculatedAs').val(); 
			var feeAmount=$('#feeAmount').val(); 
			var fromDate=$('#startPicker').val();
			var toDate=$('#endPicker').val();
			var deposit;
			if($('#deposit').is(':checked')){deposit=1;}else{deposit=0;}
			var fromdate=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
			 var todate=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
			 //alert("fromdate"+fromdate+"todate"+todate);
			if( (descriptions != null && descriptions.trim().length > 0 && feeAmount != null && feeAmount.trim().length > 0 )){ 
	        if(fromdate<=todate){
	        $('#loading').fadeIn();	    
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/define_fee_update.action",   
		     	async: false,
		     	data: {feeId:feeId,descriptions: descriptions,calculatedAs:calculatedAs,feeAmount: feeAmount,deposit: deposit,
					fromDate:fromDate,toDate:toDate },
				dataType: "html",
				cache: false,
				success:function(result){	
			 		$('.formError').hide(); 
			 		$('.tempresultfinal').fadeOut();
		 		 	$('.tempresultfinal').html(result);
					if($("#sqlReturnStatus").val()==1){
						$("#main-wrapper").html(result); 
					} else{
						$('.tempresultfinal').fadeIn();
					}
					$('#loading').fadeOut(); 
					
			 	},  
			 	error:function(result){  
			 		$('.formError').hide(); 
			 		$('.tempresultfinal').fadeOut();
		 		 	$('.tempresultfinal').html(result);
					if($("#sqlReturnStatus").val()==1){
						$("#main-wrapper").html(result); 
					} else{
						$('.tempresultfinal').fadeIn();
					}
					$('#loading').fadeOut(); 
                   return false;
			 	} 
			});
	        }
	        else
			 {
				$('.error').show();
				$('.error').html("'To Date' should be greater than 'From Date' !!!");
				return false;
			 }}else{alert("Please enter the  Description and Fee Amount");
	 			return false;
	 			}
			}
			else{return false;}
			return true;
		}); 

		$('#descriptions').blur(function(){
			var descriptions=$('#descriptions').val().trim();
			if(descriptions=="Cheque" || descriptions=="Deposit" || descriptions=="Issuing Contract" || descriptions=="Penalty" || descriptions=="Maintenance"){
				$('.commonErr').show();
				$('.commonErr').html("The record is already added!!!");
				}
			else{
				$('.commonErr').hide();
			}
		});		

		if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
		 $('#startPicker,#endPicker').datepick({
		 	onSelect: customRange, showTrigger: '#calImg'
			});

		// Default Date
		 $('#defaultActualPicker').datepick();
	});
	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	    var daysInMonth =$.datepick.daysInMonth(
	    $('#selectedYear').val(), $('#selectedMonth').val());
	    $('#selectedDay option:gt(27)').attr('disabled', false);
	    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	    if ($('#selectedDay').val() > daysInMonth) {
	        $('#selectedDay').val(daysInMonth);
	    }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

</script>
  



<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definefee"/></div>
					<div class="portlet-content">
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
					<div style="display:none;" class="tempresultfinal">
						<c:if test="${requestScope.bean != null}">
							<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
							 <c:choose>
								 <c:when test="${bean.sqlReturnStatus == 1}">
									<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
								<c:when test="${bean.sqlReturnStatus != 1}">
									<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
							</c:choose>
						</c:if>    
					</div>
			 	   <div  class="response-msg error searcherror ui-corner-all" style="width:80%; display:none;"></div>
			 	   <form name="defineFeeAdd" id="defineFeeAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.info"/></legend>						
		                            <div>
		                            	<label><fmt:message key="re.property.info.fromdate"/><span style="color:red">*</span></label>
		                            	<input type="text" name="startPicker" class="width30 validate[required]" id="startPicker" readonly="readonly" value="${bean.fromDate}"/>
		                            </div>		
									<div>
										<label><fmt:message key="re.property.info.todate"/></label>
										<input type="text" name="endPicker" class="width30" id="endPicker" readonly="readonly" value="${bean.toDate}"/>
									</div>
									
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.property.info.feeinfo"/></legend>
									<div style="display:none;"><label>Fee Id No.</label><input type="text" name="feeIdNo" class="width30" id="feeId" disabled="disabled" value="${bean.feeId}"/></div>
									<div><label><fmt:message key="re.property.info.feedescription"/><span style="color:red">*</span></label><input type="text" name="sno" class="width30 validate[required]" id="descriptions" value="${bean.descriptions}"/></div>
									<div><label><fmt:message key="re.property.info.calculatedas"/><span style="color:red">*</span></label>
									<input type="hidden" name="calculatedAs" class="width30" id="tempcalculatedAs" value="${bean.calculatedAs}"/>
										<select id="calculatedAs" class="width30 validate[required]" >
										<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="calculate" varStatus="status">
													<option value="${calculate.calculatedAs }">${calculate.calculatedCode }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
									<div>
										<label><fmt:message key="re.property.info.feeamount"/><span style="color:red">*</span></label>
										<input type="text" name="feeAmount" id="feeAmount" class="width30 validate[required,custom[onlyFloat]]" value="${bean.feeAmount}"/>
									</div>
									<div>
										<c:choose>
											<c:when test="${bean.deposit eq 1 }">
												<input  type="checkbox" checked="checked" name="deposit" id="deposit"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="deposit" id="deposit" >
											</c:otherwise>
										</c:choose>
									 	<label class=""><fmt:message key="re.property.info.deposit"/></label>
									</div>
							</fieldset> 
					  </div>
					</form>
				<div class="float-right buttons ui-widget-content ui-corner-all"> 			
					<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
					<div class="portlet-header ui-widget-header float-right" id="update"><fmt:message key="re.owner.info.update"/></div>
				</div>
			</div>
		</div>
	</div>
	