<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
	<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
 <script type="text/javascript" src="/js/thickbox-compressed.js"></script>
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var openFlag=0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){ 

	 $("#propertyFlatEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
		
	 var dmsUserId='${emp_CODE}';
		var dmsPersonId='${PERSON_ID}';
		var companyId=Number($('#headerCompanyId').val());
		var applicationId=Number($('#headerApplicationId').val());
		var functionId=Number($('#headerCategoryId').val());
		var functionType="list";//edit,delete,list,upload
		//$('#dmstabs-1, #dmstabs-2').tabs();
		//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
		$('#dms_create_document').click(function(){
			var dmsURN=$('#URNdms').val();
				slidetab=$($($(this).parent().get(0)).parent().get(0));
				loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
				//$('#tabs').tabs({selected:0});// switch to first tab
		});
		$('#dms_document_information').click(function(){
			var dmsURN=$('#URNdms').val();
				slidetab=$($($(this).parent().get(0)).parent().get(0));
			loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
			//$('#tabs').tabs({selected:1});// switch to second tab
		});
		
	$('#rentStatus').val($('#temprentStatus').val());
	$('#buildingId').val($('#tempbuildingId').val());
	$('#componentType').val($('#tempcomponentType').val());
	$('#flatStatus').val($('#tempflatStatus').val());
	$('#measurementCode').val($('#measurementCodeTemp').val());
	$('#noOfRooms').val($('#noOfRoomsTemp').val());
	
	if($('#endPicker').val() =='31-Dec-9999'){
		$('#endPicker').val("")
	}
	
	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	$("#comments").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#comments").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#comments").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    }); 

	//Approve Property Flat details
	$("#approve").click(function(){
		if($('#propertyFlatEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				propertyFlatId=$('#propertyFlatId').val();
				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				
				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var workflowStatus="Approved by Supervisor";
				var functionId=Number($('#functionId').val());
				var functionType="approve";
				var notificationId=$('#notificationId').val();
				var workflowId=$('#workflowId').val();
				var mappingId=$('#mappingId').val();
				var comments=$('#comments').val();
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/property_flat_supervisor_approval.action",
						data: {propertyFlatId:propertyFlatId,trnValue: transURN,headerFlag:headerFlag,sessionpersonId:sessionpersonId,companyId:companyId,
					 			workflowStatus:workflowStatus,functionId:functionId,functionType:functionType,notificationId:notificationId,
					 			workflowId:workflowId,mappingId:mappingId,comments:comments},  
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
									$('.formError').hide();
									$('.commonErr').hide();
									$('.tempresultfinal').fadeOut();
						 		 	$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$('.tempresultfinal').fadeIn();
										$('#loading').fadeOut(); 
										$('#approve').remove();	
										$('#reject').remove();	
										$('#close').fadeIn();
										$('.commonErr').hide();
									} else{
										$('.tempresultfinal').fadeIn();
									}
								},  
							error:function(result){
									$('.tempresultfinal').fadeOut();
									$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$("#main-wrapper").html(result); 
									} else{
										$('.tempresultfinal').fadeIn();
									};
							 	}  
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Feature Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	$('#close').click(function(){
		$('.backtodashboard').trigger('click');
	  });

	//Reject Property Flat details
	$("#reject").click(function(){
		if($('#propertyFlatEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				propertyFlatId=$('#propertyFlatId').val();
				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				
				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var workflowStatus="Rejected by Supervisor";
				var functionId=Number($('#functionId').val());
				var functionType="reject";
				var notificationId=$('#notificationId').val();
				var workflowId=$('#workflowId').val();
				var mappingId=$('#mappingId').val();
				var comments=$('#comments').val();
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/property_flat_supervisor_approval.action",
						data: {propertyFlatId:propertyFlatId,trnValue: transURN,headerFlag:headerFlag,sessionpersonId:sessionpersonId,companyId:companyId,
					 			workflowStatus:workflowStatus,functionId:functionId,functionType:functionType,notificationId:notificationId,
					 			workflowId:workflowId,mappingId:mappingId,comments:comments},  
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
									$('.formError').hide();
									$('.commonErr').hide();
									$('.tempresultfinal').fadeOut();
						 		 	$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$('.tempresultfinal').fadeIn();
										$('#loading').fadeOut(); 
										$('#approve').remove();	
										$('#reject').remove();	
										$('#close').fadeIn();
										$('.commonErr').hide();
									} else{
										$('.tempresultfinal').fadeIn();
									}
								},  
							error:function(result){
									$('.tempresultfinal').fadeOut();
									$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$("#main-wrapper").html(result); 
									} else{
										$('.tempresultfinal').fadeIn();
									};
							 	}  
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Feature Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});
	
});

</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyflat"/></div>
			  <div class="portlet-content">
			  	<div style="display:none;" class="tempresult"></div>
			  	<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
						 <c:choose>
							 <c:when test="${bean.sqlReturnStatus == 1}">
								<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
							</c:when>
							<c:when test="${bean.sqlReturnStatus != 1}">
								<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
							</c:when>
						</c:choose>
					</c:if>    
				</div>	 
				<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none"></div>
				<form id="propertyFlatEdit">
					<fieldset>
						<div class="width50 float-right" id="hrm"> 
								<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
								<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
								<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
								<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
								<input type="hidden" id="sessionpersonId" value="${bean.sessionpersonId}"/>
			 				<fieldset>
								<legend><fmt:message key="re.property.info.propertyflat2"/></legend>						
		                            <div>
		                            	<label><fmt:message key="re.property.info.fromdate"/></label>
		                            	<input type="text" name="startPicker" class="width30" id="startPicker" disabled="disabled" value="${bean.fromDate}">
		                            </div>		
									<div>
										<label><fmt:message key="re.property.info.todate"/></label>
										<input type="text" name="endPicker" class="width30" id="endPicker" disabled="disabled" value="${bean.toDate }">
									</div>																
									<div>
										<label><fmt:message key="re.property.info.deposit"/></label>
										<input type="text" name="deposit" class="width30" id="deposit" disabled="disabled" value="${bean.deposit }">
									</div>
									<div>
										<label><fmt:message key="re.property.info.rent"/><fmt:message key="re.propertyinfo.peryear"/></label>
										<input type="text" name="rent" class="width30" id="rent" value="${bean.rent }" disabled="disabled">
									</div>
									<div style="display:none;">
										<label><fmt:message key="re.property.info.rentstatus"/><span style="color:red">*</span></label>
										<input type="hidden" name="rentStatus" class="width30" id="temprentStatus" value="${bean.rentStatus}"/>
										<select id="rentStatus" class="width30" >
											<c:if test="${requestScope.lookupRentList ne null}">
												<c:forEach items="${requestScope.lookupRentList}" var="rentList" varStatus="status">
													<option value="${rentList.rentStatus }">${rentList.rentStatusName }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
									<div>
										<label><fmt:message key="re.property.info.totalarea"/></label>
										<input type="text" name="totalArea" class="width15" id="totalArea" disabled="disabled" value="${bean.totalArea}"/>
										<input type="hidden" id="measurementCodeTemp" name="measurementCodeTemp" value="${bean.measurementCode}" class="width30">
										<select id="measurementCode" class="width15  validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanMeasurement ne null}">
											<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementList" varStatus="status">
												<option value="${beanMeasurementList.measurementCode }">${beanMeasurementList.measurementValue }</option>
											</c:forEach>
											</c:if>
										</select>
									</div>
									<input type="hidden" id="URNdms" value="${bean.image }" disabled="disabled"/>
									<div class="clearfix"></div>
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
										 <div  id="dms_document_information" class="portlet-header ui-widget-header float-right"><fmt:message key="cs.common.button.viewanddownload"/></div>
						             </div>	

							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset style="height:207px;">
								<legend><fmt:message key="re.property.info.propertyflat1"/></legend>
								<div style="display:none;"><label>Id No.</label><input type="hidden" name="propertyFlatId" class="width30" disabled="disabled" id="propertyFlatId" value="${bean.propertyFlatId}"/></div>
								<div>
									<label><fmt:message key="re.property.info.buildingname"/></label>
									<input type="hidden" name="tempbuildingId" class="width30" id="tempbuildingId" value="${bean.buildingId}"/>
									<select id="buildingId" class="width30" disabled="disabled" TABINDEX=1>
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemList ne null}">
											<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
												<option value="${building.buildingId }">${building.buildingName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.floorno"/></label>
									<input type="text" name="floorNo" class="width30" id="floorNo" disabled="disabled" value="${bean.floorNo}" TABINDEX=2/>		
								</div>		
								<div>
								<label>
									<fmt:message key="re.property.info.componenttype"/></label>
									<input type="hidden" name="componentType" class="width30" id="tempcomponentType" value="${bean.componentType}"/>
									<select id="componentType" class="width30" TABINDEX=3 disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.lookupList ne null}">
											<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
												<option value="${compType.componentType }">${compType.componentTypeName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.flatnumber"/></label>
									<input type="text" name="flatNumber" class="width30" id="flatNumber" disabled="disabled" value="${bean.flatNumber}" TABINDEX=4/>
								</div>
								<div id="noOfRoomsDiv">
									<label><fmt:message key="re.property.info.noofrooms"/></label>
									<input type="hidden" name="noOfRoomsTemp" class="width30" id="noOfRoomsTemp" value="${bean.noOfRooms}"/>
									<select name="noOfRooms" class="width30" id="noOfRooms" disabled="disabled" TABINDEX=5>
										<option value="">Select</option>
										<c:forEach items="${beanRoom}" var="rooms" varStatus="status">
											<option value="${rooms.noOfRooms}">${rooms.noOfRooms }</option>
										</c:forEach>
									</select>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.status"/></label>
									<input type="hidden" name="flatStatus" class="width30" id="tempflatStatus" value="${bean.flatStatus }"/>
									<select id="flatStatus" class="width30" disabled="disabled" TABINDEX=6>
										<option value="">-Select-</option>
										<c:if test="${requestScope.lookupFlatList ne null}">
											<c:forEach items="${requestScope.lookupFlatList}" var="flatList" varStatus="status">
												<option value="${flatList.flatStatus }">${flatList.flatStatusName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>		
							 </fieldset> 
						</div>
				</fieldset>
				<div id="main-content"> 
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuredetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.featurecode"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.features"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail1"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail2"/></th>
			            				</tr>
									</thead>  
									<tbody class="tab" style="">	
										<c:forEach items="${requestScope.result1}" var="result" >
											 <tr class="rowid"> 
												<td class="width10">${result.lineNumber }</td>	 
												<td class="width20">${result.featureCode }</td>
												<td class="width20">${result.features }</td>	
												<td class="width10">${result.measurements }</td> 
												<td class="width10">${result.remarks }</td>
												<td style="display:none"> 
													<input type="text" value="${result.propertyInfoFeatureId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style="display:none;"></td>	
												<td style="display:none;"></td>	 
											</tr>  
										</c:forEach>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
			</div>
		</div>
		<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				   <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.assetdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize1}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.brand"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.modelnumber"/></th>
				            				</tr>
										</thead>  
										<tbody class="tabasset" style="">
											<c:forEach items="${requestScope.assetList}" var="result" >
											 	<tr class="assetid">  	 
													<td class="width20">${result.lineNumber}</td>
													<td class="width20">${result.maintainenceCode}</td>	
													<td class="width20 amountcalc">${result.brand}</td> 
													<td class="width20">${result.modelNumber}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.propertyFlatAssetId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	</c:forEach>	
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="clearfix"></div>
								 <div class="width50 float-right" style="margin:5px;" id="hrm">
									<fieldset>
										<legend>Supervisor Comments<span style="color:red">*</span></legend>
										<textarea id="comments" class="width100 validate[required]"></textarea>
									</fieldset>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</div>
					</div>
				</form>
			</div>
		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			<div class="portlet-header ui-widget-header float-right"  id="close"><fmt:message key="cs.common.button.close"/></div>
			<div class="portlet-header ui-widget-header float-right" id="reject"><fmt:message key="cs.sendingfax.button.reject"/></div>
			<div class="portlet-header ui-widget-header float-right" id="approve"><fmt:message key="cs.sendingfax.button.approve"/></div>
		</div>				
	</div>
</div>

