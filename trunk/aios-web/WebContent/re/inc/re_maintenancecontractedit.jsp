<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var openFlag=0;
var actualID;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){  
	$('#maintenanceTypeCode').val($('#maintenanceTypeCodeTemp').val().trim());
	$('#supplierId').val($('#supplierIdTemp').val());
	$('#purchaseOrderId').val($('#purchaseOrderIdTemp').val());

	var amcAmountTemp=Number($('#amcAmount').val());
	$('.installmentAmountCalc').each(function(){ 		// Calculation for Balance Amount in Payment TAB
		if($(this).html() != null || $(this).html() != ''){
			balance=amcAmountTemp-Number($(this).html());
			$($(this).siblings().get(2)).html(balance);
			amcAmountTemp=balance;
		}
	});

	approvedStatus=$('#approvedStatus').val();
	if( approvedStatus == 'E'){
		$('#request').remove();	
		$('#discard').remove();	
		$('#save').remove();
		$('#close').fadeIn();
		$('.childCountErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#request').fadeIn();	
		$('#discard').fadeIn();	
		$('#save').fadeIn();
		$('.childCountErr').hide().html("Workflow is Approved, You can visit and send the request.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#request').fadeIn();	
		$('#discard').fadeIn();	
		$('#save').fadeIn();
		$('.childCountErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
	}
	

	var dmsUserId='${emp_CODE}';
	var dmsPersonId='${PERSON_ID}';
	var companyId=Number($('#headerCompanyId').val());
	var applicationId=Number($('#headerApplicationId').val());
	var functionId=Number($('#headerCategoryId').val());
	var functionType="list,upload";//edit,delete,list,upload
	//$('#dmstabs-1, #dmstabs-2').tabs();
	//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
	$('#dms_create_document').click(function(){
		var dmsURN=$('#URNdms').val();
		loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
	});
	$('#dms_document_information').click(function(){
		var dmsURN=$('#URNdms').val();
		loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
	});
	
	$('.formError').remove(); 
	 $("#MaintainenceContractEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	//Save Maintenance Contract
	$("#save").click(function(){
		if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			var childCountPayment=$('#childCountPayment').val();
			if(childCount!="" && childCount > 0 && childCountPayment!="" && childCountPayment > 0 ){
				maintenanceId = $('#maintenanceId').val();
				purchaseOrderId = $('#purchaseOrderId').val();
				amcAmount=$('#amcAmount').val();
				securityDeposit=$('#securityDeposit').val();
				startPicker=$('#startPicker').val();
				endPicker=$('#endPicker').val();
				contractDate=$('#contractDate').val();
				contractId=$('#contractId').val();
				contractNumber=$('#contractNumber').val();
				maintenanceTypeCode=$('#maintenanceTypeCode').val().trim();
				supplierId=$('#supplierId').val();
				addressLine1=$('#addressLine1').val();
				addressLine2=$('#addressLine2').val();
				maintenanceSubType=$('#maintenanceSubType').val();
				var headerFlag=$("#headerFlag").val();
				dmsURN=$('#URNdms').val();

				//Work Flow Variables
				sessionPersonId = $('#sessionPersonId').val();
				companyId=Number($('#headerCompanyId').val());
				applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Not Yet Approved";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_final_update_maintainencecontract.action",
						data: {maintenanceId:maintenanceId,purchaseOrderId: purchaseOrderId, amcAmount: amcAmount,securityDeposit: securityDeposit,
							fromDate:startPicker,toDate:endPicker,contractDate: contractDate,contractId: contractNumber,maintenanceTypeCode: maintenanceTypeCode,
							supplierId: supplierId,addressLine1: addressLine1,addressLine2: addressLine2,maintenanceSubType: maintenanceSubType,dmsURN: dmsURN,
							trnValue: transURN,headerFlag:headerFlag,companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							// Scroll to top of the page
							$.scrollTo(0,300);
						} 
					});
					$('#loading').fadeOut(); 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Child").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Request Maintenance Contract
	$("#request").click(function(){
		if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			var childCountPayment=$('#childCountPayment').val();
			if(childCount!="" && childCount > 0 && childCountPayment!="" && childCountPayment > 0 ){
				maintenanceId = $('#maintenanceId').val();
				purchaseOrderId = $('#purchaseOrderId').val();
				amcAmount=$('#amcAmount').val();
				securityDeposit=$('#securityDeposit').val();
				startPicker=$('#startPicker').val();
				endPicker=$('#endPicker').val();
				contractDate=$('#contractDate').val();
				contractId=$('#contractId').val();
				contractNumber=$('#contractNumber').val();
				maintenanceTypeCode=$('#maintenanceTypeCode').val().trim();
				supplierId=$('#supplierId').val();
				addressLine1=$('#addressLine1').val();
				addressLine2=$('#addressLine2').val();
				maintenanceSubType=$('#maintenanceSubType').val();
				var headerFlag=$("#headerFlag").val();
				dmsURN=$('#URNdms').val();

				//Work Flow Variables
				sessionPersonId = $('#sessionPersonId').val();
				headerLedgerId = $('#headerLedgerId').val();
				companyId=Number($('#headerCompanyId').val());
				applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Request For Verification";
				wfCategoryId=Number($('#headerCategoryId').val());
				functionId=Number($('#headerCategoryId').val());
				functionType="request";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/maintenance_contract_update_request.action",
						data: {maintenanceId:maintenanceId,purchaseOrderId: purchaseOrderId, amcAmount: amcAmount,securityDeposit: securityDeposit,
							fromDate:startPicker,toDate:endPicker,contractDate: contractDate,contractId: contractNumber,maintenanceTypeCode: maintenanceTypeCode,
							supplierId: supplierId,addressLine1: addressLine1,addressLine2: addressLine2,maintenanceSubType: maintenanceSubType,dmsURN: dmsURN,
							trnValue: transURN,headerFlag:headerFlag,
							companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus,wfCategoryId: wfCategoryId,
							functionId: functionId,functionType: functionType,sessionPersonId: sessionPersonId,headerLedgerId: headerLedgerId},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.formError').hide();
								$('.commonErr').hide();
								$('.childCountErr').hide();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#request').remove();	
									$('#discard').remove();	
									$('#save').remove();
									$('#close').fadeIn();
									$('.commonErr').hide();
								} else{
									$('.tempresultfinal').fadeIn();
								}
								// Scroll to top of the page
								$.scrollTo(0,300);
						} 
					});
					$('#loading').fadeOut(); 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Child").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Asset Details
 	$('.addAsset').click(function(){ 
 		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_contract_asset_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
						
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(11)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(11)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(11)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}	
	});

 	//Adding Payment Details
 	$('.addPayment').click(function(){ 
 		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_maintenance_payment_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
						
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}	
	});

 	//Edit Asset Details
	$(".editAsset").click(function(){
		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        var buildingId= Number($($(slidetab).children().get(12)).text());	// Building ID
				var componentType= Number($($(slidetab).children().get(14)).text());	// Component Id
				//actualID=$($($(slidetab).children().get(8)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_contract_asset_redirect.action",  
				 	async: false,
				 	data: {buildingId: buildingId, componentType: componentType},
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(11)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(11)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(11)).children().get(3)).show();	//Processing Button
						
					 
			         var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp13= $($(slidetab).children().get(3)).text();
					 var temp4= $($(slidetab).children().get(4)).text();
					 var temp5= $($(slidetab).children().get(5)).text();
					 var temp6= $($(slidetab).children().get(6)).text();
					 var temp7= $($(slidetab).children().get(7)).text();
					 var temp8= $($(slidetab).children().get(8)).text();
					 var temp9= $($(slidetab).children().get(9)).text();
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
					 
					var temp10= $($(slidetab).children().get(12)).text();	// Building ID
					var temp11= $($(slidetab).children().get(13)).text();	// Asset Id
					var temp12= $($(slidetab).children().get(14)).text();	// Component Id
					var temp14= $($(slidetab).children().get(15)).text();	// Flat Id
	
					
					//alert("temp11:"+temp11+ "temp5:"+temp5+);	  
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp10);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp12);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(4)).children().get(1)).val(temp14);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(5)).children().get(1)).val(temp4);
					
					$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(2)).val(temp5);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp6);	
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp7);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(4)).children().get(1)).val(temp8);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(5)).children().get(1)).val(temp9);
				  	
				  	openFlag=1;
					$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}	
	});

	//Edit Payment Details
	$(".editPayment").click(function(){
		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_maintenance_payment_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
						$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(2)).val(temp3);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}	
	});


	$('.delAsset').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(10)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(10)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			  var maintenanceId=$('#maintenanceId').val(); 
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_contract_asset.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, maintenanceId: maintenanceId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}	
 	});	


	$('.delPayment').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			  var maintenanceId=$('#maintenanceId').val(); 
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_contract_payement.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, maintenanceId: maintenanceId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCountPayment=Number($('#childCountPayment').val());
		       		 if(childCountPayment > 0){
		       			childCountPayment=childCountPayment-1;
						$('#childCountPayment').val(childCountPayment);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}	
 	});	

	//Adding Payment Details
 	$('.addVisit').click(function(){ 
 		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_maintenance_visit_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
						
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}	
	});

 	//Edit Payment Details
	$(".editVisit").click(function(){
		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_maintenance_visit_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}	
	});

	$('.delVisit').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			  var maintenanceId=$('#maintenanceId').val(); 
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_contract_visit.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, maintenanceId: maintenanceId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCountPayment=Number($('#childCountPayment').val());
		       		 if(childCountPayment > 0){
		       			childCountPayment=childCountPayment-1;
						$('#childCountPayment').val(childCountPayment);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}	
 	});	

 	
 	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();  
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_discard_maintainencecontract.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 	});
	
	
	$("#close").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_contract_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	}); 
	
	$('.contractno-popup').click(function(){ 
	       tempid=$(this).parent().get(0);  
			$('#contract-popup').dialog('open');
			//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/maintenance_contract_edit_load.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.contract-result').html(result); 
				},
				error:function(result){ 
					 $('.contract-result').html(result); 
				}
			}); 
		});

	$('.purchaseorder-popup').click(function(){ 	// Load Purchase Order Details
	       tempid=$(this).parent().get(0);  
			$('#contract-popup').dialog('open');
			//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/maintenance_contract_purchase_load.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.contract-result').html(result); 
				},
				error:function(result){ 
					 $('.contract-result').html(result); 
				}
			}); 
		});
	 
	$('#contract-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		width: '40%',
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});

	$('.addassetrows').click(function(){
 		var count=Number(0);
		$('.rowid').each(function(){
			count=count+1;
			
		});  
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/asset_addrow.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tabAsset").append(result);
			if($(".tabAsset").height()<255)
				 $(".tabAsset").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabAsset").height()>255)
				 $(".tabAsset").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
	});
});
 	$('.addpayementrows').click(function(){
 		var count=Number(0);
		$('.asset').each(function(){
			count=count+1;
		});  
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/maintenance_contract_payement_edit_addrow.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tabPayment").append(result);
			if($(".tabPayment").height()<255)
				 $(".tabPayment").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabPayment").height()>255)
				 $(".tabPayment").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
	});
});

 	$('.addvisitrows').click(function(){
 		var count=Number(0);
		$('.visit').each(function(){
			count=count+1;
			
		});  
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/visit_editrow.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tabVisit").append(result);
			if($(".tabVisit").height()<255)
				 $(".tabVisit").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabVisit").height()>255)
				 $(".tabVisit").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
			
		});	
	});

	$('#contractDate').datepick();
	
	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.maintenancecontract"/></div>
		<div class="portlet-content">
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 	  
	 		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
	 		<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 
			<form id="MaintainenceContractEdit">
				<fieldset>	
					<div class="width50 float-right" id="hrm"> 
		 				<fieldset>
							<legend><fmt:message key="re.contract.info"/></legend>	
								 <div>
								 <label><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>
								 	<input type="text" name="contractDate" class="width30" id="contractDate" readonly="readonly" value="${bean.contractDate}"/>
								 </div>				
	                            <div>
	                            <input type="hidden" id="maintenanceId" value="${bean.maintenanceId}"/>
									<label><fmt:message key="re.contract.contractno"/><span style="color:red">*</span></label>
									<input type="hidden" name="contractId" id="contractId" class="width50" value="${bean.contractId}"/>
									<input type="text" name="contractNumber" id="contractNumber" class="width30 validate[required,custom[onlyNumber]]" value="${bean.contractId}"/>
									<span id="hit7" class="button" style="width: 40px ! important;display:none;">
										<a class="btn ui-state-default ui-corner-all contractno-popup"> 
											<span class="ui-icon ui-icon-newwin"> </span> 
										</a>
									</span>
								</div>	
	                         	<div>
	                         		<label><fmt:message key="re.contract.maintenancetype"/></label>
	                         		<input type="hidden" name="maintenanceTypeCodeTemp" id="maintenanceTypeCodeTemp" value="${bean.maintenanceTypeCode}"/>
									<select id="maintenanceTypeCode" class="width30 validate[required]">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.maintenanceTypeList}" var="typelist" varStatus="status">
											<option value="${typelist.maintenanceTypeCode}">${typelist.maintenanceTypeName}</option>
										</c:forEach>
									</select>
								</div>	
								<div>
	                            	<label>Maintenance SubType</label>
	                            	<input type="text" name="maintenanceSubType" class="width30" value="${bean.maintenanceSubType }" id="maintenanceSubType">
	                            </div>										
								<div>
									<label><fmt:message key="re.contract.suppliervendorname"/></label>
									<input type="hidden" name="supplierIdTemp" id="supplierIdTemp" value="${bean.supplierId }"/>
									<input type="hidden" name="supplierId" class="width30" id="supplierId" value="${bean.supplierId }">
									<input type="text" name="supplierName" class="width30" id="supplierName" value="${bean.supplierName }" disabled="disabled">
								</div>	
								 <div>
	                            	<label><fmt:message key="re.contract.addressline1"/></label>
	                            	<input type="text" name="addressLine1" class="width30" disabled="disabled" id="addressLine1" value="${bean.addressLine1}"/>
	                            </div>		
	                            <div>
	                            	<label><fmt:message key="re.contract.addressline2"/></label>
	                            	<input type="text" name="addressLine2" class="width30" disabled="disabled" id="addressLine2" value="${bean.addressLine2 }"/>
	                            </div>										
								<div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								<div id="UploadDmsDiv" class="width100">
									<input type="hidden" id="URNdms" value="${bean.dmsURN}" disabled="disabled" class="width30">
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			                            <div  id="dms_document_information" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
			                        </div>
								</div>
				    		</div>	
						</fieldset>  																		
					</div> 
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="re.contract.maintenanceinfo"/></legend>
							<div style="display:none;">
								<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40" id="sessionPersonId" disabled="disabled">
								<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
							</div>
							<div>
								<label><fmt:message key="re.contract.purchaseorderno"/><span style="color:red">*</span></label>
								<input type="hidden" name="purchaseOrderIdTemp" id="purchaseOrderIdTemp" value="${bean.purchaseOrderId }"/>
								<input type="hidden" name="purchaseOrderId" id="purchaseOrderId" class="width30 validate[required]">
								<input type="text" name="purchaseOrderNo" id="purchaseOrderNo" disabled="disabled" value="${bean.purchaseOrderNo }" class="width30 validate[required]">
								<span id="hit7" class="button" style="width: 40px ! important;">
									<a class="btn ui-state-default ui-corner-all purchaseorder-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
							</div>	
							<div>
								<label><fmt:message key="re.contract.contractamount"/><span style="color:red">*</span></label>
								<input type="text" name="amcAmount" id="amcAmount" class="width30 validate[required,custom[onlyFloat]]" value="${bean.amcAmount}"/>
							</div>
							<div>
								<label><fmt:message key="re.contract.securitydeposit"/></label>
								<input type="text" name="securityDeposit" id="securityDeposit" class="width30 validate[optional,custom[onlyFloat]]" value="${bean.securityDeposit}"/>
							</div>						
						</fieldset> 
						<fieldset>
							<legend><fmt:message key="re.contract.contractperiod"/></legend>
							<div>
								<label class="width30"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
								<input type="text" name="fromDate" id="startPicker" class="width30 validate[required]" readonly="readonly" value="${bean.fromDate }"/>
							</div>		
						    <div>
						    	<label class="width30"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
						    	<input type="text" name="toDate" id="endPicker" class="width30 validate[required]" readonly="readonly" value="${bean.toDate }"/>
						    </div>
						</fieldset>
					</div>
				</fieldset>	
			</form>
			<div class="clearfix"></div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.assetinformation"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th style=" width:5%;"><fmt:message key="re.contract.lineno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.buildingno"/></th>
				            					<th style=" width:5%;"><fmt:message key="re.property.info.componenttype"/></th>
				            					<th style=" width:5%;"><fmt:message key="re.property.info.flatno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.additionaldetails"/></th>
				            					<th class="width10"><fmt:message key="re.contract.assetno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.assetname"/></th>
				            					<th class="width10"><fmt:message key="re.contract.brand"/></th>
				            					<th class="width10"><fmt:message key="re.contract.modelno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.age"/></th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabAsset" style="">	
											<c:forEach items="${requestScope.result1}" var="result" >
												<tr class="rowid">  
													<td style=" width:5%;">${result.lineNumber}</td>	 
													<td class="width10">${result.buildingName}</td>
													<td style=" width:5%;">${result.componentTypeName}</td>
													<td style=" width:5%;">${result.flatNumber}</td>
													<td class="width10">${result.additionalDetails}</td>
													<td class="width10">${result.assetNo}</td> 
													<td class="width10">${result.assetName}</td>
													<td class="width10">${result.brand}</td>
													<td class="width10">${result.modelNo}</td>
													<td class="width10"><c:if test="${result.age ne 0}">${result.age}</c:if><c:if test="${result.age eq 0}"></c:if></td>
													<td style="display:none"> 
														<input type="hidden" value="${result.maintainenceAssetId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAsset" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAsset"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAsset" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="building_id">${result.buildingId}</td>	
													<td style="display:none;" class="asset_id">${result.assetId}</td>
													<td style="display:none;" class="componant_id">${result.buildingComponantId}</td>
													<td style="display:none;" class="flat_id">${result.flatId}</td>		 
												</tr>  
											</c:forEach>
										 <c:forEach var="i" begin="${countSize+1}" end="${countSize+2}">
											 	<tr class="rowid">  
													<td style=" width:5%;">${i}</td>	 
													<td class="width10"></td>
													<td style=" width:5%;"></td>
													<td style="width:5%;"></td>
													<td class="width10"></td>
													<td class="width10"></td> 
													<td class="width10"></td>
													<td class="width10"></td>
													<td class="width10"></td>
													<td class="width10"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>
													</td>  
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAsset" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAsset"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAsset" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="building_id"></td>	
													<td style="display:none;" class="asset_id"></td>
													<td style="display:none;" class="componant_id"></td>
													<td style="display:none;" class="flat_id"></td>		 
												</tr>  
									  		</c:forEach>
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addassetrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.paymentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCountPayment" id="childCountPayment"  value="${countSize2}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr >  
				            					<th class="width10"><fmt:message key="re.contract.lineno"/></th>
				            					<th class="width10"><fmt:message key="re.contract.date"/></th>
				            					<th class="width10"><fmt:message key="re.contract.installmentamount"/></th>
				            					<th class="width10"><fmt:message key="re.contract.balanceamount"/></th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabPayment" style="">
										<c:forEach items="${requestScope.result2}" var="result2" >
												<tr class="asset">  
													<td class="width10">${result2.lineNumber}</td>	 
													<td class="width20">${result2.date }</td>
													<td class="width20 installmentAmountCalc">${result2.instalmentAmount }</td>	
													<td class="width10 balanceAmountCalc"></td> 
													<td style="display:none"> 
														<input type="hidden" value="${result2.maintainencePayementId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>	
													<td style="display:none;"></td>	 
												</tr>  
											</c:forEach>	
											<c:forEach var="i" begin="${countSize2+1}" end="${countSize2+2}">
												 <tr class="asset">  
													<td class="width10">${i}</td>	 
													<td class="width20"></td>
													<td class="width20 balanceAmountCalc"></td>	
													<td class="width10 balanceAmountCalc"></td> 
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>		
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>		 
												</tr>  
									  		</c:forEach>
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<fieldset>
						<div class="float-right width45" style="display:none;">
							<label class="width30"><fmt:message key="re.contract.total"/></label><input type="text" name="sno" class="width30 tooltip" id="total">
						</div>
					</fieldset>
							
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addpayementrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Visit Details</div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<div style="display:none;" class="tempresult"></div>
			 				<input type="hidden" name="childCountVisit" id="childCountVisit"  value="${countSize3}"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th class="width10">Visit Number</th>
				            					<th class="width10">Person Name</th>
				            					<th class="width10">Purpose</th>
				            					<th class="width10">Remarks</th>
				            					<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabVisit" style="">
										<c:forEach items="${requestScope.result3}" var="result3" >
												<tr class="visit">  
													<td class="width10">${result3.visitNo}</td>	 
													<td class="width20">${result3.personName }</td>
													<td class="width20">${result3.purpose }</td>	
													<td class="width10">${result3.remarks }</td> 
													<td style="display:none"> 
														<input type="hidden" value="${result3.visitId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addVisit" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editVisit"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delVisit" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>	
													<td style="display:none;"></td>	 
												</tr>  
											</c:forEach>	
											<c:forEach var="i" begin="${countSize3+1}" end="${countSize3+2}">
												 <tr class="visit">  
													<td class="width10">${i}</td>	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width10"></td> 
													<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>		
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addVisit" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editVisit"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delVisit" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>		 
												</tr>  
									  		</c:forEach>
										</tbody>
			 						</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
					    	</div>
						</div> 
					</form>
					<fieldset>
						<div class="float-right width45" style="display:none;">
							<label class="width30"><fmt:message key="re.contract.total"/></label><input type="text" name="sno" class="width30 tooltip" id="total">
						</div>
					</fieldset>
							
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addvisitrows" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>		
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
				<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
				<div class="portlet-header ui-widget-header float-right " id="discard" style=" cursor:pointer;"><fmt:message key="re.contract.cancel"/></div>
				<div class="portlet-header ui-widget-header float-right" id="request" style="cursor:pointer;"><fmt:message key="cs.common.button.requestforverification"/></div> 
				<div class="portlet-header ui-widget-header float-right " id="save" style="cursor:pointer;"><fmt:message key="re.owner.info.update"/></div>
			</div>
		</div>
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="contract-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="contract-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>




