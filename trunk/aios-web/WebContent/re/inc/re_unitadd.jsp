<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style type="text/css">
.options span {
	width: 10%;
}
.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:200px;}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
td input{ 
	border: 0px;
}

</style> 
<script type="text/javascript">
var actualID;
var featureRowAddedUnit = 0;
var unitRent; var slidetab="";
$(function (){  
	manupulateLastRow();
	
	// On select process
	 //$('#componentId').val($('#componentIdTemp').val());
	 $('#unitTypeId').val($('#unitTypeTemp').val()); 
	 
	 var tempvar=$('.tabFeature>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#DeleteImage_'+rowid).hide(); 

	 $('#assetRadio0').click(function () {
		 $('#assetDiv').show();
	 });
	 
	 $('#assetRadio1').click(function () {
		 $('#assetDiv').show();
	 });

	 $('#assetRadio2').click(function () {
		 $('#assetDiv').hide();
	 });

	 $('#custom').click(function () {
		 $('#checkboxs').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#checkboxs').find('input[type=checkbox]:disabled').removeAttr('disabled');
		 $('#bedroom_count').text("");
		 $('#hall_size').text("");
		 $('#bathroom_count').text("");
		 $('#kitchen_size').text("");
	 });

	 $('#custom').attr('checked', true);
	 $('#checkboxs').find('input[type=checkbox]:disabled').removeAttr('disabled');
	 
	 $('.type_radio').change(function () {	

		if($(this).attr("id") == "vip") {

			$('#checkboxs').find('input[type=checkbox]').attr('checked', false);
			$('#bedroom').attr('checked', true);
			$('#bedroom_count').text('2');
			$('#hall').attr('checked', true);
			$('#hall_size').text('Small');
			$('#tempHallSize').val(1);
			$('#bathroom').attr('checked', true);
			$('#bathroom_count').text('1 - 1/2');
			$('#kitchen').attr('checked', true);
			$('#kitchen_size').text('Medium');
			$('#tempKitchenSize').val(2);
			$('#servent_room').attr('checked', true);
			$('#garage').attr('checked', true);
			$('#guest_room').attr('checked', true);
		}

		if($(this).attr("id") == "luxury") {

			$('#checkboxs').find('input[type=checkbox]').attr('checked', false);
			$('#bedroom').attr('checked', true);
			$('#bedroom_count').text('3');
			$('#hall').attr('checked', true);
			$('#hall_size').text('Medium');
			$('#tempHallSize').val(2);
			$('#bathroom').attr('checked', true);
			$('#bathroom_count').text('2');
			$('#kitchen').attr('checked', true);
			$('#kitchen_size').text('Medium');
			$('#tempKitchenSize').val(2);
			$('#servent_room').attr('checked', true);
			$('#garage').attr('checked', true);
			$('#driver_room').attr('checked', true);
			$('#front_garden').attr('checked', true);
			$('#guest_room').attr('checked', true);
		}

		if($(this).attr("id") == "deluxe") {

			$('#checkboxs').find('input[type=checkbox]').attr('checked', false);
			$('#bedroom').attr('checked', true);
			$('#bedroom_count').text('4');
			$('#hall').attr('checked', true);
			$('#hall_size').text('Large');
			$('#tempHallSize').val(3);
			$('#bathroom').attr('checked', true);
			$('#bathroom_count').text('3');
			$('#kitchen').attr('checked', true);
			$('#kitchen_size').text('Large');
			$('#tempKitchenSize').val(3);
			$('#servent_room').attr('checked', true);
			$('#garage').attr('checked', true);
			$('#driver_room').attr('checked', true);
			$('#front_garden').attr('checked', true);
			$('#back_garden').attr('checked', true);
			$('#guest_room').attr('checked', true);
		}

		if($(this).attr("id") == "presi") {

			$('#checkboxs').find('input[type=checkbox]').attr('checked', false);
			$('#bedroom').attr('checked', true);
			$('#bedroom_size').text('5');
			$('#hall').attr('checked', true);
			$('#hall_size').text('Large');
			$('#bathroom').attr('checked', true);
			$('#bathroom_size').text('3');
			$('#kitchen').attr('checked', true);
			$('#kitchen_size').text('Large');
			$('#servent_room').attr('checked', true);
			$('#garage').attr('checked', true);
			$('#driver_room').attr('checked', true);
			$('#front_garden').attr('checked', true);
			$('#back_garden').attr('checked', true);
			$('#guest_room').attr('checked', true);
			$('#podeo').attr('checked', true);
			$('#swimming_pool').attr('checked', true);
			$('#in_pantry').attr('checked', true);
			$('#kennel').attr('checked', true);
			$('#tennis_yard').attr('checked', true);
		}
	 	 
		if($(this).attr("id") != "custom") {

			$('#checkboxs').find('input[type=checkbox]').attr('disabled', true);
		}
	 });

	$('.checkbox_options').click( function() {

		if($(this).attr("id") == 'bedroom' && $('#bedroom').attr('checked') == true) {

			 $( "#bedroom_p" ).show();
			 $( "#bathroom_p" ).hide();
			 $( "#hall_p" ).hide();
			 $( "#kitchen_p" ).hide();
		 }

		else if($(this).attr("id") == 'bathroom' && $('#bathroom').attr('checked') == true){

			 $( "#bedroom_p" ).hide();
			 $( "#bathroom_p" ).show();
			 $( "#hall_p" ).hide();
			 $( "#kitchen_p" ).hide();
		 }

		else if($(this).attr("id") == 'hall' && $('#hall').attr('checked') == true) { 

			 $( "#bedroom_p" ).hide();
			 $( "#bathroom_p" ).hide();
			 $( "#hall_p" ).show();
			 $( "#kitchen_p" ).hide();
		 }

		else if($(this).attr("id") == 'kitchen' && $('#kitchen').attr('checked') == true) {

			 $( "#bedroom_p" ).hide();
			 $( "#bathroom_p" ).hide();
			 $( "#hall_p" ).hide();
			 $( "#kitchen_p" ).show();
		 }

		else {

			$( "#bedroom_p" ).hide();
			$( "#bathroom_p" ).hide();
			$( "#hall_p" ).hide();
			$( "#kitchen_p" ).hide();
		}
	});
	 
	$('#checkboxs').find('input[type=checkbox]:disabled').removeAttr('disabled');

	//$('.unit_options').live('onmouseout', function () {  
	//	$(this).hide();
	//});
	 
	 $('#componentId').focus();
	 
	 if($('#unitStatusTemp').val() != null && $('#unitStatusTemp').val() != '') {
		 
		 if($('#unitStatusTemp').val() == 3) {
			 $('#unitStatus').append('<option value="3">Rented</option>');
		 } else if($('#unitStatusTemp').val() == 4) {
			 $('#unitStatus').append('<option value="4">Sold</option>');
		 } else if($('#unitStatusTemp').val() == 6) {
			 $('#unitStatus').append('<option value="6">Offered</option>');
		 }
		 $('#unitStatusTemp').attr("disabled", true);
	 }
	 
	 if($('#unitStatusTemp').val()>0)
	 	$('#unitStatus').val($('#unitStatusTemp').val());
	 else
		$('#unitStatus').val(1); 

	unitRent = convertToAmount("${UNITINFO.rent}");
	$("#rent").val(unitRent);

	var getRefPropertyType = function (type) {

		if(type == 4) {
			return 1;			
		} else if(type == 3) {
			return 6;			
		} else if(type == 2) {
			return 7;			
		} else {
			return 1;
		}
	};

	var getRefUnitType = function (type) {

		if(type == 1) {
			return 4;			
		} else if(type == 6) {
			return 3;			
		} else if(type == 7) {
			return 2;			
		} else {
			return 4;
		}
	};
	
	if($('#unitTypeTemp').val() == 4 || $('#unitTypeTemp').val() == 3 || $('#unitTypeTemp').val() == 2) {
		$('#propertyName').attr('disabled', true);
		$('#componentName').attr('disabled', true);
		$('#componentListButton').hide();
		$('#propertyListButton').hide();

		$('#unitTypeId_Session').val(getRefPropertyType($('#unitTypeTemp').val()));
		$('#unitTypeId_Session').show();
		$('#unitTypeId').removeClass("validate[required]");
		$('#unitTypeId').hide();
		$('#rent_div').hide();	
	}
	
	if($('#newComponentId').val() != ""){
		
		$('#propertyName').val($('#newComponentId').val());
		$('#componentName').val($('#newComponentId').val());

		$('#unitTypeId_Session').val($('#newPropertyTypeId').val());
		$('#unitTypeId_Session').show();
		$('#unitTypeId').removeClass("validate[required]");
		$('#unitTypeId').hide();
		$('#rent_div').hide();		
		$('#propertyName').attr('disabled', true);
		$('#componentName').attr('disabled', true);
		$('#componentListButton').hide();
		$('#propertyListButton').hide();
		$('#unitId').val(Number(0));
		$('#componentId').val(Number(0));
	}

	$( "#slider_bedroom" ).slider({
		value:0,
		min: 1,
		max: 9,
		step: 1,
		orientation: "vertical",
		slide: function( event, ui ) {
			$( "#bedroom_count" ).text( ui.value );
		}
	});
	$( "#bedroom_count" ).text( "$" + $( "#slider" ).slider( "value" ) );

	var convertText = function (text_) {

		if(text_ == "1.5") 
			return "1 - 1/2";
		else if(text_ == "2.5")
			return "2 - 1/2";
		else if(text_ == "3.5")
			return "3 - 1/2";
		else if(text_ == "4.5")
			return "4 - 1/2";
		else if(text_ == "5.5")
			return "5 - 1/2";
		else
			return text_;
	};
	
	$( "#slider_bathroom" ).slider({
		value:1,
		min: 1,
		max: 6,
		step: 0.5,
		orientation: "vertical",
		slide: function( event, ui ) {
			$( "#bathroom_count" ).text( convertText (ui.value) );
		}
	});
	$( "#bathroom_count" ).text( "$" + $( "#slider" ).slider( "value" ) );

	$( "#slider_hall" ).slider({
		value:0,
		min: 1,
		max: 3,
		step: 1,
		orientation: "vertical",
		slide: function( event, ui ) {
			if(ui.value == 1) {
				$( "#tempHallSize" ).val(1);
				$( "#hall_size" ).text("Small");
			}
			else if(ui.value == 2) {
				$( "#tempHallSize" ).val(2);
				$( "#hall_size" ).text("Medium");
			}
			else if(ui.value == 3) {
				$( "#tempHallSize" ).val(3);
				$( "#hall_size" ).text("Large");
			}
		}
	});
	$( "#hall_size" ).text( "$" + $( "#slider" ).slider( "value" ) );

	$( "#slider_kitchen" ).slider({
		value:0,
		min: 1,
		max: 3,
		step: 1,
		orientation: "vertical",
		slide: function( event, ui ) {
			if(ui.value == 1) {
				$( "#tempKitchenSize" ).val(1);
				$( "#kitchen_size" ).text("Small");
			}
			else if(ui.value == 2) {
				$( "#tempKitchenSize" ).val(2);
				$( "#kitchen_size" ).text("Medium");
			}
			else if(ui.value == 3) {
				$( "#tempKitchenSize" ).val(3);
				$( "#kitchen_size" ).text("Large");
			}
		}
	});
	$( "#kitchen_size" ).text( "$" + $( "#slider" ).slider( "value" ) );

	var getText = function (value_) {

		if(value_ == "1") 
			return "Small";
		else if(value_ == "2")
			return "Medium";
		else if(value_ == "3")
			return "Large";
		else
			return "";
	};
	
	var doCheck = function () {

		$('#bedroom').attr('checked', ("${UNITINFO.bedroom}" == 0) ? false : true);
		$('#bedroom_count').text(("${UNITINFO.bedroom}" == 0) ? "" : "${UNITINFO.bedroomSize}");
		$('#bathroom').attr('checked', ("${UNITINFO.bathroom}" == 0) ? false : true);
		$('#bathroom_count').text(("${UNITINFO.bathroom}" == 0) ? "" : "${UNITINFO.bathroomSize}");
		$('#hall').attr('checked', ("${UNITINFO.hall}" == 0) ? false : true);
		$('#hall_size').text(("${UNITINFO.hall}" == 0) ? "" : getText("${UNITINFO.hallSize}"));
		$('#kitchen').attr('checked', ("${UNITINFO.kitchen}" == 0) ? false : true);
		$('#kitchen_size').text(("${UNITINFO.kitchen}" == 0) ? "" : getText("${UNITINFO.kitchenSize}"));
		$('#servent_room').attr('checked', ("${UNITINFO.serventRoom}" == 0) ? false : true);
		$('#driver_room').attr('checked', ("${UNITINFO.driverRoom}" == 0) ? false : true);
		$('#front_yard').attr('checked', ("${UNITINFO.frontYard}" == 0) ? false : true);
		$('#back_yard').attr('checked', ("${UNITINFO.backYard}" == 0) ? false : true);
		$('#front_garden').attr('checked', ("${UNITINFO.frontGarden}" == 0) ? false : true);
		$('#back_garden').attr('checked', ("${UNITINFO.backGarden}" == 0) ? false : true);
		$('#swimming_pool').attr('checked', ("${UNITINFO.swimmingPool}" == 0) ? false : true);
		$('#in_pantry').attr('checked', ("${UNITINFO.inPantry}" == 0) ? false : true);
		$('#out_pantry').attr('checked', ("${UNITINFO.outPantry}" == 0) ? false : true); 
		$('#podeo').attr('checked', ("${UNITINFO.podeo}" == 0) ? false : true);
		$('#fountain').attr('checked', ("${UNITINFO.fountain}" == 0) ? false : true);
		$('#guest_room').attr('checked', ("${UNITINFO.guestRoom}" == 0) ? false : true);
		$('#security_room').attr('checked', ("${UNITINFO.securityRoom}" == 0) ? false : true);
		$('#kennel').attr('checked', ("${UNITINFO.kennel}" == 0) ? false : true);
		$('#tennis_yard').attr('checked', ("${UNITINFO.tennisYard}" == 0) ? false : true);
	};

	doCheck();
	 
	$('.formError').remove(); 
	
	 $jquery("#propertyComponentAdd").validationEngine('attach');

	 $('#unitSizeCode').val($('#tempunitSize').val());	

	 $('.property-popup').click(function(){ 
	       tempid=$(this).parent().get(0);   
		   $('.ui-dialog-titlebar').remove();
		   var propertyId=Number($("#propertyId").val());
		   var urlaction=$($(tempid).siblings().get(1)).attr('id').trim().toLowerCase();  
		   if(urlaction=="componentname")
			   urlaction="get"+urlaction;
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_component_"+urlaction+".action", 
				data: {propertyId:propertyId},  
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.property-result').html(result);  
				},
				error:function(result){  
					 $('.property-result').html(result); 
				}
			});  
	 });
		
	 $('#component-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:500, 
			bgiframe: false,
			overflow:'hidden',
			modal: true 
	});  
	  
	//Save Property Component details
	$("#save").click(function(){ 
		$("#result-div").html("");
		if($jquery('#propertyComponentAdd').validationEngine('validate')){
				var productLines=new Array();
				var assetLinesDesc=new Array();
				var assetIds=new Array();
				var productId=""; 
				var assetDescription="";
				var unitAssetDetails="";
				$('.assetrowid').each(function(){
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					productId=$('#productid_'+rowid).val();
					assetDescription=$('#assetDescription_'+rowid).val();
					var assetId=$('#assetlineid_'+rowid).val();
					if(assetId==null || assetId=='' || assetId==0 || assetId=='undefined')
						assetId=-1;
					
					if(typeof productId != 'undefined'
								&& productId!=''){  
						productLines.push(productId);
						assetLinesDesc.push(assetDescription);
						assetIds.push(assetId);
					} 
				}); 
				for(var j=0;j<productLines.length;j++){ 
					unitAssetDetails+=productLines[j]+"__"+assetLinesDesc[j]+"__"+assetIds[j];
					if(j==productLines.length-1){   
					} 
					else{
						unitAssetDetails+="#@";
					}
				} 
				unitId = Number($('#unitId').val());
				componentId=$('#componentId').val();
				if($('#newComponentId').val() != "") {
					unitTypeId = getRefUnitType($('#newPropertyTypeId').val());
				} else if($('#unitTypeTemp').val() == 4 || $('#unitTypeTemp').val() == 3 || $('#unitTypeTemp').val() == 2) {
					unitTypeId = $('#unitTypeTemp').val();
				}
				else {
					unitTypeId=$('#unitTypeId').val();
				}
				unitNumber=Number($('#unitNumber').val());
				unitName=$('#unitName').val();
				var	unitNameArabic=$('#unitNameArabic').val();
				rooms=Number($('#rooms').val());
				rent=$('#rent').val();
				status=Number($('#unitStatus').val());
				meterNo = $('#meterNo').val();
				size = Number($('#unitSize').val());
				sizeUnit = Number($('#unitSizeCode').val());
				bedroom = ($('#bedroom').attr('checked') == true) ? 1 : 0;				
				bedroomSize = $('#bedroom_count').text();
				bathroom = ($('#bathroom').attr('checked') == true) ? 1 : 0;
				bathroomSize =  $('#bathroom_count').text();
				hall = ($('#hall').attr('checked') == true) ? 1 : 0;
				hallSize =  $('#tempHallSize').val();
				kitchen = ($('#kitchen').attr('checked') == true) ? 1 : 0;
				kitchenSize = $('#tempKitchenSize').val();
				serventRoom = ($('#servent_room').attr('checked') == true) ? 1 : 0;
				driverRoom = ($('#driver_room').attr('checked') == true) ? 1 : 0;
				frontYard = ($('#front_yard').attr('checked') == true) ? 1 : 0;
				backYard = ($('#back_yard').attr('checked') == true) ? 1 : 0;
				frontGarden = ($('#front_garden').attr('checked') == true) ? 1 : 0;
				backGarden = ($('#back_garden').attr('checked') == true) ? 1 : 0;
				garage = ($('#garage').attr('checked') == true) ? 1 : 0;
				swimmingPool = ($('#swimming_pool').attr('checked') == true) ? 1 : 0;
				inPantry = ($('#in_pantry').attr('checked') == true) ? 1 : 0;
				outPantry = ($('#out_pantry').attr('checked') == true) ? 1 : 0;
				podeo = ($('#podeo').attr('checked') == true) ? 1 : 0;
				fountain = ($('#fountain').attr('checked') == true) ? 1 : 0;
				guestRoom = ($('#guest_room').attr('checked') == true) ? 1 : 0;
				securityRoom = ($('#security_room').attr('checked') == true) ? 1 : 0;
				kennel = ($('#kennel').attr('checked') == true) ? 1 : 0;
				tennisYard = ($('#tennis_yard').attr('checked') == true) ? 1 : 0;
				var furnished = $('input:radio[name=asset]:checked').val(); 
				if(furnished==3){
					unitAssetDetails="";
				}
				$('#loading').fadeIn();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/unit_add_final_save.action",
					data: {unitId:unitId,componentId:componentId,unitTypeId:unitTypeId,unitNumber:unitNumber,
							unitName:unitName,unitNameArabic:unitNameArabic,rooms:rooms,rent:rent,status:status, meterNo:meterNo, size:size, sizeUnit:sizeUnit, 
							unitAssetDetails: unitAssetDetails, furnished:furnished,
							bedroom: bedroom, bedroomSize:bedroomSize,bathroom:bathroom,bathroomSize:bathroomSize,hall:hall,
							hallSize:hallSize,kitchen:kitchen,kitchenSize:kitchenSize,serventRoom:serventRoom,driverRoom:driverRoom,
							frontYard:frontYard,backYard:backYard,frontGarden:frontGarden,backGarden:backGarden,garage:garage,
							swimmingPool:swimmingPool,inPantry:inPantry,outPantry:outPantry,podeo:podeo,fountain:fountain,
							guestRoom:guestRoom,securityRoom:securityRoom,kennel:kennel,tennisYard:tennisYard			
						  },  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result){ 
						$("#result-div").html(result);
						var successMessage=$("#unit-success").html().trim();
						if(successMessage!=null && successMessage!=''){
							if(($('#unitId').val() == 0) && ($('#propertyName').attr('disabled') != true)) {
								var cnfrm = confirm('Unit saved. Add another unit?');
								if(!cnfrm) {
									$("#main-wrapper").html(result);	
								}
								else {
									$('#unitName').val('');
									$('#unitNumber').val('');
								}
							} else {
								$("#main-wrapper").html(result);
							}
						}else{
							$('#unit-error-add').html("Transaction goes failure..").fadeIn();
							
						}
						$('#loading').fadeOut(); 
					},
					error:function(result){
						$("#main-wrapper").html(result);	
					}
				});				
			 } else {
				return false; 
			} 
		return false; 
	});
	
	 $('#feature-type-add').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         $('#common-popup').dialog('open');
         var accessCode="UNIT_FEATURE"; 
            $.ajax({
               type:"POST",
               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
               data:{accessCode:accessCode},
               async: false,
               dataType: "html",
               cache: false,
               success:function(result){
                    $('.common-result').html(result);
                    return false;
               },
               error:function(result){
                    $('.common-result').html(result);
               }
           });
            return false;
   	}); 
	 
   $('#common-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:600,
		height:350,
		bgiframe: false,
		modal: true 
	});

	 $(".addData").click(function(){ 
			$('#warningMsg').hide();
			if($('#openFlag').val() == 0) {
			slidetab=$(this).parent().parent().get(0);  
				
				//Find the Id for fetch the value from Id
				var tempvar=$(this).parent().attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				if($jquery("#editCalendarVal").validationEngine('validate')){
						$("#AddImage_"+rowid).hide();
						$("#EditImage_"+rowid).hide();
						$("#DeleteImage_"+rowid).hide();
						$("#WorkingImage_"+rowid).show(); 
						
						$.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/unit_feature_add_get.action",
							data:{id:rowid,addEditFlag:"A"},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
							 $(result).insertAfter(slidetab);
							 $('#openFlag').val(1);
							}
					});
					
				}
				else{
					return false;
				}	
		 }else{
			return false;
		 }
		return false;
	});
		
	$(".editData").click(function(){
		$('#warningMsg').hide();
		if($jquery('#propertyInfoAdd').validationEngine('validate')){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
			//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]);
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).hide();
			$("#DeleteImage_"+rowid).hide();
			$("#WorkingImage_"+rowid).show(); 
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/unit_feature_update.action",  
				data:{id:rowid,addEditFlag:"E"},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					 //Bussiness parameter
					$(result).insertAfter(slidetab);
					var featureCode=$('#featureCode_'+rowid).text();
					var featureUnitDetail=$('#featureUnitDetails_'+rowid).text();
		   			var featureDetail= $('#featureDetails_'+rowid).text();
		   			var lookupDetailId= $('#lookupDetailId_'+rowid).val();

		   			$('#unitDetails').val(featureDetail);
		   			$('#unitFeatures').val(featureCode);
		   			$('#unitFeatureDetails').val(featureUnitDetail);
		   			$('#lookupDetailId').val(lookupDetailId);
				    $('#openFlag').val(1);
				    $('#loading').fadeOut();
				}
			});
		}else{
			$('#loading').fadeOut();
			return false;
		}	
		return false;
	});
   
 	$('.delrow').click(function(){
 		//Find the Id for fetch the value from Id
		var tempvar=$(this).attr('id');
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
	//	var lineId=$('#lineId_'+rowid).val();
		var flag=false;
		var featureCode=$('#featureCode_'+rowid).text(); 
		if(featureCode!=null && featureCode!=''){
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/unit_feature_delete.action", 
			 	async: false,
			 	data:{addEditFlag:"D",id:rowid},
			    dataType: "html",
			    cache: false,
				success:function(result){	
					$('.tempresult').html(result); 
					flag=true;
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                 return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
	
	       		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){  
						i=i+1;
						$($(this).children().get(0)).text(i); 
					 });  
			   }
		}
		else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
		return false;
 	});

 	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();
 		var targetAction;
 		if($('#newComponentId').val() != "")
 			targetAction = "property_info_list";
 		else
 			targetAction = "unit_master_redirect";
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/" + targetAction + ".action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 		return false;
 	});

	// Add Rows for Feature
	$('.addrowsfeature').click(function(){
		var i=0;
	 	var lastRowId=$('.tabFeature>tr:last').attr('id');
	 	$('#warningMsg').hide();
		if(lastRowId!=null){
		var idarray = lastRowId.split('_');
		var rowid=Number(idarray[1]);
		rowid=Number(rowid+1);
		}else{
			rowid=Number(1);
		}
		//alert(rowid);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/unit_feature_addrow.action", 
			data:{id:rowid},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tabFeature").append(result);
				//if($(".tabFeature").height()<255)
					 //$(".tabFeature").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabFeature").height()>255)
					 $(".tabFeature").css({"overflow-x":"hidden","overflow-y":"auto"});


				//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){  
						i=i+1;
						$($(this).children().get(0)).text(i); 
					 });
	     			 var tempvar=$('.tabFeature>tr:last').attr('id'); 
					 var idval=tempvar.split("_");
					 var rowid=Number(idval[1]); 
					 $('#DeleteImage_'+rowid).show(); 
			}
		});
		return false;
	});

	    if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
	 	$('#startPicker,#endPicker').datepick({
	 		onSelect: customRange, showTrigger: '#calImg'
		}); 
	 	
	 	$(".assetdelrow").live('click', function () {   
			 slidetab=$(this).parent().parent().get(0); 
 
			//Find the Id for fetch the value from Id
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var lineId=$('#lineId_'+rowid).text();
			 var uom=$('#uom_'+rowid).text().trim();
			 if(uom!=null && uom!=""){
				 var flag=true;  
				 if(flag==true){ 
	        		 var childCount=Number($('#assetchildCount').val());
	        		 if(childCount > 0){
						childCount=childCount-1;
						$('#assetchildCount').val(childCount); 
	        		 } 
	        		 $(slidetab).remove(); 
	        		//To reset sequence 
	        		var i=0;
	     			$('.assetrowid').each(function(){  
						i=i+1; 
   					 });  
				}
			 }
			 else{
				 $('#warning_message').hide().html("Empty/Last Asset information can't delete...").slideDown(1000);
				 return false;
			 } 
			 return false;
		 });
	 	
	 	$('.assetaddrows').click(function(){ 
			  var i=Number(0); 
			  var id=Number(0);
			  $('.assetrowid').each(function(){
				  var temparray=$(this).attr('id');
				  var idarray=temparray.split('_');
				  var rowid=Number(idarray[1]);  
				  id=Number($('#lineId_'+rowid).text());  
				  id+=1;
			  }); 
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/unit_asset_addrow.action", 
				 	async: false,
				 	data:{rowId: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.asset_tab tr:last').before(result);
						 if($(".asset_tab").height()>255)
							 $(".asset_tab").css({"overflow-x":"hidden","overflow-y":"auto"});
						 i=0;
						 $('.assetrowid').each(function(){
								i=i+1;
						}); 
					}
				});
			  return false;
		 });
	 	
	 	$(".autocomplete").each(function(){
			 slidetab=$(this).attr('id'); 
			 var idarray =slidetab.split('_');
		  	 var rowid=Number(idarray[1]); 
		  	 $('#productName_'+rowid+' option').each(function(){  
		 	  	 var tempProdName=$('#tempProdName_'+rowid).val();
		 	  	 var tempProductText=$(this).val();  
		 	  	 var myarray=tempProductText.split('@@');
		 	  	 var tempProductSub1=myarray[0];
		 	  	 var tempProductSub2=myarray[1];
		 	  	 var tempProductId=$('#productid_'+rowid).val();  
		 	  	 var tempproduct=tempProductSub1+"@@"+tempProductSub2; 
				 if(tempProductSub1==tempProdName){  
					 console.debug(tempProductSub1+" "+tempProdName);
					 console.debug(tempproduct);
					$('#productName_'+rowid+' option[value='+tempproduct+']').attr("selected","selected");
					$('#productCode_'+rowid+' option[value='+tempProductId+']').attr("selected","selected");
					$('#uom_'+rowid).text(tempProductSub2);
				 }
		  	}); 
		 }); 
	 	
	 	$('.autocompletecode').combobox({ 
			 selected: function(event, ui){ 
				 var elVal=$(this).val(); 
				 slidetab=$(this).attr('id'); 
			  	 var idarray =slidetab.split('_');
			  	 var rowid=Number(idarray[1]);  
			  	 var nexttab=slidetab=$(this).parent().parent().get(0);  
			  	 nexttab=$(nexttab).next(); 
			  	 $('#productName_'+rowid+' option').each(function(){
			 		 var productId=$(this).val(); 
			 		 var splitval=productId.split("@@");
			 		 productId=splitval[0]; 
			 		 if(elVal==productId){
			 			var productName=$(this).text();  
			 			$('.productnameclass_'+rowid).combobox('autocomplete', productName); 
			 			$('#uom_'+rowid).text(splitval[1]);
			 			$('#productid_'+rowid).val(productId);
			 		 }
			 	 }); 
			  	var unitVal=$('#uom_'+rowid).text();
			  	triggerAddRow(unitVal,nexttab,rowid); 
	         } 
		 });
	 	
	 	$(".autocomplete").combobox({ 
	        selected: function(event, ui){
	 	      	var productVal=$(this).val();
	 	      	proArray=productVal.split('@@');
	 	        var unitName=proArray[1];
	 	        slidetab=$(this).attr('id'); 
	 	  		var idarray =slidetab.split('_');
	 	  		var rowid=Number(idarray[1]);  
	 	  		$('#uom_'+rowid).text(unitName); 
	 	  		var nexttab=slidetab=$(this).parent().parent().get(0);  
	 		  	nexttab=$(nexttab).next();
	 	  		triggerAddRow(unitName,nexttab,rowid);  
	 	  		//change product code
	 	  		var productName=$(this).val(); 
	 	  		$('#productCode_'+rowid+' option').each(function(){
	 		 		 var productId=$(this).val(); 
	 		 		 var splitval=productName.split("@@");
	 		 		 productName=splitval[0]; 
	 		 		 if(productId==productName){
	 		 			productName=$(this).text();   
	 		 			$('.productcodeclass_'+rowid).combobox('autocomplete', productName);
	 		 			$('#productid_'+rowid).val(productId);
	 		 		 }
	 		 	 });
	        }
	     }); 
	 	
	 	if($('#assetRadio2').attr('checked')==true){ 
			 $('#assetDiv').hide();
		}
		else{
			$('#assetDiv').show();
		}

		$("#bedroom_p").mouseleave(function(){ 
			$(this).hide();
		});
		$("#bathroom_p").mouseleave(function(){ 
			$(this).hide();
		});
		$("#hall_p").mouseleave(function(){ 
			$(this).hide();
		});
		$("#kitchen_p").mouseleave(function(){ 
			$(this).hide();
		});
});
function triggerAddRow(unitVal,nexttab,rowid){    
	 $('#DeleteAImage_'+rowid).show();
	 if(unitVal!="" && $(nexttab).hasClass('lastrow')){
		 $('.assetaddrows').trigger('click');
	 }  
	return false;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".asset_tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".asset_tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('asset_tab>tr:last').removeAttr('id');  
	$('asset_tab>tr:last').removeClass('assetrowid'); 
	$('asset_tab>tr:last').addClass('lastrow');  
	$($('asset_tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('asset_tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" >
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.unitInfo" /></div>
		 <div id="result-div" style="display:none;"></div>
		 <div class="error response-msg ui-corner-all" id="unit-error-add" style="display:none;"></div>
		 <div class="error response-msg ui-corner-all" id="return-error-message" style="display:none;">${requestScope.RETURN_ERROR_MESSAGE}</div>
		 <c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		<form id="propertyComponentAdd" style="width:98% !important; margin-left: 18px;position: relative;">
			<fieldset>
			<div class="form_head"><fmt:message key="re.property.info.unitDetails" /></div>  
				<div class="width48 float-left" id="hrm">
					<fieldset style="min-height: 150px;">	
										
							<input type="hidden"  name="unitId" id="unitId" value="${UNITINFO.unitId}" class="width30"/>
								
							<div>
								<label> 
								<fmt:message key="re.property.info.propertyName" /><span style="color: red;">*</span></label>
								<input name="propertyName" type="text" id="propertyName" value="${UNITINFO.propertyName}" class="validate[required] width40">
								<input type="hidden" id="propertyId" value="${UNITINFO.propertyId}"/>
								<span class="button" id="propertyListButton">
									<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all property-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div> 
								
							<div><label><fmt:message key="re.property.info.componentName" /><span style="color:red">*</span></label>
							<!--<select id="componentId" tabindex="1" class="width50 validate[required]" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.COMPONENTLIST ne null}">
										<c:forEach items="${requestScope.COMPONENTLIST}" var="compo" varStatus="status">
											<option value="${compo.componentId }">${compo.componentName }</option>
										</c:forEach>
									</c:if>
								</select>
							-->
								<input name="componentName" type="text" id="componentName" value="${UNITINFO.componentName}" class="validate[required] width40">
								<input type="hidden" id="componentId" value="${UNITINFO.componentId}"/>
								<span class="button" id="componentListButton">
									<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all property-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div>	
							<div>
								<label><fmt:message key="re.property.info.unitType" /><span style="color:red">*</span></label>
								<input type="hidden" id="unitTypeTemp" value="${UNITINFO.unitTypeId}" class="width30" />
								<select id="unitTypeId" tabindex="2" class="validate[required]" style="width: 41%;">
									<option value="">-Select-</option>
									<c:if test="${requestScope.UNITYPELIST ne null}">
										<c:forEach items="${requestScope.UNITYPELIST}" var="unitype" varStatus="status">											
											<option value="${unitype.unitTypeId }">${unitype.type }</option>												
										</c:forEach>
									</c:if>
								</select>
								<select id="unitTypeId_Session" disabled="disabled" tabindex="2" style="display:none; width: 41%;">
									<option value="1">Villa</option>
									<option value="6">Shop</option>
									<option value="7">Flat</option>										
								</select>
							</div>
							<div>
								<label><fmt:message key="re.property.info.unitNumber" /></label>
								<input type="text" id="unitNumber" tabindex="3" value="${UNITINFO.unitNumber}" class="width40 validate[optional,custom[integer]]" />
							</div>
							<div>
								<label><fmt:message key="re.property.info.unitName" /><span style="color:red">*</span></label>
								<input type="text" id="unitName" tabindex="3" value="${UNITINFO.unitName}" class="width40 validate[required]" />
							</div>
							<div>
								<label><fmt:message key="re.property.info.unitName" />(Arabic)</label>
								<input type="text" id="unitNameArabic" tabindex="3" value="${UNITINFO.unitNameArabic}" class="width40" />
							</div>
						</fieldset> 
					</div>
					<div class="width48 float-right" style="margin-right: 18px;" id="hrm">
					<fieldset style="min-height: 150px;">
							
							<div>
								<label><fmt:message key="re.property.info.meterNo" /></label>
								<input type="text" id="meterNo" tabindex="4" value="${UNITINFO.meterNo}" class="width40" />
							</div>	
							<div style="display: none;">
								<label><fmt:message key="re.property.info.rooms" /></label>
								<input type="text" id="rooms" tabindex="4" value="${UNITINFO.rooms}" class="width40" />
							</div>			
							<div id="rent_div">
								<label><fmt:message key="re.property.info.annualRent" /></label>
								<input type="text" id="rent" tabindex="5" class="width40" />
							</div>
							<div>
								<label><fmt:message key="re.property.info.unitStatus" /></label>
								<input type="hidden" id="unitStatusTemp"  value="${UNITINFO.status}" class="width50" />
								<select id="unitStatus" tabindex="6" class="validate[required]" style="width: 41%;">
									<option value="">-Select-</option>
									<option value="1"><fmt:message
										key="re.property.info.available" /></option>
									<option value="2"><fmt:message
										key="re.property.info.pending" /></option>
									<option value="5"><fmt:message
										key="re.property.info.renovation" /></option>
								</select>
							</div>	
							<div>
								<label><fmt:message
										key="re.property.info.unitSize" /></label> 
								<input type="text" id="unitSize" name="unitSize" value="${UNITINFO.size}" class="custom[number] width15 "/>
								<select name="unitSizeCode" id="unitSizeCode" style="width: 23.5%" class="">
									<option value="">--<fmt:message key="re.property.info.sizeUnit" />--</option>
									<option value="1">Square Feet</option>
									<option value="2">Square Meter</option>
									<option value="3">Square Yard</option>
								</select>
								<input type="hidden" id="tempunitSize" name="tempunitSize" style="display: none;" value="${UNITINFO.sizeUnit}"/> 						
							</div> 	
							<div class="width100">  
								<fieldset>
									<c:choose>
										<c:when test="${UNITINFO ne null && UNITINFO ne ''}"> 
											<c:if test="${UNITINFO.furnished eq 1}">
												<input id="assetRadio2" name="asset" type="radio" value="3" /> <fmt:message key="re.property.info.furnished" />
												<input id="assetRadio1" name="asset" type="radio" value="2" /> <fmt:message key="re.property.info.semiFurnished" />
												<input id="assetRadio0" checked="checked" name="asset" type="radio" value="1" style="padding-right: 10px;"/> <fmt:message key="re.property.info.furnished" />
											</c:if>
											<c:if test="${UNITINFO.furnished eq 2}">
												<input id="assetRadio2" name="asset" type="radio" value="3" /> <fmt:message key="re.property.info.UnFurnished" />
												<input id="assetRadio1" checked="checked" name="asset" type="radio" value="2" /> <fmt:message key="re.property.info.semiFurnished" />
												<input id="assetRadio0" name="asset" type="radio" value="1" style="padding-right: 10px;"/> <fmt:message key="re.property.info.furnished" />
											</c:if>
											<c:if test="${UNITINFO.furnished eq 3}">
												<input id="assetRadio2" checked="checked" name="asset" type="radio" value="3" /> <fmt:message key="re.property.info.UnFurnished" />
												<input id="assetRadio1" name="asset" type="radio" value="2" /> <fmt:message key="re.property.info.semiFurnished" />
												<input id="assetRadio0" name="asset" type="radio" value="1" style="padding-right: 10px;"/> <fmt:message key="re.property.info.furnished" />
											</c:if> 
										</c:when>
										<c:otherwise> 
											<input id="assetRadio2" name="asset" checked="checked" type="radio" value="3" /> <fmt:message key="re.property.info.UnFurnished" />
											<input id="assetRadio1" name="asset" type="radio" value="2" /> <fmt:message key="re.property.info.semiFurnished" />
											<input id="assetRadio0" name="asset" type="radio" value="1" style="padding-right: 10px;"/> <fmt:message key="re.property.info.furnished" />
										</c:otherwise>
									</c:choose> 
								</fieldset>
							</div>					
				 	</fieldset> 
				</div>
			</fieldset>
		</form>
		<div id="main-content" style="width:98% !important; margin-left: 10px; overflow: hidden;"> 
			
				<div align="center" style="margin: 10px;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<fieldset style="padding: 5px;">
					<fieldset class="options" style="padding: 10px; width: 70%; text-align: left; background-color: #F8F8F8; color: gray;">
						<div>
							<ul>
								<li class="width20 float-left"><input type="radio" value="1" name="type_radio" class="type_radio" id="vip"><fmt:message key="re.property.info.vip" /></li> 
								<li class="width20 float-left"><input type="radio" value="2" name="type_radio" class="type_radio" id="luxury"><fmt:message key="re.property.info.luxury" /></li> 
								<li class="width20 float-left"><input type="radio" value="3" name="type_radio" class="type_radio" id="deluxe"><fmt:message key="re.property.info.deluxe" /></li> 
								<li class="width20 float-left"><input type="radio" value="4" name="type_radio" class="type_radio" id="presi"><fmt:message key="re.property.info.presidential" /></li> 
								<li class="width20 float-left"><input type="radio" value="5" name="type_radio" class="type_radio" id="custom"><fmt:message key="re.property.info.custom" /></li>
							</ul>						
						</div> 
					</fieldset>
					<fieldset class="options" id="checkboxs" style="padding: 10px; width: 70%; text-align: left;">
						<div class="width20 float-left">
							<span><input type="checkbox" value="bedroom" id="bedroom" class="checkbox_options"><fmt:message key="re.property.info.bedrooms" /> <span id="bedroom_count" style="margin-left: 2px; font-weight: bolder;"></span></span> 
							
							<div id="bedroom_p" class="unit_options">
								<div id="slider_bedroom" style="float: left; margin-top: 3px;"></div>
								<div id="bedroom_count1" style="float: left;">
									<ul style="display: list-item; padding-left: 7px;">
										<li>9</li>
										<li>8</li>
										<li>7</li>
										<li>6</li>
										<li>5</li>
										<li>4</li>
										<li>3</li>
										<li>2</li>
										<li>1</li>								
									</ul>
								</div>
							</div>
						</div>
						
						<div class="width20 float-left">
							<span><input type="checkbox" value="bathroom" id="bathroom" class="checkbox_options"><fmt:message key="re.property.info.bathrooms" /> <span id="bathroom_count" style="margin-left: 2px; font-weight: bolder;"></span></span> 
							<input id="tempBathroomSize" value="0"  type="hidden"></input>
							<div id="bathroom_p" class="unit_options" style="width: 70px;">
								<div id="slider_bathroom" style="float: left; margin-top: 3px; height: 123px;"></div>							
								<div id="bathroom_count1" style="float: left;">
									<ul style="display: list-item; padding-left: 7px;">
										<li>6</li>
										<li>5 - 1/2</li>
										<li>5</li>
										<li>4 - 1/2</li>
										<li>4</li>
										<li>3 - 1/2</li>
										<li>3</li>
										<li>2 - 1/2</li>
										<li>2</li>
										<li>1 - 1/2</li>
										<li>1</li>								
									</ul>
								</div>
								
							</div>
							</div>
						<div class="width20 float-left">
							<span><input type="checkbox" value="hall" id="hall" class="checkbox_options"><fmt:message key="re.property.info.hall" /> <span id="hall_size" style="margin-left: 2px; font-weight: bolder;"></span></span> 
							<input id="tempHallSize" value="0"  type="hidden"></input>
							<div id="hall_p" class="unit_options" style="width: 80px;">
								<div id="slider_hall" style="float: left;"></div>
								<div id="hall_count1" style="float: left;">
								<ul style="display: list-item; padding-left: 7px;">
									<li style="margin-top: 2px;">Large</li>
									<li style="margin-top: 30px;">Medium</li>
									<li style="margin-top: 30px;">Small</li>							
								</ul>
							</div>
								
							</div>
						</div>
						
						<div class="width20 float-left">
						<span><input type="checkbox" value="kitchen" id="kitchen" class="checkbox_options"><fmt:message key="re.property.info.kitchen" /> <span id="kitchen_size" style="margin-left: 2px; font-weight: bolder;"></span></span> 
						<input id="tempKitchenSize" value="0" type="hidden"></input>
						<div id="kitchen_p" class="unit_options" style="width: 80px;">
							<div id="slider_kitchen" style="float: left;"></div>
							<div id="kitchen_count1" style="float: left;">
								<ul style="display: list-item; padding-left: 7px;">
									<li style="margin-top: 2px;">Large</li>
									<li style="margin-top: 30px;">Medium</li>
									<li style="margin-top: 30px;">Small</li>								
								</ul>
							</div>
						</div>
						</div>
						
						<div>									
							<ul>
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="servent_room" id="servent_room"><fmt:message key="re.property.info.serventRoom" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="driver_room" id="driver_room"><fmt:message key="re.property.info.driverRoom" /></li> 	
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="front_yard" id="front_yard"><fmt:message key="re.property.info.frontYard" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="back_yard" id="back_yard"><fmt:message key="re.property.info.backYard" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="front_garden" id="front_garden"><fmt:message key="re.property.info.frontGarden" /></li> 
							</ul>
						</div>
						<div>
							<ul>
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="back_garden" id="back_garden"><fmt:message key="re.property.info.backGarden" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="garage" id="garage"><fmt:message key="re.property.info.garage" /></li> 													
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="swimming_pool" id="swimming_pool"><fmt:message key="re.property.info.swimmingPool" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="in_pantry" id="in_pantry"><fmt:message key="re.property.info.inPantry" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="out_pantry" id="out_pantry"><fmt:message key="re.property.info.outPantry" /></li> 	
							</ul>
						</div>
						<div>
							<ul>
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="podeo" id="podeo"><fmt:message key="re.property.info.podeo" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="fountain" id="fountain"><fmt:message key="re.property.info.fountain" /></li> 							
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="guest_room" id="guest_room"><fmt:message key="re.property.info.guestRoom" /></li> 						
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="security_room" id="security_room"><fmt:message key="re.property.info.securityRoom" /></li> 
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="kennel" id="kennel"><fmt:message key="re.property.info.kennel" /></li>
							</ul>
						</div>
						<div>
							<ul>
								<li class="float-left width20"><input class="checkbox_options" type="checkbox" value="tennis_yard" id="tennis_yard"><fmt:message key="re.property.info.tennisYard" /></li> 
							</ul>	
						</div>				
					</fieldset>
				</fieldset>
			</div>
		
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="margin-left:10px; margin-right:10px;"> 
				<form name="newFields2">
 					<div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuresDetail" />
 						<!-- Add Feature Link -->
						<a id="feature-type-add" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" 
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "  
							title="Add Feature Type" style="float: right; 
													cursor: pointer; 
													left: 49px; 
													border-bottom-width: 1px; 
													margin-bottom: 0px; 
													margin-top: -5px; 
													height: 4px; 
													margin-right: 10px; 
													padding-top: 5px; 
													padding-bottom: 10px;"> 
							
							<span class="ui-icon ui-icon-plus" style="float: left;"></span>
							<label>Add Feature Type</label> 
						</a>
 					</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="0"/> 
		 				<input type="hidden" name="openFlag" id="openFlag"  value="0"/>  
		 				
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
											<th style="display: none;"  class="width10">Number</th>
			            					<th class="width20"><fmt:message key="re.property.info.featurecode" /></th>
			            					<th class="width20"><fmt:message key="re.property.info.features" /></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuresDetail" /></th>
			            					<th style="width:10%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tabFeature" style="">	
										 <c:forEach items="${requestScope.FEATURE_DETAILS}" var="result" varStatus="status"> 
										 <tr id="fieldrow_${status.index+1}" class="rowid">
										 	<td style="display: none;"  class="width10" id="lineId_${status.index+1}">${status.index+1}</td>        
										 	<td class="width20" id="featureCode_${status.index+1}">${result.unitFeatureCode}</td>
										 	<td class="width20" id="featureDetails_${status.index+1}">${result.details}</td>
										 	<td class="width20" id="featureUnitDetails_${status.index+1}">${result.unitFeatureDetails}</td>
											<td style="width:10%;"> 
						                		<input type="hidden" value="${result.unitDetailId}" name="unitDetailId" id="unitDetailId_${status.index+1}"/>
						                		<input type="hidden" name="lineId_${status.index+1}" id="lineId_${status.index+1}" value="${status.index+1}"/>  
						                		<input type="hidden" value="${result.lookupDetailId}" name="lookupDetailId" id="lookupDetailId_${status.index+1}"/>	
					                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
											 	 <span class="ui-icon ui-icon-plus"></span>
											   </a>
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											   </a> 
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											   </a>
											    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="WorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
													<span class="processing"></span>
											   </a>
											</td> 
											</tr>
										  </c:forEach>
	  									  <c:forEach var="i" begin="${fn:length(FEATURE_DETAILS)+1}" end="${fn:length(FEATURE_DETAILS)+2}" step="1" varStatus ="status">  
											 <tr id="fieldrow_${i}" class="rowid"> 
												<td style="display: none;" class="width20" id="lineId_${i}">${i}</td>        
									           		<td class="width20" id="featureCode_${i}"></td>
									           	<td class="width20" id="featureDetails_${i}"></td>
									           		<td class="width10" id="featureUnitDetails_${i}"></td>
										 		<td  style="width:10%;" id="option_${i}">
						                			<input type="hidden" value="" name="unitDetailId_${i}" id="unitDetailId_${i}"/>	
										 			<input type="hidden" name="lineId_${i}" id="lineId_${i}" value="${i}"/>  
										 			<input type="hidden" name="lookupDetailId" id="lookupDetailId_${i}"/>	
												  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}"  title="Add Record">
												 	 <span class="ui-icon ui-icon-plus"></span>
												   </a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  id="EditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${i}" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
														<span class="processing"></span>
													</a>
												</td> 
											</tr>
	 									</c:forEach>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				
				<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
			<!-- Asset Code -->
			<div id="main-content" class="width98" style="overflow: hidden; margin-right: 10px; margin-left: 10px; margin-top: 5px;"> 
			<div id="assetDiv" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">  
 					<div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset Details</div> 
		 			<div class="portlet-content"> 
		 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
						<div id="hrm" class="hastable width100"  > 
							 <input type="hidden" name="assetchildCount" id="assetchildCount" value="${fn:length(requestScope.ASSET_INFO)}"/>  
							 <table id="asset_hastab" class="width100"> 
								<thead>
								   <tr>  
								   		<th style="width:10%;">Asset Code</th> 
									    <th style="width:10%">Asset Name</th> 
									    <th style="width:3%">UOM</th> 
									    <th style="width:10%">Description</th> 
										<th>Options</th> 
								  </tr>
								</thead> 
								<tbody class="asset_tab"> 
								<c:choose>
									<c:when test="${requestScope.ASSET_INFO ne null && requestScope.ASSET_INFO ne '' && 
										fn:length(requestScope.ASSET_INFO)>0}">
											<c:forEach var="beans" items="${ASSET_INFO}" varStatus ="status">
												<tr class="assetrowid" id="fieldrow_${status.index+1}">
												<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
												<td id="productcode_${status.index+1}" style="width:2%;">
													<select id="productCode_${status.index+1}" class="autocompletecode 
															productcodeclass_${status.index+1}" name="productCode">
														<option value="">Select</option>
														<c:forEach items="${PRODUCT_INFO}" var="bean">
															<option value="${bean.productId}">${bean.code}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="tempProdCode_${status.index+1}" value="${beans.product.productId}"/>
												</td>
												<td id="productname_${status.index+1}">
													<select id="productName_${status.index+1}" class="autocomplete 
																productnameclass_${status.index+1}" name="productName">
														<option value="">Select</option>
														<c:forEach items="${PRODUCT_INFO}" var="bean">
															<option value="${bean.productId}@@${bean.lookupDetailByProductUnit.displayName}">${bean.productName}</option>
														</c:forEach>
													</select>
													<input type="hidden" id="tempProdName_${status.index+1}" value="${beans.product.productId}"/>
												</td>
												<td id="uom_${status.index+1}"></td> 
												<td id="assetdescription_${status.index+1}">
													<input type="text" id="assetDescription_${status.index+1}" style="width:97%!important;"
														value="${beans.detail}" name="assetDescription"/>
												</td> 
												<td style="display:none">
													<input type="hidden" name="assetlineid" id="assetlineid_${status.index+1}" value="${beans.assetId}"/>
													<input type="hidden" name="productid" id="productid_${status.index+1}" value="${beans.product.productId}"/> 
												</td> 
												 <td style="width:0.5%;" class="opn_td" id="option_${i}"> 
												 		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;display:none;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip assetdelrow" id="DeleteImage_${i}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a> 
												</td>   
											</tr>
										</c:forEach>
									</c:when> 
								</c:choose> 
								<c:forEach var="i" begin="${fn:length(requestScope.ASSET_INFO)+1}" end="${fn:length(requestScope.ASSET_INFO)+2}" step="1" varStatus ="status"> 
									<tr class="assetrowid" id="fieldrow_${i}">
										<td style="display:none;" id="lineId_${i}">${i}</td> 
										<td id="productcode_${i}" style="width:2%;">
											<select id="productCode_${i}" class="autocompletecode productcodeclass_${i}" name="productCode">
												<option value="">Select</option>
												<c:forEach items="${PRODUCT_INFO}" var="bean">
													<option value="${bean.productId}">${bean.code}</option>
												</c:forEach>
											</select>
										</td>
										<td id="productname_${i}">
											<select id="productName_${i}" class="autocomplete productnameclass_${i}" name="productName">
												<option value="">Select</option>
												<c:forEach items="${PRODUCT_INFO}" var="bean">
													<option value="${bean.productId}@@${bean.lookupDetailByProductUnit.displayName}">${bean.productName}</option>
												</c:forEach>
											</select>
										</td>
										<td id="uom_${i}"></td> 
										<td id="assetdescription_${i}">
											<input type="text" id="assetDescription_${i}" style="width:97%!important;" name="assetDescription"/>
										</td> 
										<td style="display:none">
											<input type="hidden" name="assetlineid" id="assetlineid_${i}"/>
											<input type="hidden" name="productid" id="productid_${i}"/> 
										</td> 
										 <td style="width:0.5%;" class="opn_td" id="option_${i}"> 
										 		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;display:none;" title="Add Record">
					 								<span class="ui-icon ui-icon-plus"></span>
											  </a>	
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											  </a> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip assetdelrow" id="DeleteAImage_${i}" style="cursor:pointer;display:none;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											  </a>
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
													<span class="processing"></span>
											  </a> 
										</td>   
									</tr>
								</c:forEach>
						 </tbody>
					</table>
				</div> 
			</div>    
		</div>
			
		<input id="newComponentId" type="hidden" value="${NEW_COMPONENT_FOR_UNIT.componentName}">
		<input id="newPropertyTypeId" type="hidden" value="${NEW_PROPERTY_FOR_UNIT.propertyType.propertyTypeId}">

		<div class="clearfix"></div>
 		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="component-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="property-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
		 
		<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left assetaddrows" style="cursor:pointer;">add row</div> 
		</div> 
		</div>	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
			<div class="portlet-header ui-widget-header float-right cancelrec" id="discard" style=" cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right headerData" id="save" style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
		</div>	
		
			<!-- Added for Lookup Information -->
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>		
	</div>
</div></div>