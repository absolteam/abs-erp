<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var slidetab;
var actualID;
$(function(){
	var curDate=new Date().toString();
	var splitDate=curDate.split(" ");
	var day=splitDate[2];
	var mon=splitDate[1];
	var year=splitDate[3];
	fromDate=day+"-"+mon+"-"+year;
	$('#defaultPopup').val(fromDate);
	
	//var myarray=new Array();
	
	
	$("#offerFormAdd").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	
	$('.formError').remove();
	//Save Material Despatch details
	$("#save").click(function(){
		if($('#offerFormAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var buildingId=$('#buildingId').val();
				var customerId=$('#customerId').val();
				var address= $('#address').val();
				var offerDate=$('#defaultPopup').val();
				var offerStatus=$('#offerStatus').val();
				var offerValidityDays=$('#offerValidityDays').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var extensionDays=$('#extensionDays').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/add_final_save_offer_form.action",
						data: {buildingId: buildingId, customerId: customerId,presentAddress: address,offerDate: offerDate,offerStatus:offerStatus,
							offerValidityDays: offerValidityDays,fromDate: fromDate,toDate: toDate,extensionDays: extensionDays, trnValue: transURN},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);
							$('#loading').fadeOut(); 
						} 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Feature Details
 	$('.addFeature').click(function(){ 
		if($('#offerFormAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	       var buildingId=$('#buildingId').val();
	        //alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_offer_form_redirect.action", 
				data: {buildingId: buildingId}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			$('#warningMsg').hide().html("Please insert record ").slideDown();
		}
	});
	
	$(".editFeature").click(function(){
		
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        var buildingId=$('#buildingId').val();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			if(actualID != null && actualID !='' && actualID!=undefined){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_offer_form_redirect.action",  
				data: {buildingId: buildingId}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();

				 var temp5= $($(slidetab).children().get(6)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			$('#warningMsg').hide().html("Please insert record ").slideDown();
		}
	});
   
 	$('.delFeature').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_offer_form_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
 	});

	$("#discard").click(function(){
		$('#loading').fadeIn();
		var trnValue=$("#transURN").val();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_discard_offer_form.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
   		});
	});
	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/offer_form_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	$('.customer-popup').click(function(){ 
	       tempid=$(this).parent().get(0);  
	       //var orderId=$('#orderId').val();
			$('#offerform-popup').dialog('open');
			//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/offer_form_load_customer.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.offerform-result').html(result); 
				},
				error:function(result){ 
					 $('.offerform-result').html(result); 
				}
			}); 
		});
	$('.addrowsfeature').click(function(){
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/offer_form_addrow.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
	});
});
	 
	$('#offerform-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});
	 
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Offer Form</div>
		<form id="flatMaintainenceReleaseAdd">
			<fieldset>
	 			<div class="portlet-content">
		 			<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:100px;">
							<div>
							   <label class="width30">Contract Number<span style="color:red">*</span></label>
						 	  <input class="taskStartDate width30" id="contractNumber" type="text" name="contractNumber" readonly="readonly"/>
						 	  <input class="width30 taskDueDate" id="contractId" type="hidden" name="contractId">
						 	</div>
						 	<!--  <div>
								<label>Contract Number</label>	
									<select id="flatNumber" class="width30 validate[required]" >
										<option value="">-Select-</option>
										<c:if test="${requestScope.item ne null}">
											<c:forEach items="${requestScope.item}" var="flat" varStatus="status">
												<option value="${flat.propertyFlatId }">${flat.flatNumber }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>-->
						 	<div>
							 	<label class="width30">Engineer Name</label>
								<input class="width30 taskDueDate" id="personName" type="text" name="personName">
							</div> 
							<div>
							 	<label class="width30">Description</label>
								<input class="width30 description" id="description" type="text" name="description">
							</div>
						</fieldset>
				 	</div>
	 			</div>
			</form>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Amount Details</div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20">Line Number</th>
				            					<th class="width20">Maintainence Code</th>
				            					<th class="width20">Amount</th>
				            					<th class="width20">Release Type</th>
				            					<th style="width:5%;">Options</th>
											</tr>
										</thead>  
										<tbody class="tab" style="">	
										 	<%for(int i=0;i<=2;i++) { %>
											 	<tr>  	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="flat_id"></td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	<%}%>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;">Add Row</div> 
					</div>
				</div>
			</div>
		 	<div class="float-right buttons ui-widget-content ui-corner-all">
		 			<div class="portlet-header ui-widget-header float-right discard" style="display:none;">Contract</div> 
					<div class="portlet-header ui-widget-header float-right discard" id="discard" ><fmt:message key="common.button.cancel"/></div> 
					<div class="portlet-header ui-widget-header float-right save-all" id="save"><fmt:message key="common.button.save"/></div> 
			</div>
		</div>
 	</fieldset>	
 </div>


	 	
<!--  <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="offerform-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="offerform-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>	 	
-->	 	
	 	
	 	
	 	
	 	
	 	
	 	
	 	
	 	
	 	
	 	