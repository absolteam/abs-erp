<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			property features
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="components" class="autoComplete_feature width50">
	     	<option value=""></option>
	     	<c:forEach items="${itemList}" var="bean" varStatus="status">
	     		<option value="${bean.featureId}^${bean.features}">${bean.featureCode}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 propertyfeature_list" style="padding:4px;"> 
     	<ul>
     		<li class="width48 float-left"><span>Feature Code</span></li> 
   			<li class="width48 float-left"><span>Features</span></li> 
   			<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
     		<c:forEach items="${itemList}" var="bean" varStatus="status">
	     		<li id="property_${status.index+1}" class="propertyfeaturelist_li" style="height: 20px;">
	     			<input type="hidden" value="${bean.featureId}" id="featureIdbyid_${status.index+1}">
	     			<input type="hidden" value="${bean.featureCode}" id="featureCodebyid_${status.index+1}">
	     			<input type="hidden" value="${bean.features}" id="featuresbyid_${status.index+1}">  
	     			<input type="hidden" value="${bean.lookupDetailId}" id="featureLookupId_${status.index+1}">
	     			<span id="featuresCodename_${status.index+1}" style="cursor: pointer;" class="float-left width48">${bean.featureCode} </span>   
	     			<span style="cursor: pointer;" class="float-left width48">${bean.features}
	     				<span id="featureselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
	     			</span>  
	     		</li>
	     	</c:forEach> 
     	</ul> 
	</div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="close_property" style="cursor:pointer;">close</div>  
	</div>	
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){  
	$($($('#property-featurepopup').parent()).get(0)).css('width',500);
	$($($('#property-featurepopup').parent()).get(0)).css('height',250);
	$($($('#property-featurepopup').parent()).get(0)).css('padding',0);
	$($($('#property-featurepopup').parent()).get(0)).css('left',0);
	$($($('#property-featurepopup').parent()).get(0)).css('top',100);
	$($($('#property-featurepopup').parent()).get(0)).css('overflow','hidden');
	$('#property-featurepopup').dialog('open');
	$('.autoComplete_feature').combobox({ 
	       selected: function(event, ui){ 
	    	   var featuredetails=$(this).val();
	    	   var featurarray=featuredetails.split('^');
	    	   
	    	 $('#featureId').val(featurarray[0]);
	  		 $('#features').val(featurarray[1]);
	  		 $('#featureCode').val($(".autoComplete_feature option:selected").text()); 
	  		 $('#property-featurepopup').dialog("close"); 
	   } 
	}); 
	$('.propertyfeaturelist_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.propertyfeaturelist_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#featureselect_'+rowid).hasClass('selected')){ 
				 $('#featureselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#featureselect_'+rowid).addClass('selected');
	});
	
	$('.propertyfeaturelist_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		// $('#featureId').val($('#featureIdbyid_'+rowid).val());
		 $('#featureType').val($('#featuresbyid_'+rowid).val());
		 $('#featureCode').val($('#featureCodebyid_'+rowid).val()); 
		 $('#lookupDetailId').val($('#featureLookupId_'+rowid).val());
		 
		 $('#property-featurepopup').dialog("close"); 
	});
	$('#close_property').live('click',function(){ 
		 $('#property-featurepopup').dialog("close"); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.propertyfeature_list ul li{
		padding:3px;
	}
	.propertyfeature_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>