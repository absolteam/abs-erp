<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;" id="featurechildRowId_${requestScope.rowid}">
<td colspan="4" class="tdfeature" id="featurechildRowId_${requestScope.rowid}">
<div id="errMsg"></div>
<form name="propertyInfoFeatureAddEntry" id="propertyInfoFeatureAddEntry" style="position: relative;">	 
<input type="hidden" name="featureaddEditFlag" id="featureaddEditFlag"  value="${requestScope.AddEditFlag}"/>
<div class="width36 view float-left" style="height: 100px; padding-left: 20px;" id="hrm" >
	<fieldset>
		<legend>Selection</legend>
		<div style="display: none;">
			<label><fmt:message key="re.property.info.lineno"/><span class="mandatory">*</span></label>
			<input type="text" name="lineNo" id="lineNo" class="width50" disabled="disabled" />
			<input id="featureJSID" value="${requestScope.rowId}"></input>
		</div>
		<div>
			<label class="width30"><fmt:message key="re.property.info.featurecode" /><span class="mandatory">*</span></label>
			<input type="text" name="featureCode" id="featureCode" class="width45 validate[required]" disabled="disabled"/>
			<input type="hidden" name="featureId" id="featureId" class="width50" />
			<input type="hidden" name="lookupDetailId" id="lookupDetailId" class="width50" />
			<span class="button" style="width: 40px ! important; cursor: pointer;">
				<a class="btn ui-state-default ui-corner-all feature-popup"> 
					<span class="ui-icon ui-icon-newwin"> </span> 
				</a>
			</span>
		</div>
		<div>
			<label class="width30"><fmt:message key="re.property.info.features"/></label>
			<input name="featureType" id="featureType" class="width45" disabled="disabled">
		</div>
	</fieldset>
</div> 

<div class="width30 view float-left" id="hrm" style="height: 100px;">
	<fieldset>	
		<legend>Details</legend>				 
		<div>
			<label class="width30">Feature Details</label>
			<textarea  name="feature" id="feature" class="width50"></textarea>
		</div>
		<div style="display : none;">
			<label><fmt:message key="re.property.info.featuredetail2"/></label>
			<input type="text" name="remarks" id="remarks" class="width50">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	

<div class="width30 view float-left" id="hrm" style="height: 100px;">
	<fieldset>
		<legend>Uploads</legend>
		<div id="hrm" Style="margin-bottom: 10px;">
			<div id="UploadDmsDiv" class="width100">
				
				<div style="height: 80px;">									
					
					<div id="dms_image_information_feature" style="cursor: pointer; color: blue;">
						<u>Upload image here</u>
					</div>
					<div id="uploadedFeatureImages" style="padding-top: 10px; margin-top:3px; height: 30px; overflow: auto;"></div>
					
				</div>
									
			</div>
		</div>
	</fieldset>
</div> 
<div class="clearfix" style="padding-top: 10px; padding-right: 20px;"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel_feature"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right save_feature" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;"
	 class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable"
	 tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		
	<div id="property-featurepopup" class="ui-dialog-content ui-widget-content"
		style="height: 200px !important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
	</div>
</div>
</form>
	</td>  
	
</tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		
		 
		 $jquery("#propertyInfoFeatureAddEntry").validationEngine('attach');
		 
		 //Find the Id for fetch the value from Id
			var tempvar=$('.tdfeature').attr('id');
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 
		 //var tempObject=$(this);
		 var propertyDetailId=$('#propertyDetailId_'+rowid).val();
		 
		// for uploader
		if(propertyDetailId>0){
			populateUploadsPane("img","uploadedFeatureImages", "PropertyDetail", propertyDetailId);
			$('#dms_image_information_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedFeatureImages", "PropertyDetail", propertyDetailId, "uploadedFeatureImages");
			});
		}else{
			populateUploadsPane("img","uploadedFeatureImages", "PropertyDetail", "-1");
			$('#dms_image_information_feature').click(function(){
				AIOS_Uploader.openFileUploader("img","uploadedFeatureImages", "PropertyDetail", "-1", "uploadedFeatureImages");
			});
		}
		
		

		 $('.feature-popup').click(function(){ 
		       tempid=$(this).parent().get(0);   
		       $('.ui-dialog-titlebar').remove();   
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_info_feature_details.action",
				 	async: false,  
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			}); 
		 $('#property-featurepopup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:500, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
		 
		 
		 $('.cancel_feature').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdfeature').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#featureaddEditFlag').val()!=null && $('#featureaddEditFlag').val()=='A')
				{
 					$("#featureAddImage_"+rowid).show();
 				 	$("#featureEditImage_"+rowid).hide();
				}else{
					$("#featureAddImage_"+rowid).hide();
 				 	$("#featureEditImage_"+rowid).show();
				}
				 $("#featurechildRowId_"+rowid).remove();	
				 $("#featureDeleteImage_"+rowid).show();
				 $("#featureWorkingImage_"+rowid).hide();  
				 
				 tempvar=$('.featuretab>tr:last').attr('id'); 
				 idval=tempvar.split("_");
				 rowid=Number(idval[1]); 
				 $('#featureDeleteImage_'+rowid).hide(); 
				 $('#openFlag').val(0);
				 $('#openFlag').val(0);
				 $('#property-featurepopup').dialog('destroy');		
				 $('#property-featurepopup').remove(); 
				 
				 return false;
		  });
		  $('.save_feature').click(function(){ 
			 // slidetab=$(this).parent().parent().get(0);
			 
			 //Find the Id for fetch the value from Id
				var tempvar=$('.tdfeature').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 
			 //var tempObject=$(this);
			 var propertyDetailId=$('#propertyDetailId_'+rowid).val();	
			 var featureType=$('#featureType').val();	
			 var featureCode=$('#featureCode').val();
			 var feature=$('#feature').val();
			 var lookupDetailId=$('#lookupDetailId').val();
			 var lineId=$('#featurelineId_'+rowid).val();
			 var addEditFlag=$('#featureaddEditFlag').val();
			
		        if($jquery("#propertyInfoFeatureAddEntry").validationEngine('validate')){        
		        	 var flag=false;
		        	$.ajax({
						type:"POST", 
						 url:"<%=request.getContextPath()%>/feature_save.action", 
					 	async: false, 
					 	data:{	propertyDetailId:propertyDetailId,
					 			feature:feature,
					 			lookupDetailId:lookupDetailId,
							 	addEditFlag:addEditFlag,
							 	id:lineId
							 },
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('.tempresult').html(result); 
						     if(result!=null) {
                                    flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                            $("#othererror").html($('#returnMsg').html()); 
                            return false;
					 	} 
		        	}); 
		        	if(flag==true){ 

			        	//Bussiness parameter
		        			$('#propertyDetailId_'+rowid).val(propertyDetailId); 
		        			$('#featureType_'+rowid).text(featureType); 
		        			$('#featureCode_'+rowid).text(featureCode);
		        			$('#feature_'+rowid).text(feature);
		        			$('#lookupDetailId_'+rowid).val(lookupDetailId); 
		        			
						//Button(action) hide & show 
		    				 $("#featureAddImage_"+rowid).hide();
		    				 $("#featureEditImage_"+rowid).show();
		    				 $("#featureDeleteImage_"+rowid).show();
		    				 $("#featureWorkingImage_"+rowid).hide(); 
		        			 $("#featurechildRowId_"+rowid).remove();
		        			 
		        		//Feature type popup destroy
		        		 $('#property-featurepopup').dialog('destroy');		
				 		 $('#property-featurepopup').remove(); 

		        			if(addEditFlag!=null && addEditFlag=='A')
		     				{
	         					$('.featureaddrows').trigger('click');
	         					var featurecount=Number($('#featurecount').val());
	         					featurecount+=1;
	         					$('#featurecount').val(featurecount);
	         					 tempvar=$('.featuretab>tr:last').attr('id'); 
	         					 idval=tempvar.split("_");
	         					 rowid=Number(idval[1]); 
	         					 $('#featureDeleteImage_'+rowid).show(); 
		     				}
		        		}		  	 
		      } 
		      else{return false;}
		  });
});
 </script>	