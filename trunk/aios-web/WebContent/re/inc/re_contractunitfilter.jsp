<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<style type="text/css">
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
</style>
</head>
<script type="text/javascript">
var id;
var contractNumber="";
var buildingId="";
var contractDate="";
var grandTotal="";
var approvedStatus="";
var oTable1; var selectRow=""; var aData="";var aSelected = [];var activeId=0;var s1=0; 
$(function(){  
	$('.formError').remove(); 
 	/* $('#unitcontractactive').dataTable({ 
 		"sAjaxSource": "contract_jsonunitdetail.action?unitId="+unitId,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 		"aoColumns": [
 			{ "sTitle": 'Contract ID', "bVisible": false},
 			{ "sTitle": '<fmt:message key="re.contract.contractno"/>',"sWidth": "100px"},
 			{ "sTitle": '<fmt:message key="re.contract.tenantname"/>',"sWidth": "300px"},
 			{ "sTitle": '<fmt:message key="re.contract.contractdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.fromdate"/>',"sWidth": "150px"},
 			{ "sTitle": '<fmt:message key="re.contract.todate"/>',"sWidth": "150px"},
 			{ "sTitle": 'Options',"sWidth": "100px","bVisible": false},
 			
 		],
 		"sScrollY": (Number($("#main-content").height() - 200) / 2 ) - 55,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData1.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		},
		"fnInitComplete": function () {
			getInactiveContracts();
	  	}
 	});	
 	//Json Grid
 	//init datatable
 	oTable1 = $('#unitcontractactive').dataTable();
 	
 	 	
 	$('#unitcontractactive tbody tr').live('dblclick', function () {
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData =oTable1.fnGetData( this );
			  activeId=aData[0];
	 	       callReportView();
		       return false;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable1.fnGetData( this );
	          activeId=aData[0];
	 	      callReportView();
		      return false;
	      }
	});
	
	$('.contract_print').live('click',function(){
		var parentTR=$(this).parent().parent().get(0);
		var contractId=getId(parentTR); 

		$('#loading').fadeIn();
		window.open('getPropertyContractDetail.action?recordId='
				+ contractId + '&format=HTML' ,'','width=800,height=800,scrollbars=yes,left=100px');
		$('#loading').fadeOut();
		return false;
	}); 
	 */
}); 
<%-- function getInactiveContracts(){
	//Inactive contract
 	$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/inactive_contract_json_redirect.action", 
			data: {unitId: unitId},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#gridDiv1").html(result); 
			} 
	}); 
} 
function getId(parentTR){ 
	if ($(parentTR).hasClass('row_selected') ) { 
		$(parentTR).addClass('row_selected');
        aData =oTable1.fnGetData( parentTR );
        s1=aData[0]; 
    }
    else {
        oTable.$('tr.row_selected').removeClass('row_selected');
        $(parentTR).addClass('row_selected');
        aData =oTable1.fnGetData( parentTR );
        s1=aData[0];
    }
	return s1;
}	
function callReportView(){
	$('#loading').fadeIn();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/getPropertyContractDetail.action", 
		data:{format:"HTML",recordId:activeId,approvalFlag:"N"},
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$("#reporting-tree").html(result); 
			$("table:first").addClass("table-custom-design-report");
			$("table:eq(1)").find("tbody:first").addClass("tbody-custom-design-report");
			$('#loading').fadeOut();
		}
 	});
}--%>
</script>
<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Search Result(s)</div>	 
<div id="rightclickarea" style="background-color:#FFFFFF">
	<!-- <div id="gridDiv">
		<table class="display dataTable" id="unitcontractactive"></table>
	</div>
	<div id="gridDiv1"></div> -->
	<div class="width100 float-left"><span class="side-heading">Active Contract</span></div>
	<div class="data-list-container width100 float-left" style="margin-top:10px;">
	<c:choose>
		<c:when test="${fn:length(requestScope.CONTRACT_LIST_ACTIVE)gt 0}">
			<table class="width100">	
					<thead>									
					<tr>
						<th>Contract Number</th> 
						<th>Contract Date</th>
						<th>Deposit Amount</th>
						<th>Contract Fee</th>
						<th>Rent Amount</th>
						<th>Contract Period</th>
						<th>Tenant Name</th>
						<th>Tenant Information</th>
						
					</tr> 
				</thead> 
				<tbody >	
					<c:forEach items="${requestScope.CONTRACT_LIST_ACTIVE}" var="result3" varStatus="status1" >
					 	<tr> 
							<td>${result3.contractNumber }</td>
							<td>${result3.contractDate }</td>
							<td>${result3.depositAmount }</td>
							<td>${result3.contractAmount }</td>
							<td>${result3.offerAmount}</td>
							<td>From (${result3.fromDate}) UpTo (${result3.toDate})</td>
							<td>${result3.tenantName }</td>
							<td>
								<span class="inside-heding">Identity :</span> ${result3.tenantIdentity} ,
								<span class="inside-heding">Nationality :</span> ${result3.tenantNationality},
								<span class="inside-heding">Mobile :</span> ${result3.tenantMobile},
								<span class="inside-heding">Fax : </span>${result3.tenantFax},
							</td>
						</tr>
					</c:forEach>
				  
				</tbody>
			</table>
		</c:when>
	  <c:otherwise>
	  		<tr class="even rowid">Contracts Not Found</tr>
	  </c:otherwise>
	  </c:choose>	
		</div>	
		<div class="width100 float-left"><span class="side-heading">Contract History</span></div>
		<div class="data-list-container width100 float-left" style="margin-top:10px;">
			<c:choose>
				<c:when test="${fn:length(requestScope.CONTRACT_LIST_INACTIVE)gt 0}">
				<table class="width100">	
						<thead>									
						<tr>
							<th>Contract Number</th> 
							<th>Contract Date</th>
							<th>Deposit Amount</th>
							<th>Contract Fee</th>
							<th>Rent Amount</th>
							<th>Contract Period</th>
							<th>Tenant Name</th>
							<th>Tenant Information</th>
							
						</tr> 
					</thead> 
					<tbody >	
				
						<c:forEach items="${requestScope.CONTRACT_LIST_INACTIVE}" var="result" varStatus="status1" >
						 	<tr> 
								<td>${result.contractNumber }</td>
								<td>${result.contractDate }</td>
								<td>${result.depositAmount }</td>
								<td>${result.contractAmount }</td>
								<td>${result.offerAmount}</td>
								<td>From (${result.fromDate}) UpTo (${result.toDate})</td>
								<td>${result.tenantName }</td>
								<td>
									<span class="inside-heding">Identity :</span> ${result.tenantIdentity} ,
									<span class="inside-heding">Nationality :</span> ${result.tenantNationality},
									<span class="inside-heding">Mobile :</span> ${result.tenantMobile},
									<span class="inside-heding">Fax : </span>${result.tenantFax},
								</td>
							</tr>
						</c:forEach>
					  	
					</tbody>
				</table>
			</c:when>
			  <c:otherwise>
			  		<label class="caption width100">Contracts Not Found</label>
			  </c:otherwise>
			  </c:choose>
		</div>	
		<div class="vmenu">
		    <div class="first_li"><span class="print-call">Print</span></div>
			<div class="first_li"><span class="xls-download-call">Download as XLS</span></div>
			<div class="first_li"><span class="pdf-download-call">Download s PDF</span></div>					
		</div>
</div>	 