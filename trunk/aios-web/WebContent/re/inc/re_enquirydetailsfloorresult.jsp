<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<option value="">Select</option>
<c:if test="${floor != null}">
	<c:forEach items="${requestScope.floor}" var="floorList">
		<option value="${floorList.flatId}">${floorList.floorNumber} </option>
	</c:forEach>
</c:if> 