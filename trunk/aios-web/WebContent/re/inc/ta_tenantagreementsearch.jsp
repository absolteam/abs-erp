  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true; 
$(function(){ 
	 $("#mastersearchValidate").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});
	$('.formError').remove();
	$("#list2").jqGrid({
		url: '<%=request.getContextPath()%>/ta_tenantjsonsearch.action?agreementNumber='+agreementNumber+'&agreementDate='+agreementDate+'&firstRentDate='+firstRentDate+'&amount='+amount+'&functionalAmount='+functionalAmount+'&tenantName='+tenantName+'&leadDays='+leadDays, 
		 datatype: "json",
		 colNames:['agreementId','Agreement Number', 'Agreement Date', 'From Date', 'To Date', 'First Rent Date', 'Lead Days', 'Amount', 'Exchange Rate', 'Functional Amount', 'customerId', 'Customer Name'],  
		 colModel:[
                     {name:'agreementId',index:'A.RE_AGREEMENT_ID', sortable:false},
                     {name:'agreementNumber',index:'A.RE_AGREEMENT_NUMBER', width:80, sortable:true,sortable:"data"},
                     {name:'agreementDate',index:'A.AGREEMENT_DATE', width:80, sortable:true,sortable:"date"},
                     {name:'startDate',index:'A.EFFECTIVE_DATE_FROM', width:80, sortable:true,sortable:"date"},
                     {name:'endDate',index:'A.EFFECTIVE_DATE_TO', width:80, sortable:true, sorttype:"date"} ,
                     {name:'firstRentDate',index:'A.FIRST_RENT_DATE', width:80, sortable:true, sorttype:"date"} , 
                     {name:'leadDays',index:'A.LEAD_DAYS', width:50, sortable:true, sorttype:"data"} , 
                     {name:'amount',index:'A.AMOUNT', width:100, sortable:true, sorttype:"data"} , 
                     {name:'exchangeRate',index:'A.EXCHANGE_RATE', width:100, sortable:true, sorttype:"data"} , 
                     {name:'functionalAmount',index:'A.FUNCTIONAL_AMOUNT', width:100, sortable:true, sorttype:"data"} , 
                     {name:'customerId',index:'A.CUSTOMER_ID', width:100, sortable:false} ,   
                     {name:'customerName',index:'B.CUSTOMER_NAME', width:100, sortable:true, sorttype:"data"} ,                                                    
    	 ], 			  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'A.RE_AGREEMENT_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:"Tenant Agreement"
	});
	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false}); 
	$("#list2").jqGrid('hideCol',["agreementId","customerId"]);  
	
}); 
</script>
<table id="list2"></table>  
<div id="pager2"> </div> 