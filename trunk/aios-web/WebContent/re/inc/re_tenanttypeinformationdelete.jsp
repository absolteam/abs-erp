<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
$(function (){  
		$('.formError').remove();


		 approvedStatus=$('#approvedStatus').val(); 
			if( approvedStatus == 'E'){
				$('#confirm').fadeOut();	
				$('#cancel').fadeIn();
				$('.commonErr').hide().html("Workflow in is process, Cannot delete this screen.!!!").slideDown();
			}
			if(approvedStatus == 'A' ){
				$('#confirm').fadeOut();	
				$('#cancel').fadeIn();
				$('.commonErr').hide().html("Workflow is already Approved, Cannot delete this screen.!!!").slideDown();
			}
			if(approvedStatus == 'R' ){
				$('#confirm').fadeOut();	
				$('#cancel').fadeIn();
				$('.commonErr').hide().html("Workflow is already Rejected,Cannot delete this screen.!!!").slideDown();
			}
		
		$('#close').click(function(){
		 $.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
		     	async: false, 
				dataType: "html",
				cache: false,
				error: function(data) 
				{  
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
		     	}
			});
		return true;
 	});  

		$('#cancel').click(function(){
			 $.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
			     	async: false, 
					dataType: "html",
					cache: false,
					error: function(data) 
					{  
					},
			     	success: function(data)
			     	{
						$("#main-wrapper").html(data);  //gridDiv main-wrapper
			     	}
				});
			return true;
	 	}); 
		
	$('#confirm').click(function(){ 
		tenantTypeId=$('#tenantTypeId').val();
   		$.ajax({
   			type: "POST", 
   			url: "<%=request.getContextPath()%>/tenant_type_informationdelete.action",
   			data: {tenantTypeId: tenantTypeId},  
   	     	async: false,
   			dataType: "html",
   			cache: false,
   			success: function(result)
   			{ 
   				$("#main-wrapper").html(result); 
   			} 
   		}); 
   		return true;
 	}); 
 });
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.tenanttypeinformation"/></div>
			  <div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
			  <div class="portlet-content">
						<div>
							<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
								<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
							</c:if>
							<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
								<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
						   </c:if>
					    </div>
					  <form name="tenantInformationAdd" id="tenantInformationAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.tenanttypeinformation2"/></legend>						
		                        <div>
									<c:choose>
										<c:when test="${bean.payChequeFees eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="payChequeFees" id="payChequeFees"/>
											<input type="text" name="chequeAmount" disabled="disabled" id="chequeAmount" value="${bean.chequeAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payChequeFees" disabled="disabled" id="payChequeFees" >
											<input type="text" name="chequeAmount" disabled="disabled" id="chequeAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paychequefees"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.contractFees eq 1 }">
											<input type="checkbox" checked="checked" name="contractFees" disabled="disabled" id="contractFees"/>
											<input type="text" name="contractAmount" disabled="disabled" id="contractAmount" value="${bean.contractAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="contractFees" disabled="disabled" id="contractFees" >
											<input type="text" name="contractAmount" disabled="disabled" id="contractAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.contractfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.payDeposit eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="payDeposit" id="payDeposit"/>
											<input type="text" name="depositAmount" disabled="disabled" id="depositAmount" value="${bean.depositAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payDeposit" disabled="disabled" id="payDeposit" >
											<input type="text" name="depositAmount" disabled="disabled" id="depositAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paydeposit"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.rentFees eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="rentFees" id="rentFees"/>
											<input type="text" name="rentAmount" disabled="disabled" id="rentAmount" value="${bean.rentAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="rentFees" disabled="disabled" id="rentFees" >
											<input type="text" name="rentAmount" disabled="disabled" id="rentAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.rentfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.maintenanceFees eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="maintenanceFees" id="maintenanceFees"/>
											<input type="text" name="maintenanceAmount" disabled="disabled" id="maintenanceAmount" value="${bean.maintenanceAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="maintenanceFees" disabled="disabled" id="maintenanceFees" >
											<input type="text" name="maintenanceAmount" disabled="disabled" id="maintenanceAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.maintenancefees"/></label>
								</div>		
								<div>
									<c:choose>
										<c:when test="${bean.payPenalities eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="payPenalities" id="payPenalities"/>
											<input type="text" name="penalitiesAmount" disabled="disabled" id="penalitiesAmount" value="${bean.penalitiesAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payPenalities" disabled="disabled" id="payPenalities" >
											<input type="text" name="penalitiesAmount" disabled="disabled" id="penalitiesAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paypenalities"/></label>
								</div>
								<div>
									<c:choose>
										<c:when test="${bean.otherFees eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="otherFees" id="otherFees"/>
											<input type="text" name="otherAmount" disabled="disabled" id="otherAmount" value="${bean.otherAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="otherFees" disabled="disabled" id="otherFees" >
											<input type="text" name="otherAmount" disabled="disabled" id="otherAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.otherfees"/></label>
								</div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.property.info.tenanttypeinformation1"/></legend>	
		   						<div style="display:none;"><label>Tenant Type Id</label>
		   							<input type="text" name="tenantTypeId" class="width30" id="tenantTypeId" readonly="readonly" value="${bean.tenantTypeId}"/>
		   						</div>             		
		   						<div><label><fmt:message key="re.property.info.tenanttypedescription"/></label>
		   							<input type="text" name="descriptions" disabled="disabled" class="width50 validate[required]" id="descriptions" value="${bean.descriptions}"/>
		   						</div> 
		   						<div>
									<c:choose>
										<c:when test="${bean.payAfterContract eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="payAfterContract" id="payAfterContract"/>
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payAfterContract" disabled="disabled" id="payAfterContract" >
											<input type="text" name="chequeAmount" disabled="disabled" id="chequeAmount" class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.payaftercontract"/></label>
								</div>	            		
								<div>
									<c:choose>
										<c:when test="${bean.needReleaseLetter eq 1 }">
											<input type="checkbox" checked="checked" disabled="disabled" name="needReleaseLetter" id="needReleaseLetter"/>
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="needReleaseLetter" disabled="disabled" id="needReleaseLetter" >
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.needreleaseletter"/></label>
								</div>
								<div>
									<label><fmt:message key="re.property.info.offervaliditydays"/></label>
									<input type="text" name="offerValidityDays" disabled="disabled" class="width30 validate[required,custom[onlyNumber]]" id="offerValidityDays" value="${bean.offerValidityDays}"/>
								</div>	
								<div><label><fmt:message key="re.property.info.noofdaysallowlate"/></label>
	                            	<input type="text" name="noOfDaysAllowLate" disabled="disabled" class="width30 validate[required,custom[onlyNumber]]" id="noOfDaysAllowLate" value="${bean.noOfDaysAllowLate}"/>
	                            </div>												
							</fieldset> 
						</div>
						<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
						<div class="clearfix"></div>
						<div class="float-right buttons ui-widget-content ui-corner-all" style="">
							<div class="portlet-header ui-widget-header float-right" id="cancel"><fmt:message key="re.property.info.discard"/></div>
						 	<div class="portlet-header ui-widget-header float-right" id="confirm"><fmt:message key="re.property.info.confirm"/></div> 
						 </div>
				  </form>
				</div>
			</div>
		</div>
				
				