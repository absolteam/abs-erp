<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			tenant details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="componentstypes" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${TENANT_DETAILS}" var="bean" varStatus="status">
	     		<option value="${bean.tenantId}@#${bean.addressDetails}">${bean.tenantName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 tenant_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width38 float-left"><span>Tenant Name</span></li> 
   				<li class="width30 float-left"><span>Eimrate Id/Trade No</span></li>
   				<li class="width28 float-left"><span>Tenant Type</span></li>
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${TENANT_DETAILS}" var="bean" varStatus="status">
		     		<li id="tenant_${status.index+1}" class="tenant_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.tenantId}@#${bean.addressDetails}" id="tenantbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantName}" id="tenatnamebynameid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.addressDetails}" id="tenataddressbyid_${status.index+1}"> 
		     			<span id="tenantNamebyname_${status.index+1}" style="cursor: pointer;" class="float-left width38">${bean.tenantName} </span>  
		     			<span style="cursor: pointer;" class="float-left width30">${bean.tenantNumber}</span> 
		     			<span style="cursor: pointer;" class="float-left width28">${bean.addressDetails}
		     				<span id="tenantNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){ 
	$($($('#offer-popup').parent()).get(0)).css('width',500);
	$($($('#offer-popup').parent()).get(0)).css('height',250);
	$($($('#offer-popup').parent()).get(0)).css('padding',0);
	$($($('#offer-popup').parent()).get(0)).css('left',0);
	$($($('#offer-popup').parent()).get(0)).css('top',100);
	$($($('#offer-popup').parent()).get(0)).css('overflow','hidden');
	$('#offer-popup').dialog('open'); 
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){ 
	    	   var tenantdetails=$(this).val();
	    	   var teanantarray=tenantdetails.split("|");
	    	   $('#tenantId').val(teanantarray[0]);
	    	   $('#addressDetails').val(teanantarray[1]); 
		       $('#tenantName').val($(".autoComplete option:selected").text());
		       $('#offer-popup').dialog("close"); 
	   } 
	}); 
	$('.tenant_list_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.tenant_list_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#tenantNameselect_'+rowid).hasClass('selected')){ 
				 $('#tenantNameselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#tenantNameselect_'+rowid).addClass('selected');
	});
	
	$('.tenant_list_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#tenantId').val($('#tenantbyid_'+rowid).val());
		 $('#tenantName').val($('#tenatnamebynameid_'+rowid).val()); 
		 $('#addressDetails').val($('#tenataddressbyid_'+rowid).val());  
		 $('#offer-popup').dialog('close'); 
	});
	$('#close').click(function(){
		 $('#offer-popup').dialog('close'); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.tenant_list ul li{
		padding:3px;
	}
	.tenant_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>