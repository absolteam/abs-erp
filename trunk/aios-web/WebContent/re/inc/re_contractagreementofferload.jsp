<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			offer details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="componentstypes" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${CONTRACT_OFFER_LIST}" var="bean" varStatus="status">
	     		<option value="${bean.tenantId}@${bean.tenantName}@${bean.tenantGroup.tenantGrpTitle}
	     			@${bean.tenantGroup.rent}@${bean.offerNumber}@${bean.offerId}@${bean.fromDate}@${bean.toDate}
	     			@${bean.tenantGroup.contractFee}@${bean.depositAmount}">${bean.offerNumber} - ${bean.tenantName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 contract_list" style="padding:2px;">  
	     	<ul> 
	     		<li class="width10 float-left"><span>Offer No</span></li> 
	     		<li class="width30 float-left"><span>Property</span></li> 
   				<li class="width25 float-left"><span>Tenant Name</span></li> 
   				<li class="width20 float-left"><span>Tenant Type</span></li>
   				<li class="width10 float-left"><span>Tenant Group</span></li>
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${CONTRACT_OFFER_LIST}" var="bean" varStatus="status">
		     		<li id="tenant_${status.index+1}" class="contract_list_li width100 float-left">
		     			<input type="hidden" value="${bean.offerId}" id="offerIdbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.offerNumber}" id="offerNumberbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantName}" id="tenantNamebyid_${status.index+1}"> 
		     			
		     			<input type="hidden" value="${bean.fromDate}" id="fromDatebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.toDate}" id="toDatebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantId}" id="tenantIdbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantType}" id="tenantTypebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantGroup.tenantGrpTitle}" id="tenantGrpTitlebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.depositAmount}" id="depositbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantGroup.contractFee}" id="contractFeebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.tenantGroup.rent}" id="rentbyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.propertyName}" id="propertyname_${status.index+1}">
		     			<input type="hidden" value="${bean.componentName}" id="componentname_${status.index+1}">
		     			<input type="hidden" value="${bean.presentAddress}" id="presentaddress_${status.index+1}">
		     			<span id="offerNumberContract_${status.index+1}" style="cursor: pointer;" class="float-left width10">${bean.offerNumber} </span>  
		     			<span style="cursor: pointer;" class="float-left width30">${bean.propertyName}</span> 
		     			<span style="cursor: pointer;" class="float-left width25">${bean.tenantName}</span> 
		     			<span style="cursor: pointer;" class="float-left width20">${bean.tenantType}</span>  
		     			<span style="cursor: pointer;" class="float-left width10">${bean.tenantGroup.tenantGrpTitle}
		     				<span id="offercontractselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:88%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="closecontracts" style="cursor:pointer;">close</div>  
	</div>
 </div> 
<script type="text/javascript">
var currentId=""; var offerIds="";
$(function(){ 
	$($($('#contract-popup').parent()).get(0)).css('width',500);
	$($($('#contract-popup').parent()).get(0)).css('height',250);
	$($($('#contract-popup').parent()).get(0)).css('padding',0);
	$($($('#contract-popup').parent()).get(0)).css('left',0);
	$($($('#contract-popup').parent()).get(0)).css('top',100);
	$($($('#contract-popup').parent()).get(0)).css('overflow','hidden'); 
	$($($('#contract-popup').parent()).get(0)).css('z-index',999999); 
	$('#contract-popup').dialog('open'); 
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){ 
	    	 var offerdetails=$(this).val();
	    	 var offerarray=offerdetails.split('@');
	    	 $('#tenantId').val(offerarray[0]);
	  		 $('#tenantName').val(offerarray[1]);  
	  		 $('#tenantGroupTitle').val(offerarray[2]);  
	  		 $('#rentFees').val(offerarray[3]);  
	  		 $('#rentFeesAmount').val(offerarray[3]);  
	  		 $('#offerNumber').val(offerarray[4]); 
	  		 $('#offerId').val(offerarray[5]); 
	  		 $('#startPicker').val(offerarray[6]); 
	  		 $('#endPicker').val(offerarray[7]); 
	  		 $('#contractAmount').val(offerarray[8]);
	  		 $('#depositAmount').val(offerarray[9]);
	  		 offerIds=Number($('#offerId').val());
	  		 offerRentDetail();
	  		 // Added by Ahsan.M
	  		 $('#contractAmount,#depositAmount').trigger('blur'); 
	  		 $('#contract-popup').dialog('close');  
	   } 
	}); 
	$('.contract_list_li').click(function(){  
		var tempvar="";var idarray=""; var rowid="";
	    $('.contract_list_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#offercontractselect_'+rowid).hasClass('selected')){ 
				 $('#offercontractselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#offercontractselect_'+rowid).addClass('selected');
		return false;
	});
	
	$('.contract_list_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#tenantId').val($('#tenantIdbyid_'+rowid).val());
		 $('#tenantName').val($('#tenantNamebyid_'+rowid).val());  
		 $('#tenantGroupTitle').val($('#tenantGrpTitlebyid_'+rowid).val());  
		 $('#rentFees').val($('#rentbyid_'+rowid).val());  
		 $('#rentFeesAmount').val($('#rentbyid_'+rowid).val());  
		 $('#offerNumber').val($('#offerNumberbyid_'+rowid).val()); 
		 $('#offerId').val($('#offerIdbyid_'+rowid).val()); 
		 $('#startPicker').val($('#fromDatebyid_'+rowid).val()); 
		 $('#endPicker').val($('#toDatebyid_'+rowid).val()); 
		 $('#contractAmount').val($('#contractFeebyid_'+rowid).val());
		 $('#depositAmount').val($('#depositbyid_'+rowid).val());
		 $('#propertyName').val($('#propertyname_'+rowid).val());
		 $('#componentName').val($('#componentname_'+rowid).val());
		 $('#presentAddress').val($('#presentaddress_'+rowid).val());
		 
		 offerIds=Number($('#offerId').val());
		 offerRentDetail();
		 // Added by Ahsan.M
		 $('#contractAmount,#depositAmount').trigger('blur');
		 $('#contract-popup').dialog('close'); 
	});
	$('#closecontracts').live('click',function(){
		 $('#contract-popup').dialog('close'); 
	});
}); 
function offerRentDetail(){
    $.ajax({
	   		type:"POST",
	   		url:"<%=request.getContextPath()%>/contract_agreement_load_offer_rent.action", 
	   	 	async: false,
	   	 	data: {offerId: offerIds},
	   	    dataType: "html",
	   	    cache: false,
	   		success:function(result){
	   			$(".tabRent").html(result);
	   			$.fn.globeTotal();  //For Total Amount of Rent & Contract
	   			$('.dateCheck').trigger('blur');
	   			contractAmountTotal=convertToDouble($('#contractAmountTotal').val());
	   			offerId=$('#offerId').val();
	   		}
		});
}
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:388px!important;
		height: 200px!important;
		overflow: auto;
	} 
	.contract_list ul li{
		padding:3px;
	}
	.contract_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>