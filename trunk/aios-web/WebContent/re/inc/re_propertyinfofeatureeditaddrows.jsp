<tr class="row">  
	<td class="width10">${requestScope.lineNumber}</td>	 
	<td class="width20"></td>
	<td class="width20"></td>	
	<td class="width10"></td> 
	<td class="width10"></td>
	<td style="display:none"> 
		<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
		<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
		<input type="hidden" value="" name="tempLineId" id="tempLineId"/>	
	</td> 
	<td style=" width:5%;"> 
				<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeatureNew" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
	   	</a>	
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeatureNew"  style="display:none;cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeatureNew" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
			<span class="processing"></span>
		</a>
	</td>  
	<td style="display:none;"></td>	
	<td style="display:none;"></td>	 
	<td style="display:none;"></td>	
</tr>  
<script type="text/javascript">
$(function(){
	//Adding Feature Details
 	$('.addFeatureNew').click(function(){ 
 		if(openFlag==0){
			if($('#propertyInfoEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_property_feature_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 
					 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}
	});
	$(".editFeatureNew").click(function(){
		if(openFlag==0){
			if($('#propertyInfoEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_property_feature_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
					 var temp5= $($(slidetab).children().get(4)).text();
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}
	});
   
 	$('.delFeatureNew').click(function(){
 		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        buildingId=$('#buildingId').val();
	        var actionflag="D";
	 		var trnValue=$("#transURN").val(); 
	 		actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
	 		var tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
			var flag = false; 
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_property_feature_update.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, buildingId: buildingId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCountFeature=Number($('#childCountFeature').val());
		       		 if(childCountFeature > 0){
		       			childCountFeature=childCountFeature-1;
						$('#childCountFeature').val(childCountFeature);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
 		}
 	});
	
});
</script>