<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;

 </script>
 <div id="main-content">
	 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Screen</div>
	 		 	<div style="float:left;width:48%;" id="hrm" class="create-task">
	 	<fieldset>
		<div>
		<label class="width30">Offer No</label>	 
		<select><option value="">-Select-</option>
				<option></option>
		</div>
		<div>
		<label class="width30">Building No</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Tenant Name</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Address</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Address1</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<fieldset>
		<legend> Period Of Rent</legend>
		<label class="width30">From Date</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		<label class="width30">To Date</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
	
		<label class="width30">Years</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		<label class="width30">Months</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		<label class="width30">Days</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</fieldset>
		</div>
		</fieldset>
		</div>
		<div class="float:left;width:50%;" id="hrm">
			<fieldset style="min-height:280px;">
		<div style="margin-bottom:20px">
		<label class="width30">Contract No</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div style="margin-bottom:20px">
		<label class="width30">Contract Date</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div style="margin-bottom:20px">
		<label class="width30">Tenant Type</label>	 
		<select><option value="">-Select-</option>
		<option>Government</option>
		<option>Individual</option>
		<option>Partnership</option>
		</select>
		</div>
		<div style="margin-bottom:20px">
		<label class="width30">Payement Method</label>	 
		<select><option value="">-Select-</option>
		<option>Cash</option>
		<option>Cheque</option>
		</select>
		</div>
		<div>
		<label class="width30">No of Cheque</label>	 
		<select><option value="">-Select-</option>
		<option>1</option>
		<option>2</option>
		<option>3</option>
		<option>4</option>
		</select>
		</div>
		</fieldset>
		</div>
		<div>
		<fieldset>
		<legend>Rental Details</legend>
		</fieldset>
		</div>
		<div id="hrm recurStat" style="margin-bottom:10px; float:bottom; margin-top:0px; width:1060px"">
		<div class="hastable">
					<table class="hastab" style="float:left;"> 
						<thead>										
						<tr>
							<th>Sl No</th> 
							<th>Flat No</th>
							<th>Rent Amount</th>
							<th>Contract Amount</th>
						</tr> 
						</thead> 
		<tbody class="tab" style="height:110px;min-height:20px;overflow-x:hidden;">
			<c:forEach var="i" begin="1" end="6" step="1" varStatus ="status">
						 		<tr class="even"> 
										<td></td>
										<td class="tempSeqNumber"></td> 		 	 
										<td></td>   
										<td></td>	
								
					
		
		
		</tr>
		</c:forEach>
		</tbody>
		</table>
		<div style="margin-bottom:20px;float:right" >
		<label class="width30">Total</label>	 
		<input class="width50" type="text" name="assignby"  id="assignby" >
		</div>
		</div>
		<div style="margin-bottom:20px;float:right" >
		<label class="width30">Total</label>	 
		<input class="width50" type="text" name="assignby"  id="assignby" >
		</div>
		</div>
		
		
		<div id="hrm recurStat" style="margin-bottom:10px; float:bottom; margin-top:0px; width:1115px"">
		<fieldset class="width95 float-left">
		<legend>Fee Details</legend>
		<div class="hastable">
		<table class="hastab" style="float:left;"> 
						<thead>								
						<tr>
							<th>Sl No</th> 
							<th>Fee Id</th>
							<th>Description</th>
							<th>Amount</th>
						</tr> 
						</thead> 
		<tbody class="tab" style="height:110px;min-height:20px;overflow-x:hidden;">
			<c:forEach var="i" begin="1" end="6" step="1" varStatus ="status">
						 		<tr class="even"> 
										<td></td>
										<td class="tempSeqNumber"></td> 		 	 
										<td></td>   
										<td></td>	
								
					
		
		
		</tr>
		</c:forEach>
		</tbody>
		</table>
		</div>
		<div style="margin-bottom:20px;float:right" >
		<label class="width30">Total</label>	 
		<input class="width50" type="text" name="assignby"  id="assignby" >
		</div>
		</div>
		
		<div id="hrm recurStat" style="margin-bottom:15px; float:bottom; margin-top:0px; width:1060px"">
		<fieldset class="width95 float-left">
		<legend>Payement Details</legend></fieldset>
		<div class="hastable">
					<table class="hastab" style="float:left;"> 
						<thead>										
						<tr>
							
							<th>Bank Name</th>
							<th>Cheque Date</th>
							<th>Cheque No</th>
							<th>Cheque Amount</th>
						</tr> 
						</thead> 
		<tbody class="tab" style="height:110px;min-height:20px;overflow-x:hidden;">
			<c:forEach var="i" begin="1" end="6" step="1" varStatus ="status">
						 		<tr class="even"> 
										
										<td class="tempSeqNumber"></td> 		 	 
										<td></td>   
										<td></td>	
								        <td></td>
					
		
		
		</tr>
		</c:forEach>
		</tbody>
		</table>
		</div>
		<div style="margin-bottom:20px;float:right" >
		<span>Total</span>	 
		<input class="width50" type="text" name="assignby"  id="assignby" >
		</div>
		</div>
		
		<div style="float:left;width:48%;" id="hrm" class="create-task">
		<div class="clearfix">
		<div>
		<label class="width30" style="border-left: 9px margin-top:9px">Cash Amount</label>	 
		<input class="width10" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Cheque Amount</label>	 
		<input class="width10" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Grand Total</label>	 
		<input class="width10" type="text" name="assignby"  id="assignby" >
		</div>
	<div>
	<label class="width30">Amount in Words Only</label>
	</div>
	</div>
	</div>
		
		
		<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div class="portlet-header ui-widget-header float-right discard">Release</div> 
				<div class="portlet-header ui-widget-header float-right discard">Cancel</div> 
			 	<div class="portlet-header ui-widget-header float-right save-all">Save</div> 
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		