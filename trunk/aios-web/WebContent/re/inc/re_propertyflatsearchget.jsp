<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div><label><fmt:message key="re.property.info.buildingname"/></label>
	<select id="buildingId" style="width: 35.5%;">
		<option value="">-Select-</option>
		<c:if test="${requestScope.itemList ne null}">
			<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
				<option value="${building.buildingId }">${building.buildingName }</option>
			</c:forEach>
		</c:if>
	</select>
</div>	
<div><label><fmt:message key="re.property.info.componenttypename"/></label>
	<select id="componentType" style="width: 35.5%;">
		<option value="">-Select-</option>
		<c:if test="${requestScope.lookupList ne null}">
			<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
				<option value="${compType.componentType }">${compType.componentTypeName }</option>
			</c:forEach>
		</c:if>
	</select>
</div>
<div id="tempresult" style="display:none;">
	<label class=""><fmt:message key="re.common.approvedstatus"/></label>
	<select id="approvedStatus" style="width: 35.5%;">
		<option value="">-Select-</option>
		<c:forEach items="${requestScope.approvedStatus}" var="approvedStatus" varStatus="status">
	<option value="${approvedStatus.approvedStatus }">${approvedStatus.approvedStatusName}</option>
	</c:forEach>
		</select>
</div>