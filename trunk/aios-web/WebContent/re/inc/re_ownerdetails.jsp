<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(document).ready(function (){  

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/owner_information_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
});


</script>
  

<script type="text/javascript">

	
	
</script>

<div id="main-content">
				 
				 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Owner Information</div>
					
					<fieldset>

						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend>Owner Information 2</legend>					
									<div><label class="width30">Address 1<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip"  title="Enter UAE Id No"></div>
									<div><label class="width30">Address 2<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip"  title="Enter Email Id" ></div>
							        <div><label class="width30">Country<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>India</option>
											<option>America</option>
											<option>Dubai</option>
											</select>
									</div>
									<div><label class="width30">State<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>Tamilnadu</option>
											<option>Kerala</option>
											<option>Andra pradesh</option>
											</select>
									</div>	
									<div><label class="width30">City<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>Salem</option>
											<option>Coimbatore</option>
											<option>Erode</option>
											</select>
									</div>														
									<div><label class="width30">Email Id.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Email Id"></div>
		                            <div><label class="width30">MobileNo.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter MobileNo"></div>
		                            <div><label class="width30">LandLine No.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter LandLine No"></div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend>Owner Information 1</legend>
									<div><label class="width30">Owner Id No.</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label class="width30">Name<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name" ></div>
									<div><label class="width30">Owner Type<span style="color:red">*</span></label>
											<select class="width30 tooltip" >
											<option>-Select-</option>
											<option>Owner</option>
											<option>Maintainer</option>
											</select>
									</div>				
									<div><label class="width30">Profession<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name" ></div>
									<div><label class="width30">UAE Id No.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name"></div>
									<div><label class="width30">Passport No.<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name"></div>
									<div><label class="width30">Birth Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Tenant Name"></div>
							 		<div><label class="width30">From Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip"  title="Select Birth Date" readonly="readonly"></div>		
				                    <div><label class="width30">To Date<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>
							</fieldset> 
													 
						</div>
				
				 
						</div>
				
							
	<div class="float-right buttons ui-widget-content ui-corner-all"> 			
		<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel">CANCEl</div> 
		<a href="#" onclick="return personalDetailsAjaxCall();"> <div class="portlet-header ui-widget-header float-right" id="add_person" >Save</div></a>
		
	</div>
					</div>
				</div>
			