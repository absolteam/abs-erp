<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tr>
	<td colspan="4" class="tdidentity" id="childRowId_${rowId}"> 
		 <div style="padding-left: 20px;">
		 <div class="width36 view float-left" id="hrm">
				<fieldset style="height: 100px;">
					<legend>Selection</legend> 
					<div style="display: none;">
						<label>Line Number</label>
						<input type="text" name="lineNumber" id="lineNumber" class="width20 validate[required]" readonly="readonly">
						<input type="hidden" name="lookupDetailId" id="lookupDetailId" class="width20">
					</div>
					<div>
						<label class="width30">Feature Code<span style="color: red;">*</span></label>
						 <input type="hidden" name="componentFeatureId" id="componentFeatureId"/> 
						<input type="text" name="componentFeature" readonly="readonly" id="componentFeature" class="width45 validate[required] ">
						<span class="button">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all componet_featurepop width100"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
					</div> 
					<div>
						<label class="width30">Features</label>
						 <input type="hidden" /> 
						<input type="text" readonly="readonly" name="componentFeatures" id="componentFeatures"
							   class="width45 validate[required] ">						
					</div> 
				</fieldset> 
		 </div>  
		  <div class="width30 view float-left" id="hrm">
			<fieldset style="height: 100px;">
				<legend>Details</legend> 
				<div>
					<label class="width30">Features Details</label>
					<input type="text" name="componentFeatureDetail" id="componentFeatureDetail"
						   class="width50">						
				</div> 
			</fieldset>	
		</div> 
		 <div class="width30 view float-left" id="hrm">
			<fieldset style="height: 100px;">
				<legend>Uploads</legend> 
				<div id="hrm" Style="margin-bottom: 10px;">
					<div id="UploadDmsDiv" class="width100">
						
						<div style="height: 90px;">									
							
							<div id="dms_image_information_component_feature" style="cursor: pointer; color: blue;">
								<u>Upload image here</u>
							</div>
							<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
								<span id="uploadedImagesComponent"></span>
							</div>
							
						</div>					
					</div>
				</div>
			</fieldset>	
		</div> 
		 <div class="clearfix"></div> 
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"> 
		 	<input type="hidden" id="showPage" value="${showPage}"/>
			<div class="portlet-header ui-widget-header float-right rowcancel" id="cancel_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right rowsave" id="rowsave_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>
		 <div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="componet_featurepop" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="componet_featurepop-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 </div>
		 </div>
	</td>  
</tr>
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0); 
$(function(){  
	// for uploader
	var showPage=$('#showPage').val();
	var tempvar=$('.tdidentity').attr("id");
	var idarray = tempvar.split('_');
	var rowid=Number(idarray[1]);  
	var componentFeatureId = $("#componentlineid_"+rowid).val();	

	if(($('#showPage').val() == "editedit") 
			&& (componentFeatureId != null || componentFeatureId != undefined || componentFeatureId != '')){
		
		populateUploadsPane("img","uploadedImagesComponent", "ComponentDetail", componentFeatureId);
		
		$('#dms_image_information_component_feature').click(function(){
			AIOS_Uploader.openFileUploader("img","uploadedImagesComponent", "ComponentDetail", componentFeatureId);
		});
	}
	else {
		$('#dms_image_information_component_feature').click(function(){
			AIOS_Uploader.openFileUploader("img","uploadedImagesComponent", "ComponentDetail", "-1");
		});
	}
	
	$('.rowcancel').click(function(){ 
		$('.formError').remove();			 
		//Find the Id for fetch the value from Id
		$('#componet_featurepop').dialog('destroy');		
		$('#componet_featurepop').remove();
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	 
		if(showPage=="editedit"){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			$("#DeleteImage_"+rowid).show();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 } 
		tempvar=$('.tab>tr:last').attr('id'); 
		idval=tempvar.split("_");
		rowid=Number(idval[1]); 
		$('#DeleteImage_'+rowid).hide(); 
	});

	$('.rowsave').click(function(){
		 //Find the Id for fetch the value from Id
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	
		 var componentFeatureId=$('#componentFeatureId').val();
		 var componentFeature=$('#componentFeature').val();
		 var componentFeatures=$('#componentFeatures').val();
		 var componentFeatureDetail=$('#componentFeatureDetail').val();
		 var lookupDetailId=$('#lookupDetailId').val();
		 var lineId=$('#lineId_'+rowid).text();  
		 var showPage=$('#showPage').val();  
		 var componentId=$('#componentId').val(); 
		 if(componentId!=null && componentId!="" && componentId>0){
		 }
		 else{
			 componentId=0;
		 }
		 var componentLineId=$('#componentlineid_'+rowid).val(); 
		 if(componentLineId!=null && componentLineId!="" && componentLineId>0){
		 }
		 else{
			 componentLineId=0;
		 }
		 var url_action=""; 
		 if(showPage=="editedit")
			 url_action="propertycomponent_editedit_linesave";
		 else if(showPage=="editadd")
			 url_action="propertycomponent_editadd_linesave";
		 else if(showPage=="addedit")
			 url_action="propertycomponent_addedit_linesave";
		 else if(showPage=="addadd")
			 url_action="propertycomponent_addadd_linesave";
		 else{
			 $('#othererror').hide().html("There is no Action mapped.").slideDown(1000);
			 return false;
		 }
		 
		 if($jquery("#journalvoucher-form").validationEngine('validate')){      
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false, 
				 	data:{componentFeaturesName:componentFeatures,componentFeatureDetail:componentFeatureDetail, 
				 		componentFeature: componentFeature, componentId:componentId, 
				 		componentLineId:componentLineId, id:rowid, showPage:showPage,
				 		lookupDetailId:lookupDetailId
					 	 },
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tempresult').html(result); 
					     if(result!=null) {
                             flag = true;
                             featureRowsAdded = featureRowsAdded + 1;
	         					if(featureRowsAdded == 2) {
	         						$('.addrows').trigger('click');
	         						featureRowsAdded = 1;
	         					}
					     } 
				 	},  
				 	error:function(result){
				 	  $('.tempresult').html(result);
                     $("#othererror").html($('#returnMsg').html()); 
                     return false;
				 	} 
	          });
			 if(flag==true){   
		        	//Bussiness parameter 
	        		$('#componentfeature_'+rowid).text(componentFeatures);  
	        		$('#componentfeatureid_'+rowid).val(componentFeatureId);
	        		$('#componentfeatureCode_'+rowid).text(componentFeature);  
	        		$('#componentfeatureDetails_'+rowid).text(componentFeatureDetail);
	        		$('#lookupDetailId_'+rowid).val(lookupDetailId);
	        		$('#componet_featurepop').dialog('destroy');		
					$('#componet_featurepop').remove();
					//Button(action) hide & show 
        			 $("#AddImage_"+rowid).hide();
        			 $("#EditImage_"+rowid).show();
        			 $("#DeleteImage_"+rowid).show();
        			 $("#WorkingImage_"+rowid).hide(); 
	        		 $("#childRowId_"+rowid).remove();
					//Row count+1
					if(showPage=="addadd" || showPage=="editadd"){
						var childCount=Number($('#childCount').val());
						childCount=childCount+1;
						$('#childCount').val(childCount);
						var tempvar=$('.tab>tr:last').attr('id'); 
						var idval=tempvar.split("_");
						var rowid=Number(idval[1]); 
						$('#DeleteImage_'+rowid).hide(); 
					}
	        	}
		 }
		 else{
			 return false;
		 } 
	});


	$('.componet_featurepop').click(function(){ 
	      tempid=$(this).parent().get(0);   
	      $('.ui-dialog-titlebar').remove(); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_component_featuredetails.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.componet_featurepop-result').html(result);  
				},
				error:function(result){ 
					 $('.componet_featurepop-result').html(result); 
				}
			});  
	});
	
	 $('#componet_featurepop').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:500, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true
	 });
});	
</script>