<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="11" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementFeeAddEntry" id="contractAgreementFeeAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.contract.info"/></legend>
		<div>
			<label><fmt:message key="re.contract.assetno"/></label>
			<input type="hidden" name="assetId" id="assetId"  class="width40">
			<input type="text" name="assetNo" id="assetNo" class="width40  validate[required]" disabled="disabled">
			<span id="hit7" class="button" style="width: 40px ! important;">
				<a class="btn ui-state-default ui-corner-all asset-popup"> 
					<span class="ui-icon ui-icon-newwin"> </span> 
				</a>
			</span>
		</div>
		<div>
			<label><fmt:message key="re.contract.assetname"/></label>
			<input type="text" name="assetName" id="assetName" class="width50  validate[required]" disabled="disabled">
		</div>
		<div>
			<label><fmt:message key="re.contract.brand"/></label>
			<input type="text" name="brand" id="brand" class="width50">
		</div>
		<div>
			<label><fmt:message key="re.contract.modelno"/></label>
			<input type="text" name="modelNo" id="modelNo" class="width50">
		</div>
		<div>
			<label><fmt:message key="re.contract.age"/></label>
			<input type="text" name="age" id="age" class="width50  validate[optional,custom[onlyNumber]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
	<fieldset>
		<legend><fmt:message key="re.property.info.linedetails"/></legend> 
		<div>
			<label><fmt:message key="re.contract.lineno"/></label>
			<input type="text" name="lineNo" id="lineNo" disabled="disabled" class="width50 validate[required,custom[onlyNumber]]">
		</div>
		<div><label><fmt:message key="re.contract.buildingno"/>
		<span style="color:red">*</span></label>
		 <select id="buildingId" class="width30 validate[required]" >
		 <option value="">-Select-</option>
		 <c:if test="${requestScope.itemList ne null}">
		      <c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
		          <option value="${building.buildingId }">${building.buildingName }</option>
		      </c:forEach>
		</c:if>
		</select>
			</div>
		<div>
			<label><fmt:message key="re.property.info.componenttype"/><span style="color:red">*</span></label>
			<select id="buildingComponantId" class="width30 validate[required]" >
				<option value="">-Select-</option>
				<c:if test="${requestScope.lookupList ne null}">
					<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
						<option value="${compType.componentType }">${compType.componentTypeName }</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
		<div>
			<label><fmt:message key="re.property.info.flatno"/></label>	 
			<select id="flatId" class="width50" >
				<option value="">-Select-</option>
				<c:if test="${requestScope.beanFlat ne null}">
					<c:forEach items="${requestScope.beanFlat}" var="flat" varStatus="status">
						<option value="${flat.flatId }">${flat.flatNumber}</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
		<div>
			<label><fmt:message key="re.contract.additionaldetails"/></label>
			<input type="text" name="additionalDetails" id="additionalDetails" class="width50">
		</div>
	</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#contractAgreementFeeAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(11)).children().get(1)).show();	//Add Button
			$($($(slidetab).children().get(11)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(11)).children().get(3)).hide();	//Processing Button	 
			 openFlag=0;		
		 });
		 $('#add').click(function(){
			  if($('#contractAgreementFeeAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var buildingId=$('#buildingId').val();
					var buildingName=$('#buildingId :selected').text();
					var buildingComponantId=$('#buildingComponantId').val();
					var componentTypeName=$('#buildingComponantId :selected').text();
					var flatId=$('#flatId').val();
					var flatNumber=$('#flatId :selected').text();
					var additionalDetails=$('#additionalDetails').val();
					var assetId=$('#assetId').val();
					var assetNo=$('#assetNo').val();
					var assetName=$('#assetName').val();
					var brand=$('#brand').val();
					var modelNo=$('#modelNo').val();
					var age =Number($('#age').val());
					var trnVal =$('#transURN').val(); 
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_edit_contract_asset_update.action", 
					 	async: false,
					 	data: {lineNo: lineNo,buildingId: buildingId,buildingComponantId: buildingComponantId,assetId: assetId,flatId: flatId,
							   brand:brand,modelNo:modelNo ,age:age,trnValue:trnVal,actualLineId: actualID,additionalDetails:additionalDetails},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(buildingName);
		        		$($(slidetab).children().get(2)).text(componentTypeName); 
		        		$($(slidetab).children().get(3)).text(flatNumber);
		        		$($(slidetab).children().get(4)).text(additionalDetails); 
		        		$($(slidetab).children().get(5)).text(assetNo); 
		        		$($(slidetab).children().get(6)).text(assetName); 

		        		$($(slidetab).children().get(7)).text(brand); 
		        		$($(slidetab).children().get(8)).text(modelNo); 
		        		$($(slidetab).children().get(9)).text(age); 
		        		//$($(slidetab).children().get(4)).text(remarks);
		        		
		        		$($(slidetab).children().get(12)).text(buildingId); 
		        		$($(slidetab).children().get(13)).text(assetId); 
		        		$($(slidetab).children().get(14)).text(buildingComponantId); 
		        		$($(slidetab).children().get(15)).text(flatId);  
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(11)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(11)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(11)).children().get(3)).hide();	//Processing Button

						 openFlag=0;
	
				  		$($($(slidetab).children().get(10)).children().get(0)).val($('#objLineVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCount=Number($('#childCount').val());
				  		childCount=childCount+1;
						$('#childCount').val(childCount);
				   } 
			  }else{
				  return false;
			  }
		});

		 $('#buildingId').change(function(){
				var buildingId=Number($('#buildingId option:selected').val());
				if(buildingId !=0){
					$('#loading').fadeIn();
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/component_retrive_action.action",
						data:{buildingId:buildingId},
						async:false,
						dataType:"html",
						cache:false,
						success:function(result){
							$("#buildingComponantId").html(result);
						}
					});
					$('#loading').fadeOut();
				}
		});
			
		$('#buildingComponantId').change(function(){
			var buildingId=Number($('#buildingId option:selected').val());
			var componentId=Number($('#buildingComponantId option:selected').val());
			if(buildingId !=0 && componentId !=0){
				$('#loading').fadeIn();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/legal_flatnumber_load.action",
					data:{buildingId: buildingId,componentId: componentId},
					async:false,
					dataType:"html",
					cache:false,
					success:function(result){
						$("#flatId").html(result);
					}
				});
				$('#loading').fadeOut();
			}
		});
			
		 $('.asset-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#contract-popup').dialog('open');
				//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/maintenance_contract_asset_load.action",
				 	async: false, 
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.contract-result').html(result); 
					},
					error:function(result){ 
						 $('.contract-result').html(result); 
					}
				}); 
			});
		 
		$('#contract-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});
			

});
 </script>	