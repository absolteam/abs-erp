<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoFeatureAddEdit" id="propertyInfoFeatureAddEdit">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend></legend>
		<div>
			<label>Measurement</label>
			<input type="text" name="measurement" id="measurement" class="width50  validate[required]">
		</div>
		<div>
			<label>Remarks</label>
			<input type="text" name="remarks" id="remarks" class="width50  validate[required]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
	<fieldset>
		<legend>Feature Details</legend> 
		<div style="display: none;">
			<label>Line No</label>
			<input type="text" name="lineNo" id="lineNo" class="width50 validate[required]" />
		</div>
		<div>
			<label>Feature Code</label>
			<input type="text" name="featureCode" id="featureCode" class="width50 validate[required]" />
		</div>
		<div>
			<label>Features</label>
			<input name="features" id="features" class="width50 validate[required]">
		</div>
	</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;">Cancel</div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;">Save</div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#propertyInfoFeatureAddEdit").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(6)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing button 		
		 });
		 $('#add').click(function(){
			  if($('#propertyInfoFeatureAddEdit').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var featureCode=$('#featureCode').val();
					var features=$('#features').val();
					var measurement=$('#measurement').val();
					var remarks=$('#remarks').val();
									 
					var trnVal =$('#transURN').val(); 
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_edit_component_feature_update.action", 
					 	async: false,
					 	data: {lineNumber: lineNo,featureCode: featureCode,features: features,measurements: measurement, 
							remarks: remarks,trnValue:trnVal, actualLineId: actualID},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(featureCode);
		        		$($(slidetab).children().get(2)).text(features); 
		        		$($(slidetab).children().get(3)).text(measurement); 

		        		$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(6)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button
	
				  		$($($(slidetab).children().get(5)).children().get(0)).val($('#objLineVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
				   } 
			  }else{
				  return false;
			  }
		});

		
});
 </script>	