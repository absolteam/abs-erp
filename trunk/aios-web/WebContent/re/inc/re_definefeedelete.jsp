<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){
	$('#calculatedAs').val($('#tempcalculatedAs').val());
	if($('#endPicker').val() =='31-Dec-9999'){
		$('#endPicker').val("")
	}

	 $("#defineFeeAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

		$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_fee_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

		$('#confirm').click(function(){ 
			var descriptions=$('#descriptions').val().trim();
			if(descriptions!="cheque"){
			feeId=$('#feeId').val();
			$.ajax({
    			type: "POST", 
    			url: "<%=request.getContextPath()%>/define_fee_delete.action",
    			data: {feeId: feeId},  
    	     	async: false,
    			dataType: "html",
    			cache: false,
    			success: function(result)
    			{ 
    				$("#main-wrapper").html(result); 
    			} 
    		}); 
			}
			else
			{
				$('.commonErr').show();
				$('.commonErr').html("Please don't delete cheque!!!");
				return false; 
			}
    		return true;
	 	}); 

		if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
		 $('#startPicker,#endPicker').datepick({
		 	onSelect: customRange, showTrigger: '#calImg'
			});

		// Default Date
		 $('#defaultActualPicker').datepick();
	});
	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
	    var daysInMonth =$.datepick.daysInMonth(
	    $('#selectedYear').val(), $('#selectedMonth').val());
	    $('#selectedDay option:gt(27)').attr('disabled', false);
	    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	    if ($('#selectedDay').val() > daysInMonth) {
	        $('#selectedDay').val(daysInMonth);
	    }
	} 
	function customRange(dates) {
		if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
		}
		else {
			$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
		}
	}

</script>
 <div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definefee"/></div>
				<div class="portlet-content">
					<div>
						<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
							<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
						</c:if>
						<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
							<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
						</c:if>
			 	   </div>
			 	   <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
			 	   <form name="defineFeeAdd" id="defineFeeAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.info"/></legend>						
		                            <div>
		                            	<label><fmt:message key="re.property.info.fromdate"/></label>
		                            	<input type="text" name="startPicker" class="width30" id="startPicker" disabled="disabled" value="${bean.fromDate}"/>
		                            </div>		
									<div>
										<label><fmt:message key="re.property.info.todate"/></label>
										<input type="text" name="endPicker" class="width30" id="endPicker" disabled="disabled" value="${bean.toDate}"/>
									</div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.property.info.feeinfo"/></legend>
									<div style="display:none;"><label>Fee Id No.</label><input type="text" name="feeIdNo" class="width30" id="feeId" disabled="disabled" value="${bean.feeId}"/></div>
									<div><label><fmt:message key="re.property.info.feedescription"/></label><input type="text" name="sno" class="width30" id="descriptions" disabled="disabled" value="${bean.descriptions}"/></div>
									<div><label><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" name="calculatedAs" class="width30" id="tempcalculatedAs" value="${bean.calculatedAs}"/>
										<select id="calculatedAs" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="calculate" varStatus="status">
													<option value="${calculate.calculatedAs }">${calculate.calculatedCode }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>	
									<div><label><fmt:message key="re.property.info.feeamount"/></label><input type="text" name="feeAmount" class="width30" id="feeAmount" disabled="disabled" value="${bean.feeAmount}"/></div>
									<div>
										<c:choose>
											<c:when test="${bean.deposit eq 1 }">
												<input type="checkbox"  checked="checked" name="deposit" disabled="disabled" id="deposit"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="deposit" disabled="disabled" id="deposit" >
											</c:otherwise>
										</c:choose>
									 	<label class=""><fmt:message key="re.property.info.deposit"/></label>
									</div>
							</fieldset> 
					  	</div>
					</form>
				<div class="float-right buttons ui-widget-content ui-corner-all"> 			
					<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.discard"/></div> 
					<div class="portlet-header ui-widget-header float-right" id="confirm"><fmt:message key="re.property.info.confirm"/></div>
			   </div>
			</div>
		</div>
	</div>
			