<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<style>

#DOMWindow{	
	width:85%!important;
	height:88%!important;
} 

</style>
<script type="text/javascript">
$(function(){ 
	useCaseViewCall();
	
});  

function useCaseViewCall(){
	tableName=$('#tableName').val();
	recordId=$('#recordId').val();
	alert("tableName"+tableName+"--recordId--"+recordId);
	url=null;
	if(tableName=="Property")
		url="getPropertyDetal.action";
	else if(tableName=="Property")
		url="getPropertyContractDetail.action";
	else
		return false;
	if(url!=null && url!=""){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+url, 
			data:{format:"HTML",recordId:recordId},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				//alert(result);
				$("#UseCase_View_Top_Result").html(result); 
				$("#UseCase_View_Top").slideDown("slow");

			}
	 	});
	
	}else{
		alert("No Process Defined.");
	}
	
}
</script>
