<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript">
var tempid=""; 
var actionname="";
var slidetab="";
var expenseDetails = "";
$(function(){
	if(Number($('#expenseId').val())>0){ 
	    $('.expenseType').each(function(){
		    if(Number($(this).val()) == Number($('#tempexpenseType').val())){
			    $($(this).attr('checked', true));
			}
		});
	}  
	$('#expenseDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	 
	manupulateLastRow();
		
	 $jquery("#expense_details").validationEngine('attach');

    $('.amount').live('change',function(){
		triggerAddRow(getRowId($(this).attr('id')));
		return false;
	}); 

    $('.delrow').live('click',function(){ 
		 slidetab="";
		 slidetab=$(this).parent().parent().get(0);  
     	 $(slidetab).remove();  
     	 var i=1;
  	 	 $('.rowid').each(function(){   
	  		 var rowId=getRowId($(this).attr('id')); 
	  		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });

    $('.component-popup').click(function(){ 
        tempid=$(this).parent().get(0);  
 	    $('.ui-dialog-titlebar').remove(); 
 	  	$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/property_component_propertyname.action", 
 		 	async: false, 
 		    dataType: "html",
 		    cache: false,
 			success:function(result){   
 				 $('.common-result').html(result);  
 			},
 			error:function(result){  
 				 $('.common-result').html(result); 
 			}
 		});  
 	});
 	 $('#component-popup').dialog({
 		 autoOpen: false,
 		 minwidth: 'auto',
 		 width:500, 
 		 bgiframe: false,
 		 overflow:'hidden',
 		 modal: true
 	});

    $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_property_expense_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 

	 $('#expense_save').click(function(){
		 if($jquery("#expense_details").validationEngine('validate')){   
			 expenseDetails ="";
			 var expenseType = $('input:radio[name=expenseType]:checked').val(); 
			 var date = $('#expenseDate').val();
			 var description =$('#description').val();
			 var expenseId = Number($('#expenseId').val());
			 var expenseNumber = $('#expenseNumber').text().trim();
			 var propertyId=$('#propertyId').val();
			 expenseDetails = getExpenseDetails(); 
			 if(expenseDetails!=null && expenseDetails!=""){
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/saveproperty_expense.action", 
					 	async: false, 
					 	data:{	expenseId: expenseId, voucherNumber: expenseNumber, description: description,
					 			expenseType: expenseType, date: date, propertyId: propertyId, expenseDetails: expenseDetails
					 		 },
					    dataType: "html",
					    cache: false,
						success:function(result){   
							 $(".tempresult").html(result);
							 var message=$('.tempresult').html();  
							 if(message.trim()=="SUCCESS"){
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/get_property_expense.action", 
									 	async: false,
									    dataType: "html",
									    cache: false,
										success:function(result){
											$('#component-popup').dialog('destroy');		
						  					$('#component-popup').remove();  
											$("#main-wrapper").html(result); 
											if(expenseId > 0)
												$('#success_message').hide().html("Property Expense Updated..").slideDown(1000);
											else
												$('#success_message').hide().html("Property Expense Created..").slideDown(1000);
										}	
								 });
							 } 
							 else{
								 $('#page-error').hide().html(message).slideDown(1000);
								 return false;
							 }
						},
						error:function(result){  
							$('#page-error').hide().html("Internal error..").slideDown(1000);
						}
					}); 
				}else{
					$('#page-error').hide().html("Please enter expense detail.").slideDown(1000);
					return false;
				}
			}else{
				return false;
			}
	 });

	 $('#expense_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_property_expense.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#component-popup').dialog('destroy');		
				$('#component-popup').remove();  
				$("#main-wrapper").html(result);  
			}
		 });
	 });
	 var getExpenseDetails = function(){   
		 var paymentDetail="";
 		 var amountarr=new Array();
		 var referenceArray = new Array();
		 var descarr=new Array();
		 var detailId=new Array();
		 $('.rowid').each(function(){ 
				var rowId=getRowId($(this).attr('id'));  
 				 var amount=$('#amount_'+rowId).val();   
				 var lineDescription=$('#linedescription_'+rowId).val(); 
				 var expenseLineId=Number($('#expenseLineId_'+rowId).val()); 
				 var reference = $('#referenceNo_'+rowId).val();
				 if(typeof amount != 'undefined' && amount!=null && 
						 amount!=""){
 					 amountarr.push(amount);
 					if(reference!=null && reference!="")
 						referenceArray.push(reference);
					 else
						 referenceArray.push("##");  
					 detailId.push(expenseLineId);
					 if(lineDescription!=null && lineDescription!="")
						 descarr.push(lineDescription);
					 else
						 descarr.push("##"); 
				 } 
			});
			for(var j=0;j<amountarr.length;j++){ 
				paymentDetail+=amountarr[j]+"__"+descarr[j]+"__"+referenceArray[j]+"__"+detailId[j];
				if(j==amountarr.length-1){   
				} 
				else{
					paymentDetail+="@@";
				}
			}  
			return paymentDetail; 
	 };
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var amt=$('#amount_'+rowId).val();  
	 var nexttab=$('#fieldrow_'+rowId).next();  
	 if(amt!=null && amt!=""
				&& $(nexttab).hasClass('lastrow')){
		 $('#DeleteImage_'+rowId).show();
		 $('.addrows').trigger('click'); 
	 } 
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				<fmt:message key="re.property.propertyExpenseInfo"/>
			</div>	
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 <fmt:message key="re.property.info.generalInfo" />
			</div>	
			<div  style="width: 98%; margin: 10px;">
			<form id="expense_details" style="position: relative;"> 
			<div class="tempresult" style="display: none;"></div>
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset> 
						<input type="hidden" id="expenseId" value="${PROPERTY_EXPENSE.expenseId}"> 							 
						<div style="margin-bottom: 5px;">
							<label class="width30"><fmt:message key="re.property.expense.expenseVoucher" /><span class="mandatory">*</span></label> 
							<c:choose>
								<c:when test="${REFERENCE_NUMBER ne null && REFERENCE_NUMBER ne ''}">
									<span id="expenseNumber" class="width60" style="font-weight: normal;">${REFERENCE_NUMBER}</span>
								</c:when>
								<c:otherwise>
									<span id="expenseNumber" class="width60" style="font-weight: normal;">${PROPERTY_EXPENSE.expenseNumber}</span> 
								</c:otherwise>
							</c:choose>  
						</div> 
						<div class="clearfix"></div>
						<div>
							<label class="width30"> 
							<fmt:message key="re.property.info.propertyName" /><span style="color: red;">*</span></label>
							<input name="propertyName" type="text" readonly="readonly" id="propertyName" value="${PROPERTY_EXPENSE.property.propertyName}" class="validate[required] width50">
							<input type="hidden" id="propertyId" value="${PROPERTY_EXPENSE.property.propertyId}"/>
							<span class="button" style="position: relative; top: 3px;">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all component-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div>  
						<div>
							<label class="width30"><fmt:message key="sys.common.date" /><span class="mandatory">*</span></label> 
							<c:choose>
								<c:when test="${PROPERTY_EXPENSE.date ne null && PROPERTY_EXPENSE.date ne ''}">
									<c:set var="payDate" value="${PROPERTY_EXPENSE.date}"/>  
									<%String date = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
									<input type="text" name="expenseDate" value="<%=date%>" 
									id="expenseDate" readonly="readonly" class="width60 validate[required]" TABINDEX=2> 
								</c:when>  
								<c:otherwise>  
									<input type="text" name="expenseDate"
									id="expenseDate" readonly="readonly" class="width60 validate[required]" TABINDEX=2> 
								</c:otherwise>
							</c:choose> 
						</div>  
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset style="min-height: 68px;"> 	
						<div>
							<label class="width30"><fmt:message key="re.property.expense.expenseType" /><span style="color: red;">*</span></label>
							<c:choose>
						 		<c:when test="${EXPENSE_TYPE ne null && EXPENSE_TYPE ne ''}">
						 			<c:forEach items="${EXPENSE_TYPE}" var="TYPE">
						 				<span style="font-weight: normal;">${TYPE.value}</span>
						 				<input type="radio" name="expenseType" class="expenseType" value="${TYPE.key}"/>
						 			</c:forEach> 
						 		</c:when>
						 	</c:choose> 
						 	<input type="hidden" name="tempexpenseType" id="tempexpenseType" value="${PROPERTY_EXPENSE.expenseType}"/>
						</div>
						<div style="display: none;">
							<label class="width30"><fmt:message key="sys.common.description" /></label>
							<textarea rows="2" id="description" class="width60 float-left" style="position: relative!important;top:-10px!important;">${PROPERTY_EXPENSE.description}</textarea>
						</div> 
					</fieldset> 
				</div> 
				 
				<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
				<fieldset> 	
					<legend><fmt:message key="account.payment.info" /></legend>
					 <table id="hastab" class="width100"> 
								<thead>
								   <tr>   
								   	    <th style="width:1%"><fmt:message key="re.offer.lineNo" /></th>
								   	    <th style="width:3%">Ref No</th>  
 									    <th style="width:3%"><fmt:message key="sys.common.amount" /></th>  
 									    <th style="width:5%"><fmt:message key="sys.common.description" /></th> 
										<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
								  </tr>
								</thead> 
								<tbody class="tab">  
									<c:choose>
										<c:when test="${PROPERTY_EXPENSE_DETAIL ne null && PROPERTY_EXPENSE_DETAIL ne ''}">
											<c:forEach var="PAYMENT_DETAIL" items="${PROPERTY_EXPENSE_DETAIL}" varStatus="status"> 
												<tr class="rowid" id="fieldrow_${status.index+1}"> 
													<td id="lineId_${status.index+1}">${status.index+1}</td>  
													 <td>
														<input type="text" class="width98 referenceNo" value="${PAYMENT_DETAIL.referenceNo}"  id="referenceNo_${status.index+1}"/>
													</td>
													<td>
														<input type="text" class="width98 validate[optional,custom[onlyFloat]] amount" id="amount_${status.index+1}" value="${PAYMENT_DETAIL.amount}" style="text-align: right;"/>
													</td>  
													<td>
													  <input type="text" name="linedescription" id="linedescription_${status.index+1}" value="${PAYMENT_DETAIL.description}"/>
													</td>
													<td style="display:none;">
														<input type="hidden" id="expenseLineId_${status.index+1}" value="${PAYMENT_DETAIL.expenseDetailId}"/>
													</td> 
													 <td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
								 								<span class="ui-icon ui-icon-plus"></span>
														  </a>	
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																<span class="ui-icon ui-icon-wrench"></span>
														  </a> 
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
																<span class="ui-icon ui-icon-circle-close"></span>
														  </a>
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
																<span class="processing"></span>
														  </a>
													</td>   
												</tr>
											</c:forEach>
										</c:when>
									</c:choose> 
									<c:forEach var="i" begin="${fn:length(PROPERTY_EXPENSE_DETAIL)+0}" end="${fn:length(PROPERTY_EXPENSE_DETAIL)+1}" step="1" varStatus="status1"> 
										<tr class="rowid" id="fieldrow_${status1.index+1}"> 
											<td id="lineId_${status1.index+1}">${status1.index+1}</td>   
											<td>
												<input type="text" class="width98 referenceNo"  id="referenceNo_${status.index+1}"/>
											</td>
											<td>
												<input type="text" class="width98 amount" id="amount_${status1.index+1}" style="text-align: right;"/>
											</td>  
											<td>
											  <input type="text" name="linedescription" id="linedescription_${status1.index+1}"/>
											</td>
											<td style="display:none;">
												<input type="hidden" id="expenseLineId_${status1.index+1}"/>
											</td> 
											 <td style="width:0.01%;" class="opn_td" id="option_${status1.index+1}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status1.index+1}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>   
										</tr>
									</c:forEach> 
						 </tbody>
					</table>
					</fieldset>
			</div>	
			</form> 
		</div> 
		<div class="clearfix"></div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
			<div class="portlet-header ui-widget-header float-right" id="expense_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>  
			<div class="portlet-header ui-widget-header float-right" id="expense_save" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>  
		<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div> 
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="component-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div> 
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
	</div>
</div>