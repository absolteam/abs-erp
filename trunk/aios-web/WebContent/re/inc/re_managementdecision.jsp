<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;

 </script>
 
	 <div id="main-content">
	 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Define Decision</div>
	 	<div class="portlet-content">
	 	<div style="float:left;width:48%;" id="hrm" class="create-task">
	 	<fieldset style="min-height:150px">
		<div>
		<label class="width30">Building No</label>	 
		<select><option>-Select-</option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		</select>
		</div>
		<div>
		<label class="width30">Flat No</label>	 
		<select><option>-Select-</option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		<option> Flat Number </option>
		</select>
		</div>
		<div>
	 	<label class="width30">Deposit Amount</label>
	 	<input class="width40" type="checkbox" name="assignby" id="assignby" >
	 	</div>
		<div>
	 	<label class="width30">Rental Amount</label>
	 	<input class="width40" type="checkbox" name="assignby" id="assignby" >
	 	</div>
		<div>
	 	<label class="width30">Percentage</label>
	 	<input class="width20" type="text" name="assignby" id="assignby" >
	 	</div>
	 	<div>
	 	<label class="width30">Holding Rent</label>
	 	<input class="width40" type="checkbox" name="assignby" id="assignby" >
	 	</div>
	 	
		</fieldset>
		</div>
		<div class="width50 float-left" id="hrm">
			<fieldset style="min-height:150px;">
		<div>
	 	<label class="width30">From Date</label>
	 	<input class="width20" type="text" name="assignby" id="assignby" >
	 	</div>
		<div>
	 	<label class="width30">TO Date</label>
	 	<input class="width20" type="text" name="assignby" id="assignby" >
	 	</div>
		<div>
	 	<label class="width30">Increase</label>
	 	<input class="width40" type="checkbox"assignby" id="assignby" >
	 	</div>
		
		<div>
	 	<label class="width30">Decrease</label>
	 	<input class="width40" type="checkbox" name="assignby" id="assignby" >
	 	</div>
	 	<div>
	 	<label class="width30">Holding Flat</label>
	 	<input class="width40" type="checkbox" name="assignby" id="assignby" >
	 	</div>
	 	</fieldset>
		</div>
		
		<div class="float-right buttons ui-widget-content ui-corner-all">
	 			<div class="portlet-header ui-widget-header float-right discard">Cancel</div> 
				<div class="portlet-header ui-widget-header float-right discard">Save</div>
		</div>
		</div>
		</div>
		</div>
		
		
		
		
		
		