<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Property Expense</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PROPERTY EXPENSE</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">EXP.NO.<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${PROPERTY_EXPENSE_PRINT.expenseNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="sys.common.date" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="date" value="${PROPERTY_EXPENSE_PRINT.date}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
				</span>  		
			</div> 
		</div> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="re.property.info.propertyName" /><span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${PROPERTY_EXPENSE_PRINT.property.propertyName} 
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold"><fmt:message key="re.property.expense.expenseType" /> <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:set var="expenseType" value="${PROPERTY_EXPENSE_PRINT.expenseType}"/>
					<%=Constants.RealEstate.PropertyExpenseType.get(Byte.valueOf(pageContext.getAttribute("expenseType").toString()))%>		
				</span>
			</div> 
		</div>
	</div>
	<div style="clear: both;"></div>
	<div style="position:relative; top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<table class="width100">
			<tr> 
			   <th style="width:1%"><fmt:message key="re.offer.lineNo" /></th>
		   	   <th style="width:10%">Ref No</th>   
			   <th style="width:3%"><fmt:message key="sys.common.amount" /></th>     
			   <th style="width:5%"><fmt:message key="sys.common.description" /></th>  
			</tr>
			<tbody>  
				<c:choose>
					<c:when test="${PROPERTY_EXPENSE_DETAIL_PRINT ne null && PROPERTY_EXPENSE_DETAIL_PRINT ne ''}">
						<c:set var="totalAmount" value="0"/>
						<tr style="height: 25px;">
							<c:forEach begin="0" end="3" step="1">
								<td/>
							</c:forEach>
							<c:forEach items="${PROPERTY_EXPENSE_DETAIL_PRINT}" var="PAYMENT_DETAIL" varStatus="status"> 
							<tr>
								<td>${status.index+1}</td>
								 <td> ${PAYMENT_DETAIL.referenceNo}</td>      
								<td>
									${PAYMENT_DETAIL.amount}
								</td>  
								<td>
									${PAYMENT_DETAIL.description}
								</td> 
								<c:set var="totalAmount" value="${totalAmount+ PAYMENT_DETAIL.amount}"/>
							</tr> 
						</c:forEach> 
						<c:if test="${fn:length(PROPERTY_EXPENSE_DETAIL_PRINT)<5}">
								<c:set var="emptyLoop" value="${5 - fn:length(PROPERTY_EXPENSE_DETAIL_PRINT)}"/> 
								<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
									<tr style="height: 25px;">
										<c:forEach begin="0" end="3" step="1">
											<td/>
										</c:forEach>
									</tr>
								</c:forEach>
						</c:if> 
						</tr> 
						<tr>
							<td colspan="3" class="bottomTD left_align" style="font-weight: bold;">TOTAL AMOUNT</td> 
							<td class="right_align bottomTD" style="font-weight: bold;">
								<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount")) %> 
							</td>
						</tr> 
					</c:when>
				</c:choose> 
			</tbody>
		</table>
	</div>  
	 
	<div class="width100 float_left div_text_info" style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span> 
			</div> 
			<div class="width40 float_left">
				<span>Recommended by:</span> 
			</div>
			<div class="width30 float_left">
				<span>Approved by:</span> 
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
		</div>
	</div>
	</body>
</html>