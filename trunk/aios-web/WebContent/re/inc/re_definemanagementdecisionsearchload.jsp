<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div><label><fmt:message key="re.property.info.buildingname"/></label>
	<select id="buildingId" style="width: 35.5%;">
		<option value="">-Select-</option>
		<c:if test="${requestScope.itemList ne null}">
			<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
				<option value="${building.buildingId }">${building.buildingName }</option>
			</c:forEach>
		</c:if>
	</select>
</div>
<div><label><fmt:message key="re.property.info.flatnumber"/></label>
	<select id="flatId" style="width: 35.5%;">
		<option value="">-Select-</option>
	</select>
</div>
<div id="temp" style="display:none;">
	<label class=""><fmt:message key="re.common.approvedstatus"/></label>
	<select id="approvedStatus" style="width: 35.5%;">
		<option value="">-Select-</option>
		<c:forEach items="${requestScope.approvedStatus}" var="approvedStatus" varStatus="status">
	<option value="${approvedStatus.approvedStatus }">${approvedStatus.approvedStatusName}</option>
	</c:forEach>
		</select>
</div>

<script type="text/javascript">
$(document).ready(function (){  
$('#buildingId').change(function(){
			var buildingId=$('#buildingId').val();
			$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/flat_retrive_search_action.action",
	    		data:{buildingId:buildingId},
	    		async:false,
	    		dataType:"html",
	    		cache:false,
	    		success:function(result){
	    			$("#flatId").html(result);
	    		}
	    	});
		});
});
</script>
		











	