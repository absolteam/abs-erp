<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var buildingName="";
$(document).ready(function (){
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-propertyflat.action", 
		 datatype: "json", 
		 colNames:['propertyFlatId','buildingId','Component Type','Building Name','From Date','To Date','Total Area','component Type Name'], 
		 colModel:[ 
				{name:'propertyFlatId',index:'PROP_FLAT_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'componentType',index:'COMPONENT_TYPE', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME', width:100,  sortable:true},
				{name:'fromDate',index:'FROM_DATE', width:100,  sortable:true},
		  		{name:'todate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'totalArea',index:'TOTAL_AREA', width:100,  sortable:true},
		  		 {name:'componentTypeName',index:'COMPONENT_TYPE', width:150,sortable:true, sorttype:"data"},
		  	 ], 		  				  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'PROP_COM_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:"Owner Information"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["propertyFlatId","buildingId"]);

 	
 	$("#add").click( function() { 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/add_property_component.action",  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		
		}); 
		return true; 
	});

 	
	
	$("#edit").click( function() { 
	    var s;
	    s =  $("#list2").jqGrid('getGridParam','selrow');
	    //alert("s = " + s);
		if(s == null )
		{
			alert("Please select one row to edit");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);
			componentId = s2.componentId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/edit_property_component.action",  
		     	data: {componentId: componentId}, 
		     	async: false,
				dataType: "html",
				cache: false,
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true;	
		} 
		return false;
	});
	$("#delete").click(function(){
		var s = $("#list2").jqGrid('getGridParam','selrow');
		if(s == null || s=='null')
		{
			alert("Please select one row to delete");
		}
		else
		{			
			var s2 = $("#list2").jqGrid('getRowData',s);  
			componentId = s2.componentId; 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/delete_confirm_property_component.action",  
		     	data:{componentId: componentId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
		     		$("#main-wrapper").html(data);
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data); //gridDiv main-wrapper
		     	}
			});
			return true; 
		} 
		return false; 
	});
	$("#search").click(function(){
		if( (buildingName != null && buildingName.trim().length > 0)){
		buildingName=$('#buildingName').val();
		var tempExp= /%/;
		 var tempMatch=buildingName.search(tempExp);
		 if(tempMatch!= -1 ){
			 while(tempMatch!= -1){
				 buildingName=buildingName.replace('%','$');
				 tempMatch=buildingName.search(tempExp);
			 }
		 }else{
			 buildingName='$'+buildingName+'$';
		 }
		if( (buildingName != null && buildingName.trim().length > 0)) // check atleast one textbok selected
		
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/search_property_component.action",  
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
					$('.commonErr').hide();
		     		alert("error = " + $(data));
				},
		     	success: function(data)
		     	{
					$("#gridDiv").html(data);  //gridDiv main-wrapper
		     	}
			});
		}
		else
		{
			$('.commonErr').show();
			$('.commonErr').html("Please give a valid input to search !!!");
			return false; 
		}
	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_component_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
});
</script>
<body>
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property Component</div>	 
			<div class="portlet-content">
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
					<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
					<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
					<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				 </div>
				<div style="margin-bottom:10px;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width85"> 
					<div class="float-left" style="margin-top:10px;margin-left:5px;">
						<label style="font-weight:bold;">Name</label> <input type="text" id="buildingName" />
					</div>
					<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel">CANCEl</div> 
					<div class="portlet-header ui-widget-header float-right mou-cursor" id="search">SEARCH</div>
		 		</div>
				<div id="gridDiv">		
					<table id="list2"></table>  
					<div id="pager2"> </div> 
				</div>
				
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right"  id="delete">DELETE</div>
					<div class="portlet-header ui-widget-header float-right"  id="edit">EDIT</div>
					<div class="portlet-header ui-widget-header float-right"  id="add">ADD</div>
				</div>
			</div>
		</div>
	</div>
</body>