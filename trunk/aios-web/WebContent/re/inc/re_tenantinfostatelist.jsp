<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${stateList != null }">
		<c:forEach items="${stateList}" var="stateBean" varStatus="status" >
		<option value="${stateBean.stateId}">${stateBean.stateName}</option>
	</c:forEach> 
	</c:when>
</c:choose>
<script type="text/javascript">
//Get City List On Change State List
$('#state').change(function(){
	stateId=$('#state').val();
	$.ajax({
		type: "GET", 
		url: "<%=request.getContextPath()%>/tenant_info_city_load.action", 
     	async: false,
     	data:{stateId: stateId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$("#city").html(result); 
		}, 
		error: function() {
		}
	}); 		
});

</script>