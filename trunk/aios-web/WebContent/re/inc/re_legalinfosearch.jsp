<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-legal_info_search.action?buildingId="+buildingId+'&contractId='+contractId+'&caseIdCourt='+caseIdCourt+'&description='+description+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['Case Id',
		  		 	'<fmt:message key="re.property.info.caseidincourt"/>',
		  		 	'<fmt:message key="re.property.info.description"/>',
		  		 	'Building ID',
		  		 	'<fmt:message key="re.property.info.buidingname"/>',
		  		 	'Contract ID',
		  		 	'<fmt:message key="re.property.info.contractnumber"/>',
		  		 	'<fmt:message key="re.property.info.fromdate"/>',
			  		 '<fmt:message key="re.property.info.todate"/>',
			  		'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'caseId',index:'CASE_ID', width:50,  sortable:true},
				{name:'caseInCourt',index:'CASE_ID_COURT', width:100,  sortable:true},
				{name:'description',index:'CASE_DESCRIPTION', width:150,sortable:true, sorttype:"data"} ,
				{name:'buildingId',index:'BUILDING_ID', width:150,sortable:true, sorttype:"data"},
				{name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
				 {name:'contractNumber',index:'CONTRACT_NO', width:100,sortable:true, sorttype:"data"},
				{name:'fromDate',index:'FROM_DATE', width:100,sortable:true, sorttype:"data"} ,
				{name:'toDate',index:'TO_DATE', width:100,  sortable:true},
				{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'CASE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.legalinformation"/>'
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["caseId","contractId","buildingId"]);
   });  
 	</script>
	<body>
	<input type="hidden" id="bankName"/> 
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body>