<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <script type="text/javascript" src="/js/dateDifference.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
 </script>
 

<script type="text/javascript">
$(function (){  
	$('#tenantType').val($('#tenantTypeTemp').val());
	//$('#paymentMethod').val($('#paymentMethodTemp').val());
	var paymentMethodArray=String($('#paymentMethodTemp').val()).split(',');
	for(i=0;i<=paymentMethodArray.length;i++)
		 $('#paymentMethod').find("option[value="+paymentMethodArray[i]+"]").attr('selected','selected');
	//Checking Day, Month & Year 
	fromDateVar=$('#startPicker').val();
	toDateVar=$('#endPicker').val();
	if(fromDateVar != '' && toDateVar != ''){
		fromDate=fromDateVar;
		toDate=toDateVar;
		putYears=$('#years');
		putMonths=$('#months');
		putDays=$('#days');
		calculateDate(fromDate,toDate,putYears,putMonths,putDays);
		if($('#years').val()=="NaN" && " "){
			$('#years').val("0");
		}
		if($('#months').val()=="NaN" && " "){
			$('#months').val("0");
		}
		if($('#days').val()=="NaN" && " "){
			$('#days').val("0");
		}
	}
	// Calculation for Total Amount  Start Here
	var total=0;
	$('.rentAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#rentAmountTotal').val(total);
	
	total=0;
	$('.contractAmountCalc').each(function(){
		total+=Number($(this).html());
	});
	$('#contractAmountTotal').val(total);
	$('#rentAmountTotalAll').val(total);
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc
	
	total=0;
	$('.feeAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#feeAmountTotal').val(total)
	//$('#cashAmount').val(total); 
	$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	total=0;
	$('.chequeAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#chequeAmountTotal').val(total);
	$('#chequeAmountTotalAll').val(total)
	$('#chequeAmount').val(total); 
	$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc

	$('#cashAmountTotal').val($('#cashAmount').val());

	total=0;
	$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc != 'Rent'){
				total+=Number($($(this).siblings().get(2)).html());
			}
	});
	$('#otherCharges').val(total);

	$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc
	// Calculation End here
	 
	$('.formError').remove(); 
	 $("#contractAgreementEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	 $("#confirm").click(function(){
		 contractId=$('#contractId').val();
			//alert("contractId : "+contractId);
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_contract_agreement.action",
				data: {contractId: contractId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
				{ 
					$("#main-wrapper").html(result); 
				} 
			}); 
			return true;
		});
	

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_agreement_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
	

	$('.dateCheck').blur(function(){
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}
	});

	
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});

 	$.fn.globeTotal = function() { 	// Total for Rent & Contract Amount
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total)
		total=0;
		$('.contractAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		$('#contractAmountTotal').val(total); 

	 }
 	$.fn.globeTotalFee = function() { 	// Total for Fee Amount
		var total=0;
		$('.feeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#feeAmountTotal').val(total)
		$('#cashAmount').val(total); 
		$('#grandTotal').val(Number($('#cashAmount').val())+Number($('#chequeAmount').val()));

	 }
 	$.fn.globeTotalCheque = function() { 	// Total Payment Amount
		var total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#chequeAmountTotal').val(total)
		$('#chequeAmount').val(total); 
		$('#grandTotal').val(Number($('#cashAmount').val())+Number($('#chequeAmount').val()));

	 }
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractagreement"/></div>
	 	<div class="portlet-content">
		 	<form id="contractAgreementEdit">
		 		<fieldset>
		 			<div class="width50 float-left" id="hrm">
					 	<fieldset>
					 		<legend><fmt:message key="re.contract.tenantdeatails"/></legend>
							<div>
								<label class="width30"><fmt:message key="re.contract.offerno"/></label>	 
								<input class="width40" type="text" name="offerNumber" value="${bean.offerNumber }" id="offerNumber" disabled="disabled" >
								<input class="width40" type="hidden" name="offerId" value="${bean.offerId }"  id="offerId" disabled="disabled" >
								<span id="hit7" class="button" style="width: 40px ! important;">
									<a class="btn ui-state-default ui-corner-all offer-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.buildingname"/></label>	
								<input class="width40 validate[required]" type="text" name="buildingName"  value="${bean.buildingName }" id="buildingName" disabled="disabled" > 
								<input class="width40" type="hidden" name="buildingId" value="${bean.buildingId }"  id="buildingId" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.tenantname"/></label>	 
								<input class="width40" type="text" name="tenantName" value="${bean.tenantName }"  id="tenantName" disabled="disabled" >
								<input class="width40" type="hidden" name="tenantId" value="${bean.tenantId }"  id="tenantId" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.address"/></label>	 
								<input class="width40" type="text" name="presentAddress" value="${bean.presentAddress }"  id="presentAddress" disabled="disabled" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.address1"/></label>	 
								<input class="width40" type="text" name="permanantAddress" value="${bean.permanantAddress }"  id="permanantAddress" disabled="disabled" >
							</div>
							<div>
						</fieldset>
					</div>
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="re.contract.periodofrent"/></legend>
								 
							<div><label class="width30" id="fromDateCheck"><fmt:message key="re.contract.fromdate"/></label>
							<input type="text" class="width40 dateCheck " id="startPicker" value="${bean.fromDate }"  disabled="disabled" name="employerFromDate" tabindex="7" readonly="readonly">
							</div>
							
							<div><label class="width30"><fmt:message key="re.contract.todate"/></label>
							<input type="text" class="width40 employerToDate dateCheck" id="endPicker" value="${bean.toDate }"  disabled="disabled" name="employerToDate" tabindex="8" readonly="readonly">
							</div>
							<div><label class="width30"><fmt:message key="re.contract.years"/></label><input type="text" class="width40" name="years" id="years" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.months"/></label><input type="text" class="width40" name="months" id="months" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.days"/></label><input type="text" class="width40" name="days" id="days"  disabled="disabled"></div>
						</fieldset>
					</div>
					<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:134px;">
							<legend><fmt:message key="re.contract.contractdetails"/></legend>
							<div >
								<label class="width30"><fmt:message key="re.contract.contractno"/></label>	 
								<input class="width40" type="text" name="contractNumber" value="${bean.contractNumber }"  id="contractNumber" disabled="disabled" >
								<input class="width40" type="hidden" name="contractId" value="${bean.contractId }"  id="contractId" disabled="disabled" >
							</div>
							<div >
								<label class="width30"><fmt:message key="re.contract.contractdate"/></label>	 
								<input class="width40 contractDate" type="text" name="contractDate" disabled="disabled" value="${bean.contractDate }"  id="defaultPopup" readonly="readonly" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.tenanttype"/></label>
								<input type="hidden" class="width40" name="tenantTypeTemp"  value="${bean.tenantTypeId }" id="tenantTypeTemp">	 
								<select class="width40" name="tenantType"  disabled="disabled" id="tenantType">
									<option value="">Select</option>
									<c:forEach items="${requestScope.itemList}" var="tenant" varStatus="status">
										<option value="${tenant.tenantTypeId }">${tenant.tenantTypeName }</option>
									</c:forEach>
								</select>
							</div>
							<div >
								<label class="width30"><fmt:message key="re.contract.paymentmethod"/></label>	
								<input type="hidden" class="width40" name="paymentMethodTemp" disabled="disabled"  value="${bean.paymentMethod }" id="paymentMethodTemp"> 
								<select id="paymentMethod" multiple="multiple"  disabled="disabled">
									<option value="C">Cash</option>
									<option value="Q">Cheque</option>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.noofcheque"/></label>	 
								<input class="width40" type="text" name="noOfCheques" value="${bean.noOfCheques }" disabled="disabled"  id="noOfCheques" >
							</div>
						</fieldset>
					</div>
				</fieldset>
			</form>
			<div class="clearfix"></div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.rentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm">
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.lineno"/></th> 
												<th class="width20"><fmt:message key="re.contract.flatno"/></th>
												<th class="width20"><fmt:message key="re.contract.rentamount"/></th>
												<th class="width20"><fmt:message key="re.contract.contractamount"/></th>
												<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tab">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="even"> 
													<td class="width20">${result.lineNumber }</td>
													<td class="width20">${result.flatNumber }</td>	
													<td class="width20 rentAmountCalc">${result.rentAmount }</td> 
													<td class="width20 contractAmountCalc">${result.contractAmount }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.contractRentId }" name="actualLineId" id="actualLineId"/>	
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
											
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:56%">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="rentAmountTotal" id="rentAmountTotal" class="width40" disabled="disabled" >
							 	<input name="contractAmountTotal" id="contractAmountTotal" class="width40" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div style="display:none" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;">Add Row</div> 
					</div>
				</div>
			</div>	
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.feedetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div>   
						<div id="warningMsgFee" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
					 		<input type="hidden" name="childCountFee" id="childCountFee"  value="${countSize2}"/> 
							<div id="hrm">
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.lineno"/></th> 
												<th class="width20" style="display:none"><fmt:message key="re.contract.feeid"/></th>
												<th class="width20"><fmt:message key="re.contract.description"/></th>
												<th class="width20"><fmt:message key="re.contract.amount"/></th>
												<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabFee" style="">	
											<c:forEach items="${requestScope.result2}" var="result2" >
											 	<tr class="even"> 
													<td class="width20">${result2.lineNumber }</td>
													<td class="width20" style="display:none">${result2.feeId }</td>	
													<td class="width20">${result2.feeDescription }</td> 
													<td class="width20 feeAmountCalc">${result2.feeAmount }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result2.contractFeeId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFee" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFee"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFee" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
											
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:33%">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="feeAmountTotal" id="feeAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div style="display:none" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfee" style="cursor:pointer;">Add Row</div> 
					</div>
				</div>
			</div>
			
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.paymentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value="${countSize3}"/> 
							<div id="hrm">
								<div id="hrm" class="hastable width100" > 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.bankname"/></th> 
												<th class="width20"><fmt:message key="re.contract.chequedate"/></th>
												<th class="width20"><fmt:message key="re.contract.chequeno"/></th>
												<th class="width20"><fmt:message key="re.contract.chequeamount"/></th>
												<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabOwner" style="">	
											<c:forEach items="${requestScope.result3}" var="result3" >
											 	<tr class="even"> 
													<td class="width20">${result3.bankName }</td>
													<td class="width20">${result3.chequeDate }</td>	
													<td class="width20">${result3.chequeNumber }</td> 
													<td class="width20 chequeAmountCalc">${result3.chequeAmount }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result3.contractPaymentId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>		
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
											
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:33%">
								<label>Total</label>
							 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
						<div class="portlet-header ui-widget-header float-left addrowspayment">Add Row</div> 
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="width60 float-left" id="hrm">
				<fieldset>
					<legend><fmt:message key="re.contract.cashamount"/></legend>
					<div>
						<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.contract.cashamount"/></label>	 
						<input class="width30" type="text" name="cashAmount" value="${bean.cashAmount }"  id="cashAmount" disabled="disabled">
					</div>
					<div>
						<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.property.info.description"/></label>	 
						<input class="width30" type="text" name="description"  id="description" value="${bean.description }" disabled="disabled">
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="width60 float-left" id="hrm">
				<fieldset style="min-height:134px;">
					<legend><fmt:message key="re.contract.grossdetails"/></legend>
					<div style="display:none;">
						<label class="width30"><fmt:message key="re.contract.chequeamount"/></label>	 
						<input class="width30" type="text" name="chequeAmount"  id="chequeAmount" disabled="disabled" >
					</div>
					<div>
						<label class="width20">Total Rent</label>	 
						<input class="width20 float-left" type="text" name="rentAmountTotalAll"  id="rentAmountTotalAll" disabled="disabled" >
					
						<label class="width20">Total Cash Amount</label>	 
						<input class="width20" type="text" name="cashAmountTotal"  id="cashAmountTotal" disabled="disabled" >
						
					</div>
					<div>
						<label class="width20">Other Charges</label>	 
						<input class="width20 float-left" type="text" name="otherCharges"  id="otherCharges" disabled="disabled" >
						<label class="width20" style="font-size: 11px ! important;">Total Cheque Payment</label>	 
						<input class="width20" type="text" name="chequeAmountTotalAll"  id="chequeAmountTotalAll" disabled="disabled" >
					</div>
					<div>
						<label class="width20">Grand Fees</label>	 
						<input class="width20 float-left" type="text" name="grandFees"  id="grandFees" disabled="disabled" >
						<label class="width20"><fmt:message key="re.contract.grandtotal"/></label>	 
						<input class="width20" type="text" name="grandTotal"  id="grandTotal" disabled="disabled" >
					</div>
					<div style="display:none;">
						<label class="width30">Amount in Words Only</label>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div class="portlet-header ui-widget-header float-right discard" style="display:none;"><fmt:message key="re.contract.release"/></div> 
			<div class="portlet-header ui-widget-header float-right discard" id="cancel"><fmt:message key="re.contract.cancel"/></div> 
		 	<div class="portlet-header ui-widget-header float-right save-all" id="confirm"><fmt:message key="re.contract.confirm"/></div> 
		</div>
	</div>
</div>

