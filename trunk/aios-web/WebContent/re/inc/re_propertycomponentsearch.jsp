<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-property_component_search.action?buildingName="+buildingName,
		 datatype: "json", 
		 colNames:['componentId','BuildingId','BuildingName','Component Type','Component Type','No.of Component','No Of Flat','FromDate','ToDate','Total Area'], 
		 colModel:[ 
				{name:'componentId',index:'PROP_COM_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDINT_ID', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDINT_NAME', width:100,  sortable:true},
				{name:'componentType',index:'COMPONENT_TYPE', width:100,  sortable:true},
				{name:'componentTypeName',index:'COMPONENT_TYPE', width:100,  sortable:true},
		  		{name:'noOfComponent',index:'NO_OF_COMPONANT', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'noOfFlat',index:'NO_OF_FLAT', width:100,  sortable:true},
		  		 {name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'toDate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'totalArea',index:'TOTAL_AREA', width:150,sortable:true, sorttype:"data"},
			 ], 		  				  		  
		 rowNum:5, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'PROP_COM_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:"Owner Information"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["propertyId","noOfFlat","componentType","buildingId"]);
	});
 	</script>
	 </head>
	 <body>
	<input type="hidden" id="bankName"/> 
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body>
 	
 	