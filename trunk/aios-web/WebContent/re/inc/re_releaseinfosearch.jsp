<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-release_info_search.action?buildingId="+buildingId+'&contractId ='+contractId+'& tenantId ='+tenantId+'& contractDate ='+contractDate+'& paidAmount ='+paidAmount+'&buildingsId ='+buildingsId,
		 datatype: "json", 
		 colNames:['Release ID','Contract ID','Contract Number','Building ID','Building Name','Building Number','Tenant ID','Tenant Name','Contract Date','Amount'], 
		 colModel:[ 
				{name:'releaseId',index:'RELEASE_ID', width:100,  sortable:true},
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
				{name:'contractNumber',index:'CONTRACT_NO', width:100,  sortable:true},
		  		{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		{name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'buildingNumber',index:'BUILDING_NUMBER', width:100,sortable:true, sorttype:"data"},
		  		{name:'tenantId',index:'CUSTOMER_ID', width:150,sortable:true, sorttype:"data"},
		  		{name:'tenantName',index:'TENANT_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:100,sortable:true, sorttype:"data"},
		  		{name:'paidAmount',index:'PAID_AMOUNT', width:100,sortable:true, sorttype:"data"}
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'RELEASE_ID', 
		 viewrecords: true, 
		 sortorder: "asc", 
		 caption:"Release Information"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["releaseId","contractId","buildingId","customerId","tenantId","paidAmount"]);
	});  
	</script>
	 <body>
	<input type="hidden" id="bankName"/> 
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body>
 	