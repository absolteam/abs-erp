<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
   <script type="text/javascript" src="/js/thickbox-compressed.js"></script>
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}

$(function (){  
	$('#country').val($('#countryTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#tenanttype').val($('#tenantTemp').val());
	
	
	$("#tenantInformationEdit").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

	$("#close").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/tenant_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 
}); 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.tenantinformation"/></div>
			<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>
			<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
		    		<input type="hidden" id="notificationType" name="notificationType" value="${requestScope.notificationType}"/>
					<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
					<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
					<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
					<input class="width40" type="hidden" name="sessionpersonId" value="${bean.sessionPersonId }" id="sessionPersonId">
			<form name="tenantInformationEdit" id="tenantInformationEdit"> 
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.owner.info.tenantinformation2"/></legend>		
							<input type="hidden" name="birthdate" class="width30" id="tenantId" disabled="disabled" value="${bean.tenantId}" >			
                            <div>
                            	<label  style="font-family: Arial,Verdan,Sans-Serif;font-size: 87%;">
                            		<fmt:message key="re.owner.info.birthdate"/>
                            	</label>
                            	<input type="text" name="birthdate" class="width30" id="birthdate" disabled="disabled" value="${bean.birthDate}" ></div>		
							<div>
								<label><fmt:message key="re.owner.info.proffession"/></label>
								<input type="text" name="sno" class="width30" id="profession" value="${bean.profession}" disabled="disabled">
							</div>
							<div><label class=""><fmt:message key="re.owner.info.country"/></label>
									<input type="hidden" id="countryTemp" name="countryTemp" value="${bean.countryId}" class="width30">
									<select id="country" class="width30" disabled="disabled">
									<option value="">-Select-</option>
										<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
											<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
										</c:forEach>
									</select>
							</div>
					 <div>
						<label class=""><fmt:message key="re.owner.info.state"/></label>
						<input type="hidden" id="stateTemp" name="cityTemp" value="${bean.stateId }" class="width30">
							<select class="width30" id="state" disabled="disabled">
								<option value="">-Select-</option>
								<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
									<option value="${statelist.stateId }">${statelist.stateName}</option>
								</c:forEach>
							</select>
					</div>
					<div>
						<label class=""><fmt:message key="re.owner.info.city"/></label>
						<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" class="width30">
						<select  class="width30" id="city" disabled="disabled">
							<option value="">-Select-</option>
							<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
								<option value="${citylist.cityId }">${citylist.cityName}</option>
							</c:forEach>
						</select>
					</div>																					
					<div>
						<label><fmt:message key="re.owner.info.id"/></label>
						<input type="text" name="uaeidno" class="width30" id="uaeidno" value="${bean.uaeIdNo}" disabled="disabled">
					</div>
					<div>
						<label><fmt:message key="re.owner.info.description"/></label>
						<input type="text" name="uaeIdDescription" class="width30" id="uaeIdDescription" value="${bean.uaeIdDescription}" disabled="disabled">
					</div>
					<div>
						<label><fmt:message key="re.owner.info.postboxno"/></label>
						<input type="text" name="postBoxNo" class="width30" id="postBoxNo"  value="${bean.postBoxNo}" disabled="disabled">
					</div>
					<div>
						<label><fmt:message key="re.owner.info.emailid"/></label>
						<input type="text" name="emailid" class="width30" id="emailid" value="${bean.emailId}" disabled="disabled">
					</div>
                          <div><label><fmt:message key="re.owner.info.mobileno"/></label><input type="text" name="mobileno" class="width30" maxlength="12" id="mobileno" value="${bean.mobileNo}" disabled="disabled"></div>
                          <div>
                          	<label><fmt:message key="re.owner.info.landlineno"/></label>
                          	<input type="text" name="landlineno" class="width30 validate[optional,custom[onlyNumber]"  id="landlineno" disabled="disabled" value="${bean.landLineNo}">
                          </div>
                          <div>
                          	<label><fmt:message key="re.owner.info.website"/></label>
                          	<input type="text" name="website" class="width30" id="website" value="${bean.website}" disabled="disabled">
                          </div>
                          <div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
						<div id="UploadDmsDiv" class="width100">
							<label ><fmt:message key="re.property.info.imageupload"/></label>
							<input type="text" id="URNdms" value="${bean.image }" disabled="disabled" class="width30">
							<div class="float-right buttons ui-widget-content ui-corner-all" style="">
	                            <div  id="dms_document_information" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
	                        </div>
						</div>
		    		</div>
					<div  class="Err response-msg error ui-corner-all" style="width:80%; display:none;"></div>	
					</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.owner.info.tenantinformation1"/></legend>
							<input type="hidden" name="tenantno" class="width30 validate[required]" id="tenantno" disabled="disabled">
							<div>
								<label><fmt:message key="re.owner.info.tenantname"/></label>
								<input type="text" name="tenantname" class="width30" id="tenantname" value="${bean.tenantName}" disabled="disabled">
							</div>
							<div>
								<label><fmt:message key="re.owner.info.tenantdescription"/></label>
								<input type="text" name="tenantDescription" class="width30" id="tenantDescription" value="${bean.tenantDescription}" disabled="disabled">
							</div>
							<div><label><fmt:message key="re.owner.info.tenanttype"/></label>
								<input type="hidden" name="tenantTemp" id="tenantTemp" value="${bean.tenantType}">
									<select id="tenanttype" class="width30" disabled="disabled">
									<option value="">-Select-</option>
									<c:if test="${requestScope.lookupList ne null}">
								<c:forEach items="${requestScope.lookupList}" var="tenType" varStatus="status">
									<option value="${tenType.tenantType}">${tenType.description}</option>
								</c:forEach>
							</c:if>
									</select>
							</div>	
					</fieldset> 
					
					<fieldset>
	                    <legend><fmt:message key="re.owner.info.presentaddress"/></legend>
	                    <div style="height:25px" class="width60 float-left">
	                            <div id="barbox">
	                                  <div id="progressbar"></div>
	                            </div>
	                            <div id="count">500</div>
	                      </div>
	                      <p>
	                        <textarea class="width60 float-left tooltip" id="text_area_present_input" disabled="disabled">${bean.presentAddress}</textarea>
	                      </p>
	                 </fieldset>
					 <fieldset>
	                    <legend><fmt:message key="re.owner.info.permanentaddress"/></legend>
	                    <div style="height:25px" class="width60 float-left">
	                            <div id="barbox">
	                                  <div id="progressbar1"></div>
	                            </div>
	                            <div id="count1">500</div>
	                      </div>
	                      <p>
	                        <textarea class="width60 float-left tooltip" id="text_area_permanent_input" title="Please Enter Permanent Address" disabled="disabled">${bean.permanentAddress}</textarea>
	                      </p>
	                 </fieldset>								 
				</div>
				<div class="portlet-content">
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="accounts.suppliersite.label.accountinginformation"/></legend>
								<div class="width90 float-left"><label class="width30">Tenant Account Code<span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.liabilityAccountCode}" name="liabilityAccountCode" id="liabilityAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_liability_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a  id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>
								</div>
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.advanceaccountcode"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.advanceAccountCode}" name="advanceAccountCode" id="advanceAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_advance_account_action"/>
									<span  class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>	
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.clearningacccount"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.clearningAccountCode}" name="clearningAccountCode" id="clearningAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_clearning_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>																
						</fieldset> 
					</div>	
				</div>
				<div class="clearfix"></div>
				<div class="width50 float-left" style="margin:5px;" id="hrm">
					<fieldset>
						<legend>Supervisor Comments</legend>
						<textarea id="comments" class="width100" disabled="disabled">${bean.comments}</textarea>
					</fieldset>
				</div>
				<div class="clearfix"></div>			
			<div class="float-right buttons ui-widget-content ui-corner-all">
		 			<div  id="close" class="portlet-header ui-widget-header float-right"><fmt:message key="hr.common.button.close"/></div> 
			</div>
			</form>
		</div>
	</div>
</div>