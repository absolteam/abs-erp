<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link href="${pageContext.request.contextPath}/css/offer/offer_form.css" type="text/css" media="all" rel="stylesheet">    
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:80%; float:left;}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
.ui-autocomplete{
	width:194px!important;
	height:300px;
	overflow-y:auto; 
}
.offer-tooltip-css {
	color: #000000; outline: none;
	text-decoration: none;
	position: relative;
	cursor: pointer;
}
.offer-tooltip-css span {
	margin-left: -999em;
	position: absolute;
}

.tooltip-csss {
	color: #000000; outline: none;
	text-decoration: none;
	position: relative;
	cursor: pointer;
}
.tooltip-csss span {
	margin-left: -999em;
	position: absolute;
}

.tooltip-csss:hover span {
	border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); 
	position: absolute; left: 1em; top: 2em; z-index: 99;
	margin-left: 0; width: 150px;
}

.tooltip-csss:hover span {
	border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); 
	position: absolute; left: 1em; top: 2em; z-index: 99;
	margin-left: 0; width: 150px;
}

.offer-tooltip-css:hover span {
	border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
	box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); 
	position: absolute; left: 1em; top: 2em; z-index: 99;
	margin-left: 0; width: 150px;
}
.offer-offer-tooltip-css:hover img {
	border: 0; margin: -10px 0 0 -55px;
	float: left; position: absolute;
}
.offer-tooltip-css:hover em {
	font-family: Candara, Tahoma, Geneva, sans-serif; font-size: 1.2em; font-weight: bold;
	display: block; padding: 0.2em 0 0.6em 0;
}
.classic { padding: 0.8em 1em; }
.custom { padding: 0.5em 0.8em 0.8em 2em; }
* html a:hover { background: transparent; }
.classic {background: #EBEBF0;}
.critical { background: #FFCCAA; border: 1px solid #FF3334;	}
.help { background: #9FDAEE; border: 1px solid #2BB0D7;	}
.info { background: #9FDAEE; border: 1px solid #2BB0D7;	}
.warning { background: #FFFFAA; border: 1px solid #FFAD33; }

.listyle{
	border-bottom: 1px dotted #8F8F8F;
	color: #616161;
	padding:5px;
}
.listyle span{
 cursor: pointer;
}

 
 
table.property_table{ 
  	height:100%;
  	background:#E7E7E7;
  	border-collapse: separate;
	border-spacing: 10px; 
  	border:1px solid #4DB3B3;  
  	width:30%;
 } 
tbody.property_table{
 width:100%;
 float:left;
}
#ledgendtable{
 	width:95%;
 	height:auto;
 	border-spacing: 5px;
 	background:#f8f8f8;
 	border:1px solid #4DB3B3; 
 	border-radius:20px;
 	position: relative;
 	left:20px;
 	top:5px;
 }
.tabletdid{ 
 /*	background: #EBEBF0;  
 	border: 1px solid #4DB3B3;*/
 	float:left;
 	margin:1px;
}
 
.tdbox{
	background:#F8F8F8;
	font-weight:bold; 
	padding-right:15px;
}
tr{
	/*border: 1px soild #4DB3B3; 
	padding:3px;*/
}
.createOffer{
	font-weight: bold;
	padding: 30px 10px 10px;
	position: relative;
	text-transform: uppercase;
}
.createOffer:hover { 
	text-decoration: underline;
	cursor: pointer; 
}
.imgSize{ 
	z-index:-10; 
}
.trtd{ 
	float :left;
	width:100%
}
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow: hidden!important;
} 
.componentTR{
	float :left;
	width:100%;
}
.ui-accordion .ui-accordion-content{
	height:100%;
}
.tablediv{
	position: relative; 
	float: left; 
	left: 35%; 
	width: 100%; 
	margin-top: 2%;  
}
#ajaxDiv{ 
    background-image: url("./images/ajax-loader.gif");
    background-repeat: no-repeat;
    float: left;
    height: 5%;
    left: 60%;
    position: fixed;
    top: 40%;
    width: 3%;
    opacity:0;
}
.tableclass{
	padding:3px;
	margin: 2px!important;
}
.compoundDiv{
    border: 1px solid #EBEBF0; 
    height: auto;
    padding: 5px;
    position: absolute; 
    width: 60%;
}
.availableClass{
	background: #EBEBF0 
}
.mouseOverClass{
	background: #4DB3B3;
	cursor:cell;
}
.offeredClass{
	background: #66FFCC;  
}
.rentedClass{
	background: #BA61D8;
	cursor: not-allowed;
}
.soldClass{
	background: #610B21;
	cursor: not-allowed;
}
.pendingClass{
	background: #FFFF00;
	cursor: not-allowed;
}
.renovationClass{
	background: #6161FF;
	cursor: not-allowed;
}
</style>
<script type="text/javascript">
var slidetab="";
 var slidetabChild="";
$(function(){ 
	$('.listyle a').live('dblclick',function(){  
			slidetab=$(this).parent().get(0);  
			var tempvar=$($(slidetab).children().get(1)).attr("id");  
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		    var propertyId=$('#propertyid_'+rowid).html(); 
		    $("#page-error").hide();
		    $(".property-result,.createOffer").html("");
		    $(".property-result,.createOffer").remove(); 
		    if($('.processStatus_'+rowid).html()=="Complete"){ 
		    	$("#ajaxDiv").css('opacity',1); 
			    $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/offer_form_getbuildingdetails.action", 
					data:{propertyId:propertyId},
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){   
						$("<div id=\"property-result\" class=\"float-left width100 property-result\"  style=\"background-color: transparent; height:100%;\"></div>").appendTo(".parentDiv");
						$("#property-result").html(result);  
						$("<span id='createOffer_"+propertyId+"' class=\"createOffer float-right\" style=\"width:2%;\">"+
							"<span class=\"portlet-header ui-widget-header float-right\">Done</span></span>").insertAfter(".parentDiv"); 
						$("#ajaxDiv").css('opacity',0); 
					} 		
				}); 
			}
			else{ 
				$('#page-error').hide().html("Process Incomplete").slideDown(1000);
			}
	    return false;
	});  
	
	
	$('.searchauto').combobox({ 
       selected: function(event, ui){
    	  var propertyIds=$('.searchauto').val(); 
    	  triggerProperty(propertyIds);
       } 
	});  
});
function triggerProperty(propertyId){ 
	if(propertyId!=null && propertyId!=""){
		$("#page-error").hide();
	    $(".property-result,.createOffer").html("");
	    $(".property-result,.createOffer").remove(); 
	   	$("#ajaxDiv").css('opacity',1); 
	    $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/offer_form_getbuildingdetails.action", 
			data:{propertyId:propertyId},
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("<div id=\"property-result\" class=\"float-left width100 property-result\"  style=\"background-color: transparent; height:100%;\"></div>").appendTo(".parentDiv");
				$("#property-result").html(result);  
				$("<span id='createOffer_"+propertyId+"' class=\"createOffer float-right\" style=\"width:2%;\">"+
					"<span class=\"portlet-header ui-widget-header float-right\">Done</span></span>").insertAfter(".parentDiv"); 
				$("#ajaxDiv").css('opacity',0); 
			} 		
		}); 
	   }
	   else{
	    return false;
	   }
	  return false;
}
</script>
<div id="main-content" style="width: 100%; height: 100%; float: left;">
		<input type="hidden" class="docflag" value="0"/> 
		<div class="right_box float-right" style="padding: 3px;">
			<div class="building_bck portlet parentDiv ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:86%;">
					<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none; margin:5px 10px 0 5px;"></div>
				<div id="property-result" class="float-left width100 property-result" 
					style="background-color: transparent; height:100%;">
				</div> 
				<div id="ajaxDiv"></div>
			</div>
		</div>
		<div class="float-left width20" style="height:95%;"> 
		 		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:100%;">
					<div class="left_header portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property</div>	
					<div class="left_box portlet-content" style="height:95%; overflow: auto;">
						<div class="searcharea">
							<select class="width100 searchauto">
								<option value="">Search</option>
								<c:forEach var="property" items="${PROPERTY_INFO}">
									<option value="${property.propertyId}">${property.propertyName}</option> 
								</c:forEach>
							</select>
							<span class="float-right" style="margin-top:7px;"><img src="images/property/offer/search.jpg" class="searchbutton"/></span>
						</div>
		 				<div id="accordion4" class="width100 float-left" style="height:100%;"> 
		 					<c:choose>
		 						<c:when test="${requestScope.PROPERTY_TYPE ne null && requestScope.PROPERTY_TYPE ne ''}">
		
		 							<c:forEach var="propertyTypes" items="${PROPERTY_TYPE}" varStatus="propertyStatus"> 
		 								<h2 class="accordian_list_head">
		 									<a style="cursor:pointer;">
		 										<span id="propertyType_${propertyStatus.index+1}">${propertyTypes.propertyType}</span>
		 										<span style="display:none">${propertyTypes.propertyTypeId}</span> 
		 									</a>
			 							</h2> 
		 								<div class="max-height" style="overflow: none;">
											<ul class="sublist">
												<c:forEach var="property" items="${PROPERTY_INFO}" varStatus ="status">
												 	<c:choose> 
												 		<c:when test="${propertyTypes.propertyTypeId eq property.propertyTypeId}">
												 			<li class="listyle"><a class="offer-tooltip-css">${property.propertyName}
												 				<c:choose>
												 					<c:when test="${property.processStatus eq true}">
												 						<span class="classic">
												 							 ${property.cityName}<br/>
															 				 ${property.stateName}<br/>
															 				 ${property.countryName}<br/> 
														 				</span>
														 				 <span class="processStatus_${status.index+1}" style="display: none;">Complete</span>
												 					</c:when>
												 					<c:otherwise>
												 						<span class="classic">
															 				  Incomplete 
												 						</span>
												 						<span class="processStatus_${status.index+1}" style="display: none;">Incomplete</span>
												 					</c:otherwise> 
												 				</c:choose></a>
												 				<div id="propertyLi_${status.index+1}">
												 					<div id="pop-up_${status.index+1}" class="pop-up" style="overflow:hidden;">
																        <span style="display: none;" id="propertyid_${status.index+1}">${property.propertyId}</span>
					 											 
																    </div>
					 											</div> 
												 			</li>
												 		</c:when>
												 	</c:choose>
												 </c:forEach>
											</ul>
										</div>  
		 							</c:forEach>
		 						</c:when>
		 						<c:otherwise/>  
		 					</c:choose>
		 				</div> 
		 			</div>
		 		</div>
		 </div> 
		<div class="clearfix"> </div>
</div> 