<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
	<c:choose>
 		<c:when test="${beanCom != null }">
 			<c:forEach items="${beanCom}" var="companyBean" varStatus="status" >
				<option value="${companyBean.companyId}">${companyBean.companyName}</option>
			</c:forEach> 
 		</c:when>
 	</c:choose> 