<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <div class="selectable width100" id="hrm"> 
     <div class="float-left width100" style="background-color:#dfdfdf;padding:5px;">
     	<div class="width20 float-left"><span>Contract Number</span></div> 
     	<div class="width30 float-left"><span>Building Name</span></div>
     	<div class="width20 float-left"><span>Building Component</span></div>
     	<div class="width30 float-left"><span>Tenant Name</span></div>
     </div> 
     <c:choose>
	     <c:when test="${itemList ne null}">
	 		<c:forEach items="${itemList}" var="item" varStatus="status"> 
			 	 <div class="selectlist float-left width100">   
			 	 	 <input type="hidden" value="${item.contractId}"/>
			 	 	 <input type="hidden" value="${item.buildingId}"/>
			 	 	 <input type="hidden" value="${item.buildingComponentId}"/>
			 	 	 <input type="hidden" value="${item.tenantId}"/>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0 ;"><span>${item.contractNumber}</span></div>
			 	 	 <div class="width30 float-left" style="padding:1% 0  1% 0;"><span>${item.buildingName}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;"><span>${item.buildingComponent}</span></div>
			 	 	 <div class="width30 float-left" style="padding:1% 0  1% 0;"><span>${item.tenantName}</span></div>
		         </div>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<div class="width50 float-left" style="padding:1% 0  1% 0;"><span>No Records Available</span></div>
		</c:otherwise>
	</c:choose>
</div>
<script type="text/javascript">
  $(function(){
	  $(".ui-dialog-titlebar").remove();
	   $(".selectlist").click(function(){
            $(temp).css('background-color','#fff');
            $(this).css('background-color','#F39814');
            temp=this;
        });
	   $(".selectlist").mousedown(function(){
            $(this).css('background-color','#FECA40');
        });
	    $('.selectlist').click(function(){
	       $('#contractId').val($($(this).children().get(0)).val());
	       $('#buildingId').val($($(this).children().get(1)).val());
	       $('#buildingComponentId').val($($(this).children().get(2)).val());
	       $('#tenantId').val($($(this).children().get(3)).val());
	       $('#contractNumber').val($($($(this).children().get(4)).children().get(0)).html());
	       $('#buildingName').val($($($(this).children().get(5)).children().get(0)).html());
	       $('#buildingComponent').val($($($(this).children().get(6)).children().get(0)).html());
	       $('#tenantName').val($($($(this).children().get(7)).children().get(0)).html());
           
    });  
}); 
</script>