<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
	<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Property Flat</title>
 <script type="text/javascript" src="/js/thickbox-compressed.js"></script>
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var openFlag=0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){  
	$("#propertyFlatAdd").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	//Save Property Flat details
	$("#save").click(function(){
		if($('#propertyFlatAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				buildingId = $('#buildingId').val();
				floorNo=$('#floorNo').val();
				noOfRooms=$('#noOfRooms').val();
				componentType=$('#componentType').val();
				flatStatus=$('#flatStatus').val();
				image=$('#URNdms').val();
				fromDate=$('#startPicker').val();
				toDate=$('#endPicker').val();
				deposit=$('#deposit').val();
				rent=$('#rent').val();
				rentStatus=$('#rentStatus').val();
				totalArea=$('#totalArea').val();
				flatNumber=$('#flatNumber').val();
				measurementCode= $('#measurementCode').val();
				headerLedgerId = $('#headerLedgerId').val();
				workflowStatus="Not Yet Approved";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : ");
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/add_final_save_flat_info.action",
						data: {buildingId: buildingId,floorNo: floorNo,noOfRooms: noOfRooms,
							componentType: componentType, flatStatus: flatStatus, image: image, fromDate: fromDate,toDate:toDate,measurementCode: measurementCode,
							deposit: deposit,rent: rent,rentStatus: rentStatus,totalArea: totalArea,trnValue: transURN,flatNumber:flatNumber,
							headerLedgerId:headerLedgerId,workflowStatus:workflowStatus},  
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.tempresultfinal').fadeOut();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$("#main-wrapper").html(result); 
								} else{
									$('.tempresultfinal').fadeIn();
								} 
								$('#loading').fadeOut();
						},  
					 	error:function(result){
							$('.tempresultfinal').fadeOut();
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#loading').fadeOut();
					 	} 
					 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Feature Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Request Property Flat details
	$("#request").click(function(){
		if($('#propertyFlatAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				buildingId = $('#buildingId').val();
				floorNo=$('#floorNo').val();
				noOfRooms=$('#noOfRooms').val();
				componentType=$('#componentType').val();
				flatStatus=$('#flatStatus').val();
				image=$('#URNdms').val();
				fromDate=$('#startPicker').val();
				toDate=$('#endPicker').val();
				deposit=$('#deposit').val();
				rent=$('#rent').val();
				rentStatus=$('#rentStatus').val();
				totalArea=$('#totalArea').val();
				flatNumber=$('#flatNumber').val();
				measurementCode= $('#measurementCode').val();

				var headerLedgerId = $('#headerLedgerId').val();
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : ");
				
				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var applicationId=Number($('#headerApplicationId').val());
				var workflowStatus="Request For Property Component";
				var wfCategoryId=Number($('#headerCategoryId').val());
				var functionId=Number($('#headerCategoryId').val());
				var functionType="request";
				
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/add_final_request_flat_info.action",
						data: {buildingId: buildingId,floorNo: floorNo,noOfRooms: noOfRooms,
							componentType: componentType, flatStatus: flatStatus, image: image, fromDate: fromDate,toDate:toDate,measurementCode: measurementCode,
							deposit: deposit,rent: rent,rentStatus: rentStatus,totalArea: totalArea,trnValue: transURN,flatNumber:flatNumber,
							companyId:companyId,applicationId:applicationId,workflowStatus:workflowStatus,wfCategoryId:wfCategoryId,
							functionId:functionId,functionType:functionType,sessionpersonId: sessionpersonId,headerLedgerId:headerLedgerId},
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
								$('.formError').hide();
								$('.commonErr').hide();
								$('.tempresultfinal').fadeOut();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#request').remove();	
									$('#discard').remove();	
									$('#save').remove();
									$('#close').fadeIn();
									$('.commonErr').hide();
								} else{
									$('.tempresultfinal').fadeIn();
								}
							},  
						error:function(result){
								$('.tempresultfinal').fadeOut();
								$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$("#main-wrapper").html(result); 
								} else{
									$('.tempresultfinal').fadeIn();
								};
						 	}
					 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Feature Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Feature Details
 	$('.addFeature').click(function(){
 		if(openFlag==0){ 
			if($('#propertyFlatAdd').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_add_flat_feature_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
	
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}
	});
	
	$(".editFeature").click(function(){
		if(openFlag==0){
			if($('#propertyFlatAdd').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_edit_flat_feature_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
					 var temp5= $($(slidetab).children().get(4)).text();
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp5);
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}
	});
   
 	$('.delFeature').click(function(){
 		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	 		var trnValue=$("#transURN").val(); 
	 		actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
			var flag = false; 
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
			$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_delete_flat_feature_update.action", 
				 	async: false,
				 	data: {trnValue:trnValue, actualLineId: actualID},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
		       			childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
 		 }
	});

 	//Adding Feature Details
 	$('.addflat').click(function(){ 
 		if(openFlag==0){
 			if($('#propertyFlatAdd').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		       //alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_add_flat_asset_redirect.action", 
					async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
					
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}
 	});
	
	$(".editFlat").click(function(){
		if(openFlag==0){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();

		    actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			if(actualID != null && actualID !='' && actualID!=undefined){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_edit_flat_asset_redirect.action",  
					async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
	
					// var temp5= $($(slidetab).children().get(6)).text();
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				$('#warningMsg').hide().html("Please insert record ").slideDown();
			}
		}
	});

	$('.delFlat').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	 		var trnValue=$("#transURN").val(); 
	 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			var flag = false; 
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
			if(actualID != null && actualID !='' && actualID!=undefined){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_delete_flat_asset_redirect.action", 
				 	async: false,
				 	data: {trnValue:trnValue, actualLineId: actualID},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
		       			childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}
 	});	

 	$("#discard").click(function(){
 	 	$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();  
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_discard_flat_info.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 	});

 	$('.addrowsfeature').click(function(){
 		var count=Number(0);
		$('.rowid').each(function(){
			count=count+1;
			
		});  
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/flat_addrow.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
		
	});
 });

	$('.addrowsflat').click(function(){
 		var count=Number(0);
		$('.asset').each(function(){
			count=count+1;
			
		});  
		var lineNumber=count; 
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/flat_asset_addrow.action", 
	 	async: false,
	 	data:{lineNumber:lineNumber},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tabasset").append(result);
			if($(".tabasset").height()<255)
				 $(".tabasset").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabasset").height()>255)
				 $(".tabasset").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
			
		});	
	});

	$('#buildingId').change(function(){		//Retrieve No Of Rooms From Building Details
		buildingId = Number($('#buildingId').val());
		if(buildingId != 0){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_flat_load_noofrooms.action", 
			 	async: false,
			 	data:{buildingId: buildingId},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#noOfRooms").html(result);
			 	}
			});
			$('#loading').fadeOut();
		}	
	});

	$('#componentType').change(function(){		//Retrieve No Of Rooms From Building Details
		buildingId = Number($('#buildingId').val());
		componentType = Number($('#componentType').val());
		if(buildingId != 0){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_flat_load_noofrooms.action", 
			 	async: false,
			 	data:{buildingId: buildingId, componentType: componentType},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$("#noOfRooms").html(result);
			 	}
			});
			$('#loading').fadeOut();
		}	
	});

	$('#noOfRooms').change(function(){		//Retrieve Total Area , From Date & Todate From Component
		buildingId = Number($('#buildingId').val());
		noOfRooms = Number($('#noOfRooms').val());
		if(buildingId != 0 && noOfRooms !=0 ){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/property_flat_load_totalarea.action", 
			 	async: false,
			 	data:{buildingId: buildingId, noOfRooms: noOfRooms},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresult").html(result);
					$("#startPicker").val($('#fromDateComp').html());
					$("#endPicker").val($('#toDateComp').html());
					$("#totalArea").val($('#totalAreaComp').html());
					$("#measurementCodeTemp").val($('#measurementCodeComp').html());
					$('#measurementCode').val($('#measurementCodeTemp').val());
			 	}
			});
			$('#loading').fadeOut();
		}	
	});

	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_flat_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
	
	
 	var dmsUserId='${emp_CODE}';
	var dmsPersonId='${PERSON_ID}';
	var companyId=Number($('#headerCompanyId').val());
	var applicationId=Number($('#headerApplicationId').val());
	var functionId=Number($('#headerCategoryId').val());
	var functionType="list,upload";//edit,delete,list,upload
	//$('#dmstabs-1, #dmstabs-2').tabs();
	//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
	$('#dms_create_document').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
			loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
			//$('#tabs').tabs({selected:0});// switch to first tab
	});
	$('#dms_document_information').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
		loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
		//$('#tabs').tabs({selected:1});// switch to second tab
	});
    

	
	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'
		});

});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
var daysInMonth =$.datepick.daysInMonth(
$('#selectedYear').val(), $('#selectedMonth').val());
$('#selectedDay option:gt(27)').attr('disabled', false);
$('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
if ($('#selectedDay').val() > daysInMonth) {
    $('#selectedDay').val(daysInMonth);
}
} 
function customRange(dates) {
if (this.id == 'startPicker') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
}
}

</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyflat"/></div>
				<div class="portlet-content">
					<div style="display:none;" class="tempresult"></div>
					<div style="display:none;" class="tempresultfinal">
						<c:if test="${requestScope.bean != null}">
							<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
							 <c:choose>
								 <c:when test="${bean.sqlReturnStatus == 1}">
									<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
								<c:when test="${bean.sqlReturnStatus != 1}">
									<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
							</c:choose>
						</c:if>    
					</div>	 	
					<form id="propertyFlatAdd">
					<fieldset>
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.propertyflat2"/></legend>
								<div style="display:none;">
									<label>Person Id</label><input type="text" name="sessionpersonId" value="${PERSON_ID}" class="width40" id="sessionpersonId" disabled="disabled">
								</div>						
		                            <div>
		                            	<label><fmt:message key="re.property.info.fromdate"/><span style="color:red">*</span></label>
		                            	<input type="text" name="startPicker" class="width30 validate[required]" id="startPicker" readonly="readonly" TABINDEX=7>
		                            </div>		
									<div>
										<label><fmt:message key="re.property.info.todate"/></label>
										<input type="text" name="endPicker" class="width30" id="endPicker" readonly="readonly" TABINDEX=8>
									</div>																
									<div>
										<label><fmt:message key="re.property.info.deposit"/></label>
										<input type="text" name="deposit" class="width30" id="deposit" TABINDEX=9>
									</div>
									<div>
										<label><fmt:message key="re.property.info.rent"/><fmt:message key="re.propertyinfo.peryear"/><span style="color:red">*</span></label>
										<input type="text" name="rent" class="width30 validate[required,custom[onlyFloat]]" id="rent" TABINDEX=10>
									</div>
									<div style="display:none;"><label><fmt:message key="re.property.info.rentstatus"/></label>
										<select id="rentStatus" class="width30" >
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookupRentList ne null}">
												<c:forEach items="${requestScope.lookupRentList}" var="rentList" varStatus="status">
													<option value="${rentList.rentStatus }">${rentList.rentStatusName }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
								<div>
									<label><fmt:message key="re.property.info.totalarea"/><span style="color:red">*</span></label>
									<input type="hidden" id="measurementCodeTemp" name="measurementCodeTemp" class="width30">
									<input type="text" name="totalArea" class="width15 validate[required]" id="totalArea" TABINDEX=11>
									<select id="measurementCode" class="width15" TABINDEX=12 >
									<c:if test="${requestScope.beanMeasurement ne null}">
									<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementList" varStatus="status">
										<option value="${beanMeasurementList.measurementCode }">${beanMeasurementList.measurementValue }</option>
									</c:forEach>
									</c:if>
								</select>
								</div>
								<div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								  <input type="hidden" id="URNdms" value="0" disabled="disabled"/>
									<div class="clearfix"></div>
										<div class="float-right buttons ui-widget-content ui-corner-all" style="">
											 <div  id="dms_document_information" class="portlet-header ui-widget-header float-right"><fmt:message key="cs.scan.upload.upload"/></div>
							             </div>
									</div>		
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset style="height:207px;">
								<legend><fmt:message key="re.property.info.propertyflat1"/></legend>
								<div style="display:none;"><label>Id No.</label><input type="hidden" name="propertyFlatId" class="width30" disabled="disabled" id="propertyFlatId"></div>
								<div>
									<label><fmt:message key="re.property.info.buildingname"/><span style="color:red">*</span></label>
									<select id="buildingId" class="width30 validate[required]" TABINDEX=1>
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemList ne null}">
											<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
												<option value="${building.buildingId }">${building.buildingName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.floorno"/><span style="color:red">*</span></label>
									<input type="text" name="floorNo" class="width30 validate[required]" id="floorNo" TABINDEX=2>		
								</div>	
								<div>
									<label><fmt:message key="re.property.info.componenttype"/><span style="color:red">*</span></label>
									<select id="componentType" class="width30 validate[required]" TABINDEX=3>
										<option value="">-Select-</option>
										<c:if test="${requestScope.lookupList ne null}">
											<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
												<option value="${compType.componentType }">${compType.componentTypeName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.flatnumber"/><span style="color:red">*</span></label>
									<input type="text" name="flatNumber" class="width30 validate[required]" id="flatNumber" TABINDEX=4>
								</div>
								<div id="noOfRoomsDiv">
									<label><fmt:message key="re.property.info.noofrooms"/><span style="color:red">*</span></label>
									<select name="noOfRooms" class="width30 validate[required]" id="noOfRooms" TABINDEX=5>
										<option value="">Select</option>
									</select>
								</div>
								<div><label><fmt:message key="re.property.info.status"/><span style="color:red">*</span></label>
									<select id="flatStatus" class="width30 validate[required]" TABINDEX=6>
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookupFlatList ne null}">
												<c:forEach items="${requestScope.lookupFlatList}" var="flatList" varStatus="status">
													<option value="${flatList.flatStatus }">${flatList.flatStatusName }</option>
												</c:forEach>
											</c:if>
									</select>
								</div>				
						 </fieldset> 
						</div>
				</fieldset>
				</form>
				<div id="main-content" style="width:98% !important;"> 
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuredetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="0"/> 
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.featurecode"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.features"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail1"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail2"/></th>
			            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tab" style="">	
									 	<%for(int i=1;i<=3;i++) { %>
											 <tr class="rowid">  
												<td class="width10"><%=i%></td>	 
												<td class="width20"></td>
												<td class="width20"></td>	
												<td class="width10"></td> 
												<td class="width10"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>	
												<td style="display:none;"></td>	 
											</tr>  
							  		 	<%}%>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>
		<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="flatMaintainenceReleaseAdd">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.assetdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.brand"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.modelnumber"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tabasset" style="">	
										 	<%for(int i=1;i<=3;i++) { %>
											 	<tr class="asset">  	 
													<td class="width20"><%=i%></td>
													<td class="width20"></td>	
													<td class="width20 amountcalc"></td> 
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addflat" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFlat"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFlat" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	<%}%>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
				    		<!--  <div class="float-right " style="dispaly:none;">
							<label>Total</label>
						 	<input name="amountTotal" id="amountTotal" style="width:45%!important;" disabled="disabled" >
						 </div>-->
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsflat" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>	
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
				<div class="portlet-header ui-widget-header float-right cancelrec" id="discard" style=" cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="request" style=" cursor:pointer;">Request For Verification</div>
				<div class="portlet-header ui-widget-header float-right headerData" id="save" style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
				<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close">Close</div>
			</div>
		</div>
</div>	
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="property-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>
