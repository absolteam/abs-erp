<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div><label><fmt:message key="re.contract.purchaseorderno"/></label>
	<select id="purchaseOrderId" class="width40 validate[required]" >
	<option value="">-Select-</option>
	<c:if test="${requestScope.itemList ne null}">
	<c:forEach items="${requestScope.itemList}" var="purchase" varStatus="status">
		<option value="${purchase.purchaseOrderId }">${purchase.purchaseOrderNo }</option>
	</c:forEach>
	</c:if>
		</select>
</div>
<div id="temp" style="display:none;">
	<label class=""><fmt:message key="re.common.approvedstatus"/></label>
	<select id="approvedStatus" class="width40" >
		<option value="">-Select-</option>
		<c:forEach items="${requestScope.approvedStatus}" var="approvedStatus" varStatus="status">
	<option value="${approvedStatus.approvedStatus }">${approvedStatus.approvedStatusName}</option>
	</c:forEach>
		</select>
</div>