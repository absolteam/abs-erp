<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(document).ready(function (){  

	 $("#ownerInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});

		$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/maintenance_asset_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

		$('#save').click(function(){ 
			$('.tempresultfinal').fadeOut();
			if($("#ownerInformationAdd").validationEngine({returnIsValid:true})){
				var classId=Number($('#classId').val());
				var brandId=$('#brandId').val();
				var modelNumber=$('#modelNumber').val();
				var noOfPieces=$('#noOfPieces').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var subClassName=$('#subClassName').val();
				var specification=$('#specification').val();
				var detailDescription=$('#detailDescription').val();
				$.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/maintainence_asset_save.action",   
			     	async: false,
			     	data: {classId:classId,brandId: brandId,modelNumber: modelNumber,detailDescription: detailDescription,
						noOfPieces: noOfPieces,fromDate: fromDate,toDate: toDate, subClassName: subClassName,specification: specification},
					dataType: "html",
					cache: false,
					success:function(result){	
				 		$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
				 	},  
				 	error:function(result){   
				 		$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						}
                       return false;
				 	} 
				});}else{return false;}
				return true;
			}); 

		if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'
		});

	
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.buildingmaintenanceasset"/></div>
		<div class="portlet-content">
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
			<form name="ownerInformationAdd" id="ownerInformationAdd"> 
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.owner.info.validityinformation"/></legend>					
						<div>
							<label class="width30"><fmt:message key="re.owner.info.fromdate"/><span style="color:red">*</span></label>
							<input type="text" name="sno" class="width30 validate[required]" id="startPicker" readonly="readonly">
						</div>
						<div>
							<label class="width30"><fmt:message key="re.owner.info.todate"/></label>
							<input type="text" name="sno" class="width30 tooltip" id="endPicker" readonly="readonly">
						</div>
					 </fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.owner.info.assetinformation"/></legend>
						<div>
							<label><fmt:message key="re.property.info.classname"/><span style="color:red">*</span></label>
							<select id="classId" class="width30 validate[required]" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.classList ne null}">
									<c:forEach items="${requestScope.classList}" var="classes" varStatus="status">
										<option value="${classes.classId }">${classes.className }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label><fmt:message key="re.property.info.subclassname"/></label>
							<input name="subClassName" class="width30"  id="subClassName"/>
						</div>
						<div>
							<label><fmt:message key="re.property.info.itemname"/></label>
							<input name="detailDescription" class="width30"  id="detailDescription"/>
						</div>
						<div>
							<label><fmt:message key="re.property.info.detailspecification"/></label>
							<input name=specification class="width30"  id="specification"/>
						</div>
						<div>
							<label><fmt:message key="re.owner.info.brand"/></label>
							<select class="width30" id="brandId">
								<option value="">-Select-</option>
								<c:forEach items="${requestScope.lookupList}" var="typelist" varStatus="status">
									<option value="${typelist.brandId}">${typelist.brand}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class=""><fmt:message key="re.owner.info.modelnumber"/></label>
							<input type="text" name="modelNumber" class="width30" id="modelNumber">
						</div>
						<div>
							<label class=""><fmt:message key="re.owner.info.noofpieces"/><span class="mandatory">*</span></label>
							<input type="text" name="noOfPieces" class="width30 validate[required,custom[onlyNumber]]" id="noOfPieces">
						</div>
					</fieldset> 
				</div>
			</form>
			<div class="clearfix"></div>					
			<div class="float-right buttons ui-widget-content ui-corner-all"> 			
				<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.owner.info.cancel"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="save"><fmt:message key="re.owner.info.save"/></div>
			</div>
		</div>
	</div>
</div>