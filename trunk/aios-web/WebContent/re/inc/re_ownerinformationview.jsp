<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';


$(document).ready(function (){  

	$('#country').val($('#countryTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#ownerType').val($('#tempowner').val());
	$('#companyPersonId').val($('#companyPersonIdTemp').val());
	$('#existingCompanyId').val($('#companyPersonIdTemp').val());
	if($('#endPicker').val() == '31-Dec-9999'){
		$('#endPicker').val("")
	}
	ownerType=$('#ownerType').val();
	if(ownerType == '1'){
		$('#passportNoDiv').fadeOut();
	}else{
		$('#passportNoDiv').fadeIn();
	}

	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/owner_information_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
}); 	
</script>
 
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.ownerinformation"/></div>
			<div class="portlet-content">
						<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
						</c:if>
						<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
						</c:if>
			 	   <form name="ownerInformationEdit" method="post" id="ownerInformationEdit"> 
						<div class="width50 float-right" id="hrm"> 
					<fieldset>
						<legend><fmt:message key="re.owner.info.ownerinformation2"/></legend>					
							<div>
								<label class="width30"><fmt:message key="re.owner.info.address1"/></label>
								<input type="text" name="address1" disabled="disabled" class="width30 validate[required] tooltip" TABINDEX=10 id="address1" value="${bean.address1}">
							</div>
							 <div>
								<label class="width30"><fmt:message key="re.owner.info.address2"/></label>
								<input type="text" name="sno" disabled="disabled" class="width30 tooltip" id="address2" TABINDEX=11 value="${bean.address2}">
							</div>
					 		<div>
							 	<label class="width30"><fmt:message key="re.owner.info.country"/></label>
							 	<input type="hidden" id="countryTemp" name="cityTemp" value="${bean.countryId}" class="width30">
									<select id="country" class="width30 validate[required]" disabled="disabled" TABINDEX=12>
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
											<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
										</c:forEach>
									</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.owner.info.state"/></label>
								<input type="hidden" id="stateTemp" name="cityTemp" value="${bean.stateId }" class="width30">
									<select class="width30 validate[required]" id="state" disabled="disabled" TABINDEX=13>
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
											<option value="${statelist.stateId }">${statelist.stateName}</option>
										</c:forEach>
									</select>
							</div>	
							<div>
								<label class="width30"><fmt:message key="re.owner.info.city"/></label>
								<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" disabled="disabled" class="width30">
									<select  class="width30 validate[required]" id="city" disabled="disabled" TABINDEX=14>
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
											<option value="${citylist.cityId }">${citylist.cityName}</option>
										</c:forEach>
									</select>
							</div>														
							<div>
								<label class="width30"><fmt:message key="re.owner.info.emailid"/></label>
								<input type="text" name="sno" disabled="disabled" class="width30 validate[required,custom[email]]" id="emailId" TABINDEX=15 value="${bean.emailId}">
							</div>
				            <div>
				            	<label class="width30"><fmt:message key="re.owner.info.mobileno"/></label>
				            	<input type="text" name="sno" disabled="disabled" class="width30 validate[required,custom[onlyNumber]]" TABINDEX=16 id="mobileNo" value="${bean.mobileNo}">
				            </div>
				            <div>
				            	<label class="width30"><fmt:message key="re.owner.info.landlineno"/></label>
				            	<input type="text" name="sno" disabled="disabled" class="width30" id="landlineNo" TABINDEX=17 value="${bean.landlineNo}">
				            </div>
				             <div>
                            	<label class="width30"><fmt:message key="re.owner.info.website"/></label>
                            	<input type="text" name="website" disabled="disabled" class="width30" id="website" value="${bean.website}" TABINDEX=18>
                            </div>
					</fieldset>  																		
				</div> 
					<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.owner.info.ownerinformation1"/></legend>
									<div style="display:none;"><label class="width30">Owner Id No.</label><input type="text" name="drno" disabled="disabled" class="width30 tooltip" id="ownerId" disabled="disabled" value="${bean.ownerId}"></div>
									<div>
										<label class="width30"><fmt:message key="re.owner.info.ownername"/></label>
										<input type="text" name="sno" disabled="disabled" class="width30 validate[required,custom[onlyLetter]]" TABINDEX=1 id="ownerName" value="${bean.ownerName}">
									</div>
									<div><label class="width30"><fmt:message key="re.owner.info.ownertype"/></label>
									<input type="hidden" id="tempowner" name="cityTemp" value="${bean.ownerType}" class="width30">
											<select class="width30 validate[required]" disabled="disabled" id="ownerType" TABINDEX=2>
											<option value="">-Select-</option>
											<c:forEach items="${requestScope.ownerTypeList}" var="typelist" varStatus="status">
												<option value="${typelist.ownerType}">${typelist.ownerTypeName}</option>
											</c:forEach>
											</select>
									</div>	
									<div id="companyPersonDiv" style="">
										<label class="width30"><fmt:message key="re.owner.info.companyperson"/><span class="mandatory">*</span></label>
										<c:choose>
											<c:when test="${bean.ownerType eq 1}">
												<input type="hidden" disabled="disabled" id="companyPersonIdTemp" value="${bean.companyId}" class="width30">
											</c:when>
											<c:when test="${bean.ownerType eq 2}">
												<input type="hidden" id="companyPersonIdTemp" disabled="disabled" value="${bean.personId}" class="width30">
											</c:when>
											<c:otherwise>
												<input type="hidden" id="companyPersonIdTemp" disabled="disabled" value="${bean.personId}" class="width30">
											</c:otherwise>
										</c:choose>
										<input type="hidden" id="existingCompanyId" class="width30">
										<select class="width30 validate[required]" disabled="disabled" id="companyPersonId" TABINDEX=3 >
											<option value="">-Select-</option>
											<c:choose>
										 		<c:when test="${beanCom != null }">
										 			<c:forEach items="${beanCom}" var="companyBean" varStatus="status" >
														<option value="${companyBean.companyId}">${companyBean.companyName}</option>
													</c:forEach> 
										 		</c:when>
										 	</c:choose> 
										</select>
									</div>			
									<div>
										<label class="width30"><fmt:message key="re.owner.info.proffession"/></label>
										<input type="text" name="Proffession" disabled="disabled" class="width30" id="proffession" value="${bean.proffession}" TABINDEX=4/>
									</div>
									<div><label class="width30" style="font-family: Arial,Verdan,Sans-Serif;font-size: 87%;"><fmt:message key="re.owner.info.civilidno"/></label>
										<input type="text" name="uaeIdNo" disabled="disabled" class="width30" id="uaeIdNo" value="${bean.uaeIdNo}" TABINDEX=5>
									</div>
									<div  id="passportNoDiv"><label class="width30"><fmt:message key="re.owner.info.passportno"/></label>
										<input type="text" name="passportNo" disabled="disabled" class="width30 validate[required]" id="passportNo" value="${bean.passportNo}" TABINDEX=6>
									</div>
									<div><label class="width30" style="font-family: Arial,Verdan,Sans-Serif;font-size: 87%;"><fmt:message key="re.owner.info.birthdate"/></label>
										<input type="text" name="birthDate" disabled="disabled" class="width30 birthDate validate[required]" readonly="readonly" TABINDEX=7 id="defaultActualPicker" value="${bean.birthDate}">
									</div>
							 		<div><label class="width30"><fmt:message key="re.owner.info.fromdate"/></label>
							 			<input type="text" name="startPicker" disabled="disabled" class="width30 validate[required]" id="startPicker" TABINDEX=8 readonly="readonly" value="${bean.fromDate}">
							 		</div>		
				                    <div><label class="width30"><fmt:message key="re.owner.info.todate"/></label>
				                    	<input type="text" name="endPicker" disabled="disabled" class="width30" id="endPicker" TABINDEX=9 readonly="readonly" value="${bean.toDate}">
				                    </div>
							</fieldset> 
						</div>
						<div class="portlet-content">
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="accounts.suppliersite.label.accountinginformation"/></legend>
								<div class="width90 float-left"><label class="width30">Owner Account Code<span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.liabilityAccountCode}" name="liabilityAccountCode" id="liabilityAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_liability_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a  id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>
								</div>
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.advanceaccountcode"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.advanceAccountCode}" name="advanceAccountCode" id="advanceAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_advance_account_action"/>
									<span  class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>	
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.clearningacccount"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.clearningAccountCode}" name="clearningAccountCode" id="clearningAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_clearning_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>																
						</fieldset> 
					</div>	
				</div>
				 <div class="clearfix"></div>
				 <div class="width100 float-left buttons" Style="float: none !important;">
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div class="portlet-header ui-widget-header float-right headerData" id="close" style="cursor:pointer;"><fmt:message key="re.property.info.close"/></div>
					</div>
				</div>	
		</form>
	</div>
 </div>
</div>
					
				

			