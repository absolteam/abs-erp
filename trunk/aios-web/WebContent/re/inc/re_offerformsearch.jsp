<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-offer_form_search.action?customerId="+customerId+'&buildingId ='+buildingId+'& offerNumber ='+offerNumber+'& offerDate ='+offerDate+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['Offer ID','Building ID','Building Name','Customer ID','Customer Name','Offer Number','Offer Date','Work Flow Status'],  
		 colModel:[ 
				{name:'offerId',index:'OFFER_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		{name:'buildingName',index:'BUILDING_NAME', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'customerId',index:'CUSTOMER_ID', width:100,  sortable:true},
		  		 {name:'customerName',index:'TENANT_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'offerNumber',index:'OFFER_NUMBER', width:150,sortable:true, sorttype:"data"},
		  		{name:'offerDate',index:'OFFER_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true, sorttype:"data"},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'OFFER_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"Offer Form"
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["offerId","buildingId","customerId"]);
	}); 
 	</script>
	 <body>
	<input type="hidden" id="bankName"/>                              
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body>    