<div>
	<div style="margin-bottom:20px;">
		<label class="width30">Template</label>
		 <select tabindex="1" name="contractTemplate" class="width70 float-right" id="contracTemplate">
			<option value="ContractDetails">Default</option>
			<option value="ContractDetailsGovernment">Government</option>
			<option value="ContractDetailsMilitary">Military</option>
		 </select> 
	</div>
	<label>Transfer Account No(s) (Comma separated): </label>
	<textarea id="transferAccount" style="min-width: 335px; max-width: 335px; min-height: 80px; max-height: 80px;"></textarea>
	<div style="height: 20px; display: block;"></div>
	<label id="err_label" style="color: red; display: none;"> Please specify account(s) for transfer.</label>
	<hr/>
	<br/>
	<br/>
	<input type="radio" name="transferOption" id="connectTransfer" checked="checked" /> Connect and Transfer Electricity
	<br/>
	<br/>
	<input type="radio" name="transferOption" id="onlyTransfer" /> Transfer Electricity
	<br/>
	<br/>
	<label id="err_label" style="color: red; display: none;"> Please specify account for transfer</label>
</div>

<script>
$(function () { 
	
	transferType = "Connect and Transfer";
	transferText = "connect the electricity and transfer the";
	
	$('#connectTransfer').click(function() {
		
		transferType = "Connect and Transfer";
		transferText = "connect the electricity and transfer the";
	});
	
	$('#onlyTransfer').click(function() {
		
		transferType = "Transfer";	
		transferText = "transfer the";
	});
	
});
</script>