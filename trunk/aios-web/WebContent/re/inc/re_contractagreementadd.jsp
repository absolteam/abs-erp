<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <script type="text/javascript" src="/js/dateDifference.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
 </script>
 

<script type="text/javascript">
var openFlag=0;
var fromFormat='dd-MMM-yyyy';
var toFormat='MM';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyy';
$(function (){ 
	var curDate=new Date().toString();
	var splitDate=curDate.split(" ");
	var day=splitDate[2];
	var mon=splitDate[1];
	var year=splitDate[3];
	fromDate=day+"-"+mon+"-"+year;
	$('#defaultPopup').val(fromDate);
	 
	$('.formError').remove(); 
	$("#contractAgreementAdd").validationEngine({ 
		validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	    success :  false,
	    failure : function() { callFailFunction()}
	});

	$("#contractAgreementCash").validationEngine({ 
		validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	    success :  false,
	    failure : function() { callFailFunction()}
	});

	//Save Contract Agreement details
	$("#save").click(function(){
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			if($('#contractAgreementCash').validationEngine({returnIsValid:true})){
					var childCountPayment = Number($('#childCountPayment').val()); 
					grandFees=Number($('#grandFees').val());
					grandTotal=Number($('#grandTotal').val());
					if( grandFees == grandTotal ){
						offerId = $('#offerId').val();
						buildingId=$('#buildingId').val();
						contractDate=$('#defaultPopup').val();
						startPicker=$('#startPicker').val();
						endPicker=$('#endPicker').val();
						tenantId=$('#tenantId').val();
						grandTotal=$('#grandTotal').val();
						paymentMethod=$('#paymentMethod').val();
						var paymentMethod = $("#paymentMethod option:selected").map(function(){
							   return $(this).val();
							}).get().join(",");
											
						noOfCheques=Number($('#noOfCheques').val());
						cashAmount=$('#cashAmount').val();
						description=$('#description').val();
						grandTotal=$('#grandTotal').val();

						//Ledger & Company Details
						var headerLedgerId = $('#headerLedgerId').val();
						var companyId=Number($('#headerCompanyId').val());
						var applicationId=Number($('#headerApplicationId').val());
						workflowStatus="Not Yet Approved";
						
						var transURN = $('#transURN').val();
						//alert(" transURN : "+transURN+" paymentMethod : "+paymentMethod);
						if(childCountPayment == noOfCheques){
							$('#loading').fadeIn();
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/add_final_save_contract.action",
								data: {offerId: offerId, buildingId: buildingId,contractDate: contractDate,tenantId: tenantId,
									fromDate: startPicker,toDate: endPicker,propStatus: status,cashAmount: cashAmount,description: description,paymentMethod: paymentMethod,
									noOfCheques: noOfCheques, trnValue: transURN,
									companyId: companyId,workflowStatus: workflowStatus,headerLedgerId: headerLedgerId,applicationId: applicationId},  
						     	async: false,
								dataType: "html",
								cache: false,
								success: function(result){ 
									$('.tempresultfinal').fadeOut();
						 		 	$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										destroyPopups();
										$("#main-wrapper").html(result); 
									} else{
										$('.tempresultfinal').fadeIn();
									}
									// Scroll to top of the page
									$.scrollTo(0,300);
								} 
							});
							$('#loading').fadeOut();  
						}else{
							$('.childCountErr').hide().html("No of Cheques & Cheque Entries Should be same .!!!").slideDown();
							// Scroll to top of the page
							$.scrollTo(0,300);
							return false; 
						}
					}else{
						$('.childCountErr').hide().html("Grant Fees & Grand Total Should be Same.!!!").slideDown();
						// Scroll to top of the page
						$.scrollTo(0,300);
						return false; 
					}
			}else{
				return false;
			}
		 }else{
			  return false;
		 }
	});

	//Request for Verification Contract Agreement details
	$("#request").click(function(){
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			if($('#contractAgreementCash').validationEngine({returnIsValid:true})){
				var childCountPayment = Number($('#childCountPayment').val()); 
				grandFees=Number($('#grandFees').val());
				grandTotal=Number($('#grandTotal').val());
					if( grandFees == grandTotal ){
						offerId = $('#offerId').val();
						buildingId=$('#buildingId').val();
						contractDate=$('#defaultPopup').val();
						startPicker=$('#startPicker').val();
						endPicker=$('#endPicker').val();
						tenantId=$('#tenantId').val();
						grandTotal=$('#grandTotal').val();
						paymentMethod=$('#paymentMethod').val();
						var paymentMethod = $("#paymentMethod option:selected").map(function(){
							   return $(this).val();
							}).get().join(",");
											
						noOfCheques=Number($('#noOfCheques').val());
						cashAmount=$('#cashAmount').val();
						description=$('#description').val();
						grandTotal=$('#grandTotal').val();

						//Work Flow Variables
						var sessionPersonId = $('#sessionPersonId').val();
						var headerLedgerId = $('#headerLedgerId').val();
						var companyId=Number($('#headerCompanyId').val());
						var applicationId=Number($('#headerApplicationId').val());
						var workflowStatus="Request For Verification";
						var wfCategoryId=Number($('#headerCategoryId').val());
						var functionId=Number($('#headerCategoryId').val());
						var functionType="request";
						
						var transURN = $('#transURN').val();
						//alert(" transURN : "+transURN+" paymentMethod : "+paymentMethod);
						if(childCountPayment == noOfCheques){
							$('#loading').fadeIn();
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/add_final_save_contract_request.action",
								data: {offerId: offerId, buildingId: buildingId,contractDate: contractDate,tenantId: tenantId,
									fromDate: startPicker,toDate: endPicker,propStatus: status,cashAmount: cashAmount,description: description,paymentMethod: paymentMethod,
									noOfCheques: noOfCheques, trnValue: transURN,
									companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus,wfCategoryId: wfCategoryId,
									functionId: functionId,functionType: functionType,sessionPersonId: sessionPersonId,headerLedgerId: headerLedgerId},  
						     	async: false,
								dataType: "html",
								cache: false,
								success: function(result){ 
										$('.formError').hide();
										$('.commonErr').hide();
										$('.childCountErr').hide();
							 		 	$('.tempresultfinal').html(result);
										if($("#sqlReturnStatus").val()==1){
											$('.tempresultfinal').fadeIn();
											$('#loading').fadeOut(); 
											$('#request').remove();	
											$('#discard').remove();	
											$('#save').remove();
											$('#close').fadeIn();
											$('.commonErr').hide();
										} else{
											$('.tempresultfinal').fadeIn();
										}
										// Scroll to top of the page
										$.scrollTo(0,300);
								} 
							});
							$('#loading').fadeOut(); 
						}else{
							$('.childCountErr').hide().html("No of Cheques & Cheque Entries Should be same .!!!").slideDown();
							// Scroll to top of the page
							$.scrollTo(0,300);
							return false; 
						}
					}else{
						$('.childCountErr').hide().html("Grant Fees & Grand Total Should be Same.!!!").slideDown();
						// Scroll to top of the page
						$.scrollTo(0,300);
						return false; 
					}
			}else{
				return false;
			}
		 }else{
			  return false;
		 }
	});

	//Payment Method 
	$('#paymentMethod').change(function() {
		var payment=$('#paymentMethod').val();
		//alert("Inside select : "+payment);
		//var paymentFormat=payment.replace(",", "/");
		//var paymentSplit=paymentFormat.split('/');
		//alert("Inside select , payment : "+payment+" paymentSplit : "+paymentSplit);
		if(payment == 'Q'){
			$('#cashAmount').attr('disabled','disabled');
			$('#description').attr('disabled','disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else if(payment == "C,Q"){ 
			$('#cashAmount').removeAttr('disabled');
			$('#description').removeAttr('disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else {
			$('.PaymentTAB').fadeOut();
			$('#cashAmount').removeAttr('disabled');
			$('#description').removeAttr('disabled');
			$('#noOfCheques').val("");
			$.fn.globeFeeDesc();	//Remove the Existing Entry
			$.fn.globeTotalFee();  //Total for Fee Amount
			$('#noOfCheques').attr('disabled','disabled');
			$('#noOfCheques').removeClass('validate[required,custom[onlyNumber]]');
		}
	});
	
	$('#noOfCheques').blur(function() { //Payment Details Add Row by checking Numbers
		tenantTypeId=Number($('#tenantTypeId').val());
		$('.PaymentTAB').fadeIn();
		var rowCount=$('#noOfCheques').val();
		//alert("tenantTypeId : "+tenantTypeId);
		$.ajax({
	   		type:"POST",
	   		url:"<%=request.getContextPath()%>/contract_agreement_load_cheque_fee.action", 
	   	 	async: false,
	   	 	data: {noOfCheques: rowCount, tenantTypeId: tenantTypeId},
	   	    dataType: "html",
	   	    cache: false,
	   		success:function(result){
		   		$.fn.globeFeeDesc();	//Remove the Existing Entry
	   			$(".tabFee").append(result);
	   			$('#childCountPayment').val("0");
	   			/* if($(".tabFee").height()<255)
					 $(".tabFee").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabFee").height()>255)
					 $(".tabFee").css({"overflow-x":"hidden","overflow-y":"auto"}); */
	   			$.fn.globeTotalFee();  //Total for Fee Amount
	   			$.fn.globeIndexFee();  //Set Index Fee Amount
	   			$.fn.globeOtherChargesCalc();		// Other Charges Calculation
	   		},
	   		error:function(result){
	   			$(".tabFee").append(result);
	   		}
   		});
		$(".tabPayment").html("");
		$(".tabPayment").animate({height:'0',maxHeight:'255'},0);
		for(i=1;i<=rowCount;i++){
			$.fn.addRowPayment();
		}
		$('#chequeAmountTotal').val("");
		$('#chequeAmountTotalAll').val("");
	});

	$.fn.addRowPayment = function() { 	// Addrow for Payment Details
		$.ajax({
				 type:"POST",
			      url:"<%=request.getContextPath()%>/add_contract_agreementpaymentrows.action", 
		 	  	async: false,
		     dataType: "html",
		        cache: false,
			success:function(result){
				$(".tabPayment").append(result);
				if($(".tabPayment").height()<255)
					 $(".tabPayment").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabPayment").height()>255)
					 $(".tabPayment").css({"overflow-x":"hidden","overflow-y":"auto"});
				}
			});
	}
	
	//Adding Rent Details
 	$('.addRent').click(function(){ 
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_contract_rent_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			return false;
		}
	});

 	//Adding Fee Details
 	$('.addFee').click(function(){ 
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_contract_fee_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			return false;
		}
	});

 	//Adding Payment Details
 	$('.addPayment').click(function(){ 
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_contract_payment_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
				}
			});
		}else{
			return false;
		}
	});

	//Edit Rent Details
	$(".editRent").click(function(){
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_contract_rent_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Edit Fee Details
	$(".editFee").click(function(){
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_contract_fee_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Edit Payment Details
	$(".editPayment").click(function(){
		if($('#contractAgreementAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_contract_payment_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
					
				 
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
	
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
				$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(2)).val(temp4);
			
			  	$('#loading').fadeOut();
	
				}
			});
		}else{
			return false;
		}
	});

	//Delete Rent Details
 	$('.delRent').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_contract_rent_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
 	});

 	//Delete Fee Details
 	$('.delFee').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_contract_fee_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
 	});

 	//Delete Payment Details
 	$('.delPayment').click(function(){
 		slidetab=$(this).parent().parent().get(0); 
 		$('.error').hide();
        $('.childCountErr').hide();
        $('#warningMsg').hide();
 		var trnValue=$("#transURN").val(); 
 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
		var flag = false; 
		//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
		if(actualID != null && actualID !='' && actualID!=undefined){
			$('#loading').fadeIn();
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_delete_contract_payment_update.action", 
			 	async: false,
			 	data: {trnValue:trnValue, actualLineId: actualID},
			    dataType: "html",
			    cache: false,
				success:function(result){	
			 		$('.formError').hide();
	                 $('#temperror').hide(); 
					$('.tempresult').html(result); 
				     if(result!=null){
				    	 $("#transURN").val($('#objTrnVal').html());
				    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
				    	else{	
					    	flag = false;
					    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
				    	} 
				     }
				     $('#loading').fadeOut();
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
	                      $('#loading').fadeOut();
	                      return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }
		   }
		}else{
			$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
			$('#warningMsg').hide().html("Please insert record to delete").slideDown();
			return false; 
		}
 	});
 	
 	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();  
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_discard_contract.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
				destroyPopups();
				$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 		$.scrollTo(0,300);
 	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_agreement_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	$('#close').click(function(){
   	 	$('.formError').remove();
   		$('#loading').fadeIn();
		 $.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/contract_agreement_list.action",   
		     	async: false, 
				dataType: "html",
				cache: false,
				error: function(data) 
				{  
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
					$('#loading').fadeOut();
		     	}
			});
		return true;
 	});
	
	$('.offer-popup').click(function(){
		$('.commonErr').fadeOut();
		startPicker=$('#startPicker').val();
		endPicker=$('#endPicker').val();
		 if(true){ //if(startPicker != null && startPicker != '' && endPicker != null && endPicker != '' ){
	       	tempid=$(this).parent().get(0);  
			$('#contract-popup').dialog('open'); 
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/contract_agreement_load_offer.action",
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.contract-result').html(result); 
				},
				error:function(result){ 
					 $('.contract-result').html(result); 
				}
			}); 
		 }else{
			$('.commonErr').hide().html("Please select Contract From & To date !!!").slideDown();
			return false;
		 }
		});
	 
	$('#contract-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		bgiframe: false,
		modal: false,
		width:'50%',
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});
	$('.addrowsrent').click(function(){
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/add_contract_agreementrows.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});
	$('.addrowsfee').click(function(){
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/add_contract_agreementfeerows.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tabFee").append(result);
			if($(".tabFee").height()<255)
				 $(".tabFee").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabFee").height()>255)
				 $(".tabFee").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	});

	$('.dateCheck').blur(function(){
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}
	});


	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});

 	$('#cashAmount').keyup(function(){
 		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
 		$('#cashAmountTotal').val($('#cashAmount').val());
 		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
 	});
 	
 	$.fn.globeTotal = function() { 	// Total for Rent & Contract Amount
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total)
		
		total=0;
		$('.contractAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		$('#contractAmountTotal').val(total); 
		$('#rentAmountTotalAll').val(total);
		$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	 }
 	$.fn.globeTotalFee = function() { 	// Total for Fee Amount
		var total=0;
		$('.feeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#feeAmountTotal').val(total)
		//$('#cashAmount').val(total); 
		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
		$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	 }
 	$.fn.globeTotalCheque = function() { 	// Total Payment Amount
		var total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#chequeAmountTotal').val(total);
		$('#chequeAmountTotalAll').val(total)
		$('#chequeAmount').val(total); 
		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));

	 }
 	$.fn.globeIndexFee = function() { 	// Index For Fee Details
		var index=1;
		$('.indexForFee').each(function(){ 
			$(this).html(index);
			index=index+1;
		});
	 }
 	$.fn.globeFeeDesc = function() { 	// Remove the Exisiting Cheque Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Cheque'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeRentDesc = function() { 	// Remove the Exisiting Rent Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeContractDesc = function() { 	// Remove the Exisiting Contract Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Contract'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeMaintenanceDesc = function() { 	// Remove the Exisiting Maintenance Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Maintenance'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeDepositDesc = function() { 	// Remove the Exisiting Deposit Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Deposit'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	
 	$.fn.globePaymentValidation = function() { 	// Validation for Cheque Payment with Total Fee's
 		payment=$('#paymentMethod').val();
 		feeTotalAmount=Number($('#feeAmountTotal').val());
 		chequeAmountTemp=Number($('#chequeAmountTemp').val());
 		//alert("payment type :"+payment+" Total Fee Amount : "+feeTotalAmount+" chequeAmountTemp : "+chequeAmountTemp);
 		total=0;
 		$('.chequeAmountCalc').each(function(){
 			total+=Number($(this).html());
 		});
 		total = (total - chequeAmountTemp)+ Number($('#chequeAmount').val());
 		if(total <= feeTotalAmount){
 	 		return true;
 		}else{
 	 		return false;
 		}
	 }
	 $.fn.globeOtherChargesCalc=function(){		// Other Charges Calculation
		 total=0;
		$('.indexFeeDesc').each(function(){ 
				//alert("Desc : "+$(this).html());
				desc=$(this).html();
				if(desc != 'Rent'){
					total+=Number($($(this).siblings().get(2)).html());
				}
		});
		$('#otherCharges').val(total);	
	 }
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	date1=$("#startPicker").val();
	date2=$("#endPicker").val();
	if(date1 != null && date1 != "" && date2 != null && date2 != ""  ){
		var rentFees=Number($('#rentFees').val());
		fromDateMonth=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
		toDateMonth=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
		fromDateYear=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),usrtoFormat));
		toDateYear=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),usrtoFormat));
		$('.rentAmountCalc').each(function(){
	       	if(rentFees !=1){
				rentAmountMonth=Number($(this).html())/12;
		       	monthDiff=(toDateMonth-fromDateMonth)+1;
		       	yearDiff=toDateYear-fromDateYear;
		       	monthCount=monthDiff+(12*yearDiff);
		       	contractAmountThis=Math.ceil(rentAmountMonth*(monthCount));
		       	$($(this).siblings().get(2)).html(contractAmountThis);
		       	//alert("Rent Amount : "+rentAmountMonth+" Contract Amount  : "+contractAmountThis);
		       	//$('#contractAmount').val(contractAmountThis);
	       	}else{
	       		$($(this).siblings().get(2)).html($('#rentFeesAmount').val());
	       	}
		});
		$.fn.globeTotal(); // Total for Rent & Contract Amount	
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				//alert("tr position : "+ $(this).parent('tr'));
				//$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				if(rentFees !=1){
					rentFeeTabTemp=$(this).parent('tr');
					$($(rentFeeTabTemp).children().get(3)).text(contractAmountThis);
				}else{
					rentFeeTabTemp=$(this).parent('tr');
					$($(rentFeeTabTemp).children().get(3)).text($('#rentFeesAmount').val());
				}
			}
		});
		$.fn.globeTotalFee(); 	// Total for Fee's Amount
	}
	
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
	$('.dateCheck').trigger('blur');
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractagreement"/></div>
	 	<div class="portlet-content">
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 	  
	 		<div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
	 		<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 
		 	<form id="contractAgreementAdd">
		 		<fieldset>
		 			<div class="width50 float-left" id="hrm">
					 	<fieldset>
					 		<legend><fmt:message key="re.contract.tenantdeatails"/></legend>
					 		<div style="display:none;">
								<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40" id="sessionPersonId" disabled="disabled">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.offerno"/><span style="color:red">*</span></label>	 
								<input class="width40 validate[required]" type="text" name="offerNumber"  id="offerNumber" disabled="disabled" >
								<input class="width40" type="hidden" name="offerId"  id="offerId" disabled="disabled" >
								<span id="hit7" class="button" style="width: 40px ! important;">
									<a class="btn ui-state-default ui-corner-all offer-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.buildingname"/></label>	
								<input class="width40" type="text" name="buildingName"  id="buildingName" disabled="disabled" > 
								<input class="width40" type="hidden" name="buildingId"  id="buildingId" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.tenantname"/></label>	 
								<input class="width40" type="text" name="tenantName"  id="tenantName" disabled="disabled" >
								<input class="width40" type="hidden" name="tenantId"  id="tenantId" >
								<input class="width40" type="hidden" name="tenantTypeId"  id="tenantTypeId" >
								<input class="width30 " id="rentFees" type="hidden" name="rentFees" value=""  disabled="disabled">
								<input class="width30 " id="rentFeesAmount" type="hidden" name="rentFeesAmount" value=""  disabled="disabled">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.address"/></label>	 
								<input class="width40" type="text" name="presentAddress"  id="presentAddress" disabled="disabled" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.address1"/></label>	 
								<input class="width40" type="text" name="permanantAddress"  id="permanantAddress" disabled="disabled" >
							</div>
							<div>
						</fieldset>
					</div>
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="re.contract.periodofrent"/></legend>
								 
							<div><label class="width30" id="fromDateCheck"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
							<input type="text" class="width40 dateCheck validate[required]" id="startPicker" name="employerFromDate" tabindex="7" readonly="readonly">
							</div>
							
							<div><label class="width30"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
							<input type="text" class="width40 employerToDate dateCheck validate[required]" id="endPicker" name="employerToDate" tabindex="8" readonly="readonly">
							</div>
							<div><label class="width30"><fmt:message key="re.contract.years"/></label><input type="text" class="width40" name="years" id="years" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.months"/></label><input type="text" class="width40" name="months" id="months" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.days"/></label><input type="text" class="width40" name="days" id="days"  disabled="disabled"></div>
						</fieldset>
					</div>
					<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:134px;">
							<legend><fmt:message key="re.contract.contractdetails"/></legend>
							<div >
								<label class="width30"><fmt:message key="re.contract.contractno"/></label>	 
								<input class="width40" type="text" name="contractNumber"  id="contractNumber" disabled="disabled" >
								<input class="width40" type="hidden" name="contractId"  id="contractId" disabled="disabled" >
							</div>
							<div >
								<label class="width30"><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>	 
								<input class="width40 validate[required]" type="text" name="contractDate"  id="defaultPopup" readonly="readonly" >
							</div>
							<div >
								<label class="width30"><fmt:message key="re.contract.paymentmethod"/><span style="color:red">*</span></label>	 
								<select id="paymentMethod" class="validate[required]" multiple="multiple" size="2">
									<option value="C">Cash</option>
									<option value="Q">Cheque</option>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.noofcheque"/><span style="color:red">*</span></label>	 
								<input class="width40 " type="text" name="noOfCheques"  id="noOfCheques" disabled="disabled" >
							</div>
						</fieldset>
					</div>
				</fieldset>
			</form>
			<div class="clearfix"></div>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container "> 
					<form name="newFields4">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.rentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div>   
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="0"/> 
							<div id="hrm">
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.lineno"/></th> 
												<th class="width20"><fmt:message key="re.contract.flatno"/></th>
												<th class="width20"><fmt:message key="re.contract.rentamount"/></th>
												<th class="width20"><fmt:message key="re.contract.contractamount"/></th>
												<th style="width:5%;display:none;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabRent">
											<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
											 	<tr class="even" > 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20 rentAmountCalc" style="height: 20px;"></td> 
													<td class="width20 contractAmountCalc"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
													</td> 
													<td style=" width:5%;display:none;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:53%">
								<label>Total</label>
							 	<input name="rentAmountTotal" id="rentAmountTotal" style="width:45%!important;" disabled="disabled" >
							 	<input name="contractAmountTotal" id="contractAmountTotal" style="width:45%!important;" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" style="display:none;"> 
						<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>	
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.feedetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div>  
						<div id="warningMsgFee" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
					 		<input type="hidden" name="childCountFee" id="childCountFee"  value="0"/> 
							<div id="hrm">
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.lineno"/></th> 
												<th class="width20" style="display:none;"><fmt:message key="re.contract.feeid"/></th>
												<th class="width20"><fmt:message key="re.contract.description"/></th>
												<th class="width20" ><fmt:message key="re.contract.amount"/></th>
												<th style="width:5%;display:none;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabFee" style="">	
											
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width: 28%;">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="feeAmountTotal" id="feeAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" style="display:none;"> 
						<div class="portlet-header ui-widget-header float-left addrowsfee" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
		
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container PaymentTAB" style="display:none;"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.paymentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 
	
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content">  
					 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value="0"/> 
							<div id="hrm">
								<div id="hrm" class="hastable width100" > 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.bankname"/></th> 
												<th class="width20"><fmt:message key="re.contract.chequedate"/></th>
												<th class="width20"><fmt:message key="re.contract.chequeno"/></th>
												<th class="width20"><fmt:message key="re.contract.chequeamount"/></th>
												<th class="width20"><fmt:message key="re.contract.feeType"/></th>
												<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabPayment" style="">	
											<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
											 	<tr class="even"> 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20 chequeAmountCalc"></td>
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="" name="feeTypeId" id="feeTypeId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:33%">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
						<div class="portlet-header ui-widget-header float-left addrowspayment"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<form id="contractAgreementCash">
				<div class="width60 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.contract.cashamount"/></legend>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.contract.cashamount"/></label>	 
							<input class="width30 validate[optional,custom[onlyFloat]]" type="text" name="cashAmount"  id="cashAmount" disabled="disabled">
						</div>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.property.info.description"/></label>	 
							<input class="width30" type="text" name="description"  id="description" disabled="disabled">
						</div>
					</fieldset>
				</div>
			</form>
			<div class="clearfix"></div>
			<div class="width60 float-left" id="hrm">
				<fieldset style="min-height:134px;">
					<legend><fmt:message key="re.contract.grossdetails"/></legend>
					
					<div style="display:none;">
						<label class="width30"><fmt:message key="re.contract.chequeamount"/></label>	 
						<input class="width30" type="text" name="chequeAmount"  id="chequeAmount" disabled="disabled" >
					</div>
					<div>
						<label class="width20"><fmt:message key="re.contract.chequeamount"/></label>	 
						<input class="width20 float-left" type="text" name="rentAmountTotalAll"  id="rentAmountTotalAll" disabled="disabled" >
					
						<label class="width20"><fmt:message key="re.contract.totalCashAmount"/></label>	 
						<input class="width20" type="text" name="cashAmountTotal"  id="cashAmountTotal" disabled="disabled" >
						
					</div>
					<div>
						<label class="width20"><fmt:message key="re.contract.otherCharges"/></label>	 
						<input class="width20 float-left" type="text" name="otherCharges"  id="otherCharges" disabled="disabled" >
						<label class="width20" style="font-size: 11px ! important;">Total Cheque Payment</label>	 
						<input class="width20" type="text" name="chequeAmountTotalAll"  id="chequeAmountTotalAll" disabled="disabled" >
					</div>
					<div>
						<label class="width20"><fmt:message key="re.contract.grandFees"/></label>	 
						<input class="width20 float-left" type="text" name="grandFees"  id="grandFees" disabled="disabled" >
					
						<label class="width20"><fmt:message key="re.contract.grandtotal"/></label>	 
						<input class="width20" type="text" name="grandTotal"  id="grandTotal" disabled="disabled" >
					</div>
					<div style="display:none;">
						<label class="width30">Amount in Words Only</label>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
			<div class="portlet-header ui-widget-header float-right discard" id="discard"><fmt:message key="re.contract.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right" id="request" style="cursor:pointer;"><fmt:message key="cs.common.button.requestforverification"/></div>
		 	<div class="portlet-header ui-widget-header float-right save-all" id="save"><fmt:message key="re.contract.save"/></div> 
		</div>
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="contract-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="contract-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>