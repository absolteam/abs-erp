<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var slidetab;
var actualID;
$(function(){
	$('#contractId').val($('#contractIdTemp').val());
	$('#personId').val($('#personIdTemp').val());
	$('#ebBillStatusCode').val($('#ebBillStatusCodeTemp').val());
	$('#waterBillStatusCode').val($('#waterBillStatusCodeTemp').val());
	$('#releaseType').val($('#releaseTypeTemp').val());
	
	if($('#waterCertiDate').val() =='01-Jan-1900'){
		$('#waterCertiDate').val("");
	}
	if($('#defaultPopup').val() =='01-Jan-1900'){
		$('#defaultPopup').val("");
	}

	 $("#flatMaintainenceReleaseEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	
	$('.formError').remove();

	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/flat_maintainence_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
}); 	 	
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.flatmaintenancerelease"/></div>
	 	<div class="portlet-content">
		 	<form id="offerFormEdit">
				<fieldset>
		 			<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:100px;">
							<div>
								<label><fmt:message key="re.property.info.contractnumber"/></label>	
								 <input type="hidden" name="contractIdTemp" id="contractIdTemp" value="${bean.contractId }"/>
									<select id="contractId" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.item ne null}">
											<c:forEach items="${requestScope.item}" var="contract" varStatus="status">
												<option value="${contract.contractId }">${contract.contractNo }--${contract.buildingName }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>
						 	<div>
								<label><fmt:message key="re.property.info.engineername"/></label>	
								 <input type="hidden" name="personIdTemp" id="personIdTemp" value="${bean.personId }" />
									<select id="personId" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemList ne null}">
											<c:forEach items="${requestScope.itemList}" var="person" varStatus="status">
												<option value="${person.personId }">${person.personName }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>
							<fieldset>
				                    <legend><fmt:message key="re.property.info.description"/></legend>
				                    <div style="height:25px" class="width60 float-left">
				                            <div id="barbox">
				                                  <div id="progressbar"></div>
				                            </div>
				                            <div id="count">500</div>
				                      </div>
				                      <p>
				                        <textarea class="width60 float-left tooltip" disabled="disabled" id="flatDescription" title="PLease Enter Description">${bean.flatDescription }</textarea>
				                      </p>
				                 </fieldset>
						</fieldset>
				 	</div>
				 	<div class="width50 float-right" id="hrm">
						<fieldset style="min-height:100px;">
							<div>
								<label><fmt:message key="re.property.info.watercertifino"/></label>
								<input type="text" name="waterCertiNo" id="waterCertiNo" disabled="disabled" class="width30" value="${bean.waterCertiNo }" >
							</div>
							<div>
								<label><fmt:message key="re.property.info.watercertifidate"/></label>
								<input type="text" name="waterCertiDate" disabled="disabled" class="width30" id="waterCertiDate" readonly="readonly" value="${bean.waterCertiDate }">
							</div>
							<div>
								<label>Water Bill Status</label>	
									<input type="hidden" name="waterBillStatusCodeTemp" id="waterBillStatusCodeTemp" value="${bean.waterBillStatusCode }" />
									<select id="waterBillStatusCode" disabled="disabled" class="width30">
										<option value="">-Select-</option>
										<c:if test="${requestScope.beanStatus ne null}">
											<c:forEach items="${requestScope.beanStatus}" var="beanstatus" varStatus="status">
												<option value="${beanstatus.statusCode }">${beanstatus.statusName }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>
							<div>
								<label><fmt:message key="re.property.info.eleccertifino"/></label>
								<input type="text" name="electricCertiNo" disabled="disabled" id="electricCertiNo" class="width30" value="${bean.electricCertiNo }">
							</div>
							<div>
								<label><fmt:message key="re.property.info.eleccertifidate"/></label>
								<input type="text" name="electricCertiDate" disabled="disabled" id="defaultPopup" class="width30" readonly="readonly" value="${bean.electricCertiDate }">
							</div>	
							<div>
								<label>EB Bill Status</label>	
									<input type="hidden" name="ebBillStatusCodeTemp" id="ebBillStatusCodeTemp" value="${bean.ebBillStatusCode }" />
									<select id="ebBillStatusCode" disabled="disabled" class="width30 ">
										<option value="">-Select-</option>
										<c:if test="${requestScope.beanStatus ne null}">
											<c:forEach items="${requestScope.beanStatus}" var="beanstatus1" varStatus="status">
												<option value="${beanstatus1.statusCode }">${beanstatus1.statusName }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>
							<div>
								<label><fmt:message key="re.property.info.releasetype"/></label>
								<input type="hidden" name="releaseTypeTemp" id="releaseTypeTemp" value="${bean.releaseType }" />
								<select id="releaseType" disabled="disabled" class="width30">
										<option value="">-Select-</option>
										<c:if test="${requestScope.releaseStatus ne null}">
											<c:forEach items="${requestScope.releaseStatus}" var="beanstatus1" varStatus="status">
												<option value="${beanstatus1.releaseType }">${beanstatus1.releaseTypeName }</option>
											</c:forEach>
										</c:if>
									</select>
							</div>
						</fieldset>
				 	</div>
	 			</fieldset>
			</form>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.amountdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width10"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20">Condition/Status</th>
				            					<th class="width10"><fmt:message key="re.property.info.amount"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.remarks"/></th>
				            				</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="rowid">  	 
													<td class="width10">${result.lineNumber}</td>
													<td class="width10">${result.maintainenceCode}</td>	
													<td class="width20">${result.conditionStatusName }</td>
													<td class="width10 amountcalc">${result.amount}</td> 
													<td class="width20">${result.remarks}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.flatChildId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style="display:none;" class="release_type"></td>
													<td style="display:none;">${result.conditionStatusCode}</td>		 
												</tr>  
								  		 	</c:forEach>	
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					</form>
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<input class="width40" type="hidden" name="offerId" id="flatId" disabled="disabled" value="${bean.flatId}" />
		 	<div class="float-right buttons ui-widget-content ui-corner-all">
		 			<div class="portlet-header ui-widget-header float-right discard" style="display:none;">Contract</div> 
					<div class="portlet-header ui-widget-header float-right headerData" id="close" style="cursor:pointer;"><fmt:message key="re.property.info.close"/></div> 
			</div>
 		</div>
 	</div>
 </div>


			

	 	
