<tr id="featurefieldrow_${rowid}" class="featurerowid">
	<td class="width10" id="featureCode_${rowid}"></td>
      <td class="width10" id="featureType_${rowid}"></td>
      <td class="width10" id="feature_${rowid}"></td>
      <td style="width:5%;" id="featureoption_${rowid}"> 
         			<input type="hidden" name="propertyDetailId_${rowid}" id="propertyDetailId_${rowid}"  />
            		<input type="hidden" name="lookupDetailId_${rowid}" id="lookupDetailId_${rowid}" />  
            		<input type="hidden" name="featurelineId_${rowid}" id="featurelineId_${rowid}" value="${rowid}"/>  
                		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureaddDataNew" id="featureAddImage_${rowid}" style="cursor:pointer;" title="Add Record">
	 	 <span class="ui-icon ui-icon-plus"></span>
	   </a>
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureeditDataNew" id="featureEditImage_${rowid}" style="display:none;cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
	   </a> 
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featuredelrowNew" id="featureDeleteImage_${rowid}" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
	   </a>
	    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="featureWorkingImage_${rowid}" style="display:none;cursor:pointer;" title="Working">
			<span class="processing"></span>
	   </a>
                
	</td> 
</tr>
<script type="text/javascript">
$(function (){
	//Feature Detail adding
	 $(".featureaddDataNew").click(function(){ 
		slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($jquery("#editCalendarVal").validationEngine('validate')){
				$("#featureAddImage_"+rowid).hide();
				$("#featureEditImage_"+rowid).hide();
				$("#featureDeleteImage_"+rowid).hide();
				$("#featureWorkingImage_"+rowid).show(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/feature_add.action",
					data:{id:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			});
			
		}
		else{
			return false;
		}	
		 
	});

	$(".featureeditDataNew").click(function(){
		 
		 slidetab=$(this).parent().parent().get(0);  
		 
		 if($jquery("#editCalendarVal").validationEngine('validate')) {
			 
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#featureAddImage_"+rowid).hide();
					$("#featureEditImage_"+rowid).hide();
					$("#featureDeleteImage_"+rowid).hide();
					$("#featureWorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/feature_add.action",
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   			
	   			var featureCode= $('#featureCode_'+rowid).text();
	   			var featureType=$('#featureType_'+rowid).text(); 
 				var feature=$('#feature_'+rowid).text(); 
 				var lookupDetailId=$('#lookupDetailId_'+rowid).val();
 				
 				$('#featureCode').val(featureCode);
 				$('#featureType').val(featureType);
 				$('#feature').val(feature);
 				$('#lookupDetailId').val(lookupDetailId);
			    $('#openFlag').val(1);
			  
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			 			
		 }
		 else{
			 return false;
		 }	
		 
	});
	 
	 $(".featuredelrowNew").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
		 var lineId=$('#featurelineId_'+rowid).val();
		
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/feature_save.action", 
				 data:{id:lineId,addEditFlag:"D"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $('.tempresult').html(result);   
					 if(result!=null){
						 $("#featurefieldrow_"+rowid).remove(); 
						  var identitycount=Number($('#featurecount').val()); 
        				  identitycount-=1;
        				  $('#featurecount').val(identitycount);
			        		//To reset sequence 
			        		var i=0;
			     			$('.featurerowid').each(function(){  
								i=i+1;
								$($($(this).children().get(4)).children().get(1)).val(i); 
		   					 }); 
					     }  
				},
				error:function(result){
					 $('.tempresult').html(result);   
					 return false;
				}
					
			 });
			 
		 
	 });
});
 </script>