<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-tenantInformation_search.action?tenantName="+tenantName+'&tenantType ='+tenantType+'& uaeIdNo ='+uaeIdNo+'& mobileNo ='+mobileNo+'& birthDate ='+birthDate+'&presentAddress ='+presentAddress+'&profession ='+profession+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['Tenant Id','<fmt:message key="re.owner.info.tenantname"/>','<fmt:message key="re.owner.info.civilidno"/>','<fmt:message key="re.owner.info.mobileno"/>','<fmt:message key="re.owner.info.birthdate"/>','<fmt:message key="re.owner.info.presentaddress"/>','<fmt:message key="re.owner.info.permanentaddress"/>','<fmt:message key="re.owner.info.proffession"/>','<fmt:message key="re.owner.info.emailid"/>','<fmt:message key="re.owner.info.landlineno"/>',
			  		 '<fmt:message key="re.owner.info.tenanttype"/> ','tenantType','Work Flow Status','Approved Status'], 
			 colModel:[ 
					{name:'tenantId',index:'TENANT_INFO_ID', width:100,  sortable:true},
					{name:'tenantName',index:'TENANT_NAME', width:100,  sortable:true},
			  		{name:'uaeIdNo',index:'UAE_ID_NO', width:100,sortable:true, sorttype:"data"} ,
			  		{name:'mobileNo',index:'MOBILE_NO', width:100,  sortable:true},
			  		{name:'birthDate',index:'BIRTH_DATE', width:100,  sortable:true},
			  		{name:'presentAddress',index:'PRESENT_ADDRESS', width:100,  sortable:true},
			  		{name:'permanentAddress',index:'PERMANANT_ADDRESS', width:100,  sortable:true},
			  		{name:'profession',index:'PROFESSION', width:100,  sortable:true},
			  		{name:'emailId',index:'EMAIL_ID', width:100,  sortable:true},
			  		{name:'landLineNo',index:'LANDLINE_NO', width:100,  sortable:true},
			  		{name:'description',index:'DESCRIPTION', width:100,  sortable:true},
			  		{name:'tenantType',index:'TENANT_TYPE', width:100,  sortable:true},
			  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,  sortable:true},
			  		{name:'approvedStatusName',index:'A.LOV_MEANING', width:100,sortable:true, sorttype:"data"},
			 ], 		  				  		  
			 rowNum:10, 		 
			 rowList:[5,10,20,30,35], 
			 pager: '#pager2', 
			 sortname: 'TENANT_INFO_ID', 
			 viewrecords: true, 
			 sortorder: "desc", 
			 caption:'<fmt:message key="re.owner.info.tenantinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["tenantId","tenantType","approvedStatusName"]);
});
	</script>
	 <body>
	 
	<input type="hidden" id="bankName"/> 
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body>
 	
