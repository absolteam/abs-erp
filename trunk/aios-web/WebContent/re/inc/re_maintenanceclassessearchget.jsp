<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">-Select-</option>
<c:forEach items="${relativeClass}" var="bean">
	<option value="${bean.classId}">${bean.className}</option>
</c:forEach>

