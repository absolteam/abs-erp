<tr class="owner">  	 
	<td class="width20"></td>
	<td class="width20"></td>	
	<td class="width20"></td> 
	<td class="width20 percentage"></td>
	<td style="display:none"> 
		<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
	</td> 
	<td style=" width:10%;"> 
				<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwnerNew" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
	   	</a>	
	   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwnerNew"  style="display:none;cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwnerNew" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
			<span class="processing"></span>
		</a>
	</td>  
	<td style="display:none;" class="owner_id"></td>
	<td style="display:none;"></td>		 
</tr>  
<script type="text/javascript">
$(function (){
//Adding Owner Details
$('.addOwnerNew').click(function(){ 
	if(openFlag==0){
		if($jquery('#propertyInfoAdd').validationEngine('validate')){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			//alert($(this).parent().parent().get(0).tagName);
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
			
			//alert(slidetabrev);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_property_owner_redirect.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
		         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		         $('#loading').fadeOut();
		         openFlag=1;
				}
			});
		}else{
			return false;
		}
		event.preventDefault();
	}
});
$(".editOwnerNew").click(function(){
	if(openFlag==0){
		if($jquery('#propertyInfoAdd').validationEngine('validate')){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_edit_property_owner_redirect.action",  
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 $(result).insertAfter(slidetab);
				 $($($(slidetab).children().get(10)).children().get(1)).hide();	//Edit Button
		         $($($(slidetab).children().get(10)).children().get(2)).hide(); //Delete Button
		         $($($(slidetab).children().get(10)).children().get(3)).show();	//Processing Button
					
				 var temp1= $($(slidetab).children().get(0)).text();
				 var temp2= $($(slidetab).children().get(1)).text();
				 var temp3= $($(slidetab).children().get(2)).text();
				 var temp4= $($(slidetab).children().get(3)).text();
	
				 var temp7= $($(slidetab).children().get(6)).text();
	
	
	
	
				 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
				
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(2)).val(temp7);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
				$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
				
			  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
			  		$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(2)).val(temp4);
			  	$('#loading').fadeOut();
			  	openFlag=1;
	
				}
			});
		}else{
			return false;
		}
		event.preventDefault();
	}	
});

	$('.delOwnerNew').click(function(){
		if(openFlag==0){
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
		    $('.childCountErr').hide();
		    $('#warningMsg').hide();
			var trnValue=$("#transURN").val(); 
			actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
			var flag = false; 
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
			if(actualID != null && actualID !='' && actualID!=undefined){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_delete_property_owner_update.action", 
				 	async: false,
				 	data: {trnValue:trnValue, actualLineId: actualID},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
	       		if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		$('#percentageTotal').val(0)
		    			
		    		
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount);
	       		 	}
		   		}
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
			event.preventDefault();
		}	
	});
});	
</script>