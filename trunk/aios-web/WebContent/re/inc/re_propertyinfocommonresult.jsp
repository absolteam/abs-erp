<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <c:if test="${requestScope.bean != null && requestScope.bean !=''}">
 <input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
 <c:choose>
 	<c:when test="${bean.sqlReturnStatus == 1}">
 		 <div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}"</div>
 	</c:when>
 	<c:when test="${bean.sqlReturnStatus != 1}">
		<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
	</c:when>
 </c:choose> 
 </c:if> 