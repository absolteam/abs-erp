<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			property owner
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="components" class="autoComplete_main width50">
	     	<option value=""></option>
	     	<c:forEach items="${itemList}" var="beans" varStatus="status">
	     		<option value="${beans.companyId}|${beans.maintenanceCompanyTradeId}|${beans.details}">${beans.maintenanceCompanyName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 propertymain_list" style="padding:4px;"> 
     	<ul>
     		<li class="width48 float-left"><span>Company Name</span></li> 
   			<li class="width48 float-left"><span>Details</span></li> 
   			<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
     		<c:forEach items="${itemList}" var="bean" varStatus="status">
	     		<li id="property_${status.index+1}" class="propertymain_li" style="height: 20px;">
	     			<input type="hidden" value="${bean.maintenanceId}" id="maintenanceIdbyid_${status.index+1}">
	     			<input type="hidden" value="${bean.maintenanceCompanyTradeId}" id="maintenanceTradeIdbyid_${status.index+1}"> 
	     			<input type="hidden" value="${bean.companyId}" id="companyIdbyid_${status.index+1}"/>
	     			<input type="hidden" value="${bean.maintenanceCompanyName}" id="maintenanceCompanyNameid_${status.index+1}"> 
	     			<input type="hidden" value="${bean.details}" id="detailsbynameid_${status.index+1}"> 
	     			<span id="maintenanceCompanyName_${status.index+1}" style="cursor: pointer;" class="float-left width48">${bean.maintenanceCompanyName} </span>   
	     			<span style="cursor: pointer;" class="float-left width48">${bean.details}
	     				<span id="mainselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
	     			</span>  
	     		</li>
	     	</c:forEach> 
     	</ul> 
	</div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="closemaintance" style="cursor:pointer;">close</div>  
	</div>	
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){
	$($($('#maintenance-popup').parent()).get(0)).css('width',500);
	$($($('#maintenance-popup').parent()).get(0)).css('height',250);
	$($($('#maintenance-popup').parent()).get(0)).css('padding',0);
	$($($('#maintenance-popup').parent()).get(0)).css('left',0);
	$($($('#maintenance-popup').parent()).get(0)).css('top',100);
	$($($('#maintenance-popup').parent()).get(0)).css('overflow','hidden');
	$('.autoComplete_main').combobox({ 
	       selected: function(event, ui){  
	    	   var companydetails=$(this).val();
	    	   var companyarray=companydetails.split('|'); 
	    	   $('#companyCode').val(companyarray[0]);
			   $('#maintenanceTradeId').val(companyarray[1]); 
			   $('#maintenanceDetails').val(companyarray[2]); 
			   $('#maintenanceName').val($(".autoComplete_main option:selected").text()); 
		       $('#maintenance-popup').dialog("close"); 
	   } 
	}); 
	$('#maintenance-popup').dialog('open'); 
	$('.propertymain_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.propertymain_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#mainselect_'+rowid).hasClass('selected')){ 
				 $('#mainselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#mainselect_'+rowid).addClass('selected');
	});
	
	$('.propertymain_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]); 
		 $('#maintenanceId').val($('#maintenanceIdbyid_'+rowid).val());
		 $('#maintenanceTradeId').val($('#maintenanceTradeIdbyid_'+rowid).val());
		 $('#companyCode').val($('#companyIdbyid_'+rowid).val());
		 $('#maintenanceName').val($('#maintenanceCompanyNameid_'+rowid).val());
		 $('#maintenanceDetails').val($('#detailsbynameid_'+rowid).val()); 
		 $('#maintenance-popup').dialog('close'); 
	});
	$('#closemaintance').click(function(){
		$('#maintenance-popup').dialog('close'); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.propertymain_list ul li{
		padding:3px;
	}
	.propertymain_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>