<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-property_flat_search.action?buildingId="+buildingId+'&componentType ='+componentType+'& fromDate ='+fromDate+'& toDate ='+toDate+'& flatNumber ='+flatNumber+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['propertyFlatId','buildingId','Component Type','Building Name','Flat Number','component Type Name','From Date','To Date','Total Area','Work Flow Status'], 
		 colModel:[ 
				{name:'propertyFlatId',index:'PROP_FLAT_ID', width:100,  sortable:true},
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'componentType',index:'COMPONENT_TYPE', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME', width:100,  sortable:true},
				{name:'flatNumber',index:'FLAT_NUMBER', width:100,  sortable:true},
				{name:'componentTypeName',index:'A.LOV_CODE', width:150,sortable:true, sorttype:"data"},
				{name:'fromDate',index:'FROM_DATE', width:100,  sortable:true},
		  		{name:'todate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'totalArea',index:'TOTAL_AREA', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,  sortable:true},
		  	 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'PROP_FLAT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"Property Flat"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["propertyFlatId","buildingId","componentType"]);
});
	</script>
 </head>
 <body>
<input type="hidden" id="bankName"/> 
<table id="list2"></table>  
<div id="pager2"> </div> 
</body>
