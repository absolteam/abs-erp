<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<style>
.certificate_partition 
{ 
border-bottom: 1px dotted #DDDDDD; 
width: 800px;
float:left;
margin:5px 0 !important;
} 
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" /> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var actualID; var slidetab="";
var detailRowAdded = 0;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){ 
	getCurrentDate();

	 var waterCert=$("#waterCertStatus").val();
	 var electricityCert=$("#electricityCertStatus").val();
	 var addcCert=$("#addcCertStatus").val();
	 var checkPayment=$("#checkPayment").val();
	 var checkDamage=$("#checkDamage").val();
	 var printTaken=$("#printTaken").val();
	 
	//Contract AMount total	
	var	total=0;
		$('.contractAmountCalc').each(function(){
			total+=convertToDouble($(this).html());
			$(this).html(convertToAmount($(this).html()));
		});
		$('#contractAmountTotal').val(convertToAmount(total));
		$('#rentAmountTotalAll').val(convertToAmount(total));
		
		var penaltyAmount=convertToDouble($('#penaltyAmount').val());
		if(penaltyAmount!=null && penaltyAmount!=""){
			$('.penalty-calculation-area').show();
			$('#penaltyDiv').show();
		}
			
	
	var i=1;
	var activeFlag=1;
	var tabName="";
	$('.tabcontrol').each(function(){
		slidetab=$(this);
		
			if(checkPayment=="false" && checkDamage=="false" && i==1){
				$($(slidetab).children().get(0)).trigger('click');
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");
				activeFlag=i;
				$("#flowStepCount").val(i);
			}
			else if((waterCert=="false" || electricityCert=="false") && checkDamage=="true" && i==2){
				$($(slidetab).children().get(0)).trigger('click');
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");
				activeFlag=i;
				$("#flowStepCount").val(i);
			}
			else if(waterCert=="true" && electricityCert=="true" && checkDamage=="true" && checkPayment=="false" && i==3){
				$($(slidetab).children().get(0)).trigger('click');
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");
				activeFlag=i;
				$("#flowStepCount").val(i);
				$("#PrintMissing").hide();
			}
			else if(waterCert=="true" && electricityCert=="true" && checkDamage=="true" && checkPayment=="true" && printTaken=="false" && i==3){
				$($(slidetab).children().get(0)).trigger('click');
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");
				activeFlag=i;
				$("#flowStepCount").val(i);
				$('#otherCharges').attr('readonly','readonly');
				$("#PrintMissing").show();
				
			}
			else if(checkPayment=="true" && checkDamage=="true" && printTaken=="true" && i==4){
				$($(slidetab).children().get(0)).trigger('click');
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");
				activeFlag=i;
				$("#flowStepCount").val(i);
				$('#otherCharges').attr('readonly','readonly');
				$("#PrintMissing").hide();
			} 
		
			else{ 
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow.png'/>");
			} 
			
			//$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow.png'/>");
			//$($(slidetab).children().get(0)).replaceWith("<span><img width='25' height='15' src='images/icons/tab_arrow.png'/></span>"); 
			//$($(slidetab).children().get(0)).css('cursor', 'default');
			//$(slidetab).removeClass("tabcontrol");
			
			i++;

		
			
	});
	var j=1;
	$('.tabcontrol').each(function(){
		slidetab=$(this);
		if(j==1)
			tabName="Release Inspection";
		if(j==2)
			tabName="Document";
		if(j==3)
			tabName="Payments";
		if(j==4)
			tabName="ADDC Process";
		//Non clickable event
		if(activeFlag<j){
			$($(slidetab).children().get(0)).replaceWith("<span>"+tabName+"<img width='25' height='15' src='images/icons/tab_arrow.png'/></span>"); 
			//$($(slidetab).children().get(0)).css('cursor', 'default');
		}
		
		j++;
	});
	
	$('.tabImageChange').click(function(){
		$('.tabcontrol').each(function(){
			slidetab=$(this);
			$($($(slidetab).children().get(0)).children().get(0)).remove();
			if($(slidetab).hasClass('ui-state-active')){
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow_active.png'/>");  
			}else{
				$($(slidetab).children().get(0)).append("<img align='bottom' width='25' height='15' src='images/icons/tab_arrow.png'/>"); 
			} 
		});
	});

	
	
	//Document upload thickbox close button call for auto check the check box
	$(".ui-button").live('click',function(){
		checkboxCheckEven();
	});
	
	$(".ui-button-text-only").live('click',function(){
		checkboxCheckEven();
	});
	//Document upload
	var releaseId=$("#relaseId").val();
	$('#water_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","waterDocs","Release",releaseId,"ReleaseDocument");
	});
	 
	$('#electricity_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","electricityDocs","Release",releaseId,"ReleaseDocument");
	});
	
	$('#addc_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","addcDocs","Release",releaseId,"ReleaseDocument");
		
	}); 

	
	
	//Onload Call
	var releaseId=$('#releaseId').val();
	if(releaseId!=null && releaseId!=0){
		//Sum the amount
		//$.fn.calmaintenanceTotal();
		var total=0;
		$('.maintenanceamount').each(function(){ 
			total+=convertToDouble($(this).html());
		});
		$('#maintenanceTotal').val(convertToAmount(total));
		$('#damageTotal').val(convertToAmount(total));
		
		$('.maintenanceamount').each(function(){ 
			$(this).text(convertToAmount($(this).text()));
		});
		//calculation for other charges if payment processed already
		if(convertToDouble($('#refundAmount').val())>0){
			var depositAmoun=convertToDouble($('#depositAmount').val());
			var damagetotals=convertToDouble($('#damageTotal').val());
			var refundAmoun=convertToDouble($('#refundAmount').val());
			var charges=Number(depositAmoun-damagetotals);
			charges=charges-refundAmoun;
			$('#otherCharges').val(convertToAmount(charges));
		}

		totalRefundCalculation();

		//Date Check blur
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}


		//Get file names which is uploded already
		if($("#waterCertStatus").val()=='true'){
			populateUploadsPane("doc","waterDocs","Release",releaseId);
		}

		if($("#electricityCertStatus").val()=='true'){	
			populateUploadsPane("doc","electricityDocs","Release",releaseId);
		}

		if($("#addcCertStatus").val()=='true'){
			populateUploadsPane("doc","addcDocs","Release",releaseId);
		}

		//Upload check box
		checkboxCheckEven();
	}
	
	
	 $('#otherCharges').blur(function(){
			totalRefundCalculation();
	 });
	 
	 
	 $(".add_detail").live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);   
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($jquery("#editCalendarVal").validationEngine('validate')){
				$("#AddImage_"+rowid).hide();
				$("#EditImage_"+rowid).hide();
				$("#DeleteImage_"+rowid).hide();
				$("#WorkingImage_"+rowid).show();  
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_release_maintain_detail.action",
					data:{id:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			}); 
			return false;
		}
		else{
			return false;
		}	
		return false;
	});

	$(".edit_detail").live('click',function(){ 
		 if($('#openFlag').val() == 0) { 
			 slidetab=$(this).parent().parent().get(0); 
			 if($jquery("#editCalendarVal").validationEngine('validate')) {  
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
				$("#AddImage_"+rowid).hide();
				$("#EditImage_"+rowid).hide();
				$("#DeleteImage_"+rowid).hide();
				$("#WorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/add_release_maintain_detail.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				 	 dataType: "html",
					 cache: false,
				   	success:function(result){ 
				  		$(result).insertAfter(slidetab); 
		   				var assetName=$('#assetName_'+rowid).text();
		   				var damage=$('#damage_'+rowid).text(); 
		   				var balanceAmount=$('#balanceAmount_'+rowid).text(); 
	
		   				$('#assetName').val(assetName);
		   				$('#damage').val(damage);
		   				$('#balanceAmount').val(balanceAmount);
				    	$('#openFlag').val(1); 
					} 
				}); 
				 return false;	
		 }
		 else{
			 return false;
		 }	
		 }else{
			 return false;
		 } 
		 return false;
	});
	 
	 $(".del_detail").live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0); 
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		 var lineId=$('#lineId_'+rowid).val();
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/save_release_maintain_detail.action", 
				 data:{id:lineId,addEditFlag:"D"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){ 
					 $("#fieldrow_"+rowid).remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowid').each(function(){  
							i=i+1;
							var currentid=$(this).attr('id'); 
						 	var idval=currentid.split('_');
							$('#lineId_'+idval[1]).val(i);
	   					 }); 
				} 
			 });

			 //Maintenance Total Calculation
			$.fn.calmaintenanceTotal();
				
			return false;
	 });

	 //Add Row
	 $('.addrows_detail').click(function(){
			var i=0;
		 	var tabsize=$('.tab').size()+1;
			if(tabsize!=null){
				rowid=Number(tabsize+1);
			}else{
				rowid=Number(1);
			} 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/release_maintenance_addrow_get.action",
				data:{id:rowid}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				$('.asset_tab').append(result);
				 $('.rowid').each(function(){  
					 	i=i+1;
					 	var currentid=$(this).attr('id');  
					 	var idval=currentid.split('_');
						$('#lineId_'+idval[1]).val(i);
					});  
				} 
			
			});
		});
	 
		
	 $('.save_detail').click(function(){

		 var releaseId=$("#releaseId").val(); 
		 var waterCert=$("#waterCert").attr('checked');
		 var electricityCert=$("#electricityCert").attr('checked');
		 var addcCert=$("#addcCert").attr('checked');
		 var balanceAmount=convertToDouble($("#refundAmount").val());
		 var contractId=$("#contractId").val();
		 var checkPayment=$("#checkPayment").val();
		 var checkDamage=$("#checkDamage").val();
		 var printTaken=$("#printTaken").val(); 
		 var effectiveDate=$("#effectiveDate").val(); 
		 
		 if($jquery("#editCalendarVal").validationEngine('validate')){
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/final_save_release_maintain_detail.action", 
				 data:{releaseId:releaseId,
					 contractId:contractId,
					 waterCert:waterCert,
					 electricityCert:electricityCert,
					 addcCert:addcCert,
					 balanceAmount:balanceAmount,
					 checkPayment:checkPayment,
					 checkDamage:checkDamage,
					 printTaken:printTaken,
					 effectiveDate:effectiveDate
					 },
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){  
					if($("#flowStepCount").val()==4){
						$("#main-wrapper").html(result); 
					}else if($("#flowStepCount").val()==3){
						window.location.reload();
					}
					else{
						redirecTOSamePageCall();
					}	 
			   },
		       error:function(result){  
				  $("#main-wrapper").html(result);
				  $(".success").hide();
				  $('.error').show();  
			  }
		   }); 
		 }
		 else{
			 return false;
		 }		  
	 });
      
	 $(".cancel_detail").click(function(){
		 slidetab=$(this).parent().parent().get(0);
		 var trnVal=$("#transURN").val(); 
		 $.ajax({
			 type:"POST",
			 url:"<%=request.getContextPath()%>/release_contract_redirect.action", 
			 //data:{trnValue:trnVal},
			 async: false,
			  dataType: "html",
			   cache: false,
			   success:function(result){  
				 $('.formError').hide(); 
				 	$('#transURN').val("");  
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html(); 
				    if(tempsMsg!=""){
				       $('#sMsg').show(); 
				       $('#eMsg').hide();
				     }
				     else{ 
				    	$('#eMsg').show(); 
				    	$('#sMsg').hide();
				    }  
			 },
		     error:function(result){
				 $("#main-wrapper").html(result);
				 $(".success").hide();
				 $('.error').show(); 
				 $("#transURN").val(""); 
				 $("#trnValue").val("");
			 }
		 });
			 
	});
	
	
	 
	//Calculation part
	$.fn.calmaintenanceTotal = function() { 	// Total maintenance amount
		var total=0;
		$('.maintenanceamount').each(function(){ 
			total+=convertToDouble($(this).html());
		});
		$('#maintenanceTotal').val(total);

		//Convert Amount
		$('.maintenanceamount').each(function(){ 
			$(this).html(convertToAmount($(this).html()));
		});
		$('#maintenanceTotal').val(convertToAmount($('#maintenanceTotal').val()));
	 }
	
	


	 //Date part
	 $('#tenantReleaseLetterDate').datepick();
	 $('#waterCertiDate').datepick();
	 $('#electricCertiDate').datepick();//EB
	 
	 $('#startPicker,#endPicker').datepick({
	 		onSelect: customRange, showTrigger: '#calImg'
	 });

	 //Description part
	 $("#flatDescription").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#flatDescription").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#flatDescription").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 
	    
	  //Hide other action button and show the release button
		if($("#flowStepCount").val()==4 && $("#addcCert").attr('checked')==true ){
			$(".save_detail").hide();
		}
	    
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
function totalRefundCalculation(){
	var total=convertToDouble($('#maintenanceTotal').val());
	$('#damageTotal').val(total);
	var otherCharges=convertToDouble($('#otherCharges').val());
	var penaltyAmount=convertToDouble($('#penaltyAmount').val());
	$('#totalDeduction').val(total+otherCharges+penaltyAmount);
	$('#depositAmount1').val($('#depositAmount').val());
	var totalDeduction=convertToDouble($('#totalDeduction').val());
	var depositAmount=convertToDouble($('#depositAmount1').val());
	$('#refundAmount').val(depositAmount-totalDeduction);
	$('#totalDeduction1').val(totalDeduction);
	//Convert to amount format
	$('#damageTotal').val(convertToAmount($('#damageTotal').val()));
	$('#totalDeduction').val(convertToAmount($('#totalDeduction').val()));
	$('#depositAmount1').val(convertToAmount($('#depositAmount1').val()));
	$('#refundAmount').val(convertToAmount($('#refundAmount').val()));
	$('#totalDeduction1').val(convertToAmount($('#totalDeduction1').val()));
	$('#contractAmount').val(convertToAmount($('#contractAmount').val()));
	$('#depositAmount').val(convertToAmount($('#depositAmount').val()));
	$('#otherCharges').val(convertToAmount($('#otherCharges').val()));
	$('#penaltyAmount').val(convertToAmount($('#penaltyAmount').val()));
	return true;
	
}
function checkboxCheckEven(){ 
	if($("#waterDocs").text()!=''){
		$("#waterCert").attr("checked","checked");
	}
	if($("#electricityDocs").text()!=''){
		$("#electricityCert").attr("checked","checked");
	}
	if($("#addcDocs").text()!=''){
		$("#addcCert").attr("checked","checked");
	} 
}
function redirecTOSamePageCall(){
	 var releaseId=$("#releaseId").val(); 
	 var contractId=Number($("#contractId").val());
	 
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/release_contract_info_edit.action",  
     	data: {releaseId:releaseId,contractId:contractId}, 
     	async: false,
		dataType: "html",
		cache: false,
     	success: function(data){
			$("#main-wrapper").html(data); //gridDiv main-wrapper
     	}
	});
}
function getCurrentDate(){
	
	var effectDat=$('#effectiveDate').val().trim();
//Cancellation Date
if(effectDat==""){
	var curDate=new Date().toString();
	var splitDate=curDate.split(" ");
	var day=splitDate[2];
	var mon=splitDate[1];
	var year=splitDate[3];
	fromDate=day+"-"+mon+"-"+year;
	$('#effectiveDate').val(fromDate);
	$('#effectiveDate').datepick();
}
}
</script>

<div id="main-content">
<div style="display:none;" class="tempresult"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Release Information
		</div>
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Property Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
	<form id="contractAgreementEdit" style="position: relative;">
	 		<fieldset style="padding-left: 10px;">
	 			<legend>Offer & Contract Details</legend>
	 			<div class="width33 float-left" id="hrm">
				  <fieldset style="height:130px;">
						<input type="hidden" name="releaseId" value="${RELEASE_INFO.releaseId }"  id="releaseId" >
						<input type="hidden" name="checkPayment" value="${RELEASE_INFO.checkPayment }"  id="checkPayment">
						<input type="hidden" name="checkDamage" value="${RELEASE_INFO.checkDamage }"  id="checkDamage">
						<input type="hidden" name="flowStepCount" id="flowStepCount" value="1">
						<input type="hidden" name="printTaken" id="printTaken" value="${RELEASE_INFO.printTaken}">
						
						<div >
							<label class="width30"><fmt:message key="re.contract.contractno"/></label>	 
							<input class="width60" type="text" name="contractNumber" value="${bean.contractNumber }"  id="contractNumber" disabled="disabled" >
							<input class="width50" type="hidden" name="contractId" value="${bean.contractId }"  id="contractId" disabled="disabled" >
							
						</div>
						<div >
							<label class="width30"><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>	 
							<input class="width60 contractDate validate[required]" type="text" name="contractDate" value="${bean.contractDate }"  id="defaultPopup" readonly="readonly" disabled="disabled">
						</div>
						<div>
							<label class="width30">Installments<span style="color:red">*</span></label>	 
							<input class="width60 validate[required,custom[onlyNumber]]" type="text" name="noOfCheques" value="${bean.noOfCheques }"  id="noOfCheques" disabled="disabled">
						</div>
						<div><label class="width30">Contract Fee<span style="color:red">*</span></label>
						<input type="text" class="width60 validate[required]" id="contractAmount" value="${bean.contractAmount }" name="contractAmount" tabindex="7" disabled="disabled">
						</div>
						
						<div><label class="width30">Deposit<span style="color:red">*</span></label>
						<input type="text" class="width60 validate[required]" id="depositAmount" value="${bean.depositAmount }" name="depositAmount" tabindex="8" disabled="disabled">
						</div>
					</fieldset>	
				</div>
	 			<div class="width33 float-left" id="hrm">
	 				<fieldset style="height:130px;">
						<div>
							<label class="width30"><fmt:message key="re.contract.offerno"/><span style="color:red">*</span></label>	 
							<input class="validate[required] width60" type="text" name="offerNumber" value="${OFFER_INFO.offerNumber }" id="offerNumber" disabled="disabled" >
							<input class="width30" type="hidden" name="offerId" value="${OFFER_INFO.offerId }"  id="offerId" disabled="disabled" >
						</div>
						
						<div>
							<label class="width30"><fmt:message key="re.contract.tenantname"/></label>	 
							<input class="width60" type="text" name="tenantName" value="${OFFER_INFO.tenantName }"  id="tenantName" disabled="disabled" >
							<input class="width40" type="hidden" name="tenantId" value="${OFFER_INFO.tenantId }"  id="tenantId" >
							<input class="width30 " id="rentFees" type="hidden" name="rentFees" value="${OFFER_INFO.rentFees }"  disabled="disabled">
							<input class="width30 " id="rentFeesAmount" type="hidden" name="rentFeesAmount" value="${OFFER_INFO.rentAmount }"  disabled="disabled">
						</div>
						<div>
							<label class="width30"><fmt:message key="re.contract.address"/></label>	 
							<input class="width60" type="text" name="presentAddress" value="${OFFER_INFO.presentAddress }"  id="presentAddress" disabled="disabled" >
						</div>
						<div>
							<label class="width30"><fmt:message key="re.contract.address1"/></label>	 
							<input class="width60" type="text" name="permanantAddress" value="${OFFER_INFO.permanantAddress }"  id="permanantAddress" disabled="disabled" >
						</div>
					</fieldset>	
				</div>
				<div class="width33 float-left" id="hrm">
					<fieldset style="height:130px;">
						<div><label class="width30" id="fromDateCheck"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
						<input type="text" class="width60 dateCheck validate[required]" id="startPicker" value="${OFFER_INFO.fromDate }" name="employerFromDate" tabindex="7" readonly="readonly"  disabled="disabled">
						</div>
						
						<div><label class="width30"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
						<input type="text" class="width60 employerToDate dateCheck validate[required]" id="endPicker" value="${OFFER_INFO.toDate }" name="employerToDate" tabindex="8" readonly="readonly"  disabled="disabled">
						</div>
						<div><label class="width30"><fmt:message key="re.contract.years"/></label><input type="text" class="width60" name="years" id="years" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.months"/></label><input type="text" class="width60" name="months" id="months" disabled="disabled"></div>
						<div><label class="width30"><fmt:message key="re.contract.days"/></label><input type="text" class="width60" name="days" id="days"  disabled="disabled"></div>
					</fieldset>	
				</div>
				
		</fieldset>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Rent Details</div>
			<div class="portlet-content">
				<div id="hrm" class="hastable width100"> 
					<table id="hastab" class="width100">	
							<thead class="chrome_tab">									
							<tr>
								<th class="width20">Property/Component/Unit Name</th>
								<th class="width20" style=" width:5%;display:none;"><fmt:message key="re.contract.rentamount"/></th>
								<th class="width20"><fmt:message key="re.contract.contractamount"/></th>
							</tr> 
						</thead> 
						<tbody class="tab tabRent">
							<c:forEach items="${OFFER_DETAIL_LIST}" var="result" varStatus="status" >
							 	<tr class="even"> 
									<td class="width20">${result.flatNumber }</td>	
									<td class="width20 rentAmountCalc" style=" width:5%;display:none;">${result.rentAmount }</td> 
									<td class="width20 contractAmountCalc">${result.contractAmount }</td>
									<td style="display:none"> 
										<input type="hidden" value="${result.contractRentId }" name="actualLineId" id="actualLineId"/>	
										<input type="hidden" name="actionFlag" value="U"/>
		              					<input type="hidden" name="tempLineId" value=""/>
									</td> 
									
									<td style="display:none;" class=""></td>
									<td style="display:none;"></td>	
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>	
			<div class="float-right " style="width:50%">
				<label>Contract Total</label>
			 	<input name="rentAmountTotal" id="rentAmountTotal" class="width40" disabled="disabled" style="display:none">
			 	<input name="contractAmountTotal" id="contractAmountTotal" class="width70" disabled="disabled" >
			</div>
			</div>
			<div class="clearfix"></div>
	</form>
	<div class="clearfix"></div> 
	<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>  
	<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
			<li class="tabcontrol ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1" class="tabImageChange">Release Inspection</a></li>
			<li class="tabcontrol ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-2" class="tabImageChange">Document</a></li>
			<li class="tabcontrol ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-3" class="tabImageChange">Payments</a></li>
			<li class="tabcontrol ui-state-default ui-corner-top ui-tabs-selected"><a href="#tabs-4" class="tabImageChange">ADDC Process</a></li>
		</ul>
		<!--     Tab 1    -->
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-1">
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Damage Details</div>
				<div class="portlet-content">
					<div id="hrm" class="hastable width100"  > 
					 	<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div> 
						<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>
						<input type="hidden" name="trnValue" value="" readonly="readonly" id="transURN"/> 
						<input type="hidden" name="openFlag" id="openFlag"  value="0"/>  
						<input type="hidden" name="tabCount" id="tabCount"  value="2"/>  
						<table id="hastab" class="width100"> 
							<thead>
								<tr>   
									<th class="width20">Asset Name</th>
									<th class="width20">Damage</th>
									<th class="width10">Amount</th>
									<c:if test="${RELEASE_INFO.printTaken eq null || RELEASE_INFO.printTaken eq false }">
										<th style="width:5%;"><fmt:message key="accounts.calendar.label.options"/></th>
									</c:if>	
								</tr>
							</thead>
							<tbody class="tab asset_tab">
							 <c:forEach items="${RELEASE_DETAILS}" var="result" varStatus="status">                         
						        <tr id="fieldrow_${status.index+1}" class="rowid">
						            <td class="width20" id="assetName_${status.index+1}">${result.assetName}</td>
						            <td class="width20" id="damage_${status.index+1}">${result.damage}</td>
						           	<td class="maintenanceamount width10" id="balanceAmount_${status.index+1}">${result.balanceAmount}</td>
						           	<c:if test="${RELEASE_INFO.printTaken eq null || RELEASE_INFO.printTaken eq false }">
						            	 <td style="width:5%;" id="option_${status.index+1}"> 
						                		<input type="hidden" name="releaseDetailId_${status.index+1}" id="releaseDetailId_${status.index+1}"  value="${result.releaseDetailId}"/>  
						                		<input type="hidden" name="lineId_${status.index+1}" id="lineId_${status.index+1}" value="${status.index+1}"/>  
					                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip add_detail" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
											 	 <span class="ui-icon ui-icon-plus"></span>
											   </a>
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip edit_detail" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											   </a> 
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip del_detail" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											   </a>
											    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="WorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
													<span class="processing"></span>
											   </a>
						                  
										</td>
									</c:if> 
						        </tr> 
						    </c:forEach>
						    <c:if test="${RELEASE_INFO.printTaken eq null || RELEASE_INFO.printTaken eq false }">
							    <c:forEach var="i" begin="${fn:length(RELEASE_DETAILS)+1}" end="${fn:length(RELEASE_DETAILS)+1}" step="1" varStatus ="status">  
									<tr id="fieldrow_${i}" class="rowid"> 
										<td class="width20" id="assetName_${i}"></td>
							            <td class="width20" id="damage_${i}"></td>
							           	<td class="maintenanceamount width10" id="balanceAmount_${i}"></td>
								 		<td  style="width:5%;" id="option_${i}">
								 			<input type="hidden" name="releaseDetailId_${i}" id="releaseDetailId_${i}"  value="0"/>  
								 			<input type="hidden" name="lineId_${i}" id="lineId_${i}" value="${i}"/>  
										  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip add_detail" id="AddImage_${i}"  title="Add Record">
										 	 <span class="ui-icon ui-icon-plus"></span>
										   </a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip edit_detail"  id="EditImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
											</a> 
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip del_detail"  id="DeleteImage_${i}" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td> 
										
									</tr>
							 	</c:forEach>
						 	</c:if>
						 </tbody>
					   </table>
					  </div> 
					 <div class="float-right " style="width:30%">
						<label>Total</label>
				 		<input name="maintenanceTotal" id="maintenanceTotal" class="width60" disabled="disabled" >
					</div> 
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" style="display:none"> 
						<div class="portlet-header ui-widget-header float-left addrows_detail" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
					</div>
				 </div>
			 </div>
		 </div>
		 <!--     Tab 2    -->
		 <div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-2">
		 	<form id="DocumentsFrom">
				<div id="UploadDmsDiv" class="width100">
					<div class="width80 float-left" id="hrm">
						<fieldset style="height:100px;">
							<div class="width100 float-left">
								<input type="hidden" name="waterCertStatus" id="waterCertStatus" value="${RELEASE_INFO.waterCert}">
								<input type="checkbox" name="waterCert" id="waterCert" class="width5 float-left validate[required]" disabled="disabled">
								<label class="width20 float-left">ADDC Water Certificate<span class="mandatory">*</span></label>
								<div class="width50 float-left">
									<c:if test="${RELEASE_INFO.printTaken eq null || RELEASE_INFO.printTaken eq false }">
										<div class=" float-left">
										<span id="water_document_information" style="cursor: pointer; color: blue;">
											<u>Upload File</u>
										</span>
										</div>
									</c:if>		
									<div class="width80 float-right">
										<span  id="waterDocs"></span>
									</div>
								</div>
							</div>
							<span class="certificate_partition"></span>
							<div class="width100 float-left">
								<input type="hidden" name="electricityCertStatus" id="electricityCertStatus" value="${RELEASE_INFO.electricityCert}">
								<input type="checkbox" name="electricityCert" id="electricityCert" class="width5 float-left validate[required]" disabled="disabled">
								<label class="width20 float-left">ADDC Electricity Certificate<span class="mandatory">*</span></label>
								<div class="width50 float-left">
									<c:if test="${RELEASE_INFO.printTaken eq null || RELEASE_INFO.printTaken eq false }">
										<div class=" float-left">
										<span id="electricity_document_information" style="cursor: pointer; color: blue;">
											<u>Upload File</u>
										</span>
										</div>
									</c:if>
									<div class="width80 float-right">
									<span id="electricityDocs"></span>
									</div>
								</div>
							</div>
							<%-- <div class="width100 float-left">
								<input type="hidden" name="addcCertStatus" id="addcCertStatus" value="${RELEASE_INFO.addcCert}">
								<input type="checkbox" name="addcCert" id="addcCert" class="width5 float-left validate[required]" disabled="disabled">
								<label class="width20 float-left">ADDC Certificate<span class="mandatory">*</span></label>
								<div class="width50 float-left">
									<div class="float-left">
										<span id="addc_document_information" style="cursor: pointer; color: blue;">
											<u>Upload File</u>
										</span>
									</div>
									<div class="width80 float-right">
										<span id="addcDocs"></span>
									</div>
								</div>
							</div> --%>
							<div class="width100 float-left" style="height:30px;margin-top:15px;">
								<span><font size="4" color="red">Note:</font> Please upload all the required certificate's in order to proceed further</span>
							</div>
						</fieldset>
					</div>
					
				</div>	
			</form>	
		 </div>
		 <!--     Tab 3    -->
		  <div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-3">
		 	<form id="PaymentsFrom">
				<div class="float-left width60" id="hrm">
					<div class="float-left width50" id="hrm">
						<fieldset style="height:150px;">
							<div>
								<span style="font-size: 12px;font-weight: bold;">Damage Total&nbsp;&nbsp;&nbsp;</span>		
								<input type="text" name="damageTotal" id="damageTotal" value="0" readonly="readonly">
							</div>
							<div id="penaltyDiv" style="display:none">
								<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">( + )</span></div>
								<div>
									<span style="font-size: 12px;font-weight: bold;">Penalty Amount&nbsp;</span>
									<input type="text" name="penaltyAmount" id="penaltyAmount" value="${RELEASE_INFO.penaltyAmount}">
								</div>
							</div>	
							<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">( + )</span></div>
							<div>
								<span style="font-size: 12px;font-weight: bold;">Other Charges&nbsp;&nbsp;</span>
								<input type="text" name="otherCharges" id="otherCharges" value="0">
							</div>
							<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">( = )</span></div>
							<div >
								<span style="font-size: 12px;font-weight: bold;">Total Deduction</span>
								<input type="text" name="totalDeduction" id="totalDeduction" value="0" readonly="readonly">
							</div>
						</fieldset>	
					</div>
					<div class="float-left width50" id="hrm">
						<fieldset style="height:150px;">	
							<div>
								<span style="font-size: 12px;font-weight: bold;">Deposit Amount</span>
								<input type="text" name="depositAmount1" id="depositAmount1" value="0" readonly="readonly">
							</div>
							<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">( - )</span></div>
							<div>
								<span style="font-size: 12px;font-weight: bold;">Total Deduction</span>
								<input type="text" name="totalDeduction1" id="totalDeduction1" value="0" readonly="readonly">
							</div>
							<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">( = )</span></div>
							<div>
								<span style="font-size: 12px;font-weight: bold;">Refund Amount</span>
								<input type="text" name="refundAmount" id="refundAmount" value="${RELEASE_INFO.balanceAmount}" readonly="readonly">
							</div>
						</fieldset>
					</div>	
					
					<div class="width100 float-left" style="height:30px;margin-top:15px;" id="PrintMissing">
						<span><font size="4" color="red">Note:</font> Take the print out copy to proceed further</span>
					</div>
				</div>
				<div class="float-left width35 penalty-calculation-area" id="hrm" style="display:none">
					<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;padding:3px;">Penalty Calculation</span></div>
					<div>
						<label>Contract Amount(1yr)</label>
						<label>${RELEASE_INFO.contractTotalStr}</label>
					</div>
					<div>
						<label>Penalty Months</label>
						<label>${RELEASE_INFO.penaltyMonths}</label>
					</div>
					<div style="margin-left:155px"><span style="font-size: 15px;font-weight: bold;">------------------</span></div>
					<div style="padding:1px;">
						<label>Penalty Amount</label>
						<label>${RELEASE_INFO.penaltyAmountStr}</label>
					</div>
				</div>
				
			</form>	
		 </div>
		 <!--     Tab 4    -->
		 <div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="tabs-4">
		 	<div class="width90 float-left" id="hrm">
		 		<fieldset style="height:80px;">
							<input type="hidden" name="addcCertStatus" id="addcCertStatus" value="${RELEASE_INFO.addcCert}">
							<input type="checkbox" name="addcCert" id="addcCert" class="width5 float-left validate[required]" disabled="disabled">
							<label class="width20 float-left">ADDC Certificate<span class="mandatory">*</span></label>
							<div class="width50 float-left">
								<div class="float-left">
									<span id="addc_document_information" style="cursor: pointer; color: blue;">
										<u>Upload File</u>
									</span>
								</div>
								<div class="width80 float-right">
									<span id="addcDocs"></span>
								</div>
							</div>
							<div class="width90 float-left" style="position: relative; top: 14px;">
								<label class="width20 float-left">Effective Date<span style="color:red">*</span></label>
								<input type="text" class="width20 float-left" readonly="readonly"  value="${RELEASE_INFO.effectiveDate}" name="effectiveDate" id="effectiveDate">
							</div>
				</fieldset>
			</div>
		 </div>
		 
	</div>	
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " id="action_buttons" style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right cancel_detail" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
	<div class="portlet-header ui-widget-header float-right save_detail" style="cursor:pointer;">Save</div>
</div>

<input type="hidden" name="availflag" id="availflag" value="1"/>
</div>
</div>