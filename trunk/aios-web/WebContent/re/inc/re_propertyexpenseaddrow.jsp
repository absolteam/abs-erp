<tr class="rowid" id="fieldrow_${ROW_ID}"> 
	<td id="lineId_${ROW_ID}">${ROW_ID}</td>  
	<td>
		<input type="text" class="width98 referenceNo"  id="referenceNo_${ROW_ID}"/>
	</td>
	<td>
		<input type="text" class="width98 amount" id="amount_${ROW_ID}" style="text-align: right;"/>
	</td> 
	<td>
	  <input type="text" name="linedescription" id="linedescription_${ROW_ID}"/>
	</td>
	<td style="display:none;">
		<input type="hidden" id="expenseLineId_${ROW_ID}"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${ROW_ID}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${ROW_ID}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${ROW_ID}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${ROW_ID}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${ROW_ID}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>
<script type="text/javascript">
$(function(){
	$('.linesdate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
});
</script>