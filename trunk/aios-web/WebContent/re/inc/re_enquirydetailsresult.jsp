<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<option value="">Select</option>
<c:if test="${flat != null}">
	<c:forEach items="${requestScope.flat}" var="flatList">
		<option value="${flatList.flatId}">${flatList.flatNumber} </option>
	</c:forEach>
</c:if> 
