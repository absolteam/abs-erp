<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div>
	<label><fmt:message key="re.owner.info.tenanttype"/></label>
	 <select id="tenantType" class="width30" >
			<option value="">-Select-</option>
			<c:if test="${requestScope.lookupList ne null}">
				<c:forEach items="${requestScope.lookupList}" var="tenType" varStatus="status">
					<option value="${tenType.tenantType}">${tenType.description}</option>
				</c:forEach>
			</c:if>
		</select>
</div>
<div id="temp" style="display:none;">
	<label class=""><fmt:message key="re.common.approvedstatus"/></label>
	<select id="approvedStatus" class="width30" >
		<option value="">-Select-</option>
		<c:forEach items="${requestScope.approvedStatus}" var="approvedStatus" varStatus="status">
	<option value="${approvedStatus.approvedStatus }">${approvedStatus.approvedStatusName}</option>
	</c:forEach>
		</select>
</div>