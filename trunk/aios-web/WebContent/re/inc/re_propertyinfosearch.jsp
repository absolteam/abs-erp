<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-propertyinfosearch.action?buildingName="+buildingName+'&floorNo ='+floorNo+'& fromDate ='+fromDate+'& toDate ='+toDate+'& plotNo ='+plotNo+'&addressLine1 ='+addressLine1+'&yearOfBuild ='+yearOfBuild+'&approvedStatus='+approvedStatus, 
		 datatype: "json", 
		 colNames:['Building Id','<fmt:message key="re.property.info.buildingname"/>','<fmt:message key="re.property.info.propertytype"/>',
		  		 '<fmt:message key="re.property.info.nooffloors"/>','<fmt:message key="re.property.info.plotno"/>',
		  		 '<fmt:message key="re.property.info.address1"/>','<fmt:message key="re.property.info.fromdate"/>',
		  		 '<fmt:message key="re.property.info.todate"/>','<fmt:message key="re.property.info.yearofbuild"/>',
		  		'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
				{name:'buildingName',index:'BUILDING_NAME', width:100,  sortable:true},
		  		{name:'propertyTypeName',index:'PROPERTY_TYPE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'floorNo',index:'FLOOR_NO', width:100,  sortable:true},
		  		 {name:'plotNo',index:'PLOT_NO', width:150,sortable:true},
		  		{name:'addressLine1',index:'ADDRESS_LINE_1', width:100,  sortable:true},
		  		{name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'toDate',index:'TO_DATE', width:100,  sortable:true},
		  		 {name:'yearOfBuild',index:'YEAR_OF_BUILD', width:150,sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'BUILDING_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.propertyinformation"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["buildingId","propertyTypeName"]);
}); 	
</script>
<body>
<input type="hidden" id="bankName"/> 
 <table id="list2"></table>  
<div id="pager2"> </div> 
 </body>
 	