<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<option value="">Select</option>
	<c:choose>
 		<c:when test="${requestScope.itemList ne null }">
 			<c:forEach items="${requestScope.itemList}" var="calculate" varStatus="status">
				<option value="${calculate.calculatedAs }">${calculate.calculatedCode }</option>
			</c:forEach>
 		</c:when>
 	</c:choose>
