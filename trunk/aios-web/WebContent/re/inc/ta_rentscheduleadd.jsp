  <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <style>
 	 @media screen and (-webkit-min-device-pixel-ratio:0) {
.tab,.chrome_tab { display:block; }  
} 
 </style>  
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Rent Schedule</div>
			<div class="portlet-content">
			<form name="" id="">
				<div id="tempresult" style="display:none;"></div>
					<div class="width48 float-right" id="hrm"> 
			 				<fieldset class="width100">
								<legend>Rent Information</legend>
									<div class="width100 float-left">
										<div class="float-right width50">
											<label class="width20" for="">End Date</label>
											<input type="text" id="" name="" class="width40 ">
										</div>
										<div class="float-left width40">
											<label class="width30" for="">Start Date</label>
											<input type="text" id="" name="" class="width50 ">
										</div> 
									</div>
									<div class="width100 float-left">
										<div class="float-right width50">
											<label class="width20" for="">Amount</label>
											<input type="text" id="" name="" class="width60">
										</div>
										<div class="float-left width40">
											<label class="width30" for="">Currency</label>
											<select name="" id="" class="" style="width:51%">
												<option value="">--select--</option>
												<option>AED</option> 
												<option>INR</option> 
												<option>USD</option> 
											</select> 
										</div> 
									</div>
									
									<div class="width100 float-left">
										<div class="float-right" style="width:56%">
											<label class="width30" for="">Frequency</label>
											<select name="" id="" class="" style="width:54%">
												<option value="">--select--</option>
												<option>Monthly</option> 
												<option>Yearly</option>  
											</select> 
										</div>
										<div class="float-left width40">
											<label class="width30" for="leadDay">Lead Day</label>
											<input type="text" id="leadDay" name="leadDay" class="width50">
										</div>	
									</div>
									
							</fieldset>  
					</div>
				
				<div class="width50 float-left" id="hrm">
				<fieldset>
					<legend>Tenant Information</legend>
					<div>
						<label class="width30">Agreement No</label>
						<select name="" id="" class="width40">
							<option value="">--select--</option>
							<option>100000000001</option> 
							<option>100000000002</option>  
						</select>
					</div>
					<div><label class="width30">Name</label>
						<input type="text" name=""  id="" class="width40"/> 
					</div>
					<div class="width100 float-left" style="margin-bottom:14px;">
						<label class="width30" for="">Address</label>
						<input type="text" id="" name="" class="width60">
					</div> 
				</fieldset> 
				<div style="margin: 5px 0px 5px 0px; cursor: pointer;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left">
					<div id="builtSchedule"  style="margin-top:10px;margin-bottom:10px;text-align:center;" class="portlet-header ui-widget-header float-left">
						Build Schedule 
					</div>
				</div>
				</div> 
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top:10px;"> 
					<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>
					<div id="hrm" class="hastable width100"  > 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div> 
						<div  class="primErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
						 <div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>
						<input type="hidden" name="trnValue" id="trnValue"/>
						<input type="hidden" name="childCount" id="childCount"  value="0"/>
						<input type="hidden" name="countPrimary" id="countPrimary"  value="0"/>
						<table class="hastab"> 
							<thead>										
								<tr>
									<th>No</th> 
									<th>Period</th>
									<th>Due Date</th>
									<th>Currency</th>
									<th>Rent</th>
									<th>F.Currency</th>
									<th>Rate</th>
									<th>Rent</th>
									<th>AR.invoice Date</th>
									<th>Other</th>
									<th>Options</th>
								</tr> 
							</thead>  
							<tbody class="tab" style="height:300px;min-height:20px;overflow-x:hidden;">
								<%for(int i=1;i<=50;i++) { %>
							 		<tr>
								 		<td  class="width5"></td>
										<td  class="width10"></td>
										<td  class="width10"></td>
										<td  class="width10"></td> 
										<td  class="width10"></td> 
										<td  class="width10"></td> 
										<td  class="width10"></td> 
										<td  class="width10"></td> 
										<td  class="width10"></td>  
										<td  class="width5"></td>  
										<td  style="display:none">  
											<input type="hidden" value="" name="actualLineId" id="actualLineId"/>  
							 			</td>
									  	<td  style="width:7%">
											  <a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" title="Add Record">
											 	 <span class="ui-icon ui-icon-plus"></span>
											   </a>	
											   <a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" style="display:none;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											   </a> 
											   <a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											   </a>
											    <a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
													<span class="processing"></span>
											   </a>
										</td>   
									</tr>
		 						<%}%>
							</tbody>
						</table>
					</div>	 
				</div>
				<div class="float-right buttons ui-widget-content ui-corner-all" style="">
					<div class="portlet-header ui-widget-header float-right" id="cancelrec">CANCEL</div> 
				 	<div class="portlet-header ui-widget-header float-right" id="finalSave">SAVE</div> 
				</div>
				</form>
			</div> 
	</div>
</div>