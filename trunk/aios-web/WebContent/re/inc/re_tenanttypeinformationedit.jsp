<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
$(function (){  

	$('.formError').remove();

	$("#tenantInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	    success :  false,
	    failure : function() { callFailFunction()}
		});
	
	$('#cancel').click(function(){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
	     	async: false, 
			dataType: "html",
			cache: false,
			error: function(data) 
			{  
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
 	}); 

	$('#close').click(function(){
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/tenant_type_information_list.action",   
	     	async: false, 
			dataType: "html",
			cache: false,
			error: function(data) 
			{  
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
 	});   

	approvedStatus=$('#approvedStatus').val(); 
	if( approvedStatus == 'E'){
		$('#request').remove();	
		$('#cancel').remove();	
		$('#update').remove();
		$('#close').fadeIn();
		$('#payChequeFees').attr("disabled",true);
		$('#contractFees').attr("disabled",true);
		$('#payDeposit').attr("disabled",true);
		$('#rentFees').attr("disabled",true);
		$('#maintenanceFees').attr("disabled",true);
		$('#payPenalities').attr("disabled",true);
		$('#otherFees').attr("disabled",true);
		$('#chequeAmount').attr("disabled",true);
		$('#contractAmount').attr("disabled",true);
		$('#depositAmount').attr("disabled",true);
		$('#rentAmount').attr("disabled",true);
		$('#maintenanceAmount').attr("disabled",true);
		$('#penalitiesAmount').attr("disabled",true);
		$('#otherAmount').attr("disabled",true);
		$('#needReleaseLetter').attr("disabled",true);
		$('#payAfterContract').attr("disabled",true);
		$('#noOfDaysAllowLate').attr("disabled",true);
		$('#descriptions').attr("disabled",true);
		$('#offerValidityDays').attr("disabled",true);
		$('.commonErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#request').remove();	
		$('#cancel').remove();	
		$('#update').remove();
		$('#close').fadeIn();
		$('#payChequeFees').attr("disabled",true);
		$('#contractFees').attr("disabled",true);
		$('#payDeposit').attr("disabled",true);
		$('#rentFees').attr("disabled",true);
		$('#maintenanceFees').attr("disabled",true);
		$('#payPenalities').attr("disabled",true);
		$('#otherFees').attr("disabled",true);
		$('#chequeAmount').attr("disabled",true);
		$('#contractAmount').attr("disabled",true);
		$('#depositAmount').attr("disabled",true);
		$('#rentAmount').attr("disabled",true);
		$('#maintenanceAmount').attr("disabled",true);
		$('#penalitiesAmount').attr("disabled",true);
		$('#otherAmount').attr("disabled",true);
		$('#needReleaseLetter').attr("disabled",true);
		$('#payAfterContract').attr("disabled",true);
		$('#noOfDaysAllowLate').attr("disabled",true);
		$('#descriptions').attr("disabled",true);
		$('#offerValidityDays').attr("disabled",true);
		$('.commonErr').hide().html("Workflow is already Approved, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#request').fadeIn();	
		$('#cancel').fadeIn();	
		$('#update').fadeIn();
		$('.commonErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
	}
		
	$('#update').click(function(){ 
		 if($("#tenantInformationAdd").validationEngine({returnIsValid:true})){	
			 var payChequeFees;
			if($('#payChequeFees').is(':checked')){payChequeFees=1;}else{payChequeFees=0;}	
			var contractFees;
			if($('#contractFees').is(':checked')){contractFees=1;}else{contractFees=0;}
			var payDeposit;
			if($('#payDeposit').is(':checked')){payDeposit=1;}else{payDeposit=0;}
			var rentFees;
			if($('#rentFees').is(':checked')){rentFees=1;}else{rentFees=0;}
			var maintenanceFees;
			if($('#maintenanceFees').is(':checked')){maintenanceFees=1;}else{maintenanceFees=0;}
	 		var payPenalities;
			if($('#payPenalities').is(':checked')){payPenalities=1;}else{payPenalities=0;}
			var otherFees;
			if($('#otherFees').is(':checked')){otherFees=1;}else{otherFees=0;}

			var chequeAmount=Number($('#chequeAmount').val());
			var contractAmount=Number($('#contractAmount').val());
			var depositAmount=Number($('#depositAmount').val());
			var rentAmount=Number($('#rentAmount').val());
			var maintenanceAmount=Number($('#maintenanceAmount').val());
			var penalitiesAmount=Number($('#penalitiesAmount').val());
			var otherAmount=Number($('#otherAmount').val());
			
			var needReleaseLetter;
			if($('#needReleaseLetter').is(':checked')){needReleaseLetter=1;}else{needReleaseLetter=0;}
			
			var payAfterContract;
			if($('#payAfterContract').is(':checked')){payAfterContract=1;}else{payAfterContract=0;}
			
			var noOfDaysAllowLate=$('#noOfDaysAllowLate').val();
			var descriptions=$('#descriptions').val();
			var offerValidityDays=$('#offerValidityDays').val();
			var tenantTypeId=$('#tenantTypeId').val();
			workflowStatus="Not Yet Approved";
			var companyId=Number($('#headerCompanyId').val());
			var applicationId=Number($('#headerApplicationId').val());
			//alert("chequeAmount : "+chequeAmount+ " contractAmount : "+contractAmount+" depositAmount : "+depositAmount
				//	+" rentAmount : "+rentAmount+" maintenanceAmount : "+maintenanceAmount+" penalitiesAmount : "+penalitiesAmount+" otherAmount : "+otherAmount );
			$('#loading').fadeIn();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/tenant_type_informationupdate.action",
				data:{tenantTypeId:tenantTypeId,payChequeFees: payChequeFees,contractFees:contractFees,payDeposit: payDeposit,rentFees: rentFees,maintenanceFees: maintenanceFees,
					payPenalities: payPenalities,otherFees: otherFees, 
					chequeAmount: chequeAmount, contractAmount: contractAmount,depositAmount: depositAmount, rentAmount: rentAmount,maintenanceAmount: maintenanceAmount,
					penalitiesAmount: penalitiesAmount,otherAmount: otherAmount,
					needReleaseLetter: needReleaseLetter, payAfterContract: payAfterContract,noOfDaysAllowLate: noOfDaysAllowLate,descriptions: descriptions,
					offerValidityDays:offerValidityDays,workflowStatus:workflowStatus,companyId:companyId,applicationId:applicationId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				success:function(result){
					$('.tempresultfinal').fadeOut();
		 		 	$('.tempresultfinal').html(result);
					if($("#sqlReturnStatus").val()==1){
						$("#main-wrapper").html(result); 
					} else{
						$('.tempresultfinal').fadeIn();
					}
				},  
			 	error:function(result){
					$('.tempresultfinal').fadeOut();
					$('.tempresultfinal').html(result);
					if($("#sqlReturnStatus").val()==1){
						$("#main-wrapper").html(result); 
					} else{
						$('.tempresultfinal').fadeIn();
					}
			 	}  
			});
			$('#loading').fadeOut();
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});

	$('#request').click(function(){ 
		if($("#tenantInformationAdd").validationEngine({returnIsValid:true})){
			var payChequeFees;
			if($('#payChequeFees').is(':checked')){payChequeFees=1;}else{payChequeFees=0;}	
			var contractFees;
			if($('#contractFees').is(':checked')){contractFees=1;}else{contractFees=0;}
			var payDeposit;
			if($('#payDeposit').is(':checked')){payDeposit=1;}else{payDeposit=0;}
			var rentFees;
			if($('#rentFees').is(':checked')){rentFees=1;}else{rentFees=0;}
			var maintenanceFees;
			if($('#maintenanceFees').is(':checked')){maintenanceFees=1;}else{maintenanceFees=0;}
	 		var payPenalities;
			if($('#payPenalities').is(':checked')){payPenalities=1;}else{payPenalities=0;}
			var otherFees;
			if($('#otherFees').is(':checked')){otherFees=1;}else{otherFees=0;}

			var chequeAmount=Number($('#chequeAmount').val());
			var contractAmount=Number($('#contractAmount').val());
			var depositAmount=Number($('#depositAmount').val());
			var rentAmount=Number($('#rentAmount').val());
			var maintenanceAmount=Number($('#maintenanceAmount').val());
			var penalitiesAmount=Number($('#penalitiesAmount').val());
			var otherAmount=Number($('#otherAmount').val());
			var tenantTypeId=$('#tenantTypeId').val();
			var needReleaseLetter;
			if($('#needReleaseLetter').is(':checked')){needReleaseLetter=1;}else{needReleaseLetter=0;}
			
			var payAfterContract;
			if($('#payAfterContract').is(':checked')){payAfterContract=1;}else{payAfterContract=0;}
			
			var noOfDaysAllowLate=$('#noOfDaysAllowLate').val();
			var descriptions=$('#descriptions').val();
			var offerValidityDays=$('#offerValidityDays').val();
			
			//Work Flow Variables
			var sessionPersonId = $('#sessionPersonId').val();
			var companyId=Number($('#headerCompanyId').val());
			var applicationId=Number($('#headerApplicationId').val());
			var workflowStatus="Request For Tenant Type Information";
			var wfCategoryId=Number($('#headerCategoryId').val());
			var functionId=Number($('#headerCategoryId').val());
			var functionType="request";
			
			$('#loading').fadeIn();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/tenant_type_edit_request.action",
				data:{payChequeFees: payChequeFees,contractFees:contractFees,payDeposit: payDeposit,rentFees: rentFees,maintenanceFees: maintenanceFees,
					payPenalities: payPenalities,otherFees: otherFees, 
					chequeAmount: chequeAmount, contractAmount: contractAmount,depositAmount: depositAmount, rentAmount: rentAmount,maintenanceAmount: maintenanceAmount,
					penalitiesAmount: penalitiesAmount,otherAmount: otherAmount,
					needReleaseLetter: needReleaseLetter, payAfterContract: payAfterContract,noOfDaysAllowLate: noOfDaysAllowLate,descriptions: descriptions,
					offerValidityDays:offerValidityDays,companyId:companyId,applicationId:applicationId,workflowStatus:workflowStatus,wfCategoryId:wfCategoryId,
					functionId:functionId,functionType:functionType,sessionPersonId: sessionPersonId,tenantTypeId:tenantTypeId}, 
		     	async: false,
				dataType: "html",
				cache: false,
				success:function(result){
						$('.formError').hide();
						$('.commonErr').hide();
						$('.tempresultfinal').fadeOut();
			 		 	$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$('.tempresultfinal').fadeIn();
							$('#loading').fadeOut(); 
							$('#request').remove();	
							$('#update').remove();
							$('#cancel').fadeOut();
							$('#close').fadeIn();
							$('.commonErr').hide();
						} else{
							$('.tempresultfinal').fadeIn();
						}
					},  
				error:function(result){
						$('.tempresultfinal').fadeOut();
						$('.tempresultfinal').html(result);
						if($("#sqlReturnStatus").val()==1){
							$("#main-wrapper").html(result); 
						} else{
							$('.tempresultfinal').fadeIn();
						};
				 	}   
			});
			$('#loading').fadeOut();
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});	

	$('#payChequeFees').click(function(){
		$('#chequeAmount').toggle();
	});

	$('#contractFees').click(function(){
		$('#contractAmount').toggle();
	});

	$('#payDeposit').click(function(){
		$('#depositAmount').toggle();
	});

	$('#rentFees').click(function(){
		$('#rentAmount').toggle();
	});

	$('#maintenanceFees').click(function(){
		$('#maintenanceAmount').toggle();
	});

	$('#payPenalities').click(function(){
		$('#penalitiesAmount').toggle();
	});

	$('#otherFees').click(function(){
		$('#otherAmount').toggle();
	});
	
});
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.tenanttypeinformation"/></div>
					 <div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
					 <div class="portlet-content">
						<div style="display:none;" class="tempresultfinal">
							<c:if test="${requestScope.bean != null}">
								<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
								 <c:choose>
									 <c:when test="${bean.sqlReturnStatus == 1}">
										<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
									</c:when>
									<c:when test="${bean.sqlReturnStatus != 1}">
										<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
									</c:when>
								</c:choose>
							</c:if>    
						</div>
					  <form name="tenantInformationAdd" id="tenantInformationAdd"> 
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
			 					<div style="display:none;">
									<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40" id="sessionPersonId" disabled="disabled">
									<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
								</div>
								<legend><fmt:message key="re.property.info.tenanttypeinformation2"/></legend>
								<div>
									<c:choose>
										<c:when test="${bean.payChequeFees eq 1 }">
											<input type="checkbox" checked="checked" name="payChequeFees" id="payChequeFees"/>
											<input type="text" name="chequeAmount" id="chequeAmount" value="${bean.chequeAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payChequeFees" id="payChequeFees" >
											<input type="text" name="chequeAmount" id="chequeAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paychequefees"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.contractFees eq 1 }">
											<input type="checkbox" checked="checked" name="contractFees" id="contractFees"/>
											<input type="text" name="contractAmount" id="contractAmount" value="${bean.contractAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="contractFees" id="contractFees" >
											<input type="text" name="contractAmount" id="contractAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.contractfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.payDeposit eq 1 }">
											<input type="checkbox" checked="checked" name="payDeposit" id="payDeposit"/>
											<input type="text" name="depositAmount" id="depositAmount" value="${bean.depositAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payDeposit" id="payDeposit" >
											<input type="text" name="depositAmount" id="depositAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paydeposit"/></label>
								</div>	
								<div>
									<c:choose>
										<c:when test="${bean.rentFees eq 1 }">
											<input type="checkbox" checked="checked" name="rentFees" id="rentFees"/>
											<input type="text" name="rentAmount" id="rentAmount" value="${bean.rentAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="rentFees" id="rentFees" >
											<input type="text" name="rentAmount" id="rentAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.rentfees"/></label>
								</div>						
	                            <div>
									<c:choose>
										<c:when test="${bean.maintenanceFees eq 1 }">
											<input type="checkbox" checked="checked" name="maintenanceFees" id="maintenanceFees"/>
											<input type="text" name="maintenanceAmount" id="maintenanceAmount" value="${bean.maintenanceAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="maintenanceFees" id="maintenanceFees" >
											<input type="text" name="maintenanceAmount" id="maintenanceAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.maintenancefees"/></label>
								</div>		
								<div>
									<c:choose>
										<c:when test="${bean.payPenalities eq 1 }">
											<input type="checkbox" checked="checked" name="payPenalities" id="payPenalities"/>
											<input type="text" name="penalitiesAmount" id="penalitiesAmount" value="${bean.penalitiesAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="payPenalities" id="payPenalities" >
											<input type="text" name="penalitiesAmount" id="penalitiesAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.paypenalities"/></label>
								</div>
								<div>
									<c:choose>
										<c:when test="${bean.otherFees eq 1 }">
											<input type="checkbox" checked="checked" name="otherFees" id="otherFees"/>
											<input type="text" name="otherAmount" id="otherAmount" value="${bean.otherAmount }" class="width30" style="">
										</c:when>
										<c:otherwise>
											<input  type="checkbox" name="otherFees" id="otherFees" >
											<input type="text" name="otherAmount" id="otherAmount"  class="width30" style="display:none;">
										</c:otherwise>
									</c:choose>
									<label><fmt:message key="re.property.info.otherfees"/></label>
								</div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend><fmt:message key="re.property.info.tenanttypeinformation1"/></legend>	
			   						<div style="display:none;"><label>Tenant Type Id</label>
			   							<input type="text" name="tenantTypeId" class="width30" id="tenantTypeId" readonly="readonly" value="${bean.tenantTypeId}"/>
			   						</div>             		
			   						<div><label><fmt:message key="re.property.info.tenanttypedescription"/><span style="color:red">*</span></label>
			   							<input type="text" name="descriptions" class="width50 validate[required]" id="descriptions" value="${bean.descriptions}"/>
			   						</div> 
			   						<div>
										<c:choose>
											<c:when test="${bean.payAfterContract eq 1 }">
												<input type="checkbox" checked="checked" name="payAfterContract" id="payAfterContract"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="payAfterContract" id="payAfterContract" >
												<input type="text" name="chequeAmount" id="chequeAmount" class="width30" style="display:none;">
											</c:otherwise>
										</c:choose>
										<label><fmt:message key="re.property.info.payaftercontract"/></label>
									</div>	            		
									<div>
										<c:choose>
											<c:when test="${bean.needReleaseLetter eq 1 }">
												<input type="checkbox" checked="checked" name="needReleaseLetter" id="needReleaseLetter"/>
											</c:when>
											<c:otherwise>
												<input  type="checkbox" name="needReleaseLetter" id="needReleaseLetter" >
											</c:otherwise>
										</c:choose>
										<label><fmt:message key="re.property.info.needreleaseletter"/></label>
									</div>
									<div>
										<label><fmt:message key="re.property.info.offervaliditydays"/><span style="color:red">*</span></label>
										<input type="text" name="offerValidityDays" class="width30 validate[required,custom[onlyNumber]]" id="offerValidityDays" value="${bean.offerValidityDays}"/>
									</div>	
									<div><label><fmt:message key="re.property.info.noofdaysallowlate"/><span style="color:red">*</span></label>
		                            	<input type="text" name="noOfDaysAllowLate" class="width30 validate[required,custom[onlyNumber]]" id="noOfDaysAllowLate" value="${bean.noOfDaysAllowLate}"/>
		                            </div>								
							</fieldset> 
						</div>
						<div class="clearfix"></div>
						<div class="float-right buttons ui-widget-content ui-corner-all" style="">
							<div class="portlet-header ui-widget-header float-right" id="cancel"><fmt:message key="re.property.info.discard"/></div>
							<div class="portlet-header ui-widget-header float-right" id="request">Request For Verification</div>
						 	<div class="portlet-header ui-widget-header float-right" id="update">Update</div> 
						 	<div style="display:none;" class="portlet-header ui-widget-header float-right" id="close">Close</div>
						</div>
				   </form>
		       </div>
		</div>
  </div>