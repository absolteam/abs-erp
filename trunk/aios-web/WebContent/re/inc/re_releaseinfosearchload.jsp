<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div><label><fmt:message key="re.property.info.contractno"/></label>
	<select id="contractId" class="width30" >
		<option value="">-Select-</option>
		<c:if test="${requestScope.item ne null}">
			<c:forEach items="${requestScope.item}" var="contract" varStatus="status">
				<option value="${contract.contractId }">${contract.contractNumber }</option>
			</c:forEach>
		</c:if>
	</select>
</div>
<div><label><fmt:message key="re.property.info.buildingno"/></label>
	<select id="buildingId" class="width30" >
		<option value="">-Select-</option>
		<c:if test="${requestScope.itemList ne null}">
			<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
				<option value="${building.buildingId }">${building.buildingNumber }</option>
			</c:forEach>
		</c:if>
	</select>
</div>

<div><label><fmt:message key="re.property.info.buildingname"/></label>
	<select id="buildingsId" class="width30">
		<option value="">-Select-</option>
		<c:if test="${requestScope.buildingList ne null}">
			<c:forEach items="${requestScope.buildingList}" var="buildingname" varStatus="status">
				<option value="${buildingname.buildingId }">${buildingname.buildingName}</option>
			</c:forEach>
		</c:if>
	</select>
</div>

<div><label><fmt:message key="re.property.info.tenantname"/></label>
	<select id="tenantId" class="width30 validate[required]">
		<option value="">-Select-</option>
		<c:if test="${requestScope.tenantList ne null}">
			<c:forEach items="${requestScope.tenantList}" var="tenType" varStatus="status">
				<option value="${tenType.tenantId }">${tenType.tenantName }</option>
			</c:forEach>
		</c:if>
	</select>
</div>
	









<!-- <div style="display:none;" class="tempbuidingId">
</div>-->

<!--  <script type="text/javascript">
$(document).ready(function (){  
$('#contractId').change(function(){
			var contractId=$('#contractId').val();
			$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/building_retrive_search_action.action",
	    		data:{contractId:contractId},
	    		async:false,
	    		dataType:"html",
	    		cache:false,
	    		success:function(result){
	    			$(".tempbuidingId").html(result);
	    		}
	    	});
		});
});
</script>-->