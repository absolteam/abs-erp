<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript" src="/js/dateDifference.js"></script>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var actualID;
var openFlag=0;
$(function(){

	$('#contractId').val($('#contractIdTemp').val());
	$('#buildingId').val($('#buildingIdTemps').val());
	$('#componentId').val($('#componentIdTemp').val());
	$('#flatId').val($('#flatIdTemp').val());
	$('#caseStatus').val($('#caseStatusTemp').val());

	var total=0;
	$('.totalExpensesCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#totalExpenses').val(total);

	approvedStatus=$('#approvedStatus').val();
	if( approvedStatus == 'E'){
		$('#request').remove();	
		$('#discard').remove();	
		$('#save').remove();
		$('#close').fadeIn();
		$('.childCountErr').hide().html("Workflow in is process, Cannot edit this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#request').fadeIn();	
		$('#discard').fadeIn();	
		$('#save').fadeIn();
		$('.childCountErr').hide().html("Workflow is Approved, You can Add Session Details and send for Approval.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#request').fadeIn();	
		$('#discard').fadeIn();	
		$('#save').fadeIn();
		$('.childCountErr').hide().html("Workflow is already Rejected,Edit this screen and Send For Request.!!!").slideDown();
	}
	
	$('.formError').remove(); 
	 $("#propertyComponentAdd").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	//Update Legal Information details
	$("#save").click(function(){
		$('.tempresultfinal').fadeOut();
		if($('#propertyComponentAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				
				caseIdCourt=$('#caseIdCourt').val();
				contractId = Number($('#contractId').val());
				buildingId = $('#buildingId').val();
				description=$('#text_area_present_input').val();
				//Checked Enable Flag
				var buildingCompFlag;
				if($('#buildingCompFlag').is(':checked')){buildingCompFlag=1;}else{buildingCompFlag=0;}
				componentId=$('#componentId').val();
				var flatId=$('#flatId').val();
				
				fromDate=$('#startPicker').val();
				toDate=$('#endPicker').val();
				caseStatus=$('#caseStatus').val();
				oppositPersonId=$('#oppositPersonId').val();

				var headerFlag=$("#headerFlag").val();
				caseId=$('#caseId').val();  //header value

				//Work Flow Variables
				companyId=Number($('#headerCompanyId').val());
				applicationId=Number($('#headerApplicationId').val());
				workflowStatus="Not Yet Approved";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/edit_final_save_legal_info.action",
						data: {caseIdCourt: caseIdCourt, contractId: contractId, buildingId: buildingId, description: description,buildingCompFlag: buildingCompFlag,componentId: componentId,
							caseStatus: caseStatus, fromDate: fromDate, toDate: toDate, oppositePersonName: oppositPersonId,flatId: flatId,
							trnValue: transURN,caseId:caseId,headerFlag:headerFlag,
							companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus}, 
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							// Scroll to top of the page
							$.scrollTo(0,300);
						} 
					});
					$('#loading').fadeOut(); 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					// Scroll to top of the page
					$.scrollTo(0,300);
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Session Lines").slideDown();
				// Scroll to top of the page
				$.scrollTo(0,300);
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Update & Request - Legal Information details
	$("#request").click(function(){
		$('.tempresultfinal').fadeOut();
		if($('#propertyComponentAdd').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				
				caseIdCourt=$('#caseIdCourt').val();
				contractId = Number($('#contractId').val());
				buildingId = $('#buildingId').val();
				description=$('#text_area_present_input').val();
				//Checked Enable Flag
				var buildingCompFlag;
				if($('#buildingCompFlag').is(':checked')){buildingCompFlag=1;}else{buildingCompFlag=0;}
				componentId=$('#componentId').val();
				var flatId=$('#flatId').val();
				
				fromDate=$('#startPicker').val();
				toDate=$('#endPicker').val();
				caseStatus=$('#caseStatus').val();
				oppositPersonId=$('#oppositPersonId').val();

				var headerFlag=$("#headerFlag").val();
				caseId=$('#caseId').val();  //header value

				//Work Flow Variables
				var sessionPersonId = $('#sessionPersonId').val();
				var headerLedgerId = $('#headerLedgerId').val();
				var companyId=Number($('#headerCompanyId').val());
				var applicationId=Number($('#headerApplicationId').val());
				var workflowStatus="Request For Verification";
				var wfCategoryId=Number($('#headerCategoryId').val());
				var functionId=Number($('#headerCategoryId').val());
				var functionType="request";
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/legal_info_update_request.action",
						data: {caseIdCourt: caseIdCourt, contractId: contractId, buildingId: buildingId, description: description,buildingCompFlag: buildingCompFlag,componentId: componentId,
							caseStatus: caseStatus, fromDate: fromDate, toDate: toDate, oppositePersonName: oppositPersonId,flatId: flatId,
							trnValue: transURN,caseId:caseId,headerFlag:headerFlag,
							companyId: companyId,applicationId: applicationId,workflowStatus: workflowStatus,wfCategoryId: wfCategoryId,
							functionId: functionId,functionType: functionType,sessionPersonId: sessionPersonId,headerLedgerId: headerLedgerId}, 
				     	async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
								$('.formError').hide();
								$('.commonErr').hide();
								$('.childCountErr').hide();
					 		 	$('.tempresultfinal').html(result);
								if($("#sqlReturnStatus").val()==1){
									$('.tempresultfinal').fadeIn();
									$('#loading').fadeOut(); 
									$('#request').remove();	
									$('#discard').remove();	
									$('#save').remove();
									$('#close').fadeIn();
									$('.commonErr').hide();
								} else{
									$('.tempresultfinal').fadeIn();
								}
								// Scroll to top of the page
								$.scrollTo(0,300);
						} 
					});
					$('#loading').fadeOut(); 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					// Scroll to top of the page
					$.scrollTo(0,300);
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Session Lines").slideDown();
				// Scroll to top of the page
				$.scrollTo(0,300);
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Adding Feature Details
	$('.addSession').click(function(){ 
		if(openFlag==0){
			if($('#propertyComponentAdd').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_legal_info_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(12)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(12)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(12)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
		}	
	});
	
	$(".editSession").click(function(){
		if(openFlag==0){
			if($('#propertyInfoAdd').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//actualID=$($($(slidetab).children().get(9)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_legal_info_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(12)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(12)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(12)).children().get(3)).show();	//Processing Button
						
					 
			         var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp11= $($(slidetab).children().get(14)).text();
					 var temp3= $($(slidetab).children().get(3)).text();
					 var temp4= $($(slidetab).children().get(4)).text();
					 var temp5= $($(slidetab).children().get(5)).text();
					 var temp6= $($(slidetab).children().get(6)).text();
					 var temp7= $($(slidetab).children().get(7)).text();
					 var temp8= $($(slidetab).children().get(8)).text();
					 var temp12= $($(slidetab).children().get(9)).text();
					 var temp10= $($(slidetab).children().get(10)).text();
					 
					 var temp9= $($(slidetab).children().get(13)).text();
					 var temp15= $($(slidetab).children().get(15)).text();	// DMS URN
					 
		
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp11);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(4)).children().get(1)).val(temp3);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(5)).children().get(1)).val(temp4);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(6)).children().get(1)).val(temp5);
					
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp6);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp7);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp9);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(4)).children().get(1)).val(temp12);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(5)).children().get(1)).val(temp10);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(6)).children().get(1)).val(temp15);

				  	sessionStatus=temp9;
					if(sessionStatus == '3' || sessionStatus == '4'){
						$('#sessionToDate').val("");
						$('#sessionToDate').attr('disabled','disabled');
					}else{
						$('#sessionToDate').removeAttr('disabled');
					}
					
				  	openFlag=1;
				  	$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}	
	});
  
	$('.delSession').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        caseId=$('#caseId').val();
	        var actionflag="D";
	 		var trnValue=$("#transURN").val(); 
	 		actualID=$($($(slidetab).children().get(11)).children().get(0)).val();
	 		var tempLineId=$($($(slidetab).children().get(11)).children().get(2)).val();
			var flag = false; 
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_session_feature_update.action", 
				 	async: false,
				 	data: {caseId: caseId, actualLineId: actualID, actionFlag: actionflag,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
		       			childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
		       		$.fn.globeTotalExpenses();	//Total Expenses calculation
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}	
 	});

/*	$('#contractId').change(function(){
		var contractId=$('#contractId').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/contract_retrive_edit_action.action",
			data:{contractId:contractId},
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
			$(".tempbuidingId").html(result);
			}
		});
		var buildingId=$('#buildingId option:selected').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/component_retrive_edit_action.action",
			data:{buildingId:buildingId},
			async:false,
			dataType:"html",
			cache:false,
			success:function(result){
			$("#componentId").html(result);
			
			}
		});
	});     */

	$('#buildingId').change(function(){
		var buildingId=Number($('#buildingId option:selected').val());
		if(buildingId !=0){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/component_retrive_action.action",
				data:{buildingId:buildingId},
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					$("#componentId").html(result);
				}
			});
			$('#loading').fadeOut();
		}
	});

	$('#componentId').change(function(){
		var buildingId=Number($('#buildingId option:selected').val());
		var componentId=Number($('#componentId option:selected').val());
		if(buildingId !=0 && componentId !=0){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/legal_flatnumber_load.action",
				data:{buildingId: buildingId,componentId: componentId},
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					$("#flatId").html(result);
				}
			});
			$('#loading').fadeOut();
		}
	});

	$('#flatId').change(function(){
		var flatId=Number($('#flatId option:selected').val());
		if(flatId !=0){
			$('#loading').fadeIn();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/legal_contractnumber_load.action",
				data:{flatId: flatId},
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
					$("#contractId").html(result);
				}
			});
			$('#loading').fadeOut();
		}
	});

	$("#text_area_present_input").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#text_area_present_input").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    }); 

    $("#text_area_permanent_input").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar1").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar1').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar1").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#text_area_permanent_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#text_area_permanent_input").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    });  

	$("#discard").click(function(){
		$('#loading').fadeIn();
		var trnValue=$("#transURN").val();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_discard_legal_info.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
   		});
	});
	
	$("#close").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/legal_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) {
				$('#loading').fadeOut();
			},
	     	success: function(data){
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
				$('#loading').fadeOut();
	     	}
		});
		return true;
	});
	$('.addrowsrent').click(function(){
		$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/legal_info_editrow.action", 
	 	async: false,
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tab").append(result);
			if($(".tab").height()<255)
				 $(".tab").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tab").height()>255)
				 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
		}
	});
});

	$.fn.globeTotalExpenses = function() { 	// Total for Expenses
		var total=0;
		$('.totalExpensesCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#totalExpenses').val(total)

	 }

	$("#text_area_present_input").focus(function(){
	    $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it
	    return false;
	}); 

	$("#text_area_present_input").keyup(function(){//Detect keypress in the textarea
	    var text_area_box =$(this).val();//Get the values in the textarea
	    var max_numb_of_words = 500;//Set the Maximum Number of words
	    var main = text_area_box.length*100;//Multiply the lenght on words x 100

	    var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	    var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	    if(text_area_box.length <= max_numb_of_words){
	        $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	        $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	        $('#progressbar').animate({//Increase the width of the css property "width"
	        "width": value+'%',
	        }, 1);//Increase the progress bar
	    }
	    else{
	         $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	         $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	    }
	    return false;
	});

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	}); 
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.legalinformation"/></div>
	 	<div class="portlet-content">
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>
			<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	 
		 	<form id="propertyComponentAdd">
			 	<div class="width50 float-left" id="hrm">
				 	<fieldset>
				 		<div style="display:none;">
							<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40" id="sessionPersonId" disabled="disabled">
							<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.caseidno"/></label>	 
							<input class="width40" type="text" name="caseId"  id="caseId" disabled="disabled" value="${bean.caseId}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.caseidincourt"/></label>	 
							<input class="width40" type="text" name="caseIdCourt"  id="caseIdCourt" value="${bean.caseIdCourt}"/>
						</div>
						
						<div>
							<label class="width30"><fmt:message key="re.property.info.buildingid"/></label>
							<input type="hidden" name="buildingIdTemp" id="buildingIdTemps" value="${bean.buildingId}">	 
							<select id="buildingId" class="width30" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemList ne null}">
									<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
										<option value="${building.buildingId }">${building.buildingName }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<c:choose>
								<c:when test="${bean.buildingCompFlag eq 1 }">
									<input type="checkbox" checked="checked" name="buildingCompFlag" id="buildingCompFlag"/>
								</c:when>
								<c:otherwise>
									<input class="width40 recurStatus"  type="checkbox"  name="buildingCompFlag"  id="buildingCompFlag">
								</c:otherwise>
							</c:choose>
							<label><fmt:message key="re.property.info.buildingcomponent"/></</label>
					   </div>	
						<div class="recurStat">
							<label class="width30 recurStat"><fmt:message key="re.property.info.buildingcomponent"/></label>	
							<input type="hidden" name="componentIdTemp" id="componentIdTemp" value="${bean.componentId}">	  
							<select id="componentId" class="width30" >
								<option value="">-Select-</option>
									<c:forEach items="${requestScope.componentList}" var="component">
										<option value="${component.componentId}">${component.componentName} </option>
									</c:forEach>
							</select>
						</div>
						<div class="recurStat">
							<label class="width30 "><fmt:message key="re.property.info.flatno"/></label>	 
							<input type="hidden" name="flatIdTemp" id="flatIdTemp" value="${bean.flatId}">	
							<select id="flatId" class="width30" >
								<option value="">-Select-</option>
								<c:forEach items="${requestScope.flatList}" var="flat">
									<option value="${flat.flatId}">${flat.flatNumber} </option>
								</c:forEach>
							</select>
						</div>
						<div  class="recurStat">
							<label class="width30"><fmt:message key="re.property.info.contractno"/></label>
							<input type="hidden" name="contractIdTemp" id="contractIdTemp" value="${bean.contractId}">	 
							<select id="contractId" class="width30" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemListCon ne null}">
									<c:forEach items="${requestScope.itemListCon}" var="contract" varStatus="status">
										<option value="${contract.contractId }">${contract.contractNumber }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<fieldset>
		                    <legend><fmt:message key="re.property.info.casedescription"/><span style="color:red">*</span></legend>
		                    <div style="height:25px" class="width60 float-left">
		                            <div id="barbox">
		                                  <div id="progressbar"></div>
		                            </div>
		                            <div id="count">500</div>
		                      </div>
		                      <p>
		                        <textarea class="width60 float-left tooltip validate[required]" id="text_area_present_input">${bean.description}</textarea>
		                      </p>
	                   </fieldset>
						
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset style="min-height:175px;">
						<div>
							<label class="width30 "><fmt:message key="re.property.info.fromdate"/></label>	 
							<input class="width40 " type="text" name="assignby" readonly="readonly" id="startPicker" value="${bean.fromDate}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.todate"/></label>	 
							<input class="width40" type="text" name="assignby" readonly="readonly"  id="endPicker" value="${bean.toDate}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.status"/></label>
							<input type="hidden" name="caseStatusTemp" id="caseStatusTemp" value="${bean.caseStatus}">		 
							<select id="caseStatus">
								<c:forEach items="${requestScope.legalStatusList}" var="legalstatus" varStatus="status">
									<option value="${legalstatus.sessionStatusCode }">${legalstatus.sessionStatus}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.oppositeperson"/></label>	
							<input class="width40" type="text" name="assignby"  id="oppositPersonId" value="${bean.oppositePersonName}">
						</div>
		
					</fieldset>
		 		</div>
		 	</form>
		 	<div class="clearfix"></div> 
		 	<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.sessiondetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
							<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
			 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm" class="hastable width100">  
								<table id="hastab" class="width100">	
									<thead class="chrome_tab">
										<tr>
											<th style="width:5%"><fmt:message key="re.property.info.sessionno"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.sessiondate"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.courtname"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.lawyername"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.submitteddocument"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.receiveddocument"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.expenses"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.sessionresult"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.status"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.todate"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.description"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="even"> 
													<td style="width:5%">${result.sessionNumber}</td>
												    <td  style="width:10%">${result.sessionDate}</td>
												    <td  style="width:5%">${result.courtName}</td>
													<td  style="width:10%">${result.lawyerName}</td>
													<td  style="width:10%">${result.submittedDoc}</td>
													<td  style="width:10%">${result.receivedDoc}</td>
													<td  style="width:5%" class="totalExpensesCalc">${result.expenses}</td>
													<td  class="status" style="width:10%">${result.sessionResult}</td>
													<td  style="width:10%">${result.sessionName}</td>
													<c:choose>
													<c:when test="${result.sessionToDate ne '31-Dec-9999' }">
														<td style="width:5%">${result.sessionToDate}</td>
													</c:when>
													<c:otherwise>
														<td style="width:5%"></td>
													</c:otherwise>
												</c:choose>
													<td  style="width:10%">${result.statusDescription }</td>
													<td style="display:none"> 
														<input type="text" value="${result.caseChildId}" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:10%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addSession" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editSession" style="cursor:pointer;"  title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delSession" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="session_status">${result.sessionStatus}</td>
													<td style="display:none;" class="court_name">${result.courtNameCode}</td>
													<td style="display:none;" class="dms_urn">${result.dmsURN}</td>		
												</tr>
											</c:forEach>
											<%for(int i=0;i<=2;i++) { %>
									  		<tr>
										  		<td style="width:5%"></td>
											    <td  style="width:10%"></td>
												<td  style="width:5%"></td>
												<td  style="width:10%"></td>
												<td  style="width:10%"></td>
												<td  style="width:10%"></td>
												<td  style="width:5%" class="totalExpensesCalc"></td>
												<td  style="width:10%"></td>
												<td  style="width:10%"></td>
												<td  style="width:5%"></td>
												<td  style="width:10%"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
													<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
													<input type="hidden" value="" name="tempLineId" id="tempLineId"/>
												</td>
												<td style="width:10%">
												   <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addSession" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   </a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editSession"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delSession" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td> 
												<td  style="display:none;"  class="session_status"></td>
												<td  style="display:none;" class="court_name"></td>
												<td style="display:none;" class="dms_urn"></td>	
									  		</tr>	
								 		<%}%>  
							 		</tbody>
								</table>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:55.5%">
								<label>Total</label>
							 	<input name="totalExpenses" id="totalExpenses" class="" disabled="disabled" style="width:12%;">
							</div>
							<div class="clearfix"></div>
						</div>	 
					</form>	
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>		
			<div style="display:none;" class="tempbuidingId" >
			</div>
			<input type="hidden" id="name"/>							
			<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
				<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
				<div class="portlet-header ui-widget-header float-right cancelrec" id="discard" style=" cursor:pointer;"><fmt:message key="re.property.info.discard"/></div>
				<div class="portlet-header ui-widget-header float-right" id="request" style="cursor:pointer;"><fmt:message key="cs.common.button.requestforverification"/></div> 
				<div class="portlet-header ui-widget-header float-right headerData" id="save" style="cursor:pointer;"><fmt:message key="re.owner.info.update"/></div>					
			</div>
		</div>
	</div>
</div>					
								
								
								
								
								
								