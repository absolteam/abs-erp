<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<c:forEach items="${requestScope.itemList}" var="result" >
 	<tr class="even"> 
		<td class="width10 ">${result.lineNumber}</td>
		<td class="width20 ">${result.subClassName}</td>	
		<td class="width20 ">${result.conditionStatusName}</td> 
		<td class="width20 " >${result.releaseTypeName}</td>
		<td class="width20 " style="">${result.remarks }</td>
		<td class="width20 assetAmountCalc" style="">${result.amount }</td>
		<td style="display:none"> 
			<input type="text" value="" name="actualLineId" id="actualLineId"/>
		</td> 
		<td style=" width:10%;display:none;"> 
					<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAsset" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
		   	</a>	
		   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAsset" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAsset" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
				<span class="processing"></span>
			</a>
		</td>  
		<td style="display:none;" class="session_status"></td>
		<td style="display:none;"></td>	
	</tr>
</c:forEach>
<input type="hidden" id="count1" value="${count}"/>