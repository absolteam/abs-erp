<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementPaymentAddEntry" id="contractAgreementPaymentAddEntry" style="position: relative;">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.contract.chequedetails"/></legend>
		<div>
			<label><fmt:message key="re.contract.chequeno"/><span style="color:red">*</span></label>
			<input type="text" name="chequeNumber" id="chequeNumber" class="width50  validate[required]">
		</div>
		<div>
			<label><fmt:message key="re.contract.chequeamount"/><span style="color:red">*</span></label>
			<input type="text" name="chequeAmount" id="chequeAmount" class="width50  validate[required,custom[onlyFloat]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.contract.linesdetails"/></legend> 
			<div>
				<label><fmt:message key="re.contract.bankname"/><span style="color:red">*</span></label>
				<input type="text" name="bankName" id="bankName" class="width50 validate[required]" />
			</div>
			<div>
				<label><fmt:message key="re.contract.chequedate"/><span style="color:red">*</span></label>
				<input type="text" name="chequeDate" id="defaultActualPicker" class="width50 validate[required]" readonly="readonly" />
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		
		 
		 $jquery("#contractAgreementPaymentAddEntry").validationEngine('attach');

		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button	 
			openFlag=0;		
		 });
		 $('#add').click(function(){
			  if($jquery('#contractAgreementPaymentAddEntry').validationEngine('validate')){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var bankName=$('#bankName').val();
					var chequeDate=$('#defaultActualPicker').val();
					var chequeNumber=$('#chequeNumber').val();
					var chequeAmount=$('#chequeAmount').val();
									 
					var trnVal =$('#transURN').val(); 

					var contractId=$('#contractId').val();  
					var actionFlag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_contract_payment_save.action", 
					 	async: false,
					 	data: {contractId: contractId, bankName: bankName,chequeDate: chequeDate,chequeNumber: chequeNumber,chequeAmount: chequeAmount, 
							trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(bankName);
		        		$($(slidetab).children().get(1)).text(chequeDate);
		        		$($(slidetab).children().get(2)).text(chequeNumber); 
		        		$($(slidetab).children().get(3)).text(chequeAmount); 

		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCountPayment=Number($('#childCountPayment').val());
				  		childCountPayment=childCountPayment+1;
						$('#childCountPayment').val(childCountPayment);

						$.fn.globeTotalCheque();  //For Total Amount of Payment
						$.fn.globeTotalFee();	// For Grand Total
				   } 
			  }else{
				  return false;
			  }
		});

		// Default Date
			$('#defaultActualPicker').datepick();
});
 </script>	