<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){
	$('#relativeClassId').val($('#temprelativeClassId').val());

	 $("#defineFeeAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});


	

	 	$("#cancel").click(function(){ 
	 		$.ajax({
	 			type: "POST",  
	 			url: "<%=request.getContextPath()%>/maintenance_classes_list.action",   
	 	     	async: false,
	 			dataType: "html",
	 			cache: false,
	 			error: function(data) 
	 			{
	 			},
	 	     	success: function(data)
	 	     	{
	 				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	 	     	}
	 		});
	 		return true;
	 	});
});

</script>
  



<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.maintenanceclasses"/></div>
				<div class="portlet-content">
					<div>
						<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
							<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
						</c:if>
						<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
							<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
						</c:if>
			 	   </div>
			 	   <form name="defineFeeAdd" id="defineFeeAdd"> 
						<div class="width50 float-left" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.classinformation"/></legend>
								<div style="display:none;"><label>classId.</label><input type="text" name="classId" class="width30" id="classId" disabled="disabled" value="${bean.classId}"/></div>						
		                             <div >
									<label class="" style="font-weight:bold;"><fmt:message key="re.property.info.classname"/></label> 
									<input type="text" id="classname"  disabled="disabled" class="width30 validate[required]" value="${bean.className}"/>
								</div> 
								
								<div><label><fmt:message key="re.property.info.relatedclass"/></label>
								<input type="hidden" name="calculatedAs" class="width30" id="temprelativeClassId" value="${bean.relativeClassId}"/>
									<select  id="relativeClassId" class="width30" disabled="disabled">
										<option value="">-Select-</option>
										<c:forEach items="${relativeClass}" var="bean">
											<option value="${bean.classId}">${bean.className}</option>
										</c:forEach>
									</select>
								</div>
								</fieldset>
					   		</div> 
					   	</form>
				<div class="clearfix"></div>
				<div class="float-right buttons ui-widget-content ui-corner-all"> 			
					<div class="portlet-header ui-widget-header float-right mou-cursor" id="cancel"><fmt:message key="re.property.info.close"/></div> 
				</div>
			</div>
		</div>
	</div>
			