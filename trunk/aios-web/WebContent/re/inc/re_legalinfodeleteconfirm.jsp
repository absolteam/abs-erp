<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<title>Tenant Details</title>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
.extrapad{
padding:10px!important;
}
</style>

<script type="text/javascript">
var actualID;
$(function (){  
	$('#contractId').val($('#contractIdTemp').val());
	$('#buildingId').val($('#buildingIdTemps').val());
	$('#componentId').val($('#componentIdTemp').val());
	$('#caseStatus').val($('#caseStatusTemp').val());
	
	$('.formError').remove(); 
	 $("#releaseInfoAdd").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	
	 	$("#confirm").click(function(){
	 		caseId=$('#caseId').val();
				//alert("itemId : "+itemId);
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/delete_legal_info.action",
					data: {caseId: caseId},  
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result)
					{ 
						$("#main-wrapper").html(result); 
					} 
				}); 
				return true;
			});

	 	$('#contractId').change(function(){
			var contractId=$('#contractId').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/contract_retrive_edit_action.action",
				data:{contractId:contractId},
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
				$(".tempbuidingId").html(result);
				}
			});
			var buildingId=$('#buildingId option:selected').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/component_retrive_edit_action.action",
				data:{buildingId:buildingId},
				async:false,
				dataType:"html",
				cache:false,
				success:function(result){
				$("#componentId").html(result);
				
				}
			});
		});
		 
		
		$("#cancel").click(function(){ 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/legal_info_list.action",   
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
		     	}
			});
			return true;
		}); 
	}); 
</script>
<div id="main-content">
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.legalinformation"/></div>
	 	<div class="portlet-content">
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
		 	<form id="propertyComponentAdd">
			 	<div class="width50 float-left" id="hrm">
				 	<fieldset>
						<div>
							<label class="width30"><fmt:message key="re.property.info.caseidno"/></label>	 
							<input class="width40" type="text" name="caseId"  id="caseId" disabled="disabled" value="${bean.caseId}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.caseidincourt"/></label>	 
							<input disabled="disabled" class="width40" type="text" name="caseIdCourt"  id="caseIdCourt" value="${bean.caseIdCourt}"/>
						</div>
						
						<div>
							<label class="width30"><fmt:message key="re.property.info.buildingid"/></label>
							<input type="hidden" name="buildingIdTemp" id="buildingIdTemps" value="${bean.buildingId}">	 
							<select id="buildingId" disabled="disabled" class="width30" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemList ne null}">
									<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
										<option value="${building.buildingId }">${building.buildingName }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<c:choose>
								<c:when test="${bean.buildingCompFlag eq 1 }">
									<input type="checkbox" checked="checked" disabled="disabled" name="buildingCompFlag" id="buildingCompFlag"/>
								</c:when>
								<c:otherwise>
									<input class="width40 recurStatus" disabled="disabled"  type="checkbox"  name="buildingCompFlag"  id="buildingCompFlag">
								</c:otherwise>
							</c:choose>
							<label><fmt:message key="re.property.info.buildingcomponent"/></</label>
					   </div>	
						<div class="recurStat">
							<label class="width30 recurStat"><fmt:message key="re.property.info.buildingcomponent"/></label>	
							<input type="hidden" name="componentIdTemp" id="componentIdTemp" value="${bean.componentId}">	  
							<select id="componentId" disabled="disabled" class="width30" >
								<option value="">-Select-</option>
									<c:forEach items="${requestScope.componentList}" var="component">
										<option value="${component.componentId}">${component.componentName} </option>
									</c:forEach>
							</select>
						</div>
						<div class="recurStat">
							<label class="width30 "><fmt:message key="re.property.info.flatno"/></label>	 
							<input type="hidden" name="flatIdTemp" id="flatIdTemp" value="${bean.flatId}">	
							<select id="flatId" disabled="disabled" class="width30" >
								<option value="">-Select-</option>
								<c:forEach items="${requestScope.flatList}" var="flat">
									<option value="${flat.flatId}">${flat.flatNumber} </option>
								</c:forEach>
							</select>
						</div>
						<div  class="recurStat">
							<label class="width30"><fmt:message key="re.property.info.contractno"/></label>
							<input type="hidden" name="contractIdTemp" id="contractIdTemp" value="${bean.contractId}">	 
							<select id="contractId" disabled="disabled" class="width30" >
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemListCon ne null}">
									<c:forEach items="${requestScope.itemListCon}" var="contract" varStatus="status">
										<option value="${contract.contractId }">${contract.contractNumber }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<fieldset>
		                    <legend><fmt:message key="re.property.info.casedescription"/><span style="color:red">*</span></legend>
		                    <div style="height:25px" class="width60 float-left">
		                            <div id="barbox">
		                                  <div id="progressbar"></div>
		                            </div>
		                            <div id="count">500</div>
		                      </div>
		                      <p>
		                        <textarea disabled="disabled" class="width60 float-left tooltip validate[required]" id="text_area_present_input">${bean.description}</textarea>
		                      </p>
	                   </fieldset>
						
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset style="min-height:175px;">
						<div>
							<label class="width30 "><fmt:message key="re.property.info.fromdate"/></label>	 
							<input class="width40 " type="text" disabled="disabled" name="assignby" readonly="readonly" id="startPicker" value="${bean.fromDate}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.todate"/></label>	 
							<input class="width40" type="text" disabled="disabled" name="assignby" readonly="readonly"  id="endPicker" value="${bean.toDate}"/>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.status"/></label>
							<input type="hidden" name="caseStatusTemp" id="caseStatusTemp" value="${bean.caseStatus}">		 
							<select id="caseStatus" disabled="disabled">
								<c:forEach items="${requestScope.legalStatusList}" var="legalstatus" varStatus="status">
									<option value="${legalstatus.sessionStatusCode }">${legalstatus.sessionStatus}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.oppositeperson"/></label>	
							<input class="width40" disabled="disabled" type="text" name="assignby"  id="oppositPersonId" value="${bean.oppositePersonName}">
						</div>
		
					</fieldset>
		 		</div>
		 	</form>
		 	<div class="clearfix"></div> 
		 	<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.sessiondetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
							<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
			 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm" class="hastable width100">  
								<table id="hastab" class="width100">	
									<thead class="chrome_tab">
										<tr>
											<th style="width:5%"><fmt:message key="re.property.info.sessionno"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.sessiondate"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.courtname"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.lawyername"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.submitteddocument"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.receiveddocument"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.expenses"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.sessionresult"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.status"/></th>
											<th style="width:5%"><fmt:message key="re.property.info.todate"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.description"/></th>
											<th style="width:10%"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="even"> 
													<td style="width:5%">${result.sessionNumber}</td>
												    <td  style="width:10%">${result.sessionDate}</td>
												    <td  style="width:5%">${result.courtName}</td>
													<td  style="width:10%">${result.lawyerName}</td>
													<td  style="width:10%">${result.submittedDoc}</td>
													<td  style="width:10%">${result.receivedDoc}</td>
													<td  style="width:5%">${result.expenses}</td>
													<td  class="status" style="width:10%">${result.sessionResult}</td>
													<td  style="width:10%">${result.sessionName}</td>
													<c:choose>
													<c:when test="${result.sessionToDate ne '31-Dec-9999' }">
														<td style="width:5%">${result.sessionToDate}</td>
													</c:when>
													<c:otherwise>
														<td style="width:5%"></td>
													</c:otherwise>
												</c:choose>
													<td  style="width:10%">${result.statusDescription }</td>
													<td style="display:none"> 
														<input type="text" value="${result.caseChildId}" name="actualLineId" id="actualLineId"/>
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:10%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addSession" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editSession" style="cursor:pointer;"  title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delSession" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class="session_status">${result.sessionStatus}</td>
													<td style="display:none;" class="court_name">${result.courtNameCode}</td>
													<td style="display:none;" class="dms_urn">${result.dmsURN}</td>		
												</tr>
											</c:forEach>
							 		</tbody>
								</table>
							</div>
						</div>	 
					</form>	
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>							
		<div class="clearfix"></div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
			<div class="portlet-header ui-widget-header float-right cancelrec" id="cancel" style=" cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
			<div class="portlet-header ui-widget-header float-right headerData" id="confirm" style="cursor:pointer;"><fmt:message key="re.property.info.confirm"/></div></div>
	</div>
</div>					
								
								
								
								
								
								