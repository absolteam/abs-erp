<div>
	<label style="display: inline-block; width: 209px ! important;">Account No.s (Comma separated): </label>
	<textarea id="accountNo" style="min-width: 256px; max-width: 256px; min-height: 50px; max-height: 50px;"></textarea>
	<br/>
	<label style="display: inline-block; width: 76px ! important;">Service Code: </label>
	<br/>
	<input type="text" id="generalServiceType" />
	<div style="height: 20px; display: block;"></div>
	<hr/>
	<br/>
	<input style="margin-top: 20px;" type="radio" name="releaseOption" id="withDis" checked="checked" /> Clearance with disconnection
	<br/>
	<br/>
	<input type="radio" name="releaseOption" id="withoutDis" /> Clearance without disconnection
</div>

<script>
$(function (){ 
	
	releaseType = "withDis";
	generalServiceType = "GS";
	
	$('#withDis').live('click', function() {	
		releaseType = "withDis";
	});
	
	$('#withoutDis').live('click', function() {		
		releaseType = "withoutDis";	
	});
});
</script>