<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <script type="text/javascript" src="/js/dateDifference.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
 </script>
 

<script type="text/javascript">
var openFlag=0;
var fromFormat='dd-MMM-yyyy';
var toFormat='MM';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyy';
$(function (){  
	$('#tenantType').val($('#tenantTypeTemp').val());

	var paymentMethodArray=String($('#paymentMethodTemp').val()).split(',');
	for(i=0;i<=paymentMethodArray.length;i++)
		 $('#paymentMethod').find("option[value="+paymentMethodArray[i]+"]").attr('selected','selected');	

	// Calculation for Total Amount  Start Here
	var total=0;
	$('.rentAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#rentAmountTotal').val(total);
	
	total=0;
	$('.contractAmountCalc').each(function(){
		total+=Number($(this).html());
	});
	$('#contractAmountTotal').val(total);
	$('#rentAmountTotalAll').val(total);
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc
	
	total=0;
	$('.feeAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#feeAmountTotal').val(total)
	//$('#cashAmount').val(total); 
	$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	total=0;
	$('.chequeAmountCalc').each(function(){ 
		total+=Number($(this).html());
	});
	$('#chequeAmountTotal').val(total);
	$('#chequeAmountTotalAll').val(total)
	$('#chequeAmount').val(total); 
	//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc

	$('#cashAmountTotal').val($('#cashAmount').val());

	total=0;
	$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc != 'Rent'){
				total+=Number($($(this).siblings().get(2)).html());
			}
	});
	$('#otherCharges').val(total);

	$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
	$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc
	// Calculation End here

	
	
	
	//Checking Day, Month & Year 
	fromDateVar=$('#startPicker').val();
	toDateVar=$('#endPicker').val();
	if(fromDateVar != '' && toDateVar != ''){
		fromDate=fromDateVar;
		toDate=toDateVar;
		putYears=$('#years');
		putMonths=$('#months');
		putDays=$('#days');
		calculateDate(fromDate,toDate,putYears,putMonths,putDays);
		if($('#years').val()=="NaN" && " "){
			$('#years').val("0");
		}
		if($('#months').val()=="NaN" && " "){
			$('#months').val("0");
		}
		if($('#days').val()=="NaN" && " "){
			$('#days').val("0");
		}
	}

	$('.formError').remove(); 
	 $("#contractRenewalEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	//Save Contract Renewal details
	$("#save").click(function(){
		if($('#contractRenewalEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(true){
				contractId=$('#contractId').val();
				offerId = $('#offerId').val();
				contractDate=$('#defaultPopup').val();
				toDate=$('#extendedToDate').val();
				extendedToDate=$('#extendedToDate').val();
				tenantId=$('#tenantId').val();
				var paymentMethod = $("#paymentMethod option:selected").map(function(){
					   return $(this).val();
					}).get().join(",");
				noOfCheques=$('#noOfCheques').val();
				cashAmount=$('#cashAmount').val();
				description=$('#description').val();
				grandTotal=$('#grandTotal').val();
				
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/contract_renewal_update.action",
						data: {contractId: contractId,offerId: offerId, contractDate: contractDate,toDate: toDate,tenantId: tenantId,paymentMethod: paymentMethod,
							noOfCheques: noOfCheques,cashAmount: cashAmount,description: description,grandTotal:grandTotal,
							extendedToDate: extendedToDate, trnValue: transURN},  
						async: false,
						dataType: "html",
						cache: false,
						success: function(result){ 
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							}
							$('#loading').fadeOut(); 
						} 
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of contract Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Payment Method 
	$('#paymentMethod').change(function() {
		var payment=$('#paymentMethod').val();
		//alert("Inside select : "+payment);
		//var paymentFormat=payment.replace(",", "/");
		//var paymentSplit=paymentFormat.split('/');
		//alert("Inside select , payment : "+payment+" paymentSplit : "+paymentSplit);
		if(payment == 'Q'){
			$('#cashAmount').attr('disabled','disabled');
			$('#description').attr('disabled','disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else if(payment == "C,Q"){ 
			$('#cashAmount').removeAttr('disabled');
			$('#description').removeAttr('disabled');
			$('#noOfCheques').removeAttr('disabled');
			$('#noOfCheques').addClass('validate[required,custom[onlyNumber]]');
		}else {
			$('.PaymentTAB').fadeOut();
			$('#cashAmount').removeAttr('disabled');
			$('#description').removeAttr('disabled');
			$('#noOfCheques').val("");
			$.fn.globeFeeDesc();	//Remove the Existing Entry
			$.fn.globeTotalFee();  //Total for Fee Amount
			$('#noOfCheques').attr('disabled','disabled');
			$('#noOfCheques').removeClass('validate[required,custom[onlyNumber]]');
		}
	});
	

	

	$('#noOfCheques').blur(function() { //Payment Details Add Row by checking Numbers
		$('.PaymentTAB').fadeIn();
		var rowCount=$('#noOfCheques').val();
		$.ajax({
	   		type:"POST",
	   		url:"<%=request.getContextPath()%>/contract_renewal_load_cheque_fee.action", 
	   	 	async: false,
	   	 	data: {noOfCheques: rowCount},
	   	    dataType: "html",
	   	    cache: false,
	   		success:function(result){
		   		$.fn.globeFeeDesc();	//Remove the Existing Entry
		   		$(".tabFee").append(result);
		   		$('#childCountPayment').val("0");
	   			/* if($(".tabFee").height()<255)
					 $(".tabFee").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabFee").height()>255)
					 $(".tabFee").css({"overflow-x":"hidden","overflow-y":"auto"}); */
	   			$.fn.globeTotalFee();  //Total for Fee Amount
	   			$.fn.globeIndexFee();  //Set Index Fee Amount
	   			
	   		},
	   		error:function(result){
	   			$(".tabFee").append(result);
	   		}
   		});
		$(".tabPayment").html("");
		$(".tabPayment").animate({height:'0',maxHeight:'255'},0);
		for(i=1;i<=rowCount;i++){
			$.fn.addRowPayment();
		}
		$('#chequeAmountTotal').val("");
	});
	$.fn.addRowPayment = function() { 	// Addrow for Payment Details
		$.ajax({
			 type:"POST",
		      url:"<%=request.getContextPath()%>/add_contract_renewalpaymentrows.action", 
	 	  	async: false,
	     dataType: "html",
	        cache: false,
		success:function(result){
			$(".tabPayment").append(result);
			if($(".tabPayment").height()<255)
				 $(".tabPayment").animate({height:'+=20',maxHeight:'255'},0);
			 if($(".tabPayment").height()>255)
				 $(".tabPayment").css({"overflow-x":"hidden","overflow-y":"auto"});
			}
		});
	}

	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();  
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/edit_discard_contract.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 	});

	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/contract_renewal_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	$('.dateCheck').blur(function(){
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}
	});

 	

 	$("#description").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#description").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#description").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    }); 

 	// System Date Greater than
	$('#validMinPicker').datepick({
		minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	$('#extendedToDate').datepick({
 		onSelect: customRangeCalc, showTrigger: '#calImg'
	});

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});

 	
 	$('#cashAmount').keyup(function(){
 		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
 		$('#cashAmountTotal').val($('#cashAmount').val());
 		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
 	});
 	
 	$.fn.globeTotal = function() { 	// Total for Rent & Contract Amount
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total)
		
		total=0;
		$('.contractAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		$('#contractAmountTotal').val(total); 
		$('#rentAmountTotalAll').val(total);
		$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	 }
 	$.fn.globeTotalFee = function() { 	// Total for Fee Amount
		var total=0;
		$('.feeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#feeAmountTotal').val(total)
		//$('#cashAmount').val(total); 
		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));
		$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc

	 }
 	$.fn.globeTotalCheque = function() { 	// Total Payment Amount
		var total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#chequeAmountTotal').val(total);
		$('#chequeAmountTotalAll').val(total)
		$('#chequeAmount').val(total); 
		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
		$('#grandTotal').val(Number($('#cashAmountTotal').val())+Number($('#chequeAmountTotalAll').val()));

	 }
 	$.fn.globeIndexFee = function() { 	// Index For Fee Details
		var index=1;
		$('.indexForFee').each(function(){ 
			$(this).html(index);
			index=index+1;
		});
	 }
 	$.fn.globeFeeDesc = function() { 	// Remove the Exisiting Cheque Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Cheque'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeRentDesc = function() { 	// Remove the Exisiting Rent Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeContractDesc = function() { 	// Remove the Exisiting Contract Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Contract'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globePaymentValidation = function() { 	// Validation for Cheque Payment with Total Fee's
 		payment=$('#paymentMethod').val();
 		feeTotalAmount=Number($('#feeAmountTotal').val());
 		chequeAmountTemp=Number($('#chequeAmountTemp').val());
 		//alert("payment type :"+payment+" Total Fee Amount : "+feeTotalAmount+" chequeAmountTemp : "+chequeAmountTemp);
 		total=0;
 		$('.chequeAmountCalc').each(function(){
 			total+=Number($(this).html());
 		});
 		total = (total - chequeAmountTemp)+ Number($('#chequeAmount').val());
 		if(total <= feeTotalAmount){
 	 		return true;
 		}else{
 	 		return false;
 		}
	 }
	 $.fn.globeOtherChargesCalc=function(){		// Other Charges Calculation
		 total=0;
		$('.indexFeeDesc').each(function(){ 
				//alert("Desc : "+$(this).html());
				desc=$(this).html();
				if(desc != 'Rent'){
					total+=Number($($(this).siblings().get(2)).html());
				}
		});
		$('#otherCharges').val(total);	
	 }

	 
	
});
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
function customRangeCalc(dates){
	date1=$("#endPicker").val();
	date2=$("#extendedToDate").val();
	if(date1 != null && date1 != "" && date2 != null && date2 != ""  ){
		fromDateMonth=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
		toDateMonth=Number(formatDate(new Date(getDateFromFormat($("#extendedToDate").val(),fromFormat)),toFormat));
		fromDateYear=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),usrtoFormat));
		toDateYear=Number(formatDate(new Date(getDateFromFormat($("#extendedToDate").val(),fromFormat)),usrtoFormat));
		$('.rentAmountCalc').each(function(){
			rentAmountMonth=Number($(this).html())/12;
	       	monthDiff=toDateMonth-fromDateMonth;
	       	yearDiff=toDateYear-fromDateYear;
	       	monthCount=monthDiff+(12*yearDiff);
	       	contractAmountThis=rentAmountMonth*(monthCount);
	       	$($(this).siblings().get(2)).html(contractAmountThis);
	       	//alert("Rent Amount : "+rentAmountMonth+" Contract Amount  : "+contractAmountThis);
	       	//$('#contractAmount').val(contractAmountThis);
		});
		$.fn.globeTotal(); // Total for Rent & Contract Amount	
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				//alert("tr position : "+ $(this).parent('tr'));
				//$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				rentFeeTabTemp=$(this).parent('tr');
				$($(rentFeeTabTemp).children().get(3)).text(contractAmountThis);
			}
		});
		$.fn.globeTotalFee(); 	// Total for Fee's Amount
	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Renewal</div>
	 		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
	 	<form id="contractRenewalEdit">
	 		<fieldset>
	 			<div class="width50 float-left" id="hrm">
				 	<fieldset>
				 		<legend>Tenant Details</legend>
						<div>
							<label class="width30">Offer No</label>	 
							<input class="width40" type="text" name="offerNumber" value="${bean.offerNumber }" id="offerNumber" disabled="disabled" >
							<input class="width40" type="hidden" name="offerId" value="${bean.offerId }"  id="offerId" disabled="disabled" >
							<span id="hit7" class="button" style="width: 40px ! important;">
								<a class="btn ui-state-default ui-corner-all offer-popup"> 
									<span class="ui-icon ui-icon-newwin"> </span> 
								</a>
							</span>
						</div>
						<div>
							<label class="width30">Building Name</label>	
							<input class="width40 validate[required]" type="text" name="buildingName"  value="${bean.buildingName }" id="buildingName" disabled="disabled" > 
							<input class="width40" type="hidden" name="buildingId" value="${bean.buildingId }"  id="buildingId" >
						</div>
						<div>
							<label class="width30">Tenant Name</label>	 
							<input class="width40" type="text" name="tenantName" value="${bean.tenantName }"  id="tenantName" disabled="disabled" >
							<input class="width40" type="hidden" name="tenantId" value="${bean.tenantId }"  id="tenantId" >
						</div>
						<div>
							<label class="width30">Address</label>	 
							<input class="width40" type="text" name="presentAddress" value="${bean.presentAddress }"  id="presentAddress" disabled="disabled" >
						</div>
						<div>
							<label class="width30">Address1</label>	 
							<input class="width40" type="text" name="permanantAddress" value="${bean.permanantAddress }"  id="permanantAddress" disabled="disabled" >
						</div>
						<div>
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend> Period Of Rent</legend>
							 
						<div><label class="width30" id="fromDateCheck">From Date</label>
						<input type="text" class="width40 dateCheck " id="startPicker" value="${bean.fromDate }" disabled="disabled" name="employerFromDate" tabindex="7" readonly="readonly">
						</div>
						
						<div><label class="width30">TO Date<span style="color:red">*</span></label>
						<input type="text" class="width40 employerToDate dateCheck  validate[required]" id="endPicker" value="${bean.toDate }" name="employerToDate" tabindex="8" readonly="readonly">
						</div>
						<div><label class="width30">Years</label><input type="text" class="width40" name="years" id="years" disabled="disabled"></div>
					<div><label class="width30">Months</label><input type="text" class="width40" name="months" id="months" disabled="disabled"></div>
					<div><label class="width30">Days</label><input type="text" class="width40" name="days" id="days"  disabled="disabled"></div>
					<div><label class="width30">Extended To Date<span style="color:red">*</span></label><input type="text" class="width40  validate[required]" name="days" id="extendedToDate"></div>
					<div><label class="width30">Penalty Fees</label><input type="text" class="width40" name="days" id="penaltyFees" value="${result.balanceAmount }"/></div>
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset style="min-height:134px;">
						<legend>Contract Details</legend>
						<div >
							<label class="width30">Contract No</label>	 
							<input class="width40" type="text" name="contractNumber" value="${bean.contractNumber }"  id="contractNumber" disabled="disabled" >
							<input class="width40" type="hidden" name="contractId" value="${bean.contractId }"  id="contractId" disabled="disabled" >
						</div>
						<div >
							<label class="width30">Contract Date</label>	 
							<input class="width40 contractDate" type="text" name="contractDate" value="${bean.contractDate }"  id="defaultPopup" disabled="disabled" >
						</div>
						<div>
							<label class="width30">Tenant Type</label>
							<input type="hidden" class="width40" name="tenantTypeTemp"  value="${bean.tenantTypeId }" id="tenantTypeTemp">	 
							<select class="width40" name="tenantType"  id="tenantType" disabled="disabled">
								<option value="">Select</option>
								<c:forEach items="${requestScope.itemList}" var="tenant" varStatus="status">
									<option value="${tenant.tenantTypeId }">${tenant.tenantTypeName }</option>
								</c:forEach>
							</select>
						</div>
						<div >
							<label class="width30">Payement Method<span style="color:red">*</span></label>	
							<input type="hidden" class="width40" name="paymentMethodTemp"  value="" id="paymentMethodTemp"> 
							<select id="paymentMethod" class="validate[required]" multiple="multiple" size="2">
								<option value="C">Cash</option>
								<option value="Q">Cheque</option>
							</select>
						</div>
						<div>
							<label class="width30">No of Cheque</label>	 
							<input class="width40" type="text" name="noOfCheques" value="" id="noOfCheques" disabled="disabled" >
						</div>
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend> Renewal Reason</legend>
						<div style="height:25px;width:75%;" class="float-left">
	                            <div id="barbox">
	                                  <div id="progressbar"></div>
	                            </div>
	                            <div id="count">500</div>
	                      </div>
	                      <p>
	                        <textarea class="width60 float-left" id="description"></textarea>
	                      </p>
					</fieldset>
				</div>
			</fieldset>
		</form>
		<div class="clearfix"></div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Rent Details</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
				 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
				 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width20">Line No</th> 
											<th class="width20">Flat No</th>
											<th class="width20">Rent Amount</th>
											<th class="width20">Contract Amount</th>
											<th style="width:5%;display:none;">Options</th>
										</tr> 
									</thead> 
									<tbody class="tab">
										<c:forEach items="${requestScope.result1}" var="result" >
										 	<tr class="even"> 
												<td class="width20">${result.lineNumber }</td>
												<td class="width20">${result.flatNumber }</td>	
												<td class="width20 rentAmountCalc">${result.rentAmount }</td> 
												<td class="width20 contractAmountCalc">${result.contractAmount }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result.contractRentId }" name="actualLineId" id="actualLineId"/>	
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>
												</td> 
												<td style=" width:5%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:56%">
							<label>Total</label>
						 	<input name="rentAmountTotal" id="rentAmountTotal" class="width40" disabled="disabled" >
						 	<input name="contractAmountTotal" id="contractAmountTotal" class="width40" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
		</div>	
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container "> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Fee Details</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div>   
					<div id="warningMsgFee" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
				 		<input type="hidden" name="childCountFee" id="childCountFee"  value="${countSize2}"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width20">Line No</th> 
											<th class="width20">Fee Id</th>
											<th class="width20">Description</th>
											<th class="width20" >Amount</th>
											<th style="width:5%;display:none;">Options</th>
										</tr> 
									</thead> 
									<tbody class="tabFee" style="">	
										<c:forEach items="${requestScope.result2}" var="result2" >
										 	<tr class="even"> 
												<td class="width20">${result2.lineNumber }</td>
												<td class="width20">${result2.feeId }</td>	
												<td class="width20 indexFeeDesc">${result2.feeDescription }</td> 
												<td class="width20 feeAmountCalc">${result2.feeAmount }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result2.contractFeeId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:5%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFee" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFee"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFee" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:33%">
							<label>Total</label>
						 	<input name="feeAmountTotal" id="feeAmountTotal" class="width70" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfee" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
		</div>
		
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container PaymentTAB"  style="display:none;"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payment Details</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content">  
				 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value=""/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100" > 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width20">Bank Name</th> 
											<th class="width20">Cheque Date</th>
											<th class="width20">Cheque No</th>
											<th class="width20">Cheque Amount</th>
											<th style="width:5%;">Options</th>
										</tr> 
									</thead> 
									<tbody class="tabPayment" style="">	
										<c:forEach var="i" begin="1" end="3" step="1" varStatus ="status">
											 	<tr class="even"> 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20 chequeAmountCalc"></td>
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:33%">
							<label>Total</label>
						 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
					<div class="portlet-header ui-widget-header float-left addrowspayment">Add Row</div> 
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
			<form id="contractAgreementCash">
				<div class="width60 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.contract.cashamount"/></legend>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.contract.cashamount"/></label>	 
							<input class="width30" type="text" name="cashAmount" value=""  id="cashAmount" disabled="disabled">
						</div>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.property.info.description"/></label>	 
							<input class="width30" type="text" name="description"  id="description" value="" disabled="disabled">
						</div>
					</fieldset>
				</div>
			</form>
			<div class="width60 float-left" id="hrm">
				<fieldset style="min-height:134px;">
					<legend><fmt:message key="re.contract.grossdetails"/></legend>
					<div style="display:none;">
						<label class="width30"><fmt:message key="re.contract.chequeamount"/></label>	 
						<input class="width30" type="text" name="chequeAmount"  id="chequeAmount" disabled="disabled" >
					</div>
					<div>
						<label class="width20">Total Rent</label>	 
						<input class="width20 float-left" type="text" name="rentAmountTotalAll"  id="rentAmountTotalAll" disabled="disabled" >
					
						<label class="width20">Total Cash Amount</label>	 
						<input class="width20" type="text" name="cashAmountTotal"  id="cashAmountTotal" disabled="disabled" >
						
					</div>
					<div>
						<label class="width20">Other Charges</label>	 
						<input class="width20 float-left" type="text" name="otherCharges"  id="otherCharges" disabled="disabled" >
						<label class="width20" style="font-size: 11px ! important;">Total Cheque Payment</label>	 
						<input class="width20" type="text" name="chequeAmountTotalAll"  id="chequeAmountTotalAll" disabled="disabled" >
					</div>
					<div>
						<label class="width20">Grand Fees</label>	 
						<input class="width20 float-left" type="text" name="grandFees"  id="grandFees" disabled="disabled" >
						<label class="width20"><fmt:message key="re.contract.grandtotal"/></label>	 
						<input class="width20" type="text" name="grandTotal"  id="grandTotal" disabled="disabled" >
					</div>
					<div style="display:none;">
						<label class="width30">Amount in Words Only</label>
					</div>
				</fieldset>
			</div>
		<div class="clearfix"></div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div class="portlet-header ui-widget-header float-right discard" style="display:none;">Release</div> 
			<div class="portlet-header ui-widget-header float-right discard" id="cancel">Cancel</div> 
		 	<div class="portlet-header ui-widget-header float-right save-all" id="save">Save</div> 
		</div>
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="contract-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="contract-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>