<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  
	
		
});



</script>
  



<div id="main-content">
				 
				 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Tenant Type Information</div>
					
					<fieldset>

						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend>Tenant Type Information 2</legend>						
		                            <div><label>No. of Days Allow Late<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip"  title="Select Birth Date" ></div>		
									<div><label>No. of Days for Offer<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" ></div>
									<div><label for="curEnable">Pay Penalties</label>
										<input type="checkbox" name="curEnable" id="curEnable" style="width:2%;"  class="validate[required]"></div>							
									<div><label for="curEnable">Pay Cheque Fees</label>
										<input type="checkbox" name="curEnable" id="curEnable" style="width:2%;"  class="validate[required]"></div>							
									<div><label for="curEnable">Need Release Letter</label>
										<input type="checkbox" name="curEnable" id="curEnable" style="width:2%;"  class="validate[required]"></div>							
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset>
								<legend>Tenant Type Information 1</legend>	
			   						<div><label>Tenant Type Id<span style="color:red">*</span></label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>             		
			   						<div><label>Tenant Type Description<span style="color:red">*</span></label><input type="text" name="sno" class="width50 tooltip" title="Enter Designation" ></div>             		
									<div><label for="curEnable">Pay Deposit</label>
										<input type="checkbox" name="curEnable" id="curEnable" style="width:2%;"  class="validate[required]"></div>							
									<div><label for="curEnable">Pay After Contract</label>
										<input type="checkbox" name="curEnable" id="curEnable" style="width:2%;"  class="validate[required]"></div>							
							</fieldset> 
							
							
						</div>
				
				</fieldset>
							
	<div class="float-right buttons ui-widget-content ui-corner-all"> 			
		<a href="#" onclick="return editpersonalDetailsAjaxCall();"><div class="portlet-header ui-widget-header float-right"  id="edit">Cancel</div></a>
		<a href="#" onclick="return personalDetailsAjaxCall();"><div class="portlet-header ui-widget-header float-right" id="add_person" >Save</div></a>
		
	</div>
					</div>
				</div>
			</div>