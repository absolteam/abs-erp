<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<style>
.rowid{
	height:23px;
}
</style>
<script type="text/javascript">
var openFlag=0;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=""; 
var tempid=""; var accountTypeId=0;
var transactionFlag=0;
$(function (){   
	
	//Combination pop-up config
    $('.codecombination-popup').click(function(){
    	tempid=$(this).parent().get(0);  
	      var accountTypeId=0; 
	      var actionname=$(tempid).attr('id');
	      slidetab=$(tempid).attr('id'); 
	      var actionname=$(tempid).attr('id');
	  	  if(actionname=="feecode"){
	  		 accountTypeId=3; //Revenue Account
	  	  }
	  	  else if(actioname="depositcode"){
	  		  accountTypeId=2; //Liability Account
	  	  }   
          $('#codecombination-popup').dialog('open');
             $.ajax({
                type:"POST",
                url:"<%=request.getContextPath()%>/getcombination_redirect.action",
                 async: false,
                 data:{accountTypeId:accountTypeId},
                dataType: "html",
                cache: false,
                success:function(result){
                     $('.codecombination-result').html(result);
                },
                error:function(result){
                     $('.codecombination-result').html(result);
                }
            });
    }); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:400,
			bgiframe: false,
			modal: true 
		}); 
	
	//Payment Method 
	$.fn.globePaymentModechange=function(){		// Other Charges Calculation
		
		var paymentMode=$('#paymentMode').val();
		//alert($('#paymentModeCheque').attr('checked'));
		//alert(paymentMode);
		
		if(paymentMode==1){

			$('#bankName').removeClass('validate[required]');
			$('#defaultActualPicker').removeClass('validate[required]');
			$('#chequeNumber').removeClass('validate[required]');
			
			$('#bankName').attr('readonly','readonly');
			$('#defaultActualPicker').attr('readonly','readonly');
			$('#chequeNumber').attr('readonly','readonly');

			$('#bankName').val("-NA-");
			$('#defaultActualPicker').val("-NA-");
			$('#chequeNumber').val("-NA-");
			
			$('#bankNameDiv').css("display","none");
			$('#defaultActualPickerDiv').css("display","none");
			$('#chequeNumberDiv').css("display","none");
			
		}else if(paymentMode==2) {
			$('#bankName').val("");
			$('#defaultActualPicker').val("");
			$('#chequeNumber').val("");

			$('#bankNameDiv').css("display","block");
			$('#defaultActualPickerDiv').css("display","block");
			$('#chequeNumberDiv').css("display","block");
			
			$('#bankName').removeAttr('readonly');
			$('#defaultActualPicker').removeAttr('readonly');
			$('#chequeNumber').removeAttr('readonly');
			
			$('#bankName').addClass('validate[required]');
			$('#defaultActualPicker').addClass('validate[required]');
			$('#chequeNumber').addClass('validate[required]');
		}else if(paymentMode==3){
			$('#bankName').val("");
			$('#defaultActualPicker').val("");
			$('#chequeNumber').val("");

			$('#bankNameDiv').css("display","block");
			$('#defaultActualPickerDiv').css("display","block");
			$('#chequeNumberDiv').css("display","none");
			
			$('#chequeNumber').val("-NA-");

			
			$('#bankName').addClass('validate[required]');
			$('#defaultActualPicker').addClass('validate[required]');
			$('#chequeNumber').removeClass('validate[required]');
			
		}else{
			return false;
		}
	}
	
	//$('#hrm fieldset').css("border","none");
	
	<%--$('#example').dataTable({ 
 		"sAjaxSource": "json_contract_offer_detail_list.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 		"aoColumns": [
 			{ "sTitle": 'Offer ID', "bVisible": false},
 			{ "sTitle": 'Property/Component/Unit Name'},
 			{ "sTitle": 'Rent Amount'},
 		],
 		
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#example').dataTable();

 	/* Click event handler */
 	$('#example tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});

	--%>
	
	
	 $('.offer-popup').click(function(){ 
	       tempid=$(this).parent().get(0);  
	       $('.ui-dialog-titlebar').remove(); 
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/contract_offer_redirect.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.contract-result-pop').html(result); 
				},
				error:function(result){  
					 $('.contract-result-pop').html(result); 
				}
			});  
		});
	 $('#contract-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:1000, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	 });
		 
	 

	$('#offerId').change(function() {
		var offerId=Number($('#offerId').val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/contract_agreement_offer_detail_list_get.action",
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('.contract-result-pop').html(result); 
			},
			error:function(result){ 
				 $('.contract-result-pop').html(result); 
			}
		}); 
	});
	
	$('#noOfCheques').change(function() { //Payment Details Add Row by checking Numbers
		tenantTypeId=Number($('#tenantTypeId').val());
		$('.PaymentTAB').fadeIn();
		var rowCount=Number($('#noOfCheques').val());
		//alert("tenantTypeId : "+tenantTypeId);
		var contractAmount=convertToDouble($('#contractAmount').val());
		var depositAmount=convertToDouble($('#depositAmount').val());
		var i=1;
		$('.rowid').each(function(){  
			if(i>2){
				$(this).remove();
			}
			i=i+1;
			
	 	});
		$(".tabPayment").animate({height:'0',maxHeight:'255'},0);
		for(i=1;i<=rowCount;i++){
				$.fn.addRowPayment();
		}
		$('#chequeAmountTotal').val("");
		$('#chequeAmountTotalAll').val("");
		contractPaymentAddVisibility();
	});
	
	$.fn.addRowPayment = function() { 	// Addrow for Payment Details
		$.ajax({
				 type:"POST",
			      url:"<%=request.getContextPath()%>/add_contract_agreementpaymentrows.action", 
		 	  	async: false,
		     dataType: "html",
		        cache: false,
			success:function(result){
				$(".tabPayment").append(result);
				}
			});

		//To reset sequence 
		var i=0;
			$('.rowid').each(function(){  
			i=i+1;
			//$($(this).children().get(0)).text(i); 
			$($($(this).children().get(5)).children().get(1)).val(i);
		 });  
	}
	


	//Save Contract Agreement details
	$("#save").click(function(){
		$(".error,.success").hide();
		if($jquery('#contractAgreementAdd, #contractAgreementReceive, #contractAgreementCash').validationEngine('validate')){
				var childCountPayment = convertToDouble($('#childCountPayment').val()); 
					grandFees=convertToDouble($('#rentAmountTotalAll').val());
					grandTotal=convertToDouble($('#grandTotal').val());
					if( grandFees == grandTotal ){
						offerId = $('#offerId').val();
						buildingId=$('#buildingId').val();
						contractDate=$('#defaultPopup').val();
					
						tenantId=$('#tenantId').val();
						grandTotal=convertToDouble($('#grandTotal').val());
						paymentMethod=$('#paymentMethodTemp').val();
						
											
						noOfCheques=convertToDouble($('#noOfCheques').val());
						cashAmount=convertToDouble($('#cashAmount').val());
						description=$('#description').val();
	
						contractAmount=convertToDouble($('#contractAmount').val());
						depositAmount=convertToDouble($('#depositAmount').val());
						contractNumber=$('#contractNumber').val();
						var paymentReceived=$('#paymentReceived').val();
						var renewalFlag=null;;

						var remarks = $('#remarks').val(); 
						var leasedFor = $('#leasedFor').val(); 
						
						//alert(" transURN : "+transURN+" paymentMethod : "+paymentMethod);
						//if(childCountPayment == noOfCheques){
							$('#loading').fadeIn();
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/add_final_save_contract.action",
								data: {offerId: offerId,contractNumber:contractNumber, contractDate: contractDate,tenantId: tenantId,
									cashAmount: cashAmount,description: description,contractAmount:contractAmount,
									depositAmount:depositAmount,paymentMethod: paymentMethod,remarks:remarks,leasedFor: leasedFor,
									noOfCheques: noOfCheques,paymentReceived:paymentReceived,renewalFlag:renewalFlag
									},  
						     	async: false,
								dataType: "html",
								cache: false,
								success: function(result){  
									$('#contract-popup').dialog('destroy');		
									$('#contract-popup').remove(); 
									destroyPopups();
									$("#main-wrapper").html(result); 	
								} 
							});
							$('#loading').fadeOut();  
					}else{
						$('.childCountErr').hide().html("Rent total & Grand Total Should be Same.!!!").slideDown();
						// Scroll to top of the page
						$.scrollTo(0,300);
						return false; 
					}
		 }else{
			 $('.childCountErr').hide().html("Enter all the required information.!!!").slideDown();
			  return false;
		 }
	});
	
	$("#discard").click(function(){
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/contract_agreement_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#contract-popup').dialog('destroy');		
				$('#contract-popup').remove();  
				destroyPopups();
		 		$("#main-wrapper").html(result);
			},
			error:function(result){
				$('#contract-popup').dialog('destroy');		
				$('#contract-popup').remove(); 
				destroyPopups();
				$("#main-wrapper").html(result);
			} 
    	});
 	});

	if($("#contractId").val()>0){

		
		$('#paymentReceived').val($('#paymentReceivedTemp').val());
		$('#hit7').hide();
		//No of installment
		$('#noOfCheques').attr('disabled','disabled');
		
		// Calculation for Total Amount  Start Here
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total);
		
		total=0;
		$('.contractAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		$('#contractAmountTotal').val(total);
		$('#rentAmountTotalAll').val(total);
		//$('#grandFees').val(Number($('#otherCharges').val())+Number($('#rentAmountTotalAll').val())); // Grand Fee Calc
		
		
		total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#chequeAmountTotal').val(total);
		$('#chequeAmountTotalAll').val(total);
		$('#chequeAmount').val(total); 

		//$.fn.globeTotalFee();
		// Total for Fee Amount
 		var contractAmount=Number($('#contractAmount').val());
		var depositAmount=Number($('#depositAmount').val());
		$('#grandFees').val(contractAmount+depositAmount); // Grand Fee Calc
		var totalRent=Number($('#grandFees').val());
		totalRent+=Number($('#contractAmountTotal').val());
		$('#rentAmountTotalAll').val(totalRent);
		
 		$('#grandTotal').val(Number($('#otherCharges').val())+Number($('#chequeAmountTotalAll').val()));


 		//change the amount to amount formate
 		convertContractScreenAmount();
 	

 		//Date Check blur
		fromDateVar=$('#startPicker').val();
		toDateVar=$('#endPicker').val();
		if(fromDateVar != '' && toDateVar != ''){
			fromDate=fromDateVar;
			toDate=toDateVar;
			putYears=$('#years');
			putMonths=$('#months');
			putDays=$('#days');
			calculateDate(fromDate,toDate,putYears,putMonths,putDays);
			if($('#years').val()=="NaN" && " "){
				$('#years').val("0");
			}
			if($('#months').val()=="NaN" && " "){
				$('#months').val("0");
			}
			if($('#days').val()=="NaN" && " "){
				$('#days').val("0");
			}
		}
		
	}else{
		var curDate=new Date().toString();
		var splitDate=curDate.split(" ");
		var day=splitDate[2];
		var mon=splitDate[1];
		var year=splitDate[3];
		fromDate=day+"-"+mon+"-"+year;
		$('#defaultPopup').val(fromDate);
		contractPaymentAddVisibility();

	}
	
	 if($("#contractId").val() > 0) {
		 
			populateUploadsPane("doc","contractDocs","Contract", $("#contractId").val());				
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","contractDocs","Contract",$("#contractId").val(),"Contracts");
			});
		}
		else {
			$('#dms_document_information').click(function(){
				AIOS_Uploader.openFileUploader("doc","contractDocs","Contract","-1","Contracts");
			});
		}
 	
	// System Date Greater than
	$('#validMinPicker').datepick({
	minDate: 0, showTrigger: '#calImg'});
	//Default Date Picker
	$('#defaultPopup').datepick();

	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	});


 	$('#chequeAmountTotalAll').change(function(){
 		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
 		$('#grandTotal').val(Number($('#otherCharges').val())+Number($('#chequeAmountTotalAll').val()));
 	});

 	$('#contractAmount,#depositAmount').blur(function(){
			$.fn.globeTotalFee();  //For Total Amount of Rent & Contract
			contractAmount=convertToAmount($("#contractAmount").val());
			depositAmount=convertToAmount($("#depositAmount").val());
			var roid=1;
			$('.rowid').each(function(){ 
				if(roid==1){
					$($(this).children().get(3)).text(contractAmount);
				}else if(roid==2){
					$($(this).children().get(3)).text(depositAmount);
				}
				roid=roid+1;
			});
	});
	
 	$.fn.globeTotal = function() { 	// Total for Rent & Contract Amount
		var total=0;
		$('.rentAmountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		$('#rentAmountTotal').val(total);
		
		total=0;
		$('.contractAmountCalc').each(function(){
			total+=Number(convertToDouble($(this).html()));
		});
		$('#contractAmountTotal').val(total); 
		$('#rentAmountTotalAll').val(total);

		//Convert to amount
		$('.contractAmountCalc').each(function(){
			$(this).html(convertToAmount($(this).html()));
		});
		$('#contractAmountTotal').val(convertToAmount($('#contractAmountTotal').val())); 
		$('#rentAmountTotalAll').val(convertToAmount($('#rentAmountTotalAll').val()));
	 }
 	$.fn.globeTotalFee = function() { 	

 	 	// Total for Fee Amount
 		var contractAmount=convertToDouble($('#contractAmount').val());
		var depositAmount=convertToDouble($('#depositAmount').val());
		$('#grandFees').val(contractAmount+depositAmount); // Grand Fee Calc
		var totalRent=convertToDouble($('#grandFees').val());
		totalRent+=convertToDouble($('#contractAmountTotal').val());
		$('#rentAmountTotalAll').val(totalRent);
		$('#grandTotal').val(convertToDouble($('#cashAmountTotal').val())+convertToDouble($('#chequeAmountTotalAll').val()));
		
		//Convert to amount
		$('#grandFees').val(convertToAmount($('#grandFees').val()));
		$('#rentAmountTotalAll').val(convertToAmount($('#rentAmountTotalAll').val()));
		$('#grandTotal').val(convertToAmount($('#grandTotal').val()));
	 }

 	$.fn.globeTotalCheque = function() { 	// Total Payment Amount
		var total=0;
		$('.chequeAmountCalc').each(function(){ 
			total+=convertToDouble($(this).html());
		});
		$('#chequeAmountTotal').val(total);
		$('#chequeAmountTotalAll').val(total);
		$('#chequeAmount').val(total); 
		//$('#grandAmount').val(Number($('#cashAmount').val())+Number($('#chequeAmountTotal').val())); // Grand Amount Calc
		$('#grandTotal').val(convertToDouble($('#cashAmountTotal').val())+convertToDouble($('#chequeAmountTotalAll').val()));
		
		//Convert to amount
		$('.chequeAmountCalc').each(function(){ 
			$(this).html(convertToAmount($(this).html()));
		});
		$('#chequeAmountTotal').val(convertToAmount($('#chequeAmountTotal').val()));
		$('#chequeAmountTotalAll').val(convertToAmount($('#chequeAmountTotalAll').val()));
		$('#chequeAmount').val(convertToAmount($('#chequeAmount').val()));
		$('#grandTotal').val(convertToAmount($('#grandTotal').val()));

	 }

 	$.fn.globePaymentValidation = function() { 	// Validation for Cheque Payment with Total Fee's
 		payment=$('#paymentMethod').val();
 		feeTotalAmount=convertToDouble($('#feeAmountTotal').val());
 		chequeAmountTemp=convertToDouble($('#chequeAmountTemp').val());
 		//alert("payment type :"+payment+" Total Fee Amount : "+feeTotalAmount+" chequeAmountTemp : "+chequeAmountTemp);
 		total=0;
 		$('.chequeAmountCalc').each(function(){
 			total+=convertToDouble($(this).html());
 		});
 		total = (total - chequeAmountTemp)+ convertToDouble($('#chequeAmount').val());
 		if(total <= feeTotalAmount){
 	 		return true;
 		}else{
 	 		return false;
 		}
	 }
	<%--
 	
 	
 	
 	$.fn.globeIndexFee = function() { 	// Index For Fee Details
		var index=1;
		$('.indexForFee').each(function(){ 
			$(this).html(index);
			index=index+1;
		});
	 }
 	$.fn.globeFeeDesc = function() { 	// Remove the Exisiting Cheque Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Cheque'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeRentDesc = function() { 	// Remove the Exisiting Rent Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeContractDesc = function() { 	// Remove the Exisiting Contract Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Contract'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeMaintenanceDesc = function() { 	// Remove the Exisiting Maintenance Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Maintenance'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	$.fn.globeDepositDesc = function() { 	// Remove the Exisiting Deposit Details in Fee Id
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Deposit'){
				$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				$(this).parent('tr').remove();
			}
		});
	 }
 	
 	
	 $.fn.globeOtherChargesCalc=function(){		// Other Charges Calculation
		 total=0;
		$('.indexFeeDesc').each(function(){ 
				//alert("Desc : "+$(this).html());
				desc=$(this).html();
				if(desc != 'Rent'){
					total+=Number($($(this).siblings().get(2)).html());
				}
		});
		$('#otherCharges').val(total);	
	 }
	

	--%>
	//Adding Payment Details
	$('.addPayment').click(function(){ 
		if(openFlag==0){
			if($jquery('#contractAgreementAdd').validationEngine('validate')){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_add_contract_payment_redirect.action", 
				 	data: {addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
			         
			         var temp4= $($(slidetab).children().get(3)).text();//Setting the contract fee & deposit amount if exist
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp4);

			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
		}
	});
	//Edit Payment Details
		$(".editPayment").click(function(){
			if(openFlag==0){
				if($jquery('#contractAgreementAdd').validationEngine('validate')){
					$('#loading').fadeIn();
					slidetab=$(this).parent().parent().get(0); 
					$('.error').hide();
			        $('.childCountErr').hide();
			        $('#warningMsg').hide();
			        
					actualID=$($($(slidetab).children().get(5)).children().get(0)).val();
					id=$($($(slidetab).children().get(5)).children().get(1)).val();
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_add_contract_payment_redirect.action",  
					 	data: {addEditFlag:"E",id:id},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $($($(slidetab).children().get(6)).children().get(0)).hide();	//Add Button
						 $($($(slidetab).children().get(6)).children().get(1)).hide();	//Edit Button
				         $($($(slidetab).children().get(6)).children().get(2)).hide(); //Delete Button
				         $($($(slidetab).children().get(6)).children().get(3)).show();	//Processing Button
							
						 
						 var temp1= $($(slidetab).children().get(0)).text();
						 var temp2= $($(slidetab).children().get(1)).text();
						 var temp3= $($(slidetab).children().get(2)).text();
						 var temp4= $($(slidetab).children().get(3)).text();
						 var temp5= $($($(slidetab).children().get(5)).children().get(2)).val();//Fee Type
							//alert(temp1+"--"+temp2+"--"+temp3+"--"+temp4);
						 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
							if((temp3==null || temp3=="" || temp3=="-NA-") && (temp1!=null && temp1!="" && temp1!="-NA-" && temp1!="Cash")){
								$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val("3");
							}else if(temp1!=null && temp1!="" && temp1!="-NA-" && temp1!="Cash"){
								$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val("2");
							}else{
								$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val("1");
							}
						
						//Trigger for payment mode
						 $.fn.globePaymentModechange();
						
							$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp3);
							$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp4);
							$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp5);
							if(temp1=="Cash")
								temp1="-NA-";
						  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp1);
						  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp2);
						openFlag=1;
					  	$('#loading').fadeOut();
			
						}
					});
					
				}else{
					return false;
				}
			}		
		});
		
		//Delete Payment Details
	 	$('.delPayment').click(function(){
	 		if(openFlag==0){
		 		slidetab=$(this).parent().parent().get(0); 
		 		$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		 		var trnValue=$("#transURN").val(); 
		 		actualID=$($($(slidetab).children().get(6)).children().get(0)).val();
				var flag = false; 
				//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID); 
				id=$($($(slidetab).children().get(6)).children().get(1)).val();
				//if(actualID != null && actualID !='' && actualID!=undefined){
					$('#loading').fadeIn();
			       	 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_add_contract_payment_save.action", 
					 	async: false,
					 	data: {addEditFlag:"D",id:id},
					    dataType: "html",
					    cache: false,
						success:function(result){	
					 		$('.formError').hide();
			                 $('#temperror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	 $("#transURN").val($('#objTrnVal').html());
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
			                              flag = true;
						    	else{	
							    	flag = false;
							    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){ 
					 		 $('.tempresult').html(result);
			                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
			                      $('#loading').fadeOut();
			                      return false;
					 	} 
			       	});
			       	if(flag==true){
			       		 $(this).parent().parent('tr').remove();
			       		 var childCount=Number($('#childCount').val());
			       		 if(childCount > 0){
			       			childCount=childCount-1;
							$('#childCount').val(childCount);
			       		 }
				   }
				/*}else{
					$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
					$('#warningMsg').hide().html("Please insert record to delete").slideDown();
					return false; 
				}*/
	 		}	
	 	});

	 	$('.dateCheck').blur(function(){
			fromDateVar=$('#startPicker').val();
			toDateVar=$('#endPicker').val();
			if(fromDateVar != '' && toDateVar != ''){
				fromDate=fromDateVar;
				toDate=toDateVar;
				putYears=$('#years');
				putMonths=$('#months');
				putDays=$('#days');
				calculateDate(fromDate,toDate,putYears,putMonths,putDays);
				if($('#years').val()=="NaN" && " "){
					$('#years').val("0");
				}
				if($('#months').val()=="NaN" && " "){
					$('#months').val("0");
				}
				if($('#days').val()=="NaN" && " "){
					$('#days').val("0");
				}
			}
		});
	
	 	$('#contractAmount').val(convertToDouble($('#contractAmount').val()));
	 	$('#depositAmount').val(convertToDouble($('#depositAmount').val()));
});
function convertContractScreenAmount(){
	$('.contractAmountCalc').each(function(){
		$(this).html(convertToAmount($(this).html()));
	});
	$('.chequeAmountCalc').each(function(){
		$(this).html(convertToAmount($(this).html()));
	});
	$('#contractAmountTotal').val(convertToAmount($('#contractAmountTotal').val()));
	$('#grandFees').val(convertToAmount($('#grandFees').val()));
	$('#rentAmountTotalAll').val(convertToAmount($('#rentAmountTotalAll').val()));
	$('#grandTotal').val(convertToAmount($('#grandTotal').val()));
	$('#chequeAmountTotal').val(convertToAmount($('#chequeAmountTotal').val()));
	$('#contractAmount').val(convertToAmount($('#contractAmount').val()));
	$('#depositAmount').val(convertToAmount($('#depositAmount').val()));
	
}
//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	date1=$("#startPicker").val();
	date2=$("#endPicker").val();
	if(date1 != null && date1 != "" && date2 != null && date2 != ""  ){
		var rentFees=Number($('#rentFees').val());
		fromDateMonth=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
		toDateMonth=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
		fromDateYear=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),usrtoFormat));
		toDateYear=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),usrtoFormat));
		$('.rentAmountCalc').each(function(){
	       	if(rentFees !=1){
				rentAmountMonth=Number($(this).html())/12;
		       	monthDiff=(toDateMonth-fromDateMonth)+1;
		       	yearDiff=toDateYear-fromDateYear;
		       	monthCount=monthDiff+(12*yearDiff);
		       	contractAmountThis=Math.ceil(rentAmountMonth*(monthCount));
		       	$($(this).siblings().get(2)).html(contractAmountThis);
		       	//alert("Rent Amount : "+rentAmountMonth+" Contract Amount  : "+contractAmountThis);
		       	//$('#contractAmount').val(contractAmountThis);
	       	}else{
	       		$($(this).siblings().get(2)).html($('#rentFeesAmount').val());
	       	}
		});
		$.fn.globeTotal(); // Total for Rent & Contract Amount	
		$('.indexFeeDesc').each(function(){ 
			//alert("Desc : "+$(this).html());
			desc=$(this).html();
			if(desc == 'Rent'){
				//alert("tr position : "+ $(this).parent('tr'));
				//$(".tabFee").animate({height:'-=20',maxHeight:'255'},0);
				if(rentFees !=1){
					rentFeeTabTemp=$(this).parent('tr');
					$($(rentFeeTabTemp).children().get(3)).text(contractAmountThis);
				}else{
					rentFeeTabTemp=$(this).parent('tr');
					$($(rentFeeTabTemp).children().get(3)).text($('#rentFeesAmount').val());
				}
			}
		});
		$.fn.globeTotalFee(); 	// Total for Fee's Amount
	}
	
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
	$('.dateCheck').trigger('blur');
}
function combinationauto(){
	if($('#combinationId_json').val()!=null && $('#combinationId_json').val()!=""){
		var codeArray=new Array();
		companyCode=$('#company_json').val();
		codeArray=companyCode.split('[');
		companyCode=codeArray[0];

		var companyCodeDesc=codeArray[1];

		codeArray="";
		costCode=$('#cost_json').val();
		codeArray=costCode.split('[');
		costCode=codeArray[0];

		var costCodeDesc=codeArray[1];

		codeArray="";
		naturalCode=$('#natural_json').val();
		codeArray=naturalCode.split('[');
		naturalCode=codeArray[0];

		var naturalCodeDesc=codeArray[1];

		codeArray="";
		var analyisCodeDesc="";
		if(analyisCode!=""){
			analyisCode=$('#analysis_json').val();
			codeArray=analyisCode.split('[');
			analyisCode=codeArray[0]; 
			analyisCodeDesc=codeArray[1];
		}
		codeArray="";
		var bufferCode1Desc="";
		buffer1Code=$('#buffer1_json').val();
		if(buffer1Code!=""){ 
			codeArray=buffer1Code.split('[');
			buffer1Code=codeArray[0]; 
			bufferCode1Desc=codeArray[1]; 
		} 
		codeArray="";
		var bufferCode2Desc="";
		buffer2Code=$('#buffer2_json').val();
		if(buffer2Code!=""){ 
			codeArray=buffer2Code.split('[');
			buffer2Code=codeArray[0]; 
			bufferCode2Desc=codeArray[1];
		}
		var commonCodes= companyCode+"."+costCode+"."+naturalCode+"."+analyisCode+"."+buffer1Code+"."+buffer2Code; 
		var commonCodesDesc= companyCodeDesc.replace("]", "")+"."+costCodeDesc.replace("]", "")+"."+
					naturalCodeDesc.replace("]", "")+"."+analyisCodeDesc.replace("]", "")+"."+
					bufferCode1Desc.replace("]", "")+"."+bufferCode2Desc.replace("]", "");  
		if(accountTypeId==3){ 
			$('#contractFee').val(combinationId);
			$('.feeDesc').html(naturalCode+" ["+naturalCodeDesc);
        }
        else{
        	$('#contractDeposit').val(combinationId); 
			$('.depositDesc').html(naturalCode+" ["+naturalCodeDesc);
        }
		accountTypeId=0;
		 $('#codecombination-popup').dialog("close");
		  return false;
	}
	else{
		alert("Please select one combination");
		  return false;
	}  
	  return false;
}

function contractPaymentAddVisibility(){
	var restrictionPlace=Number(0);
	$('.rowid').each(function(){
		var bankName=$($(this).children().get(0)).html();
		if(bankName==null || bankName==''){
			return false;
		}
		restrictionPlace=Number(restrictionPlace)+1;
	});
	var position=Number(0);
	$('.rowid').each(function(){
		var display=$($($(this).children().get(6)).children().get(0)).css('display');
		var bankName=$($(this).children().get(0)).html();
		//alert("display-->"+display+"-restrictionPlace-->"+restrictionPlace+"--position-->"+position);
		if(display!='none' && restrictionPlace<position && (bankName==null ||bankName=='')){
			$($($(this).children().get(6)).children().get(0)).hide();
		}else if(display=='none' && restrictionPlace==position){
			$($($(this).children().get(6)).children().get(0)).show();
		}
		position=Number(position)+1;
	});
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractagreement"/></div>
	 		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
	 		<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
	 		<div style="display:none;" class="tempresultfinal">
				
			</div>
		 	<form id="contractAgreementAdd" style="position: relative;">
		 		<fieldset style="padding-left: 10px;">
		 			<div class="form_head"><fmt:message key="re.contract.offerContractDetails"/></div>
		 			<div class="width33 float-left" id="hrm">
		 				<fieldset style="min-height:130px;">
					 		<div style="display:none;">
								<label>Person Id</label><input type="text" name="sessionPersonId" value="${PERSON_ID}" class="width50" id="sessionPersonId" disabled="disabled">
								<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
								<input type="hidden" name="renewalFlag" id="renewalFlag" value="${bean.renewalFlag}"/>
								
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.offerno"/><span style="color:red">*</span></label>	 
								<input class="validate[required] width50" type="text" name="offerNumber" value="${requestScope.OFFER_INFO.offerNumber }" id="offerNumber" disabled="disabled" >
								<input class="width50" type="hidden" name="offerId" value="${requestScope.OFFER_INFO.offerId }"  id="offerId" disabled="disabled" >
								<span class="button" style="width: 40px ! important; cursor: pointer;">
									<a class="btn ui-state-default ui-corner-all offer-popup"> 
										<span class="ui-icon ui-icon-newwin"> </span> 
									</a>
								</span>
							</div>
							
							<div>
								<label class="width30"><fmt:message key="re.contract.tenantname"/></label>	 
								<input class="masterTooltip width50" type="text" name="tenantName" value="${requestScope.OFFER_INFO.tenantName }"  id="tenantName" readonly="readonly" >
								<input class="width50" type="hidden" name="tenantId" value="${requestScope.OFFER_INFO.tenantId }"  id="tenantId" >
								<input class="width50" type="hidden" name="tenantGroupTitle" id="tenantGroupTitle" >
								<input class="width50 " id="rentFees" type="hidden" name="rentFees" value="${OFFER_INFO.rentFees }"  disabled="disabled">
								<input class="width40 " id="rentFeesAmount" type="hidden" name="rentFeesAmount" value="${OFFER_INFO.rentAmount }"  disabled="disabled">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.contract.address"/></label>	 
								<input class="masterTooltip width50" type="text" name="presentAddress" value="${requestScope.OFFER_INFO.presentAddress }"  id="presentAddress" readonly="readonly" >
							</div>
							<%-- 	<div>
								<label class="width30"><fmt:message key="re.contract.address1"/></label>	 
								<input class="width50" type="text" name="permanantAddress" value="${OFFER_INFO.permanantAddress }"  id="permanantAddress" disabled="disabled" >
							</div> --%>
							<div>
								<label class="width30"><fmt:message key="re.property.info.propertyName"/></label>	 
								<input class="masterTooltip width50" title="" type="text" name="propertyName"  id="propertyName" value="${requestScope.OFFER_INFO.propertyName}" readonly="readonly" >
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.componentName"/></label>	 
								<input class="masterTooltip width50" title="" type="text" name="componentName" id="componentName" value="${requestScope.OFFER_INFO.componentName}" readonly="readonly" >
							</div>
						</fieldset>
					</div>
		 		
					<div class="width33 float-left" id="hrm">
						<fieldset style="min-height:130px;">
							<div><label class="width40" id="fromDateCheck"><fmt:message key="re.contract.fromdate"/><span style="color:red">*</span></label>
							<input type="text" class="width50 dateCheck validate[required]" id="startPicker" value="${requestScope.OFFER_INFO.fromDate }" name="employerFromDate"  readonly="readonly"  disabled="disabled">
							</div>
							
							<div><label class="width40"><fmt:message key="re.contract.todate"/><span style="color:red">*</span></label>
							<input type="text" class="width50 employerToDate dateCheck validate[required]" id="endPicker" value="${requestScope.OFFER_INFO.toDate }" name="employerToDate" readonly="readonly"  disabled="disabled">
							</div>
							<div><label class="width40"><fmt:message key="re.contract.years"/></label><input type="text" class="width50" name="years" id="years" disabled="disabled"></div>
						<div><label class="width40"><fmt:message key="re.contract.months"/></label><input type="text" class="width50" name="months" id="months" disabled="disabled"></div>
						<div><label class="width40"><fmt:message key="re.contract.days"/></label><input type="text" class="width50" name="days" id="days"  disabled="disabled"></div>
						</fieldset>
					</div>
					<div class="width33 float-left" id="hrm">
		 				<fieldset style="min-height:130px;">
							<div >
								<label class="width40"><fmt:message key="re.contract.contractno"/></label>	 
								<input class="width50" type="text" name="contractNumber" value="${bean.contractNumber }"  id="contractNumber" disabled="disabled" >
								<input class="width50" type="hidden" name="contractId" value="${bean.contractId }"  id="contractId" disabled="disabled" >
							</div>
							<div >
								<label class="width40"><fmt:message key="re.contract.contractdate"/><span style="color:red">*</span></label>	 
								<input class="width50 contractDate validate[required]" type="text" name="contractDate" value="${bean.contractDate }"  id="defaultPopup" readonly="readonly" >
							</div>
							
							<div style="display:none;">
								<label class="width40"><fmt:message key="re.contract.paymentmethod"/><span style="color:red">*</span></label>	
								<input type="hidden" class="width50" name="paymentMethodTemp"  value="${bean.paymentMethod}" id="paymentMethodTemp"> 
								<input class="float-left width10 " type="checkbox" name="" id="paymentModeCash">
								<span class="width20 float-left" >Cash</span>
								<input class="float-left width10 " type="checkbox"  name="" id="paymentModeCheque" >
								<span class="width20 float-left" >Cheque</span>
							</div>
							<div><label class="width40"><fmt:message key="re.contract.contractFee"/><span style="color:red">*</span></label>
							<input type="text" class="width50 validate[required,custom[number]]" id="contractAmount" value="${bean.contractAmount }" name="contractAmount" tabindex="1">
							</div>
							<div><label class="width40"><fmt:message key="re.contract.deposit"/><span style="color:red">*</span></label>
							<input type="text" class="width50 validate[required,custom[number]]" id="depositAmount" value="${bean.depositAmount }" name="depositAmount" tabindex="2">
							</div>
							<div>
								<label class="width40"><fmt:message key="re.contract.installments"/><span style="color:red">*</span></label>	 
								<input class="width50 validate[required,custom[number]]" type="text" name="noOfCheques" maxlength="1" value="${bean.noOfCheques }"  id="noOfCheques" tabindex="3">
							</div>
						</fieldset>	
					</div>
					
				</fieldset>
			</form>
			<div class="clearfix"></div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="border: none !important;"> 
					<form name="newFields2">
	 					<div class="form_head portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.rentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none"></div>	 
						<div class="portlet-content form_content_alignment"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm">
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.lineno"/></th> 
												<th class="width20"><fmt:message key="re.contract.propertyComponentUnit"/></th>
												<th class="width20" style=" width:5%;display:none;"><fmt:message key="re.contract.rentamount"/></th>
												<th class="width20"><fmt:message key="re.contract.contractamount"/></th>
												<th style="width:5%;display:none;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tab tabRent">
											<c:forEach items="${requestScope.OFFER_DETAIL_LIST}" var="result" varStatus="status" >
											 	<tr class="even"> 
													<td class="width20">${status.index+1}</td>
													<td class="width20">${result.flatNumber }</td>	
													<td class="width20 rentAmountCalc" style=" width:5%;display:none;">${result.rentAmount }</td> 
													<td class="width20 contractAmountCalc">${result.contractAmount }</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.contractRentId }" name="actualLineId" id="actualLineId"/>	
														<input type="hidden" name="actionFlag" value="U"/>
						              					<input type="hidden" name="tempLineId" value=""/>
													</td> 
													<td style=" width:5%;display:none;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:30%">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="rentAmountTotal" id="rentAmountTotal" class="width40" disabled="disabled" style="display:none">
							 	<input name="contractAmountTotal" id="contractAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
				</div>
			
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
						style="border: none !important;"> 
					<form name="newFields2">
	 					<div class="form_head portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.paymentdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content form_content_alignment">  
					 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value="${countSize3}" /> 
							<div id="hrm">
								<div id="hrm" class="hastable width100" > 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">									
											<tr>
												<th class="width20"><fmt:message key="re.contract.bankCash"/></th> 
												<th class="width20"><fmt:message key="re.contract.chequedate"/></th>
												<th class="width20"><fmt:message key="re.contract.chequeno"/></th>
												<th class="width20"><fmt:message key="re.contract.amount"/></th>
												<th class="width20"><fmt:message key="re.contract.feeType"/></th>
												<th style="width:5%;"><fmt:message key="re.contract.options"/></th>
											</tr> 
										</thead> 
										<tbody class="tabPayment" style="">	
										<c:choose>
										<c:when test="${fn:length(requestScope.CONTRACT_PAYMENT)gt 0}">
											<c:forEach items="${requestScope.CONTRACT_PAYMENT}" var="result3" varStatus="status1" >
											 	<tr class="even rowid"> 
											 	 	<c:choose>
											 	 		<c:when test="${(result3.chequeNumber eq null || result3.chequeNumber eq '') && result3.bankName ne null && result3.bankName ne ''}">
															<td class="width20">${result3.bankName}</td>
															<td class="width20">${result3.chequeDate }</td>	
															<td class="width20">-NA-</td> 
														</c:when>	
											 	 		<c:when test="${result3.chequeNumber ne null && result3.chequeNumber ne ''}">
															<td class="width20">${result3.bankName}</td>
															<td class="width20">${result3.chequeDate }</td>	
															<td class="width20">${result3.chequeNumber }</td> 
														</c:when>
														<c:otherwise>
															<td class="width20">Cash</td>
															<td class="width20">-NA-</td>	
															<td class="width20">-NA-</td> 
														</c:otherwise>
													</c:choose>
													<td class="width20 chequeAmountCalc">${result3.chequeAmount }</td>
													<td class="width20">${result3.feeType}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result3.contractPaymentId }" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="${status1.index+1}" name="lindId" id="lindId"/>	
														<input type="hidden" value="${result3.feeTypeId}" name="feeTypeId" id="feeTypeId"/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="display:none;cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
										  </c:when>
										  <c:otherwise>
										  		<c:forEach var="i" begin="1" end="2" step="1" varStatus ="status1" >
											 	<tr class="even rowid"> 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width20"></td> 
													<td class="width20 chequeAmountCalc"></td>
													<td class="width20"></td> 
													<td style="display:none"> 
														<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="${i}" name="lindId" id="lindId"/>	
														<input type="hidden" value="" name="feeTypeId" id="feeTypeId"/>	
													</td> 
													<td style=" width:5%;"> 
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="cursor:pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td> 
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>	
												</tr>
											</c:forEach>
										  </c:otherwise>
										  </c:choose>	
										</tbody>
									</table>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="float-right " style="width:33%">
								<label><fmt:message key="re.contract.total"/></label>
							 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
							</div>
							<div class="clearfix"></div>
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
						<div class="portlet-header ui-widget-header float-left addrowspayment"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			<div class="clearfix"></div>
			<form id="contractAgreementCash" style="display:none;">
				<div class="width60 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.contract.cashamount"/></legend>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.contract.cashamount"/></label>	 
							<input class="width30" type="text" name="cashAmount" value="${bean.cashAmount}"  id="cashAmount" disabled="disabled">
						</div>
						<div>
							<label class="width30" style="border-left: 9px margin-top:9px"><fmt:message key="re.property.info.description"/></label>	 
							<input class="width30" type="text" name="description"  id="description" value="${bean.description }" disabled="disabled">
						</div>
					</fieldset>
				</div>
			</form>
			<div class="clearfix"></div>
			<%-- <div class="width40 float-left" id="hrm">
				<fieldset style="min-height:140px;">
					<legend>Account Details</legend>
					<div style="margin-bottom: 6%;">
						<label class="width30">Contract Fee<span class="mandatory">*</span></label>	
						<span style="height: 20px;" class="float-left width48 feeDesc"></span>
						<span class="button" id="feecode"  style="position: relative; top:6px;">
							<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
						<c:choose> 
							<c:when test="${ACCOUNT_INFO eq 'FALSE'}">
								<input type="hidden" name="contractFee" id="contractFee"/>   
							</c:when> 
							<c:otherwise>
								<input type="hidden" name="contractFee" id="contractFee" value="${implemenation.contractFee}"/>  
							</c:otherwise>
						</c:choose> 
					</div>
					<div>
						<label class="width30">Contract Deposit<span class="mandatory">*</span></label> 
						<span style="height: 20px;" class="float-left width48 depositDesc"></span>
						 <span class="button" id="depositcode" style="position: relative; top:3px;">
							<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						 </span>  
						 <c:choose>  
							<c:when test="${ACCOUNT_INFO eq 'FALSE'}"> 
								<input type="hidden" name="contractDeposit" id="contractDeposit" value=""/> 
					 		</c:when>
					 		<c:otherwise>
					 			<input type="hidden" name="contractDeposit" id="contractDeposit" value="${implemenation.contractDeposit}"/>   
					 		</c:otherwise>	
					 	 </c:choose>	 
					</div>
				</fieldset>
			</div> --%>
		<form id="contractAgreementReceive">
			<div class="width100 float-left" id="hrm">
				<fieldset style="min-height:134px;">
					<legend><fmt:message key="re.contract.grossdetails"/></legend>
					<div style="display:none;">
						<label class="width30"><fmt:message key="re.contract.chequeamount"/></label>	 
						<input class="width30" type="text" name="chequeAmount"  id="chequeAmount" disabled="disabled" >
					</div>
					
					<div>
						<label class="width20" style="font-size: 11px ! important;display:none;">Total Cheque Payment</label>	 
						<input class="width20" type="text" name="chequeAmountTotalAll"  id="chequeAmountTotalAll" style="display:none;" disabled="disabled" >
					</div>
					<div>
						<label class="width20"><fmt:message key="re.contract.grandFees"/></label>	 
						<input class="width20 float-left" type="text" name="grandFees"  id="grandFees" disabled="disabled" >
						<label class="width20"><fmt:message key="re.contract.totalRent"/></label>	 
						<input class="width20 float-left" type="text" name="rentAmountTotalAll"  id="rentAmountTotalAll" disabled="disabled" >
					
						<label class="width20" style="display:none;"><fmt:message key="re.contract.totalCashAmount"/></label>	 
						<input class="width20" type="text" name="cashAmountTotal"  id="cashAmountTotal" disabled="disabled" style="display:none;">
					</div>
					<div>
						<label class="width20"><fmt:message key="re.contract.otherCharges"/></label>	 
						<input class="width20 float-left" type="text" name="otherCharges"  id="otherCharges" disabled="disabled" >
						<label class="width20"><fmt:message key="re.contract.grandtotal"/></label>	 
						<input class="width20" type="text" name="grandTotal"  id="grandTotal" disabled="disabled" >
					</div>
					<fieldset>
					<div>
						<label class="width30 float-left"><fmt:message key="re.contract.paymentRecievedBy"/><span style="color:red">*</span></label>
						<input type="hidden" name="paymentReceivedTemp" id="paymentReceivedTemp" value="${bean.paymentReceived}"/>
						<select id="paymentReceived"  class="width50 validate[required] float-left" >
							<option value="">--Select--</option>
							<c:forEach items="${PERSON_LIST}" var="per" varStatus="status">
								<c:choose>
								<c:when test="${bean.paymentReceived eq per.personId}">
									<option value="${per.personId}" selected="selected">${per.firstName} ${per.lastName}</option>
								</c:when>
								<c:otherwise>
									<option value="${per.personId}">${per.firstName} ${per.lastName}</option>
								</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div>
						<label class="width30 float-left"><fmt:message key="re.contract.purposeOfleasing"/>:</label>
						<textarea class="float-left" id="leasedFor" rows="2" cols="6" style="max-height: 50px; min-height: 50px; max-width: 50%; min-width: 50%;">${bean.leasedFor}</textarea>
					</div>
					<div>
						<label class="width30 float-left"><fmt:message key="re.contract.remarks"/></label>
						<textarea class="float-left" id="remarks" rows="2" cols="6" style="max-height: 50px; min-height: 50px; max-width: 50%; min-width: 50%;">${bean.remarks}</textarea>
					</div>
					</fieldset>
					<fieldset style="height: 55px;">
						<div id="dms_document_information" style="cursor: pointer; color: blue;">
							<u><fmt:message key="re.property.info.uploadDocsHere" /></u>
						</div>
						<div style="padding-top: 10px; margin-top:3px; height: 38px; overflow: auto;">
							<span id="contractDocs"></span>
						</div>	
					</fieldset>
					<div style="display:none;">
						<label class="width30"><fmt:message key="re.contract.amountInWords"/></label>
					</div>
				</fieldset>
			</div>
		</form>	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div style="display:none;cursor:pointer;" class="portlet-header ui-widget-header float-right" id="close"><fmt:message key="re.property.info.close"/></div>
			<div class="portlet-header ui-widget-header float-right cancelrec" id="discard" style=" cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div><!--
			<div class="portlet-header ui-widget-header float-right" id="request" style="cursor:pointer;"><fmt:message key="cs.common.button.requestforverification"/></div> 
			--><div class="portlet-header ui-widget-header float-right headerData" id="save" style="cursor:pointer;"><fmt:message key="re.owner.info.save"/></div> 
		</div>
	</div>
</div>

<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="contract-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="contract-result-pop"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div>
</div>