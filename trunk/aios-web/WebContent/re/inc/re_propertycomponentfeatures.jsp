<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			component features
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="componentstypes" class="autoComplete_feature width50">
	     	<option value=""></option>
	     	<c:forEach items="${componentfeatureList}" var="bean" varStatus="status">
	     		<option value="${bean.componentFeature}">${bean.componentFeatureDetails}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 unitfeature_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width48 float-left"><span>Features</span></li> 
   				<li class="width48 float-left"><span>Features Details</span></li>
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${componentfeatureList}" var="bean" varStatus="status">
		     		<li id="unitfeature_${status.index+1}" class="unitfeaturelist_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.componentFeature}" id="unitfeaturebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.componentFeatureDetails}" id="unitfeaturebynameid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.lookupDetailId}" id="lookupdetailsid_${status.index+1}"> 
		     			<span style="cursor: pointer;" class="float-left width48">${bean.componentFeature}</span> 
		     			<span id="unitfeaturebyname_${status.index+1}" style="cursor: pointer;" class="float-right width48">${bean.componentFeatureDetails}
		     				<span id="unitfeatureselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="closeline" style="cursor:pointer;">close</div>  
	</div>
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){ 
	$($($('#componet_featurepop').parent()).get(0)).css('width',500);
	$($($('#componet_featurepop').parent()).get(0)).css('height',250);
	$($($('#componet_featurepop').parent()).get(0)).css('padding',0);
	$($($('#componet_featurepop').parent()).get(0)).css('left',0);
	$($($('#componet_featurepop').parent()).get(0)).css('top',100);
	$($($('#componet_featurepop').parent()).get(0)).css('overflow','hidden');
	$('#componet_featurepop').dialog('open');
	$('.autoComplete_feature').combobox({ 
	       selected: function(event, ui){ 
	    	   $('#componentFeature').val($(this).val());
		       $('#componentFeatures').val($(".autoComplete_feature option:selected").text());
		       $('#componet_featurepop').dialog('close'); 
	   } 
	}); 
	$('.unitfeaturelist_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid=""; 
		$('.unitfeaturelist_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#unitfeatureselect_'+rowid).hasClass('selected')){ 
				 $('#unitfeatureselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#unitfeatureselect_'+rowid).addClass('selected');
	});
	
	$('.unitfeaturelist_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#componentFeature').val($('#unitfeaturebyid_'+rowid).val());
		 $('#componentFeatures').val($('#unitfeaturebynameid_'+rowid).val());  
		 $('#lookupDetailId').val($('#lookupdetailsid_'+rowid).val()); 
		 $('#componet_featurepop').dialog('close');
	});
	$('#closeline').click(function(){
		$('#componet_featurepop').dialog('close');
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.unitfeature_list ul li{
		padding:3px;
	}
	.unitfeature_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>