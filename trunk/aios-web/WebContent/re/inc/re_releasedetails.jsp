<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
.extrapad{
padding:10px!important;
}
</style>

<script type="text/javascript">


$(function (){  
	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });	
});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
  var daysInMonth =$.datepick.daysInMonth(
  $('#selectedYear').val(), $('#selectedMonth').val());
  $('#selectedDay option:gt(27)').attr('disabled', false);
  $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
  if ($('#selectedDay').val() > daysInMonth) {
      $('#selectedDay').val(daysInMonth);
  }
} 
function customRange(dates) {
if (this.id == 'birthdate') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#endPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
  



<div id="main-content">
				 
				 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Release Form</div>
					<div class="portlet-content"> 
					<fieldset>
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend>Release Information 2</legend>	
									<div><label>Date</label><input type="text" name="sno" class="width30 tooltip" id="birthdate"></div>					
									<div><label>Tenant No.</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Tenant Name</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>From Date</label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
									<div><label>To Date</label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>																
									<div><label>Water Certifi. No.<span style="color:red">*</span></label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" ></div>
									<div><label>Water Certifi. Date</label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset style="width:479px;">
								<legend>Release Information 1</legend>
									<div><label>Contract No.<span style="color:red">*</span></label>
											<select value="" class="width30 tooltip" >
											<option>-Select-</option>
											<option>123</option>
											<option>420</option>
											</select>
									</div>	
									<div><label>Building No.</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Building Name</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Elec. Certfi. No.<span style="color:red">*</span></label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" ></div>
									<div><label>Elec. Certfi. Date</label><input type="text" name="sno" class="width30 tooltip" id="birthdate" title="Select Birth Date" readonly="readonly"></div>		
									<div><label>Tenant Release Letter No.<span style="color:red">*</span></label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" ></div>
									<div style="width:364px;"><label style="margin-right:20px;">Tenant Release Letter Date <span style="color:red">*</span></label><input type="text" name="drno" class="width40 tooltip" title="Enter Tenant Number" ></div>
							</fieldset> 
							
							
						</div>
				
					</fieldset>
						<form name="newFields">
		 				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Rental Details</div>
							 
								<div id="" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th class="width10">Line No.</th>
				            					<th class="width20">Flat Number</th>
				            					<th class="width20">Rent</th>
				            					<th class="width10">Paid Amount</th>
				            					<th class="width10">Balance</th>
											</tr>
										</thead>  
										<tbody class="tab" style="">	
											 <%for(int i=0;i<=2;i++) { %>
												 <tr>  
													<td class="width10 extrapad "></td>	 
													<td class="width20 extrapad "></td>
													<td class="width20 extrapad"></td>	
													<td class="width10 extrapad"></td> 
													<td class="width10 extrapad "></td>
													<td style="display:none"> 
														<input type="hidden" name="tempId" value="" id="tempId"/>
														<input type="hidden" name="curIdVal" value="" id="curIdVal"/>	
													</td>
													<td style="display:none;"></td>		 
												</tr>  
									  		 <%}%>
										</tbody>
			 						</table>
										<div class="iDiv" style="display: none;">
										</div>
									</div>
									<div class="vGrip">
										<span></span>
									</div>
					     
			                <div class="float-right width45">	
							</div> 
							<fieldset>
								<div class="float-right width45">
									<label class="width20">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">
									<label class="width20">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">							
									<label class="width20">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">							
								</div>
							</fieldset>
					</form>
						<form name="newFields">
		 				<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Fee Details</div>
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
			 							<thead class="chrome_tab">
											<tr>  
				            					<th class="width10">Line No.</th>
				            					<th class="width20">Fee ID</th>
				            					<th class="width20">Description</th>
				            					<th class="width10">Fee Amount</th>
				            					<th class="width10">Paid Amount</th>
				            					<th class="width10">Balance</th>			            					
				            					<th style="width:5%;">Options</th>
											</tr>
										</thead>  
										<tbody class="tab" style="">	
											 <%for(int i=0;i<=2;i++) { %>
												 <tr>  
													<td class="width10"></td>	 
													<td class="width20"></td>
													<td class="width20"></td>	
													<td class="width10"></td> 
													<td class="width10"></td> 
													<td class="width10"></td>
													<td style="display:none"> 
														<input type="hidden" name="tempId" value="" id="tempId"/>
														<input type="hidden" name="curIdVal" value="" id="curIdVal"/>	
													</td> 
													<td style="width:5%;" style=float-right> 
					  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" style="cursor:pointer;" title="Add Record">
					 	 									<span class="ui-icon ui-icon-plus"></span>
													   	</a>	
													   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  style="display:none;cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
														</a> 
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;"></td>		 
												</tr>  
									  		 <%}%>
										</tbody>
			 						</table>
								
									</div>
					</form>
					<fieldset>
					<div class="float-right width45">
						<label class="width10">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">
						<label class="width10">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">							
						<label class="width10">Total</label><input type="text" name="sno" class="width20 tooltip" title="Enter Tenant Name">							
					</div>
				 	</fieldset>
						<div id="hrm" class="width100"> 
			 				<fieldset class="width50" style="margin:0 auto">
								<legend>Final Calculation</legend>						
									<div><label>Due Amount</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Deposit Amount</label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" readonly="readonly"></div>
									<div><label>Payable Amount</label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>																
									<div><label>Paid Amount<span style="color:red">*</span></label><input type="text" name="drno" class="width30 tooltip" title="Enter Tenant Number" ></div>
									<div><label>Balance Amount</label><input type="text" name="sno" class="width30 tooltip" title="Enter Designation" readonly="readonly"></div>																
							</fieldset> 
						</div>
						<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:0 auto" style="margin:10px ;"> 
								<div class="portlet-header ui-widget-header float-right cancelrec" style=" cursor:pointer;">Cancel</div> 
								<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;">Print</div>
								<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;">Accept</div>
							</div>
					</div>
				</div>
				</div>
			</div>
		</div>
</div>