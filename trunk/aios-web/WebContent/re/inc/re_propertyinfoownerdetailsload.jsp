<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="re.property.info.owners" />
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="components" class="autoComplete_owner width50">
	     	<option value=""></option>
	     	<c:forEach items="${itemList}" var="bean" varStatus="status">
	     		<option value="${bean.ownerId}@${bean.ownerTypeId}@${bean.uaeNo}@${bean.ownerType}">${bean.ownerName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 propertyowner_list" style="padding:4px;"> 
     	<ul>
     		<li class="width48 float-left"><span><fmt:message key="re.property.info.ownerName" /></span></li> 
   			<li class="width48 float-left"><span><fmt:message key="re.property.info.ownerType" /></span></li> 
   			<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
     		<c:forEach items="${itemList}" var="bean" varStatus="status">
	     		<li id="property_${status.index+1}" class="propertyownerlist_li" style="height: 20px;">
	     			<input type="hidden" value="${bean.ownerId}" id="ownerIdbyid_${status.index+1}">
	     			<input type="hidden" value="${bean.ownerTypeId}" id="ownerTypeIdbyid_${status.index+1}"> 
	     			<input type="hidden" value="${bean.uaeNo}" id="uaeNobyid_${status.index+1}"/>
	     			<input type="hidden" value="${bean.ownerName}" id="ownerNamebynameid_${status.index+1}"> 
	     			<input type="hidden" value="${bean.ownerType}" id="ownerTypebynameid_${status.index+1}"> 
	     			<span id="ownerbyname_${status.index+1}" style="cursor: pointer;" class="float-left width48">${bean.ownerName} </span>   
	     			<span style="cursor: pointer;" class="float-left width48">${bean.ownerType}
	     				<span id="ownerselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
	     			</span>  
	     		</li>
	     	</c:forEach> 
     	</ul> 
	</div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="close_owners" style="cursor:pointer;">close</div>  
	</div>	
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){ 
	$($($('#property-popup').parent()).get(0)).css('width',500);
	$($($('#property-popup').parent()).get(0)).css('height',250);
	$($($('#property-popup').parent()).get(0)).css('padding',0);
	$($($('#property-popup').parent()).get(0)).css('left',0);
	$($($('#property-popup').parent()).get(0)).css('top',100);
	$($($('#property-popup').parent()).get(0)).css('overflow','hidden');
	$('#property-popup').dialog('open');
	$('.autoComplete_owner').combobox({ 
	       selected: function(event, ui){ 
    	   $('#componentTypeId').val($(this).val());
    	   var ownerdetails=$(this).val();
    	   var ownerarray=ownerdetails.split("@");
    	   $('#ownerId').val(ownerarray[0]);
    	   $('#ownerTypeId').val(ownerarray[1]);
    	   $('#uaeNo').val(ownerarray[2]);
    	   $('#ownerType').val(ownerarray[3]);
	       $('#ownerName').val($(".autoComplete_owner option:selected").text());
	       $('#property-popup').dialog("close"); 
	   } 
	}); 
	$('.propertyownerlist_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.propertyownerlist_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);  
			 if($('#ownerselect_'+rowid).hasClass('selected')){ 
				 $('#ownerselect_'+rowid).removeClass('selected');
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#ownerselect_'+rowid).addClass('selected');
	});
	
	$('.propertyownerlist_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#ownerId').val($('#ownerIdbyid_'+rowid).val());
		 $('#ownerTypeId').val($('#ownerTypeIdbyid_'+rowid).val());
		 $('#uaeNo').val($('#uaeNobyid_'+rowid).val());
		 $('#ownerName').val($('#ownerNamebynameid_'+rowid).val());
		 $('#ownerType').val($('#ownerTypebynameid_'+rowid).val());
		 $('#property-popup').dialog("close"); 
	});
	$('#close_owners').live('click',function(){ 
		$('#property-popup').dialog("close"); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
		position: absolute!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.propertyowner_list ul li{
		padding:3px;
	}
	.propertyowner_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>