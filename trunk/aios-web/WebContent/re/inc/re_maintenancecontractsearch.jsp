<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
$(document).ready(function (){
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-property_contract_search.action?addressLine1="+addressLine1+'&purchaseOrderId ='+purchaseOrderId+'& fromDate ='+fromDate+'& toDate ='+toDate+'& contractDate ='+contractDate+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['MAINTENANCE CONTRACT ID',
		  		 	'PURCHASE ORDER ID',
		  		 	'<fmt:message key="re.contract.purchaseorderno"/>',
		  		 	'<fmt:message key="re.contract.contractdate"/>',
		  		 	'<fmt:message key="re.contract.securitydeposit"/>',
		  		 	'<fmt:message key="re.contract.addressline1"/>',
		  		 	'<fmt:message key="re.contract.fromdate"/>',
		  		 	'<fmt:message key="re.contract.todate"/>',
		  		 	'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'maintenanceId',index:'MAINTENANCE_CONTRACT_ID', width:100,  sortable:true},
				{name:'purchaseOrderId',index:'PURCHASE_ORDER_ID', width:100,  sortable:true},
				{name:'purchaseOrderNo',index:'PO_PO_NUMBER', width:100,  sortable:true},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'securityDeposit',index:'SECURITY_DEPOSIT', width:100,  sortable:true},
		  		 {name:'addressLine1',index:'ADDRESS_LINE1', width:150,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'toDate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true},
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'MAINTENANCE_CONTRACT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"Maintenance Contract"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["maintenanceId","purchaseOrderId"]);
 	});
</script>
 </head>
 <body>
<input type="hidden" id="bankName"/> 
<table id="list2"></table>  
<div id="pager2"> </div> 
</body>