<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-tenent_type_search.action?descriptions="+descriptions+'&noOfDaysAllowLate ='+noOfDaysAllowLate+'& offerValidityDays ='+offerValidityDays+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['Tenant TypeId','<fmt:message key="re.property.info.tenanttypedescription"/>','<fmt:message key="re.property.info.noofdaysallowlate"/>','<fmt:message key="re.property.info.offervaliditydays"/>','<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'tenantTypeId',index:'TENANT_TYPE_ID', width:100,  sortable:true},
				{name:'descriptions',index:'DESCRIPTION', width:100,  sortable:true},
		  		{name:'noOfDaysAllowLate',index:'NO_OF_DAYS_ALLOW_LATE', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'offerValidityDays',index:'OFFER_VALIDITY_DAYS', width:100,  sortable:true},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:100,sortable:true, sorttype:"data"},
		  ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'TENANT_TYPE_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.property.info.tenanttypeinformation"/>'
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["tenantTypeId"]);
});
 	</script>
	 <body>
	<input type="hidden" id="bankName"/>                              
	<table id="list2"></table>  
	<div id="pager2"> </div> 
	</body> 