<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;
var customerName;
var supplierName;
var supplierSiteName;
$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-contract_agreement_search.action?buildingId="+buildingId+'&contractNumber ='+contractNumber+'& tenantId ='+tenantId+'& contractDate ='+contractDate+'& fromDate ='+fromDate+'&toDate ='+toDate+'&grandTotal ='+grandTotal+'&approvedStatus='+approvedStatus,
		 datatype: "json", 
		 colNames:['Contract ID','<fmt:message key="re.contract.contractno"/>','Offer ID','Building ID',
			  		 '<fmt:message key="re.contract.buildingname"/>','Tenant ID','<fmt:message key="re.contract.tenantname"/>',
			  		 '<fmt:message key="re.contract.contractdate"/>','<fmt:message key="re.contract.fromdate"/>',
			  		 '<fmt:message key="re.contract.todate"/>','<fmt:message key="re.contract.amount"/>',
			  		'<fmt:message key="cs.common.button.workflowstatus"/>'], 
		 colModel:[ 
				{name:'contractId',index:'CONTRACT_ID', width:100,  sortable:true},
				{name:'contractNumber',index:'CONTRACT_NO', width:50,  sortable:true},
		  		{name:'offerId',index:'OFFER_ID', width:150,sortable:true, sorttype:"data"} ,
		  		{name:'buildingId',index:'BUILDING_ID', width:100,  sortable:true},
		  		 {name:'buildingName',index:'BUILDING_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'tenantId',index:'CUSTOMER_ID', width:150,sortable:true, sorttype:"data"},
		  		{name:'tenantName',index:'TENANT_NAME', width:100,sortable:true, sorttype:"data"},
		  		{name:'contractDate',index:'CONTRACT_DATE', width:50,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'FROM_DATE', width:50,sortable:true, sorttype:"data"},
		  		{name:'toDate',index:'TO_DATE', width:50,sortable:true, sorttype:"data"},
		  		{name:'grandTotal',index:'AMOUNT', width:50,sortable:true, sorttype:"data"},
		  		{name:'workflowStatus',index:'WORKFLOW_STATUS', width:150,sortable:true}
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'CONTRACT_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"Contract Agreement"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["offerId","contractId","buildingId","tenantId"]);

 	});
 	</script>
 	
 	 <body>
 	 
 	<input type="hidden" id="bankName"/> 
 	<table id="list2"></table>  
 	<div id="pager2"> </div> 
 	</body>