<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="7" class="tdidentity">
<div id="errMsg"></div>
<form name="releaseInfoFeeAddEntry" id="releaseInfoFeeAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.property.info.amountdetails"/></legend>
		<div>
			<label><fmt:message key="re.property.info.feeamount"/><span style="color:red">*</span></label>
			<input type="text" name="feeAmount" id="feeAmount" class="width50 checkBalance validate[required,custom[onlyFloat]]">
		</div>
		<div style="display:none;"> 
			<label>Paid Amount<span style="color:red">*</span></label>
			<input type="text" name="paidAmount" id="paidAmount" class="width50 checkBalance validate[required,custom[onlyFloat]]">
		</div>
		<div style="display:none;">
			<label><fmt:message key="re.property.info.balanceamount"/></label>
			<input type="text" name="balanceAmount" id="balanceAmount" class="width50" disabled="disabled">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
	<fieldset>
		<legend><fmt:message key="re.property.info.linesdetails"/></legend> 
		<div>
			<label><fmt:message key="re.property.info.lineno"/><span style="color:red">*</span></label>
			<input type="text" name="lineNo" id="lineNo" class="width50 validate[required,custom[onlyNumber]]" />
		</div>
		<div>
			<label><fmt:message key="re.property.info.feeid"/><span style="color:red">*</span></label>
			<input type="text" name="feeId" id="feeId" class="width50 validate[required,custom[onlyNumber]]" />
		</div>
		<div>
		<label><fmt:message key="re.property.info.description"/></label>
			<input type="text" name="feeDescription" id="feeDescription" class="width50" >
		</div>
	</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#releaseInfoFeeAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(7)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(7)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(7)).children().get(3)).hide();	//Processing Button	 		
		 });
		 $('#add').click(function(){
			  if($('#releaseInfoFeeAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var feeId=$('#feeId').val();
					var feeDescription=$('#feeDescription').val();
					var feeAmount=$('#feeAmount').val();
					var paidAmount=$('#paidAmount').val();
					var balanceAmount=$('#balanceAmount').val();
									 
					var trnVal =$('#transURN').val(); 
					var releaseId=$('#releaseId').val();  	// Master Primary Key
					var actionFlag=$($($(slidetab).children().get(6)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(6)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_release_fee_save.action", 
					 	async: false,
					 	data: {lineNumber: lineNo,feeId: feeId,feeDescription: feeDescription,feeAmount: feeAmount,paidAmount: paidAmount, 
							trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag,releaseId:releaseId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(feeId);
		        		$($(slidetab).children().get(2)).text(feeDescription); 
		        		$($(slidetab).children().get(3)).text(feeAmount); 
		        		$($(slidetab).children().get(4)).text(paidAmount); 
		        		$($(slidetab).children().get(5)).text(balanceAmount);

		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(7)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(7)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(7)).children().get(3)).hide();	//Processing Button
	
						$($($(slidetab).children().get(6)).children().get(2)).val($('#objTempVal').html()); 
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCountFee=Number($('#childCountFee').val());
				  		childCountFee=childCountFee+1;
						$('#childCountFee').val(childCountFee);
				   } 
			  }else{
				  return false;
			  }
		});
		$('.checkBalance').blur(function(){
			feeAmount=$('#feeAmount').val();
			paidAmount=$('#paidAmount').val();
			if(feeAmount != '' && paidAmount !='' ){
				balanceAmount=Number(feeAmount) - Number(paidAmount);
				$('#balanceAmount').val(balanceAmount);
			}
		});

});
 </script>	