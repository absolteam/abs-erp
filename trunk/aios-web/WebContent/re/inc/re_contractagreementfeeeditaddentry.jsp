<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="contractAgreementFeeAddEntry" id="contractAgreementFeeAddEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>					 
		<legend><fmt:message key="re.contract.feedetails"/></legend>
		<div>
			<label><fmt:message key="re.contract.description"/></label>
			<input type="text" name="feeDescription" id="feeDescription" class="width50 " disabled="disabled">
		</div>
		<div>
			<label><fmt:message key="re.contract.amount"/><span style="color:red">*</span></label>
			<input type="text" name="feeAmount" id="feeAmount" class="width50  validate[required,custom[onlyFloat]]">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.contract.linesdetails"/></legend> 
			<div>
				<label><fmt:message key="re.contract.lineno"/><span style="color:red">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50 validate[required,custom[onlyNumber]]" />
			</div>
			<div>
				<label><fmt:message key="re.contract.feeid"/><span style="color:red">*</span></label>
				<input type="text" name="feeId" id="feeId" class="width50 validate[required]" disabled="disabled"/>
				<span id="hit7" class="button" style="width: 40px ! important;">
					<a class="btn ui-state-default ui-corner-all fee-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.contract.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.contract.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#contractAgreementFeeAddEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			  $('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(0)).show();	//Add Button
			$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
			$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button	 
			openFlag=0;		
		 });
		 
		 $('#add').click(function(){
			  if($('#contractAgreementFeeAddEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var feeId=$('#feeId').val();
					var feeDescription=$('#feeDescription').val();
					var feeAmount=$('#feeAmount').val();
									 
					var trnVal =$('#transURN').val(); 

					var contractId=$('#contractId').val();  
					var actionFlag=$($($(slidetab).children().get(4)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(4)).children().get(2)).val();
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_add_contract_fee_save.action", 
					 	async: false,
					 	data: {contractId: contractId, lineNumber: lineNo,feeId: feeId,feeDescription: feeDescription,feeAmount: feeAmount, 
							trnValue:trnVal,tempLineId:tempLineId,actionFlag: actionFlag},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(feeId);
		        		$($(slidetab).children().get(2)).text(feeDescription); 
		        		$($(slidetab).children().get(3)).text(feeAmount); 

		        		//$($(slidetab).children().get(4)).text(remarks);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(4)).children().get(2)).val($('#objTempVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
	
				  		var childCountFee=Number($('#childCountFee').val());
				  		childCountFee=childCountFee+1;
						$('#childCountFee').val(childCountFee);

						$.fn.globeTotalFee();  //For Total Amount of Fee
				   } 
			  }else{
				  return false;
			  }
		});

		 $('.fee-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#contract-popup').dialog('open'); 
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/contract_agreement_load_fee.action",
				 	async: false, 
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.contract-result').html(result); 
					},
					error:function(result){ 
						 $('.contract-result').html(result); 
					}
				}); 
			});
		 
		$('#contract-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});

});
 </script>	