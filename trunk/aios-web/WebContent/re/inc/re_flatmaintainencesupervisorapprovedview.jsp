<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var openFlag=0;
var slidetab;
var actualID;
$(function(){
	$('#contractId').val($('#contractIdTemp').val());
	$('#personId').val($('#personIdTemp').val());
	$('#ebBillStatusCode').val($('#ebBillStatusCodeTemp').val());
	$('#waterBillStatusCode').val($('#waterBillStatusCodeTemp').val());
	$('#releaseType').val($('#releaseTypeTemp').val());
	
	if($('#waterCertiDate').val() =='01-Jan-1900'){
		$('#waterCertiDate').val("");
	}
	if($('#defaultPopup').val() =='01-Jan-1900'){
		$('#defaultPopup').val("");
	}

	$('#close').click(function(){
	   	 $('.formError').remove();
	   		$('#loading').fadeIn();
			 $.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/flat_maintainence_list.action",   
			     	async: false, 
					dataType: "html",
					cache: false,
					error: function(data) 
					{  
					},
			     	success: function(data)
			     	{
						$("#main-wrapper").html(data);  //gridDiv main-wrapper
						$('#loading').fadeOut();
			     	}
				});
			return true;
	 });

});		
		
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.flatmaintenancerelease"/></div>
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	
				<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
		    	<input type="hidden" id="notificationType" name="notificationType" value="${requestScope.notificationType}"/>
				<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
				<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
				<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
		
	 		<div class="portlet-content">
	 			<form id="flatMaintainenceReleaseEdit">
					<fieldset>
			 			<div class="width50 float-left" id="hrm">
							<fieldset style="min-height:100px;">
							<div style="display:none">
								<label>Session Person Id</label><input type="text" name="sessionpersonId" value="${bean.sessionpersonId }" class="width40" id="sessionpersonId" disabled="disabled">
							</div>
								<div>
									<label><fmt:message key="re.property.info.contractnumber"/></label>	
									 <input type="hidden" name="contractIdTemp" id="contractIdTemp" value="${bean.contractId }"/>
										<select id="contractId" class="width30" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.item ne null}">
												<c:forEach items="${requestScope.item}" var="contract" varStatus="status">
													<option value="${contract.contractId }">${contract.contractNo }--${contract.buildingName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
							 	<div>
									<label><fmt:message key="re.property.info.engineername"/></label>	
									 <input type="hidden" name="personIdTemp" id="personIdTemp" value="${bean.personId }" />
										<select id="personId" class="width30 validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.itemList ne null}">
												<c:forEach items="${requestScope.itemList}" var="person" varStatus="status">
													<option value="${person.personId }">${person.personName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<fieldset>
				                    <legend><fmt:message key="re.property.info.description"/></legend>
				                    <div style="height:25px" class="width60 float-left">
				                            <div id="barbox">
				                                  <div id="progressbar"></div>
				                            </div>
				                            <div id="count">500</div>
				                      </div>
				                      <p>
				                        <textarea class="width60 float-left tooltip" disabled="disabled" id="flatDescription" title="PLease Enter Description">${bean.flatDescription }</textarea>
				                      </p>
				                 </fieldset>
							</fieldset>
					 	</div>
					 	<div class="width50 float-right" id="hrm">
							<fieldset style="min-height:100px;">
								<div>
									<label><fmt:message key="re.property.info.watercertifino"/></label>
									<input type="text" name="waterCertiNo" id="waterCertiNo" disabled="disabled" class="width30 validate[required]" value="${bean.waterCertiNo }" >
								</div>
								<div>
									<label><fmt:message key="re.property.info.watercertifidate"/></label>
									<input type="text" name="waterCertiDate" class="width30" id="waterCertiDate" disabled="disabled" value="${bean.waterCertiDate }">
								</div>
								<div>
									<label>Water Bill Status</label>	
										<input type="hidden" name="waterBillStatusCodeTemp" id="waterBillStatusCodeTemp" value="${bean.waterBillStatusCode }" />
										<select id="waterBillStatusCode" class="width30 validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanStatus ne null}">
												<c:forEach items="${requestScope.beanStatus}" var="beanstatus" varStatus="status">
													<option value="${beanstatus.statusCode }">${beanstatus.statusName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<div>
									<label>Supervisor Comments</label>
									<input type="text" name="waterBillComments" id="waterBillComments" disabled="disabled" class="width30 validate[required]" value="${bean.waterBillComments }">
								</div>	
								<div>
									<label><fmt:message key="re.property.info.eleccertifino"/></label>
									<input type="text" name="electricCertiNo" id="electricCertiNo" disabled="disabled" class="width30" value="${bean.electricCertiNo }">
								</div>
								<div>
									<label><fmt:message key="re.property.info.eleccertifidate"/></label>
									<input type="text" name="electricCertiDate" id="defaultPopup" class="width30" disabled="disabled" value="${bean.electricCertiDate }">
								</div>	
								<div>
									<label>EB Bill Status</label>	
										<input type="hidden" name="ebBillStatusCodeTemp" id="ebBillStatusCodeTemp" value="${bean.ebBillStatusCode }" />
										<select id="ebBillStatusCode" class="width30 validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanStatus ne null}">
												<c:forEach items="${requestScope.beanStatus}" var="beanstatus1" varStatus="status">
													<option value="${beanstatus1.statusCode }">${beanstatus1.statusName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
								<div>
									<label>Supervisor Comments</label>
									<input type="text" name="ebBillComments" id="ebBillComments" disabled="disabled" class="width30 validate[required]" value="${bean.ebBillComments }">
								</div>	
								<div>
									<label><fmt:message key="re.property.info.releasetype"/></label>
									<input type="hidden" name="releaseTypeTemp" id="releaseTypeTemp" value="${bean.releaseType }" />
									<select id="releaseType" class="width30" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.releaseStatus ne null}">
												<c:forEach items="${requestScope.releaseStatus}" var="beanstatus1" varStatus="status">
													<option value="${beanstatus1.releaseType }">${beanstatus1.releaseTypeName }</option>
												</c:forEach>
											</c:if>
										</select>
								</div>
							</fieldset>
					 	</div>
	 			</fieldset>
			</form>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.amountdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width10"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width10"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20">Condition/Status</th>
				            					<th class="width10"><fmt:message key="re.property.info.amount"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.remarks"/></th>
				            					<th class="width20">Supervisor Comments</th>
				            					
											</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="rowid">  	 
													<td class="width10">${result.lineNumber}</td>
													<td class="width10">${result.maintainenceCode}</td>	
													<td class="width20">${result.conditionStatusName }</td>
													<td class="width10 amountcalc">${result.amount}</td> 
													<td class="width20">${result.remarks}</td>
													<td class="width20">${result.comments}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.flatChildId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style="display:none;" class="release_type">${result.releaseType}</td>
													<td style="display:none;">${result.conditionStatusCode}</td>		 
												</tr>  
								  		 	</c:forEach>	
										    <c:forEach var="i" begin="${countSize+1}" end="${countSize+2}">
											 	<tr class="rowid" style="display:none;">
											 		<td class="width10">${i}</td>
													<td class="width10"></td>	
													<td class="width20"></td>	
													<td class="width10 amountcalc"></td> 
													<td class="width20"></td>
													<td class="width20"></td>
													<td style="display:none"> 
														<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
														<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
														<input type="hidden" value="" name="tempLineId" id="tempLineId"/>  
													</td> 
													<td style="display:none;" class="release_type"></td>
													<td style="display:none;"></td>	 
												</tr>  
								  		 	</c:forEach>
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
				    		<!--  <div class="float-right " style="display:none;">
							<label>Total</label>
						 	<input name="amountTotal" id="amountTotal" style="width:45%!important;" disabled="disabled" >
						 </div>-->
						</div> 
					</form>
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" style="display:none;"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>
			<input class="width40" type="hidden" name="offerId" id="flatId" disabled="disabled" value="${bean.flatId}" />
		 	<div class="float-right buttons ui-widget-content ui-corner-all">
		 			<div  id="close" class="portlet-header ui-widget-header float-right"><fmt:message key="hr.common.button.close"/></div> 
			</div>
 		</div>
 	</div>
 </div>
 


	 	
