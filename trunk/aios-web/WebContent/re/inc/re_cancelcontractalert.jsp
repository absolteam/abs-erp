<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript">
var accountTypeId=0; var idval=0;
var cancelDetail ="";
$(function(){
	
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Cancellation Contract Receipt</div>
		<div class="portlet-content">
		    <div id="temp-result" style="display:none;"></div> 
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	
			<form id="contractreceipt-form" name="contractdetails">
				<input type="hidden" name="alertId" id="alertId" value="${CANCELLATION_ALERT.alertId}"/> 
				<input type="hidden" name="contractId" id="contractId" value="${CANCELLATION_ALERT.contractId}"/>  
				<input type="hidden" name="propertyId" id="propertyId" value="${CANCELLATION_ALERT.propertyId}"/> 
				<input type="hidden" name="propertyName" id="propertyName" value="${CANCELLATION_ALERT.propertyName}"/> 
				<div class="width100 float-left" id="hrm">
					<fieldset>
						<legend>Contact Details</legend>  
						<div class="float-right width20">
							<label class="width30">C.No</label> 
							<span>${CANCELLATION_ALERT.contractNumber}</span>
						</div> 
						<div style="margin:5px;">
							<label class="width30">Receipt from Mr/M/s</label> 
							<span id="tenantName">${CANCELLATION_ALERT.tenantName}</span>
							<input type="hidden" id="tenantId" value="${CANCELLATION_ALERT.description}"/>
						</div>
						<div class="float-right width20">
							<label class="width30">Date</label> 
							<span>${CANCELLATION_ALERT.contractDate}</span>
						</div>  
						<div style="margin:5px;">
							<label class="width30">Contract Fee</label> 
							<span>${CANCELLATION_ALERT.contractAmount}</span>
						</div>
						<div style="margin:5px;">
							<label class="width30">Contract Deposit</label> 
							<span>${CANCELLATION_ALERT.depositAmount}</span>
						</div> 
					</fieldset>
				</div>  
				<div class="clearfix"></div>  
					<div id="hrm" class="hastable width100"> 
					<fieldset>
					<legend>Cancel Contract Payments</legend>
							<table id="hastab" class="width100"> 
								<thead>
							   <tr>  
						   		 <th style="width:5px;">PDC</th>   
								 <th>Fee Type</th>
								 <th>Amount</th> 
								 <th>Combination</th> 
							  </tr>
							</thead> 
							<tbody class="tab"> 
								<c:choose>
									<c:when test="${fn:length(CANCELLATION_ALERT.cancellationList)gt 0}">
										<c:forEach items="${CANCELLATION_ALERT.cancellationList}" var="bean" varStatus="status1" >
										 	<tr class="rowid" id="fieldrow_${status1.index+1}" style="height:25px;">  
								 	 			<td style="width:5px;">
								 	 				<c:choose>
														<c:when test="${bean.pdcAccount ne true}">
								 	 						<input type="checkbox" disabled="disabled" name="postDated" id="postedDated_${status1.index+1}">
								 	 					</c:when>
								 	 					<c:otherwise>
								 	 						<input type="checkbox" checked="checked" disabled="disabled" name="postDated" id="postedDated_${status1.index+1}">
								 	 					</c:otherwise>
													</c:choose> 
								 	 			</td> 
												<td id="feeType_${status1.index+1}">${bean.feeDescription}</td>
												<td id="rentalReturn_${status1.index+1}">${bean.rentalReturn}</td> 
												<td>  
													<c:choose>
														<c:when test="${bean.pdcAccount ne true}"> 
															<c:if test="${bean.unEarnedAccountCode ne null && bean.unEarnedAccountCode ne ''}">
																<span>${bean.unEarnedAccountCode}</span>
																<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}"/> 
															</c:if>  
														</c:when>
														<c:when test="${bean.pdcAccount eq true}">
															<c:if test="${bean.pdcAccountCode ne null && bean.pdcAccountCode ne ''}">
																<span>${bean.pdcAccountCode}</span>
															</c:if> 
															<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}"/> 
														</c:when>
														<c:otherwise> 
															 <span>Deposit</span>
														</c:otherwise>
													</c:choose> 
												</td>  
											</tr>
										</c:forEach>
									  </c:when>
								</c:choose>	  
							</tbody>
						</table>
					</fieldset>
				</div> 
				<div class="portlet-content">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
						<div class="portlet-header ui-widget-header float-right print">print</div>
					</div>  
				</div>
			</form>
		</div>	
	</div>	
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>	
</div>