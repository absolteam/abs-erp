<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.opn_td{
	width:6%;
}
#DOMWindow{	
	width:95%!important;
	height:90%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.spanfont{
	font-weight: normal;
}
.divpadding{
		padding-bottom: 7px;
}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var contractId=0;
var processFlag=0;
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click'); 

	$('.processct').click(function(){   
		contractId=$('#contractId').val();
		 var processct=$(this).attr('id');
		 processFlag=4; 
		if(processct=="approve"){
			processFlag = 1;
			processContracWorkFlow(contractId,processFlag);
		}else{
			processFlag = 2;
			$('.ui-dialog-titlebar').remove();   
			$('#contract_cancel').dialog('open'); 
		}  
		return false;
	});

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	});

	$('#contract_cancel').dialog({
 		autoOpen: false,
 		width: 500,
 		height: 200,
 		zIndex : 99999,
 		bgiframe: true,
 		modal: true,
 		buttons: {
 			"Ok": function(){  
 				var comment = $('#comment').val();
 				$.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/comment_workflow_reject_save.action",
 					data:{comment:comment},
 				 	async: false,
 				    dataType: "html",
 				    cache: false,
 				    success:function(result){ 
 				    	processContracWorkFlow(contractId,processFlag);
 				    }
 				});
 				$(this).dialog("close");   
 			}
 		}
 	}); 
});
function processContracWorkFlow(contractId, processFlag){ 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/workflowProcess.action", 
		async: false, 
		data:{recordId: contractId, finalyzed:1, processFlag: processFlag},
			 dataType: "html",
			 cache: false,
		     success:function(result){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/contract_tranaction_saverevert.action", 
				 	async: false, 
				 	data:{ 
				 		contractId: contractId, processFlag: processFlag
				 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresultfinal").html(result);
						 var message=$('#returnMsg').html().toUpperCase(); 
						 if(message.trim()=="SUCCESS"){
							$('#DOMWindow').remove();
							$('#DOMWindowOverlay').remove();
							window.location.reload();
						 }else{
							 $('#contract-error').hide().html("Transaction Failure.").slideDown(1000);
						 }
					},
					error:function(result){  
						$('#contract-error').hide().html("Transaction Failure.").slideDown(1000);
					}
				});  
			},
			error:function(result){  
				$('#contract-error').hide().html("Transaction Failure.").slideDown(1000);
			}
	}); 
}
</script>
<div id="main-content"> 
	<div id="contract_cancel" style="display: none;  z-index: 9999999"> 
		<div class="width98 float-left" style="margin-top:10px; padding:3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div> 		
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	  <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.contract.contractagreement"/></div>
	 		<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
	 		<div style="display:none;" class="tempresultfinal"> 
			</div>
		 	<form id="contractAgreementAproval">
		 		<div class="width100 float-left" id="hrm"> 
		 			<input type="hidden" id="contractId" value="${CONTRACT_INFO.contractId}"/>
			 		<fieldset>
			 			<legend><fmt:message key="re.contract.offerContractDetails"/></legend>
			 			<div id="hrm" class="hastable width100" style="margin: 5px 0 5px 0;"> 
							<table id="hastab" class="width100">	
								<thead>									
									<tr>
										<th class="width10"><fmt:message key="re.contract.contractno"/></th> 
										<th class="width10"><fmt:message key="re.contract.contractdate"/></th>
										<th class="width10"><fmt:message key="re.contract.contractFee"/></th>
										<th class="width10"><fmt:message key="re.contract.deposit"/></th>
										<th style="width:5%;"><fmt:message key="re.contract.installments"/></th>
									</tr> 
								</thead> 
								<tbody class="tab">
									 <tr>
									 	<td>
									 		${CONTRACT_INFO.contractNo}
									 	</td> 
									 	<td>
									 		<c:set var="cdate" value="${CONTRACT_INFO.contractDate}"/>
											<%=DateFormat.convertDateToString(pageContext.getAttribute("cdate").toString()) %>
									 	</td> 
									 	<td>
									 		<c:set var="fee" value="${CONTRACT_INFO.contractFee}"/>
											<%=AIOSCommons.formatAmount(pageContext.getAttribute("fee"))%>
									 	</td> 
									 	<td>
									 		<c:set var="deposit" value="${CONTRACT_INFO.deposit}"/>
											<%=AIOSCommons.formatAmount(pageContext.getAttribute("deposit"))%>
									 	</td> 
									 	<td>
									 		${CONTRACT_INFO.installments}
									 	</td> 
									 </tr>
								</tbody> 
							</table>
						</div>		
			 		</fieldset>
		 		</div>
		 		<div id="hrm" class="width100 float-left" style="margin: 5px 0 5px 0;"> 
		 			<fieldset>
			 			<legend>Corresponding  Properties:</legend>
			 			<div id="hrm" class="hastable width100"> 
							<table id="hastab" class="width100">	
								<thead>									
									<tr>
										<th class="width10"><fmt:message key="re.contract.propertyname"/></th> 
										<th class="width10"><fmt:message key="re.contract.address"/></th> 
									</tr> 
								</thead> 
								<tbody class="tab">
									<c:forEach items="${CONTRACT_PROPERTY}" var="property" varStatus="status" >
										<tr>
											<td>${property.propertyName}</td>
											<td>${property.addressOne}</td>
										</tr>
									</c:forEach>
								</tbody> 
							</table>
						</div>		
			 		</fieldset>
		 		</div>
		 		<div class="width100 float-left" style="margin: 5px 0 5px 0;" id="hrm"> 
		 			<fieldset>
			 			<legend>Corresponding  Tenant(s):</legend>
			 			<div id="hrm" class="hastable width100"> 
							<table id="hastab" class="width100">	
								<thead>									
									<tr>
										<th class="width10"><fmt:message key="re.contract.tenantname"/></th> 
										<th class="width10"><fmt:message key="re.contract.tenanttype"/></th> 
										<th class="width10"><fmt:message key="re.contract.tenantidentity"/></th> 
										<th class="width10"><fmt:message key="re.contract.tenantnationality"/></th> 
										<th class="width10"><fmt:message key="re.contract.tenantotherinfo"/></th> 
									</tr> 
								</thead> 
								<tbody class="tab">
									<tr>
									 <c:choose>
									 	<c:when test="${CONTRACT_COMPANY ne null && CONTRACT_COMPANY ne ''}">
									 		<td>${CONTRACT_COMPANY.companyName}</td>
									 		<td>Company</td>
									 		<td/><td/><td/>
									 	</c:when>
									 	<c:otherwise>	
									 		<td>${CONTRACT_PERSON.firstName} ${CONTRACT_PERSON.lastName}</td>
									 		<td>Person</td><td/><td/><td/>
									 	</c:otherwise>
									 </c:choose>
									 </tr>
								</tbody> 
							</table>
						</div>		
			 		</fieldset>
		 		</div>
		 		<div class="width100 float-left" id="hrm"> 
		 			<fieldset>
			 			<legend><fmt:message key="re.contract.paymentdetails"/>:</legend>
			 			<div id="hrm" class="hastable width100" style="margin: 5px 0 5px 0;"> 
							<table id="hastab" class="width100">	
								<thead>									
									<tr>
										<th class="width20"><fmt:message key="re.contract.bankCash"/></th> 
										<th class="width20"><fmt:message key="re.contract.chequeno"/></th>
										<th class="width20"><fmt:message key="re.contract.chequedate"/></th>
										<th class="width20"><fmt:message key="re.contract.amount"/></th>
									</tr> 
								</thead> 
								<tbody class="tab">
									<c:forEach items="${CONTRACT_PAYMENT}" var="payments" varStatus="status" >
										<tr>
											<td>${payments.bankName}</td>
											<td>${payments.chequeNo}</td>
											<td>
												<c:if test="${payments.chequeDate ne null && payments.chequeDate ne '' }">
													<c:set var="date" value="${payments.chequeDate}"/>
													<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString()) %>
												</c:if> 
											</td>
											<td> 
												<c:set var="amount" value="${payments.amount}"/>
												<%=AIOSCommons.formatAmount(pageContext.getAttribute("amount"))%> 
											</td>
										</tr>
									</c:forEach>
								</tbody> 
							</table>
						</div>		
			 		</fieldset>
		 		</div> 
			</form>
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right processct" id="reject" style="cursor:pointer;"><fmt:message key="accounts.common.button.reject"/></div>  
		<div class="portlet-header ui-widget-header float-right processct" id="approve" style="cursor:pointer;"><fmt:message key="accounts.common.button.approve"/></div>
		<div class="callJq" style="display: none;"> </div>  
	 </div>
  </div>
  </div>