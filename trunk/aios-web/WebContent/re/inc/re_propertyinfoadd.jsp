<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/thickbox.css" type="text/css" />

<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style>

</style>
<script type="text/javascript">
var startpick;
var slidetab;
var actualID;
var openFlag=0;
var total;
var currentOwnerId;
var currentOwnerType;

var editing = "${editingProperty}";
var propertyEditId = "${buildingId}";
var isApprove = "${isApprove}";
var seletedFeatureEdit;
var editFeatureId;

var ownerRowsPopulated = 0;
var featureRowsPopulated = 0;

var temp;
var acMeter;
var serviceMeter;
var rent2;
var tempid="";
var propertyNameProvided = true;
var accountTypeId=0;

var autoPropertyName = new Array();
var autoPropertyNameArabic = new Array();
var accountTypeId=0; 
var unitRecordId = "${unitRecordId}";
var decisionFlag=false;
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}

$(function (){  
	$('.formError').remove(); 
	
	
	$jquery("#propertyInfoAdd").validationEngine('attach');
	
	 var tempvar=$('.featuretab>tr:last').attr('id'); 
	 var idval=tempvar.split("_");
	 var rowid=Number(idval[1]); 
	 $('#featureDeleteImage_'+rowid).hide(); 
	 
	
	
	$('.delOwner').each(function(){
		var tempvar=$(this).attr('id');
		var idval=tempvar.split("_");
		var rowid=Number(idval[1]);
		//if(rowid>1)
		//	$('#DeleteOwner_'+rowid).hide();
	});

	if(editing == "true") {
		var cost = convertToAmount("${marketValue}");
		var tax = convertToAmount("${propertyTax}");
		var rentY = convertToAmount("${rentPerYr}");
	
		$("#marketValue").val(cost);
		$("#propertyTax").val(tax);
		$("#rentPerYear").val(rentY);

		$("#propertyStatus").val("${propStatus}");
		$("#status").val($("#propertyStatus").val());

		$("#measurementCode").val("${measurementCode}");
		$("#buildingSizeUnit").val("${buildingSizeUnit}");

		$('#propertyType').attr('disabled', true);
		$('#buildingName').attr('readOnly', true);
		
		$('.accounts').hide(); 
	}
	
	$("#propertyType").change(function(){ 
		var selectedPropertyType = $("#propertyType").val();
		if(selectedPropertyType == "1") {			
			$('#ac').hide();
			$('#service').hide(); 
			//$("#acMeter").removeClass("validate[required]");
			//$("#serviceMeter").removeClass("validate[required]");			
		} else {
			$('#ac').show();
			$('#service').show();
			//$("#acMeter").addClass("validate[required]");
			//$("#serviceMeter").addClass("validate[required]");
		}
		
		if(selectedPropertyType != "2" && selectedPropertyType != "3") {
			$("#floorInfo").hide();	
			$("#floorNo").removeClass("validate[required]");
			$("#fire").hide();	
			$("#floorNo").val("");
			//$("#fire_meter").removeClass("validate[required]");
		}
		else {
			$("#floorInfo").show();
			$("#floorNo").addClass("validate[required]");			
		}
		
		if(selectedPropertyType == "2") {
			$("#fire").show();	
			//$("#fire_meter").addClass("validate[required]");	
		} 
		//else if(selectedPropertyType == "3") { 
			//$('#ac').hide();
			//$("#acMeter").removeClass("validate[required]");
	//	} 

		if(selectedPropertyType == "1" || selectedPropertyType == "6" || selectedPropertyType == "7") {
			$('#save').hide();
			$('#next').show();
		} else {
			$('#save').show();
			$('#next').hide();
		}
		
		if(selectedPropertyType == "4") {		//Empty Land
			$('#div_yearofbuild').hide();
			$('#div_buildingcost').hide();
			$('#div_buildingSize').hide();
			$("#yearOfBuild").removeClass("validate[required,custom[integer,maxSize[4]]]");
			$("#marketValue").removeClass("validate[required]");
			$("#buildingSize").removeClass("validate[required,custom[number]]");
			$("#yearOfBuild").val("");
			$("#marketValue").val("");
			$("#buildingSize").val("0.0");
			//$('.propertyinfoName-en').html('<fmt:message key="re.property.info.landname" />');
			//$('.propertyinfoName-ar').html('<fmt:message key="re.property.info.landnamearabic" />');
		} else {
			$("#buildingSize").addClass("validate[required,custom[number]]");
			$('#div_buildingSize').show();
			
			$("#yearOfBuild").addClass("validate[required,custom[integer,maxSize[4]]]");
			$('#div_yearofbuild').show();

			$("#marketValue").addClass("validate[required]");
			$('#div_buildingcost').show();
			//$('.propertyinfoName-en').html('<fmt:message key="re.property.info.buildingname" /> <span class="mandatory">*</span>');
			//$('.propertyinfoName-ar').html('<fmt:message key="re.property.info.buildingname" />');
		}
	});

	$("#propertyType").trigger('change');
	
	if($('#country').val() == 252) { // Update screen if UAE is selected as country for property being edited
		$('#address1').hide();	
		$('#address2').hide();
		$('#area_combo').show();
		$('#zone_combo').show();
		$('#addressLine1').removeClass("validate[required]");
		$('#addressLine2').removeClass("validate[required]");
		$('#uae_area').val($('#addressLine1').val());
		$('#uae_zone').val($('#addressLine2').val());
	}

	$(".save_or_next").click(function(){
		$('.error,.success').hide();
		$('.tempresultfinal').fadeOut(); 
		$(".tempresultfinal").html("");
		if($jquery('#propertyInfoAdd').validationEngine('validate')){ 
				var childCount = $('#childCount').val();
				var  childCountFeature=$('#childCountFeature').val();
				var  childCountComponent=$('#childCountComponent').val();
				companyId = $('#companyCode').val();
				total=0;
				$('.percentage').each(function(){ 
					total+=Number($(this).html());
				});
				//alert(companyId);
				if((childCount!="" && childCount > 0  && companyId != "") || editing == "true"){
					if(total == 100){
						var buildingId = Number($('#buildingId').val());
						var buildingName = $('#buildingName').val();
						var buildingNameArabic = $('#buildingNameArabic').val();
						var propertyType=$('#propertyType').val();
						var floorNo=$('#floorNo').val();
						var plotNo=$('#plotNo').val();
						var addressLine1=$('#addressLine1').val();
						var addressLine2=$('#addressLine2').val();					
						var country=$('#country').val();
						if(country == 252) {

							if($('#uae_area').val() == "other")
								addressLine1 = $('#area_other').val();
							else
								addressLine1 = $('#uae_area').val();
							
							if( $('#uae_zone').val() == "other")
								addressLine2 = $('#zone_other').val();
							else
								addressLine2 = $('#uae_zone').val();
						} 
						var state=$('#state').val();
						var city=$('#city').val();
						var district=$('#district').val();
						var startPicker=$('#startPicker').val();
						var endPicker=$('#endPicker').val();
						var status=$('#status').val();
						var landSize=$('#landSize').val();
						var propertyTax=$('#propertyTax').val();
						var yearOfBuild=$('#yearOfBuild').val();
						var marketValue=convertToDouble($('#marketValue').val());
						var measurementCode= $('#measurementCode').val();
						var image=$('#URNdms').val();
						var propertyTypeId=$('#propertyType').val();
						var rent2 = convertToDouble($('#rentPerYear').val());
						var acMeter = $('#acMeter').val();
						var serviceMeter = $('#serviceMeter').val();
						var fireMeter = $('#fire_meter').val(); 

						var propertyCode = $('#propertyCode').val(); 
						var propertyCodeArabic = $('#propertyCodeArabic').val(); 
						
						var buildingSize = $("#buildingSize").val();
						var buildingSizeUnit = Number($("#buildingSizeUnit").val());
					//	var combinationId=Number($('#combinationId').val());
					//	var revenueId=Number($('#revenueId').val());
					//	var codeCombination=$('#codeCombination').val();
	
						//Maintenance
						var maintenanceCompanyId = $('#maintenanceId').val();
						var companyId = $('#companyCode').val();
						var maintenanceCompanyDetails = $('#maintenanceDetails').val();		
						var managementDecision="";
						managementDecision=$('#managementDecisiontext').val(); 
						if(Number($('#managementDecisionId').val())>0 && $('#managementDecisionId').val()!="")
							managementDecision="";
						else
							managementDecision=$('#managementDecisiontext').val();
						 
						var targetAction;
						if(propertyTypeId == "1" || propertyTypeId == "6" || propertyTypeId == "7")
							targetAction = "save_property_jumpto_units";
						else
							targetAction = "add_final_save_property";

						//alert(propertyCodeArabic);
						var propertyTypeNature=$('#propertyType option:selected').text();
						
						if(buildingId>0){
							successMessage="Successfully Created";
						}else{
							successMessage="Successfully Modified";
						}
						
						if(true){
							$('#loading').fadeIn();
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/" + targetAction + ".action",
								data: {buildingId: buildingId, buildingName: buildingName, propertyTypeId: propertyTypeId,
									propertyTypeName: propertyType,floorNo: floorNo,plotNo: plotNo,addressLine1: addressLine1,
									addressLine2: addressLine2,countryCode: country,stateName: state,cityName: city,
									propStatus: status, landSize: landSize,propertyTax: propertyTax, yearOfBuild: yearOfBuild,
									marketValue: marketValue,measurementCode: measurementCode, companyId: companyId, 
									maintenanceCompanyId: maintenanceCompanyId,	maintenanceCompanyDetails: maintenanceCompanyDetails,
									rentPerYr: rent2, acMeter: acMeter, serviceMeter: serviceMeter, 
									buildingSize: buildingSize, buildingSizeUnit: buildingSizeUnit,  
									isApprove:isApprove, unitRecordId: unitRecordId, fireMeter: fireMeter, 
									managementDecision:managementDecision, propertyCode: propertyCode, propertyCodeArabic: propertyCodeArabic,
									buildingNameArabic: buildingNameArabic, propertyTypeNature:propertyTypeNature},  
								async: false,
								dataType: "html",
								cache: false,
								success:function(result){
									$(".tempresultfinal").html(result);
									var returnErrorMessage=$("#return-error-message").html().trim();
									if(returnErrorMessage==null || returnErrorMessage==""){
										$('#codecombination-popup').dialog('destroy');		
								  		$('#codecombination-popup').remove();
								  		$('#management_cn').dialog('destroy');		
								  		$('#management_cn').remove();
								  		$('#management_cnview').dialog('destroy');	
								  		$('#maintenance-popup').remove();
								  		$('#maintenance-popup').dialog('destroy');	 
								  		$('#management_cnview').remove();
								  		$('#managementDecision').val("");
								  		destroyPopups();
										$("#main-wrapper").html(result); 
									}else{
										$(".success").html(successMessage);
										$(".success").show();
									}
								},  
							 	error:function(result){
							 		$(".tempresultfinal").html(result);
									var returnErrorMessage=$("#return-error-message").html().trim();
									$(".childCountErr").hide().html(returnErrorMessage).slideDown();
							 		$('#codecombination-popup').dialog('destroy');		
							  		$('#codecombination-popup').remove(); 
							  		$('#management_cn').dialog('destroy');		
							  		$('#management_cn').remove();
							  		$('#maintenance-popup').remove();
							  		$('#maintenance-popup').dialog('destroy');	
							  		$('#management_cnview').dialog('destroy');		
							  		$('#management_cnview').remove();
							  		$('#managementDecision').val("");
									//$("#main-wrapper").html(result); 
									
							 	} 
							});
							$('#loading').fadeOut();
						}else{
							$('.childCountErr').hide().html('<fmt:message key="re.property.info.internalError" />').slideDown();
							// Scroll to top of the page
							$.scrollTo(0,300);
							return false; 
						}
					}else{
						$('.childCountErr').hide().html('<fmt:message key="re.property.info.percentageCheck" />').slideDown();
						// Scroll to top of the page
						$.scrollTo(0,300);
						return false;
					}
				}else{
					$('.childCountErr').hide().html('Enter the maintenance Information.').slideDown();
					// Scroll to top of the page
					$.scrollTo(0,300);
					return false; 
				} 
		 }else{
			 $('#requiredMsgOnSave').html('<fmt:message key="re.property.info.childCheck" />').slideDown().delay(2500).slideUp();
			 return false;
		  }
	});

	if(editing == "true"){
		populateUploadsPane("doc","uploadedDocs","Property",propertyEditId);
		populateUploadsPane("img","uploadedImages","Property",propertyEditId);
		
		$('#dms_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","uploadedDocs","Property",propertyEditId,"PropertyDocuments");
		});
		
		$('#dms_image_information').click(function(){
			AIOS_Uploader.openFileUploader("img","uploadedImages","Property",propertyEditId, "PropertyImages");
		});
	}
	else {
		$('#dms_document_information').click(function(){
			AIOS_Uploader.openFileUploader("doc","uploadedDocs","Property","-1","PropertyDocuments");
		});
		
		$('#dms_image_information').click(function(){
			AIOS_Uploader.openFileUploader("img","uploadedImages","Property","-1", "PropertyImages");
		});
	}
	
	$('.feature-popup').click(function(){ 
		
	       tempid=$(this).parent().get(0); 
			$('#property-popup').dialog('open');
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_property_maintenance_redirect.action",
			 	async: false, 
			 	data:{},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.property-result').html(result); 
				},
				error:function(result){ 
					 $('.property-result').html(result); 
				}
			}); 
		});

	$('.maintenance-popup').click(function(){  
		$('.error,.success').hide();
	       tempid=$(this).parent().get(0); 
	       $('.ui-dialog-titlebar').remove();    
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/add_add_property_maintenance_redirect.action",
			 	async: false, 
			 	data:{},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.property-result').html(result); 
				},
				error:function(result){ 
					 $('.property-result').html(result); 
				}
			}); 
		});
	
	$('#maintenance-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:500, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	
	
	// Adding Owner Details
	$('.addOwner').click(function(){ 
		$('.error,.success').hide();
		if(openFlag==0){
			if($jquery('#propertyInfoAdd').validationEngine('validate')){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_add_property_owner_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(5)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
			         $('#loading').fadeOut();
			         openFlag=1;
					}
				});
			}else{
				return false;
			}
		}
	});
	
	$(".editOwner").click(function(){
		$('.error,.success').hide();
		if(openFlag==0){
			if($jquery('#propertyInfoAdd').validationEngine('validate')){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				actualID=$($($(slidetab).children().get(4)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_edit_property_owner_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(result).insertAfter(slidetab);
						 $($($(slidetab).children().get(5)).children().get(1)).hide();	//Edit Button
				         $($($(slidetab).children().get(5)).children().get(2)).hide(); //Delete Button
				         $($($(slidetab).children().get(5)).children().get(3)).show();	//Processing Button
		
				         var temp1= $($(slidetab).children().get(0)).text();
						 var temp2= $($(slidetab).children().get(1)).text();
						 var temp3= $($(slidetab).children().get(2)).text();
						 var temp4= $($(slidetab).children().get(3)).text();
						 var temp7= $($(slidetab).children().get(6)).text();
						 
						$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
							$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(2)).val(temp7);
						$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp2);
						$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp3);
						
					  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp4);
					  		$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(2)).val(temp4);
		
					  	$('#loading').fadeOut();
					  	 openFlag=1;
					}
				});
			}else{
				return false;
			}
	  	}
	});
   
 	$('.delOwner').click(function(){
 		$('.error,.success').hide();
 		var ownerIdToDelete = $($(this).closest('tr').children().get(6)).text();
 		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        $('#warningMsg_f').hide();
	 		var trnValue=$("#transURN").val(); 
	 		var ownerId=$('#ownerId').val();
	 		var ownerType = $($(this).closest('tr').children().get(7)).text(); //////////// GET OWNER TYPE
	 		actualID=$($($(slidetab).children().get(4)).children().get(0)).val(); // GET OWNER ID
			var flag = false; 
			
			if(ownerIdToDelete != null && ownerIdToDelete !='' && ownerIdToDelete!=undefined){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/add_delete_property_owner_update.action", 
				 	async: false,
				 	data: {trnValue:trnValue, actualLineId: actualID, ownerId: ownerIdToDelete, ownerType: ownerType},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		$('#percentageTotal').val(0);
		       		 
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
 		}
 	});	

 	
 	$("#discard").click(function(){
 		$('#loading').fadeIn();
 		var trnValue=$("#transURN").val();
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/add_discard_property.action", 
		 	async: false,
		 	data:{trnValue:trnValue},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
		  		$('#management_cn').dialog('destroy');		
		  		$('#management_cn').remove();
		  		$('#management_cnview').dialog('destroy');		
		  		$('#management_cnview').remove();
		  		$('#managementDecision').val("");
		  		destroyPopups();
		 		$("#main-wrapper").html(result);
		 		$('#maintenance-popup').remove();
		  		$('#maintenance-popup').dialog('destroy');	
				$("#transURN").val("");  
				$('#loading').fadeOut();
			},
			error:function(result){
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
		  		$('#management_cn').dialog('destroy');	
		  		$('#management_cnview').dialog('destroy');		
		  		$('#management_cnview').remove();
		  		$('#management_cn').remove();
		  		$('#managementDecision').val("");
		  		$('#maintenance-popup').remove();
		  		$('#maintenance-popup').dialog('destroy');	
				$("#main-wrapper").html(result);
				$("#transURN").val("");
				$('#loading').fadeOut();
			} 
    	});
 	});
	
	// Add Rows for Owner
	$('.addrowsowner').click(function(){
		var count=Number(0);
		$('.owner').each(function(){
			count=count+1;
		});  
		var lineNumber=count; 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/property_info_owner_addrow.action", 
		 	async: false,
		 	data:{lineNumber:lineNumber},
		    dataType: "html",
		    cache: false,
			success:function(result){
				$(".tabOwner").append(result);
				if($(".tabOwner").height()<255)
					 $(".tabOwner").animate({height:'+=20',maxHeight:'255'},0);
				 if($(".tabOwner").height()>255)
					 $(".tabOwner").css({"overflow-x":"hidden","overflow-y":"auto"});
				 var tempvar=$(".tabOwner>.owner:last").attr("id");        
			     var idarray = tempvar.split('_');
				 var rowId=Number(idarray[1]); 
		         $('#DeleteOwner_'+rowId).show();
			}
		});
	});

	
	
	
	//----------------------------Feature add process-------------------------------------------------------------------------
	//Feature Detail adding
		 $(".featureaddData").click(function(){ 
			slidetab=$(this).parent().parent().get(0);  
			//Find the Id for fetch the value from Id
			var tempvar=$(this).parent().attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			if($jquery("#editCalendarVal").validationEngine('validate')){
					$("#featureAddImage_"+rowid).hide();
					$("#featureEditImage_"+rowid).hide();
					$("#featureDeleteImage_"+rowid).hide();
					$("#featureWorkingImage_"+rowid).show(); 
					
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/feature_add.action",
						data:{id:rowid,addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#openFlag').val(1);
						}
				});
				
			}
			else{
				return false;
			}	
			 
		});

		$(".featureeditData").click(function(){
			 
			 slidetab=$(this).parent().parent().get(0);  
			 
			 if($jquery("#editCalendarVal").validationEngine('validate')) {
				 
					 
					//Find the Id for fetch the value from Id
						var tempvar=$(this).attr("id");
						var idarray = tempvar.split('_');
						var rowid=Number(idarray[1]); 
						$("#featureAddImage_"+rowid).hide();
						$("#featureEditImage_"+rowid).hide();
						$("#featureDeleteImage_"+rowid).hide();
						$("#featureWorkingImage_"+rowid).show(); 
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/feature_add.action",
					 data:{id:rowid,addEditFlag:"E"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){
					
				   $(result).insertAfter(slidetab);

				 //Bussiness parameter
		   			
		   			var featureCode= $('#featureCode_'+rowid).text();
		   			var featureType=$('#featureType_'+rowid).text(); 
      				var feature=$('#feature_'+rowid).text(); 
      				var lookupDetailId=$('#lookupDetailId_'+rowid).val();
      				
      				$('#featureCode').val(featureCode);
      				$('#featureType').val(featureType);
      				$('#feature').val(feature);
      				$('#lookupDetailId').val(lookupDetailId);
				    $('#openFlag').val(1);
				  
				},
				error:function(result){
				//	alert("wee"+result);
				}
				
				});
				 			
			 }
			 else{
				 return false;
			 }	
			 
		});
		 
		 $(".featuredelrow").click(function(){ 
			 slidetab=$(this).parent().parent().get(0); 

			//Find the Id for fetch the value from Id
				var tempvar=$(this).attr("id");
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 
			 var lineId=$('#featurelineId_'+rowid).val();
			
				 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/feature_save.action", 
					 data:{id:lineId,addEditFlag:"D"},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.tempresult').html(result);   
						 if(result!=null){
							 $("#featurefieldrow_"+rowid).remove(); 
							  var identitycount=Number($('#featurecount').val()); 
	         				  identitycount-=1;
	         				  $('#featurecount').val(identitycount);
				        		//To reset sequence 
				        		var i=0;
				     			$('.featurerowid').each(function(){  
									i=i+1;
									$($($(this).children().get(4)).children().get(1)).val(i); 
			   					 }); 
						     }  
					},
					error:function(result){
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 });
				 
			 
		 });


		 $('.featureaddrows').click(function(){
			 var i=0;
			 var lastRowId=$('.featurerowid:last').attr('id');
				if(lastRowId!=null){
					var idarray = lastRowId.split('_');
					//alert(idarray);
					
					rowid=Number(idarray[1]);
					rowid=Number(rowid+1);
				}else{
					rowid=Number(1);
				}
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/feature_addrow.action",
					data:{id:rowid}, 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					$(".featuretab").append(result);
					//To reset sequence 
					 $('.featurerowid').each(function(){  
							i=i+1;
							var tempvar=$(this).attr("id");
							var idarray = tempvar.split('_');
							var rowid1=Number(idarray[1]); 
							$('#featurelineId_'+rowid1).val(i);
							
						});  
					} 
	  			
				});
			});
		 
		 
		 $('#feature-type-add').click(function(){
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         var accessCode="PROPERTY_FEATURE"; 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
		 
	   $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:600,
			height:350,
			bgiframe: false,
			modal: true 
		});
	//------------------------Feature End-------------------------------------------------

	$('#uae_zone').change(function(){ 
		if ($('#uae_zone').val() == "other") {
			
			$('#zone_other_div').show();				
		} else {

			$('#zone_other_div').hide();
		}

		//if(editing != "true") {
			if($('#uae_zone').val() != "" && $('#uae_zone').val() != "other" ) {
	
				autoPropertyName[4] = trimSpaces($('#uae_zone').val());
				try {
					temp = $('#' + cityId).text().split('/')[1];
				} catch (e) {
					temp = "";
				}
				autoPropertyNameArabic[4] = temp;
				$('#propertyCode').val(autoPropertyName.join("-"));	
				$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));	
	
				if(propertyNameProvided == false)
					$('#buildingName').val($('#propertyCode').val());	
				
			} else if($('#uae_zone').val() == "other") {
	
				autoPropertyName[4] = "";
				autoPropertyNameArabic[4] = "";
				$('#propertyCode').val(autoPropertyName.join("-"));	
				$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));	
				
				if(propertyNameProvided == false)
					$('#buildingName').val($('#propertyCode').val());	
			}
		//}
	});
	
	$('#uae_area').change(function(){ 

		//if(editing != "true") {
			if($('#uae_area').val() != "" && $('#uae_area').val() != "other") {
	
				autoPropertyName[3] = trimSpaces($('#uae_area').val().split("-")[0]);
				try {
					temp = $('#uae_area').val().split("/")[1];
				} catch (e) {
					temp = "";
				}
				autoPropertyNameArabic[3] = temp;
				$('#propertyCode').val(autoPropertyName.join("-"));	
				$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));	
	
				if(propertyNameProvided == false)
					$('#buildingName').val($('#propertyCode').val());	
				
			} else if($('#uae_area').val() == "other") {
	
				autoPropertyName[3] = "";
				autoPropertyNameArabic[3] = "";
				$('#propertyCode').val(autoPropertyName.join("-"));
				$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));	
	
				if(propertyNameProvided == false)
					$('#buildingName').val($('#propertyCode').val());	
			}
		//}
		
		if($('#city').val() == "6764") {

			$('#area_other_div').hide();
			$('#zone_other_div').hide();
			
			if($('#uae_area').val() == "KCA - Khalifa City A") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="SE - 4">SE - 4</option>').val('SE - 4');
				
			} else if ($('#uae_area').val() == "MBZ - Mohammad Bin Zayyed City") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="Z5">Z5</option>').val('Z5');
				
			} else if ($('#uae_area').val() == "SH - Shahama") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="S 9/10">S 9/10</option>').val('S 9/10');
				
			} else if ($('#uae_area').val() == "MBZ- Mussafah Shabia") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="ME - 9">ME - 9</option>').val('ME - 9');
				$('#uae_zone').append('<option value="ME - 10">ME - 10</option>');
				$('#uae_zone').append('<option value="ME - 11">ME - 11</option>');
				$('#uae_zone').append('<option value="ME - 12">ME - 12</option>');
				
			} else if ($('#uae_area').val() == "ALM - Al Mushrif") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="AUH W-22">AUH W-22</option>').val('AUH W-22');
				
			} else if ($('#uae_area').val() == "ALK - Al Khubirat Villa") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="AUH W24-01 PLOT 25">AUH W24-01 PLOT 25</option>').val('AUH W24-01 PLOT 25');
				
			} else if ($('#uae_area').val() == "MRC - Marina Royal Compound") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="AUH W42">AUH W42</option>').val('AUH W42');
				
			} else if ($('#uae_area').val() == "EL - Eldarado") {
				
				$('#uae_zone').find('option').remove().end().append('<option value="AUH E-10">AUH E-10</option>').val('AUH E-10');
				
			}else {

				$('#uae_zone').find('option').remove().end().append('<option value="Zone 1">Zone 1</option>').val('Zone 1');
				$('#uae_zone').append('<option value="Zone 2">Zone 2</option>');
				$('#uae_zone').append('<option value="Zone 3">Zone 3</option>');
			}
			$('#uae_zone').append('<option value="other">Other</option>');
		} else if ($('#city').val() == "55000") {

			$('#area_other_div').hide();
			$('#zone_other_div').hide();
			
			$('#uae_zone').find('option').remove().end().append('<option value="Zone 1">Zone 1</option>').val('Zone 1');
			$('#uae_zone').append('<option value="Zone 2">Zone 2</option>');
			$('#uae_zone').append('<option value="Zone 3">Zone 3</option>');
			$('#uae_zone').append('<option value="other">Other</option>');			
		}		

		if ($('#uae_area').val() == "other") {			
			$('#area_other_div').show();							
		}
	});
	
	$('#city').change(function(){
		
		$('#area_other_div').hide();
		$('#zone_other_div').hide();
		
		if($('#city').val() == "6764") {

			$('#uae_area').find('option').remove().end().append('<option value="KCA - Khalifa City A">KCA - Khalifa City A  / مدينة خليفة </option>').val('KCA - Khalifa City A');
			$('#uae_area').append('<option value="MBZ - Mohammad Bin Zayyed City">MBZ - Mohammad Bin Zayyed City / محمد بن زايد </option>');
			$('#uae_area').append('<option value="SH - Shahama">SH - Shahama / الشهامة </option>');
			$('#uae_area').append('<option value="RH - Rahba">RH - Rahba / الرحبة </option>');
			$('#uae_area').append('<option value="MBZ- Mussafah Shabia">MBZ- Mussafah Shabia / المصفح بشعبية </option>');
			$('#uae_area').append('<option value="ALM - Al Mushrif">ALM - Al Mushrif / المشرف </option>');
			$('#uae_area').append('<option value="ALK - Al Khubirat Villa">ALK - Al Khubairat Villa / آل الخبيرات فيلا </option>');
			$('#uae_area').append('<option value="MRC - Marina Royal Compound">MRC - Marina Royal Compound / مارينا رويال المركب </option>');
			$('#uae_area').append('<option value="EL - Eldorado">EL - Eldorado / الدورادو </option>');
			
			$('#uae_area').val("${addressLine1}");
			$('#uae_area').trigger('change');
			
			$('#uae_zone').val("${addressLine2}");
			$('#uae_area').append('<option value="other">Other</option>');
		    
		} else if($('#city').val() == "55000") {

			$('#uae_area').find('option').remove().end().append('<option value="ADI - Abu Dhabi Island">ADI - Abu Dhabi Island / جزيرة أبو ظبي </option>').val('ADI - Abu Dhabi Island');
			$('#uae_area').append('<option value="ALR - AL Reem Island">ALR - AL Reem Island / جزيرة الريم </option>');

			$('#uae_area').val("${addressLine1}");
			$('#uae_area').trigger('change');
			
			$('#uae_zone').val("${addressLine2}");
			$('#uae_area').append('<option value="other">Other</option>');
		}
	});

	$('#city').trigger('change');
	
	//Get State List On Change Country List
	$('#country').change(function(){
		
		countryId=$('#country').val();
		//if(editing != "true") {
			var countryCode = trimSpaces($('#' + countryId).text().split('-')[0]);
			try {
				temp = $('#' + countryId).text().split('/')[1];
			} catch (e) {
				temp = "";
			}
			var countryCodeArabic = temp;
			autoPropertyName = new Array();
			autoPropertyNameArabic = new Array();
			
			autoPropertyName[0] = countryCode;
			autoPropertyNameArabic[0] = countryCodeArabic;
			$('#propertyCode').val(autoPropertyName.join("-"));
			$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));
			
			if($('#buildingName').val() == '' || propertyNameProvided == false) {
	
				propertyNameProvided = false;
				$('#buildingName').attr('disabled', true);
				$('#buildingName').val($('#propertyCode').val());
			}
		//}
		
		if(countryId == 252) {
			
			$('#address1').hide();	
			$('#address2').hide();
			$('#area_combo').show();
			$('#zone_combo').show();
			$('#addressLine1').removeClass("validate[required]");
			$('#addressLine2').removeClass("validate[required]");
			
		} else {

			$('#address1').show();	
			$('#address2').show();
			$('#area_combo').hide();
			$('#zone_combo').hide();
			$('#area_other_div').hide();
			$('#zone_other_div').hide();
			$('#addressLine1').addClass("validate[required]");
		}
		
		$.ajax({
			type: "GET", 
			url: "<%=request.getContextPath()%>/getCountryStates.action", 
	     	async: false,
	     	data:{countryId: countryId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#state").html(result); 
			}, 
			error: function() {
			}
		}); 		
	});

	$.fn.globeTotal = function() { 	// Total for Percentage Calculation
		total=0;
		$('.percentage').each(function(){ 
			total+=Number($(this).html());
		});
		$('#percentageTotal').val(total)
	};
	
 	/* $('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    }); */
	 	
 	//Combination pop-up config
    $('.codecombination-popup').click(function(){
          tempid=$($(this).parent().get(0)).attr('id');   
          $('.ui-dialog-titlebar').remove();   
             $.ajax({
                type:"POST",
                url:"<%=request.getContextPath()%>/combination_treeview.action",
                async: false, 
                dataType: "html",
                cache: false,
                success:function(result){
                     $('.codecombination-result').html(result);
                },
                error:function(result){
                     $('.codecombination-result').html(result);
                }
            });
    }); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
	 });
     
       
     $('.m_decisionadd').click(function(){
    	 $('#management_cn').dialog('open'); 
     });
     
     $('.m_decisionview').click(function(){
    	 $('#management_cnview').dialog('open'); 
     });
     
    // Management Dialog			
 	$('#management_cn').dialog({
 		autoOpen: false,
 		width: 800,
 		height: 500,
 		bgiframe: true,
 		modal: true,
 		buttons: {
 			"Ok": function(){  
 				$(this).dialog("close"); 
 			}, 
		 	"Clear": function() { 
				$('#managementDecisiontext').val("");
				$(this).dialog("close"); 
			} 
 		}
 	});
  
 	$('#management_cnview').dialog({
 		autoOpen: false,
 		width: 800,
 		height: 400,
 		bgiframe: true,
 		modal: true,
 		buttons: {
 			"OK": function(){  
 				$(this).dialog("close"); 
 			} 
 		}
 	});

 	if(editing == "true") {

		if($('#country').val() == 252) {

	 		$('#uae_area').val("other");
	 		$('#uae_zone').val("other");
			$('#area_other_div').show();
			$('#zone_other_div').show();
			$('#area_other').val("${addressLine1}");
			$('#zone_other').val("${addressLine2}");
		}
	}
});

function concatPlotNo() {

	//if (editing != "true") {
		autoPropertyName[5] = $('#plotNo').val();
		autoPropertyNameArabic[5] = $('#plotNo').val();
		$('#propertyCode').val(autoPropertyName.join("-"));
		$('#propertyCodeArabic').val(autoPropertyNameArabic.join("-"));		
		if(propertyNameProvided == false)
			$('#buildingName').val($('#propertyCode').val());
	//}	
}
 
function setCombination(combinationTreeId,combinationTree){ 
	if(tempid=="revenuecode"){  
        $('#revenueId').val(combinationTreeId);
    	$('.revenueDesc').html(combinationTree);
    }
    else{
        $('#combinationId').val(combinationTreeId);
        $('.assetDesc').html(combinationTree); 
    } 
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="re.property.info.propertyinformation" />
		</div>
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Property Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose>  
		<div class="portlet-content">
			
			<div style="display: none;" class="tempresultfinal">
			<c:if test="${bean != null}">
				<input type="hidden" id="sqlReturnStatus"
					value="${bean.sqlReturnStatus}" />
				<c:choose>
					<c:when test="${bean.sqlReturnStatus == 1}">
						<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
					</c:when>
					<c:when test="${bean.sqlReturnStatus != 1}">
						<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
					</c:when>
				</c:choose>
			</c:if>
			</div>
			
			<div class="childCountErr response-msg error ui-corner-all" style="width: 90% !important; display: none;">
			</div>
			
			<form id="propertyInfoAdd" style="width: 98%;position: relative;">
				
				<fieldset style="padding-left: 10px;">
					<div class="form_head"><fmt:message key="re.property.info.generalInfo" /></div>  
					<c:choose>
						<c:when test="${requestScope.buildingId ne null && requestScope.buildingId ne '' && requestScope.buildingId gt 0}">
							<c:choose>
								<c:when test="${requestScope.managementDecisionId ne null && requestScope.managementDecisionId ne ''
													&& requestScope.managementDecisionId gt 0}">
									<div  class="m_decisionview tooltip" title="View Management Decision">
										<img alt="M.D" src="./images/mdcn-view.jpg">
										<input type="hidden" id="managementDecisionId" value="${requestScope.managementDecisionId}"/> 
									</div> 
								</c:when>
								<c:otherwise>
									<div class="m_decisionadd tooltip" title="Add Management Decision">
										<img alt="M.D" src="./images/mdcn-view.png">
										<input type="hidden" id="managementDecisionId"/>
									</div>  
								</c:otherwise>
							</c:choose>  
						</c:when> 
					</c:choose> 
					<div class="width33 float-left" id="hrm">
					<fieldset style="min-height: 310px;" class="form_content_alignment">
					<div style="display: none;"><label>Person Id</label><input
						type="text" name="sessionPersonId" value="${PERSON_ID}" class="width40"
						id="sessionPersonId" disabled="disabled">
					</div>
					<div style="display: none;"><label class="width30">Id No.</label><input
						type="hidden" name="buildingId" value="${buildingId }" id="buildingId"
						class="width30" disabled="disabled">
					</div>
					 
					<div><label class="width33"><fmt:message
						key="re.property.info.propertytype" /><span class="mandatory">*</span></label>
						<select id="propertyType" class="validate[required]" TABINDEX=1
							style="width: 62.5%;" >
							<option value="">-Select-</option>
						
							<c:forEach items="${propertyTypes}" var="propertyType"
								varStatus="status">
								<c:choose>
									<c:when test="${propertyType.propertyTypeId ne propertyTypeId }">
										<option value="${propertyType.propertyTypeId }">${propertyType.type}</option>
									</c:when>
									<c:otherwise>
										<option value="${propertyType.propertyTypeId }" selected="selected">${propertyType.type}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>						
						</select>
					</div>
					<div><label class="width33 tooltip propertyinfoName-en" title="Enter Building Name"><fmt:message
						key="re.property.info.buildingname" /><span class="mandatory">*</span></label>
					<input type="text" name="buildingName" value="${buildingName}"
						id="buildingName" class="width60" TABINDEX=2>
						<div class="clearfix"></div>
					</div>
					<div><label class="width33 tooltip propertyinfoName-ar" title="Enter Building Name (العربية)">
					<fmt:message
						key="re.property.info.buildingname" /> (العربية)</label>
					<input type="text" name="buildingNameArabic" value="${buildingNameArabic}"
						id="buildingNameArabic" class="width60" TABINDEX=2>
					</div>
					<div id="floorInfo"><label class="width33"><fmt:message key="re.property.info.floorsOrSections" /><span class="mandatory">*</span></label> <input
						type="text" id="floorNo" name="floorNo" value="${floorNo}"
						class="width60 validate[required]" TABINDEX=3></div>
									
					<div><label class="width33"><fmt:message
						key="re.property.info.country" /><span class="mandatory">*</span></label> <select
						id="country" class="validate[required]" TABINDEX=4
						style="width: 62.5%;">
						<option value="">- Select -</option>
					<c:choose>
						<c:when test="${COUNTRIES ne null && COUNTRIES ne ''}">
							<c:forEach items="${COUNTRIES}" var="country" varStatus="status">
								<c:choose>
									<c:when test="${ country.countryId ne countryId }">
										<option id="${country.countryId}" value="${country.countryId}">${country.countryCode}
										- ${country.countryName}</option>
									</c:when>
						
									<c:otherwise>
										<option id="${country.countryId}" value="${country.countryId}" selected="selected">${country.countryCode}
										- ${country.countryName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
					</select></div> 
					
					<div><label class="width33"><fmt:message
						key="re.property.info.state" /><span class="mandatory">*</span></label> <select
						id="state" class="validate[required]" TABINDEX=5 style="width: 62.5%;">
						<option>-Select-</option>
						<c:choose>
							<c:when test="${stateId ne null && stateId ne '' && stateId ne 0}">
								<option id="${stateId}" value="${stateId}" selected="selected">${stateName}</option>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					</select></div>
					<div><label class="width33"><fmt:message
						key="re.property.info.city" /><span class="mandatory">*</span></label> <select
						id="city" class="validate[required]" TABINDEX=6 style="width: 62.5%;">
						<option value="">-Select-</option>
						<c:choose>
							<c:when test="${cityId ne null && cityId ne '' && cityId ne 0}">
								<option id="${cityId}" value="${cityId}" selected="selected">${cityName}</option>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
						</select></div> 
					
					<div id="address1"><label class="width33"><fmt:message
						key="re.property.info.addressLine1" /><span class="mandatory">*</span></label>
					<input type="text" id="addressLine1" name="addressLine1"
						value="${addressLine1}" class="width60 validate[required]" TABINDEX=7>
						<div class="clearfix"></div>
					</div>
					
					<div id="address2"><label class="width33"><fmt:message
						key="re.property.info.addressLine2" /></label> <input type="text"
						id="addressLine2" name="addressLine2" value="${addressLine2}"
						class="width60" TABINDEX=8></div>
				
					<div id="area_combo" style="display: none;">
						<label class="width33"><fmt:message
							key="re.property.area" /></label> 
						<select id="uae_area" style="width: 62.5%;" TABINDEX=9>
							<option value="Area 1">Area 1</option>
							<option value="Area 2">Area 2</option>
							<option value="Area 3">Area 3</option>
							<option value="other">Other</option>
						</select>
					</div>
						
					<div id="area_other_div" style="display: none;"><label class="width33">
						<fmt:message
							key="re.property.area" /></label> <input type="text"
						id="area_other" name="area_other" value=""
						class="width60" TABINDEX=8></div>
					
					<div id="zone_combo" style="display: none;">
						<label class="width33"><fmt:message
							key="re.property.zoneSector" /></label> 
						<select id="uae_zone" style="width: 62.5%;" TABINDEX=10>
							<option value="Zone 1">Zone 1</option>
							<option value="Zone 2">Zone 2</option>
							<option value="Zone 3">Zone 3</option>
							<option value="other">Other</option>
						</select>
					</div>
				
					<div id="zone_other_div" style="display: none;"><label class="width33">
						<fmt:message
							key="re.property.zoneSector" /></label> <input type="text"
						id="zone_other" name="zone_other" value=""
						class="width60" TABINDEX=8></div>
					
					<div><label class="width33"><fmt:message
						key="re.property.info.plotno" /><span class="mandatory">*</span></label> <input
						type="text" id="plotNo" name="plotNo" value="${plotNo}"
						class="width60 validate[required]" TABINDEX=11 onblur="concatPlotNo();"></div>
						
						
					</fieldset>
					</div>
					
					<div class="width33 float-left" style="margin-left: 5px;" id="hrm">
						<fieldset style="height: 310px;" class="form_content_alignment">
												
						<div style="display: none;"><label class="width30"><fmt:message
							key="re.property.info.fromdate" /><span>*</span></label> <input type="text"
							name="fromDate" id="startPicker" value="00000"
							class="width60 validate[required]" TABINDEX=12
							title="Select Birth Date" readonly="readonly">
						</div>
						
						<div style="display: none;"><label class="width30"><fmt:message
							key="re.property.info.todate" /></label> <input type="text" name="toDate"
							id="endPicker" value="00000" class="width30" TABINDEX=13
							title="Enter Designation" readonly="readonly">
						</div> 
						
						
						<div><label class="width30"><fmt:message
							key="re.property.info.landsize" /><span class="mandatory">*</span></label> <input
							type="text" name="landSize" id="landSize" value="${landSize}"
							class="width20 validate[required,custom[number]]" TABINDEX=15>
							<select id="measurementCode" style="width: 38.5%" TABINDEX=16>							
								<option value="1">Square Feet</option>							
								<option value="2">Square Meter</option>					
								<option value="3">Square Yard</option>															
							</select>
						</div>
						
						<div id="div_buildingSize"><label class="width30"><fmt:message
							key="re.property.info.buildingSize" /><span class="mandatory">*</span></label> <input
							type="text" name="buildingSize" id="buildingSize" value="${buildingSize}"
							class="width20 validate[required,custom[number]]" TABINDEX=17>
							<select id="buildingSizeUnit" style="width: 38.5%" TABINDEX=18>								
								<option value="1">Square Feet</option>							
								<option value="2">Square Meter</option>						
								<option value="3">Square Yard</option>															
							</select>
						</div>
						
						<div><label class="width30"><fmt:message
							key="re.property.info.propertytax" /></label> <input type="text"
							name="propertyTax" id="propertyTax" 
							class="width60 validate[optional]" TABINDEX=19>
						</div>
						
						<div><label class="width30"><fmt:message
						key="re.property.info.annualRent" /></label> <input type="text"
							name="rentPerYear" id="rentPerYear" 
							class="width60 validate[optional]" TABINDEX=20>
						</div>
						
						<div id="div_yearofbuild"><label class="width30"><fmt:message
							key="re.property.info.yearofbuild" /><span class="mandatory">*</span></label>
						<input type="text" name="yearOfBuild" id="yearOfBuild"
							value="${yearOfBuild}"
							class="width60 validate[required,custom[integer,maxSize[4]]]"
							maxlength="4" TABINDEX=21>
						</div>
						
						<div id="div_buildingcost"><label class="width30"><fmt:message
							key="re.property.info.buildingcost" /><span class="mandatory">*</span></label>  
							<input type="text" name="marketValue" id="marketValue"							
									class="width60 validate[required]" TABINDEX=22>
						</div>
						<div style="display: none;">
							<div class="accounts" style="margin-bottom: 6%;">
								<label class="width30"><fmt:message
								key="re.property.info.assetAccount" /><span class="mandatory">*</span></label>
								<span style="height: 20px;" class="float-left width48 assetDesc"></span>
								<span class="button" id="assetcode"  style="position: relative; top:6px;">
									<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<c:choose> 
									<c:when test="${ACCOUNT_INFO eq 'FALSE'}">
										<input type="hidden" name="combinationId" id="combinationId"/>   
									 	<input type="hidden" name="codeCombination" id="codeCombination"/> 
									</c:when> 
									<c:otherwise>
										<input type="hidden" name="combinationId" id="combinationId" value="${implemenation.propertyAsset}"/>  
									</c:otherwise>
								</c:choose> 
							</div>
							<div class="accounts">
								<label class="width30"><fmt:message
									key="re.property.info.rentRevenue" /><span class="mandatory">*</span></label> 
								<span style="height: 20px;" class="float-left width48 revenueDesc"></span>
								 <span class="button" id="revenuecode" style="position: relative; top:3px;">
									<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								 </span>  
								 <c:choose>  
									<c:when test="${ACCOUNT_INFO eq 'FALSE'}"> 
										<input type="hidden" name="revenueId" id="revenueId" value=""/> 
							 		</c:when>
							 		<c:otherwise>
							 			<input type="hidden" name="revenueId" id="revenueId" value="${implemenation.propertyRevenue}"/>   
							 		</c:otherwise>	
							 	 </c:choose>	 
						 	</div>
					 	</div>
						<div id="ac"><label class="width30"><fmt:message
							key="re.property.info.acMeter" /></label> <input type="text"
							name="acMeter" id="acMeter" value="${acMeter}"
							class="width60" TABINDEX=24>
						</div>
						
						<div id="service"><label class="width30"><fmt:message
							key="re.property.info.serviceMeter" /></label> <input type="text"
							name="serviceMeter" id="serviceMeter" value="${serviceMeter}"
							class="width60" TABINDEX=25>
						</div>
						
						<div id="fire"><label class="width30"><fmt:message
						key="re.property.info.fireAlarmMeter" /></label> <input type="text"
							name="fire_meter" id="fire_meter" value="${fireMeter}"
							class="width60" TABINDEX=25>
						</div>
						<div><label class="width30"><fmt:message
							key="re.property.info.status" /><span class="mandatory">*</span></label> <select
							id="status" class="validate[required]" TABINDEX=14
							style="width: 62.5%;">
							<option value="1"><fmt:message
								key="re.property.info.available" /></option>
							<option value="2"><fmt:message
								key="re.property.info.pending" /></option>
							<option value="5"><fmt:message
								key="re.property.info.renovation" /></option>
						</select>
						<input type="hidden" id="propertyStatus" />
						</div>
					</fieldset>
					</div> 
 
					<div class="width33 float-right" style="position: relative;" id="hrm"> 
						<fieldset style="height: 310px;" class="form_content_alignment">
							<div id="hrm" style="margin-bottom: 10px;">
								<div id="UploadDmsDiv" class="width100">
									
									<div style="height: 110px;">									
										
										<div id="dms_image_information" style="cursor: pointer; color: blue;">
											<u><fmt:message
												key="re.property.info.uploadImagesHere" /></u>
										</div>
										<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
											<span id="uploadedImages"></span>
										</div>
										
									</div>
									<div style="padding-top: 5px; padding-bottom: 5px;"><hr/></div>
									<div style="height: 110px; padding-top: 0px;"> 										
										
										<div id="dms_document_information" style="cursor: pointer; color: blue;">
											<u><fmt:message
												key="re.property.info.uploadDocsHere" /></u>
										</div>
										<div style="padding-top: 10px; margin-top:3px; height: 150px; overflow: auto;">
											<span id="uploadedDocs"></span>
										</div>	
										
									</div>							
								</div>
							</div>
						</fieldset>	
					</div>
				
				</fieldset> 
					<c:choose>
						<c:when test="${requestScope.buildingId ne null && requestScope.buildingId ne ''}">
							<c:choose>
								<c:when test="${requestScope.managementDecisionId ne null && requestScope.managementDecisionId ne ''
													&& requestScope.managementDecisionId gt 0}">
									<div id="management_cnview"> 		
										<label class="width30"><fmt:message
												key="re.property.info.view" /></label>
										<textarea rows="20" cols="90" id="managementDecisiontext" readonly="readonly">${requestScope.managementDecision}</textarea>
									</div>
								</c:when>
								<c:otherwise>
								<div id="management_cn"> 
									<label class="width30"><fmt:message
												key="re.property.info.add" /></label>
									<textarea rows="20" cols="90" id="managementDecisiontext"></textarea>
								</div>
								</c:otherwise>
							</c:choose>  
						</c:when> 
					</c:choose>    
			</form>
			<div id="main-content-property">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" 
				style="border: none !important;">
			<form name="newFields2">
			<div class="form_head portlet-header ui-widget-header"><span
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message
				key="re.property.info.ownerdetails" /></div>
			<div id="temperror" class="response-msg error ui-corner-all"
				style="width: 98% !important; display: none"></div>
			<div id="warningMsg" class="response-msg notice ui-corner-all"
				style="width: 80%; display: none;"></div>
			<div class="portlet-content">
			<div style="display: none;" class="tempresult"></div>
			<input type="hidden" name="trnValue" id="transURN" readonly="readonly" />
			<input type="hidden" name="childCount" id="childCount" value="0" /> <input
				type="hidden" name="percentageTotal" id="percentageTotal"
				style="width: 45% !important;" disabled="disabled">
			<input type="hidden" name="propertyCode" id="propertyCode" value="${propertyCode}" readonly="readonly" />
			<input type="hidden" name="propertyCodeArabic" id="propertyCodeArabic" value="${propertyCodeArabic}" readonly="readonly" />
			<div id="hrm">
			<div id="hrm" class="hastable width100 form_content_alignment">
			<table id="hastab" class="width100">
				<thead class="chrome_tab">
					<tr>
						<th class="width20"><fmt:message
							key="re.property.info.ownername" /></th>
						<th class="width20"><fmt:message
							key="re.property.info.ownertype" /></th>
						<th class="width20"><fmt:message key="re.owner.info.civilidno" /></th>
						<th class="width20"><fmt:message
							key="re.property.info.percentage" /></th>
						<th style="width: 10%;"><fmt:message
							key="re.property.info.options" /></th>
					</tr>
				</thead>
				<tbody class="tabOwner" style="">
					<c:choose>
						<c:when test="${PROPERTY_OWNERS_DISPLAY ne null && PROPERTY_OWNERS_DISPLAY ne ''}">
							<c:forEach items="${PROPERTY_OWNERS_DISPLAY}" var="PROPERTY_OWNERS" varStatus="ostatus">
								<tr>
									<td class="width20">${PROPERTY_OWNERS.ownerName }</td>
									<td class="width20">${PROPERTY_OWNERS.ownerType }</td>
									<td class="width20">${PROPERTY_OWNERS.uaeNo }</td>
									<td class="width20 percentage">${PROPERTY_OWNERS.percentage }</td>
									<td style="display: none"><input type="hidden"
										value="${PROPERTY_OWNERS.propertyInfoOwnerId }" name="actualLineId"
										id="actualLineId" /> <input type="hidden" name="actionFlag"
										value="U" /> <input type="hidden" name="tempLineId" value="" /></td>
									<td style="width: 5%;"><a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwner"
										style="display: none; cursor: pointer;" title="Add Record"> <span
										class="ui-icon ui-icon-plus"></span> </a> <a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwner"
										style="cursor: pointer;" title="Edit Record"> <span
										class="ui-icon ui-icon-wrench"></span> </a> <a id="DeleteOwner_${ostatus.index+1}"
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwner"
										style="cursor: pointer;" title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <a
										class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
										style="display: none; cursor: pointer;" title="Working"> <span
										class="processing"></span> </a></td>
									<td style="display: none;" class="owner_id">${PROPERTY_OWNERS.ownerId
									}</td>
									<td style="display: none;" class="owner_type">${PROPERTY_OWNERS.ownerType
									}</td>
									<td style="display: none;"></td>
								</tr>
							</c:forEach>
						</c:when>
					</c:choose>  
					<c:forEach var="i" begin="1" end="2">
						<tr class="owner">
						<td class="width20"></td>
						<td class="width20"></td>
						<td class="width20"></td>
						<td class="width20 percentage"></td>
						<td style="display: none"><input type="hidden" value=""
							name="actualLineId" id="actualLineId" /></td>
						<td style="width: 10%;"><a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwner"
							style="cursor: pointer;" title="Add Record"> <span
							class="ui-icon ui-icon-plus"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwner"
							style="display: none; cursor: pointer;" title="Edit Record"> <span
							class="ui-icon ui-icon-wrench"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwner" id="DeleteOwner_${i}"
							style="cursor: pointer;" title="Delete Record"> <span
							class="ui-icon ui-icon-circle-close"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
							style="display: none; cursor: pointer;" title="Working"> <span
							class="processing"></span> </a></td>
						<td style="display: none;" class="owner_id"></td>
						<td style="display: none;" class="owner_type"></td>
					</tr>
					</c:forEach> 
				</tbody>
			</table>
			<div class="iDiv" style="display: none;"></div>
			</div>
			<div class="vGrip"><span></span></div>
			</div>
			</div>
			</form>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="form_head portlet-header ui-widget-header float-left addrowsowner"
				style="cursor: pointer; display: none;"><fmt:message
				key="re.property.info.addrow" /></div>
			</div>
			</div>
			</div>
			<div id="main-content-property">
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
					style="border: none !important;">
			<form name="newFields1">
			<div class="form_head portlet-header ui-widget-header">
				<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				<fmt:message key="re.property.info.featuredetails" />
				<!-- Add Feature Link -->
				<a id="feature-type-add" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" 
					class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "  
					title="Add Feature Type" style="float: right; 
											cursor: pointer; 
											left: 49px; 
											border-bottom-width: 1px; 
											margin-bottom: 0px; 
											margin-top: -5px; 
											height: 4px; 
											margin-right: 10px; 
											padding-top: 5px; 
											padding-bottom: 10px;"> 
					
					<span class="ui-icon ui-icon-plus" style="float: left;"></span>
					<label>Add Feature Type</label> 
				</a>
			</div>
			<div id="temperror" class="response-msg error ui-corner-all"
				style="width: 98% !important; display: none"></div>
			<div id="warningMsg_f" class="response-msg notice ui-corner-all"
				style="width: 80%; display: none;"></div>
			<div class="portlet-content">
			<div style="display: none;" class="tempresult"></div>
			<input type="hidden" name="childCount" id="childCountFeature" value="0" />
			<div id="hrm">
			<div id="hrm" class="hastable width100 form_content_alignment">
			<input type="hidden" id="featurecount" value="${fn:length(PROPERTY_FEATURES_DISPLAY)}"/>
			<table id="hastab" class="width100">
				<thead>
					<tr>   
						<th class="width10"><fmt:message key="re.property.info.featurecode" /></th>
						<th class="width10"><fmt:message key="re.property.info.features" /></th>
						<th class="width10"><fmt:message key="re.property.info.featuresDetail" /></th>
						<th style="width:5%;"><fmt:message key="common.options"/></th>
					</tr>
					</thead>
					<tbody class="featuretab">
					 <c:forEach items="${requestScope.PROPERTY_FEATURES_DISPLAY}" var="PROPERTY_FEATURES" varStatus="status">                         
				        <tr id="featurefieldrow_${status.index+1}" class="featurerowid">
				    		<td class="width10" id="featureCode_${status.index+1}">${PROPERTY_FEATURES.lookupDetail.accessCode}</td>
				            <td class="width10" id="featureType_${status.index+1}">${PROPERTY_FEATURES.lookupDetail.displayName}</td>
				            <td class="width10" id="feature_${status.index+1}">${PROPERTY_FEATURES.feature}</td>
				             <td style="width:5%;" id="featureoption_${status.index+1}"> 
		             			<input type="hidden" name="propertyDetailId_${status.index+1}" id="propertyDetailId_${status.index+1}"  value="${PROPERTY_FEATURES.propertyDetailId}"/>
		                		<input type="hidden" name="lookupDetailId_${status.index+1}" id="lookupDetailId_${status.index+1}"  value="${PROPERTY_FEATURES.lookupDetail.lookupDetailId}"/>  
		                		<input type="hidden" name="featurelineId_${status.index+1}" id="featurelineId_${status.index+1}" value="${status.index+1}"/>  
	                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureaddData" id="featureAddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 	 <span class="ui-icon ui-icon-plus"></span>
							   </a>
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureeditData" id="featureEditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span>
							   </a> 
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featuredelrow" id="featureDeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
							   </a>
							    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="featureWorkingImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
									<span class="processing"></span>
							   </a>
				                  
							</td> 
				        </tr> 
				    </c:forEach>
				    <c:forEach var="i" begin="${fn:length(PROPERTY_FEATURES_DISPLAY)+1}" end="${fn:length(PROPERTY_FEATURES_DISPLAY)+1}" step="1" varStatus ="status">  
						<tr id="featurefieldrow_${i}" class="featurerowid">
							<td class="width10" id="featureCode_${i}"></td>
				            <td class="width10" id="featureType_${i}"></td>
				            <td class="width10" id="feature_${i}"></td>
				             <td style="width:5%;" id="featureoption_${i}"> 
		             			<input type="hidden" name="propertyDetailId_${i}" id="propertyDetailId_${i}"  />
		                		<input type="hidden" name="lookupDetailId_${i}" id="lookupDetailId_${i}" />  
		                		<input type="hidden" name="featurelineId_${i}" id="featurelineId_${i}" value="${i}"/>
	                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureaddData" id="featureAddImage_${i}" style="cursor:pointer;" title="Add Record">
							 	 <span class="ui-icon ui-icon-plus"></span>
							   </a>
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featureeditData" id="featureEditImage_${i}" style="display:none;cursor:pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span>
							   </a> 
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip featuredelrow" id="featureDeleteImage_${i}" style="cursor:pointer;" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
							   </a>
							    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="featureWorkingImage_${i}" style="display:none;cursor:pointer;" title="Working">
									<span class="processing"></span>
							   </a>
				                  
							</td> 
						</tr>
				 	</c:forEach>
				 </tbody>
			</table>
			<div class="iDiv" style="display: none;"></div>
			</div>
			<div class="vGrip"><span></span></div>
			</div>
			</div>
			</form>
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="form_head ui-widget-header float-left featureaddrows"
				style="cursor: pointer; display: none;"><fmt:message
				key="re.property.info.addrow" /></div>
			</div>
			</div>
			</div>
			<div id="main-content-property">
			<div
				class=" portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
				style="border: none !important;">
			<form name="newFields1">
			<div class="form_head portlet-header ui-widget-header"><span
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.maintenanceDetails" /></div>
			<div id="temperror" class="response-msg error ui-corner-all"
				style="width: 98% !important; display: none;"></div>
			<div id="wrgNotice" class="response-msg notice ui-corner-all"
				style="width: 80%; display: none;"></div>
			<div class="portlet-content">
			<div style="display: none;" class="tempresult"></div>
			<input type="hidden" name="childCount" id="childCountComponent"
				value="0" />
			<div id="hrm">
			<div id="hrm" class="hastable width100">
			<div class="width45 view float-right" id="hrm"
				style="margin-right: 15px;">
			<div id="othererror" class="response-msg error ui-corner-all"
				style="width: 60%; display: none;"></div>
			</div>
			<div class="view float-left width100" id="hrm">
				<fieldset>
				<div style="display: none;"><label><fmt:message
						key="re.property.info.companyId" /><span class="mandatory">*</span></label> <input
					type="text" name="companyCode" id="companyCode"
					value="${PROPERTY_MAINTENANCE.companyId}"
					class="width30 validate[required]" readonly="readonly" /> 
					<input
					type="hidden" name="maintenanceId"
					value="${PROPERTY_MAINTENANCE.maintenanceId}"
					id="maintenanceId" class="width50" /> </div>
					
					<div style="float: left; width: 33%"><label class="width30"><fmt:message
						key="re.property.info.maintenanceCompanyName" /></label> <input name="maintenanceName"
					id="maintenanceName" class="width70"
					value="${PROPERTY_MAINTENANCE.maintenanceCompanyName}" 
					disabled="disabled"> <span id="hit7" class="button"
					style="width: 40px ! important; cursor: pointer;"> <a
					class="btn ui-state-default ui-corner-all maintenance-popup"> <span
					class="ui-icon ui-icon-newwin"> </span> </a> </span></div>
					
					<div style="float: left; width: 33%"><label class="width30"><fmt:message
						key="re.property.info.companyDetails" /></label> <input name="maintenanceDetails"
					id="maintenanceDetails" class="width70"
					value="${PROPERTY_MAINTENANCE.details}" 
					disabled="disabled"></div>
					
					<div style="float: left; width: 33%"><label class="width30"><fmt:message
						key="re.property.info.tradeNo" /></label> <input name="maintenanceTradeId"
					id="maintenanceTradeId" class="width70"
					value="${PROPERTY_MAINTENANCE.maintenanceCompanyTradeId}" 
					disabled="disabled"></div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="iDiv" style="display: none;"></div>
			</div>
			<div class="vGrip"><span></span></div>
			</div>
			</div>
			</form>
			</div>
			</div>
			</div>
		<div id="requiredMsgOnSave" class="response-msg error ui-corner-all" style="width: 98% !important; display: none;">
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div style="display: none; cursor: pointer;"
				class="property_action_btn portlet-header ui-widget-header float-right" id="close"><fmt:message
				key="re.property.info.close" /></div>
			<div class="property_action_btn portlet-header ui-widget-header float-right cancelrec"
				id="discard" style="cursor: pointer;"><fmt:message
				key="re.property.info.cancel" /></div>
			<div style="display: none;" class="property_action_btn portlet-header ui-widget-header float-right" id="request"
				style="cursor: pointer;"><fmt:message
				key="cs.common.button.requestforverification" /></div>
			<div class="property_action_btn portlet-header ui-widget-header float-right headerData save_or_next"
				id="next" style="cursor: pointer;"><fmt:message
						key="re.property.info.next" /></div>
			<div class="property_action_btn portlet-header ui-widget-header float-right headerData save_or_next"
				id="save" style="cursor: pointer;"><fmt:message
				key="re.property.info.save" /></div>
		</div>
	</div>
</div> 
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="codecombination-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>
<!-- Added for Lookup Information -->
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="common-result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>
<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;"
	 class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable"
	 tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		
	<div id="maintenance-popup" class="ui-dialog-content ui-widget-content"
		style="height: 200px !important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
	</div>
</div>