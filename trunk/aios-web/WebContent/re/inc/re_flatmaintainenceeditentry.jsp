<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="flatMaintainenceReleaseEditEntry" id="flatMaintainenceReleaseEditEntry">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
	<fieldset>	
	<legend><fmt:message key="re.property.info.amountdetails"/></legend> 				 
		<div>
			<label><fmt:message key="re.property.info.amount"/><span style="color:red">*</span></label>
			<input type="text" name="amount" id="amount" class="width50 validate[required,custom[onlyFloat]]">
		</div>
		<!--  <div>
			<label class=""><fmt:message key="re.property.info.releasetype"/><span style="color:red">*</span></label>	 
			  <select id="releaseType" class="width50 flatId validate[required]">
				<option value="">-Select-</option>
				<c:if test="${item != null}">
					<c:forEach items="${requestScope.item}" var="flatList">
						<option value="${flatList.releaseType}">${flatList.releaseTypeName} </option>
					</c:forEach>
				</c:if>
			</select>
		</div>-->
		<div>
			<label><fmt:message key="re.property.info.remarks"/></label>
			<input type="text" name="remarks" id="remarks" class="width50 ">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.linesdetails"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.lineno"/><span style="color:red">*</span></label>
				<input type="text" name="lineNo" id="lineNo" class="width50" disabled="disabled">
			</div>
			<div>
				<label><fmt:message key="re.property.info.maintenancecode"/><span class="mandatory">*</span></label>
				<input type="text" name="maintainenceCode" id="maintainenceCode" disabled="disabled" class="width50  validate[required]">
				<span id="hit7" class="button" style="width: 40px ! important;">
					<a class="btn ui-state-default ui-corner-all maintenance-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
			<div>
				<label class="">Condition/Status<span class="mandatory">*</span></label>	 
				 <select id="conditionStatusCode" class="width50 validate[required]">
					<option value="">-Select-</option>
					<c:if test="${conditionStatus != null}">
						<c:forEach items="${requestScope.conditionStatus}" var="condition">
							<option value="${condition.conditionStatusCode}">${condition.conditionStatusName} </option>
						</c:forEach>
					</c:if>
				</select>
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
	 $(function(){ 
		 $('.formError').remove(); 
		 $("#flatMaintainenceReleaseEditEntry").validationEngine({ 
			 
			 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
		     success :  false,
		     failure : function() { callFailFunction()}
		});
		$('.cancel').click(function(){ 
			$('.formError').remove();
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(6)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(6)).children().get(2)).hide(); 	//Delete button
	        $($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing button 	
	        openFlag=0;	
		 });
		 $('#add').click(function(){
			  if($('#flatMaintainenceReleaseEditEntry').validationEngine({returnIsValid:true})){
				  $('#loading').fadeIn();
					var tempObject=$(this);					
					var lineNo=$('#lineNo').val();
					var maintainenceCode=$('#maintainenceCode').val();
					var amount=$('#amount').val();
					//var releaseType=$('#releaseType').val();
					//var releaseTypeName=$('#releaseType :selected').text();

					var remarks=$('#remarks').val();
					var conditionStatusCode=$('#conditionStatusCode').val();
					var conditionStatusName=$('#conditionStatusCode :selected').text();

					var flatId=$('#flatId').val();				 
					var trnVal =$('#transURN').val(); 

					var actualLineId=$($($(slidetab).children().get(5)).children().get(0)).val();
					var uflag=$($($(slidetab).children().get(5)).children().get(1)).val();
					var tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
					//alert("actualLineId : "+actualLineId+" uflag : "+uflag+" tempLineId : "+tempLineId);
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_edit_flat_maintainence_update.action", 
					 	async: false,
					 	data: {lineNumber: lineNo,maintainenceCode: maintainenceCode,amount: amount,flatId:flatId,
							remarks: remarks, conditionStatusCode: conditionStatusCode, 
							trnValue:trnVal,actualLineId:actualLineId,actionFlag:uflag,tempLineId:tempLineId},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$($(slidetab).children().get(0)).text(lineNo);
		        		$($(slidetab).children().get(1)).text(maintainenceCode);
		        		$($(slidetab).children().get(2)).text(conditionStatusName);
		        		$($(slidetab).children().get(3)).text(amount); 
		        		//$($(slidetab).children().get(4)).text(releaseTypeName); 
		        		$($(slidetab).children().get(4)).text(remarks); 

		        		//$($(slidetab).children().get(8)).text(releaseType); 
		        		$($(slidetab).children().get(8)).text(conditionStatusCode);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(6)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(6)).children().get(2)).hide(); 	//Delete Button
						$($($(slidetab).children().get(6)).children().get(3)).hide();	//Processing Button

						openFlag=0;
	
						$($($(slidetab).children().get(5)).children().get(0)).val($('#objLineVal').html());
				  		$($($(slidetab).children().get(5)).children().get(2)).val($('#objTempId').html());
				  		$("#transURN").val($('#objTrnVal').html());
				  		$.fn.globeTotal();  //For Total Amount of Amount
				   } 
			  }else{
				  return false;
			  }
		});

		 $('.maintenance-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
				$('#flat-popup').dialog('open');
				//shockaction=$($(tempid).siblings().get(1)).attr('id'); 
				var contractId=Number($('#contractId option:selected').val());
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/flat_maintenance_details.action",
				 	async: false, 
				 	data:{contractId:contractId},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.flaat-result').html(result); 
					},
					error:function(result){ 
						 $('.flaat-result').html(result); 
					}
				}); 
			});
		 
		$('#flat-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			bgiframe: false,
			modal: false,
			width:'40%',
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() { 
					$(this).dialog("close"); 
				} 
			}
		});	

		
});
 </script>	