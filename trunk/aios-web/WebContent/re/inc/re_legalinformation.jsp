<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript" src="/js/dateDifference.js"></script>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
 
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function(){
	$(".recurStatus").click(function() {
		$('.recurStat').slideToggle();
	});
	$("#text_area_present_input").focus(function(){
	    $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it
	    return false;
	}); 

	$("#text_area_present_input").keyup(function(){//Detect keypress in the textarea
	    var text_area_box =$(this).val();//Get the values in the textarea
	    var max_numb_of_words = 500;//Set the Maximum Number of words
	    var main = text_area_box.length*100;//Multiply the lenght on words x 100

	    var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	    var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	    if(text_area_box.length <= max_numb_of_words){
	        $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	        $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	        $('#progressbar').animate({//Increase the width of the css property "width"
	        "width": value+'%',
	        }, 1);//Increase the progress bar
	    }
	    else{
	         $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	         $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	    }
	    return false;
	});
	$('#startPicker').datepick({onSelect: customRange, showTrigger: '#calImg'});
	 });
	function checkLinkedDays() {
	    var daysInMonth = $.datepick.daysInMonth(
	        $('#selectedYear').val(), $('#selectedMonth').val());
	    $('#selectedDay option:gt(27)').attr('disabled', false);
	    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	    if ($('#selectedDay').val() > daysInMonth) {
	        $('#selectedDay').val(daysInMonth);
	    }
	}
	function customRange(dates) {
	    if (($(this).attr('class')) == startpick) {
	            $('.endPicker').datepick('option', 'minDate', dates[0] || null);
	        }
	        else {
	            $(startpick).datepick('option', 'maxDate', dates[0] || null);
	        }
	}


</script>
<div id="main-content">
	 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	 		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Legal Information</div>
	 	<div class="portlet-content">
	 	<div style="float:left;width:48%;" id="hrm" class="create-task">
	 	<fieldset>
		<div>
		<label class="width30">Case IdNo</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Case Id in court</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Contract No</label>	 
		<input class="width40" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Case Description</label>	 
		<textarea  class="width50" id="text_area_present_input" name="assignby"  id="assignby" ></textarea>
		</div>
		
		<div>
		<label class="width30">Building ID</label>	 
		<select><option value="">- Select -</option>
		<option value="">1</option>
		<option>2</option>
		</select>
		
		</div>
		<div>
		<label class="width30">Building Component</label>	 
		<input class="width40 recurStatus"  type="checkbox" checked="checked" name="assignby"  id="assignby">
		</div>
		
		<div class="recurStat">
		<label class="width30 recurStat">Building Component</label>	 
		<select><option value="">- Select -</option>
		<option>1</option>
		<option>2</option>
		</select>
		</div>
		</fieldset>
		</div>
		
		<div class="width50 float-left" id="hrm">
			<fieldset style="min-height:175px;">
		
		<div>
		<label class="width30 startPicker">From Date</label>	 
		<input class="width40 startPicker" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30 endPicker">To Date</label>	 
		<input class="width40 endPicker" type="text" name="assignby"  id="assignby" >
		</div>
		<div>
		<label class="width30">Status</label>	 
		<select><option>proceed</option>
				<option>closed</option>
		</select>
		</div>
		<div>
		
		<label class="width30">Opposite person</label>	
		<input class="width40" type="text" name="assignby"  id="assignby" >
		<input type="checkbox" id="assignby" name="assignby"> 
		<input class="width=20" type="button" value="Add Row">
		</div>
	
	 </fieldset>
	 </div>
	
	 </div>
	  
	 <div class="clearfix"></div> 
	 <div class="portlet-content"> 
	  
		 
		<div id="hrm" class="hastable width100">  
				<table id="hastab" class="width100">	
			<thead class="chrome_tab">
			<tr>
				<th style="width:7%">Session No</th>
				<th style="width:7%">Session Date</th>
				<th style="">Lawyer Name</th>
				<th style="height:0px;width:163px;">Submitted Document</th>
				<th style="width:188px;">Received Document</th>
				<th style="width:194px">Expenses</th>
				<th style="width:250px">Session Result</th>
				<th style="width:7%">Status</th>
				<th style="width:1%">Options</th>
				
			</tr>
		</thead>
		<tbody class="tab">
			<%for(int i=1;i<=3;i++) { %>
				  <tr>
				  		
				  		<td class=""></td>
					    <td  class="" style="padding-right:4px;padding-left:110px;width:150px;"></td>
						<td  class="" style="padding-right:97px;width:50px;"></td>
						<td  class="width5" style="width:196px;"></td>
						<td  class="width5" style="width:155px;"></td>
						<td  class="width5" style="width:300px;"></td>
						<td  class="width5"></td>
						<td  class="width5"></td>
						<td  style="width3">
						    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" style="cursor:pointer;" title="Add Record">
						 		 <span class="ui-icon ui-icon-plus"></span>
						     </a>	
						      
							 <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  style="cursor:pointer;" title="Delete Record">
								 <span class="ui-icon ui-icon-circle-close"></span>
							 </a>
							  
						</td> 
				  </tr>	
			 <%}%>  
		 </tbody>
	</table>
					</div>	 
								
		<div class="width50 float-left" id="hrm">
		<fieldset>	
		<div>
		<span>Final Conclusion</span>
		
		<textarea  class="width100"  name="assignby"  id="assignby" ></textarea>
		</div>	
			</fieldset>
			</div>	
			</div>								
			<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" >  
			<div class="portlet-header ui-widget-header float-right discard">Cancel</div> 
				<div class="portlet-header ui-widget-header float-right discard">Edit</div> 
			 	<div class="portlet-header ui-widget-header float-right save-all">Save</div> 					
				</div>
				
				
				</div>
								
			</div>					
								
								
								
								
								
								