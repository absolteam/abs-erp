<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript">
var accountTypeId=0;var idval=0;
var paidArray=new Array();
var receiptEntry ="";
$(function(){
	
	$('.paidDate').datepick({ maxDate: 0}); 
	
	$('.contractPayment').each(function(){
		 var rent=convertToAmount($(this).text());
		 $(this).text(rent); 
		 var tempvar=$(this).attr('id');
		 var splitval=tempvar.split("_"); 
		 var idval=splitval[1]; 
		 $('#contractPayment_'+idval).text(rent); 
	 }); 

	$(".paymentStatus").each(function(){
		var tempvar=$(this).attr("id");
		var idarray = tempvar.split('_');
		var id=Number(idarray[1]);  
		$(this).val(($("#paymentStatusTemp_"+id).val())); 
		if($(this).val()==1){
			paidArray.push($('#contractPaymentId_'+id).val());
		} 
	});
	
	 $('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0);   
	      $('.ui-dialog-titlebar').remove();   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
    
    $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
		});
     
     $('.print').click(function(){
    	 var accountDetails="";
    	 var combinationIds=new Array();
    	 var tenantName=$('#tenantName').html().trim();
 		 var amounts=new Array();
 		 var contractId=$('#contractId').val();
 		 var transactionAmount=0;
 		 var alertId = $('#alertId').val();
 		 var postAmount=new Array();
 		 var postCheque=new Array();
 		 var postDetails="";  
    	 $('.rowid').each(function(){
    		 var tempvar=$(this).attr('id');
     		 var splitval=tempvar.split("_");  
     		 idval=splitval[1]; 
     		 var checkFlag=$('#postedDated_'+idval).attr('checked'); 
     		 var paymentStatus=$('#paymentStatus_'+idval).val(); 
     		 var currentId=$("#contractPaymentId_"+idval).val(); 
     		 if(checkFlag==1){
    			 var amount=$('#chequeAmount_'+idval).text().trim(); 
    			 var cnumber=$('#chequeNumber_'+idval).text().trim(); 
    			 //var paidStatus=$('#postedDated_'+val());
    			 postAmount.push(amount);
    			 postCheque.push(cnumber);
    		 }  
    		 else{
    			 if(paymentStatus==1){
    				 var flag=addPayments(currentId);
        			 if(flag){
        				 combinationIds.push($('#codeCombinationId_'+idval).val()); 
            			 var amount=$('#chequeAmount_'+idval).text().trim(); 
            			 amounts.push(amount.replace(/,/gi,'').toString());
        			 } 
    			 } 
    		 }
    	 });  
		 for(var i=0;i<amounts.length;i++){
			 accountDetails=accountDetails+combinationIds[i]+'@'+amounts[i]+'#';
			 transactionAmount=Number(transactionAmount)+Number(amounts[i]);
		 }   
		 for(var i=0;i<postAmount.length;i++){
			 postDetails=postDetails+postAmount[i]+'@'+postCheque[i]+'#'; 
		 } 
		 //Payment table update process
 		 var paymentStatusStr="";
 		 var paidDate="";
 		 $(".paymentStatus").each(function(){
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var id=Number(idarray[1]);  
			if($("#paymentStatus_"+id).val()==""){
				$("#paymentStatus_"+id).val(null);
				$("#paidDate_"+id).val(null);
			}
			paymentStatusStr+=$("#contractPaymentId_"+id).val()+"#"+$("#paymentStatus_"+id).val()+"#"+$("#paidDate_"+id).val()+","; 
			paidDate+=$("#contractPaymentId_"+id).val()+"#"+$("#paidDate_"+id).val()+",";
		}); 
 		var propertyName = "Rent Income "+$('#propertyName').val().trim();
  		receiptEntry = getReceiptTransaction();   
  		$.ajax({
            type:"POST",
            url:"<%=request.getContextPath()%>/update_contract_payment_final.action",
            async: false,
            data:{contractId:contractId,paymentStatusStr:paymentStatusStr,paidDate:paidDate, postDetails:postDetails},
            dataType: "html",
            cache: false,
            success:function(result){  
            	$.ajax({
                    type:"POST",
                    url:"<%=request.getContextPath()%>/createreceipt_transaction.action",
                    async: false,
                    data:{receiptEntry: receiptEntry, accountName:tenantName, contractId:contractId, propertyName: propertyName, alertId:alertId},
                    dataType: "html",
                    cache: false,
                    success:function(result){  
                   	 window.open('contractreceipt_printview.action?contractId='
                   				+ contractId +'','_blank','width=800,height=800,scrollbars=yes,left=100px');
                   	 window.location.reload();
                    } 
                });
            } 
        }); 
     });

     var getReceiptTransaction = function(){
    	 receiptEntry = "";
    	 var receiptDetails = "";
		 var combinationArray=new Array(); 
		 var paymentStatusArray=new Array();
		 var paymentLines=new Array(); 
		 var actionFlags=new Array();
		 $('.rowid').each(function(){ 
			 var rowId=getRowId($(this).attr('id'));  
			 var paymentLineId = Number($('#contractPaymentId_'+rowId).val());
			 var codeCombinationId = Number($('#codeCombinationId_'+rowId).val());
			 var actionFlag= $('#postedDated_'+rowId).attr('checked');
			 var paymentStatus = $('#paymentStatus_'+rowId).val(); 
			 if(paymentStatus==1)
				 paymentStatus =true; 
			 else
				 paymentStatus =false;
			 if(typeof codeCombinationId != 'undefined'){ 
				 combinationArray.push(codeCombinationId);
				 paymentLines.push(paymentLineId);
				 actionFlags.push(actionFlag);
				 paymentStatusArray.push(paymentStatus);
			 }
		 });
		 for(var j=0;j<combinationArray.length;j++){  
			receiptDetails+=combinationArray[j]+"__"+paymentLines[j]+"__"+actionFlags[j]+"__"+paymentStatusArray[j];
			if(j==combinationArray.length-1){   
			} 
			else{
				receiptDetails+="#@";
			}
		} 
		return receiptDetails; 
     };
     
     $('.discard').click(function(){
    	 window.location.reload();
     }); 
     
 	//Edit Payment Details
		$(".paymentStatus").change(function(){
			slidetab=$(this).parent().parent().get(0);  
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var id=Number(idarray[1]); 
			var paymentStatus=$("#paymentStatus_"+id).val();
			if(paymentStatus==3){
					var chequeAmount=$('#chequeAmount_'+id).text();
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/edit_contract_payment_get.action",  
					 	data: {addEditFlag:"E",id:id},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#chequeAmount').val(chequeAmount);
						}
					});
					
			}else{
				if($("#childRowId_"+id).html()!=null && $("#childRowId_"+id).html()!="")
					$("#childRowId_"+id).remove();
			}
		});
 	
		 
		 $('.rowid').each(function(){
			 var rowId=getRowId($(this).attr('id'));  
			 if(Number($('#paymentStatus_'+rowId).val())==1){
				 $('.codecombination-popup').remove(); 
				 return false;
			 }
		 });
});
function setCombination(combinationTreeId,combinationTree){
	var tempvar =$(tempid).attr('id'); 
    var idVal=tempvar.split("_");
    var rowid=Number(idVal[1]);  
	$('#codecombination-text_'+rowid).text(combinationTree); 
	$('#codeCombinationId_'+rowid).val(combinationTreeId);
} 
function addPayments(currentId){ 
	var flag=true;
	 for(var i=0;i<paidArray.length;i++){
		if(currentId==paidArray[i]){
			flag=false;
			break;
		} 
	 }
	 return flag;
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Contract Receipt</div>
		<div class="portlet-content">
		    <div id="temp-result" style="display:none;"></div> 
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	
			<form id="contractreceipt-form" name="contractdetails">
				<input type="hidden" name="alertId" id="alertId" value="${CONTRACT_TRANSCATION.alertId}"/> 
				<input type="hidden" name="contractId" id="contractId" value="${CONTRACT_TRANSCATION.contractId}"/>  
				<input type="hidden" name="propertyId" id="propertyId" value="${CONTRACT_TRANSCATION.propertyId}"/> 
				<input type="hidden" name="propertyName" id="propertyName" value="${CONTRACT_TRANSCATION.propertyName}"/> 
				<div class="width100 float-left" id="hrm">
					<fieldset>
						<legend>Contact Details</legend>  
						<div class="float-right width20">
							<label class="width30">C.No</label> 
							<span>${CONTRACT_TRANSCATION.contractNumber}</span>
						</div> 
						<div style="margin:5px;">
							<label class="width30">Receipt from Mr/M/s</label> 
							<span id="tenantName">${CONTRACT_TRANSCATION.tenantName}</span>
							<input type="hidden" id="tenantId" value="${CONTRACT_TRANSCATION.description}"/>
						</div>
						<div class="float-right width20">
							<label class="width30">Date</label> 
							<span>${CONTRACT_TRANSCATION.contractDate}</span>
						</div>  
						<div style="margin:5px;">
							<label class="width30">Contract Fee</label> 
							<span>${CONTRACT_TRANSCATION.contractAmount}</span>
						</div>
						<div style="margin:5px;">
							<label class="width30">Contract Deposit</label> 
							<span>${CONTRACT_TRANSCATION.depositAmount}</span>
						</div> 
					</fieldset>
				</div>  
				<div class="clearfix"></div>  
					<div id="hrm" class="hastable width100"> 
					<fieldset>
					<legend>Contract payments</legend>
							<table id="hastab" class="width100"> 
								<thead>
							   <tr>  
							   		<th style="width:5px;">PDC</th>  
									<th>Bank/Cash</th> 
									<th><fmt:message key="re.contract.chequedate"/></th>
									<th><fmt:message key="re.contract.chequeno"/></th>
									<th>Fee Type</th>
									<th>Amount</th>
									<th>Payment Status</th> 
									<th style="display:none;">Paid Date</th> 
									<th>Combination</th> 
							  </tr>
							</thead> 
							<tbody class="tab"> 
								<c:choose>
									<c:when test="${fn:length(requestScope.CONTRACT_TRANSCATION.contractAgreementList)gt 0}">
										<c:forEach items="${CONTRACT_TRANSCATION.contractAgreementList}" var="bean" varStatus="status1" >
										 	<tr class="rowid" id="fieldrow_${status1.index+1}" style="height:25px;"> 
										 	 	<c:choose>
										 	 		<c:when test="${(bean.chequeNumber eq null || bean.chequeNumber eq '') && bean.bankName ne null && bean.bankName ne ''}">
														<td style="width:5px;"><input style="display: none;" readonly="readonly" type="checkbox" name="postDated" id="postedDated_${status1.index+1}"></td>
														<td class="width20" id="bankName_${status1.index+1}">${bean.bankName}</td>
														<td class="width10" id="chequeDate_${status1.index+1}">${bean.chequeDate }</td>	
														<td class="width10" id="chequeNumber_${status1.index+1}">-NA-</td> 
													</c:when>	
										 	 		<c:when test="${bean.chequeNumber ne null && bean.chequeNumber ne ''}">
										 	 			<td style="width:5px;">
										 	 				<c:choose>
																<c:when test="${bean.actionFlag ne true}">
										 	 						<input type="checkbox" disabled="disabled" name="postDated" id="postedDated_${status1.index+1}">
										 	 					</c:when>
										 	 					<c:otherwise>
										 	 						<input type="checkbox" checked="checked" disabled="disabled" name="postDated" id="postedDated_${status1.index+1}">
										 	 					</c:otherwise>
															</c:choose> 
										 	 			</td>
														<td class="width20" id="bankName_${status1.index+1}">${bean.bankName}</td>
														<td class="width10" id="chequeDate_${status1.index+1}">${bean.chequeDate }</td>	
														<td class="width10" id="chequeNumber_${status1.index+1}">${bean.chequeNumber}</td> 
													</c:when>
													<c:otherwise> 
														<td><input style="display: none;" readonly="readonly" type="checkbox" name="postDated" id="postedDated_${status1.index+1}"></td>
														<td class="width20" id="bankName_${status1.index+1}">Cash</td>
														<td class="width10" id="chequeDate_${status1.index+1}">-NA-</td>	
														<td class="width10" id="chequeNumber_${status1.index+1}">-NA-</td>  
													</c:otherwise>  
												</c:choose>
												<td id="feeType_${status1.index+1}">${bean.feeDescription}</td>
												<td class="width20 chequeAmountCalc" id="chequeAmount_${status1.index+1}">${bean.attribute1}</td>
												<td style="display:none"> 
													<input type="hidden" value="${bean.contractPaymentId}" name="actualLineId" id="contractPaymentId_${status1.index+1}"/> 
												</td> 
												<c:choose>
													<c:when test="${bean.actionFlag ne true && bean.paymentVisibility eq 'Y'}">
														<td class="width10"> 
															<input type="hidden" value="${bean.paymentStatus}"  name="paymentStatusTemp_${status1.index+1}" id="paymentStatusTemp_${status1.index+1}"/>
															<select class="paymentStatus" id="paymentStatus_${status1.index+1}">
																<option value="">Select</option>
																<option value="1">Paid</option>
																<option value="2">Pending</option>
																<option value="3">Cancelled</option>
															</select> 
														</td> 
														<td class="width15" style="display:none;"><input type="text" readonly="readonly" name="paidDate" id="paidDate_${status1.index+1}" class="width95 paidDate" value="${bean.createdDate}"></td>
													</c:when>
													<c:otherwise>
														<td class="width10"> 
															<input type="hidden" value="${bean.paymentStatus}" name="paymentStatusTemp_${status1.index+1}" id="paymentStatusTemp_${status1.index+1}"/>
															<select class="paymentStatus" id="paymentStatus_${status1.index+1}" disabled="disabled">
																<option value="">Select</option>
																<option value="1">Paid</option>
																<option value="2">Pending</option>
																<option value="3">Cancelled</option>
															</select> 
														</td> 
														<td class="width15" style="display: none;">
															<input type="text" disabled="disabled"  name="paidDate" id="paidDate_${status1.index+1}" class="width95 paidDate" value="${bean.paidDate}">
														</td>
													</c:otherwise>
												</c:choose>	
												<td>  
													<c:choose>
														<c:when test="${bean.actionFlag ne true && bean.paymentVisibility eq 'Y'}">
															<c:if test="${bean.accountCode ne null && bean.accountCode ne ''}">
																<span>${bean.accountDescription} [${bean.accountCode}]</span>
															</c:if> 
															<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}"/> 
															<!--<span class="float-right width70" id="codecombination-text_${status1.index+1}"></span>
															<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}"/>  
															<span class="button" id="assetcode_${status1.index+1}"  style="position: relative;">
																<a style="cursor: pointer;" id="codecombination-popup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
																	<span class="ui-icon ui-icon-newwin"></span> 
																</a>
															</span>
														--></c:when>
														<c:otherwise> 
															<c:if test="${bean.accountCode ne null && bean.accountCode ne ''}">
																<span>${bean.accountDescription} [${bean.accountCode}]</span>
															</c:if> 
															<%-- <span class="float-right width70" id="codecombination-text_${status1.index+1}"></span> --%>
															<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}"/><!--  
															<span class="button" id="assetcode_${status1.index+1}"  style="position: relative;">
																<a style="cursor: pointer;" id="codecombination-popup_${status1.index+1}" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
																	<span class="ui-icon ui-icon-newwin"></span> 
																</a>
															</span>
													--></c:otherwise>
													</c:choose> 
												</td>  
											</tr>
										</c:forEach>
									  </c:when>
								</c:choose>	  
							</tbody>
							</table>
							</fieldset>
					</div>
				
				<div class="portlet-content">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
						<div class="portlet-header ui-widget-header float-right print">print</div>
					</div>  
				</div>
			</form>
		</div>	
	</div>	
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>	
</div>