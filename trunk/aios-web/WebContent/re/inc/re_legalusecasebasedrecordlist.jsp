<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>

#DOMWindow{	
	width:85%!important;
	height:88%!important;
} 

</style>

<script type="text/javascript">
var id;
var personId="";
var contractId="";
var flatDescription="";
var approvedStatus="";
var oTable; var selectRow=""; var aData="";var aSelectedRelease = [];var s="";var s1=""; var s2="";

$(document).ready(function (){
	var tableName=$('#tableName1').val();
	/* $('#legal').dataTable({ 
 		"sAjaxSource": "legal_usecase_record_details.action?tableName="+tableName,
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Record ID', "bVisible": false},
 			{ "sTitle": $("#tableNameTemp").val()+' Name',"sWidth": "125px"},
 			{ "sTitle": $("#tableNameTemp").val()+' Number',"sWidth": "125px"},
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRowRelease, aDataRelease, iDisplayIndex ) {
 			if (jQuery.inArray(aDataRelease.DT_RowId, aSelectedRelease) !== -1) {
 				$(nRowRelease).addClass('row_selected');
 			}
 		}
 	});	 */
 	//Json Grid
 	//init datatable
 	oTable = $('#legal').dataTable();

 	/* Click event handler */
 	$('#legal tbody tr').live('click', function () { 
 		  if ( $(this).hasClass('row_selected') ) {
 			$(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	        s=aData[0];
 	    }
 	});
 	
 	$('#legal tr').live('dblclick', function () { 
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	        aData =oTable.fnGetData( this );
	        $("#recordId1").val(aData[0]);
	        $("#recordName").val(aData[1]);
	        $("#recordNumber").val(aData[2]);
	        useCaseViewCall();
	        $('#DOMWindowOverlay').trigger('click');
	        //$("table:first").addClass("table-custom-design");
	        //$("table:last").find("tbody:first").addClass("tbody-custom-design");
	        return false;
	    }
	     
	});

 
 	
});
</script>
<body>
	<div id="main-content">
	  <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	  <input type="hidden" id="tableNameTemp" value="${Table_Name}" />
		 <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>${Table_Name} Information</div>
			<div class="portlet-content">
			  <div  class="response-msg error commonErr ui-corner-all" style="width:80%; display:none;"></div>
				<div style="display:none;" class="tempresultfinal">
					<c:if test="${requestScope.bean != null}">
						<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					</c:if>    
				</div>	
				<div>
					<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
						<div class="success response-msg ui-corner-all">${requestScope.succMsg}</div>
					</c:if>
					<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
						<div class="error response-msg ui-corner-all">${requestScope.errMsg}</div>
					</c:if>
				 </div>
				<div id="gridDiv">
					<table class="display" id="legal"></table>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
				<!--<div class="portlet-header ui-widget-header float-right"  id="view"><fmt:message key="re.property.info.view"/></div>	
					<!--<div class="portlet-header ui-widget-header float-right"  id="delete"><fmt:message key="re.property.info.delete"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="edit"><fmt:message key="re.property.info.edit"/></div>
					<div class="portlet-header ui-widget-header float-right"  id="add"><fmt:message key="re.property.info.add"/></div>
				--></div>
				
			</div>
		</div>
		
	</div>
</body>