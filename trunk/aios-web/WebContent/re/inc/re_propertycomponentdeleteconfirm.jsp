<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
	<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  
	$('#componentType').val($('#componentTypeTemp').val());
	$('#buildingId').val($('#buildingIdTemp').val());

	$('.formError').remove(); 
	 $("#propertyComponentEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	 $("#confirm").click(function(){
		 componentId=$('#componentId').val();
			//alert("itemId : "+itemId);
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_property_component.action",
				data: {componentId: componentId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
				{ 
					$("#main-wrapper").html(result); 
				} 
			}); 
			return true;
		});
	 
	
	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_component_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

		
    if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
 	$('#startPicker,#endPicker').datepick({
 		onSelect: customRange, showTrigger: '#calImg'
	}); 
		
});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Property Component</div>
		<form id="propertyComponentEdit">
			<fieldset>
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend>Property Component 2</legend>						
	                           <div>
	                           	<label>From Date<span style="color:red">*</span></label>
	                           	<input type="text" name="fromDate" class="width30" id="startPicker" value="${bean.fromDate }" readonly="readonly">
	                           </div>		
							<div>
								<label>To Date<span style="color:red">*</span></label>
								<input type="text" name="toDate" class="width30" id="endPicker" value="${bean.toDate }" readonly="readonly">
							</div>																
							<div>
								<label>Total Area<span style="color:red">*</span></label>
								<input type="text" name="totalArea" id="totalArea" value="${bean.totalArea }" class="width30">
							</div>
					</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend> Property Component 1</legend>
							<div>
								<label>Id No.</label>
								<input type="text" name="componentId" id="componentId" value="${bean.componentId }" class="width30" disabled="disabled" />
							</div>
							<div><label>Building Id No.<span style="color:red">*</span></label>
								<input type="hidden" id="buildingIdTemp" value="${bean.buildingId }" />
								<select id="buildingId" class="width30 tooltip" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.itemList ne null}">
										<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
											<option value="${building.buildingId }">${building.buildingName }</option>
										</c:forEach>
									</c:if>
								</select>
							</div>	
							<div>
								<label>Component Type<span style="color:red">*</span></label>
								<input type="hidden" id="componentTypeTemp" value="${bean.componentType }" />"
								<select id="componentType" class="width30" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.lookupList ne null}">
										<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
											<option value="${compType.componentType }">${compType.componentTypeName }</option>
										</c:forEach>
									</c:if>
								</select>
							</div>
							<div>
								<label>No Of Component<span style="color:red">*</span></label>
								<input type="text" id="noOfComponent" value="${bean.noOfComponent }" class="width30" />
							</div>			
							<div>
								<label>No Of Flat in Component<span style="color:red">*</span></label>
								<input type="text" id="noOfFlat" value="${bean.noOfFlat }" class="width30" />
							</div>							
				 	</fieldset> 
				</div>
			</fieldset>
		</form>
		<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:1063px;"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Feature Details</div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10">Line No.</th>
			            					<th class="width20">Feature Code</th>
			            					<th class="width20">Features</th>
			            					<th class="width10">Measurement</th>
			            					<th class="width10">Remarks</th>
			            					<th style="width:5%;">Options</th>
										</tr>
									</thead>  
									<tbody class="tabFeature" style="">	
										<c:forEach items="${requestScope.result1}" var="result" >
											<tr>  
												<td class="width10">${result.lineNumber }</td>	 
												<td class="width20">${result.featureCode }</td>
												<td class="width20">${result.features }</td>	
												<td class="width10">${result.measurements }</td> 
												<td class="width10">${result.remarks }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result.propertyInfoFeatureId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>	
												<td style="display:none;"></td>	 
											</tr>  
										</c:forEach>
									 	<%for(int i=0;i<=2;i++) { %>
											 <tr>  
												<td class="width10"></td>	 
												<td class="width20"></td>
												<td class="width20"></td>	
												<td class="width10"></td> 
												<td class="width10"></td>
												<td style="display:none"> 
													<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
													<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
													<input type="hidden" value="" name="tempLineId" id="tempLineId"/>
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="display:none;cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>	
												<td style="display:none;"></td>	 
											</tr>  
							  		 	<%}%>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
					<div class="portlet-header ui-widget-header float-right cancelrec" id="cancel" style=" cursor:pointer;">Cancel</div> 
					<div class="portlet-header ui-widget-header float-right headerData" id="confirm" style="cursor:pointer;">Confirm</div>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
		</div>				
	</div>
</div>