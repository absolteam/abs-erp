<tr class="rowid">  
	<td class="width10">${requestScope.lineNumber}</td>	 
	<td class="width10"></td>
	<td class="width10"></td>
	<td class="width10"></td> 
	<td class="width10"></td>
	<td class="width10"></td>
	<td class="width10"></td>
	<td class="width10"></td>
	<td style="display:none"> 
		<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
		<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
		<input type="hidden" value="" name="tempLineId" id="tempLineId"/>
	</td>  
	<td style=" width:5%;"> 
				<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addAssetNew" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
	   	</a>	
	   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editAssetNew"  style="display:none;cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delAssetNew" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
			<span class="processing"></span>
		</a>
	</td>  
	<td style="display:none;" class="building_id"></td>	
	<td style="display:none;" class="asset_id"></td>		 
</tr> 
<script type="text/javascript">
$(function() {

	//Adding Asset Details
 	$('.addAssetNew').click(function(){ 
 		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				//alert($(this).parent().parent().get(0).tagName);
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
				
				//alert(slidetabrev);
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_add_contract_asset_redirect.action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 var temp1= $($(slidetab).children().get(0)).text();
						
					 $($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					 $($($(slidetab).children().get(9)).children().get(0)).hide();	//Add Button
			         $($($(slidetab).children().get(9)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(9)).children().get(3)).show();	//Processing Button
			         openFlag=1;
			         $('#loading').fadeOut();
					}
				});
			}else{
				return false;
			}
 		}	
	});

	//Edit Asset Details
	$(".editAssetNew").click(function(){
		if(openFlag==0){
			if($('#MaintainenceContractEdit').validationEngine({returnIsValid:true})){
				$('#loading').fadeIn();
				slidetab=$(this).parent().parent().get(0); 
				$('.error').hide();
		        $('.childCountErr').hide();
		        $('#warningMsg').hide();
		        
				//actualID=$($($(slidetab).children().get(8)).children().get(0)).val();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_edit_contract_asset_redirect.action",  
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $($($(slidetab).children().get(9)).children().get(1)).hide();	//Edit Button
			         $($($(slidetab).children().get(9)).children().get(2)).hide(); //Delete Button
			         $($($(slidetab).children().get(9)).children().get(3)).show();	//Processing Button
						
					 
					 var temp1= $($(slidetab).children().get(0)).text();
					 var temp2= $($(slidetab).children().get(1)).text();
					 var temp3= $($(slidetab).children().get(2)).text();
					 var temp4= $($(slidetab).children().get(3)).text();
					 var temp5= $($(slidetab).children().get(4)).text();
					 var temp6= $($(slidetab).children().get(5)).text();
					 var temp7= $($(slidetab).children().get(6)).text();
					 var temp8= $($(slidetab).children().get(7)).text();
					 //var temp3=$($($(slidetab).children().get(2)).children().get(0)).attr('checked');
					   var temp9= $($(slidetab).children().get(10)).text();
					  var temp10= $($(slidetab).children().get(11)).text();
					  var temp11= $($(slidetab).children().get(12)).text();
		
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(1)).children().get(1)).val(temp1);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(2)).children().get(1)).val(temp9);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(3)).children().get(1)).val(temp11);
					$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(4)).children().get(1)).val(temp4);
				  	$($($($($($(trval).children().children().get(1)).children().get(1)).children().get(0)).children().get(5)).children().get(1)).val(temp5);	
					
				  	
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(1)).children().get(1)).val(temp6);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(2)).children().get(1)).val(temp7);
				  	$($($($($($(trval).children().children().get(1)).children().get(0)).children().get(0)).children().get(3)).children().get(1)).val(temp8);
				  	openFlag=1;
					$('#loading').fadeOut();
		
					}
				});
			}else{
				return false;
			}
		}	
	});
	
	$('.delAssetNew').click(function(){
		if(openFlag==0){
	 		slidetab=$(this).parent().parent().get(0); 
	 		$('.error').hide();
	        $('.childCountErr').hide();
	        $('#warningMsg').hide();
	        actualID=$($($(slidetab).children().get(8)).children().get(0)).val();
			 var actionflag="D";
			 var tempLineId=$($($(slidetab).children().get(8)).children().get(2)).val();
			  var trnValue=$('#transURN').val();
			  var maintenanceId=$('#maintenanceId').val(); 
			 var flag = false;
			//alert("Del Row - trnValue : "+trnValue+"  actualID :"+actualID+" tempLineId: "+tempLineId); 
			if((actualID != null && actualID !='' && actualID!=undefined) || (tempLineId != null && tempLineId !='' && tempLineId!=undefined)){
				$('#loading').fadeIn();
		       	 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/edit_delete_contract_asset.action", 
				 	async: false,
				 	data: {actualLineId: actualID, actionFlag:actionflag, maintenanceId: maintenanceId,trnValue: trnValue,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){	
				 		$('.formError').hide();
		                 $('#temperror').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	 $("#transURN").val($('#objTrnVal').html());
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                              flag = true;
					    	else{	
						    	flag = false;
						    	$("#temperror").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
				 	},  
				 	error:function(result){ 
				 		 $('.tempresult').html(result);
		                      $("#warningMsg").hide().html($('#returnMsg').html()).slideDown(); 
		                      $('#loading').fadeOut();
		                      return false;
				 	} 
		       	});
		       	if(flag==true){
		       		 $(this).parent().parent('tr').remove();
		       		 var childCount=Number($('#childCount').val());
		       		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount);
		       		 }
			   }
			}else{
				$("#warningMsg").css({color: "#0077CC", fontWeight:"bold", fontSize:"12px"}); 
				$('#warningMsg').hide().html("Please insert record to delete").slideDown();
				return false; 
			}
		}	
 	});	
});	
</script>