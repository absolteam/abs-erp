<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
var id;

$(document).ready(function (){
		 
	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-ownerInformation_search.action?ownerName="+ownerName+'&Proffession ='+Proffession+'& fromDate ='+fromDate+'& toDate ='+toDate+'& uaeIdNo ='+uaeIdNo+'&address1 ='+address1+'&passportNo ='+passportNo+'&mobileNo='+mobileNo, 
		 datatype: "json", 
		 colNames:['Owner Id','<fmt:message key="re.owner.info.ownername"/>','<fmt:message key="re.owner.info.proffession"/>','<fmt:message key="re.owner.info.fromdate"/>','<fmt:message key="re.owner.info.todate"/>','<fmt:message key="re.owner.info.birthdate"/>','<fmt:message key="re.owner.info.civilidno"/>','<fmt:message key="re.owner.info.address1"/>','<fmt:message key="re.owner.info.passportno"/>','<fmt:message key="re.owner.info.mobileno"/>','<fmt:message key="re.owner.info.landlineno"/>','<fmt:message key="re.owner.info.emailid"/>'], 
		 colModel:[ 
					{name:'ownerId',index:'OWNER_ID', width:100,  sortable:true},
					{name:'ownerName',index:'NAME', width:100,  sortable:true},
			  		{name:'Proffession',index:'PROFESSION', width:150,sortable:true, sorttype:"data"} ,
			  		{name:'fromDate',index:'FROM_DATE', width:100,  sortable:true},
			  		{name:'toDate',index:'TO_DATE', width:150,sortable:true, sorttype:"data"},
			  		{name:'birthDate',index:'BIRTH_DATE', width:150,sortable:true, sorttype:"data"},
			  		{name:'uaeIdNo',index:'UAE_ID_NO', width:150,sortable:true, sorttype:"data"},
			  		{name:'address1',index:'ADDRESS_LINE_1', width:150,sortable:true, sorttype:"data"}, 
			  		{name:'passportNo',index:'PASSPORT_NO', width:150,sortable:true, sorttype:"data"},
			  		{name:'mobileNo',index:'MOBILE_NO', width:150,sortable:true, sorttype:"data"},
			  		{name:'landlineNo',index:'LANDLINE_NO', width:150,sortable:true, sorttype:"data"},
			  		{name:'emailId',index:'EMAIL_ID', width:150,sortable:true, sorttype:"data"},
			 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'OWNER_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:"<fmt:message key="re.owner.info.ownerinformation"/>"
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["ownerId"]);
});
 	</script>
 	 </head>
 	 <body>
 	<table id="list2"></table>  
 	<div id="pager2"> </div> 
 	</body>






 	
