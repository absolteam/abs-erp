<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">   
<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$.jgrid.no_legacy_api = true;
$.jgrid.useJSON = true;
var startpick;
var slidetab;
var actualID;
var openFlag=0;
$(function(){

	$('#buildingId').val($('#buildingIdTemp').val());
	 $("#offerFormEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	 $("#comments").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#comments").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#comments").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 	
	
	$('.formError').remove();

	//Approve Offer Form details
	$("#approve").click(function(){
		if($('#offerFormEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var offerId=$('#offerId').val();
				var buildingId=$('#buildingId').val();
				var customerId=$('#customerId').val();
				var address= $('#address').val();
				var offerDate=$('#defaultPopup').val();
				var offerValidityDays=$('#offerValidityDays').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var extensionDays=$('#extensionDays').val();
				

				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				
				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var workflowStatus="Offer Form Approved by Supervisor";
				var functionId=Number($('#functionId').val());
				var functionType="approve";
				var notificationId=$('#notificationId').val();
				var workflowId=$('#workflowId').val();
				var mappingId=$('#mappingId').val();
				var comments=$('#comments').val();

				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/offer_form_supervisor_approval.action",
						data: {offerId: offerId, buildingId: buildingId, customerId: customerId,presentAddress: address,offerDate: offerDate,
								offerValidityDays: offerValidityDays,fromDate: fromDate,toDate: toDate,extensionDays: extensionDays,
								headerFlag: headerFlag, trnValue: transURN,sessionpersonId:sessionpersonId,
								companyId:companyId,mappingId:mappingId,workflowStatus:workflowStatus,functionId:functionId,
								functionType:functionType,notificationId:notificationId,workflowId:workflowId,comments:comments},  
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
									$('.formError').hide();
									$('.commonErr').hide();
									$('.tempresultfinal').fadeOut();
						 		 	$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$('.tempresultfinal').fadeIn();
										$('#loading').fadeOut(); 
										$('#approve').remove();	
										$('#reject').remove();	
										$('#close').fadeIn();
										$('.commonErr').hide();
									} else{
										$('.tempresultfinal').fadeIn();
									}
								},  
							error:function(result){
									$('.tempresultfinal').fadeOut();
									$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$("#main-wrapper").html(result); 
									} else{
										$('.tempresultfinal').fadeIn();
									};
							 	}  
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	//Reject Offer Form details
	$("#reject").click(function(){
		if($('#offerFormEdit').validationEngine({returnIsValid:true})){
			var childCount = $('#childCount').val(); 
			if(childCount!="" && childCount > 0){
				var offerId=$('#offerId').val();
				var buildingId=$('#buildingId').val();
				var customerId=$('#customerId').val();
				var address= $('#address').val();
				var offerDate=$('#defaultPopup').val();
				var offerValidityDays=$('#offerValidityDays').val();
				var fromDate=$('#startPicker').val();
				var toDate=$('#endPicker').val();
				var extensionDays=$('#extensionDays').val();
				

				var headerFlag=$("#headerFlag").val();
				var transURN = $('#transURN').val();
				//alert(" transURN : "+transURN+" exchangeRate : "+exchangeRate);
				
				//Work Flow Variables
				var sessionpersonId = $('#sessionpersonId').val();
				var companyId=Number($('#headerCompanyId').val());
				var workflowStatus="Offer Form Rejected by Supervisor";
				var functionId=Number($('#functionId').val());
				var functionType="reject";
				var notificationId=$('#notificationId').val();
				var workflowId=$('#workflowId').val();
				var mappingId=$('#mappingId').val();
				var comments=$('#comments').val();

				if(true){
					$('#loading').fadeIn();
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/offer_form_supervisor_approval.action",
						data: {offerId: offerId, buildingId: buildingId, customerId: customerId,presentAddress: address,offerDate: offerDate,
								offerValidityDays: offerValidityDays,fromDate: fromDate,toDate: toDate,extensionDays: extensionDays,
								headerFlag: headerFlag, trnValue: transURN,sessionpersonId:sessionpersonId,
								companyId:companyId,mappingId:mappingId,workflowStatus:workflowStatus,functionId:functionId,
								functionType:functionType,notificationId:notificationId,workflowId:workflowId,comments:comments},  
				     	async: false,
						dataType: "html",
						cache: false,
						success:function(result){
									$('.formError').hide();
									$('.commonErr').hide();
									$('.tempresultfinal').fadeOut();
						 		 	$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$('.tempresultfinal').fadeIn();
										$('#loading').fadeOut(); 
										$('#approve').remove();	
										$('#reject').remove();	
										$('#close').fadeIn();
										$('.commonErr').hide();
									} else{
										$('.tempresultfinal').fadeIn();
									}
								},  
							error:function(result){
									$('.tempresultfinal').fadeOut();
									$('.tempresultfinal').html(result);
									if($("#sqlReturnStatus").val()==1){
										$("#main-wrapper").html(result); 
									} else{
										$('.tempresultfinal').fadeIn();
									};
							 	}  
					});
					 
				}else{
					$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
					return false; 
				}
			}else{
				$('.childCountErr').hide().html("Please insert data of Offer Lines").slideDown();
				return false; 
			}
		 }else{
			  return false;
		  }
	});

	$('#close').click(function(){
		$('.backtodashboard').trigger('click');
	  });

	
	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/offer_form_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 
}); 
	
</script>
<div id="main-content">
 	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.offerform"/></div>
		<div class="portlet-content">
			<form id="offerFormEdit">
				<fieldset>
	 				<div style="display:none;" class="tempresultfinal">
						<c:if test="${requestScope.bean != null}">
							<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
							 <c:choose>
								 <c:when test="${bean.sqlReturnStatus == 1}">
									<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
								<c:when test="${bean.sqlReturnStatus != 1}">
									<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
								</c:when>
							</c:choose>
						</c:if>    
					</div>	
					<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
		    		<input type="hidden" id="notificationType" name="notificationType" value="${requestScope.notificationType}"/>
					<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
					<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
					<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/> 
		 			<div style="float:left;width:48%;" id="hrm" class="">
					 	<fieldset>
							<div>
								<label class="width30"><fmt:message key="re.offer.offerno"/></label>	 
								<input class="width40" type="text" name="offerNumber" value="${bean.offerNumber }" id="offerNumber" disabled="disabled">
								<input class="width40" type="hidden" name="offerId" value="${bean.offerId }" id="offerId" disabled="disabled" >
								<input class="width40" type="hidden" name="sessionpersonId" value="${bean.sessionpersonId }" id="sessionpersonId" disabled="disabled" >
							</div>
						 	<div>
						 		<label class="width30"><fmt:message key="re.offer.buildingname"/></label>
						 		<input class="width40" type="hidden" name="buildingIdTemp" value="${bean.buildingId }" id="buildingIdTemp" disabled="disabled" >
								<select id="buildingId" class="width40 validate[required]" disabled="disabled" >
									<option value="">-Select-</option>
									<c:if test="${requestScope.itemList ne null}">
										<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
											<option value="${building.buildingId }">${building.buildingName }</option>
										</c:forEach>
									</c:if>
								</select>
							</div>	
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.customername"/></label>
							 	<input class="width40 validate[required]" type="text" name="customerName" id="customerName" value="${bean.customerName }" disabled="disabled" >
							 	<input class="width40" type="hidden" name="customerId" id="customerId" value="${bean.customerId }" disabled="disabled" >
							</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.address"/></label>
							 	<input class="width40" type="text" name="address" value="${bean.presentAddress }" id="address" disabled="disabled">
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.mobileno"/></label>
							 	<input class="width40" type="text" name="mobileNo" value="${bean.mobileNo }" id="mobileNo" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.landLineNo"/></label>
							 	<input class="width40" type="text" name="landLineNo" value="${bean.landLineNo }" id="landLineNo" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.email"/></label>
							 	<input class="width40" type="text" name="emailId" value="${bean.emailId }" id="emailId" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.country"/></label>
							 	<input class="width40" type="text" name="country" value="${bean.countryName }" id="country" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.state"/></label>
							 	<input class="width40" type="text" name="state" value="${bean.stateName }" id="state" disabled="disabled" >
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.city"/></label>
							 	<input class="width40" type="text" name="city" value="${bean.cityName }" id="city" disabled="disabled" >
						 	</div>
					 	</fieldset>
		 			</div>
	 				<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:100px;">
							<div>
							   <label class="width30"><fmt:message key="re.offer.offerdate"/></label>
						 	  <input class="width30 validate[required]" id="defaultPopup" type="text" name="offerDate" value="${bean.offerDate }" disabled="disabled"/>
						 	</div>
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.customertype"/></label>
								<input class="width30 taskDueDate" id="customerType" type="text" name="customerType" value="${bean.customerTypeId }"  disabled="disabled">
							</div> 
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.offervaliditydays"/></label>
								<input class="width10" id="offerValidityDays" type="text"  name="offerValidityDays" disabled="disabled" value="${bean.offerValidityDays }">
							</div> 
							<div>
							 	<label class="width30"><fmt:message key="re.offer.extensiondays"/></label>
								<input class="width10 " id="extensionDays" type="text" name="extensionDays" value="${bean.extensionDays }">
							</div> 
				 		</fieldset>
				 	</div>
	 				<div class="width50 float-left" id="hrm">
						<fieldset style="min-height:140px;">
							<legend><fmt:message key="re.offer.expectedcontractperiod"/></legend>
							<div>
							 	<label class="width30"><fmt:message key="re.offer.from"/></label>
								<input class="width30 validate[required]" id="startPicker" type="text" name="fromDate" value="${bean.fromDate }" disabled="disabled"/>
							</div> 
						 	<div>
							 	<label class="width30"><fmt:message key="re.offer.to"/></label>
								<input class="width30 validate[required]" id="endPicker" type="text" name="toDate" value="${bean.toDate }" disabled="disabled"/>
							</div> 
						</fieldset>
					</div>
				</fieldset>
			<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.offer.amountdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important;  display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important;  display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.offer.lineno"/></th>
				            					<th class="width20"><fmt:message key="re.offer.flatnumber"/></th>
				            					<th class="width20"><fmt:message key="re.offer.rentamount"/></th>
				            					<th class="width20"><fmt:message key="re.offer.contractamount"/></th>
				            				</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.result1}" var="result" >
											 	<tr class="offer">  	 
													<td class="width20">${result.lineNumber}</td>
													<td class="width20">${result.flatNumber}</td>	
													<td class="width20">${result.rentAmount}</td> 
													<td class="width20">${result.contractAmount}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.offerDetailId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style="display:none;" class="owner_id">${result.propertyFlatId}</td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	</c:forEach>	
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="clearfix"></div>
								 <div class="width50 float-right" style="margin:5px;" id="hrm">
									<fieldset>
										<legend>Supervisor Comments<span style="color:red">*</span></legend>
										<textarea id="comments" class="width100 validate[required]"></textarea>
									</fieldset>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
						</div> 
					
				</div>
			</div>
			</form>
		</div>
		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			<div class="portlet-header ui-widget-header float-right"  id="close"><fmt:message key="cs.common.button.close"/></div>
			<div class="portlet-header ui-widget-header float-right" id="reject"><fmt:message key="cs.sendingfax.button.reject"/></div>
			<div class="portlet-header ui-widget-header float-right" id="approve"><fmt:message key="cs.sendingfax.button.approve"/></div>
		</div>
		</div>
 </div>


	 	
	
	 	 	
