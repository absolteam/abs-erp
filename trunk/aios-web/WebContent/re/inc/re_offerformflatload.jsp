<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <div class="selectable width100" id="hrm"> 
     <div class="float-left width100" style="background-color:#dfdfdf;padding:5px;">
     	<div class="width20 float-left"><span>Flat Number</span></div> 
     	<div class="width20 float-left"><span>Rent Amount</span></div>
     	<div class="width20 float-left"><span>Floor No</span></div>
     	<div class="width20 float-left"><span>No of Rooms</span></div>
     	<div class="width20 float-left"><span>Component Type</span></div> 
     </div> 
	<c:choose>
	     <c:when test="${itemList ne null}">
			<c:forEach items="${itemList}" var="item" varStatus="status"> 
			 	 <div class="selectlist float-left width100">   
			 	 	 <input type="hidden" value="${item.propertyFlatId}"/>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0 ;"><span>${item.flatNumber}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;"><span>${item.rentAmount}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0 ;"><span>${item.floorNo}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;"><span>${item.noOfRooms}</span></div>
			 	 	 <div class="width20 float-left" style="padding:1% 0  1% 0;"><span>${item.componentTypeName}</span></div>
		         </div>
		   </c:forEach>
		</c:when>
		<c:otherwise>
			<div class="width50 float-left" style="padding:1% 0  1% 0;"><span>No Records Available</span></div>
		</c:otherwise>
  </c:choose>
</div>
<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='MM';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyy';
  $(function(){
	  $(".ui-dialog-titlebar").remove();
	   $(".selectlist").click(function(){
            $(temp).css('background-color','#fff');
            $(this).css('background-color','#F39814');
            temp=this;
        });
	   $(".selectlist").mousedown(function(){
            $(this).css('background-color','#FECA40');
        });
	    $('.selectlist').click(function(){
	       	$('#propertyFlatId').val($($(this).children().get(0)).val());
	       	$('#flatNumber').val($($($(this).children().get(1)).children().get(0)).html());
	       	$('#rentAmount').val($($($(this).children().get(2)).children().get(0)).html());
	       	var rentFees=Number($('#rentFees').val());
	       	if(rentFees !=1){
				//Date Difference Calculation
		       	fromDateMonth=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
				toDateMonth=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
				fromDateYear=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),usrtoFormat));
				toDateYear=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),usrtoFormat));
		       	rentAmountMonth=Number($($($(this).children().get(2)).children().get(0)).html())/12;
		       	monthDiff=(toDateMonth-fromDateMonth)+1;
		       	yearDiff=toDateYear-fromDateYear;
		       	monthCount=monthDiff+(12*yearDiff);
		       	contractAmountThis=Math.ceil(rentAmountMonth*(monthCount));
		       	$('#contractAmount').val(contractAmountThis);
	       	}else{
	       		$('#contractAmount').val($('#rentFeesAmount').val());
	       	}
           
    });  
}); 
</script>