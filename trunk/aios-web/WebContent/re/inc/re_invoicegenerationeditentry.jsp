<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="6" class="tdidentity">
<div id="errMsg"></div>
<form name="invoiceGenerationEditEntry" id="invoiceGenerationEditEntry">		
<div class="width48 float-right" id="hrm">
	<fieldset>					 
		<legend></legend>
		<div>
			<label class="width30">Functional Amount</label>
			<input type="text" name="linesFunctionalAmount" id="linesFunctionalAmount" class="width50" disabled="disabled" >
		</div>
		<div>
			<label class="width30">Note<span class="mandatory">*</span></label>
			<input type="text" name="note" id="note" class="width50  validate[required]">
		</div>
		<div id="calcError" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div> 
	</fieldset>  
</div>	 
<div class="width50 float-left" id="hrm" >
		<fieldset>
			<legend></legend> 
			<div>
				<label class="width30">Line Number<span class="mandatory">*</span></label>
				<input name="lineNumber" id="lineNumber" class="width50  validate[required,custom[onlyNumber]]" disabled="disabled"/>
			</div>
			<div>
				<label class="width30">Revenue Code<span class="mandatory">*</span></label>
				<input name="liabilityAccountCode" id="liabilityAccountCode" class="width50 validate[required]"  readonly="readonly" />
				<input type="hidden" id="reterive_liability_account_action"/>
				<span class="button" style="margin-top:3px;">
					<a  id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
						<span class="ui-icon ui-icon-newwin"></span> 
					</a>
				</span>
			</div>
			<div>
				<label class="width30">Amount<span class="mandatory">*</span></label>
				<input type="text" name="linesAmount" id="linesAmount" class="width50 calcChild validate[required,custom[onlyFloat]]" >
			</div>
		</fieldset> 
		<div id="othererrors" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div> 
</div> 
<div class="clearfix"></div> 
	<div class="float-right buttons ui-widget-content ui-corner-all"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate"><fmt:message key="accounts.banktransaction.button.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  ><fmt:message key="accounts.banktransaction.button.save"/></div>
	</div>
</form>
</td>  
	
 </tr>
 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
	<div class="common_result width100"></div>
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
</div>
<script type="text/javascript">
var trval=$('.tdidentity').parent().get(0);
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';	var checkFlag=false;var tempPay="";var paycheckFlag=false;
$(function(){ 
	$('.formError').remove(); 
	$("#invoiceGenerationEditEntry").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
			
	$('.cancel').click(function(){ 
		$('.formError').remove();
		$($(this).parent().parent().parent().get(0)).remove();
		$($($(slidetab).children().get(6)).children().get(0)).hide(); 
		$($($(slidetab).children().get(6)).children().get(1)).show();	 
        $($($(slidetab).children().get(6)).children().get(2)).show(); 
        $($($(slidetab).children().get(6)).children().get(3)).hide(); 
        $('#openFlag').val(0);
        $('#common-popup').dialog('destroy');		
		$('#common-popup').remove();
	});
	$('#add').click(function(){  
		if($('#invoiceGenerationEditEntry').validationEngine({returnIsValid:true})){
		  	$('#loading').fadeIn();
			var tempObject=$(this);	
			invoiceId=$('#invoiceId').val();
			var lineNumber=$('#lineNumber').val();
			var linesAmount=$('#linesAmount').val();
			var linesFunctionalAmount=$('#linesFunctionalAmount').val();
			var note=$('#note').val();

			revenueCodeDesc= $('#liabilityAccountCode').val();
			//Accounting Variables
			var liabilityAccountCode = $('#liabilityAccountCode').val();

			var codeArray=new Array();
			codeArray=liabilityAccountCode.split('---');
			liabilityAccountCode=codeArray[0]; 

			var trnVal =$('#transURN').val(); 

			var actualLineId=$($($(slidetab).children().get(5)).children().get(0)).val();
			var uflag=$($($(slidetab).children().get(5)).children().get(1)).val();
			var tempLineId=$($($(slidetab).children().get(5)).children().get(2)).val();
		 
			var flag=false;
			if(true){  
		      		$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/re_invoice_generation_edit_update.action", 
				 	async: false,
				 	data: {invoiceId: invoiceId,lineNumber: lineNumber,revenueCode: liabilityAccountCode,linesAmount: linesAmount,linesFunctionalAmount: linesFunctionalAmount,
					 	note: note,trnValue: trnVal,actualLineId: actualLineId,actionFlag: uflag,tempLineId: tempLineId},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
				 		$('.formError').hide();
		                         $('#othererrors').hide(); 
						$('.tempresult').html(result); 
					     if(result!=null){
					    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
		                                  flag = true;
					    	else{	
						    	flag = false;
						    	$('#othererrors').show();
						    	 $("#othererrors").hide().html($('#returnMsg').html()).slideDown();
					    	} 
					     }
					     $('#loading').fadeOut();
					     $('#openFlag').val(0);
					     $('#common-popup').dialog('destroy');		
						 $('#common-popup').remove();
				 	},  
				 	error:function(result){
					 	alert("err "+result);
				 		 $('.tempresult').html(result);
		                          $("#othererrors").hide().html($('#returnMsg').html()).slideDown(); 
		                          $('#loading').fadeOut();
		                          return false;
				 	} 
		       	}); 
	      		if(flag==true){
	      			$($(slidetab).children().get(0)).text(lineNumber); 
	        		$($(slidetab).children().get(1)).text(revenueCodeDesc); 
	        		$($(slidetab).children().get(2)).text(linesAmount);
	        		$($(slidetab).children().get(3)).text(linesFunctionalAmount);
	        		$($(slidetab).children().get(4)).text(note);
				   
				  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
				  	$($($(slidetab).children().get(6)).children().get(0)).hide(); 
					$($($(slidetab).children().get(6)).children().get(1)).show();	 
			        $($($(slidetab).children().get(6)).children().get(2)).show(); 
			        $($($(slidetab).children().get(6)).children().get(3)).hide(); 
		
			        $($($(slidetab).children().get(5)).children().get(0)).val($('#objLineVal').html());
			  		$($($(slidetab).children().get(5)).children().get(2)).val($('#objTempId').html());
			  		$("#transURN").val($('#objTrnVal').html());
		
					$.fn.globeTotal();
	  		 	} 
			}else{
				$("#othererrors").hide().html("Payable amount should be lesser or equal to cheque amount.").slideDown(); 
				return false;
			}    
		 }else{
		  return false;
		 }
 	});

	//Functional Amount Calculation
	$('#linesAmount').blur(function(){ 
		lmt=$('#linesAmount').val();
		loanamt="";
		ccode=$('#currencyCode :selected').text(); 
		codeSpiltedVal=$('#currencyCode :selected').val();  
		myArray=codeSpiltedVal.split("@@");   
		codeSpiltedVal=myArray[1];  
		if(lmt!=null && lmt!='' && ccode=="AED"){ 
			exrate=Number(1); 
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			exrate=Number(exrate).toFixed(codeSpiltedVal);  
			
			fnamt=(loanamt*exrate).toFixed(codeSpiltedVal); 
			$('#exchangeRateVal').val(exrate);
			$('#linesAmount').val(loanamt);  
			$('#linesFunctionalAmount').val(fnamt);
		}else{
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			$('#exratetype').attr('disabled',false);
			exrate=$('#exchangeRateVal').val(); 
		}
	});

	$('.common-popup').click(function(){	
		$('.formError').remove();	 
	    tempid=$(this).parent().get(0);  
	    accountType=$(this).attr('id'); 
	   	var commonaction="";
		actionVal="";
		var ledgerId=Number($('#headerLedgerId').val()); 
		commonaction=$($(tempid).siblings().get(2)).attr('id');
	      
		if(commonaction=="reterive_liability_account_action"){
   	    		commonaction="common_retrieveaccount";   
   	    		actionVal="vendor";
		}
		else if(commonaction=="reterive_advance_account_action"){
   	    		commonaction="common_retrieveaccount"; 
   	    		actionVal="advance";
		}else if(commonaction=="reterive_clearning_account_action"){
   	    		commonaction="common_retrieveaccount";
   	    		actionVal="clearing";
   	  	}
   	      
		$('#common-popup').dialog('open');
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+commonaction+".action", 
			 	async: false, 
			 	data:{ledgerId:ledgerId, accountType:accountType},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('.common_result').html(result); 
				},
				error:function(result){ 
					 $('.common_result').html(result); 
				}
		}); 
	});	 
				
	$('#common-popup').dialog({ 
		autoOpen: false,
		minwidth: 'auto',
		width:700,
		bgiframe: false,
		modal: false,
		buttons: {
			"Cancel": function() {  
					$(this).dialog("close");  
			}, 
			"Ok": function(){
				if(actionVal!=null && actionVal!=""){ 
					var s = $("#list2").jqGrid('getGridParam','selrow');
					if(s == null){
						alert("Please select one account code");
						return false;
					}
					else{			
						var s2 = $("#list2").jqGrid('getRowData',s);   
						var commonCodes=s2.commonCode;
						var commonDescs=s2.commonDesc;  
						
						var joinCode= commonCodes+"---"+commonDescs;
						 
						if(actionVal=="vendor")
							$('#liabilityAccountCode').val(joinCode);
						else if(actionVal=="advance") 
							$('#advanceAccountCode').val(joinCode);
						else if(actionVal=="clearing") 
							$('#clearningAccountCode').val(joinCode);
					} 
				}
				$(this).dialog("close");  
			}				
		}	
	});  
});
 </script>	