<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr style="background-color: #F8F8F8;">
<td colspan="5" class="tdidentity">
<div id="errMsg"></div>
<form name="propertyInfoAddEdit" id="propertyInfoAddEdit" style="position: relative;">	 
<div class="width45 view float-right" id="hrm" style="margin-right:15px;">
<fieldset>					 
		<legend><fmt:message key="re.property.info.percentageinfo"/></legend>
		<div>
			<label><fmt:message key="re.property.info.percentage"/><span class="mandatory">*</span></label>
			<input type="text" name="percentage" id="percentage" class="width50 percentage validate[required,custom[number]]">
			<input type="hidden" name="percentageTemp" id="percentageTemp" class="width50 ">
		</div>
	</fieldset> 
	<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>  
</div>	
<div class="width50 view float-left" id="hrm" >
		<fieldset>
			<legend><fmt:message key="re.property.info.ownerinformation"/></legend> 
			<div>
				<label><fmt:message key="re.property.info.ownername"/><span class="mandatory">*</span></label>
				<input type="text" name="ownerName" id="ownerName" disabled="disabled" class="width50  validate[required]" />
				<input type="hidden" name="ownerId" id="ownerId" class="width50" />
				<span class="button" style="width: 40px ! important; cursor: pointer;">
					<a class="btn ui-state-default ui-corner-all ownerInfo-popup"> 
						<span class="ui-icon ui-icon-newwin"> </span> 
					</a>
				</span>
			</div>
			<div>
				<label><fmt:message key="re.property.info.ownertype"/></label>
				<input type="text" name="ownerType" id="ownerType" disabled="disabled" class="width50  validate[required]" />
				<input type="hidden" name="ownerTypeId" id=ownerTypeId class="width50" />
			</div>
			<div>
				<label><fmt:message key="re.owner.info.civilidno"/></label>
				<input name="uaeNo" id="uaeNo" disabled="disabled"  class="width50">
			</div>
		</fieldset>
</div> 
<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="margin:10px 20px 20px 0;"> 
		<div class="portlet-header ui-widget-header float-right cancel"  id="validationDate" style="cursor:pointer;"><fmt:message key="re.property.info.cancel"/></div> 
		<div class="portlet-header ui-widget-header float-right editData" id="add"  style="cursor:pointer;"><fmt:message key="re.property.info.save"/></div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;"
	 class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable"
	 tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		
	<div id="property-popup" class="ui-dialog-content ui-widget-content"
		style="height: 200px !important; min-height: 48px; width: auto;">
		<div class="property-result"></div>
	</div>
</div>
</form>
	</td>  
	
 </tr>
 <script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var percentageTotal=$('#percentageTotal').val();
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';
   	 var sum=0;
   	 currentOwnerId = $($(slidetab).children().get(6)).text(); /////;
   	 currentOwnerType = $($(slidetab).children().get(1)).text(); ////////
	 $(function(){ 
		 $('.formError').remove(); 
		

		$jquery("#propertyInfoAddEdit").validationEngine('attach');

 		$.fn.percentCheck = function() { 	
 			
			percentage=Number($('#percentage').val()); // Checking Percentage
			percentageTemp=Number($('#percentageTemp').val());
			percentageTotal=Number($('#percentageTotal').val());
			$('#percentageTotal').val((percentageTotal-percentageTemp)+percentage);
			total=(percentageTotal-percentageTemp)+percentage;
			$('#percentageTotal').val(total);
			//alert("percentage : "+percentage+" percentageTemp : "+percentageTemp+" percentageTotal : "+percentageTotal );
	  		if( total>100 || percentage>100){
				$('#othererror').slideDown();
				$('#othererror').html("Sum of Percentage Should not greater than 100");
				//$('#percentage').val("");
			}else{
				$('#othererror').hide();
			}	
		};

		$('.cancel').click(function(){ 
			$('.formError').remove();
			$('#property-popup').dialog('destroy');		
			 $('#property-popup').remove(); 
			$($(this).parent().parent().parent().get(0)).remove();
			$($($(slidetab).children().get(5)).children().get(1)).show();	//edit button
	        $($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete button
	        $($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing button 	
	        openFlag=0;		
		 });
		 
		 $('#add').click(function(){
			  if($jquery('#propertyInfoAddEdit').validationEngine('validate')){
					percentage=Number($('#percentage').val());
					percentageTemp=Number($('#percentageTemp').val());
				  	total=0;
					$('.percentage').each(function(){ 
						total+=Number($(this).html());
					});
					  	
					percentageTotal=total;
					total=(percentageTotal-percentageTemp)+percentage;
					$('#percentageTotal').val(total);
					//sum=percentageTemp+percentage;
					//alert("percentage : "+percentage+" percentageTemp : "+percentageTemp+" percentageTotal : "+percentageTotal+" total : "+total);
			    if(total <= 100  ){
				   $('#loading').fadeIn();
					var tempObject=$(this);					
					var ownerId=$('#ownerId').val();
					var ownerName=$('#ownerName').val();
					var ownerType=$('#ownerType').val();
					var ownerTypeId=$('#ownerTypeId').val();
					var uaeNo=$('#uaeNo').val();
					var percentage=$('#percentage').val();
					//$.fn.percentCheck(); //For Checking Percentage			 

					var trnVal =$('#transURN').val(); 
					
					var flag=false;
		        	$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/add_edit_property_owner_update.action", 
					 	async: false,
					 	data: {ownerId: ownerId, currentOwnerId: currentOwnerId, ownerName: ownerName,ownerType: ownerType, ownerTypeId:ownerTypeId,
							currentOwnerType: currentOwnerType, uaeNo: uaeNo, 
							percentage: percentage,trnValue:trnVal, actualLineId: actualID},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$('.formError').hide();
	                           $('#othererror').hide(); 
							$('.tempresult').html(result); 
						     if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                                    flag = true;
						    	else{	
							    	flag = false;
							    	 $("#othererror").hide().html($('#returnMsg').html()).slideDown();
						    	} 
						     }
						     $('#loading').fadeOut();
					 	},  
					 	error:function(result){
						 	alert("err "+result);
					 		 $('.tempresult').html(result);
	                            $("#othererror").hide().html($('#returnMsg').html()).slideDown(); 
	                            $('#loading').fadeOut();
	                            return false;
					 	} 
		        	});
		        	if(flag==true){
		        		$('#property-popup').dialog('destroy');		
		   			 $('#property-popup').remove(); 
		        		$($(slidetab).children().get(0)).text(ownerName);
		        		$($(slidetab).children().get(1)).text(ownerType);
		        		$($(slidetab).children().get(2)).text(uaeNo); 
		        		$($(slidetab).children().get(3)).text(percentage); 

		        		
		        		$($(slidetab).children().get(6)).text(ownerId);
					   
					  	$($(tempObject).parent().parent().parent().parent().get(0)).remove(); 
					  	$($($(slidetab).children().get(5)).children().get(1)).show();	//Edit Button
						$($($(slidetab).children().get(5)).children().get(2)).show(); 	//Delete Button
						$($($(slidetab).children().get(5)).children().get(3)).hide();	//Processing Button
						openFlag=0;	
				  		$($($(slidetab).children().get(4)).children().get(0)).val($('#objLineVal').html());
				  		$("#transURN").val($('#objTrnVal').html());
				  		$.fn.globeTotal();  //For Total Percentage
				  		
				   } 
			  } else{
				  $('#othererror').slideDown();
					$('#othererror').html("Sum of Percentage Should not greater than 100");
					 return false;
			  }
			}	  
			  else{
				  return false;
			  }
		});

		 $('.ownerInfo-popup').click(function(){ 
		       tempid=$(this).parent().get(0);  
		       $('.ui-dialog-titlebar').remove();  
			 	$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/property_info_owner_details.action",
				 	async: false, 
				 	data:{},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						 $('.property-result').html(result); 
					},
					error:function(result){ 
						 $('.property-result').html(result); 
					}
				}); 
			});
		 
		$('#property-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:500, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
		
});
</script>	