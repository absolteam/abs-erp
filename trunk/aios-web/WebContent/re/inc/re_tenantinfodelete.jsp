<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 


<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){  
	$('#country').val($('#countryTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#tenanttype').val($('#tenantTemp').val());
	
	$("#tenantInformationAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	 	});
	$('.formError').remove();

	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });

	approvedStatus=$('#approvedStatus').val(); 
	if( approvedStatus == 'E'){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow in is process, Cannot delete this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow is already Approved, Cannot delete this screen.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow is already Rejected,Cannot delete this screen.!!!").slideDown();
	}

	 $('#birthdate').datepick({onSelect: customRange, showTrigger: '#calImg'});
	 $("#text_area_present_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_present_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_present_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    }); 

	    $("#text_area_permanent_input").keyup(function(){//Detect keypress in the textarea 
	        var text_area_box =$(this).val();//Get the values in the textarea
	        var max_numb_of_words = 500;//Set the Maximum Number of words
	        var main = text_area_box.length*100;//Multiply the lenght on words x 100

	        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
	        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

	        if(text_area_box.length <= max_numb_of_words){
	            $("#progressbar1").css("background-color","#5fbbde");//Set the background of the progressbar to blue
	            $('#count1').html(count);//Output the count variable previously calculated into the div with id= count
	            $('#progressbar1').animate({//Increase the width of the css property "width"
	            "width": value+'%',
	            }, 1);//Increase the progress bar
	        }
	        else{
	             $("#progressbar1").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
	             $("#text_area_permanent_input").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
	        }
	        return false;
	    });

	    $("#text_area_permanent_input").focus(function(){
	        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
	        return false;
	    });  


	    $('#cancel').click(function(){
	    	 $('.formError').remove();
			 $.ajax({
					type: "POST",  
					url: "<%=request.getContextPath()%>/tenant_info_list.action",   
			     	async: false, 
					dataType: "html",
					cache: false,
					error: function(data) 
					{  
					},
			     	success: function(data)
			     	{
						$("#main-wrapper").html(data);  //gridDiv main-wrapper
			     	}
				});
			return true;
	  });  

	    $('#confirm').click(function(){ 
	    	tenantId=$('#tenantId').val();
    		$.ajax({
    			type: "POST", 
    			url: "<%=request.getContextPath()%>/tenant_information_delete.action",
    			data: {tenantId: tenantId},  
    	     	async: false,
    			dataType: "html",
    			cache: false,
    			success: function(result)
    			{ 
    				$("#main-wrapper").html(result); 
    			} 
    		}); 
    		return true;
	 	}); 
    	$('#endDate').datepick();	 
	});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
  var daysInMonth =$.datepick.daysInMonth(
  $('#selectedYear').val(), $('#selectedMonth').val());
  $('#selectedDay option:gt(27)').attr('disabled', false);
  $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
  if ($('#selectedDay').val() > daysInMonth) {
      $('#selectedDay').val(daysInMonth);
  }
} 
function customRange(dates) {
if (this.id == 'birthdate') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#endPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

</script>
  



<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.owner.info.tenantinformation"/></div>
			 <div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
			<div class="portlet-content">
			<div>
				<c:if test="${requestScope.succMsg!=null && requestScope.succMsg!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.succMsg}"/></div>
				</c:if>
				<c:if test="${requestScope.errMsg!=null && requestScope.errMsg!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.errMsg}"/></div>
				</c:if>
			 </div>
			<form name="tenantInformationEdit" id="tenantInformationEdit"> 
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.owner.info.tenantinformation2"/></legend>		
							<div style="display:none"><label>Tenant Id</label><input type="hidden" name="tenantId" class="width30" id="tenantId" readonly="readonly" value="${bean.tenantId}" >
							<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>
							</div>			
                             <div><label><fmt:message key="re.owner.info.birthdate"/></label><input type="text" name="birthdate" class="width30" id="birthdate" readonly="readonly" disabled="disabled" value="${bean.birthDate}"></div>		
							<div><label><fmt:message key="re.owner.info.proffession"/></label><input type="text" name="sno" class="width30 tooltip" id="profession" disabled="disabled" value="${bean.profession}"></div>
							<div><label class=""><fmt:message key="re.owner.info.country"/></label>
									<input type="hidden" id="countryTemp" name="countryTemp" value="${bean.countryId}" class="width30">
									<select id="country" class="width30 validate[required]" disabled="disabled">
									<option value="">-Select-</option>
										<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
											<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
										</c:forEach>
									</select>
							</div>
							 <div>
								<label class=""><fmt:message key="re.owner.info.state"/></label>
								<input type="hidden" id="stateTemp" name="cityTemp" value="${bean.stateId }" class="width30">
									<select class="width30 validate[required]" id="state" disabled="disabled">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
											<option value="${statelist.stateId }">${statelist.stateName}</option>
										</c:forEach>
									</select>
					       </div>
							<div>
								<label class=""><fmt:message key="re.owner.info.city"/></label>
								<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" class="width30">
									<select  class="width30 validate[required]" id="city" disabled="disabled">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
											<option value="${citylist.cityId }">${citylist.cityName}</option>
										</c:forEach>
									</select>
							</div>																						
							<div>
								<label><fmt:message key="re.owner.info.id"/></label>
								<input type="text" name="uaeidno" class="width30" id="uaeidno" disabled="disabled" value="${bean.uaeIdNo}">
							</div>
							<div>
								<label><fmt:message key="re.owner.info.description"/></label>
								<input type="text" name="uaeIdDescription" class="width30" disabled="disabled" id="uaeIdDescription" value="${bean.uaeIdDescription}" >
							</div>
							<div>
								<label><fmt:message key="re.owner.info.postboxno"/></label>
								<input type="text" name="postBoxNo" class="width30 validate[required]" disabled="disabled" id="postBoxNo"  value="${bean.postBoxNo}">
							</div>
							<div><label><fmt:message key="re.owner.info.emailid"/></label><input type="text" name="emailid" class="width30" id="emailid" disabled="disabled" value="${bean.emailId}"></div>
                            <div><label><fmt:message key="re.owner.info.mobileno"/></label><input type="text" name="mobileno" class="width30" id="mobileno" disabled="disabled" value="${bean.mobileNo}"></div>
                            <div><label><fmt:message key="re.owner.info.landlineno"/></label><input type="text" name="landlineno" class="width30" id="landlineno" disabled="disabled" value="${bean.landLineNo}"></div>
                            <div>
                            	<label><fmt:message key="re.owner.info.website"/></label>
                            	<input type="text" name="website" class="width30" disabled="disabled" id="website" value="${bean.website}" >
                            </div>
                            <div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								<div id="UploadDmsDiv" class="width100">
									<label ><fmt:message key="re.property.info.imageupload"/></label>
									<input type="text" id="URNdms"  value="${bean.image }" disabled="disabled" class="width30">
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			                            <div  id="dms_document_information" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
			                        </div>
								</div>
				    		</div>
					</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.owner.info.tenantinformation1"/></legend>
							<div style="display:none"><label>Tenant No.</label><input type="text" name="tenantno" class="width30" id="tenantno" disabled="disabled"></div>
							<div><label><fmt:message key="re.owner.info.tenantname"/></label><input type="text" name="tenantname" class="width30" id="tenantname" disabled="disabled" value="${bean.tenantName}"></div>
							<div>
								<label><fmt:message key="re.owner.info.tenantdescription"/></label>
								<input type="text" name="tenantDescription" class="width30" id="tenantDescription" disabled="disabled" value="${bean.tenantDescription}">
							</div>
							<div><label><fmt:message key="re.owner.info.tenanttype"/></label>
							<input type="hidden" name="tenantTemp" id="tenantTemp" value="${bean.tenantType}">
									<select id="tenanttype" class="width30" disabled="disabled">
									<option value="">-Select-</option>
									<c:if test="${requestScope.lookupList ne null}">
								<c:forEach items="${requestScope.lookupList}" var="tenType" varStatus="status">
									<option value="${tenType.tenantType}">${tenType.description}</option>
								</c:forEach>
							</c:if>
									</select>
								</div>	
							<div style="display:none"><label>Status</label><input type="text" name="drno" class="width30 tooltip" id="status" disabled="disabled"></div>	
					</fieldset> 
					
					<fieldset>
	                    <legend><fmt:message key="re.owner.info.presentaddress"/></legend>
	                    <div style="height:25px" class="width60 float-left">
	                            <div id="barbox">
	                                  <div id="progressbar"></div>
	                            </div>
	                            <div id="count">500</div>
	                      </div>
	                      <p>
	                        <textarea class="width60 float-left tooltip" id="text_area_present_input" disabled="disabled">${bean.presentAddress}</textarea>
	                      </p>
	                 </fieldset>
					 <fieldset>
	                    <legend><fmt:message key="re.owner.info.permanentaddress"/></legend>
	                    <div style="height:25px" class="width60 float-left">
	                            <div id="barbox">
	                                 <div id="progressbar1"></div>
	                            </div>
	                            <div id="count1">500</div>
	                      </div>
	                      <p>
	                        <textarea class="width60 float-left tooltip" id="text_area_permanent_input" title="Please Enter Permanent Address" disabled="disabled" >${bean.permanentAddress}</textarea>
	                      </p>
	                 </fieldset>								 
				</div>
				<div class="portlet-content">
					<div class="width50 float-left" id="hrm">
						<fieldset>
							<legend><fmt:message key="accounts.suppliersite.label.accountinginformation"/></legend>
								<div class="width90 float-left"><label class="width30">Tenant Account Code<span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.liabilityAccountCode}" name="liabilityAccountCode" id="liabilityAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_liability_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a  id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>
								</div>
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.advanceaccountcode"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.advanceAccountCode}" name="advanceAccountCode" id="advanceAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_advance_account_action"/>
									<span  class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>	
								<div class="width90 float-left"><label class="width30"><fmt:message key="accounts.suppliersite.label.clearningacccount"/><span style="color:red;">*</span></label>
									<input type="text" class="width50 validate[required]" value="${bean.clearningAccountCode}" name="clearningAccountCode" id="clearningAccountCode" readonly="readonly">
									<input type="hidden" id="reterive_clearning_account_action"/>
									<span class="button" style="margin-top:3px;">
										<a id="liabilites" style="cursor: pointer;" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"></span> 
										</a>
									</span>	
								</div>																
						</fieldset> 
					</div>	
				</div>
				<div class="clearfix"></div>
				<div class="float-right buttons ui-widget-content ui-corner-all" style="">
					<div class="portlet-header ui-widget-header float-right" id="cancel"><fmt:message key="re.property.info.discard"/></div>
				 	<div class="portlet-header ui-widget-header float-right" id="confirm"><fmt:message key="re.property.info.confirm"/></div>
				</div>
			</form>
		</div>
	</div>
</div>