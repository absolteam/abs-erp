<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
var fromFormat='dd-MMM-yyyy';
var toFormat='yyyyMMdd';
var usrfromFormat='dd-MMM-yyyy';
var usrtoFormat='yyyyMMdd';


$(document).ready(function (){  
	$('#buildingId').val($('#buildingIdTemp').val());
	$('#flatId').val($('#flatIdTemp').val());
	$('#calculatedAsDeposit').val($('#calculatedAsDepositTemp').val());
	$('#calculatedAsRental').val($('#calculatedAsRentalTemp').val());
	$('#calculatedAsOther').val($('#calculatedAsOtherTemp').val());

	var depositAmount;
	if($('#depositAmount').is(':checked')){$('#depositAmountDiv').fadeIn();}else{$('#depositAmountDiv').fadeOut();}
	var rentalAmount;
	if($('#rentalAmount').is(':checked')){$('#rentalAmountDiv').fadeIn();}else{$('#rentalAmountDiv').fadeOut();}
	var otherAmount;
	if($('#otherAmount').is(':checked')){$('#otherAmountDiv').fadeIn();}else{$('#otherAmountDiv').fadeOut();}
	
	
	$("#defineManagementAdd").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	$("#comments").keyup(function(){//Detect keypress in the textarea 
        var text_area_box =$(this).val();//Get the values in the textarea
        var max_numb_of_words = 500;//Set the Maximum Number of words
        var main = text_area_box.length*100;//Multiply the lenght on words x 100

        var value= (main / max_numb_of_words);//Divide it by the Max numb of words previously declared
        var count= max_numb_of_words - text_area_box.length;//Get Count of remaining characters

        if(text_area_box.length <= max_numb_of_words){
            $("#progressbar").css("background-color","#5fbbde");//Set the background of the progressbar to blue
            $('#count').html(count);//Output the count variable previously calculated into the div with id= count
            $('#progressbar').animate({//Increase the width of the css property "width"
            "width": value+'%',
            }, 1);//Increase the progress bar
        }
        else{
             $("#progressbar").css("background-color","yellow");//If More words is typed into the textarea than the specified limit ,Change the progress bar from blue to yellow
             $("#comments").val(text_area_box.substr(0,max_numb_of_words));//Remove the excess words using substring
        }
        return false;
    });

    $("#comments").focus(function(){
        $(this).animate({"height": "75px",}, "slow" );//Expand the textarea on clicking on it 
        return false;
    });

	$(".cancel").click(function(){ 
		$('#loading').fadeIn();
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/define_management_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		$('#loading').fadeOut();
		return true;
	}); 

	$('#startPicker').blur(function(){
		$('.error').hide();
	});

	$('#percentage').blur(function(){
		var percentage=$('#percentage').val();
		if(percentage>100){
			$('#percentage').val("");		
			$('.availErr').show();
			$('.availErr').html("Percentage Should not greater than 100");
			
		}else{
			$('.availErr').hide();
		}	
	});

	$('#close').click(function(){
		$('.backtodashboard').trigger('click');
	  });

	$('#approve').click(function(){ 
		if($("#defineManagementAdd").validationEngine({returnIsValid:true})){
			var decisionId=$('#decisionId').val();
			
			comments=$('#comments').val();

			//Work Flow Variables
			sessionPersonId = $('#sessionPersonId').val();
			companyId=Number($('#headerCompanyId').val());
			applicationId=Number($('#headerApplicationId').val());
			workflowStatus="Approved by Supervisor";
			functionId=Number($('#functionId').val());
			functionType="approve";
			notificationId=$('#notificationId').val();
			workflowId=$('#workflowId').val();
			mappingId=$('#mappingId').val();

			if(true){
				$('#loading').fadeIn();
				fromDate=$("#startPicker").val();
				toDate=$("#endPicker").val();
				$('#loading').fadeIn();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/define_management_supervisor_approval.action",
					data: {decisionId:decisionId,
						comments: comments, sessionPersonId: sessionPersonId,companyId: companyId,applicationId: applicationId,mappingId: mappingId,
						workflowStatus: workflowStatus,functionId: functionId,functionType: functionType,notificationId: notificationId,workflowId: workflowId},  
			     	async: false,
					dataType: "html",
					cache: false,
					success:function(result){
							$('.formError').hide();
							$('.commonErr').hide();
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$('.tempresultfinal').fadeIn();
								$('#loading').fadeOut(); 
								$('#approve').remove();	
								$('#reject').remove();	
								$('#close').fadeIn();
								$('.commonErr').hide();
							} else{
								$('.tempresultfinal').fadeIn();
							}
						},  
					error:function(result){
							$('.tempresultfinal').fadeOut();
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							};
					 	} 
				});
				$('#loading').fadeOut();
			}else{
				$('.error').show();
				$('.error').html("'To Date' should be greater than 'From Date' !!!");
				return false;
			} 
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});

	$('#reject').click(function(){ 
		if($("#defineManagementAdd").validationEngine({returnIsValid:true})){
			var decisionId=$('#decisionId').val();
			
			comments=$('#comments').val();

			//Work Flow Variables
			sessionPersonId = $('#sessionPersonId').val();
			companyId=Number($('#headerCompanyId').val());
			applicationId=Number($('#headerApplicationId').val());
			workflowStatus="Rejected by Supervisor";
			functionId=Number($('#functionId').val());
			functionType="reject";
			notificationId=$('#notificationId').val();
			workflowId=$('#workflowId').val();
			mappingId=$('#mappingId').val();

			if(true){
				$('#loading').fadeIn();
				fromDate=$("#startPicker").val();
				toDate=$("#endPicker").val();
				$('#loading').fadeIn();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/define_management_supervisor_approval.action",
					data: {decisionId:decisionId,
						comments: comments, sessionPersonId: sessionPersonId,companyId: companyId,applicationId: applicationId,mappingId: mappingId,
						workflowStatus: workflowStatus,functionId: functionId,functionType: functionType,notificationId: notificationId,workflowId: workflowId},  
			     	async: false,
					dataType: "html",
					cache: false,
					success:function(result){
							$('.formError').hide();
							$('.commonErr').hide();
							$('.tempresultfinal').fadeOut();
				 		 	$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$('.tempresultfinal').fadeIn();
								$('#loading').fadeOut(); 
								$('#approve').remove();	
								$('#reject').remove();	
								$('#close').fadeIn();
								$('.commonErr').hide();
							} else{
								$('.tempresultfinal').fadeIn();
							}
						},  
					error:function(result){
							$('.tempresultfinal').fadeOut();
							$('.tempresultfinal').html(result);
							if($("#sqlReturnStatus").val()==1){
								$("#main-wrapper").html(result); 
							} else{
								$('.tempresultfinal').fadeIn();
							};
					 	}  
				});
				$('#loading').fadeOut();
			}else{
				$('.error').show();
				$('.error').html("'To Date' should be greater than 'From Date' !!!");
				return false;
			} 
		}else{
			$('.childCountErr').hide().html("Internal Error, Please contact Administrator").slideDown();
			return false; 
		}
	});
	
	$('#buildingId').change(function(){
		var buildingId=$('#buildingId').val();
		$('#loading').fadeIn();
		$.ajax({
    		type:"POST",
    		url:"<%=request.getContextPath()%>/flat_retrive_edit_action.action",
    		data:{buildingId:buildingId},
    		async:false,
    		dataType:"html",
    		cache:false,
    		success:function(result){
    			$("#flatId").html(result);
    			$('.FlatAmountDetails').hide();
    		}
    	});
		$('#loading').fadeOut();
	});

	$('#flatId').change(function(){
		var buildingId=$('#buildingId').val();
		var flatId=Number($('#flatId option:selected').val());
		if(flatId != 0){
			$('#loading').fadeIn();
			$.ajax({
	    		type:"POST",
	    		url:"<%=request.getContextPath()%>/flat_check_edit_action.action",
	    		data:{buildingId:buildingId,flatId:flatId},
	    		async:false,
	    		dataType:"html",
	    		cache:false,
	    		success:function(result){
	    			$('.tempresult').html(result); 
	    			msg=$('#resultMessage').html().trim().toUpperCase(); 
	    			if(msg == 'OK'){
	    				$.ajax({
		    				type:"POST",
		    				url:"<%=request.getContextPath()%>/flat_retrieve_amount.action" ,
		    				data:{buildingId:buildingId,flatId:flatId},
		    				async:false,
		    				dataType:"html",
		    				cache:false,
		    				success:function(result){
			    				$('.FlatAmountDetails').html(result);
			    				$('.FlatAmountDetails').show();
		    				}
	    				});
	    			}
	    			else{
	    				$('.availErr').show();
	    				$('.availErr').html("Building and Flat Combination Already Exists!!!");
	    				$('#flatId').val("");
	    				$('.FlatAmountDetails').hide();
	    			}
	    		}
	    	});
			$('#loading').fadeOut();
		}else{
			$('.FlatAmountDetails').hide();
		}
	});

	$("#depositAmount").click(function() {
		$('#depositAmountDiv').slideToggle();
	});
	$("#rentalAmount").click(function() {
		$('#rentalAmountDiv').slideToggle();
	});
	$("#otherAmount").click(function() {
		$('#otherAmountDiv').slideToggle();
	});
		
	if (!$.browser.msie) {
			$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
		}
		 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		 $('#selectedMonth,#linkedMonth').change();
		 $('#l10nLanguage,#rtlLanguage').change();
		 if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		 }
		$('#startPicker,#endPicker').datepick({
		 	onSelect: customRange, showTrigger: '#calImg'
		});

});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth(
    $('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth) {
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'startPicker') {
	$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	}
	else {
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}
</script>
  

<script type="text/javascript">

</script>

<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.definemanagementdecision"/></div>
        <div class="portlet-content">
        <div  class="response-msg error availErr ui-corner-all" style="width:80%; display:none;"></div>
       		<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 
			<input type="hidden" id="notificationId" value="${requestScope.notificationId}"/>
		    		<input type="hidden" id="notificationType" name="notificationType" value="${requestScope.notificationType}"/>
					<input type="hidden" id="functionId" value="${requestScope.functionId}"/>
					<input type="hidden" id="workflowId" value="${requestScope.workflowId}"/>
					<input type="hidden" id="mappingId" value="${requestScope.mappingId}"/>
					<input class="width40" type="hidden" name="sessionpersonId" value="${bean.sessionPersonId }" id="sessionPersonId">
			<form name="defineManagementAdd" id="defineManagementAdd">
				<div class="width50 float-left" id="hrm">
					<fieldset>
				 		<div>
				 			<input type="hidden" name="decisionId" id="decisionId" value="${bean.decisionId }" />
							<label class="width30"><fmt:message key="re.property.info.buildingno"/></label>	
						 	<input type="hidden" name="buildingIdTemp" id="buildingIdTemp" value="${bean.buildingId }" />
						 	<select id="buildingId" class="width30 validate[required]" disabled="disabled">
								<option value="">-Select-</option>
								<c:if test="${requestScope.itemList ne null}">
									<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
										<option value="${building.buildingId }">${building.buildingName }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.flatno"/></label>	 
						 <input type="hidden" name="flatIdTemp" id="flatIdTemp" value="${bean.flatId }" />
							<select id="flatId" class="width30 validate[required]" disabled="disabled">
								<option value="">-Select-</option>
								<c:if test="${requestScope.item ne null}">
									<c:forEach items="${requestScope.item}" var="flat" varStatus="status">
										<option value="${flat.flatId }">${flat.flatNumber }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<fieldset>
							<div>
							 	<label class="width30"><fmt:message key="re.property.info.depositamount"/></label>
							 	<c:choose>
									<c:when test="${bean.depositAmount eq 1 }">
										<input class="width40" type="checkbox" checked="checked" disabled="disabled" name="depositAmount" id="depositAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" name="depositAmount"  disabled="disabled" id="depositAmount" >
									</c:otherwise>
								</c:choose>
							 </div>
							 <div id="depositAmountDiv">
								<div>
								 	<c:choose>
							 			<c:when test="${bean.depositIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="depositIncDec" disabled="disabled" id="depositInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="depositIncDec" disabled="disabled" id="depositDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" disabled="disabled" name="depositIncDec"  id="depositInc">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="depositIncDec" disabled="disabled" id="depositDec" checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsDepositTemp" value="${bean.calculatedAsDeposit}">
									<select id="calculatedAsDeposit" class="width40" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate" varStatus="status">
												<option value="${calculate.calculatedAs }">${calculate.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" disabled="disabled" type="text" value="${bean.percentageDeposit }" name="percentageDeposit" id="percentageDeposit">
							 	</div>
							 </div>
					 	</fieldset>
					 	<fieldset>
						 	<div>
							 	<label class="width30"><fmt:message key="re.property.info.rentalamount"/></label>
							 	<c:choose>
									<c:when test="${bean.rentalAmount eq 1 }">
										<input class="width40" type="checkbox" disabled="disabled" checked="checked" name="rentalAmount" id="rentalAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" disabled="disabled" name="rentalAmount" id="rentalAmount" >
									</c:otherwise>
								</c:choose>
						 	</div>
						 	<div id="rentalAmountDiv">
							 	<div>
							 		<c:choose>
							 			<c:when test="${bean.rentalIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" disabled="disabled" type="radio" name="rentalIncDec"  id="rentalInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" disabled="disabled" name="rentalIncDec"  id="rentalDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" disabled="disabled" type="radio" name="rentalIncDec"  id="rentalInc" >
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" disabled="disabled" name="rentalIncDec"  id="rentalDec" checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsRentalTemp" value="${bean.calculatedAsRental}">
									<select id="calculatedAsRental" class="width40" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate1" varStatus="status">
												<option value="${calculate1.calculatedAs }">${calculate1.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" type="text" value="${bean.percentageRental }" name="percentageRental" id="percentageRental" disabled="disabled">
							 	</div>
							 </div>
					 	</fieldset>
					 	<fieldset>
						 	<div>
							 	<label class="width30">Other Amount</label>
							 	<c:choose>
									<c:when test="${bean.otherAmount eq 1 }">
										<input class="width40" type="checkbox" disabled="disabled" checked="checked" name="otherAmount" id="otherAmount"/>
									</c:when>
									<c:otherwise>
										<input class="width40" type="checkbox" disabled="disabled" name="otherAmount" id="otherAmount" >
									</c:otherwise>
								</c:choose>
						 	</div>
						 	<div id="otherAmountDiv">
							 	<div>
								 	<c:choose>
							 			<c:when test="${bean.otherIncDec eq 1}">
										 	<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" disabled="disabled" name="otherIncDec"  id="otherInc" checked="checked">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" name="otherIncDec"  disabled="disabled" id="otherDec">
									 	</c:when>
									 	<c:otherwise>
									 		<label class="width30"><fmt:message key="re.property.info.increase"/></label>
										 	<input class="width10 float-left" type="radio" name="otherIncDec" disabled="disabled" id="otherInc">
										 	<label class="width20 "><fmt:message key="re.property.info.decrease"/></label>
										 	<input class="width10 " type="radio" disabled="disabled" name="otherIncDec"  id="otherDec"  checked="checked">
									 	</c:otherwise>
								 	</c:choose>
							 	</div>
							 	<div>
									<label class="width30"><fmt:message key="re.property.info.calculatedas"/></label>
									<input type="hidden" id="calculatedAsOtherTemp" value="${bean.calculatedAsOther}">
									<select id="calculatedAsOther" class="width40" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemCalc ne null}">
											<c:forEach items="${requestScope.itemCalc}" var="calculate2" varStatus="status">
												<option value="${calculate2.calculatedAs }">${calculate2.calculatedCode }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
							 	<div>
								 	<label class="width30"><fmt:message key="re.property.info.percentage"/></label>
								 	<input class="width40 validate[optional,custom[onlyFloat]]" disabled="disabled" type="text" value="${bean.percentageOther }" name="percentageOther" id="percentageOther">
							 	</div>
							 </div>
					 	</fieldset>
						<div>
						 	<c:choose>
								<c:when test="${bean.holdingFlat eq 1 }">
									<input class="width40" type="checkbox" disabled="disabled" checked="checked" name="holdingFlat" id="holdingFlat"/>
								</c:when>
								<c:otherwise>
									<input class="width40" type="checkbox" disabled="disabled" name="holdingFlat" id="holdingFlat" >
								</c:otherwise>
							</c:choose>
						 	<label class="width30"><fmt:message key="re.property.info.holdingflat"/></label>
						 	
						 </div>
						 <div>
						 	<c:choose>
								<c:when test="${bean.holdingRent eq 1 }">
									<input class="width40" type="checkbox" disabled="disabled" checked="checked" name="holdingRent" id="holdingRent"/>
								</c:when>
								<c:otherwise>
									<input class="width40" type="checkbox" disabled="disabled" name="holdingRent" id="holdingRent" >
								</c:otherwise>
							</c:choose>
						 	<label class="width30"><fmt:message key="re.property.info.holdingcontract"/></label>
						 </div>
					</fieldset>
				</div>
				<div class="width50 float-left FlatAmountDetails" id="hrm">
					<fieldset>
						<div>
							<label class="width30"><fmt:message key="re.offer.rentamount"/></label>
							<input type="text" name="rent" class="width30"  value="${beanFlatAmount.rent }" id="rent" disabled="disabled">
						</div>		
						<div>
							<label class="width30"><fmt:message key="re.property.info.depositamount"/></label>
							<input type="text" name="deposit" class="width30" value="${beanFlatAmount.deposit }" id="deposit" disabled="disabled">
						</div>
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset style="">
						<div>
							<label class="width30"><fmt:message key="re.property.info.fromdate"/></label>
							<input type="text" name="startPicker" class="width30 validate[required]" disabled="disabled"  id="startPicker" value="${bean.fromDate }" />
						</div>		
						<div>
							<label class="width30"><fmt:message key="re.property.info.todate"/></label>
							<input type="text" name="endPicker" class="width30 validate[required]" disabled="disabled"  id="endPicker" value="${bean.toDate }" />
						</div>
						<div>
							<label class="width30"><fmt:message key="re.property.info.remarks"/></label>
							<input type="text" name="remarks" class="width30" value="${bean.remarks }" id="remarks" disabled="disabled" > 
						</div>
					</fieldset>
				</div>
			  <div class="clearfix"></div>
			 <div class="width50 float-right" style="margin:5px;" id="hrm">
				<fieldset>
					<legend>Supervisor Comments<span style="color:red">*</span></legend>
					<textarea id="comments" class="width100 validate[required]"></textarea>
				</fieldset>
			</div>
			<div style="display:none;" class="tempresult"></div>
			<div class="clearfix"></div>			
			<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			<div class="portlet-header ui-widget-header float-right"  id="close"><fmt:message key="cs.common.button.close"/></div>
			<div class="portlet-header ui-widget-header float-right" id="reject"><fmt:message key="cs.sendingfax.button.reject"/></div>
			<div class="portlet-header ui-widget-header float-right" id="approve"><fmt:message key="cs.sendingfax.button.approve"/></div>
		</div>
		</form>
		</div>
	</div>
</div>
							
							
		
		
		
		