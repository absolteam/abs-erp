<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>

<style type="text/css">
.ui-pg-input{
width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
#gbox_list2{
width :70%;
}
</style>
<script type="text/javascript">
$(document).ready(function (){

	$('.formError').remove();
	$("#list2").jqGrid({
		 url:"<%=request.getContextPath()%>/json-maintenanceasset_search.action?brandId ="+brandId+'& fromDate ='+fromDate+'& toDate ='+toDate+'& modelNumber ='+modelNumber+'&noOfPieces='+noOfPieces, 
		 datatype: "json", 
		 colNames:['buildingAssetId','<fmt:message key="re.property.info.classname"/>','<fmt:message key="re.property.info.subclassname"/>',
		  		 '<fmt:message key="re.owner.info.brandid"/>','<fmt:message key="re.owner.info.brand"/>','<fmt:message key="re.owner.info.modelnumber"/>',
		  		 '<fmt:message key="re.owner.info.noofpieces"/>','<fmt:message key="re.owner.info.fromdate"/>','<fmt:message key="re.owner.info.todate"/>'], 
		 colModel:[ 
				{name:'buildingAssetId',index:'BUILDING_MAINTENANCE_ASSET_ID', width:100,  sortable:true},
				{name:'className',index:'CLASS_NAME', width:150,sortable:true, sorttype:"data"},
				{name:'subClassName',index:'SUB_CLASS_NAME', width:150,sortable:true, sorttype:"data"},
		  		{name:'brandId',index:'LOV_CODE', width:100,  sortable:true},
		  		{name:'brand',index:'LOV_MEANING', width:150,sortable:true, sorttype:"data"},
		  		{name:'modelNumber',index:'MODEL_NUMBER', width:150,sortable:true, sorttype:"data"},
		  		{name:'noOfPieces',index:'NO_OF_PIECES', width:150,sortable:true, sorttype:"data"},
		  		{name:'fromDate',index:'BEGIN_DATE', width:150,sortable:true, sorttype:"data"}, 
		  		{name:'toDate',index:'END_DATE', width:150,sortable:true, sorttype:"data"},
		  		
		 ], 		  				  		  
		 rowNum:10, 		 
		 rowList:[5,10,20,30,35], 
		 pager: '#pager2', 
		 sortname: 'BUILDING_MAINTENANCE_ASSET_ID', 
		 viewrecords: true, 
		 sortorder: "desc", 
		 caption:'<fmt:message key="re.owner.info.buildingmaintenanceasset"/>'
		 
	});  
 	$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false,search:false});
 	$("#list2").jqGrid('hideCol',["buildingAssetId","brandId"]);
});  
 	</script>
 <body>
<input type="hidden" id="bankName"/> 
<table id="list2"></table>  
<div id="pager2"> </div> 
</body>
 	