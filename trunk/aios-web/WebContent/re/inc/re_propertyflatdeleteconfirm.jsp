<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
	<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">


$(function (){ 
	$('#rentStatus').val($('#temprentStatus').val());
	$('#buildingId').val($('#tempbuildingId').val());
	$('#componentType').val($('#tempcomponentType').val());
	$('#measurementCode').val($('#measurementCodeTemp').val());
	$('#flatStatus').val($('#tempflatStatus').val());
	$('.tooltip').tooltip({
        track: true,
        delay: 0,
        showURL: false,
        showBody: " - ",
        fade: 250
    });
	
	$("#confirm").click(function(){
		propertyFlatId=$('#propertyFlatId').val();
			//alert("itemId : "+itemId);
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_property_flat.action",
				data: {propertyFlatId: propertyFlatId},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result)
				{ 
					$("#main-wrapper").html(result); 
				} 
			}); 
			return true;
		});
	 
	
	$("#cancel").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_flat_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	}); 

	approvedStatus=$('#approvedStatus').val(); 
	if( approvedStatus == 'E'){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow in is process, Cannot delete this screen.!!!").slideDown();
	}
	if(approvedStatus == 'A' ){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow is already Approved, Cannot delete this screen.!!!").slideDown();
	}
	if(approvedStatus == 'R' ){
		$('#confirm').fadeOut();	
		$('#cancel').fadeIn();
		$('.commonErr').hide().html("Workflow is already Rejected,Cannot delete this screen.!!!").slideDown();
	}


	
	if (!$.browser.msie) {
		$('a[href=#range],a[href=#extend]').parent().addClass('tabSplit');
	}
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#startPicker,#endPicker').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'
		});

});

//Prevent selection of invalid dates through the select controls
function checkLinkedDays() {
var daysInMonth =$.datepick.daysInMonth(
$('#selectedYear').val(), $('#selectedMonth').val());
$('#selectedDay option:gt(27)').attr('disabled', false);
$('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
if ($('#selectedDay').val() > daysInMonth) {
    $('#selectedDay').val(daysInMonth);
}
} 
function customRange(dates) {
if (this.id == 'startPicker') {
$('#endPicker').datepick('option', 'minDate', dates[0] || null);
}
else {
	$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
}
}

</script>
 
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyflat"/></div>
				<div  class="response-msg error commonErr ui-corner-all" style="width:95%; display:none;"></div>
				<div class="portlet-content">
					<form id="propertyFlatAdd">
					<fieldset>
						<div class="width50 float-right" id="hrm"> 
			 				<fieldset>
								<legend><fmt:message key="re.property.info.propertyflat2"/></legend>						
		                            <div><label><fmt:message key="re.property.info.fromdate"/></label><input type="text" name="startPicker" class="width30" id="startPicker" disabled="disabled" value="${bean.fromDate}"></div>		
									<div><label><fmt:message key="re.property.info.todate"/></label><input type="text" name="endPicker" class="width30" id="endPicker" disabled="disabled" value="${bean.toDate }"></div>																
									<div><label><fmt:message key="re.property.info.deposit"/></label><input type="text" name="deposit" class="width30" id="deposit" value="${bean.deposit }" disabled="disabled"></div>
									<div>
										<label><fmt:message key="re.property.info.rent"/><fmt:message key="re.propertyinfo.peryear"/></label>
										<input type="text" name="rent" class="width30" id="rent" value="${bean.rent }" disabled="disabled"></div>
									<div  style="display:none;"><label><fmt:message key="re.property.info.rentstatus"/></label>
									<input type="hidden" name="rentStatus" class="width30" id="temprentStatus" value="${bean.rentStatus}"/>
										<select id="rentStatus" class="width30 validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookupRentList ne null}">
												<c:forEach items="${requestScope.lookupRentList}" var="rentList" varStatus="status">
													<option value="${rentList.rentStatus }">${rentList.rentStatusName }</option>
												</c:forEach>
											</c:if>
										</select>
									</div>
									<div>
										<label><fmt:message key="re.property.info.totalarea"/></label>
										<input type="text" name="totalArea" class="width15 validate[required]" disabled="disabled" id="totalArea" value="${bean.totalArea}"/>
										<input type="hidden" id="measurementCodeTemp" name="measurementCodeTemp" value="${bean.measurementCode}" class="width30">
										<select id="measurementCode" class="width15  validate[required]" disabled="disabled"  >
											<option value="">-Select-</option>
											<c:if test="${requestScope.beanMeasurement ne null}">
											<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementList" varStatus="status">
												<option value="${beanMeasurementList.measurementCode }">${beanMeasurementList.measurementValue }</option>
											</c:forEach>
											</c:if>
										</select>
									</div>
									<div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
										<div id="UploadDmsDiv" class="width100">
											<label class="width35"><fmt:message key="re.property.info.imageupload"/></label>
											<input type="text" id="URNdms" value="${bean.image }" disabled="disabled" class="width30">
											<div class="float-right buttons ui-widget-content ui-corner-all" style="">
					                            <div  id="dms_create_documen" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
					                        </div>
										</div>
						    		</div>
							</fieldset>  																		
						</div> 
						<div class="width50 float-left" id="hrm">
							<fieldset style="height:207px;">
								<legend><fmt:message key="re.property.info.propertyflat1"/></legend>
								<div style="display:none;"><label>Id No.</label><input type="hidden" name="propertyFlatId" class="width30" disabled="disabled" id="propertyFlatId" value="${bean.propertyFlatId}"/></div>
								<div>
									<label><fmt:message key="re.property.info.flatnumber"/></label>
									<input type="text" name="flatNumber" class="width30 validate[required]" id="flatNumber" disabled="disabled" value="${bean.flatNumber}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.buildingname"/></label>
									<input type="hidden" name="buildingId" class="width30" id="tempbuildingId" value="${bean.buildingId}"/>
									<select id="buildingId" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.itemList ne null}">
											<c:forEach items="${requestScope.itemList}" var="building" varStatus="status">
												<option value="${building.buildingId }">${building.buildingName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.floorno"/></label>
									<input type="text" name="floorNo" class="width30" id="floorNo" disabled="disabled" value="${bean.floorNo}"/>		
								</div>	
								<div>
									<label><fmt:message key="re.property.info.noofrooms"/></label>
									<input type="text" name="noOfRooms" class="width30" disabled="disabled" id="noOfRooms" value="${bean.noOfRooms}"/>
								</div>					
								<div>
									<label><fmt:message key="re.property.info.componenttype"/></label>
									<input type="hidden" name="componentType" class="width30" id="tempcomponentType" value="${bean.componentType}"/>
									<select id="componentType" class="width30 validate[required]" disabled="disabled">
										<option value="">-Select-</option>
										<c:if test="${requestScope.lookupList ne null}">
											<c:forEach items="${requestScope.lookupList}" var="compType" varStatus="status">
												<option value="${compType.componentType }">${compType.componentTypeName }</option>
											</c:forEach>
										</c:if>
									</select>
								</div>
								<div>
									<label><fmt:message key="re.property.info.status"/></label>
									<input type="hidden" name="flatStatus" class="width30" id="tempflatStatus" value="${bean.flatStatus }"/>
									<select id="flatStatus" class="width30 validate[required]" disabled="disabled">
											<option value="">-Select-</option>
											<c:if test="${requestScope.lookupFlatList ne null}">
												<c:forEach items="${requestScope.lookupFlatList}" var="flatList" varStatus="status">
													<option value="${flatList.flatStatus }">${flatList.flatStatusName }</option>
												</c:forEach>
											</c:if>
									</select>
								</div>			
							 </fieldset> 
						</div>
				</fieldset>
				</form>
			<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuredetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;">${requestScope.wrgMsg}</div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.featurecode"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.features"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail1"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail2"/></th>
			            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tabFeature" style="">	
										<c:forEach items="${requestScope.result1}" var="result" >
											<tr>  
												<td class="width10">${result.lineNumber }</td>	 
												<td class="width20">${result.featureCode }</td>
												<td class="width20">${result.features }</td>	
												<td class="width10">${result.measurements }</td> 
												<td class="width10">${result.remarks }</td>
												<td style="display:none"> 
													<input type="text" value="${result.propertyInfoFeatureId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;"></td>	
												<td style="display:none;"></td>	 
											</tr>  
										</c:forEach>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				</div>
				<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;">Add Row</div> 
				</div>
		</div>
		<div id="main-content" style="width:98% !important;"> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
					<form name="newFields2">
	 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.assetdetails"/></div>
						<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
						<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
						<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
						<div class="portlet-content"> 
							<div style="display:none;" class="tempresult"></div>
					 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
					 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
					 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
							<div id="hrm"> 
								<div id="hrm" class="hastable width100"> 
									<table id="hastab" class="width100">	
		 								<thead class="chrome_tab">
											<tr>  
				            					<th class="width20"><fmt:message key="re.property.info.linenumber"/></th>
				            					<th class="width20"><fmt:message key="re.property.info.maintenancecode"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.brand"/></th>
				            					<th class="width20"><fmt:message key="re.owner.info.modelnumber"/></th>
				            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
											</tr>
										</thead>  
										<tbody class="tab" style="">
											<c:forEach items="${requestScope.assetList}" var="result" >
											 	<tr class="rowid">  	 
													<td class="width20">${result.lineNumber}</td>
													<td class="width20">${result.maintainenceCode}</td>	
													<td class="width20 amountcalc">${result.brand}</td> 
													<td class="width20">${result.modelNumber}</td>
													<td style="display:none"> 
														<input type="hidden" value="${result.propertyFlatAssetId}" name="actualLineId" id="actualLineId"/>   
										              	<input type="hidden" name="actionFlag" value="U"/>
										              	<input type="hidden" name="tempLineId" value=""/>	
													</td> 
													<td style=" width:5%;"> 
					  									<a style="display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addflat" style="cursor:pointer;"  title="Add this row" >
														 	 <span class="ui-icon ui-icon-plus"></span>
														</a>
														<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFlat" style="cursor:pointer;" title="Edit this row" >
															<span class="ui-icon ui-icon-wrench"></span>
														</a>
														<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFlat" style="cursor:pointer;" title="Delete this Row" >
															<span class="ui-icon ui-icon-circle-close"></span>
														</a>
														<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
															<span class="processing"></span>
														</a>
													</td>  
													<td style="display:none;" class=""></td>
													<td style="display:none;"></td>		 
												</tr>  
								  		 	</c:forEach>	
										</tbody>
		 							</table>
									<div class="iDiv" style="display: none;">
									</div>
								</div>
								<div class="vGrip">
									<span></span>
								</div>
				    		</div>
				    		<!--  <div class="float-right " style="dispaly:none;">
							<label>Total</label>
						 	<input name="amountTotal" id="amountTotal" style="width:45%!important;" disabled="disabled" >
						 </div>-->
						</div> 
					</form>
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
					</div>
				</div>
			</div>	
			<input type="hidden" name="approvedStatus" id="approvedStatus" value="${bean.approvedStatus}"/>		
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
			<div class="portlet-header ui-widget-header float-right cancelrec" id="cancel" style=" cursor:pointer;"><fmt:message key="re.property.info.discard"/></div> 
			<div class="portlet-header ui-widget-header float-right headerData" id="confirm" style="cursor:pointer;"><fmt:message key="re.property.info.confirm"/></div></div>			
	</div>
</div>	
</div>
		