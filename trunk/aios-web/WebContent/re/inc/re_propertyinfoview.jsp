<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
  <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
 
  <script type="text/javascript" src="/js/thickbox-compressed.js"></script>
<link rel="stylesheet" href="/css/thickbox.css" type="text/css" />
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<title>Tenant Details</title>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
</style>

<script type="text/javascript">
function loadTB(title,url,imgPath){
    tb_show(title,url,imgPath);
}
$(function (){  
	$('#district').val($('#districtTemp').val());
	$('#city').val($('#cityTemp').val());
	$('#state').val($('#stateTemp').val());
	$('#country').val($('#countryTemp').val());
	$('#propertyType').val($('#propertyTypeTemp').val());
	$('#measurementCode').val($('#measurementCodeTemp').val());
	$('#status').val($('#statusTemp').val());
	$('.formError').remove(); 
	 $("#propertyInfoEdit").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

 	var dmsUserId='${emp_CODE}';
	var dmsPersonId='${PERSON_ID}';
	var companyId=Number($('#headerCompanyId').val());
	var applicationId=Number($('#headerApplicationId').val());
	var functionId=Number($('#headerCategoryId').val());
	var functionType="list";//edit,delete,list,upload
	//$('#dmstabs-1, #dmstabs-2').tabs();
	//'dmsUserId':'${emp_CODE}','dmsUserName':'${employee_ID}','dmsPersonId':'${personId}','dmsPersonName':'${personName}'
	$('#dms_create_document').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
			loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&height=500&width=900&modal=true&isFile=Y','/images/loadingAnimation.gif');
			//$('#tabs').tabs({selected:0});// switch to first tab
	});
	$('#dms_document_information').click(function(){
		var dmsURN=$('#URNdms').val();
			slidetab=$($($(this).parent().get(0)).parent().get(0));
		loadTB('','<%=request.getContextPath()%>/dms_create_newversion.action?dmsURN='+dmsURN+'&dmsPersonId='+dmsPersonId+'&dmsUserId='+dmsUserId+'&companyId='+companyId+'&applicationId='+applicationId+'&functionType='+functionType+'&height=500&width=900&modal=true&isFile=N','/images/loadingAnimation.gif');
		//$('#tabs').tabs({selected:1});// switch to second tab
	});

	$("#close").click(function(){ 
		$.ajax({
			type: "POST",  
			url: "<%=request.getContextPath()%>/property_info_list.action",   
	     	async: false,
			dataType: "html",
			cache: false,
			error: function(data) 
			{
			},
	     	success: function(data)
	     	{
				$("#main-wrapper").html(data);  //gridDiv main-wrapper
	     	}
		});
		return true;
	});
});	 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.propertyinformation"/></div>
		<div class="portlet-content">
		<form id="propertyInfoEdit">
			<fieldset>
				<div class="width50 float-right" id="hrm"> 
	 				<fieldset>
						<legend><fmt:message key="re.property.info.propertyinformation2"/></legend>						
			                <div>
			                	<label class="width30"><fmt:message key="re.property.info.fromdate"/></label>
			                	<input type="text" name="fromDate" id="startPicker" value="${bean.fromDate }" disabled="disabled" class="width30 validate[required]"  title="Select Birth Date" readonly="readonly">
			                </div>		
		                    <div>
		                    	<label class="width30"><fmt:message key="re.property.info.todate"/></label>
		                    	<input type="text" name="toDate" id="endPicker" value="${bean.toDate }" class="width30 validate[required]" disabled="disabled" title="Enter Designation" readonly="readonly">
		                    </div>
							<div><label class="width30"><fmt:message key="re.property.info.status"/></label>
								<input type="hidden" id="statusTemp" name="statusTemp" value="${bean.propStatus }" class="width30 validate[required]">
								<select id="status" class="tooltip" disabled="disabled"  style="width:31.5%;">
										<option value="">-Select-</option>
										<c:forEach items="${requestScope.propertyStatus}" var="propertyStatusList" varStatus="status">
											<option value="${propertyStatusList.propStatusCode }">${propertyStatusList.propStatus }</option>
										</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.landsize"/></label>
								<input type="text" name="landSize" id="landSize" value="${bean.landSize }" disabled="disabled" class="width10 validate[required]" >
								<input type="hidden" id="measurementCodeTemp" name="measurementCodeTemp" value="${bean.measurementCode}" class="width30">
								<select id="measurementCode" class="width20  validate[required]" disabled="disabled">
									<option value="">-Select-</option>
									<c:if test="${requestScope.beanMeasurement ne null}">
									<c:forEach items="${requestScope.beanMeasurement}" var="beanMeasurementList" varStatus="status">
										<option value="${beanMeasurementList.measurementCode }">${beanMeasurementList.measurementValue }</option>
									</c:forEach>
									</c:if>
								</select>
							</div>
					        <div>
					        	<label class="width30"><fmt:message key="re.property.info.propertytax"/></label>
					        	<input type="text" name="propertyTax" id="propertyTax" value="${bean.propertyTax }" disabled="disabled" class="width30 validate[required]">
					        </div>
	                       	<div>
	                        	<label class="width30"><fmt:message key="re.property.info.yearofbuild"/></label>
	                        	<input type="text" name="yearOfBuild" id="yearOfBuild" value="${bean.yearOfBuild }" disabled="disabled" class="width30 validate[required]">
	                       	</div>
	                      	<div>
	                      		<label class="width30"><fmt:message key="re.property.info.buildingcost"/></label>
	                      		<input type="text" name="marketValue" id="marketValue" value="${bean.marketValue }" disabled="disabled" class="width30 validate[required]">
	                      	</div>
	                        <div id="hrm" class="" Style="*float:none!important;margin-bottom:10px;">
								<div id="UploadDmsDiv" class="width100">
									<label class="width30"><fmt:message key="re.property.info.imageupload"/></label>
									<input type="text" id="URNdms" value="${bean.image }" disabled="disabled" class="width30">
									<div class="float-right buttons ui-widget-content ui-corner-all" style="">
			                            <div  id="dms_document_informatio" class="portlet-header ui-widget-header float-left" ><fmt:message key="re.property.info.upload"/></div>
			                        </div>
								</div>
				    		</div>		                            
					</fieldset>  																		
				</div> 
				<div class="width50 float-left" id="hrm">
					<fieldset>
						<legend><fmt:message key="re.property.info.propertyinformation1"/></legend>
							<div style="display:none;">
								<label class="width30">Id No.</label>
								<input type="hidden" name="buildingNumber" id="buildingNumber" value="${bean.buildingNumber }" class="width30" disabled="disabled">
								<input type="hidden" name="buildingId" id="buildingId" value="${bean.buildingId }" class="width30" disabled="disabled">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.propertytype"/></label>
								<input type="hidden" id="propertyTypeTemp" name="propertyTypeTemp" value="${bean.propertyTypeName }" class="width30 validate[required]">
								<select id="propertyType" class="validate[required]" disabled="disabled"  style="width:31.5%;">
									<option value="">-Select-</option>
									<c:forEach items="${requestScope.propertyList}" var="propertyTypeList" varStatus="status">
											<option value="${propertyTypeList.propertyTypeCode }">${propertyTypeList.propertyTypeName }</option>
										</c:forEach>
								</select>
							</div>	
							<div>
								<label class="width30 tooltip" title="Enter Building Name" ><fmt:message key="re.property.info.buildingname"/></label>
								<input type="text" name="buildingName" id="buildingName" value="${bean.buildingName }" disabled="disabled" class="width30">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.plotno"/></label>
								<input type="text" id="plotNo" disabled="disabled" name="plotNo" value="${bean.plotNo }" class="width30 validate[required]">
							</div>				
							<div>
								<label class="width30"><fmt:message key="re.property.info.nooffloors"/></label>
								<input type="text" id="floorNo" disabled="disabled" name="floorNo" value="${bean.floorNo }" class="width30 validate[required]">
							</div>
							
							<div>
								<label class="width30"><fmt:message key="re.property.info.addressLine1"/></label>
								<input type="text" id="addressLine1" disabled="disabled" name="addressLine1" value="${bean.addressLine1 }" class="width30 validate[required]">
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.addressLine2"/></label>
								<input type="text" id="addressLine2" disabled="disabled" name="addressLine2" value="${bean.addressLine2 }" class="width30 validate[required]">
							</div>
							
							<div>
								<label class="width30"><fmt:message key="re.property.info.country"/></label>
								<input type="hidden" id="countryTemp" name="countryTemp" value="${bean.countryId }" class="width30 validate[required]">
								<select id="country" class="tooltip" disabled="disabled"  style="width:31.5%;">
									<option value=""> - Select - </option>
									<c:forEach items="${requestScope.countryList}" var="countrylist" varStatus="status">
										<option value="${countrylist.countryId }">${countrylist.countryCode}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30"><fmt:message key="re.property.info.state"/></label>
								<input type="hidden" id="stateTemp" name="stateTemp" value="${bean.stateId }" class="width30">
								<select id="state" class="tooltip" disabled="disabled"  style="width:31.5%;">
									<option>-Select-</option>
									<c:forEach items="${requestScope.stateList}" var="statelist" varStatus="status">
										<option value="${statelist.stateId }">${statelist.stateName}</option>
									</c:forEach>
								</select>
							</div>	
							<div>
								<label class="width30"><fmt:message key="re.property.info.city"/></label>
								<input type="hidden" id="cityTemp" name="cityTemp" value="${bean.cityId }" class="width30 validate[required]">
								<select id="city" class="tooltip" disabled="disabled"  style="width:31.5%;">
									<option value="">-Select-</option>
									<c:forEach items="${requestScope.cityList}" var="citylist" varStatus="status">
										<option value="${citylist.cityId }">${citylist.cityName}</option>
									</c:forEach>
								</select>
							</div>	
								<div style="display:none;">
								<label class="width30"><fmt:message key="re.property.info.district"/></label>
								<input type="hidden" id="districtTemp" name="districtTemp" value="${bean.districtName }" class="width30 validate[required]">
								<select id="district" class="width30 tooltip" disabled="disabled">
									<option value="">-Select-</option>
									<option value="MAS">Chennai</option>
									<option value="TPJ">Trichy</option>
									<option value="MDU">Madurai</option>
									<option value="TEN">Tirunelveli</option>
								</select>
							</div>	
					</fieldset> 
				</div> 
			</fieldset>
		</form>
		<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.ownerdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important; display:none"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:98% !important; display:none"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
				 		<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/> 
				 		<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width20"><fmt:message key="re.property.info.ownername"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.ownertype"/></th>
			            					<th class="width20"><fmt:message key="re.owner.info.civilidno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.percentage"/></th>
			            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tabOwner" style="">
										<c:forEach items="${requestScope.result1}" var="result" >
											<tr>  	 
												<td class="width20">${result.ownerName }</td>
												<td class="width20">${result.ownerType }</td>	
												<td class="width20">${result.uaeNo }</td> 
												<td class="width20">${result.percentage }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result.propertyInfoOwnerId }" name="actualLineId" id="actualLineId"/>	
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/> 
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addOwner" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editOwner"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delOwner" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="owner_id">${result.ownerId }</td>
												<td style="display:none;"></td>		 
											</tr>  
										</c:forEach>	
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsowner" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>	
		<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
				<form name="newFields1">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.featuredetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important;  display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important;  display:none;"></div>  
					<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:98% !important;  display:none;"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
		 				<input type="hidden" name="childCount" id="childCountFeature"  value="${countSize2}"/> 
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.featurecode"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.features"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail1"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.featuredetail2"/></th>
			            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tabFeature" style="">	
										<c:forEach items="${requestScope.result2}" var="result2" >
											<tr>  
												<td class="width10">${result2.lineNumber }</td>	 
												<td class="width20">${result2.featureCode }</td>
												<td class="width20">${result2.features }</td>	
												<td class="width10">${result2.measurements }</td> 
												<td class="width10">${result2.remarks }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result2.propertyInfoFeatureId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFeature" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFeature"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFeature" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="feature_id">${result2.featureId }</td>	
												<td style="display:none;"></td>	 
											</tr>  
										</c:forEach>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfeature" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
		</div>
		<div id="main-content"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="width:130% !important;"> 
				<form name="newFields1">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.componentdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:98% !important;  display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:98% !important;  display:none;"></div>  
					<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:98% !important;  display:none;"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
		 				<input type="hidden" name="childCount" id="childCountComponent"  value="${countSize3}"/> 
						<div id="hrm"> 
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">
										<tr>  
			            					<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.componenttype"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.noofcomponent"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.noofflatincomponent"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.fromdate"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.todate"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.totalarea"/></th>
			            					<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr>
									</thead>  
									<tbody class="tabComponent" style="">	
										<c:forEach items="${requestScope.result3}" var="result3" >
											<tr> 
												<td class="width10">${result3.lineNumber }</td>
												<td class="width20">${result3.componentTypeName }</td>	
												<td class="width10">${result3.noOfComponent }</td> 
												<td class="width10">${result3.noOfFlatInComponent }</td>
												<td class="width10">${result3.fromDate }</td>
												<td class="width10">${result3.toDate }</td>
												<td class="width10">${result3.totalArea }  ${result3.measurementValue }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result3.propertyComponentId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addComponent" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editComponent"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delComponent" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="feature_id">${result3.componentType }</td>	
												<td style="display:none;"></td>	 
											</tr>  
										</c:forEach>
									</tbody>
	 							</table>
								<div class="iDiv" style="display: none;">
								</div>
							</div>
							<div class="vGrip">
								<span></span>
							</div>
			    		</div>
					</div> 
				</form>
				</div>
				<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowscomponent" style="cursor:pointer;">Add Row</div> 
				</div>
			</div>
		</div>	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px ;"> 
				<div class="portlet-header ui-widget-header float-right headerData" id="close" style="cursor:pointer;"><fmt:message key="re.property.info.close"/></div>
		</div>	
	</div>
</div>


				
