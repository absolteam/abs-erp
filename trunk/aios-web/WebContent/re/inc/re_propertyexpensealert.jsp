<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
 
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var accountTypeId=0; 
$(function(){ 
	$('#expense_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_property_expense.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});

	$('#expense_print').click(function(){  
		var expenseId=Number($('#expenseId').val()); 	
   		// HTML PRINT  
   		if('${THIS.templatePrint}' == null || '${THIS.templatePrint}' == "false" || '${THIS.templatePrint}'== '') {  
    		window.open('property_expense_payment_print.action?expenseId='+ expenseId +'&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
       		window.location.reload();
   		} 
   		// TEMPLATE PRINT
   		else if('${THIS.templatePrint}' != null && '${THIS.templatePrint}' == "true") { 
   			$.ajax({
   				type:"POST",
   				url: '<%=request.getContextPath()%>/property_expense_payment_print.action?', 
   				async: false, 
   				data : {expenseId: expenseId},
   				dataType: "html",
   				cache: false,
   				success:function(result){   
   					window.open('<%=request.getContextPath()%>/printing/applet/re_print_receipt.html','_blank', 
   								'width=450,height=150,scrollbars=yes,left=200px,top=200px'); 
   				},
   				error:function(result){  
   					alert("Error");
   				}
   			});
   			return false;
   		} 
	});  
}); 
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				<fmt:message key="re.property.propertyExpenseInfo"/>
			</div>	
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 <fmt:message key="re.property.info.generalInfo" />
			</div>	
			<div><form id="directPaymentCreation">
			<div style="width: 98%; margin: 10px;">
			<div>
			<div class="tempresult" style="display: none;"></div>
			<input type="hidden" id="chequeFlag" value="false"/>
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset> 
						<input type="hidden" id="expenseId" value="${PROPERTY_EXPENSE.expenseId}"> 							 
						<div style="padding: 3px;">
							<label class="width30"><fmt:message key="re.property.expense.expenseVoucher" /></label> 
							<span class="width60 view">${PROPERTY_EXPENSE.expenseNumber}</span>
										
						</div> 
						<div style="padding: 3px;">
							<label class="width30"><fmt:message key="re.property.info.propertyName" /></label>  
							<span class="width60 view">${PROPERTY_EXPENSE.property.propertyName}</span> 
						</div>  
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset>
						<div style="padding: 3px;">
							<label class="width30"><fmt:message key="sys.common.date" /></label>  
							<c:set var="payDate" value="${PROPERTY_EXPENSE.date}"/>  
							<%String date = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
							<span class="width60 view"><%=date%></span>  
						</div>    	 
						<div style="padding: 3px;">
							<label class="width30"><fmt:message key="re.property.expense.expenseType" /></label>  
							<c:set var="expenseType" value="${PROPERTY_EXPENSE.expenseType}"/>
							<span class="width60 view">
								<%=Constants.RealEstate.PropertyExpenseType.get(Byte.valueOf(pageContext.getAttribute("expenseType").toString()))%>
							</span>  
						</div> 
					</fieldset>
				</div> 
			</div>
		
			<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
				<fieldset> 	
					<legend><fmt:message key="account.payment.info" /></legend>
					 <table id="hastab" class="width100"> 
								<thead>
								   <tr>   
								   	   <th style="width:1%"><fmt:message key="re.offer.lineNo" /></th>
								   	    <th style="width:3%">Ref No</th>      
									   <th style="width:3%"><fmt:message key="sys.common.amount" /></th>   
									   <th style="width:5%"><fmt:message key="sys.common.description" /></th>  
								  </tr>
								</thead> 
								<tbody class="tab">  
									<c:forEach var="PAYMENT_DETAIL" items="${PROPERTY_EXPENSE_DETAIL}" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${status.index+1}"> 
											<td id="lineId_${status.index+1}">${status.index+1}</td>  
											 <td>
												 ${PAYMENT_DETAIL.referenceNo}
											</td>
											<td>
												${PAYMENT_DETAIL.amount}
											</td> 
											<td>
												<c:set var="lineDate" value="${PAYMENT_DETAIL.date}"/>  
												<%=DateFormat.convertDateToString(pageContext.getAttribute("lineDate").toString())%> 
											</td>
											<td>
												${PAYMENT_DETAIL.description}
											</td>
											<td style="display:none;">
												<input type="hidden" id="expenseLineId_${status.index+1}" value="${PAYMENT_DETAIL.expenseDetailId}"/>
											</td> 
										</tr>
									</c:forEach>
						 		</tbody>
						</table>
					</fieldset>
				</div>		
				</div> 
			</form> 
			</div> 
			<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
				<div class="portlet-header ui-widget-header float-right" id="expense_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
				<div class="portlet-header ui-widget-header float-right" id="expense_print" style="cursor:pointer;">Print</div>
			 </div> 
		</div>
</div>