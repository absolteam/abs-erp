<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 <%@page import="org.apache.struts2.ServletActionContext"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@page import="java.util.List"%>
 <%@page import="java.util.Map"%>
 <%@page import="com.opensymphony.xwork2.ActionContext"%> 
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<title>Tenant Details</title>
<script type="text/javascript">
	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
</script>
 

<style type="text/css">
.ui-pg-input{

width:30%!important;
}
.ui-jqgrid-btable{
vertical-align:middle!important;
}
.extrapad{
padding:10px!important;
}
</style>

<script type="text/javascript">
var actualID;
$(function (){ 
	$('#paymentMethod').val($('#paymentMethodTemp').val());
	$('#balanceAmount').val(Number($('#dueAmount').val())-Number($('#amount').val()));
	$('.formError').remove(); 
	 $("#releaseInfoAdd").validationEngine({ 
		 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
		var feetotal=0.0;
		var noOfCheques=$('#noOfCheques').val();
		var count=0;
		$('.chequeAmountCalc').each(function(){
			if(count<noOfCheques)
				feetotal+=	parseFloat($(this).html());
			count++;
			$('#chequeAmountTotal').val(feetotal);
			$('#balance').val(feetotal);
		});

		var calculation=0.0;
		var noOfCheques=$('#noOfCheques').val();
		var count1=0;
		$('.feeAmountCalc').each(function(){
			if(count1<noOfCheques)
				calculation+=	parseFloat($(this).html());
			count1++;
			$('#feeAmountTotal').val(calculation);
			$('#dueAmount').val(calculation);
			$('#balanceAmount').val(Number($('#dueAmount').val())-Number($('#amount').val()));
		});

		$("#close").click(function(){ 
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/release_info_list.action",   
		     	async: false,
				dataType: "html",
				cache: false,
				error: function(data) 
				{
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
		     	}
			});
			return true;
		}); 
}); 

</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.releaseinfo"/></div>
		<form id="releaseInfoAdd">
			<fieldset>
				<div class="portlet-content"> 
					<div class="width50 float-right" id="hrm"> 
		 				<fieldset>
							<legend><fmt:message key="re.property.info.releaseinfo2"/></legend>	
							<input type="hidden" id="releaseId" value="${bean.releaseId}"/>
								<div>
									<label><fmt:message key="re.property.info.contractdate"/></label>
									<input type="text" name="date" id="contractDate" class="width30" disabled="disabled" value="${bean.contractDate}"/>
								</div>					
								<div>
									<label><fmt:message key="re.property.info.tenantno"/></label>
									<input type="text" name="tenantId" id="tenantId"class="width30" disabled="disabled" value="${bean.tenantId}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.tenantname"/></label>
									<input type="text" name="tenantName" id="tenantName" class="width30" disabled="disabled" value="${bean.tenantName}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.fromdate"/></label>
									<input type="text" name="fromDate" class="width30" id="fromDate" disabled="disabled" value="${bean.fromDate}"/>
								</div>		
								<div>
									<label><fmt:message key="re.property.info.todate"/></label>
									<input type="text" name="toDate" id="toDate" class="width30" disabled="disabled" value="${bean.toDate}"/>
								</div>																
								<div>
									<label><fmt:message key="re.property.info.watercertifino"/><span style="color:red">*</span></label>
									<input type="text" name="waterCertiNo" id="waterCertiNo" disabled="disabled" class="width30 validate[required]" value="${bean.waterCertiNo}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.watercertifidate"/></label>
									<input type="text" name="waterCertiDate" class="width30" disabled="disabled" id="waterCertiDate" value="${bean.waterCertiDate}"/>
								</div>		
						</fieldset>  																		
					</div> 
					<div class="width50 float-left" id="hrm">
						<fieldset style="width:479px;">
							<legend><fmt:message key="re.property.info.releaseinfo1"/></legend>
							<div>
								<label><fmt:message key="re.property.info.releaseletterno"/><span style="color:red">*</span></label>
								<input type="text" name="tenantReleaseLetterNo" id="tenantReleaseLetterNo" disabled="disabled" class="width30 validate[required]" value="${bean.tenantReleaseLetterNo}"/>
								</div>
								<div style="width:364px;">
									<label style=""><fmt:message key="re.property.info.releaseletterdate"/><span style="color:red">*</span></label>
									<input type="text" name="tenantReleaseLetterDate" id="tenantReleaseLetterDate" disabled="disabled" class="width40 validate[required]" value="${bean.tenantReleaseLetterDate}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.eleccertifino"/><span style="color:red">*</span></label>
									<input type="text" name="electricCertiNo" id="electricCertiNo" disabled="disabled" class="width30 validate[required]" value="${bean.electricCertiNo}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.eleccertifidate"/></label>
									<input type="text" name="electricCertiDate" id="defaultPopup" disabled="disabled" class="width30" readonly="readonly" value="${bean.electricCertiDate}"/>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.contractno"/><span style="color:red">*</span></label>
									<input type="text" id="contractId" class="width30 validate[required]" disabled="disabled" value="${bean.contractId}"/>
									<input type="hidden" id="contractNumber" class="width30 validate[required]" disabled="disabled" />
									<span id="hit7" class="button" style="width: 40px ! important;">
										<a class="btn ui-state-default ui-corner-all contract-popup"> 
											<span class="ui-icon ui-icon-newwin"> </span> 
										</a>
									</span>
								</div>	
								<div>
									<label><fmt:message key="re.property.info.buildingno"/></label>
									<input type="text" name="buildingNumber" class="width30" id="buildingNumber"  disabled="disabled" value="${bean.buildingNumber}"/>
									<input type="hidden" name="buildingId" class="width30" id="buildingId"  disabled="disabled" value="${bean.buildingId}"/>
								</div>
								<div>
									<label><fmt:message key="re.property.info.buildingname"/></label>
									<input type="text" name="buildingName" id="buildingName" class="width30" disabled="disabled" value="${bean.buildingName}"/>
								</div>
								<div>
									<label class=""><fmt:message key="re.contract.paymentmethod"/><span style="color:red">*</span></label>
									<input type="hidden" name="paymentMethod" class="width30" id="paymentMethodTemp" value="${bean.paymentMethod}"/>	 
									<select id="paymentMethod" disabled="disabled" class="width30 validate[required]">
										<option value="">Select</option>
										<option value="C">Cash</option>
										<option value="Q">Cheque</option>
									</select>
								</div>
								<div>
									<label class=""><fmt:message key="re.contract.noofcheque"/><span style="color:red">*</span></label>	 
									<input class="width30 " type="text" name="noOfCheques"  id="noOfCheques" disabled="disabled" value="${bean.noOfCheques}"/>
								</div>
							</fieldset> 
					</div>
				</div>
			</fieldset>
		</form>
		<div class="clearfix"></div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.rentdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
						<div style="display:none;" class="tempresult"></div>
						<input type="hidden" name="trnValue" id="transURN" readonly="readonly"/>
		 				<input type="hidden" name="childCount" id="childCount"  value="${countSize}"/> 
				 		<input type="hidden" name="headerFlag" value="1" id="headerFlag"/>
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.flatno"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.rent"/></th>
			            					<th class="width20" style="display:none;">Paid Amount</th>
			            					<th class="width20"><fmt:message key="re.property.info.balance"/></th>
											<th style="width:10%;display:none;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabRent" style="">	
										<c:forEach items="${requestScope.itemList}" var="result" >
										 	<tr class="even"> 
												<td class="width10">${result.lineNumber}</td>
												<td class="width20">${result.flatNumber}</td>	
												<td class="width20">${result.rentAmount}</td> 
												<td class="width20 message" style="display:none;">${result.message}</td>
												<td class="width20">${result.balanceAmount}</td>
												<td style="display:none"> 
													<input type="text" value="${result.contractRentId}" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>	
												</td> 
												<td style=" width:10%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class="session_status"></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
										<%for(int i=1;i<=2;i++) { %>
										<tr>  
											<td class="width10"></td>	 
											<td class="width20"></td>
											<td class="width20" style="height: 20px;"></td>	
											<td class="width10" style="display:none;"></td> 
											<td class="width10"></td>
											<td style="display:none"> 
												<input type="hidden" value="" name="actualLineId" id="actualLineId"/>
												<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
												<input type="hidden" value="" name="tempLineId" id="tempLineId"/>
											</td> 
											<td style="width:10%; display:none;" > 
												<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRent" style="cursor:pointer;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span>
											   	</a>	
											    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRent"  style="display:none;cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
												</a> 
												<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRent" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
												</a>
												 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
													<span class="processing"></span>
												</a>
											</td>  
											<td style="display:none;"></td>	
											<td style="display:none;"></td>	 
										</tr>  
										<%}%>
									</tbody>
								</table>
							</div>
						</div>
						<div style="margin-bottom:20px;float:right;margin-right: 24%;display:none;" >
							<label class="width10"><fmt:message key="re.property.info.total"/></label>		
							<input class="width40" type="text" name="assignby"  id="assignby" > 
							<input class="width40" type="text" name="assignby"  id="assignby" >
						</div>
					</div> 
				</form>
				<div style="display:none;"  class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsrent" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>	
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container "> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.feedetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div> 
					<div  class="childCountErr response-msg error ui-corner-all" style="width:1063px; display:none;"></div>  
					<div id="warningMsgFee" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content"> 
				 	<div style="display:none;" class="tempresult"></div>
		 				<input type="hidden" name="childCountFee" id="childCountFee"  value="${countSize2}"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100"> 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width10"><fmt:message key="re.property.info.lineno"/></th>
			            					<th class="width10"><fmt:message key="re.property.info.feeid"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.description"/></th>
			            					<th class="width20"><fmt:message key="re.property.info.feeamount"/></th>
			            					<th class="width20" style="display:none;">Paid Amount</th>
			            					<th class="width10" style="display:none;"><fmt:message key="re.property.info.balance"/></th>			            					
											<th style="width:10%;display:none;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabFee" style="">	
										<c:forEach items="${requestScope.result2}" var="result2" varStatus="status">
										 	<tr class="even"> 
												<td class="width10 indexForFee">${status.index+1}</td>
												<td class="width10">${result2.feeId }</td>	
												<td class="width20">${result2.feeDescription }</td> 
												<td class="width20 feeAmountCalc" style="height: 20px;">${result2.feeAmount }</td>
												<td class="width20" style="display:none;">${result2.paidAmount }</td> 
												<td class="width10" style="display:none;"></td>
												<td  style="display:none">  
													<input type="hidden" value="${result2.releaseFeeId}" name="actualLineId" id="actualLineId"/>   
									              	<input type="hidden" name="actionFlag" value="U"/>
									              	<input type="hidden" name="tempLineId" value=""/>  
									 			</td>	
												<td style=" width:10%;display:none;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFee" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   	<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFee"   title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFee" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:28%">
							<label><fmt:message key="re.property.info.total"/></label>
						 	<input name="feeAmountTotal" id="feeAmountTotal" class="width70" disabled="disabled" value="${total}"/>
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
					<div class="portlet-header ui-widget-header float-left addrowsfee" style="cursor:pointer;"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>
		<div id="main-content" style="width:98% !important;"> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container PaymentTAB"> 
				<form name="newFields2">
 					<div class="mainhead portlet-header ui-widget-header" ><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.paymentdetails"/></div>
					<div id="temperror" class="response-msg error ui-corner-all" style="width:1063px; display:none"></div>  
					<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>	 
					<div class="portlet-content">  
				 		<input type="hidden" name="childCountPayment" id="childCountPayment"  value="${noOfCheques}"/> 
						<div id="hrm">
							<div id="hrm" class="hastable width100" > 
								<table id="hastab" class="width100">	
	 								<thead class="chrome_tab">									
										<tr>
											<th class="width20"><fmt:message key="re.property.info.bankname"/></th> 
											<th class="width20"><fmt:message key="re.property.info.chequedate"/></th>
											<th class="width20"><fmt:message key="re.property.info.chequeno"/></th>
											<th class="width20"><fmt:message key="re.property.info.chequeamount"/></th>
											<th style="width:5%;"><fmt:message key="re.property.info.options"/></th>
										</tr> 
									</thead> 
									<tbody class="tabPayment" style="">	
										<c:forEach items="${requestScope.result3}" var="result3" >
										 	<tr class="even"> 
												<td class="width20">${result3.bankName }</td>
												<td class="width20">${result3.chequeDate }</td>	
												<td class="width20">${result3.chequeNumber }</td> 
												<td class="width20 chequeAmountCalc">${result3.chequeAmount }</td>
												<td style="display:none"> 
													<input type="hidden" value="${result3.releasePaymentId }" name="actualLineId" id="actualLineId"/>
													<input type="hidden" name="actionFlag" value="U"/>
					              					<input type="hidden" name="tempLineId" value=""/>		
												</td> 
												<td style=" width:5%;"> 
				  									<a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addPayment" style="display:none;cursor:pointer;" title="Add Record">
				 	 									<span class="ui-icon ui-icon-plus"></span>
												   	</a>	
												   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editPayment"  style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
													</a> 
													<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delPayment" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
													</a>
													 <a  class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;cursor:pointer;" title="Working">
														<span class="processing"></span>
													</a>
												</td>  
												<td style="display:none;" class=""></td>
												<td style="display:none;"></td>	
											</tr>
										</c:forEach>
										
									</tbody>
								</table>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="float-right " style="width:33%">
							<label><fmt:message key="re.property.info.total"/></label>
						 	<input name="chequeAmountTotal" id="chequeAmountTotal" class="width70" disabled="disabled" >
						</div>
						<div class="clearfix"></div>
					</div> 
				</form>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"  style="display:none"> 
					<div class="portlet-header ui-widget-header float-left addrowspayment"><fmt:message key="re.property.info.addrow"/></div> 
				</div>
			</div>
		</div>
		<div id="hrm" class="width100"> 
				<fieldset class="width50" style="margin:0 auto">
				<legend><fmt:message key="re.property.info.finalcalculation"/></legend>						
					<div><label><fmt:message key="re.property.info.dueamount"/></label><input type="text" name="dueAmount" id="dueAmount" class="width30" disabled="disabled"></div>
					<div><label><fmt:message key="re.property.info.depositamount"/></label><input type="text" name="depositAmount" id="amount" class="width30" disabled="disabled" value="${amount}"/></div>
					<div><label><fmt:message key="re.contract.cashamount"/></label><input type="text" name="payableAmount" id="cashAmount" class="width30" ></div>																
					<div style="display:none;"><label><fmt:message key="re.property.info.paidamount"/><span style="color:red">*</span></label><input type="text" name="paidAmount" id="paidAmount" class="width30" ></div>
					<div><label><fmt:message key="re.property.info.balanceamount"/></label><input type="text" name="balanceAmount" id="balanceAmount" class="width30" disabled="disabled"></div>
					<div><label><fmt:message key="re.property.info.balance"/></label><input type="text" name="balance" id="balance" class="width30" disabled="disabled"></div>																
			</fieldset> 
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:0 auto" style="margin:10px ;"> 
			<div class="portlet-header ui-widget-header float-right headerData" id="close" style="cursor:pointer;"><fmt:message key="re.property.info.close"/></div>
 			<div class="portlet-header ui-widget-header float-right headerData" style="cursor:pointer;display:none;">Print</div>
		</div>
	</div>
</div>

	
		


