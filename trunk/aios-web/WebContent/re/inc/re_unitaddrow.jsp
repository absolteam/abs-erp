 <tr id="fieldrow_${requestScope.rowid}" class="rowid"> 
		<td style="display: none;" class="width10" id="lineId_${requestScope.rowid}">${requestScope.rowid}</td>        
        <td class="width20" id="featureCode_${requestScope.rowid}"></td>  	
        <td class="width20" id="featureDetails_${requestScope.rowid}"></td>
        <td class="width10" id="featureUnitDetails_${requestScope.rowid}"></td>
 		<td  style="width:10%;" id="option_${requestScope.rowid}">
            <input type="hidden" value="" name="unitDetailId_${requestScope.rowid}" id="unitDetailId_${requestScope.rowid}"/>	
 			<input type="hidden" name="lineId_${requestScope.rowid}" id="lineId_${requestScope.rowid}" value="${requestScope.rowid}"/>  
 			<input type="hidden" name="lookupDetailId_${requestScope.rowid}" id="lookupDetailId_${requestScope.rowid}"/>
		  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${requestScope.rowid}"  title="Add Record">
		 	 <span class="ui-icon ui-icon-plus"></span>
		   </a>	
		   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"  id="EditImage_${requestScope.rowid}" style="display:none; cursor: pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
			</a> 
			<a style="cursor: pointer;display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${requestScope.rowid}" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
			</a>
			<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${requestScope.rowid}" style="display:none;" title="Working">
				<span class="processing"></span>
			</a>
		</td> 
	</tr>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function(){
	 $(".addData").click(function(event){ 
		 if($('#openFlag').val() == 0) {
		slidetab=$(this).parent().parent().get(0);  
		
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($("#editCalendarVal").validationEngine({returnIsValid:true})){
				$("#AddImage_"+rowid).hide();
				$("#EditImage_"+rowid).hide();
				$("#DeleteImage_"+rowid).hide();
				$("#WorkingImage_"+rowid).show(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/unit_feature_add_get.action",
					data:{id:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			});
			
		}
		else{
			return false;
		}	
	 }else{
		return false;
	 }
	    event.preventDefault();
			 
	});
	$(".editData").click(function(event){
		if($('#propertyInfoAdd').validationEngine({returnIsValid:true})){
			$('#loading').fadeIn();
			slidetab=$(this).parent().parent().get(0); 
			$('.error').hide();
			//Find the Id for fetch the value from Id
			var tempvar=$(this).attr("id");
			var idarray = tempvar.split('_');
			var rowid=Number(idarray[1]); 
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).hide();
			$("#DeleteImage_"+rowid).hide();
			$("#WorkingImage_"+rowid).show(); 
			
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/unit_feature_update.action",  
				data:{id:rowid,addEditFlag:"E"},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					 //Bussiness parameter
					$(result).insertAfter(slidetab);
					var featureCode=$('#featureCode_'+rowid).text();
					var featureUnitDetail=$('#featureUnitDetails_'+rowid).text();
		   			var featureDetail=$('#featureDetails_'+rowid).text();
		   			$('#unitDetails').val(featureUnitDetail);
		   			$('#unitFeatures').val(featureCode);
		   			$('#unitFeatureDetails').val(featureDetail);
				    $('#openFlag').val(1);
	
				}
			});
		}else{
			return false;
		}
        event.preventDefault();
		
	});
   
 	$('.delrow').click(function(event){
 		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		 var lineId=$('#lineId_'+rowid).val();
		 
		var flag=false;
		
	       	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/unit_feature_delete.action", 
			 	async: false,
			 	data:{addEditFlag:"D",id:lineId},
			    dataType: "html",
			    cache: false,
				success:function(result){	
					$('.tempresult').html(result); 
					flag=true;
			 	},  
			 	error:function(result){ 
			 		 $('.tempresult').html(result);
	                 return false;
			 	} 
	       	});
	       	if(flag==true){
	       		 $(this).parent().parent('tr').remove();
	       		 var childCount=Number($('#childCount').val());
	       		 if(childCount > 0){
	       			childCount=childCount-1;
					$('#childCount').val(childCount);
	       		 }

	       		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){  
						i=i+1;
						$($(this).children().get(0)).text(i); 
					 });  
		   }
	        event.preventDefault();
			   
 	});
});
</script>