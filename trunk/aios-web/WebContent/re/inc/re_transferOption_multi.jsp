<div>
	<label class="label_text">Transfer Account No: </label>
	<input type="text" id="transferAccount" style="width: 200px; margin-left: 10px;" value = ""/>
	<div style="height: 10px; display: block;"></div>
	<label id="err_label" style="color: red; display: none;"> Please specify account for transfer.</label>
	<hr/>
	<br/>
	<label class="label_text">Transfer Type</label>
	<br/>
	<br/>
	<input type="radio" name="transferOption" id="connectTransfer" checked="checked" /> Connect and Transfer Electricity
	<br/>
	<input type="radio" name="transferOption" id="onlyTransfer" /> Transfer Electricity
	<br/>
	<br/>
	<hr/>
	<br/>
	<label class="label_text">Contract Print Style</label>
	<br/>
	<br/>
	<input type="radio" name="multiContract" id="Single" checked="checked" /> Single Contract
	<br/>
	<input type="radio" name="multiContract" id="multiple" /> Per Unit Contract
	<br/>
	<br/>
</div>

<style>
	.label_text{
		color: captiontext;
		font-weight: bold;
	}
</style>

<script>
$(function () { 
	
	transferType = "Connect and Transfer";
	transferText = "connect the electricity and transfer the";
	contractPrintStyle = "single";
	
	$('#connectTransfer').click(function() {
		
		transferType = "Connect and Transfer";
		transferText = "connect the electricity and transfer the";
	});
	
	$('#onlyTransfer').click(function() {
		
		transferType = "Transfer";	
		transferText = "transfer the";
	});
	
	$('#Single').click(function() {
		
		contractPrintStyle = "single";
	});
	
	$('#multiple').click(function() {
		
		contractPrintStyle = "multiple";
	});
	
});
</script>