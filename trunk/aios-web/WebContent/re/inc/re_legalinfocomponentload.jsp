<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<option value="">Select</option>
<c:if test="${componentList != null}">
	<c:forEach items="${requestScope.componentList}" var="component">
		<option value="${component.componentId}">${component.componentName} </option>
	</c:forEach>
</c:if> 
