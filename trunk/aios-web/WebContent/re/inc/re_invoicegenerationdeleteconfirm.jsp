<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="/js/jsforAjaxPage.js"></script>
 
 <script type="text/javascript">
 var startpick;
 var slidetab;
 var actualID;
 var exratetype;
 var msg="";
 var currencyCode="";
 var actionName=""; var codeId="";
 $(function(){

	 $('#transactionType').val($('#transactionTypeTemp').val());
	 $('#currencyCodeTemp').val($('#currencyCodeTemp').val());

	 var total=0;
		var curPre=$('#curPrecision').val();
		$('.amountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#amountTotal').val(total)

		total=0;
		$('.functionalAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#functionalAmountTotal').val(total);
	 
	 $("#invoiceGenerationEdit").validationEngine({ 
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});

	 $('.disabledClass').attr('disabled','disabled');

	$('.showClass').hide();

	var ccode="";  
	var myArray = new Array(); 
	var codeSpilt="";
	var codeSpiltedVal=""
	var exrate="";
	var fnamt=""; 
 	$("#currencyCode option").each(function(){ 
		ccode = $(this).val().trim(); 
		codeSpiltedVal=$(this).val().trim(); 
		myArray=ccode.split("@@");
		codeSpilt=myArray[0];
		codeSpiltedVal=myArray[1];	  
		ccode=codeSpilt.toUpperCase();
		ccode=ccode.toString();  
		if(ccode=="AED"){  
			$("#currencyCode option[value='AED@@"+codeSpiltedVal+"']").attr("selected", "selected"); 
			$("#rateType option[value='T']").attr("selected", "selected");  
			$('#rateType').attr('disabled',true);
			exrate=Number(1).toFixed(codeSpiltedVal); 
			$('#exchangeRate').val(exrate);
			$('#curPrecision').val(codeSpiltedVal);
		}  
	});

 	//select CurCode and CurPrec
 	var selectCurCode=$('#currencyCode :selected').val();
 	myArray=selectCurCode.split("@@");
 	selectCurCode=myArray[0];
 	$('#selectCurCode').val(selectCurCode);
 	var selectCurPrec=myArray[1];
 	$('#selectCurPrec').val(selectCurPrec);
 	 
 	$('#currencyCode').change(function() { 
        $("#currencyCode option:selected").each(function () {
       	 currencyCode = $(this).text();
       	
           });
      })
     .trigger('change');

	 $('#currencyCode').change(function(){ 
	 		//select CurCode and CurPrec
 	 	selectCurCode=$('#currencyCode :selected').val();
 	 	myArray=selectCurCode.split("@@");
 	 	selectCurCode=myArray[0];
 	 	$('#selectCurCode').val(selectCurCode);
 	 	selectCurPrec=myArray[1];
 	 	$('#selectCurPrec').val(selectCurPrec); 

		var currencyCode=$('#currencyCode :selected').val();
		var res=$('#fnCurrencyCode').val()+"@@"+$('#fnCurPrec').val();

		if(currencyCode == res){
			$('#rateType option[value="T"]').attr("selected","selected");
			var thisPrec=Number($('#fnCurPrec').val());
			var exRate=Number(1);
			var exRateVal=exRate.toFixed(thisPrec);
			$('#exchangeRate').val(exRateVal);
			$('#rateType').attr("disabled",true);
			$('#exchangeRate').attr("disabled",true);
		}else{
			currencyCode=$('#selectCurCode').val();
			$.ajax({
			      type:"GET",
			      url:"<%=request.getContextPath()%>/exchange_rate_retreive_action.action",
			      data:{currencyCode:currencyCode},
			      async: false,
				  dataType: "html",
				  cache: false,
				  success:function(result){  
					  //alert(result);
			    	  $('.tempresult').html(result); 
				    	  if($('#objExRate').html() != 0 && $('#objRvRate').html() != 0){
			    		  $('#rateType option[value="T"]').attr("selected","selected");
				    	  $('#rateType').attr("disabled",false);
				    	  $('#exchangeRate').attr("disabled",true);
 				    	  $('#exchangeRate').val(Number($('#objExRate').html()).toFixed(Number($('#objPercision').html())));
 				    	  $('#tableExRate').val(Number($('#objExRate').html()).toFixed(Number($('#objPercision').html())));
  			    	  }else{
  			    		  $('#rateType option[value="U"]').attr("selected","selected");
  			    		  $('#rateType').attr("disabled",true);
   			    		  $('#exchangeRate').val("");
  			    		  $('#exchangeRate').attr("disabled",false);
  			    		  $('#tableExRate').val("");
  			    		  $('#exchangeRate').focus();
			    	  }
			      },
			      error:function(result){
			    	  $('.tempresult').html(result); 
			      }
			});
  		}
	});

	$('#rateType').change(function(){
		var exratetype=$('#rateType :selected').val();
		if(exratetype == 'T'){
   			 $('#exchangeRate').val($('#tableExRate').val());
  			 $('#exchangeRate').attr("disabled",true);
		}
  		else{
  			$('#exchangeRate').val("");
  			$('#exchangeRate').focus();
			$('#exchangeRate').attr("disabled",false);
  		}
	});

	$('#chequeAmount').blur(function(){
		var chqamt=Number($('#chequeAmount').val());
		var exRate=$('#exchangeRate').val();
		var curPrec=$('#selectCurPrec').val();
		chqamt=chqamt.toFixed(curPrec);
		var chqFnamt=(chqamt*exRate).toFixed(curPrec); 
		$('#chequeAmount').val(chqamt); 
		$('#functionalCurrencyValue').val(chqFnamt);
	});
		
	$('#exchangeRate').blur(function(){
		var exRate=Number($('#exchangeRate').val());
		var tempPrec=Number($('#selectCurPrec').val());
		var finalVal=(exRate).toFixed(tempPrec);
		$('#exchangeRate').val(finalVal);

		var chqamt=Number($('#amount').val());
		var fnCurrVal=(exRate*chqamt).toFixed(tempPrec);
		$('#functionalAmount').val(fnCurrVal);
	});
				
	$('#amount').blur(function(){ 
		var lmt=$('#amount').val();
		var loanamt="";
		ccode=$('#currencyCode :selected').text(); 
		codeSpiltedVal=$('#currencyCode :selected').val();  
		myArray=codeSpiltedVal.split("@@");   
		codeSpiltedVal=myArray[1];  
		if(lmt!=null && lmt!='' && ccode=="AED"){ 
			exrate=Number(1); 
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			exrate=Number(exrate).toFixed(codeSpiltedVal);  
			
			fnamt=(loanamt*exrate).toFixed(codeSpiltedVal); 
			$('#exchangeRateVal').val(exrate);
			$('#amount').val(loanamt);  
			$('#functionalAmount').val(fnamt);
		}
		else{
			loanamt=Number(lmt).toFixed(codeSpiltedVal);  
			$('#exratetype').attr('disabled',false);
			exrate=$('#exchangeRateVal').val(); 
		}
	});
		
	$('.calc').change(function(){
		if($('#chequeAmount').val() != null && $('#chequeAmount').val() != "" && $('#exchangeRate').val() != null && $('#exchangeRate').val() != "" ){
			var unit=parseFloat($('#exchangeRate').val());
			var item=parseFloat($('#chequeAmount').val());
			var total=unit * item;
			$('#functionalCurrencyValue').val(total);
		}else{
			$('#functionalCurrencyValue').val("");
		}
	});
	$.fn.globeTotal = function() { 
		var total=0;
		var curPre=$('#curPrecision').val();
		$('.amountCalc').each(function(){ 
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#amountTotal').val(total)

		total=0;
		$('.functionalAmountCalc').each(function(){
			total+=Number($(this).html());
		});
		total=total.toFixed(curPre);
		$('#functionalAmountTotal').val(total); 
 
	 }
	  
	$("#confirm").click(function(){
		invoiceId=$('#invoiceId').val();
		//alert("itemId : "+itemId);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/re_invoice_generation_delete.action",
			data: {invoiceId: invoiceId},  
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result)
			{ 
				$("#main-wrapper").html(result); 
			} 
		}); 
		return true;
	});
	
 	$('#cancel').click(function(){
   	 	$('.formError').remove();
   		$('#loading').fadeIn();
		 $.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/re_invoice_generation_list.action",   
		     	async: false, 
				dataType: "html",
				cache: false,
				error: function(data) 
				{  
				},
		     	success: function(data)
		     	{
					$("#main-wrapper").html(data);  //gridDiv main-wrapper
					$.scrollTo(0,300);
					$('#loading').fadeOut();
		     	}
			});
		return true;
 	});


	// System Date Greater than
	$('#dueDate').datepick();

	//Extra
    var res=$('#fnCurrencyCode').val()+"@@"+$('#fnCurPrec').val();
 	$('#currencyCode').val(res);
	currencyCode=$('#currencyCode :selected').val();
	if(currencyCode == res){
 		$('#exratetype').attr("disabled",true);
 		$('#exchangeRate').attr("disabled",true);
	}else{
		$('#exratetype').attr("disabled",false);
 		$('#exchangeRate').attr("disabled",false);
	}
});
function customRange(dates) { 
	$('.glDateformError').remove();
	 var glDate ="";
	 glDate =$('#glDate').val().trim();
	// var calPeriodId=$('#calPeriodId').val();
	 var ledgerId=$('#headerLedgerId').val();
	 var categoryId=$('#categoryId').val();
	 if(glDate!="" && glDate!=null){
		 $('#status').hide();
		 $('#availErr').hide(); 
		 $('#temperror').hide(); 
		 $('#checking').show();  
		 $("#checking").html('<img align="absmiddle" src="../images/loader.gif" /> Checking availability...');
		
		 $.ajax({
			 type: "POST",
			 url: "<%=request.getContextPath()%>/gl_journal_dateavail.action", 
			 data: {glDate:glDate, ledgerId:ledgerId, categoryId:categoryId},
			  dataType: "html",
			 success: function(result){  
				 $('.tempresult').html(result); 
				msg=$('#resultMessage').html().trim().toUpperCase(); 
				 if(msg == 'OK'){  
					 $('#status').show(); 
					 $('#glCategoryId').val($('#objGlCategoryId').html());
			 		 $('#glCategoryName').val($('#objGlCategoryName').html());
			 		 $('#glLedgerId').val($('#objGlLedgerId').html());
			 		 $('#glPeriodId').val($('#objGlPeriodId').html());
					 $('#status').html(' <img align="absmiddle" src="../images/accepted.png" /> ');
					 $('#status').attr("title", "Validation Success"); 
					 $('#availFlag').val("1");
				 } 
				 else{ 
					 $('#status').show();  
					 var css = {};  
					 css['display'] = 'inline';
					 css['position'] = 'absolute';
					 css['margin-top'] ='5px';
					 css['cursor']='pointer';
					 $('#status').css(css); 
					 $('#status').html(' <img align="absmiddle" src="../images/cancel.png" /> ');
					 $('#status').attr("title", "GL Details missing, please contact administrator.");  
					 $('#availFlag').val("0");
				 }
			} 	
		}); 
		 $('#checking').hide();
	}else{  
		var el=$(this).attr('class');
	
	}
} 

 </script> 
<div id="main-content">
    <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
        <div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Invoice Generation</div>
         <div style="display:none;" class="tempresult"></div>
         <div id="ajaxresult" style="display:none;"></div>    
        <div class="portlet-content">
        	<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMsg}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	
			<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div>
			<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>
			<div id="warningMsg" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>
        	<form name="invoiceGenerationEdit" id="invoiceGenerationEdit">
        		<div class="width100 float-left" id="hrm" style="margin-bottom:10px;">
					<fieldset>
						<legend>Invoice Details</legend>
						<div class="width30 float-left">
							<label class="width30">Invoice Number</label>
							<input type="text" value="${bean.invoiceNumber }" class="width50 disabledClass" name="invoiceNumber" id="invoiceNumber" disabled="disabled"  >
							<input type="hidden" value="${bean.invoiceId }" class="width50 disabledClass" name="invoiceId" id="invoiceId" disabled="disabled"  >
						</div>
						<div class="width30 float-left">
							<label class="width30">Invoice Date<span style="color: red;">*</span></label>
							<input name="glDate" type="text" value="${bean.invoiceDate }" readonly="readonly" id="glDate"  class="startPicker validate[required] width50">
							 <div id="checking"></div>
							<div id="status" class="width5 float-left tooltip" style="position:absolute;display: inline;"></div> 
						</div>
						<div class="width30 float-left">
							<label class="width30">Transaction Type<span class="mandatory">*</span></label>
							<input type="hidden" id="transactionTypeTemp" value="${bean.transactionType}">
							<select class="width50 disabledClass validate[required]" name="transactionType" id="transactionType">
								<option value="">--Select--</option>
								<option value="1">Rent Receivable</option>
								<option value="2">Fee Receivable</option>
							</select>
						</div>
						<div class="clearfix"></div>
						<div class="width30 float-left">
							<label class="width30">Contract Number<span class="mandatory">*</span></label>
							<input type="text" value="${bean.contractNumber }" class="width50 disabledClass  validate[required]" name="contractNumber" id="contractNumber" disabled="disabled"  >
							<input type="hidden" value="${bean.contractId }" class="width50 disabledClass" name="contractId" id="contractId" disabled="disabled"  >
							<span id="hit7" class="button" style="width: 40px ! important;">
								<a class="btn ui-state-default ui-corner-all contract-popup"> 
									<span class="ui-icon ui-icon-newwin"> </span> 
								</a>
							</span>
						</div>
						<div class="width30 float-left">
							<label class="width30">Rent Schedule No.</label>
							<input type="text" value="${bean.rentScheduleNumber }" class="width50 disabledClass" name="rentScheduleNumber" id="rentScheduleNumber">
						</div>
						<div class="width30 float-left">
							<label class="width30">Schedule Line No.</label>
							<input type="text" value="${bean.scheduleLineNumber }" class="width50 disabledClass" name="scheduleLineNumber" id="scheduleLineNumber" >
						</div>
						<div class="clearfix"></div>
						<div class="width30 float-left">
							<label class="width30">Building Name</label>
							<input type="text" value="${bean.buildingName }" class="width50 disabledClass" name="buildingName" id="buildingName" disabled="disabled"  >
							<input type="hidden" value="${bean.buildingId }" class="width50 disabledClass" name="buildingId" id="buildingId" disabled="disabled"  >
						</div>
						<div class="width30 float-left">
							<label class="width30">Building Component</label>
							<input type="text" value="${bean.buildingComponent }" class="width50 disabledClass" name="buildingComponent" id="buildingComponent" disabled="disabled" >
							<input type="hidden" value="${bean.buildingComponentId }" class="width50 disabledClass" name="buildingComponentId" id="buildingComponentId">
						</div>
						<div class="width30 float-left">
							<label class="width30">Tenant Name</label>
							<input type="text" value="${bean.tenantName }" class="width50 disabledClass" name="tenantName" id="tenantName" disabled="disabled"  >
							<input type="hidden" value="${bean.tenantId }" class="width50 disabledClass" name="tenantId" id="tenantId" disabled="disabled"  >
						</div>
						<div class="clearfix"></div>
						<div class="width30 float-left">
							<label class="width30">Due Date<span class="mandatory">*</span></label>
							<input type="text" value="${bean.dueDate }" class="width50 disabledClass validate[required]" name="dueDate" id="dueDate" readonly="readonly"  >
						</div>
						<div class="width30 float-left">
							<label class="width30">Amount<span class="mandatory">*</span></label>
							<input type="text" value="${bean.amount }" class="width50 disabledClass validate[required,custom[onlyFloat]]" name="amount" id="amount">
						</div>
						<div class="width30 float-left">
							<label class="width30">Functional Amount<span class="mandatory">*</span></label>
							<input type="text" value="${bean.functionalAmount }" class="width50 disabledClass" name="functionalAmount" id="functionalAmount" disabled="disabled"  >
						</div>
					</fieldset>
				</div>
				<div class="clearfix"></div>
				<div class="width100" id="hrm" style="margin-bottom:10px;">
					<fieldset>
						<legend>Currency Details</legend>
						<div class="width30 float-left">
							<label class="width30"><fmt:message key="accounts.banktransaction.label.currency"/><span class="mandatory">*</span></label>
							<input type="hidden" id="currencyCodeTemp" value="${bean.currencyCode }">"
							<select class="width50 disabledClass validate[required]" name="currencyCode" id="currencyCode">
								<option value="">--Select--</option>
								<c:choose>
							 		<c:when test="${selectCurrency != null }">
							 			<c:forEach items="${selectCurrency}" var="currencyBean" varStatus="status" >
											<option value="${currencyBean.currencyCode}@@${currencyBean.curPrecsion}">${currencyBean.currencyCode}</option>
										</c:forEach> 
							 		</c:when>
							 	</c:choose>  
							</select>
						</div>
						<div class="width30 float-left">
								<label class="width30"><fmt:message key="accounts.banktransaction.label.ratetype"/><span class="mandatory">*</span></label>
								<input type="hidden" id="rateTypeTemp" value="${bean.rateType }">
								<select class="width50 disabledClass validate[required]" name="rateType" id="rateType">
									<option value="">--Select--</option>
									<option value="U">User</option>
									<option value="T">Table</option>
								</select>
							</div>
							<div class="width30 float-left">
								<label class="width30"><fmt:message key="accounts.banktransaction.label.exchangerate"/><span class="mandatory">*</span></label>
								<input type="text" value="${bean.exchangeRate }" class="width50 calc disabledClass validate[required,custom[onlyFloat]]" name="exchangeRate" disabled="disabled"  id="exchangeRate">
							</div>
					</fieldset>
				</div>
				<div class="clearfix"></div> 
				<div style="margin-top:10px;" class="width100" id="hrm">
					<fieldset>
						<legend><fmt:message key="accounts.journal.label.desc"/><span style="color: red;">*</span></legend>
						<input type="text" value="${bean.description }" name="description" id="description" class="width90 disabledClass validate[required]" style="margin:5px;" />
					</fieldset>
			 	</div>
				<div class="clearfix"></div>
				<div class="portlet-content"> 
					<div class="childContent">
					<input type="hidden" name="childCount" id="childCount"   value="${countSize}"/>
					<input type="hidden" name="trnValue" value="" id="transURN" readonly="readonly"/>
					 <input type="hidden" name="openFlag" id="openFlag"  value="0"/> 
					<div id="hrm" style="margin-bottom:10px;">
						<div class="hastable">
							<table class="hastab"> 
								<thead>										
								<tr>
									<th class="width10">Line Number</th> 
									<th class="width20">Revenue Code</th>
									<th class="width20">Amount</th>
									<th class="width20">Functional Amount</th>
									<th class="width20">Note</th>
									<th  style="width:5%;"><fmt:message key="accounts.banktransaction.label.options"/></th>
								</tr> 
								</thead> 
							 
								<tbody class="tab">
								<c:forEach items="${requestScope.result1}" var="result" >
									<tr class="invoicelines"> 
										<td class="width10">${result.lineNumber}</td>		 
										<td class="width20">${result.revenueCode}</td>	 
										<td class="width20 amountCalc">${result.linesAmount}</td>   
										<td class="width20 functionalAmountCalc">${result.linesFunctionalAmount}</td>	 
										<td class="width20 ">${result.note}</td> 
										<td style="display:none;">
											<input type="hidden" value="${result.invoiceLineId}" name="actualLineId" id="actualLineId"/>   
							              	<input type="hidden" name="actionFlag" value="U"/>
							              	<input type="hidden" name="tempLineId" value=""/>
										</td> 
										<td style="width:5%;">
											<a style="cursor: pointer;display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" title="Add this row" >
											 	 <span class="ui-icon ui-icon-plus"></span>
											</a>
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" title="Edit this row" >
												<span class="ui-icon ui-icon-wrench"></span>
											</a>
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" title="Delete this Row" >
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td>
										<td style="display:none;"></td>
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
									</tr>
								</c:forEach>
								<c:forEach var="i" begin="${countSize+1}" end="${countSize+3}">
									<tr class="invoicelines"> 
										<td class="width10">${i}</td>		 
										<td class="width20"></td>	 
										<td class="width20 amountCalc"></td>   
										<td class="width20 functionalAmountCalc"></td>	 
										<td class="width20 "></td> 
										<td style="display:none;">
											<input type="hidden" value="0" name="actualLineId" id="actualLineId"/>
											<input type="hidden" value="I" name="actionFlag" id="actionFlag"/> 
											<input type="hidden" value="" name="tempLineId" id="tempLineId"/> 
										 </td> 
										<td style="width:5%;">
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" title="Add this row" >
											 	 <span class="ui-icon ui-icon-plus"></span>
											</a>
											<a style="display:none;cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" title="Edit this row" >
												<span class="ui-icon ui-icon-wrench"></span>
											</a>
											<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" title="Delete this Row" >
												<span class="ui-icon ui-icon-circle-close"></span>
											</a>
											<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  style="display:none;" title="Working">
												<span class="processing"></span>
											</a>
										</td>
										<td style="display:none;"></td>
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
										<td style="display:none;"></td>
										<td style="display:none;"></td> 
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</div>	
					</div>					
				
					<div class="clearfix"></div>
					<div class="float-right " style="width:72%">
						<label><fmt:message key="re.contract.total"/></label>
					 	<input name="amountTotal" id="amountTotal"  style="width:29%;" disabled="disabled" > 
					 	<input name="functionalAmountTotal" id="functionalAmountTotal"  style="width:29%;" disabled="disabled" >
					</div> 
				</div>
				</div>
				
				<div class="clearfix"></div>
	       		<div class="float-right buttons ui-widget-content ui-corner-all" style="">
					<div class="portlet-header ui-widget-header float-right" id="cancel"><fmt:message key="re.offer.cancel"/></div>
				 	<div class="portlet-header ui-widget-header float-right save" id="confirm"><fmt:message key="re.owner.info.confirm"/></div> 
				</div>
				
				
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="curPrecision"/>
<input type="hidden" id="personId" value="${requestScope.personId}"/>
<input type="hidden" id="approverId" value=""/>
<input type="hidden" id="workflowStatus" value="approvesave"/>	
<input type="hidden" id="workflowFnType" value="approvesave"/>	
<input type="hidden" id="codeCombinationHidden" value=""/>
<input type="hidden" id="coaHeaderIdHidden" value=""/>
<input type="hidden" name="fnCurrencyCode" id="fnCurrencyCode" value="${fnCurrency.currencyCode}"/> 
<input type="hidden" name="fnCurPrec" id="fnCurPrec" value="${fnCurrency.precision}"/>
<input type="hidden" id="tableExRate" /> 
<input type="hidden" id="selectCurCode" />
<input type="hidden" id="selectCurPrec" />

<input type="hidden" id="availFlag" value="0"/>




<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; height: 200px; width: auto; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span>
		<a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;">
			<span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;"><fmt:message key="accounts.materialdespatch.button.close"/></span>
		</a>
	</div>
	<div id="contract-popup" class="ui-dialog-content ui-widget-content" style="height: 200px!important; min-height: 48px; width: auto;">
		<div class="contract-result"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.ok"/></button>
			<button type="button" class="ui-state-default ui-corner-all"><fmt:message key="accounts.materialdespatch.button.cancel"/></button>
		</div>
	</div>
</div>	