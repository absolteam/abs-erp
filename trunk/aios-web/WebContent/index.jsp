<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>ERP</title>
<style>
	body {
		background: #fcfcfc;
		font-family: wf_segoe-ui_normal,'Segoe UI',Segoe,'Segoe WP',Tahoma,"Trebuchet MS", Arial, Helvetica, sans-serif;
	}
</style>
</head>
<% if (request.getMethod().equalsIgnoreCase("POST")) { %>
    <frameset cols="1,*" frameborder="no" border="0" noresize>
        <frame name="left" src="window.jsp?KEY=${param.KEY}" bordercolor="#000000" border="0" frameborder="0" scrolling="no" noresize>
        <frame name="main" src="start.html" bordercolor="#000000" border="0" frameborder="0" scrolling="no" noresize>
    </frameset>
<% } else { %>
	<script>
		window.location = "<%=request.getContextPath()%>/index_main.action";
	</script>
<% } %>
</html>