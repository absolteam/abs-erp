<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var memberCardId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	 oTable = $('#MemberCardDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_all_member_cards.action",

		"aoColumns" : [ { 
			"mDataProp" : "cardTypeName"
		 },{
			"mDataProp" : "startDigit"
		 },{
			"mDataProp" : "endDigit"
		 },{
			"mDataProp" : "currentDigit"
		 },{
			"mDataProp" : "statusPoints"
		 }], 
	});	 

	 
 	$jquery("#member_card_validation").validationEngine('attach'); 
		
	$('#edit').click(function(){   
 		if(memberCardId != null && memberCardId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_member_card_entry.action", 
		     	async: false, 
		     	data:{memberCardId: memberCardId},
				dataType: "json",
				cache: false,
				success: function(response){ 
 					 $('#cardType').val(response.memberCardVO.cardType);
					 $('#startDigit').val(response.memberCardVO.startDigit);
					 $('#endDigit').val(response.memberCardVO.endDigit);
					 $('#currentDigit').val(null!=response.memberCardVO.currentDigit ? response.memberCardVO.currentDigit :
						 response.memberCardVO.startDigit);
					 $('#statusPoints').val(response.memberCardVO.statusPoints);
 					 $('#description').val(response.memberCardVO.description);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.")
			return false;
		}
 		return false;
	});  

	$('#delete').click(function(){  
		if(memberCardId != null && memberCardId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_member_card.action", 
	     	async: false,
	     	data:{memberCardId: memberCardId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_member_card.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	$(
																			'#success_message')
																			.hide()
																			.html(
																					"Record deleted..")
																			.slideDown(
																					);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown();
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								alert(
										"Please select a record to delete.");
								return false;
							}
							return false;
						});

		/* Click event handler */
		$('#MemberCardDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				memberCardId = aData.memberCardId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				memberCardId = aData.memberCardId;
			}
		});

		/* Click event handler */
		$('#MemberCardDT tbody tr').live('dblclick', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				memberCardId = aData.memberCardId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				memberCardId = aData.memberCardId;
			}
			$('#edit').trigger('click');
		});

		$('.member_card_discard').click(function(){ 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_member_card.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) { 
												$("#main-wrapper").html(result);
											}
										});
							});

		$('#startDigit').change(function(){   
			if(Number($('#currentDigit').val()) ==0) 
				$('#currentDigit').val($('#startDigit').val());
		});

		$('.member_card_save').click(function(){   
			if($jquery("#member_card_validation").validationEngine('validate')){
			var cardType = $('#cardType').val();
 			var startDigit = Number($('#startDigit').val()); 
 			var endDigit = Number($('#endDigit').val()); 
 			var currentDigit = Number($('#currentDigit').val()); 
 			var statusPoints = Number($('#statusPoints').val()); 
			var description =$('#description').val();  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_member_card.action", 
		     	async: false,
		     	data:{
		     			memberCardId : memberCardId, cardType: cardType, startDigit: startDigit, endDigit: endDigit,
		     			currentDigit: currentDigit, statusPoints: statusPoints, description: description
		     		 },
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_member_card.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	if (memberCardId > 0)
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record updated")
																				.slideDown(
																						1000);
																	else
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record created")
																				.slideDown(
																						1000);

																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else
								return false;
						});
		
		
		{ 
			memberCardId = Number($('#memberCardRecordId').val());
			if(memberCardId > 0) {
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_member_card_entry.action", 
			     	async: false, 
			     	data:{memberCardId: Number($('#memberCardRecordId').val())},
					dataType: "json",
					cache: false,
					success: function(response){ 
						 $('#cardType').val(response.memberCardVO.cardType);
						 $('#startDigit').val(response.memberCardVO.startDigit);
						 $('#endDigit').val(response.memberCardVO.endDigit);
						 $('#currentDigit').val(null!=response.memberCardVO.currentDigit ? response.memberCardVO.currentDigit :
							 response.memberCardVO.startDigit);
						 $('#statusPoints').val(response.memberCardVO.statusPoints);
	 					 $('#description').val(response.memberCardVO.description);
	 					 $('#memberCardRecordId').val(Number(0));
					} 		
				}); 
			} 
		} 
	});
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>member
			card
		</div>
		<div class="portlet-content">
			<input type="hidden" id="memberCardRecordId" value="${requestScope.memberCardId}"/>
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div class="clearfix"></div>
			<form id="member_card_validation" style="position: relative;">
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 100px;">
								<div>
									<label for="status">Status Points<span
										style="color: red;">*</span> </label> <input type="text"
										name="statusPoints" id="statusPoints" tabindex="5"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="cardType">Card Type<span
										style="color: red;">*</span> </label> <select name="cardType"
										id="cardType" class="width51 validate[required]" tabindex="1">
										<option value="">Select</option>
										<c:forEach var="cardType" items="${CARD_TYPES}">
											<option value="${cardType.key}">${cardType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="startDigit">Start Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="startDigit" id="startDigit" tabindex="2"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="endDigit">End Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="endDigit" id="endDigit" tabindex="3"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="currentDigit">Current Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="currentDigit" id="currentDigit" tabindex="4"
										class="width50 validate[required,custom[onlyNumber]]"
										readonly="readonly" />
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
						<div
							class="portlet-header ui-widget-header float-right member_card_discard">
							<fmt:message key="accounts.common.button.cancel" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right member_card_save">
							<fmt:message key="accounts.common.button.save" />
						</div>
					</div>
				</div>
			</form>
			<div id="rightclickarea">
				<div id="coupon_list">
					<table id="MemberCardDT" class="display">
						<thead>
							<tr>
								<th>Card Type</th>
								<th>Start Digit</th>
								<th>End Digit</th>
								<th>Current Digit</th>
								<th>Status Points</th>
							</tr>
						</thead>

					</table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>

			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="edit">
					<fmt:message key="accounts.common.button.edit" />
				</div>
			</div>
		</div>
	</div>
</div>