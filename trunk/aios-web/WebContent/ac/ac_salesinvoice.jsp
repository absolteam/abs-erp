 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var salesInvoiceId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var deliveryFlag=null;
var continuousFlag=null;
$(function(){
	$('.formError').remove();
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove(); 
	$('#price-common-popup').dialog('destroy');		
	$('#price-common-popup').remove(); 
	 
	 oTable = $('#SalesInvoiceDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_sales_invoice_list.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "salesInvoiceDate"
		 }, {
			"mDataProp" : "invoiceTypeName"
		 },{
			"mDataProp" : "customerName"
		 },{
			"mDataProp" : "creditTermName"
		 },{
			"mDataProp" : "statusName"
		 } ], 
	});	 
	 oTable.fnSort( [ [0,'desc'] ] );		
	$('#add').click(function(){   
		salesInvoiceId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_invoice_entry.action", 
	     	async: false, 
	     	data:{salesInvoiceId: salesInvoiceId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  
	
	$('#view').click(function(){   
 		if(salesInvoiceId != null && salesInvoiceId != 0){
 			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_invoice_view.action", 
		     	async: false, 
		     	data:{salesInvoiceId: salesInvoiceId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	});  

	$('#edit').click(function(){   
 		if(salesInvoiceId != null && salesInvoiceId != 0){
 			
 			if(deliveryFlag){
				$('#error_message').hide().html("Can not edit!!! Invoice has been processed already").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
 			
 			if(continuousFlag){
				$('#error_message').hide().html("Can not edit!!! Invoice is used for continuous invoice").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
 			
 			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/sales_invoice_edit.action", 
		     	async: false, 
		     	data:{salesInvoiceId: salesInvoiceId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(salesInvoiceId != null && salesInvoiceId != 0){
			
			if(deliveryFlag){
				$('#error_message').hide().html("Can not delete!!! Invoice has been processed already").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			if(continuousFlag){
				$('#error_message').hide().html("Can not delete!!! Invoice is used for continuous invoice").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/sales_invoice_delete.action", 
	     	async: false,
	     	data:{salesInvoiceId: salesInvoiceId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_sales_invoice.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#SalesInvoiceDT tbody tr').live('click', function () {  
		 $("#edit").css('opacity','1');
   	 	 $("#delete").css('opacity','1');
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  salesInvoiceId = aData.salesInvoiceId;
			  deliveryFlag=aData.deliveryFlag;
	          if(deliveryFlag==true || continuousFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          salesInvoiceId = aData.salesInvoiceId;
	          deliveryFlag=aData.deliveryFlag;
	          continuousFlag=aData.editFlag;
	          if(deliveryFlag==true || continuousFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
  	});
	
	$('#print').click(function(){ 
		if(salesInvoiceId != null && salesInvoiceId != 0){
			window.open("<%=request.getContextPath()%>/sales_invoice_printout.action?salesInvoiceId="+salesInvoiceId+"&format=HTML",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px'); 
			var compKey='${THIS.companyKey}';
			if(compKey=='kane' ){
	 			window.open("<%=request.getContextPath()%>/sales_duty_invoice_printout.action?salesInvoiceId="+salesInvoiceId+"&format=HTML",
						'_blank','width=900,height=600,scrollbars=yes,left=100px,top=2px'); 
	 		}
			return false;
		}else{
			alert("Please select a record to print."); 
		} 
		return false;
	});  
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sales Invoice</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
 	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="sales_invoice_list">
					<table id="SalesInvoiceDT" class="display">
						<thead>
							<tr>
								<th>Reference Number</th> 
								<th>Date</th> 
								<th>Type</th> 
 								<th>Customer</th> 
								<th>Credit Term</th> 
								<th>Status</th> 
							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>	
				<div class="first_li"><span>Edit</span></div>
 				<div class="first_li"><span>Delete</span></div>
 				<div class="first_li"><span>View</span></div>
 				<div class="first_li"><span>Print</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="print"><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div> 
</div> 