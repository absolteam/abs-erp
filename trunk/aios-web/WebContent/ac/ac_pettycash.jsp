<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var pettyCashId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; var editStatus = "false";
var printStatus = "false";
$(function(){ 
	$('.formError').remove();
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove();  
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
	$('#PettyCashDT').dataTable({ 
		"sAjaxSource": "get_pettycash_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Petty Cash", "bVisible": false},
			{ "sTitle": "Voucher No"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Transaction Type"}, 
			{ "sTitle": "Amount"}, 
			{ "sTitle": "Receiver"}, 
			{ "sTitle": "Ref.Type"},  
			{ "sTitle": "COA"}, 
			{ "sTitle": "Narration"}, 
			{ "sTitle": "Created By"},  
			{ "sTitle": "editstatus", "bVisible": false},
			{ "sTitle": "printstatus", "bVisible": false}
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	 
	$('#processJV').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pettycash_processjv.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#updatePettyCashDetail').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/updatepettycash_expensedetail.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#add').click(function(){   
		pettyCashId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_pettycash_entry.action", 
	     	async: false, 
	     	data:{pettyCashId: pettyCashId,settlementVoucherId:0},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(pettyCashId != null && pettyCashId != 0){
			if(editStatus == "false"){
				$('#error_message').hide().html("Can not edit!!! Petty Cash have child.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			} 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_pettycash_entry.action", 
		     	async: false, 
		     	data:{pettyCashId: pettyCashId,settlementVoucherId:0},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide(); 
		if(pettyCashId != null && pettyCashId != 0){
			if(editStatus == "false"){
				$('#error_message').hide().html("Can not delete!!! Petty Cash have child.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			} 
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/pettycash_deletentry.action", 
	     	async: false,
	     	data:{pettyCashId: pettyCashId},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage == "SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_all_petty_cash.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted.").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});  
	
	$("#print").click(function() {  
		if(pettyCashId > 0 && printStatus == "true"){	 
			window.open('show_pettycash_printout.action?pettyCashId='+ pettyCashId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
	
	//init datatable
	oTable = $('#PettyCashDT').dataTable();
	 
	/* Click event handler */
	$('#PettyCashDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          pettyCashId = aData[0];  
	          editStatus = aData[10];
	          printStatus = aData[11];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this ); 
	          pettyCashId = aData[0];  
	          editStatus = aData[10];
	          printStatus = aData[11];
	      }
		  
		  if(editStatus == "true"){ 
        	  $('#edit').css('opacity', 1);
        	  $('#delete').css('opacity', 1);
          } else{
        	  $('#edit').css('opacity', 0.5);
        	  $('#delete').css('opacity', 0.5);
          }
		  
		  if(printStatus == "true"){ 
        	  $('#print').css('opacity', 1); 
          } else{
        	  $('#print').css('opacity', 0.5);
          }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>petty cash</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="petty_cash_list">
					<table class="display" id="PettyCashDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="updatePettyCashDetail"  style="display: none;">Update PettyCash Detail</div>
				<div class="portlet-header ui-widget-header float-right" id="processJV"  style="display: none;">Process JV</div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>  
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>