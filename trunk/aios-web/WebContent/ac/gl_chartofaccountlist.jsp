<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
</style>
<script type="text/javascript">
var accountId=0; var segmentId=0;
var oTable; var selectRow=""; var aSelected = [];  var aData="";
$(function(){
	$('.formError').remove();
	$('#COAAccountDT').dataTable({ 
		"sAjaxSource": "chart_of_accounts_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "ACCOUNT_ID", "bVisible": false},
			{ "sTitle": "SEGMENT_ID",  "bVisible": false},
			{ "sTitle": '<fmt:message key="accounts.coa.label.segmentname"/>'},
			{ "sTitle": '<fmt:message key="accounts.coa.label.accountcode"/>'},
			{ "sTitle": '<fmt:message key="accounts.coa.label.accountdescription"/>'},
			{ "sTitle": '<fmt:message key="accounts.coa.label.accounttype"/>'}, 
		],
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'asc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	 
	$('#add').click(function(){  
		var showPage="add";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/chart_of_accounts_coaaddentry.action", 
	     	async: false,
	     	data:{showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
 			} 		
		}); 
	});

	$('#edit').click(function(){   
		var showPage="edit"; 
		if(accountId>0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/chart_of_accounts_coaeditentry.action", 
		     	async: false,
		     	data:{accountId:accountId, showPage:showPage},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		}else{
			alert("Select any one record to edit.");
			return false;
		}
	});

	$('#delete').click(function(){
		if(accountId>0){
			 var cnfrm = confirm('Selected record will be deleted permanently');
	 		 if(!cnfrm)
	 			return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/chart_of_accounts_coadeleteentry.action", 
		     	async: false,
		     	data:{accountId:accountId, segmentId:segmentId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		}else{
			alert("Select any one record to Delete.");
			return false;
		}
	});

	$('#reset-ledger').click(function(){ 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/reset_ledger.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
 			} 		
		});  
	});
	
	$('#delete-ledger').click(function(){ 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/delete_ledger.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
 			} 		
		});  
	});
	
	// update coa
	$('#update-accounts').click(function(){ 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/udpate_coa_accounts.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				 if(response.returnMessage=="SUCCESS"){
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/chart_of_accounts_retrieve.action", 
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){ 
								$("#main-wrapper").html(result);  
								$('#success_message').hide().html("Records updated.").slideDown(1000); 
								$('#success_message').delay(2000).slideUp();
							}
					 });
				 }
 			} 		
		});  
	});

	//init datatable
	oTable = $('#COAAccountDT').dataTable();
	 
	/* Click event handler */
	$('#COAAccountDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) { 
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          accountId=aData[0]; 
	          segmentId=aData[1]; 
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          accountId=aData[0]; 
	          segmentId=aData[1]; 
	      }
	});

	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('#template-add').click(function(){ 
		$('.response-msg').hide();
		var showPage="add-template";
		if($('#COAAccountDT tbody tr td').hasClass('dataTables_empty')){ 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_combination_template.action", 
		     	async: false, 
		     	data:{showPage:showPage},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#temp-result").html(result);
					$('.callJq').trigger('click'); 
				} 		
			}); 
		}else{  
			$('#error_message').hide().html("Chart Of Accounts already exits.").slideDown(1000);
			$('#error_message').delay(2000).slideUp();
		}
		return false;
	});
	
}); 
</script>
<div id="main-content">
	<div id="temp-result" style="display:none;"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.coa.label.chartofaccounts"/>
		</div>
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">${requestScope.success}</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">${requestScope.error}</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="chartof_accounts">
						<table class="display" id="COAAccountDT"></table>
				</div> 
			</div>		
			<div class="vmenu"> 
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
				<div class="first_li"><span>Template-Add</span></div>
			</div>
			<span class="callJq"></span>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete-ledger" style="display: none;">Delete Ledger</div> 
				<div class="portlet-header ui-widget-header float-right" id="update-accounts" style="display: none;">Update COA</div> 
				<div class="portlet-header ui-widget-header float-right" id="reset-ledger" style="display: none;">Reset Ledger</div> 
				<div class="portlet-header ui-widget-header float-right" id="template-add" style="display: none;"><fmt:message key="accounts.common.button.templateadd"/></div> 			
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit"  ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div> 
			</div>
		</div>
	</div>
</div>