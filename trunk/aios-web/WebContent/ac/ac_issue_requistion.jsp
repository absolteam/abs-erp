<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var issueRequistionId;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 
var editRecord = true;
$(function (){ 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove(); 
	$('.formError').remove(); 
	$('#IssueRequisitionDT').dataTable({ 
 		"sAjaxSource": "issue_requistion_list.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	    "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Issue_Requisition_Id', "bVisible": false},
 			{ "sTitle": 'Reference Number'},
 			{ "sTitle": 'Issue Date'}, 
 			{ "sTitle": 'Requisition'}, 
 			{ "sTitle": 'Location'},  
 			{ "sTitle": 'Issue Person'}, 
 			{ "sTitle": 'EditFlag', "bVisible": false},
 			{ "sTitle": 'Customer'},
 			{ "sTitle": 'Other Reference'},
 			{ "sTitle": 'Description'}
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	

	//init datatable
	oTable = $('#IssueRequisitionDT').dataTable();
	
 	/* Click event handler */
 	$('#IssueRequisitionDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			$(this).addClass('row_selected');
 	       	aData =oTable.fnGetData( this );
 	      	issueRequistionId=aData[0]; 
 	      	editRecord = aData[6];
	        if(editRecord == false){
	        	$("#edit").css('opacity','0.5');
	        	$("#delete").css('opacity','0.5');
	        }else{
	        	$("#edit").css('opacity','1');
	        	$("#delete").css('opacity','1');
	        }
 	    }
 	    else {
 	      oTable.$('tr.row_selected').removeClass('row_selected');
 	      $(this).addClass('row_selected');
 	      aData =oTable.fnGetData( this );
 	      issueRequistionId=aData[0];
 	      editRecord = aData[6];
	      if(editRecord == false){
	       	$("#edit").css('opacity','0.5');
	        $("#delete").css('opacity','0.5');
	      }else{
	        $("#edit").css('opacity','1');
	        $("#delete").css('opacity','1');
	      }
 	    }
 	});
 	
 	$('#add').click(function(){
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/issue_requistion_entry.action",
 			data: {issueRequistionId : 0}, 
 			async:false,
 			dataType:"html",
 			cache:false,
 			success:function(result){
 				$("#main-wrapper").html(result);
 			}
 		});	
 	});

 	$('#edit').click(function(){   
 		if(issueRequistionId!=null && issueRequistionId!=0) {
 			if(editRecord == false){
				$('#error_message').hide().html("Can not edit!!! Return has been done for this issuance.").slideDown();
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/issue_requistion_entry.action", 
		     	async: false,
		     	data:{issueRequistionId: issueRequistionId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				}
			 });
		} else{
			alert("Please select a record to edit.");
			 return false;
		 }
	}); 

 	$('#delete').click(function(){  
 		if(issueRequistionId!=null && issueRequistionId!=0) {
 			if(editRecord == false){
				$('#error_message').hide().html("Can not delete!!! Return has been done for this issuance.").slideDown();
				$('#error_message').delay(2000).slideUp();
				return false;
			}
 			var cnfrm = confirm("Selected record will be permanently deleted.");
 			if(!cnfrm)
 				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_issue_requistion.action", 
		     	async: false,
		     	data:{issueRequistionId: issueRequistionId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);
					 var message=$.trim($('.tempresult').html()); 
					 if(message=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_issue_requistion.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
									$('#success_message').delay(2000).slideUp();
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			alert("Please select a record to delete.");
			 return false;
		 }
	}); 
 	
 	$("#print").click(function() {  
 		if(issueRequistionId!=null && issueRequistionId!=0) {
			window.open('issue_requistion_printout.action?issueRequistionId='+ issueRequistionId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
}); 
</script>

<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Issue Requisition</div> 
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all" style="display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="display:none;"></div> 
			<div class="tempresult" style="display:none;"></div> 
	    	<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="IssueRequisitionDT"></table>
				</div>	
			</div>		
			<div class="vmenu">
	 	      	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="print"><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>	 
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>
