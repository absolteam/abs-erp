 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var invoiceId=0; 
var supplierId = 0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
var invoiceFlag=null;
var continuesFlag=null;
$(function(){
	$('.formError').remove();
	$('#DOMWindow').remove();
	$('#DOMWindowOverlay').remove();
	if(typeof($('#common-popup')!="undefined")){ 
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove();  
		 $('#codecombination-popup').dialog('destroy');		
		 $('#codecombination-popup').remove(); 
	 }
	$('#invoiceDataTable').dataTable({ 
		"sAjaxSource": "getinvoice_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Invoice", "bVisible": false, "bSearchable": false},
			{ "sTitle": "Supplier_ID", "bVisible": false, "bSearchable": false},
			{ "sTitle": "Invoice No."},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Supplier No.", "bVisible": false, "bSearchable": false}, 
			{ "sTitle": "Supplier Name"}, 
			{ "sTitle": "Reference"}, 
			{ "sTitle": "Amount"}, 
			{ "sTitle": "Status"}, 
			{ "sTitle": "Invoiced", "bVisible": false, "bVisible": false, "bSearchable": false},
			{ "sTitle": "Continues", "bVisible": false, "bVisible": false, "bSearchable": false},
			{ "sTitle": "Created By"}
		], 
		"sScrollY": $("#main-content").height() - 235, 
		"aaSorting": [[0, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){   
		invoiceId=Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/invoice_addentry.action", 
	     	async: false, 
	     	data:{invoiceId:invoiceId,recordId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
 			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(invoiceId!=null && invoiceId!=0){
			/* if(invoiceFlag){
				$('#error_message').hide().html("Can not edit!!! Invoice has been processed already").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			} */
			
			if(continuesFlag){
				$('#error_message').hide().html("Can not edit!!! Invoice has been used for continues").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/invoice_edit.action", 
		     	async: false, 
		     	data:{invoiceId:invoiceId, supplierId: supplierId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  
	
	$('#view').click(function(){   
		if(invoiceId!=null && invoiceId!=0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/invoice_view.action", 
		     	async: false, 
		     	data:{invoiceId:invoiceId, supplierId: supplierId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		}else{
			alert("Please select a record to view.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(invoiceId!=null && invoiceId!=0){
			/* if(invoiceFlag){
				$('#error_message').hide().html("Can not delete!!! Invoice has been processed already").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			} */
			if(continuesFlag){
				$('#error_message').hide().html("Can not delete!!! Invoice has been used in continues invoice").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/invoice_deletentry.action", 
	     	async: false,
	     	data:{invoiceId:invoiceId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message=$('.tempresult').html().toLowerCase().trim(); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/get_invoice_payment.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Invoice deleted..").slideDown(1000); 
							$.scrollTo(0,300);
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#print').click(function(){  
		if(invoiceId>0){
			window.open('invoice_payment_print.action?invoiceId='+ invoiceId +'&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		}else{
			alert("Please select a record to print.");
			return false;
		} 
	}); 
	
	//init datatable
	oTable = $('#invoiceDataTable').dataTable();
	 
	/* Click event handler */
	$('#invoiceDataTable tbody tr').live('click', function () {  
		 $("#edit").css('opacity','1');
   	  	 $("#delete").css('opacity','1');
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          invoiceId=aData[0];  
	          supplierId = aData[1];
	          invoiceFlag=aData[8]; 
	          if(invoiceFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	          continuesFlag=aData[9]; 
	          if(continuesFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          invoiceId=aData[0];  
	          supplierId = aData[1];
	          invoiceFlag=aData[8];  
	          if(invoiceFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	          
	          continuesFlag=aData[9]; 
	          if(continuesFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>invoice</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="invoice_list">
						<table class="display" id="invoiceDataTable"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"><span>View</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view"><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>