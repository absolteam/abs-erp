<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable; var selectRow = ""; var aData = ""; var aSelected = [];
	var tenderId = 0; var currencyId = 0;
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#common-popup-tender').dialog('destroy');		
	$('#common-popup-tender').remove();   
	$(function() {

		$('.formError').remove(); 
		tenderGridLoad();		 
		$('#tenderDTList tbody tr').live('click', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          tenderId = aData[0]; 
		          currencyId = aData[5]; 
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          tenderId = aData[0];
		          currencyId = aData[5]; 
		      }
		});
		
		$("#add").click(function() {
  			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/addOrEditTenderRedirect.action", 
		     	async: false,
		     	data: {
		     		tenderId: 0
		     	},
				dataType: "html",
				cache: false,
				success: function(result) { 
					$("#main-wrapper").html(result); 
				} 		
			}); 
 			return false; 
		});
				
		$("#edit").click(function() {
			if(tenderId!=null && tenderId!=0){
 				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/addOrEditTenderRedirect.action", 
			     	async: false,
			     	data: {
			     		tenderId: tenderId
			     	},
					dataType: "html",
					cache: false,
					success: function(result) { 
						$("#main-wrapper").html(result); 
					} 		
				}); 
 			}
			else{
				alert("Please select a record to edit");
				return false;
			} 
		});
				
		$("#delete").click(function() {
			if(tenderId!=null && tenderId!=0){
				var cnfrm = confirm("Selected record will be permanently deleted.");
				if(!cnfrm)
					return false;
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/delete_tender.action", 
			     	async: false,
			     	data: {
			     		tenderId: tenderId 
			     	},
					dataType: "json",
					cache: false,
					success: function(response) { 
						if(response.returnMessage=="SUCCESS"){
							$.ajax({
								type: "POST", 
								url: "<%=request.getContextPath()%>/tenderListRedirect.action", 
						     	async: false, 
								dataType: "html",
								cache: false,
								success: function(result){ 
									$("#main-wrapper").html(result);   
									} 		
							}); 
							$('#success_message').hide().html("Record Deleted").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						}
						else{
							$('#error_message').hide().html("Delete failure").slideDown(1000);
							$('#error_message').delay(2000).slideUp();
							return false;
						}  
					} 		
			}); 
 			}
			else{
				alert("Please select a record to delete");
				return false;
			} 
		});
			
	});
	
	function checkDataTableExsist(){
		oTable = $('#tenderDTList').dataTable();
		oTable.fnDestroy();
		$('#tenderDTList').remove();
		$('#gridDiv').html("<table class='display' id='tenderDTList'></table>");
		tenderGridLoad();
	}
	function tenderGridLoad(){
		$('#tenderDTList').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/getAllTenders.action",
							"sPaginationType" : "full_numbers",
							"bJQueryUI" : true,
							"iDisplayLength" : 25,
							"aoColumns" : [
									{
										"sTitle" : "tenderId",
										"bVisible" : false
									},
									{
										"sTitle" : '<fmt:message key="accounts.tender.list.tenderNumber" />',
										"sWidth" : "100px"
									},
									{
										"sTitle" : '<fmt:message key="accounts.tender.list.date" />',
										"sWidth" : "100px"
									},
									{
										"sTitle" : '<fmt:message key="accounts.tender.list.expiry" />',
										"sWidth" : "100px"
									},
									{
										"sTitle" : "currencyId",
										"bVisible" : false
									},
									{
										"sTitle" : '<fmt:message key="accounts.tender.currency" />',
										"sWidth" : "100px"
									}, ],
							"sScrollY" : $("#main-content").height() - 240,
							"aaSorting" : [ [ 1, 'asc' ] ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex) {
								if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
									$(nRow).addClass('row_selected');
								}
							}
						});

		oTable = $('#tenderDTList').dataTable();
	}
</script>

<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			tender
		</div>
		<div id="success_message" class="response-msg success ui-corner-all"
			style="display: none;"></div>
		<div id="error_message" class="response-msg error ui-corner-all"
			style="display: none;"></div>

		<div style="display: none;" class="tempresults"></div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table id="tenderDTList" class="display"></table>
			</div>
		</div>
		<div class="vmenu">
			<div class="first_li">
				<span>Add</span>
			</div>
			<div class="sep_li"></div>
			<div class="first_li">
				<span>Edit</span>
			</div>
			<div class="first_li">
				<span>Delete</span>
			</div>
		</div>
		<div style="position: relative;" id="action_buttons"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right" id="delete">
				<fmt:message key="re.property.info.delete" />
			</div>
			<div class="portlet-header ui-widget-header float-right" id="edit">
				<fmt:message key="re.property.info.edit" />
			</div>
			<div class="portlet-header ui-widget-header float-right" id="add">
				<fmt:message key="re.property.info.add" />
			</div>
		</div>
	</div>
</div>
