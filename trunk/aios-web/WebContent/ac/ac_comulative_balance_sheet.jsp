
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var multipleValues = null;

$(".pdf-download-call").click(function(){ 
	
		ids = document.getElementById("year").value;
		

			window.open('<%=request.getContextPath()%>/get_comulative_balancesheet_report.action?selectedCalendarIds='
												+ multipleValues, +'_blank',
										'width=800,height=700,scrollbars=yes,left=100px,top=2px');
						return false;

});

$("select[name='year']").change(function() {
    // multipleValues will be an array
    multipleValues = $(this).val() || [];
    
    
	    $('#example').dataTable(
				{
					"sAjaxSource" : "get_ledger_fiscals.action?selectedCalendarIds="
						+ multipleValues,
					"sPaginationType" : "full_numbers",
					"bJQueryUI" : true,
					"bDestroy" : true,
					"iDisplayLength" : 25,
					"aoColumns" : [ {
						"sTitle" : "Balance",

						"sDefaultContent": "n/a" ,
					}, {
						"sTitle" : "Credit/Debit",
							"sDefaultContent": "n/a"
					}, {
						"sTitle" : "Period Name",
							"sDefaultContent": "n/a",
					},  ], 
				
					//"bPaginate": false,
					/* "aaSorting" : [ [ 1, 'desc' ] ], */
					"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
						

						$('td:eq(0)', nRow).html(aData.balance);
						
						if (aData.side == true) {
							$('td:eq(1)', nRow).html("Debit");	
						} else {
							$('td:eq(1)', nRow).html("Credit");
						}						
						
						$('td:eq(2)', nRow).html(aData.period.name);
					},
				
				"sScrollY" : $("#main-content").height() - 235,
	    
	    
	    
	    
	    
	
	});
});
</script>
<div id="main-content">
	<div id="purchase_print" style="display: none;">
		<div class="width100 float-left" id="hrm">
			<div>
				<label class="width30">Contact Person</label> <input type="text"
					class="width60" name="contactPerson" id="contactPerson" />
			</div>
			<div>
				<label class="width30">Delivery Date</label> <input type="text"
					class="width60" name="deliveryDate" id="deliveryDate" />
			</div>
			<div>
				<label class="width30">Delivery Address</label>
				<textarea rows="4" class="width60" id="deliveryAddress"></textarea>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>comulative
			balance sheet

		</div>
		<div class="portlet-content">





			<div class="width45 float-left" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;">Fiscal
						Calendar</label> <select name="year" id="year" style="width: 51%;"
						multiple="multiple">
						<c:forEach var="calendar" items="${calenderList}">
							<option value="${calendar.calendarId}">${calendar.name}</option>
						</c:forEach>
					</select>

				</div>

			</div>

			<div class="width45 float-right" style="padding: 5px;"></div>


			<div class="tempresult" style="display: none;"></div>

			<div id="rightclickarea">
				<div id="ledgerFiscals">
					<table class="display" id="example"></table>
				</div>
			</div>


		</div>
	</div>

</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		
	</div>
</div>