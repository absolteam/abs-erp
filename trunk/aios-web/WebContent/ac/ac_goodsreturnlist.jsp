 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css"> 
#purchase_print{
	overflow: hidden;
}
</style>
<script type="text/javascript"> 
var goodsReturnId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var receiveFlag=null;
var creditNoteFlag=null;
$(function(){
	$('.formError').remove();
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();   
	$('#GoodsReturnsDT').dataTable({ 
		"sAjaxSource": "goodsreturn_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Return_ID", "bVisible": false},
			{ "sTitle": "Return Number"},
			{ "sTitle": "Return Date"},
			{ "sTitle": "Supplier Number"}, 
			{ "sTitle": "Supplier Name"}, 
			{ "sTitle": "GRN"},
			{ "sTitle": "Status", "bVisible": false},
			{ "sTitle": "CreditStatus", "bVisible": false},
		], 
		"sScrollY": $("#main-content").height() - 240, 
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	
	$('#add').click(function(){   
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/goodsreturn_addentry.action", 
	     	async: false, 
	     	data:{returnId : Number(0),recordId : Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				} 		
		});  
	});  
	
	$('#view').click(function(){   
		if(goodsReturnId!=null && goodsReturnId!=0){
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/goodsreturn_editentry.action", 
		     	async: false, 
		     	data:{returnId : goodsReturnId,recordId : Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		} else {
			alert("Please select a record to edit.");
			return false;
		}
	});  
	  
	$('#edit').click(function(){   
		if(goodsReturnId!=null && goodsReturnId!=0){ 
			
			if(creditNoteFlag){
				$('#error_message').hide().html("Can not edit! Return has Debit note.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/goodsreturn_addentry.action", 
		     	async: false, 
		     	data:{returnId : goodsReturnId,recordId : Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
 				} 		
			}); 
		} else {
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		if(goodsReturnId!=null && goodsReturnId!=0){
			 
			if(creditNoteFlag){
				$('#error_message').hide().html("Can not delete! Return has Debit Note.").slideDown(1000);
				$('#error_message').delay(2000).slideUp();
				return false;
			}
			
			
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/goodsreturn_deletentry.action", 
		     	async: false,
		     	data:{returnId: goodsReturnId},
				dataType: "json",
				cache: false,
				success: function(response){  
 					if(response.returnMessage=="SUCCESS"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/getgoods_return.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html("Record deleted.").slideDown(1000); 
 							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(response.returnMessage).slideDown(1000);
						return false;
					} 
				} 		
			});
		} else {
			alert("Please select a record to delete.");
			return false;
		} 
	}); 

	//init datatable
	oTable = $('#GoodsReturnsDT').dataTable();
	 
	/* Click event handler */
	$('#GoodsReturnsDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          goodsReturnId=aData[0];  
	          creditNoteFlag=aData[7];
	          if(creditNoteFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          goodsReturnId=aData[0];
	          creditNoteFlag=aData[7];
	          if(creditNoteFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	});
	
	$("#print").click(function() {  
		if(goodsReturnId>0){	 
			window.open('goods_return_printout.action?returnId='+ goodsReturnId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>goods return</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="goods_returns_list">
						<table class="display" id="GoodsReturnsDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
				<div class="first_li"><span><fmt:message key="accounts.common.button.add"/></span></div>
	 	       	<div class="first_li"><span><fmt:message key="accounts.common.button.edit"/></span></div>
				<div class="sep_li"></div>	 
 				<div class="first_li"><span><fmt:message key="accounts.common.button.delete"/></span></div>
 				<div class="first_li"><span><fmt:message key="accounts.common.button.view"/></span></div>
 				<div class="first_li"><span>Print</span></div>
			</div> 
			 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				 <div class="portlet-header ui-widget-header float-right" id="print">Print</div>
			 	<div class="portlet-header ui-widget-header float-right" id="view"><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>
			</div>
		</div>
	</div>
</div>