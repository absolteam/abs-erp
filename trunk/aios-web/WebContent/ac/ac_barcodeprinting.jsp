<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.ui-autocomplete-input{
	width:43%!important;
}
</style>
<script type="text/javascript"> 
$(function(){
	$('#barcodeReset').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_barcode_printing.action",
						async : false,
						dataType : "html",
						cache : false,
						success : function(result) {
							$("#main-wrapper").html(result);
						}
					});
		});
	
	$('#barcodePrint').click(function(){ 
		var storeId = Number($('#storeId').val());
		var sectionId = Number($('#sectionId').val());
		var rackId = Number($('#rackId').val());
		var shelfId = Number($('#shelfId').val());
		var productId = Number($('#productId').val());
		var quantity = Number($('#quantity').val());
		var noPrint = $('#noPrint').attr('checked');
		window.open('barcode_print_pdf.action?storeId='+ storeId + '&sectionId='+sectionId + '&rackId='+rackId + '&shelfId='+shelfId+ '&noPrint='+noPrint+ '&productId='+productId+ '&quantity='+quantity+ '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		});
	
		$('#storeId').change(function(){  
			var storeId= Number($(this).val());
			$('.sectionDiv').hide();
			$('.rackDiv').hide();
			$('.shelfDiv').hide();
			$('.productDiv').hide();
			$('.qtyDiv').hide();
			$('.noPrintDiv').hide();
			$('#temp-product-stock').html('');
			if(storeId > 0){
				$('#sectionId').html('');
				$('#sectionId')
				.append(
						'<option value="">Select</option>');
				$('#rackId').html('');
				$('#rackId')
				.append(
						'<option value="">Select</option>');
				$('#shelfId').html('');
				$('#shelfId')
				.append(
						'<option value="">Select</option>');
				$('#productId').html('');
				$('#productId')
				.append(
						'<option value="">Select</option>');
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_store_section.action",
						async : false,
						data: {storeId: storeId},
						dataType : "json",
						cache : false,
						success : function(response) {
							if(response.aaData != null){
								$('.sectionDiv').show();
								$(response.aaData)
								.each(
										function(index) {
									$('#sectionId')
										.append('<option value='
												+ response.aaData[index].aisleId
												+ '>' 
												+ response.aaData[index].sectionName
												+ '</option>');
								}); 
							} 
						}
				}); 
			}
			return false;
		});
		
		$('#sectionId').change(function(){  
			var sectionId= Number($(this).val());
			$('.rackDiv').hide();
			$('.shelfDiv').hide();
			$('.productDiv').hide();
			$('.qtyDiv').hide();
			$('.noPrintDiv').hide();
			$('#temp-product-stock').html('');
			if(sectionId > 0){
				$('#rackId').html('');
				$('#rackId')
				.append(
						'<option value="">Select</option>');
				$('#shelfId').html('');
				$('#shelfId')
				.append(
						'<option value="">Select</option>');
				$('#productId').html('');
				$('#productId')
				.append(
						'<option value="">Select</option>');
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_store_rack.action",
						async : false,
						data: {sectionId: sectionId},
						dataType : "json",
						cache : false,
						success : function(response) {
							if(response.aaData != null){
								$(response.aaData)
								.each(
										function(index) {
											$('.rackDiv').show(); 
									$('#rackId')
										.append('<option value='
												+ response.aaData[index].shelfId
												+ '>' 
												+ response.aaData[index].name
												+ '</option>');
								}); 
							} 
						}
				}); 
			}
			return false;
		});
		
		$('#rackId').change(function(){  
			var rackId= Number($(this).val());
 			$('.shelfDiv').hide();
			$('.productDiv').hide();
			$('.qtyDiv').hide();
			$('.noPrintDiv').hide();
			$('#temp-product-stock').html('');
			if(rackId > 0){
				$('#shelfId').html('');
				$('#shelfId')
				.append(
						'<option value="">Select</option>');
				$('#productId').html('');
				$('#productId')
				.append(
						'<option value="">Select</option>');
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_store_shelf.action",
						async : false,
						data: {rackId: rackId},
						dataType : "json",
						cache : false,
						success : function(response) {
							if(response.aaData != null){
 								$('.shelfDiv').show(); 
								$(response.aaData)
								.each(
										function(index) {
									$('#shelfId')
										.append('<option value='
												+ response.aaData[index].shelfId
												+ '>' 
												+ response.aaData[index].name
												+ '</option>');
								}); 
							} 
						}
				}); 
			}
			return false;
		});
		
		$('#shelfId').change(function(){  
			var shelfId= Number($(this).val()); 
			$('.productDiv').hide();
			$('.qtyDiv').hide();
			$('.noPrintDiv').hide();
			$('#temp-product-stock').html('');
			if(shelfId > 0){ 
				$('#productId').html('');
				$('#productId')
				.append(
						'<option value="">Select</option>');
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_store_shelf_product.action",
						async : false,
						data: {shelfId: shelfId},
						dataType : "json",
						cache : false,
						success : function(response) {
							if(response.aaData != null){ 
								$('.productDiv').show(); 
								$('.noPrintDiv').show();
								$(response.aaData)
								.each(
										function(index) {
									$('#productId')
										.append('<option value='
												+ response.aaData[index].productId
												+ '>' 
												+ response.aaData[index].productName
												+ '</option>');
								}); 
							} 
						}
				}); 
			}
			return false;
		});
		
		$('#productId').change(function(){  
			var productId= Number($(this).val()); 
			$('#temp-product-stock').html('');
 			$('.qtyDiv').hide();
			if(productId > 0){ 
				$('.qtyDiv').show();
			}
		});
		
		$('.product-search-barcode').combobox({
			selected : function(event, ui) { 
				var productId = Number($(this).val()); 
				var storeId = Number($('#storeId').val());
				$('.qtyDiv').show();
 				if(productId > 0 && storeId == 0){
					getProductDefaultStore(productId);
					$('.qtyDiv').show();
				}
			}
		});
	});
	
	function getProductStockDetails(productId){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_product_stock_table.action",
			async : false,
			data: {productId: productId},
			dataType : "html",
			cache : false,
			success : function(result) { 
				$("#temp-product-stock").html(result);
			}
		});
	}
	
	function getProductDefaultStore(productId){
		$('#temp-product-stock').html('');
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_product_default_store.action",
			async : false,
			data: {productId: productId},
			dataType : "json",
			cache : false,
			success : function(response) { 
				if(response.storeName!=null && response.storeName!='')
 					$("#temp-product-stock").html(response.storeName);
				else{
					var storeId = Number($('#storeId').val());
					if(storeId == 0)
						$("#temp-product-stock").html("Store not found");
				}
					
			}
		});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Barcode
			Printing
		</div> 
		<form name="barCodeform">
			<div class="portlet-content">
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48" id="hrm">
						<fieldset style="min-height: 50px;">
							<div class="productDiv">
								<label class="width30"> Product </label> <select name="productId"
									id="productId" class="width50 product-search-barcode">
									<option value="">Select</option>
									<c:forEach var="productDetail" items="${PRODUCT_INFO}">
										<option value="${productDetail.productId}">${productDetail.code} - ${productDetail.productName}</option>
									</c:forEach>
								</select>
							</div>
							<div class="qtyDiv">
								<label class="width30"> Quantity </label>
								<input type="text" id="quantity" name="quantity"
									class="width48"/>
							</div>
							<div id="temp-product-stock" style="font-weight: bold; color: #858914;"></div>
						</fieldset>
					</div>
					<div class="float-left width50" id="hrm">
						<fieldset style="min-height: 50px;">
							<div>
								<label class="width30"> Store </label> <select name="storeId"
									id="storeId" class="width50">
									<option value="">Select</option>
									<c:forEach var="storeDetail" items="${STORE_INFO}">
										<option value="${storeDetail.storeId}">${storeDetail.storeName}</option>
									</c:forEach>
								</select>
							</div>
							<div class="sectionDiv" style="display: none;">
								<label class="width30"> Section </label> <select name="sectionId"
									id="sectionId" class="width50">
									<option value="">Select</option>
								</select>
							</div>
							<div class="rackDiv" style="display: none;">
								<label class="width30"> Rack </label> <select name="rackId"
									id="rackId" class="width50">
									<option value="">Select</option>
								</select>
							</div>
							<div class="shelfDiv" style="display: none;">
								<label class="width30"> Shelf </label> <select name="shelfId"
									id="shelfId" class="width50">
									<option value="">Select</option>
								</select>
							</div> 
							<div class="noPrintDiv" style="display: none;">
								<label class="width30"> No Print </label> <input type="checkbox"
									name="noPrint" id="noPrint" class="width5" />
							</div>
						</fieldset>
					</div>
				</div> 
			</div> 
			
			<div class="clearfix"></div>
			<div class="portlet-content">
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
					<div class="portlet-header ui-widget-header float-right"
						id="barcodeReset">
						<fmt:message key="accounts.common.button.reset" />
					</div>
					<div class="portlet-header ui-widget-header float-right"
						id="barcodePrint">
						<fmt:message key="accounts.common.button.print" />
					</div>
				</div>
			</div>
		</form>
	</div>
</div>