<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var receiveId =0;
var oTable;

var fromDate = null;
var toDate = null;
var selectedSupplierId = 0;
var selectedProductId = 0;
var selectedStoreId = 0;



populateDatatable();
	
function populateDatatable(){
	if ($("#supplierId option:selected").val() != undefined
			&& $("#supplierId option:selected").val() != null
			&& $("#supplierId option:selected").val() != "") {
		selectedSupplierId = $("#supplierId option:selected").val();
	}
	
	
	if ($("#productId option:selected").val() != undefined
			&& $("#productId option:selected").val() != null
			&& $("#productId option:selected").val() != "") {
		selectedProductId = $("#productId option:selected").val();
	}
	
	
	if ($("#storeId option:selected").val() != undefined
			&& $("#storeId option:selected").val() != null
			&& $("#storeId option:selected").val() != "") {
		selectedStoreId = $("#storeId option:selected").val();
	}
	

	$('#example').dataTable({ 
		"sAjaxSource": "get_grn_list_json.action?storeId="+selectedStoreId+"&productId="+selectedProductId+"&supplierId="+selectedSupplierId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true,
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Receive_ID", "bVisible": false},
			{ "sTitle": "Receive Number"},
			{ "sTitle": 'Receive Date'},  
			{ "sTitle": "Reference NO."},
			{ "sTitle": 'Amount'}, 
			{ "sTitle": 'Supplier NO.'}, 
			{ "sTitle": 'Supplier Name'}, 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
	receiveId = 0;
}

$('#example tbody tr').live('click', function () {  
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      receiveId=aData[0];   
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      receiveId=aData[0];  
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		window.open("<%=request.getContextPath()%>/get_grn_report_XLS.action?receiveId="+receiveId+"&storeId="+selectedStoreId+"&productId="+selectedProductId+"&supplierId="+selectedSupplierId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
 	
 	$(".print-call").click(function(){ 
 		window.open("<%=request.getContextPath()%>/get_grn_printout.action?receiveId="+receiveId+"&storeId="+selectedStoreId+"&productId="+selectedProductId+"&supplierId="+selectedSupplierId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	

 	$(".pdf-download-call").click(function(){ 
	 	
 		window.open("<%=request.getContextPath()%>/get_grn_report_pdf.action?receiveId="+receiveId+"&storeId="+selectedStoreId+"&productId="+selectedProductId+"&supplierId="+selectedSupplierId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
	
	$('#storeId').combobox({
		selected : function(event, ui) {
			populateDatatable();
		}
	});
	
	$('#productId').combobox({
		selected : function(event, ui) {
			populateDatatable();
		}
	});
	
	$('#supplierId').combobox({
		selected : function(event, ui) {
			populateDatatable();
		}
	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Goods Receive Note
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td width="10%" align="center">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> :
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="10%" align="center">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> :
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
				<tr>
					<td align="center">
						<label >Supplier</label> :
					</td>
					<td>
						<div class="width60">
							<select id="supplierId">
								<option value="">Select</option>
								<c:forEach var="supplier" items="${suppliersList}">
									<option value="${supplier.supplierId}">${supplier.supplierName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td align="center">
						<label >Product</label> :
					</td>
					<td>
						<div class="width60">
							<select id="productId">
								<option value="">Select</option>
								<c:forEach var="product" items="${productsList}">
									<option value="${product.productId}">${product.productName}</option>						
								</c:forEach>
							
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center">
						<label >Store</label> :
					</td>
					<td>
						<div class="width60">
							<select id="storeId">
								<option value="">Select</option>
								<c:forEach var="store" items="${storeList}">
									<option value="${store.storeId}">${store.storeName}</option>						
								</c:forEach>
							
							</select>
						</div>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<div class="width40 float-left" style="padding: 5px;">
				<div>
					<div>
						
					</div>
				</div>
				<div>
					
				</div>
			</div>
			<div class="width40 float-right" style="padding: 5px;">
				<div>
					
				</div>
				
			</div>

			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>