<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var requisitionId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var requisitionFlag=false;
var requisitionStatus = '';
$(function(){
	 $('.formError').remove(); 
 	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
	 oTable = $('#RequisitionDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "getrequistion_list_self.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber",
			"sWidth": "15%"
		 },{
			"mDataProp" : "date",
			"sWidth": "12%"
		 },{
			"mDataProp" : "locationName"
		 }, {
			"mDataProp" : "personName",
			"sWidth": "20%"
		 }, {
			"mDataProp" : "statusStr",
			"sWidth": "10%"
		 }], 
	});	 
	oTable.fnSort( [ [0,'desc'] ] );	
	
	$('#add').click(function(){   
		requisitionId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_requisition_entry.action", 
	     	async: false, 
	     	data:{requisitionId: Number(0),recordId:Number(0),selfFlag:true},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
 		if(requisitionId != null && requisitionId != 0){
 			if(requisitionFlag){
 				$('#error_message').hide().html("Can not edit!!! Requisition has been done for the selected record.").slideDown();
 				$('#error_message').delay(2000).slideUp();
 				return false;
 			}
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_requisition_entry.action", 
		     	async: false, 
		     	data:{requisitionId: requisitionId,recordId:Number(0),selfFlag:true},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(requisitionFlag){
			$('#error_message').hide().html("Can not delete!!! Requisition has been done for the selected record.").slideDown();
			$('#error_message').delay(2000).slideUp();
			return false;
		}
		if(requisitionId != null && requisitionId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false; 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/requisition_delete.action", 
	     	async: false,
	     	data:{requisitionId: requisitionId,selfFlag:true},
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_requisition_list_self.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 
	 
	/* Click event handler */
	$('#RequisitionDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  requisitionId = aData.requisitionId;
			  requisitionStatus = $.trim(aData.statusStr);  
	          if(requisitionStatus != "Open"){
	        	  requisitionFlag = true;
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  requisitionFlag = false;
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          requisitionId = aData.requisitionId;
	          requisitionStatus = $.trim(aData.statusStr);
	          if(requisitionStatus != "Open"){
	        	  requisitionFlag = true;
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  requisitionFlag = false;
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
  	});
	
	$("#print").click(function() {  
		if(requisitionId != null && requisitionId != 0){
			window.open('print_requisition_details.action?requisitionId='+ requisitionId+ '&format=HTML', '',
						'width=800,height=800,scrollbars=yes,left=100px');
		}
		else{
			alert("Please select a record to print.");
			return false;
		}
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Requisition</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
 		 	<div id="rightclickarea">
			 	<div id="material_requisition_list">
					<table id="RequisitionDT" class="display">
						<thead>
							<tr>
								<th>Reference Number</th> 
								<th>Date</th> 
								<th>Location</th>  
								<th>Person</th>  
								<th>Status</th>  
							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
	 	       	<div class="first_li"><span>Edit</span></div>
				<div class="sep_li"></div>		 
 				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>