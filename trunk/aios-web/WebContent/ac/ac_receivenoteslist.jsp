<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var receiveId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var invoiceFlag=null;
$(function(){ 
	 $('#DOMWindow').remove();
	 $('#DOMWindowOverlay').remove();
	 $('#store-popup').dialog('destroy');		
	 $('#store-popup').remove();  
	 $('#store-popuprcv').dialog('destroy');		
	 $('#store-popuprcv').remove();  
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove();   
	 $('.formError').remove();
	$('#ReceiveNoteDT').dataTable({ 
		"sAjaxSource": "receivenotes_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Receive_ID", "bVisible": false},
			{ "sTitle": "Receive Number","sWidth": "40px"},
			{ "sTitle": 'Receive Date',"sWidth": "40px"},  
			{ "sTitle": "Purchase Ref.","sWidth": "150px"},
			{ "sTitle": "Invoice NO.","sWidth": "100px"},
			{ "sTitle": "Delivery NO.","sWidth": "100px"},
			{ "sTitle": 'Amount',"sWidth": "60px"}, 
			{ "sTitle": "Invoiced", "bVisible": false},
			{ "sTitle": 'Supplier NO.', "bVisible": false}, 
			{ "sTitle": 'Supplier Name'}, 
			{ "sTitle": "Status","sWidth": "60px"},
			{ "sTitle": "Created By"}
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){  
		receiveId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/receivenotes_showentry.action", 
	     	async: false,
	     	data:{receiveId:receiveId,recordId:Number(0)},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
			} 		
		}); 
	}); 
	
	$('#receive_barcode').click(function(){    
		 if(receiveId!=null && receiveId!=0){
			 window.open('receive_barcode_print_pdf.action?receiveId='+ receiveId + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');  
		 }else{
				alert("Please select a record to edit.");
				return false;
			}
		});

	$('#view').click(function(){
		if(receiveId!=null && receiveId!=0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/receivenotes_view.action", 
		     	async: false,
		     	data:{receiveId:receiveId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
				} 		
			});
		}
		else{
			alert("Please select a record to view.");
			return false;
		}
	}); 
	
	$('#update_stock').click(function(){ 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/update_grnproduct_stock.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.sqlReturnMessage);
			} 		
		}); 
	}); 
	
	$('#edit').click(function(){
		if(receiveId!=null && receiveId!=0){
			if(invoiceFlag){
				$('#error_message').hide().html("Can not edit!!! GRN is already invoiced ").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/receivenotes_showentry.action", 
		     	async: false,
		     	data:{receiveId:receiveId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);   
				} 		
			});
		}
		else{
			alert("Please select a record to edit.");
			return false;
		}
	}); 


	$('#delete').click(function(){  
		if(receiveId!=null && receiveId!=0){
			if(invoiceFlag){
				$('#error_message').hide().html("Can not delete!!! GRN is already invoiced").slideDown(1000);
				$('#error_message').delay(3000).slideUp();
				return false;
			}
					
			  var cnfrm = confirm('Selected record will be deleted permanently');
				if(!cnfrm)
					return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/receivenotes_deletentry.action", 
		     	async: false,
		     	data:{receiveId:receiveId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
					$.scrollTo(0,300);
				} 		
			}); 
		}
		else{
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#print').click(function(){  
		if(receiveId>0){
			window.open('receive_note_print.action?receiveId='+ receiveId +'&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px'); 
		}else{
			alert("Please select a record to print.");
			return false;
		} 
	}); 

	//init datatable
	oTable = $('#ReceiveNoteDT').dataTable();
	 
	/* Click event handler */
	$('#ReceiveNoteDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          receiveId=aData[0];  
	          invoiceFlag=aData[7];  
	          if(invoiceFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          receiveId=aData[0]; 
	          invoiceFlag=aData[7];  
	          if(invoiceFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>goods receive notes</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Transaction Success</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Transaction Failure</div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
	   	    <div id="rightclickarea">
			 	<div id="journal_accounts">
					<table class="display" id="ReceiveNoteDT"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"><span>View</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="receive_barcode" >Barcode Print</div>
				<div class="portlet-header ui-widget-header float-right" id="update_stock" style="display: none;">update stock</div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="view" ><fmt:message key="accounts.common.button.view"/></div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>