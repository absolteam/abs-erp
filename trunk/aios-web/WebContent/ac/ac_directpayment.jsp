 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
.void_payment_view{
   display: block;
    height: 20px;
    width: 20px; 
    background: url(images/notification-bg.png) no-repeat;
    background-position: center;
    float: right; 
} 
</style>
<script type="text/javascript"> 
var directPaymentId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; var flag = ""; var checkPrint = false;
$(function(){
	$('.formError').remove();
	if(typeof($('#common-popup')!="undefined")){ 
		 $('#common-popup').dialog('destroy');		
		 $('#common-popup').remove();  
		 $('#codecombination-popup').dialog('destroy');		
		 $('#codecombination-popup').remove(); 
	 }
	$('#directPaymentDataTable').dataTable({ 
		"sAjaxSource": "getdirectpayment_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Direct Payment", "bVisible": false,"bSearchable": false},
			{ "sTitle": "Payment NO."},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Payment Mode"}, 
			{ "sTitle": "Amount"},
			{ "sTitle": "Payable"}, 
			{ "sTitle": "Bank Name"}, 
			{ "sTitle": "Account NO."},  
			{ "sTitle": "Cheque NO."},
			{ "sTitle": "Cheque Date"},
			{ "sTitle": "Flag", "bVisible": false,"bSearchable": false},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	 
	$('#processJV').click(function(){   
 		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/direct_payment_processjv.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				alert(response.returnMessage);
			} 		
		}); 
	}); 
	
	$('#add').click(function(){   
		directPaymentId = Number(0);
		var recordId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/direct_payment_addentry.action", 
	     	async: false, 
	     	data:{directPaymentId:directPaymentId, recordId : recordId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
			} 		
		}); 
	});  

	$('#edit').click(function(){    
		var recordId = Number(0);
			if(directPaymentId!=null && directPaymentId!=0){
				if(flag == "true") {
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/direct_payment_addentry.action", 
			     	async: false, 
			     	data:{directPaymentId: directPaymentId, recordId: recordId},
					dataType: "html",
					cache: false,
					success: function(result){ 
						$("#main-wrapper").html(result);  
					} 		
				});
			} else{
				$('#error_message').hide().html("Direct Payment marked as void, can't be edit.").slideDown();
				$('#error_message').delay(2000).slideUp(); 
				return false;
			}
		} else {
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide(); 
		var alertId = Number(0);
		if(directPaymentId!=null && directPaymentId!=0){
			if(flag == "true") {
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/direct_payment_deletentry.action", 
		     	async: false,
		     	data:{directPaymentId: directPaymentId, alertId: alertId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);  
					var message=$.trim($('.tempresult').html().toLowerCase()); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_direct_payments_list.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html("Payment deleted..").slideDown(1000); 
								$('#success_message').delay(2000).slideUp(); 
							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
						$('#error_message').delay(2000).slideUp();
						return false;
					} 
				} 		
			}); 
		}else{
			$('#error_message').hide().html("Direct Payment marked as void, can't be delete.").slideDown();
			$('#error_message').delay(2000).slideUp(); 
			return false;
		}
		} else {
			alert("Please select a record to delete.");
			return false;
		}
	}); 

	$('#print').click(function(){  
   		// HTML PRINT  
   		if(directPaymentId>0){
   			window.open('direct_payment_printview.action?directPaymentId='+ directPaymentId +'&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px'); 
   			return false;
   	   	}else{
			alert("Please select a record to print.");
			return false;
		}
	}); 
	
	$('#chequePrint').click(function(){  
		// HTML PRINT  
   		if(directPaymentId>0){
   			window.open('direct_payment_chequeprint.action?directPaymentId='+ directPaymentId +'&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px'); 
   			return false;
   	   	}else{
			alert("Please select a record to print.");
			return false;
		} 
   		return false;
	}); 

	//init datatable
	oTable = $('#directPaymentDataTable').dataTable();
	 
	/* Click event handler */
	$('#directPaymentDataTable tbody tr').live('click', function () {  
		$('#chequePrint').css('opacity', 0.5);
		chequePrint = false;   
		 if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          directPaymentId=aData[0];  
	          flag = aData[10];
	          var cprint =  $.trim(aData[3]);
 	          if(cprint == 'Cheque'){
	        	  chequePrint = true;   
	        	  $('#chequePrint').css('opacity', 1);
	          } 
		  }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          directPaymentId=aData[0];  
	          flag = aData[10];
	          var cprint =  $.trim(aData[3]);
	          if(cprint == 'Cheque'){
	        	  chequePrint = true;   
	        	  $('#chequePrint').css('opacity', 1);
	          } 
	       }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>direct payments</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="purchase_list">
						<table class="display" id="directPaymentDataTable"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>Cheque Print</span></div>
				<div class="first_li"><span>Print</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="processJV" style="display: none;">Process JV</div>
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="chequePrint" style="opacity: 0.5!important;">Cheque Print</div>
				<div class="portlet-header ui-widget-header float-right" id="print" ><fmt:message key="accounts.common.button.print"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>