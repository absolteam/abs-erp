<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="html" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%-- <c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" charset="utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<title>${THIS.companyName}</title>

<link rel="stylesheet" media="all" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery-ui-1.8.6.custom.css" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	rel="stylesheet" type="text/css" media="all" />

<link
	href="${pageContext.request.contextPath}/css/styles/default/ui.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/forms.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/tables.css"
	rel="stylesheet" type="text/css" media="all" />
<link href="${pageContext.request.contextPath}/css/messages.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/HRM.css" type="text/css"
	media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/validationEngine.jquery.css"
	type="text/css" media="screen" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/jquery.datepick.css">
<link href="${pageContext.request.contextPath}/css/jquery.notice.css"
	rel="stylesheet" type="text/css" media="all" />

<c:choose>
	<c:when test="${sessionScope.lang eq 'ar' }">
		<link
			href="${pageContext.request.contextPath}/css/styles/erp_new/erp_new_ar.css"
			rel="stylesheet" type="text/css" media="all" />
	</c:when>
	<c:otherwise>
		<link
			href="${pageContext.request.contextPath}/css/styles/erp_new/erp_new.css"
			rel="stylesheet" type="text/css" media="all" />
	</c:otherwise>
</c:choose>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
	var $ = $.noConflict(false);
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.2.min.js"></script>
<script type="text/javascript">
	var $jquery = $.noConflict(true);
</script>
<%-- <script src="${pageContext.request.contextPath}/css/styles/erp_new/js/jquery.js" type="text/javascript"></script> --%>
<script
	src="${pageContext.request.contextPath}/css/styles/erp_new/js/cufon-yui.js"
	type="text/javascript"></script>
<%-- <script src="${pageContext.request.contextPath}/css/styles/erp_new/js/Magistral.js" type="text/javascript"></script> --%>
<script
	src="${pageContext.request.contextPath}/css/styles/erp_new/js/customfunctions.js"
	type="text/javascript"></script>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui/flick/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/superfish.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tooltip.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tablesorter.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/tablesorter-pager.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/cookie.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/DateTime.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/date.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/custom.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/scrollTo.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.notice.js"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.validationEngine-en.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.validationEngine.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.num2words.js"
	type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/js/jquery.layout.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/js/jquery.jclock-1.2.0.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<!-- New Menu StyleSheet -->
<!--<link type="text/css" href="${pageContext.request.contextPath}/js/MainMenu/menu.css" rel="stylesheet" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/MainMenu/menu.js"></script>-->

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/menu/pro_menu.css" />
<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}/js/menu/stuHover.js"></script> 
 --%>
<!-- FOR AUTOCOMPLETE -->
<script
	src="${pageContext.request.contextPath}/js/jquery-autocomplete/jquery.ui.autocompletescreen.js"></script>
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/demos.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.autocomplete.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/css/jquery-autocomplete-css/jquery.ui.button.css"
	rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.datepick.js"></script>
<script type="text/javascript">
	var contextPath = "${pageContext.request.contextPath}";
</script>

<!-- FOR TREE -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/treeview/jquery.treeview.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/treeview/jquery.treeview.js"></script>

<script type="text/javascript" 
	src="${pageContext.request.contextPath}/js/vertical.slider.handle.image.js"></script>	
<script
	src="${pageContext.request.contextPath}/js/menu/jquery.hoverIntent.minified.js"></script>

<script
	src="${pageContext.request.contextPath}/js/menu/jquery.dcmegamenu.1.3.3.js"></script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/menu/orange.css"
	type="text/css" />

<link href="${pageContext.request.contextPath}/js/keyboard/jquery.keypad.css" rel="stylesheet"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/keyboard/jquery.plugin.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/keyboard/jquery.keypad.js"></script>


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/data-table_css/demo_table.css"
	type="text/css" />
<script type="text/javascript" language="javascript"
	src="${pageContext.request.contextPath}/js/data-table_js/jquery.dataTables.js"></script>

<link href="${pageContext.request.contextPath}/css/general.css"
	rel="stylesheet" type="text/css" media="all" />


<link rel="stylesheet" type="text/css"
	media="screen and (min-device-width: 800px)"
	href="${pageContext.request.contextPath}/css/W800.css" />
<link rel="stylesheet" type="text/css"
	media="screen and (min-device-width: 1024px)"
	href="${pageContext.request.contextPath}/css/W1024.css" />
<link rel="stylesheet" type="text/css"
	media="screen and (min-device-width: 1280px)"
	href="${pageContext.request.contextPath}/css/W1280.css" />
<link rel="stylesheet" type="text/css"
	media="screen and (min-device-width: 1360px)"
	href="${pageContext.request.contextPath}/css/W1360.css" />


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.notice.css"
	type="text/css" media="all" />
 <script type="text/javascript">
 $(function() {
	$('#common-popup-new').dialog('destroy');		
	$('#common-popup-new').remove(); 
	$('#pos-invoice-popup').dialog('destroy');		
	$('#pos-invoice-popup').remove(); 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove(); 
	$('#pos-common-popup').dialog('destroy');		
	$('#pos-common-popup').remove();   
});
</script>
<style>
.main_oe_overlay {
	background: #000;
	opacity: 0;
	position: fixed;
	top: 0px;
	left: 0px;
	width: 100%;
}
#main-wrapper {
    margin-left: -3px;
    margin-top: -5px;
}
</style>
</head>

<body>
	<div class="wrapper">
		<div id="right_side_container">
			<div id="page-wrapper">
				<div style="margin:0px; width:93%;">
					<%@ include file="../ac/POSOrderingSystemheader.jsp"%> 
					<div id="main-wrapper" class="content_left width100"> 
						<div id="main-content"></div>
					</div>
				</div>
			</div>
		</div>
		<%-- <div>
			<c:choose>
				<c:when test="${sessionScope.pos_items_per_page ne 0}">
					<input type="hidden" id="pos_pagination" value="${sessionScope.pos_items_per_page}" />
				</c:when>
				<c:otherwise>
					<input type="hidden" id="pos_pagination" value="false" />
				</c:otherwise>
			</c:choose>
		</div> --%>
		<div>
			<span style="display: none;"
				class="ui-icon ui-icon-circle-arrow-w float-right show-side-bar"></span>

			<div id="ajaxLoader"
				style="width: 100px; left: 600px; top: 300px; position: absolute; z-index: 9999; display: none;">
				<div
					style="height: 50px; width: 220px; border: solid thin grey; background-color: white;"
					title="Loading...">
					<span
						style="text-align: center; color: grey; width: 220px; margin: 5px; display: block;">Loading...</span>
					<img
						src="${pageContext.request.contextPath}/images/ajaxloadingbar.gif"
						title="Loading...">
				</div>
			</div>
		</div> 
	</div> 
	<div id="message" style="display: none;"> 
	</div>
</body>
</html>