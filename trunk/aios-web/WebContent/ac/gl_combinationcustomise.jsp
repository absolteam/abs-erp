 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.DOMWindow.js"></script>  
<style type="text/css"> 
 
</style>
<script type="text/javascript">  
$(function(){ 
	 $('#common-popup').dialog('destroy');		
	 $('#common-popup').remove(); 
	 
	$('#add').click(function(){ 
		var showPage="add-customise"; 
		var calendarId=""; 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_customise_calendar.action", 
	     	async: false, 
	     	data:{showPage:showPage,calendarId:calendarId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#temp-result").html(result); 
				$.scrollTo(0,300);
			} 		
		}); 
		return false;
	});
	
	$('#implementationAllGroups').change(function(){
		var newImplementationId= $(this).val();
  		$.ajax({
			type:"GET",
			url: '<%=request.getContextPath()%>/reset_company.action',
			async:false,
			data:{newImplementationId:newImplementationId},
			dataType:"html",
			cache:false,
			success:function(result){
				 $("#page-wrapper-hidden-div").html(result);
				 var message=$('#page-wrapper-hidden-div').html(); 
				 if(message.trim()=="SUCCESS"){
					 window.location.href = '<%=request.getContextPath()%>/user_selected_company.action';
				 }else{
					 $('#page-wrapper-error').hide().html("Unable to switch company.").slideDown(1000);
				 }
				
			}
		});
  	});
	
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
});
</script>
<div id="main-content">
	<div id="temp-result" style="display:none;"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="height:60%">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create COA</div>	 	 
		<div class="portlet-content" style="position: relative;"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all">Transaction Success</div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all">Transaction Failure</div>
			</c:if>  
			<div style="font-weight: bold; left: 450px; position: relative; top: 100px; width: 20%">
					WELCOME ${personName}
					<div>
						<c:if test="${AllImplementation ne null}">
							<div style="float: left; margin: 15px 0 0 0 !important;">
								<select id="implementationAllGroups">
									<c:forEach items="${AllImplementation}" var="imp">
										<c:choose>
										<c:when test="${newImplementationId eq imp.implementationId}">
											<option value="${imp.implementationId}" selected="selected">${imp.companyName}</option>
										</c:when>
										<c:otherwise>
											<option value="${imp.implementationId}">${imp.companyName}</option>
										</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</div>
						</c:if>
					</div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" style="position: relative; top: 200px;"> 
				
			<div class="portlet-header ui-widget-header float-right callJq" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>