<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var adjustmentId = 0;
var oTable; var selectRow=""; var aData="";var aSelected = [];var s=0; 

$(function (){ 
	if(typeof($('#codecombination-popup')!="undefined")){  
		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove();  
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
	}
	$('.formError').remove(); 
	$('#reconciliation_adjustment').dataTable({ 
 		"sAjaxSource": "reconciliation_adjustment_list.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	    "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Reconcile_ADJ_ID', "bVisible": false,"bSearchable": false},
 			{ "sTitle": 'Adjustment Number'},
 			{ "sTitle": 'Date',}, 
 			{ "sTitle": 'Account Number',}, 
 			{ "sTitle": 'Description'}, 
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#reconciliation_adjustment').dataTable();

 	/* Click event handler */
 	$('#reconciliation_adjustment tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	       aData =oTable.fnGetData( this );
 	       adjustmentId=aData[0]; 
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	       aData =oTable.fnGetData( this );
 	       adjustmentId=aData[0];
 	       
 	    }
 	});
 	
 	$('#add').click(function(){
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/reconciliation_adjustment_entry.action",
 			data: {adjustmentId : 0,recordId:Number(0)}, 
 			async:false,
 			dataType:"html",
 			cache:false,
 			success:function(result){
 				$("#main-wrapper").html(result);
 			}
 		});	
 	});

 	$('#edit').click(function(){  
 		if(adjustmentId>0) {
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/reconciliation_adjustment_entry.action", 
		     	async: false,
		     	data:{adjustmentId: adjustmentId,recordId:Number(0)},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				}
			 });
		} else{
			 alert("Please select a record to edit.");
			 return false;
		 }
	}); 

 	$('#delete').click(function(){  
 		if(adjustmentId>0) {
 			var cnfrm = confirm("Selected record will be permanently deleted.");
 			if(!cnfrm)
 				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/delete_reconciliation_adjustment.action", 
		     	async: false,
		     	data:{adjustmentId: adjustmentId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);
					 var message=$('.tempresult').html(); 
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_reconciliation_adjustment.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){ 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
									$('#success_message').delay(2000).slideUp();
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			 alert("Please select a record to delete.");
			 return false;
		 }
	}); 
});



</script>

<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">	 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Reconciliation Adjustment</div> 
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all width90" style="display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all width90" style="display:none;"></div> 
			<div class="tempresult" style="display:none;"></div> 
	    	<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="reconciliation_adjustment"></table>
				</div>	
			</div>		
			<div class="vmenu">
	 	      	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
				<div class="first_li"><span>Delete</span></div>
			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete"><fmt:message key="accounts.common.button.delete"/></div>
				<div class="portlet-header ui-widget-header float-right" id="edit"><fmt:message key="accounts.common.button.edit"/></div>	 
				<div class="portlet-header ui-widget-header float-right" id="add"><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>
