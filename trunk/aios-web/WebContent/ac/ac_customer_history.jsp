<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var oTable; var selectRow=""; var aSelected = []; var aData="";

var fromDate = null;
var toDate = null;
var selectedCustomerId = 0;
var selectedStatusId = 0;
var paymentStatus=3;
var selectedType = null;
$(function(){


$('#customers').combobox({
	selected : function(event, ui) {
		selectedCustomerId = $('#customers :selected').val();
	}
});


/* Click event handler */
$('#salesInvoiceDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
		  aData = oTable.fnGetData(this);
		  salesInvoiceId = aData.salesInvoiceId;
      }
      else {
          oTable.$('tr.row_selected').removeClass('row_selected');
          $(this).addClass('row_selected');
          aData = oTable.fnGetData(this);
          salesInvoiceId = aData.salesInvoiceId;
      }
	});



$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});
			
	$(".xls-download-call").click(function(){
		window.open("<%=request.getContextPath()%>/get_sales_invoice_report_XLS.action?customerId="+selectedCustomerId+"&fromDate="+fromDate+"&toDate="+toDate+"&paymentStatus="+paymentStatus,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
 	
 	$(".print-call").click(function(){ 
 		window.open("<%=request.getContextPath()%>/get_customer_history_report_printout.action?customerId="+selectedCustomerId+"&fromDate="+fromDate+"&toDate="+toDate+"&paymentStatus="+paymentStatus+"&format=HTML",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
	 	
 		window.open("<%=request.getContextPath()%>/get_sales_invoice_report_pdf.action?customerId="+selectedCustomerId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});

	 $('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		

	});
	
	 $('.paymentStatus').click(function() {
		 paymentStatus = $(this).val();
	});
	
	populateDatatable();
	
	$('#list').click(function() {
		populateDatatable();
	});
	
	$('#reset').click(function() {
		$(".fromDate").val("");
		$(".toDate").val("");
		toDate="";
		fromDate="";
		selectedCustomerId=0;
		$('#customers').combobox();
	});
});	

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}

	function populateDatatable(){
		oTable=$('#salesInvoiceDT').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "show_customer_history_json.action?customerId="+selectedCustomerId
			+"&fromDate="+fromDate+"&toDate="+toDate+"&paymentStatus="+paymentStatus,

			"aoColumns" : [{
				"mDataProp" : "customerName"
			 },{
				"mDataProp" : "customerNumber"
			 }, {
				"mDataProp" : "pendingInvoice"
			 },{
				"mDataProp" : "creditTermName"
			 } ], 
		});	 
		
		
		
	}
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Customer History Report
		</div>
		<div class="portlet-content">
			<form id="General_Form">
			<table width="100%">
				<tr>
					<td width="10%" align="center">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> :
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="10%" align="center">
						<label style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> :
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
				<tr>
					<td align="center">
						<label >Customer</label> :
					</td>
					<td>
						<div class="width60">
							<select id="customers">
								<option value="">Select</option>
								<c:forEach var="customer" items="${CUSTOMER_LIST}">
									<option value="${customer.customerId}">${customer.customerName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td colspan="2">
						<input type="radio" class="width5 paymentStatus" name="paymentStatus" value="3" checked="checked"><label class="width10">Both</label> 
						<input type="radio" class="width5 paymentStatus" name="paymentStatus" value="2"><label class="width10">Pending</label>
						<input type="radio" class="width5 paymentStatus" name="paymentStatus" value="1"><label class="width10">Complete</label>  
					</td>
				</tr>
			</table>
			</form>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div id="reset" class="portlet-header ui-widget-header float-right reset">
					Clear
				</div>
				<div id="list" class="portlet-header ui-widget-header float-right list">
					List
				</div>
			</div>
			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table id="salesInvoiceDT" class="display">
						<thead>
							<tr>
								<th>Customer</th> 
								<th>Customer Number</th> 
								<th>Pending Amount</th> 
								<th>Credit Term</th> 
							</tr>
						</thead>
				
					</table> 
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS" style="display: none;">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>