<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable; var selectRow = ""; var aData = ""; var aSelected = [];
	var quotationId = 0; var currencyId = 0;
	var purchaseFlag=false;
	$(function() { 
 		$('#common-popup').dialog('destroy');		
		$('#common-popup').remove(); 
		$('.formError').remove();
		$('#QuotationDT').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/showall_quotationjson.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "quotationId", "bVisible": false},	
				{ "sTitle": "currencyId", "bVisible": false},	
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationNumber" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.tender.list.tenderNumber" />', "sWidth": "100px"},
				{ "sTitle": 'Requisition', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationDate" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationExpiry" />', "sWidth": "100px"}, 
				{ "sTitle": 'Supplier', "sWidth": "100px"},
				{ "sTitle": "supplierId", "bVisible": false},
				{ "sTitle": "Status", "sWidth": "60px"},
				{ "sTitle": "status", "bVisible": false}
			],
			"sScrollY": $("#main-content").height() - 240,
			"aaSorting": [[0, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
	
		oTable = $('#QuotationDT').dataTable();		 
		$('#QuotationDT tbody tr').live('click', function () {  
			  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          quotationId = aData[0]; 
		          currencyId = aData[1];
		          purchaseFlag=aData[10];
		          if(purchaseFlag==true){
		        	  $("#edit").css('opacity','0.5');
		        	  $("#delete").css('opacity','0.5');
		          }else{
		        	  $("#edit").css('opacity','1');
		        	  $("#delete").css('opacity','1');
		          }
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData( this );
		          quotationId = aData[0];
		          currencyId = aData[1];
		          purchaseFlag=aData[10];
		          if(purchaseFlag==true){
		        	  $("#edit").css('opacity','0.5');
		        	  $("#delete").css('opacity','0.5');
		          }else{
		        	  $("#edit").css('opacity','1');
		        	  $("#delete").css('opacity','1');
		          }
		      }
		});
		
		$("#add").click(function() {
			$('.error,.success').hide(); 
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/addOrEditQuotationRedirect.action", 
		     	async: false,
		     	data: {
		     		quotationId: 0
		     	},
				dataType: "html",
				cache: false,
				success: function(result) { 
					$("#main-wrapper").html(result); 
				} 		
			}); 
 			return false; 
		});
				
		$("#edit").click(function() { 
			$('.error,.success').hide();
			if(quotationId!=null && quotationId!=0){
				if(purchaseFlag){
					$('#error_message').hide().html("Can not edit!!! Purchase has been done for the selected Quotation ").slideDown();
					$('#error_message').delay(2000).slideUp();
					return false;
				}
 				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/addOrEditQuotationRedirect.action", 
			     	async: false,
			     	data: {
			     		quotationId: quotationId
			     	},
					dataType: "html",
					cache: false,
					success: function(result) { 
						$("#main-wrapper").html(result); 
					} 		
				}); 
 			}
			else{
				alert("Please select a record to edit.");
				return false;
			}
		});
				
		$("#view").click(function() { 
			$('.error,.success').hide();
			if(quotationId!=null && quotationId!=0){
				
 				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/viewQuotationRedirect.action", 
			     	async: false,
			     	data: {
			     		quotationId: quotationId
			     	},
					dataType: "html",
					cache: false,
					success: function(result) { 
						$("#main-wrapper").html(result); 
					} 		
				}); 
 			}
			else{
				alert("Please select a record to view.");
				return false;
			}
		});
		
		$("#delete").click(function() {
			$('.error,.success').hide();
			if(quotationId!=null && quotationId!=0){ 
				if(purchaseFlag){
					$('#error_message').hide().html("Can not delete!!! Purchase has been done for the selected Quotation ").slideDown();
					$('#error_message').delay(2000).slideUp();
					return false;
				}
				var cnfrm = confirm("Selected record will be permanently deleted.");
				if(!cnfrm)
					return false;
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/delete_quotation.action",
											async : false,
											data : {
												quotationId : quotationId,
												currencyId : currencyId
											},
											dataType : "json",
											cache : false,
											success : function(response) {
												if(response.returnMessage=="SUCCESS"){
													$.ajax({
														type: "POST", 
														url: "<%=request.getContextPath()%>/quotationListRedirect.action", 
												     	async: false, 
														dataType: "html",
														cache: false,
														success: function(result){ 
															$("#main-wrapper").html(result);   
 														} 		
													}); 
													$('#success_message').hide().html("Record Deleted").slideDown(1000); 
													$('#success_message').delay(2000).slideUp();
												}
												else{
													$('#error_message').hide().html("Delete failure").slideDown(1000);
													$('#error_message').delay(2000).slideUp();
													return false;
												}  
											}
										});
 							} else {
								alert(
										"Please select a record to delete.") 
								return false;
							}
						});

	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			quotation
		</div>
		<div id="error_message" class="response-msg error ui-corner-all"
			style="display: none;"></div>
		<div id="success_message" class="success response-msg ui-corner-all"
			style="display: none;"></div>
		<div>
			<c:if
				test="${requestScope.errorMsg ne null && requestScope.errorMsg ne ''}">
				<div class="error response-msg ui-corner-all">
					<c:out value="${requestScope.errorMsg}" />
				</div>
			</c:if>
			<c:if
				test="${requestScope.successMsg ne null && requestScope.successMsg ne ''}">
				<div class="success response-msg ui-corner-all">
					<c:out value="${requestScope.successMsg}" />
				</div>
			</c:if>
		</div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table id="QuotationDT" class="display"></table>
			</div>
		</div>
		<div class="vmenu">
			<div class="first_li">
				<span>Add</span>
			</div>
			<div class="sep_li"></div>
			<div class="first_li">
				<span>Edit</span>
			</div>
			<div class="first_li">
				<span>Delete</span>
			</div>
		</div>
		<div style="position: relative;" id="action_buttons"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right" id="view">
				View
			</div>
			<div class="portlet-header ui-widget-header float-right" id="delete">
				<fmt:message key="re.property.info.delete" />
			</div>
			<div class="portlet-header ui-widget-header float-right" id="edit">
				<fmt:message key="re.property.info.edit" />
			</div>
			<div class="portlet-header ui-widget-header float-right" id="add">
				<fmt:message key="re.property.info.add" />
			</div>
		</div>
	</div>
</div>
