<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;
var selectedLocationId = 0;
var selectedPersonId = 0;
var productId = 0;

populateDatatable();

$('#locations').combobox({
	selected : function(event, ui) {
		selectedLocationId = $('#locations :selected').val(); 
	}
});

$('#persons').combobox({
	selected : function(event, ui) {
		selectedPersonId = $('#persons :selected').val(); 
	}
});

$('#productId').combobox({
	selected : function(event, ui) {
		productId = $('#productId :selected').val(); 
	}
});
$('#list_grid').click(function(){  
	populateDatatable();
});
function populateDatatable(){
	fromDate=$('#startPicker').val();
	toDate=$('#endPicker').val(); 
	$('#IssueRequisitionReportDT').dataTable({ 
		"sAjaxSource": "get_issue_requisition_report_list_json.action?productId="+productId+"&locationId="+selectedLocationId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": 'Issue_Requisition_Id', "bVisible": false},
			{ "sTitle": 'Reference Number'}, 
			{ "sTitle": 'Issue Date'}, 
			{ "sTitle": 'Location',},  
			{ "sTitle": 'Requisition Person',},
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#IssueRequisitionReportDT').dataTable();
	selectedRowId =0;
}
var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		if(selectedRowId > 0) {
			window.open("<%=request.getContextPath()%>/get_issue_requisition_report_XLS.action?requisitionId="+selectedRowId,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
		}
		return false; 
	});
 	
 	$(".print-call").click(function(){
 		window.open("<%=request.getContextPath()%>/get_issue_requisition_report_htmlprint.action?productId="+productId+"&locationId="+selectedLocationId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		if(selectedRowId > 0) {
 		window.open("<%=request.getContextPath()%>/get_issue_requisition_report_pdf.action?requisitionId="+selectedRowId+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
 		} else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
		}
		return false;
		
	});

	 
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Issue Requisition Report
		</div>
		<div class="portlet-content">
		<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<table width="100%">
				<tr>
					<td>Location :</td>
					<td>
						<div class="width60">
							<select id="locations">
								<option value="">Select</option>
								<c:forEach var="location" items="${LOCATION_LIST}">
									<option value="${location.locationId}">${location.locationName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td width="15%" align="center">
						From Date:
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td> 
				</tr> 
				<tr>
					<td>Requisition Person :</td>
					<td>
						<div class="width60">
							<select id="persons">
								<option value="">Select</option>
								<c:forEach var="person" items="${PERSON_LIST}">
									<option value="${person.personId}">${person.firstName} ${person.lastName}</option>						
								</c:forEach>
							</select>
						</div>
					</td> 
					<td width="15%" align="center">
						To Date:
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
				<tr>
					<td>Product :</td>
					<td>
						<div class="width60">
							<select id="productId">
								<option value="-1">--All--</option>
								<c:forEach var="product" items="${PRODUCT_INFO}">
									<option value="${product.productId}">${product.code} -- ${product.productName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
				</tr>
			</table>
			<div class="clearfix"></div>
 			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
					style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
					<div class="portlet-header ui-widget-header float-right"
						id="list_grid" style="cursor: pointer; color: #fff;">
						list gird
					</div> 
				</div>
			<div class="clearfix"></div>
			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="IssueRequisitionReportDT">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		 
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
