<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content{padding:1px;}

.buttons { margin:4px;}

table.display td{
padding:3px;
}

.ui-widget-header{
	padding:4px;
}

.ui-autocomplete {
	width:17%!important;
	height: 200px!important;
	overflow: auto;
}
 
.ui-autocomplete-input {
	width: 50%;
}
	
.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}
label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var quotationId = 0; var supplierId = 0; var productId = 0;
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 
	 $('.formError').remove();
	
	 getQuotationsList();
	 
	$('#deliveryDate').datepick(); 
	
	$('#list_grid').click(function(){ 
		getQuotationsList();
	});
	
	 $(".print-call").click(function(){  
		fromDate=$('.fromDate').val();
		toDate=$('.toDate').val(); 
		window.open('<%=request.getContextPath()%>/quotation_report_printouthtm.action?fromDate='+fromDate+'&toDate='+toDate+'&supplierId='+supplierId+'&productId='+productId+"&format=HTML",
					'_blank','width=1000,height=700,scrollbars=yes,left=100px,top=2px'); 
	});
	 $(".pdf-download-call").click(function(){ 
		  if (quotationId != 0) {
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			 percentage=$('#percentage').val();

			window.open('<%=request.getContextPath()%>/quotation_report_download_jasper.action?fromDate='
					+ fromDate + '&toDate='
					+ toDate + '&quotationId='
					+ quotationId+"&format=PDF", +'_blank',
			'width=800,height=700,scrollbars=yes,left=100px,top=2px');	
			return false;
		  } else {
			  	$('#error_message').hide().html("Please select a record to view.").slideDown();
				$('#error_message').delay(3000).slideUp();
			 }
			
	 }); 
	  $(".xls-download-call").click(function(){
		  if (quotationId != 0) {
			 fromDate=$('.fromDate').val();
			 toDate=$('.toDate').val();
			window.open('<%=request.getContextPath()%>/quotation_report_download_xls.action?fromDate='
									+ fromDate + '&toDate='
									+ toDate + '&quotationId='
									+ quotationId, +'_blank',
							'width=800,height=700,scrollbars=yes,left=100px,top=2px');

			return false;
		  } else {
			  $('#error_message').hide().html("Please select a record to view.").slideDown();
			  $('#error_message').delay(3000).slideUp();
			 }
		}); 
	  
	$('#supplierId').combobox({
		selected : function(event, ui) {
			supplierId = Number($(this).val());
		}
	 });
	
	$('#productId').combobox({
		selected : function(event, ui) {
			productId = Number($(this).val());
		}
	 });
	  
    $('#startPicker,#endPicker').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});
}); 

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
} 

function getQuotationsList() { 
	var fromDate=$('.fromDate').val();
	var toDate=$('.toDate').val(); 

	$('#QuotationRDT').dataTable({ 
		"sAjaxSource": "<%=request.getContextPath()%>/getQuotations.action?quotationDate="
			+ fromDate + "&expiryDate=" + toDate + "&productId=" + productId
			+ "&supplier=" + supplierId,				
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "quotationId", "bVisible": false},	
			{ "sTitle": "currencyId", "bVisible": false},	
			{ "sTitle": 'Reference Number', "sWidth": "100px"}, 
			{ "sTitle": 'Tender', "sWidth": "100px"},
			{ "sTitle": 'Requisition', "sWidth": "100px"},
			{ "sTitle": '<fmt:message key="accounts.quotation.quotationDate" />', "sWidth": "110px"}, 
			{ "sTitle": '<fmt:message key="accounts.quotation.quotationExpiry" />', "sWidth": "100px", "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.supplier.supplierNumber" />', "sWidth": "100px", "bVisible": false},
			{ "sTitle": '<fmt:message key="hr.supplier.name" />', "sWidth": "100px", "bVisible": false},
			{ "sTitle": 'Supplier',"sWidth": "100px"},
			{ "sTitle": 'Product',"sWidth": "150px"},
		],
		"sScrollY": $("#main-content").height() - 235,
		"aaSorting": [[1, 'asc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	 
	oTable = $('#QuotationRDT').dataTable();	
}  
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>quotation
		</div>  
		<div class="portlet-content"> 
			<table width="100%">
				<tr>
					<td>Supplier</td>
					<td>
						<div class="width95">	
							<select id="supplierId" name="supplierId" class="supplierId">
								<option value="-1">--All--</option>
								<c:forEach var="supplier" items="${suppliersList}">
									<option value="${supplier.supplierId}">${supplier.supplierName}</option>						
								</c:forEach> 
							</select>
						</div>
					</td>
					<td width="10%" align="center">
						From
					</td>
					<td width="35%">
						<input type="text" 
							name="fromDate" class="width90 fromDate" id="startPicker" readonly="readonly">
					</td>
				</tr>
				<tr>
					<td>Product</td>
					<td>
						<div class="width95">
							<select
								name="productId" id="productId" class="productId">
								<option value="-1">--All--</option>
								<c:forEach var="product" items="${PRODUCT_LIST}">
									<option value="${product.productId}">${product.productName}--${product.code}</option>						
								</c:forEach>
							</select>
						</div>
					</td> 
					<td width="10%" align="center">
						To
					</td>
					<td width="35%">
						<input type="text"
							name="toDate" class="width90 toDate" id="endPicker" readonly="readonly">
					</td>
				</tr>
			</table> 
			<div class="clearfix"></div>
 			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
					style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
					<div class="portlet-header ui-widget-header float-right"
						id="list_grid" style="cursor: pointer; color: #fff;">
						list gird
					</div> 
			</div>
			<div class="clearfix"></div>
			<div id="rightclickarea">			
				<div id="gridDiv">
					<table id="QuotationRDT" class="display"></table>					
				</div>
			</div> 
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS" style="display: none;">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>