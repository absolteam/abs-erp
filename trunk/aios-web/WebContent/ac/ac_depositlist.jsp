<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
 <style type="text/css">
table.display td{
	padding:7px 10px;
} 
.ui-autocomplete-input {width:25%;} 
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
.ui-autocomplete{
	width:194px!important;
}
</style>
<script type="text/javascript"> 
var journalId=0;
var journalDate = ""; 
var totalAmount="";  
var entryDate="";  var debitDetails=""; var combinationIds=new Array();
var transactionIds= new Array();
var linesdescs=new Array();
var oTable; var selectRow=""; var aSelected = []; var aData="";
var creditAmount=new Array();
var journalLineIds=new Array();
$(function(){

	 $('#example').dataTable({ 
		"sAjaxSource": "deposittransaction_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 10,
		"aoColumns": [ 
		    { "sTitle": 'Line Id',"bVisible": false}, 
			{ "sTitle": 'Combination Id',"bVisible": false}, 
			{ "sTitle": 'Select',sWidth:'75px'}, 
			{ "sTitle": 'Combination'}, 
			{ "sTitle": 'Transaction Amount'},
			{ "sTitle": 'Is Debit'},  
			{ "sTitle": 'Description'},
			{ "sTitle": 'Transaction Id',"bVisible": false},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
	//	"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	  
	//init datatable
	oTable = $('#example').dataTable();  
	
	$('.selectcode').live('click',function(){ 
		var currentId=$(this).attr('id');
		var idVal=currentId.split('_');
		var rowId=Number(idVal[1]); 
		var flag=false;
		var transactionId=$('#selectedDetailId_'+rowId).val();
		if($('#selectedDetailId_'+rowId).attr('checked')){
			transactionIds.push(transactionId); 
		} 
		else{
			for(var i=0;i<transactionIds.length;i++){
				if(transactionId==transactionIds[i] && flag!=true){
					transactionIds.remove(i);
					flag=true;
				}
			}
		} 
	});
 
	$('#deposit').live('click',function(){   
		var combinationId=$('.bankaccount').val(); 
		for(var i=0;i<transactionIds.length;i++){
			debitDetails+=transactionIds[i]+",";
		}
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/create_deposittransaction.action", 
	     	async: false,
	     	data:{combinationId:combinationId,debitDetails:debitDetails},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);  
				$.scrollTo(0,300);
			} 		
		});
	});
});
Array.prototype.remove = function(index) {
    this.splice(index, 1);
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Deposits</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	   <div class="width100 float-left" style="position: relative;">
				<select name="bankAccount" class="bankaccount width20">  
					<option value="">Search</option>   
					<c:choose>
						<c:when test="${requestScope.BANKS ne null && requestScope.BANKS ne''}">
							<c:forEach items="${BANKS}" var="bank">
								<optgroup label="${bank.bankName}">
									<c:forEach items="${BANK_ACCOUNT_ENTRIES}" var="account"> 
										<c:choose>
											<c:when test="${bank.bankId eq account.bankId}">
												<option value="${account.combinationId}" style="padding-left: 20px;">${account.bankAccount}</option> 
											</c:when>
										</c:choose> 
									</c:forEach> 
								</optgroup>
							</c:forEach> 
						</c:when>
					</c:choose>
					
				</select>
				<span style="cursor: pointer;"><img style="position: absolute;" width="24" height="24" src="images/icons/Glass.png"></span>
			</div>  
		 	<div id="deposit_accounts">
					<table class="display" id="example"></table>
			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="discard" ><fmt:message key="accounts.common.button.discard"/></div>
				<div class="portlet-header ui-widget-header float-right" id="deposit" >deposit</div>	 
			</div>
		</div>
	</div>
</div>