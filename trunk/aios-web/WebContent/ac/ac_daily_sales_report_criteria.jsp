<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedOrderType = 0;
var selectedPaymentTypes = 0;
var selectedStoreId = 0;
populateDatatable();

$(function(){
	
	$('#paymentTypes').combobox({  
		selected : function(event, ui) {
			selectedPaymentTypes = $('#paymentTypes :selected').val(); 
		}
	});
	
	$('#store').combobox({  
		selected : function(event, ui) {
			selectedStoreId = $('#store :selected').val(); 
		}
	}); 
	
	$('#startPicker,#endPicker').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});

	$('#list_grid').click(function(){
		populateDatatable();
	}); 
	
	$('.fromDate').change(function(){
		fromDate = $(this).val(); 
	});
	
	$('.toDate').change(function(){
		toDate = $(this).val();
	});
	
	$(".xls-download-call").click(function(){
		window.open("<%=request.getContextPath()%>/get_daily_sales_report_XLS.action?selectedStoreId="+selectedStoreId+"&orderType="+selectedOrderType+"&paymentType="+selectedPaymentTypes+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
		
	$(".print-call").click(function(){
		window.open("<%=request.getContextPath()%>/get_daily_sales_report_printout.action?selectedStoreId="+selectedStoreId+"&orderType="+selectedOrderType+"&paymentType="+selectedPaymentTypes+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
			'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
		
	$(".pdf-download-call").click(function(){ 
		window.open("<%=request.getContextPath()%>/get_daily_sales_report_pdf.action?selectedStoreId="+selectedStoreId+"&orderType="+selectedOrderType+"&paymentType="+selectedPaymentTypes+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF",
			'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
	
});

function populateDatatable(){
	$('#DailySalesDT').dataTable({ 
		"sAjaxSource": "get_daily_sales_report_list_json.action?selectedStoreId="+selectedStoreId+"&orderType="+selectedOrderType+"&paymentType="+selectedPaymentTypes+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "POSId", "bVisible": false},
			{ "sTitle": "S.NO"},
			{ "sTitle": "Product"},
			{ "sTitle": "Reference"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Store Name"},
			{ "sTitle": "User"},
	     ], 
		"sScrollY": $("#main-content").height() - 300, 
		"aaSorting": [[1, 'asc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#DailySalesDT').dataTable();
}

var selectedType = null;

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
} 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Daily Sales Report
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td style="display: none;">Order Type :</td>
					<td style="display: none;">
						<div class="width60">
							<select id="orderTypes">
								<option value="">Select</option>
								<option value="1">Dine In</option>
								<option value="2">Take Away</option>
								<option value="1">Delivery</option>						
							</select>
						</div>
					</td>
					<td>Store :</td>
					<td>
						<div class="width60">
							<select id="store">
								<option value="">Select</option>
								<c:forEach var="store" items="${STORE_DETAIL}">
									<option value="${store.storeId}">${store.storeName}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
					<td>Payment Type :</td>
					<td>
						<div class="width60">
							<select id="paymentTypes">
								<option value="">Select</option>
								<c:forEach var="paymentType" items="${PAYMENT_TYPE}">
									<option value="${paymentType.key}">${paymentType.value}</option>						
								</c:forEach>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td width="10%" align="center">
						From 
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" >
					</td>
					<td width="10%" align="center">
						To 
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width60 toDate" id="endPicker"
						readonly="readonly" >
					</td>
				</tr>
			</table>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
				<div class="portlet-header ui-widget-header float-right"
					id="list_grid" style="cursor: pointer; color: #fff;">
					list gird
				</div> 
			</div>
			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="DailySalesDT"> 
					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div> 