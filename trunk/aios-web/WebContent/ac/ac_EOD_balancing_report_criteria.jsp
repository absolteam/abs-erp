<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedUserTillId = 0;
var selectedStoreId = 0;
var selectedPersonId = 0;



 $('#userTills').combobox({
	selected : function(event, ui) {
		selectedUserTillId = $('#userTills :selected').val();
	}
});

$('#stores').combobox({
	selected : function(event, ui) {
		selectedStoreId = $('#stores :selected').val();
	}
});


$('#users').combobox({
	selected : function(event, ui) {
		selectedPersonId = $('#users :selected').val();
	}
}); 

$('#call-eod_list').click(function(){  
	populateDatatable();
}); 
function populateDatatable(){
	$('#example').dataTable({ 
		"sAjaxSource": "get_EOD_balancing_report_list_json.action?userTillId="+selectedUserTillId
		+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "Balance Id", "bVisible": false},
			{ "sTitle": "Reference"},
			{ "sTitle": "Date"}, 
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#example').dataTable();
	selectedRowId = 0;
}



$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
			window.open("<%=request.getContextPath()%>/get_EOD_balancing_report_XLS.action?userTillId="+selectedUserTillId
					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false; 
	});
 	
 	$(".print-call").click(function(){
 			window.open("<%=request.getContextPath()%>/get_EOD_balancing_report_printout.action?userTillId="+selectedUserTillId
 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
 			window.open("<%=request.getContextPath()%>/get_EOD_balancing_report_pdf.action?userTillId="+selectedUserTillId
 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		return false;
		
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
		}

	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			EOD Balancing Report
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td>Till :</td>
					<td>
						<select id="userTills">
							<option value="">Select</option>
							<c:forEach var="pos" items="${TILL}">
								<option value="${pos.posUserTillId}">${pos.tillNumber}</option>						
							</c:forEach>
						</select>
					</td>
					<td>Store :</td>
					<td>
						<select id="stores">
							<option value="">Select</option>
							<c:forEach var="st" items="${STORE_LIST}">
								<option value="${st.storeId}">${st.storeName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				
				<tr>
					<td width="10%" align="center">
						From :
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width95 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="10%" align="center">
						To :
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width95 toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
				<tr>
					<td>User :</td>
					<td>
						<select id="users">
							<option value="">Select</option>
							<c:forEach var="pr" items="${PERSON_LIST}">
								<option value="${pr.personId}">${pr.firstName} ${pr.lastName}</option>						
							</c:forEach>
						</select>
					</td>
					<td colspan="2"><div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						
						<div
							class="portlet-header ui-widget-header float-right call-eod_list"
							id="call-eod_list">
							Get Result
						</div>
					</div></td>
				</tr> 
			</table>

			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="example">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
<div
	style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
	class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
	tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
	<div
		class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
		unselectable="on" style="-moz-user-select: none;">
		<span class="ui-dialog-title" id="ui-dialog-title-dialog"
			unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a
			href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"
			unselectable="on" style="-moz-user-select: none;"><span
			class="ui-icon ui-icon-closethick" unselectable="on"
			style="-moz-user-select: none;">close</span></a>
	</div>
	<div id="common-popup" class="ui-dialog-content ui-widget-content"
		style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<button type="button" class="ui-state-default ui-corner-all">Ok</button>
			<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
		</div>
	</div>
</div>