<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var eodBalancingId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();  
	$('#store_multiselect_popup').dialog('destroy');
	$('#store_multiselect_popup').remove(); 
	 oTable = $('#EODBalancingDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_all_eod_balancing.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNumber"
		 }, {
			"mDataProp" : "eodDate"
		 }, {
			"mDataProp" : "posUserName"
		 },{
			"mDataProp" : "storeName"
		 }], 
	});	 
	 oTable.fnSort( [ [0,'desc'] ] );		
	$('#add').click(function(){   
		eodBalancingId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_eod_balancing_entry.action", 
	     	async: false, 
	     	data:{eodBalancingId: eodBalancingId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	}); 
	
	$('#perform_eod').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/perform_eod_balancing.action", 
	     	async: false, 
			dataType: "json",
			cache: false,
			success: function(response){ 
				if(response.returnMessage == "SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_eod_balancing.action", 
				     	async: false,  
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);
						} 		
					}); 
				} 
			} 		
		}); 
	}); 
	
	$('#eod_print').click(function(){    
		 if(eodBalancingId!=null && eodBalancingId!=0){
			 window.open('eod_balancing_print.action?eodBalancingId='+ eodBalancingId + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');  
		 }else{
				alert("Please select a record to edit.");
				return false;
			}
		});
	 
	/* Click event handler */
	$('#EODBalancingDT tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
			  aData = oTable.fnGetData(this);
			  eodBalancingId = aData.eodBalancingId;
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData(this);
	          eodBalancingId = aData.eodBalancingId;
	      }
  	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>production balancing</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="promotions_list">
					<table id="EODBalancingDT" class="display">
						<thead>
							<tr>
								<th>Reference No</th> 
								<th>Date</th> 
								<th>Person</th> 
								<th>Store</th>  
 							</tr>
						</thead>
				
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
 			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="eod_print" ><fmt:message key="accounts.common.button.print"/></div>	
				<div class="portlet-header ui-widget-header float-right" id="perform_eod" >perform eod</div>	
				<div class="portlet-header ui-widget-header float-right" style="display: none;" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>