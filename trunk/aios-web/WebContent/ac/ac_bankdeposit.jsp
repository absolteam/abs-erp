<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
var bankDepositId = 0;
var oTable; var selectRow=""; var aData="";var aSelected = [];
$(function(){ 
 	$('#codecombination-popup').dialog('destroy');		
	$('#codecombination-popup').remove(); 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove(); 

	$('.formError').remove(); 
	$('#BankDeposit').dataTable({ 
 		"sAjaxSource": "bank_deposit_list_json.action",
 	    "sPaginationType": "full_numbers",
 	    "bJQueryUI": true, 
 	   "iDisplayLength": 25,
 		"aoColumns": [
 			{ "sTitle": 'Deposit ID', "bVisible": false},
 			{ "sTitle": 'Deposit Number'},
 			{ "sTitle": 'Date',},
 			{ "sTitle": 'Depositor'},
 			{ "sTitle": 'Description'},
 			{ "sTitle": 'Created By'}, 
 		],
 		"sScrollY": $("#main-content").height() - 235,
 		"aaSorting": [[1, 'desc']], 
 		"fnRowCallback": function( nRow, aData1, iDisplayIndex ) {
 			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
 				$(nRow).addClass('row_selected');
 			}
 		}
 	});	
 	//Json Grid
 	//init datatable
 	oTable = $('#BankDeposit').dataTable();

 	/* Click event handler */
 	$('#BankDeposit tbody tr').live('click', function () {  
 		  if ( $(this).hasClass('row_selected') ) {
 			  $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       bankDepositId=aData[0]; 
 	    }
 	    else {
 	        oTable.$('tr.row_selected').removeClass('row_selected');
 	        $(this).addClass('row_selected');
 	        aData =oTable.fnGetData( this );
 	       bankDepositId=aData[0];
 	       
 	    }
 	});
 	
 	$('#add').click(function(){
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/add_bank_deposit.action",
 			data: {bankDepositId : Number(0),recordId:Number(0)}, 
 			async:false,
 			dataType:"html",
 			cache:false,
 			success:function(result){
 				$("#main-wrapper").html(result);
 			}
 		});	
 	});

 	$('#delete').click(function(){   
		if(bankDepositId>0) {
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/bankdeposit_deletentry.action", 
		     	async: false,
		     	data:{bankDepositId: bankDepositId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);
					 var message=$.trim($('.tempresult').html()); 
					 if(message=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/bank_deposit_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record deleted.").slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#error_message').hide().html(message).slideDown(1000);
						 $('#error_message').delay(2000).slideUp();
						 return false;
					 }
				}
			 });
		} else{
			 alert("Please select a record to delete.");
			 return false;
		 }
	}); 


 	$('#view').click(function(){
 		 $('#page-error').hide();
			if(bankDepositId == 0){
				alert("Please select one record to view");
				return false;
			}
			else{	
			$.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/view_bank_deposit.action",  
		     	data: {bankDepositId:bankDepositId,recordId:Number(0)},  
		     	async: false,
				dataType: "html",
				cache: false,
				success: function(result){  
					$("#main-wrapper").html(result);
					return false;
		     	},
		     	error:function(result){
		     		$("#main-wrapper").html(result);
					return false;
	            } 
			}); 
  		}
 		return false; 
 	});
 	
 	$('#print').click(function(){  
		if(bankDepositId != null && bankDepositId != 0){ 
			window.open("<%=request.getContextPath()%>/get_bank_deposit_report_printout.action?selectedDepositId="+bankDepositId+"&format=HTML",
					'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
			return false; 
		}else{
			alert("Please select a record to print."); 
		} 
		return false;
	}); 

 	
 	$('#download').click(function(){
 		if(bankDepositId == 0){
			alert("Please select one record to view");
			return false;
		}
		else{		
			
			var bankDepositId = Number(s); 
			
			window.open('<%=request.getContextPath()%>/bank_deposit_download_view.action?recordId='
														+ bankDepositId
														+ '&approvalFlag=N&format=HTML',
												'',
												'width=800,height=800,scrollbars=yes,left=100px');
							}
							return false;
						});

	});
</script>

<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Bank
			Deposits
		</div>
		<div class="portlet-content">
			<div id="success_message"
				class="response-msg success ui-corner-all"
				style="display: none;"></div>
			<div id="error_message"
				class="response-msg error ui-corner-all"
				style="display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="gridDiv">
					<table class="display" id="BankDeposit"></table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Add</span>
				</div>
				<div class="sep_li"></div>
				<div class="first_li">
					<span>Edit</span>
				</div>
				<div class="first_li" style="display: none;">
					<span>View</span>
				</div>
				<div class="first_li">
					<span>Delete</span>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right" id="delete">
					<fmt:message key="accounts.common.button.delete" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="print">
					print
				</div>
				<div class="portlet-header ui-widget-header float-right" style="display: none;" id="view">
					<fmt:message key="accounts.common.button.view" />
				</div>
				<div class="portlet-header ui-widget-header float-right" id="add">
					<fmt:message key="accounts.common.button.add" />
				</div>
			</div>
		</div>
	</div>
</div>
