<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
var segmentId = Number(0);
var segmentDetailId = Number(0);
$(function(){ 
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	});  
	
	$jquery("#segment_tree").fancytree({ 
		extensions: ['contextMenu'],
 		icons: false, 
		autoScroll: true,
		debugLevel: 0,
		source: {  
	         url: "<%=request.getContextPath()%>/show_segment_treeview.action"
	    },
  		contextMenu: {
 	        menu: { 
 	          'add': { 'name': 'Add', 'icon': 'add' },	
 	          'edit': { 'name': 'Edit', 'icon': 'edit' }, 
 	          'delete': { 'name': 'Delete', 'icon': 'delete' },
 	          'sep1': '---------',
 	          'quit': { 'name': 'Quit', 'icon': 'quit' },
 	          'sep2': '---------',
 	        },
 	        actions: function(node, action, options) {
 	        	segmentId = node.key;
 	        	segmentDetailId = node.key;
 	        	$("#"+action).trigger('click');
 	        }
 		}
	});
	
	$('#add').click(function(){
		var segmentDetailId = Number(0); 
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/segment_addentry.action", 
			async: false,
			dataType: "html",
			data: {segmentId: segmentId, segmentDetailId: segmentDetailId},
			cache: false,
			success:function(result){   
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#temp-result').html(result); 
				$('.callJq').trigger('click');  
			}
		});
		return false;
	});
	
	$('#edit').click(function(){
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/segment_addentry.action", 
			async: false,
			dataType: "html",
			data: {segmentId: segmentId, segmentDetailId: segmentDetailId},
			cache: false,
			success:function(result){   
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#temp-result').html(result); 
				$('.callJq').trigger('click');  
			}
		});
		return false;
	});
	
	$('#delete').click(function(){
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/segment_delete.action", 
			async: false,
			dataType: "json",
			data: {segmentId: segmentId},
			cache: false,
			success:function(response){   
				if(response.returnMessage == "SUCCESS"){
					callSegmentTree("Record deleted.");
				}else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
				}
			}
		});
		return false;
	});
	
	$('.segment_save').live('click',function(){
		var segmentDetailId = Number($('#segmentDetailId').val()); 
		var segmentName = $.trim($('#segmentName').val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/segment_save.action", 
			async: false,
			dataType: "json",
			data: {segmentId: segmentId, segmentDetailId: segmentDetailId, segmentName: segmentName},
			cache: false,
			success:function(response){   
				 if(response.returnMessage == "SUCCESS"){
					 var message = "Record created.";
					 if(segmentDetailId > 0){
						 message = "Record updated.";
					 }
					 callSegmentTree(message);
				 }else{
					$('#error_message').hide().html(response.returnMessage).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
				}
			}
		});	
		return false;
	});
	
	$('.segment_discard').live('click',function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove(); 
		return false;
	});
}); 
function callSegmentTree(message){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_segments_view.action", 
		async: false,
		dataType: "html", 
		cache: false,
		success:function(result){   
			$('#DOMWindow').remove();
			$('#DOMWindowOverlay').remove(); 
			$("#main-wrapper").html(result); 
 			$('#success_message').hide().html(message).slideDown(1000);
			$('#success_message').delay(2000).slideUp();
		}
	});	 
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Segment</div>	
		<div class="portlet-content width48 float-right" id="hrm">
			<div class="portlet-header float-right">Note: For Creation of
				segment, right click on the segment name.</div>
			<div class="clearfix"></div>  
		</div> 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" 
				style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>  
 			<div id="segment_tree"></div> 
 			<div id="add" style="display: none;"></div>
 			<div id="edit" style="display: none;"></div>
 			<div id="delete" style="display: none;"></div>
 		</div>
 	</div>
 	<div class="clearfix"></div>  
 	<div id="temp-result" style="display: none;"></div>
	<span class="callJq"></span>
  </div>