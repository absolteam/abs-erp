<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 50%;
}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var oTable;

var fromDate = null;
var toDate = null;

var selectedProductId = 0;
var selectedPersonId = 0;
var selectedFromStoreId = 0;
var selectedToStoreId = 0;
var selectedTypeId = 0;
var selectedReasonId = 0;


populateDatatable();



$('#persons').combobox({
	selected : function(event, ui) {
		selectedPersonId = $('#persons :selected').val();
		populateDatatable();
	}
});

$('#products').combobox({
	selected : function(event, ui) {
		selectedProductId = $('#products :selected').val();
		populateDatatable();
	}
});

$('#fromStores').combobox({
	selected : function(event, ui) {
		selectedFromStoresId = $('#fromStores :selected').val();
		populateDatatable();
	}
});

$('#toStores').combobox({
	selected : function(event, ui) {
		selectedToStoresId = $('#toStores :selected').val();
		populateDatatable();
	}
});

$('#types').combobox({
	selected : function(event, ui) {
		selectedTypeId = $('#sources :selected').val();
		populateDatatable();
	}
});

$('#reasons').combobox({
	selected : function(event, ui) {
		selectedReasonId = $('#sources :selected').val();
		populateDatatable();
	}
});

function populateDatatable(){
	$('#MaterialTransferDTR').dataTable({ 
		"sAjaxSource": "get_material_transfer_report_list_json.action?productId="+selectedProductId
		+"&personId="+selectedPersonId+"&fromStoreId="+selectedFromStoreId+"&toStoreId="+selectedToStoreId
		+"&typeId="+selectedTypeId+"&reasonId="+selectedReasonId+"&fromDate="+fromDate+"&toDate="+toDate,
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "Material Transfer Id", "bVisible": false},
			{ "sTitle": "Reference"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Transfer Person"},
	     ], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
		}
	});	
	//Json Grid
	//init datatable
	oTable = $('#MaterialTransferDTR').dataTable();
	selectedRowId = 0;
}

$('#MaterialTransferDTR tbody tr').live('click', function () {  
	  if ( $(this).hasClass('row_selected') ) {
		  $(this).addClass('row_selected');
      aData =oTable.fnGetData( this );
      selectedRowId=aData[0];   
  }
  else {
      oTable.$('tr.row_selected').removeClass('row_selected');
      $(this).addClass('row_selected');
      aData =oTable.fnGetData( this ); 
      selectedRowId=aData[0];  
  }
});


var selectedType = null;

$('#startPicker,#endPicker').datepick({
	onSelect : customRange,
	showTrigger : '#calImg'
});

function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


					
	$(".xls-download-call").click(function(){
		if(selectedRowId > 0 ) {
			window.open("<%=request.getContextPath()%>/get_material_transfer_report_XLS.action?materialTransferId="+selectedRowId,
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
 		}
		return false; 
	});
 	
 	$(".print-call").click(function(){
 		if(selectedRowId > 0 ) {
 			window.open("<%=request.getContextPath()%>/get_material_transfer_report_printout.action?materialTransferId="+selectedRowId+"&format=HTML",
				'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
 		} else{
 			window.open("<%=request.getContextPath()%>/material_transfer_all_printout.action?productId="+selectedProductId
 					+"&personId="+selectedPersonId+"&fromStoreId="+selectedFromStoreId+"&toStoreId="+selectedToStoreId
 					+"&typeId="+selectedTypeId+"&reasonId="+selectedReasonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML",
 					'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
 		}
		return false;
		
	});
 	
 	$(".pdf-download-call").click(function(){ 
 		if(selectedRowId > 0 ) {
 			window.open("<%=request.getContextPath()%>/get_material_transfer_report_pdf.action?materialTransferId="+selectedRowId+"&format=PDF",
				'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
 		} else{
			$('#error_message').hide().html("Please select a record.").slideDown();
			$('#error_message').delay(3000).slideUp();
 		}
		return false;
		
	});

	$('.fromDate').change(function() {

		fromDate = $('.fromDate').val();
		if (toDate != null) {
			populateDatatable();
		}

	});

	$('.toDate').change(function() {

		toDate = $('.toDate').val();
		if (fromDate != null) {
			populateDatatable();
		}

	});
	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Material Transfer Report
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr>
					<td>Transfer Person :</td>
					<td>
						<select id="persons">
							<option value="">Select</option>
							<c:forEach var="pr" items="${PERSON_LIST}">
								<option value="${pr.personId}">${pr.firstName} ${pr.lastName}</option>
							</c:forEach>
						</select>
					</td>
					<td>Product :</td>
					<td>
						<select id="products">
							<option value="">Select</option>
							<c:forEach var="pr" items="${PRODUCT_LIST}">
								<option value="${pr.productId}">${pr.productName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>From Store :</td>
					<td>
						<select id="fromStores">
							<option value="">Select</option>
							<c:forEach var="st" items="${STORE_LIST}">
								<option value="${st.storeId}">${st.storeName}</option>						
							</c:forEach>
						</select>
					</td>
					<td>To Store :</td>
					<td>
						<select id="toStores">
							<option value="">Select</option>
							<c:forEach var="store" items="${STORE_LIST}">
								<option value="${store.storeId}">${store.storeName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>Type :</td>
					<td>
						<select id="types">
							<option value="">Select</option>
							<c:forEach var="type" items="${TRANSFER_TYPE}">
								<option value="${type.lookupDetailId}">${type.displayName}</option>						
							</c:forEach>
						</select>
					</td>
					<td>Reason :</td>
					<td>
						<select id="reasons">
							<option value="">Select</option>
							<c:forEach var="reason" items="${TRANSFER_REASON}">
								<option value="${reason.lookupDetailId}">${reason.displayName}</option>						
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td width="15%" align="center">
						From 
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width60 fromDate" id="startPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
					<td width="15%" align="center">
						To 
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="toDate" id="endPicker"
						readonly="readonly" onblur="triggerDateFilter()">
					</td>
				</tr>
			</table>

			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="documents">
					<table class="display" id="MaterialTransferDTR">

					</table>
				</div>
			</div>


		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div> 