 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var customerQuotationId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	 
	$('#common-popup').dialog('destroy');		
	$('#common-popup').remove();   
    $('#baseprice-popup-dialog').dialog('destroy');		
    $('#baseprice-popup-dialog').remove(); 
	$('#sales-quotation-product-inline-popup').dialog('destroy');		
	$('#sales-quotation-product-inline-popup').remove(); 
	
	oTable = $('#CustomerQuotationDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "getcustomer_quotationlist.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNo"
		 }, {
			"mDataProp" : "orderDateFormat"
		 }, {
			"mDataProp" : "expiryDateFormat"
		 }, {
			"mDataProp" : "shippingDateFormat"
		 }, {
			"mDataProp" : "customerName"
		 },{
			"mDataProp" : "salesRep"
		 }], 
	});	 	
	oTable.fnSort( [ [0,'desc'] ] );	 
	$('#add').click(function(){   
		customerQuotationId = Number(0);
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/customer_quotation_entry.action", 
	     	async: false, 
	     	data:{customerQuotationId: customerQuotationId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(customerQuotationId != null && customerQuotationId != 0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customer_quotation_entry.action", 
		     	async: false, 
		     	data:{customerQuotationId: customerQuotationId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){  
		$('.response-msg').hide();
		if(customerQuotationId != null && customerQuotationId != 0){
			var cnfrm = confirm("Selected record will be permanently deleted.");
			if(!cnfrm)
				return false;
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/customer_quotation_deletentry.action", 
	     	async: false,
	     	data:{customerQuotationId: customerQuotationId},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$(".tempresult").html(result);  
				var message=$.trim($('.tempresult').html().toLowerCase()); 
				if(message!=null && message=="success"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_customer_quotation.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){ 
							$("#main-wrapper").html(result);   
							$('#success_message').hide().html("Record deleted..").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						} 		
					}); 
				}
				else{
					$('#error_message').hide().html(message).slideDown(1000);
					$('#error_message').delay(2000).slideUp();
					return false;
				} 
			} 		
		}); 
		}else{
			alert("Please select a record to delete.");
			return false;
		}
	});   
	 
	/* Click event handler */
	$('#CustomerQuotationDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this );
	          customerQuotationId = aData.customerQuotationId;  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData = oTable.fnGetData( this ); 
	          customerQuotationId = aData.customerQuotationId;  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Quotation</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div>  
		 	<div id="rightclickarea">
			 	<div id="customer_quotation_list">
					<table id="CustomerQuotationDT" class="display">
						<thead>
							<tr>
								<th>Reference No</th>
								<th>Quotation Date</th>
								<th>Expiry Date</th>
								<th>Shipping Date</th>
								<th><fmt:message key="accounts.customer.customername"/></th>
								<th><fmt:message key="accounts.customer.salerepresentative"/></th> 
							</tr>
						</thead> 
					</table> 
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div> 
				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>