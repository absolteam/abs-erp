<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.ui-autocomplete-input {
	width: 50% !important;
}

.ui-autocomplete {
	height: 250px;
	overflow-y: auto;
	overflow-x: hidden;
}

.ui-combobox-button {
	height: 32px;
}
</style>
<script type="text/javascript"> 
var transactionId=0;  
var oTable; var selectRow=""; var aSelected = []; var aData="";
var categoryId = 0;
var fromDate = "";
var toDate = "";
var combinationId = 0;
$(function(){
	$('.formError').remove();
	dummyTable();
	
	$('.print-call').click(function(){
		fromDate = $('#fromDate').val();
		toDate =  $('#toDate').val();  
		window.open("show_transaction_reporthtm.action?categoryId="+ categoryId+"&combinationId="+combinationId+"&fromDate="+fromDate+"&toDate="+toDate + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		return false;
	});  

	$('.pdf-download-call').click(function(){
		if(transactionId!=null && transactionId!="" && transactionId > 0){
			window.open('show_journal_report.action?journalId='+ transactionId + '&format=HTML', '',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a record to view.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});

	$(".xls-download-call").click(function(){ 
		window.open("<%=request.getContextPath()%>/show_journal_excel_report.action?categoryId="+ categoryId+"&combinationId="+combinationId+"&fromDate="+fromDate+"&toDate="+toDate
		, '_blank',
		'width=0,height=0');
	});
		
		$('#categoryName').combobox({
			selected : function(event, ui) {
				categoryId = Number($(this).val());  
			}
		});
	
		$('.autoComplete').comboboxpos({ 
			selected : function(event, ui) { 
				combinationId = Number($(this).val());  
				$(".autoComplete option:selected").text().replace(/[\s\n\r]+/g, ' ').trim();
				var combtext = $(".autoComplete option:selected").text().replace(/[\s\n\r]+/g, ' ').trim(); 
				$('.ui-autocomplete-input:first').val(combtext); 
				$('.ui-autocomplete-input:first').trigger('change');
			}
		}); 
		 
		$('.ui-autocomplete-input:first').change(function(){ 
			$(this).val($(this).val().replace(/[\s\n\r]+/g, ' ').trim()); 
		}); 
		
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
			$('#themeRollerSelect option:not(:selected)').remove();
		}
		$('#fromDate,#toDate').datepick({
			onSelect : customRange,
			showTrigger : '#calImg'
		});
		
		$('#list_grid').click(function(){
			oTable = $('#TransactionRDT').dataTable();
			oTable.fnDestroy();
			filterJsonTransaction();
		});
	});
	
function filterJsonTransaction() {
	fromDate = $('#fromDate').val();
	toDate =  $('#toDate').val();  
	$('#TransactionRDT')
			.dataTable(
					{ 
						"sAjaxSource" : "transaction_jsonlist.action?categoryId="
							+ categoryId+"&combinationId="+combinationId+"&fromDate="+fromDate+"&toDate="+toDate,
						"sPaginationType" : "full_numbers",
						"bJQueryUI" : true,
						"iDisplayLength" : 25,
						"aoColumns" : [
								{
									"sTitle" : "JOURNAL_ID",
									"bVisible" : false
								},
								{
									"sTitle" : '<fmt:message key="accounts.jv.label.vouchernumber"/>'
								},
								{
									"sTitle" : '<fmt:message key="accounts.jv.label.jvdate"/>'
								},
								{
									"sTitle" : 'Debit'
								},
								{
									"sTitle" : 'Credit'
								},
								{
									"sTitle" : '<fmt:message key="accounts.jv.label.categoryname"/>'
								},
								{
									"sTitle" : '<fmt:message key="accounts.jv.label.entrydate"/>'
								},
								{
									"sTitle" : '<fmt:message key="accounts.jv.label.description"/>'
								}, ],
						"sScrollY" : $("#main-content").height() - 270,
						//"bPaginate": false,
						"aaSorting" : [ [ 1, 'desc' ] ],
						"fnRowCallback" : function(nRow, aData,
								iDisplayIndex) {
							if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
								$(nRow).addClass('row_selected');
							}
						}
					});
		//init datatable
		oTable = $('#TransactionRDT').dataTable();
	}

	function dummyTable() {
		$('#TransactionRDT')
				.dataTable(
						{ 
							"sPaginationType" : "full_numbers",
							"bJQueryUI" : true,
							"iDisplayLength" : 25,
							"aoColumns" : [
									{
										"sTitle" : "JOURNAL_ID",
										"bVisible" : false
									},
									{
										"sTitle" : '<fmt:message key="accounts.jv.label.vouchernumber"/>'
									},
									{
										"sTitle" : '<fmt:message key="accounts.jv.label.jvdate"/>'
									},
									{
										"sTitle" : 'Debit'
									},
									{
										"sTitle" : 'Credit'
									},
									{
										"sTitle" : '<fmt:message key="accounts.jv.label.categoryname"/>'
									},
									{
										"sTitle" : '<fmt:message key="accounts.jv.label.entrydate"/>'
									},
									{
										"sTitle" : '<fmt:message key="accounts.jv.label.description"/>'
									}, ],
							"sScrollY" : $("#main-content").height() - 270,
							//"bPaginate": false,
							"aaSorting" : [ [ 1, 'desc' ] ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex) {
								if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
									$(nRow).addClass('row_selected');
								}
							}
						});
		//init datatable
		oTable = $('#TransactionRDT').dataTable();
	}
	function checkLinkedDays() {
		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}
	function customRange(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);
		} else {
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.journal.label.journalvoucher" />
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="width100 float-left" id="hrm">
				<div class="width48 float-right">
					<fieldset style="min-height: 60px;">
						<div class="width100">
							<div class="width48 float-right">
								<label class="width30" for="toDate">To Date</label> <input
									type="text" id="toDate" class="width50" readonly="readonly" />
							</div>
							<div class="width50 float-left">
								<label class="width30" for="fromDate">From Date</label> <input
									type="text" id="fromDate" class="width50" readonly="readonly" />
							</div>
						</div>
						<div
							class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
							style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
							<div class="portlet-header ui-widget-header float-right"
								id="list_grid" style="cursor: pointer; color: #fff;">list
								gird</div>
						</div>
					</fieldset>
				</div>
				<div class="float-left width50">
					<fieldset style="min-height: 60px;">
						<div>
							<label class="width30">Account Code</label> <select
								name="comb_tree" id="combinationSelectTree"
								class="autoComplete width60">
								<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}"
									varStatus="status1">
									<option value=""></option>
									<option value="${bean.combinationId}">
										${bean.accountByCompanyAccountId.account}
										[${bean.accountByCompanyAccountId.code}]
										<c:choose>
											<c:when
												test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
												<c:forEach var="costbean" items="${bean.costVos}"
													varStatus="cstatus1">
													<option value="${costbean.combinationId}">
														${costbean.accountByCostcenterAccountId.account}
														[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
														<c:choose>
															<c:when
																test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
																<c:forEach var="naturalbean"
																	items="${costbean.naturalVos}" varStatus="nstatus1">
																	<option value="${naturalbean.combinationId}">
																		${naturalbean.accountByNaturalAccountId.account}
																		[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]
																		<c:choose>
																			<c:when
																				test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
																				<c:forEach var="analysisbean"
																					items="${naturalbean.analysisVos}"
																					varStatus="astatus1">
																					<option value="${analysisbean.combinationId}">
																						${analysisbean.accountByAnalysisAccountId.account}
																						[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]
																						<c:choose>
																							<c:when
																								test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
																								<c:forEach var="buffer1bean"
																									items="${analysisbean.buffer1Vos}"
																									varStatus="b1status1">
																									<option value="${buffer1bean.combinationId}">
																										${buffer1bean.accountByBuffer1AccountId.account}
																										[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]
																										<c:choose>
																											<c:when
																												test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																												<c:forEach var="buffer2bean"
																													items="${buffer1bean.buffer2Vos}"
																													varStatus="b2status">
																													<option
																														value="${buffer2bean.combinationId}">${buffer2bean.accountByBuffer2AccountId.account}
																														[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]
																													</option>
																												</c:forEach>
																											</c:when>
																										</c:choose>
																									</option>
																								</c:forEach>
																							</c:when>
																						</c:choose>
																					</option>
																				</c:forEach>
																			</c:when>
																		</c:choose>
																	</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class="width30">Category</label> <select
								name="categoryName" id="categoryName"
								class="categoryName width50">
								<option value="">Select</option>
								<c:choose>
									<c:when test="${CATEGORY_LIST ne null && CATEGORY_LIST ne ''}">
										<c:forEach items="${CATEGORY_LIST}" var="CATEGORY">
											<option value="${CATEGORY.categoryId}">${CATEGORY.name}</option>
										</c:forEach>
									</c:when>
								</c:choose>
							</select>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="tempresult" style="display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
				<div id="transactions">
					<table class="display" id="TransactionRDT"></table>
				</div>
			</div>
			<div class="vmenu">
				<div class="first_li">
					<span>Print</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="tempresult" style="display: none;"></div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer; display: none;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>