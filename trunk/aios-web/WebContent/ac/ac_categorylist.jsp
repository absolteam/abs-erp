 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var categoryId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	$('#example').dataTable({ 
		"sAjaxSource": "getcategory_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Category Id", "bVisible": false},
			{ "sTitle": "Category Name"},
			{ "sTitle": "From Date"}, 
			{ "sTitle": "To Date"}, 
			{ "sTitle": "Status"}, 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	$('#add').click(function(){   
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/category_addentry.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#main-wrapper").html(result);   
			} 		
		}); 
	});  

	$('#edit').click(function(){   
		if(categoryId!=null && categoryId!=0){
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/category_updateentry.action", 
				data:{categoryId:categoryId},
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){ 
					$("#main-wrapper").html(result);  
					$.scrollTo(0,300);
				} 		
			}); 
		}else{
			alert("Please select a record to edit.");
			return false;
		}
	});  

	$('#delete').click(function(){ 
		if(categoryId!=null && categoryId!=0){ 
			var cnfrm = confirm('Selected category will be deleted permanently');
			if(!cnfrm)
				return false;
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/category_deleteentry.action", 
		     	async: false,
		     	data:{categoryId:categoryId},
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);   
					var message=$('.tempresult').html().toLowerCase().trim(); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/getall_categorylist.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$("#main-wrapper").html(result);   
								$('#success_message').hide().html("Record Deleted.").slideDown(1000); 
								$.scrollTo(0,300);
							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
						return false;
					} 
				} 		
			});
		}else{
			alert("Please select a record to delete.");
			return false;
		} 
	}); 

	//init datatable
	oTable = $('#example').dataTable();
	 
	/* Click event handler */
	$('#example tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          categoryId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          categoryId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>category</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div>
			<c:if test="${requestScope.success!=null && requestScope.success!=''}">
				<div class="success response-msg ui-corner-all"><fmt:message key="${requestScope.success}"/></div>
				</c:if>
				<c:if test="${requestScope.error!=null && requestScope.error!=''}">
				<div class="error response-msg ui-corner-all"><fmt:message key="${requestScope.error}"/></div>
			</c:if> 
	   	    <div class="tempresult" style="display:none;"></div>
	   	    
		 	<div id="rightclickarea">
			 	<div id="purchase_list">
						<table class="display" id="example"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Add</span></div>
				<div class="sep_li"></div>		 
				<div class="first_li"><span>Edit</span></div>
 				<div class="first_li"><span>Delete</span></div>
			</div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="delete" ><fmt:message key="accounts.common.button.delete"/></div> 
				<div class="portlet-header ui-widget-header float-right" id="edit" ><fmt:message key="accounts.common.button.edit"/></div>
				<div class="portlet-header ui-widget-header float-right" id="add" ><fmt:message key="accounts.common.button.add"/></div>	 
			</div>
		</div>
	</div>
</div>