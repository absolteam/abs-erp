<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
select.width51 {
	width: 51%;
}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var popval=""; 
$(function(){   
	$jquery("#check_outs").validationEngine('attach');   
	
	 $('#checkout_discard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_asset_check_out.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('#common-popup').dialog('destroy');		
					 $('#common-popup').remove(); 
					$("#main-wrapper").html(result);  
				}
		 });
	 });

	 $('#checkout_save').click(function(){   
		 if($jquery("#check_outs").validationEngine('validate')){
 			var assetCheckOutId = Number($('#assetCheckOutId').val());   
			var checkOutNumber = $('#checkOutNumber').val();
			var assetCreationId = Number($('#assetCreationId').val());
			var checkOutDue = Number($('#checkOutDue').val());
 	 		var checkOutDate = $('#checkOutDate').val(); 
	 		var checkOutTo = Number($('#checkOutTo').val()); 
	 		var checkOutby = Number($('#checkOutby').val());  
	 		var cmpDeptLocationId = Number($('#cmpDeptLocationId').val());  
	 		var description = $('#description').val();
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_asset_check_out.action", 
				 	async: false,
				 	data:{
					 		assetCreationId: assetCreationId, assetCheckOutId: assetCheckOutId, checkOutNumber: checkOutNumber, description: description,
					 		checkOutDue: checkOutDue, assetCheckOutDate: checkOutDate, checkOutTo: checkOutTo, checkOutBy: checkOutby, cmpDeptLocationId: cmpDeptLocationId
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){  
						 if(response.returnMessage == "SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/show_asset_check_out.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove(); 
										$("#main-wrapper").html(result); 
										if(assetCreationId > 0)
											$('#success_message').hide().html("Record updated").slideDown(1000);
										else
											$('#success_message').hide().html("Record created").slideDown(1000);
										$('#success_message').delay(3000).slideUp();
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown(1000);
					}
			 }); 
		 }else{return false;}
	 });

	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	 $('#department-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	 $('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		popval = $(this).attr('id');  
		var actionValue = "";
		var personTypes="ALL";
		if(popval == "asset")
			actionValue = "show_asset_checkout_popup";
		else if(popval == "cmpDeptLocation")
			actionValue = "common_departmentbranch_list";
		else
			actionValue = "common_person_list";
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionValue+".action", 
		 	async: false,  
		 	data:{personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 

	if(Number($('#assetCreationId').val())>0){
		$('#checkOutDue').val($('#tempCheckOutDue').val()); 
	} 

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });
	
	 if(Number('${requestScope.assetCheckOutId}')>0){
		$('#checkOutDate').datepick();
	 }else{
		$('#checkOutDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	 } 
});

function personPopupResult(personid, personname, commonParam){
	if(popval =="checkToPersonTo"){
		$('#checkOutTo').val(personid);
		$('#checkToPersonName').val(personname); 
	}else{
		$('#checkOutby').val(personid);
		$('#checkByPersonName').val(personname); 
	}
	$('#common-popup').dialog("close");  
}
function departmentBranchPopupResult(locationId, locationName, commonParam){
	$('#cmpDeptLocationId').val(locationId);
	$('#cmpDeptLocationName').val(locationName);
}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationId').val(assetId);
	$('#assetCreationName').val(assetName); 
}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>check out
		</div> 
		<form name="check_outs" id="check_outs" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetCheckOutId" name="assetCheckOutId"
					value="${CHECK_OUT.assetCheckOutId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 120px;">  
							<div>
								<label class="width30" for="checkOutTo">CheckOut By<span
									class="mandatory">*</span></label> 
									<c:choose>
										<c:when test="${CHECK_OUT.personByCheckOutBy ne null && CHECK_OUT.personByCheckOutBy ne ''}">
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${CHECK_OUT.personByCheckOutBy.firstName} ${CHECK_OUT.personByCheckOutBy.lastName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${CHECK_OUT.personByCheckOutBy.personId}"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="checkByPersonName" class="width50 validate[required]"
												value="${requestScope.personName}"/>
											<input type="hidden" readonly="readonly" id="checkOutby" value="${requestScope.checkOutBy}"/>
										</c:otherwise>
									</c:choose>  
 									<span class="button">
										<a style="cursor: pointer;" id="checkByPersonby" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
							</div>
							<div>
								<label class="width30" for="checkOutTo">CheckOut To<span
									class="mandatory">*</span></label> 
									<input type="text" readonly="readonly" id="checkToPersonName" class="width50 validate[required]"
										value="${CHECK_OUT.personByCheckOutTo.firstName} ${CHECK_OUT.personByCheckOutTo.lastName}"/>
									<input type="hidden" readonly="readonly" id="checkOutTo" value="${CHECK_OUT.personByCheckOutTo.personId}"/>
 									<span class="button">
										<a style="cursor: pointer;" id="checkToPersonTo" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
							</div> 
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${CHECK_OUT.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30" for="checkOutNumber">CheckOut Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${CHECK_OUT.assetCheckOutId ne null && CHECK_OUT.assetCheckOutId ne ''}">
										<input type="text" readonly="readonly" name="checkOutNumber"
											id="checkOutNumber" class="width50 validate[required]"
											value="${CHECK_OUT.checkOutNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="checkOutNumber"
											id="checkOutNumber" class="width50 validate[required]"
											value="${requestScope.checkOutNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset Name<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="assetCreationName"
										value="${CHECK_OUT.assetCreation.assetName}" class="width50 validate[required]"/>
									<input type="hidden" id="assetCreationId" value="${CHECK_OUT.assetCreation.assetCreationId}"/>
									<span class="button">
										<a style="cursor: pointer;" id="asset" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
							</div>
							<div style="display: none;">
								<label class="width30" for="checkOutDue">CheckOut Due<span
									class="mandatory">*</span> </label> <select name="checkOutDue"
									id="checkOutDue" class="width51">
									<option value="">Select</option>
									<c:forEach var="checkOutDue" items="${CHECK_OUT_DUE}">
										<option value="${checkOutDue.key}">${checkOutDue.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempCheckOutDue"
									value="${CHECK_OUT.checkOutDue}" />
							</div>
							<div>
								<label class="width30" for="checkOutDate">CheckOut Date<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" id="checkOutDate" 
									 value="${CHECK_OUT.outDate}" class="width50 validate[required]"/>
							</div>  
							<div>
								<label class="width30" for="location">Location<span
									class="mandatory">*</span></label> 
									<c:choose>
										<c:when test="${CHECK_OUT.assetCheckOutId ne null && CHECK_OUT.assetCheckOutId ne '' && CHECK_OUT.assetCheckOutId gt 0}">
											<input type="text" readonly="readonly" id="cmpDeptLocationName" class="width50 validate[required]"
												value="${CHECK_OUT.cmpDeptLocation.company.companyName} &gt;&gt; ${CHECK_OUT.cmpDeptLocation.department.departmentName} &gt;&gt; ${CHECK_OUT.cmpDeptLocation.location.locationName}" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="cmpDeptLocationName" class="width50 validate[required]"/>
										</c:otherwise>
									</c:choose> 
									<input type="hidden" readonly="readonly" id="cmpDeptLocationId" value="${CHECK_OUT.cmpDeptLocation.cmpDeptLocId}"/>
 									<span class="button">
										<a style="cursor: pointer;" id="cmpDeptLocation" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="checkout_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="checkout_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div> 
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>