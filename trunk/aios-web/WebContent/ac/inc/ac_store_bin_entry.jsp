<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<div id="hrm" class="portlet-content width100 bin-entry">
<input type="hidden" id="sectionReferenceId"
	value="${requestScope.sectionReferenceId}" />
	<fieldset style="height: auto !important;">
		<legend>Rack Information</legend>
		<div class="portlet-content">
			<div id="bin-page-error"
				class="response-msg error ui-corner-all width90"
				style="display: none;"></div>
			<div id="hrm" class="hastable width100">
			<c:set var="shelfAsileId" value="0"/>
				<table id="bin-hastab" class="width100">
					<thead>
						<tr>
							<th style="width: 3%">Name</th>
							<th style="width: 3%">Rack Type</th>
							<th style="width: 3%">Dimension (l*b*h)</th>
							<th style="width: 3%">Shelf</th>
							<th style="width: 0.3%">Options</th>
						</tr>
					</thead>
					<tbody class="bintab">
						<c:forEach var="binDetail" items="${BIN_DETAILS}"
							varStatus="status">
							<tr class="binrowid tempbinrowid"
								id="binfieldrow_${status.index+1}">
								<td style="display: none;" id="binlineId_${status.index+1}">${binDetail.referenceNumber}</td>
								<td><input type="text" class="width90 binSectionName" value="${binDetail.name}"
									name="binSectionName" id="binSectionName_${status.index+1}" />
								</td>
								<td><select name="binSection"
									id="binSection_${status.index+1}" class="binSection"
									class="width90">
										<option value="">Select</option>
										<c:forEach var="binType" items="${BIN_TYPES}">
											<option value="${binType.key}">${binType.value}</option>
										</c:forEach>
								</select> <input type="hidden" id="tempbinSection_${status.index+1}"
									value="${binDetail.shelfType}" />
								</td>
								<td><input type="text" class="width90 bindimension"
									style="border: 0px;" name="bindimension"
									value="${binDetail.dimension}"
									id="bindimension_${status.index+1}" /></td>
								<td>
									<c:choose>
										<c:when test="${binDetail.aisle.aisleId ne null && binDetail.aisle.aisleId ne ''}">
											<div
												class="portlet-header ui-widget-header float-right addRacks"
												id="addRack_${status.index+1}" style="cursor: pointer;"
												onclick="addRacks(${status.index+1});">Edit Shelf</div>
										</c:when>
										<c:otherwise>
											<div
												class="portlet-header ui-widget-header float-right addRacks"
												id="addRack_${status.index+1}" style="cursor: pointer;"
												onclick="addRacks(${status.index+1});">Add Shelf</div>
										</c:otherwise>
									</c:choose> 
								</td>
								<td style="width: 0.01%;" class="opn_td"
									id="option_${status.index+1}"><a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip binaddData"
									id="BinAddImage_${status.index+1}"
									style="display: none; cursor: pointer;" title="Add Record">
										<span class="ui-icon ui-icon-plus"></span> </a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bineditData"
									id="BinEditImage_${status.index+1}"
									style="display: none; cursor: pointer;" title="Edit Record">
										<span class="ui-icon ui-icon-wrench"></span> </a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bindelrow"
									id="BinDeleteImage_${status.index+1}" style="cursor: pointer;"
									title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
									id="WorkingImage_${status.index+1}" style="display: none;"
									title="Working"> <span class="processing"></span> </a> <input
									type="hidden" name="shelfid" value="${binDetail.shelfId}"
									id="shelfid_${status.index+1}" /> <input type="hidden"
									name="confirmSave" value="${binDetail.confirmSave}"
									id="confirmSave_${status.index+1}" />
									<c:set var="shelfAsileId" value="${binDetail.aisle.aisleId}"/>
									<input type="hidden" name="shelfaisleId" value="${binDetail.aisle.aisleId}" id="shelfaisleId_${status.index+1}" />
								</td>
							</tr>
						</c:forEach>
						<c:forEach var="i" begin="${fn:length(BIN_DETAILS)+1}"
							end="${fn:length(BIN_DETAILS)+2}" step="1">
							<tr class="binrowid" id="binfieldrow_${i}">
								<td style="display: none;" id="binlineId_${i}">${i}</td>
								<td><input type="text" class="width90 binSectionName"
									name="binSectionName" id="binSectionName_${i}" />
								</td>
								<td><select name="binSection" id="binSection_${i}"
									class="binSection" class="width90">
										<option value="">Select</option>
										<c:forEach var="binType" items="${BIN_TYPES}">
											<option value="${binType.key}">${binType.value}</option>
										</c:forEach>
								</select>
								</td>
								<td><input type="text" class="width90 bindimension"
									style="border: 0px;" name="bindimension" id="bindimension_${i}" />
								</td>
								<td>
									<div
										class="portlet-header ui-widget-header float-right addRacks"
										id="addRack_${i}" style="cursor: pointer;"
										onclick="addRacks(${i});">Add Shelf</div></td>
								<td style="width: 0.01%;" class="opn_td" id="option_${i}"><a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip binaddData"
									id="BinAddImage_${i}" style="display: none; cursor: pointer;"
									title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
								</a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bineditData"
									id="BinEditImage_${i}" style="display: none; cursor: pointer;"
									title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
								</a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bindelrow"
									id="BinDeleteImage_${i}"
									style="display: none; cursor: pointer;" title="Delete Record">
										<span class="ui-icon ui-icon-circle-close"></span> </a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
									id="WorkingImage_${i}" style="display: none;" title="Working">
										<span class="processing"></span> </a> <input type="hidden"
									name="shelfid" value="0" id="shelfid_${i}" /> <input
									type="hidden" name="confirmSave" value="false"
									id="confirmSave_${i}" />
									<input type="hidden" name="shelfaisleId" value="${shelfAsileId}" id="shelfaisleId_${i}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset>
	<div class="clearfix"></div>
	<div style="display: none;"
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
		<div class="portlet-header ui-widget-header float-left binaddrows"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.addrow" />
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="margin: 10px;">
		<div class="portlet-header ui-widget-header float-right closeDom"
			style="cursor: pointer;">Cancel</div>
		<div class="portlet-header ui-widget-header float-right bin_save"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
</div>