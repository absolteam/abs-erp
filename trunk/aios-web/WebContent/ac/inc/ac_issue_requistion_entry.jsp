<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.quantityerr {
	position: absolute;
	display: block;
	font-size: x-small !important;
	color: #ff255c;
}
</style>
<script type="text/javascript">
var slidetab="";
var requistionDetails="";
var tempid = ""; var accessCode = "";
$(function(){   
	
	$jquery("#requistionValidation").validationEngine('attach'); 
	$jquery(".amount,.totalAmountH").number(true,2); 
	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_issue_requistion.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove(); 
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/issue_requistion_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('#requistion_save').click(function(){
		 requistionDetails = "";
		 if($jquery("#requistionValidation").validationEngine('validate')){
 		 		
	 			var issueRequistionId = Number($('#issueRequistionId').val()); 
	 			var referenceNumber = $('#referenceNumber').val();
	 			var issueDate = $('#issueDate').val();  
	 			var locationId = Number($('#locationId').val());  
	 			var requisitionId = Number($('#requisitionId').val());
 	 			var shippingSource = Number($('#shippingSource').val());  
	 			var personId = Number($('#personId').val());  
 	 			var description=$('#description').val();
 	 			var customerId = Number($('#customerId').val());  
 	 			var otherReference = $('#otherReference').val();
	 			requistionDetails = getRequistionDetails();   
	   			if(requistionDetails!=null && requistionDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_issue_requisition.action", 
	 				 	async: false, 
	 				 	data:{	issueRequistionId: issueRequistionId, referenceNumber: referenceNumber, issueDate: issueDate, locationId: locationId,
	 				 			personId: personId, shippingSourceId : shippingSource, description: description, requistionDetail: requistionDetails,
	 				 			requisitionId: requisitionId, customerId: customerId, otherReference: otherReference
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(response.returnMessage == "SUCCESS"){
	 							 $.ajax({
	 									type:"POST",
	 									url:"<%=request.getContextPath()%>/show_issue_requistion.action", 
	 								 	async: false,
	 								    dataType: "html",
	 								    cache: false,
	 									success:function(result){
	 										$('#common-popup').dialog('destroy');		
	 										$('#common-popup').remove();  
	 					  					$('#codecombination-popup').dialog('destroy');		
	 					  					$('#codecombination-popup').remove(); 
	 										$("#main-wrapper").html(result); 
	 										if(issueRequistionId==0)
	 											$('#success_message').hide().html("Record created.").slideDown(1000);
	 										else
	 											$('#success_message').hide().html("Record updated.").slideDown(1000);
	 										$('#success_message').delay(2000).slideUp();
	 									}
	 							 });
	 						 } 
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter requisition details.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 
		
		$('.packageType').live('change',function(){
			$('.quantityerr').remove(); 
			var rowId=getRowId($(this).attr('id')); 
			$('#productQty_'+rowId).val('');
			var packageUnit = Number($('#packageUnit_'+rowId).val());
			var packageType = Number($('#packageType_'+rowId).val());  
			var unitrate = Number($jquery('#amount_'+rowId).val());
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			} else if(packageUnit > 0 && packageType == -1){
				$('#productQty_'+rowId).val(packageUnit);
				if(unitrate > 0 ){
					total = Number(unitrate * packageUnit);
					$jquery('#totalAmountH_'+rowId).val(total);
					$('#totalAmount_'+rowId).text($('#totalAmountH_'+rowId).val());
					triggerAddRow(rowId);
				} 
 			}
			return false;
		});
		
		$('.packageUnit').live('change',function(){
			var rowId=getRowId($(this).attr('id'));
			$('#baseUnitConversion_'+rowId).text('');
			$('#productQty_'+rowId).val('');
			var packageUnit = Number($(this).val()); 
			var packageType = Number($('#packageType_'+rowId).val()); 
			var unitrate = Number($jquery('#amount_'+rowId).val());
			var productType = $.trim($('#productType_'+rowId).val());
			var total=Number(0); 
			$('.quantityerr').remove(); 
			if(packageType == -1 && packageUnit > 0){
				var extproductQty =  Number($('#extproductQty_'+rowId).val());
				var availQty = Number($('#totalAvailQty_'+rowId).val());
				 var issueRequistionDetailId = Number($('#issueRequistionDetailId_'+rowId).val());
		 	 	 if(issueRequistionDetailId > 0){
		 	 		availQty = availQty + extproductQty;
		 	 	 }
 		 	 	if(packageUnit > availQty && productType != 'C') {
		 	 		$('#productQty_'+rowId).parent().append("<span class='quantityerr' id=inverror_"+rowId+">Issue Qty should be lt or eq stock qty.</span>"); 
		 	 		$('#productQty_'+rowId).val('');
		 	 		$('#packageUnit_'+rowId).val('');
		 	 		$('#totalAmount_'+rowId).html('');
		 	 	 } else {
		 	 		$('#productQty_'+rowId).val(packageUnit);
		 	 		if(packageUnit >0 && unitrate > 0){ 
		 	 			total = Number(unitrate * packageUnit);
						$jquery('#totalAmountH_'+rowId).val(total);
 		 	 	 		$('#totalAmount_'+rowId).text($('#totalAmountH_'+rowId).val());
		 	 	 	}
 		 	 	 }  
 			}
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			}
			triggerAddRow(rowId);
			return false;
		}); 
		
		$('.productQty').live('change',function(){  
	 		$('.quantityerr').remove(); 
	 	 	 var lineID = getRowId($(this).attr('id'));
	 	 	 var productQty =  Number($('#productQty_'+lineID).val()); 
	 	 	 var extproductQty =  Number($('#extproductQty_'+lineID).val());
	 	 	 var amount = Number($('#amount_'+lineID).val());
	 	 	 var availQty = Number($('#totalAvailQty_'+lineID).val());
	 		 $('#stock_'+lineID).val(availQty-productQty);
	 		 var productType = $.trim($('#productType_'+rowId).val());
	 	 	 var issueRequistionDetailId = Number($('#issueRequistionDetailId_'+lineID).val());
	 	 	 if(issueRequistionDetailId > 0){
	 	 		availQty = availQty + extproductQty;
	 	 	 }
	 	 	 if(productQty > availQty && productType != 'C') {
	 	 		$(this).parent().append("<span class='quantityerr' id=inverror_"+lineID+">Issue Qty should be lt or eq stock qty.</span>"); 
 	 	 		$('#productQty_'+lineID).val('');
	 	 		$('#packageUnit_'+lineID).val('');
	 	 		$('#totalAmount_'+lineID).html('');
	 	 	 } else {
	 	 		var packageType = Number($('#packageType_'+lineID).val());   
	 	 		if(productQty > 0 && packageType == -1){
	 				$('#packageUnit_'+lineID).val(productQty);
	 			}else if(productQty > 0 && packageType > 0){ 
	 				getProductConversionUnit(lineID);
	 			} 
	 	 		if(productQty >0 && amount > 0){  
	 	 			var total = Number(amount * productQty);
					$jquery('#totalAmountH_'+lineID).val(total);
		 	 	 	$('#totalAmount_'+lineID).text($('#totalAmountH_'+lineID).val());
	 	 	 	}
	 	 	 	triggerAddRow(lineID);
	 	 	 }  
	 		 return false;
	 	 });

	 	$('.amount').live('change',function(){ 
	 		 var lineID = getRowId($(this).attr('id'));
	 	 	 var productQty =  Number($('#productQty_'+lineID).val());
	 	 	 var amount = Number($jquery('#amount_'+lineID).val());
	 	 	 $('#baseAmount_'+lineID).val('');
	 	 	 if(productQty >0 && amount > 0){
	 	 		$('#totalAmount_'+lineID).text(Number(productQty) *  (amount));
	 	 	 }
	 		triggerAddRow(lineID);
	 		return false;
	 	 });
		
	 var getRequistionDetails = function(){ 
		 var productArray = new Array();
		 var quantityArray = new Array();
	 	 var amountArray = new Array(); 
	 	 var descriptionArray = new Array(); 
	 	 var issueRequistionDetailArray = new Array(); 
	 	 var requistionDetailArray = new Array(); 
	 	 var shelfArray = new Array(); 
	 	 var sectionArray = new Array(); 
	 	 var stockArray = new Array(); 
	 	 var packageTypeArray = new Array(); 
	 	 var packageUnitArray = new Array(); 
	 	 var bacthNOArray = new Array(); 
	 	 var expiryDateArray = new Array(); 
	 	 var requistionDetail = ""; 
	 		$('.rowid').each(function(){  
	 			 var rowId = getRowId($(this).attr('id'));  
	 			 var productId = $('#productid_'+rowId).val();   
	 			 var quantity = $('#productQty_'+rowId).val();  
	 			 var amount = $jquery('#amount_'+rowId).val();    
	 			 var lineDescription=$('#linesDescription_'+rowId).val();
	 			 var issueRequistionDetailId = Number($('#issueRequistionDetailId_'+rowId).val());
	 			 var requisitionDetailId = Number($('#requistionDetailId_'+rowId).val());
	 			 var shelfid = Number($('#shelfid_'+rowId).val());
	 			 var section = Number($('#section_'+rowId).val());
	 			 var packageType = Number($('#packageType_'+rowId).val());
	 			 var packageUnit = Number($('#packageUnit_'+rowId).val());
	 			 var stock = Number($('#stock_'+rowId).val());
	 			 var bacthNumber = $('#batchNumber_'+rowId).val();
	 			 var expiryDate = $('#expiryDate_'+rowId).val();
	 			 if(typeof productId != 'undefined' && productId!=null && 
	 					productId!="" && quantity!='' && amount!=''){
	 				productArray.push(productId); 
	 				quantityArray.push(quantity); 
	 				amountArray.push(amount);
	  				if(lineDescription == null || lineDescription == "")
	  					descriptionArray.push("##");
	 				else 
	 					descriptionArray.push(lineDescription); 
	  				issueRequistionDetailArray.push(issueRequistionDetailId); 
	  				shelfArray.push(shelfid);
	  				requistionDetailArray.push(requisitionDetailId);
	  				sectionArray.push(section);
	  			 	stockArray.push(stock);
	  			 	packageTypeArray.push(packageType);
	  			 	packageUnitArray.push(packageUnit);
	  			 	if(bacthNumber == null || bacthNumber == "")
	  			 		bacthNOArray.push("##");
	  			 	else 
	  			 		bacthNOArray.push(bacthNumber); 
	  			 	if(expiryDate == null || expiryDate == "")
	  			 		expiryDateArray.push("##");
	  			 	else 
	  			 		expiryDateArray.push(expiryDate); 
	 			 } 
	 		});
	 		 
	  		for(var j=0;j<productArray.length;j++){ 
	  			requistionDetail += productArray[j]+"__"+quantityArray[j]+"__"+amountArray[j]+"__"+ descriptionArray[j]
	  				+"__"+issueRequistionDetailArray[j]+"__"+shelfArray[j]+"__"+requistionDetailArray[j]+"__"+sectionArray[j]
	  				+"__"+stockArray[j]+"__"+packageTypeArray[j]+"__"+packageUnitArray[j]+"__"+bacthNOArray[j]+"__"+expiryDateArray[j];
	 			if(j==productArray.length-1){   
	 			} 
	 			else{
	 				requistionDetail += "@#";
	 			}
	 		} 
	  		 
	 		return requistionDetail; 
	 	}; 

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

 	$('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

 	  $('#product-stock-popup').live('click',function(){  
 		   $('#common-popup').dialog('close'); 
 	   });

 	 $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });


 	$('.shipping-source-lookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode = $(this).attr("id");
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingSource').html("");
			$('#shippingSource').append("<option value=''>Select</option>");
			loadLookupList("shippingSource"); 
		} 
	});


 	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('.common-department-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	$('.show_customer_list').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false,  
		 	data: { pageInfo: "sales_order"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	$('.common-requisition-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_requisitionpopup_list.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	 $('#customer-common-popup').live('click',function(){  
     	$('#common-popup').dialog('close'); 
      });


 	$('.product-stock-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		var rowId = getRowId($(this).attr('id'));  
		var itemType = '';
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_popup.action", 
		 	async: false,   
		 	data: {itemType: itemType, rowId: rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
 	
 	$('.show_store_list_sales_delivery').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]); 
		var productId = Number($('#productid_'+tableRowId).val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_common_popup.action", 
		 	async: false,  
		 	data: { rowId: tableRowId, productId: productId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0); 
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	{
		if(Number('${ISSUE_REQUISTION.issueRequistionId}')>0){
			$('#issueDate').datepick();
			if('${ISSUE_REQUISTION.requisition}' == null || '${ISSUE_REQUISTION.requisition}' == ''){
				//manupulateLastRow();
			} else{
				$('.common-department-popup').remove();
			}
			$('#shippingSource').val($('#tempShippingSource').val()); 
			$('.rowid').each(function(){   
		   		 var rowId=getRowId($(this).attr('id')); 
		   		 $('#section_'+rowId).val($('#sectionTemp_'+rowId).val());
		   		 $('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
			 });  
			 
		}else{
			$('#issueDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
		} 
	} 
 	manupulateLastRow();
});

function getProductBaseUnit(rowId){
	$('#baseUnitConversion_'+rowId).text('');
	var packageQuantity = Number($('#packageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	var basePrice = Number($('#baseAmount_' + rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#productQty_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
			$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
			if(response.productPackagingDetailVO.basePrice != null && response.productPackagingDetailVO.basePrice > 0){
				$jquery('#amount_' + rowId).val(response.productPackagingDetailVO.basePrice);
			} 
			$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
 		} 
	});  
	var productQty = Number($('#productQty_'+rowId).val());
	var unitrate = Number($jquery('#amount_'+rowId).val());
	var extproductQty =  Number($('#extproductQty_'+rowId).val());
	var availQty = Number($('#totalAvailQty_'+rowId).val());
	var issueRequistionDetailId = Number($('#issueRequistionDetailId_'+rowId).val());
 	if(issueRequistionDetailId > 0){
 		availQty = availQty + extproductQty;
 	}
 	if(productQty > availQty) {
 		$('#productQty_'+rowId).parent().append("<span class='quantityerr' id=inverror_"+rowId+">Issue Qty should be lt or eq stock qty.</span>"); 
	 	$('#productQty_'+rowId).val('');
 	}else if(productQty > 0 && unitrate > 0){ 
		var totalAmount = Number(packageQuantity * unitrate); 
		$jquery('#totalAmountH_'+rowId).val(totalAmount);
		$('#totalAmount_'+rowId).text($('#totalAmountH_'+rowId).val());
	}else{
		$('#totalAmount_'+rowId).text('');
	}  
	return false;
}

function getProductConversionUnit(rowId){
	var packageQuantity = Number($('#productQty_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
		} 
	});   
	return false;
}

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}

function triggerAddRow(rowId){   
	var productid = $('#productid_'+rowId).val();  
	var productQty = $('#productQty_'+rowId).val(); 
	var amount = $('#amount_'+rowId).val();
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid!=null && productid!=""
			&& productQty!=null && productQty!=""  
			&& amount!=null && amount!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}
function departmentBranchPopupResult(locationId,locationName){
	$('#locationId').val(locationId);
	$('#locationName').val(locationName);
}
function getStockDetails(currentObj){
	var productId = $('#productid_'+currentObj).val();
	var storeId = $('#storeid_'+currentObj).val(); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_stock_details.action",
			async : false,
			data : {
				productId : productId,
				storeId : storeId
			},
			dataType : "json",
			cache : false,
			success : function(response) {
				if (response.stockVO.availableQuantity != null
						&& response.stockVO.availableQuantity != '') { 
					$('#totalAvailQty_' + currentObj).val(
							response.stockVO.availableQuantity);
					$jquery('#amount_' + currentObj).val(response.stockVO.unitRate);
					$('#baseAmount_' + currentObj).val(response.stockVO.unitRate);
					$('#totalAmount_' + currentObj).text(
							response.stockVO.totalAmount);
					$('#batchNumber_' + currentObj).text(
							response.stockVO.batchNumber);
					$('#productExpiry_' + currentObj).text(
							response.stockVO.productExpiry);
					triggerAddRow(currentObj);
				} else {
					$('#line-error').hide().html(
							"For Product " + $('#product_' + currentObj).text()
									+ " in store "
									+ $('#store_' + currentObj).text()
									+ " is empty.").slideDown(2000);
					$('#line-error').delay().slideUp();
				}
				return false;
			} 
		});
		return false;
	}
	
	function commonProductPopup(aData, rowId) {
		$('#stock_'+rowId).val("");
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.productName);
		$('#unitCode_' + rowId).html(aData.unitCode);
		$('#store_' + rowId).html(aData.storeName);
		$('#totalAvailQty_' + rowId).val(aData.availableQuantity); 
		$jquery('#amount_' + rowId).val(aData.unitRate); 
		$('#baseAmount_' + rowId).val(aData.unitRate); 
 		$('#shelfid_' + rowId).val(aData.shelfId);
 		$('#batchNumber_' + rowId).val(aData.batchNumber); 
		$('#expiryDate_' + rowId).val(aData.expiryDate); 
 		$('#baseUnitConversion_'+rowId).text('');
 		$('#productType_' + rowId).val(aData.productType); 
		getProductPackagings("packageType",aData.productId, rowId);
 		//var quantity = Number($('#productQty_'+rowId).val());
 		//var totalAvailQty=Number($('#totalAvailQty_'+rowId).val());
 		//$('#stock_'+rowId).val(totalAvailQty-quantity);
		//$('.amount').trigger('change');
	}
	
	function getProductPackagings(idName, productId, rowId){
		$('#'+idName+'_'+rowId).html('');
		$('#'+idName+'_'+rowId)
		.append(
				'<option value="-1">Select</option>');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.productPackageVOs != null){ 
					$.each(response.productPackageVOs, function (index) {
						if(response.productPackageVOs[index].productPackageId == -1){ 
							$('#'+idName+'_'+rowId)
							.append('<option value='
									+ response.productPackageVOs[index].productPackageId
									+ '>' 
									+ response.productPackageVOs[index].packageName
									+ '</option>');
						}else{ 
							var optgroup = $('<optgroup>');
				            optgroup.attr('label',response.productPackageVOs[index].packageName);
				            optgroup.css('color', '#c85f1f'); 
				             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
				                var option = $("<option></option>");
				                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
				                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
				                option.css('color', '#000'); 
				                option.css('margin-left', '10px'); 
				                optgroup.append(option);
				             });
				             $('#'+idName+'_'+rowId).append(optgroup);
						}  
					}); 
					$('#'+idName+'_'+rowId).multiselect('refresh'); 
 				} 
			} 
		});  
		return false;
	}
	
	function personPopupResult(personid, personname, commonParam) {
		$('#personId').val(personid);
		$('#personNumber').val(personname);
		$('#common-popup').dialog("close");
	}
	
	function commonRequisitionPopup(aData){
		$('.common-department-popup').remove();
		$('#requisitionId').val(aData.requisitionId);
		$('#requisitionReference').val(aData.referenceNumber);
		$('#locationName').val(aData.locationName);
		$('#locationId').val(aData.locationId);
		var requisitionId = aData.requisitionId;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getrequisition_product_issueance_details.action", 
		 	async: false,  
		 	data: {requisitionId: requisitionId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.storehead').show(); 
				 $('.tab').html(result);  
				 return false;
			} 
		});  
		return false;
	}
	
	/*function commonStorePopup(storeId, storeName, rowId) { 
		$('#storeid_' + rowId).val(storeId);
		$('#store_' + rowId).html(storeName);
		getStockDetails(rowId);
		return false;
	}*/

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	
	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName);
		return false;
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Issue
			Requisition
		</div> 
		<form name="requistionValidation" id="requistionValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="issueRequistionId" name="issueRequistionId"
					value="${ISSUE_REQUISTION.issueRequistionId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 120px;"> 
							<div>
								<label class="width30"> Shipping Source</label> <select
									name="shippingSource" id="shippingSource" class="width51">
									<option value="">Select</option>
									<c:forEach var="SOURCE" items="${SHIPPING_SOURCE}">
										<option value="${SOURCE.lookupDetailId}">${SOURCE.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingSource"
									value="${ISSUE_REQUISTION.lookupDetail.lookupDetailId}" />
									<span
									class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="SHIPPING_SOURCE"
									class="btn ui-state-default ui-corner-all shipping-source-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
 							<div>
 								<label class="width30">Issuing Person</label> 
 								<c:choose>
									<c:when
									test="${ISSUE_REQUISTION.person ne null && ISSUE_REQUISTION.person ne ''}"> 
										<input type="text" readonly="readonly" id="personName" name="personName" 	
											value="${ISSUE_REQUISTION.person.firstName} ${ISSUE_REQUISTION.person.lastName}" class="width50" />
										<input type="hidden" id="personId" value="${ISSUE_REQUISTION.person.personId}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="personName" name="personName"  
											value="${requestScope.personName}" class="width50" />
										<input type="hidden" id="personId" value="${requestScope.personId}"/>
									</c:otherwise>
								</c:choose>   
								<span class="button"
									style="">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>  
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${ISSUE_REQUISTION.description}</textarea>
							</div>
							<div>
								<label class="width30">Add More Sections </label>
								<span
										class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="SECTIONS"
										class="btn ui-state-default ui-corner-all shipping-source-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>  <c:choose>
										<c:when
											test="${ISSUE_REQUISTION.referenceNumber ne null && ISSUE_REQUISTION.referenceNumber ne ''}">
												<input type="text" readonly="readonly" value="${ISSUE_REQUISTION.referenceNumber}"
												 id="referenceNumber" class="width50"/>
											</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" value="${requestScope.referenceNumber}" 
												id="referenceNumber" class="width50"/>
										</c:otherwise>
									</c:choose> 
							</div> 
							<div>
								<label class="width30"> Issue Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${ISSUE_REQUISTION.issueDate ne null && ISSUE_REQUISTION.issueDate ne ''}">
										<c:set var="issueDate" value="${ISSUE_REQUISTION.issueDate}" />
										<input name="issueDate" type="text" readonly="readonly"
											id="issueDate"
											value="<%=DateFormat.convertDateToString(pageContext.getAttribute("issueDate").toString())%>"
											class="issueDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="issueDate" type="text" readonly="readonly"
											id="issueDate" class="issueDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Requisition</label> <input type="text"
									readonly="readonly" name="requisitionReference" id="requisitionReference"
									class="width50" value="${ISSUE_REQUISTION.requisition.referenceNumber}" />
								<span class="button">
									<a style="cursor: pointer;" id="location"
									class="btn ui-state-default ui-corner-all common-requisition-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="requisitionId"
									id="requisitionId"
									value="${ISSUE_REQUISTION.requisition.requisitionId}" />
							</div> 
							<div>
								<label class="width30">Location Name<span
									style="color: red;">*</span> </label> <input type="text"
									readonly="readonly" name="locationName" id="locationName"
									class="width50 validate[required]"
									value="${ISSUE_REQUISTION.cmpDeptLocation.location.locationName}" />
								<span class="button">
									<a style="cursor: pointer;" id="location"
									class="btn ui-state-default ui-corner-all common-department-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="locationId"
									id="locationId"
									value="${ISSUE_REQUISTION.cmpDeptLocation.cmpDeptLocId}" />
							</div>
							<div>
								<label class="width30">Customer Name</label>
								<c:set var="customerName" />
								<c:choose>
									<c:when test="${ISSUE_REQUISTION.customer.personByPersonId ne null}">
										<c:set var="customerName"
											value="${ISSUE_REQUISTION.customer.personByPersonId.firstName} ${SALES_ORDER.customer.personByPersonId.lastName}" />
									</c:when>
									<c:when test="${ISSUE_REQUISTION.customer.company ne null}">
										<c:set var="customerName"
											value="${ISSUE_REQUISTION.customer.company.companyName}" />
									</c:when>
									<c:when test="${ISSUE_REQUISTION.customer.cmpDeptLocation ne null}">
										<c:set var="customerName"
											value="${ISSUE_REQUISTION.customer.cmpDeptLocation.company.companyName}" />
									</c:when>
								</c:choose>
								<input type="text" readonly="readonly" name="customerName"
									value="${customerName}" id="customerName" class="width50" /> <input
									type="hidden" id="customerId" name="customerId"
									value="${ISSUE_REQUISTION.customer.customerId}" /> <span
									class="button"> <a
									style="cursor: pointer;" id="customer"
									class="btn ui-state-default ui-corner-all show_customer_list width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>  
							<div>
								<label class="width30">Other Reference</label>  										
								<input type="text" value="${ISSUE_REQUISTION.referenceNumber}"
								 id="otherReference" class="width50"/>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>Issue Requisition Detail<span
									class="mandatory">*</span></legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th style="display: none;">UOM</th>
										<th style="display: none;">Costing Type</th>
										<th style="display: none;width: 10%;" class="storehead">Store</th>
										<th style="width: 8%;">Packaging</th> 
										<th style="width: 4%">Quantity</th>
										<th style="width: 5%;">Base Qty</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 5%;">Total</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 10%;">Section</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${ISSUE_REQUISTION_DETAIL}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> 
												<input type="hidden" name="productType" id="productType_${status.index+1}" value="${DETAIL.productType}"/> 
												<span class="width60 float-left"
												id="product_${status.index+1}">${DETAIL.product.productName}</span>
												<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all product-stock-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td id="uom_${status.index+1}" style="display: none;">
												</td>
											<td id="costingType_${status.index+1}" style="display: none;"></td>
											<td style="display: none;">
												<input type="hidden" name="shelfId"
												id="shelfid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" />
											 <span
												id="store_${status.index+1}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
										</td>
										<td><input type="text"
											class="width96 packageUnit validate[optional,custom[number]]"
											id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
										</td> 
										<td><input type="hidden" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> <input type="hidden"
												name="totalAvailQty" id="totalAvailQty_${status.index+1}" value="${DETAIL.availableQuantity}"/>
												<input type="hidden" value="${DETAIL.baseQuantity}"
													name="extproductQty" id="extproductQty_${status.index+1}" />
												<span id="baseUnitConversion_${status.index+1}" class="width10" 
													style="display: none;">${DETAIL.baseUnitName}</span>
												<span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="amount width98 validate[optional,custom[number]] right-align">
												<input type="hidden" name="baseAmount" id="baseAmount_${status.index+1}" value="${DETAIL.unitRate}"/>
											</td>
											<td><c:set var="totalAmount"
													value="${DETAIL.quantity * DETAIL.unitRate}" /> <span
												id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
												<input type="text" style="display: none;" id="totalAmountH_${status.index+1}" class="totalAmountH"/>
											</td>
										
											<td><input type="text" name="linesDescription"
												id="linesDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td>
												<select name="section" class="section width98" id="section_${status.index+1}">
													<option value="">-Select-</option>
													<c:forEach var="sec" items="${SECTIONS}" varStatus="status1">
														<option value="${sec.lookupDetailId}">${sec.displayName}</option>
													</c:forEach>
												</select>
												<input type="hidden" name="sectionTemp"
												id="sectionTemp_${status.index+1}"
												value="${DETAIL.lookupDetail.lookupDetailId}" class="sectionTemp width98"
												/>
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="issueRequistionDetailId"
												id="issueRequistionDetailId_${status.index+1}"
												value="${DETAIL.issueRequistionDetailId}" />
												<input type="hidden" name="requistionDetailId" value="${DETAIL.requisitionDetail.requisitionDetailId}"
													 id="requistionDetailId_${status.index+1}"/>
												<input type="hidden" id="stock_${status.index+1}" value="${DETAIL.stock}"/>
												<input type="hidden" name="batchNumber" value="${DETAIL.batchNumber}"
													 id="batchNumber_${status.index+1}"/>
												<input type="hidden" name="expiryDate" value="${DETAIL.expiryDate}" 
													id="expiryDate_${status.index+1}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(ISSUE_REQUISTION_DETAIL)+1}"
										end="${fn:length(ISSUE_REQUISTION_DETAIL)+2}"
										step="1" varStatus="status">

											<tr class="rowid" id="fieldrow_${i}">
												<td style="display: none;" id="lineId_${i}">${i}</td>
												<td><input type="hidden" name="productId"
													id="productid_${i}" />
													<input type="hidden" name="productType"
													id="productType_${i}" /> 
													<span id="product_${i}" class="width60 float-left"></span> 
													<span class="width10 float-right" id="unitCode_${i}" style="position: relative;"></span>
													<span
													class="button float-right"> <a
														style="cursor: pointer;" id="prodID_${i}"
														class="btn ui-state-default ui-corner-all product-stock-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td id="uom_${i}" style="display: none;"></td>
												<td id="costingType_${i}" style="display: none;"></td>
												<td style="display: none;">
												<input type="hidden" name="shelfId"
													id="shelfid_${i}"/>
												<span id="store_${i}"></span> <span
													class="button float-right"> <a
														style="cursor: pointer;" id="storeID_${i}"
														class="btn ui-state-default ui-corner-all common-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td>
													<select name="packageType" id="packageType_${i}" class="packageType">
														<option value="">Select</option>
													</select> 
												</td> 
												<td><input type="text"
													class="width96 packageUnit validate[optional,custom[number]]"
													id="packageUnit_${i}"  />
		 										</td>
												<td><input type="hidden" name="productQty"
													id="productQty_${i}" class="productQty validate[optional,custom[number]] width80"> <input
													type="hidden" name="totalAvailQty" id="totalAvailQty_${i}" />
													<input type="hidden" name="extproductQty" id="extproductQty_${i}" />
													<span id="baseUnitConversion_${i}" class="width10" 
														style="display: none"></span>
													<span id="baseDisplayQty_${i}"></span>
												</td>
												<td><input type="text" name="amount" id="amount_${i}"
													style="text-align: right;"
													class="amount width98 validate[optional,custom[number]] right-align"> 
													<input type="hidden" name="baseAmount" id="baseAmount_${i}"/>
												</td>
												<td>
													<span id="totalAmount_${i}" style="float: right;"></span>
													<input type="text" style="display: none;" id="totalAmountH_${i}" class="totalAmountH"/>
												</td>
												<td><input type="text" name="linesDescription"
													id="linesDescription_${i}" class="width98" maxlength="150">
												</td>
												<td>
													<select name="section" class="section width98" id="section_${i}">
														<option value="">-Select-</option>
														<c:forEach var="sec" items="${SECTIONS}" varStatus="status1">
															<option value="${sec.lookupDetailId}">${sec.displayName}</option>
														</c:forEach>
													</select>
													<input type="hidden" name="sectionTemp"
													id="sectionTemp_${i}"
													value="" class="sectionTemp width98"
													/>
												</td>
												<td style="width: 1%;" class="opn_td" id="option_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="cursor: pointer; display: none;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span
														class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="issueRequistionDetailId"
													id="issueRequistionDetailId_${i}" />
													<input type="hidden" name="requistionDetailId" id="requistionDetailId_${i}"/>
													<input type="hidden" id="stock_${i}"/>
													<input type="hidden" name="batchNumber" id="batchNumber_${i}"/>
													<input type="hidden" name="expiryDate" id="expiryDate_${i}" />
												</td>
											</tr>
										</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="requistion_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>