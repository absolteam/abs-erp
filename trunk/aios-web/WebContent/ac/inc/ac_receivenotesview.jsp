<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Goods Receive Notes</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
<script type="text/javascript">
$(function(){ 
$('.receive_discard').click(function(){
	 $('.formError').remove();	
	 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/receive_notes_discard.action", 
	 	async: false, 
	    dataType: "html",
	    cache: false,
		success:function(result){
			 $('#common-popup').dialog('destroy');		
			 $('#common-popup').remove();  
			 $('#store-popup').dialog('destroy');		
			 $('#store-popup').remove();  
			 $('#store-popuprcv').dialog('destroy');		
			 $('#store-popuprcv').remove();  
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
			 $("#main-wrapper").html(result);  
		}
	});
});
});
</script> 
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>goods receive notes</div>	 	 
		<div class="portlet-content"> 
			<body style="margin-top: 15px; font-size: 12px;"> 
			<div class="width100" >
				<span class="title-heading"><span>${THIS.companyName}</span></span> 
			</div>
			
			<div style="clear: both;"></div>
			<div class="width98" ><span class="heading"><span>GOODS RECEIVE NOTE</span></span></div>
			<div style="clear: both;"></div>
			<div style="height: 150px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
				 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
				<div class="width40 float_right">
					 <div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">GRN.NO.<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${RECEIVE_INFO.receiveNumber}</span></span>  
					</div>
					<div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">REF.NO.<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">
							<c:choose>
								<c:when test="${RECEIVE_INFO.referenceNo ne null && RECEIVE_INFO.referenceNo ne ''}">
									${RECEIVE_INFO.referenceNo}
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span></span>  
					</div>
					<div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">PO.NO.<span class="float_right">:</span></span>
							<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">  
								${PURCHASE_NUMBER} 
							</span>
						</span>  
					</div>
					<div class="width98 div_text_info">
						<span class="float_left left_align width28 text-bold">DATE<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:set var="date" value="${RECEIVE_INFO.receiveDate}"/>  
							<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
						</span>  		
					</div> 
					<div class="width98 div_text_info" style="display: none;">
						<span class="float_left width28 left_align">SP.NO<span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${RECEIVE_INFO.supplier.supplierNumber}</span>  
					</div>
				</div> 
				<div class="width50 float_left">
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">NAME <span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
							<c:choose>
								<c:when test="${RECEIVE_INFO.supplier.person ne null && RECEIVE_INFO.supplier.person ne ''}">
									${RECEIVE_INFO.supplier.person.firstName} ${RECEIVE_INFO.supplier.person.lastName} 
									 <c:set var="supplierType" value="Person"/>
								</c:when>
								<c:when test="${RECEIVE_INFO.supplier.company ne null && RECEIVE_INFO.supplier.company ne ''}">
									${RECEIVE_INFO.supplier.company.companyName} 
									 <c:set var="supplierType" value="Person"/>
								</c:when>
								<c:otherwise>
									${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyName}
									<c:set var="supplierType" value="Company"/>
								</c:otherwise>
							</c:choose>
						</span> 
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">ADDRESS <span class="float_right">:</span></span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
							<c:choose> 
								<c:when test="${RECEIVE_INFO.supplier.person ne null && RECEIVE_INFO.supplier.person ne ''}">
									<c:choose> 
										<c:when test="${RECEIVE_INFO.supplier.person.postboxNumber  ne null && RECEIVE_INFO.supplier.person.postboxNumber ne '' }">
											P.O.Box: ${RECEIVE_INFO.supplier.person.postboxNumber}
											<c:choose>
												<c:when test="${RECEIVE_INFO.supplier.person.residentialAddress  ne null && RECEIVE_INFO.supplier.person.residentialAddress ne '' }">
													, ${RECEIVE_INFO.supplier.person.residentialAddress}  
												</c:when>	
											</c:choose>
										</c:when> 
										<c:otherwise> 
											-N/A-
										</c:otherwise>
									</c:choose>	
								</c:when>
								<c:when test="${RECEIVE_INFO.supplier.person ne null && RECEIVE_INFO.supplier.person ne ''}">
									<c:choose> 
										<c:when test="${RECEIVE_INFO.supplier.company.companyPobox ne null && RECEIVE_INFO.supplier.company.companyPobox ne ''}">
											P.O.Box: ${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPobox}
											<c:choose>
												<c:when test="${RECEIVE_INFO.supplier.company.companyAddress ne null && RECEIVE_INFO.supplier.company.companyAddress ne ''}">
													, ${RECEIVE_INFO.supplier.company.companyAddress} 
												</c:when> 
											</c:choose>
										</c:when> 
										<c:otherwise> 
											-N/A-
										</c:otherwise>
									</c:choose>	
								</c:when>
								<c:otherwise> 
									<c:choose> 
										<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPobox ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPobox ne ''}">
											P.O.Box: ${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPobox}
											<c:choose>
												<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyAddress ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company.companyAddress ne ''}">
													, ${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyAddress} 
												</c:when> 
											</c:choose>
										</c:when> 
										<c:otherwise> 
											-N/A-
										</c:otherwise>
									</c:choose>	
								</c:otherwise>	 
							</c:choose>			
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">MOBILE NO <span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${RECEIVE_INFO.supplier.person ne null && RECEIVE_INFO.supplier.person ne ''}">
									<c:choose>
										<c:when test="${RECEIVE_INFO.supplier.person.mobile ne null && RECEIVE_INFO.supplier.person.mobile ne ''}">
											${RECEIVE_INFO.supplier.person.mobile} 
										</c:when>
										<c:otherwise>
											-N/A-
										</c:otherwise>
									</c:choose> 
								</c:when>
								<c:when test="${RECEIVE_INFO.supplier.company ne null && RECEIVE_INFO.supplier.company ne ''}">
									<c:choose>
										<c:when test="${RECEIVE_INFO.supplier.company.companyPhone ne null && RECEIVE_INFO.supplier.company.companyPhone ne ''}">
											${RECEIVE_INFO.supplier.company.companyPhone} 
										</c:when> 
										<c:otherwise>
											-N/A-
										</c:otherwise>
									</c:choose>	
								</c:when>
								<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company ne ''}">
									<c:choose>
										<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPhone ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPhone ne ''}">
											${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyPhone} 
										</c:when> 
										<c:otherwise>
											-N/A-
										</c:otherwise>
									</c:choose>	
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span>
					</div>
					<div class="width100 div_text_info">
						<c:choose>
							<c:when test="${RECEIVE_INFO.supplier.person ne null && RECEIVE_INFO.supplier.person ne ''}">
								<c:choose>
									<c:when test="${RECEIVE_INFO.supplier.person.telephone ne null && RECEIVE_INFO.supplier.person.telephone ne ''}">
									 	<span class="float_left left_align width28 text-bold">TEL NO <span class="float_right">:</span> </span>
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${RECEIVE_INFO.supplier.person.telephone}</span>
									</c:when>
									<c:otherwise>
										<span class="float_left left_align width28 text-bold">TEL NO <span class="float_right">:</span> </span>
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
									</c:otherwise>
								</c:choose> 
							</c:when>
							<c:when test="${RECEIVE_INFO.supplier.company ne null && RECEIVE_INFO.supplier.company ne ''}">
								<c:choose>
									<c:when test="${RECEIVE_INFO.supplier.company.companyFax ne null && RECEIVE_INFO.supplier.company.companyFax ne ''}">
										<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span> </span>
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${RECEIVE_INFO.supplier.company.companyFax}</span>
									</c:when> 
									<c:otherwise>
										<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span></span> 
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
									</c:otherwise>
								</c:choose>	
							</c:when>  
							<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company ne ''}">
								<c:choose>
									<c:when test="${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyFax ne null && RECEIVE_INFO.supplier.cmpDeptLocation.company.companyFax ne ''}">
										<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span> </span>
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyFax}</span>
									</c:when> 
									<c:otherwise>
										<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span></span> 
										<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
									</c:otherwise>
								</c:choose>	
							</c:when>  
						</c:choose> 
					</div> 
				</div> 
			</div>
			<div style="margin-top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
				 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
				<table class="width100">
					<tr> 
						<th style="width:2%;">S.NO:</th>
						<th class="width10">CODE</th>
						<th class="width30">DESCRIPTION</th>
						<th class="width10">UNITS</th>  
						<th class="width10">QUANTITY</th>
						<th class="width10">PRICE</th> 
						<th class="width10">STORE</th>
						<th class="width10">TOTAL AMOUNT</th>
					</tr>
					<tbody>  
						<c:choose>
							<c:when test="${RECEIVE_DETAILS_INFO ne null && RECEIVE_DETAILS_INFO ne ''}">
								<c:set var="totalAmount" value="0"/>
								<tr style="height: 25px;">
									<c:forEach begin="0" end="7" step="1">
										<td/>
									</c:forEach>
								</tr>
								<c:forEach items="${RECEIVE_DETAILS_INFO}" var="RECEIVE_DETAIL" varStatus="status"> 
									<tr>
										<td>${status.index+1}</td>
										<td>${RECEIVE_DETAIL.product.code}</td>
										<td>${RECEIVE_DETAIL.product.productName}</td>
										<td>${RECEIVE_DETAIL.product.lookupDetailByProductUnit.displayName}</td> 
										<td>${RECEIVE_DETAIL.receiveQty}</td>
										<td class="right_align">
											<c:set var="unitRate" value="${RECEIVE_DETAIL.unitRate}"/> 
											<%=AIOSCommons.formatAmount(pageContext.getAttribute("unitRate"))%> 
										</td>
										<td>${RECEIVE_DETAIL.shelf.aisle.store.storeNumber} (${RECEIVE_DETAIL.shelf.aisle.aisleNumber})</td>
										<td class="right_align">
											<c:set var="totalUnitPrice" value="${RECEIVE_DETAIL.receiveQty * RECEIVE_DETAIL.unitRate}"/> 
											<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalUnitPrice"))%> 
										</td> 
										<c:set var="totalAmount" value="${totalAmount+ (RECEIVE_DETAIL.receiveQty * RECEIVE_DETAIL.unitRate)}"/>
									</tr>
								</c:forEach> 
								<c:if test="${fn:length(RECEIVE_DETAILS_INFO)<5}">
									<c:set var="emptyLoop" value="${5 - fn:length(RECEIVE_DETAILS_INFO)}"/> 
									<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
										<tr style="height: 25px;">
											<c:forEach begin="0" end="7" step="1">
												<td/>
											</c:forEach>
										</tr>
									</c:forEach>
								</c:if> 
								<tr>
									<td colspan="7" class="bottomTD left_align" style="font-weight: bold;">TOTAL AMOUNT</td>
									<td class="right_align bottomTD" style="font-weight: bold;">
										<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount")) %> 
									</td>
								</tr>
								<tr> 
									<%
										double netAmount = (Double.valueOf(pageContext.getAttribute("totalAmount").toString()));
									%>
									<td colspan="8" class="left_align"><span style="font-weight: bold;"> AMOUNT IN WORDS : </span> 
										<%
										 	String decimalAmount = String.valueOf(AIOSCommons
										 					.formatAmount(netAmount));
										 			decimalAmount = decimalAmount.substring(decimalAmount
										 					.lastIndexOf('.') + 1);
										 			String amountInWords = AIOSCommons
										 					.convertAmountToWords(netAmount);
										 			if (Double.parseDouble(decimalAmount) > 0) {
										 				amountInWords = amountInWords.concat(" and ") 
										 						.concat(AIOSCommons
										 								.convertAmountToWords(decimalAmount))
										 						.concat(" Fils ");
										 				String replaceWord = "Only";
										 				amountInWords = amountInWords.replaceAll(replaceWord,
										 						"");
										 				amountInWords = amountInWords.concat(" " + replaceWord);
										 			}
										 %> <%=amountInWords%>
									</td>
									<!--<td style="font-weight: bold;">
										<table>
											<tr>
												<td style="border: 0px;">NET AMOUNT</td> 
											</tr> 
										</table> 
									 </td>
									<td style="font-weight: bold;"> 
										<table>
											<tr> 
												<td style="border: 0px;" class="right_align"><%=AIOSCommons.formatAmount(netAmount) %> </td> 
											</tr> 
										</table>  
									</td>
								--></tr>
							</c:when>
						</c:choose> 
					</tbody>
				</table>
			</div> 
			<div class="width100 float_left div_text_info" style="margin-top: 50px;">
				<div class="width100 float_left">
					<div class="width30 float_left">
						<span>Prepared by:</span> 
					</div>  
				</div>
				<div class="width100 float_left" style="margin-top: 30px;">
					<div class="width30 float_left">
						<span class="span_border width70" style="display: -moz-inline-stack;"></span>
					</div> 
				</div>
			</div>
			</body>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div
					class="portlet-header ui-widget-header float-right receive_discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
			</div>
	</div>
</div>	
</html>
