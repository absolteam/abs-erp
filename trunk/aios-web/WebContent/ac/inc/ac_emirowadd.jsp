<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid userrow" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}"></td> 
	<td> 
		<input type="text" name="emiDueDate" readonly="readonly" class="emiDueDate doblur width95" id="dueDate_${rowId}" style="border:0px;"/>
	</td> 
	<td> 
		<input type="text" style="text-align:right;border:0px;" class="width95 doblur instalment" id="instalment_${rowId}"/>
	</td>  
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width90 doblur  interest" id="interest_${rowId}"/>
	</td>
	<c:choose>
		<c:when test="${LOAN_DETAILS_EMI.eibor ne null &&  LOAN_DETAILS_EMI.eibor ne ''}">
			<td class="eiborhead">
				<input type="text" style="text-align:right;border:0px;" class="width95 eibor" id="eibor_${rowId}"/>
			</td>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">
				$(function(){
					$('.eiborhead').remove(); 
				});
			</script>
		</c:otherwise>
	</c:choose>   
	<c:choose>
		<c:when test="${LOAN_DETAILS_EMI.insurance ne null &&  LOAN_DETAILS_EMI.insurance gt 0}">
			<td class="insurancehead">
				<input type="text" style="text-align:right;border:0px;" class="width95 insurance" id="insurance_${rowId}"/>
			</td>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">
				$(function(){
					$('.insurancehead').remove(); 
				});
			</script>
		</c:otherwise>
	</c:choose>    
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width95 addtionalcharges" id="addtionalcharges_${rowId}"/>
	</td>
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width95 depreciation" id="depreciation_${rowId}"/>
	</td>
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width95 cumulative" id="cumulative_${rowId}"/>
	</td>
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width95 balance" id="balance_${rowId}"/>
	</td>
	<td>
		<input type="text" name="description" class="description width95" id="description_${rowId}" value="" style="border:0px;"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
			<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
			<span class="processing"></span>
		  </a>
	</td>   
</tr>
<script type="text/javascript">
$(function(){ 
	$('.emiDueDate').datepick();
});
</script>