<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
 var receiveFlag=null;
$(function(){ 
	 oTable = $('#GoodsReturnList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_alljsonreturn_goods.action",

		"aoColumns" : [ { 
			"mDataProp" : "returnNumber"
		 }, {
			"mDataProp" : "goodsReturnDate"
		 }, {
			"mDataProp" : "supplierName"
		 }, {
			"mDataProp" : "grnNumber"
		 }], 
	}); 
	 
	/* Click event handler */
	$('#GoodsReturnList tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		commonGoodsReturnPopup(aData.goodsReturnId, aData.returnNumber, aData.supplierName);  
		$('#goodsreturn-common-popup').trigger('click');
		return false;
  	});

	$('#GoodsReturnList tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          purchaseId=aData[0];  
	          receiveFlag=aData.receiveFlag;
	          if(receiveFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          purchaseId=aData[0];  
	          receiveFlag=aData.receiveFlag;
	          if(receiveFlag==true){
	        	  $("#edit").css('opacity','0.5');
	        	  $("#delete").css('opacity','0.5');
	          }else{
	        	  $("#edit").css('opacity','1');
	        	  $("#delete").css('opacity','1');
	          }
	      }
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>goods return</div>	 	 
		<div class="portlet-content"> 
  			 	<div id="goods_return_list">
					<table id="GoodsReturnList" class="display">
						<thead>
							<tr>
								<th>Reference</th> 
								<th>Date</th> 
								<th>Supplier</th>  
								<th>Receive</th>
							</tr>
						</thead> 
					</table> 
 			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="goodsreturn-common-popup" >close</div> 
 			</div>
		</div>
	</div>
</div>