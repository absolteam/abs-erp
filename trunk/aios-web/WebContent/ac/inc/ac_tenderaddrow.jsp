<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td><select name="" class="width98 itemnature"
		id="itemnature_${rowId}">
			<option value="A">Asset</option>
			<option value="E">Expense</option>
			<option value="I" selected="selected">Inventory</option>
	</select> 
	</td>
	<td><span class="float-left width60" id="product_${rowId}"></span>
		<span class="button float-right prod" style="height: 16px;"> <a
			style="cursor: pointer; height: 100%;" id="productline_${rowId}"
			class="btn ui-state-default ui-corner-all product-common-popup width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input type="hidden"
		id="productid_${rowId}"/></td>
	<td style="display: none;" id="uom_${rowId}" class="uom"></td>
	<td><input type="text" class="width96 tquantity validate[optional,custom[number]]"
		id="quantity_${rowId}"/>
		<input type="text" style="display: none;"
			class="totalAmountH" id="totalAmountH_${rowId}"/>
	</td>
	<td><input type="text" class="width96 tunitrate"
		id="unitrate_${rowId}" style="text-align: right;" /></td> 
	<td class="totalamount validate[optional,custom[number]]" id="totalamount_${rowId}"
		style="text-align: right;"></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${rowId}" /></td>
	<td style="display: none;"><input type="hidden"
		id="tenderLineId_${rowId}" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a></td>
</tr>
<script type="text/javascript">
$(function() {
	$jquery("#tenderCreation").validationEngine('attach'); 
	$jquery("#unitrate_${rowId},#totalAmountH_${rowId}").number(true, 3); 
});
</script>