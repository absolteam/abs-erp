<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<style type="text/css">  
.barcodeText{text-align: center; font-size: 9px; font-weight: bold;}
.productLabel{text-align: center; font-size: 10px;}
</style>
	<body onload="window.print();">
	<table id="maincategory-table" class="pos-item-table width100">
	<c:set var="barRow" value="0" />
	<c:forEach begin="0" step="3"  items="${BARCODE_INFO}"> 
		<tr>
			<!-- TD 1 -->
			<td>
				<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
			</td>
			<!-- TD 2 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if>
			<!-- TD 3 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if>  
			<!-- TD 4 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if>  
		</tr>
		<tr>
			<!-- TD 1 -->
			<td>
				<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
			</td>
			<!-- TD 2 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if>
			<!-- TD 3 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if> 
			<!-- TD 4 -->
			<c:if test="${BARCODE_INFO[barRow+0]!= null}">
				<td>
					<div style="width:100%;">
					<div class="productLabel">${BARCODE_INFO[barRow+0].productName}</div>
					<div style="width:30%;">
					<img name="preview" id="preview" 
					src="data:image/png;base64,${BARCODE_INFO[barRow+0].productPic}">
					</div>
					<div class="barcodeText">${BARCODE_INFO[barRow+0].productCode}</div> 
				</div> 
				</td>
			</c:if>  
			<c:set var="barRow" value="${categoryRow + 3}" />
		</tr>
 	</c:forEach>
 	</table>
	<div class="clear-fix"></div>
	 
	</body>
</html>