<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){  
	$('#PettyCashVoucher').dataTable({ 
		"sAjaxSource": "show_alljson_nonspendvouchers.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "PettyCashId", "bVisible": false},
			{ "sTitle": "PersonId", "bVisible": false},
			{ "sTitle": "Expense", "bVisible": false},  
			{ "sTitle": "Voucher No"},
			{ "sTitle": "Employee"}, 
 			{ "sTitle": "Invoice"},
 			{ "sTitle": "Remaining Balance"}
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	
	//init datatable
	oTable = $('#PettyCashVoucher').dataTable();
	 
	/* Click event handler */
	$('#PettyCashVoucher tbody tr').live('dblclick', function () {  
		 aData = oTable.fnGetData( this );
         commonPettyCashDetail(aData[0], aData[1], aData[2], aData[3], aData[4], aData[5], aData[6]);
         $('#pettycashvoucher-close').dialog('close');
         return false;
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>petty cash vouchers</div>	 	 
		<div class="portlet-content"> 
 	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="rightclickarea">
			 	<div id="petty_cash_list">
					<table class="display" id="PettyCashVoucher"></table>
				</div> 
			</div>	 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="pettycashvoucher-close" >
					<fmt:message key="accounts.common.button.cancel"/></div>  
			</div>
		</div>
	</div>
</div>