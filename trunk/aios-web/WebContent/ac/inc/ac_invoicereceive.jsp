 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%>   
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			receive details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="receivelist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${RECEIVE_LIST_INFO}" var="bean" varStatus="status">  
	     		<option value="${bean.receiveId}|${bean.purchase.supplier.supplierNumber}|
	     		${bean.purchase.supplier.person.firstName}${bean.purchase.supplier.cmpDeptLocation.company.companyName}
	     		|${bean.purchase.creditTerm.creditTermId}|${bean.purchase.creditTerm.name}
	     		|${bean.purchase.creditTerm.discountDay}|${bean.purchase.creditTerm.discount}" id="receivebysuppliernameid_${status.index+1}">${bean.receiveNumber}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 receive_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width20 float-left"><span>Receive.No</span></li>  
   				<li class="width10 float-left"><span>Date</span></li>   
   				<li class="width20 float-left"><span>Supplier No</span></li> 
   				<li class="width25 float-left"><span>Name</span></li> 
   				<li class="width20 float-left"><span>Term</span></li> 
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${RECEIVE_LIST_INFO}" var="bean" varStatus="status">
		     		<li id="receivelist_${status.index+1}" class="receive_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.receiveId}" id="receivebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.receiveNumber}" id="receivebynumberid_${status.index+1}">  
		     			<input type="hidden" value="${bean.purchase.supplier.person.firstName}${bean.purchase.supplier.cmpDeptLocation.company.companyName}" id="receivebysuppliernamebyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.supplier.supplierNumber}" id="receivebysuppliernumberbyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.creditTerm.creditTermId}" id="receivebypaymenttermbyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.creditTerm.name}" id="receivebypaymenttermnamebyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.creditTerm.discountDay}" id="receivebypaymenttermdaysbyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.creditTerm.discount}" id="receivebypaymenttermdiscountbyid_${status.index+1}">
		     			<input type="hidden" value="${bean.purchase.creditTerm.name}" id="receivebypaymenttermnamebyid_${status.index+1}">
		     			<c:set var="date" value="${bean.receiveDate}"/>
		     			<%String date = DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
											pageContext.setAttribute("date", date);%> 
		     			<span id="receivebynumber_${status.index+1}" style="cursor: pointer; width:18%" class="float-left">${bean.receiveNumber}</span>
		     			<span id="receivebydate_${status.index+1}" style="cursor: pointer; width:12%" class="float-left">${date}</span> 
		     			<span id="receivebysuppliernumber_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.purchase.supplier.supplierNumber}</span>
		     			<span id="receivebysupplier_${status.index+1}" style="cursor: pointer; width:22%;" class="float-left">${bean.purchase.supplier.person.firstName}${bean.purchase.supplier.cmpDeptLocation.company.companyName}</span>
		     			<span id="receivebypaymenttermname_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.purchase.creditTerm.name}
		     				<span id="receiveNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
       selected: function(event, ui){  
    	   var receivedetails=$(this).val();
	       var receivearray=receivedetails.split("|");
	       $('#receiveId').val(receivearray[0]);  
	       $('#receiveNo').val($(".autoComplete option:selected").text()); 
	       $('#supplierNumber').val(receivearray[1].trim());  
	       $('#supplierName').val(receivearray[2].trim()); 
	       $('#paymentTermId').val(receivearray[3].trim());   
	       $('#common-popup').dialog("close");  
	       if($('#receiveId').val()>0)
	    	   callInvoiceReceiveDetails($('#receiveId').val());
   		} 
	}); 
	 
	 $('.receive_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.receive_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#receiveNameselect_'+rowid).hasClass('selected')){ 
					 $('#receiveNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#receiveNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.receive_list_li').live('dblclick',function(){ 
			var tempvar="";var idarray = ""; var rowid="";
			 currentId=$(this);  
			 tempvar=$(currentId).attr('id');   
			 idarray = tempvar.split('_'); 
			 rowid=Number(idarray[1]);  
			 $('#receiveId').val($('#receivebyid_'+rowid).val());
			 $('#receiveNo').val($('#receivebynumberid_'+rowid).val());  
			 $('#supplierNumber').val($("#receivebysuppliernumberbyid_"+rowid).val());  
	         $('#supplierName').val($("#receivebysuppliernamebyid_"+rowid).val());  
	         $('#paymentTermId').val($("#receivebypaymenttermbyid_"+rowid).val());    
			 $('#common-popup').dialog('close'); 
			 if($('#receiveId').val()>0)
				callInvoiceReceiveDetails($('#receiveId').val());
			 return false; 
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
		return false; 
 });
 function callInvoiceReceiveDetails(receiveId){ 
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_invoicereceive_detail.action", 
		 	async: false,  
		 	data:{receiveId:receiveId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('.tab').html(result);  
			},
			error:function(result){  
				$('.tab').html(result); 
			}
		}); 
		return false; 
}
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.receive_list ul li{
		padding:3px;
	}
	.receive_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>