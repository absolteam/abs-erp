<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		$('.formError').remove();

		oTable = $('#productPricingCalcs').dataTable({
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "show_product_pricing_calc.action",

			"aoColumns" : [ {
				"mDataProp" : "pricingType"
			}, {
				"mDataProp" : "calculationTypeCode"
			}, {
				"mDataProp" : "calculationSubTypeCode"
			}, {
				"mDataProp" : "salesAmount"
			} ],
		});

		/* Click event handler */
		$('#productPricingCalcs tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					commonProductPricePopup(aData.productPricingCalcId,
							aData.calculationTypeCode,
							aData.calculationSubTypeCode, aData.salesAmount, $(
									'#currentRowId').val());
					$('#price-common-popup').trigger('click');
				});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>pricing details
		</div>
		<div class="portlet-content">
			<input type="hidden" id="currentRowId" value="${requestScope.rowId}" />
			<div id="price_json_list">
				<table id="productPricingCalcs" class="display">
					<thead>
						<tr>
							<th>Pricing Type</th>
							<th>Calculation Type</th>
							<th>Sub Type</th>
							<th>Sales Price</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="price-common-popup">close</div>
			</div>
		</div>
	</div>
</div>
