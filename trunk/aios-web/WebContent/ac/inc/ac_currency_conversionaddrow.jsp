<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td><span id="lineId_${requestScope.rowId}">${requestScope.rowId}</span>
	<td><select name="currencyConversion" id="currencyConversion_${requestScope.rowId}"
		class="currencyConversion">
			<option value="">Select</option>
			<c:forEach var="nonFnCurrency" items="${NON_DEFAULT_CURRENCY}">
				<option value="${nonFnCurrency.currencyId}">${nonFnCurrency.code}--${nonFnCurrency.countryName}</option>
			</c:forEach>
	</select></td>
	<td><input type="text" class="conversionRate"
		id="conversionRate_${requestScope.rowId}" style="text-align: right;" /></td>
	<td><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		style="cursor: pointer; display: none;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		style="display: none; cursor: pointer;" title="Working"> <span
			class="processing"></span> </a>
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$jquery("#currencyConversionValidation").validationEngine('attach'); 
	$jquery(".conversionRate").number(true,3); 
});
</script>