<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<script type="text/javascript">
var slidetab="";
var productAccount = "";
$(function(){
	if(Number($('#productId').val())>0){
		 
		$('#itemCategoryId option').each(function(){
			var categoryval="";
			var catarray=$(this).text().split("_"); 
			if(catarray.length>0){
				for(var i=0;i<catarray.length;i++){
					categoryval=categoryval+" "+catarray[i];
				} 
				$(this).text(categoryval); 
		    } 
		});

		$('#unitId').val($('#tempunitId').val());
		$('#itemTypeId').val($('#tempitemTypeId').val());
		$('#subItemType').val($('#tempsubItemType').val());
		$('#itemNatureId').val($('#tempitemNatureId').val());
		$('#status').val($('#tempstatus').val()); 
		$('#productCategory').val($('#tempproductCategory').val());
		$('#costingType').val($('#tempCostingType').val());
		$('#reOrdering').val($('#tempreOrdering').val());

		if($.trim($('#tempitemTypeId').val()) == 'E'){
			$('#inventoryDiv').hide();
			$('#expenseDiv').show();
		}else{
			$('#inventoryDiv').show();
			$('#expenseDiv').hide();
		}
		
		if($('#specialPos').attr('checked') == true){
			$('.categoryspan').hide();
			$('#productCategory').removeClass('validate[required]');
		}
			
		
		var maxLeadTime = Number($('#maxLeadTime').val());
		var maxUsage = Number($('#maxUsage').val());
		var minLeadTime = Number($('#minLeadTime').val());
		if(maxUsage > 0 && maxLeadTime > 0) {
			$('#reOrderLevel').val(Number(maxUsage * maxLeadTime));
			if(minLeadTime > 0) {
				$('#avgLeadTime').val(Number(maxLeadTime + minLeadTime) / 2);
 			} 
		} 
 		var minUsage = Number($('#minUsage').val()); 
 		if(maxUsage > 0 && minUsage > 0) { 
			$('#avgUsage').val(Number(maxUsage + minUsage) / 2);  
		}  
		callMinReOrderLevel();
	} 


	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	
	$('#itemTypeId').change(function(){
		$('#productCode').text(''); 
		var itemType = $('#itemTypeId').val();
		$('#inventoryAccountCode').text(''); 
		$('#inventoryAccount').val(''); 
		$('#expenseAccountCode').text(''); 
		$('#expenseAccount').val(''); 
		if(itemType!=null && itemType!=''){ 
			$('#subItemType').html('');
			$('#subItemType')
				.append(
						'<option value="">Select</option>');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_item_subtype.action", 
				data:{itemType :itemType},
			 	async: false, 
			    dataType: "json",
			    cache: false,
				success:function(response){  
 					var itemTypes = response.itemSubTypes;
 					for (var prop in itemTypes) { 
					  if (itemTypes.hasOwnProperty(prop)) 
						  $('#subItemType')
							.append(
									'<option value='+prop+'>'+itemTypes[prop]+'</option>'); 
					} 
 					if(itemType == 'E'){
 						$('#inventoryDiv').hide(); 
 						$('#expenseDiv').show(); 
 					}
 					else{
 						$('#expenseDiv').hide(); 
 						$('#inventoryDiv').show(); 
 					}
				} 
			});
		}
	});
	
	$('#productCode').change(function(){
		var prdcode = $.trim($('#productCode').val());
		var tempPrdcode = $.trim($('#tempproductCode').val());
		if(prdcode == '')
			$('#usercode').val('false');
		else{
			$('#usercode').val('true');
			$("#productBarCode").val(prdcode);
		}  
		$('#productCode').removeClass('validate[required,custom[prdcode]]').addClass('validate[required]'); 
		$('.prrow').each(function(){   
			 var code = $.trim($(this).text()); 
			 if(code == prdcode && tempPrdcode != code){
				 $('#productCode').removeClass('validate[required]').addClass('validate[required,custom[prdcode]]');  
				 return false;
			 } 
 		 }); 
	});

	$('#subItemType').change(function(){
		var usercode = $('#usercode').val();
		if(usercode == 'false'){
			if($(this).val()!=null && $(this).val()!=''){
	 			if($(this).val() != Number($("#tempsubItemType").val())){
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/getproduct_code.action", 
						data:{subItemType: $(this).val()},
					 	async: false, 
					    dataType: "json",
					    cache: false,
						success:function(response){  
	 				 		$("#productCode").val(response.productCode); 
	 				 		$("#productBarCode").val(response.productCode); 
						} 
					});
				}else{
					$("#productCode").text($("#tempproductCode").val()); 
	 			}
			}else{
				$("#productCode").text(''); 
				$("#productBarCode").val(''); 
			}
		} 
	});

	$('#maxLeadTime').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			var maxLeadTime = Number($('#maxLeadTime').val());
			var maxUsage = Number($('#maxUsage').val());
			var minLeadTime = Number($('#minLeadTime').val());
			if(maxUsage > 0 && maxLeadTime > 0) {
				$('#reOrderLevel').val(Number(maxUsage * maxLeadTime));
				if(minLeadTime > 0) {
					$('#avgLeadTime').val(Number(maxLeadTime + minLeadTime) / 2);
					callMinReOrderLevel(); 
				} 
			} 
		}
	});

	$('#minLeadTime').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			var minLeadTime = Number($('#minLeadTime').val());
			var maxLeadTime = Number($('#maxLeadTime').val());
 			if(minLeadTime > 0 && maxLeadTime > 0) {
				$('#avgLeadTime').val(Number(maxLeadTime + minLeadTime) / 2);
				var maxUsage = Number($('#maxUsage').val());
				if(maxUsage > 0) {
					$('#reOrderLevel').val(Number(maxUsage * maxLeadTime)); 
					callMinReOrderLevel();  
				} 
			} 
		}
	});
	
	$('.product-unitlookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	$('.product-lookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
		if(accessCode=="ITEM_NATURE"){
			$('#itemNatureId').html("");
			$('#itemNatureId').append("<option value=''>Select</option>");
			loadLookupList("itemNatureId"); 
		} else if(accessCode=="PRODUCT_UNITNAME"){
			$('#unitId').html("");
			$('#unitId').append("<option value=''>Select</option>");
			loadLookupList("unitId"); 
		}   
	});

	$('#maxUsage').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			var maxUsage = Number($('#maxUsage').val());
			var minUsage = Number($('#minUsage').val()); 
			var maxLeadTime = Number($('#maxLeadTime').val());
			if(maxUsage > 0 && minUsage > 0) { 
				$('#avgUsage').val(Number(maxUsage + minUsage) / 2); 
				if(maxLeadTime > 0) {
					$('#reOrderLevel').val(Number(maxUsage * maxLeadTime)); 
					callMinReOrderLevel(); 
				} 
			}  
		}
	});

	$('#minUsage').change(function(){
		if($(this).val()!=null && $(this).val()!=''){
			var maxUsage = Number($('#maxUsage').val());
			var minUsage = Number($('#minUsage').val()); 
			var maxLeadTime = Number($('#maxLeadTime').val());
			if(maxUsage > 0 && minUsage > 0) { 
				$('#avgUsage').val(Number(maxUsage + minUsage) / 2);
				if(maxLeadTime > 0) {
					$('#reOrderLevel').val(Number(maxUsage * maxLeadTime)); 
					callMinReOrderLevel(); 
				}  
			} 
		}
	});

	$('#reOrderQuantity').change(function(){
		var maxLeadTime = Number($('#maxLeadTime').val());
		var maxUsage = Number($('#maxUsage').val());
		if(maxLeadTime > 0) {
			$('#reOrderLevel').val(Number(maxUsage * maxLeadTime)); 
			callMinReOrderLevel(); 
		} 
	});

	 $('#coaaccount-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });  

     $jquery("#addProductValidation").validationEngine('attach'); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,  
			width:800,  
			bgiframe: false,
			modal: true 
		});

     $('.codecombination-popup').click(function(){  
		 $('.ui-dialog-titlebar').remove();  
 		 slidetab = $(this).attr('id');
         $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/combination_treeview.action",
              async: false, 
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.codecombination-result').html(result);
              },
              error:function(result){
                   $('.codecombination-result').html(result);
              }
          });
	 });

     $('.treecancel').click(function(){ 
 		$('#codecombination-popup').dialog('close');
 	 });
     
     $('.product-store-popup').live('click',function(){  
 		$('.ui-dialog-titlebar').remove();  
 		var rowId = Number($(this).attr('id'));
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/show_store_session.action", 
 		 	async: false,  
 		 	data:{rowId : rowId},
 		    dataType: "html",
 		    cache: false,
 			success:function(result){   
 				$('.store-result').html(result);  
 			},
 			error:function(result){  
 				 $('.store-result').html(result); 
 			}
 		});  
 		return false;
 	});

 	$('#store-popup').dialog({
 		 autoOpen: false,
 		 minwidth: 'auto',
 		 width:800, 
 		 bgiframe: false,
 		 overflow:'hidden',
 		 zIndex:100000,
 		 modal: true 
 	}); 
	
	$('.discard').click(function(){
		$('.formError').remove();	
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/product_retrieve.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#store-popup').dialog('destroy');		
				$('#store-popup').remove();  
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$(
				'#codecombination-popup')
				.dialog(
						'destroy');
		$(
				'#codecombination-popup')
				.remove();
		 		$("#main-wrapper").html(result); 
			}
		});
	});

	 var keyPressed = false; 
     var chars = [];  
     $(window).keypress(function (e) {
         if (e.which >= 48 && e.which <= 57) {
             chars.push(String.fromCharCode(e.which));
         } 
         if (keyPressed == false) {  
             setTimeout(function(){   
                 if (chars.length >= 10) { 
                     var productBarCode = chars.join(""); 
                     $("#productBarCode").val(productBarCode); 
                     } 
                 chars = [];
                 keyPressed = false; 
              },500);
         }
         keyPressed = true;  
       }); 
	
	$('.save').click(function(){ 
		var id=$('.save').attr('id'); 
		if($jquery("#addProductValidation").validationEngine('validate')){
 			var productId=Number($('#productId').val());
			var productName=$('#productName').val();
			var itemType=$('#itemTypeId').val();
			var itemNature=Number($('#itemNatureId').val());
			var status=$('#status').val();
			var unitId=$('#unitId').val();
			var productCode=$.trim($('#productCode').val());
			var inventoryAccount=Number($('#inventoryAccount').val());
			var expenseAccount=Number($('#expenseAccount').val());
 			var productCategory=Number($('#productCategory').val());
			var costingType =  Number($('#costingType').val());
			var subItemType = Number($('#subItemType').val());
			var reOrdering = $('#reOrdering').val();
			var maxLeadTime = Number($('#maxLeadTime').val());
			var minLeadTime = Number($('#minLeadTime').val());
			var maxUsage =  Number($('#maxUsage').val());
			var minUsage = Number($('#minUsage').val());
			var reOrderQuantity = Number($('#reOrderQuantity').val());
			var fromHours = $('#fromHours').val();
			var toHours = $('#toHours').val();
			var barCode = $('#productBarCode').val();
			var shelfId = Number($('#shelfDefaultId').val());
			var specialPos =  $('#specialPos').attr('checked');
			var publicName = $('#publicName').val();  
			var userCode = $('#usercode').val();
			var description = $('#description').val();
			var hsCode = $('#hsCode').val(); 
			if(id=="add"){
				url_action="product_save";
			 }
			 else{
				url_action="product_update";
			} 
			 if(status==1){
				 status=true;
			 }
			 else{
				 status=false;
			 }
			 if(inventoryAccount>0 || expenseAccount>0){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/"+url_action+".action",  
					 	async: false, 
					 	data:{	productId: productId, productName: productName, itemType: itemType, itemNature:itemNature, costingType: costingType, 
						 	    unitId: unitId, inventoryAccount: inventoryAccount, status: status,  publicName: publicName, specialPos: specialPos,
						 	    productCode: productCode, productCategory: productCategory, expenseAccount: expenseAccount, barCode: barCode, userCode: userCode,
						 	    subItemType: subItemType, reOrdering: reOrdering, maxLeadTime: maxLeadTime, minLeadTime: minLeadTime, shelfId: shelfId,
						 	    maxUsage: maxUsage, minUsage: minUsage, reOrderQuantity: reOrderQuantity, fromHours: fromHours, 
						 	    toHours: toHours, description: description,hsCode:hsCode
						 	},
					    dataType: "html",
					    cache: false,
						success:function(result){ 
					 		$(".tempresult").html(result); 
							 var message=$.trim($('#returnMsg').html()); 
							 if(message=="Success"){
								 var screenMessage = productId > 0 ? "Record updated." :  "Record created.";
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/product_retrieve.action",
																	async : false,
																	dataType : "html",
																	cache : false,
																	success : function(
																			result) {
																		$('#store-popup').dialog('destroy');		
																		$('#store-popup').remove();
																		$(
																				'#common-popup')
																				.dialog(
																						'destroy');
																		$(
																				'#common-popup')
																				.remove();
																		$(
																		'#codecombination-popup')
																		.dialog(
																				'destroy');
																$(
																		'#codecombination-popup')
																		.remove();
																		$(
																				"#main-wrapper")
																				.html(
																						result);
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						screenMessage)
																				.slideDown(
																						1000);
																		$('#success_message').delay(2000).slideUp();
																	}
																});
													} else {
														$('#product-error')
																.hide()
																.html(message)
																.slideDown(1000);
														$('#product-error').delay(2000).slideUp();
														return false;
													}
												}
											});
								} else {
									$('#product-error').hide().html(
											"Combination should be selected.")
											.slideDown(1000);
									$('#product-error').delay(2000).slideUp();
									return false;
								}
							} else {
								return false;
							}
						});

		var productcom = $('#productCombination').text().replace(/[\s\n\r]+/g,
				' ');
		$('#productCombination').text(productcom);

		 $('#specialPos').change(function(){ 
				if($(this).attr('checked') == true){ 
					$('.categoryspan').hide();
					$('#productCategory').removeClass('validate[required]');
				}else{ 
					$('.categoryspan').show();
					$('#productCategory').addClass('validate[required]');
				}
			 }); 
		 $('#product_image_information').click(function(){
			 var productId= $('#productId').val();
				if(productId>0){
					AIOS_Uploader.openFileUploader("img","productImages", "Product", productId,"ProductImages");
				}else{
					AIOS_Uploader.openFileUploader("img","productImages","Product","-1","ProductImages");
				}
			});
		 if($('#productId').val() != null && $('#productId').val() != "") {
			 
			 $("#history_tabs").show();
			 getProductSales();
			 getProductPurchases();
			 populateUploadsPane("img","productImages","Product",$('#productId').val());
		 }
		 
		 $("#history_tabs").tabs({
			  selected: 0
		});		
	});
	
	function commonCOAPopup(aaData) { 
		if (aaData[3] == Number(1)) {
			$('#inventoryAccount').val(aaData[0]);
			$('#inventoryAccountCode').text(aaData[1]+"["+aaData[2]+"]");
		} else if (aaData[3] == Number(4)) {
			$('#expenseAccount').val(aaData[0]);
			$('#expenseAccountCode').text(aaData[1]+"["+aaData[2]+"]");
		} else if (aaData[3] == Number(3)) {
			$('#revenueAccount').val(aaData[0]);
			$('#revenueAccountCode').text(aaData[1]+"["+aaData[2]+"]");
		}
		return false;
	}

	function callMinReOrderLevel() {
		var avgUsage = Number($('#avgUsage').val());
		var avgLeadTime = Number($('#avgLeadTime').val());
		var reOrderLevel = Number($('#reOrderLevel').val());
		if (avgUsage > 0 && avgLeadTime > 0 && reOrderLevel > 0) {
			var minReOrderLevel = reOrderLevel - (avgUsage - avgLeadTime);
			$('#minLevels').val(minReOrderLevel);
			callMaxReOrderLevel();
		}
	}

	function callMaxReOrderLevel() {
		var minUsage = Number($('#minUsage').val());
		var minLeadTime = Number($('#minLeadTime').val());
		var reOrderQuantity = Number($('#reOrderQuantity').val());
		var reOrderLevel = Number($('#reOrderLevel').val());
		if (minUsage > 0 && minLeadTime > 0 && reOrderQuantity > 0) {
			var maxReOrderLevel = reOrderLevel + reOrderQuantity
					- (minUsage * minLeadTime);
			$('#maxLevels').val(maxReOrderLevel);
		}
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	
	function setCombination(combinationTreeId,combinationTree){
		if(slidetab == "expense"){
			$('#expenseAccount').val(combinationTreeId);
			$('#expenseAccountCode').text(combinationTree); 
		}else{
			$('#inventoryAccount').val(combinationTreeId);
			$('#inventoryAccountCode').text(combinationTree); 
		} 
	}
	
	function getProductSales() {
		
		var productId=Number($('#productId').val());
		
		if(productId == null || productId == 0 || productId == "") {
			return false;
		}
		
		 oTable = $('#ProductSalesHistory').dataTable({ 
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",
				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : true,
				"bLengthChange" : true,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				/* "sScrollY": $("#main-content").height() , */
				"sAjaxSource" : "get_product_sale_history.action?productId=" + productId,

				"aoColumns" : [ {
		    	    "mDataProp" : "salesReferenceNumber"
		   		 },{
					"mDataProp" : "customerNameAndNumber"
				 }
		   		/* <c:if test="${sessionScope.unified_price eq 'false'}">
		   		 ,{
					"mDataProp" : "storeName"
				 }</c:if> */
		   		 ,  {
					"mDataProp" : "description"
				 }, {
					"mDataProp" : "deliveredQuantity"
				 }, {
					"mDataProp" : "unitRate"
				 }, {
					"mDataProp" : "salesPersonName"
				 }, {
					"mDataProp" : "total"
				 }, {
					"mDataProp" : "status"
				 }, {
					"mDataProp" : "isInvoiced"
				 } ], 
			});	 
	}
	
	function getProductPurchases() {
		
		var productId=Number($('#productId').val());
		
		if(productId == null || productId == 0 || productId == "") {
			return false;
		}
		
		 oTable = $('#ProductPurchasesHistory').dataTable({ 
				"bJQueryUI" : true,
				"sPaginationType" : "full_numbers",
				"bFilter" : true,
				"bInfo" : true,
				"bSortClasses" : true,
				"bLengthChange" : true,
				"bProcessing" : true,
				"bDestroy" : true,
				"iDisplayLength" : 10,
				/* "sScrollY": $("#main-content").height() , */
				"sAjaxSource" : "get_product_purchase_history.action?productId=" + productId,

				"aoColumns" : [ {
		    	    "mDataProp" : "purchaseOrderReferenceNumber"
		   		 },{
					"mDataProp" : "supplierNameAndNumber"
				 }
		   		/* <c:if test="${sessionScope.unified_price eq 'false'}">
		   		 ,{
					"mDataProp" : "storeName"
				 }</c:if> */
		   		 ,  {
					"mDataProp" : "description"
				 }, {
					"mDataProp" : "receivedQty"
				 }, {
					"mDataProp" : "unitRate"
				 }, {
					"mDataProp" : "totalRate"
				 }, {
					"mDataProp" : "deliveryDate"
				 } , {
					"mDataProp" : "receiveDate"
				 }], 
			});	 
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="height: 95% !important;">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Product
		</div> 
		<form name="addproductform" id="addProductValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="product-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="productId" name="productId"
					value="${PRODUCT_INFO.productId}" />
				<input type="hidden" id="usercode" value="false"/>
				<div class="width100 float-left" id="hrm">
					<div class="float-left width35" id="hrm">
						<fieldset style="min-height: 281px;">
							<div>
								<label class="width30"> Product Code<span
									style="color: red;">*</span> </label> 
									 <input name="productCode"
									type="text" id="productCode"
									value="${PRODUCT_INFO.code}"
									class="validate[required] width60">
								 <input
									name="tempproductCode" type="hidden" id="tempproductCode"
									value="${PRODUCT_INFO.code}">
							</div> 
							<div>
								<label class="width30"> Product Name<span
									style="color: red;">*</span> </label> <input name="productName"
									type="text" id="productName"
									value="${PRODUCT_INFO.productName}"
									class="validate[required] width60">
							</div>
							<div>
								<label class="width30">Unit Name<span
									style="color: red;">*</span> </label> <select name="unitId"
									class="validate[required] width61" id="unitId">
									<option value="">Select</option>
									<c:choose>
										<c:when test="${UNITLIST ne null && UNITLIST ne ''}">
											<c:forEach items="${UNITLIST}" var="bean" varStatus="status">
												<option value="${bean.lookupDetailId}">${bean.displayName}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="tempunitId" name="tempunitId"
									value="${PRODUCT_INFO.lookupDetailByProductUnit.lookupDetailId}" />
								<span
										class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="PRODUCT_UNITNAME"
										class="btn ui-state-default ui-corner-all product-unitlookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30">Item type<span
									style="color: red;">*</span> </label> <select name="itemType"
									class="width61 validate[required]" id="itemTypeId">
									<option value="">Select</option>
									<c:forEach var="itemType" items="${ITEM_TYPES}">
										<option value="${itemType.key}">${itemType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempitemTypeId" name="tempitemType"
									value="${PRODUCT_INFO.itemType}" />
							</div>
							<div>
								<label class="width30">Sub type<span style="color: red;">*</span>
								</label> <select name="subItemType" class="width61 validate[required]"
									id="subItemType">
									<option value="">Select</option>
									<c:forEach var="itemType" items="${itemSubTypes}">
										<option value="${itemType.key}">${itemType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempsubItemType"
									name="tempsubItemType" value="${PRODUCT_INFO.itemSubType}" />
							</div>
							<div>
								<label class="width30">Costing Type<span
									style="color: red;">*</span> </label> <select name="costingType"
									class="width61 validate[required]" id="costingType">
									<option value="">Select</option>
									<c:forEach var="costingType" items="${COSTING_TYPES}">
										<option value="${costingType.key}">${costingType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCostingType"
									name="tempCostingType" value="${PRODUCT_INFO.costingType}" />
							</div>
							<div>
								<label class="width30"> Status<span style="color: red;">*</span>
								</label> <select name="status" name="status" id="status"
									class="width61 validate[required]">
									<option value="">Select</option>
									<option value="1" selected="selected">Active</option>
									<option value="0">In Active</option>
								</select>
								<c:choose>
									<c:when
										test="${PRODUCT_INFO.status eq true && PRODUCT_INFO.status ne false}">
										<input type="hidden" id="tempstatus" name="tempstatus"
											value="1" />
									</c:when>
									<c:when
										test="${PRODUCT_INFO.status ne true && PRODUCT_INFO.status eq false}">
										<input type="hidden" id="tempstatus" name="tempstatus"
											value="0" />
									</c:when>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Bar Code</label> <input
									name="productBarCode" type="text" id="productBarCode"
									value="${PRODUCT_INFO.barCode}" class="width60">
							</div> 
							<div>
								<label class="width30">HS Code</label> <input name="hsCode"
									type="text" id="hsCode"
									value="${PRODUCT_INFO.hsCode}"
									class=" width60">
							</div>
						</fieldset>
					</div>
					<div class="float-left  width35">
						<fieldset style="min-height: 281px;">
							<div style="height: 270px; overflow-y: auto;">
								<div id="inventoryDiv" 
									style="position: relative; top: 5px; min-height: 30px;">
									<label class="width30 float-left">Inventory<span
										style="color: red;">*</span> </label>
									<c:choose>
										<c:when
											test="${PRODUCT_INFO.combinationByInventoryAccount.combinationId > 0}">
											<span id="productCombination" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
												${PRODUCT_INFO.combinationByInventoryAccount.accountByAnalysisAccountId.account}
												[${PRODUCT_INFO.combinationByInventoryAccount.accountByAnalysisAccountId.code}] </span>
										</c:when>
										<c:otherwise>
											<span id="inventoryAccountCode" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
											</span>
										</c:otherwise>
									</c:choose>
									<span class="button width10 float-left"> <a
										style="cursor: pointer;" id="inventory"
										class="btn ui-state-default ui-corner-all codecombination-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" id="inventoryAccount" name="inventoryAccount"
										value="${PRODUCT_INFO.combinationByInventoryAccount.combinationId}" />
								</div> 
								<div id="expenseDiv" style="display: none;  
									position: relative; top: 5px; min-height: 30px;">
									<label class="width30 float-left">Expense<span
										style="color: red;">*</span> </label>
									<c:choose>
										<c:when
											test="${PRODUCT_INFO.combinationByExpenseAccount.combinationId > 0}">
											<span id="expenseCombination" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
												${PRODUCT_INFO.combinationByExpenseAccount.accountByAnalysisAccountId.account}
												[${PRODUCT_INFO.combinationByExpenseAccount.accountByAnalysisAccountId.code}] </span>
										</c:when>
										<c:otherwise>
											<span id="expenseAccountCode" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
											</span>
										</c:otherwise>
									</c:choose>
									<span class="button width10 float-left"> <a
										style="cursor: pointer;" id="expense"
										class="btn ui-state-default ui-corner-all codecombination-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" id="expenseAccount" name="expenseAccount"
										value="${PRODUCT_INFO.combinationByExpenseAccount.combinationId}" />
								</div>
								<div>
									<label class="width30">Category <span class="categoryspan"
											style="color: red;">*</span></label> <select name="productCategory"
										class="width51 validate[required]" id="productCategory">
										<option value="">Select</option>
										<c:forEach var="category" items="${PRODUCT_CATEGORY}">
											<optgroup label="${category.categoryName}"
												style="color: #c85f1f;">
												<c:if
													test="${category.subCategories ne null && category.subCategories ne ''}">
													<c:forEach var="subCategory"
														items="${category.subCategories}">
														<option style="margin-left: 8px; color: #000"
															value="${subCategory.productCategoryId}">${subCategory.categoryName}</option>
													</c:forEach>
												</c:if>
											</optgroup>
										</c:forEach>
									</select> <input type="hidden" id="tempproductCategory"
										name="tempproductCategory"
										value="${PRODUCT_INFO.productCategory.productCategoryId}" />
								</div>
								<div>
									<label class="width30">Item Nature </label> <select name="itemNature"
										class="width51" id="itemNatureId">
										<option value="">Select</option>
										<c:forEach var="itemNature" items="${ITEM_NATURE}">
											<option value="${itemNature.lookupDetailId}">${itemNature.displayName}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempitemNatureId"
										name="tempitemNature"
										value="${PRODUCT_INFO.lookupDetailByItemNature.lookupDetailId}" /> <span
										class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="ITEM_NATURE"
										class="btn ui-state-default ui-corner-all product-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30">Shelf</label>
									<input type="hidden" id="shelfDefaultId" name="shelfDefaultId" value="${PRODUCT_INFO.shelf.shelfId}"/>
									<c:choose>
										<c:when test="${PRODUCT_INFO.shelf ne null && PRODUCT_INFO.shelf ne ''}">
											<input type="text" readonly="readonly" class="width50" id="shelfName" name="shelfName" 
												value="${PRODUCT_INFO.storeName}" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" class="width50" id="shelfName" name="shelfName" />
										</c:otherwise>
									</c:choose>  
									<span class="button"> <a style="cursor: pointer;" 
									class="btn ui-state-default ui-corner-all product-store-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30">From Hours </label> <input type="text"
										readonly="readonly" class="width50" id="fromHours"
										name="fromHours" value="${PRODUCT_INFO.fromHours}" />
								</div>
								<div>
									<label class="width30">To Hours</label> <input type="text"
										readonly="readonly" class="width50" id="toHours"
										name="toHours" value="${PRODUCT_INFO.toHours}" />
								</div>
								<div>
									<label for="specialPos" class="width30">Special POS </label> 
									<c:choose>
										<c:when test="${PRODUCT_INFO.specialPos eq true}">
											<input type="checkbox" name="specialPos" id="specialPos" checked="checked"/>
										</c:when>
										<c:otherwise>
											<input type="checkbox" name="specialPos" id="specialPos"/>
										</c:otherwise>
									</c:choose> 
								</div>
								<div>
									<label for="publicName" class="width30">Public Name
									</label> <input type="text" name="publicName" id="publicName"
										value="${PRODUCT_INFO.publicName}" class="width50" />
								</div> 
								<c:if test="${PRODUCT_INFO.productPic ne null && PRODUCT_INFO.productPic ne ''}">
									<div>
										<label class="width30">Image</label>
										<span class="fileinput-preview">  
												<c:if test="${PRODUCT_INFO.productPic ne null && PRODUCT_INFO.productPic ne ''}">
													<img name="preview" id="preview" height="125" width="125"
														src="data:image/png;base64,${PRODUCT_INFO.productPic}">
												</c:if>
											</span>  
										</div> 
								</c:if>  
								<div>
									<label for="description" class="width30">Description
									</label> <textarea name="description" id="description" class="width50" >${PRODUCT_INFO.description}</textarea>
								</div> 
							</div> 
						</fieldset>
					</div>
					<div class="float-left width30">
						<fieldset style="min-height: 281px;">
							<div style="height: 280px; overflow-y: auto;">
								<div>
									<label class="width30">ReOrdering</label> <select
										name="reOrdering" id="reOrdering" class="width61">
										<option value="">Select</option>
										<option value="1" selected="selected">Open</option>
										<option value="0">Close</option>
									</select> <input type="hidden" value="${PRODUCT_INFO.reOdering}"
										id="tempreOrdering" />
								</div>
								<div>
									<label class="width30">Max Lead Time </label> <input
										name="maxLeadTime" type="text" id="maxLeadTime"
										value="${PRODUCT_INFO.maxLeadTime}" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Min Lead Time</label> <input
										name="minLeadTime" type="text" id="minLeadTime"
										value="${PRODUCT_INFO.minLeadTime}" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Avg Lead Time </label> <input
										name="avgLeadTime" type="text" id="avgLeadTime"
										readonly="readonly" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Max Usage</label> <input name="maxUsage"
										type="text" id="maxUsage" value="${PRODUCT_INFO.maxUsage}"
										class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Min Usage</label> <input name="minUsage"
										type="text" id="minUsage" value="${PRODUCT_INFO.minUsage}"
										class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Avg Usage</label> <input name="avgUsage"
										type="text" id="avgUsage" readonly="readonly" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">ReOrder Quantity</label> <input
										name="reOrderQuantity" type="text" id="reOrderQuantity"
										value="${PRODUCT_INFO.reOrderQuantity}" class="width60 validate[optional, custom[number]]">
								</div>
								<div>
									<label class="width30">ReOrder Level</label> <input
										name="reOrderLevel" readonly="readonly" type="text"
										id="reOrderLevel" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Min Levels</label> <input
										name="minLevels" type="text" id="minLevels"
										readonly="readonly" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
								<div>
									<label class="width30">Max Levels</label> <input
										name="maxLevels" readonly="readonly" type="text"
										id="maxLevels" class="width60 validate[optional, custom[onlyNumber]]">
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div style="display: none;">
				<c:forEach var="product" items="${PRODUCTS_LIST}" varStatus="status"> 
					<span class="prrow">${product.code}</span>
				</c:forEach>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="${showPage}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="width: auto; overflow: hidden;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
		<div id="history_tabs" style="display: none;">
			<ul>
				<li><a href="#sale">Sales</a></li>
				<li><a href="#purchase">Purchases</a></li>
				<li><a href="#UploadDmsDiv">Uploads</a></li>
			</ul>
			<div id="sale">
				<table id="ProductSalesHistory" class="display">
						<thead>
							<tr> 
								<th>Sale Ref.</th> 
								<th>Customer</th> 
								<%-- <c:if test="${sessionScope.unified_price eq 'false'}">
									<th>Store</th> 
								</c:if> --%>
								<th>Description</th>  
								<th>Quantity</th> 
								<th>Unit Rate</th> 
								<th>Salesperson</th> 
								<th>Subtotal</th> 
								<th>Status</th> 
								<th>Invoiced</th> 
							</tr>
						</thead>
				
					</table> 
			</div>
			<div id="purchase">				
				<table id="ProductPurchasesHistory" class="display">
					<thead>
						<tr> 
							<th>Order Ref.</th> 
							<th>Supplier</th> 
							<th>Description</th>  
							<th>Quantity</th> 
							<th>Unit Rate</th> 
							<th>Subtotal</th> 
							<th>Delivery Date</th>
							<th>Received Date</th>
						</tr>
					</thead>			
				</table> 
			</div>
			<!--#########################################  Tab 3 Documents ################################################### -->
			<div id="UploadDmsDiv" class="width50">
				
				<div>									
					
					<div id="product_image_information" style="cursor: pointer;">
						<u>Upload image here</u>
					</div>
					<div style="padding-top: 10px; margin-top:3px; ">
						<span id="productImages"></span>
					</div>
					
				</div>	
								
			</div>
		</div>
	</div>
</div>