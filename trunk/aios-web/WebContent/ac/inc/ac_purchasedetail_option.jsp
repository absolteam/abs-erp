<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach var="detail" items="${PURCHASE_DETAILINFO}"
	varStatus="status">
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
		<td id="itemtype_${status.index+1}"><select name="itemnature"
			class="width98 itemnature" id="itemnature_${status.index+1}">
				<option value="A">Asset</option>
				<option value="E">Expense</option>
				<option value="I" selected="selected">Inventory</option>
		</select> <input type="hidden" id="tempitemnature_${status.index+1}"
			name="itemnature" value="${detail.product.itemType}" /></td>
		<td><span class="float-left width60"
			id="product_${status.index+1}">${detail.product.code} -
				${detail.product.productName}</span> 
			<span class="width10 float-right" id="unitCode_${status.index+1}"
					style="position: relative;">${detail.product.lookupDetailByProductUnit.accessCode}</span>
			<input type="hidden"
			id="productid_${status.index+1}" value="${detail.product.productId}" />
		</td>
		<td><span class="float-left width60"
			id="storeName_${status.index+1}">${detail.storeName}</span> <input
			type="hidden" name="storeId" id="storeId_${status.index+1}"
			value="${detail.storeId}" /> <input type="hidden" name="shelfId"
			id="shelfId_${status.index+1}" value="${detail.shelfId}" /></td>
		<td id="uom_${status.index+1}" class="uom" style="display: none;"></td>
		<td> 
			<select name="packageType" id="packageType_${status.index+1}" class="packageType">
				<option value="-1">Select</option> 
				<c:forEach var="packageType" items="${detail.productPackageVOs}">
					<c:choose>
						<c:when test="${packageType.productPackageId gt 0}">
							<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
							 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
							 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
							 	</c:forEach> 
							 </optgroup>
						</c:when> 
					</c:choose> 
				</c:forEach>
			</select>  
			<input type="hidden" name="temppackageType" id="temppackageType_${status.index+1}" value="${detail.packageDetailId}"/>
		</td> 
		<td><input type="text" value="${detail.packageUnit}"
			class="width96 packageUnit validate[optional,custom[number]]" id="packageUnit_${status.index+1}"/> 
		</td>  
		<td> 
			<input type="hidden" class="width70 quantity validate[optional,custom[number]]"
				id="quantity_${status.index+1}" value="${detail.quantity}" />
			<span id="baseUnitConversion_${status.index+1}" class="width15" style="display: none;">${detail.productPackageDetailVO.conversionUnitName}</span>
			<span id="baseDisplayQty_${status.index+1}">${detail.baseQuantity}</span> 
		</td>
		<td><input type="text"
			class="width96 unitrate validate[optional,custom[number]]"
			id="unitrate_${status.index+1}" value="${detail.unitRate}"
			style="text-align: right;" />
			<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" value="${detail.unitRate * detail.packageUnit}"
				id="totalAmountH_${status.index+1}" class="totalAmountH"/> 
				<input type="hidden" id="basePurchasePrice_${status.index+1}"/>
		</td>
		<td class="totalamount" id="totalamount_${status.index+1}"
			style="text-align: right;"></td>
		<td><input type="text" name="linedescription"
			id="linedescription_${status.index+1}" value="${detail.description}" />
		</td>
		<td style="display: none;"><input type="hidden"
			id="purchaseLineId_${status.index+1}"
			value="${detail.purchaseDetailId}" /> <input type="hidden"
			id="requisitionLineId_${status.index+1}"
			value="${detail.requisitionDetailId}" /> <input type="text"
			value="${detail.batchNumber}" class="width96 batchNumber"
			id="batchNumber_${status.index+1}" /> <input type="text"
			readonly="readonly" value="${detail.expiryDate}"
			class="width96 expiryBatchDate"
			id="expiryBatchDate_${status.index+1}" />
		</td>
		<td>
			<div id="moreoption_${status.index+1}" style="cursor: pointer; text-align: center; background-color: #3ADF00;" 
				class="portlet-header ui-widget-header float-left moreoption">
				MORE OPTION
			</div>
		</td>
		<td style="width: 0.01%;" class="opn_td" id="option_${status.index+1}"><a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
			id="DeleteImage_${status.index+1}" style="cursor: pointer;"
			title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
		</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
			id="WorkingImage_${status.index+1}" style="display: none;"
			title="Working"> <span class="processing"></span> </a></td>
	</tr>
</c:forEach>
<c:forEach var="i" begin="${fn:length(PURCHASE_DETAILINFO)+1}"
	end="${fn:length(PURCHASE_DETAILINFO)+2}" step="1" varStatus="status1">
	<tr class="rowid" id="fieldrow_${i}">
		<td id="lineId_${i}" style="display: none;">${i}</td>
		<td><select name="itemnature" class="width98 itemnature"
			id="itemnature_${i}">
				<option value="A">Asset</option>
				<option value="E">Expense</option>
				<option value="I" selected="selected">Inventory</option>
		</select> <input type="hidden" id="tempitemnature_${i}" name="itemnature" /></td>
		<td><span class="float-left width60" id="product_${i}"></span> 
			<span class="width10 float-right" id="unitCode_${i}" style="position: relative;"></span>
			<span
			class="button float-right prod" style="height: 16px;"> <a
				style="cursor: pointer; cursor: pointer; height: 100%;"
				id="productline_${i}"
				class="btn ui-state-default ui-corner-all purchaseproduct-common-popup width100 float-right">
					<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
			type="hidden" id="productid_${i}" /></td>
		<td><span class="float-left width60" id="storeName_${i}"></span>
			<input type="hidden" name="storeId" id="storeId_${i}" value="" /> <input
			type="hidden" name="shelfId" id="shelfId_${i}" /> <span
			class="button float-right"> <a style="cursor: pointer;"
				id="prodIDStr_${i}"
				class="btn ui-state-default ui-corner-all purchase-store-popup width100">
					<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
		</td>
		<td id="uom_${i}" class="uom" style="display: none;"></td>
		<td>
			<select name="packageType" id="packageType_${i}" class="packageType">
				<option value="">Select</option>
			</select> 
			<input type="hidden" name="temppackageType" id="temppackageType_${i}"/> 
		</td> 
		<td><input type="text"
			class="width96 packageUnit validate[optional,custom[number]]" id="packageUnit_${i}"/> 
		</td>  
		<td><input type="hidden"
			class="width70 quantity validate[optional,custom[number]]"
			id="quantity_${i}" />
			<span id="baseUnitConversion_${i}" class="width15 float-right" style="display: none;"></span>
			<span id="baseDisplayQty_${i}"></span>
		</td>
		<td><input type="text"
			class="width96 unitrate validate[optional,custom[number]]"
			id="unitrate_${i}" style="text-align: right;" />
			<input type="hidden" id="basePurchasePrice_${i}"/>
			<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${i}" class="totalAmountH"/> 
		</td>
		<td class="totalamount" id="totalamount_${i}"
			style="text-align: right;"></td>
		<td><input type="text" name="linedescription"
			id="linedescription_${i}" /></td>
		<td style="display: none;"><input type="hidden"
			id="purchaseLineId_${i}" /> <input type="hidden"
			id="requisitionLineId_${i}" /> <input type="text"
			class="width96 batchNumber" id="batchNumber_${i}" /> <input
			type="text" readonly="readonly" class="width96 expiryBatchDate"
			id="expiryBatchDate_${i}" /></td>
		<td>
			<div id="moreoption_${i}"
				style="cursor: pointer; text-align: center; background-color: #3ADF00;"
				class="portlet-header ui-widget-header float-left moreoption">
				MORE OPTION</div>
		</td>
		<td style="width: 0.01%;" class="opn_td" id="option_${i}"><a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
			id="AddImage_${i}" style="display: none; cursor: pointer;"
			title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
			id="EditImage_${i}" style="display: none; cursor: pointer;"
			title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
		</a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
			id="DeleteImage_${i}" style="display: none; cursor: pointer;"
			title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
		</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
			id="WorkingImage_${i}" style="display: none;" title="Working"> <span
				class="processing"></span> </a></td>
	</tr>
</c:forEach>
<script type="text/javascript">
	$(function() { 
		$jquery(".unitrate,.totalAmountH").number(true,3); 
		var totalPurchase = 0;
		setTimeout(function() { 
			manupulateOptionLastRow();
	    }, 100); 
		$('.rowid').each(function(){   
			var rowId = getRowId($(this).attr('id'));
 			$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val()); 
 			$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
 			$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val()); 
 			var totalamount = Number($jquery('#totalAmountH_'+rowId).val());
  	 		totalPurchase = Number(totalPurchase+totalamount);
		});
		$jquery('.totalPurchaseAmountH').val(totalPurchase);
		$('#totalPurchaseAmount').text($('.totalPurchaseAmountH').val());
	});

	function manupulateOptionLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
</script>