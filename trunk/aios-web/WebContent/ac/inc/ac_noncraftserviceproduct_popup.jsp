<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 var oTable; var selectRow=""; var aSelected = []; var aData="";
	$(function() {
		oTable = $('#ProductNonCraftList')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_noncraftservice_product_jsonlist.action",

							"aoColumns" : [ {
								"mDataProp" : "code"
							}, {
								"mDataProp" : "productName"
							}, {
								"mDataProp" : "unitName"
							}, {
								"mDataProp" : "costingMethod"
							} ]
						});

		/* Click event handler */
		$('#ProductNonCraftList tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					nonCraftProductPopup(aData,$(
					'#currentRowId').val());
					$('#noncraft-product-popup').trigger('click');
					return false;
				});

		/* Click event handler */
		$('#ProductNonCraftList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
		
		$('#noncraft-product-popup').click(function () { 
			$('#common-popup').dialog("close");
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
		</div>
		<div class="portlet-content">
			<div id="product_json_list">
				<table id="ProductNonCraftList" class="display">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th>
							<th>Unit Type</th>
							<th>Costing Method</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="noncraft-product-popup">close</div>
			</div>
			<input type="hidden" id="currentRowId" value="${requestScope.rowId}" />
			<input type="hidden" id="productItemType"
				value="${requestScope.itemType}" />
		</div> 
	</div>
</div>