<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var pointOfSaleId = 0;
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		$('.formError').remove();
		var pointOfSaleId = Number($('#pointOfSaleId').val());
		var salesType = Number($('#salesType').val());
		var salesDate = $('#salesDate').val();
		var customerId = Number$($('#customerId').val());
		oTable = $('#example')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_all_point_of_sales.action?pointOfSaleId="
									+ pointOfSaleId
									+ "&salesType="
									+ salesType
									+ "&salesDate="
									+ salesDate
									+ "&customerId=" + customerId,

							"aoColumns" : [ {
								"mDataProp" : "referenceNumber"
							}, {
								"mDataProp" : "salesType"
							}, {
								"mDataProp" : "date"
							}, {
								"mDataProp" : "customerName"
							} ],
						});

		/* Click event handler */
		$('#example tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				pointOfSaleId = aData.pointOfSaleId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				pointOfSaleId = aData.pointOfSaleId;
			}
		});

		/* Click event handler */
		$('#example tbody tr').live('dblclick', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				pointOfSaleId = aData.pointOfSaleId;
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
				aData = oTable.fnGetData(this);
				pointOfSaleId = aData.pointOfSaleId;
			}
			pointofSaleDetails(pointOfSaleId, aData.referenceNumber);
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>point
			of sale
		</div>
		<div class="portlet-content">
			<div id="rightclickarea">
				<div id="pos_list">
					<table id="example" class="display">
						<thead>
							<tr>
								<th>POS Reference</th>
								<th>Sales Type</th>
								<th>Sales Date</th>
								<th>Customer Name</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div class="portlet-header ui-widget-header float-right close-pos"
			id="close-pos">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>