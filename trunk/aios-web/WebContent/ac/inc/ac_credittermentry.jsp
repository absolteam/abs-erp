<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accountTypeId = 0;
var combinationType ="";
$(function(){
	 
	$jquery("#creditterm_details").validationEngine('attach'); 
	$('#termName').focus(); 
	if(Number($('#creditTermId').val()) > 0){
		$('#discountMode').val($('#tempDiscountMode').val());
		$('#penaltyMode').val($('#tempPenaltyMode').val());
		$('#penaltyType').val($('#tempPenaltyType').val());
		$('#dueDate').val($('#tempDueDate').val()); 
		var isSuppleirFlag = '${CREDIT_TERM_INFO.isSupplier}';
 		if(isSuppleirFlag == "false"){
			$('#customerChecked').attr('checked', true); 
		} else if(isSuppleirFlag == "true"){ 
			$('#supplierChecked').attr('checked', true);
		}
	} 

     $('.credit_termdiscard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_all_creditterms.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
	 });

     $('.credit_termsave').click(function(){ 
    	 if($jquery("#creditterm_details").validationEngine('validate')){
			 var creditTermId=Number($('#creditTermId').val());
			 var name = $('#termName').val();
 			 var dueDate = Number($('#dueDate').val());
			 var discountDay = Number($('#discountDay').val());
			 var discount = Number($('#discount').val());
			 var discountMode = Number($('#discountMode').val());
 			 var penaltyType = Number($('#penaltyType').val());
			 var penaltyMode = Number($('#penaltyMode').val());
			 var penalty =  Number($('#penalty').val());
	 		 var isSupplier = $('#supplierChecked').attr('checked');
	 		 var description = $('#description').val();
  			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/savecreditterm.action", 
				 	async: false,
				 	data:{	creditTermId: creditTermId , name: name, dueDay: dueDate, discountDay: discountDay, discount:discount, penalty: penalty, 
				 			discountMode: discountMode, penaltyType: penaltyType, penaltyMode: penaltyMode,isSupplier: isSupplier, description: description
				 		},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tempresult").html(result); 
						var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/show_all_creditterms.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$("#main-wrapper").html(result); 
										if(creditTermId > 0) 
											$('#success_message').hide().html("Record updated").slideDown(1000);
										else
											$('#success_message').hide().html("Record created.").slideDown(1000);
										$('#success_message').delay(2000).slideUp();
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(message).slideDown();
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error.").slideDown();
						$('#page-error').delay(2000).slideUp();
					}
			 });
		 }
		 else{
			 return false;}
	 });
});
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>credit term</div>	
		  <form name="creditterm_details" id="creditterm_details" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height: 170px;"> 
							<div>
								<label class="width30" for="penaltyType"><fmt:message key="accounts.creditterm.credittermpenaltytype"/></label>
								<select name="penaltyType" id="penaltyType" class="width51">
									<option value="">Select</option>
									<c:forEach var="penaltyType" items="${PENALTY_TYPE}">
										<option value="${penaltyType.key}">${penaltyType.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempPenaltyType" value="${CREDIT_TERM_INFO.penaltyType}"/> 
							</div> 
							<div>
								<label class="width30" for="penaltyMode"><fmt:message key="accounts.creditterm.credittermpenaltymode"/></label>
								<select name="penaltyMode" id="penaltyMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="penaltyMode" items="${PAYMENT_MODE}">
										<option value="${penaltyMode.key}">${penaltyMode.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempPenaltyMode" value="${CREDIT_TERM_INFO.penaltyMode}"/>
							</div> 
							<div>
								<label class="width30" for="penalty"><fmt:message key="accounts.creditterm.credittermpenalty"/></label> 
								<input type="text" name="penalty" id="penalty" maxlength="3" class="width50 validate[optional,custom[number]]" value="${CREDIT_TERM_INFO.penalty}"/>
							</div> 
							<div>
								<label class="width30" for="description">Description</label> 
								<textarea class="width51" id="description">${CREDIT_TERM_INFO.description}</textarea>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height: 170px;"> 
							<div>
								<label class="width30" for="termName"><fmt:message key="accounts.creditterm.credittermname"/><span style="color: red;">*</span></label> 
								<input type="hidden" id="creditTermId" name="creditTermId" value="${CREDIT_TERM_INFO.creditTermId}"/>
								<input type="hidden" id="termTempName" value="${CREDIT_TERM_INFO.name}"/>
								<input type="text" name="termName" id="termName" maxlength="20" class="width50 validate[required]" value="${CREDIT_TERM_INFO.name}"/>
							</div> 
							<div>
								<label class="width30" for="dueDate">Due Date</label> 
								<select name="dueDate" id="dueDate" class="width51">
									<option value="">Select</option>
									<c:forEach var="dueDate" items="${CREDIT_TERM_BASEDAY}">
										<option value="${dueDate.key}">${dueDate.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempDueDate" value="${CREDIT_TERM_INFO.dueDay}"/>
							</div> 
							<div>
								<label class="width30" for="discountDay">Due Days</label> 
								<input type="text" name="discountDay" id="discountDay" class="width50 validate[optional,custom[number]]" value="${CREDIT_TERM_INFO.discountDay}"/>
							</div> 
							<div>
								<label class="width30" for="discountMode"><fmt:message key="accounts.creditterm.credittermdiscountmode"/></label> 
								<select name="discountMode" id="discountMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="discountMode" items="${PAYMENT_MODE}">
										<option value="${discountMode.key}">${discountMode.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempDiscountMode" value="${CREDIT_TERM_INFO.discountMode}"/>
							</div> 
							<div>
								<label class="width30" for="discount"><fmt:message key="accounts.creditterm.credittermdiscount"/></label> 
								<input type="text" name="discount" id="discount" class="width50 validate[optional,custom[number]]" value="${CREDIT_TERM_INFO.discount}"/>
							</div> 
							<div>
								<label class="width30">Discount For</label>
								<div class="float-left width20">
									<input type="radio" id="customerChecked" name="discountfor" class="float-left validate[required]">
									<label for="customerChecked" class="width70">Customer</label>   
								</div>
								<div class="float-left width30">
									<input type="radio" id="supplierChecked" name="discountfor" class="float-left validate[required]">
									<label for="supplierChecked" class="width40">Supplier</label>  
								</div>
							</div>
						</fieldset>
					</div> 
			</div>  
		</div>    
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right credit_termdiscard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right credit_termsave" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
  </form>
  </div> 
</div>