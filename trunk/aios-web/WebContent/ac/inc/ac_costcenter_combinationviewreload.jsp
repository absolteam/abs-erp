<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){ 
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	}); 
});
</script>
<c:choose>
	<c:when
		test="${COMBINATION_TREE_VIEW ne null && COMBINATION_TREE_VIEW ne ''}">
		<ul id="tree">
			<li><a style="font-weight: bold;" class="createcombi">Combination</a>
				<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}"
					varStatus="status">
					<li><a id="company_${status.index+1}_${status.index+1}"
						class="combination_ companys"> <input type="hidden"
							id="companyId_${status.index+1}_${status.index+1}"
							value="${bean.combinationId}" />
							${bean.accountByCompanyAccountId.account}
							[${bean.accountByCompanyAccountId.code}]</a> <c:choose>
							<c:when
								test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
								<ul>
									<c:forEach var="costbean" items="${bean.costVos}"
										varStatus="cstatus">
										<li><a class="combination_ costscenter"
											id="costcenter_${status.index+1}_${cstatus.index+1}"> <input
												type="hidden"
												id="costcenterId_${status.index+1}_${cstatus.index+1}"
												value="${costbean.combinationId}" />
												${costbean.accountByCostcenterAccountId.account}
												[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
										</a></li>
									</c:forEach>
								</ul>
							</c:when>
						</c:choose></li>
				</c:forEach></li>
		</ul>
	</c:when>
</c:choose>