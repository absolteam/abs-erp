<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowidcharges" id="fieldrowcharges_${requestScope.rowId}">
	<td id="chargesLineId_${requestScope.rowId}" style="display: none;">${requestScope.rowId}</td>
	<td><select name="chargesTypeId"
		id="chargesTypeId_${requestScope.rowId}" class="chargestypeid width90">
			<option value="">Select</option>
			<c:forEach var="TYPE" items="${CHARGES_TYPE}">
				<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
			</c:forEach>
	</select></td>
	<td><input type="text" name="charges"
		id="charges_${requestScope.rowId}" class="charges width98"
		style="text-align: right;">
	</td>
	<td><input type="text" name="description"
		id="description_${requestScope.rowId}" class="description width98">
	</td>
	<td style="width: 1%;" class="opn_td"
		id="optioncharges_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
		id="AddImagecharges_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
		id="EditImagecharges_${requestScope.rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
		id="DeleteImagecharges_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImagecharges_${requestScope.rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="posChargeId"
		id="posChargeId_${requestScope.rowId}" />
	</td>
</tr>
<script type="text/javascript">
$(function() {
	$jquery('.charges').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['7|8|9|' + $jquery.keypad.BACK, 
	        '4|5|6|' + $jquery.keypad.CLEAR, 
	        '1|2|3|' + $jquery.keypad.CLOSE, 
	        '.|0|00']}); 
}); 	
</script>