<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
	.hr_main_content {
		width: 100% !important;
		overflow: hidden;
	}
</style> 
<script type="text/javascript"> 
var slidetab="";
var rowId="";
var accessCode = "";
$(function (){ 
	manupulateLastRow();
	if(Number($('#customerId').val()>0)){
		//$('.savediscard').show();
		$('#customerCompany').attr('disabled',true);
		$('#customerBalance').attr('disabled',true);
		$('#customerPerson').attr('disabled',true); 
		
		if(Number($('#cmpDeptLocCustomerId').val())>0){ 
			$('#customerCompany').attr('checked',true);
			showCompany($('#companyCustomerId').val());
		}else{
			$('#customerPerson').attr('checked',true);
			showPerson($('#customerPersonId').val());
		}
		
		$('#memberCarType').val($('#tempmemberType').val());  
		$('#creditTermId').val($('#tempterm').val());  
		$('#customerAcType').val($('#tempcustomertype').val());
		$('#shippingMethod').val($('#tempshippingMethod').val());
		$('#refferalSource').val($('#temprefferalSource').val());
	} 
	 
	$jquery("#customer_details").validationEngine('attach'); 
	$('.refferalsource-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	$('.shippingmethod-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="REFFERAL_SOURCE"){
			$('#refferalSource').html("");
			$('#refferalSource').append("<option value=''>Select</option>");
			loadLookupList("refferalSource"); 
		} else if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingMethod').html("");
			$('#shippingMethod').append("<option value=''>Select</option>");
			loadLookupList("shippingMethod"); 
		}  
	});
	  
	$('#customerCompany').click(function() { 
		$('#customerCompany').attr('disabled', true);
		$('#customerPerson').attr('disabled', true); 
		$('#company-selection-div').show();
		$('#person-selection-div').hide();
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/add_company.action",
			data: {companyId : 0, isCustomer: true, isSupplier: false}, 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result);
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		identityShowHideCall(false);
		$('#companyTypeIdTemp').val("12,"+$('#companyTypeIdTemp').val());
		companyTypeAutoSelectd(); 
		$("#customer-result-div").html($("#customer-data-div").html());
		$("#customer-data-div").html("");
		$('#companyTypeId').attr('disabled', true);
		$('#customerinfo').val(true);
		 
	});
	
	$('#customerPerson').click(function() { 
		$('#customerPerson').attr('disabled', true);
		$('#customerCompany').attr('disabled', true); 
		$('#company-selection-div').hide();
		$('#person-selection-div').show();
		$('#loading').fadeIn();
		var personId = 0;
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
			data: {personId: personId, isCustomer: true, isSupplier: false},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result); 
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		
		identityShowHideCall(false);
		dependentShowHideCall(false);
		$('#personTypeIdTemp').val("8,"+$('#personTypeIdTemp').val());
		personTypeAutoSelectd();
		$('#customerinfo').val(true);
		$("#customer-result-div").html($("#customer-data-div").html());
		$("#customer-data-div").html("");
		$('#personType').attr('disabled', true);
 
	});

	 
	
	//pop-up config
	    $('#employeecall').click(function(){
	          $('.ui-dialog-titlebar').remove();  
	          $('#common-popup').dialog('open');
	          var rowId=-1; 
	          var personTypes="ALL";
	             $.ajax({
	                type:"POST",
	                url:"<%=request.getContextPath()%>/common_person_list.action",
	                data:{id:rowId,personTypes:personTypes},
	                async: false,
	                dataType: "html",
	                cache: false,
	                success:function(result){
	                     $('.common-result').html(result);
	                     return false;
	                },
	                error:function(result){
	                     $('.common-result').html(result);
	                }
	            });
	             return false;
	    }); 
	
	    $('#companycall').click(function(){
	          $('.ui-dialog-titlebar').remove();  
	          $('#common-popup').dialog('open');
	          var rowId=-1; 
	          var companyTypes="ALL";
	             $.ajax({
	                type:"POST",
	                url:"<%=request.getContextPath()%>/common_company_list.action",
	                data:{id:rowId,companyTypes:companyTypes},
	                async: false,
	                dataType: "html",
	                cache: false,
	                success:function(result){
	                     $('.common-result').html(result);
	                     return false;
	                },
	                error:function(result){
	                     $('.common-result').html(result);
	                }
	            });
	             return false;
	    }); 
	    $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
	    
	    
	    $('.person-popup').live('click',function(){ 
	        $('.ui-dialog-titlebar').remove(); 
	        $('#job-common-popup').dialog('open');
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/get_employees_with_job.action", 
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){
	                   $('.job-common-result').html(result);
	              },
	              error:function(result){
	                  alert(result);
	                   $('.job-common-result').html(result);
	              }
	          });
	           return false;
		  }); 
		
		  $('#job-common-popup').dialog({
		      autoOpen: false,
		      minwidth: 'auto',
		      width:800,
		      height:450,
		      bgiframe: false,
		      modal: true 
		  });
		  

	  $('.addrows').click(function(){ 
		  var i=Number(1);   
		  var id=Number(getRowId($(".customer-tab>.customer-rowid:last").attr('id')));  
		  id=id+1; 
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.customer-tab tr:last').before(result);
					 if($(".customer-tab").height()>255)
						 $(".customer-tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.customer-rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	  $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.customer-rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 });

	  $('.contactname').live('change',function(){
		  rowId=getRowId($(this).attr('id'));
		  triggerAddRow(rowId);
	   });

	  $('.contactperson').live('change',function(){
		  rowId=getRowId($(this).attr('id'));
		  triggerAddRow(rowId);
	  });

	  $('#memberCarType').live('change',function(){ 
		  var tempmemberType = $('#tempmemberType').val();
		  var cardType = $(this).val(); 
 		  $('#cardNumber').val('');
		  if(cardType!='' && tempmemberType!=cardType){  
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_member_card_number.action", 
				 	async: false,
				 	data:{cardType: cardType},
				    dataType: "json",
				    cache: false,
					success:function(response){ 
						if(null!=response.memberCardVO && Number(response.memberCardVO.updatedDigit) > 0)
							$('#cardNumber').val(response.memberCardVO.updatedDigit); 
						else
							$('#memberCarType').val('');
					}
			 });
		  }else
			 $('#cardNumber').val(cardType !='' ? $('#tempcardNumber').val() : ''); 
		  return false; 
	  }); 
		 
	$('.cdiscard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_all_customers.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('#codecombination-popup').dialog('destroy');		
					$('#codecombination-popup').remove(); 
					$("#main-wrapper").html(result);  
				}
		 });
	 });

	 $('#csave').click(function(){ 
		 if($jquery("#customer_details").validationEngine('validate')){
 			 var customerId=$('#customerId').val();
			 var customerNumber=$('#customerNumber').val();
			 var customerType=$('#customerType').val();
			 var creditTermId=Number($('#creditTermId').val());
			 var creditLimit=$('#creditLimit').val();
			 var openingBalance=Number($('#openingBalance').val());
			 var bankName=$('#bankName').val();
			 var branchName=$('#branchName').val();
			 var iban=$('#iban').val();
			 var accountNumber=$('#accountNumber').val();   
			 var personId=Number($('#personCustomerId').val());
			 var cmpDeptLocId=Number($('#cmpDeptLocCustomerId').val());  
			 var combinationId=$('#combinationId').val(); 
			 var memberCarType = $('#memberCarType').val(); 
			 var cardNumber = Number$('#cardNumber').val();
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/customer_update.action", 
					data:{
							customerId: customerId,
							customerNumber: customerNumber,
							customerType: customerType, 
							creditTermId: creditTermId,
							openingBalance: openingBalance,
							bankName: bankName,
							branchName: branchName,
							ibanNumber: iban,
							accountNumber: accountNumber,
							creditLimit: creditLimit,
							personId: personId,
							cmpDeptLocId: cmpDeptLocId,
							combinationId: combinationId,
							memberCarType: memberCarType,
							cardNumber: cardNumber
						},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('#codecombination-popup').dialog('destroy');		
						$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);  
					}
			 });
		 }
		 else{
			return false;
		}
	 });
});
function getRowId(id){   
	var idval=id.split('_'); 
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var name=$('#contactName_'+rowId).val();  
	 var person=$('#contactPerson_'+rowId).val(); 
	 var nexttab=$('#customer-fieldrow_'+rowId).next(); 
	if(name!=null && name!=""
			&& person!=null && person!=""  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click'); 
	}
}
function showCompany(companyId){
	$.ajax({
		type: "POST", 
		url:"<%=request.getContextPath()%>/add_company.action",
		data: {companyId : companyId, isCustomer: true}, 
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result) { 
			$("#sub-content").html(result);
		},
		error: function(result) { 
			alert("error: " + result); 
		} 
	}); 
	
	identityShowHideCall(false);
	$('#companyTypeIdTemp').val("12,"+$('#companyTypeIdTemp').val());
	companyTypeAutoSelectd();
	$('#companyTypeId').attr('disabled', true);
	$('#customerinfo').val(true);
	$("#customer-result-div").html($("#customer-data-div").html());
	$("#customer-data-div").html("");
}
function showPerson(personId){
	$.ajax({
		type: "POST", 
		url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
		data: {personId: personId, isCustomer: true},
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result) { 
			$("#sub-content").html(result); 
		},
		error: function(result) { 
			alert("error: " + result); 
		} 
	}); 
	
	identityShowHideCall(false);
	dependentShowHideCall(false);
	$('#personTypeIdTemp').val("8,"+$('#personTypeIdTemp').val());
	personTypeAutoSelectd();
	$('#personType').attr('disabled', true);
	$('#customerinfo').val(true);
	$("#customer-result-div").html($("#customer-data-div").html());
	$("#customer-data-div").html("");
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".customer-tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".customer-tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.customer-tab>tr:last').removeAttr('id');  
	$('.customer-tab>tr:last').removeClass('customer-rowid').addClass('lastrow');  
	$($('.customer-tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.customer-tab>tr:last').append("<td style=\"height:10px;\"></td>");
	} 
}
function personPopupResult(personid,personname,commonParam){
	$("#customer-data-div").html($("#customer-result-div").html());
	showPerson(personid);
}
function companyPopupResult(companyId,companyName,commonParam){
	$("#customer-data-div").html($("#customer-result-div").html());
	showCompany(companyId);
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script> 
<div id="main-content">	
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			customer
		</div> 
			<div id="hrm" class="width100" style="margin:5px;">
				<fieldset><legend><fmt:message key="accounts.customer.customertype" /></legend> 
					<div class="float-left width30"><input name="customerType" id="customerCompany" type="radio"> <fmt:message key="accounts.customer.type.company" /></div>
					<div class="float-left width30"><input name="customerType" id="customerPerson" type="radio"> <fmt:message key="accounts.customer.type.person" /></div>
					<div class="float-left width40" id="person-selection-div" style="display:none;">
		                 <label class="width20">Person</label>
		                 <span class="button" style="position: relative;top:6px; ">
							<a style="cursor: pointer;" id="employeecall" class="btn ui-state-default ui-corner-all width100 employee-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
	                </div>
	                <div class="float-left width40" id="company-selection-div" style="display:none;">
	                   	<label class="width20">Company</label>
	                    <span class="button" style="position: relative;top:6px; ">
							<a style="cursor: pointer;" id="companycall" class="btn ui-state-default ui-corner-all width100 company-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
	                </div>
				</fieldset> 
			</div>
			<div class="width100 float-left" id="hrm">
				<div id="customer-data-div" style="display:none;">
						<input name="customerId" id="customerId" type="hidden" value="${CREDIT_TERM_INFORMATION.customerId}"> 
						<input name="customerPersonId" id="customerPersonId" type="hidden" value="${CREDIT_TERM_INFORMATION.personByPersonId.personId}"> 
					  
					<c:choose>
						<c:when test="${CREDIT_TERM_INFORMATION.company ne null}">
							<input name="companyCustomerId" id="companyCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.company.companyId}"> 
							<input name="cmpDeptLocCustomerId" id="cmpDeptLocCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.company.companyId}"> 
						</c:when>
						<c:otherwise>
							<input name="companyCustomerId" id="companyCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.company.companyId}"> 
							<input name="cmpDeptLocCustomerId" id="cmpDeptLocCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.cmpDeptLocId}"> 
							<input type="hidden" id="departmentID" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.department.departmentId}"/> 
							<input type="hidden" id="locationID" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.location.locationId}"/> 
						</c:otherwise>
					</c:choose>
						 <form id="customer_details" name="customer_details" style="position: relative;">
							<div class="width48 float-right">
								<fieldset>  
									<div>
										<label class="width30"><fmt:message key="accounts.customer.creditlimit"/></label>
										<input type="text" name="creditLimit" class="width50 validate[optional,custom[number]]" id="creditLimit" value="${CREDIT_TERM_INFORMATION.creditLimit}"/>
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.salerepresentative"/></label>
										 <input type="text" name="employeeName" class="width50" id="employeeName" 
										 		value="${CREDIT_TERM_INFORMATION.personBySaleRepresentative.firstName} ${CREDIT_TERM_INFORMATION.personBySaleRepresentative.lastName}"/>
										  <span class="button" id="employee"  style="position: relative; top:4px;">
				                              <a style="cursor: pointer;" id="employeepopup" class="btn ui-state-default ui-corner-all width10 person-popup"> 
				                                  <span class="ui-icon ui-icon-newwin">
				                                  </span> 
				                              </a>
				                          </span> 
				                     	  <input type="hidden" class="saleRepresentativeId" value="${CREDIT_TERM_INFORMATION.personBySaleRepresentative.personId}"/> 
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.referalsource" /></label>
										<select name="refferalSource" id="refferalSource" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CUSTOMER_REFFERAL_SOURCE ne null && CUSTOMER_REFFERAL_SOURCE ne '' }">
													<c:forEach items="${CUSTOMER_REFFERAL_SOURCE}" var="refferalSource">
														<option value="${refferalSource.lookupDetailId}">${refferalSource.displayName}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select>
										<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="REFFERAL_SOURCE"
											class="btn ui-state-default ui-corner-all refferalsource-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										<input type="hidden" id="temprefferalSource" value="${CREDIT_TERM_INFORMATION.lookupDetailByRefferalSource.lookupDetailByRefferalSource}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.shippingmethods" /></label>
										<select name="shippingMethod" id="shippingMethod" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${SHIPPING_METHOD ne null && SHIPPING_METHOD ne '' }">
													<c:forEach items="${SHIPPING_METHOD}" var="shippingMethod">
														<option value="${shippingMethod.lookupDetailId}">${shippingMethod.displayName}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select>
										<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SHIPPING_SOURCE"
											class="btn ui-state-default ui-corner-all shippingmethod-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										<input type="hidden" id="tempshippingMethod" value="${CREDIT_TERM_INFORMATION.lookupDetailByShippingMethod.lookupDetailByRefferalSource}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.openingBalance" /></label>
										<input type="text" name="openingBalance" class="width50 validate[optional,custom[number]]" id="openingBalance" value="${CREDIT_TERM_INFORMATION.openingBalance}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.common.description"/></label>
										<textarea id="customerDescription" class="width51">${CREDIT_TERM_INFORMATION.description}</textarea>
									</div>
								</fieldset>
							</div>
							<div class="width50 float-left">
								<fieldset style="min-height: 141px;"> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.customerno" /><span style="color:red">*</span></label> 
										<c:choose>
											<c:when test="${CREDIT_TERM_INFORMATION.customerId >0}">
												<input type="text" name="customerNumber" readonly="readonly" class="width50 validate[required]" 
													id="customerNumber" value="${CREDIT_TERM_INFORMATION.customerNumber}"/>
											</c:when>
											<c:otherwise>
												<input type="text" name="customerNumber" readonly="readonly" class="width50 validate[required]" 
													id="customerNumber" value="${CUSTOMER_NUMBER}"/>
											</c:otherwise>
										</c:choose>
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.customertype" /><span style="color:red">*</span></label>
										<select name="customerAcType" id="customerAcType" class="width51 validate[required]">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CUSTOMER_TYPE_LIST ne null && CUSTOMER_TYPE_LIST ne '' }">
													<c:forEach items="${CUSTOMER_TYPE_LIST}" var="CUSTOMER_TYPE">
														<option value="${CUSTOMER_TYPE.key}">${CUSTOMER_TYPE.value}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select>
										<input type="hidden" id="tempcustomertype" value="${CREDIT_TERM_INFORMATION.customerType}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.billingaddress" /><span style="color:red">*</span></label> 
										<input type="text" class="width50 validate[required]" id="billingAddress" value="${CREDIT_TERM_INFORMATION.billingAddress}"/>
									</div> 
									<div>
										<label class="width30">Card Type</label> 
										<select name="memberCarType" id="memberCarType" class="width51">
											<option value="">Select</option> 
											<c:forEach items="${MEMBER_CARD_TYPES}" var="memberCarType">
												<option value="${memberCarType.key}">${memberCarType.value}</option>
											</c:forEach> 
										</select>
										<input type="hidden" id="tempmemberType" value="${CREDIT_TERM_INFORMATION.memberType}"/>
									</div>
									<div>
										<label class="width30">Card Number</label>  
										<input type="text" id="cardNumber" value="${CREDIT_TERM_INFORMATION.memberCardNumber}"
											class="width50" readonly="readonly"/>
										<input type="hidden" id="tempcardNumber" value="${CREDIT_TERM_INFORMATION.memberCardNumber}"
											readonly="readonly"/>	
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.creditterm"/></label>
										<select name="creditTermId" id="creditTermId" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CREDIT_TERM_LIST ne null && CREDIT_TERM_LIST ne '' }">
													<c:forEach items="${CREDIT_TERM_LIST}" var="CREDIT_TERM">
														<option value="${CREDIT_TERM.creditTermId}">${CREDIT_TERM.name}</option>
													</c:forEach>
												</c:when>
											</c:choose>
										</select>
										<input type="hidden" id="tempterm" value="${CREDIT_TERM_INFORMATION.creditTerm.creditTermId}"/>
									</div> 
							</fieldset>
							</div>
							<div class="clearfix"></div> 
							<div id="hrm" class="hastable width100" style="position: relative; top: 3px; bottom: 5px;">
							<fieldset> 
								<legend>Shipping Addresses<span style="color:red">*</span></legend>
									<table id="hastab" class="width100">
										<thead>
											<tr> 
												<th><fmt:message key="accounts.creditterm.shippingname" /></th>
												<th><fmt:message key="accounts.creditterm.contactperson" /></th>
												<th><fmt:message key="accounts.creditterm.contactno" /></th>
												<th><fmt:message key="accounts.creditterm.address" /></th> 
												<th style="width: 0.01%;"><fmt:message key="accounts.common.label.options" /></th>
											</tr>
										</thead>
										<tbody class="customer-tab">
											<c:forEach var="SHIPPING_DETAIL" items="${CREDIT_TERM_INFORMATION.shippingDetails}" varStatus="status">
												<tr class="customer-rowid" id="customer-fieldrow_${status.index+1}"> 
													<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td>
													<td>
														<input type="text" id="contactName_${status.index+1}" value="${SHIPPING_DETAIL.name}" class="contactname"/>
													</td>
													<td>
														<input type="text" id="contactPerson_${status.index+1}" value="${SHIPPING_DETAIL.contactPerson}" class="contactperson"/>
													</td>
													<td>
														<input type="text" id="contactNo_${status.index+1}" value="${SHIPPING_DETAIL.contactNo}"/>
													</td>
													<td>
														<input type="text" id="customeraddress_${status.index+1}" value="${SHIPPING_DETAIL.address}"/>
													</td>
													<td style="display: none;">
														<input type="hidden" id="shippingId_${status.index+1}" value="${SHIPPING_DETAIL.shippingId}"/>
													</td>
													<td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
													</td>   
												</tr>
											</c:forEach> 
											<c:forEach var ="i" begin="${fn:length(CREDIT_TERM_INFORMATION.shippingDetails)+1}" end="${fn:length(CREDIT_TERM_INFORMATION.shippingDetails)+2}" step="1" varStatus="status1">
												<tr class="customer-rowid" id="customer-fieldrow_${i}"> 
													<td id="lineId_${i}" style="display:none;">${status1.index+1}</td>
													<td>
														<input type="text" id="contactName_${i}" class="contactname"/>
													</td>
													<td>
														<input type="text" id="contactPerson_${i}" class="contactperson"/>
													</td>
													<td>
														<input type="text" id="contactNo_${i}"/>
													</td>
													<td>
														<input type="text" id="address_${i}"/>
													</td>
													<td style="display: none;">
														<input type="hidden" id="shippingId_${i}"/>
													</td>
													<td style="width:0.01%;" class="opn_td" id="option_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none;cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
													</td> 
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</fieldset>	
							</div>
						</form>
					</div> 
				</div>
	<div class="clearfix"></div> 
	<div id="sub-content" class="width100 float-left"> 
	</div> 
	<div class="clearfix"></div>  
	<div style="display: none;" class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
		<div class="portlet-header ui-widget-header float-right sdiscard" id="cdiscard" ><fmt:message key="accounts.common.cancel" /></div>
		<div class="portlet-header ui-widget-header float-right ssave" id="csave" ><fmt:message key="accounts.common.save" /></div> 
	</div>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div> 
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="job-common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
   		 <div class="job-common-result width100"></div>
   		 <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div> 
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="common-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 </div>
  </div>
</div> 