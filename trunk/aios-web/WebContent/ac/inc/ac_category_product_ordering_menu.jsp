<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>

	.box-model-product {
		/* background:  rgba(255, 255, 255, 0.73) !important; */
		background: #FFF none repeat scroll 0% 0% !important;
		border: 1px solid #E3E3E3;
		border-radius: 5px;
		/* box-shadow: 0px 0px 2px 1px #8F458F; */
		color: #8F458F;
		margin: 10px;
		transition: background-image 0.7s ease;
		width: 200px;
		height: 237px;
	}
	
	.box-model-product:hover {
		background: rgba(223, 219, 228, 0.36) none repeat scroll 0% 0% !important;
		border: 1px solid rgb(212, 206, 206);
		border-radius: 5px;
		box-shadow: 0px 0px 2px 1px rgb(227, 221, 227);
		color: rgb(160, 61, 185);
	}
	
	.product_image_big {
		width: 180px;
		border: none;
		border-radius: 50%;
		margin: 0 auto;
		display: block;
		box-shadow: 0 0 5px 0 #E7E2DB;
		margin-top: 30px;
		margin-bottom: 30px;
	}
	
	.box-model-product > span {
		font-size: 14px !important;
		font-weight: normal;
	}
	
	.selectProduct {
		background: #70365B; /* rgba(103, 22, 103, 0.75); */
		box-shadow: 0 0 5px 0px #ccc;
		border: none;
		border-radius: 3px;
		color: white;
		display: block;
		padding-top: 10px !important;
		margin: -5px !important;
		border-bottom-right-radius: 0px;
		border-bottom-left-radius: 0px;
	}
	
	.small_dscrp {
		color: gray !important;
	}
	
	.small_dscrp:hover {
		font-weight: normal !important;
	}
	
	.info-mask {
		background: transparent !important;
		color: transparent;
		height: 187px !important;
		display: block;
		z-index: 999;
		border: 0px solid black;
		width: 100%;
		margin-top: -65px;
		margin-left: -5px;
		border-radius: 5px;
		padding: 5px;
		font-size: 12px;
		text-align: center;
		padding: 55px 5px 6px;
		transition: background-color 0.5s ease;
		cursor: pointer;
		margin-bottom: -5px;
		display: flex;
	}
	
	.info-mask:hover {
		background: rgba(32, 32, 32, 0.73) none repeat scroll 0% 0% !important;
		color: #F2F2F2;
	}
	
</style>

<script>
	
	var p_id = 0;
	
	$(function() {
		
		$(".info-mask").hover(function() {
			$(".watermark_logo").css("bottom", "1px");
			$(".price_tag").hide();
			p_id = $(this).attr('id').split("_")[1];
			$("#watermark_" + p_id).css("bottom", "27px");
			$("#price_" + p_id).show();
			$("#price_" + p_id).css("display", "block");
		});
	});
	
	function show_previous_img(productId, productCode) {
		
		var currentImageNumber = $("#background-number_" + productId).val();
		
		if(currentImageNumber == 1 || currentImageNumber == "1" || currentImageNumber == "") {
			return false;
		}
		
		updateImageForProduct(Number(currentImageNumber) - 1, productCode, productId);
	}
	
	function show_next_img(productId, productCode) {
		
		var currentImageNumber = $("#background-number_" + productId).val();
		
		if(currentImageNumber == "") {
			return false;
		}
		
		updateImageForProduct(Number(currentImageNumber) + 1, productCode, productId);
	}
	
	function updateImageForProduct(newImageNumber, productCode, productId) {
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/online-menu-get-product-image.action",
			async : false,
			data : {productCode: productCode, requiredImageNumber: Number(newImageNumber)},
			dataType : "json",
			cache : false,
			success : function(result) {
				if(result.imgPath == "none") {
					return false;
				}
				$("#background_" + productId).attr("style", "background: url(images/ordering-menu/company/" + result.imgPath + ") no-repeat 0px 38px !important; background-size: 100% !important; max-height: none !important;");
				$("#background-number_" + productId).val(newImageNumber);
			},
			error: function(result) {
				console.debug("Error getting product image");
			}
		});
	}
</script>

<span class="line"></span>
<br/><br/>

<c:choose>
	<c:when	test="${PRODUCT_DETAILS ne null && fn:length(PRODUCT_DETAILS) > 0}">
		<div class="width100 content-item-table products-panel" id="scroll-pane" align="center" style="">			
			<c:set var="row" value="0" />			
			<c:forEach items="${PRODUCT_DETAILS}" var="product">
				
				<div id="background_${product.productId}" class="box-model box-model-product lazy" data-original="images/ordering-menu/company/${THIS.companyKey}/products/${product.code}_1.png" 
						style="background: url(images/ordering-menu/company/${THIS.companyKey}/noimage.png) no-repeat 50% 50% !important; background-size: 13% !important; max-height: none !important;">																	
					
					<input type="hidden" id="background-number_${product.productId}" value="1" />
					<input type="hidden" id="productId_${product.productId}" class="productId" /> 
					<input type="hidden" id="specialPrd_${product.productId}" value="${product.specialPos}" class="specialPrd" />
					
					<span class="selectProduct" id="selectProduct_${product.productId}">
						${product.productName}
					</span>
																							
					<span class="price_tag" id="price_${product.productId}" style="font-size: 10px !important; position: relative; bottom: -178px; left: 10px; margin-bottom: -31px !important; color: orange; display: none;">Price 
						<span style="font-size: 20px;">												
							<c:choose>
								<c:when test="${product.productPricingDetails ne null}">
									<c:forEach begin="0" end="0" step="1" items="${product.productPricingDetails}" var="price">
										${price.sellingPrice}<span style="font-size: 15px;">AED</span>
									</c:forEach>
								</c:when>
								<c:otherwise>
									N/A
								</c:otherwise>
							</c:choose>
							<img onclick="show_previous_img(${product.productId},'${product.code}')" style="position: relative; top: 5px; left: 10px;" src="images/arrow_pre.png" height="18px" />
							<img onclick="show_next_img(${product.productId},'${product.code}')" style="position: relative; top: 5px; right: -10px;" src="images/arrow_next.png" height="18px" />
						</span>
					</span>
					
					<div class="info-mask" id="descrp_${product.productId}">											
						<span style="font-size: 10px; position: relative; left: 70px;">${product.code}</span> <br/><br/>
						<p style="padding: 0; text-align: left; margin-left: -8px;margin-top: 20px; line-height: 1.5;">${product.description}</p>			
					</div>	
					
					<img class="watermark_logo" id="watermark_${product.productId}" style="position: relative; left: -89px; bottom: 1px; opacity: 0.9;" src="images/grandma's_cheesecake_indexlogo.png" height="25px" title="${THIS.companyName}"/>								
				</div>
				
			</c:forEach>						
			
			<div style="position: fixed; right: 25px; bottom: 10px;">
				<img src="images/back-to-top.png" title="Back to top" width="80px" style="opacity: 0.6; cursor: pointer;" onclick="backToTop();" />
			</div>
		</div>		
	</c:when>
	<c:otherwise>
		<div style="height: 55px;" align="center" class="products-panel">
			<span style="color: rgb(255, 149, 95); font-size: 15px; font-weight: 400;">
				Oops!! today we don't have any products to display, in selected category.
			</span>
		</div>
	</c:otherwise>
</c:choose>

<script type="text/javascript">
	
	$(function() {	
		$("div.lazy").lazyload({
			effect : "fadeIn",
			container : $("#main-content"),
		});
	});
	
	function backToTop() {
		$('#main-content,html').animate({
			scrollTop: 0 ,
		 	}, 1000
		);
	}
	
</script>