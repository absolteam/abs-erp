<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend>Requisition By Location</legend>
	<div>
		<c:forEach var="ACTIVE_RETURNS" items="${ISSUE_REQUISITION_INFO}">
			<label for="activeReturnNumber_${ACTIVE_RETURNS.issueRequistionId}" class="width10">
				<input type="checkbox" name="activeReturnNumber" class="activeReturnNumber" id="activeReturnNumber_${ACTIVE_RETURNS.issueRequistionId}"/>
				${ACTIVE_RETURNS.referenceNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultIssueSelect($('.activeReturnNumber').attr('id'));
	$('.activeReturnNumber').click(function(){   
		validateIssueInfo($(this));   
	});
});
</script>