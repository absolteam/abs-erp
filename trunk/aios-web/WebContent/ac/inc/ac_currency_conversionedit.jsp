<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
var currencyConversions = [];
$(function(){
	$jquery(".conversionRate").number(true,3); 
	$jquery("#currencyConversionValidation").validationEngine('attach'); 
	$('.saveConversion').click(function(){   
		 if($jquery("#purchaseentry_details").validationEngine('validate')){
			 currencyConversions = getCurrencyConversions();  
		 		if(currencyConversions != null && currencyConversions != ""){
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/currency_conversion_save.action", 
					 	async: false, 
					 	data:{	
					 			currencyConversions: JSON.stringify(currencyConversions)
						 	 },
					    dataType: "json",
					    cache: false,
						success:function(response){   
							 if(($.trim(response.returnMessage)=="SUCCESS")) 
								 currencyConversionDiscard("Record updated.");
							 else{
								 $('#page-error').hide().html(response.returnMessage).slideDown();
								 $('#page-error').delay(2000).slideUp();
								 return false;
							 }
						} 
					}); 
				}else{
					$('#page-error').hide().html("Please enter currency conversions.").slideDown();
					$('#page-error').delay(2000).slideUp();
					return false;
				} 
		 } 
		 return false;
	 });
	
	var getCurrencyConversions = function(){
		currencyConversions = []; 
		var conversionCurrency = Number($('#currencyConversion').val());  
		var conversionRate = Number($jquery('#conversionRate').val()); 
		var currencyConversionId = Number($('#currencyConversionId').val()); 
			if(typeof conversionCurrency != 'undefined' && conversionCurrency > 0 && 
					conversionRate > 0 && currencyConversionId > 0){
				currencyConversions.push({
				"conversionCurrency" : conversionCurrency,
				"conversionRate" : conversionRate,
				"currencyConversionId" :currencyConversionId
			});
		}  
		return currencyConversions;
	 }; 
	
	$('.cancelConversion').click(function(){  
		 currencyConversionDiscard("");
		 return false;
	 });
});
function currencyConversionDiscard(message){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_currency_conversions.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) { 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
</script>
<div id="main-content">
	<c:choose>
		<c:when
			test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From :<span>
							${COMMENT_IFNO.name}</span> </label> <label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div>
		</c:when>
	</c:choose>
	<form name="newFields" id="currencyConversionValidation"
		style="position: relative;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Currency
			Conversion
		</div>
		<div class="portlet-content width100 float-left" id="hrm">
			<div id="page-error" class="response-msg error ui-corner-all"
				style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
			<div class="width48 float-left">
				<input type="hidden"  id="currencyConversion" name="currencyConversion"
						value="${NON_DEFAULT_CURRENCY.currency.currencyId}" />
				<input type="hidden"  id="currencyConversionId" name="currencyConversionId"
						value="${NON_DEFAULT_CURRENCY.currencyConversionId}" />
				<div style="padding: 5px;">
					<label class="width20">Functional Currency</label> <span
						style="position: relative; top: 7px;">${DEFAULT_CURRENCY.code}
						(${DEFAULT_CURRENCY.countryName})</span>
				</div>
				<div style="padding: 5px;">
					<label class="width20">Conversion Currency</label> <span
						style="position: relative; top: 7px;">${NON_DEFAULT_CURRENCY.conversionCurrency}
						(${NON_DEFAULT_CURRENCY.countryName})</span>
				</div>
				<div style="padding: 5px;">
					<label class="width20">Conversion Rate<span
									class="mandatory">*</span></label> <input type="text"
						id="conversionRate" name="conversionRate" class="conversionRate validate[required] width30"
						value="${NON_DEFAULT_CURRENCY.conversionRate}" />
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</form>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="margin: 10px;">
		<div
			class="portlet-header ui-widget-header float-right cancelConversion"
			style="cursor: pointer;">
			<fmt:message key="accounts.currencymaster.button.cancel" />
		</div>
		<div
			class="portlet-header ui-widget-header float-right saveConversion"
			style="cursor: pointer;">
			<fmt:message key="accounts.currencymaster.button.save" />
		</div>
	</div>
</div>