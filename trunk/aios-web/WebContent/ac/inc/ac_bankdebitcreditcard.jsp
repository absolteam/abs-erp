<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <table id="hastab" class="width100">
	<thead>
		<tr>
			<th class="width10">Account No.</th>
			<th class="width10">Account Holder</th>
			<th class="width10">Card Number</th> 
			<th class="width10">Card Type</th>
			<th class="width10">Issuing Entity</th>
			<th class="width10">Valid from</th> 
			<th class="width10">Valid to</th> 
			<th class="width10">Limit</th> 
			<th class="width10">Purpose</th>  
			<th class="width10"><fmt:message
					key="accounts.calendar.label.options" /></th>
		</tr>
	</thead>
	<tbody class="tab">  
			<c:forEach var="i" begin="1"
				end="2" step="1"
				varStatus="status">
				<tr id="fieldrow_${i}" class="rowid">
					<td class="width10" id="accountNumber_${i}"></td>
					<td class="width10" id="accountHolder_${i}"></td>
					<td class="width10" id="cardNumber_${i}"></td>  
					<td class="width10" id="cardType_${i}"></td>  
					<td class="width10" id="issuingEntity_${i}"></td>
					<td class="width10" id="validFrom_${i}"></td> 
					<td class="width10" id="validTo_${i}"></td> 
					<td class="width10" id="limit_${i}"></td> 
					<td class="width10" id="purpose_${i}"></td> 
					<td class="width10" id="option_${i}">
					<input type="hidden" id="bankAccountId_${i}" />
					<input type="hidden" id="lineId_${i}" value="${i}" /> 
					<input type="hidden" id="csc_${i}"/> 
					<input type="hidden" id="pin_${i}"/> 
					<input type="hidden" id="typeidval_${i}"/>  
					<input type="hidden" id="issuingid_${i}"/>  
					<input type="hidden" id="description_${i}"/>  
					<input type="hidden" id="branchContact_${i}"/>
					<input type="hidden" id="contactPerson_${i}"/>
				     <a
						style="cursor: pointer;"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankaddData"
						id="AddImage_${i}" title="Add Record"> <span
							class="ui-icon ui-icon-plus"></span> </a> <a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
						id="EditImage_${i}" style="display: none; cursor: pointer;"
						title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
					</a> <a style="cursor: pointer; display: none;"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
						id="DeleteImage_${i}" title="Delete Record"> <span
							class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
						id="WorkingImage_${i}" style="display: none;" title="Working">
							<span class="processing"></span> </a>
					</td>

				</tr>
			</c:forEach> 
	</tbody>
</table>