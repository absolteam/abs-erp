<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend><fmt:message key="accounts.grn.label.activepurchaseorder"/></legend>
	<div>
		<c:forEach var="ACTIVE_PO" items="${PURCHASE_INFO}">
			<label for="purchase_${ACTIVE_PO.purchaseId}" class="width10">
				<input type="checkbox" name="activePONumber" class="activePONumber" id="purchase_${ACTIVE_PO.purchaseId}"/>
				${ACTIVE_PO.purchaseNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultPOSelect($('.activePONumber').attr('id'));
	$('.activePONumber').click(function(){   
		validatePurchaseInfo($(this));   
	});
});
</script>