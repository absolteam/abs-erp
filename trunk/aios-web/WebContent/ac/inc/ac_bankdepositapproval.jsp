<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var accountTypeId=0;
var paymentLineDetails="";
$(function(){
	$('#depositDate').datepick();
	$('.common-popup').click(function(){  
	    //$('.ui-dialog-titlebar').remove();  
	    var id=getRowId($(this).attr('id'));  
		alert(id);
	    actionname="getbankaccount_details_multiple";  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionname+".action", 
			 	async: false,  
			 	data:{id:id},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
		$('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		//Combination pop-up config
	    $('.codecombination-popup').live('click',function(){
	        tempid=$(this).parent().get(0);
	        accountTypeId=0; 
	        actionname="";
	        $('.ui-dialog-titlebar').remove();  
	        actionname=$(tempid).attr('id');
	        var idVal=actionname.split('_');
	        if(typeof(idVal[1]!='undefined') && idVal[1]!=null && idVal[1]!=""){
	      	  actionname=idVal[0];
	        }
	        else{
	      	  actionname=$(tempid).attr('id');
	        }  
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/combination_treeview.action",
	             async: false, 
	             dataType: "html",
	             cache: false,
	             success:function(result){
	                  $('.codecombination-result').html(result); 
	             },
	             error:function(result){
	                  $('.codecombination-result').html(result);
	             }
	         });
	         return false;
	    }); 
	     
	     $('#codecombination-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800, 
				bgiframe: false,
				modal: true 
			});
		
	$('#paymentFrom').change(function(){
		var paymentFrom=$('#paymentFrom').val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/bank_deposit_payments.action",
			data:{paymentFrom:paymentFrom},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$("#payment_list_div").html(result);  
				return false;
			}
		 }); 
	});


	$('#deposit_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/bank_deposit_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});

	$('#deposit_save').click(function(){
		paymentLineDetails="";
		if($("#loanentry_details").validationEngine({returnIsValid:true})){   
			var bankDepositId=Number($('#bankDepositId').val());
			var bankDepositNumber=Number($('#bankDepositNumber').val());
			var depositDate=$('#depositDate').val();
			var description=$('#description').val();
			var paymentFrom=$('#paymentFrom').val();
			paymentLineDetails = getPaymentDetails();  
			if(paymentLineDetails!=null && paymentLineDetails!=""){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_bank_deposit.action", 
				 	async: false, 
				 	data:{	bankDepositId:bankDepositId, bankDepositNumber:bankDepositNumber, date:depositDate, description: description,
				 			paymentFrom:paymentFrom,bankDepositDetails: paymentLineDetails},
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html();  
						 if(message.trim()=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/bank_deposit_list.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
					  					$('#common-popup').remove();  
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html("Direct Payment Transaction Success..").slideDown(1000);
									}
							 });
						 } 
						 else{
							 $('#page-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error..").slideDown(1000);
					}
				}); 
			}else{
				$('#page-error').hide().html("Please enter payment detail info.").slideDown(1000);
				return false;
			}
		}else{
			return false;
		}
	});

	var getPaymentDetails = function(){
		var bankAccountIds=new Array();
		var amounts=new Array();
		var recordIds=new Array();
		var useCases=new Array();
		var combinationIds=new Array();
		var paymentDetail="";
		$('.rowid').each(function(){ 
			var rowId=getRowId($(this).attr('id'));
			if($('#paymentChecked_'+rowId).is(':checked')){
				 var bankAccount=$('#accountId_'+rowId).val();   
				 var amount=$('#amount_'+rowId).val();   
				 var recordId=$('#recordId_'+rowId).val();
				 var useCase=$('#useCase_'+rowId).val();
				 var combinationId=$('#codeCombination_'+rowId).val();
				 if(typeof bankAccount == 'undefined' || bankAccount==null || bankAccount=="")
					 recordId="-1";
				 
				 if(typeof recordId == 'undefined' || recordId==null || recordId=="")
					 bankAccount="-1";
				 
				 if(typeof useCase == 'undefined' || useCase==null || useCase=="")
					 useCase="-1";
				 
				 if(typeof combinationId == 'undefined' || combinationId==null || combinationId=="")
					 combinationId="-1";
				 
				 if(typeof amount != 'undefined' && amount!=null && 
						 amount!="" && combinationId!=null && combinationId!=null){
					 bankAccountIds.push(bankAccount); 
					 amounts.push(amount);
					 recordIds.push(recordId);
					 useCases.push(useCase);
					 combinationIds.push(combinationId);
				 } 
			}
		});
		for(var j=0;j<bankAccountIds.length;j++){ 
			paymentDetail+=bankAccountIds[j]+"@"+amounts[j]+"@"+recordIds[j]+"@"+useCases[j]+"@"+combinationIds[j];
			if(j==bankAccountIds.length-1){   
			} 
			else{
				paymentDetail+="##";
			}
		}  
		return paymentDetail;
	}; 

	var bankDepositId=Number($('#bankDepositId').val());		
	 $('#deposit_document_information').click(function(){
			if(bankDepositId>0){
				AIOS_Uploader.openFileUploader("doc","depositDocs","BankDeposit",bankDepositId,"BankDepositDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","depositDocs","BankDeposit","-1","BankDepositDocuments");
			}
		});
	 
		populateUploadsPane("doc","depositDocs","BankDeposit",bankDepositId);	

});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function setCombination(combinationTreeId,combinationTree){ 
  if(actionname=="cash"){  
	  $('#cashCombinationId').val(combinationTreeId); 
      $('.cashCodeCombination').val(combinationTree); 
  }
  else if(actionname=="codeID"){
	  var tempvar =$(tempid).attr('id');	
      var idVal=tempvar.split("_");
      var rowid=Number(idVal[1]); 
	  $('#combinationId_'+rowid).val(combinationTreeId); 
      $('#codeCombination_'+rowid).val(combinationTree); 
      triggerAddRow(rowid);
	  return false;
  }
  actionname="";
} 
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 Bank Deposit Information
			</div>	
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 General Information
			</div>	
			<div><form id="directPaymentCreation">
			<div style="width: 98%; margin: 10px;">
			<div>
			<div class="tempresult" style="display: none;"></div>
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset> 
						<input type="hidden" id="bankDepositId" value="${BANK_DEPOSIT.bankDepositId}"> 							 
						<div>
							<label class="width30">Deposit Number<span class="mandatory">*</span></label> 
							<label class="width60 view">${BANK_DEPOSIT.bankDepositNumber}</label>
						</div> 
						<div>
							<label class="width30">Deposit Date<span class="mandatory">*</span></label> 
							<c:choose>
								<c:when test="${BANK_DEPOSIT.date ne null && BANK_DEPOSIT.date ne ''}">
									<c:set var="payDate" value="${BANK_DEPOSIT.date}"/>  
									<%String date = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
									<label class="width60 view"><%=date%></label> 
								</c:when>  
								<c:otherwise>  
									<input type="text" name="depositDate"
									id="depositDate" readonly="readonly" class="width60 validate[required]" TABINDEX=2> 
								</c:otherwise>
							</c:choose> 
						</div>
						   
						<div>
							<label class="width30 tooltip">Description</label>
							<label class="width60 view">${BANK_DEPOSIT.description}</label>
						</div> 
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset> 		
						 <div style="height: 60px;">									
								<div id="deposit_document_information" style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
									<span id="depositDocs"></span>
								</div>
						</div>	
					</fieldset>
				</div> 
			</div>
		
			<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
				<fieldset> 	
					<legend>Payment Information</legend>
					 <table id="hastab" class="width100"> 
								<thead>
								   <tr>  
									    <th style="width:10%">Bank Account</th>   
									    <th style="width:2%">Amount</th>   
								  </tr>
								</thead> 
								<tbody class="tab" id="payment_list_div">  
									<c:choose>
								<c:when test="${BANK_DEPOSIT_DETAIL ne null && BANK_DEPOSIT_DETAIL ne ''}">
									<c:forEach var="PAYMENT_DETAIL" items="${BANK_DEPOSIT_DETAIL}" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${status.index+1}"> 
											<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td> 
											<td>
												${PAYMENT_DETAIL.bankDepositDetail.bankAccount.accountNumber} 
											</td>
											<td>
												${PAYMENT_DETAIL.bankDepositDetail.amount}
											</td> 
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td>No Records</td>
									</tr>
								</c:otherwise>
							</c:choose>
						 </tbody>
					</table>
					</fieldset>
			</div>		
			</div> 
			</form> 
			</div> 
			<div class="clearfix"></div>
			
			<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
			</div> 
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
			</div> 
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="codecombination-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
			</div>
		</div>
</div>