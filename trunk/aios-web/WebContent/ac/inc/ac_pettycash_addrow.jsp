<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="text" id="personName_${requestScope.rowId}"
		class="personid width80" /> <input type="hidden" id="personId_${requestScope.rowId}" />
		<span class="button float-right"> <a
			style="cursor: pointer; position: relative; top: 5px;"
			id="personID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_expense_person_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="hidden" name="combinationId_${requestScope.rowId}"
		id="combinationId_${requestScope.rowId}" /> <input type="text"
		id="codeCombination_${requestScope.rowId}" readonly="readonly" class="width80" /> <span
		class="button" id="codeID_${requestScope.rowId}"> <a style="cursor: pointer;"
			class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" name="linesExpenseAmount" id="linesExpenseAmount_${requestScope.rowId}"
		style="text-align: right;"
		class="linesExpenseAmount width98 right-align validate[optional,custom[number]]">
	</td>
	<td><input type="text" name="billno" id="billno_${requestScope.rowId}"
		class="billno width98"></td>
	<td><input type="text" name="linesDescription"
		id="linesDescription_${requestScope.rowId}" class="width98"></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span> </a> <input
		type="hidden" id="pettyCashDetailId_${requestScope.rowId}" /></td>
</tr>
<script type="text/javascript">
$(function(){
	$jquery('.linesExpenseAmount').number(true, 2);
});
</script>