<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${PURCHASE_BYID.purchaseNumber}
	</td>  
	<td>
		<c:set var="podate" value="${PURCHASE_BYID.date}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("podate").toString())%> 
	</td> 
	<td> 
		<c:set var="expirydate" value="${PURCHASE_BYID.expiryDate}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("expirydate").toString())%>  
	</td> 
	<td style="display: none;">
		 ${PURCHASE_BYID.totalPurchaseQty}
	</td>
	<td style="text-align: right;" id="totalpoamt_${rowId}">
		<c:set var="totalamt" value="${PURCHASE_BYID.totalPurchaseAmount}"/>
		<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
	</td>
	<td>
		${PURCHASE_BYID.description}
	</td>   
	<td style="display:none;">
		<input type="hidden" id="purchaseId_${rowId}" value="${PURCHASE_BYID.purchaseId}"/>
		<c:choose>
			<c:when test="${PURCHASE_BYID.continuousPurchase ne null && PURCHASE_BYID.continuousPurchase eq true}">
				<input type="checkbox" name="continuousPurchase" id="continuousPurchase_${rowId}" class="width3" checked="checked"/>
			</c:when>
			<c:otherwise>
				<input type="checkbox" name="continuousPurchase" id="continuousPurchase_${rowId}" class="width3" />
			</c:otherwise>
		</c:choose>
 	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}"> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatarc" id="EditImage_${rowId}" style="cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowrc" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a> 
	</td>   
</tr>