<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%> 
<script type="text/javascript">
$(function(){ 
	$('.emiDueDate').datepick();
	if($('.rowid').hasClass('userrow')){
		manupulateLastRow();
	} 
	
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/emi_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	
	$('.doblur').live('change',function(){ 
		var rowId=getRowId($(this).attr('id')); 
		triggerAddRow(rowId);
		//calculateBalance(rowId);
		return false;
	});  
	
	$('.extend').live('change',function(){ 
		 $('.calculate').show(); 
		 $('#savediscard').show();
		return false;
	}); 
	
	if(($('#emilistsize').val()!=null && $('#emilistsize').val()!="" && $('#emilistsize').val()>0)){ 
		if($('#loantype').val()=='F'){
			$('#interestRate').attr('disabled',true);
		}
		else{
			$('#interestRate').attr('disabled',false);
		}
		$('#interestRate').val($('#rate').val());
		$('.calculate').show(); 
	    $('#savediscard').show();
	} 
	else{  
		$('.insterestrate').hide();
		$('.calculate').hide(); 
	    $('#savediscard').hide();
	}
	
	var calculateBalance = function(rowId){
		var dueDate=$('#dueDate_'+rowId).val();  
		var instalment=$('#instalment_'+rowId).val();
		var interest=$('#interest_'+rowId).val();
		var eiborAmount=$('#eiborAmount_'+rowId).val();
		var insurance=$('#insurance_'+rowId).val();
		var otherCharges=$('#addtionalcharges_'+rowId).val(); 
		if(typeof instalment!='undefined' && instalment!=null && instalment!="" 
				&& typeof interest!= 'undefined' && interest!=null && interest!=""){
			instalment=instalment.replace(/,/gi,''); 
			interest=interest.replace(/,/gi,'');
			if(typeof eiborAmount=='undefined' || eiborAmount==null || eiborAmount!=""){
				eibor = 0;
			}else{
				eiborAmount=eiborAmount.replace(/,/gi,'');
			}
			if(typeof insurance=='undefined' || insurance==null || insurance!=""){
				insurance = 0;
			}else{
				insurance=insurance(/,/gi,'');
			}
			if(typeof otherCharges=='undefined' || otherCharges==null || otherCharges==""){
				otherCharges = 0;
			}else{
				otherCharges=otherCharges(/,/gi,'');
			}
			 $('.build_schedule').trigger('click');
		} 
				 
		 
	};
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid userrow').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var dueDate=$('#dueDate_'+rowId).val();  
	 var instalment=$('#instalment_'+rowId).val();
	 var interest=$('#interest_'+rowId).val();
	 var eibor=$('#eiborAmount_'+rowId).val();
	 var insurance=$('#insurance_'+rowId).val();
	 var addtionalcharges=$('#addtionalcharges_'+rowId).val();
	 var depreciation=$('#depreciation_'+rowId).val();
	 var cumulative=$('#cumulative_'+rowId).val();
	 var balance=$('#balance_'+rowId).val();
	 var description=$('#description_'+rowId).val();
	 var nexttab=$('#fieldrow_'+rowId).next();  
	if(dueDate!=null && dueDate!=""
		&& instalment!=null && instalment!=""
		&& interest!=null && interest!=""  
		&& $(nexttab).hasClass('lastrow')){  
			$('.addrows').trigger('click');
	} 
}
</script>
<input type="hidden" id="emilistsize" value="${fn:length(requestScope.EMI_DETAIL_LIST)}"/>
<input type="hidden" id="loantype" value="${LOAN_TYPE}"/>
<c:choose>
	<c:when test="${requestScope.EMI_DETAIL_LIST ne null && requestScope.EMI_DETAIL_LIST ne '' && fn:length(requestScope.EMI_DETAIL_LIST)>0}">
	<fieldset>
		<legend>EMI Details</legend>
		<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div> 
		<input type="hidden" id="rate" value="${requestScope.RATE}"/>
		<div id="hrm" class="hastable width100"  >  
			<input type="hidden" name="childCount" id="childCount" value=""/>  
			<table id="hastab" class="width100"> 
				<thead>
			   		<tr> 
						<th style="width:1%">Line No</th> 
				   	 	<th style="width:6%">Due Date</th>  
				   	 	<th style="width:5%">Instalment</th>   
				   	 	<th style="width:5%">Interest</th>   
				   	 	<th class="eiborhead" style="width:5%">EIBOR</th>   
				   	 	<th class="insurancehead" style="width:5%">Insurance</th>
				   	 	<th style="width:5%">Other Charges</th>
				   	 	<th style="width:5%">Depreciation</th>
				   	 	<th style="width:5%">Cumulative</th>    
				   	 	<th style="width:5%">Balance</th>    
				   	 	<th style="width:5%">Description</th>   
						<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
		 		 </tr>
				</thead> 
				<tbody class="tab">  
					
							<c:forEach var="bean" items="${EMI_DETAIL_LIST}" varStatus ="status"> 
								<tr class="rowid" id="fieldrow_${status.index+1}">
									<td id="lineId_${status.index+1}">${status.index+1}</td> 
									<td> 
										<input type="text" name="emiDueDate" class="emiDueDate width95" readonly="readonly" id="dueDate_${status.index+1}" 
											style="border:0px;" value="${bean.emiDueDate}"/>
									</td> 
									<td>
										<c:set var="instalments" value="${bean.instalment}"/>
										<%String instalment = AIOSCommons.formatAmount(pageContext.getAttribute("instalments"));
														pageContext.setAttribute("instalments", instalment);%>
										<input type="text" style="text-align:right;border:0px;" class="width95 principal extend" 
											id="instalment_${status.index+1}" value="${instalments}"/>
									</td> 
									<td>
										<c:set var="interest" value="${bean.interest}"/>
										<%String interest = AIOSCommons.formatAmount(pageContext.getAttribute("interest"));
														pageContext.setAttribute("interest", interest);%>
										<input type="text" style="text-align:right;border:0px;width:92%;" class="interest extend" id="interest_${status.index+1}"
											value="${interest}" />
									</td>
									<c:choose>
										<c:when test="${bean.eiborAmount ne 0 && bean.eiborAmount!= 0}">
											<td>
												<c:set var="eiborAmount" value="${bean.eiborAmount}"/>
												<%String eiborAmount = AIOSCommons.formatAmount(pageContext.getAttribute("eiborAmount"));
																pageContext.setAttribute("eiborAmount", eiborAmount);%>
												<input type="text" style="text-align:right;border:0px;" class="width95 eiborAmount extend" id="eiborAmount_${status.index+1}"
													value="${eiborAmount}" />
											</td>
										</c:when>
										<c:otherwise>
											<script type="text/javascript">
												$(function(){
													$('.eiborhead').remove(); 
												});
											</script>
										</c:otherwise>
									</c:choose> 
									<c:choose>
										<c:when test="${bean.insurance ne 0 && bean.insurance!= 0}">
											<td> 
												<c:set var="insurance" value="${bean.insurance}"/>
												<%String insurance = AIOSCommons.formatAmount(pageContext.getAttribute("insurance"));
															pageContext.setAttribute("insurance", insurance);%>
												<input type="text" style="text-align:right;border:0px;" class="width95 insurance extend" id="insurance_${status.index+1}"
													value="${insurance}"/>
											 </td>
										</c:when>
										<c:otherwise>
											<script type="text/javascript">
												$(function(){
													$('.insurancehead').remove(); 
												});
											</script>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${bean.otherCharges ne null && bean.otherCharges ne 0 && bean.otherCharges gt 0}">
											<td>
												<c:set var="otherCharges" value="${bean.otherCharges}"/>
												<%String otherCharges = AIOSCommons.formatAmount(pageContext.getAttribute("otherCharges"));
																pageContext.setAttribute("otherCharges", otherCharges);%>
												<input type="text" style="text-align:right;border:0px;" class="width95 addtionalcharges extend" id="addtionalcharges_${status.index+1}"
													value="${otherCharges}" />
											</td>
										</c:when>
										<c:otherwise>
											<td>
												<input type="text" style="text-align:right;border:0px;" class="width95 addtionalcharges extend" id="addtionalcharges_${status.index+1}"/>
											</td>
										</c:otherwise>
									</c:choose> 	
									<td>
										<c:set var="emiDepreciation" value="${bean.emiDepreciation}"/>
										<%String emiDepreciation = AIOSCommons.formatAmount(pageContext.getAttribute("emiDepreciation"));
														pageContext.setAttribute("emiDepreciation", emiDepreciation);%>
										<input type="text" style="text-align:right;border:0px;" class="width95 depreciation" id="depreciation_${status.index+1}"
											value="${emiDepreciation}"/>
									</td>
									<td>
										<c:set var="emiCumulative" value="${bean.emiCumulative}"/>
										<%String emiCumulative = AIOSCommons.formatAmount(pageContext.getAttribute("emiCumulative"));
														pageContext.setAttribute("emiCumulative", emiCumulative);%>
										<input type="text" style="text-align:right;border:0px;"  class="width95 cumulative" id="cumulative_${status.index+1}"
											value="${emiCumulative}"/>
									</td>
									<td>
									<c:set var="amount" value="${bean.amount}"/>
										<%String amount = AIOSCommons.formatAmount(pageContext.getAttribute("amount"));
														pageContext.setAttribute("amount", amount);%>
										<input type="text" style="text-align:right;border:0px;" class="width95 balance" id="balance_${status.index+1}"
											value="${amount}"/>
									</td>
									<td>
										<input type="text" name="description" class="description width95" id="description_${status.index+1}" value="${bean.description}" style="border:0px;"/>
									</td> 
									 <td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
											<span class="ui-icon ui-icon-plus"></span>
										  </a>	
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span>
										  </a> 
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;display:none;" title="Delete Record">
											<span class="ui-icon ui-icon-circle-close"></span>
										  </a>
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
											<span class="processing"></span>
										  </a>
									</td>   
								</tr>
							</c:forEach> 
				</tbody>
			</table>
		</div> 
	</fieldset>
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div>  
	</c:when>
</c:choose>  