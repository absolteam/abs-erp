<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			store details<input type="hidden" value="${ROW_ID}" id="rowid"/>  
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="storelist" class="storeAutoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${STORE_DETAILS}" var="bean" varStatus="status">  
	     		<option value="${bean.storeId}">${bean.storeName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 store_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width30 float-left"><span>Store Number</span></li>  
   				<li class="width30 float-left"><span>Store Name</span></li>   
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${STORE_DETAILS}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="store_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.storeId}" id="storebyids_${status.index+1}">  
		     			<input type="hidden" value="${bean.storeName}" id="storebynameid_${status.index+1}"> 
 		     			<span id="storebycode_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.storeNumber}</span>
		     			<span id="storebyname_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.storeName}
		     				<span id="storeNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
	<input type="hidden" id="pageInfo" value="${requestScope.pageInfo}"/>
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.storeAutoComplete').combobox({ 
	       selected: function(event, ui){  
		       var rowid=$("#rowid").val(); 
		       $('#store_'+rowid).text($(".storeAutoComplete option:selected").text());
		       $('#storeid_'+rowid).val($(".storeAutoComplete option:selected").val()); 
		       var pageInfo = $('#pageInfo').val();
				  if(pageInfo!=null && pageInfo == "show_stock"){
					  getStockDetails(rowid);
				  }
		       $('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.store_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.store_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#storeNameselect_'+rowid).hasClass('selected')){ 
					 $('#storeNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#storeNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.store_list_li').live('dblclick',function(){ 
		  var rowtdid=$("#rowid").val();
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#store_'+rowtdid).text($('#storebynameid_'+rowid).val());
		  $('#storeid_'+rowtdid).val($('#storebyids_'+rowid).val()); 
		  var pageInfo = $('#pageInfo').val();
		  if(pageInfo!=null && pageInfo == "show_stock"){
			  getStockDetails(rowtdid);
		  }
		  $('#common-popup').dialog('close');  
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 });
  
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.store_list ul li{
		padding:3px;
	}
	.store_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>