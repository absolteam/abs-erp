<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />

<style type="text/css">
#purchase_print {
	overflow: hidden;
} 
.portlet-content{padding:1px;}
.buttons { margin:4px;}

.ui-widget-header{
	padding:4px;
}

	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}

.buttons {
	margin: 4px;
}

#filter-form td {
	padding: 5px!important;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 90%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">  
var supplierId = 0; var dueDate = "";
$(function(){  
	 
	 $('#ageingDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	 
	 $("#supplierId").multiselect();
	 		
	 $(".print-call").click(function(){
		var ageingDate = $('#ageingDate').val();
 		var suppliers = generateSupplierData(); 
 		var dueDateCheck = $('#dueDateCheck').attr('checked');
		if(suppliers != null  && suppliers!= ''){
			window.open('<%=request.getContextPath()%>/supplier_ageing_analysisreport.action?dueDateCheck='+dueDateCheck+'&ageingDate='+ ageingDate +'&suppliers='
						+suppliers,'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			alert("Please select supplier for ageing analysis.");
			return false;
		}
	 }); 
	 
	 $(".xls-download-call").click(function(){ 
		var ageingDate = $('#ageingDate').val();
 		var suppliers = generateSupplierData(); 
		if(suppliers != null  && suppliers!= ''){ 
			window.open('<%=request.getContextPath()%>/supplier_ageing_analysisreportxlx.action?dueDateCheck='+dueDateCheck+'&ageingDate='+ ageingDate +'&suppliers='
						+suppliers,'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			alert("Please select supplier for ageing analysis.");
			return false;
		} 
	});
}); 
	
function generateSupplierData(){
  	var jsonData = []; 
	$('#supplierId option:selected').each(function(){
 		jsonData.push(Number($(this).val())); 
 	}); 
	if(null != jsonData && jsonData.length > 0)
 		return JSON.stringify(jsonData);
	else
		return null;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Supplier Ageing
		</div>
		<div class="portlet-content">
			<form id="filter-form">
				<div class="width100 float-left">
					<table width="90%" > 
						<tr> 
							<td class="width40">
								<div>
									<label for="ageingDate">Ageing Date</label> 
									<input type="text" class="width30"
										name="ageingDate" class="ageingDate" id="ageingDate" readonly="readonly">
								</div>
							</td> 
						</tr>
						<tr> 
							<td class="width40">
								<div class="float-left width10">
									<input type="radio" id="invocieDateCheck" name="invoicedt" class="width5"/> 
									<label for="invocieDateCheck" style="position: relative; top: 2px;">Invoice</label> 
								</div>
								<div class="float-left width10">
									<input type="radio" id="dueDateCheck" name="invoicedt" class="width5" checked="checked"/>
									<label for="dueDateCheck" style="position: relative; top: 2px;">Due Date</label> 
								</div>
							</td> 
						</tr>
					</table> 
				</div>
				<div class="clearfix"></div> 
				<div class="width100 float-left"> 
					<div class="width50 float-left" style="margin: 5px 5px 0">
						<label class="width100" style="font-weight: bold;">Supplier</label>
						<select id="supplierId" class="supplier width100" name="supplier" multiple="multiple">
							<c:forEach var="supplier" items="${SUPPLIER_DETAILS}">
								<option value="${supplier.supplierId}">${supplier.supplierName}</option>						
							</c:forEach> 
						</select>	
					</div>  
				</div>  
			</form> 
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>