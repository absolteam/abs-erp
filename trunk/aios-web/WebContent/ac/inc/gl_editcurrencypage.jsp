<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 $(function(){
	 
     $('.cancelrec').click(function(){
    	 $.ajax({
				type:"GET", 
				url: "<%=request.getContextPath()%>/currency_master_code.action",  
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result)
				{ 
    				 $("#main-wrapper").html(result);
				} 
		});
     });

     $jquery("#editcurrencyValidate").validationEngine('attach');
     
     if($('#default_currency').val()=="true"){
    	 $('#defaultCurrency').attr('disabled',true);
     }

	 $('.savedata').click(function(){
		 
		 var curId=$("#currency").val();
		 var curEnable=$("#curEnable").attr('checked');
		 var currencyCode=$("#selCode option:selected").val();
		 var defaultCurrency=$('#defaultCurrency').attr('checked');
		 if($jquery("#editcurrencyValidate").validationEngine('validate'))
        {
		 $.ajax({
				type:"GET", 
				url:"<%=request.getContextPath()%>/gl_currency_edit_save.action",
											async : false,
											data : {
												currencyId : curId,
												currencyStatus : curEnable,
												currencyCode : currencyCode,
												defaultCurrency : defaultCurrency
											},
											dataType : "html",
											cache : false,
											success : function(result) {
												$("#main-wrapper").html(result);
												if ($('.success').html() == "") {
													$(".error").show();
													$('.success').hide();
												} else {
													$(".error").hide();
													$('.success').show();
												}
												;
											},
											error : function(result) {
												$("#main-wrapper").html(result);
												$(".error").show();
												$('.success').hide();
											}

										});
							} else {
								return false;
							}
						});
	});
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<form id="editcurrencyValidate" name="editcurrency" method="post">
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>currency</div> 

			<div class="portlet-content">
				<div class="tempresult" style="display: none;"></div>
				<div class="suc"></div>
				<div class="err"></div> 
				<div id="hrm">
					<fieldset>
						<div>
							<label class="width20"><fmt:message
									key="accounts.currencymaster.label.currencycode" />
							</label> <select name="curId" style="width: 16%;" id="selCode"
								class="validate[required]" disabled="disabled">
								<option value="">--Select--</option>
								<c:if test="${CURRENCYPOOLINFO ne null }">
									<c:forEach items="${CURRENCYPOOLINFO}" var="bean">
										<c:choose>
											<c:when
												test="${bean.currencyId eq CURRENCYINFO.currencyPoolId}">
												<option value="${bean.currencyId}" selected="selected">${bean.currencyCountry}--${bean.currencyCode}</option>
											</c:when>
											<c:otherwise>
												<option value="${bean.currencyId}">${bean.currencyCountry}--${bean.currencyCode}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label class="width20">Default</label>
							<c:if test="${CURRENCYINFO.defaultCurrency eq true}">
								<input type="checkbox" name="defaultCurrency"
									id="defaultCurrency" checked="checked" />
							</c:if>
							<c:if test="${CURRENCYINFO.defaultCurrency eq false}">
								<input type="checkbox" name="defaultCurrency"
									id="defaultCurrency" />
							</c:if>
						</div>
						<div>
							<label class="width20"><fmt:message
									key="accounts.currencymaster.label.status" />
							</label>
							<c:if test="${CURRENCYINFO.defalutFlag eq true}">
								<input type="checkbox" name="curEnable" id="curEnable"
									checked="checked" />
							</c:if>
							<c:if test="${CURRENCYINFO.defalutFlag eq false}">
								<input type="checkbox" name="curEnable" id="curEnable" />
							</c:if>
						</div>
						<input type="hidden" name="currency" id="currency"
							value="${CURRENCYINFO.currencyId}" /> <input type="hidden"
							name="default_currency" id="default_currency"
							value="${DEFAULT_CURRENCY}" />
					</fieldset>
				</div>

				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
					style="margin: 10px;">
					<div class="portlet-header ui-widget-header float-right cancelrec"
						style="cursor: pointer;">
						<fmt:message key="accounts.currencymaster.button.cancel" />
					</div>
					<div class="portlet-header ui-widget-header float-right savedata"
						style="cursor: pointer;">
						<fmt:message key="accounts.currencymaster.button.save" />
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
