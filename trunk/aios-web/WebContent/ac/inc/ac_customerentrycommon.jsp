<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
#DOMWindow {
	width: 70% !important;
	height: 53% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
}
</style>
<script type="text/javascript"> 
	$(function() {
		$('#pos-customer-discard').click(function(){ 
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		});
		
		$('#pos-customer-save').click(function(){  	
 			 var customerPopUpId = Number($('#customerPopUpId').val());
 			 var customerName = $('#customerPOSName').val();
			 var mobileNumber = $('#mobileNumber').val();
			 var customerAddress = $('#customerAddress').val();
			 var customerNature = Number($('#customerNature').val());;
			 var urlPath = "";
			 var gender = 'M'; 
			 if(customerNature <= 1){
				 if($('#fgender').attr('checked'))
					 gender = 'F';
				 
			 } 
			 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/common_customer_save.action",
										async : false,
										data : {
											firstName : customerName,
											mobile : mobileNumber,
											address : customerAddress,
											gender : gender,
 											customerId: customerPopUpId
										},
										dataType : "json",
										cache : false,
										success : function(response) {
											if (response != null) {
												$('#customerName')
														.val(
																response.personObject.customerName);
												$('#customerId')
														.val(
																response.personObject.customerId); 
												$('#DOMWindow').remove();
												$('#DOMWindowOverlay').remove();
											}
											return false;
										}
									});
							return false;
						});
 	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> customer
		</div>
		<div class="portlet-header ui-widget-header customerblock">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.common.generalinformation" />
		</div>
		<div class="width100 float-left" id="hrm">
			<div id="customer-data-div">
				<input name="customerPopUpId" id="customerPopUpId" type="hidden"
					value="${CUSTOMER_DETAIL.customerId}"> 
					<input name="customerNature" id="customerNature" type="hidden"
					value="${CUSTOMER_DETAIL.customerNature}">
				<form id="customer_details" name="customer_details"
					style="position: relative;">
					<div class="width50 float-left">
						<fieldset>
							<div>
								<label class="width30"><fmt:message
										key="accounts.customer.customerno" /><span style="color: red">*</span>
								</label>
								<c:choose>
									<c:when test="${CUSTOMER_DETAIL.customerId gt 0}">
										<input type="text" name="customerNumber" readonly="readonly"
											class="width50 validate[required]" id="customerNumber"
											value="${CUSTOMER_DETAIL.customerNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" name="customerNumber" readonly="readonly"
											class="width50 validate[required]" id="customerNumber"
											value="${CUSTOMER_NUMBER}" />
									</c:otherwise>
								</c:choose>

							</div>
							<div>
								<label class="width30">Customer Name<span
									style="color: red">*</span> </label>
								<c:set var="customerName" value="" />
								<input type="text" class="width50 validate[required]"
									id="customerPOSName" value="${CUSTOMER_DETAIL.customerName}" />
							</div>
							<c:choose>
								<c:when test="${CUSTOMER_DETAIL.customerId ne null}">
									<c:if test="${CUSTOMER_DETAIL.customerNature eq 1}">
										<div>
											<label class="width30">Gender<span style="color: red">*</span>
											</label> <span class="width15"> <label for="mgender"
												style="width: auto;">Male</label> <c:choose>
													<c:when
														test="${CUSTOMER_DETAIL.type eq 'M'}">
														<input type="radio" name="gender" checked="checked"
															class="float-left" id="mgender" />
													</c:when>
													<c:otherwise>
														<input type="radio" name="gender" class="float-left"
															id="mgender" />
													</c:otherwise>
												</c:choose> </span> <span class="width15"> <label for="fgender"
												style="width: auto;">Female</label> <c:choose>
													<c:when
														test="${CUSTOMER_DETAIL.type eq 'F'}">
														<input type="radio" name="gender" checked="checked"
															id="fgender" />
													</c:when>
													<c:otherwise>
														<input type="radio" name="gender" id="fgender" />
													</c:otherwise>
												</c:choose> </span>
										</div>
									</c:if>
								</c:when>
								<c:otherwise>
									<div>
										<label class="width30">Gender<span style="color: red">*</span>
										</label> <span class="width15"> <label for="mgender"
											style="width: auto;">Male</label> <input type="radio"
											name="gender" checked="checked" class="float-left"
											id="mgender" /> </span> <span class="width15"> <label
											for="fgender" style="width: auto;">Female</label> <input
											type="radio" name="gender" id="fgender" /> </span>
									</div>
								</c:otherwise>
							</c:choose>
							<div>
								<label class="width30">Mobile Number<span
									style="color: red">*</span> </label> <input type="text"
									class="width50 validate[required]" id="mobileNumber" value="${CUSTOMER_DETAIL.mobileNumber}"/>
							</div>
							<div>
								<label class="width30">Address<span style="color: red">*</span>
								</label>
								<textarea id="customerAddress" class="width50" style="height:85px;"
									name="customerAddress">${CUSTOMER_DETAIL.residentialAddress}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
		<div class="clearfix"></div>


		<div style="margin-top: 5px;"
			class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div
				class="portlet-header ui-widget-header float-right pos-customer-discard"
				id="pos-customer-discard">cancel</div>
			<div
				class="portlet-header ui-widget-header float-right  pos-customer-save"
				id="pos-customer-save">save</div>
		</div>
	</div>
</div>
