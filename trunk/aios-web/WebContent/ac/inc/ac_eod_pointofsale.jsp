<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="portlet-content class90" id="hrm" style="margin-top: 10px;">
	<fieldset>
		<legend>EOD Detail</legend>
		<div id="hrm" class="hastable width100">
			<c:choose>
				<c:when
					test="${POS_RECEIPTS ne null && POS_RECEIPTS ne '' && fn:length(POS_RECEIPTS)>0 }">
					<table id="hastab" class="width100">
						<thead>
							<tr>
								<th style="width: 5%;">Currency Mode</th>
								<th style="width: 5%;">Quantity</th>
								<th style="width: 5%;">Till Amount</th>
								<th style="width: 3%;">Actual Amount</th>
								<th style="width: 3%;">Variance</th>
							</tr>
						</thead>
						<tbody class="tab">
							<c:forEach var="posReceipt" items="${POS_RECEIPTS}"
								varStatus="status">
								<tr class="rowid" id="fieldrow_${status.index+1}">
									<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
									<td><input type="hidden"
										value="${posReceipt.currencyMode}" class="currencymode"
										id="currencymode_${status.index+1}" />
										${posReceipt.receiptType}</td>
									<td id="quantity_${status.index+1}" class="quantity">${posReceipt.receiptQuantity}</td>
									<td id="tillamount_${status.index+1}"
										class="tillamount right-align">${posReceipt.receivedTotal}</td>
									<td><input type="text" name="actualamount"
										id="actualamount_${status.index+1}"
										class="actualamount right-align"></td>
									<td id="variance_${status.index+1}"
										class="variance right-align"></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<span style="color: red;">No sales found on
						${requestScope.salesDate}</span>
				</c:otherwise>
			</c:choose>
		</div>
	</fieldset>
	<c:if
		test="${POS_EMPLOYEE_SERVICE ne null && POS_EMPLOYEE_SERVICE ne '' && fn:length(POS_EMPLOYEE_SERVICE)>0 }">
		<div class="portlet-content class90" id="hrm"
			style="margin-top: 10px;">
			<div class="portlet-content class90" id="hrm"
				style="margin-top: 10px;">
				<fieldset>
					<legend>Employee Service</legend>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%;">Employee</th>
									<th style="width: 5%;">Quantity</th>
									<th style="width: 3%;">Amount</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="employeeService" items="${POS_EMPLOYEE_SERVICE}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td style="display: none;">${status.index+1}</td>
										<td>${employeeService.receiptType}</td>
										<td>${employeeService.receiptQuantity}</td>
										<td>${employeeService.receivedTotal}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
		</div>
	</c:if>
</div>