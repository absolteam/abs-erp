<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.quantityerr{ font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<script type="text/javascript">
var creditDetails = "";
$(function(){
	$jquery("#creditnotesValidation").validationEngine('attach'); 
	 
	if($('#creditId').val()>0){
		$('.rowid').each(function(){
			var rowId=getRowId($(this).attr('id'));  
			$('#creditType_'+rowId).val($('#tempcreditType_'+rowId).val()); 
		});
	}

	$('#creditDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
    
	$('.sales_delivery_popup').click(function(){  
 		$('.ui-dialog-titlebar').remove();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_delivery_note_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		}); 
		return false; 
	});

	 $('#sales-delivery-popup').live('click',function(){  
    	 $('#common-popup').dialog('close'); 
     });
	

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.delrow').live('click',function(){ 
		 slidetab = $(this).parent().parent().get(0);  
     	 $(slidetab).remove();  
     	 var i=1;
     	 var trSize = $(".tab>tr").size();
     	 if(trSize > 0){
     		 $('.rowid').each(function(){   
       			var rowId=getRowId($(this).attr('id')); 
       		 	$('#lineId_'+rowId).html(i);
     			i=i+1; 
     		 });  
         }else {
        	 $('.credit_note_details').hide(); 
        	 { 
   				$('#currencyId').val('');
   				$('#currencyCode').val('');
  				$('#customerName').val('');
  				$('#salesDeliveryNumber').val('');
  				$('#customerNumber').val('');
 				$('#customerId').val('');
  			} 
         } 
	 }); 

	$('.save').click(function(){  
		creditDetails = "";
		var creditId = Number($('#creditId').val());
		var creditNumber = $('#creditNumber').val();
		var creditDate = $('#creditDate').val();
		var currencyId = Number($('#currencyId').val());
		var customerId = Number($('#customerId').val());
	 	var description=$('#description').val(); 
	 	if($jquery("#creditnotesValidation").validationEngine('validate')){
 			creditDetails = getCreditDetails();   
 			if(creditDetails!=""){
				$.ajax({
					type:"POST",
					url:'<%=request.getContextPath()%>/credtnote_save.action',
				 	async: false,
				 	data:{  date: creditDate, creditId: creditId, creditNumber: creditNumber, currencyId: currencyId, customerId: customerId,
					 	 	description: description, creditDetails: creditDetails
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){ 
	 					 if(response.returnMessage=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getcredit_listdetails.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove();  
										$("#main-wrapper").html(result);
										if (creditId > 0)
											$('#success_message').hide().html("Record updated").slideDown(1000);
										else
											$('#success_message').hide().html("Record created").slideDown(1000);
										$('#success_message').delay(3000).slideUp();
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(3000).slideUp();
							 return false;
						 }
					}
				 });
			}else{
				$('#page-error').hide().html("Please enter credit details.").slideDown(1000);
				 $('#page-error').delay(3000).slideUp();
				 return false;
			} 
		} 
		else{
		 return false;
	 	}
	 });

	var getCreditDetails=function(){
		 var productArray=new Array();  
		 var inwardArray=new Array(); 
		 var inwardedArray = new Array();
 		 var descArray=new Array(); 
		 var deliveryArray = new Array();
		 var creditDetailArray = new Array();
		 var creditDetail="";
		 $('.rowid').each(function(){
		 	 var rowId = getRowId($(this).attr('id')); 
			 var productId = $('#productid_'+rowId).val(); 
			 var inwardQuantity = Number($('#inwardQuantity_'+rowId).val());
			 var inwardedQuantity  = Number($('#inwardedQuantity_'+rowId).text());
			 var description = $('#description_'+rowId).val();  
			 var salesDeliveryDetailId = Number($('#salesDeliveryDetailId_'+rowId).val());  
			 var creditDetaiId = Number(0);  
 			 if(inwardQuantity > 0){  
 				productArray.push(productId);  
 				inwardArray.push(inwardQuantity);
 				inwardedArray.push(inwardedQuantity);
 				deliveryArray.push(salesDeliveryDetailId);  
 				creditDetailArray.push(creditDetaiId);  
 				if(description!="")
 					descArray.push(description);
 				else
 					descArray.push("##");
			 }  
		  });  
		  for(var j=0; j < inwardArray.length; j++){  
			  creditDetail += inwardArray[j]+"__"+descArray[j]+"__"+deliveryArray[j]+"__"+creditDetailArray[j]+"__"+inwardedArray[j];
			if(j==inwardArray.length-1){   
			} 
			else{
				creditDetail += "#@";
			}
		 }   
 		 return creditDetail;
	 };

	 $('#discard').click(function(){  
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcredit_listdetails.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#main-wrapper").html(result); 
			}
		 });
	 });

	 $('.inwardQuantity').live('change',function(){   
		 $('.quantityerr').remove(); 
 	 	 var rowId = getRowId($(this).attr('id')); 
 	 	 var inwardQty =  Number($('#inwardQuantity_'+rowId).val());
 	 	 var inwardedQty =  Number($('#inwardedQuantity_'+rowId).text());
 	 	 var deliveredQuantity = Number($('#deliveredQuantity_'+rowId).text());  
  	 	 if ((inwardQty + inwardedQty) > deliveredQuantity) {
 	 		$(this).parent().append("<span class='quantityerr' id=inverror_"+rowId+">Inward Qty should be lt or eq delivery qty.</span>"); 
 	 		$('#inwardQuantity_'+rowId).val($('#tempInwardQuantity_'+rowId).val());
 	 	 } 
 		 return false;
 	 });

 	 if($('#creditId').val()){  
 		$('.credit_note_details').show();
 	 }
	 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function loadDeliveryDetails(salesDeliveryNoteId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_sales_delivery_detail.action", 
	 	async: false,  
	 	data: {salesDeliveryNoteId: salesDeliveryNoteId},
	 	dataType: "json",
	    cache: false,
		success:function(response){ 
			{ 
  				$('#currencyId').val(response.salesDeliveryNoteVO.currencyId);
  				$('#currencyCode').val(response.salesDeliveryNoteVO.currencyCode);
 				$('#customerName').val(response.salesDeliveryNoteVO.customerName);
 				$('#salesDeliveryNumber').val(response.salesDeliveryNoteVO.referenceNumber);
 				$('#customerNumber').val(response.salesDeliveryNoteVO.customer.customerNumber);
				$('#customerId').val(response.salesDeliveryNoteVO.customer.customerId);
 			} 
			$('.tab').html('');
 			var counter = 0; 
 			var invoiceQuantity = '';
 			var unitRate = 0; 
 			$(response.salesDeliveryNoteVO.salesDeliveryDetailVOs)
			.each(
					function(index) {
						counter += 1; 
						var inwardedQuantity = '';
			 			var inwardQuantity = '';
						if(null!=response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].invoiceQuantity) { 
							invoiceQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].invoiceQuantity;
			 	 		} 
						unitRate = Number(response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].unitRate).toFixed(2);
						if(null!=response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardedQuantity) { 
							inwardedQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardedQuantity;
							inwardQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardQuantity;
 			 	 		} 
						$('.tab')
						.append(
						
							"<tr id='fieldrow_"+counter+"' class='rowid'>"
								+"<td id='lineId_"+counter+"' style='display:none;'>"+counter+"</td>"
								+"<td>"
									+"<input type='hidden' id='productid_"+counter+"'"
									+" value='"+response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].product.productId+"' name='productId'/>"
									+"<span id='product_"+counter+"'>"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].product.productName
									+"</span>"
								+"</td>" 
								+"<td>" 
									+"<input type='hidden' id='shelfId_"+counter+"'"
									+" value='"+response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].shelf.shelfId+"' name='shelfId'/>"
									+"<span id=store_"+counter+">"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].shelf.shelfName
									+"</span>" 
								+"</td>"
								+"<td id='deliveredQuantity_"+counter+"'>"+ 
									response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].deliveredQuantity
								+"</td>"
								+"<td id='invoiceQuantity_"+counter+"'>"+ 
									invoiceQuantity
								+"</td>"
								+"<td>"
									+"<span style='float: right;' id='unitRate_"+counter+"'>"+
										unitRate
									+"</span>" 
								+"</td>"  
								+"<td>"+
									inwardedQuantity
								+"</td>"
								+"<td>"
									+"<input type='text' id='inwardQuantity_"+counter+"' name='inwardQuantity' value='"+inwardQuantity+"' class='inwardQuantity'>"
									+"<input type='hidden' id='tempInwardQuantity_"+counter+"' name='tempInwardQuantity' value='"+inwardQuantity+"' class='tempInwardQuantity'>"
								+"</td>"
								+"<td>"
									+"<input type='text' id='description_"+counter+"' name='description'>"
								+"</td>"
								+"<td>"
								   +"<a class='btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow' id='DeleteImage_"+counter+"'"
								   		+"style='cursor:pointer;' title='Delete Record'>"
										+"<span class='ui-icon ui-icon-circle-close'></span>"
								   +"</a>" 
									+"<input type='hidden' id='salesDeliveryDetailId_"+counter+"' value='"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].salesDeliveryDetailId
									+"' name='salesDeliveryDetailId'>"
									+"<input type='hidden' id='creditDetaiId_"+counter+"' name='creditDetaiId'>"
								+"</td>"
							+"</tr>"
					);
			});
			$('.credit_note_details').show(); 
		}
	});  
	return false;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>credit note</div>	
		  <form name="creditnotesValidation" id="creditnotesValidation" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div> 
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="height: 80px">  
							<div style="display: none;">
								<label class="width30">Customer Ref<span style="color: red;">*</span></label>
 								<input type="text" readonly="readonly" name="customerNumber" id="customerNumber" class="width50" value="${CREDIT_INFO.customer.customerNumber}"/> 
							</div> 
							<div>
								<label class="width30">Customer Name<span style="color: red;">*</span></label>
								<input type="hidden" id="customerId" name="customerId" value="${CREDIT_INFO.customer.customerId}"/>
								<c:set var="customerName"/>
								<c:choose>	
									<c:when test="${CREDIT_INFO.customer.personByPersonId ne null && CREDIT_INFO.customer.personByPersonId ne ''}">
										<c:set var="customerName" value="${CREDIT_INFO.customer.personByPersonId.firstName} ${CREDIT_INFO.customer.personByPersonId.lastName}"/>
									</c:when>
									<c:when test="${CREDIT_INFO.customer.company ne null && CREDIT_INFO.customer.company ne ''}">
										<c:set var="customerName" value="${CREDIT_INFO.customer.company.companyName}"/>
									</c:when>
									<c:when test="${CREDIT_INFO.customer.cmpDeptLocation ne null && CREDIT_INFO.customer.cmpDeptLocation ne ''}">
										<c:set var="customerName" value="${CREDIT_INFO.customer.cmpDeptLocation.company.companyName}"/>
									</c:when>
								</c:choose>
								<input type="text" readonly="readonly" name="customerName" id="customerName" class="width50" value="${customerName}"/> 
							</div> 
							<div>
								<label class="width30" for="description">Description</label> 
								<textarea class="width50" id="description" readonly="readonly"></textarea>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="height: 80px">
 							<div>
								<label class="width30">Credit Number<span style="color: red;">*</span></label> 
								<input type="hidden" name="creditId" id="creditId" value="${CREDIT_INFO.creditId}"/>
								<c:choose>
									<c:when test="${CREDIT_INFO.creditId ne null && CREDIT_INFO.creditId ne ''}">
										<input type="text" readonly="readonly" name="creditNumber" id="creditNumber" class="width50" value="${CREDIT_INFO.creditNumber}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="creditNumber" id="creditNumber" class="width50" value="${requestScope.creditNumber}"/>
									</c:otherwise>
								</c:choose> 
 							</div>  
							<div>
								<label class="width30" for="creditDate">Date<span style="color: red;">*</span></label> 
								<c:choose>
									<c:when test="${CREDIT_INFO.date ne null && CREDIT_INFO.date ne ''}">
										<c:set var="date" value="${CREDIT_INFO.date}"/>  
										<% 
											String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
										%>
										<input type="text" readonly="readonly" name="creditDate" class="width50 validate[required]" value="<%=date%>"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="creditDate"  class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div style="display: none;">
								<label class="width30">Delivery Number<span style="color: red;">*</span></label>  
 							 	<input type="text" readonly="readonly" name="salesDeliveryNumber" id="salesDeliveryNumber" class="width50"/>
								<c:if test="${CREDIT_INFO.creditId eq null || CREDIT_INFO.creditId eq ''}">
									<span class="button">
										<a style="cursor: pointer;" id="sales_delivery" class="btn ui-state-default ui-corner-all sales_delivery_popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
								</c:if> 
							</div>   
							<div>
								<label class="width30">Currency<span style="color: red;">*</span></label>  
								<input type="hidden" readonly="readonly" name="currencyId" id="currencyId" value="${CREDIT_INFO.currency.currencyId}"/>
								<input type="text" readonly="readonly" name="currencyCode" id="currencyCode" class="width50" value="${CREDIT_INFO.currency.currencyPool.code}"/>
							</div>
						</fieldset>
					</div> 
			</div>  
		</div>  
		<div class="clearfix"></div>  
		<div class="portlet-content class90 credit_note_details" style="margin-top:10px;display: none;" id="hrm"> 
 			<fieldset>
				<legend>Credit Details<span
									class="mandatory">*</span></legend>
 					<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
 					<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div> 
					<div id="hrm" class="hastable width100">  
						 <input type="hidden" name="childCount" id="childCount" value="${fn:length(CREDIT_DETAIL_INFO)}"/>  
						 <table id="hastab" class="width100"> 
							<thead>
							   <tr>   
									<th class="width10">Product</th> 
								    <th style="width:5%">Store</th> 
								    <th style="width:5%">Deliverd Qty</th>  
								    <th style="width:5%">Invoiced Qty</th>  
								    <th style="width:5%">Unit Rate</th> 
								    <th style="width:5%">Inwarded Qty</th> 
									<th style="display: none;">Inward Qty</th>  
								    <th style="display: none;">Description</th>  
									<th style="display: none;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
							<tbody class="tab">  
								<c:if test="${CREDIT_DETAIL_INFO ne null && CREDIT_DETAIL_INFO ne '' && fn:length(CREDIT_DETAIL_INFO)>0}">	
									<c:forEach var="bean" items="${CREDIT_DETAIL_INFO}" varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td> 
											<td id="${status.index+1}">  
												<input type="hidden" name="productId" id="productId_${status.index+1}" 
													value="${bean.productId}"/>
												${bean.productName}
											</td>
											<td>
												<input type="hidden" name="shelfId" id="shelfId_${status.index+1}" 
													value="${bean.shelfId}"/>
												${bean.shelfName}
											</td>  
											<td id="deliveredQuantity_${status.index+1}">
												${bean.deliveryQuantity}
											</td> 
											<td id="invoiceQuantity_${status.index+1}">
												${bean.invoiceQuantity}
											</td>  
											<td>
												${bean.unitRate}
											</td> 
											<td id="inwardedQuantity_${status.index+1}">
												${bean.inwardedQuantity}
											</td> 
											<td style="display: none;">  
												<input type="text" name="inwardQuantity" id="inwardQuantity_${status.index+1}" class="inwardQuantity"
													value="${bean.returnQuantity}"/> 
												<input type="hidden" name="tempInwardQuantity" id="tempInwardQuantity_${status.index+1}" class="tempInwardQuantity"
													value="${bean.returnQuantity}"/> 
											</td> 
											<td style="display: none;">  
												<input type="text" name="description" style="display: none;" id="description_${status.index+1}" value="${bean.description}"/> 
											</td>  
											<td style="display: none;">
												<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span>
											    </a>	
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="cursor:pointer;display:none;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											   </a> 
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											   </a>
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
													<span class="processing"></span>
											   </a>
											   <input type="hidden" name="salesDeliveryDetailId" id="salesDeliveryDetailId_${status.index+1}" value="${bean.salesDeliveryDetailId}"/> 
												<input type="hidden" id="creditDetaiId_${status.index+1}" name="creditDetaiId" value="${bean.creditDetailId}">
											</td> 
										</tr>
									</c:forEach>  
									</c:if>
						 	</tbody>
						</table>
					</div> 
			</fieldset>
		</div> 
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard" id="discard" >Close</div> 
		</div>
		<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div>  
		<div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
  	</form>
  </div> 
</div>