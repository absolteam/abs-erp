<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%> 
<%@page import="java.text.DecimalFormat;" %>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			loan details
</div>
 <div id="hrm">  
 <span style="display: none;" id="emipage">${requestScope.PAGE}</span>
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="banklist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${LOAN_LIST}" var="bean" varStatus="status">
	     		<c:set var="loanamount" value="${bean.amount}"/>
	     		<c:set var="emiFirstDuedate" value="${bean.emiFirstDuedate}"/> 
	     		<%
	     			DecimalFormat decimalFormat=new DecimalFormat();
	     			String loanamount=decimalFormat.format(pageContext.getAttribute("loanamount"));
	     			loanamount = AIOSCommons.formatAmount(pageContext.getAttribute("loanamount"));
	     			pageContext.setAttribute("loanamount", loanamount); 
	     			String date = DateFormat.convertDateToString(pageContext.getAttribute("emiFirstDuedate").toString());
					pageContext.setAttribute("date", date);
	     		%>
	     		<option value="${bean.loanId}|${bean.currency.currencyPool.code}|${loanamount}|${date}|${bean.leadDay}
	     			|${bean.loanType}|${bean.bankAccount.accountNumber}|${bean.emiFrequency}|${bean.rateType}|${bean.rateVariance}">${bean.loanNumber}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 loan_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width30 float-left"><span>Loan Number</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${LOAN_LIST}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="loan_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.loanId}" id="loanbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.loanNumber}" id="loannumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.currency.currencyPool.code}" id="loancurrencyid_${status.index+1}"> 
		     			<c:set var="loanamount" value="${bean.amount}"/>
			     		<%
			     			DecimalFormat decimalFormat=new DecimalFormat();
			     			String loanamount=decimalFormat.format(pageContext.getAttribute("loanamount"));
			     			loanamount = AIOSCommons.formatAmount(pageContext.getAttribute("loanamount"));
			     			pageContext.setAttribute("loanamount", loanamount); 
			     		%>
		     			<input type="hidden" value="${loanamount}" id="loanamountid_${status.index+1}">  
		     			 <c:set var="emiFirstDuedate" value="${bean.emiFirstDuedate}"/>
		     			<%String date = DateFormat.convertDateToString(pageContext.getAttribute("emiFirstDuedate").toString());
											pageContext.setAttribute("date", date);%>
		     			<input type="hidden" value="${date}" id="duedateid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.leadDay}" id="leaddayid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.loanType}" id="loantypeid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.bankAccount.accountNumber}" id="accountnumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.emiFrequency}" id="emifrequencyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.rateType}" id="ratetpeid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.rateVariance}" id="ratevarianceid_${status.index+1}"> 
		     			<span id="loanbynumber_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.loanNumber} 
		     				<span id="loanNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript">
 var emifrequency="";
 var ratetype="";
 var ratevariance="";
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var loandetails=$(this).val();
		       var loanarray=loandetails.split("|");
		       $('#loanId').val(loanarray[0]);  
		       $('#loanNumber').val($(".autoComplete option:selected").text());
		       $('#currency').val(loanarray[1]);
		       $('#approvalAmount').val(loanarray[2]); 
		       $('#emiDueDate').val(loanarray[3]);
		       $('#leadDay').val(loanarray[4]); 
		       if(loanarray[5]=='P')
		    	  $('#loanType').val("Predictable");
		       else
		    	  $('#loanType').val("Unpredictable");
		       $('#accountNumber').val(loanarray[6]); 
		       if(loanarray[7]=='M')
		    		 emifrequency="Monthly";
		    	 else if(loanarray[7]=='D')
		    		 emifrequency="Daily";
		    	 else if(loanarray[7]=='W')
		    		 emifrequency="Weekly";
		    	 else if(loanarray[7]=='Q')
		    		 emifrequency="Quaterly";
		    	 else if(loanarray[7]=='H')
		    		 emifrequency="SemiAnually";
		    	 else if(loanarray[7]=='Y')
		    		 emifrequency="Anually";
		       $('#emiFrequency').val(emifrequency); 
		       if(loanarray[8]=='F')
		    	   ratetype="Flat";
		       else if(loanarray[8]=='D')
		    	   ratetype="Diminishing";
		       ('#rateType').val(ratetype); 
		       if(loanarray[9]=='F')
		    		 emifrequency="Fixed";
		       else if(loanarray[9]=='V')
		    		 emifrequency="Vary";
		       else if(loanarray[9]=='M')
		    		 emifrequency="Mixed";
		       $('#rateVariance').val(ratevariance); 
		       $('#common-popup').dialog("close");  
	   } 
	}); 
	 
	 $('.loan_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.loan_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#loanNameselect_'+rowid).hasClass('selected')){ 
					 $('#loanNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#loanNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.loan_list_li').live('dblclick',function(){
		 	emifrequency="";ratetype="";ratevariance="";
			var tempvar="";var idarray = ""; var rowid="";
			 currentId=$(this);  
			 tempvar=$(currentId).attr('id');   
			 idarray = tempvar.split('_'); 
			 rowid=Number(idarray[1]);  
			 $('#loanId').val($('#loanbyid_'+rowid).val());
			 $('#loanNumber').val($('#loannumberid_'+rowid).val());  
			 $('#currency').val($("#loancurrencyid_"+rowid).val());
	         $('#approvalAmount').val($("#loanamountid_"+rowid).val()); 
	         $('#emiDueDate').val($("#duedateid_"+rowid).val());
	         $('#leadDay').val($("#leaddayid_"+rowid).val());
	         if($('#loantypeid_'+rowid).val()=='P')
	        	 $('#loanType').val("Predictable");
		     else
		    	 $('#loanType').val("Unpredictable");
	    	 $('#accountNumber').val($('#accountnumberid_'+rowid).val());
	    	 if($('#emifrequencyid_'+rowid).val()=='M')
	    		 emifrequency="Monthly";
	    	 else if($('#emifrequencyid_'+rowid).val()=='D')
	    		 emifrequency="Daily";
	    	 else if($('#emifrequencyid_'+rowid).val()=='W')
	    		 emifrequency="Weekly";
	    	 else if($('#emifrequencyid_'+rowid).val()=='Q')
	    		 emifrequency="Quaterly";
	    	 else if($('#emifrequencyid_'+rowid).val()=='H')
	    		 emifrequency="SemiAnually";
	    	 else if($('#emifrequencyid_'+rowid).val()=='Y')
	    		 emifrequency="Anually";
	    	 $('#emiFrequency').val(emifrequency);  
	    	 if($('#ratevarianceid_'+rowid).val()=='F')
	    		 ratetype="Flat";
	    	 else if($('#ratevarianceid_'+rowid).val()=='D')
	    		 ratetype="Diminishing";  
	    	 if($('#ratetpeid_'+rowid).val()=='F')
	    		 ratevariance="Fixed";
	    	 else if($('#ratetpeid_'+rowid).val()=='V')
	    		 ratevariance="Vary"; 
	    	 else if($('#ratetpeid_'+rowid).val()=='M')
	    		 ratevariance="Mixed";
	    	 $('#rateVariance').val(ratetype);  
	    	 $('#rateType').val(ratevariance);
	         if($('#emipage').html()=="emi"){
	        	getEMISchedule();
	         }
			 $('#common-popup').dialog('close'); 
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		}); 
	 
		var getEMISchedule=function(){
			var loanId=$('#loanId').val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_emischedule.action", 
			 	async: false,   
			 	data:{loanId:loanId},
			    dataType: "html",
			    cache: false,
			    success:function(result){   
			    	$('.emilinedetails').html(result);     
			    }
			});
		};
 });
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.loan_list ul li{
		padding:3px;
	}
	.loan_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>