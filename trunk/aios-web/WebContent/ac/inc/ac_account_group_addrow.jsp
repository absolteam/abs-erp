<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><span
		id="codeCombination_${requestScope.rowId}"></span>
		<span class="button float-right" id="tempCombID_${requestScope.rowId}"> <a
			style="cursor: pointer;" id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all codecombination-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
		<input type="hidden" name="combinationId" id="combinationId_${requestScope.rowId}" 
		value="" />
	</td> 
	
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;" title="Working"> <span
			class="processing"></span> </a> <input type="hidden"
		name="accountGroupDetailId" id="accountGroupDetailId_${requestScope.rowId}" /></td>
</tr>