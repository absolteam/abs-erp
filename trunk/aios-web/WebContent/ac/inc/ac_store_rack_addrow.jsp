<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rackrowid"
	id="rackfieldrow_${rowId}">
	<td style="display: none;" id="racklineId_${rowId}">${rowId}</td>
	<td><input type="text" class="width90 shelfName"
			name="shelfName" id="shelfName_${rowId}" />
	</td>
	<td><select name="rackType" id="rackType_${rowId}"
		class="width90">
			<option value="">Select</option>
			<c:forEach var="rackType" items="${RACK_TYPES}">
				<option value="${rackType.key}">${rackType.value}</option>
			</c:forEach>
	</select>
	</td>
	<td><input type="text" class="width90 rackdimension"
		style="border: 0px;" name="rackdimension"
		id="rackdimension_${rowId}" /></td>
	<td style="width: 0.01%;" class="opn_td"
		id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackaddData"
		id="RackAddImage_${rowId}"
		style="display: none; cursor: pointer;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackeditData"
		id="RackEditImage_${rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackdelrow"
		id="RackDeleteImage_${rowId}"
		style="display: none; cursor: pointer;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="rackid" value="0"
		id="rackid_${rowId}" />
		 <input type="hidden" name="rackaisleId" value="0"
				id="rackaisleId_${rowId}" />
		</td>
</tr>