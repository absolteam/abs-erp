<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<input type="hidden" id="nonStockProductIds" value="${requestScope.NON_STOCK_PRODUCTS}"/>
<div>
	<c:forEach var="stock" items="${NON_STOCK_PRODUCTS}">
		<input class="stockerrorList" type="hidden" id="${stock}" value="${stock}"/>
	</c:forEach>
</div>