<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
var receiptId = 0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	$('#common-popup').dialog('open');  
	$($($('#common-popup').parent()).get(0)).css('top',0);
	$($($('#common-popup').parent()).get(0)).css('overflow','hidden'); 
	$('#common-popup').css('overflow','hidden'); 
	$('#PettyCashPopup').dataTable({ 
		"sAjaxSource": "get_receipt_reference.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Receipt_Id", "bVisible": false},
			{ "sTitle": "Ref.Type"},
			{ "sTitle": "Voucher No"},
			{ "sTitle": "Invoice No"}, 
			{ "sTitle": "Amount"}, 
			{ "sTitle": "Description"},
			{ "sTitle": "Created By"} 
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	
	//init datatable
	oTable = $('#PettyCashPopup').dataTable();
	 
	/* Click event handler */
	$('#PettyCashPopup tbody tr').live('dblclick', function () {  
	      aData = oTable.fnGetData( this ); 
	      callReceiptReference(aData[0], aData[1], aData[2], aData[4]);
	      $('#pettycash-common-popup').trigger('click');
	      return false;
	});
	
	/* Click event handler */
	$('#PettyCashPopup tbody tr').live('click', function() {
		if ($(this).hasClass('row_selected')) {
			$(this).addClass('row_selected');
			} else {
			oTable.$('tr.row_selected').removeClass('row_selected');
			$(this).addClass('row_selected');
			}
		});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>receipt reference</div>	 	 
		<div class="portlet-content">  
		 	<div id="pettycash_receipt_list">
				<table class="display" id="PettyCashPopup"></table>
			</div>  
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div class="portlet-header ui-widget-header float-right"
			id="pettycash-common-popup">close</div>
	</div>
</div>