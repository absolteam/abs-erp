 <%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:forEach var="bean" varStatus="status" items="${RECEIVE_DETAIL_INFO}"> 
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td id="lineId_${status.index+1}">${status.index+1}</td> 
		<td id="productcode_${status.index+1}"> 
			${bean.product.code}
			<input type="hidden" name="productId" id="productId_${status.index+1}" value="${bean.product.productId}"/> 
		</td>
		<td id="productname_${status.index+1}"> 
			${bean.product.productName}
		</td> 
		<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
		<td id="quantity_${status.index+1}" class="quantity">
			${bean.receiveQty}
		</td> 
		<td id="amount_${status.index+1}" class="amount" style="text-align: right;">
			<c:set var="unitrate" value="${bean.unitRate}"/>
			<%=AIOSCommons.formatAmount(pageContext.getAttribute("unitrate"))%> 
			<input type="hidden" id="amountval_${status.index+1}" value="${bean.unitRate}"/>
		</td>   
		<td id="total_${status.index+1}" style="text-align: right;"> 
			 <c:set var="total" value="${bean.receiveQty * bean.unitRate}"/>
			 <%=AIOSCommons.formatAmount(pageContext.getAttribute("total"))%> 
		</td>  
		<td> 
			<input type="text" name="linedescription" id="linedescription_${status.index+1}"/> 
		</td> 
		<td style="display:none"> 
			<input type="hidden" id="invoiceLineId_${status.index+1}"/>
		</td> 
		<td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
			  </a>	
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
					<span class="ui-icon ui-icon-wrench"></span>
			  </a> 
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
					<span class="ui-icon ui-icon-circle-close"></span>
			  </a>
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
					<span class="processing"></span>
			  </a>
		</td>   
	</tr>
</c:forEach> 
<script type="text/javascript">
$(function(){ 
	calculateTotalInvoice();
});
</script>