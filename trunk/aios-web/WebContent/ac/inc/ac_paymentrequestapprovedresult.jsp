<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#PaymentRequestList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_approved_payment_request.action",

		"aoColumns" : [ { 
			"mDataProp" : "paymentReqNo"
		 }, {
			"mDataProp" : "date"
		 }, {
			"mDataProp" : "requestedBy"
		 }, {
			"mDataProp" : "createdBy"
		 }], 
	}); 
	oTable.fnSort( [ [0,'desc'] ] );	
	
	/* Click event handler */
	$('#PaymentRequestList tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		paymentRequestPopup(aData.paymentRequestId, aData.paymentReqNo, aData.date);  
		$('#paymentrequest-common-popup').trigger('click');
		return false;
  	});

	$('#PaymentRequestList tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') )
			  $(this).addClass('row_selected');
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
  	      }
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>payment request</div>	 	 
		<div class="portlet-content"> 
  			 	<div id="paymentrequest_list">
					<table id="PaymentRequestList" class="display">
						<thead>
							<tr>
								<th>Reference</th> 
								<th>Date</th>  
								<th>Requested By</th>
								<th>Created By</th>  
							</tr>
						</thead> 
					</table> 
 			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="paymentrequest-common-popup" >close</div> 
 			</div>
		</div>
	</div>
</div>