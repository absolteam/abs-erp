<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.rowid {
	height: 23px;
}
</style>
<fieldset>
	<legend>
		Purchase Details<span class="mandatory">*</span>
	</legend>
	<div id="line-error" class="response-msg error ui-corner-all width90"
		style="display: none;"></div>
	<div id="warning_message"
		class="response-msg notice ui-corner-all width90"
		style="display: none;"></div>
	<div id="hrm" class="hastable width100">
		<input type="hidden" name="childCount" id="childCount" value="" />
		<table id="hastab" class="width100">
			<thead>
				<tr>
					<th style="width: 5%">Item Type</th>
					<th style="width: 5%">Product</th>
					<th style="width: 5%; display: none;">UOM</th>
					<th style="width: 8%;">Packaging</th> 
					<th style="width: 5%">Quantity</th>
					<th style="width: 4%">Base Qty</th>
					<th style="width: 5%">Unit Rate</th>
					<th style="width: 5%">Total Amount</th>
					<th style="width: 5%">Description</th>
					<th style="width: 2%">MORE</th>
					<th style="width: 0.01%;"><fmt:message
							key="accounts.common.label.options" /></th>
				</tr>
			</thead>
			<tbody class="tab">
				<c:forEach var="bean" items="${QUOTATION_LINE_DETAIL}"
					varStatus="status">
					<tr class="rowid" id="fieldrow_${status.index+1}">
						<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
						<td>${bean.itemTypeStr}</td>
						<td class="product" id="product_${status.index+1}">
							<span class="width60 float-left">${bean.product.productName}</span> 
							<span class="width10 float-right" id="unitCode_${status.index+1}"
									style="position: relative;">${bean.product.lookupDetailByProductUnit.accessCode}</span>
							<input type="hidden"
							id="productid_${status.index+1}" style="border: 0px;"
							value="${bean.product.productId}" /> <input type="hidden"
							name="storeId" id="storeId_${status.index+1}" /> <input
							type="hidden" name="shelfId" id="shelfId_${status.index+1}" /></td>
						<td id="uom_${status.index+1}" class="uom" style="display: none;">
						</td>
						<td><select name="packageType"
							id="packageType_${status.index+1}" class="packageType">
								<option value="-1">Select</option>
								<c:forEach var="packageType" items="${bean.productPackageVOs}">
									<c:choose>
										<c:when test="${packageType.productPackageId gt 0}">
											<optgroup label="${packageType.packageName}"
												style="color: #c85f1f;">
												<c:forEach var="packageDetailType"
													items="${packageType.productPackageDetailVOs}">
													<option style="margin-left: 10px; color: #000;"
														value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option>
												</c:forEach>
											</optgroup>
										</c:when> 
									</c:choose>
								</c:forEach>
						</select> <input type="hidden" id="temppackageType_${status.index+1}"
							value="-1" /></td>
						<td><input type="text"
							class="width96 packageUnit validate[optional,custom[number]]"
							id="packageUnit_${status.index+1}" value="${bean.packageUnit}" />
						</td>
						<td><input type="hidden" name="quantity"
							id="quantity_${status.index+1}" class="width96 quantity"
							value="${bean.totalUnits}" />
							<span id="baseDisplayQty_${status.index+1}">${bean.totalUnits}</span>
						</td>
						<td><input type="text" name="unitrate"
							id="unitrate_${status.index+1}" class="width96 unitrate"
							value="${bean.unitRate}" /> <input type="text"
							style="display: none;" name="totalAmountH" readonly="readonly"
							id="totalAmountH_${status.index+1}" class="totalAmountH"
							value="${bean.unitRate * bean.totalUnits}" />
							<input type="hidden" id="basePurchasePrice_${status.index+1}" value="${bean.unitRate}"/>
						</td>
						<td class="totalamount" id="totalamount_${status.index+1}"
							style="text-align: right;"></td>
						<td><input type="text" name="linedescription"
							id="linedescription_${status.index+1}" />
						</td>
						<td>
							<div id="moreoption_${status.index+1}"
								style="cursor: pointer; text-align: center; background-color: #3ADF00;"
								class="portlet-header ui-widget-header float-left moreoption">
								MORE OPTION</div>
						</td> 
						<td style="display: none;"><input type="hidden"
							id="purchaseLineId_${status.index+1}" /> <input type="hidden"
							id="requisitionLineId_${status.index+1}"/> 
							<input type="text"
							class="width96 batchNumber" id="batchNumber_${status.index+1}" />
							<input type="text" readonly="readonly"
							class="width96 expiryBatchDate"
							id="expiryBatchDate_${status.index+1}" /></td>
						<td style="width: 0.01%;" class="opn_td"
							id="option_${status.index+1}"><a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
							id="AddImage_${status.index+1}"
							style="display: none; cursor: pointer;" title="Add Record"> <span
								class="ui-icon ui-icon-plus"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
							id="EditImage_${status.index+1}"
							style="display: none; cursor: pointer;" title="Edit Record">
								<span class="ui-icon ui-icon-wrench"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
							id="DeleteImage_${status.index+1}" style="cursor: pointer;"
							title="Delete Record"> <span
								class="ui-icon ui-icon-circle-close"></span> </a> <a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
							id="WorkingImage_${status.index+1}" style="display: none;"
							title="Working"> <span class="processing"></span> </a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</fieldset>
<script type="text/javascript">
	$(function() {
		var totalPurchase = 0;
		$jquery(".unitrate,.totalAmountH").number(true, 3);
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
			$('#totalamount_' + rowId).text($('#totalAmountH_' + rowId).val());
			var totalamount = Number($jquery('#totalAmountH_' + rowId).val());
			totalPurchase = Number(totalPurchase + totalamount);
		});
		$jquery('.totalPurchaseAmountH').val(totalPurchase);
		$('#totalPurchaseAmount').text($('.totalPurchaseAmountH').val());
	});
</script>