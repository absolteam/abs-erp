<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowidcharges" id="fieldrowcharges_${ROW_ID}">
	<td id="chargesLineId_${ROW_ID}">${ROW_ID}</td>
	<td><select name="chargesTypeId" id="chargesTypeId_${ROW_ID}"
		class="chargestypeid width70">
			<option value="">Select</option>
			<c:forEach var="TYPE" items="${CHARGES_TYPE}">
				<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;" id="CHARGES_TYPE_${ROW_ID}"
			class="btn ui-state-default ui-corner-all chargestype-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td> 
	<td><input type="text" name="charges" id="charges_${ROW_ID}"
		class="charges validate[optional,custom[number]] width98" style="text-align: right;"></td>
	<td><input type="text" name="description"
		id="description_${ROW_ID}" class="description width98"></td>
	<td style="width: 1%;" class="opn_td" id="optioncharges_${ROW_ID}">
		<a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
		id="AddImagecharges_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
		id="EditImagecharges_${ROW_ID}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
		id="DeleteImagecharges_${ROW_ID}"
		style="cursor: pointer; display: none;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImagecharges_${ROW_ID}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="customerQuotationChargesId"
		id="customerQuotationChargesId_${ROW_ID}" /></td>
</tr>
