<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
 <style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
}); 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			category
		</div> 
		<form id="productcategory_validation" style="position: relative;">
			<div class="width100" id="hrm">
				<input type="hidden" id="productCategoryId" name="productCategoryId" value="${PRODUCT_CATEGORY.productCategoryId}"/>
				<div class="edit-result">
					<div class="width48 float-right">
						<c:choose>
								<c:when test="${PRODUCT_CATEGORY.specialPos eq true}">
									<fieldset class="specialPOSDiv" style="min-height: 185px;">
										<div class='float-left'>
										<span class="fileinput-preview">  
													<c:choose>
														<c:when test="${PRODUCT_CATEGORY.categoryPic ne null && PRODUCT_CATEGORY.categoryPic ne ''}">
															<img name="preview" id="preview" height="125" width="125"
														src="data:image/png;base64,${PRODUCT_CATEGORY.categoryPic}">
														</c:when>
														<c:otherwise>
															<img alt="No Image" name="preview" id="preview" height="125"
											width="125" src="./images/No_Image.jpg">  
														</c:otherwise> 
														</c:choose>
												</span> 
											 
										</div> 
									</fieldset>
								</c:when> 
							</c:choose> 
						
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 185px;">
							<div>
								<label for="categoryName">Category Name </label> <input type="text" disabled="disabled"
									name="categoryName" id="categoryName"
									value="${PRODUCT_CATEGORY.categoryName}"
									class="width50 validate[required]" />
							</div>
							<div>
								<label for="parentCategory">Parent Category </label> <select
									name="parentCategory" id="parentCategory" class="width51" disabled="disabled">
									<option value="">Select</option>
									<c:forEach var="parentCategory" items="${PARENT_CATEGORY}">
										<option value="${parentCategory.productCategoryId}">${parentCategory.categoryName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempparentCategory"
									value="${PRODUCT_CATEGORY.parentCategoryId}" />
							</div>
							<div>
								<label for="activeStatus">Status </label> 
								<c:choose>
									<c:when test="${PRODUCT_CATEGORY.status eq true}">
										<input type="checkbox" name="activeStatus" disabled="disabled" id="activeStatus" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="activeStatus" disabled="disabled" id="activeStatus"/>
									</c:otherwise>
								</c:choose> 
							</div>
							<div>
								<label for="specialPos">Special POS </label> 
								<c:choose>
									<c:when test="${PRODUCT_CATEGORY.specialPos eq true}">
										<input type="checkbox" name="specialPos" id="specialPos" disabled="disabled" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="specialPos" disabled="disabled" id="specialPos"/>
									</c:otherwise>
								</c:choose> 
							</div>
							<c:choose>
								<c:when test="${PRODUCT_CATEGORY.specialPos eq true}">
									<div class="specialPOSDiv">
										<label for="publicName">Public Name<span
											style="color: red;">*</span>
										</label> <input type="text" name="publicName" id="publicName"
											value="${PRODUCT_CATEGORY.publicName}" class="width50" />
									</div>
									<div class="specialPOSDiv">
										<label for="kitchenPrint">Printer<span
											style="color: red;">*</span>
										</label> 
											<label for="kitchenPrint" style="float: none;">Kitchen</label>
											<c:choose>
												<c:when test="${PRODUCT_CATEGORY.kitchenPrint ne null && PRODUCT_CATEGORY.kitchenPrint eq true}">
													<input type="radio" name="radioField" id="kitchenPrint" checked="checked"/>
												</c:when>
												<c:otherwise>
													<input type="radio" name="radioField" id="kitchenPrint"/>
												</c:otherwise>
											</c:choose>
											<label for="barPrint" style="float: none;">Bar Print</label>
											<c:choose>
												<c:when test="${PRODUCT_CATEGORY.barPrint ne null && PRODUCT_CATEGORY.barPrint eq true}">
													<input type="radio" name="radioField" id="barPrint" checked="checked"/>
												</c:when>
												<c:otherwise>
													<input type="radio" name="radioField" id="barPrint"/>
												</c:otherwise>
											</c:choose>
											
									</div>
								</c:when> 
							</c:choose> 
							<div class="divpadding">
								<label for="description">Description</label>
								 <span
									class="width60 spanfont">${PRODUCT_CATEGORY.description}</span>     
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div> 
			</div>
		</form>
	</div>
</div>
<div class="clearfix"></div>