<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${INVOICE_NOTE_BYID.invoiceNumber}
	</td> 
	<td>
		<c:set var="invdate" value="${INVOICE_NOTE_BYID.date}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("invdate").toString())%> 
	</td> 
	<td>
		 ${INVOICE_NOTE_BYID.totalInvoiceQty}
	</td>
	<td style="text-align: right;">
		<c:set var="totalamt" value="${INVOICE_NOTE_BYID.totalInvoiceAmount}"/>
		<%= AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
	</td>
	<td>
		${INVOICE_NOTE_BYID.description}
	</td>   
	<td style="display:none;">
		<input type="hidden" id="invoiceId_${rowId}" value="${INVOICE_NOTE_BYID.invoiceId}"/> 
	</td> 
	 <td class="opn_td" id="option_${rowId}"> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatapy" id="EditImage_${rowId}" style="cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowpy" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a> 
	</td>  
</tr>