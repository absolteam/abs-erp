<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend>Transfer Returns</legend>
	<div>
		<c:forEach var="ACTIVE_RETURNS" items="${MATERIAL_TRANSFER_INFO}">
			<label for="activeReturnNumber_${ACTIVE_RETURNS.materialTransferId}" class="width10">
				<input type="checkbox" name="activeReturnNumber" class="activeReturnNumber" id="activeReturnNumber_${ACTIVE_RETURNS.materialTransferId}"/>
				${ACTIVE_RETURNS.referenceNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultTransferSelect($('.activeReturnNumber').attr('id'));
	$('.activeReturnNumber').click(function(){   
		validateTransferInfo($(this));   
	});
});
</script>