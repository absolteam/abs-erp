<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
var currencyConversions = [];
$(function(){
	$jquery("#currencyConversionValidation").validationEngine('attach'); 
	$jquery(".conversionRate").number(true,3); 
	manupulateLastRow();
	
	$('.currencyConversion,.conversionRate').live('change',function(){
		var rowId = getRowId($(this).attr('id'));  
 		triggerAddRow(rowId);
	});
	
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).text(i);
			 i=i+1; 
 		 });   
  		 return false;
	 }); 
	 
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
 		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/currency_conversion_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 
	
	$('.saveConversion').click(function(){   
		currencyConversions = getCurrencyConversions();  
 		if(currencyConversions != null && currencyConversions != ""){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/currency_conversion_save.action", 
			 	async: false, 
			 	data:{	
			 			currencyConversions: JSON.stringify(currencyConversions)
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(($.trim(response.returnMessage)=="SUCCESS")) 
						 currencyConversionDiscard("Record created.");
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown();
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				} 
			}); 
		}else{
			$('#page-error').hide().html("Please enter currency conversions.").slideDown();
			$('#page-error').delay(2000).slideUp();
			return false;
		} 
		return false;
	 });
	
	var getCurrencyConversions = function(){
		currencyConversions = []; 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var conversionCurrency = Number($('#currencyConversion_'+rowId).val());  
			var conversionRate = Number($jquery('#conversionRate_'+rowId).val()); 
 			if(typeof conversionCurrency != 'undefined' && conversionCurrency > 0 && conversionRate > 0){
 				currencyConversions.push({
					"conversionCurrency" : conversionCurrency,
					"conversionRate" : conversionRate,
					"currencyConversionId" : Number(0)
				});
			} 
		});  
		return currencyConversions;
	 }; 
	
	$('.cancelConversion').click(function(){  
		 currencyConversionDiscard("");
		 return false;
	 });
});
function currencyConversionDiscard(message){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_currency_conversions.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) { 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function triggerAddRow(rowId) {
	var currencyConversion = $('#currencyConversion_' + rowId).val();
	var conversionRate = $('#conversionRate_' + rowId).val(); 
	var nexttab = $('#fieldrow_' + rowId).next();
	if (currencyConversion != null && currencyConversion != "" && conversionRate != null
			&& conversionRate != "" && $(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
	}
	
}
function getRowId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<form name="newFields" id="currencyConversionValidation" style="position: relative;">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;"
					class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Currency Conversion
			</div>  
			<div class="portlet-content width100 float-left" id="hrm"> 
				<div id="page-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="width48 float-left">
					<div style="padding: 5px;">
						<label class="width20">Functional Currency</label>  
						<span style="position: relative; top: 7px;">${DEFAULT_CURRENCY.code} (${DEFAULT_CURRENCY.countryName})</span>
					</div>
				</div>
			</div>  
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top: 10px;"> 
				<div id="hrm"> 
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width60">
							<thead class="chrome_tab">
								<tr>
									<th style="width:1%">S.NO</th>
									<th style="width: 8%;">Conversion Currency</th>
									<th class="width10">
										Conversion Rate
									</th>
									<th style="width: 0.5%;"><fmt:message
											key="accounts.currencymaster.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab" >
								<c:forEach var="i" begin="1" end="2" step="1"> 
									<tr class="rowid" id="fieldrow_${i}">
										<td>
											<span id="lineId_${i}">${i}</span>
 										<td>
 											<select name="currencyConversion" id="currencyConversion_${i}"
 												class="currencyConversion">
 												<option value="">Select</option>
 												<c:forEach var="nonFnCurrency" items="${NON_DEFAULT_CURRENCY}">
 													<option value="${nonFnCurrency.currencyId}">${nonFnCurrency.code}--${nonFnCurrency.countryName}</option>
 												</c:forEach> 
 											</select>
 										</td>
										<td>
											<input type="text" class="conversionRate" id="conversionRate_${i}" style="text-align: right;"/>
										</td>
										<td ><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											style="cursor: pointer; display: none;" title="Add Record"> <span
												class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${i}" style="cursor: pointer; display: none;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											style="display: none; cursor: pointer;" title="Working"> <span
												class="processing"></span> </a></td> 
									</tr>
								</c:forEach>
							</tbody>
						</table>
 					</div> 
				</div>
			</div>
		</form>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right cancelConversion"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right saveConversion"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.save" />
			</div> 
		</div>
		<div style="display: none;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="portlet-header ui-widget-header float-left addrows"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.addrow" />
			</div>
		</div>
	</div> 
</div>