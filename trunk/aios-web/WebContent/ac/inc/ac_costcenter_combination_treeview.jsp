<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){ 
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	}); 
 

	$('.ui-autocomplete-input').live('focus',function(){  
		$(this).val(''); 
	});

	 
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	           var combtext=$(".autoComplete option:selected").text().trim(); 
	           var result = combtext.lastIndexOf("[")+1;
	           var result1= combtext.lastIndexOf("]");
	           result=combtext.substring(result,result1);
	           var combarray=result.split(".");
	           segmentId=combarray.length; 
	           combinationId=$(this).val();  
	           var e=event.which; 
	           if(e==13){
	        	 if(segmentId>1){
	        		 setCombination(combinationId,combtext);
		 	         $('#codecombination-popup').dialog('close');
		         }else{
		        	 $('.selectionerror').hide().html("You can't create transaction with selected account.").slideDown(); 
		        	 return false;
			     } 
		       }else{
		    	   makeTreeview(combtext); 
			   }
	       }
		}); 
	
		
	$('#codecombination-popup').dialog('open');
	$('#codecombination-popup').css('overflow','hidden');  
 	$($($('#codecombination-popup').parent()).get(0)).css('top',0);
	$('.companys').live('click',function(){  
		$('.selectionerror').hide().html("You can't create transaction with Company.").slideDown(); 
	});
	$('.costscenter').live('click',function(){   
		$('.selectionerror').hide(); 
	}); 
	 
	$('.costscenter').live('dblclick',function(){
		$('.selectionerror').hide();  
		setCombination($($($(this).children()).get(0)).val(),$(this).text().trim());
		$('#codecombination-popup').dialog('close');
	});
	$('.treecancel').click(function(){ 
		$('#codecombination-popup').dialog('close');
	});
});
function makeTreeview(combtext) { 
	loadTree();
	combtext=combtext.replace(/[\s\n\r]+/g, ' ' ).trim(); 
	var textpos=combtext.lastIndexOf('[');
	combtext=combtext.substring(0, textpos);
	$('#tree li a').each(function(){
		var treevalue=$(this).text().replace(/[\s\n\r]+/g, ' ' ).trim(); 
		var treepos=treevalue.lastIndexOf('[');
		treevalue=treevalue.substring(0, treepos);
		if(treevalue==combtext){  
			$(this).addClass('hover');  
			var parentsize=Number($(this).parents('li').size()-1);    
			for(var i=0;i<parentsize;i++){ 
				$($($(this).parents('li').children('div')).get(i)).removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			} 
			$(this).parents().show(); 
		} 
	});  
	$('.ui-autocomplete-input').val('');
}
function loadTree(){   
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/costcenter_combination_reloadview.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$("#tree").empty();
						$('#sidetreecontrol').html(result);
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="ac.combination.combinationtreeview" />
		</div>
		<div class="portlet-content">
			<div id="temp-result" style="display: none;"></div>
			<div id="page-error" class="response-msg error ui-corner-all width90"
				style="display: none;"></div>
			<div id="page-success"
				class="response-msg success ui-corner-all width90"
				style="display: none;"></div>
			<div class="width100 float-left" id="hrm"
				style="height: 400px; overflow-y: auto;">
				<div class="float-right  width48">
					<select name="comb_tree" id="combinationSelectTree"
						class="autoComplete">
						<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}"
							varStatus="status1">
							<option value=""></option>
							<option value="${bean.combinationId}">
								${bean.accountByCompanyAccountId.account}
								[${bean.accountByCompanyAccountId.code}]
								<c:choose>
									<c:when
										test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
										<c:forEach var="costbean" items="${bean.costVos}"
											varStatus="cstatus1">
											<option value="${costbean.combinationId}">
												${costbean.accountByCostcenterAccountId.account}
												[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
												<c:choose>
													<c:when
														test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
														<c:forEach var="naturalbean"
															items="${costbean.naturalVos}" varStatus="nstatus1">
															<option value="${naturalbean.combinationId}">
																${naturalbean.accountByNaturalAccountId.account}
																[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]
																<c:choose>
																	<c:when
																		test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
																		<c:forEach var="analysisbean"
																			items="${naturalbean.analysisVos}"
																			varStatus="astatus1">
																			<option value="${analysisbean.combinationId}">
																				${analysisbean.accountByAnalysisAccountId.account}
																				[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]
																				<c:choose>
																					<c:when
																						test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
																						<c:forEach var="buffer1bean"
																							items="${analysisbean.buffer1Vos}"
																							varStatus="b1status1">
																							<option value="${buffer1bean.combinationId}">
																								${buffer1bean.accountByBuffer1AccountId.account}
																								[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]
																								<c:choose>
																									<c:when
																										test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																										<c:forEach var="buffer2bean"
																											items="${buffer1bean.buffer2Vos}"
																											varStatus="b2status">
																											<option value="${buffer2bean.combinationId}">${buffer2bean.accountByBuffer2AccountId.account}
																												[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]
																											</option>
																										</c:forEach>
																									</c:when>
																								</c:choose>
																							</option>
																						</c:forEach>
																					</c:when>
																				</c:choose>
																			</option>
																		</c:forEach>
																	</c:when>
																</c:choose>
															</option>
														</c:forEach>
													</c:when>
												</c:choose>
											</option>
										</c:forEach>
									</c:when>
								</c:choose>
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="float-left  width50">
					<div id="main">
						<div id="sidetree">
							<div id="sidetreecontrol"></div>
							<c:choose>
								<c:when
									test="${COMBINATION_TREE_VIEW ne null && COMBINATION_TREE_VIEW ne ''}">
									<ul id="tree">
										<li><a style="font-weight: bold;" class="createcombi">Combination</a>
											<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}"
												varStatus="status">
												<li><a id="company_${status.index+1}_${status.index+1}"
													class="combination_ companys"> <input type="hidden"
														id="companyId_${status.index+1}_${status.index+1}"
														value="${bean.combinationId}" />
														${bean.accountByCompanyAccountId.account}
														[${bean.accountByCompanyAccountId.code}]</a> <c:choose>
														<c:when
															test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
															<ul>
																<c:forEach var="costbean" items="${bean.costVos}"
																	varStatus="cstatus">
																	<li><a class="combination_ costscenter"
																		id="costcenter_${status.index+1}_${cstatus.index+1}">
																			<input type="hidden"
																			id="costcenterId_${status.index+1}_${cstatus.index+1}"
																			value="${costbean.combinationId}" />
																			${costbean.accountByCostcenterAccountId.account}
																			[${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
																	</a></li>
																</c:forEach>
															</ul>
														</c:when>
													</c:choose></li>
											</c:forEach></li>
									</ul>
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="float-right width48">
					<span class="selectionerror"></span>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div>
			<div class="portlet-header ui-widget-header float-right treecancel"
				style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
		</div>
	</div>
</div>