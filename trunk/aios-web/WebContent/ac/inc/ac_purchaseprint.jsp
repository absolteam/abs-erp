<%@page
	import="com.sun.org.apache.xalan.internal.xsltc.compiler.Pattern"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Purchase Order</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<span class="title-heading"><span>${THIS.companyName}</span> </span> 
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>PURCHASE ORDER</span> </span>
	</div>
	<div style="clear: both;"></div>
	<div
		style="height: 150px; border-style: solid; border-width: medium; -moz-border-radius: 6px; -webkit-border-radius: 6px; border-radius: 6px;"
		class="width98 float-left">
		<c:set var="float" value="float-left"/>
		<c:if test="${PURCHASE_ORDER_PRINT.supplier ne null && PURCHASE_ORDER_PRINT.supplier ne ''}">
			<c:set var="float" value="float_right"/>
		</c:if>
		<div class="width40 ${float}">
			<div class="width98 div_text_info">
				<span class="float_left left_align width40 text-bold">P.O.NO.
					<span class="float_right">:</span> </span> <span
					class="span_border left_align width50 text_info"
					style="display: -moz-inline-stack;"><span
					style="color: #CC2B2B;">${PURCHASE_ORDER_PRINT.purchaseNumber}</span>
				</span>
			</div>
			<div class="width98 div_text_info">
				<span class="float_left left_align width40 text-bold">DATE<span
					class="float_right">:</span> </span> <span
					class="span_border left_align width50 text_info"
					style="display: -moz-inline-stack;"> <c:set var="date"
						value="${PURCHASE_ORDER_PRINT.date}" /> <%=DateFormat.convertDateToString(pageContext.getAttribute(
					"date").toString())%> </span>
			</div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width40 text-bold">SHPPING TERM<span
					class="float_right">:</span> </span> <span
					class="span_border left_align width50 text_info"
					style="display: -moz-inline-stack;"> 
					<c:choose>
						<c:when test="${PURCHASE_ORDER_PRINT.lookupDetail ne null && PURCHASE_ORDER_PRINT.lookupDetail ne ''}">
							${PURCHASE_ORDER_PRINT.lookupDetail.displayName}
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose>
					
				</span>
			</div>  
		</div>
		<c:if test="${PURCHASE_ORDER_PRINT.supplier ne null && PURCHASE_ORDER_PRINT.supplier ne '' }"> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">SUPPLIER <span
					class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> <c:choose>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.person ne null && PURCHASE_ORDER_PRINT.supplier.person ne ''}">
							${PURCHASE_ORDER_PRINT.supplier.person.firstName} ${PURCHASE_ORDER_PRINT.supplier.person.lastName} 
							 <c:set var="supplierType" value="Person" />
						</c:when>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.company ne null && PURCHASE_ORDER_PRINT.supplier.company ne ''}">
							${PURCHASE_ORDER_PRINT.supplier.company.companyName}
							<c:set var="supplierType" value="Company" />
						</c:when>
						<c:otherwise>
							${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyName}
							<c:set var="supplierType" value="Company" />
						</c:otherwise>
					</c:choose> </span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">ADDRESS
					<span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> <c:choose>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.person ne null && PURCHASE_ORDER_PRINT.supplier.person ne ''}">
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.person.postboxNumber  ne null && PURCHASE_ORDER_PRINT.supplier.person.postboxNumber ne '' }">
									P.O.Box: ${PURCHASE_ORDER_PRINT.supplier.person.postboxNumber}
									<c:choose>
										<c:when
											test="${PURCHASE_ORDER_PRINT.supplier.person.residentialAddress  ne null && PURCHASE_ORDER_PRINT.supplier.person.residentialAddress ne '' }">
											, ${PURCHASE_ORDER_PRINT.supplier.person.residentialAddress}  
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when test="${PURCHASE_ORDER_PRINT.supplier.company.companyPobox ne null && PURCHASE_ORDER_PRINT.supplier.company.companyPobox ne ''}">
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.company.companyPobox ne null && PURCHASE_ORDER_PRINT.supplier.company.companyPobox ne ''}">
									P.O.Box: ${PURCHASE_ORDER_PRINT.supplier.company.companyPobox}
									<c:choose>
										<c:when
											test="${PURCHASE_ORDER_PRINT.supplier.company.companyAddress ne null && PURCHASE_ORDER_PRINT.supplier.company.companyAddress ne ''}">
											, ${PURCHASE_ORDER_PRINT.supplier.company.companyAddress} 
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPobox ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPobox ne ''}">
									P.O.Box: ${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPobox}
									<c:choose>
										<c:when
											test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyAddress ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyAddress ne ''}">
											, ${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyAddress} 
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose> </span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">MOBILE
					NO <span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> <c:choose>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.person ne null && PURCHASE_ORDER_PRINT.supplier.person ne ''}">
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.person.mobile ne null && PURCHASE_ORDER_PRINT.supplier.person.mobile ne ''}">
									${PURCHASE_ORDER_PRINT.supplier.person.mobile} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.company ne null && PURCHASE_ORDER_PRINT.supplier.company ne ''}">
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.company.companyPhone ne null && PURCHASE_ORDER_PRINT.supplier.company.companyPhone ne ''}">
									${PURCHASE_ORDER_PRINT.supplier.company.companyPhone} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:when
							test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company ne ''}">
							<c:choose>
								<c:when
									test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPhone ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPhone ne ''}">
									${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyPhone} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose> </span>
			</div>
			<div class="width100 div_text_info">
				<c:choose>
					<c:when
						test="${PURCHASE_ORDER_PRINT.supplier.person ne null && PURCHASE_ORDER_PRINT.supplier.person ne ''}">
						<c:choose>
							<c:when
								test="${PURCHASE_ORDER_PRINT.supplier.person.telephone ne null && PURCHASE_ORDER_PRINT.supplier.person.telephone ne ''}">
								<span class="float_left left_align width28 text-bold">TEL
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">${PURCHASE_ORDER_PRINT.supplier.person.telephone}</span>
							</c:when>
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">TEL
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:when
						test="${PURCHASE_ORDER_PRINT.supplier.company ne null && PURCHASE_ORDER_PRINT.supplier.company ne ''}">
						<c:choose>
							<c:when
								test="${PURCHASE_ORDER_PRINT.supplier.company.companyFax ne null && PURCHASE_ORDER_PRINT.supplier.company.companyFax ne ''}">
								<span class="float_left left_align width28 text-bold">FAX
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">${PURCHASE_ORDER_PRINT.supplier.company.companyFax}</span>
							</c:when>
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">FAX
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:when
						test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company ne ''}">
						<c:choose>
							<c:when
								test="${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyFax ne null && PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyFax ne ''}">
								<span class="float_left left_align width28 text-bold">FAX
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">${PURCHASE_ORDER_PRINT.supplier.cmpDeptLocation.company.companyFax}</span>
							</c:when>
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">FAX
									NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info"
									style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>
					</c:when>
				</c:choose>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">CONTACT
					<span class="float_right">:</span> </span> <span
					class="span_border left_align width65 text_info"
					style="display: -moz-inline-stack;"> <c:choose>
						<c:when
							test="${PURCHASE_ORDER_PRINT.personByPurchaseManager ne null && PURCHASE_ORDER_PRINT.personByPurchaseManager ne ''}">
							${PURCHASE_ORDER_PRINT.personByPurchaseManager.firstName} ${PURCHASE_ORDER_PRINT.personByPurchaseManager.lastName}
						</c:when>
						<c:otherwise>-N/A-</c:otherwise>
					</c:choose> </span>
			</div>
		</div>
		</c:if>
	</div>
	<div
		style="margin-top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; -webkit-border-radius: 6px; border-radius: 6px;"
		class="width98 float-left">
		<table class="width100">
			<tr>
				<th style="width: 2%;">S.NO:</th>
				<th class="width10">CODE</th>
				<th class="width30">DESCRIPTION</th>
				<th class="width10">UNITS</th>
				<th class="width10">QUANTITY</th>
				<th class="width10">PRICE</th>
				<th class="width10">TOTAL AMOUNT</th>
			</tr>
			<tbody>
				<c:choose>
					<c:when
						test="${PURCHASE_DETAIL_PRINT ne null && PURCHASE_DETAIL_PRINT ne ''}">
						<c:set var="totalAmount" value="0" />
						<tr style="height: 25px;">
							<c:forEach begin="0" end="6" step="1">
								<td />
							</c:forEach>
						</tr>
						<c:forEach items="${PURCHASE_DETAIL_PRINT}"
							var="PURCHASE_DETAIL" varStatus="status">
							<tr>
								<td>${status.index+1}</td>
								<td>${PURCHASE_DETAIL.product.code}</td>
								<td>${PURCHASE_DETAIL.product.productName}</td>
								<td>${PURCHASE_DETAIL.unitCode}</td>
								<td>${PURCHASE_DETAIL.quantity}</td>
								<td class="right_align">${PURCHASE_DETAIL.unitRateStr}</td>
								<td class="right_align">${PURCHASE_DETAIL.totalRateStr}</td> 
							</tr>
						</c:forEach>
						<c:if test="${fn:length(PURCHASE_DETAIL_PRINT)<5}">
							<c:set var="emptyLoop"
								value="${5 - fn:length(PURCHASE_DETAIL_PRINT)}" />
							<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
								<tr style="height: 25px;">
									<c:forEach begin="0" end="6" step="1">
										<td />
									</c:forEach>
								</tr>
							</c:forEach>
						</c:if>
						<tr>
							<td colspan="6" class="bottomTD left_align"
								style="font-weight: bold;">TOTAL AMOUNT</td>
							<td class="right_align bottomTD" style="font-weight: bold;">
								${PURCHASE_ORDER_PRINT.totalPurchaseAmountStr}</td>
						</tr>
						<tr>

							<td colspan="5" class="left_align"><span
								style="font-weight: bold;"> AMOUNT IN WORDS : </span>${PURCHASE_ORDER_PRINT.amountInWords}</td>
							<td style="font-weight: bold;">
								<table>
									<tr>
										<td style="border: 0px;">NET AMOUNT</td>
									</tr>
								</table>
							</td>
							<td style="font-weight: bold;">
								<table>
									<tr>
										<td style="border: 0px;" class="right_align">${PURCHASE_ORDER_PRINT.totalPurchaseAmountStr}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</c:when>
				</c:choose>
			</tbody>
		</table>
	</div>
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">PAYMENT
			TERMS <span class="float_right">:</span> </span> <span
			class="span_border left_align text_info"
			style="width: 77%; display: -moz-inline-stack;"> <c:choose>
				<c:when
					test="${PURCHASE_ORDER_PRINT.creditTerm.name ne null && PURCHASE_ORDER_PRINT.creditTerm.name ne ''}">
						${PURCHASE_ORDER_PRINT.creditTerm.name}
					</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose> </span>
	</div>
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">PO.DESCRIPTION<span
			class="float_right">:</span> </span> <span
			class="span_border left_align text_info"
			style="width: 77%; display: -moz-inline-stack;"> <c:choose>
				<c:when
					test="${PURCHASE_ORDER_PRINT.description ne null && PURCHASE_ORDER_PRINT.description ne ''}">
					${PURCHASE_ORDER_PRINT.description}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose> </span>
	</div>
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">DELIVERY
			ADDRESS<span class="float_right">:</span> </span> <span
			class="span_border left_align text_info"
			style="width: 77%; display: -moz-inline-stack;"> <c:choose>
				<c:when
					test="${PURCHASE_ORDER_PRINT.deliveryAddress ne null && PURCHASE_ORDER_PRINT.deliveryAddress ne ''}">
					${PURCHASE_ORDER_PRINT.deliveryAddress}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose> </span>
	</div>
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">DELIVERY
			DATE<span class="float_right">:</span> </span> <span
			class="span_border left_align text_info"
			style="width: 77%; display: -moz-inline-stack;"> <c:choose>
				<c:when
					test="${PURCHASE_ORDER_PRINT.deliveryDate ne null && PURCHASE_ORDER_PRINT.deliveryDate ne ''}">
					<c:set var="deliveryDate"
						value="${PURCHASE_ORDER_PRINT.deliveryDate}" />
					<%=DateFormat.convertDateToString(pageContext
							.getAttribute("deliveryDate").toString())%>
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose> </span>
	</div>
	
	<div class="width100 float_left div_text_info"
		style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span>
			</div>
			<div class="width40 float_left">
				<span>Recommended by:</span>
			</div>
			<div class="width30 float_left">
				<span>Approved by:</span>
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left">
				<span class="span_border width70"
					style="display: -moz-inline-stack;"></span>
			</div>
		</div>
	</div>
</body>
</html>