<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#customerfieldset>div {
	padding: 3px;
}
</style>
<script type="text/javascript">
	$(function() { 
		
		$('#driverId').change(function(){   
			$('#driverMobile').val($(this).val().split('|')[1]);
		});
		
		$('#freeDelivery').change(function(){   
			if($(this).attr('checked') == true){
				$('#deliveryCharges').hide();
				$('#deliveryCharges').val('');
			} 
			else
				$('#deliveryCharges').show();
		});  
	});
</script>
<div class="width100" style="overflow: hidden;">

	<div class="mainhead portlet-header ui-widget-header">
		<span style="display: none;"
			class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> delivery
		details
	</div>
	<div class="width100 float-left" id="hrm">
		<div id="customer-data-div">
			<input name="customerId" id="customerId" type="hidden">
			<form id="customer_details" name="customer_details"
				style="position: relative;">
				<div class="width50 float-right">
					<fieldset style="min-height: 110px;" id="customerfieldset">
						<div>
							<label class="width30">Customer Name </label> <span
								class="width50">${CUSTOMER_INFO.customerName}</span>
						</div>
						<div>
							<label class="width30">Mobile </label> <span class="width50"
								id="customerMobile">${CUSTOMER_INFO.mobile}</span>
						</div>
						<div>
							<label class="width30">Address</label> <span class="width50"
								id="customerLocation">${CUSTOMER_INFO.residentialAddress}</span>
						</div>
					</fieldset>
				</div>
				<div class="width50 float-left">
					<fieldset style="min-height: 110px;">
						<div>
							<label class="width30">Driver </label> <select id="driverId"
								name="dirver" class="width50">
								<option value="">Select</option>
								<c:forEach var="dirver" items="${PERSONS_LIST}">
									<option value="${dirver.personId}|${dirver.mobile}">${dirver.firstName}
										${dirver.lastName}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class="width30">Driver Mobile </label> <input type="text"
								name="driverMobile" id="driverMobile" class="width50" />
						</div>
						<div>
							<label class="width30">Free Delivery </label> <input
								type="checkbox" name="freeDelivery" id="freeDelivery"
								checked="checked" /> <input type="text" name="deliveryCharges"
								id="deliveryCharges"
								style="width: 44% !important; display: none; text-align: right;" />
						</div>
					</fieldset>
				</div>
				<div class="clearfix"></div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>


	<div style="margin-top: 5px;"
		class="savediscard portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div
			class="portlet-header ui-widget-header float-right pos-customerorder-discard"
			id="pos-customerorder-discard">cancel</div>
		<div
			class="portlet-header ui-widget-header float-right  pos-customerorder-dispatch"
			id="pos-customerorder-dispatch">dispatch</div>
	</div>
</div>
