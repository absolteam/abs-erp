<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Petty Cash</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
.flag {background: url(./images/checked.png); background-repeat: no-repeat; height: 30px; width:20px; float: left;}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PETTY CASH SUMMARY</span></span></div> 
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>Voucher No</th>   
			    <th>Date</th>  
			    <th>Type</th> 
			    <th>Employee</th> 
			    <th>Expense Account</th> 
			    <th>Narration</th> 
			    <th>Cash In</th> 
			    <th>Cash Out</th> 
			    <th>Expense</th> 
			    <th>Remaining Balance</th>  
			    <th>Bill No</th> 
			    <th>Settle With</th> 
			    <th>Settled</th> 
			    <th>Created By</th> 
			    <th>Closed</th> 
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="14" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${PETTY_CASH_PRINT}" var="PETTY_CASH" varStatus="status"> 
					<tr> 
						<td>${PETTY_CASH.pettyCashNo}</td>
						<td>${PETTY_CASH.pettyCashDate}</td> 
						<td>${PETTY_CASH.pettyCashTypeName}</td>
						<td>${PETTY_CASH.employeeName}</td>
						<td>
							${PETTY_CASH.expenseAccount}
						</td>
						<td>${PETTY_CASH.description}</td>
						<td>
							 <c:if test="${PETTY_CASH.cashIn ne null && PETTY_CASH.cashIn ne '' && PETTY_CASH.cashIn ne 0}">
							 	<c:set var="cashin" value="${PETTY_CASH.cashIn}"/>
							 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("cashin"))%>
							 </c:if>
						</td>
						<td>
							<c:if test="${PETTY_CASH.cashOut ne null && PETTY_CASH.cashOut ne '' && PETTY_CASH.cashOut ne 0}">
							 	<c:set var="cashout" value="${PETTY_CASH.cashOut}"/>
							 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("cashout"))%>
							 </c:if>
						</td>
						<td>
							<c:if test="${PETTY_CASH.expenseAmount ne null && PETTY_CASH.expenseAmount ne '' && PETTY_CASH.expenseAmount ne 0}">
							 	<c:set var="expenseAmount" value="${PETTY_CASH.expenseAmount}"/>
							 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("expenseAmount"))%>
							 </c:if>
						</td>
						<td>
							<c:if test="${PETTY_CASH.remainingBalance ne null && PETTY_CASH.remainingBalance ne '' && PETTY_CASH.remainingBalance ne 0}">
							 	<c:set var="remainingBalance" value="${PETTY_CASH.remainingBalance}"/>
							 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("remainingBalance"))%>
							 </c:if>
						</td> 
						<td>${PETTY_CASH.invoiceNumber}</td>
						<td>${PETTY_CASH.pettyCash.pettyCashNo}</td>
						<td>
							<c:if test="${PETTY_CASH.settelmentFlag}">
								<span class="flag"></span>
							</c:if>
						</td>
						<td>${PETTY_CASH.personByCreatedBy.firstName} ${PETTY_CASH.personByCreatedBy.lastName}</td>
						<td>
							<c:if test="${PETTY_CASH.closedFlag}">
								<span class="flag"></span>
							</c:if>
						</td>  
					</tr>
				</c:forEach> 
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="14" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach> 
			</tbody>
		</table>
		<table class="width30" style="margin-top :10px;">
			<tbody> 
				<tr>
					<td style="text-align: left;">Petty Cash</td>
					<td>
						${PETTYCASH_SUMMARAY.cashOutFormat}
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">Total Expense</td>  
					<td>
						${PETTYCASH_SUMMARAY.expenseAmountFormat}
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">Carry Forward</td>  
					<td>
						${PETTYCASH_SUMMARAY.cashForwaredStr}
					</td>
				</tr>
				<tr>
					<td style="text-align: left;">Remaining Balance</td> 
					<td>
						${PETTYCASH_SUMMARAY.remainingBalanceFormat}
					</td>
				</tr>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div> 
	</body>
</html>