<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.invoice_lines {
	display: none;
}
</style>
<script type="text/javascript">
var tempid="";
var slidetab="";
var invoiceLineDetails=""; 
var receiveDetails = "";
$(function(){
	$jquery("#invoicevalidation").validationEngine('attach'); 
	 
	if(Number($('#invoiceId').val())>0){
		//calculateTotalInvoice(); 
		$('#invoiceDate').datepick({ 
		     showTrigger: '#calImg'});  
	     $('.invoice_lines').show();
	     $('#status').val($('#tempstatus').val());
	     $('.rowid').each(function(){
	    	var rowid = getRowId($(this).attr('id')); 
	    	var receiveCId = $('#receiveId_'+rowid).val(); 
	    	$('#activeGRNNumber_'+receiveCId).attr('checked', true); 
	    	$('#grnLabel_'+receiveCId).css('color','#37b134'); 
		 });
	}else{
		$('#invoiceDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	}
	
	$('.activeGRNNumberUpdate').click(function(){   
		validateGRNInfo($(this));   
	});

	  $('#supplier-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
	  
	  $('#invoiceAmount').change(function(){
		 var invoiceAmount = Number($(this).val());
		 var totalInvoice = Number($('#totalInvoice').val());
		 if(invoiceAmount > totalInvoice){
			 $('#invoiceAmount').removeClass('validate[required,custom[number]]').addClass('validate[required,custom[number],max['+totalInvoice+']]'); 
		 }else{
			 $('#invoiceAmount').removeClass('validate[required,custom[number],max['+totalInvoice+']]').addClass('validate[required,custom[number]]'); 
		 } 
	  });
	   
	$('.supplierinvoice-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,   
		 	data:{pageInfo : "invoice"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0); 
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.invoiceQty').live('change',function(){ 
		$('.quantityerr').remove(); 
		var rowId = getRowId($(this).attr('id')); 
		var invoiceQty = Number($('#invoiceQty_'+rowId).val().trim());
		var receiveQty = Number($('#receiveqty_'+rowId).text().trim());
		var unitrate = Number($('#unitRate_'+rowId).text().replace(/,/g, '').trim());  
		var invoicedqty = Number($('#curinvoicedqty_'+rowId).text().trim());
		$('#total_'+rowId).text(Number(invoiceQty * unitrate).toFixed(2));
		var invoicedQty =  Number($('#invoicedqty_'+rowId).text().trim());  
		var totalAvailedQty = Number((invoiceQty + invoicedQty) - invoicedqty);    
		if(totalAvailedQty > receiveQty) { 
			$(this).parent().append("<span class='quantityerr' id=inverror_"+rowId+">Invoice Qty should be lt or eq receive qty.</span>");  
			var invoiceHidden = $('#invoiceHiddenQty_'+rowId).val();
			$('#invoiceQty_'+rowId).val(invoiceHidden);
			$('#total_'+rowId).text(Number(invoiceHidden * unitrate).toFixed(2));
		}  
		return false;
	});

	$('.delrowinv').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
     	 var deleteId = getRowId($(slidetab).attr('id')); 
     	 var deleteReceiveId = $('#receiveId_'+deleteId).val();  
     	 $('.activeGRNNumber').each(function(){   
     		var receiveId = getRowId($(this).attr('id')); 
     		 if(deleteReceiveId == receiveId){
				$(this).attr('checked',false);
				 return false;
			 }
     	 });
     	 $(slidetab).remove();  
     	 var i=1;
  	 	 $('.rowid').each(function(){   
  			 var rowId=getRowId($(this).attr('id')); 
  		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
  	 	if($('.tab tr').size()==0)
			$('.invoice_lines').hide();
		 return false;
	 }); 

	$('.delrow_rc').live('click',function(){
		
		/*  slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deleteReceiveId = $('#receiveInvId_'+idval).val();
		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidpo').each(function(){   
  			 var rowId=getRowId($(this).attr('id')); 
  		 	 $('#lineIdrc_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-1 tr').size()==0){ 
			 $('.activeGRNNumber').each(function(){   
	      		var receiveId = getRowId($(this).attr('id'));  
	      		 if(deleteReceiveId == receiveId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId=getRowId($(this).attr('id')); 
			   		 	 var pageReceiveId = $('#receiveId_'+rowId).val();
			   		 	 if(pageReceiveId == receiveId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.invoice_lines').hide();
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 }  */
	 });

	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.closeDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 return false;
	 });

	 $('.sessionSave').live('click',function(){ 
		 receiveDetails = getReceiveDetails();  
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_receive_session_edit.action", 
			 	async: false,  
			 	data:{receiveDetail: receiveDetails},
			    dataType: "json",
			    cache: false,
				success:function(response){  
					 $('#totalInvoice').val(response.invoiceAmount);
					 $('#DOMWindow').remove();
					 $('#DOMWindowOverlay').remove();
					 var invoiceAmount = $('#invoiceAmount').val();
			 		 var totalInvoice = $('#totalInvoice').val();
			 		 if(invoiceAmount > totalInvoice){
						 $('#invoiceAmount').removeClass('validate[required,custom[number]]').addClass('validate[required,custom[number],max['+totalInvoice+']]'); 
					 }else{
						 $('#invoiceAmount').removeClass('validate[required,custom[number],max['+totalInvoice+']]').addClass('validate[required,custom[number]]'); 
					 } 
				} 
			});  
			return false;
	 });

	 var getReceiveDetails = function(){ 
		 var invoice = new Array(); 
		 var description = new Array();
		 var receiveline = new Array();  
		 var invoiceFlags = new Array();  
		 receiveDetails = "";
		 $('.rowidrc').each(function(){ 
			 var rowid = getRowId($(this).attr('id')); 
			 var invoiceQty =  Number($('#invoiceQty_'+rowid).val());  
			 var linedescription = $('#linedescription_'+rowid).val(); 
			 var receiveLineId = $('#receiveLineId_'+rowid).val(); 
			 var invoicedFlag =$("#InvoicedCheck_"+rowid).attr('checked');
			 if(typeof receiveLineId != 'undefined' && receiveLineId!=null && receiveLineId!=""){ 
				 invoice.push(invoiceQty);
				 receiveline.push(receiveLineId); 
				 if(linedescription!=null && linedescription!="")
					description.push(linedescription);
				 else
					description.push("##");  
				 
				 invoiceFlags.push(invoicedFlag);
			 }
		 });
		 for(var j=0;j<receiveline.length;j++){ 
		 	receiveDetails+=invoice[j]+"__"+description[j]+"__"+receiveline[j]+"__"+invoiceFlags[j];
			if(j==receiveline.length-1){   
			}
			else{
				receiveDetails+="#@";
			}
		}  
		return receiveDetails;
	 };

	$('.editDatainv').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var receiveId = $('#receiveId_'+rowId).val(); 
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_receivedetail_edit.action", 
		 	async: false,  
		 	data:{receiveId: receiveId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});  
		return false;
	});

	$('#invoice_save').click(function(){
		if($jquery("#invoicevalidation").validationEngine('validate')){
 		 if($('.tab tr').size() > 0) {
			 var receiveArray = new Array();
			 $('.activeGRNNumber').each(function(){
				 if($(this).attr('checked')){ 
					 receiveArray.push(getRowId($(this).attr('id')));
				 }
			 });
			 receiveDetails = "";
			 for(var i=0; i<receiveArray.length;i++){
				 receiveDetails+=receiveArray[i]+"@#";
			 } 
			 var invoiceId = Number($('#invoiceId').val());
			 var invoiceNumber = $('#invoiceNumber').val();
			 var tempInvoiceNumber = $('#tempInvoiceNumber').val();
			 var date = $('#invoiceDate').val();   
			 var description =$('#description').val(); 
			 var supplierId =$('#supplierId').val();
			 var status = $('#status').val();
			 var invoiceAmount = $('#invoiceAmount').val();
	 		 var totalInvoice = $('#totalInvoice').val();
	 		 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_invoice.action", 
			 	async: false,  
			 	data:{ receiveDetail: receiveDetails, supplierId: supplierId, invoiceNumber: invoiceNumber, invoiceAmount: invoiceAmount,  date: date,
				 	   tempInvoiceNumber: tempInvoiceNumber, invoiceId: invoiceId, description: description, totalInvoice: totalInvoice,  status: status
				 	 },
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$(".tempresult").html(result); 
					var message=$.trim($('.tempresult').html());  
					 if(message=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/get_invoice_payment.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove();  
				  					$('#DOMWindow').remove();
									$('#DOMWindowOverlay').remove();
									$("#main-wrapper").html(result); 
								}
						 });
						 if(invoiceId == 0) {
							 $('#success_message').hide().html("Record created.").slideDown(1000);
						 } else {
							 $('#success_message').hide().html("Record updated.").slideDown(1000);
						 }
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 } 
				} 
			});  
	     } else {
	    	 $('#page-error').hide().html("GRN details not found.").slideDown(1000);
	    	 $('#page-error').delay(2000).slideUp();
			 return false;
		  }
		}else {
			return false;
		}
	 });

	 $('#invoice_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_invoice_payment.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result); 
			},
			error:function(result){  
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result); 
			}
		});  
	 });
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function showActiveReceiveNotes(supplierId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_supplier_activereceives.action", 
	 	async: false,  
	 	data:{supplierId: supplierId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.invoice-info').html(result);   
			 if(typeof($('.activeGRNNumber').attr('id'))=="undefined"){   
				$('.invoice_lines').hide();
				$('.tab').html('');
				$('.invoice-info').find('div').css("color","#ca1e1e").html("No active GRN found for this supplier.");
			 } 
		},
		error:function(result){   
		}
	});  
}
function callDefaultGRNSelect(receiveObj){
	if (typeof receiveObj != 'undefined') {
		var id= Number(1); 
		var receiveId = getRowId(receiveObj); 
		$('#'+receiveObj).attr('checked',true);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_receive_details.action", 
		 	async: false,  
		 	data:{receiveId: receiveId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.invoice_lines').show();
				$('.tab').html(result);  
			} 
		});  
	}
	calculateTotalInvoice();
	return false;
}
function validateGRNInfo(currentObj){
	var receiveId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.invoice_lines').show();
		} else {
			$('.invoice_lines').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_receive_details.action", 
		 	async: false,  
		 	data:{receiveId: receiveId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.tab').append(result); 
			} 
		});  
	} else {
		 $('.rowid').each(function(){ 
			 var rowId = getRowId($(this).attr('id')); 
			 var removeReceiveId = $('#receiveId_'+rowId).val();
			 if(removeReceiveId == receiveId){
				 $('#fieldrow_'+rowId).remove();
				 return false;
			 }
		 });
		 if($('.tab tr').size()==0)
			$('.invoice_lines').hide();
	}
	calculateTotalInvoice();
	return false;
}
function calculateTotalInvoice(){
	var totalInvoice=0;
	$('.rowid').each(function(){
		var rowId=getRowId($(this).attr('id')); 
		var totalRegx = new RegExp(',', 'g'); 
		var totalamount=Number($('#totalamt_'+rowId).text().trim().replace(totalRegx,''));   
		totalInvoice=Number(totalInvoice+totalamount);  
	}); 
	$('#totalInvoiceAmount').text(Number(totalInvoice).toFixed(2));
	$('#totalInvoice').val(totalInvoice);
}
function commonSupplierPopup(supplierId, supplierName,
		combinationId, accountCode, param){
	$('#supplierId').val(supplierId);
	$('#supplierName').val(supplierName);
	showActiveReceiveNotes(supplierId);
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			 Invoice
		</div>	 
		<form name="invoicevalidation" id="invoicevalidation" style="position: relative;"> 
		<div class="portlet-content">  
		  	<div class="tempresult" style="display:none;"></div>
		  	<input type="hidden" id="invoiceId" name="invoiceId" value="${INVOICE_INFO.invoiceId}"/>
			<div class="width100 float-left" id="hrm"> 
				<div class="tempresult" style="display: none;"></div>
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width48 float-left" id="hrm">
					<fieldset style="min-height: 110px;">  			 
						<div>
							<label class="width30">Invoice No.<span class="mandatory">*</span></label>  
							<input type="hidden" id="tempInvoiceNumber" value="${INVOICE_INFO.invoiceNumber}"> 		
							<c:choose>
								<c:when test="${INVOICE_INFO.invoiceNumber ne null && INVOICE_INFO.invoiceNumber ne  ''}">
									<input type="text" name="invoiceNumber" value="${INVOICE_INFO.invoiceNumber}" readonly="readonly"
											id="invoiceNumber" class="width50 validate[required]" TABINDEX=1>  
								</c:when>
								<c:otherwise>
									<input type="text" name="invoiceNumber" readonly="readonly" value="${requestScope.invoiceNumber}" 
											id="invoiceNumber" class="width50 validate[required]">  
								</c:otherwise>
							</c:choose> 
						</div> 
						<div>
							<label class="width30">Invoice Date<span class="mandatory">*</span></label> 
							<c:choose>
								<c:when test="${INVOICE_INFO.date ne null && INVOICE_INFO.date ne ''}">
									<c:set var="invoiceDate" value="${INVOICE_INFO.date}"/>  
									<%String date = DateFormat.convertDateToString(pageContext.getAttribute("invoiceDate").toString());%>
									<input type="text" name="invoiceDate" value="<%=date%>" 
									id="invoiceDate" readonly="readonly" class="width50 validate[required]"> 
								</c:when>  
								<c:otherwise>  
									<input type="text" name="invoiceDate"
									id="invoiceDate" readonly="readonly" class="width50 validate[required]"> 
								</c:otherwise>
							</c:choose> 
						</div>   
						<div>
							<label class="width30">Total Invoice</label> 
							<input type="text" name="totalInvoice" value="${INVOICE_INFO.totalInvoice}"
									id="totalInvoice" readonly="readonly" class="width50"> 
						</div>	
						<div>
							<label class="width30">Invoice Amount<span class="mandatory">*</span></label> 
							<input type="text" name="invoiceAmount" value="${INVOICE_INFO.invoiceAmount}"
									id="invoiceAmount" class="width50 validate[required,custom[number]]"> 
						</div>	
						<div style="display:none;">
							<label class="width30">Supplier No.<span class="mandatory">*</span></label>  
							<c:choose>
								<c:when test="${INVOICE_INFO.invoiceId eq null || INVOICE_INFO.invoiceId eq '' || INVOICE_INFO.invoiceId eq 0}">
									<input type="text" name="supplierNumber"
											id="supplierNumber" readonly="readonly" class="width50" value="${INVOICE_INFO.supplier.supplierNumber}"/>
									<span class="button float-right" style="position: relative; top: 8px; right: 35px;">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all supplierinvoice-common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span> 
								</c:when> 
								<c:otherwise>
									<input type="text" name="supplierNumber"
											id="supplierNumber" readonly="readonly" class="width50" value="${INVOICE_INFO.supplier.supplierNumber}"/>
								</c:otherwise> 
							</c:choose>
						</div>  
					</fieldset>
				</div>
				<div class="width48 float-right" id="hrm">
					<fieldset style="min-height: 110px;">  
						 <div>
							<label class="width30">Supplier<span class="mandatory">*</span></label> 
							<c:choose>
								<c:when test="${INVOICE_INFO.supplier.person ne null && INVOICE_INFO.supplier.person ne '' }">
									<input type="text" name="supplierName" value="${INVOICE_INFO.supplier.person.firstName} ${INVOICE_INFO.supplier.person.lastName}" 
											readonly="readonly" id="supplierName" class="width50 validate[required]" TABINDEX=3>  
								</c:when>
								<c:when test="${INVOICE_INFO.supplier.cmpDeptLocation ne null && INVOICE_INFO.supplier.cmpDeptLocation ne '' }">
									<input type="text" name="supplierName" value="${INVOICE_INFO.supplier.cmpDeptLocation.company.companyName}" 
											readonly="readonly" id="supplierName" class="width50 validate[required]">  
								</c:when>
								<c:when test="${INVOICE_INFO.supplier.company ne null && INVOICE_INFO.supplier.company ne '' }">
									<input type="text" name="supplierName" value="${INVOICE_INFO.supplier.company.companyName}" 
											readonly="readonly" id="supplierName" class="width50 validate[required]">  
								</c:when>
								<c:otherwise>
									<input type="text" name="supplierName"
										id="supplierName" readonly="readonly" class="width50 validate[required]">
									<span class="button">
										<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all supplierinvoice-common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>   
								</c:otherwise>
							</c:choose>  
							<input type="hidden" readonly="readonly" name="supplierId" id="supplierId" value="${INVOICE_INFO.supplier.supplierId}"/>
						</div> 
						<div>
							<label class="width30 tooltip">Status</label>
							<select name="status" id="status" class="width51 validate[required]">
								<c:forEach var="STATUS" items="${INVOICE_STATUS}">
									<option value="${STATUS.key}">${STATUS.value}</option>
								</c:forEach>
							</select>
							<input type="hidden" id="tempstatus" value="${INVOICE_INFO.status}"/>
						</div>
						<div>
							<label class="width30 tooltip">Description</label>
							<textarea id="description" class="width51">${INVOICE_INFO.description}</textarea>
						</div> 
					</fieldset>
				</div> 
			</div> 
			</div>
			<div class="clearfix"></div>    
			<div class="invoice-info" id="hrm">
			 <c:if test="${GOODS_RECEIVE_INFO ne null && GOODS_RECEIVE_INFO ne ''}">
			 	<fieldset> 
					<legend><fmt:message key="accounts.grn.label.activegrn"/><span
									class="mandatory">*</span></legend>
					<div>
						<c:forEach var="ACTIVE_GRN" items="${GOODS_RECEIVE_INFO}">
							<label for="activeGRNNumber_${ACTIVE_GRN.receiveId}" class="width10" id="grnLabel_${ACTIVE_GRN.receiveId}">
								<input type="checkbox" name="activeGRNNumber" class="activeGRNNumber activeGRNNumberUpdate" id="activeGRNNumber_${ACTIVE_GRN.receiveId}"/>
								${ACTIVE_GRN.receiveNumber}
							</label>
						</c:forEach>
					</div>
				</fieldset>
			 </c:if> 
			</div>
			<div class="clearfix"></div> 
			<div id="hrm" class="portlet-content width100 invoice_lines"> 
				<fieldset id="hrm"> 	
					<legend>Invoice Detail<span
									class="mandatory">*</span></legend>
					<div class="portlet-content"> 
					<div id="hrm" class="hastable width100"  > 
					 <table id="hastab" class="width100"> 
						<thead>
						   <tr>   
					   	    <th style="width:5%;">Receive NO.</th>
					   	    <th style="width:5%;">Ref.NO</th>   
					   	    <th style="width:5%;">Receive Date</th>   
					   	    <th style="display: none;">Receive Qty.</th>  
					   	    <th style="width:5%;">Total Amount</th> 
						    <th style="width:5%;">Description</th> 
							<th style="width:1%;"><fmt:message key="accounts.common.label.options"/></th> 
						  </tr>
						</thead> 
						<tbody class="tab">  
							<c:choose>
								<c:when test="${INVOICE_RECEIVE_DETAIL ne null && INVOICE_RECEIVE_DETAIL ne ''}">
									<c:forEach var="INVOICE_RECEIVE" items="${INVOICE_RECEIVE_DETAIL}" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${status.index+1}"> 
											<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td> 
											<td>
												<input type="hidden" class="receiveId" id="receiveId_${status.index+1}" value="${INVOICE_RECEIVE.receiveId}" />
												${INVOICE_RECEIVE.receiveNumber}
											</td> 
											<td>
												${INVOICE_RECEIVE.referenceNo} 
											</td>
											<td>
												<c:set var="receivedate" value="${INVOICE_RECEIVE.receiveDate}"/>
												<%=DateFormat.convertDateToString(pageContext.getAttribute("receivedate").toString())%> 
											</td> 
											<td style="display: none;">
												${INVOICE_RECEIVE.totalReceiveQty}
											</td>  
											<td style="text-align: right;"> 
												<c:set var="totalamt" value="${INVOICE_RECEIVE.totalReceiveAmount}"/>
												<%= AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
											</td> 
											<td>
										 		 ${INVOICE_RECEIVE.description}  
											</td> 
										 	<td style="width:0.01%;" class="opn_td" id="option_${status.index+1}"> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											  </a> 
											</td>   
										</tr>
									</c:forEach>
								</c:when>
							</c:choose> 
						</tbody>
					</table>
					</div>
					</div>
				</fieldset>
			</div>  
		</form>  
		<div class="clearfix"></div>
		<div id="temp-result" style="display:none;"></div>
		<div class="width30 float-right" style="font-weight: bold; display: none;">
			<span>Total: </span>
			<span id="totalInvoiceAmount"></span>
		</div>
		<div class="clearfix"></div>
		<span class="callJq"></span>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
			<div class="portlet-header ui-widget-header float-right" id="invoice_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>  
		 </div> 
		<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div> 
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div> 
	</div>
</div>