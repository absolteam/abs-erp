<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() { 
		var ptdsize = $($("#product-table>tbody>tr:first").children()).size();
		if (ptdsize == 1) {
			$($("#product-table>tbody>tr:first").children()).each(function() {
				$(this).addClass('float-left width25');
			});
		}
		
		$('.selectPopupProduct').click(function() {
			var productId = Number(getRowId($(this).attr('id')));
			var specialProductId = Number($('#specialProductId').val());
				var productName = $.trim($(this).text());
				$(this).css('background', '#f4fa58');
				var previewObject = { 
					"productId" : productId,
					"productName" : productName,
					"comboType" : true,
					"comboProductId" : specialProductId,
					"specialProduct" : false
			};
				addProductPreview(previewObject); 
				return false;
		});

		$('.product-splpopup-search').combobox(
				{
					selected : function(event, ui) {
						var productId = Number($(this).val());
						var productName = $.trim($('.product-search :selected')
								.text());
						var specialProductId = Number($('#specialProductId').val());
						var previewObject = {
							"productId" : productId,
							"productName" : productName,
							"comboType" : true,
							"comboProductId" : specialProductId,
							"specialProduct" : false
						};
						addProductPreview(previewObject);
					}
				});
		
		 
	});
</script>
<style>
.icon-search-class{
	cursor: pointer;
	position: absolute;
	right: 7%;
	z-index: 1;
	margin: 4px 0px !important;
}
</style>
<c:choose>
	<c:when
		test="${PRODUCT_DETAILS ne null && fn:length(PRODUCT_DETAILS) > 0}">
		<fieldset style="max-height: 370px !important;" class="splproductset">
			<div class="width100">
				<span id="inlineTargetKeypad" style="width: 100% !important;"></span>
			</div>
			<div class="mainhead portlet-header" style="width: auto !important;">
				<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Product
				<div class="float-right width40">
					<span class="float-right icon-search-class"><img width="24"
						height="24" src="images/icons/Glass.png"> </span> <select
						name="productSearch" id="productSearch" class="product-splpopup-search">
						<option value="">Search</option>
						<c:forEach var="productDetails" items="${PRODUCT_DETAILS}">
							<option value="${productDetails.productId}">${productDetails.productName}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="width100 content-item-table" id="scroll-pane">
				<table id="product-table" class="pos-item-table width100">
					<c:set var="row" value="0" />
					<c:forEach begin="0" step="5" items="${PRODUCT_DETAILS}">
						<tr>
							<!-- TD 1 -->
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="selectPopupProduct"
										id="selectProduct_${PRODUCT_DETAILS[row+0].productId}">${PRODUCT_DETAILS[row+0].productName}
									</span><input type="hidden"
										id="productId_${PRODUCT_DETAILS[row+0].productId}"
										class="productId" />
								</div></td>
							<!-- TD 2 -->
							<c:if test="${PRODUCT_DETAILS[row+1]!= null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="selectPopupProduct"
											id="selectProduct_${PRODUCT_DETAILS[row+1].productId}">${PRODUCT_DETAILS[row+1].productName}
										</span><input type="hidden"
											id="productId_${PRODUCT_DETAILS[row+1].productId}"
											class="productId" />
									</div></td>
							</c:if>
							<!-- TD 3 -->
							<c:if test="${PRODUCT_DETAILS[row+2]!= null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="selectPopupProduct"
											id="selectProduct_${PRODUCT_DETAILS[row+2].productId}">${PRODUCT_DETAILS[row+2].productName}
										</span><input type="hidden"
											id="productId_${PRODUCT_DETAILS[row+2].productId}"
											class="productId" />
									</div></td>
							</c:if>
							<!-- TD 4 -->
							<c:if test="${PRODUCT_DETAILS[row+3]!= null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="selectPopupProduct"
											id="selectProduct_${PRODUCT_DETAILS[row+3].productId}">${PRODUCT_DETAILS[row+3].productName}
										</span><input type="hidden"
											id="productId_${PRODUCT_DETAILS[row+3].productId}"
											class="productId" />
									</div></td>
							</c:if>
							<!-- TD 5 -->
							<c:if test="${PRODUCT_DETAILS[row+4]!= null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="selectPopupProduct"
											id="selectProduct_${PRODUCT_DETAILS[row+4].productId}">${PRODUCT_DETAILS[row+4].productName}
										</span><input type="hidden"
											id="productId_${PRODUCT_DETAILS[row+4].productId}"
											class="productId" />
									</div></td>
							</c:if>
							<c:set var="row" value="${row + 5}" />
						</tr>
					</c:forEach>
				</table>
			</div>
		</fieldset>
	</c:when>
</c:choose>
