<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach var="receiveDetail"
	items="${RECEIVE_DETAILS}" varStatus="status">
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
		<td><span>${receiveDetail.product.code}</span></td>
		<td><span class="width60 float-left">${receiveDetail.product.productName}</span>
			<span class="width10 float-right">${receiveDetail.product.lookupDetailByProductUnit.accessCode}</span> <input
			type="hidden" id="productid_${status.index+1}"
			value="${receiveDetail.product.productId}" />
		</td>
		<td><span>${receiveDetail.receiveQty}</span>
			<c:set var="receiveQty" value="${receiveDetail.receiveQty}"/>
		</td>
		<td>
			<c:choose>
				<c:when test="${receiveDetail.returnedQty ne null && receiveDetail.returnedQty >0}">
					<span>${receiveDetail.returnedQty}</span> 
				</c:when>
				<c:otherwise>
					<span>-N/A-</span> 
				</c:otherwise>
			</c:choose> 
		</td>
		<td>
			${receiveDetail.conversionUnitName}
			<input type="hidden" id="packageType_${status.index+1}" value="${receiveDetail.packageDetailId}"  
				name="packageType" class="packageType"/>
		</td>
		<td><input type="text"
			class="packageUnit validate[optional,custom[number]]"
			id="packageUnit_${status.index+1}" />
		</td>
		<td><input type="hidden"
			class="quantity validate[optional,custom[number]]"
			id="quantity_${status.index+1}" />
			<span id="baseUnitConversion_${status.index+1}" class="width10" style="display: none;">${receiveDetail.baseUnitName}</span>
			<span id="baseDisplayQty_${status.index+1}"></span>
		</td>
		<td>
			<span class="float-right" id="unitrate_${status.index+1}"> </span>
			<input type="text" style="display: none;" value="${receiveDetail.unitRate}" id="unitrateH_${status.index+1}" class="unitRate"/> 
		</td>
		<td >
			<span class="float-right" id="totalamount_${status.index+1}"></span>
			<input type="text" style="display: none;" value="${receiveDetail.receiveQty * receiveDetail.unitRate}" 
				id="totalAmountH_${status.index+1}" class="totalAmountH"/> 
		</td>
		<td style="display: none;"><input type="hidden"
			id="returnLineId_${status.index+1}"/> <input type="hidden"
			id="purchaseId_${status.index+1}" />
			<input type="hidden" value="${receiveDetail.receiveDetailId}"
			id="receiveDetailId_${status.index+1}" />
			<input type="hidden" value="${receiveDetail.unitRate}"
				id="unitrate_${status.index+1}" />
		</td>
		<td style="width: 0.01%;" class="opn_td" id="option_${status.index+1}"><a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
			id="DeleteImage_${status.index+1}" style="cursor: pointer;"
			title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
		</a>
		</td>
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){
	$jquery('.unitRate,.totalAmountH').number(true, 2);
	$('.rowid').each(function(){
		var rowId = getRowId($(this).attr('id'));
		$('#unitrate_'+rowId).text($('#unitrateH_'+rowId).val());
		$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
	});
});
</script>