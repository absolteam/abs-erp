<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:choose>
	<c:when test="${FILTER_ACCOUNTS ne null && FILTER_ACCOUNTS ne ''}">
		<c:forEach var="ACCOUNT_CODE" items="${FILTER_ACCOUNTS}">
			<option value="${ACCOUNT_CODE.accountId}">${ACCOUNT_CODE.code}---${ACCOUNT_CODE.account}</option>
		</c:forEach>
	</c:when>
</c:choose>