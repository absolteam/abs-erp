<tr class="customer-rowid" id="customer-fieldrow_${ROW_ID}"> 
	<td id="lineId_${ROW_ID}" style="display:none;">${ROW_ID}</td>
	<td>
		<input type="text" id="contactName_${ROW_ID}" class="contactname"/>
	</td>
	<td>
		<input type="text" id="contactPerson_${ROW_ID}" class="contactperson"/>
	</td>
	<td>
		<input type="text" id="contactNo_${ROW_ID}"/>
	</td>
	<td>
		<input type="text" id="address_${ROW_ID}"/>
	</td>
	<td style="display: none;">
		<input type="hidden" id="shippingId_${ROW_ID}"/>
	</td>
	<td style="width:0.01%;" class="opn_td" id="option_${ROW_ID}">
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${ROW_ID}" style="display:none;cursor:pointer;" title="Add Record">
				<span class="ui-icon ui-icon-plus"></span>
	  </a>	
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${ROW_ID}" style="display:none; cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
	  </a> 
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${ROW_ID}" style="display:none;cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
	  </a>
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${ROW_ID}" style="display:none;" title="Working">
			<span class="processing"></span>
	  </a>
	</td> 
</tr>