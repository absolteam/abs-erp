<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>create eibor</div>
		<div class="portlet-content">
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div style="display:none;" class="tempresult"></div>	 	 
			<div class="page-error response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	
			<form id="eiborvalidate-form" name="eiborentrydetails">
				<input type="hidden" name="eiborId" id="eiborId" value="${bean.eiborIdId}"/>   
				<div class="portlet-content"> 
					<div id="hrm" class="hastable width100">
						<div id="lineerror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
						<div id="warning_message" class="response-msg notice ui-corner-all"  style="width:90%; display:none;"></div>
						<input type="hidden" name="childCount" id="childCount"/>  
						<table id="hastab" class="width100">	
							<thead>
								<tr> 
									<th>EIBOR Type</th> 
									<th>Date</th> 
									<th>Bank Name</th> 
									<th>Combination</th> 
									<th style="width:6%;"><fmt:message key="accounts.common.label.options"/></th>
								</tr>
							</thead>
							<tbody class="tab"> 
								<c:set var="i" value="1"/>
								<c:choose>
									<c:when test="${requestScope.EIBOR_DETAILS ne null && requestScope.EIBOR_DETAILS ne ''}"> 
										<tr id="fieldrow_${i}" class="rowid">		  	
											<td id="eibortype_${i}">${EIBOR_DETAILS.eiborType}</td>
											<td id="eibordate_${i}">${EIBOR_DETAILS.eiborDate}</td>
											<td id="bankname_${i}">${EIBOR_DETAILS.bankName}</td> 
											<td id="combination_${i}">${EIBOR_DETAILS.combination}</td>  
											<td style="display:none;" id="lineId_${i}">${i}</td> 
											<td class="opn_td">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" style="cursor:pointer;display:none;" id="AddImage_${i}" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editeditData"  id="EditImage_${i}" style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editdelrow"  id="DeleteImage_${i}" style="cursor:pointer;display:none;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>  
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="i" begin="1" end="2" step="1" varStatus ="status"> 
											<tr id="fieldrow_${i}" class="rowid"> 	
												<td id="eibortype_${i}"></td>
												<td id="eibordate_${i}"></td>
												<td id="bankname_${i}"></td> 
												<td id="combination_${i}"></td> 
												<td style="display:none;" id="lineId_${i}">${i}</td>
												<td class="opn_td" id="option_${i}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>  
											</tr>
										</c:forEach>	
									</c:otherwise>
								</c:choose> 						 									 
					 		</tbody>
						</table>
					</div>
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
						<div class="portlet-header ui-widget-header float-right save" id="${requestScope.page}"><fmt:message key="accounts.common.button.save"/></div>
					</div> 
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
					</div> 
				</div>
			</form>
		</div>	
	</div>		
</div>