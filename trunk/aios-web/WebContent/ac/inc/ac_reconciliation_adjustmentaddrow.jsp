<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><select name="adjustmentType" id="adjustmentType_${ROW_ID}"
		class="width98 adjustmentType">
			<option value="">Select</option>
			<c:forEach var="ADJUST_TYPE" items="${ADJUSTMENT_TYPES}">
				<option value="${ADJUST_TYPE.key}">${ADJUST_TYPE.value}</option>
			</c:forEach>
	</select></td>
	<td><input type="hidden" name="combinationId_${ROW_ID}"
		id="combinationId_${ROW_ID}" /> <input type="text"
		name="codeCombination_${ROW_ID}" readonly="readonly"
		id="codeCombination_${ROW_ID}" class="codeComb width80"> <span
		class="button" id="codeID_${ROW_ID}"> <a
			style="cursor: pointer;"
			class="btn ui-state-default ui-corner-all codecombination-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td style=""><input type="text" name="amount"
		id="amount_${ROW_ID}" style="text-align: right;"
		class="amount width98 right-align validate[optional,custom[number]]">
	</td>
	<td><input type="text" name="linesDescription"
		id="linesDescription_${ROW_ID}" class="width98" maxlength="150">
	</td>
	<td style="width: 1%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		name="adjustmentDetailId" id="adjustmentDetailId_${ROW_ID}" /></td>
</tr>
<script type="text/javascript"> 
$(function(){
	$jquery("#adjustmentValidation").validationEngine('attach');
	$jquery(".amount").number(true, 2);
});
</script>