<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td><select name="promotionType"
		class="width96 promotionType validate[optional]"
		id="promotionType_${rowId}">
			<option value="">Select</option>
			<c:forEach var="packType" items="${PACK_TYPES}">
				<option value="${packType.key}">${packType.value}</option>
			</c:forEach>
	</select>
	</td>
	<td><select name="calculationType"
		class="width96 calculationType validate[optional]"
		id="calculationType_${rowId}">
			<option value="">Select</option>
			<option value="O">Off</option>
			<option value="E">Extra</option>
	</select></td>
	<td><input type="text"
		class="width96 promotion validate[optional,custom[number]]"
		id="promotion_${rowId}" style=" text-align: right;" /></td>
	<td><span class="coupon" id="coupon_${rowId}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="couponPopup_${rowId}"
			class="btn ui-state-default ui-corner-all common-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input type="hidden"
		id="couponId_${rowId}"></td>
	<td><input type="text" name="promotionPoints"
		id="promotionPoints_${rowId}" class="width96 promotionPoints" /></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${rowId}" class="width96" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		id="promotionMethodId_${rowId}" /></td>
</tr>