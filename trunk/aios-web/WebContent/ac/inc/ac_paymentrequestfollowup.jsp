<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.divstyle {
	padding:3px;
} 
.spanfont{
	font-weight: normal;
}
.divpadding{
		padding-bottom: 7px;
}
</style>
<script type="text/javascript">
var tempid="";
var slidetab="";
var requestDetails=""; 
var paymentRequestId = 0;
$(function(){  
	$("#requestvalidation").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	}); 
	$('.followupDate').datepick(); 
	$('.rowid').each(function(){    
		var rowId=getRowId($(this).attr('id'));
		$('#currency_'+rowId+' option[value='+$('#tempcurrency_'+rowId).val()+']').attr("selected","selected");  
		$('#paymode_'+rowId+' option[value='+$('#temppaymode_'+rowId).val()+']').attr("selected","selected");  
		$('#trackstatus_'+rowId+' option[value='+$('#temptrackstatus_'+rowId).val()+']').attr("selected","selected");  
	});
	$('#request_discard').click(function(){ 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_paymentrequest.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
			}
		 });
	 });
	$('#request_save').click(function(){ 
		if($('#requestvalidation').validationEngine({returnIsValid:true})){ 
		 processRequestFollowUp(Number($('#paymentRequestId').val()));
		} else {
			return false;
		}
	});
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function processRequestFollowUp(paymentRequestId){
	requestDetails = getRequestDetails(); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/payment_request_updatefollowup.action", 
	 	async: false, 
	 	data:{ 
	 		paymentRequestId: paymentRequestId, requestDetail: requestDetails
	 	},
	    dataType: "html",
	    cache: false,
		success:function(result){
			$(".tempresult").html(result); 
			var message=$('.tempresult').html();  
			 if(message.trim()=="SUCCESS"){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/show_all_paymentrequest.action", 
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$("#main-wrapper").html(result); 
						}
				 });
				 $('#success_message').hide().html("Payment request followup updated.").slideDown(1000);  
				 $('#success_message').delay(2000).slideUp();
			 }
			 else{
				 $('#page-error').hide().html(message).slideDown(1000);
				 $('#page-error').delay(2000).slideUp();
				 return false;
			 }
		},
		error:function(result){  
			$('#page-error').hide().html("Process Failure.").slideDown(1000);
		}
	});  
}
function getRequestDetails(){
	requestDetails = ""; 
	var followups = new Array();
	var lineid=new Array(); 
	var followupsdate = new Array();
	var currencies=new Array();
	var paymodes=new Array();
	var amounts=new Array();
	var track = new Array();
	$('.rowid').each(function(){  
		var rowId=getRowId($(this).attr('id'));  
		var followup=$('#followup_'+rowId).val();
		var followupdate=$('#followupdate_'+rowId).val(); 
		var detailId = Number($('#paymentRequestId_'+rowId).val());  
		var currency=$('#currency_'+rowId).val();
		var paymode=$('#paymode_'+rowId).val();
		var amount=Number($('#amount_'+rowId).val());  
		var trackstatus=$('#trackstatus_'+rowId).val(); 
		currencies.push(currency);
		paymodes.push(paymode);
		amounts.push(amount); 
		track.push(trackstatus);
		if(followup!=null && followup!="")
			followups.push(followup);
		else
			followups.push("##");
		if(followupdate!=null && followupdate!="")
			followupsdate.push(followupdate);
		else
			followupsdate.push("##");
		lineid.push(detailId); 
	});
	for(var j=0;j<lineid.length;j++){ 
		requestDetails+=currencies[j]+"__"+paymodes[j]+"__"+amounts[j]+"__"+followups[j]+"__"+followupsdate[j]+"__"+track[j]+"__"+lineid[j];
		if(j==lineid.length-1){   
		} 
		else{
			requestDetails+="#@";
		}
	}  
	return requestDetails;
 }
</script>
<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			payment request information
		</div>	
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			 General Information
		</div>	
		<form name="requestvalidation" id="requestvalidation"> 
		<div class="portlet-content">  
		  	<div class="tempresult" style="display:none;"></div>
		  	<input type="hidden" id="paymentRequestId" name="paymentRequestId" value="${PAYMENT_REQUEST.paymentRequestId}"/>
			<div class="width100 float-left" id="hrm"> 
				<div class="tempresult" style="display: none;"></div>
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset style="min-height: 78px;">  			 
						<div class="divstyle">
							<label class="width30">Request No.<span class="mandatory">*</span></label>   
							<span>${PAYMENT_REQUEST.paymentReqNo}</span>
						</div> 
						<div class="divstyle">
							<label class="width30">Request Date<span class="mandatory">*</span></label>  
							<c:set var="requestDate" value="${PAYMENT_REQUEST.requestDate}"/>  
							<span>
								<%= DateFormat.convertDateToString(pageContext.getAttribute("requestDate").toString())%>
							 </span> 
						</div> 
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset>  
						 <div class="divstyle">
							<label class="width30 tooltip">Status<span class="mandatory">*</span></label>
							<span> 
								<c:set var="payrequest" value="${PAYMENT_REQUEST.status}"/>
								<%=Constants.Accounts.PaymentRequestStatus.get(Byte.valueOf(pageContext.getAttribute("payrequest").toString())) %>
							</span> 
						</div>
						<div class="divstyle">
							<label class="width30 tooltip">Description</label>
							<textarea rows="2" id="description" class="width60" readonly="readonly">${PAYMENT_REQUEST.description}</textarea>
						</div> 
					</fieldset>
				</div> 
			</div> 
			</div>
			<div class="clearfix"></div>  
			<div id="hrm" class="portlet-content width100 invoice_lines"> 
				<fieldset id="hrm"> 	
					<legend>Payment Request Detail</legend>
					<div class="portlet-content"> 
					<div id="hrm" class="hastable width100"  > 
					 <table id="hastab" class="width100"> 
						<thead>
						   <tr>   
						    <th style="width:1%;">Line No.</th> 
					   	    <th style="width:5%;">Ref#</th>   
					   	    <th style="width:3%;">Payment Mode</th>  
					   	    <th style="width:3%;">Amount</th> 
						    <th style="width:5%;">Description</th> 
						    <th style="width:3%;">Decision</th>  
						    <th style="width:5%;">Comments</th>  
						    <th style="width:5%;">Req.Status</th>  
						    <th style="width:5%;">Followup</th>  
						    <th style="width:3%;">Followup Date</th>
						  </tr>
						</thead> 
						<tbody class="tab">  
							<c:choose>
								<c:when test="${PAYMENT_REQUEST.paymentRequestDetails ne null && PAYMENT_REQUEST.paymentRequestDetails ne ''}">
									<c:forEach var="REQUEST_DETAIL" items="${PAYMENT_REQUEST.paymentRequestDetails}" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${status.index+1}"> 
											<td id="lineId_${status.index+1}">${status.index+1}</td> 
											 <td> 
												 ${REQUEST_DETAIL.requestReference}
											</td> 
											<c:set var="mgtstatus" value="${REQUEST_DETAIL.mgtStatus}"/>
											<%if(pageContext.getAttribute("mgtstatus").equals(Constants.Accounts.ManagementStatus.Approve.getCode())){ %>
												<td> 
													${REQUEST_DETAIL.lookupDetail.displayName}
													<input type="hidden" id="paymode_${status.index+1}" value="${REQUEST_DETAIL.lookupDetail.lookupDetailId}"/>
												</td> 
												<td style="text-align: right;"> 
											  		${REQUEST_DETAIL.amount}
											  		<input type="hidden" id="amount_${status.index+1}" class="amount" value="${REQUEST_DETAIL.amount}"/>
												</td> 
											<% } else {%>
												<td> 
													<select id="currency_${status.index+1}" class="currency">
													 	<option value="">Select</option>
													 	<c:forEach var="CURRENCY" items="${CURRENCY_INFO}">
													 		<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
													 	</c:forEach>
													 </select>
													 <input type="hidden" id="tempcurrency_${status.index+1}" value="${REQUEST_DETAIL.currency.currencyId}"/>
												</td> 
												<td>
													<select id="paymode_${status.index+1}" class="paymode">
														<c:forEach var="MODE" items="${PAYMENT_MODE}">
															<option value="${MODE.lookupDetailId}">${MODE.displayName}</option>
														</c:forEach>
													</select> 
													<input type="hidden" id="temppaymode_${status.index+1}" value="${REQUEST_DETAIL.lookupDetail.lookupDetailId}"/>
												</td>  
												<td style="text-align: right;"> 
											  		<input type="text" id="amount_${status.index+1}" class="amount" style="text-align: right;" value="${REQUEST_DETAIL.amount}"/>
												</td> 
											<% } %>  
											<td>
										 		 ${REQUEST_DETAIL.description}
										 		 <input type="hidden" id="paymentRequestId_${status.index+1}" value="${REQUEST_DETAIL.paymentRequestDetailId}"/>
											</td> 
											<td> 
												<%=Constants.Accounts.ManagementStatus.get(Byte.valueOf(pageContext.getAttribute("mgtstatus").toString()))%>
											</td>  
											<td>
												 <c:forEach var="comments" items="${MANAGEMENT_COMMENTS}"> 
													<c:if test="${comments.key eq REQUEST_DETAIL.paymentRequestDetailId}">
														${comments.value}
													</c:if>
												</c:forEach>
											</td>
											<td>
												<select id="trackstatus_${status.index+1}" class="trackstatus">
													<option value="">Select</option>
													<c:forEach var="TRACK" items="${TRACK_STATUS}">
														<option value="${TRACK.key}">${TRACK.value}</option>
													</c:forEach>
												</select> 
												<input type="hidden" id="temptrackstatus_${status.index+1}" value="${REQUEST_DETAIL.requestStatus}"/> 
											</td>
											<td>
												 <input type="text" id="followup_${status.index+1}" class="followup" value="${REQUEST_DETAIL.followUp}"/>
											</td>
											<td>
												<c:set var="flDate" value="${REQUEST_DETAIL.followUpDate}"/>
												<%
													if(null!=pageContext.getAttribute("flDate") && !("").equals(pageContext.getAttribute("flDate"))) {
														String flDate = DateFormat.convertDateToString(pageContext.getAttribute("flDate").toString()); %>
														<input type="text" id="followupdate_${status.index+1}" class="followupDate" value="<%=flDate%>" readonly="readonly"/>
												<% } else {%>	 
												<input type="text" id="followupdate_${status.index+1}" class="followupDate" readonly="readonly"/>
												<%} %> 
											</td>
										</tr>
									</c:forEach>
								</c:when>
							</c:choose>  
						</tbody>
					</table>
					</div>
					</div>
				</fieldset>
			</div>  
			<div class="clearfix"></div> 
		</form>   
		<div class="width30 float-right" style="font-weight: bold; display: none;">
			<span>Total: </span>
			<span id="totalInvoiceAmount"></span>
		</div>
		<div class="clearfix"></div>  
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
			<div class="portlet-header ui-widget-header float-right" id="request_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
			<div class="portlet-header ui-widget-header float-right" id="request_save" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		 </div> 
	</div>
</div>