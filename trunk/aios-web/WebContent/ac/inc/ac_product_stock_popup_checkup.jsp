<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
 var oTable; var selectRow=""; var aSelected = []; var aData="";
	$(function() {
		oTable = $('#ProductStockList')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"sScrollY": $("#main-content").height() - 150,
							"sAjaxSource" : "show_product_stock_checkup_jsonlist.action", 
							"aoColumns" : [  {
								"mDataProp" : "productCode"
							 }, {
								"mDataProp" : "productName"
							 },{
								"mDataProp" : "categoryName"
							 },{ 
								"mDataProp" : "subCategoryName"
							 },{ 
								"mDataProp" : "productUnit"
							 },{
								"mDataProp" : "sellingPrice" 
							 },{
								"mDataProp" : "availableQuantity" 
							 },{
								"mDataProp" : "storeName" 
							 } ], 
						});

		/* Click event handler */
		/* $('#ProductStockList tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					commonProductPopup(aData, $(
							'#currentRowId').val());
					$('#product-stock-popup').trigger('click');
				}); */

		/* Click event handler */
		$('#ProductStockList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
			$('#stock-balance').click(function(){  
				$('.response-msg').hide();
				var month=$('#month').val()+','+$('#year').val();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/udate_periodic_stock.action", 
			     	async: false,
			     	data:{month: month},
					dataType: "json",
					cache: false,
					success: function(response){  
						if(response.sqlReturnMessage=="SUCCESS"){
							$('#success_message').hide().html("Updated successfully").slideDown(1000); 
							$('#success_message').delay(2000).slideUp();
						}
						else{
							$('#error_message').hide().html(response.returnMessage).slideDown(1000);
							$('#error_message').delay(2000).slideUp();
							return false;
						} 
					} 		
				}); 
				
			});
			
			$('#loadStock').click(function(){  
				var showPage=$("#xlFileName").val();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/upload_stocks.action", 
			     	async: false,
			     	data:{showPage:showPage},
					dataType: "json",
					cache: false,
					success: function(object){ 
						if(object.sqlReturnMessage=='SUCCESS'){
							alert("Successfully Done");
							return false;
						}else{
							alert("Error...");
							return false;
						}
					} 		
				}); 
				return false;
			}); 
			
			$('#loadStockWithPeirod').click(function(){  
				var showPage=$("#xlFileName").val();
				var month=$('#month').val()+','+$('#year').val();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/upload_stocks_with_period.action", 
			     	async: false,
			     	data:{showPage:showPage,month: month},
					dataType: "json",
					cache: false,
					success: function(object){ 
						if(object.sqlReturnMessage=='SUCCESS'){
							alert("Successfully Done");
							return false;
						}else{
							alert("Error...");
							return false;
						}
					} 		
				}); 
				return false;
			}); 
			
			$('#receive-stock').click(function(){  
				var toDate=$('#toDate').val();
				var fromDate=$('#fromDate').val();
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/update_stock_recive_period.action", 
			     	async: false,
			     	data:{toDate:toDate,fromDate:fromDate},
					dataType: "json",
					cache: false,
					success: function(object){ 
						if(object.sqlReturnMessage=='SUCCESS'){
							alert("Successfully Done");
							return false;
						}else{
							alert("Error...");
							return false;
						}
					} 		
				}); 
				return false;
			}); 
			
			 $('#fromDate,#toDate').datepick({
				 onSelect: customRange,showTrigger: '#calImg'});   
	});
	function customRange(dates) {
		if (this.id == 'fromDate') {
			$('#toDate').datepick('option', 'minDate', dates[0] || null);  
			
		}
		else{
			$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
		} 
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Product Stock
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div id="rightclickarea">
			<div id="product_json_list">
				<table id="ProductStockList" class="display">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th>
							<th>Category</th>
							<th>Sub Category</th>
							<th>Unit</th>
							<th>Selling Price</th>
							<th>Quantity</th>
							<th>Store</th>
						</tr>
					</thead>
				</table>
			</div>
			
		</div>
			
		</div>
		<div>
			<div class="width50 float-left">
				 <div class="width50 float-left">
					<label class="width40" for="month">Reconciliation Period</label>
					<select id="month" name="month" class="width30 ">
						<c:forEach items="${MONTH_LIST}" var="comp1" varStatus="status">
							<option value="${comp1}">${comp1}</option>
						</c:forEach>
					</select>
					<select id="year" name="year" class="width30 ">
						<option value="2015">2015</option>
						<option value="2014">2014</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
					</select>
				</div> 
				<div style="display:block;" class="width50 portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons" > 
					<div class="portlet-header ui-widget-header float-right width20" id="loadStock">Upload Stock</div>
					<div class="portlet-header ui-widget-header float-right width20" id="loadStockWithPeirod">Upload Stock Periodic</div>
					<input type="text" id="xlFileName" class="width70">
				</div>
			</div>
			<div class="width50 float-left" style="display:block;">
				<div>
					<label class="width40 float-left" for="fromDate">From Date</label>
					<input type="text" readonly="readonly" name="fromDate" id="fromDate" tabindex="4" class="width50 "/>
				</div>
				<div>
					<label class="width40 float-left" for="toDate">To Date</label>
					<input type="text" readonly="readonly" name="toDate" id="toDate" tabindex="4" class="width50"/>
				</div>
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right" id="receive-stock" >Receive By Period</div>
				</div>
			</div>
			
			
		</div>
		
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="stock-balance" >Update Stock Reconciliation</div>	 
			</div>
	</div>
</div>