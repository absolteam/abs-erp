<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td style="display: none;" id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><input type="text" id="serviceDate_${ROW_ID}"
		class="width96 serviceDate" readonly="readonly" />
	</td>
	<td><input type="text" class="width96 extimatedDate"
		readonly="readonly" id="extimatedDate_${ROW_ID}"/>
	</td>
	<td><select name="priority" id="priority_${ROW_ID}" class="width96 priority">
			<option value="">Select</option>
			<c:forEach var="priority" items="${ASSET_PRIORITY_TYPE}">
				<option value="${priority.key}">${priority.value}</option>
			</c:forEach>
	</select></td>
	<td><input type="text" readonly="readonly"
		class="width96 nextServiceDate" id="nextServiceDate_${ROW_ID}"/>
	</td>
	<td><input type="text" class="width70 serviceManager"
		readonly="readonly" id="serviceManager_${ROW_ID}" />
		<input type="hidden" id="serviceManagerId_${ROW_ID}"/> <span class="button"> <a
			style="cursor: pointer;" id="incharge_${ROW_ID}"
			class="btn ui-state-default ui-corner-all common-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" class="width96 linedescription"
		id="description_${ROW_ID}" value="${detail.description}" /> <input type="hidden"
		id="assetServiceDetailId_${ROW_ID}" />
	</td>
	<td style="width: 0.01%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working"> <span
			class="processing"></span> </a></td>
</tr>
<script type="text/javascript">
$(function(){
	$('.serviceDate,.extimatedDate,.nextServiceDate').datepick();
});
</script>