<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
}); 
</script>
<div id="main-content">
	<div id="py_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="height: 95% !important;">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Product
		</div> 
		<form name="addproductform" id="addProductValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="product-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="productId" name="productId"
					value="${PRODUCT_INFO.productId}" />
				<input type="hidden" id="usercode" value="false"/>
				<div class="width100 float-left" id="hrm">
					<div class="float-left width50" id="hrm">
						<fieldset style="min-height: 281px;">
							<div class="divpadding">
								<label class="width30"> Product Code </label> 
									<span
									class="width60 spanfont">${PRODUCT_INFO.code}</span>  
							</div> 
							<div class="divpadding">
								<label class="width30"> Product Name</label> 
									<span
									class="width60 spanfont">${PRODUCT_INFO.productName}</span>   
							</div>
							<div class="divpadding">
								<label class="width30">Unit Name</label>
									<span
									class="width60 spanfont">${PRODUCT_INFO.lookupDetailByProductUnit.displayName}</span>  
							</div>
							<div class="divpadding">
								<label class="width30">Item type</label> 
									<span
									class="width60 spanfont">${PRODUCT_INFO.itemTypeName}</span>   
							</div>
							<div class="divpadding">
								<label class="width30">Sub type
								</label>
								<span
									class="width60 spanfont">${PRODUCT_INFO.subItemTypeName}</span>    
							</div>
							<div class="divpadding">
								<label class="width30">Costing Type </label> 
									<span
									class="width60 spanfont">${PRODUCT_INFO.costingMethod}</span>    
							</div>
							<div class="divpadding">
								<label class="width30"> Status
								</label>  
								<span
									class="width60 spanfont">${PRODUCT_INFO.statusStr}</span>    
							</div>
							<div class="divpadding">
								<label class="width30"> Bar Code</label> <span
									class="width60 spanfont">${PRODUCT_INFO.barCode}</span>    
							</div> 
						</fieldset>
					</div>
					<div class="float-left  width48">
						<fieldset style="min-height: 281px;">
							<div style="height: 195px; overflow-y: auto;">
								<div class="divpadding">
									<label class="width30 float-left">Inventory </label>
									<c:choose>
										<c:when
											test="${PRODUCT_INFO.combinationByInventoryAccount.combinationId > 0}">
											<span id="productCombination" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
												${PRODUCT_INFO.combinationByInventoryAccount.accountByAnalysisAccountId.account}
												[${PRODUCT_INFO.combinationByInventoryAccount.accountByAnalysisAccountId.code}] </span>
										</c:when>
										<c:otherwise>
											<span id="inventoryAccountCode" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
											</span>
										</c:otherwise>
									</c:choose> 
								</div> 
								<div class="divpadding" id="expenseDiv" style="display: none;  
									position: relative; top: 5px; min-height: 30px;">
									<label class="width30 float-left">Expense</label>
									<c:choose>
										<c:when
											test="${PRODUCT_INFO.combinationByExpenseAccount.combinationId > 0}">
											<span id="expenseCombination" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
												${PRODUCT_INFO.combinationByExpenseAccount.accountByAnalysisAccountId.account}
												[${PRODUCT_INFO.combinationByExpenseAccount.accountByAnalysisAccountId.code}] </span>
										</c:when>
										<c:otherwise>
											<span id="expenseAccountCode" class="width50 float-left"
												style="position: relative; top: 3px; padding-bottom: 5px;">
											</span>
										</c:otherwise>
									</c:choose> 
								</div>
								<div class="divpadding">
									<label class="width30">Category </label> 
									 <span
									class="width60 spanfont">${PRODUCT_INFO.category.name}</span>    
								</div>
								<div class="divpadding">
									<label class="width30">Item Nature </label> 
									 <span
									class="width60 spanfont">${PRODUCT_INFO.itemNatureName}</span>    
								</div>
								<div class="divpadding">
									<label class="width30">Shelf</label>
									 <span
									class="width60 spanfont">${PRODUCT_INFO.storeName}</span>    
								</div>  
								<c:choose>
									<c:when test="${PRODUCT_INFO.specialPos eq true}">
										<div class="specialPOSDiv divpadding">
											<label for="publicName" class="width30">Public Name</label>
											<span
									class="width60 spanfont">${PRODUCT_INFO.publicName}</span>    
										</div> 
									</c:when> 
								</c:choose> 
							<c:choose>
								<c:when test="${PRODUCT_INFO.specialPos eq true}">
									<div class="specialPOSDiv divpadding">
										<span class="fileinput-preview">  
													<c:choose>
														<c:when test="${PRODUCT_INFO.productPic ne null && PRODUCT_INFO.productPic ne ''}">
															<img name="preview" id="preview" height="125" width="125"
														src="data:image/png;base64,${PRODUCT_INFO.productPic}">
														</c:when>
														<c:otherwise>
															<img alt="No Image" name="preview" id="preview" height="125"
											width="125" src="./images/No_Image.jpg">  
														</c:otherwise>
													</c:choose> 
												</span>  
										</div> 
								</c:when> 
							</c:choose>	
							</div>
						</fieldset>
					</div> 
				</div>
			</div>
			<div class="clearfix"></div>  
		</form>
	</div>
</div>
<div class="clearfix"></div> 