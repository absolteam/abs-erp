<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
#codecombination-popup{
	overflow: hidden;
}
</style>
<script type="text/javascript">
var combinationId=0;
var combinationId=""; 
var companyCode=""; 
var costCode=""; 
var naturalCode=""; 
var analyisCode=""; 
var buffer1Code=""; 
var bufferCode2=""; 
var oTable; var selectRow=""; var aSelected = []; var aData=""; 
var accountTypeId=0; 
$(function(){ 
	if($('#accountType').val()!="" && $('#accountType').val()!=null){
		accountTypeId=$('#accountType').val();
	}
	else{
		accountTypeId=0;
	} 
	
	$('#example').dataTable({ 
		"sAjaxSource": "getcombination_jsonlist.action?accountTypeId="+accountTypeId, 
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
		"aoColumns": [
			{ "sTitle": "COMBINATION_ID", "bVisible": false}, 
			{ "sTitle": '<fmt:message key="accounts.code.label.companyaccount"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.code.label.costaccount"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.code.label.naturalaccount"/>'},  
			{ "sTitle": '<fmt:message key="accounts.code.label.analysisaccount"/>'}, 
			{ "sTitle": '<fmt:message key="accounts.code.label.buffer1"/>'},  
			{ "sTitle": '<fmt:message key="accounts.code.label.buffer2"/>'}, 
		], 
		"sScrollY": $("#codecombination-popup").height() - 160,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	if(accountTypeId>0){ 
	}
	
	
	$('#example tbody tr').live('dblclick', function (){   
       	  aData =oTable.fnGetData( this );
       	  combinationId=aData[0]; 
          companyCode=aData[1]; 
          costCode=aData[2];
          naturalCode=aData[3];
          analyisCode=aData[4];
          buffer1Code=aData[5];
          bufferCode2=aData[6];
          $('#combinationId_json').val(combinationId);
          $('#company_json').val(companyCode);
          $('#cost_json').val(costCode);
          $('#natural_json').val(naturalCode);
          $('#analysis_json').val(analyisCode);
          $('#buffer1_json').val(buffer1Code);
          $('#buffer2_json').val(bufferCode2);  
          combinationauto(); 
          return false;
         // $("#combinationauto").click(); 
	});
	
	//init datatable
	oTable = $('#example').dataTable(); 
});
</script>
<div id="main-content"> 
	<input type="hidden" id="accountType" value="${accountType}"/> 
  	    <div class="tempresult" style="display:none;"></div>
 	<div id="trans_combination_accounts">
		<table class="display" id="example"></table>
	</div> 
	<input type="hidden" id="combinationId_json"/>
	<input type="hidden" id="company_json"/>
	<input type="hidden" id="cost_json"/>
	<input type="hidden" id="natural_json"/>
	<input type="hidden" id="analysis_json"/>
	<input type="hidden" id="buffer1_json"/>
	<input type="hidden" id="buffer2_json"/> 
</div>