<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<td colspan="11" class="tdidentity"
	id="childRowTD_${requestScope.rowid}">
	<div id="errMsg"></div>
	<form name="bankaccountinfo" id="bankaccountinfo-validate"
		style="position: relative;">
		<input type="hidden" name="addEditFlag" id="addEditFlag"
			value="${requestScope.AddEditFlag}" />
		<div class="float-left width50" id="hrm">
			<fieldset style="min-height: 230px;">
				<div>
					<label class="width30">Account Number<span
						style="color: red">*</span> </label> <input type="text"
						name="accountNumber" id="accountNumber"
						class="width50 validate[required]">
				</div>
				<div>
					<label class="width30">Account Type<span style="color: red">*</span>
					</label> <select name="accountType" id="accountType"
						class="width51 validate[required]">
						<option value="">Select</option>
						<c:forEach var="accountType" items="${ACCOUNT_TYPES}">
							<option value="${accountType.key}">${accountType.value}</option>
						</c:forEach>
					</select>
				</div>
				<div>
					<label class="width30">Branch Name<span style="color: red">*</span>
					</label> <input type="text" name="branchName" id="branchName"
						class="width50 validate[required]">
				</div>
				<div>
					<label class="width30">Account Holder</label> <input type="text"
						name="holderName" id="holderName" class="width50" maxlength="200"> 
				</div>
				<div>
					<label class="width30">Purpose</label> <input type="text"
						name="accountPurpose" id="accountPurpose"
						class="width50">
				</div>
				<div>
					<label class="width30">IBAN</label> <input type="text" 
						name="iban" id="iban" class="width50">
				</div>
				<div>
					<label class="width30">Routing Number</label> <input type="text"
						name="routingNumber" id="routingNumber"
						class="width50">
				</div>
				<div>
					<label class="width30">Swift Code/BIC</label> <input type="text"
						 name="iban" id="swiftCode" class="width50">
				</div>
			</fieldset>
		</div>
		<div class="width48 float-left" id="hrm">
			<fieldset style="min-height: 230px;">
				<div>
					<label class="width30">Credit Limit</label> <input type="text"
						 name="creditLimit" id="creditLimit" class="width50 validate[optional,customer[number]]">
				</div>
				<div>
					<label class="width30">Currency</label> <select
						name="currencyId" id="currencyId" class="width51">
						<c:forEach var="CURRENCY" items="${CURRENCY_LIST}">
							<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
						</c:forEach>
					</select>
				</div>
				<div>
					<label class="width30">Operation Mode</label> <select
						name="operationMode" id="modeOfOperation" class="width51">
						<option value="">Select</option>
						<c:forEach var="OPERATION" items="${MODE_OF_OPERATION}">
							<option value="${OPERATION.lookupDetailId}">${OPERATION.displayName}</option>
						</c:forEach>
					</select> <span class="button" style="position: relative;"> <a
						style="cursor: pointer;" id="MODE_OF_OPERATION"
						class="btn ui-state-default ui-corner-all modeofoperation-lookup width100">
							<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
				</div>
				<div>
					<label class="width30">Signatory 1</label> <select
						name="signatory1" id="signatory1" class="width51">
						<option value="">Select</option>
						<c:forEach var="SIGNATORY1" items="${SIGNATORIES}">
							<option value="${SIGNATORY1.lookupDetailId}">${SIGNATORY1.displayName}</option>
						</c:forEach>
					</select> <span class="button" style="position: relative;"> <a
						style="cursor: pointer;" id="SIGNATORIES"
						class="btn ui-state-default ui-corner-all banksignator1-lookup width100">
							<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
				</div>
				<div>
					<label class="width30">Signatory 2</label> <select
						name="signatory2" id="signatory2" class="width51">
						<option value="">Select</option>
						<c:forEach var="SIGNATORY2" items="${SIGNATORIES}">
							<option value="${SIGNATORY2.lookupDetailId}">${SIGNATORY2.displayName}</option>
						</c:forEach>
					</select> <span class="button" style="position: relative;"> <a
						style="cursor: pointer;" id="SIGNATORIES"
						class="btn ui-state-default ui-corner-all banksignator2-lookup width100">
							<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
				</div>
				<div>
					<label class="width30">Address</label> <input type="text"
						name="address" id="address" class="width50">
				</div>
				<div>
					<label class="width30">Contact Number</label> <input type="text"
						name="branchContact" id="branchContact"
						class="width50 validate[optional[custom[onlyNumber]]]">
				</div>
				<div>
					<label class="width30">Contact Person</label> <input type="text"
						name="contactPerson" id="contactPerson"
						class="width50">
				</div>

			</fieldset>
			<div id="othererror" class="response-msg error ui-corner-all"
				style="width: 60%; display: none;"></div>
		</div>
		<div class="clearfix"></div>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
			style="margin: 20px;">
			<div
				class="portlet-header ui-widget-header float-right cancel_account"
				id="cancel_${requestScope.rowid}" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div
				class="portlet-header ui-widget-header float-right update_account"
				id="update_${requestScope.rowid}" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="holder-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="holder-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</form>
</td>
<script type="text/javascript"> 
var accessCode = "";
var tempAccessCode = "";
 $(function(){   
	 $jquery("#bankaccountinfo-validate").validationEngine('attach'); 
		 
		  $('.cancel_account').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
					$("#AddImage_"+rowid).show();
				 	$("#EditImage_"+rowid).hide();
				}else{
					$("#AddImage_"+rowid).hide();
				 	$("#EditImage_"+rowid).show();
				}
				 $("#childRowTD_"+rowid).remove();	
				 $("#DeleteImage_"+rowid).show();
				 $("#WorkingImage_"+rowid).hide(); 
				 $('#holder-popup').dialog('destroy');		
			  	 $('#holder-popup').remove(); 
				 $('#openFlag').val(0);
			    
		  });
		  $('.update_account').click(function(){ 
			 //Find the Id for fetch the value from Id
			 if($jquery("#bankaccountinfo-validate").validationEngine('validate')){
				 var tempvar=$('.tdidentity').attr('id');
				 var idarray = tempvar.split('_');
				 var rowid=Number(idarray[1]);  
				 var bankAccountId=Number($('#bankAccountId_'+rowid).val());	
				 var accountNumber=$('#accountNumber').val();
				 var accountType=$('#accountType').val();
				 var accountPurpose=$('#accountPurpose').val();
				 var branchName=$('#branchName').val();
				 var address=$('#address').val();
				 var branchContact=$('#branchContact').val();
				 var contactPerson=$('#contactPerson').val(); 
				 var routingNumber=$("#routingNumber").val();
				 var iban=$("#iban").val();
				 var holderId = $('#holderName').val(); 
				 var creditLimit = Number($('#creditLimit').val()); 
				 var swiftCode = $('#swiftCode').val();
				 var currencyId = Number($('#currencyId').val());
				 var modeOfOperation = Number($('#modeOfOperation').val());
				 var signatory1 = Number($('#signatory1').val());
				 var signatory2 = Number($('#signatory2').val()); 
				 var lineId=$('#lineId_'+rowid).val();
				 var addEditFlag=$('#addEditFlag').val(); 
		      
		         var flag=false;
		         $.ajax({
						type:"POST", 
						url:"<%=request.getContextPath()%>/bank_account_save.action",
					 	async: false, 
					 	data:{
					 		bankAccountId: bankAccountId, accountNumber: accountNumber, accountType: accountType, accountPurpose: accountPurpose,
					 		branchName: branchName, address: address, branchContact: branchContact, contactPerson: contactPerson, creditLimit: creditLimit,
					 		rountingNumber: routingNumber, iban: iban, addEditFlag: addEditFlag, holderId: holderId, swiftCode: swiftCode, id: lineId,
					 		currencyId: currencyId, modeOfOperation: modeOfOperation, signatory1: signatory1, signatory2: signatory2
					 		},
					    dataType: "html",
					    cache: false,
						success:function(result){
							$('#holder-popup').dialog('destroy');		
					  		$('#holder-popup').remove(); 
							$('.tempresult').html(result); 
						     if(result!=null) {
                                  flag = true;
						     }
						     $('#openFlag').val(0);
					 	},  
					 	error:function(result){
					 		 $('.tempresult').html(result);
                          $("#othererror").html($('#returnMsg').html()); 
                          return false;
					 	} 
		        }); 
		        if(flag==true){ 
	        		//Bussiness parameter
        			$('#accountNumber_'+rowid).text(accountNumber); 
        			$('#accountType_'+rowid).text($('#accountType option:selected').text()); 
        			$('#accountPurpose_'+rowid).text(accountPurpose); 
        			$('#accountHolder_'+rowid).text($('#holderName').val());
        			$('#branchName_'+rowid).text(branchName);
        			$('#rountingNumber_'+rowid).text(routingNumber);
        			$('#iban_'+rowid).text(iban);
        			$('#swift_'+rowid).val(swiftCode);
        			$('#sign1_'+rowid).val(signatory1);
        			$('#sign2_'+rowid).val(signatory2);
        			$('#address_'+rowid).val(address); 
        			$('#holderidval_'+rowid).val(holderName); 
        			$('#currencyidval_'+rowid).val(currencyId); 
        			$('#modeidval_'+rowid).val(modeOfOperation); 
        			$('#typeidval_'+rowid).val(accountType);
        			$('#creditLimit_'+rowid).text(creditLimit);
        			$('#currency_'+rowid).text($('#currencyId option:selected').text());
        			if($('#modeOfOperation').val()!='')
        				$('#mode_'+rowid).text($('#modeOfOperation option:selected').text());
        			$('#branchContact_'+rowid).val(branchContact); 
        			$('#contactPerson_'+rowid).val(contactPerson);
        			 
					//Button(action) hide & show 
    				 $("#AddImage_"+rowid).hide();
    				 $("#EditImage_"+rowid).show();
    				 $("#DeleteImage_"+rowid).show();
    				 $("#WorkingImage_"+rowid).hide();  
        			 $("#childRowTD_"+rowid).remove();	 
        			
				  	var childCount=Number($('#childCount').val());
					childCount=childCount+1;
					$('#childCount').val(childCount); 
					triggerAddRow(rowid);
        		}		  	 
		      } 
		      else{return false;}
		  });

		  $('#person-list-close').live('click',function(){
				 $('#holder-popup').dialog('close');
			 });

		  $('#discard-lookup').live('click',function(){
			  $('#holder-popup').dialog('close');
			}); 

		$('.modeofoperation-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id");   
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.holder-result').html(result);
		                  $('#holder-popup').dialog('open');
		  				   $($($('#holder-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.holder-result').html(result);
		             }
		         });
		   return false;
		});	

		$('.banksignator1-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id"); 
			 tempAccessCode = "banksignatory1"; 
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.holder-result').html(result);
		                  $('#holder-popup').dialog('open');
		  				   $($($('#holder-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.holder-result').html(result);
		             }
		         });
		   return false;
		});	

		$('.banksignator2-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id");  
			 tempAccessCode = "banksignatory2";
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.holder-result').html(result);
		                  $('#holder-popup').dialog('open');
		  				   $($($('#holder-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.holder-result').html(result);
		             }
		         });
		   return false;
		});	

		//Lookup Data Roload call
		$('#save-lookup').live('click',function(){  
	 		if(accessCode=="MODE_OF_OPERATION"){
				$('#modeOfOperation').html("");
				$('#modeOfOperation').append("<option value=''>Select</option>");
				loadLookupList("modeOfOperation"); 
			} else if(accessCode=="SIGNATORIES"){
				if(tempAccessCode=="banksignatory1"){
					$('#signatory1').html("");
					$('#signatory1').append("<option value=''>Select</option>");
					loadLookupList("signatory1"); 
				}else{
					$('#signatory2').html("");
					$('#signasignatory2tory1').append("<option value=''>Select</option>");
					loadLookupList("signatory2"); 
				}
			}  
		});
	});
	 
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
