<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:forEach var="bean" items="${REQUISITION_DETAIL}" varStatus ="status">   
	<tr class="rowid" id="fieldrow_${status.index+1}"> 
		<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td>
		<td>
			 <span>${bean.itemType}</span>
		</td>  
		<td class="product" id="product_${status.index+1}"> 
			<span>${bean.product.productName}</span>
			<input type="hidden" id="productid_${status.index+1}" style="border:0px;" 
				value="${bean.product.productId}"/>
		</td> 
		<td id="uom_${status.index+1}" class="uom" style="display: none;">  
		</td>  
		<td >
		<input type="text" name="quantity" id="quantity_${status.index+1}" value="${bean.quantity}"
			class="width96 validate[required,custom[number]] quantity"/> 
		</td> 
		<td>
			<input type="text" name="unitrate" id="unitrate_${status.index+1}" value="${bean.unitRate}" style="text-align: right;"
				class="width96 validate[required,custom[number]] unitrate"/>
		</td> 
		<td class="totalamount"  style="text-align: right;">
			 <span id="totalamount_${status.index+1}">${bean.totalAmount}</span>
		</td> 
		<td style="display:none;">
			<input type="hidden" id="quotationLineId_${status.index+1}"/>
			<input type="hidden" id="requisitionLineId_${status.index+1}" value="${bean.requisitionDetailId}"/>
		</td>
		<td>
			 <input type="text" name="linedescription" id="linedescription_${status.index+1}"/>
		</td> 
		 <td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						<span class="ui-icon ui-icon-plus"></span>
			  </a>	
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
					<span class="ui-icon ui-icon-wrench"></span>
			  </a>  
		  	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
				  <span class="ui-icon ui-icon-circle-close"></span>
		 	  </a> 
			  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
				  <span class="processing"></span>
			 </a>
		</td>   
	</tr>
</c:forEach>