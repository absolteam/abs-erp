<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	var accountId = 0;
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		$('#ChartOfAccountListTable')
				.dataTable(
						{
							"sAjaxSource" : "natural_of_accounts_jsonlist.action?accountTypeId="
									+ $('#accountTypeId').val(),
							"sPaginationType" : "full_numbers",
							"bJQueryUI" : true,
							"iDisplayLength" : 25,
							"aoColumns" : [
									{
										"sTitle" : "ACCOUNT_ID",
										"bVisible" : false
									},
									{
										"sTitle" : '<fmt:message key="accounts.coa.label.accountcode"/>'
									},
									{
										"sTitle" : '<fmt:message key="accounts.coa.label.accountdescription"/>'
									}, {
										"sTitle" : "ACCOUNT_TYPE_ID",
										"bVisible" : false
									} ],
							"sScrollY" : $("#main-content").height() - 235,
							//"bPaginate": false,
							"aaSorting" : [ [ 0, 'desc' ] ],
							"fnRowCallback" : function(nRow, aData,
									iDisplayIndex) {
								if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
									$(nRow).addClass('row_selected');
								}
							}
						});

		//init datatable
		oTable = $('#ChartOfAccountListTable').dataTable();

		/* Click event handler */
		$('#ChartOfAccountListTable tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
			}
		});

		$('#ChartOfAccountListTable tbody tr').live('dblclick', function() {
			aData = oTable.fnGetData(this);
			commonCOAPopup(aData);
			$('#coaaccount-common-popup').trigger('click');
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.coa.label.chartofaccounts" />
		</div>
		<div class="portlet-content">
			<div id="chartOfAccountList">
				<table class="display" id="ChartOfAccountListTable"></table>
			</div>
		</div>
		<input type="hidden" id="accountTypeId"
			value="${requestScope.accountTypeId}" />
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right"
				id="coaaccount-common-popup">close</div>
		</div>
	</div>
</div>