<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
$(function(){
	 
	$('input,select').attr('disabled', true);
	 {
		 $.ajax({
				type: "POST",  
				url: "<%=request.getContextPath()%>/show_pos_user_till.action",  
		     	data: {posUserTillId: Number($('#posUserTillId').val())},  
		     	async: false,
				dataType: "json",
				cache: false,
				success: function(result){  
					pOSUserTillVOJson=result.pOSUserTillVOJson;
					$('#posUserTillId').val(pOSUserTillVOJson.posUserTillId);
					$('#employeeName').val(pOSUserTillVOJson.personName);
					$('#personId').val(pOSUserTillVOJson.personId);
					$('#storeName').val(pOSUserTillVOJson.storeName);
					$('#storeId').val(pOSUserTillVOJson.storeId);
					$('#tillNumber').val(pOSUserTillVOJson.tillNumber);
					$('#supervisorName').val(pOSUserTillVOJson.supervisorName);
					$('#supervisorId').val(pOSUserTillVOJson.supervisorId);
					$('#kitchenPrinter').val(pOSUserTillVOJson.kitchenPrinter);
					$('#barPrinter').val(pOSUserTillVOJson.barPrinter);
					if(pOSUserTillVOJson.isActive==1)
						$('#isActive').attr("checked",true);
					else
						$('#isActive').attr("checked",false);
						
		     	} 
		}); 
	 }
}); 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>POS User</div>	 	 
		<div class="portlet-content">  
	   	    <form id="POSUserTillForm" name="POSUserTillForm" style="position: relative;">
				<div id="hrm" class="float-left width70"  >
 					<fieldset style="min-height: 170px;">
						  <legend>POS User Till</legend> 
					      <input type="hidden" id="posUserTillId" value="${requestScope.posUserTillId}"/>
			      	  	<div>
			      	  		<label class="width30">Employee</label>
							<input type="text" id="employeeName" readonly="readonly" class=" width50 validate[required]"
								value=""/>
							<input type="hidden" id="personId" value="" class="personId" />
						</div>
						<div>
			      	  		<label class="width30">Store</label>
							<input type="text" id="storeName" readonly="readonly" class=" width50 validate[required]"
								value=""/> 
							<input type="hidden" id="storeId" value="" class="storeId" />
						</div>	
										
						<div>
							<label class="width30">Till Number</label>
							<input type="text" class="width50 tillNumber validate[required]" name="tillNumber" id="tillNumber"/>
						</div> 
						<div>
			      	  		<label class="width30">Supervisor</label>
							<input type="text" id="supervisorName" readonly="readonly" class=" width50 validate[required]"/>
							<input type="hidden" id="supervisorId" class="supervisorId" />
						</div>
						<div>
							<label class="width30">Kitchen Printer</label>
							<input type="text" class="width50 kitchenPrinter" name="kitchenPrinter" id="kitchenPrinter" />
						</div> 
						<div>
							<label class="width30">Bar Printer</label>
							<input type="text" class="width50 barPrinter" name="barPrinter" id="barPrinter" />
						</div> 
						<div>
							<label class="width30">Active</label>
							<input type="checkbox" class="width5" id="isActive" />
						</div> 	
						<div class="clearfix"></div> 
				 </fieldset>
			</div>
		</form> 
		</div>
	</div> 
</div>