<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="hrm" class="hastable width100">
	<div class="float-right width48">
		<table id="hastab" class="width100">
			<thead>
				<tr>
					<th>Store</th>
					<th>Section</th>
					<th>Rack</th>
					<th>Shelf</th>
				</tr>
			</thead>
			<tbody class="tab">
				<c:choose>
					<c:when test="${STOCK_DETAILS ne null && STOCK_DETAILS ne ''}">
						<c:forEach items="${STOCK_DETAILS}" var="stock">
							<tr> 
								<td>${stock.shelf.shelf.aisle.store.storeName}</td>
								<td>${stock.shelf.shelf.aisle.sectionName}</td>
								<td>${stock.shelf.shelf.name}</td>
								<td>${stock.shelf.name}</td>
							</tr>
						</c:forEach> 
					</c:when>
					<c:otherwise>
						<tr> 
							<td colspan="4"> No stock available.</td>
						</tr>
					</c:otherwise>
				</c:choose> 
			</tbody>
		</table>
	</div>
</div>