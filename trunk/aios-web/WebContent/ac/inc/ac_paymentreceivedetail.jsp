 <%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:forEach var="bean" varStatus="status" items="${RECEIVE_DETAIL_INFO}"> 
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
		<td id="productcode_${status.index+1}"> 
			${bean.product.code}
		</td>
		<td id="productname_${status.index+1}"> 
			${bean.product.productName}
		</td> 
		<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
		<td id="receiveqty_${status.index+1}">${bean.receiveQty}</td> 
		<td id="unitrate_${status.index+1}" style="text-align: right;">
			 <c:set var="units" value="${bean.unitRate}"/>
			<%=AIOSCommons.formatAmount(pageContext.getAttribute("units")) %>
			<input type="hidden" id="unitrateval_${status.index+1}" value="${bean.unitRate}"/>
		</td>   
		<td id="total_${status.index+1}" style="text-align: right;">
			 <c:set var="total" value="${bean.receiveQty * bean.unitRate}"/>
			 <%=AIOSCommons.formatAmount(pageContext.getAttribute("total")) %>
		</td>  
		<td style="display:none"> 
			<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
			<input type="hidden" id="detailid_${status.index+1}">
		</td> 
		<td> 
			<input type="text" name="lineDescription" id="lineDescription_${status.index+1}"/> 
		</td> 
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){
	var total=0;
	$('.rowid').each(function(){ 
		var rowId=getRowId($(this).attr('id')); 
		var totalRegx = new RegExp(',', 'g'); 
		var temptotal=Number($('#total_'+rowId).text().trim().replace(totalRegx,''));   
		total=Number(temptotal+total); 
	});
	$('#amount').val(convertToAmount(total)); 
});
</script>