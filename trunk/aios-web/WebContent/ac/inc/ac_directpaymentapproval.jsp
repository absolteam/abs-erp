<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
 <style>
 .spanfont {
	font-weight: normal;
	position: relative;
	top: 7px;
}
 </style>
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var directPaymentId=null;
$(function(){ 
	
	directPaymentId=Number($('#directPaymentId').val());  
	
}); 

function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	if(returnVO.returnStatusName=='AddApproved' 
			|| returnVO.returnStatusName=='ModificationApproved'
			|| returnVO.returnStatusName=='DeleteApproved'){
			try {
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/direct_payment_approval_save.action", 
				 	async: false,
				 	data: {directPaymentId:directPaymentId,messageId:messageId,
				 		workflowReturnMessage:returnVO.returnStatusName,
				 		processType:returnVO.processType},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
					}
			 });
			
		} catch (e) {
	
		}
	}
}
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 Direct Payment
			</div>	 
			<div><form id="directPaymentCreation">
			<div style="width: 98%; margin: 10px;">
			<div>
			<div class="tempresult" style="display: none;"></div>
			<input type="hidden" id="chequeFlag" value="false"/>
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset style="min-height: 80px"> 
						<input type="hidden" id="directPaymentId" value="${DIRECT_PAYMENT.directPaymentId}"> 							 
						<div style="padding: 3px;">
							<label class="width30">Payment Voucher</label> 
							<span class="width60 view spanfont">${DIRECT_PAYMENT.paymentNumber}</span>
										
						</div> 
						<div style="padding: 3px;">
							<label class="width30">Payment Date</label> 
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.paymentDate ne null && DIRECT_PAYMENT.paymentDate ne ''}">
									<c:set var="payDate" value="${DIRECT_PAYMENT.paymentDate}"/>  
									<%String date = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
									<span class="width60 view spanfont"><%=date%></span> 
								</c:when>  
								<c:otherwise>  
									<span class="width60 view"></span> 
								</c:otherwise>
							</c:choose> 
						</div>   
						 <c:choose>
						 	<c:when test="${DIRECT_PAYMENT.paymentMode eq 1 || DIRECT_PAYMENT.paymentMode eq 5}"> 
						 		<div class="cash" style="padding: 3px;">
									<label class="width30 spanfont">Cash Account</label>
									<input type="hidden" name="combinationId" id="cashCombinationId" value="${DIRECT_PAYMENT.combination.combinationId}"/> 
									<span class="width60 view spanfont">${DIRECT_PAYMENT.combination.accountByNaturalAccountId.code}[${DIRECT_PAYMENT.combination.accountByNaturalAccountId.account}]</span>
								</div>
						 	</c:when>
						 	<c:when test="${DIRECT_PAYMENT.paymentMode eq 2}">
						 		 
						 		<div style="padding: 3px;">
									<label class="width30">Account No</label>
									<span class="width60 spanfont">${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.accountNumber}]</span>
									<input type="hidden" readonly="readonly" name="accountId" id="accountId" value="${DIRECT_PAYMENT.bankAccount.bankAccountId}"/>
								</div>
								<div style="padding: 3px;">
									<label class="width30">Cheque No</label> 
									<span class="width60 spanfont">${DIRECT_PAYMENT.chequeNumber}</span>
								</div>
								<div style="padding: 3px;">
									<label class="width30">Cheque Date</label> 
									<c:set var="chequeDate" value="${DIRECT_PAYMENT.chequeDate}"/>  
									<%String chequeDate = DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString());%>
									<span class="width60"><%=chequeDate%></span>
								</div>
						 	</c:when>
						 	<c:when test="${DIRECT_PAYMENT.paymentMode eq 3 || DIRECT_PAYMENT.paymentMode eq 4}">
						 		 
						 		<div style="padding: 3px;">
									<label class="width30">Card No</label>
									<span class="width60 spanfont">${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.cardNumber}]</span>
									<input type="hidden" readonly="readonly" name="cardAccountId" id="cardAccountId" value="${DIRECT_PAYMENT.bankAccount.bankAccountId}"/>
								</div> 	 
						 	</c:when>
						 	<c:when test="${DIRECT_PAYMENT.paymentMode eq 6}">
						 		 
						 		<div style="padding: 3px;">
									<label class="width30">Transfer From</label>
									<span class="width60 spanfont">${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.accountNumber}]</span>
									<input type="hidden" readonly="readonly" name="transferAccountId" id="transferAccountId" value="${DIRECT_PAYMENT.bankAccount.bankAccountId}"/>
								</div> 	 
						 	</c:when>
						 </c:choose> 
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset style="min-height: 80px"> 		
						 <div style="padding: 3px;">
							<label class="width30 tooltip">Currency</label>
							<span class="width60 view spanfont">${DIRECT_PAYMENT.currency.currencyPool.code}</span>
							<input type="hidden" id="default_currency" value="${DEFAULT_CURRENCY}"/>
						</div> 
						<div style="padding: 3px;">
							<label class="width30">Payable</label> 
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.supplier.person ne null && DIRECT_PAYMENT.supplier.person ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.supplier.person.firstName} ${ DIRECT_PAYMENT.supplier.person.lastName}</span>
								</c:when>
								<c:when test="${DIRECT_PAYMENT.supplier.company ne null && DIRECT_PAYMENT.supplier.company ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.supplier.company.companyName}</span>
								</c:when>
								<c:when test="${DIRECT_PAYMENT.supplier.cmpDeptLocation ne null && DIRECT_PAYMENT.supplier.cmpDeptLocation ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyName}</span>
								</c:when>  
								 <c:when test="${DIRECT_PAYMENT.customer.personByPersonId ne null && DIRECT_PAYMENT.customer.personByPersonId ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.customer.personByPersonId.firstName} ${ DIRECT_PAYMENT.customer.personByPersonId.lastName}</span>
								</c:when>
								<c:when test="${DIRECT_PAYMENT.customer.company ne null && DIRECT_PAYMENT.customer.company ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.customer.company.companyName}</span>
								</c:when>
								<c:when test="${DIRECT_PAYMENT.customer.cmpDeptLocation ne null && DIRECT_PAYMENT.customer.cmpDeptLocation ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.customer.cmpDeptLocation.company.companyName}</span>
								</c:when> 
								<c:when test="${DIRECT_PAYMENT.personByPersonId ne null && DIRECT_PAYMENT.personByPersonId ne '' }">
									<span class="width60 view spanfont">${DIRECT_PAYMENT.personByPersonId.firstName} ${DIRECT_PAYMENT.personByPersonId.lastName}</span>
								</c:when> 
								<c:otherwise>
									<span class="width60 view spanfont">${DIRECT_PAYMENT.others}</span>
								</c:otherwise>
							</c:choose>  
						</div> 
						<div style="padding: 3px;">
							<label class="width30 tooltip">Description</label>
							<span class="width60 view spanfont">${DIRECT_PAYMENT.description}</span>
						</div> 
					</fieldset>
				</div> 
			</div>
		
			<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
				<fieldset> 	
					<legend>Payment Information</legend>
					 <table id="hastab" class="width100"> 
								<thead>
								   <tr>   
								   	    <th style="width:1%">L.No</th>
								   	    <th style="width:2%">Invoice No.</th>   
									    <th style="width:5%">Account Code</th>   
									    <th style="width:2%">Amount</th>   
									    <th style="width:5%">Description</th> 
								  </tr>
								</thead> 
								<tbody class="tab">  
									<c:choose>
										<c:when test="${DIRECT_PAYMENT_DETAIL ne null && DIRECT_PAYMENT_DETAIL ne ''}">
											<c:forEach var="PAYMENT_DETAIL" items="${DIRECT_PAYMENT_DETAIL}" varStatus="status"> 
												<tr class="rowid" id="fieldrow_${status.index+1}"> 
													<td id="lineId_${status.index+1}">${status.index+1}</td> 
													<td>
														${PAYMENT_DETAIL.invoiceNumber}
													</td> 
													 <td>
														<input type="hidden" name="combinationId_${status.index+1}" value="${PAYMENT_DETAIL.combination.combinationId}" id="combinationId_${status.index+1}"/> 
														${PAYMENT_DETAIL.combination.accountByNaturalAccountId.code}[${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}]
													</td>
													<td>
														${PAYMENT_DETAIL.amount}
													</td> 
													<td>
													  ${PAYMENT_DETAIL.description}
													</td>
													<td style="display:none;">
														<input type="hidden" id="directPaymentLineId_${status.index+1}" value="${PAYMENT_DETAIL.directPaymentDetailId}"/>
													</td> 
													
												</tr>
											</c:forEach>
										</c:when>
									</c:choose> 
									
						 </tbody>
					</table>
					</fieldset>
			</div>		
			</div> 
			</form> 
			</div> 
			
		</div>
</div>