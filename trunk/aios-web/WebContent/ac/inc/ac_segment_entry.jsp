<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 28% !important;
	height: 15% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
	top: 100px !important;;
}
</style>
<div id="hrm" class="width100 float-left">
	<input type="hidden" id="segmentDetailId" value="${requestScope.segmentDetailId}"/>
	<div class="width100 float-left">
		<label class="width30">Segment Type</label> 
		<label class="width60" style="font-weight: bold;">${PARENT_SEGMENT.accessCode}</label>
	</div>
	<div class="width100 float-left" style="position: relative; top:8px;">
		<label class="width30">Segment Name<span style="color: red;">*</span>
		</label> <input type="text" value="${SEGMENT_INFO.segmentName}"
			id="segmentName" class="width40" />
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="position: absolute; right: 0px; margin: 10px; bottom: 0px; overflow: hidden;">
		<div class="portlet-header ui-widget-header float-right segment_discard"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div> 
		<div class="portlet-header ui-widget-header float-right segment_save"
			style="cursor: pointer;" id="addSegment">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
</div>
