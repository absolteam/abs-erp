<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@page import="java.text.DecimalFormat;" %>
<script type="text/javascript">
$(function(){ 
	$('.emiDueDate').datepick();
	if($('.rowid').hasClass('userrow')){
		manupulateLastRow();
	} 
	
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/emi_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	
	$('.doblur').live('change',function(){ 
		var rowId=getRowId($(this).attr('id')); 
		triggerAddRow(rowId);
		//calculateBalance(rowId);
		return false;
	});  
	
	$('.extend').live('change',function(){ 
		// $('.calculate').show(); 
		// $('#savediscard').show();
		return false;
	});  
	
	var calculateBalance = function(rowId){
		var dueDate=$('#dueDate_'+rowId).val();  
		var instalment=$('#instalment_'+rowId).val();
		var interest=$('#interest_'+rowId).val();
		var eiborAmount=$('#eiborAmount_'+rowId).val();
		var insurance=$('#insurance_'+rowId).val();
		var otherCharges=$('#addtionalcharges_'+rowId).val(); 
		if(typeof instalment!='undefined' && instalment!=null && instalment!="" 
				&& typeof interest!= 'undefined' && interest!=null && interest!=""){
			instalment=instalment.replace(/,/gi,''); 
			interest=interest.replace(/,/gi,'');
			if(typeof eiborAmount=='undefined' || eiborAmount==null || eiborAmount!=""){
				eibor = 0;
			}else{
				eiborAmount=eiborAmount.replace(/,/gi,'');
			}
			if(typeof insurance=='undefined' || insurance==null || insurance!=""){
				insurance = 0;
			}else{
				insurance=insurance(/,/gi,'');
			}
			if(typeof otherCharges=='undefined' || otherCharges==null || otherCharges==""){
				otherCharges = 0;
			}else{
				otherCharges=otherCharges(/,/gi,'');
			}
			 $('.build_schedule').trigger('click');
		} 
				 
		 
	};
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid userrow').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var dueDate=$('#dueDate_'+rowId).val();  
	 var instalment=$('#instalment_'+rowId).val();
	 var interest=$('#interest_'+rowId).val();
	 var eibor=$('#eiborAmount_'+rowId).val();
	 var insurance=$('#insurance_'+rowId).val();
	 var addtionalcharges=$('#addtionalcharges_'+rowId).val();
	 var depreciation=$('#depreciation_'+rowId).val();
	 var cumulative=$('#cumulative_'+rowId).val();
	 var balance=$('#balance_'+rowId).val();
	 var description=$('#description_'+rowId).val();
	 var nexttab=$('#fieldrow_'+rowId).next();  
	if(dueDate!=null && dueDate!=""
		&& instalment!=null && instalment!=""
		&& interest!=null && interest!=""  
		&& $(nexttab).hasClass('lastrow')){  
			$('.addrows').trigger('click');
	} 
}
</script>
<fieldset>
		<legend>EMI Schedule</legend>
		<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div> 
		<div id="hrm" class="hastable width100"  >  
		<input type="hidden" name="childCount" id="childCount" value=""/>  
		<table id="hastab" class="width100"> 
			<thead>
		   		<tr> 
					<th style="width:1%">Line No</th> 
			   	 	<th style="width:6%">Due Date</th>  
			   	 	<c:choose>
   	 					<c:when test="${requestScope.EMI_DETAIL_PAYMENT ne null && requestScope.EMI_DETAIL_PAYMENT ne '' && fn:length(requestScope.EMI_DETAIL_PAYMENT)>0}">
   	 						<c:forEach var="beans" items="${EMI_DETAIL_HEAD}" varStatus="statush">
   	 							<th class="emihead" id="head_${statush.index+1}" style="width:5%">${beans.name}</th>
   	 						</c:forEach>
   	 					</c:when>
   	 					<c:otherwise> 
   	 						<c:set var="i" value="${1}"/>
							<th class="emihead" id="head_${i}" style="width:5%">Instalment</th>  
							<c:set var="i" value="${i+1}"/>
			   	 			<th class="emihead" id="head_${i}" style="width:5%">Interest</th>   
			   	 			<c:if test="${requestScope.ALERT_LOAN_DETAILS.eibor ne null && requestScope.ALERT_LOAN_DETAILS.eibor ne ''}">
			   	 				<c:set var="i" value="${i+1}"/>
			   	 				<th class="eiborhead emihead" id="head_${i}" style="width:5%">EIBOR</th> 
			   	 			</c:if>
			   	 			<c:choose>
					   	 		<c:when test="${requestScope.LOAN_CHARGES_DETAIL ne null && requestScope.LOAN_CHARGES_DETAIL ne ''}">
					   	 			<c:forEach var="cbean" items="${LOAN_CHARGES_DETAIL}">
					   	 				<c:set var="i" value="${i+1}"/>
					   	 				<th class="insurancehead emihead" id="head_${i}" style="width:5%">${cbean.name}</th>
					   	 			</c:forEach>
					   	 		</c:when>
					   	 	</c:choose>
					   	 	<c:set var="i" value="${i+1}"/>
			   	 			<th id="head_${i}" style="width:5%" class="emihead">Depreciation</th>
			   	 			<c:set var="i" value="${i+1}"/>
			   	 			<th id="head_${i}" style="width:5%" class="emihead">Cumulative</th>  
			   	 			<c:set var="i" value="${i+1}"/>  
			   	 			<th id="head_${i}" style="width:5%" class="emihead">Balance</th>   
						</c:otherwise>	 
   	 			  </c:choose> 
   	 			  <th style="width:5%">Description</th>	
	 		 </tr>
			</thead> 
			<tbody class="tab">  
				<c:choose>
					<c:when test="${requestScope.EMI_DETAIL_PAYMENT ne null && requestScope.EMI_DETAIL_PAYMENT ne '' && fn:length(requestScope.EMI_DETAIL_PAYMENT)>0}">
						<c:forEach var="emibean" items="${EMI_DETAIL_PAYMENT}" varStatus="estatus"> 
							<tr class="paid">
								<td id="lineId_${estatus.index+1}">${estatus.index+1}</td> 
								<td class="paid"> 
									<input type="text" name="emiDueDate" class="emiDueDate width95 paid" readonly="readonly" id="dueDate_${estatus.index+1}" 
										style="border:0px;" value="${emibean.emiDueDate}"/>
								</td> 
								<c:choose>
			   	 					<c:when test="${requestScope.EMI_DETAIL_HEAD ne null && requestScope.EMI_DETAIL_HEAD ne ''}">
			   	 						<c:forEach var="beans" items="${EMI_DETAIL_HEAD}">
			   	 							<td class="paid">
			   	 								<c:set var="paidAmount" value="${beans.amount}"/>
												<%String paidAmount = AIOSCommons.formatAmount(pageContext.getAttribute("paidAmount"));
																pageContext.setAttribute("paidAmount", paidAmount);%>
			   	 								<input type="text" value="${paidAmount}" class="width95 paid" readonly="readonly" style="border:0px;text-align: right;"/> 
			   	 							</td> 
			   	 						</c:forEach>
			   	 					</c:when>
			   	 				</c:choose>
			   	 				<td class="paid">
									<input type="text" readonly="readonly" name="description" class="description width95 paid" id="description_${estatus.index+1}" value="${emibean.description}" style="border:0px;"/>
								</td> 
							</tr> 
						</c:forEach>
					</c:when>
				</c:choose>	
				<c:choose>
					<c:when test="${requestScope.LOAN_SCHEDULE ne null && requestScope.LOAN_SCHEDULE ne '' && fn:length(requestScope.LOAN_SCHEDULE)>0}">
						<c:forEach var="bean" items="${LOAN_SCHEDULE}" varStatus ="status"> 
							<tr class="rowid" id="fieldrow_${status.index+1}">
								<td id="lineId_${status.index+1}">${bean.lineNumber}</td> 
								<td> 
									<input type="text" name="emiDueDate" class="emiDueDate width95" readonly="readonly" id="dueDate_${status.index+1}" 
										style="border:0px;" value="${bean.emiDueDate}"/>
								</td> 
								<td>
										<c:set var="instalment" value="${bean.instalment}"/>
										<%String instalment = AIOSCommons.formatAmount(pageContext.getAttribute("instalment"));
														pageContext.setAttribute("instalment", instalment);%>
										<input type="text" style="text-align:right;border:0px;" readonly="readonly" class="width95 principal extend" 
											id="instalment_${status.index+1}" value="${instalment}"/>
									</td> 
									<td>
										<c:set var="interest" value="${bean.interest}"/>
										<%String interest = AIOSCommons.formatAmount(pageContext.getAttribute("interest"));
														pageContext.setAttribute("interest", interest);%>
										<input type="text" style="text-align:right;border:0px;width:92%;" readonly="readonly" class="interest extend" id="interest_${status.index+1}"
											value="${interest}" />
									</td>
									<c:choose>
										<c:when test="${bean.eiborAmount ne 0 && bean.eiborAmount!= 0}">
											<td>
												<c:set var="eiborAmount" value="${bean.eiborAmount}"/>
												<%String eiborAmount = AIOSCommons.formatAmount(pageContext.getAttribute("eiborAmount"));
																pageContext.setAttribute("eiborAmount", eiborAmount);%>
												<input type="text" style="text-align:right;border:0px;" readonly="readonly" class="width95 eiborAmount extend" id="eiborAmount_${status.index+1}"
													value="${eiborAmount}" />
											</td>
										</c:when> 
									</c:choose> 
									<c:choose>
							   	 		<c:when test="${bean.loanCharges ne null && bean.loanCharges ne ''}">
							   	 			<c:forEach var="cbean" items="${bean.loanCharges}"> 
												<td> 
													<c:set var="insurance" value="${cbean.value}"/>
													<%String insurance = AIOSCommons.formatAmount(pageContext.getAttribute("insurance"));
																pageContext.setAttribute("insurance", insurance);%>
													<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width90 insurance extend" id="insurance_${status.index+1}"
														value="${insurance}"/>
												 </td>  
							   	 			</c:forEach>
							   	 		</c:when>
							   	 	</c:choose>
									<td>
										<c:set var="emiDepreciation" value="${bean.emiDepreciation}"/>
										<%String emiDepreciation = AIOSCommons.formatAmount(pageContext.getAttribute("emiDepreciation"));
														pageContext.setAttribute("emiDepreciation", emiDepreciation);%>
										<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width95 depreciation" id="depreciation_${status.index+1}"
											value="${emiDepreciation}"/>
									</td>
									<td>
										<c:set var="emiCumulative" value="${bean.emiCumulative}"/>
										<%String emiCumulative = AIOSCommons.formatAmount(pageContext.getAttribute("emiCumulative"));
														pageContext.setAttribute("emiCumulative", emiCumulative);%>
										<input type="text" readonly="readonly" style="text-align:right;border:0px;"  class="width95 cumulative" id="cumulative_${status.index+1}"
											value="${emiCumulative}"/>
									</td>
									<td>
									<c:set var="amount" value="${bean.amount}"/>
										<%String amount = AIOSCommons.formatAmount(pageContext.getAttribute("amount"));
														pageContext.setAttribute("amount", amount);%>
										<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width95 balance" id="balance_${status.index+1}"
											value="${amount}"/>
									</td>  
								<td>
									<input type="text" readonly="readonly" name="description" class="description width95" id="description_${status.index+1}" value="${bean.description}" style="border:0px;"/>
								</td> 
								<td style="display:none;">
									<input type="text" name="status" class="status width95" id="status_${status.index+1}" value="${bean.paidStatus}" style="border:0px;"/>
								</td>
							</tr>
						</c:forEach> 
					</c:when>  
				</c:choose>
			</tbody>
		</table>
	</div> 
</fieldset>  