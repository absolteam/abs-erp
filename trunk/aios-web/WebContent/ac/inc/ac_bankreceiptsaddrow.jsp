<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td id="lineId_${ROW_ID}" style="display: none;">${ROW_ID}</td>
	<td><select name="paymentMode" class="paymentMode"
		id="paymentMode_${ROW_ID}">
			<option value="">Select</option>
			<c:choose>
				<c:when test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
					<c:forEach items="${MODE_OF_PAYMENT}" var="MODE">
						<option value="${MODE.key}">${MODE.value}</option>
					</c:forEach>
				</c:when>
			</c:choose>
	</select></td>
	<td><input type="text" name="institutionName"
		id="institutionName_${ROW_ID}" class="institutionName"></td>
	<td><input type="text" name="institutionRef"
		id="institutionRef_${ROW_ID}" class="institutionRef"></td>
	<td><input type="text" name="institutionDate" readonly="readonly"
		id="institutionDate_${ROW_ID}" class="institutionDate"></td>
	<td style="display: none;">
		<select name="referenceDetail" class="referenceDetail"
			id="referenceDetail_${ROW_ID}">
				<option value="">Select</option>
				<c:choose>
					<c:when
						test="${RECEIPT_REFERENCE ne null && RECEIPT_REFERENCE ne ''}">
						<c:forEach items="${RECEIPT_REFERENCE}" var="reference">
							<option
								value="${reference.lookupDetailId}">${reference.displayName}</option>
						</c:forEach>
					</c:when>
				</c:choose>
			</select>
			<span
			class="button" style="position: relative;"> <a
				style="cursor: pointer;" id="BANK_RECEIPT_REFERENCE_${status.index+1}"
				class="btn ui-state-default ui-corner-all reference_category_lookup width100">
			<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><input type="text" class="combinationlines width60" value="${requestScope.COMBINATION_LINECODE}"
		readonly="readonly" id="combinationlines_${ROW_ID}" /> <input value="${requestScope.COMBINATION_LINEID}"
		type="hidden" id="combinationId_${ROW_ID}" /> <span
		class="button width10 float-right" id="cashcode_${ROW_ID}"
		style="position: relative; top: 6px; right: 8px;">
			<a style="cursor: pointer;" id="combination_${ROW_ID}"
			class="btn ui-state-default ui-corner-all codecombination-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text"
		class="amount validate[optional,custom[number]]"
		id="amount_${ROW_ID}" style="text-align: right;" /></td>
	<td style="display: none;"><select name="paymentStatus" class="paymentStatus"
		id="paymentStatus_${ROW_ID}">
			<option value="">Select</option>
			<c:choose>
				<c:when test="${PAYMENT_STATUS ne null && PAYMENT_STATUS ne ''}">
					<c:forEach items="${PAYMENT_STATUS}" var="PAY_STATUS">
						<option value="${PAY_STATUS.key}">${PAY_STATUS.value}</option>
					</c:forEach>
				</c:when>
			</c:choose>
	</select></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${ROW_ID}" /></td>
	<td style="display: none;"><input type="hidden"
		id="bankReceiptsLineId_${ROW_ID}" /> <input type="hidden"
		id="changeFlag_${ROW_ID}" /> <input type="hidden"
		name="recordId" id="recordId_${ROW_ID}" /> <input
		type="hidden" name="useCase" id="useCase_${ROW_ID}" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a></td>
</tr>
<script type="text/javascript">
$(function(){
	$('.institutionDate').datepick({onSelect: validateCheque, showTrigger: '#calImg'});
	$jquery("#bankReceiptsForm").validationEngine('attach'); 
	$jquery(".amount").number(true, 2); 
});
</script>