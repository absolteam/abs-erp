<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
</style>
<script type="text/javascript">
var accountTypeId=0;
var slidetab;
var tempid;
$(function(){
	 $jquery("#coavalidate-form").validationEngine('attach'); 
	 
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0);  
	      $('.ui-dialog-titlebar').remove();   
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
	
	 $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: false	
		}); 
	 
	 $('.discard').click(function(){
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);
			},
			error: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		});
	 });
	 
	 $(document).keyup(function(e) { 
		  if (e.keyCode == 27){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	});  
	 
	 $('.skip').click(function(){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customise_contract_combination.action", 
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){  
					$('#codecombination-popup').dialog('destroy');		
			  		$('#codecombination-popup').remove(); 
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
	  });
	 
	 $('.save').click(function(){
		var combinationId=$('#combinationId').val();
		var revenueId=$('#revenueId').val();
		if($jquery("#coavalidate-form").validationEngine('validate')){
 			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/create_customise_property.action",
				data:{revenueId:revenueId, combinationId:combinationId},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresult").html(result);
					var message=$('#returnMsg').html();  
					if(message.trim()=="Success"){  
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/customise_contract_combination.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$('#codecombination-popup').dialog('destroy');		
						  		$('#codecombination-popup').remove(); 
								$("#DOMWindow").html(result);  
								$.scrollTo(0,300);
							} 		
						});  
					 }
					else{
						$('.page-error').hide().html($('#returnMsg').html()).slideDown(1000);
						return false;
					}
				}
			 });
		}
		else{
			return false;
		}
	 });
});
function setCombination(combinationTreeId,combinationTree){ 
	tempid=$(tempid).attr('id'); 
	if(tempid=="assetcode"){
		$('#combinationId').val(combinationTreeId);
		$('#combinationcode').val(combinationTree);  
	}
	else if(tempid=="revenuecode"){
		$('#revenueAccount').val(combinationTree);
		$('#revenueId').val(combinationTreeId); 
	}  
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create Asset Account</div>
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;"> 
				<div class="width60" id="hrm">
					<fieldset>
						<legend>Asset Details</legend>
						<div>
							<label class="width30">Asset Account<span style="color: red;">*</span></label>
							<input type="hidden" name="combinationId" id="combinationId"/> 
							<input type="text" name="combinationcode" id="combinationcode" readonly="readonly" class="width48 validate[required]">
							<span class="button" id="assetcode">
								<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
							 <input type="hidden" name="codeassetCombination" id="codeCombination"/> 
						</div>
						<div>
							<label class="width30">Rent Revenue<span style="color: red;">*</span></label>
							<input type="hidden" name="revenueId" id="revenueId"/> 
							<input type="text" name="revenueAccount" id="revenueAccount" readonly="readonly" class="width48 validate[required]"/>
							<span class="button" id="revenuecode">
								<a style="cursor: pointer;" TABINDEX=23 class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						 	<input type="hidden" name="coderentCombination" id="codeCombination"/>  
						</div>
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="assetcode" style="font-weight:bold;"></div>
				<div class="rentcode" style="font-weight:bold;"></div>
				<div class="clearfix"></div>  
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.cancel"/></div> 
					<div class="portlet-header ui-widget-header float-right save" id="add"><fmt:message key="accounts.common.button.save"/></div>
				</div> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
					<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
				</div>
			</form>
		</div>	
	</div>		
	<div style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div>
</div>