<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
 <style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
	
	$('input, select, checkbox').attr('disabled', true);
}); 
</script>
<div id="main-content">
	<div id="cy_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<form id="editcurrencyValidate" name="editcurrency">
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>currency</div> 

			<div class="portlet-content"> 
				<div id="hrm">
					<fieldset>
						<div>
							<label class="width20"><fmt:message
									key="accounts.currencymaster.label.currencycode" />
							</label> <select name="curId" style="width: 16%;" id="selCode"
								class="validate[required]" disabled="disabled">
								<option value="">--Select--</option>
								<c:if test="${CURRENCYPOOLINFO ne null }">
									<c:forEach items="${CURRENCYPOOLINFO}" var="bean">
										<c:choose>
											<c:when
												test="${bean.currencyId eq CURRENCYINFO.currencyPoolId}">
												<option value="${bean.currencyId}" selected="selected">${bean.currencyCountry}--${bean.currencyCode}</option>
											</c:when>
											<c:otherwise>
												<option value="${bean.currencyId}">${bean.currencyCountry}--${bean.currencyCode}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
							</select>
						</div>
						<div>
							<label class="width20">Default</label>
							<c:if test="${CURRENCYINFO.defaultCurrency eq true}">
								<input type="checkbox" name="defaultCurrency"
									id="defaultCurrency" checked="checked" />
							</c:if>
							<c:if test="${CURRENCYINFO.defaultCurrency eq false}">
								<input type="checkbox" name="defaultCurrency"
									id="defaultCurrency" />
							</c:if>
						</div>
						<div>
							<label class="width20"><fmt:message
									key="accounts.currencymaster.label.status" />
							</label>
							<c:if test="${CURRENCYINFO.defalutFlag eq true}">
								<input type="checkbox" name="curEnable" id="curEnable"
									checked="checked" />
							</c:if>
							<c:if test="${CURRENCYINFO.defalutFlag eq false}">
								<input type="checkbox" name="curEnable" id="curEnable" />
							</c:if>
						</div>
						<input type="hidden" name="currency" id="currency"
							value="${CURRENCYINFO.currencyId}" /> <input type="hidden"
							name="default_currency" id="default_currency"
							value="${DEFAULT_CURRENCY}" />
					</fieldset>
				</div> 
			</div>
		</div>
	</form>
</div>
