<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.quantityerr{ font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<div id="hrm" class="portlet-content width100 paymentline-info" style="height: 90%;"> 
	<div class="portlet-content"> 
	<div  id="page-error-popup" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	<div id="warning_message-popup" class="response-msg notice ui-corner-all width90" style="display:none;"></div>
	<div id="hrm" class="hastable width100"  >  
		 <table id="hastab-1" class="width100"> 
			<thead>
			   <tr>  
			   		<th>Product Code</th> 
				    <th>Product</th> 
				    <th>UOM</th>
				    <th>Invoice Qty</th>   
				    <th>Unit Rate</th> 
				    <th>Total</th>  
				    <th>Description</th> 
			  </tr>
			</thead> 
			<tbody class="tab-1">  
				<c:forEach var="bean" varStatus="status" items="${INVOICE_DETAIL_BYID}"> 
					<tr class="rowidiv" id="fieldrowiv_${status.index+1}">
						<td style="display:none;" id="lineIdiv_${status.index+1}">${status.index+1}</td> 
						<td id="productcode_${status.index+1}"> 
							${bean.product.code}
						</td>
						<td id="productname_${status.index+1}"> 
							${bean.product.productName}
						</td> 
						<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
						<td id="invoiceqty_${status.index+1}">${bean.invoiceQty}</td> 
						<td id="unitRate_${status.index+1}" style="text-align: right;">
							<c:set var="unitrate" value="${bean.unitRate}"/>
							<%=AIOSCommons.formatAmount(pageContext.getAttribute("unitrate")) %> 
						</td>  
						<td id="total_${status.index+1}" style="text-align: right;"> 
							<c:set var="total" value="${bean.unitRate * bean.invoiceQty}"/>
							<%=AIOSCommons.formatAmount(pageContext.getAttribute("total")) %>
						</td>  
						<td style="display:none"> 
							<input type="hidden" name="invoiceInvId" id="invoiceInvId_${status.index+1}" value="${requestScope.invoiceId}"/>
							<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
							<input type="hidden" name="invoiceLineId" id="invoiceLineId_${status.index+1}" value="${bean.invoiceDetailId}"/>  
						</td>  
						<td>
							 <input type="text" name="linedescription" id="linedescription_${status.index+1}" value="${bean.description}"/>
						</td> 
					</tr>
				</c:forEach>
			</tbody>
			</table>
		</div>  
	</div>   
	<div class="clearfix"></div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
		<div class="portlet-header ui-widget-header float-left sessionPaymentSave" style="cursor:pointer;"><fmt:message key="accounts.common.button.ok"/></div>
		<div class="portlet-header ui-widget-header float-left closePaymentDom" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>
	</div> 
</div> 