<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.ui-autocomplete-input {
	min-height: 26px !important;
}

.ui-autocomplete {
	height: 250px;
	overflow-y: auto;
	overflow-x: hidden;
}
</style>
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"
		class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> <label class="width30" id="segment_name"></label>
</div>
<div id="hrm">
	<div class="width98 accountsType" style="display: none; border-bottom: 1px solid #CCCCCC; margin-bottom: 15px; padding-bottom: 5px;">
		<label class="width30" style="margin-top: 7px;">Account Type</label> <select
			name="showAccountType" id="showAccountType" class="width50">
			<option value="">Select</option>
			<option value="1">Assets</option>
			<option value="2">Liabilities</option>
			<option value="3">Revenue</option>
			<option value="4">Expenses</option>
			<option value="5">Owners Equity</option>
		</select>
	</div>
	<div class="width98">
		 <select
			name="accountlist" class="autoComplete width50">
			<option value=""></option>
			<c:choose>
				<c:when
					test="${COMBINATION_ACCOUNTS_LIST ne null && COMBINATION_ACCOUNTS_LIST ne '' && fn:length(COMBINATION_ACCOUNTS_LIST)>0 }">
					<c:forEach var="accounts" items="${COMBINATION_ACCOUNTS_LIST}">
						<option value="${accounts.accountId}">${accounts.code}---${accounts.account}</option>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<c:if
						test="${requestScope.RETURN_MESSAGE eq null || requestScope.RETURN_MESSAGE eq ''}">
						<option value="error">No Account.</option>
					</c:if>
				</c:otherwise>
			</c:choose>
		</select>
	</div>
</div>
<div
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
	style="position: absolute; top: 80%; left: 80%;">
	<div class="portlet-header ui-widget-header float-right code_close"
		style="cursor: pointer;">close</div>
</div>
<script type="text/javascript"> 
 $(function(){ 
	if(segmentId==1){
		$('#segment_name').html("Company Account");
	}else if(segmentId==2){
		$('#segment_name').html("Cost Center");
	}else if(segmentId==3){
		$('#segment_name').html("Natural Account");
		$('.accountsType').show(); 
	}else if(segmentId==4){
		$('#segment_name').html("Analysis Account");
	}else if(segmentId==5){
		$('#segment_name').html("Buffer1");
	}else if(segmentId==6){
		$('#segment_name').html("Buffer2");
	}else{
		$('#segment_name').html("No Account");
	}
	 
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 $('.ui-autocomplete').css('width',"194px;");
	 $('.code_close').click(function(){
		 segmentId=0;
		 $('#common-popup').dialog("close");  
		 $('.ui-autocomplete').css('height',450);
		 $('.ui-autocomplete').css('width',"47%");
		 
	 });
	$('.autoComplete').combobox({ 
	   selected: function(event, ui){  
		   var accountId=$(this).val();
		   if(accountId>0){
			   $('#common-popup').dialog("close");  
			   saveCombination(accountId); 
		   }else{
			   $('#common-popup').dialog("close");  
		  }  
	   } 
	}); 

	$('#showAccountType').change(function(){
		if($(this).val()!=null && $(this).val()!=""){
			var accountTypeId=$(this).val();
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/filter_account_codes.action", 
			 	async: false, 
			 	data:{accountTypeId:accountTypeId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.autoComplete').html(result);
				} 
			});
		}
	});
	
	
 });
function saveCombination(accountId){  
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_combination_codes.action", 
		 	async: false, 
		 	data:{combinationId:combinationId, accountId:accountId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('#temp-result').html(result);  
				 var message=$('#temp-result').html().trim().toLowerCase(); 
				 $('#common-popup').dialog('destroy');		
				 $('#common-popup').remove(); 
				 if(message=="success"){  
					 loadTree();
					$('#page-success').hide().html("Successfully Created").slideDown(1000);
				 }
				 else{ 
					loadTree();
					$('#page-error').hide().html(message).slideDown(1000);
				 }
			},
			error:function(result){   
				$('#temp-result').html(result); 
				$('#page-error').hide().html("Internal error!!!").slideDown(1000); 
			}
	});  
}
</script>
