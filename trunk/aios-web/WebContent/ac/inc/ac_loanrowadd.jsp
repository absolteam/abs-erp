<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}"></td>
	<td>
		<input type="text" readonly="readonly"  class="width50 linedate" id="linedate_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width70 lineamount" id="amount_${rowId}"  style="border:0px;text-align; right;"/>
	</td> 
	<td>
		<input type="text" class="width98 linedecription" id="linedecription_${rowId}"  style="border:0px;"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>