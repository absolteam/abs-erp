<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>


.c_c_c_c>div {
	font-size: 14px !important;
	font-weight: bold;
}

.c_c_c_c>div>span {
	font-size: 14px !important;
}

.pos-right-content>div>label {
	font-size: 14px !important;
}

.pos-right-content>div>span {
	font-size: 14px !important;
}

#transaction-block>label {
	font-size: 14px !important;
}

#amount-block>label {
	font-size: 14px !important;
}

#coupon-block>label {
	font-size: 14px !important;
}

#exchange-block>label {
	font-size: 14px !important;
}

#tabs-1{padding:2px!important;}

.balace-due-label{
	color:#003300; 
	background-color: #eeccdd;
	width:auto; 
	padding-top:3px; 
	padding-right:3px;
	font-family: Georgia!important;
	font-weight: bold!important;
  	font-size: 14pt!important;
  	border: 1.5px solid #aaaaaa;
  	padding: 5px;
}

.inner-pos-div>div>span {
	font-size: 14px !important;
}
</style>
<script type="text/javascript">
	var slidetab = "";
	var salesOrderDetails = "";
	var chargesDetails = "";
	var tempid = "";
	var additionalCharges = []; 
	var posSessionId=null;
	
	var closeWin = 1;
	var printer = [];
	var posPrintReciptContent = "";
	 
	$(function() { 
		 
		$jquery('#inlineTargetKeypad').keypad({ keypadOnly: false,target: $jquery('.inlineTarget:first'),closeText: 'Done',
				separator: '|', prompt: '', 
		    layout: ['6|7|8|9|' + $jquery.keypad.BACK, 
		        '2|3|4|5|' + $jquery.keypad.CLEAR, 
		        '1|0|.|00|' + $jquery.keypad.CLOSE,  
		        '10|20|50|100',
		        '200|500|1000'],onClose: function(value, inst) { 
		        	$(".inlineTarget").trigger( "change" );
			    }, beforeShow: function(div, inst) { 
			    	$(div).find(".keypad-key").css( "width", "17%" );
			    	$(div).find(".keypad-special").css( "width", "23%" );
		    	}}); 
		 
		
		var keypadTarget = null; 
		$jquery('.inlineTarget').focus(function() { 
		    if (keypadTarget != this) { 
		        keypadTarget = this; 
		        $jquery('#inlineTargetKeypad').keypad('option', {target: this}); 
		    } 
		});
		
		
		$jquery('.discount,.charges').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['7|8|9|' + $jquery.keypad.BACK, 
	        '4|5|6|' + $jquery.keypad.CLEAR, 
	        '1|2|3|' + $jquery.keypad.CLOSE, 
	        '.|0|00'],onClose: function(value, inst) { 
	        	$(this).trigger( "change" );
		    }, beforeShow: function(div, inst) { 
		        
	    	}}
		
		); 
		
		
		//$jquery('#receiveAmount').keypad({showOn:'focus',isRTL: true});
		
 		 $jquery("#pointOfSaleValidation").validationEngine('attach'); 
		
		 manupulateLastRow();  
		 
		 $('.delrowcharges').live('click',function(){ 
			 slidetab=$(this).parent().parent().get(0);  
	       	 $(slidetab).remove();  
	       	 var i=1;
	    	 $('.rowid').each(function(){   
	    		 var rowId=getRowId($(this).attr('id')); 
	    		 $('#chargesLineId_'+rowId).html(i);
				 i=i+1; 
	 		 });   
 		 });  

		$('.pos-invoice-reset').click(function(){  
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/generate_pointofsale_invoice.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-common-popup').dialog('destroy');		
					$('#pos-common-popup').remove(); 
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$('#qz').remove();
					$("#main-wrapper").html(result); 
				} 
			});  
		});

		$('.pos-invoice-discard').click(function(){   
			var pointOfSaleId = Number($('#pointOfSaleTempId').val());
			var deleteFlag = false;
			if(pointOfSaleId > 0)
				deleteFlag = true; 
 			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/delete_saved_pos_order.action",
				data:{posSessionId: posSessionId, pointOfSaleId: pointOfSaleId, deleteFlag: deleteFlag},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					posSessionId=null;
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-invoice-popup').dialog('destroy');		
					$('#pos-invoice-popup').remove(); 
					$('#pos-common-popup').dialog('destroy');		
					$('#pos-common-popup').remove();   
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$('#qz').remove();
					$("#main-wrapper").html(result);
				}
			}); 
		}); 
		
		$('.pos-back').click(function(){  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_saved_pos_order.action",
				data:{posSessionId: posSessionId},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup-new').dialog('destroy');		
					$('#common-popup-new').remove(); 
					$('#pos-common-popup').dialog('destroy');		
					$('#pos-common-popup').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);
				}
			});
		}); 
		
		$('.pos-receipt-copy').click(function(){  
			printHTML();
			return false;
		});
		
		$('.secret-pin-validate').click(function(){
			var allowSave1 = validateProductCost();
			if(allowSave1 == "true"){ 
 				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/processprint_pointofsaletemp.action", 
					async: false,
					dataType: "json",
					data: {posSessionId: posSessionId},
					cache: false,
					success:function(response){   
						if (response.returnMessage == "SUCCESS"){  
							$('.ui-dialog-titlebar').remove();  
							$.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_pointofsale_secretpin.action", 
								async: false,
								dataType: "html",
								cache: false,
								success:function(result){  
									 $('#temp-result').html(result); 
									 $('.callJq').trigger('click');  
									 $('#employeeId').focus();
								}
							}); 
						}else{
							$('#page-error').hide().html(response.returnMessage).slideDown(1000);
							$('#page-error').delay(2000).slideUp(); 
							return false;
						}
					}
				});	 
			}else{
				$('#page-error').hide().html("Product Selling below cost price.").slideDown(1000);
				$('#page-error').delay(2000).slideUp(); 
				return false;
			} 
		});
		
		$('.pos-receipt').click(function(){
			$('.pos-receipt-print').hide();
			var allowSave = validateProductCost();
			if(allowSave == "true"){  
				additionalCharges = getChargesType(); 
				var personId = Number($('#personId').val());
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/processprint_pointofsale.action", 
					async: false,
					dataType: "json",
					data: {posSessionId: posSessionId, personId: personId, additionalCharges : JSON.stringify(additionalCharges)},
					cache: false,
					success:function(response){   
						if (response.returnMessage == "SUCCESS"){  
							$('.pos-invoice-reset').hide();
							$('.pos-receipt-print').hide();
							$('.pos-receipt-save').hide();
							$('.pos-receipt-dispatch').hide();
							$('.pos-receipt').hide();
							$('.pos-back').hide(); 
							var printed = printHTML();
							if(printed == "true"){
								$.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/save_pointofsale.action", 
									async: false,
									dataType: "json",
									data: {posSessionId: posSessionId, additionalCharges : JSON.stringify(additionalCharges)},
									cache: false,
									success:function(response){   
										if (response.returnMessage == "SUCCESS"){ 
											$('#pointOfSaleTempId').val(Number(0)); 
											$('.pos-invoice-discard').text('close');
											$('#common-popup-new').dialog('destroy');		
											$('#common-popup-new').remove(); 
											$('.pos-receipt-copy').show();
											$('.pos-receipt').remove();
											$('#pos-invoice-popup').dialog('destroy');		
											$('#pos-invoice-popup').remove();   
											$('#pos-common-popup').dialog('destroy');		
											$('#pos-common-popup').remove();   
											$('#DOMWindow').remove();
											$('#DOMWindowOverlay').remove();
											return false;
										} else{
											$('.pos-back').show(); 
											$('.pos-receipt').show();
											$('.pos-invoice-reset').show();
											$('.pos-invoice-discard').show();
											$('.pos-receipt-print').show();
											$('.pos-receipt-save').show();
										}
									} 
								});
							}  else{
								$('.pos-receipt-copy').remove();
								$('.pos-receipt').remove();
								$('#pos-invoice-popup').dialog('destroy');		
								$('#pos-invoice-popup').remove(); 
								$('#DOMWindow').remove();
								$('#DOMWindowOverlay').remove();
								alert("Printer connection lost, Please refresh the browser & continue (or) allow browser security warning.");
							}
							return false;
						} else{
							$('#page-error').hide().html(response.returnMessage).slideDown(1000);
							$('#page-error').delay(2000).slideUp(); 
							return false;
						}
					} 
				}); 
			}else{
				$('#page-error').hide().html("Product Selling below cost price.").slideDown(1000);
				$('#page-error').delay(2000).slideUp();
				$('.pos-receipt-print').show();
				return false;
			} 
 			return false;
		});
		
		var validateProductCost = function(){ 
			var allowSave = "true";
			$('.rowid').each(function(){  
	 			var rowId = getRowId($(this).attr('id'));  
	 			allowSave = $.trim($('#allowSave_'+rowId).val());
	 			if(allowSave == "false"){
	 				allowSave = "false";
	 				return allowSave;
	 			}
			}); 
			return allowSave;
		};
		 
		
		$('#pos-invoice-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		
		$('#pos-customerorder-discard').live('click',function(){ 
			$('#pos-invoice-popup').dialog('close'); 	 
		});
		
		$('#pos-customerorder-dispatch').live('click',function(){  
			additionalCharges = getChargesType(); 
			var freeDelivery = $('#freeDelivery').attr('checked');
			var deliveryCharges = $('#deliveryCharges').val();
			var driverMobile = $('#driverMobile').val();
			var driverId =  $('#driverId').val().split('|')[0]; 
			var customerMobile = $.trim($('#customerMobile').text());
			var customerLocation = $.trim($('#customerLocation').text());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/dispatch_pointofsale.action", 
				async: false,
				   dataType: "json",
				   data: {	posSessionId: posSessionId, additionalCharges : JSON.stringify(additionalCharges),  deliveryCharges: deliveryCharges,
					   		freeDelivery: freeDelivery, driverMobile: driverMobile, driverId: driverId, customerMobile: customerMobile,
					   		customerLocation: customerLocation},
				   cache: false,
				success:function(response){   
					if (response.returnMessage == "SUCCESS"){ 
						$('.pos-invoice-discard').text('close');
						$('.pos-invoice-reset').remove();
						$('.pos-receipt-dispatch').remove();
						$('.pos-receipt-print').remove();
						$('.pos-receipt-copy').show();
						$('.pos-receipt-save').remove();
						$('.pos-back').remove();
						$('#common-popup-new').dialog('destroy');		
						$('#common-popup-new').remove(); 
						$('#pos-invoice-popup').dialog('destroy');		
						$('#pos-invoice-popup').remove();  
						$('#pos-common-popup').dialog('destroy');		
						$('#pos-common-popup').remove();   
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						printHTML();
					} 
				} 
			}); 
 			return false;
		});
		
		$('#pos-receipt-dispatch').click(function(){ 
			$('.ui-dialog-titlebar').remove();  
			tempid = $(this).parent().get(0);
			var customerId = Number($('#customerId').val()); 
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_posdirver_customerdetails.action", 
			 	async: false,  
			 	data: {customerId: customerId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
	 				 $('.pos-invoice-result').html(result);  
					 $('#pos-invoice-popup').dialog('open'); 
					 $($($('#pos-invoice-popup').parent()).get(0)).css('top',0,'important');
				} 
			});
 	  		return false;
		});
		
		$('.pos-receipt-save').click(function(){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_pointofsale_temporary.action", 
				async: false,
				   dataType: "json",
				   data: {posSessionId: posSessionId},
				   cache: false,
				   success:function(response){   
					   if(response.returnMessage == "SUCCESS"){
						   $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_pointofsales.action", 
								async : false,
								dataType : "html",
								cache : false,
								success : function(result) {
									posSessionId=null;
									$('#common-popup-new').dialog('destroy');		
									$('#common-popup-new').remove(); 
									$('#pos-invoice-popup').dialog('destroy');		
									$('#pos-invoice-popup').remove(); 
									$('#pos-common-popup').dialog('destroy');		
									$('#pos-common-popup').remove();  
									$('#DOMWindow').remove();
									$('#DOMWindowOverlay').remove();
									$("#main-wrapper").html(result);
								}
							});
					   } 
				   } 
			});  
 			return false;
		});
		
		$('.pos-receipt-print').click(function(){  
			additionalCharges : JSON.stringify(additionalCharges);
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pointofsale_restarunt_receiptprint.action", 
				async: false,
				dataType: "json",
				data: {posSessionId: posSessionId, additionalCharges : JSON.stringify(additionalCharges)},
				cache: false,
				success:function(response){   
					if (response.returnMessage == "SUCCESS"){  
						printHTML();
					} 
				} 
			});  
 			return false;
		});
		
		$('.discountmode').click(function(){ 
			var rowId = getRowId($(this).attr('id')); 
			processDiscount(rowId);
		});
		
		$('.discount').change(function(){ 
			var rowId = getRowId($(this).attr('id')); 
			processDiscount(rowId);
		});

		$('.paymentMode').click(function(){   
			$('#coupon-block').hide();
			$('#exchange-block').hide();
			$('#transaction-block').hide();
			var paymentMode = getRowId($(this).attr('id'));
			$('.paymentMode').each(function(){ 
 				if($($($(this).parent()).get(0)).hasClass('selected')){
					$($($(this).parent()).get(0)).removeClass('selected');
				}
			});
			$($($(this).parent()).get(0)).addClass('selected');
			$('#receiveAmount').focus();
			if(paymentMode == 4){
				$('#amount-block').hide();
				$('#coupon-block').show();
				$('#transaction-block').hide();
			}else if(paymentMode == 5){
				$('#exchange-block').show();
				$('#amount-block').hide();
				$('#transaction-block').hide();
			}else if(paymentMode == 2 || paymentMode == 3 ){ 
				$('#amount-block').show();
				$('#transaction-block').show();
			}else{ 
				var receiveCashAmount = convertStringToDouble($.trim($('#outStanding').text())); 
				if(receiveCashAmount != 0 && receiveCashAmount != ''){
					var receiveAmount = Number($.trim($('#receiveAmount').val()));
					if(receiveAmount == 0)
						$('#receiveAmount').val(receiveCashAmount);
					$('#amount-block').show();
				} 
			} 
  			showPaymentSummary(getRowId($(this).attr('id'))); 
		});

		$('#receiveAmount,#couponNumber,#exchangeReference').change(function(){   
			var paymentMode = Number(0);
			$('.paymentMode').each(function(){ 
				if($($($(this).parent()).get(0)).hasClass('selected')){
					paymentMode = getRowId($(this).attr('id'));
				}
			}); 
			showPaymentSummary(paymentMode); 
		}); 
		
		$('.chargestypeid,.charges').live('change',function(){ 
	 		triggerChargesAddRow(getRowId($(this).attr('id')));
	 		return false;
	 	 }); 

		$('.addrowscharges').click(function(){ 
			  var i=Number(1); 
			  var id=Number(getRowId($(".tabcharges>.rowidcharges:last").attr('id'))); 
			  id=id+1;  
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/pos_charges_addrow.action", 
				 	async: false,
				 	data:{rowId: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tabcharges tr:last').before(result);
						 if($(".tabcharges").height()>255)
							 $(".tabcharges").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowidcharges').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#chargesLineId_'+rowId).html(i);
							 i=i+1; 
				 		 }); 
					}
				});
			  return false;
		 });
		
		
		$('#discount-tab,#charges-tab').click(function(){  
			//checkModificationDeleteApprover();
	 	 }); 
		
		$('#common-popup-new').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		}); 
		
		$('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		}); 
		
		posSessionId='${requestScope.posSessionId}';  
		
		var printerFlag = $('#printerFlag').val();
		if(printerFlag == "true"){
			customPrintOrder();	
		}
	});

	function getRowId(id){   
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	 
	
	function processDiscount(rowId){
		$('#discount-error').hide();
		var productId = Number($('#productid_'+rowId).val());
		var discountMode= Number($('input[name=discountmode_'+rowId+']:checked').val());
		var discount = Number($('#discount_'+rowId).val());
		var itemOrder = Number($('#itemOrder_'+rowId).val());
		var comboType = $('#comboType_'+rowId).val();
		var standardPrice =  Number($('#standardPrice_'+rowId).val()); 
		var basePrice =  Number($('#basePrice_'+rowId).val());
		var productQty = Number($('#productQty_'+rowId).text()); 
		$('#allowSave_'+rowId).val('true');
		if(productId > 0 && discountMode>-1){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/pos_calc_inline_discount.action",
					async : false,
					data : {
						productId : productId,
						discountMode : discountMode,
						discount : discount, itemOrder: itemOrder, comboType: comboType
					},
					dataType : "json",
					cache : false,
					success : function(response) {
						$('#totalAmount_' + rowId).text(
								response.productTotalPrice);
						var totalPriceValue = convertStringToDouble(response.productTotalPrice);  
						var avgeragePrice = Number(totalPriceValue / productQty);
						 if(avgeragePrice < basePrice){ 
				 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
								$('#discount-error').delay(2000).slideUp();
				 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
				 				$('#allowSave_'+rowId).val('false');
					 		}
						 else if(avgeragePrice < standardPrice){ 
							$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
							$('#discount-error').delay(2000).slideUp();
							$('#fieldrow_'+rowId).css('background', '#F7BE81');
						}
 						else
							$('#fieldrow_'+rowId).css('background', '#fff');
						showPaymentSummary(Number(100));//100 represent dummy paymentmode just for reload the payment summery

					}
				});
		} 
		return false;
	}

	function manupulateLastRow() {
		var hiddenSize = 1;
  		var tdSize = 0;
		var actualSize = 0; 
		
		tdSize = $($(".tabcharges>tr:first").children()).size();
 		actualSize = Number(tdSize - hiddenSize);
		
		$('.tabcharges>tr:last').removeAttr('id');
		$('.tabcharges>tr:last').removeClass('rowidcharges')
				.addClass('lastrow');
		$($('.tabcharges>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tabcharges>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}

	function triggerChargesAddRow(rowId) {
		var chargesTypeId = $('#chargesTypeId_' + rowId).val();
		var charges = $('#charges_' + rowId).val();
		var nexttab = $('#fieldrowcharges_' + rowId).next();
		if (chargesTypeId != null && chargesTypeId != "" && charges != null
				&& charges != "" && $(nexttab).hasClass('lastrow')) {
			$('#DeleteImagecharges_' + rowId).show();
			$('.addrowscharges').trigger('click');
		}
		showPaymentSummary(Number(100));//100 represent dummy paymentmode just for to reload the payment summery
	}

	function showPaymentSummary(paymentMode) {  
		
		
 		var receiveAmount = Number($('#receiveAmount').val());  
 		var couponNumber =  Number($('#couponNumber').val());  
 		var exchangeReference = $.trim($('#exchangeReference').val());
 		var customerId = Number($('#customerId').val());   
 		var transactionNumber = $('#transactionNumber').val();
 		var dispatchDiscount =  Number(0);  
 		var dispatchCost =  Number(0);
 		
 		if(paymentMode==100)
			receiveAmount=Number(1);
 		
		if(paymentMode > 0 && (receiveAmount > 0 || couponNumber > 0 || exchangeReference != '')){
			if(paymentMode==100){
				receiveAmount=0.0;
				paymentMode=1;
			}
			additionalCharges = getChargesType(); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_pos_payment_summary.action",
						async : false,
						data : {
							additionalCharges : JSON
									.stringify(additionalCharges),
							paymentMode : paymentMode,
							receiveAmount : receiveAmount,
							couponNumber : couponNumber,
							exchangeReference : exchangeReference,
							customerId : customerId,
							transactionNumber : transactionNumber,
							dispatchDiscount: dispatchDiscount,
							dispatchCost: dispatchCost
						},
						dataType : "json",
						cache : false,
						success : function(response) {
							$('.c_c_c_c').show();
							if (response.pointOfSaleVO != null) {
								$('#salesAmount').text(
										response.pointOfSaleVO.salesAmount);
								$('#additionalCost').text(
										response.pointOfSaleVO.additionalCost);
								$('#totalDue').text(
										response.pointOfSaleVO.totalDue);
								if (response.pointOfSaleVO.customerDiscount != null
										&& response.pointOfSaleVO.customerDiscount != '')
									$('#customerDiscount')
											.text(
													response.pointOfSaleVO.customerDiscount);
								if (response.pointOfSaleVO.cashReceived != null
										&& response.pointOfSaleVO.cashReceived != '') {
									$('#cashReceived')
											.text(
													response.pointOfSaleVO.cashReceived);
									$('#ch').show();
								}
								if (response.pointOfSaleVO.cardReceived != null
										&& response.pointOfSaleVO.cardReceived != '') {
									$('#cardReceived')
											.text(
													response.pointOfSaleVO.cardReceived);
									$('#cd').show();
								}
								if (response.pointOfSaleVO.chequeReceived != null
										&& response.pointOfSaleVO.chequeReceived != '') {
									$('#chequeReceived')
											.text(
													response.pointOfSaleVO.chequeReceived);
									$('#cq').show();
								}
								if (response.pointOfSaleVO.couponReceived != null
										&& response.pointOfSaleVO.couponReceived != '') {
									$('#couponReceived')
											.text(
													response.pointOfSaleVO.couponReceived);
									$('#cp').show();
								}
								if (response.pointOfSaleVO.exchangeReceived != null
										&& response.pointOfSaleVO.exchangeReceived != '') {
									$('#exchangeReceived')
											.text(
													response.pointOfSaleVO.exchangeReceived);
									$('#er').show();
								}
								$('#receivedTotal').text(
										response.pointOfSaleVO.receivedTotal);
								if (response.pointOfSaleVO.outStanding != null
										&& response.pointOfSaleVO.outStanding != '') {
									$('#outStanding').text(
											response.pointOfSaleVO.outStanding);
									$('.outstanding').show();
								} else{ 
									$('.outstanding').show();
									$('#outStanding').text('0.00');
								} 

								if (response.pointOfSaleVO.balanceDue != null
										&& response.pointOfSaleVO.balanceDue != '') {
									$('#balanceDue').text(
											response.pointOfSaleVO.balanceDue);
									$('.balancedue').show();
									$('.outstanding').hide();
								} else
									$('.balancedue').hide();
							} else {
								$('#receipt-error').hide().html(
										response.returnMessage).slideDown();
								$('#receipt-error').delay(2000).slideUp();
							}
						}
					});
			$(
					'#receiveAmount,#couponNumber,#exchangeReference,#transactionNumber')
					.val('');
		}
		return false;
	}
	function getChargesType() {
		additionalCharges = [];
		$('.rowidcharges').each(function() {
			var rowid = getRowId($(this).attr('id'));
			var chargesType = Number($('#chargesTypeId_' + rowid).val());
			var charges = Number($('#charges_' + rowid).val());
			var description = $('#description_' + rowid).val();
			if (chargesType > 0 && charges > 0) {
				additionalCharges.push({
					"chargesType" : chargesType,
					"charges" : charges,
					"description": description
				});
			}
		});
		return additionalCharges; 
	} 
	
	function customPrintOrder(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/sendorder_toprinters.action",
			async : false, 
			dataType : "json",
			cache : false,
			success : function(response) { 
				if(null!=response.printers && response.printers !=""){
					printer = response.printers.split(',');  
					if(printer.length>1){
					 	setTimeout(function() {
							customPrintHTML(printer[0], response.kitchenOrders);
						}, 500);
						 setTimeout(function() {
							customPrintHTML(printer[1], response.barOrders);
						}, 2000);
					}else{
						if(null!=response.kitchenOrders && $.trim(response.kitchenOrders) !=''){ 
							setTimeout(function() {
								customPrintHTML(printer[0], response.kitchenOrders);
							}, 500);
						}else{ 
							setTimeout(function() {
								customPrintHTML(printer[0], response.barOrders);
							}, 500);
						} 
					} 
				} 
			}
		}); 
	}
	
	function checkModificationDeleteApprover(){ 
		var ordPrinted =false;
		var pointOfSaleId = Number($('#pointOfSaleTempId').val());
		
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/check_pos_printorder.action",
			data:{pointOfSaleId: pointOfSaleId,posSessionId:posSessionId},
			async : false,
			dataType : "json",
			cache : false,
			success : function(response) { 
				ordPrinted=response.printerFlag;
				if(ordPrinted){
					 $('.ui-dialog-titlebar').remove(); 
			         $('#common-popup-new').dialog('open');
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/validate_approver.action",
						async : false,
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.common-result-new').html(result);
						}
					});
					ordPrinted = false;
				}else{ 
					ordPrinted = true;
				}
			}
		});
		return ordPrinted;
	}
</script>
<div id="main-content" style="overflow: hidden;">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"
			style="padding: 8px 4px 4px 8px !important; width: auto;">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Point Of Sale :: <i>${requestScope.SALES_SOTRE_NAME}</i> 
			<c:if test="${sessionScope.SHIFT_SESSION ne null && fn:length(sessionScope.SHIFT_SESSION) > 0}">
				<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> | Work Schedule :: 
				<i>
					<c:forEach var="shift" items="${sessionScope.SHIFT_SESSION}">
						${shift.employeeName} (${shift.startTime} - ${shift.endTime})<span>,</span>
					</c:forEach>
				</i>
			</c:if> 
		</div> 
		<form id="pointOfSaleValidation"
			style="position: relative;"> 
			<div id="tabs"
				class="ui-tabs ui-widget ui-widget-content ui-corner-all width99"
				style="float: left; *float: none;">
				<input type="hidden" id="printerFlag" 
					value="${requestScope.printerFlag}"/>
				<input type="hidden" id="personId"/>
				<input type="hidden" id="customerId"
					value="${requestScope.customerId}" /> <input type="hidden"
					id="shippingId" value="${requestScope.shippingId}" /> <input
					type="hidden" id="deliveryOption"
					value="${requestScope.deliveryOption}" />
				<input type="hidden" id="pointOfSaleTempId"
					value="${requestScope.pointOfSaleTempId}" /> 
				<input type="hidden" id="nonStockProductIds" value=""/>
				<div id="hrm" class="float-left width98">
					<fieldset style="margin: 0px;" class="width99">
						<ul
							class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99"
							style="margin: 0px;">
							<li
								class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a
								href="#tabs-1">Receipt Details </a></li>
							<li class="ui-state-default ui-corner-top ui-tabs-selected"><a
								href="#tabs-2" id="discount-tab" >Product Discount </a></li>
							<li class="ui-state-default ui-corner-top ui-tabs-selected"><a
								href="#tabs-3" id="charges-tab">Additional Charges </a></li>
						</ul>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-1">
							<div class="portlet-content">
								<div id="page-error"
									class="response-msg error ui-corner-all"
									style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
								<div class="tempresult" style="display: none;"></div>
								<div class="width100 float-left" id="hrm">
									<div class="float-left  width48">
										<fieldset style="min-height: 85px;">
											<table class="pos-item-table width100">
												<tr>
													<c:forEach var="paymentType" items="${PAYMENT_TYPE}"
														varStatus="status">
														<c:if test="${status.index % 2 eq 0}">
															<td>
																<div class="box-model"
																	style="background-color: #A9D0F5;">
																	<span style="font-size: 16px !important;"
																		class="paymentMode ${paymentType.key == 1 ? ' coins' : paymentType.key == 3 ? 'card_visa' : 'exchange'}"
																		id="paymentMode_${paymentType.key}">${paymentType.value}
																	</span>
																</div>
															</td>
														</c:if>
													</c:forEach>
												</tr>
												<tr>
													<c:forEach var="paymentType" items="${PAYMENT_TYPE}"
														varStatus="status">
														<c:if test="${status.index % 2 ne 0}">
															<td>
																<div class="box-model"
																	style="background-color: #A9D0F5;">
																	<span style="font-size: 16px !important;"
																		class="paymentMode ${paymentType.key == 2 ? 'bank_pencil' : paymentType.key == 4 ? 'coupon' : 'voucher'}"
																		id="paymentMode_${paymentType.key}">${paymentType.value}</span>
																</div>
															</td>
														</c:if>
													</c:forEach>
												</tr>
											</table>
										</fieldset>
									</div>
									<div class="float-right width48 pos-right-content">
										<div>
											<label class="width30"> Reference No</label> <span style="position: relative; top: 8px;"
												id="referenceNumber">${requestScope.salesRefNumber} </span>
										</div>
										<div>
											<label class="width30"> Sales Date </label> <span style="position: relative; top: 8px;"
												id="salesDate"> ${requestScope.salesDate} </span>
										</div> 
										<c:if test="${requestScope.customerId gt 0}">
											<div>
												<label class="width30"> Customer </label> <span style="position: relative; top: 8px;">
													${requestScope.customerName}</span>
											</div>
											<c:if test="${requestScope.shippingId ne null && requestScope.shippingId ne '' && 
												requestScope.shippingId gt 0 && requestScope.shippingId > 0}">
												<div>
													<label class="width30"> Shipping Address </label> <span style="position: relative; top: 8px;">
														${requestScope.shippingAddress} </span>
												</div>
											</c:if>
											<c:if test="${requestScope.customerCoupon ne null && requestScope.customerCoupon ne '' && 
												requestScope.customerCoupon gt 0}">
												<div>
													<label class="width30"> Coupon </label> <span style="position: relative; top: 8px;"
														id="issueCoupon">${requestScope.customerCoupon}</span>
												</div>
											</c:if>
										</c:if>
									</div>
								</div>
								<div class="clearfix"></div>
								<fieldset style="max-height: 390px; height: 390px; margin:3px;overflow-y: auto;">
									<div class="pos-summary width100">
										<div class="width40 float-right">
											<div class="width80 float-left inner-pos-div">
												<div class="width50 float-left">
													<span>Sales Amount</span>
												</div>
												<div class="width30 float-right">
													<span id="salesAmount" class="right-align float-right">${requestScope.productTotalPrice}</span>
												</div>
												<div class="width50 float-left">
													<span>Additional Cost</span>
												</div>
												<div class="width30 float-right">
													<span id="additionalCost" class="right-align float-right"></span>
												</div>
												<c:if
													test="${requestScope.customerDiscount ne null && requestScope.customerDiscount ne ''}">
													<div class="width50 float-left">
														<span>Discount</span>
													</div>
													<div class="width30 float-right">
														<span id="customerDiscount"
															class="right-align float-right">${requestScope.customerDiscount}</span>
													</div>
												</c:if>
												<div class="width50 float-left">
													<span>Total Due</span>
												</div>
												<div class="width30 float-right">
													<span id="totalDue" class="right-align float-right">${requestScope.totalDues}</span>
												</div>
												<div class="width80 float-left">
													<span>Received</span>
													<div style="margin: 3px; padding: 0px 15px;"
														class="c_c_c_c">
														<div id="ch">
															Cash : <span id="cashReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cd">
															Card : <span id="cardReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cp">
															Coupon : <span id="couponReceived"
																class="right-align float-right"></span>
														</div>
														<div id="cq">
															Cheque : <span id="chequeReceived"
																class="right-align float-right"></span>
														</div>
														<div id="er">
															Exchange : <span id="exchangeReceived"
																class="right-align float-right"></span>
														</div>
													</div>
												</div>
												<div class="width30 float-right">
													<span id="receivedTotal" class="right-align float-right"></span>
												</div>
												<div class="clearfix"></div>
												<div class="pos-seperator float-right width30 outstanding"></div>
												<div class="clearfix"></div>
												<div class="width50 float-left outstanding">
													<span>Outstanding</span>
												</div>
												<div class="width30 float-right outstanding">
													<span id="outStanding" class="right-align float-right">${requestScope.totalDues}</span>
												</div>
												<div class="clearfix"></div>
												<div class="pos-seperator float-right width30"></div>
												<div class="clearfix"></div>
												<div class="width50 float-left balancedue"
													style="display: none;">
													<span class="balace-due-label">Change Due</span>
												</div>
												<div class="width30 float-right balancedue" style="display: none;">
													<span id="balanceDue" class="right-align float-right balace-due-label"></span>
												</div>
											</div>
										</div>
										<div class="width58 float-left">
											<div class="portlet-content">
												<div id="receipt-error"
													class="response-msg error ui-corner-all"
													style="display: none;"></div>
												<div id="transaction-block" style="display: none;">
													<label>Transaction Number</label> <input type="text"
														class="width40 validate[optional] inlineTarget"
														id="transactionNumber" />
												</div>
												<div id="amount-block">
													<label>Enter Amount</label> <input type="text"
														class="width40 validate[optional,custom[number]] right-align inlineTarget"
														id="receiveAmount" />
												</div>
												<div id="coupon-block" style="display: none;">
													<label>Coupon Number</label> <input type="text"
														class="width40 validate[optional,custom[onlyNumber]] inlineTarget"
														id="couponNumber" />
												</div>
												<div id="exchange-block" style="display: none;">
													<label>Exchange Number</label> <input type="text"
														class="width40 validate[optional] inlineTarget"
														id="exchangeReference" />
												</div>
											</div>
											<span id="inlineTargetKeypad" style="width: 100% !important;"></span>
										</div>
									</div>
									<div class="clearfix"></div>
									<div
										class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left "
										style="margin: 10px;">
										<c:if test="${sessionScope.pos_saveorder_button eq 'show'}">
											<div
												class="portlet-header ui-widget-header float-right pos-receipt-save"
												style="cursor: pointer; background: #0B610B; color: #fff; padding: 10px;">
												Save Order</div> 
										</c:if>
										<c:if test="${sessionScope.pos_printorder_button eq 'show'}">
											<div
												class="portlet-header ui-widget-header float-right pos-receipt-print"
												style="cursor: pointer; background: #01DF01; color: #fff; padding: 10px;">
												${sessionScope.pos_printorder}</div> 
										</c:if>
									</div>
									<div
										class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
										style="margin: 10px;"> 
										<div
											class="portlet-header ui-widget-header float-right pos-invoice-discard"
											style="cursor: pointer; background: #DF0101; color: #fff; padding: 10px;">
											Cancel
										</div> 
										<div
											class="portlet-header ui-widget-header float-right pos-invoice-reset"
											style="cursor: pointer; background: #610B21; color: #fff; padding: 10px;">
											Reset
										</div>
										<c:choose>
											<c:when test="${requestScope.salesType ne 3}">
												<c:set var="display" value="block"/>
												<c:if test="${requestScope.showPinNumber eq true}">
													<c:set var="display" value="none"/>
													<div class="portlet-header ui-widget-header float-right secret-pin-validate"
															id="secret-pin-validate" style="cursor: pointer; background: #0B3B0B; color: #fff; padding: 10px;">Print
															Receipt
													</div> 
												</c:if>
 												<div class="portlet-header ui-widget-header float-right pos-receipt"
													id="pos-receipt" 
														style="display: ${display}; cursor: pointer; background: #0B3B0B; color: #fff; padding: 10px;">Print
														Receipt
												</div>  
											</c:when>
											<c:otherwise>
												<div
													class="portlet-header ui-widget-header float-right pos-receipt-dispatch" id="pos-receipt-dispatch" 
													style="cursor: pointer; background: #0B3B0B; color: #fff; padding: 10px;">Dispatch Order
												</div> 
											</c:otherwise>
										</c:choose> 
										<div
											class="portlet-header ui-widget-header float-right pos-receipt-copy"
											id="pos-receipt-copy"
											style="cursor: pointer; display: none; background: #0B3B0B; color: #fff; padding: 10px;">Print
											Receipt</div> 
										<div
											class="portlet-header ui-widget-header float-right pos-back"
											style="cursor: pointer; background: #AEB404; color: #fff; padding: 10px;">
											Back</div>

									</div>
								</fieldset>
							</div>
						</div>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-2">
							<div class="portlet-content width100" id="hrm" style="margin-top: 10px; max-height:400px; overflow-y: auto;">
								<div id="hrm" class="hastable width100">
									<div id="discount-error"
										class="response-msg error ui-corner-all width80"
										style="display: none;"></div>
									<table id="hastab" class="width100">
										<thead>
											<tr>
												<th style="width: 8%;">Product</th>
												<th style="width: 3%;">Quantity</th>
												<th style="width: 3%;">Price</th>
												<th style="width: 2%;">Points</th>
												<th style="width: 3%;">Mode</th>
												<th style="width: 3%;">Discount</th>
												<th style="width: 3%;">Total</th> 
											</tr>
										</thead>
										<tbody class="tab">
											<c:forEach var="salesInvoice" items="${SALES_INVOICE}"
												varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
													<td><input type="hidden" name="productId"
														id="productid_${status.index+1}"
														value="${salesInvoice.productId}" /> <span
														class="width98" id="product_${status.index+1}">${salesInvoice.productName}</span>
													</td>
													<td id="productQty_${status.index+1}" class="productQty">
														${salesInvoice.quantity}</td>
													<td id="amount_${status.index+1}"
														class="amount right-align">${salesInvoice.displayUnitPrice}</td>
													<td id="productPoint_${status.index+1}"
														class="productPoint"><c:if
															test="${salesInvoice.totalPoints gt 0}">
												${salesInvoice.totalPoints}</c:if>
													</td>
													<td><%-- <select name="discountmode"
														id="discountmode_${status.index+1}"
														class="discountmode width98">
															<option value="-1">Select</option>
															<option value="1">Percentage</option>
															<option value="0">Amount</option>
													</select> --%>
													 <input type="radio" name="discountmode_${status.index+1}" checked="checked" id="percentagemode_${status.index+1}"
													 	class="discountmode width10" value="1">Percentage
													 <input type="radio" name="discountmode_${status.index+1}" id="amountmode_${status.index+1}"
													 	class="discountmode width10" value="0">Amount
													</td>
													<td><input type="text" name="discount"
														id="discount_${status.index+1}" class="discount width98" />
													</td>
													
													<td class="right-align" id="totalAmount_${status.index+1}">${salesInvoice.displayPrice}</td>
													<td style="display: none;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="cursor: pointer; display: none;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a> 
														<input type="hidden" value="${salesInvoice.itemOrder}" id="itemOrder_${status.index+1}">
														<input type="hidden" value="${salesInvoice.comboItemOrder}" id="comboItemOrder_${status.index+1}">
														<input type="hidden" value="${salesInvoice.specialPos}" id="comboType_${status.index+1}"> 
														<input type="hidden" value="${salesInvoice.standardPrice}" id="standardPrice_${status.index+1}"> 
														<input type="hidden" value="${salesInvoice.basePrice}" id="basePrice_${status.index+1}"> 
														<input type="hidden" value="true" id="allowSave_${status.index+1}"> 
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
							id="tabs-3">
							<div class="hastable width100 float-left"
								style="margin-top: 10px;">
								<table id="hastab_charges" class="width100">
									<thead>
										<tr>
											<th class="width10">Charges Type</th>
											<th style="width: 5%;">Charges</th>
											<th style="width: 5%;">Description</th>
											<th style="width: 1%;" class="options"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tabcharges">
										<c:forEach begin="0" end="1" step="1" varStatus="status1">
											<tr class="rowidcharges"
												id="fieldrowcharges_${status1.index+1}">
												<td id="chargesLineId_${status1.index+1}"
													style="display: none;">${status1.index+1}</td>
												<td><select name="chargesTypeId"
													id="chargesTypeId_${status1.index+1}"
													class="chargestypeid width90">
														<option value="">Select</option>
														<c:forEach var="TYPE" items="${CHARGES_TYPE}">
															<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
														</c:forEach>
												</select>
												</td>
												<td><input type="text" name="charges"
													id="charges_${status1.index+1}" class="charges width98"
													style="text-align: right;"></td>
												<td><input type="text" name="description"
													id="description_${status1.index+1}" class="description width98"></td>
												<td style="width: 1%;" class="opn_td"
													id="optioncharges_${status1.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
													id="AddImagecharges_${status1.index+1}"
													style="cursor: pointer; display: none;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
													id="EditImagecharges_${status1.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
													id="DeleteImagecharges_${status1.index+1}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImagecharges_${status1.index+1}"
													style="display: none;" title="Working"> <span
														class="processing"></span> </a> <input type="hidden"
													name="posChargeId" id="posChargeId_${status1.index+1}" />
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
				</div>
			</div> 
			<div class="clearfix"></div>   
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div
					class="portlet-header ui-widget-header float-left addrowscharges"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div> 
			</div>
			<div id="temp-result" style="display: none;"></div>
			<span class="callJq"></span>
			<div class="clearfix"></div>   
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="pos-invoice-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="pos-invoice-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>  
			<div align="center" id="content">
				<h1 id="title" style="display: none;"></h1> 
				<applet id="qz" code="qz.PrintApplet.class"
					archive="${pageContext.request.contextPath}/printing/applet/qz-print.jar" 
					width="0" height="0" align="left"> 
					<param name="jnlp_href" value="${pageContext.request.contextPath}/printing/applet/qz-print_jnlp.jnlp">
					<param name="cache_option" value="plugin">
				</applet>
			</div>
		</form>
	</div> 
</div> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jZebra.js"></script>