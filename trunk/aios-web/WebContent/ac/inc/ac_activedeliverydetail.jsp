<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
.quantityerr{ font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<script type="text/javascript">
$(function(){
	 var salesInvoiceId = Number($('#salesInvoiceId').val());   
	 $('.rowidrc').each(function(){ 
		 var rowid = getRowId($(this).attr('id'));  
		 var invocieLineId =  Number($('#invocieLineId_'+rowid).val());   
 		 if(invocieLineId > 0) 
			 $(this).css('background','#FFFFCC'); 
		 if(invocieLineId > 0 && salesInvoiceId == 0){ 
			 $('#amount_'+rowid).attr('readonly',true);
			 $('#invoiceQty_'+rowid).attr('readonly',true);
		 }
	 });
	
});
</script>
<div id="hrm" class="portlet-content width100 invoiceline-info" style="height: 90%;"> 
	<div class="portlet-content"> 
	<div  id="page-error-popup" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	<div id="warning_message-popup" class="response-msg notice ui-corner-all width90" style="display:none;"></div>
	<div id="hrm" class="hastable width100"  >  
		 <table id="hastab-1" class="width100"> 
			<thead>
			   <tr>  
			   		<th>Product</th>  
				    <th>Delivery Quantity</th> 
				    <th style="display:none;">Inward Quantity</th>
  				    <th>Invoice Quantity</th> 
				    <th>Unit Rate</th> 
				    <th>Total</th>  
				    <th style="display : none;"><fmt:message key="accounts.common.label.options"/></th> 
			  </tr>
			</thead> 
			<tbody class="tab-1">  
				<c:forEach var="bean" varStatus="status" items="${SALESDELIVERY_DETAIL_BYID}"> 
					<tr class="rowidrc" id="fieldrowrc_${status.index+1}">
						<td style="display:none;" id="lineIdrc_${status.index+1}">${status.index+1}</td>  
						<td id="productname_${status.index+1}"> 
							${bean.product.productName}
						</td> 
 						<td id="deliveryqty_${status.index+1}">${bean.deliveryQuantity}</td>
 						<td style="display:none;" id="inwardQuantity_${status.index+1}">${bean.inwardQuantity}</td>
  						<td>  
							<c:set var="total" value="${bean.invoiceQuantity * bean.unitRate}"/>
							<span>${bean.deliveryQuantity}</span>
							<input type="hidden" class="width90 invoiceQty"
								value="${bean.deliveryQuantity}" name="invoiceQty" id="invoiceQty_${status.index+1}"/>
							<input type="hidden" value="${bean.invoiceQuantity}" name="invoiceHiddenQty" id="invoiceHiddenQty_${status.index+1}"/>
					 	</td>  
						<td>
							<c:set var="unitrate" value="${bean.unitRate}"/> 
							<input type="hidden" name="basePrice" id="basePrice_${status.index+1}"/>
							<input type="hidden" name="unitRate" id="amount_${status.index+1}" style="text-align: right;" readonly="readonly" 
								value="${bean.unitRate}" class="unitrate width50"/> 	
							<span class="float-right">${bean.unitRate}</span>
						</td>  
						<td id="total_${status.index+1}" style="text-align: right;"> 
							${bean.totalRate}
 						</td>   
						<td style="width:0.01%; display: none;" class="opn_td" id="optionrc_${status.index+1}">
						  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_rc" id="DeleteCImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
								<span class="ui-icon ui-icon-circle-close"></span>
						  </a>
						  <input type="hidden" name="deliveryNoteId" id="deliveryNoteId_${status.index+1}" value="${requestScope.salesDeliveryNoteId}"/>
						  <input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
						  <input type="hidden" id="shelfid_${status.index+1}" value="${bean.shelf.shelfId}"/>
						  <input type="hidden" id="storeid_${status.index+1}" value="${bean.shelf.shelf.aisle.store.storeId}"/>
 						  <input type="hidden" name="deliveryNoteDetailId" id="deliveryNoteDetailId_${status.index+1}" value="${bean.salesDeliveryDetailId}"/> 
						  <input type="hidden" id="invocieLineId_${status.index+1}" value="${bean.salesInvoiceDetailId}"/> 
						</td>   
					</tr>
				</c:forEach>
			</tbody>
			</table>
		</div>  
	</div>   
	<div class="clearfix"></div>  
	<input type="hidden" id="currentRowId" value="${requestScope.currentRowId}"/>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
		<div class="portlet-header ui-widget-header float-left salesInvoicesessionSave" style="cursor:pointer; display: none;"><fmt:message key="accounts.common.button.ok"/></div>
		<div class="portlet-header ui-widget-header float-left salesInvoicecloseDom" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>
	</div>  
</div> 