<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.realestate.domain.entity.Contract"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
</style>
<script  type="text/javascript">
$(function(){
	$('.process-coa').click(function(){
		var codeArray=new Array();
		var accountArray=new Array();
		var typeArray=new Array();
		var segmentArray=new Array();
		var accountDetails="";
		$('.rowid').each(function(){
			var rowId = getRowId($(this).attr('id'));
			var accountCode = $('#code_'+rowId).text().trim();
			var description = $('#accountDescription_'+rowId).val().trim();
			var accountType = Number($('#accountTypeId_'+rowId).val().trim());
			var segmentId = $('#segmentId_'+rowId).text().trim();
			codeArray.push(accountCode);
			accountArray.push(description);
			typeArray.push(accountType);
			segmentArray.push(segmentId);
		});
		for(var j=0;j<codeArray.length;j++){ 
			accountDetails+=codeArray[j]+"__"+accountArray[j]+"__"+typeArray[j]+"__"+segmentArray[j];
			if(j==codeArray.length-1){   
			} 
			else{
				accountDetails+="#@";
			}
		}
		var showPage=$(this).attr('id');
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/save_account_coatemplate.action", 
	     	async: false, 
	     	data:{accountDescription: accountDetails, showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){  
				$("#account-entry").html(result);
			} 		
		}); 
	});

	$('#combination-viewback').click(function(){
		var showPage=$('.process-coa').attr('id');
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_combination_template.action", 
	     	async: false, 
	     	data:{showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){  
				$("#DOMWindow").html(result);
			} 		
		}); 
	});
	
	$('.discard-coa-view').click(function(){
		var page=$(this).attr('id');
		var urlPath="";
		if(page=="add-template")
			urlPath = "discard_coatemplate";
		else
			urlPath = "discard_coatemplate_customise";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/"+urlPath+".action", 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result){  
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove(); 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="account-entry">
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			CHART OF ACCOUNTS TEMPLATE  
		</div> 
		<div class="portlet-content class98" id="hrm"> 
			<div id="temp-result" style="display: none;"></div> 
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			<div id="page-success" class="response-msg success ui-corner-all width90" style="display:none;"></div>  
			<div id="hrm" class="hastable width100"> 
		  		<c:choose>
		  			<c:when test="${COA_TEMPLATE ne null && COA_TEMPLATE ne ''}">
		  				<table id="hastab" class="width100"> 
		  					<thead>
							   <tr> 
							   		<th style="width: 8%;"><fmt:message key="accounts.coa.label.segmentdetails"/></th>  
									<th style="width: 10%;"><fmt:message key="accounts.coa.label.accountcode"/></th> 
								    <th style="width: 30%;"><fmt:message key="accounts.coa.label.description"/></th>  
								    <th style="width: 5%;"><fmt:message key="accounts.coa.label.accounttype"/></th> 
							  </tr>
							</thead> 
						<tbody class="tab"> 
							<c:forEach var="TEMPLATE" items="${COA_TEMPLATE}" varStatus="status"> 
								<tr id="fieldrow_${status.index+1}" class="rowid">	
									<td id="segment_${status.index+1}">${TEMPLATE.segment.segment}</td>
									<td id="code_${status.index+1}">${TEMPLATE.code}</td>
									<td>
										<input type="text" id="accountDescription_${status.index+1}" value="${TEMPLATE.account}"/>
									</td>
									<td id="accounttype_${status.index+1}">
										<c:choose>
											<c:when test="${TEMPLATE.accountType ne null && TEMPLATE.accountType ne ''}">
											<c:set var="accountType" value="${TEMPLATE.accountType}"/> 
												 <%=Constants.Accounts.AccountType.get(Integer.valueOf(pageContext.getAttribute("accountType").toString()))%>
											</c:when>
										</c:choose>
										<input type="hidden" id="accountTypeId_${status.index+1}" value="${TEMPLATE.accountType}"/>
									</td> 
									<td id="segmentId_${status.index+1}" style="display:none;">
										${TEMPLATE.segment.segmentId}
									</td>  
								</tr>
							</c:forEach>
		  				</table>
		  			</c:when>
		  		</c:choose>
		  	</div>
		 </div>
		 <div class="clearfix"> </div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 	
			<div class="portlet-header ui-widget-header float-right discard-coa-view" id="${showPage}" ><fmt:message key="accounts.common.button.discard"/></div>
			<div class="portlet-header ui-widget-header float-right process-coa" id="${showPage}" ><fmt:message key="accounts.common.button.process"/></div>	
		</div>  
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 	
			<div class="portlet-header ui-widget-header float-right" id="combination-viewback" ><fmt:message key="accounts.common.button.back"/></div>	
		</div> 
	</div>
</div> 	</div>