<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Goods Return Report</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
</style>


<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">


	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading"><span>Goods Return</span>
		</span>
	</div>
	<div style="clear: both;"></div>

	<div class="clear-fix"></div> 

	<div style="position: relative; top: 10px; height: 100%;"
		class="data-list-container width98 float_left">

		<table class="width100">
			<thead>
				<tr>
					<th>${RETURN_INFO.supplierName}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td align="center" style="padding-right: 0px">
						<table style="width: 98%">
							<tr>
								<th>Reference Number</th>
								<th>Date</th>
							</tr>
							<tr>
								<td>${RETURN_INFO.returnNumber}</td>
								<td>${RETURN_INFO.goodsReturnDate}</td>
							</tr>
							<tr>
								<td colspan="4" align="center" style="padding-right: 0px;">
									<table
										style="width: 99%; margin-top: 10px; margin-bottom: 10px;">
										<tr>
											<th>Product Code</th>
											<th>Product</th>
											<th>Unit</th>
											<th>Return Quantity</th>
											<th>Unit Rate</th>
											<th>Total</th>
										</tr>
										<c:forEach items="${RETURN_INFO.goodsReturnDetailVOs}" var="returnDt">
											<tr>
												<td>${returnDt.productCode}</td>
												<td>${returnDt.productName}</td>
												<td>${returnDt.unitNameStr}</td>
												<td>${returnDt.packageUnit}</td>
												<td style="text-align: right;">${returnDt.unitRateStr}</td>
												<td style="text-align: right;">${returnDt.totalAmount}</td>
											</tr>
										</c:forEach>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3" style="text-align: right;">Total</td>
								<td style="text-align: right;">${RETURN_INFO.displayTotalAmount}</td>
							</tr>
						</table></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>