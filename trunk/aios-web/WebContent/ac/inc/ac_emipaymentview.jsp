 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@page import="java.text.DecimalFormat" %>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%> 
<style>
#hrm div {
    float: left;
    position: relative;
    width: 100%;
}
</style>
<script type="text/javascript">
var emiPayments=""; var accountTypeId=0; var idval=0;
$(function(){
	convertChar(); 
	//Combination pop-up config
    $('.codecombination-popup').click(function(){  
 		 var tempvar=$(this).attr('id');
 		 var splitval=tempvar.split("_"); 
 		 idval=splitval[1];  
         $('.ui-dialog-titlebar').remove();   
             $.ajax({
                type:"POST",
                url:"<%=request.getContextPath()%>/combination_treeview.action",
                async: false, 
                dataType: "html",
                cache: false,
                success:function(result){
                     $('.codecombination-result').html(result);
                },
                error:function(result){
                     $('.codecombination-result').html(result);
                }
            });
    }); 
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
	});

     $('.discard').click(function(){
    	 window.location.reload();
     });

     $('.process').click(function(){
    	 emiPayments="";
         var loanId=$('#loanId').val();
         var dueDate=$('#dueDate').text();
         emiPayments=getEmiPayments();
         $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/save_emipayments.action",
             async: false,
             data:{loanId:loanId, dueDate:dueDate, attribute1:emiPayments},
             dataType: "html",
             cache: false,
             success:function(result){
            	 $(".tempresult").html(result);
            	 var message=$('#return_message').html(); 
				 if(message.trim()=="SUCCESS"){
					 window.location.reload();
				 }
				 else{
					 $('#page-error').hide().html(message).slideDown(1000);
					 return false;
				 }
             },
             error:function(result){
            	 $(".tempresult").html(result);
             }
         });
     });

     var getEmiPayments =function(rowId){
 		var amountArray=new Array();
 		var paymentName=new Array(); 
 		var combination=new Array(); 
 		var loanAmount=0;
 		$('.rowid').each(function(){
 	 		var rowId=getRowId($(this).attr('id'));
 	 		var name=$('#name_'+rowId).text();
 	 		var amount=$('#paidAmount_'+rowId).val();
 	 		var codeId=$('#codeCombinationId_'+rowId).val();
 	 		amountArray.push(amount);
 	 		paymentName.push(name);
 	 		if(typeof codeId!='undefined' && codeId!='')
 	 			combination.push(codeId);
 	 		else
 	 			combination.push(0);
 	 	});
 		 
 		for(var j=0;j<paymentName.length;j++){ 
 			loanAmount=amountArray[j].replace(/,/gi,'');
 			emiPayments+=paymentName[j]+"#@"+loanAmount+"#@"+combination[j];
 			if(j==paymentName.length-1){   
 			} 
 			else{
 				emiPayments+="##@";
 			}
 		} 
 		return emiPayments;
 	};
});
function convertChar(){
	if($('#emiFrequency').text()=='M')
		 emifrequency="Monthly";
	 else if($('#emiFrequency').text()=='D')
		 emifrequency="Daily";
	 else if($('#emiFrequency').text()=='W')
		 emifrequency="Weekly";
	 else if($('#emiFrequency').text()=='Q')
		 emifrequency="Quaterly";
	 else if($('#emiFrequency').text()=='H')
		 emifrequency="SemiAnually";
	 else if($('#emiFrequency').text()=='Y')
		 emifrequency="Anually";
	 $('#emiFrequency').text(emifrequency);  

	 if($('#rateVariance').text()=='F')
		 ratetype="Flat";
	 else if($('#rateVariance').text()=='D')
		 ratetype="Diminishing";  
	 if($('#rateType').text()=='F')
		 ratevariance="Fixed";
	 else if($('#rateType').text()=='V')
		 ratevariance="Vary"; 
	 else if($('#rateType').text()=='M')
		 ratevariance="Mixed";
	 $('#rateVariance').text(ratetype);  
	 $('#rateType').text(ratevariance);
	 if($('#loanType').text()=='P')
    	 $('#loanType').text("Predictable");
     else
    	 $('#loanType').text("Unpredictable");
}

function setCombination(combinationTreeId,combinationTree){
	$('#codecombination-text_'+idval).text(combinationTree); 
	$('#codeCombinationId_'+idval).val(combinationTreeId); 
} 
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>emi details</div>	
		  <form name="loanentry_details" id="loanentry_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="loanId" name="loanId" value="${ALERT_LOAN_DETAILS.loanId}"/>
				<div class="width100 float-left" id="hrm"> 
					<fieldset>
						<legend>EMI Details</legend>
						<div class="width100 float-left">
							<div class="width48 float-right"> 
								<div>
									<label class="width30" for="rateType">Rate Type</label>
									<span id="rateType" class="width50">${ALERT_LOAN_DETAILS.rateType}</span> 
								</div> 
								<div>
									<label class="width30" for="rateType">Rate Variance</label>
									<span id="rateVariance" class="width50">${ALERT_LOAN_DETAILS.rateVariance}</span>
								</div> 
								<div>
									<label class="width30" for="emiFrequency">EMI Frequency</label>
									<span id="emiFrequency" class="width50">${ALERT_LOAN_DETAILS.emiFrequency}</span>
								</div> 
								<div>
									<label class="width30" for="emiDueDate">Due Due</label>
									 <c:set var="emiFirstDuedate" value="${ALERT_LOAN_DETAILS.emiFirstDuedate}"/>  
									 <%
									 	String[] string=pageContext.getAttribute("emiFirstDuedate").toString().split(" ");
									 	StringBuilder builder=new StringBuilder(); 
									 	builder.append(string[2].concat("-")+string[1].concat("-")+string[5]);  
									 %>	
									 <span id="dueDate" class="width50"><%=builder%></span>
								</div>  
							</div>
							<div class="width50 float-left">
								<div>
									<label class="width30" for="loanNumber">Loan Number</label>
									<span id="loanNumber" class="width50">${ALERT_LOAN_DETAILS.loanNumber}</span> 
								</div> 
								<div style="display:none;">
									<label class="width30" for="currency">Currency</label>
									<span id="currency" class="width50">${ALERT_LOAN_DETAILS.currency.currencyPool.code}</span>
								</div> 
								<div>
									<label class="width30" for="loanType">Loan Type</label>
									<span id="loanType" class="width50">${ALERT_LOAN_DETAILS.loanType}</span>
								</div>
								<div>
									<label class="width30" for="approvalAmount">Approval Amount</label>
									<c:set var="loanamount" value="${ALERT_LOAN_DETAILS.amount}"/>
						     		<%
						     			DecimalFormat decimalFormat=new DecimalFormat();
						     			String loanamount=decimalFormat.format(pageContext.getAttribute("loanamount"));
						     			loanamount = AIOSCommons.formatAmount(pageContext.getAttribute("loanamount"));
						     			pageContext.setAttribute("loanamount", loanamount); 
						     		%>
						     		<span id="approvalAmount" class="width50">${loanamount}</span>
								</div> 
								<div>
									<label class="width30" for="accountNumber">Bank</label>
									<span>${ALERT_LOAN_DETAILS.bankAccount.bank.bankName}</span>
									<span id="approvalAmount" class="width50">A/C No ${ALERT_LOAN_DETAILS.bankAccount.accountNumber}</span>
								</div>  
							</div>
						</div>  
					</fieldset>
				</div>   
				<div class="clearfix"></div> 
 				<div class="portlet-content" id="hrm">
 					<fieldset>
						<legend>EMI Schedule</legend>
						<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
						<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div> 
						<div id="hrm" class="hastable width100"  >  
							<input type="hidden" name="childCount" id="childCount" value=""/>  
							<table id="hastab" class="width100"> 
								<thead>
							   		<tr>  
								   	 	<c:choose>
					   	 					<c:when test="${requestScope.EMI_DETAILS ne null && requestScope.EMI_DETAILS ne ''}"> 
				   	 							<th style="width:1%">Line No</th> 
				   	 							<th style="width:5%">Name</th>
				   	 							<th style="width:5%">Amount</th>
				   	 							<th style="width:5%">Combiantion</th> 
					   	 					</c:when> 
					   	 			  </c:choose>  
						 		 </tr>
								</thead> 
								<tbody class="tab">  
									<c:choose>
										<c:when test="${requestScope.EMI_DETAILS ne null && requestScope.EMI_DETAILS ne '' && fn:length(requestScope.EMI_DETAILS)>0}">
											<c:forEach var="beans" items="${EMI_DETAILS}" varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">  
													<td>${status.index+1}</td>
													<td id="name_${status.index+1}">${beans.name}</td>
					   	 							<td id="amount_${status.index+1}">
					   	 								<c:set var="paidAmount" value="${beans.amount}"/>
														<%String paidAmount = AIOSCommons.formatAmount(pageContext.getAttribute("paidAmount"));
																		pageContext.setAttribute("paidAmount", paidAmount);%>
					   	 								<input type="text" id="paidAmount_${status.index+1}" value="${paidAmount}" class="width95" style="border:0px;text-align: right;"/> 
					   	 							</td> 
					   	 							<td id="combination_${status.index+1}"> 
					   	 								<c:set var="current" value="${status.index+1}"/>
					   	 								<c:choose>
					   	 									<c:when test="${fn:length(requestScope.EMI_DETAILS)>current}">
					   	 										<span class="float-left width70" id="codecombination-text_${status.index+1}"></span>
																<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status.index+1}"/>  
							   	 								<span class="button float-right" style="position: relative;">
																	<a style="cursor: pointer;" id="codecombination-popup_${status.index+1}" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
																		<span class="ui-icon ui-icon-newwin"></span> 
																	</a>
																</span>
					   	 									</c:when>
					   	 								</c:choose> 
					   	 							</td>
					   	 						</tr>  
					   	 					</c:forEach>  
										</c:when>
									</c:choose>	 
								</tbody>
							</table>
						</div> 
					</fieldset> 
 				</div>  
		</div>  
		<div class="clearfix"></div> 
		<div id="savediscard" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;">  
			<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
			<div class="portlet-header ui-widget-header float-right process" style="cursor:pointer;">process</div>   
		 </div>  
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>	
  </form>
  </div> 
</div> 