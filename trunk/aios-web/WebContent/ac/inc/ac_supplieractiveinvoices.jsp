<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${SUPPLIER_INVOICE ne null && fn:length(SUPPLIER_INVOICE) > 0}">
	<fieldset>
		<legend>Active Supplier Invoice</legend> 
		<div class="clearfix"></div>
		<c:forEach var="invoice" items="${SUPPLIER_INVOICE}" varStatus="status">
			<div class="width20 float-left">
				<input type="checkbox" class="activeInvoice float-left" style="top: 3px; position: relative;"
					 name="activeInvoice_${invoice.invoiceId}" id="activeInvoice_${invoice.invoiceId}"/>
				<label class="width90 float-right filter" for="activeInvoice_${invoice.invoiceId}">${invoice.invoiceReference}</label>
			</div>
		</c:forEach>
	</fieldset>
</c:if>