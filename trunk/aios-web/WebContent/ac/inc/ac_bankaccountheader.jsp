<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <table id="hastab" class="width100">
	<thead>
		<tr>
			<th class="width10">Account No.</th>
			<th class="width10">Account Holder</th>
			<th class="width10">Type</th> 
			<th class="width10">Routing</th>
			<th class="width10">IBAN</th> 
			<th class="width10">Currency</th>
			<th class="width10">Mode</th> 
			<th class="width10">Purpose</th> 
			<th class="width10">Branch Name</th> 
			<th class="width10"><fmt:message
					key="accounts.calendar.label.options" /></th>
		</tr>
	</thead>
	<tbody class="tab">  
			<c:forEach var="i" begin="1"
				end="2" step="1"
				varStatus="status">
				<tr id="fieldrow_${i}" class="rowid">
					<td class="width10" id="accountNumber_${i}"></td>
					<td class="width10" id="accountHolder_${i}"></td>
					<td class="width10" id="accountType_${i}"></td>  
					<td class="width10" id="rountingNumber_${i}"></td>
					<td class="width10" id="iban_${i}"></td> 
					<td class="width10" id="currency_${i}"></td>
					<td class="width10" id="mode_${i}"></td> 
					<td class="width10" id="accountPurpose_${i}"></td>
					<td class="width10" id="branchName_${i}"></td> 
					<td style="display: none;" class="width10" id="creditLimit_${i}"></td> 
					<td class="width10" id="option_${i}">
					<input type="hidden" id="bankAccountId_${i}" />
					<input type="hidden" id="lineId_${i}" value="${i}" /> 
					<input type="hidden" id="swift_${i}"/> 
					<input type="hidden" id="holderidval_${i}"/>
					<input type="hidden" id="currencyidval_${i}"/>
					<input type="hidden" id="typeidval_${i}"/>
					<input type="hidden" id="modeidval_${i}"/>
					<input type="hidden" id="sign1_${i}"/> 
					<input type="hidden" id="sign2_${i}"/>
					<input type="hidden" id="address_${i}"/>
					<input type="hidden" id="branchContact_${i}"/>
					<input type="hidden" id="contactPerson_${i}"/>
				     <a
						style="cursor: pointer;"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankaddData"
						id="AddImage_${i}" title="Add Record"> <span
							class="ui-icon ui-icon-plus"></span> </a> <a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
						id="EditImage_${i}" style="display: none; cursor: pointer;"
						title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
					</a> <a style="cursor: pointer; display: none;"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
						id="DeleteImage_${i}" title="Delete Record"> <span
							class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
						id="WorkingImage_${i}" style="display: none;" title="Working">
							<span class="processing"></span> </a>
					</td>

				</tr>
			</c:forEach> 
	</tbody>
</table>