<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${ISSUE_REQUISTION_BYID.referenceNumber}
	</td> 
	<td> 
		<c:set var="issueDate" value="${ISSUE_REQUISTION_BYID.issueDate}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("issueDate").toString())%>  
	</td> 
	<td>
		 ${ISSUE_REQUISTION_BYID.totalIssueQty}
	</td>
	<td style="text-align: right;">
		${ISSUE_REQUISTION_BYID.totalReceiveAmount} 
	</td> 
	<td style="display:none;">
		<input type="hidden" id="issueRequistionId_${rowId}" value="${ISSUE_REQUISTION_BYID.issueRequistionId}"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}"> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editReturnData" id="EditImage_${rowId}" style="cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowReturn" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a> 
	</td>   
</tr>