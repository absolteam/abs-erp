<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var receivePerson = Number(0);
var productionReceiveId = Number(0);
var slidetab = "";
var receiveDetails = [];
var accessCode = "";
$(function(){
	manupulateLastRow();
	$jquery("#productReceiveValidation").validationEngine('attach'); 

	$('#receiveDate').datepick({
		showTrigger: '#calImg'});

	 $('#productionreceive_discard').click(function(event){  
		 productionReceiveDiscard("");
		 return false;
	 });

	 $('#productionreceive_save').click(function(){
		 receiveDetails = [];
		 if($jquery("#productReceiveValidation").validationEngine('validate')){
 		 		
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var receiveDate = $('#receiveDate').val(); 
	 			var receiveSource = Number($('#receiveSource').val());   
	 			var description=$('#description').val();  
 	 			receiveDetails = getReceiveDetails();  
	 			 
	   			if(receiveDetails!=null && receiveDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_production_receive.action", 
	 				 	async: false, 
	 				 	data:{	productionReceiveId: productionReceiveId, referenceNumber: referenceNumber, receiveDate: receiveDate, 
		 				 		receiveSource: receiveSource, receivePerson: receivePerson, description: description, 
		 				 		receiveDetails: JSON.stringify(receiveDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							productionReceiveDiscard(productionReceiveId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					},
	 					error:function(result){  
	 						$('#page-error').hide().html("Internal error.").slideDown(1000);
	 						$('#page-error').delay(2000).slideUp();
	 					}
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter prodction receive details.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 

	  var getReceiveDetails = function(){
		  receiveDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val());  
				var storeId = Number($('#storeid_'+rowId).val()); 
				var storeDetail = Number($('#storeDetail_'+rowId).val()); 
				var productQty = Number($('#productQty_'+rowId).val()); 
				var amount = Number($('#amount_'+rowId).val()); 
				var linesDescription = $.trim($('#linesDescription_'+rowId).text());  
				var productReceiveDetailId =  Number($('#productReceiveDetailId_'+rowId).val()); 
				if(typeof productId != 'undefined' && productId > 0 && storeDetail > 0 && productQty > 0
						&&   amount > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"storeId" : storeId,
						"rackId" : storeDetail,
						"productQty" : productQty,
						"amount": amount,
						"linesDescription": linesDescription,
						"productReceiveDetailId": productReceiveDetailId
					});   
					receiveDetails.push({
						"receiveDetails" : jsonData
					});
				} 
			});  
			return receiveDetails;
		 };

	 $('.receive-source-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	     $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	


	 $('.show_product_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		var tableRowId = getRowId($(this).attr('id'));
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_common_popup.action", 
		 	async: false,  
		 	data: {itemType: "I", rowId: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#product-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/production_receive_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.store-common-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_shelfdetails.action", 
			 	async: false,  
			 	data:{rowId : rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
					 return false;
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});
	   
	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="PRODUCTION_SOURCE"){
			$('#receiveSource').html("");
			$('#receiveSource').append("<option value=''>Select</option>");
			loadLookupList("receiveSource"); 
		}  
	});

 	$('.productQty,.amount').live('change',function(){  
 	 	var rowId = getRowId($(this).attr('id'));
 		var productQty = Number($('#productQty_'+rowId).val());
 		var amount = Number($('#amount_'+rowId).val());  
 		if(productQty > 0 && amount > 0){
 	 		var totalAmount = Number(productQty * amount).toFixed(2);
 	 		$('#totalAmount_'+rowId).html(totalAmount);
 	 		triggerAddRow(rowId);
 	 	}
 	 	return false;
	});
	
 	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 

 	 if(Number('${PRODUCTION_RECEIVES.productionReceiveId}')> 0){
 		productionReceiveId = Number('${PRODUCTION_RECEIVES.productionReceiveId}');
 		receivePerson = Number('${PRODUCTION_RECEIVES.personByReceivePerson.personId}'); 
 		$('#receiveSource').val('${PRODUCTION_RECEIVES.lookupDetail.lookupDetailId}');
	 }
	 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#receivePerson').val(personname);
	receivePerson = personid;
	$('#common-popup').dialog("close");
}
function commonProductPopup(productId, productName, rowId) {  
	$('#productid_' + rowId).val(productId);
	$('#product_' + rowId).html(productName);
}
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
	var storeid = Number($('#storeid_'+rowId).val());  
	var productQty = Number($('#productQty_'+rowId).val()); 
	var amount = Number($('#amount_'+rowId).val());
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid > 0 && storeid > 0
			&& productQty > 0
			&& amount > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function productionReceiveDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_production_receives.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();
			$('#codecombination-popup').dialog('destroy');		
			$('#codecombination-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	function setCombination(combinationTreeId, combinationTree) {
		$('#combinationId').val(combinationTreeId);
		$('#codeCombination').val(combinationTree);
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> production
			receive
		</div> 
		<form name="productReceiveValidation" id="productReceiveValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30" for="receivePerson">Receive
									Person<span style="color: red;">*</span> </label> <input type="text"
									readonly="readonly" name="receivePerson" id="receivePerson"
									class="width50 validate[required]"
									value="${PRODUCTION_RECEIVES.personByReceivePerson.firstName } ${PRODUCTION_RECEIVES.personByReceivePerson.lastName }" />
								<span class="button"
									style="position: relative !important; top: 6px !important; right: 40px; float: right;">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${PRODUCTION_RECEIVES.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PRODUCTION_RECEIVES.referenceNumber ne null && PRODUCTION_RECEIVES.referenceNumber ne ''}">
												${PRODUCTION_RECEIVES.referenceNumber} 
													<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${PRODUCTION_RECEIVES.referenceNumber} "
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="receiveDate"> Receive Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PRODUCTION_RECEIVES.receiveDate ne null && PRODUCTION_RECEIVES.receiveDate ne ''}">
										<c:set var="receiveDate"
											value="${PRODUCTION_RECEIVES.receiveDate}" />
										<input name="receiveDate" type="text" readonly="readonly"
											id="receiveDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("receiveDate").toString())%>"
											class="receiveDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="receiveDate" type="text" readonly="readonly"
											id="receiveDate"
											class="receiveDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Receive Source<span
									style="color: red;">*</span> </label> <select name="receiveSource"
									id="receiveSource" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="SOURCE" items="${PRODUCTION_SOURCE}">
										<option value="${SOURCE.lookupDetailId}">${SOURCE.displayName}</option>
									</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="PRODUCTION_SOURCE"
									class="btn ui-state-default ui-corner-all receive-source-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Production Receive Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th class="width10">Store</th>
										<th style="width: 5%;">Quantity</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 5%;">Total</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${PRODUCTION_RECEIVES.productionReceiveDetails}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.shelf.aisle.store.storeId}" /> <input
												type="hidden" name="storeDetail" id="storeDetail_${i}"
												value="${DETAIL.shelf.shelfId}" /> <span
												id="store_${status.index+1}">${DETAIL.shelf.aisle.store.storeName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;"
													id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all store-common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80">
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="amount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><c:set var="totalAmount"
													value="${DETAIL.quantity * DETAIL.unitRate}" /> <span
												id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
											</td>
											<td><input type="text" name="linesDescription"
												id="linesDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productReceiveDetailId"
												id="productReceiveDetailId_${status.index+1}"
												value="${DETAIL.productReceiveDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(PRODUCTION_RECEIVES.productionReceiveDetails)+1}"
										end="${fn:length(PRODUCTION_RECEIVES.productionReceiveDetails)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show_product_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="hidden" name="storeId"
												id="storeid_${i}" /> <input type="hidden"
												name="storeDetail" id="storeDetail_${i}" /> <span
												id="store_${i}"></span> <span class="button float-right">
													<a style="cursor: pointer;" id="storeID_${i}"
													class="btn ui-state-default ui-corner-all store-common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${i}"
												class="productQty validate[optional,custom[number]] width80">
											</td>
											<td><input type="text" name="amount" id="amount_${i}"
												style="text-align: right;"
												class="amount width98 validate[optional,custom[number]] right-align">
											</td>
											<td><span id="totalAmount_${i}" style="float: right;"></span>
											</td>
											<td><input type="text" name="linesDescription"
												id="linesDescription_${i}" class="width98" maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productReceiveDetailId"
												id="productReceiveDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="productionreceive_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="productionreceive_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>