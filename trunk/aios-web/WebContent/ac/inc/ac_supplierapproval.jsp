<%@ page language="java" contentType="text/html; charset=UTF-8	"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
	.hr_main_content {
		width: 100% !important;
		overflow: hidden;
	}
</style> 
<script type="text/javascript"> 
	$(document).ready(function (){ 
		if(Number($('#supplierId').val()>0)){
			//$('.savediscard').show();
 			$('#openingBalance').attr('disabled',true);
			$('#supplierPerson').attr('disabled',true);
			
			if(Number($('#cmpDeptLocSupplierId').val())>0){ 
				$('#supplierCompany').attr('checked',true);
				showCompany($('#companySupplierId').val());
			}else{
				$('#supplierPerson').attr('checked',true);
				showPerson($('#personSupplierId').val());
				
			}
			
			$('#supplierAcType').val($('#tempsupplieractype').val()); 
			$('#paymentTermId').val($('#tempterm').val());
			$('#acClassification').val($('#tempclass').val()); 

		}
		
		 
		 $jquery("#supplier_details").validationEngine('attach'); 
		 
			$('#supplierCompany').click(function() { 
				$('#supplierCompany').attr('disabled', true);
				$('#supplierPerson').attr('disabled', true);
				$('#company-selection-div').show();
				$('#person-selection-div').hide();
				
 				$.ajax({
					type: "POST", 
					url:"<%=request.getContextPath()%>/add_company.action",
					data: {companyId : 0, isSupplier: true}, 
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result) { 
						$("#sub-content").html(result);
					},
					error: function(result) { 
						alert("error: " + result); 
					} 
				}); 
				
				identityShowHideCall(false);
				$('#companyTypeIdTemp').val("8,"+$('#companyTypeIdTemp').val());
				companyTypeAutoSelectd();
				$('#supplierinfo').val(true);
				$("#supplier-result-div").html($("#supplier-data-div").html());
				$("#supplier-data-div").html("");
  			});
			
			$('#supplierPerson').click(function() { 
				$('#supplierPerson').attr('disabled', true);
				$('#supplierCompany').attr('disabled', true); 
				$('#company-selection-div').hide();
				$('#person-selection-div').show();
 				$.ajax({
					type: "POST", 
					url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
					data: {personId: 0, isSupplier: true},
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result) { 
						$("#sub-content").html(result); 
					},
					error: function(result) { 
						alert("error: " + result); 
					} 
				});
				$('#supplierinfo').val(true);
				identityShowHideCall(false);
				dependentShowHideCall(false);
				$('#personTypeIdTemp').val("7,"+$('#personTypeIdTemp').val());
				personTypeAutoSelectd();
				$("#supplier-result-div").html($("#supplier-data-div").html());
				$("#supplier-data-div").html("");
				$('#personType').attr('disabled', true);

  			});

			$('#openingBalance').change(function(){
				if(Number($('#openingBalance').val())>0){
					$('#currencyId').addClass('validate[required]');
					$('#currency').show();
				}else{
					$('#currencyId').removeClass('validate[required]');
					$('#currency').hide();
				}
			});

			$('.sdiscard').click(function(){ 
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/supplier_list.action", 
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){ 
							$('#codecombination-popup').dialog('destroy');		
							$('#codecombination-popup').remove(); 
							$("#main-wrapper").html(result);  
						}
				 });
			 });

			 $('#ssave').click(function(){ 
				 if($jquery("#supplier_details").validationEngine('validate')){
 					 var supplierId=$('#supplierId').val();
					 var supplierNumber=$('#supplierNumber').val();
					 var supplierAcType=$('#supplierAcType').val();
					 var acClassification=$('#acClassification').val();
					 var paymentTermId=Number($('#paymentTermId').val());
					 var creditLimit=$('#creditLimit').val();
					 var openingBalance=Number($('#openingBalance').val());
					 var bankName=$('#bankName').val();
					 var branchName=$('#branchName').val();
					 var iban=$('#iban').val();
					 var accountNumber=$('#accountNumber').val();   
					 var personId=Number($('#personSupplierId').val());
					 var cmpDeptLocId=Number($('#cmpDeptLocSupplierId').val());  
					 var combinationId=$('#combinationId').val(); 
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/supplier_update.action", 
							data:{
									supplierId:supplierId,
									supplierNumber:supplierNumber,
									supplierAcType:supplierAcType,
									acClassification:acClassification,
									paymentTermId:paymentTermId,
									openingBalance:openingBalance,
									bankName:bankName,
									branchName:branchName,
									ibanNumber:iban,
									accountNumber:accountNumber,
									creditLimit:creditLimit,
									personId:personId,
									cmpDeptLocId:cmpDeptLocId,
									combinationId:combinationId
								},
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){  
								$('#codecombination-popup').dialog('destroy');		
								$('#codecombination-popup').remove(); 
								$("#main-wrapper").html(result);  
							}
					 });
				 }
				 else{
					return false;
				}
			 });
			 
			//pop-up config
		     $('#employeecall').click(function(){
		          $('.ui-dialog-titlebar').remove();  
		          $('#common-popup').dialog('open');
		          var rowId=-1; 
		          var personTypes="ALL";
		             $.ajax({
		                type:"POST",
		                url:"<%=request.getContextPath()%>/common_person_list.action",
		                data:{id:rowId,personTypes:personTypes},
		                async: false,
		                dataType: "html",
		                cache: false,
		                success:function(result){
		                     $('.common-result').html(result);
		                     $('#common-popup').dialog('open');
							 $($($('#common-popup').parent()).get(0)).css('top',0);
		                     return false;
		                },
		                error:function(result){
		                     $('.common-result').html(result);
		                }
		            });
		             return false;
		    }); 
		     $('#company_popupclose').live('click',function(){
		    	 $('#common-popup').dialog('close');
		    	 return false;
		     });
		     
		     $('#companycall').click(function(){
		          $('.ui-dialog-titlebar').remove();  
		          $('#common-popup').dialog('open');
		          var rowId=-1; 
		          var companyTypes="ALL";
		             $.ajax({
		                type:"POST",
		                url:"<%=request.getContextPath()%>/common_company_list.action",
		                data:{id:rowId,companyTypes:companyTypes},
		                async: false,
		                dataType: "html",
		                cache: false,
		                success:function(result){
		                     $('.common-result').html(result);
		                     $('#common-popup').dialog('open');
							 $($($('#common-popup').parent()).get(0)).css('top',0);
		                     return false;
		                },
		                error:function(result){
		                     $('.common-result').html(result);
		                }
		            });
		             return false;
		    }); 
		    $('#common-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800,
				height:600,
				bgiframe: false,
				modal: true 
			});
	});
	function showCompany(companyId){
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/add_company.action",
			data: {companyId : companyId, isSupplier: true}, 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result);
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		
		identityShowHideCall(false);
		$('#companyTypeIdTemp').val("8,"+$('#companyTypeIdTemp').val());
		companyTypeAutoSelectd();
		$('#supplierinfo').val(true);
		$("#supplier-result-div").html($("#supplier-data-div").html());
		$("#supplier-data-div").html("");
	}
	function showPerson(personId){
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
			data: {personId: personId, isSupplier: true, isCustomer: false},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result); 
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		identityShowHideCall(false);
		dependentShowHideCall(false);
		$('#personTypeIdTemp').val("7,"+$('#personTypeIdTemp').val());
		personTypeAutoSelectd();
		$('#personType').attr('disabled', true);
		$('#supplierinfo').val(true);
		$("#supplier-result-div").html($("#supplier-data-div").html());
		$("#supplier-data-div").html("");
	}
	function personPopupResult(personid,personname,commonParam){
		$("#supplier-data-div").html($("#supplier-result-div").html());
		showPerson(personid);
	}
	function companyPopupResult(companyId,companyName,commonParam){
		$("#supplier-data-div").html($("#supplier-result-div").html());
		showCompany(companyId);
	}
</script>
<div id="main-content">	
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
		
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				supplier
			</div> 
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 <fmt:message key="accounts.common.generalinformation" />
			</div> 
			<div id="hrm" class="width100" style="margin:5px;">
				<fieldset><legend><fmt:message key="hr.supplier.supplierType" /></legend> 
					<div class="float-left width30"><input name="supplierType" id="supplierCompany" type="radio"> <fmt:message key="hr.supplier.type.company" /></div>
					<div class="float-left width30"><input name="supplierType" id="supplierPerson" type="radio"> <fmt:message key="hr.supplier.type.person" /></div>
					<div class="float-left width40" id="person-selection-div" style="display:none;">
		                 <label class="width20">Person</label>
		                 <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="employeecall" class="btn ui-state-default ui-corner-all width100 employee-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
	                </div>
	                <div class="float-left width40" id="company-selection-div" style="display:none;">
	                   	<label class="width20">Company</label>
	                    <span class="button" style="position: relative;top:0px; ">
							<a style="cursor: pointer;" id="companycall" class="btn ui-state-default ui-corner-all width100 company-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
	                </div>
				</fieldset> 
				
			</div>
			<div class="width98 float-left" id="hrm">
				<div id="supplier-data-div" style="display:none;">
					<input name="supplierId" id="supplierId" type="hidden" value="${SUPPLIER_INFORMATION.supplierId}"> 
					<input name="personSupplierId" id="personSupplierId" type="hidden" value="${SUPPLIER_INFORMATION.person.personId}"> 
					<c:choose>
						<c:when test="${SUPPLIER_INFORMATION.company ne null}">
							<input name="companySupplierId" id="companySupplierId" type="hidden" value="${SUPPLIER_INFORMATION.company.companyId}">
							<input name="cmpDeptLocSupplierId" id="cmpDeptLocSupplierId" type="hidden" value="${SUPPLIER_INFORMATION.company.companyId}"> 
							
						</c:when>
						<c:otherwise>
							<input name="companySupplierId" id="companySupplierId" type="hidden" value="${SUPPLIER_INFORMATION.cmpDeptLocation.company.companyId}">
							<input name="cmpDeptLocSupplierId" id="cmpDeptLocSupplierId" type="hidden" value="${SUPPLIER_INFORMATION.cmpDeptLocation.cmpDeptLocId}"> 
							<input type="hidden" id="departmentID" value="${SUPPLIER_INFORMATION.cmpDeptLocation.department.departmentId}"/> 
							<input type="hidden" id="locationID" value="${SUPPLIER_INFORMATION.cmpDeptLocation.location.locationId}"/>  
						</c:otherwise>
					</c:choose>
					 
					 <form id="supplier_details" name="supplier_details" style="position: relative;">
						<div class="width48 float-right">
							<fieldset style="min-height:132px;"> 
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.openingBalance" /></label>
									<input type="text" name="openingBalance" class="width50 validate[optional,custom[number]]" id="openingBalance"
										 value="${SUPPLIER_INFORMATION.openingBalance}"/>
								</div>
								<div id="currency" style="display: none;">
									<label class="width30">Currency<span style="color:red">*</span></label>
									<select name="currencyId" class="width51" id="currencyId">
										<option value="">Select</option>
										<c:choose>
											<c:when test="${CURRENCY_LIST ne null && CURRENCY_LIST ne ''}">
												<c:forEach var="cbean" items="${CURRENCY_LIST}">
													<option value="${cbean.currencyId}">${cbean.currencyPool.code}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.bankName" /></label>
									<input type="text" name="bankName" class="width50" id="bankName" value="${SUPPLIER_INFORMATION.bankName}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.branchName" /></label>
									<input type="text" name="branchName" class="width50" id="branchName" value="${SUPPLIER_INFORMATION.branchName}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.iban" /></label>
									<input type="text" name="iban" class="width50" id="iban" value="${SUPPLIER_INFORMATION.ibanNumber}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.accountNo" /></label>
									<input type="text" name="accountNumber" class="width50" id="accountNumber" 
										value="${SUPPLIER_INFORMATION.accountNumber}"/>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset> 
								<div>
									<label class="width30"><fmt:message key="hr.supplier.supplierNumber" /><span style="color:red">*</span></label> 
									<c:choose>
										<c:when test="${SUPPLIER_INFORMATION.supplierId >0}">
											<input type="text" name="supplierNumber" readonly="readonly" class="width50" 
												id="supplierNumber" value="${SUPPLIER_INFORMATION.supplierNumber}"/>
										</c:when>
										<c:otherwise>
											<input type="text" name="supplierNumber" readonly="readonly" class="width50" 
												id="supplierNumber" value="${SUPPLIER_NUMBER}"/>
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30"><fmt:message key="hr.supplier.supplierType" /></label>
									<select name="supplierAcType" id="supplierAcType" class="width51">
										<option value="">Select</option>
										<c:choose>
											<c:when test="${SUPPLIER_ACTYPE ne null && SUPPLIER_ACTYPE ne '' }">
												<c:forEach items="${SUPPLIER_ACTYPE}" var="bean">
													<option value="${bean.key}">${bean.value}</option>
												</c:forEach>
											</c:when>
										</c:choose> 
									</select>
									<input type="hidden" id="tempsupplieractype" value="${SUPPLIER_INFORMATION.supplierType}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="hr.supplier.supplierInfo" /></label>
									<select name="acClassification" id="acClassification" class="width51">
										<option value="">Select</option>
										<c:choose>
											<c:when test="${SUPPLIER_CLASSIFICATION ne null && SUPPLIER_CLASSIFICATION ne '' }">
												<c:forEach items="${SUPPLIER_CLASSIFICATION}" var="entry">
													<option value="${entry.key}">${entry.value}</option>
												</c:forEach>
											</c:when>
										</c:choose>  
									</select>
									<input type="hidden" id="tempclass" value="${SUPPLIER_INFORMATION.classification}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.paymentTerms" /></label>
									<select name="paymentTerm" id="paymentTermId" class="width51">
										<option value="">Select</option>
										<c:choose>
											<c:when test="${PAYMENT_TERM_LIST ne null && PAYMENT_TERM_LIST ne '' }">
												<c:forEach items="${PAYMENT_TERM_LIST}" var="pbean">
													<option value="${pbean.creditTermId}">${pbean.name}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
									<input type="hidden" id="tempterm" value="${SUPPLIER_INFORMATION.creditTerm.paymentTermId}"/>
								</div>
								<div>
									<label class="width30"><fmt:message key="accounts.supplier.creditLimit" /></label>
									<input type="text" name="creditLimit" class="width50" id="creditLimit" value="${SUPPLIER_INFORMATION.creditLimit}"/>
								</div>
						</fieldset>
					</div>
				</form>
			</div>
		</div> 
		<div class="clearfix"></div> 
		<div id="sub-content" class="width100 float-left"> 
		</div> 
		<div class="clearfix"></div>  
		
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	 	</div> 
	</div>
</div>