<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
.caption {
	padding: 2px;
	padding-top: 5px !important;
}

.data {
	padding: 2px;
	padding-top: 5px !important;
}

.data-container {
	padding: 5px;
}

td {
	padding-top: 8px !important;
}

th {
	font-weight: bold;
	padding-top: 8px !important;
}

@media print {
	.hideWhilePrint {
		display: none;
	}
}
</style>
<script type="text/javascript"> 
 
 </script>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">

	<div class="width100 float-left">
		<span class="heading">Stock Reorder</span>
	</div>
	<div id="main-content" class="hr_main_content">
		<div class="width100 float-left">
			<div id="hrm" class=" hastable width100">
				<table class="width100">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th> 
							<th>Category</th> 
							<th>Reorder Quantity</th>
							<th>Available Quantity</th>
							<th>Cost Price</th>
							<th>Standard Price</th>
							<th>Sale Price</th>
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach var="stockDetail" items="${REORDER_STOCKS}"
							varStatus="status">
							<tr class="rowid">
								<td>${stockDetail.productCode}</td>
								<td>${stockDetail.productName}</td>
								<td>${stockDetail.categoryName}</td>
 								<td>${stockDetail.reorderQuantity}</td>
								<td>${stockDetail.availableQuantity}</td>
								<td>${stockDetail.costPrice}</td>
								<td>${stockDetail.standardPrice}</td>
								<td>${stockDetail.unitRate}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>

		</div>
	</div> 
</body>