<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend>Active Invoice</legend>
	<div>
		<c:forEach var="ACTIVE_INVOICE" items="${INVOICE_NOTES}">
			<label for="activeInvoiceNumber_${ACTIVE_INVOICE.invoiceId}" class="width10" id="grnLabel_${ACTIVE_INVOICE.invoiceId}">
				<input type="checkbox" name="activeInvoiceNumber" class="activeInvoiceNumber activeInvNumberUpdate" id="activeInvoiceNumber_${ACTIVE_INVOICE.invoiceId}"/>
				${ACTIVE_INVOICE.invoiceNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultInvoiceSelect($('.activeInvoiceNumber').attr('id'));
	$('.activeInvoiceNumber').click(function(){   
		validateInvoiceInfo($(this));   
	});
});
</script>