<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
}
</style>
<div id="hrm" class="hastable width100">
	<table id="hastab" class="width100">
		<thead>
			<tr>
				<th style="width: 5%">Item Type</th>
				<th style="width: 10%">Product</th>
				<th style="width: 8%" class="hidemandatorymrp">Store<span
					class="mandatory hidemandatorymrp">*</span>
				</th>
				<th style="display: none;">UOM</th>
				<th style="width: 8%;">Packaging</th>
				<th style="width: 5%">Quantity</th>
				<th style="width: 5%">Base Qty</th>
				<th style="width: 5%">Unit Rate</th>
				<th style="width: 10%">Batch NO</th>
				<th style="width: 6%">Expiry</th>
				<th style="width: 4%">Total Amount</th>
				<th style="width: 8%">Description</th>
				<th style="width: 0.01%;"><fmt:message
						key="accounts.common.label.options" /></th>
			</tr>
		</thead>
		<tbody class="tabMrp">
			<c:forEach var="detail" items="${PURCHASE_DETAILINFO}"
				varStatus="status2">
				<tr class="rowidmrp" id="fieldrowmrp_${status2.index+1}">
					<td id="lineIdmrp_${status2.index+1}" style="display: none;">${status2.index+1}</td>
					<td id="itemtypemrp_${status2.index+1}"><c:choose>
							<c:when test="${detail.product.itemType eq 'I'}">
								Inventory
							</c:when>
							<c:when test="${detail.product.itemType eq 'E'}">
								Expense
							</c:when>
							<c:when test="${detail.product.itemType eq 'A'}">
								Asset
							</c:when>
						</c:choose></td>
					<td><span class="float-left width60"
						id="productmrp_${status2.index+1}">${detail.product.code} -
							${detail.product.productName}</span>
						<span class="width10 float-right" id="unitCodemrp_${status2.index+1}"
								style="position: relative;">${detail.product.lookupDetailByProductUnit.accessCode}</span>
						<input type="hidden"
						id="productidmrp_${status2.index+1}"
						value="${detail.product.productId}" /></td>
					<td class="hidemandatorymrp"><span class="float-left width60"
						id="storeNamemrp_${status2.index+1}">${detail.storeName}</span> <input
						type="hidden" name="storeIdmrp" id="storeIdmrp_${status2.index+1}"
						value="${detail.storeId}" /> <input type="hidden"
						name="shelfIdmrp" id="shelfIdmrp_${status2.index+1}"
						value="${detail.shelfId}" />
						<span class="button float-right">
							<a style="cursor: pointer;" id="prodIDStrmrp_${status2.index+1}"
							class="btn ui-state-default ui-corner-all purchase-store-popupmrp width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</td>
					<td id="uommrp_${status2.index+1}" class="uom"
						style="display: none;"></td>
					<td> 
						<select name="packageTypemrp" id="packageTypemrp_${status2.index+1}" class="packageTypemrp">
							<option value="-1">Select</option> 
							<c:forEach var="packageType" items="${detail.productPackageVOs}">
								<c:choose>
									<c:when test="${packageType.productPackageId gt 0}">
										<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
										 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
										 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
										 	</c:forEach> 
										 </optgroup>
									</c:when> 
								</c:choose> 
							</c:forEach>
						</select>  
						<input type="hidden" name="temppackageTypemrp" id="temppackageTypemrp_${status2.index+1}" value="${detail.packageDetailId}"/>
					</td> 
					<td><input type="text"
						class="width96 packageUnitmrp validate[optional,custom[number]]"
							id="packageUnitmrp_${status2.index+1}" value="${detail.packageUnit}" /> 
					</td> 
					<td>  
						<input type="hidden" class="width96 quantitymrp validate[optional,custom[number]]"
							id="quantitymrp_${status2.index+1}" value="${detail.baseQuantity}" />
						<span id="baseUnitConversionmrp_${status2.index+1}" class="width25" style="display: none;">${detail.productPackageDetailVO.conversionUnitName}</span>
						<span id="baseDisplayQtymrp_${status2.index+1}">${detail.baseQuantity}</span> 
					</td>
					<td><input type="text"
						class="width96 unitratemrp validate[optional,custom[number]]"
						id="unitratemrp_${status2.index+1}" value="${detail.unitRate}"
						style="text-align: right;" />
						<input type="text" style="display: none;" name="totalAmountHmrp" readonly="readonly" id="totalAmountHmrp_${status2.index+1}" 
							class="totalAmountHmrp" value="${detail.unitRate * detail.packageUnit}"/> 
						<input type="hidden" id="basePurchasePricemrp_${status2.index+1}"/>
					</td>
					<td><input type="text" class="width96 batchNumbermrp" value="${detail.batchNumber}"
						id="batchNumbermrp_${status2.index+1}" maxlength="40"/>
					</td>
					<td><input type="text" readonly="readonly"
						class="width96 expiryBatchDatemrp" value="${detail.expiryDate}"
						id="expiryBatchDatemrp_${status2.index+1}" />
					</td>
					<td class="totalamountmrp" id="totalamountmrp_${status2.index+1}"
						style="text-align: right;"> 
					</td>
					<td><input type="text" name="linedescriptionmrp"
						id="linedescriptionmrp_${status2.index+1}"
						value="${detail.description}" />
					</td>
					<td style="display: none;"><input type="hidden"
						id="purchaseLineIdmrp_${status2.index+1}"
						value="${detail.purchaseDetailId}" /> <input type="hidden"
						id="requisitionLineIdmrp_${status2.index+1}"
						value="${detail.requisitionDetailId}" /></td>
					<td style="width: 0.01%;" class="opn_td"
						id="option_${status2.index+1}"><a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowmrp"
						id="DeleteImagemrp_${status2.index+1}" style="cursor: pointer;"
						title="Delete Record"> <span
							class="ui-icon ui-icon-circle-close"></span> </a> <a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
						id="WorkingImageMrp_${status2.index+1}" style="display: none;"
						title="Working"> <span class="processing"></span> </a>
					</td>
				</tr>
			</c:forEach>

			<c:forEach var="i" begin="${fn:length(PURCHASE_DETAILINFO)+1}"
				end="${fn:length(PURCHASE_DETAILINFO)+2}" step="1"
				varStatus="status3">
				<tr class="rowidmrp" id="fieldrowmrp_${i}">
					<td id="lineIdmrp_${i}" style="display: none;">${i}</td>
					<td><select name="itemnaturemrp" class="width98 itemnatureMrp"
						id="itemnaturemrp_${i}">
							<option value="A">Asset</option>
							<option value="E">Expense</option>
							<option value="I" selected="selected">Inventory</option>
					</select> <input type="hidden" id="tempitemnature_${i}" name="itemnature" />
					</td>
					<td><span class="float-left width60" id="productmrp_${i}"></span>
						<span class="width10 float-right" id="unitCodemrp_${i}"
							style="position: relative;"></span>
						<span class="button float-right prod" style="height: 16px;">
							<a style="cursor: pointer; cursor: pointer; height: 100%;"
							id="productlinemrp_${i}"
							class="btn ui-state-default ui-corner-all purchaseproduct-common-popupmrp width100 float-right">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
						type="hidden" id="productidmrp_${i}" />
					</td>
					<td class="hidemandatorymrp"><span class="float-left width60"
						id="storeNamemrp_${i}"></span> <input type="hidden" name="storeId"
						id="storeIdmrp_${i}" /> <input type="hidden" name="shelfId"
						id="shelfIdmrp_${i}" /> <span class="button float-right">
							<a style="cursor: pointer;" id="prodIDStrmrp_${i}"
							class="btn ui-state-default ui-corner-all purchase-store-popupmrp width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
					<td id="uommrp_${i}" class="uom" style="display: none;"></td>
					<td>
						<select name="packageTypemrp" id="packageTypemrp_${i}" class="packageTypemrp">
							<option value="-1">Select</option>
						</select> 
						<input type="hidden" name="temppackageTypemrp" id="temppackageTypemrp_${i}"/> 
					</td>
					<td><input type="text"
						class="width96 packageUnitmrp validate[optional,custom[number]]" id="packageUnitmrp_${i}"/> 
					</td>  
					<td><input type="hidden"
						class="width96 quantitymrp validate[optional,custom[number]]"
						id="quantitymrp_${i}" />
						<span id="baseUnitConversionmrp_${i}" class="width25" style="display: none;"></span>
						<span id="baseDisplayQtymrp_${i}"></span>
					</td>
					<td><input type="text"
						class="width96 unitratemrp validate[optional,custom[number]]"
						id="unitratemrp_${i}" style="text-align: right;" />
						<input type="text" style="display: none;" name="totalAmountHmrp" readonly="readonly" id="totalAmountHmrp_${i}" 
							class="totalAmountHmrp"/>
						<input type="hidden" id="basePurchasePricemrp_${i}"/>
					</td>
					<td><input type="text" class="width96 batchNumbermrp"
						id="batchNumbermrp_${i}" maxlength="40"/>
					</td>
					<td><input type="text" readonly="readonly"
						class="width96 expiryBatchDatemrp" id="expiryBatchDatemrp_${i}" />
					</td>
					<td class="totalamountmrp" id="totalamountmrp_${i}"
						style="text-align: right;"></td>
					<td><input type="text" name="linedescriptionmrp"
						id="linedescriptionmrp_${i}" />
					</td>
					<td style="display: none;"><input type="hidden"
						id="purchaseLineIdmrp_${i}" /> <input type="hidden"
						id="requisitionLineIdmrp_${i}" /></td>
					<td style="width: 0.01%;" class="opn_td" id="option_${i}"><a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowmrp"
						id="DeleteImageMrp_${i}" style="display: none; cursor: pointer;"
						title="Delete Record"> <span
							class="ui-icon ui-icon-circle-close"></span> </a> <a
						class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
						id="WorkingImageMrp_${i}" style="display: none;" title="Working">
							<span class="processing"></span> </a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="clearfix"></div>
	<div style="position: relative;"
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div class="portlet-header ui-widget-header float-left confirmBacthPurchase"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div class="portlet-header ui-widget-header float-left closeBacthPurchase"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$jquery(".unitratemrp,.totalAmountHmrp").number(true,3); 
		if (Number($('#supplierId').val()) > 0)
			$('.hidemandatorymrp').hide();
		$('.expiryBatchDatemrp').datepick();
		setTimeout(function() { 
			manupulateMRPLastRow();
	    }, 100);  
		
		$('.rowidmrp').each(function(){
			var rowId = $(this).attr('id').split('_')[1]; 
 			$('#packageTypemrp_'+rowId).val($('#temppackageTypemrp_'+rowId).val());
 			$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
 		});
	});

	function manupulateMRPLastRow() {
		var hiddenSize = 0;
		$($(".tabMrp>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tabMrp>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize); 
		$('.tabMrp>tr:last').removeAttr('id');
		$('.tabMrp>tr:last').removeClass('rowidmrp').addClass('lastrowmrp');
		$($('.tabMrp>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tabMrp>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}
</script>