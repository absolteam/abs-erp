<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accountTypeId=0;
$(function(){
	$("#paymentterm_details").validationEngine({  
		 validationEventTriggers:"keyup blur",  
	     success :  false,
	     failure : function() {
	     	callFailFunction();
	     }
	});
	$('#baseDay').val($('#tempbaseDay').val());
	//Combination pop-up config
    $('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0);   
	      $('.ui-dialog-titlebar').remove();   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
		});

     $('.termdiscard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpaymentterm_detaillist.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#codecombination-popup').dialog('destroy');		
					$('#codecombination-popup').remove();  
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
	 });

     $('.termsave').click(function(){ 
		 var paymentTermId=Number($('#paymentTermId').val());
		 var name=$('#termName').val();
		 var noOfDays=$('#noOfDays').val();
		 var discountDays=$('#discountDays').val();
		 var discount=$('#discount').val();
		 var discountReceived=Number($('#discountCode').val());
		 var baseDay=$('#baseDay').val();
		 if($("#paymentterm_details").validationEngine({returnIsValid:true})){   
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/savepaymentterm.action", 
				 	async: false,
				 	data:{	paymentTermId:paymentTermId, name:name, noOfDays:noOfDays, discountDays:discountDays, discount:discount,
				 			discountReceived:discountReceived, baseDay:baseDay},
				    dataType: "html",
				    cache: false,
					success:function(result){
						$(".tempresult").html(result); 
						var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getpaymentterm_detaillist.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#codecombination-popup').dialog('destroy');		
										$('#codecombination-popup').remove();  
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html(message).slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Sorry!Internal error.").slideDown(1000);
					}
			 });
		 }
		 else{
			 return false;}
	 });
});
function setCombination(combinationTreeId,combinationTree){
	$('#discountCode').val(combinationTreeId);
	$('#discountReceived').val(combinationTree);
} 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>payment term information</div>
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>	
		  <form name="paymentterm_details" id="paymentterm_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset> 
							<div>
								<label class="width30" for="baseDay">Base Day<span style="color: red;">*</span></label>
								<select name="baseDay" id="baseDay" class="width50 validate[required]">
									<option value="">Select</option>
										<c:forEach var="entry" items="${PAYMENT_TERM_BASE_DATE}">
										<option value="${entry.key}">${entry.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempbaseDay" value="${PAYMENT_TERM_INFO.baseDay}"/>
							</div> 
							<div>
								<label class="width30" for="discount">Discount<span style="color: red;">*</span></label>
								<input type="text" name="discount" id="discount" class="width50 validate[required,custom[onlyFloat,percentRange]]" value="${PAYMENT_TERM_INFO.discount}"/>
							</div> 
							<div>
								<label class="width30" for="discountReceived">Discount Received<span style="color: red;">*</span></label>
								<input type="hidden" id="discountCode" name="discountCode" value="${PAYMENT_TERM_INFO.combination.combinationId}"/>
								<c:choose>
									<c:when test="${PAYMENT_TERM_INFO.combination.combinationId > 0}">
										<input type="text" readonly="readonly" name="discountReceived" id="discountReceived" class="width50 validate[required]" 
											value="${PAYMENT_TERM_INFO.combination.accountByNaturalAccountId.code}[ ${PAYMENT_TERM_INFO.combination.accountByNaturalAccountId.account}]"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="discountReceived" id="discountReceived" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
								<span class="button">
									<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset> 
							<div>
								<label class="width30" for="termName">Term Name<span style="color: red;">*</span></label> 
								<input type="hidden" id="paymentTermId" name="paymentTermId" value="${PAYMENT_TERM_INFO.paymentTermId}"/>
								<input type="text" name="termName" id="termName" maxlength="15" class="width50 validate[required]" value="${PAYMENT_TERM_INFO.name}"/>
							</div> 
							<div>
								<label class="width30" for="noOfDays">No.Of Days<span style="color: red;">*</span></label> 
								<input type="text" name="noOfDays" id="noOfDays" maxlength="3" class="width50 validate[required,custom[onlyNumber]]" value="${PAYMENT_TERM_INFO.noOfDays}"/>
							</div> 
							<div>
								<label class="width30" for="discountDays">Discount Days<span style="color: red;">*</span></label> 
								<input type="text" name="discountDays" id="discountDays" maxlength="3" class="width50 validate[required,custom[onlyNumber]]" value="${PAYMENT_TERM_INFO.discountDays}"/>
							</div> 
						</fieldset>
					</div> 
			</div>  
		</div>    
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right termdiscard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right termsave" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
  </form>
  </div> 
</div>