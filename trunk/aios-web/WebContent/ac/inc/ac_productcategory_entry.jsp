<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var fileUpload=null;
$(function(){
	
	$('#specialPos').change(function(){
		var specialPos = $(this).attr('checked');
		if(specialPos ==  true){
			$('.specialPOSDiv').show();
		}else{
			$('.specialPOSDiv').hide();
		}
	}); 
	
	$('.product_category_save').click(function(){   
		 if($jquery("#productcategory_validation").validationEngine('validate')){ 
			var categoryName = $('#categoryName').val();
			var parentCategory = Number($('#parentCategory').val());  
			var description = $('#description').val();  
			var specialPos =  $('#specialPos').attr('checked');
			var activeStatus =  $('#activeStatus').attr('checked');
			var publicName = $('#publicName').val();  
			var kitchenPrint =  $('#kitchenPrint').attr('checked');
			var barPrint = $('#barPrint').attr('checked');
			var productCategoryId = Number($('#productCategoryId').val());  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_product_category.action", 
		     	async: false,
		     	data:{
		     			productCategoryId : productCategoryId, categoryName: categoryName, parentCategory: parentCategory, publicName: publicName,
		     			specialPos: specialPos, kitchenPrint: kitchenPrint, barPrint: barPrint, description: description, activeStatus: activeStatus
		     		 },
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
						if(productCategoryId>0)
							callScreenDiscard("Record updated.");
						else
							callScreenDiscard("Record created.");
					}
				}
			});
							} else
								return false;
						});

	$('.product_category_discard').click(function(){  
		 callScreenDiscard("");
	 });
	 
	 if(Number($('#productCategoryId').val()> 0)){    
		$('#parentCategory').val($('#tempparentCategory').val());
		populateUploadsPane("img","productCategoryImages","ProductCategory", $('#productCategoryId').val());  
	 } 
	 
	 $('#productcategory_document_information').click(function(){
		 var productCategoryId=Number($('#productCategoryId').val());
		 if(productCategoryId>0){
				AIOS_Uploader.openFileUploader("img","productCategoryImages", "ProductCategory", productCategoryId,"ProductCategoryImages");
			}else{
				AIOS_Uploader.openFileUploader("img","productCategoryImages","ProductCategory","-1","ProductCategoryImages");
			} 
		});
});
function callScreenDiscard(statusMessage){
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/show_product_category.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$("#main-wrapper").html(result);
				if (statusMessage != null && statusMessage != '') {
					$('#success_message').hide().html(statusMessage).slideDown(
							1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			category
		</div> 
		<form id="productcategory_validation" style="position: relative;">
			<div class="width100" id="hrm">
				<input type="hidden" id="productCategoryId" name="productCategoryId" value="${PRODUCT_CATEGORY.productCategoryId}"/>
				<div class="edit-result">
					<div id="hrm" class="width48 float-right">
						<fieldset style="min-height: 175px;">
							<div style="height: 160px;">
								<div id="productcategory_document_information"
									style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
								</div>
								<div
									style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
									<span id="productCategoryImages"></span>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 185px;">
							<div>
								<label for="categoryName">Category Name<span
									style="color: red;">*</span> </label> <input type="text"
									name="categoryName" id="categoryName"
									value="${PRODUCT_CATEGORY.categoryName}"
									class="width50 validate[required]" />
							</div>
							<div>
								<label for="parentCategory">Parent Category </label> <select
									name="parentCategory" id="parentCategory" class="width51">
									<option value="">Select</option>
									<c:forEach var="parentCategory" items="${PARENT_CATEGORY}">
										<option value="${parentCategory.productCategoryId}">${parentCategory.categoryName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempparentCategory"
									value="${PRODUCT_CATEGORY.parentCategoryId}" />
							</div>
							<div>
								<label for="activeStatus">Status </label> 
								<c:choose>
									<c:when test="${PRODUCT_CATEGORY.status eq true}">
										<input type="checkbox" name="activeStatus" id="activeStatus" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="activeStatus" id="activeStatus"/>
									</c:otherwise>
								</c:choose> 
							</div>
							<div>
								<label for="specialPos">Special POS </label> 
								<c:choose>
									<c:when test="${PRODUCT_CATEGORY.specialPos eq true}">
										<input type="checkbox" name="specialPos" id="specialPos" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="specialPos" id="specialPos"/>
									</c:otherwise>
								</c:choose> 
							</div>
							<c:choose>
								<c:when test="${PRODUCT_CATEGORY.specialPos eq true}"> 
									<div class="specialPOSDiv" style="margin-bottom: 3px;">
										<label for="kitchenPrint">Printer<span
											style="color: red;">*</span>
										</label> 
											<label for="kitchenPrint" style="float: none;">Kitchen</label>
											<c:choose>
												<c:when test="${PRODUCT_CATEGORY.kitchenPrint ne null && PRODUCT_CATEGORY.kitchenPrint eq true}">
													<input type="radio" name="radioField" id="kitchenPrint" checked="checked"/>
												</c:when>
												<c:otherwise>
													<input type="radio" name="radioField" id="kitchenPrint"/>
												</c:otherwise>
											</c:choose>
											<label for="barPrint" style="float: none;">Bar Print</label>
											<c:choose>
												<c:when test="${PRODUCT_CATEGORY.barPrint ne null && PRODUCT_CATEGORY.barPrint eq true}">
													<input type="radio" name="radioField" id="barPrint" checked="checked"/>
												</c:when>
												<c:otherwise>
													<input type="radio" name="radioField" id="barPrint"/>
												</c:otherwise>
											</c:choose>
											
									</div>
								</c:when>
								<c:otherwise> 
									<div style="display: none;margin-bottom:3px;" class="specialPOSDiv">
										<label>Printer<span
											style="color: red;">*</span>
										</label> <label for="kitchenPrint" style="float: none;">Kitchen</label><input type="radio" name="radioField" id="kitchenPrint"
											value=""/>
											 <label for="barPrint" style="float: none;">Bar</label> <input type="radio" name="radioField" id="barPrint"
											value=""/>
									</div>
								</c:otherwise>
							</c:choose> 
							<div>
								<label for="publicName">Public Name
								</label> <input type="text" name="publicName" id="publicName"
									value="${PRODUCT_CATEGORY.publicName}" class="width50" />
							</div>
							
							<div>
								<label for="description">Description</label>
								<textarea class="width50" id="description">${PRODUCT_CATEGORY.description}</textarea>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div
						class="portlet-header ui-widget-header float-right product_category_discard">
						<fmt:message key="accounts.common.button.cancel" />
					</div>
					<div
						class="portlet-header ui-widget-header float-right product_category_save">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</div>
			</div>
		</form>
	</div>
</div>