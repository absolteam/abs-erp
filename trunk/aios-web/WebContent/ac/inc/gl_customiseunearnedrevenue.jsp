<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
</style>
<script type="text/javascript">
var accountTypeId=0;
var slidetab;
var tempid;
$(function(){
	$jquery("#coavalidate-form").validationEngine('attach'); 
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0); 
	      $('.ui-dialog-titlebar').remove();   
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
			return false;
	});
	
	$('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: false
	}); 
	 
	 $('.discard').click(function(){
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);
			},
			error: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		});
	 });
	 
	 $(document).keyup(function(e) { 
		 var id=$('.save').attr('id'); 
		  if (e.keyCode == 27 && id=="add-customise"){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	});  
	 
	 $('.skip').click(function(){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customise_pointofsale_control.action", 
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){ 
					$('#codecombination-popup').dialog('destroy');		
			  		$('#codecombination-popup').remove(); 
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
	  });
	 
	 $('.save').click(function(){ 
		 $('.error').hide();
			var unearnedRevenueId=$('#unearnedRevenueId').val();
			var pdcReceivedId=$('#pdcReceivedId').val();
			var pdcIssuedId= $('#pdcIssuedId').val();
		 if(unearnedRevenueId==null || unearnedRevenueId=='' || unearnedRevenueId==0){
			$('.page-error').hide().html("Choose Unearned Revenue").slideDown(1000);
			return false;
		}
		if(pdcReceivedId==null || pdcReceivedId=='' || pdcReceivedId==0){
			$('.page-error').hide().html("Choose PDC Received").slideDown(1000);
			return false;
		}
		if(pdcIssuedId==null || pdcIssuedId=='' || pdcIssuedId==0){
			$('.page-error').hide().html("Choose PDC Issued").slideDown(1000);
			return false;
		}
	
		 if($jquery("#coavalidate-form").validationEngine('validate')){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/create_customise_unearnedrevenue.action",
				data:{unearnedRevenue: unearnedRevenueId, pdcReceived: pdcReceivedId, pdcIssued: pdcIssuedId},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresult").html(result);
					var message=$('#returnMsg').html(); 
					if(message.trim()=="Success"){  
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/customise_pointofsale_control.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){  
								$('#codecombination-popup').dialog('destroy');		
						  		$('#codecombination-popup').remove(); 
						  		$("#DOMWindow").html(result);   
								$('#success_message').hide().html(message).slideDown(1000);
								$.scrollTo(0,300);
							} 		
						});  
					 }
					else{
						$('.page-error').hide().html($('#returnMsg').html()).slideDown(1000);
						return false;
					}
				}
			 });
		}
		else{
			return false;
		}
	 });
});
 
function setCombination(combinationTreeId,combinationTree){ 
	tempid=$(tempid).attr('id');  
	if(typeof tempid!='undefined'&& tempid=="unearnedRevenueCode"){
		$('#unearnedRevenueId').val(combinationTreeId); 
		$('#unearnedRevenue').val(combinationTree);
	}
	else if(typeof tempid!='undefined'&& tempid=="pdcReceivedCode"){
		$('#pdcReceivedId').val(combinationTreeId); 
		$('#pdcReceived').val(combinationTree);
	} 
	else if(typeof tempid!='undefined'&& tempid=="pdcIssuedCode"){
		$('#pdcIssuedId').val(combinationTreeId); 
		$('#pdcIssued').val(combinationTree);
	}  
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create Unearned Account</div>
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:80% !important; display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;"> 
				<div class="width60" id="hrm">
					<fieldset>
						<legend>Unearned Account Details</legend>
						<div>
							<label class="width30">Unearned Revenue<span style="color: red;">*</span></label>
							<input type="hidden" name="unearnedRevenueId" id="unearnedRevenueId" value="${UNEARNED_REVENUE.combinationId}"/> 
							<c:choose>
								<c:when test="${UNEARNED_REVENUE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="unearnedRevenue" id="unearnedRevenue" value="${UNEARNED_REVENUE.accountByCostcenterAccountId.account}.${UNEARNED_REVENUE.accountByNaturalAccountId.account}.${UNEARNED_REVENUE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="unearnedRevenue" id="unearnedRevenue" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose> 
 							<span class="button" id="unearnedRevenueCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">PDC Received<span style="color: red;">*</span></label>
							<input type="hidden" name="pdcReceivedId" id="pdcReceivedId" value="${PDC_RECEIVED.combinationId}"/> 
							<c:choose>
								<c:when test="${PDC_RECEIVED.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="pdcReceived" id="pdcReceived" value="${PDC_RECEIVED.accountByCostcenterAccountId.account}.${PDC_RECEIVED.accountByNaturalAccountId.account}.${PDC_RECEIVED.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="pdcReceived" id="pdcReceived" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose> 
							<span class="button" id="pdcReceivedCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">PDC Issued<span style="color: red;">*</span></label>
							<input type="hidden" name="pdcIssuedId" id="pdcIssuedId" value="${PDC_ISSUED.combinationId}"/> 
							<c:choose>
								<c:when test="${PDC_ISSUED.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="pdcIssued" id="pdcIssued" value="${PDC_ISSUED.accountByCostcenterAccountId.account}.${PDC_ISSUED.accountByNaturalAccountId.account}.${PDC_ISSUED.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 validate[required]">
								</c:when>
								<c:otherwise>
									<input type="text" name="pdcIssued" id="pdcIssued" readonly="readonly" class="width48 validate[required]">
								</c:otherwise>
							</c:choose>  
							<span class="button" id="pdcIssuedCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="unearned_revenue" style="font-weight:bold;"></div> 
				<div class="pdc_received" style="font-weight:bold;"></div> 
				<div class="pdc_issued" style="font-weight:bold;"></div>
				<div class="clearfix"></div>  
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
					<div class="portlet-header ui-widget-header float-right save" id="add"><fmt:message key="accounts.common.button.save"/></div>
				</div> 
				<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
					<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
				</div>
			</form>
		</div>	
	</div>		
	<div style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div>
</div>