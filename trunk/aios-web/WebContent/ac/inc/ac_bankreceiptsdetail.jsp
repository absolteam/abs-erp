<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
$(function(){
	$('#checkbankreceiptAll').change(function(){
		var checked = $(this).attr('checked');
		if(checked ==  true){
		  	$('.checkbankreceipt').attr('checked', true);
		}else{
			$('.checkbankreceipt').attr('checked', false);
		}
		calculateDepositAmount();
		return false;
	});
	
	$('.checkbankreceipt').change(function(){
		var checked = $(this).attr('checked');
		if(checked ==  true){
			$('#checkbankreceiptAll').attr('checked', true);
			 $('.rowid').each(function(){   
	  		 	var rowId = getRowId($(this).attr('id')); 
	  		 	var checkedReceipt = $('#checkbankreceipt_'+rowId).attr('checked');
	  		 	if(checkedReceipt == false){
	  		 		$('#checkbankreceiptAll').attr('checked', false);
	  		 		return false;
	  		 	} 
			 }); 
		}else{
			$('#checkbankreceiptAll').attr('checked', false);
		} 
		calculateDepositAmount();
		return false;
	});
	$jquery('.totalDepositAmountH').number(true, 2);
});
function calculateDepositAmount(){
 	var depositAmount = Number(0);
 	$('.rowid').each(function(){   
 		var rowId=getChildRowId($(this).attr('id')); 
 		var checkbankreceipt = $('#checkbankreceipt_'+rowId).attr('checked');
  		if(checkbankreceipt == true){
  			depositAmount += Number($('#amountval_'+rowId).val());
 		}
	});
 	$jquery('.totalDepositAmountH').val(depositAmount);
 	$('#totalDepositAmount').text($('.totalDepositAmountH').val());
}
function getChildRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="hrm" class="hastable width100 float-left"
	style="margin-top: 10px;">
	<fieldset>
		<legend>
			Deposit Details<span class="mandatory">*</span>
		</legend>
		<c:choose>
			<c:when
				test="${BANK_RECEIPTS_DETAIL ne null && BANK_RECEIPTS_DETAIL ne '' && fn:length(BANK_RECEIPTS_DETAIL)>0 }">
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th>
								Check All
								<input type="checkbox" id="checkbankreceiptAll" 
										name="checkbankreceiptAll" class="checkbankreceiptAll" 
										style="float: left; position: relative; top: -3px; left: 20px;"/>
							</th>
							<th>Receipt Reference</th>
							<th>Payment Mode</th>
							<th>Cheque No</th>
							<th>Cheque Date</th> 
							<th>Amount</th>
							<th>Description</th> 
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach var="RECEIPTS_DETAIL" items="${BANK_RECEIPTS_DETAIL}"
							varStatus="statusReceipt">
							<tr class="rowid" id="fieldrow_${statusReceipt.index+1}"> 
								<td id="lineId_${statusReceipt.index+1}" style="display: none;">${statusReceipt.index+1}</td> 
								<td>
									<input type="checkbox" id="checkbankreceipt_${statusReceipt.index+1}" 
										name="checkbankreceipt" class="checkbankreceipt"/>
								</td> 
								<td>${RECEIPTS_DETAIL.referenceNo}</td>
								<td>${RECEIPTS_DETAIL.paymentModeName}</td>
								<td>${RECEIPTS_DETAIL.bankRefNo}</td>
								<td><c:if
										test="${RECEIPTS_DETAIL.chequeDate ne null &&  RECEIPTS_DETAIL.chequeDate ne ''}">
										<c:set var="chequeDate" value="${RECEIPTS_DETAIL.chequeDate}" />
										<%=DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString())%>
									</c:if></td> 
								<td style="text-align: right;"
									id="amount_${statusReceipt.index+1}"><c:set var="amount"
										value="${RECEIPTS_DETAIL.amount}" /> <%=AIOSCommons.formatAmount(pageContext.getAttribute("amount"))%>
										<input type="hidden" id="amountval_${statusReceipt.index+1}" value="${RECEIPTS_DETAIL.amount}"/>
								</td>
								<td><input type="text"
									name="description_${statusReceipt.index+1}"
									id="description_${statusReceipt.index+1}"
									class="description width80"></td>
								<td style="width: 0.01%; display: none;" class="opn_td"
									id="option_${statusReceipt.index+1}"><a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
									id="DeleteImage_${statusReceipt.index+1}"
									style="cursor: pointer;" title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <input
									type="hidden"
									id="bankReceiptsDetailId_${statusReceipt.index+1}"
									value="${RECEIPTS_DETAIL.bankReceiptsDetailId}" />
									<input type="hidden" id="bankReceiptsUseCase_${statusReceipt.index+1}"
										value="${RECEIPTS_DETAIL.useCase}" />
									<input type="hidden" id="bankReceiptsRecordId_${statusReceipt.index+1}"
										value="${RECEIPTS_DETAIL.recordId}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="width40 float-right" style="border: 0px none; margin-top:5px;">
					<tr style="border: 0px none;">
						<td colspan="5" style="border: 0px none; font-weight: bold;" class="width30">Total Deposit</td>
						<td id="totalDepositAmount" class="width98" style="border: 0px none; font-weight: bold; float: right;">0.00</td>
					</tr>
				</table>
				<input type="text" style="display: none;" readonly="readonly" class="totalDepositAmountH"/>
			</c:when>
			<c:otherwise>
				<span style="color: #d71e23; font-size: smaller !important;">No
					Receipt(s) found for this Receipts type.</span>
			</c:otherwise>
		</c:choose>
	</fieldset>
</div>
