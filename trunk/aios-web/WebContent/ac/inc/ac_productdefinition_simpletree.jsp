<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript">
$(function(){
	$("#product-definition-view").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	}); 
});
</script>
<div id="sidetreecontrol"></div>
<ul id="product-definition-view">
	<li>${requestScope.productName} <c:if
			test="${PRODUCT_DEFINITIONS ne null && PRODUCT_DEFINITIONS ne ''}">
			<ul>
				<c:forEach items="${PRODUCT_DEFINITIONS}" var="productDefinition">
					<li>${productDefinition.definitionLabel}<c:if
							test="${productDefinition.productDefinitionVOs ne null && productDefinition.productDefinitionVOs ne ''}">
							<ul>
								<c:forEach var="mapProduct"
									items="${productDefinition.productDefinitionVOs}">
									<li>
										<c:choose>
											<c:when test="${mapProduct.pricingType eq 'Default'}"> 
												<input type="checkbox" name="productedit" style="display: none;" id="definitionproductedit_${mapProduct.productDefinitionId}"
													 class="definition_product_edit" title="Default Price" checked="checked"/> 
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="productedit" id="definitionproductedit_${mapProduct.productDefinitionId}"
													 class="definition_product_edit" title="Default Price" style="display: none;"/> 
											</c:otherwise>
										</c:choose> 
										${mapProduct.specialProductName}  ${mapProduct.pricingDetail}
									<span style="position: absolute; cursor: pointer;"
											id="definitionproductdelete_${mapProduct.productDefinitionId}"
											class="definition_product_delete float-right"><img
												width="10" height="10"
												src="./images/cancel.png">
										</span>
 									</li>
								</c:forEach>
							</ul>
						</c:if>
						 
					</li>
				</c:forEach>
			</ul>

		</c:if>
	</li>
</ul>