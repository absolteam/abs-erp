<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Inventory Report</title>
	<link href="${pageContext.request.contextPath}/css/report-print.css" rel="stylesheet" type="text/css" media="all" />
</head>

	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span>  
	</div>
	
	<div style="clear: both;"></div>
	<span class="title-heading" style="cursor: pointer;" onclick="window.print();"><span>Print</span></span>
	<div class="width98" ><span class="heading"><span>INVENTORY REPORT</span></span></div>
	<div style="clear: both;"></div> 
	<div class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>PRODUCT CODE</th>
				<th>PRODUCT NAME</th>
				<th>UNIT</th> 
				<th>ITEM TYPE</th> 
				<th>SUB CATEGORY</th> 
				<th>ITEM NATURE</th> 
 			</tr>
			<tbody>   
				<c:forEach items="${PRODUCT_PRINT_LIST}" var="PRODUCT_PRINT" varStatus="status"> 
					<tr> 
						<td>
							 ${PRODUCT_PRINT.code}
						</td>
						<td>
							${PRODUCT_PRINT.productName}
						</td> 
						<td>
							${PRODUCT_PRINT.lookupDetailByProductUnit.displayName}
						</td> 
						<td>
							
							<c:choose>
								<c:when test="${PRODUCT_PRINT.itemType eq 'I'}">
									Inventory
								</c:when>
								<c:when test="${PRODUCT_PRINT.itemType eq 'E'}">
									Expense
								</c:when>
								<c:when test="${PRODUCT_PRINT.itemType eq 'A'}">
									Asset
								</c:when>
							</c:choose>
 						</td>
 						<td>
 							<c:choose>
 								<c:when test="${PRODUCT_PRINT.productCategory ne null && PRODUCT_PRINT.productCategory ne ''}">
 									${PRODUCT_PRINT.productCategory.categoryName}
 								</c:when>
 								<c:otherwise>
 									-N/A-
 								</c:otherwise>
 							</c:choose> 
 						</td>
 						<td>
 							<c:choose>
 								<c:when test="${PRODUCT_PRINT.lookupDetailByItemNature ne null && PRODUCT_PRINT.lookupDetailByItemNature ne ''}">
 									${PRODUCT_PRINT.lookupDetailByItemNature.displayName}
 								</c:when>
 								<c:otherwise>
 									-N/A-
 								</c:otherwise>
 							</c:choose> 
 						</td>
					</tr>
				</c:forEach>  
			</tbody>
		</table>
	</div>  
	</body>
</html> 