<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 1000;
}
</style>
<script type="text/javascript">
var slidetab="";
var adjustmentDetails="";
$(function(){   
	manupulateLastRow();
	 
	 $jquery("#adjustmentValidation").validationEngine('attach');
	 
	$('#adjustmentDate').datepick({ 
	    onClose: function(dates) { 
		    if(dates!='')
			    $('.adjustmentDateformError').remove();
		}, 
	    showTrigger: '#calImg'});  

	 $('.discardadj_popup').live('click',function(){
		$('#common-popup-adjpopup').dialog('destroy');		
		$('#common-popup-adjpopup').remove();  
		$('#codecombination-popup').dialog('destroy');		
		$('#codecombination-popup').remove(); 
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabadj>.rowidadj:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/reconciliation_adjustment_addrowadj.action", 
			 	async: false,
			 	data:{rowid: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabadj tr:last').before(result);
					 if($(".tabadj").height()>255)
						 $(".tabadj").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidadj').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('#adjustment_save').live('click',function(){
		 	adjustmentDetails = "";
 	 		if($jquery("#adjustmentValidation").validationEngine('validate')){	
	 			var adjustmentId = Number($('#adjustmentId').val()); 
	 			var referenceNumber = "";
	 			var adjustmentDate = $('#adjustmentDate').val(); 
	 			var description=$('#descriptionadj').val();  
	 			var bankAccountId =  $('#accountIdadj').val();  
	 			adjustmentDetails = getReconciliationDetails();   
	   			if(adjustmentDetails!=null && adjustmentDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_reconciliation_adjustmentpopup.action", 
	 				 	async: false, 
	 				 	data:{	adjustmentId: adjustmentId, referenceNumber: referenceNumber, bankAccountId: bankAccountId, date: adjustmentDate, 
		 				 		description: description, adjustmentDetail: adjustmentDetails
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){  
	 						 if(response.returnMessage == "SUCCESS"){
	 							$('#common-popup-adjpopup').dialog('destroy');		
								$('#common-popup-adjpopup').remove();  
				  				$('#codecombination-popup').dialog('destroy');		
				  				$('#codecombination-popup').remove(); 
				  				$('#DOMWindow').remove();
				  				$('#DOMWindowOverlay').remove();
	 						 } 
	 						 else{
	 							 $('#page-error-adj').hide().html(message).slideDown(1000);
	 							 $('#page-error-adj').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error-adj').hide().html("Please enter adjustment details.").slideDown(1000);
	 				$('#page-error-adj').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
 	 		return false;
	 	});

	 var getReconciliationDetails = function(){ 
	 		var amountArray = new Array();
	 		var descArray = new Array();
	  		var combinationArray = new Array();
	 		var adjustmentDetailIdArray = new Array();
	 		var adjustmentTypeArray = new Array(); 
	 		var adjustmentDetail = ""; 
	 		$('.rowidadj').each(function(){ 
		 		
	 			 var rowId = getRowId($(this).attr('id'));  
	 			 var combinationId = $('#combinationId_'+rowId).val();   
	 			 var adjustmentType=$('#adjustmentType_'+rowId).val();  
	 			 var amount = $('#amount_'+rowId).val();    
	 			 var lineDescription=$('#linesDescription_'+rowId).val();
	 			 var adjustmentDetailId = Number($('#adjustmentDetailId_'+rowId).val());
	 			 
	 			 if(typeof combinationId != 'undefined' && combinationId!=null && 
	 					combinationId!="" && adjustmentType!='' && amount!=''){
	 				combinationArray.push(combinationId); 
	 				adjustmentTypeArray.push(adjustmentType); 
	 				amountArray.push(amount);
	  				if(lineDescription ==null || lineDescription=="")
	  					descArray.push("##");
	 				else 
	 					descArray.push(lineDescription); 
	  				adjustmentDetailIdArray.push(adjustmentDetailId); 
	 			 } 
	 		});
	  		for(var j=0;j<combinationArray.length;j++){ 
	  			adjustmentDetail += adjustmentTypeArray[j]+"__"+combinationArray[j]+"__"+amountArray[j]+"__"+
	  								descArray[j]+"__"+adjustmentDetailIdArray[j];
	 			if(j==combinationArray.length-1){   
	 			} 
	 			else{
	 				adjustmentDetail += "@#";
	 			}
	 		}  
	 		return adjustmentDetail;
	 	}; 

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidadj').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

   //Combination pop-up config
 	$('.codecombination-popup').live('click',function(){ 
 	    tempid=$(this).parent().get(0);  
 	    $('.ui-dialog-titlebar').remove();   
 	 	$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/combination_treeview.action", 
 		 	async: false, 
 		    dataType: "html",
 		    cache: false,
 			success:function(result){   
 				 $('.codecombination-result').html(result);  
 			},
 			error:function(result){ 
 				 $('.codecombination-result').html(result); 
 			}
 		});  
 		return false;
 	});
 	
 	 $('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: true
 	 });

 	$('.common-popup-adjpopup').live('click',function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getbankaccount_details_adjpopup.action",
										async : false, 
										dataType : "html",
										cache : false,
										success : function(result) {
											$('.common-result-adjpopup').html(result);
										},
										error : function(result) {
											$('.common-result-adjpopup').html(result);
										}
									});
							return false;
						});

		$('#common-popup-adjpopup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			modal : true
		});

		$('.adjustmentType').live('change', function() {
			triggerAddRow(getRowId($(this).attr('id')));
			return false;
		});

		$('.amount').live('change', function() {
			triggerAddRow(getRowId($(this).attr('id')));
			return false;
		}); 
	});
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tabadj>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tabadj>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tabadj>tr:last').removeAttr('id');
		$('.tabadj>tr:last').removeClass('rowidadj').addClass('lastrow');
		$($('.tabadj>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tabadj>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function setCombination(combinationTreeId, combinationTree) {
		var idVals = $(tempid).attr('id').split("_");
		var rowids = Number(idVals[1]);
		$('#combinationId_' + rowids).val(combinationTreeId);
		$('#codeCombination_' + rowids).val(combinationTree);
		$('#codeCombination_' + rowids).val().replace(/[\s\n\r]+/g, ' ').trim();
		triggerAddRow(rowids);
	}
	function triggerAddRow(rowId) {
		var combinationId = $('#combinationId_' + rowId).val();
		var adjustmentType = $('#adjustmentType_' + rowId).val();
		var amount = $('#amount_' + rowId).val();
		var nexttab = $('#fieldrowadj_' + rowId).next();
		
		if (combinationId != null && combinationId != ""
				&& adjustmentType != null && adjustmentType != ""
				&& amount != null && amount != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> 
			Reconciliation Adjustment
		</div> 
		<form name="adjustmentValidation" id="adjustmentValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error-adj"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div> 
				<input type="hidden" id="adjustmentId" name="adjustmentId" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 85px;"> 
							<div>
								<label class="width20"><fmt:message
										key="accounts.jv.label.description" />
								</label>
								<textarea rows="2" cols="4" id="descriptionadj" name="descriptionadj" class="width60"></textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 85px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span>
								</label>  
									<input type="text" readonly="readonly" class="validate[required] width50"
										value="${requestScope.referenceNumber}"/>
							</div>
 							<div>
								<label class="width30"> Date<span style="color: red;">*</span>
								</label>
								<c:choose>
									<c:when
										test="${ADJUSTMENT_INFO.date ne null && ADJUSTMENT_INFO.date ne ''}">
										<c:set var="date" value="${ADJUSTMENT_INFO.date}" />
										<input name="adjustmentDate" type="text" readonly="readonly"
											id="adjustmentDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("date").toString())%>"
											class="adjustmentDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="adjustmentDate" type="text" readonly="readonly"
											id="adjustmentDate"
											class="adjustmentDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Bank Account<span
									style="color: red;">*</span>
								</label>
								<input type="text" readonly="readonly"
											name="bankAccountadjNumber" id="accountadjNumber"
											class="width50 validate[required]" />
								<span class="button"
									style="position: absolute; margin-top: 8px; float: right;">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all common-popup-adjpopup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="bankAccountId" id="accountIdadj"/>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>Adjustment Detail<span
									class="mandatory">*</span></legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 1%"><fmt:message
												key="accounts.jv.label.jvlinno" />
										</th>
										<th style="width: 3%">Adjustment Type</th>
										<th style="width: 6%"><fmt:message
												key="accounts.jv.label.jvcc" />
										</th>
										<th style="width: 2%"><fmt:message
												key="accounts.jv.label.amount" />
										</th>
										<th style="width: 5%"><fmt:message
												key="accounts.journal.label.desc" />
										</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tabadj"> 
									<c:forEach var="i"
										begin="${1}"
										end="${2}"
										step="1" varStatus="status"> 
										<tr class="rowidadj" id="fieldrowadj_${i}">
											<td id="lineId_${i}">${i}</td>
											<td><select name="adjustmentType"
												id="adjustmentType_${i}" class="width98 adjustmentType">
													<option value="">Select</option>
													<c:forEach var="ADJUST_TYPE" items="${ADJUSTMENT_TYPES}">
														<option value="${ADJUST_TYPE.key}">${ADJUST_TYPE.value}</option>
													</c:forEach>
											</select></td>
											<td><input type="hidden" name="combinationId_${i}"
												id="combinationId_${i}" /> <input type="text"
												name="codeCombination_${i}" readonly="readonly"
												id="codeCombination_${i}" class="codeComb width80">
												<span class="button" id="codeID_${i}"> <a
													style="cursor: pointer;"
													class="btn ui-state-default ui-corner-all codecombination-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
											<td style=""><input type="text" name="amount"
												id="amount_${i}" style="text-align: right;"
												class="amount width98 right-align validate[optional,custom[number]]"></td>
											<td><input type="text" name="linesDescription"
												id="linesDescription_${i}" class="width98" maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="adjustmentDetailId"
												id="adjustmentDetailId_${i}" /></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discardadj_popup"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right adjustment_save"
					id="adjustment_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="common-popup-adjpopup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result-adjpopup width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>