<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var tempid="";
var slidetab="";
var requestDetails="";  
var accessCode = "";
var sectionRowId = 0;
$(function(){
	 
	$jquery("#requestvalidation").validationEngine('attach'); 
	$jquery(".amount,.totalInvoiceAmountH").number(true, 2); 
	
	manupulateLastRow(); 

	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		var rowId=getRowId($(this).attr('id')); 
	   		$('#lineId_'+rowId).html(i);
			i=i+1; 
		 });    
	 });
	
	$('#personName').change(function(){
		$('#personId').val('');
	});

	$('#request_discard').click(function(){ 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_all_paymentrequest.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
			}
		 });
	 });
	
	 $('.common-popup-person').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data : {personTypes: "1"}, 
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);   
					$('#common-popup').dialog('open'); 
					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});
			return false;   
		});

	$('#request_save').click(function(){ 
		 var paymentRequestId = Number($('#paymentRequestId').val());  
		 var requestDate = $('#requestDate').val(); 
		 var description = $("#description").val(); 
		 var status = $('#status').val();
		 var paymentReqNo = $('#paymentRequestNo').val();
		 var personId = Number($('#personId').val());
		 var personName = $('#personName').val();
		 requestDetails = getRequestDetails();  
		 if($jquery("#requestvalidation").validationEngine('validate')){
			 if(requestDetails!=null && requestDetails!=""){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/save_paymentrequest.action", 
					 	async: false,
					 	data:{ paymentRequestId: paymentRequestId, requestDate: requestDate, status: status, description: description,
					 		   paymentRequestNo: paymentReqNo, requestDetail: requestDetails, personId: personId, personName: personName
						 	 },
					    dataType: "html",
					    cache: false,
						success:function(result){
							$(".tempresult").html(result); 
							var message=$.trim($('.tempresult').html());  
							 if(message=="SUCCESS"){
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/show_all_paymentrequest.action", 
									 	async: false,
									    dataType: "html",
									    cache: false,
										success:function(result){ 
											$("#main-wrapper").html(result); 
										}
								 });
								 if(paymentRequestId>0)
								 	$('#success_message').hide().html("Record updated.").slideDown(1000); 
								 else
									 $('#success_message').hide().html("Record created.").slideDown(1000); 
								 $('#success_message').delay(2000).slideUp();
							 }
							 else{
								 $('#page-error').hide().html(message).slideDown(1000);
								 $('#page-error').delay(2000).slideUp();
								 return false;
							 }
						},
						error:function(result){  
							$('#page-error').hide().html("Internal error.").slideDown(1000);
							$('#page-error').delay(2000).slideUp();
						}
				 });
			}
			 else{
				 $('#page-error').hide().html("Please enter request details.").slideDown(1000);
				 $('#page-error').delay(2000).slideUp();
				 return false;
			}
		 }else{return false;}
	 });


	var getRequestDetails = function(){
		requestDetails = "";
		var references=new Array(); 
		var paymodes=new Array();
		var amounts=new Array();
		var lineid=new Array();
		var lineDescriptions=new Array(); 
		$('.rowid').each(function(){ 
 			var rowId=getRowId($(this).attr('id'));  
			var reference=$('#reference_'+rowId).val(); 
			var paymode=$('#paymode_'+rowId).val();
			var amount=Number($jquery('#amount_'+rowId).val()); 
			var linedescription=$('#description_'+rowId).val();
			var detailId = Number($('#paymentRequestId_'+rowId).val());
			if(typeof reference != 'undefined' && reference!=null && reference!=""){
				references.push(reference); 
				paymodes.push(paymode);
				amounts.push(amount);
				if(linedescription!=null && linedescription!="")
					lineDescriptions.push(linedescription);
				else
					lineDescriptions.push("##");
				lineid.push(detailId);
			} 
		});
		for(var j=0;j<references.length;j++){ 
			requestDetails+=references[j]+"__"+paymodes[j]+"__"+amounts[j]+"__"+lineDescriptions[j]+"__"+lineid[j];
			if(j==references.length-1){   
			} 
			else{
				requestDetails+="#@";
			}
		} 
 		return requestDetails;
	 };
	
	 $('.request_category-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	          $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
			if(accessCode=="REQUEST_CATEGORY"){
				$('#reference_'+sectionRowId).html("");
				$('#reference_'+sectionRowId).append("<option value=''>Select</option>");
				loadLookupList("reference_"+sectionRowId); 
			}   
		});

	 	$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			modal : true
		});
		
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/paymentrequest_addrow.action",
										async : false,
										data : {
											rowId : id
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$('.tab tr:last').before(result);
											if ($(".tab").height() > 255)
												$(".tab").css({
													"overflow-x" : "hidden",
													"overflow-y" : "auto"
												});
											$('.rowid').each(
													function() {
														var rowId = getRowId($(
																this)
																.attr('id'));
														$('#lineId_' + rowId)
																.html(i);
														i = i + 1;
													}); 
										}
									});
							return false;
						});

		$('.reference').live('change', function() {
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});
		$('.amount').live('change', function() {
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});
		 
		$('.paymode').live('change', function() {
			var rowId = getRowId($(this).attr('id'));
			triggerAddRow(rowId);
			return false;
		});
		
		if(Number($('#paymentRequestId').val()> 0)){ 
			$('#totalInvoiceAmount').text($('.totalInvoiceAmountH').val());
			$('#requestDate').datepick({showTrigger: '#calImg'}); 
			$('.splrowid').each(function(){    
				var rowId=getRowId($(this).attr('id')); 
				$('#paymode_'+rowId+' option[value='+$('#temppaymode_'+rowId).val()+']').attr("selected","selected");  
				$('#reference_'+rowId).val($('#tempReference_'+rowId).val());  
			});
		} else {
			$('#requestDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
		}
		
	});
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function triggerAddRow(rowId) {
		var reference = $('#reference_' + rowId).val(); 
		var paymode = $('#paymode_' + rowId).val();
		var amount = $('#amount_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (reference != null && reference != "" && paymode != null && paymode != ""
				&& amount != null && amount != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
		calculateAmount();
	}
	function calculateAmount() {
		var amount = Number(0);
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));
			amount += Number($jquery('#amount_' + rowId).val());
		}); 
		$jquery('.totalInvoiceAmountH').val(amount); 
		$('#totalInvoiceAmount').text($('.totalInvoiceAmountH').val());
	}
	function personPopupResult(personId, personName, commonParam){  
		$('#personName').val(personName); 
		$('#personId').val(personId); 
		$('#common-popup').dialog("close"); 
	}
	 
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> payment
			request
		</div> 
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		<form name="requestvalidation" id="requestvalidation" style="position: relative;">
			<div class="portlet-content">
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="paymentRequestId" name="paymentRequestId"
					value="${PAYMENT_REQUEST.paymentRequestId}" />
				<div class="width100 float-left" id="hrm">
					<div class="tempresult" style="display: none;"></div>
					<div id="page-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div class="width48 float-left" id="hrm">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30">Request No.<span
									class="mandatory">*</span>
								</label>
								<c:choose>
									<c:when
										test="${PAYMENT_REQUEST.paymentReqNo ne null && PAYMENT_REQUEST.paymentReqNo ne ''}">
										<input type="text" name="paymentRequestNo"
											value="${PAYMENT_REQUEST.paymentReqNo}" id="paymentRequestNo"
											readonly="readonly" class="width50 validate[required]">
									</c:when>
									<c:otherwise>
										<input type="text" name="paymentRequestNo"
											value="${requestScope.paymentRequestNo}"
											id="paymentRequestNo" readonly="readonly"
											class="width50 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Request Date<span
									class="mandatory">*</span>
								</label>
								<c:choose>
									<c:when
										test="${PAYMENT_REQUEST.requestDate ne null && PAYMENT_REQUEST.requestDate ne ''}">
										<c:set var="requestDate"
											value="${PAYMENT_REQUEST.requestDate}" />
										<%
											String date = DateFormat.convertDateToString(pageContext
															.getAttribute("requestDate").toString());
										%>
										<input type="text" name="requestDate" value="<%=date%>"
											id="requestDate" readonly="readonly"
											class="width50 validate[required]">
									</c:when>
									<c:otherwise>
										<input type="text" name="requestDate" id="requestDate"
											readonly="readonly" class="width50 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Person<span class="mandatory">*</span></label> 
								<c:set var="personName" value="${PAYMENT_REQUEST.personName}"/>
								<c:if test="${PAYMENT_REQUEST.personByPersonId ne null && PAYMENT_REQUEST.personByPersonId.personId gt 0}">
									<c:set var="personName" value="${PAYMENT_REQUEST.personByPersonId.firstName} ${PAYMENT_REQUEST.personByPersonId.lastName}"/>
								</c:if>
 								<input type="hidden" id="personId" name="personId" value="${PAYMENT_REQUEST.personByPersonId.personId}"/>
								<input type="text" name="personName" id="personName" 
									class="width50 validate[required] personName" value="${personName}"/>
								 <span class="button">
									<a style="cursor: pointer;" id="person" class="btn ui-state-default ui-corner-all common-popup-person width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span> 
							</div>  
						</fieldset>
					</div>
					<div class="width48 float-right" id="hrm">
						<fieldset style="min-height:90px;"> 
							<div>
								<label class="width30 tooltip">Status<span
									class="mandatory">*</span>
								</label> <select name="status" id="status"
									class="width51 validate[required]">
									<c:forEach var="STATUS" items="${PAYMENT_REQUEST_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempstatus"
									value="${PAYMENT_REQUEST.status}" />
							</div>
							<div>
								<label class="width30">Description</label>
								<textarea id="description" class="width50">${PAYMENT_REQUEST.description}</textarea>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="hrm" class="portlet-content width100 invoice_lines">
				<fieldset id="hrm">
					<legend>Payment Request Detail<span
									class="mandatory">*</span></legend>
					<div class="portlet-content">
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr> 
										<th style="width: 5%;">Category</th> 
										<th style="width: 3%;">Payment Mode</th>
										<th style="width: 3%;">Amount</th>
										<th style="width: 5%;">Description</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${PAYMENT_REQUEST_DETAIL ne null && PAYMENT_REQUEST_DETAIL ne ''}">
											<c:forEach var="REQUEST_DETAIL"
												items="${PAYMENT_REQUEST_DETAIL}"
												varStatus="status">
												<tr class="rowid splrowid" id="fieldrow_${status.index+1}">
													<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
													<td>
														<select name="reference" id="reference_${status.index+1}" class="reference width70">
															<option value="">Select</option>
															<c:forEach var="category" items="${REQUEST_CATEGORY}">
																<option value="${category.lookupDetailId}">${category.displayName}</option>
															</c:forEach>
														</select>
														<input type="hidden" id="tempReference_${status.index+1}"
															value="${REQUEST_DETAIL.lookupDetail.lookupDetailId}"/>
														<span
															class="button" style="position: relative;"> <a
																style="cursor: pointer;" id="REQUEST_CATEGORY_${status.index+1}"
																class="btn ui-state-default ui-corner-all request_category-lookup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
													</td> 
													<td><select id="paymode_${status.index+1}"
														class="paymode">
															<c:forEach var="MODE" items="${PAYMENT_MODE}">
																<option value="${MODE.key}">${MODE.value}</option>
															</c:forEach>
													</select> <input type="hidden" id="temppaymode_${status.index+1}"
														value="${REQUEST_DETAIL.paymentMode}" /></td>
													<td><input type="text" id="amount_${status.index+1}"
														class="amount validate[optional,custom[number]]" style="text-align: right;"
														value="${REQUEST_DETAIL.amount}" /></td>
													<td><input type="text"
														id="description_${status.index+1}" class="linedescription"
														value="${REQUEST_DETAIL.description}" /> <input
														type="hidden" id="paymentRequestId_${status.index+1}"
														value="${REQUEST_DETAIL.paymentRequestDetailId}" /></td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a></td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
									<c:forEach var="i"
										begin="${fn:length(PAYMENT_REQUEST_DETAIL)+1}"
										end="${fn:length(PAYMENT_REQUEST_DETAIL)+2}"
										step="1" varStatus="status1">
										<tr class="rowid" id="fieldrow_${i}">
											<td id="lineId_${i}" style="display: none;">${i}</td>
											<td>
												<select name="reference" id="reference_${i}" class="reference width70">
													<option value="">Select</option>
													<c:forEach var="category" items="${REQUEST_CATEGORY}">
														<option value="${category.lookupDetailId}">${category.displayName}</option>
													</c:forEach>
												</select>
												<span
													class="button" style="position: relative;"> <a
														style="cursor: pointer;" id="REQUEST_CATEGORY_${i}"
														class="btn ui-state-default ui-corner-all request_category-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><select id="paymode_${i}" class="paymode">
													<c:forEach var="MODE" items="${PAYMENT_MODE}">
														<option value="${MODE.key}">${MODE.value}</option>
													</c:forEach>
											</select></td>
											<td><input type="text" id="amount_${i}" class="amount validate[optional,custom[number]]"
												style="text-align: right;" /></td>
											<td><input type="text" id="description_${i}"
												class="linedescription" /> <input type="hidden"
												id="paymentRequestId_${i}" /></td>
											<td style="width: 0.01%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<input type="hidden" id="defaultCurrency"
				value="${DEFAULT_CURRENCY.currencyId}" />
			<div class="clearfix"></div>
		</form>
		<div class="float-right" style="width: 43%;">
			<span style="font-weight: bold;">Total: </span> 
			<span style="font-weight: bold;" id="totalInvoiceAmount"></span>
			<input type="text" class="totalInvoiceAmountH" style="display: none;" readonly="readonly" value="${requestScope.totalAmount}"/>
		</div>
		<div class="clearfix"></div>
		<div style="display: none;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="portlet-header ui-widget-header float-left addrows"
				style="cursor: pointer;">
				<fmt:message key="accounts.common.button.addrow" />
			</div>
		</div>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right"
				id="request_discard" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right"
				id="request_save" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
		<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
	</div>
</div>