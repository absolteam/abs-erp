<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 <script type="text/javascript">
var materialRequisitionId = Number(0);
var storeId = Number(0);
var slidetab = "";
var requisitionDetails = [];

$(function(){
	//manupulateLastRow();
	$jquery("#materialRequisitionValidation").validationEngine('attach');  
	
	$('#requisitionDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
    
	 $('#material_requisition_discard').click(function(event){  
		 materialRequisitionDiscard("");
		 return false;
	 });

	

	 $('#product-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 $('#store-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 

	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
});

function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	if(returnVO.returnStatusName=='DeleteApproved')
		deleteMaterialRequest(messageId,returnVO);
}


function deleteMaterialRequest(messageId,returnVO){
	$('.response-msg').hide();
	materialRequisitionId=Number($("#materialRequisitionId").val());
	if(materialRequisitionId != null && materialRequisitionId != 0){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/material_requisition_delete.action", 
	     	async: false,
	     	data:{materialRequisitionId: materialRequisitionId,alertId: messageId},
			dataType: "json",
			cache: false,
			success: function(response){  
				 $(".tempresult").html(result);
				 var message=$.trim($('.tempresult').html()); 
				 if(message=="SUCCESS"){
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					window.location.reload();
				 }else{
					 $('#journal-error').hide().html("Transaction Failure.").slideDown(1000);
				 }
			},
			error:function(result){  
				$('#journal-error').hide().html("Transaction Failure.").slideDown(1000);
			}
		});
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function commonStorePopup(storeid, storename, param) {
	storeId = storeid;
	$('#store').val(storename);
}
function commonProductPopup(productId, productName, rowId) {
	$('#productid_' + rowId).val(productId);
	$('#product_' + rowId).html(productName); 
} 
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
 	var productQty = Number($('#productQty_'+rowId).val()); 
 	var nexttab=$('#fieldrow_'+rowId).next();  
 	if(productid > 0 && productQty > 0 
 		 		&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
	return false;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function materialRequisitionDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_material_requisition.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();
			$("#main-wrapper").html(result);
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
} 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material
			requisition
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.generalinfo" />
		</div>
		<form name="materialRequisitionValidation"
			id="materialRequisitionValidation" style="position: relative;">
			<input type="hidden" id="materialRequisitionId"
											name="materialRequisitionId"
											value="${MATERIAL_REQUISITION.materialRequisitionId} "
											class="width50" />
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 60px;">  
							<div>
								<label class="width30" for="searchLocation">Store
								 </label> <input type="text"
									readonly="readonly" name="store" id="store"
									class="width50"
									value="${MATERIAL_REQUISITION.store.storeName}" />
								<span class="button"
									style="position: relative !important; top: 6px !important; right: 40px; float: right;">
									<a style="cursor: pointer;" 
									class="btn ui-state-default ui-corner-all requisition-store-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="description" readonly="readonly"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${MATERIAL_REQUISITION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 60px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_REQUISITION.referenceNumber ne null && MATERIAL_REQUISITION.referenceNumber ne ''}"> 
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${MATERIAL_REQUISITION.referenceNumber} "
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="requisitionDate"> Requested Employee</label>
								<input type="text" readonly="readonly" id="employee"
											name="employee"
											value="${MATERIAL_REQUISITION.person.firstName} ${MATERIAL_REQUISITION.person.lastName}"
											class=" width50" />
							</div>
							<div>
								<label class="width30" for="requisitionDate"> Requisition Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_REQUISITION.requisitionDate ne null && MATERIAL_REQUISITION.requisitionDate ne ''}">
										<c:set var="requisitionDate"
											value="${MATERIAL_REQUISITION.requisitionDate}" />
										<input name="requisitionDate" type="text" readonly="readonly"
											id="requisitionDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("requisitionDate").toString())%>"
											class="requisitionDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="requisitionDate" type="text" readonly="readonly"
											id="requisitionDate"
											class="requisitionDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Material Requisition Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th> 
 										<th class="width5">Quantity</th>
 										<th class="width10"><fmt:message
												key="accounts.journal.label.desc" /></th>
										
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${MATERIAL_REQUISITION.materialRequisitionDetails}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.productName}</span>
												
											</td>
 											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> 
											</td> 
											<td><input type="text" name="linesDescription"
												id="linesDescription_${status.index+1}"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											
										</tr>
									</c:forEach>
									
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<%-- <div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="material_requisition_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="material_requisition_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div> --%>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>