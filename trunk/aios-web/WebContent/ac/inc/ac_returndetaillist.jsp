<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:forEach var="bean" varStatus="status" items="${RETURN_LINE_DETAILS}"> 
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
		<td id="productcode_${status.index+1}"> 
			${bean.product.code}
		</td>
		<td id="productname_${status.index+1}"> 
			${bean.product.productName}
		</td>  
		<td>${bean.conversionUnitName}</td>  
		<td>${bean.packageUnit}</td>  
		<td>
			${bean.returnQty}
			<span style="display: none;">${bean.baseUnitName}</span>
		</td>   
		<td>
			<input type="text" id="unitrate_${status.index+1}" style="text-align: right;" class="unitRate" value="${bean.receiveDetail.unitRate}"/> 
			<input type="text" id="totalAmountH_${status.index+1}" style="display: none;" class="totalAmountH" value="${bean.returnQty * bean.receiveDetail.unitRate}"/> 
		</td> 
		<td id="total_${status.index+1}" style="text-align: right;">
 		</td>  
		<td style="display:none"> 
			<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/>
			<input type="hidden" name="debitLineId" id="debitLineId_${status.index+1}" value="0"/> 
			<input type="hidden" name="returnDetailId" id="returnDetailId_${status.index+1}" value="${bean.returnDetailId}"/>
			<input type="hidden" name="returnDetailId" id="returnqty_${status.index+1}" value="${bean.returnQty}"/>
 		</td>  
		<td style="display: none;"> 
		   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		   </a> 
		</td> 
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){
	$jquery('.unitRate,.totalAmountH').number(true, 3);
	$('.rowid').each(function(){
		var rowId = getRowId($(this).attr('id'));
		$('#unitrate_'+rowId).text($('#unitrateH_'+rowId).val());
		$('#total_'+rowId).text($('#totalAmountH_'+rowId).val());
	});
});
</script>