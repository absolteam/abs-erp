<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000!important;
}
.returnerr {
	font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<div id="hrm" class="portlet-content width100 requisitionline-info"
	style="height: 90%;">
	<div class="portlet-content">
		<div id="page-error-popup"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<div id="warning_message-popup"
			class="response-msg notice ui-corner-all width90"
			style="display: none;"></div>
		<div id="hrm" class="hastable width100">
			<table id="hastab-1" class="width100">
				<thead>
					<tr>
						<th class="width10">Product</th>
						<th class="width10">From Store</th> 
						<th class="width10">Transfer Type</th>
						<th class="width10" style="display: none;">Reason</th>
						<th class="width10">Transfer Shelf</th>
						<th class="width5">Requisition Qty</th>
						<th class="width5">Transfered Qty</th>
						<th class="width5">Transfer Qty</th>
						<th class="width5" style="display: none;">Price</th>
						<th class="width5" style="display: none;">Total</th>
						<th class="width10"><fmt:message
								key="accounts.journal.label.desc" /></th>
						<th style="width: 1%;"><fmt:message
								key="accounts.common.label.options" /></th>
					</tr>
				</thead>
				<tbody class="tab-1">
					<c:forEach var="bean" varStatus="status"
						items="${REQUISITION_DETAIL_SESSION}">
						<tr class="rowidrq" id="fieldrowrq_${status.index+1}">
							<td style="display: none;" id="lineIdRQ_${status.index+1}">${status.index+1}</td>
							<td><input type="hidden" name="productId"
								id="productid_${status.index+1}" value="${bean.product.productId}" /> <span>${bean.product.productName}</span>
							</td>
							<td>
								<span id="storeFromName_${status.index+1}">${bean.storeName}</span>
							<input type="hidden" name="storeFromId"
								id="storeFromId_${status.index+1}" value="${bean.shelfFromId}" />
								<span class="button float-right"> <a
									style="cursor: pointer;" id="storeFromID_${status.index+1}"
									class="btn ui-state-default ui-corner-all store-from-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</td>
							<td><select name="transferType" id="transferType_${status.index+1}"
								class="width60 transferType">
									<option value="">Select</option>
									<c:forEach var="transferType" items="${TRANSFER_TYPE}">
										<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
									</c:forEach>
							</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="MATERIAL_TRANSFER_TYPE_${status.index+1}"
									class="btn ui-state-default ui-corner-all transfer-type-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								<input type="hidden" id="temptransferType_${status.index+1}" value="${bean.transferType}"/>
							</td>
							<td style="display: none;"><select name="transferReason" id="transferReason_${status.index+1}"
								class="width60 transferReason">
									<option value="">Select</option>
									<c:forEach var="transferReason" items="${TRANSFER_REASON}">
										<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
									</c:forEach>
							</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="MATERIAL_TRANSFER_REASON_${status.index+1}"
									class="btn ui-state-default ui-corner-all transfer-reason-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								<input type="hidden" id="temptransferReason_${status.index+1}" value="${bean.transferReason}"/>
							</td>
							<td><input type="hidden" name="shelfId"
								id="shelfId_${status.index+1}"
								value="${bean.shelfToId}" /> <span
								id="shelf_${status.index+1}">${bean.transferStoreName}</span>
								<span class="button float-right"> <a
									style="cursor: pointer;" id="shelfID_${status.index+1}"
									class="btn ui-state-default ui-corner-all store-shelf-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</td>
							<td><span>${bean.requisitionQty}</span></td>
							<td><span>${bean.transferedQty}</span></td>
							<td><input type="text" name="productQty"
								id="productQty_${status.index+1}" value="${bean.transferQty}"
								class="productQty validate[optional,custom[number]] width80">
								<input type="hidden" name="totalAvailQty"
								id="totalAvailQty_${status.index+1}">
							</td>
							<td style="display: none;"><input type="text" name="amount" id="amount_${status.index+1}"
								readonly="readonly" style="text-align: right;" value="${bean.unitRate}"
								class="amount width98 validate[optional,custom[number]] right-align">
							</td>
							<td style="display: none;"><span id="totalAmount_${status.index+1}" style="float: right;">${bean.totalAmount}</span>
							</td>
							<td><input type="text" name="linesDescription"
								id="linesDescription_${status.index+1}" class="width98" maxlength="150">
							</td>
							<td style="width: 1%;" class="opn_td" id="option_${status.index+1}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowrq"
								id="DeleteRQImage_${status.index+1}" style="cursor: pointer;"
								title="Delete Record"> <span
									class="ui-icon ui-icon-circle-close"></span> </a> <input
								type="hidden" name="materialRequisitionDetailId"
								id="materialRequisitionDetailId_${status.index+1}" value="${bean.materialRequisitionDetailId}" />
								<input
									type="hidden" name="materialTransferDetailId"
								id="materialTransferDetailId_${status.index+1}" value="${bean.materialTransferDetailId}" />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div
			class="portlet-header ui-widget-header float-left sessionTransferSave"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div
			class="portlet-header ui-widget-header float-left closeTransferDom"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$('.rowidrq').each(function(){ 
		var rowId = getRowId($(this).attr('id'));  
		$('#transferType_'+rowId).val($('#temptransferType_'+rowId).val());
		$('#transferReason_'+rowId).val($('#temptransferReason_'+rowId).val());
	});
});
</script>