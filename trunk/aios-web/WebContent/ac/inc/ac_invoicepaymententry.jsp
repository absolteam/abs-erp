<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var actionname="";
var paymentDetails="";
var tempid="";
var slidetab ="";
var invoiceDetails = "";
$(function(){  
	$('#chequeNumber').attr('autocomplete','off');
	 $jquery("#paymentValidation").validationEngine('attach'); 
	 
	$('#paymentDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	$('#chequeDate').datepick();
	
	$('.paymentsupplier-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
			 	async: false,  
 			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);   
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});  
			return false;
		});

	$('.paymentcheque-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getbankaccountwithcheque_details.action", 
			 	async: false,  
 			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);   
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});  
			return false;
		});

	$('.paymenttransfer-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getbankaccount_details.action", 
			 	async: false,  
			 	data : {showPage : "payments", pageInfo : "payments"}, 
 			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);   
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});  
			return false;
		});

	$('.cardpayment-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
	    var cardtypes = $('#paymentMode').val();
		var cardTypeArray = cardtypes.split('@@'); 
		var cardType = Number(cardTypeArray[0]);
 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/get_creditdebitcard_details.action", 
			 	async: false,  
			 	data : {showPage : "payments", cardType: cardType, pageInfo : "payments"}, 
 			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('.common-result').html(result);   
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.common-result').html(result);  
				}
			});  
			return false;
		});
	
	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.pdiscard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$('#codecombination-popup').dialog('destroy');		
					$('#codecombination-popup').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);  
				}
			});
	 });

	 $('#supplier-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });

	$('#paymentMode').change(function(){ 
		$('#cashCombinationId').val(''); 
	    $('.cashCodeCombination').val(''); 
	    $('#accountNumber').val(''); 
	    $('.chequeNumber').val(''); 
	    $('#chequeDate').val(''); 
	    $('#accountId').val('');
	    $('#cardNumber').val();
	    $('#transferAccountId').val('');
		if($(this).val()!=null && $(this).val()!=""){
			 var arrayVal = $(this).val().split('@@'); 
			 if(arrayVal[1]=="CHQ") {
				 $('#accountNumber').addClass('validate[required]'); 
				 $('#chequeDate').addClass('validate[required]'); 
				 $('#codeCombination').removeClass('validate[required]');  
				 $('#cardNumber').removeClass('validate[required]'); 
				 $('.cardNumberformError').remove();
				 $('.codeCombinationformError').remove();
				 $('.bank').show(); 
				 $('.cash').hide();
				 $('.cards').hide(); 
				 $('.interaccount').hide(); 
			 } 
			 else if(arrayVal[1]=="CSH" || arrayVal[1]=="WRT"){
				$('.cash').show(); 
				$('.bank').hide(); 
				$('.interaccount').hide(); 
				$('.cards').hide();
				$('#accountNumber').removeClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]'); 
				$('#cardNumber').removeClass('validate[required]'); 
				$('#codeCombination').addClass('validate[required]'); 
				$('.accountNumberformError').remove();
				$('.chequeDateformError').remove();
				$('.cardNumberformError').remove();
			}  else if(arrayVal[1]=="CCD" || arrayVal[1]=="DCD"){
				$('.bank').hide();
				$('.cash').hide(); 
				$('.interaccount').hide(); 
				$('.cards').show();
				$('.accountNumberformError').remove();
				$('.chequeDateformError').remove(); 
				$('.codeCombinationformError').remove();
				$('#accountNumber').removeClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]');  
				$('#codeCombination').removeClass('validate[required]');  
				$('#cardNumber').addClass('validate[required]'); 
			} else if(arrayVal[1]=="IAT"){
				$('.bank').hide();
				$('.cash').hide(); 
				$('.cards').hide();
				$('.interaccount').show(); 
				$('.chequeDateformError').remove(); 
				$('.codeCombinationformError').remove();
				$('.cardNumberformError').remove();
				$('#accountNumber').addClass('validate[required]'); 
				$('#chequeDate').removeClass('validate[required]');  
				$('#codeCombination').removeClass('validate[required]');  
				$('#cardNumber').removeClass('validate[required]'); 
			}
		}else{
			$('.bank').hide();
			$('.cash').hide(); 
			$('.cards').hide();
			$('.interaccount').hide(); 
		} 
	});

	//Combination pop-up config
    $('.codecombination-popup').click(function(){
        tempid=$(this).parent().get(0); 
        $('.ui-dialog-titlebar').remove(); 
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/combination_treeview.action",
             async: false, 
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.codecombination-result').html(result); 
             },
             error:function(result){
                  $('.codecombination-result').html(result);
             }
         });
         return false;
    }); 

    $('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: true 
	});

    $('.editDatapy').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var invoiceId = $('#invoiceId_'+rowId).val(); 
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_invoicedetail.action", 
		 	async: false,  
		 	data:{invoiceId: invoiceId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});  
		return false;
	});

    $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.closePaymentDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 return false;
	 });

	$('.sessionPaymentSave').live('click',function(){ 
	   invoiceDetails = getInvoiceDetails();  
	   $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_invoice_session.action", 
		 	async: false,  
		 	data:{invoiceDetail: invoiceDetails},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.tempresult').html(result);  
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
			} 
		 });  
		 return false;
	 });

	var getInvoiceDetails = function(){  
		 var description = new Array();
		 var invoiceline = new Array();  
		 invoiceDetails = "";
		 $('.rowidiv').each(function(){ 
			 var rowid = getRowId($(this).attr('id'));   
			 var linedescription = $('#linedescription_'+rowid).val(); 
			 var invoiceLineId = $('#invoiceLineId_'+rowid).val(); 
			 if(typeof invoiceLineId != 'undefined' && invoiceLineId!=null && invoiceLineId!=""){  
				 invoiceline.push(invoiceLineId); 
				 if(linedescription!=null && linedescription!="")
					description.push(linedescription);
				 else
					description.push("##");  
			 }
		 });
		 for(var j=0;j<invoiceline.length;j++){ 
			invoiceDetails+=description[j]+"__"+invoiceline[j];
			if(j==invoiceline.length-1){   
			}
			else{
				invoiceDetails+="#@";
			}
		}  
		return invoiceDetails;
	 };
	
	$('.psave').click(function(){  
		if($jquery("#paymentValidation").validationEngine('validate')){
 			if($('.tab tr').size() > 0) {
				 var invoiceArray = new Array();
				 $('.activeInvoiceNumber').each(function(){
					 if($(this).attr('checked')){ 
						 invoiceArray.push(getRowId($(this).attr('id')));
					 }
				 });
				 invoiceDetails = "";
				 for(var i=0; i<invoiceArray.length;i++){
					 invoiceDetails+=invoiceArray[i]+"@#";
				 } 
				var paymentId= Number($('#paymentId').val());
				var paymentNumber= $('#paymentNumber').val();
				var paymentDate =$('#paymentDate').val(); 
				var loanId= Number(0);
				var currencyId= Number(0); 
				var accountId= Number($('#accountId').val()); 
				var description= $('#description').val();
				var paymentMode = $('#paymentMode').val();
				var payModeArray = paymentMode.split('@@');
				paymentMode = Number(payModeArray[0]);
				var cardAccountId = Number($('#cardAccountId').val());
				var cashCombinationId = Number($('#cashCombinationId').val()); 
				var chequeNumber= Number($('#chequeNumber').val());
				var chequeDate = $('#chequeDate').val(); 
				var chequeId = Number($('#chequeId').val());
				var transferAccountId = Number($('#transferAccountId').val());
				$.ajax({
					type:"POST",
					url:'<%=request.getContextPath()%>/payment_inventory_save.action',
				 	async: false,
				 	data:{	
					 		paymentId: paymentId, paymentNumber: paymentNumber, paymentDate: paymentDate, paymentDetails: invoiceDetails, cardAccountId: cardAccountId,
					 		loanId: loanId, currencyId: currencyId, bankAccountId: accountId, description: description, paymentMode: paymentMode, transferAccountId: transferAccountId, 
					 		cashCombinationId: cashCombinationId, chequeNumber: chequeNumber, chequeDate: chequeDate, chequeBookId: chequeId
					 	},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$.trim($('.tempresult').html()); 
						 if(message=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove();   
										$('#codecombination-popup').dialog('destroy');		
										$('#codecombination-popup').remove();  
										$('#DOMWindow').remove();
										$('#DOMWindowOverlay').remove();
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html("Record created.").slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					}
				 });
			} else {
				$('#page-error').hide().html("Invoice details not found.").slideDown(1000);
		    	 $('#page-error').delay(2000).slideUp();
				 return false;
			} 
		}
		else{
		 return false;
	 	}
	 });

	$('.delrowpy').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
    	 var deleteId = getRowId($(slidetab).attr('id')); 
    	 var deleteInvoiceId = $('#invoiceId_'+deleteId).val();  
    	 $('.activeInvoiceNumber').each(function(){   
    		var invoiceId = getRowId($(this).attr('id')); 
    		 if(deleteInvoiceId == invoiceId){
				$(this).attr('checked',false);
				 return false;
			 }
    	 });
    	 $(slidetab).remove();  
    	 var i=1;
 	 	 $('.rowid').each(function(){   
 			 var rowId=getRowId($(this).attr('id')); 
 		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
 	 	if($('.tab tr').size()==0)
			$('.payment_lines').hide();
		 return false;
	 }); 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function setCombination(combinationTreeId, combinationTree){  
	  $('#cashCombinationId').val(combinationTreeId); 
      $('.cashCodeCombination').val(combinationTree); 
} 
function showActiveInvoice(supplierId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_supplier_activeinvoice.action", 
	 	async: false,  
	 	data:{supplierId: supplierId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.payment-info').html(result);   
			 if(typeof($('.activeInvoiceNumber').attr('id'))=="undefined"){   
				$('.payment_lines').hide();
				$('.tab').html('');
				$('.payment-info').find('div').css("color","#ca1e1e").html("No active invoice found for this supplier.");
			 } 
		} 
	});  
}
function callDefaultInvoiceSelect(invoiceObj){
	if (typeof invoiceObj != 'undefined') {
		var id= Number(1); 
		var invoiceId = getRowId(invoiceObj); 
		$('#'+invoiceObj).attr('checked',true);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_invoice_details.action", 
		 	async: false,  
		 	data:{invoiceId: invoiceId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.payment_lines').show();
				$('.tab').html(result);  
			} 
		});  
	}
	return false;
}
function validateInvoiceInfo(currentObj){
	var invoiceId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
		 	if($('.rowid').html()!=null && $('.rowid').html()!=''){
		 		$('.rowid').each(function(){
			 		var rowid = getRowId($(this).attr('id'));
		 			id=Number($('#lineId_'+rowid).text());  
					id=id+1;
		 		}); 
			} else
				id=id+1;
	 		$('.payment_lines').show();
		} else {
			$('.payment_lines').show();
			id=id+1; 
		}  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_invoice_details.action", 
		 	async: false,  
		 	data:{invoiceId: invoiceId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.tab').append(result); 
			} 
		});  
	} else {
		 $('.rowid').each(function(){ 
			 var rowId = getRowId($(this).attr('id')); 
			 var removeInvoiceId = $('#invoiceId_'+rowId).val();
			 if(removeInvoiceId == invoiceId){
				 $('#fieldrow_'+rowId).remove();
				 return false;
			 }
		 });
		 if($('.tab tr').size()==0)
			$('.payment_lines').hide();
	}
	return false;
}
function processDiscount(paymentTermId){
	if(paymentTermId!=0){ 
	 var amountRegx = new RegExp(',', 'g'); 
	 var amount=Number($('#amount').val().replace(amountRegx,''));   
	 var paymentDate = $('#paymentDate').val();
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_discount_details.action", 
		 	async: false,
		 	data:{totalAmount: amount, paymentTermId: paymentTermId, paymentDate:paymentDate},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$("#dicount_details").html(result);  
			}
	  });
	}
	return false;
}
function commonSupplierPopup(supplierId, supplierName,
		combinationId, accountCode, param){
	$('#supplierId').val(supplierId);
	$('#supplierName').val(supplierName);
	showActiveInvoice(supplierId);
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Payments</div>
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="re.property.info.generalInfo" /></div>
			<form name="paymentValidation" id="paymentValidation" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">   
					<div class="width49 float-left"> 
						<fieldset style="min-height:105px;"> 
							<div>
								<label class="width30" for="paymentNumber"><fmt:message key="account.payment.paymentNumber" /><span class="mandatory">*</span></label> 
								<input type="hidden" id="paymentId" name="paymentId" value="${PAYMENT_INFO.paymentId}"/>
								<c:choose>
									<c:when test="${PAYMENT_INFO.paymentId ne null && PAYMENT_INFO.paymentId ne ''}"> 
										<input type="text" readonly="readonly" name="paymentNumber" id="paymentNumber" class="width50 validate[required]" value="${PAYMENT_INFO.paymentNumber}"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="paymentNumber" id="paymentNumber" class="width50 validate[required]" value="${PAYMENT_NUMBER}"/>
									</c:otherwise>
								</c:choose>  
							</div> 
							<div>
								<label class="width30" for="paymentDate"><fmt:message key="account.payment.paymentDate" /><span class="mandatory">*</span></label> 
								<c:choose>
									<c:when test="${PAYMENT_INFO.date ne null && PAYMENT_INFO.date ne ''}">
										<c:set var="date" value="${PAYMENT_INFO.date}"/>  
										<% 
											String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
										%>
										<input type="text" readonly="readonly" name="paymentDate" id="paymentDate" class="width50 validate[required]" value="<%=date%>"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="paymentDate" id="paymentDate" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="width30"><fmt:message key="account.payment.paymentMode" /><span class="mandatory">*</span></label>
								<select name="paymentMode" class="width51 validate[required]" id="paymentMode">
									<option value="">Select</option>  
									<c:choose>
										<c:when test="${PAYMENT_MODE ne null && PAYMENT_MODE ne ''}">
											<c:forEach var="MODE" items="${PAYMENT_MODE}">
												<option value="${MODE.key}">${MODE.value}</option> 
											</c:forEach>
										</c:when>
									</c:choose> 
								</select> 
								<input type="hidden" id="tempModeId" name="tempModeId" value="${PAYMENT_INFO.paymentMode}"/>
							</div>	
							<div class="cash" style="display: none;">
							<label class="width30">Cash Account<span class="mandatory">*</span></label>
							<input type="hidden" name="combinationId" id="cashCombinationId" value="${PAYMENT_INFO.combination.combinationId}"/> 
							<c:choose>
								<c:when test="${PAYMENT_INFO.combination.combinationId ne null && PAYMENT_INFO.combination.combinationId ne ''}">
									<input type="text" name="codeCombination" readonly="readonly" id="codeCombination" value="${PAYMENT_INFO.combination.accountByNaturalAccountId.code}[${PAYMENT_INFO.combination.accountByNaturalAccountId.account}]" class="cashCodeCombination width50">
								</c:when>
								<c:otherwise>
									<input type="text" name="codeCombination" readonly="readonly" id="codeCombination" class="cashCodeCombination width50">
								</c:otherwise>
							</c:choose> 
							<span class="button width10 float-right" id="cash_combi" style="position: relative!important;top:7px!important; right:33px!important;">
								<a style="cursor: pointer;" id="cashcombination" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div>
						<div class="bank" style="display: none;">
							<label class="width30">Account No<span class="mandatory">*</span></label>
							<input type="text" id="accountNumber" class="width50 accountNumber" readonly="readonly" name="accountNumber" value="${PAYMENT_INFO.bankAccount.bank.bankName} [${PAYMENT_INFO.bankAccount.accountNumber}]"/> 
							<span class="button width10 float-right" style="position: relative!important;top:7px!important; right:33px!important;">
								<a style="cursor: pointer;" id="bankaccountid" class="btn ui-state-default ui-corner-all paymentcheque-common-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span> 
							<input type="hidden" readonly="readonly" name="accountId" id="accountId" value="${PAYMENT_INFO.bankAccount.bankAccountId}"/>
						</div>
						<div class="bank" style="display: none;">
							<label class="width30">Cheque No</label>
							<input type="text" id="chequeNumber" readonly="readonly" class="width50 chequeNumber" name="chequeNumber" value="${PAYMENT_INFO.chequeNumber}"/>
							<input type="hidden" id="chequeId" value="${PAYMENT_INFO.chequeBook.chequeBookId}"/>
						 </div>
						 <div class="bank" style="display: none;">
							<label class="width30">Cheque Date<span class="mandatory">*</span></label>
								<c:choose>
									<c:when test="${PAYMENT_INFO.chequeDate ne null && PAYMENT_INFO.chequeDate ne ''}">
										<c:set var="cheqDate" value="${PAYMENT_INFO.chequeDate}"/>  
										<%String cheqDate = DateFormat.convertDateToString(pageContext.getAttribute("cheqDate").toString());%>
										<input type="text" name="chequeDate" value="<%=cheqDate%>" 
											id="chequeDate" readonly="readonly" class="width50 validate[required]"> 
									</c:when>  
									<c:otherwise>  
										<input type="text" name="chequeDate" 
											id="chequeDate" readonly="readonly" class="width50 validate[required]"> 
									</c:otherwise>
							</c:choose>  
						 </div>
						 <div class="cards" style="display: none;">
							<label class="width30">Card No<span class="mandatory">*</span></label>
							<input type="text" id="cardNumber" class="width50 cardNumber" name="cardNumber" value="${PAYMENT_INFO.bankAccount.cardNumber}"/>
							<span class="button width10 float-right" style="position: relative!important;top:7px!important; right:33px!important;">
								<a style="cursor: pointer;" id="cardaccountid" class="btn ui-state-default ui-corner-all cardpayment-common-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span> 
							<input type="hidden" readonly="readonly" name="cardAccountId" id="cardAccountId" value="${PAYMENT_INFO.bankAccount.bankAccountId}"/>
						 </div>
						 <div class="interaccount" style="display: none;">
							<label class="width30">Transfer From<span class="mandatory">*</span></label>
							<input type="text" id="transferAccount" class="width50 cardNumber" name="cardNumber" value="${PAYMENT_INFO.bankAccount.accountNumber}"/>
							<span class="button width10 float-right" style="position: relative!important;top:7px!important; right:33px!important;">
								<a style="cursor: pointer;" id="transferAccountid" class="btn ui-state-default ui-corner-all paymenttransfer-common-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span> 
							<input type="hidden" readonly="readonly" name="transferAccountId" id="transferAccountId" value="${PAYMENT_INFO.bankAccount.bankAccountId}"/>
						 </div>  
						</fieldset>
					</div>
					<div class="width48 float-left"> 
						<fieldset style="min-height:105px;">  
							<div style="display: none;">
								<label class="width30">Supplier No.<span class="mandatory">*</span></label>  
								<c:choose>
									<c:when test="${PAYMENT_INFO.paymentId eq null || PAYMENT_INFO.paymentId eq '' || PAYMENT_INFO.paymentId eq 0}">
										<input type="text" name="supplierNumber"
												id="supplierNumber" readonly="readonly" class="width50" value="${PAYMENT_INFO.supplier.supplierNumber}"/>
									</c:when> 
									<c:otherwise>
										<input type="text" name="supplierNumber"
												id="supplierNumber" readonly="readonly" class="width60"/>
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Supplier<span class="mandatory">*</span></label> 
								<c:choose>
									<c:when test="${PAYMENT_INFO.supplier.person ne null && PAYMENT_INFO.supplier.person ne '' }">
										<input type="text" name="supplierName" value="${PAYMENT_INFO.supplier.person.firstName} ${PAYMENT_INFO.supplier.person.lastName}" 
												readonly="readonly" id="supplierName" class="width50 validate[required]"/>  
									</c:when>
									<c:when test="${PAYMENT_INFO.supplier.cmpDeptLocation ne null && PAYMENT_INFO.supplier.cmpDeptLocation ne '' }">
										<input type="text" name="supplierName" value="${PAYMENT_INFO.supplier.cmpDeptLocation.company.companyName}" 
												readonly="readonly" id="supplierName" class="width50 validate[required]">  
									</c:when>
									<c:otherwise>
										<input type="text" name="supplierName"
											id="supplierName" readonly="readonly" class="width50 validate[required]">  
										<span class="button float-right" style="position: relative; top: 6px; right: 11%;">
											<a style="cursor: pointer;" id="supplier" class="btn ui-state-default ui-corner-all paymentsupplier-common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span> 
									</c:otherwise>
								</c:choose>  
								<input type="hidden" readonly="readonly" name="supplierId" id="supplierId" value="${PAYMENT_INFO.supplier.supplierId}"/>
							</div>  
							<div>
								<label class="width30" for="description"><fmt:message key="accounts.quotation.description" /></label>
								<textarea rows="2" id="description" class="width51 float-left">${PAYMENT_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>  
			</div>  
		</div>    
		<div class="clearfix"></div>   
		<div class="payment-info float-left width97" id="hrm">
			 <c:if test="${GOODS_RECEIVE_INFO ne null && GOODS_RECEIVE_INFO ne ''}">
			 	<fieldset> 
					<legend>Active Invoice<span
									class="mandatory">*</span></legend>
					<div>
						<c:forEach var="ACTIVE_INVOICE" items="${INVOICE_NOTES}">
							<label for="activeInvoiceNumber_${ACTIVE_INVOICE.invoiceId}" class="width10" id="grnLabel_${ACTIVE_INVOICE.invoiceId}">
								<input type="checkbox" name="activeInvoiceNumber" class="activeInvoiceNumber activeInvNumberUpdate" id="activeInvoiceNumber_${ACTIVE_INVOICE.invoiceId}"/>
								${ACTIVE_INVOICE.invoiceNumber}
							</label>
						</c:forEach>
					</div>
				</fieldset>
			 </c:if> 
		</div>
		<div class="clearfix"></div>   
		<div id="hrm" class="portlet-content width100 payment_lines" style="display: none;"> 
			<fieldset id="hrm"> 	
				<legend>Payment Detail<span
									class="mandatory">*</span></legend>
				<div class="portlet-content"> 
				<div id="hrm" class="hastable width100"  > 
				 <table id="hastab" class="width100"> 
					<thead>
					   <tr>   
				   	    <th style="width:5%;">Invoice NO.</th> 
				   	    <th style="width:5%;">Invoice Date</th>   
				   	    <th style="width:5%;">Invoice Qty.</th>  
				   	    <th style="width:5%;">Total Amount</th> 
					    <th style="width:5%;">Description</th> 
						<th style="width:1%;"><fmt:message key="accounts.common.label.options"/></th> 
					  </tr>
					</thead> 
					<tbody class="tab">   
					</tbody>
				</table>
				</div>
				</div>
			</fieldset> 
		</div>
		<div id="temp-result" style="display:none;"></div>
		<div class="clearfix"></div>   
		<span class="callJq"></span>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right pdiscard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right psave" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
  </form>
  </div> 
</div>