<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
} 
.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width:50%!important;
	}
	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}


</style>
<script type="text/javascript"> 

var oTable; var selectRow=""; var aSelected = []; var aData="";
var locationId = 0;
var personId = 0; var productId = 0; var datefrom = ""; var dateto ="";
$(function(){  
		
		$('#list_grid').click(function(){ 
			populateDatatable();
		});
		
		 $(".print-call").click(function(){  
			 datefrom=$('#fromDate').val();
			 dateto=$('#endDate').val();   
				window.open('<%=request.getContextPath()%>/requisition_report_printout.action?locationId='+locationId+'&personId='+personId+'&datefrom='+datefrom+'&dateto='+dateto+'&productId='+productId,
						'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');	
			 }); 
		  
		  
		  $('.location').combobox({
				selected : function(event, ui) {
					locationId = $('#locationId :selected').val();
				}
			}); 
		  
		  $('.product').combobox({
				selected : function(event, ui) {
					productId = $('#productId :selected').val();
				}
			}); 
		  
		  $('.person').combobox({
				selected : function(event, ui) {
					personId = $('#personId :selected').val();
				}
			}); 
		  
		  populateDatatable();
		  
		  $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
			$('#selectedMonth,#linkedMonth').change();
			$('#l10nLanguage,#rtlLanguage').change();
			if ($.browser.msie) {
			        $('#themeRollerSelect option:not(:selected)').remove();
			}
			$('#fromDate,#endDate').datepick({
			 onSelect: customRange, showTrigger: '#calImg'}); 
	}); 
	
function populateDatatable() {  
	datefrom=$('#fromDate').val();
	dateto=$('#endDate').val(); 
	$('#RequisitionDTR').dataTable(
			{
				"sAjaxSource" : "requisition_jsonlist_reportcriteria.action?datefrom="
						+ datefrom + "&dateto=" + dateto + "&productId="
						+ productId + "&personId=" + personId
						+ "&locationId=" + locationId,
				"sPaginationType" : "full_numbers",
				"bJQueryUI" : true,
				"bDestroy" : true,
				"iDisplayLength" : 25,
				"aoColumns" : [ {
					"sTitle" : "RequisitionDetailId",
					"bVisible" : false
				}, {
					"sTitle" : "Requisiiton Number"
				}, {
					"sTitle" : "Requisiiton Date"
				}, {
					"sTitle" : 'Person'
				}, {
					"sTitle" : "Location"
				}, {
					"sTitle" : "Product"
				}, {
					"sTitle" : "Quantity"
				}, {
					"sTitle" : 'Status',
					"bVisible" : false
				},],
				"sScrollY" : $("#main-content").height() - 235,
				//"bPaginate": false,
				"aaSorting" : [ [ 1, 'desc' ] ],
				"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
					if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
						$(nRow).addClass('row_selected');
					}
				}
			});
	
	oTable = $('#RequisitionDTR').dataTable();  
}
	
		
		//Prevent selection of invalid dates through the select controls
		function checkLinkedDays() {
			var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
					'#selectedMonth').val());
			$('#selectedDay option:gt(27)').attr('disabled', false);
			$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
					true);
			if ($('#selectedDay').val() > daysInMonth) {
				$('#selectedDay').val(daysInMonth);
			}
		}
		function customRange(dates) {
			if (this.id == 'fromDate') {
				$('#endDate').datepick('option', 'minDate', dates[0] || null); 
			} else {
				$('#fromDate').datepick('option', 'maxDate', dates[0] || null);
			}
		}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>item requisition
		</div>
		<div class="portlet-content">
			<div class="width45 float-right" style="padding: 5px;">
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.datefrom" /></label> <input type="text"
						name="fromDate" class="width60 fromDate" id="fromDate"
						readonly="readonly">
				</div>
				<div>
					<label class="width30" style="margin-top: 5px;"><fmt:message
							key="hr.department.label.dateto" /></label> <input type="text"
						name="toDate" class="width60 toDate" id="endDate"
						readonly="readonly">
				</div>  
				
			</div>
			
			<div class="width45 float-left" style="padding: 5px;"> 
				<div>
					<label class="width30" for="productId">Product</label>
					<select id="productId" class="product" name="product">
						<option value="-1">--All--</option>
						<c:forEach var="product" items="${PRODUCT_INFO}">
							<option value="${product.productId}">${product.code} -- ${product.productName}</option>						
						</c:forEach> 
					</select>		
				</div>  
				<div>
					<label class="width30" for="locationId">Location</label>
					<select id="locationId" class="location" name="location">
						<option value="-1">--All--</option>
						<c:forEach var="location" items="${LOCATION_INFO}">
							<option value="${location.locationId}">${location.locationName}</option>						
						</c:forEach>
					
					</select>		
				</div>  
				<div>
					<label class="width30" for="personId">Person</label>
					<select id="personId" class="person" name="person">
						<option value="-1">--All--</option>
						<c:forEach var="person" items="${PERSON_INFO}">
							<option value="${person.personId}">${person.firstName} ${person.lastName}</option>						
						</c:forEach>
					
					</select>		
				</div> 
			</div> 
 			<div class="clearfix"></div>
 			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
					style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
					<div class="portlet-header ui-widget-header float-right"
						id="list_grid" style="cursor: pointer; color: #fff;">
						list gird
					</div> 
				</div>
			<div class="clearfix"></div>
			<div id="rightclickarea">
				<div id="requisition_list">
					<table class="display" id="RequisitionDTR"></table>
				</div>
			</div>
			
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>