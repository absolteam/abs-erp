<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 var oTable1; var selectRow1=""; var aSelected1 = []; var aData1="";
	$(function() {
		
		productItemType=$('#productItemType').val();
		loadProductJsonData();

		/* Click event handler */
		$('#ProductCommonList tbody tr').live(
				'dblclick',
				function() {
					aData1 = oTable1.fnGetData(this);
					var productmrp = $('#productmrp').val();
					if(productmrp == "false"){
						commonProductPopup(aData1.productId, aData1.productName, null, aData1.itemSubType, aData1);
					}else{
						commonProductPopupMrp(aData1);
					} 
					$('#product-inline-popup').dialog("close");
					return false;
				});

		/* Click event handler */
		$('#ProductCommonList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable1.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
		
		$('#productItemType').change(function(){
			productItemType=$(this).val();
			loadProductJsonData();
		});
	});
	
	function loadProductJsonData(){
		oTable1 = $('#ProductCommonList')
		.dataTable(
				{
					"bJQueryUI" : true,
					"sPaginationType" : "full_numbers",
					"bFilter" : true,
					"bInfo" : true,
					"bSortClasses" : true,
					"bLengthChange" : true,
					"bProcessing" : true,
					"bDestroy" : true,
					"iDisplayLength" : 10,
					"sAjaxSource" : "show_purchase_product_common_jsonlist.action?itemType="+productItemType,

					"aoColumns" : [ {
						"mDataProp" : "code"
					}, {
						"mDataProp" : "productName"
					}, {
						"mDataProp" : "unitName"
					}, {
						"mDataProp" : "costingMethod"
					} ]
				});
	}
</script>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
		</div>
		<div class="portlet-content">
			<div id="product_json_list">
				<div class="width50" style="margin-left: 5px;"><label class="width30" >Item Type</label> 
					<select name="productItemType" class="width50 productItemType"
						id="productItemType">
							<option value="I">Inventory</option>
							<option value="A">Asset</option>
							<option value="E">Expense</option>
					</select>
				</div>
				<input type="hidden" id="productmrp" value="false"/>
				<table id="ProductCommonList" class="display" width="100%">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th>
							<th>Unit Type</th>
							<th>Costing Method</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- <div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="product-common-popup">close</div>
			</div> -->
		</div>
	</div>
