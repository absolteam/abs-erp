<tr id="fieldrow_${requestScope.rowId}" class="rowid">
	<td class="width20" id="name_${requestScope.rowId}"></td>
    <td class="width5" id="startTime_${requestScope.rowId}"></td>
    <td class="width5" id="endTime_${requestScope.rowId}"></td>
    <td class="width5" id="extention_${requestScope.rowId}"></td>
    <td class="width5" id="periodstatus_${requestScope.rowId}"></td>
    <td style="display:none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td> 
	<td  style="width:2%;" id="option_${requestScope.rowId}">
		<input type="hidden" name="periodId_${requestScope.rowId}" id="periodId_${requestScope.rowId}"  value="0"/>   
		<input type="hidden" name="statusline" id="statusline_${requestScope.rowId}"/>  
	  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatanew" id="AddImage_${requestScope.rowId}"  title="Add Record">
	 	 <span class="ui-icon ui-icon-plus"></span>
	   </a>	
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatanew"  id="EditImage_${requestScope.rowId}" style="display:none; cursor: pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a style="cursor: pointer;display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"  id="DeleteImage_${requestScope.rowId}" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${requestScope.rowId}" style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td> 
</tr>	
<script type="text/javascript"> 
$(function(){
	$(".addDatanew").click(function(event){ 
		 if($('#openFlag').val() == 0) {
		slidetab=$(this).parent().parent().get(0);  
		
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		if($jquery("#editCalendarVal").validationEngine('validate')){  
				$("#AddImage_"+rowid).hide();
				$("#EditImage_"+rowid).hide();
				$("#DeleteImage_"+rowid).hide();
				$("#WorkingImage_"+rowid).show(); 
				
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/gl_calendar_period_add_action.action",
					data:{rowId:rowid,addEditFlag:"A"},
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $(result).insertAfter(slidetab);
					 $('#openFlag').val(1);
					}
			});
			event.preventDefault();
		}
		else{
			return false;
		}	
		 }else{
			return false;
		 }
		 return false; 
	});

	$(".editDatanew").click(function(event){
		 
		 if($('#openFlag').val() == 0) { 
		 slidetab=$(this).parent().parent().get(0);  

		 if($jquery("#editCalendarVal").validationEngine('validate')){  
 			 if($('#availflag').val()==1){
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/gl_calendar_period_edit_action.action",
				 data:{rowId:rowid,addEditFlag:"E"},
				 async: false,
			  dataType: "html",
				 cache: false,
			   success:function(result){
				
			   $(result).insertAfter(slidetab);

			 //Bussiness parameter
	   			var name=$('#name_'+rowid).text();
	   			var startTime=$('#startTime_'+rowid).text(); 
	   			var endTime=$('#endTime_'+rowid).text(); 
	   			var extention=$('#extention_'+rowid).text();
	   			var periodstatus=$('#statusline_'+rowid).val();
	   			$('#name').val(name);
	   			$('#extention').val(extention);
	   			$('#startPicker_').val(startTime);
	   			$('#endPicker_').val(endTime); 
	   			$('.periodStatus').val(periodstatus);
			    $('#openFlag').val(1);
			   
			},
			error:function(result){
			//	alert("wee"+result);
			}
			
			});
			 }
				else{
					$('#temperror').hide().html("Calendar name <span style='font-weight:bold; display: inline;'>\""+name+ "\"</span> is already is use !!!").slideDown(1000);
				}
			 event.preventDefault();			
		 }
		 else{
			 return false;
		 }	
		 }else{
			 return false;
		 } 
		 return false; 
	}); 
});	
</script>	