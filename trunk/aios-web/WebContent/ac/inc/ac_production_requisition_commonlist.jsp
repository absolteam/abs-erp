<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 var oTable; var selectRow=""; var aSelected = []; var aData="";
	$(function() {
		oTable = $('#ProductionRequisitionCDT')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_production_requisition_commonlist.action",

							"aoColumns" : [ {
								"mDataProp" : "referenceNumber"
							}, {
								"mDataProp" : "requisitionDateStr"
							}]
						});
		oTable.fnSort( [ [0,'desc'] ] );	
		/* Click event handler */
		$('#ProductionRequisitionCDT tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					productionRequisitionPopup(aData);
					$('#productrequisition-popup').trigger('click');
					return false;
				});

		/* Click event handler */
		$('#ProductionRequisitionCDT tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
		
		$('#productrequisition-popup').click(function () { 
			$('#common-popup').dialog("close");
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>production requisition
		</div>
		<div class="portlet-content">
			<div id="production_json_list">
				<table id="ProductionRequisitionCDT" class="display">
					<thead>
						<tr>
							<th>Reference</th>
							<th>Date</th> 
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="productrequisition-popup">close</div>
			</div>
		</div> 
	</div>
</div>