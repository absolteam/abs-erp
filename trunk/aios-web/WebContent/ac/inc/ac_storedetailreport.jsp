<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Store Detail</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>STORE DETAILS</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Storage Name<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					 ${STORE_DETAILS.storageType}
				</span>
			</div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">Store Manager<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					${STORE_DETAILS.personName}
				</span>  		
			</div>  
		</div>  
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Reference Number<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					 ${STORE_DETAILS.storeNumber}
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">Store Name <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${STORE_DETAILS.storeName}
				</span>
			</div>
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		 Aisle
		<table class="width100">
			<tr>  
				<th>Reference</th>
				<th>Dimension</th>
				<th>Section Type</th> 
 			</tr>
			<tbody>  
				<c:forEach items="${STORE_DETAILS.aisleVOs}" var="AISLE_DETAIL" varStatus="status"> 
					<tr> 
						<td>${AISLE_DETAIL.aisleNumber}</td>
						<td>${AISLE_DETAIL.dimension}</td>
 						<td>${AISLE_DETAIL.sectionType}</td> 
					</tr>
					<tbody>  
						<c:forEach items="${AISLE_DETAIL.shelfVOs}" var="SHELFS" varStatus="status">
							<tr>
								<th>Shelf Type</th>
								<th>Dimension</th>
							</tr> 
							<tr> 
								<td>${SHELFS.shelfTypeName}</td>
								<td>${SHELFS.dimension}</td>
							</tr>
							<tr>
								<th>Rack Type</th>
								<th>Dimension</th>
							</tr>
							<tr> 
								<td>${SHELFS.rackTypeName}</td>
								<td>${SHELFS.rackDimension}</td>
							</tr>
						</c:forEach> 
					</tbody>
				</c:forEach> 
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	<div class="width100  div_text_info float_left" style="position:relative; top: 15px;">
		<span class="float_left left_align width20 text-bold">Address<span class="float_right">:</span> </span>
		<span class="span_border left_align text_info" style="width:77%;display: -moz-inline-stack;">
			<c:choose>
				<c:when test="${STORE_DETAILS.address ne null && STORE_DETAILS.address ne ''}">
					${STORE_DETAILS.address}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose>  
		</span>
	</div> 
	</body>
</html>