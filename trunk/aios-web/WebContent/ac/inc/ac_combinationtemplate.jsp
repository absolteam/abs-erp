<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
</style>
<script type="text/javascript">
var combinationId=0;
var segmentId=0; 
$(function(){  
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	});

	$('#selectAllCombination').click(function(){
		if($(this).is(':checked'))
	        $('input:checkbox').attr('checked','checked');  
		else
	        $('input:checkbox').removeAttr('checked'); 
	});

	$('.commoncheck').click(function(){
		var flag=false;
		if($(this).is(':checked')) {
			$(this).siblings('ul').find('input[type="checkbox"]').attr('checked', 'checked');
			$(this).parents('ul').siblings('input[type="checkbox"]').attr('checked', 'checked');
		}else{
			$(this).siblings('ul').find('input[type="checkbox"]').removeAttr('checked', 'checked');
		}
		$('.commoncheck').each(function(){
			if($(this).is(':checked'))
				flag=true;
			else{
				flag=false;
				return false;
			}
		});
		if(flag)
			$('#selectAllCombination').attr('checked','checked');
		else
			$('#selectAllCombination').removeAttr('checked');		
	});

	if($('#combination_idlist').html()!=null && $('#combination_idlist').html()!=''){
		$('.COMBI_ID').each(function(){
			var idval=$(this).attr('id');
			var combinationval=$('#'+idval).val();
			$('.commoncheck').each(function(){
				if($(this).siblings('input[type="hidden"]').val()==combinationval){
					$(this).attr('checked', 'checked');
				}
			});
		});
	} 

	$('.process-combination').click(function(){
		var combinationIdArray=new Array();
		var combinationTemplateIds = "";
		$('.commoncheck').each(function(){
			if($(this).is(':checked')) {
				var idval = $(this).attr('id');
				combinationIdArray.push($('.'+idval).val());
			}
		});
		for(var j=0;j<combinationIdArray.length;j++){ 
			combinationTemplateIds+=combinationIdArray[j];
			if(j==combinationIdArray.length-1){   
			} 
			else{
				combinationTemplateIds+="#@";
			}
		}
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_coa_template.action", 
	     	async: false, 
	     	data:{combinationTemplateIds: combinationTemplateIds},
			dataType: "html",
			cache: false,
			success: function(result){ 
				$("#coa-tempresult").html(result);
			} 		
		}); 
	});

	$('.discard-view').click(function(){
		var page=$(this).attr('id');
		var urlPath="";
		if(page=="add-template")
			urlPath = "discard_coatemplate";
		else
			urlPath = "discard_coatemplate_customise";
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/"+urlPath+".action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){  
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove(); 
				$("#main-wrapper").html(result);
			} 		
		}); 
	});
});
</script>
<div id="coa-tempresult">
	<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
			<div class="mainhead portlet-header ui-widget-header">
				<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				CHART OF ACCOUNTS TEMPLATE  
			</div> 
			<div class="portlet-content">  
				<div id="temp-result" style="display: none;"></div> 
				<span id="combination_idlist">
				<c:if test="${COMBINATION_ID ne null && COMBINATION_ID ne ''}">
					<c:forEach var="COMBI_ID" items="${COMBINATION_ID}" varStatus="status0"> 
						<input type="hidden" class="COMBI_ID" id="COMBI_ID_${status0.index+1}" value="${COMBI_ID}"/>
					</c:forEach>
				</c:if>
				</span>
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
				<div id="page-success" class="response-msg success ui-corner-all width90" style="display:none;"></div>  
			  	<div class="width100 float-left" id="hrm">  
					<div class="float-left  width50">   
						<div id="main">
							<div id="sidetree">
								<div id="sidetreecontrol"></div> 
									<c:choose>
										<c:when test="${COMBINATION_TEMPLATE_TREE_VIEW ne null && COMBINATION_TEMPLATE_TREE_VIEW ne ''}">  
											<ul id="tree">  
												<li><input type="checkbox" id="selectAllCombination" name="selectAllCombination"/> 
													<c:forEach var="bean" items="${COMBINATION_TEMPLATE_TREE_VIEW}" varStatus="status">
														<li>
															<input type="hidden" class="companybox_${status.index+1}" value="${bean.combinationId}"/>
															<input type="checkbox" id="companybox_${status.index+1}" name="companybox" class="commoncheck"/> 
															<a id="company_${status.index+1}_${status.index+1}" class="combination_ companys">
																	${bean.COATemplateByCompanyAccountId.account} [${bean.COATemplateByCompanyAccountId.code}]</a> 
																<c:choose>
															   		<c:when test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
															   		<ul>
															   			<c:forEach var="costbean" items="${bean.costVos}" varStatus="cstatus">
															   				<li>
															   					<input type="hidden" class="costbox_${status.index+1}${cstatus.index+1}" value="${costbean.combinationId}"/>
															   					<input type="checkbox" id="costbox_${status.index+1}${cstatus.index+1}" name="costbox" class="commoncheck"/> 
																				<a  class="combination_ costscenter" id="costcenter_${status.index+1}_${cstatus.index+1}">
																					${costbean.COATemplateByCostcenterAccountId.account} [${bean.COATemplateByCompanyAccountId.code}.${costbean.COATemplateByCostcenterAccountId.code}]
																				</a>
																					<c:choose>
															   							<c:when test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
															   								<ul>
																								<c:forEach var="naturalbean" items="${costbean.naturalVos}" varStatus="nstatus">
																									<li>
																										<input type="hidden" class="naturalbox_${status.index+1}${cstatus.index+1}${nstatus.index+1}" value="${naturalbean.combinationId}"/>
																										<input type="checkbox" id="naturalbox_${status.index+1}${cstatus.index+1}${nstatus.index+1}" name="naturalbox" class="commoncheck"/> 
																										<a  class="combination_ naturals" id="natural_${cstatus.index+1}_${nstatus.index+1}">
																											${naturalbean.COATemplateByNaturalAccountId.account} [${bean.COATemplateByCompanyAccountId.code}.${costbean.COATemplateByCostcenterAccountId.code}.${naturalbean.COATemplateByNaturalAccountId.code}]</a>
																									<c:choose>
															   											<c:when test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
															   											 	<ul>
																												<c:forEach var="analysisbean" items="${naturalbean.analysisVos}" varStatus="astatus">
																													<c:if test="${naturalbean.COATemplateByNaturalAccountId eq analysisbean.COATemplateByNaturalAccountId}"> 
																														<li>
																														<input type="hidden" class="analysisbox_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}" value="${analysisbean.combinationId}"/>
																														<input type="checkbox" id="analysisbox_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}" name="analysisbox" class="commoncheck"/>
																														<a  class="combination_ analysis" id="analysis_${nstatus.index+1}_${astatus.index+1}">
																															${analysisbean.COATemplateByAnalysisAccountId.account} [${bean.COATemplateByCompanyAccountId.code}.${costbean.COATemplateByCostcenterAccountId.code}.${naturalbean.COATemplateByNaturalAccountId.code}.${analysisbean.COATemplateByAnalysisAccountId.code}]</a>
																														 <c:choose> 
															   																<c:when test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0 &&
															   																		naturalbean.COATemplateByNaturalAccountId.accountId eq analysisbean.COATemplateByNaturalAccountId.accountId}"> 
																																<ul>  
																																	<c:forEach var="buffer1bean" items="${analysisbean.buffer1Vos}" varStatus="b1status"> 
																																		 <li>
																																		 	<input type="hidden" class="buffer1box_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}${b1status.index+1}" value="${buffer1bean.combinationId}"/>
																																			<input type="checkbox" id="buffer1box_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}${b1status.index+1}" name="buffer1box" class="commoncheck"/>
																																			<a  class="combination_ buffer1s" id="buffer1_${astatus.index+1}_${b1status.index+1}">
																																				${buffer1bean.COATemplateByBuffer1AccountId.account} [${bean.COATemplateByCompanyAccountId.code}.${costbean.COATemplateByCostcenterAccountId.code}.${naturalbean.COATemplateByNaturalAccountId.code}.${analysisbean.COATemplateByAnalysisAccountId.code}.${buffer1bean.COATemplateByBuffer1AccountId.code}]</a>
																																			<c:choose>
															   																					<c:when test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																																					<ul>
																																						<c:forEach var="buffer2bean" items="${buffer1bean.buffer2Vos}" varStatus="b2status">
																																							<li>
																																								<input type="hidden" class="buffer2box_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}${b1status.index+1}${b1status.index+1}" value="${buffer2bean.combinationId}"/>
																																								<input type="checkbox" id="buffer2box_${status.index+1}${cstatus.index+1}${nstatus.index+1}${astatus.index+1}${b1status.index+1}${b2status.index+1}" name="buffer2box" class="commoncheck"/>
																																								<a class="combination_ buffer2s"  id="buffer2_${b1status.index+1}_${b2status.index+1}">
																																									${buffer2bean.COATemplateByBuffer2AccountId.account} [${bean.COATemplateByCompanyAccountId.code}.${costbean.COATemplateByCostcenterAccountId.code}.${naturalbean.COATemplateByNaturalAccountId.code}.${analysisbean.COATemplateByAnalysisAccountId.code}.${buffer1bean.COATemplateByBuffer1AccountId.code}.${buffer2bean.COATemplateByBuffer2AccountId.code}]</a></li>
																																						</c:forEach>
																																					</ul>
																																				</c:when>
																																			</c:choose>
																																		</li>
																																	</c:forEach> 
																																</ul>
																														</c:when>
																													</c:choose>	
																													</li>	
																													</c:if> 
																												</c:forEach>
																											</ul>
															   											</c:when>
															   										</c:choose>	 
																									</li>
																								</c:forEach>
																							</ul>
															   							</c:when>
															   						</c:choose> 
																				</li>
																		</c:forEach> 
																		</ul>
															   		</c:when>
															   	</c:choose>  
															</li>  
													</c:forEach>  
												</li> 
											</ul> 
										</c:when>
									</c:choose>
								</div> 
							</div> 
						</div> 
					</div>  
			</div> 
			<div class="clearfix"> </div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 	
				<div class="portlet-header ui-widget-header float-right discard-view" id="${requestScope.showPage}" ><fmt:message key="accounts.common.button.discard"/></div>
				<div class="portlet-header ui-widget-header float-right process-combination" id="${requestScope.showPage}" ><fmt:message key="accounts.common.button.process"/></div>	
			</div> 
	  	</div> 
	</div>
</div>