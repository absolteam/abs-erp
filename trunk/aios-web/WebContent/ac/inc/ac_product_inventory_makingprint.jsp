<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
<style>
@media print {
	.hideWhilePrint {
		display: none;
	}
}

td {
	border: 1px solid #8c8c8c;
	font-size: 9px!important;
}

.productcode { 
	width: 200px;
}
.productcode span {
	font-size: 10px; 
}
.productcqty { 
	width: 200px;
}
</style>
<script type="text/javascript">
	
</script>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div id="main-content" class="hr_main_content">
		<div class="width100 float-left">
			<c:forEach var="categoryVO" items="${PRODUCT_DETAILS}"
				varStatus="status">
				<div class="width100 float-left">
					<span style="font-weight: bold;">${categoryVO.categoryName}</span>
					<table class="width100">
						<tbody class="tab">
							<c:set var="productRow" value="0" />
							<c:forEach begin="0" step="8" items="${categoryVO.productVOs}">
								<tr>
									<!-- TD 1 -->
									<td class="productcode"><span>${categoryVO.productVOs[productRow+0].productName}</span>
									</td>
									<td class="productcqty"></td>
									<!-- TD 2 -->
									<c:if test="${categoryVO.productVOs[productRow+1]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+1].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<!-- TD 3 -->
									<c:if test="${categoryVO.productVOs[productRow+2]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+2].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<!-- TD 3 -->
									<c:if test="${categoryVO.productVOs[productRow+3]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+3].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>

									<!-- TD 4 -->
									<c:if test="${categoryVO.productVOs[productRow+4]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+4].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<!-- TD 5 -->
									<c:if test="${categoryVO.productVOs[productRow+5]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+5].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<!-- TD 6 -->
									<c:if test="${categoryVO.productVOs[productRow+6]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+6].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<!-- TD 7 -->
									<c:if test="${categoryVO.productVOs[productRow+7]!= null}">
										<td class="productcode"><span>${categoryVO.productVOs[productRow+7].productName}</span>
										</td>
										<td class="productcqty"></td>
									</c:if>
									<c:set var="productRow" value="${productRow + 8}" />
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</c:forEach>
		</div>
	</div>
</body>