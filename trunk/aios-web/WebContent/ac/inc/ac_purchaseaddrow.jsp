<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td><select name="itemnature" class="width98 itemnature"
		id="itemnature_${rowId}">
			<option value="A">Asset</option>
			<option value="E">Expense</option>
			<option value="I" selected="selected">Inventory</option>
	</select>
	</td>
	<td><span class="float-left width60" id="product_${rowId}"></span>
		<span class="width10 float-right" id="unitCode_${rowId}" style="position: relative;"></span>
		<span class="button float-right prod" style="height: 16px;"> <a
			style="cursor: pointer; height: 100%;" id="productline_${rowId}"
			class="btn ui-state-default ui-corner-all purchaseproduct-common-popup width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input type="hidden"
		id="productid_${rowId}" />
	</td>
	<td><span class="float-left width60" id="storeName_${rowId}"></span>
		<input type="hidden" name="storeId" id="storeId_${rowId}" value="" />
		<input type="hidden" name="shelfId" id="shelfId_${rowId}" /> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="prodIDStr_${rowId}"
			class="btn ui-state-default ui-corner-all purchase-store-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td id="uom_${rowId}" class="uom" style="display: none;"></td>
	<td>
		<select name="packageType" id="packageType_${rowId}" class="packageType">
			<option value="">Select</option>
		</select> 
	</td> 
	<td><input type="text"
		class="width96 packageUnit validate[optional,custom[number]]"
		id="packageUnit_${rowId}" />
 	</td>
	<td><input type="hidden"
		class="width96 quantity validate[optional,custom[number]]"
		id="quantity_${rowId}" />
		<span id="baseUnitConversion_${rowId}" class="width10" style="display: none;"></span>
		<span id="baseDisplayQty_${rowId}"></span>
 	</td>
	<td><input type="text"
		class="width96 unitrate validate[optional,custom[number]]"
		id="unitrate_${rowId}" style="text-align: right;" />
		<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${rowId}" 
			class="totalAmountH"/> 
		<input type="hidden" id="basePurchasePrice_${rowId}"/>
	</td>
	<td class="totalamount validate[optional,custom[number]]"
		id="totalamount_${rowId}" style="text-align: right;"></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${rowId}" />
	</td>
	<td style="display: none;"><input type="hidden"
		id="purchaseLineId_${rowId}" /> <input type="hidden"
		id="requisitionLineId_${rowId}" /></td>
	<td>
		<div id="moreoption_${rowId}"
			style="cursor: pointer; text-align: center; background-color: #3ADF00;"
			class="portlet-header ui-widget-header float-left moreoption">
			MORE OPTION</div></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a>
	</td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#purchaseentry_details").validationEngine('attach');
		$jquery("#unitrate_${rowId},#totalAmountH_${rowId}").number(true, 3); 
 	});
</script>