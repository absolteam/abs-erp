<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:forEach var="DETAIL"
	items="${REQUISITION_DETAIL}" varStatus="status">
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
		<td><input type="hidden" name="productId"
			id="productid_${status.index+1}" value="${DETAIL.product.productId}" />
			<span id="product_${status.index+1}">${DETAIL.product.productName}</span> 
		</td>
		<td id="uom_${status.index+1}" style="display: none;"></td>
		<td id="costingType_${status.index+1}" style="display: none;"></td>
		<td><input type="hidden" name="shelfId"
			id="shelfid_${status.index+1}" value="${DETAIL.shelfId}" /> <span
			id="store_${status.index+1}">${DETAIL.storeName}</span> <span class="button float-right">
				<a style="cursor: pointer;"
				id="storeID_${status.index+1}"
				class="btn ui-state-default ui-corner-all show_store_list_sales_delivery width100">
					<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
		</td>
		<td><input type="text" name="productQty"
			id="productQty_${status.index+1}" value="${DETAIL.quantity}"
			class="productQty validate[optional,custom[number]] width80">
			<input type="hidden" name="totalAvailQty" value="${DETAIL.availableQty}"
			id="totalAvailQty_${status.index+1}" />
			<input type="hidden" name="extproductQty" id="extproductQty_${status.index+1}" />
		</td>
		<td><input type="text" name="amount"
			id="amount_${status.index+1}" value="${DETAIL.unitRate}"
			style="text-align: right;"
			class="amount width98 validate[optional,custom[number]] right-align">
		</td>
		<td><span
			id="totalAmount_${status.index+1}" style="float: right;">${DETAIL.totalAmount}</span>
		</td>
		<td><input type="text" name="linesDescription"
			id="linesDescription_${status.index+1}" value="${DETAIL.description}"
			class="width98" maxlength="150">
		</td>
		<td style="width: 1%;" class="opn_td" id="option_${status.index+1}"><a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
			id="AddImage_${status.index+1}"
			style="cursor: pointer; display: none;" title="Add Record"> <span
				class="ui-icon ui-icon-plus"></span> </a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
			id="EditImage_${status.index+1}"
			style="display: none; cursor: pointer;" title="Edit Record"> <span
				class="ui-icon ui-icon-wrench"></span> </a> <a
			class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
			id="DeleteImage_${status.index+1}" style="cursor: pointer;"
			title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
		</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
			id="WorkingImage_${status.index+1}" style="display: none;"
			title="Working"> <span class="processing"></span> </a> <input
			type="hidden" name="issueRequistionDetailId"
			id="issueRequistionDetailId_${status.index+1}"/>
			<input type="hidden" name="requistionDetailId" value="${DETAIL.requisitionDetailId}"
			id="requistionDetailId_${status.index+1}"/>
		</td>
	</tr>
</c:forEach>