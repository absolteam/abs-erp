<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript">
var currentId=""; var tempvar=""; var idarray=""; var rowid="";
$(function(){ 
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	$("#tree_bank").treeview({
		collapsed: true,
		animated: "medium",
		control:"#sidetreecontrol",
		persist: "location"
	}); 
	$('.chequeid').live('click',function(){ 
		$('.chequeid').each(function(){  
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			 if($('#bankaccountselect_'+rowid).hasClass('selected')){ 
				 $('#bankaccountselect_'+rowid).removeClass('selected');
			 }
			 currentId=0;
		});
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankaccountselect_'+rowid).addClass('selected');
	});
	
	$('.chequeid').live('dblclick',function(){  
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		var chequeno = Number($('#currentChqNo_'+rowid).val());
		$('#chequeNumber').val(Number(chequeno+1));   
		$('#chequeId').val($('#chequeid_'+rowid).text());
		currentId=$($($($($(this).parent()).get(0)).parent()).get(0)).attr('id');   
		idarray = currentId.split('_');
		rowid=Number(idarray[1]);  
		$('#accountNumber').val($('#accountNO_'+rowid).html());
		$('#accountId').val($('#accountid_'+rowid).html());  
		currentId=$('#'+currentId).parent().parent().get(0); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankName').val($('#bankname_'+rowid).html());  
		$('#bankId').val($('#bankid_'+rowid).html()); 
		$('#bankaccountselect_'+rowid).removeClass('selected');
		$('#common-popup').dialog('close');  
		return false;
	});
	
	$('.close').click(function(){
		 $('#common-popup').dialog('close'); 
	});
});
</script> 
<style>
.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>
<div class="portlet-content">  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Bank Account Details
</div>
<span style="display: none;" id="bankTransfer">${requestScope.PAGE_VALUE}</span>
<ul id="tree_bank">
	<li><strong>Banks</strong></li> 
	<c:choose>
		<c:when test="${requestScope.BANK_DETAILS ne null && requestScope.BANK_DETAILS ne''}">
			<c:forEach items="${BANK_DETAILS}" var="bank" varStatus="status1"> 
				<li id="bank_${status1.index+1}">
					<span style="display: none;" id="bankid_${status1.index+1}">${bank.bankId}</span>
					<span id="bankname_${status1.index+1}" class="bankid">${bank.bankName} </span>
					<ul> 
						<c:forEach items="${BANK_ACCOUNT_DETAILS}" var="account" varStatus="status"> 
							<c:choose>
								<c:when test="${bank.bankId eq account.bank.bankId}"> 
									<span style="display: none;" id="accountid_${status.index+1}">${account.bankAccountId}</span>
									<span style="display: none;" id="accountNO_${status.index+1}">${account.accountNumber}</span>
									<span style="display: none;" id="accountcombinationid_${status.index+1}">${account.combination.combinationId}</span>
									<li id="accountnumber_${status.index+1}" class="accountid width30" style="cursor: pointer;">${account.accountNumber}
										<c:if test="${account.chequeBooks ne null && account.chequeBooks ne '' && fn:length(account.chequeBooks)>0}">
											<ul>
												<c:forEach items="${account.chequeBooks}" var="CHEQUES_INFO" varStatus="cstatus">  
													<span style="display: none;" id="chequeid_${CHEQUES_INFO.chequeBookId}">${CHEQUES_INFO.chequeBookId}</span>
													<c:choose>
														<c:when test="${CHEQUES_INFO.currentNo ne null && CHEQUES_INFO.currentNo ne ''}">
															<input type="hidden" id="currentChqNo_${CHEQUES_INFO.chequeBookId}" value="${CHEQUES_INFO.currentNo}"/>
														</c:when>
														<c:otherwise>
															<input type="hidden" id="currentChqNo_${CHEQUES_INFO.chequeBookId}" value="${CHEQUES_INFO.startingNo-1}"/>
														</c:otherwise>
													</c:choose> 
													<li id="cheque_${CHEQUES_INFO.chequeBookId}" class="chequeid width50" style="cursor: pointer;">${CHEQUES_INFO.chequeBookNo} (${CHEQUES_INFO.startingNo} - ${CHEQUES_INFO.endingNo})</li> 
													<span id="bankaccountselect_${CHEQUES_INFO.chequeBookId}" class="float-right" style="height: 30px; width:20px; position: relative ;right:70px; top:-31px; "></span>
												 </c:forEach>
											</ul>
										</c:if> 
									</li> 
								</c:when>
							</c:choose> 
						</c:forEach> 
					</ul>
				</li>	
			</c:forEach> 
		</c:when>
	</c:choose>		
</ul>  
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right close" style="cursor:pointer;">close</div>  
</div>  
</div>  