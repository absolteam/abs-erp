<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${DELIVERY_NOTE_BYID.referenceNumber}
	</td>  
	<td>
		<c:set var="deliveryDate" value="${DELIVERY_NOTE_BYID.deliveryDate}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("deliveryDate").toString())%> 
	</td> 
	<td style="display: none;">
		 ${DELIVERY_NOTE_BYID.totalDeliveryQty}
	</td>
	<td style="text-align: right;" id="addtionalCharges_${rowId}">
		${DELIVERY_NOTE_BYID.addtionalCharges} 
	</td> 
	<td style="text-align: right;" id="invAmount_${rowId}">
		${DELIVERY_NOTE_BYID.totalDeliveryAmount} 
	</td> 
	<td id="deliveryDescription_${rowId}">
		  ${DELIVERY_NOTE_BYID.description}
	</td> 
	 <td class="opn_td" id="option_${rowId}"> 
	  <input type="hidden" id="salesDeliveryNoteId_${rowId}" value="${DELIVERY_NOTE_BYID.salesDeliveryNoteId}"/>
	  <input type="hidden" id="deliveryAmount_${rowId}" value="${DELIVERY_NOTE_BYID.totalDeliveryAmount}"/>
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv" id="EditImage_${rowId}" style="cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
	  </a> 
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowinv" id="DeleteImage_${rowId}" style="cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
	  </a> 
	</td>  
</tr>