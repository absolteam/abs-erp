<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style>
#DOMWindow {
	width: 18% !important;
	height: 10% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
	top: 150px!important;;
}
</style>
<div id="hrm" class="width100 float-left">
	<div>
		<label class="width30">Employee Id</label> <input type="text"
			id="employeeId" class="width50" />
	</div>
	<div>
		<label class="width30">PIN Number</label> <input type="password"
			id="secretPin" class="width50" maxlength="4"/>
	</div>
	<div style="position: relative; top: 2px;">
		<span style="color: red; font-size: 10px; margin: 0px;" class="pinerror"></span>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$("#employeeId,#secretPin").click(function(){
 		$(this).val('');
	});
	$("#employeeId").keyup(function(){
 		var contentLength = $(this).val().length;
		if(contentLength > 3)
			$('#secretPin').focus();
	});
	$("#secretPin").keyup(function(){
 		var employeeId = $("#employeeId").val();
 		var contentLength1 = $(this).val().length;
		if(contentLength1 > 3){
			var employeeSecretPin = $(this).val();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/validate_person_secretpin.action", 
				data:{employeeId: employeeId,  employeeSecretPin: employeeSecretPin},
				async: false,
				dataType: "json",
				cache: false,
				success:function(response){  
					if(response.returnMessage == "SUCCESS"){
						$('#personId').val(response.posPersonId);
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#secret-pin-validate').remove(); 
						$('.pos-receipt').trigger('click'); 
					}else{
						$("#secretPin").val('');
						$('.pinerror').hide().html(response.returnMessage).slideDown();
						$('.pinerror').delay(1600).slideUp();
					} 
				}
			}); 
		}
	});
});
</script>