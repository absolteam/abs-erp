 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			eibor details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="banklist" class="autoComplete width50">
	     	<option value=""></option> 
	     	<c:forEach items="${EIBOR_DETAILS}" var="bean" varStatus="status">
	     		<option value="${bean.eiborId}@@${bean.eiborRate}">${bean.eiborTypes}</option> 
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 eibor_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width30 float-left"><span>EIBOR Type</span></li>  
   				<li class="width30 float-left"><span>EIBOR Date</span></li>  
   				<li class="width30 float-left"><span>EIBOR Rate</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${EIBOR_DETAILS}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="eibor_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.eiborId}" id="eiborbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.eiborTypes}" id="eibornameid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.eiborRate}" id="eiborrateid_${status.index+1}"> 
		     			<span id="eiborname_${status.index+1}" style="cursor: pointer;" class="width30 float-left">${bean.eiborTypes}</span>
		     			<span id="eibordate_${status.index+1}" style="cursor: pointer;" class="width30 float-left">${bean.eiborDate}</span>
		     			<span id="eiborrate_${status.index+1}" style="cursor: pointer;" class="width30 float-left">${bean.eiborRate}    
		     				<span id="eiborNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript">
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var idvar=$(this).val();
	    	   var eiborval=idvar.split('@@'); 
	    	   $('#eiborId').val(eiborval[0]);  
	    	   $('#eiborRate').val(eiborval[1]);  
		       $('#eiborType').val($(".autoComplete option:selected").text());
		       $('#common-popup').dialog("close"); 
	   } 
	}); 
	 
	 $('.eibor_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.eibor_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#eiborNameselect_'+rowid).hasClass('selected')){ 
					 $('#eiborNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#eiborNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.eibor_list_li').live('dblclick',function(){
			var tempvar="";var idarray = ""; var rowid="";
			 currentId=$(this);  
			 tempvar=$(currentId).attr('id');   
			 idarray = tempvar.split('_'); 
			 rowid=Number(idarray[1]);  
			 $('#eiborId').val($('#eiborbyid_'+rowid).val());
			 $('#eiborRate').val($('#eiborrateid_'+rowid).val()); 
			 $('#eiborType').val($('#eibornameid_'+rowid).val());  
			 $('#common-popup').dialog('close'); 
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});
 });
 </script>
 <style>
	 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;}
	.ui-autocomplete{
		width:194px!important;
	} 
	.eibor_list ul li{
		padding:3px;
	}
	.eibor_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>