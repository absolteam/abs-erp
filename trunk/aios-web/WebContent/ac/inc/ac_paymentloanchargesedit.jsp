<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var tempid="";
$(function(){
	//Combination pop-up config
    $('.codecombination-popup').click(function(){
        tempid=$(this).parent().get(0);  
        $('.ui-dialog-titlebar').remove(); 
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/combination_treeview.action",
             async: false, 
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.codecombination-result').html(result); 
             },
             error:function(result){
                  $('.codecombination-result').html(result);
             }
         }); 
    }); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
	 });

     $('.psave').click(function(){   
 		var paymentId=$('#paymentId').val();
 		var paymentNumber=$('#paymentNumber').val();
 		var paymentDate=$('#paymentDate').val();
 		var receiveId=Number(0);
 		var paymentTermId=Number(0);  
 		var invoiceNumber="";
 		var loanId=Number($('#loanId').val());
 		paymentDetails=getPaymentDetails(); 
 		if($("#paymentValidation").validationEngine({returnIsValid:true})){   
 		 $.ajax({
 				type:"POST",
 				url:'<%=request.getContextPath()%>/payment_save.action',
 			 	async: false,
 			 	data:{	
 				 		paymentId:paymentId, paymentNumber:paymentNumber, paymentDate:paymentDate, loanId:loanId, receiveId:receiveId, paymentTermId:paymentTermId,
 				 		invoiceNumber:invoiceNumber,paymentDetails:paymentDetails
 				 	},
 			    dataType: "html",
 			    cache: false,
 				success:function(result){
 					 $(".tempresult").html(result);
 					 var message=$('.tempresult').html(); 
 					 if(message.trim()=="SUCCESS"){
 						 $.ajax({
 								type:"POST",
 								url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
 							 	async: false,
 							    dataType: "html",
 							    cache: false,
 								success:function(result){
 									$('#codecombination-popup').dialog('destroy');		
 				  					$('#codecombination-popup').remove();
 									$("#main-wrapper").html(result); 
 									$('#success_message').hide().html(message).slideDown(1000);
 								}
 						 });
 					 }
 					 else{
 						 $('#page-error').hide().html(message).slideDown(1000);
 						 return false;
 					 }
 				}
 			 });
 		} 
 		else{
 		 return false;
 	 	}
 	 });

 	 var getPaymentDetails=function(){
 		 var receiveLines=new Array(); 
 		 var productLines=new Array(); 
 		 var unitLines=new Array(); 
 		 var loanCharges=new Array();
 		 var combination=new Array();
 		 var productId="";
 		 var receiveQty=""; 
 		 var unitrateQty="";
 		 var loanChargeId=""; 
 		 var combinationId="";
 		 paymentDetails=""; 
 		 $('.rowid').each(function(){
 		 	var rowId=getRowId($(this).attr('id')); 
 			productId=0;
 			receiveQty=0; 
 			unitrateQty=0;
 			loanChargeId=$('#otherChargesId_'+rowId).val(); 
 			combinationId=$('#otherChargesCombinationId_'+rowId).val(); 
 			 if(typeof loanChargeId!='undefined' &&
 					typeof combinationId!='undefined' && loanChargeId!='' && combinationId!=''){ 
 				receiveLines.push(receiveQty); 
 				productLines.push(productId);
 				unitLines.push(unitrateQty);  
 				loanCharges.push(loanChargeId);
 				combination.push(combinationId);
 			 }  
 		  }); 
 		  for(var j=0;j<receiveLines.length;j++){ 
 			paymentDetails+=productLines[j]+"@"+receiveLines[j]+"@"+unitLines[j]+"@"+loanCharges[j]+"@"+combination[j];
 			if(j==receiveLines.length-1){   
 			} 
 			else{
 				paymentDetails+="@@";
 			}
 		 }  
 		 return paymentDetails;
 	 };

     $('.pdiscard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove();   
					$("#main-wrapper").html(result);  
				}
			});
	 });
     $('#paymentDate').datepick({ 
 	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
});
function setCombination(combinationTreeId,combinationTree){
   var tempvar =$(tempid).attr('id');	
   var idVal=tempvar.split("_");
   var rowid=Number(idVal[1]); 
   $('#otherChargesCombinationId_'+rowid).val(combinationTreeId); 
   $('#otherChargesCode_'+rowid).html(combinationTree);  
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>payment entry</div>
		 <c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Comment From   :<span> ${COMMENT_IFNO.person.firstName} ${COMMENT_IFNO.person.lastName}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		  <form name="paymentValidation" id="paymentValidation"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">    
					<input type="hidden" id="paymentId" name="paymentId" value="${PAYMENT_INFO.paymentId}"/>
					<div class="width48 float-right"> 
						<fieldset>
							<legend>charges Detail</legend>  
							<div class="width100 float-left">
								<label class="width30">Account Number</label> 
								<span>${PAYMENT_INFO.loan.bankAccount.accountNumber} (${PAYMENT_INFO.loan.bankAccount.bank.bankName})</span>
							</div> 
							<div class="width100 float-left">
								<label class="width30">Currency</label> 
								<span>${PAYMENT_INFO.loan.currency.currencyPool.code} </span>
							</div>
							<div class="width100 float-left">
								<label class="width30">Loan Amount</label> 
								<span>${PAYMENT_INFO.loan.amount}</span>
							</div>  
							<div class="width100 float-left">
								<label class="width30">Maturity Date</label> 
								<c:set var="mdate" value="${PAYMENT_INFO.loan.maturityDate}"/>  
								<% 
									String mdate=DateFormat.convertDateToString(pageContext.getAttribute("mdate").toString());
								%>
								<span><%=mdate%></span>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset>
							<legend>charges detail</legend> 
							<div class="width100 float-left">
								<label class="width30">Payment Number</label> 
								<span>${PAYMENT_INFO.paymentNumber}</span> 
							</div> 
							<div class="width100 float-left">
								<label class="width30">Payment Date</label> 
								<c:set var="pdate" value="${PAYMENT_INFO.date}"/>  
								<% 
									String pdate=DateFormat.convertDateToString(pageContext.getAttribute("pdate").toString());
								%>
								<input type="text" readonly="readonly" name="paymentDate"  value="<%=pdate%>" id="paymentDate" class="width50 validate[required]"/>
							</div> 
							<div class="width100 float-left">
								<label class="width30">Loan Number</label> 
								<input type="hidden" id="loanId" name="loanId" value="${PAYMENT_INFO.loan.loanId}"/> 
								<span>${PAYMENT_INFO.loan.loanNumber}</span>
							</div>  
							<div class="width100 float-left">
								<label class="width30">Approval Date</label> 
								<c:set var="sdate" value="${PAYMENT_INFO.loan.sanctionDate}"/>  
								<% 
									String sdate=DateFormat.convertDateToString(pageContext.getAttribute("sdate").toString());
								%>
								<span><%=sdate%></span>
							</div>  
						</fieldset>
					</div> 
				</div>   
		</div>    
		<div class="clearfix"></div> 
		<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
 				<legend>Payment Details</legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100" >   
						 <table id="hastab" class="width100"> 
							<thead>
							   <tr>  
							   		<th style="width:5%">Name</th>  
								    <th style="width:5%">Amount</th> 
								    <th style="width:5%">Combination</th>  
							  </tr>
							</thead> 
							<tbody class="tab"> 
								<c:forEach var="bean" items="${PAYMENT_DETAILS}" varStatus="status"> 
									<tr class="rowid" id="fieldrow_${status.index+1}"> 
										<td>${bean.loanCharge.name}</td>
										<td>${bean.loanCharge.value}</td>
										<td>
											<span class="otherChargesCode width50" id="otherChargesCode_${status.index+1}">
												${bean.loanCharge.combination.accountByNaturalAccountId.account} [${bean.loanCharge.combination.accountByNaturalAccountId.code}]
											</span>
											<span class="button float-right width20" id="otherchargescode_${status.index+1}"  style="position: relative; top:6px;">
												<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
													<span class="ui-icon ui-icon-newwin"></span> 
												</a>
											</span> 
											<input type="hidden" name="otherChargesCombinationId" id="otherChargesCombinationId_${status.index+1}"
												value="${bean.loanCharge.combination.combinationId}"/> 
										</td>
										<td style="display: none;">
											<input type="hidden" name="otherChargesId" id="otherChargesId_${status.index+1 }" value="${bean.loanCharge.otherChargeId}"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
					</table>
				</div> 
			</fieldset>
		</div>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right pdiscard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right psave" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
  </form>
  </div> 
</div>