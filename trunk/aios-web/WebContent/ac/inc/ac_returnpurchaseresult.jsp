 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<div id="purchase_detail">
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			purchase details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="plist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${PURCHASE_INFO}" var="bean" varStatus="status">  
	     		<option value="${bean.purchaseId}|${bean.supplier.supplierNumber}|${bean.supplier.person.lastName}${bean.supplier.cmpDeptLocation.company.companyName}|${bean.creditTerm.name}">${bean.purchaseNumber}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 purchase_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width20 float-left"><span>Purchase No</span></li>  
   				<li class="width20 float-left"><span>Supplier No</span></li>  
   				<li class="width25 float-left"><span>Supplier Name</span></li>  
   				<li class="width25 float-left"><span>Payment Term</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${PURCHASE_INFO}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="purchase_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.purchaseId}" id="purchasebyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.purchaseNumber}" id="purchasenumberbyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.supplier.supplierNumber}" id="purchasesuppliernumberbyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.supplier.person.firstName} ${bean.supplier.person.lastName}${bean.supplier.cmpDeptLocation.company.companyName}"
		     				 id="purchasesuppliernamebyid_${status.index+1}">
		     			<input type="hidden" value="${bean.creditTerm.name}" id="purchasetermbyid_${status.index+1}">  
		     			<span id="purchsebynumber_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.purchaseNumber}</span> 
		     			<span id="purchasebysuppliernumber_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.supplier.supplierNumber}</span>
		     			<span id="purchasebysupplier_${status.index+1}" style="cursor: pointer;" class="float-left width30">
		     				${bean.supplier.person.firstName} ${bean.supplier.person.lastName}${bean.supplier.cmpDeptLocation.company.companyName}</span>
		     			<span id="purchasebyterm_${status.index+1}" style="cursor: pointer;" class="float-left width25">${bean.creditTerm.name}
		     				<span id="purchaseNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
		<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	 </div>
 </div>  
 </div> 
 <script type="text/javascript"> 
 $(function(){  
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open');  
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var purchasedetails=$(this).val();
		       var purchasearray=purchasedetails.split("|"); 
		       $('#purchaseNumber').val($(".autoComplete option:selected").text());
		       $('#purchaseId').val(purchasearray[0]);  
		       $('#supplierNumber').val(purchasearray[1]); 
		       $('#supplierName').val(purchasearray[2]);  
		       $('#termName').val(purchasearray[3]);   
		       getAllPurchases($('#purchaseId').val()); 
		       $('.goodsreturns').show();
		       $('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.purchase_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.purchase_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#purchaseNameselect_'+rowid).hasClass('selected-')){ 
					 $('#purchaseNameselect_'+rowid).removeClass('selected-');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#purchaseNameselect_'+rowid).addClass('selected-');
		});
	 
	 $('.purchase_list_li').live('dblclick',function(){ 
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#purchaseId').val($('#purchasebyid_'+rowid).val());
		  $('#purchaseNumber').val($('#purchasenumberbyid_'+rowid).val());  
		  $('#supplierNumber').val($('#purchasesuppliernumberbyid_'+rowid).val()); 
		  $('#supplierName').val($('#purchasesuppliernamebyid_'+rowid).val());  
		  $('#termName').val($('#purchasetermbyid_'+rowid).val());   
		  if($('#purchaseId').val()>0){
			  getAllPurchases($('#purchaseId').val());
			  $('.goodsreturns').show();
		  }
		  $('#common-popup').dialog("close");   
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 }); 
function getAllPurchases(purchaseId){
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchase_detail.action", 
		 	async: false, 
		 	data:{purchaseId:purchaseId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
		 		$(".tab").html(result);  
			} 
		}); 
}
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.purchase_list ul li{
		padding:3px;
	}
	.purchase_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected-{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>