<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="mainhead portlet-header ui-widget-header width98">
	<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>alter
	finished goods
</div>
<div class="width80 float-left" id="hrm">
	<div>
		<label class="width30" for="processQuantity">Process Quantity<span
			style="color: red;">*</span> </label> <input type="text" id="processQuantity"
			name="processQuantity" value="${requestScope.processQuantity}"
			class="width50 validate[required,custom[number]]" />
	</div>
	<div>
		<label class="width30">Comment<span style="color: red;">*</span>
		</label>
		<textarea id="comment" name="comment"
			class="width51 validate[required]"></textarea>
	</div>
</div>
<div
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
	style="margin: 10px;"> 
	<div class="portlet-header ui-widget-header float-right discard"
		id="popup_discard" style="cursor: pointer;">
		<fmt:message key="accounts.common.button.cancel" />
	</div>
	<div
		class="portlet-header ui-widget-header float-right alteredfinishedgoods workinprocess_alteredsave"
		id="addAlteredfinishedgoods_${requestScope.alterFinishedGoods}"
		style="cursor: pointer;">Confirm & Save
	</div>
</div>