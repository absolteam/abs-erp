<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
var rewardPolicyId = 0; 
$(function(){ 
		
	$('input, select').attr('disabled', true); 
	  {
		  
		  $.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_reward_policy_entry.action", 
		     	async: false, 
		     	data:{rewardPolicyId: Number($('#rewardPolicyId').val())},
				dataType: "json",
				cache: false,
				success: function(response){ 
					 $('#policyName').val(response.rewardPolicyVO.policyName);
					 $('#reward').val(response.rewardPolicyVO.reward);
					 $('#rewardType').val(response.rewardPolicyVO.rewardType);
					 $('#couponName').val(response.rewardPolicyVO.couponVO.couponName);
					 $('#couponId').val(response.rewardPolicyVO.couponVO.couponId);
					 $('#percentage').attr('checked', response.rewardPolicyVO.isPercentage ? true : false);
					 $('#description').val(response.rewardPolicyVO.description);
				} 		
			});  
	  } 
	});
	 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>reward
			policy
		</div>
		<div class="portlet-content">
			<div id="success_message" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="tempresult" style="display: none;"></div>
			<div class="clearfix"></div>
			<form id="reward_policy_validation" style="position: relative;">
			<input type="hidden" id="rewardPolicyId" value="${requestScope.rewardPolicyId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 115px;">
								<div>
									<label for="percentage">Percentage </label> <input
										type="checkbox" id="percentage" name="percentage" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="policyName">Policy Name<span
										style="color: red;">*</span> </label> <input type="text"
										name="policyName" id="policyName" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="reward">Reward<span style="color: red;">*</span>
									</label> <input type="text" name="reward" id="reward" tabindex="3"
										class="width50 validate[required,custom[number]]" />
								</div>
								<div>
									<label for="rewardType">Reward Type<span
										style="color: red;">*</span> </label> <select name="rewardType"
										id="rewardType" class="width51 validate[required]"
										tabindex="4">
										<option value="">Select</option>
										<c:forEach var="rewardType" items="${REWARD_TYPES}">
											<option value="${rewardType.key}">${rewardType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="coupon">Coupon<span style="color: red;">*</span>
									</label> <input type="text" name="couponName" id="couponName"
										tabindex="5" class="width50 validate[required]"
										readonly="readonly" /> <input type="hidden" id="couponId"
										name="couponId" />
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="clearfix"></div> 