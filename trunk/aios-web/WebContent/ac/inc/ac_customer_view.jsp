<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
	.hr_main_content {
		width: 100% !important;
		overflow: hidden;
	}
</style> 
<script type="text/javascript"> 
var slidetab="";
var rowId="";
var accessCode = "";
$(function (){ 
 
	if(Number($('#customerId').val()>0)){
		//$('.savediscard').show();
		$('#customerCompany').attr('disabled',true);
		$('#customerBalance').attr('disabled',true);
		$('#customerPerson').attr('disabled',true); 
		
		if(Number($('#cmpDeptLocCustomerId').val())>0){ 
			$('#customerCompany').attr('checked',true);
			showCompany($('#companyCustomerId').val());
		}else{
			$('#customerPerson').attr('checked',true);
			showPerson($('#customerPersonId').val());
		}
		
		$('#memberCarType').val($('#tempmemberType').val());  
		$('#creditTermId').val($('#tempterm').val());  
		$('#customerAcType').val($('#tempcustomertype').val());
		$('#shippingMethod').val($('#tempshippingMethod').val());
		$('#refferalSource').val($('#temprefferalSource').val());
	} 
	 
	 
	$('.refferalsource-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	$('.shippingmethod-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="REFFERAL_SOURCE"){
			$('#refferalSource').html("");
			$('#refferalSource').append("<option value=''>Select</option>");
			loadLookupList("refferalSource"); 
		} else if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingMethod').html("");
			$('#shippingMethod').append("<option value=''>Select</option>");
			loadLookupList("shippingMethod"); 
		}  
	});
	  
	$('#customerCompany').click(function() { 
		$('#customerCompany').attr('disabled', true);
		$('#customerPerson').attr('disabled', true); 
		$('#company-selection-div').show();
		$('#person-selection-div').hide();
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/add_company.action",
			data: {companyId : 0, isCustomer: true, isSupplier: false}, 
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result);
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		identityShowHideCall(false);
		$('#companyTypeIdTemp').val("12,"+$('#companyTypeIdTemp').val());
		companyTypeAutoSelectd(); 
		$("#customer-result-div").html($("#customer-data-div").html());
		$("#customer-data-div").html("");
		$('#companyTypeId').attr('disabled', true);
		$('#customerinfo').val(true);
		 
	});
	
	$('input, select').attr('disabled', true);
	$('#customerPerson').click(function() { 
		$('#customerPerson').attr('disabled', true);
		$('#customerCompany').attr('disabled', true); 
		$('#company-selection-div').hide();
		$('#person-selection-div').show();
		$('#loading').fadeIn();
		var personId = 0;
		$.ajax({
			type: "POST", 
			url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
			data: {personId: personId, isCustomer: true, isSupplier: false},
	     	async: false,
			dataType: "html",
			cache: false,
			success: function(result) { 
				$("#sub-content").html(result); 
			},
			error: function(result) { 
				alert("error: " + result); 
			} 
		}); 
		
		identityShowHideCall(false);
		dependentShowHideCall(false);
		$('#personTypeIdTemp').val("8,"+$('#personTypeIdTemp').val());
		personTypeAutoSelectd();
		$('#customerinfo').val(true);
		$("#customer-result-div").html($("#customer-data-div").html());
		$("#customer-data-div").html("");
		$('#personType').attr('disabled', true);
 
	});

	 
	
	//pop-up config
	    $('#employeecall').click(function(){
	          $('.ui-dialog-titlebar').remove();  
	          $('#common-popup').dialog('open');
	          var rowId=-1; 
	          var personTypes="ALL";
	             $.ajax({
	                type:"POST",
	                url:"<%=request.getContextPath()%>/common_person_list.action",
	                data:{id:rowId,personTypes:personTypes},
	                async: false,
	                dataType: "html",
	                cache: false,
	                success:function(result){
	                     $('.common-result').html(result);
	                     return false;
	                },
	                error:function(result){
	                     $('.common-result').html(result);
	                }
	            });
	             return false;
	    }); 
	
	    $('#companycall').click(function(){
	          $('.ui-dialog-titlebar').remove();  
	          $('#common-popup').dialog('open');
	          var rowId=-1; 
	          var companyTypes="ALL";
	             $.ajax({
	                type:"POST",
	                url:"<%=request.getContextPath()%>/common_company_list.action",
	                data:{id:rowId,companyTypes:companyTypes},
	                async: false,
	                dataType: "html",
	                cache: false,
	                success:function(result){
	                     $('.common-result').html(result);
	                     return false;
	                },
	                error:function(result){
	                     $('.common-result').html(result);
	                }
	            });
	             return false;
	    }); 
	    $('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:450,
			bgiframe: false,
			modal: true 
		});
	    
	    
	    $('.person-popup').live('click',function(){ 
	        $('.ui-dialog-titlebar').remove(); 
	        $('#job-common-popup').dialog('open');
	           $.ajax({
	              type:"POST",
	              url:"<%=request.getContextPath()%>/get_employees_with_job.action", 
	              async: false,
	              dataType: "html",
	              cache: false,
	              success:function(result){
	                   $('.job-common-result').html(result);
	              },
	              error:function(result){
	                  alert(result);
	                   $('.job-common-result').html(result);
	              }
	          });
	           return false;
		  }); 
		
		  $('#job-common-popup').dialog({
		      autoOpen: false,
		      minwidth: 'auto',
		      width:800,
		      height:450,
		      bgiframe: false,
		      modal: true 
		  });
		  

	  $('.addrows').click(function(){ 
		  var i=Number(1);   
		  var id=Number(getRowId($(".customer-tab>.customer-rowid:last").attr('id')));  
		  id=id+1; 
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.customer-tab tr:last').before(result);
					 if($(".customer-tab").height()>255)
						 $(".customer-tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.customer-rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	  $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.customer-rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 });

	  $('.contactname').live('change',function(){
		  rowId=getRowId($(this).attr('id'));
		  triggerAddRow(rowId);
	   });

	  $('.contactperson').live('change',function(){
		  rowId=getRowId($(this).attr('id'));
		  triggerAddRow(rowId);
	  });

	  $('#memberCarType').live('change',function(){ 
		  var tempmemberType = $('#tempmemberType').val();
		  var cardType = $(this).val(); 
 		  $('#cardNumber').val('');
		  if(cardType!='' && tempmemberType!=cardType){  
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_member_card_number.action", 
				 	async: false,
				 	data:{cardType: cardType},
				    dataType: "json",
				    cache: false,
					success:function(response){ 
						if(null!=response.memberCardVO && Number(response.memberCardVO.updatedDigit) > 0)
							$('#cardNumber').val(response.memberCardVO.updatedDigit); 
						else
							$('#memberCarType').val('');
					}
			 });
		  }else
			 $('#cardNumber').val(cardType !='' ? $('#tempcardNumber').val() : ''); 
		  return false; 
	  });  
	 
});
function getRowId(id){   
	var idval=id.split('_'); 
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var name=$('#contactName_'+rowId).val();  
	 var person=$('#contactPerson_'+rowId).val(); 
	 var nexttab=$('#customer-fieldrow_'+rowId).next(); 
	if(name!=null && name!=""
			&& person!=null && person!=""  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click'); 
	}
}
function showCompany(companyId){
	$.ajax({
		type: "POST", 
		url:"<%=request.getContextPath()%>/add_company.action",
		data: {companyId : companyId, isCustomer: true}, 
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result) { 
			$("#sub-content").html(result);
		},
		error: function(result) { 
			alert("error: " + result); 
		} 
	}); 
	
	identityShowHideCall(false);
	$('#companyTypeIdTemp').val("12,"+$('#companyTypeIdTemp').val());
	companyTypeAutoSelectd();
	$('#companyTypeId').attr('disabled', true);
	$('#customerinfo').val(true);
	$("#customer-result-div").html($("#customer-data-div").html());
	$("#customer-data-div").html("");
}
function showPerson(personId){
	$.ajax({
		type: "POST", 
		url:"<%=request.getContextPath()%>/personal_details_add_redirect.action",
		data: {personId: personId, isCustomer: true},
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result) { 
			$("#sub-content").html(result); 
		},
		error: function(result) { 
			alert("error: " + result); 
		} 
	}); 
	
	identityShowHideCall(false);
	dependentShowHideCall(false);
	$('#personTypeIdTemp').val("8,"+$('#personTypeIdTemp').val());
	personTypeAutoSelectd();
	$('#personType').attr('disabled', true);
	$('#customerinfo').val(true);
	$("#customer-result-div").html($("#customer-data-div").html());
	$("#customer-data-div").html("");
}

function personPopupResult(personid,personname,commonParam){
	$("#customer-data-div").html($("#customer-result-div").html());
	showPerson(personid);
}
function companyPopupResult(companyId,companyName,commonParam){
	$("#customer-data-div").html($("#customer-result-div").html());
	showCompany(companyId);
}
 
</script> 
<div id="main-content">	 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			customer
		</div> 
			<div id="hrm" class="width100" style="margin:5px;">
				<fieldset><legend><fmt:message key="accounts.customer.customertype" /></legend> 
					<div class="float-left width30"><input name="customerType" id="customerCompany" type="radio"> <fmt:message key="accounts.customer.type.company" /></div>
					<div class="float-left width30"><input name="customerType" id="customerPerson" type="radio"> <fmt:message key="accounts.customer.type.person" /></div>
					<div class="float-left width40" id="person-selection-div" style="display:none;">
		                 <label class="width20">Person</label> 
	                </div>
	                <div class="float-left width40" id="company-selection-div" style="display:none;">
	                   	<label class="width20">Company</label> 
	                </div>
				</fieldset> 
			</div>
			<div class="width100 float-left" id="hrm">
				<div id="customer-data-div" style="display:none;">
						<input name="customerId" id="customerId" type="hidden" value="${CREDIT_TERM_INFORMATION.customerId}"> 
						<input name="customerPersonId" id="customerPersonId" type="hidden" value="${CREDIT_TERM_INFORMATION.personByPersonId.personId}"> 
					  
					<c:choose>
						<c:when test="${CREDIT_TERM_INFORMATION.company ne null}">
							<input name="companyCustomerId" id="companyCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.company.companyId}"> 
							<input name="cmpDeptLocCustomerId" id="cmpDeptLocCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.company.companyId}"> 
						</c:when>
						<c:otherwise>
							<input name="companyCustomerId" id="companyCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.company.companyId}"> 
							<input name="cmpDeptLocCustomerId" id="cmpDeptLocCustomerId" type="hidden" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.cmpDeptLocId}"> 
							<input type="hidden" id="departmentID" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.department.departmentId}"/> 
							<input type="hidden" id="locationID" value="${CREDIT_TERM_INFORMATION.cmpDeptLocation.location.locationId}"/> 
						</c:otherwise>
					</c:choose>
						 <form id="customer_details" name="customer_details" style="position: relative;">
							<div class="width48 float-right">
								<fieldset>  
									<div>
										<label class="width30"><fmt:message key="accounts.customer.creditlimit"/></label>
										<input type="text" name="creditLimit" class="width50 validate[optional,custom[number]]" id="creditLimit" value="${CREDIT_TERM_INFORMATION.creditLimit}"/>
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.salerepresentative"/></label>
										 <input type="text" name="employeeName" class="width50" id="employeeName" 
										 		value="${CREDIT_TERM_INFORMATION.personBySaleRepresentative.firstName} ${CREDIT_TERM_INFORMATION.personBySaleRepresentative.lastName}"/>
										   <input type="hidden" class="saleRepresentativeId" value="${CREDIT_TERM_INFORMATION.personBySaleRepresentative.personId}"/> 
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.referalsource" /></label>
										<select name="refferalSource" id="refferalSource" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CUSTOMER_REFFERAL_SOURCE ne null && CUSTOMER_REFFERAL_SOURCE ne '' }">
													<c:forEach items="${CUSTOMER_REFFERAL_SOURCE}" var="refferalSource">
														<option value="${refferalSource.lookupDetailId}">${refferalSource.displayName}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select> 
										<input type="hidden" id="temprefferalSource" value="${CREDIT_TERM_INFORMATION.lookupDetailByRefferalSource.lookupDetailByRefferalSource}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.shippingmethods" /></label>
										<select name="shippingMethod" id="shippingMethod" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${SHIPPING_METHOD ne null && SHIPPING_METHOD ne '' }">
													<c:forEach items="${SHIPPING_METHOD}" var="shippingMethod">
														<option value="${shippingMethod.lookupDetailId}">${shippingMethod.displayName}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select> 
										<input type="hidden" id="tempshippingMethod" value="${CREDIT_TERM_INFORMATION.lookupDetailByShippingMethod.lookupDetailByRefferalSource}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.openingBalance" /></label>
										<input type="text" name="openingBalance" class="width50 validate[optional,custom[number]]" id="openingBalance" value="${CREDIT_TERM_INFORMATION.openingBalance}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.common.description"/></label>
										<textarea id="customerDescription" class="width51">${CREDIT_TERM_INFORMATION.description}</textarea>
									</div>
								</fieldset>
							</div>
							<div class="width50 float-left">
								<fieldset style="min-height: 141px;"> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.customerno" /><span style="color:red">*</span></label> 
										<c:choose>
											<c:when test="${CREDIT_TERM_INFORMATION.customerId >0}">
												<input type="text" name="customerNumber" readonly="readonly" class="width50 validate[required]" 
													id="customerNumber" value="${CREDIT_TERM_INFORMATION.customerNumber}"/>
											</c:when>
											<c:otherwise>
												<input type="text" name="customerNumber" readonly="readonly" class="width50 validate[required]" 
													id="customerNumber" value="${CUSTOMER_NUMBER}"/>
											</c:otherwise>
										</c:choose>
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.customertype" /><span style="color:red">*</span></label>
										<select name="customerAcType" id="customerAcType" class="width51 validate[required]">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CUSTOMER_TYPE_LIST ne null && CUSTOMER_TYPE_LIST ne '' }">
													<c:forEach items="${CUSTOMER_TYPE_LIST}" var="CUSTOMER_TYPE">
														<option value="${CUSTOMER_TYPE.key}">${CUSTOMER_TYPE.value}</option>
													</c:forEach>
												</c:when>
											</c:choose> 
										</select>
										<input type="hidden" id="tempcustomertype" value="${CREDIT_TERM_INFORMATION.customerType}"/>
									</div> 
									<div>
										<label class="width30"><fmt:message key="accounts.customer.billingaddress" /><span style="color:red">*</span></label> 
										<input type="text" class="width50 validate[required]" id="billingAddress" value="${CREDIT_TERM_INFORMATION.billingAddress}"/>
									</div> 
									<div>
										<label class="width30">Card Type</label> 
										<select name="memberCarType" id="memberCarType" class="width51">
											<option value="">Select</option> 
											<c:forEach items="${MEMBER_CARD_TYPES}" var="memberCarType">
												<option value="${memberCarType.key}">${memberCarType.value}</option>
											</c:forEach> 
										</select>
										<input type="hidden" id="tempmemberType" value="${CREDIT_TERM_INFORMATION.memberType}"/>
									</div>
									<div>
										<label class="width30">Card Number</label>  
										<input type="text" id="cardNumber" value="${CREDIT_TERM_INFORMATION.memberCardNumber}"
											class="width50" readonly="readonly"/>
										<input type="hidden" id="tempcardNumber" value="${CREDIT_TERM_INFORMATION.memberCardNumber}"
											readonly="readonly"/>	
									</div>
									<div>
										<label class="width30"><fmt:message key="accounts.customer.creditterm"/></label>
										<select name="creditTermId" id="creditTermId" class="width51">
											<option value="">Select</option>
											<c:choose>
												<c:when test="${CREDIT_TERM_LIST ne null && CREDIT_TERM_LIST ne '' }">
													<c:forEach items="${CREDIT_TERM_LIST}" var="CREDIT_TERM">
														<option value="${CREDIT_TERM.creditTermId}">${CREDIT_TERM.name}</option>
													</c:forEach>
												</c:when>
											</c:choose>
										</select>
										<input type="hidden" id="tempterm" value="${CREDIT_TERM_INFORMATION.creditTerm.creditTermId}"/>
									</div> 
							</fieldset>
							</div>
							<div class="clearfix"></div> 
							<div id="hrm" class="hastable width100" style="position: relative; top: 3px; bottom: 5px;">
							<fieldset> 
								<legend><fmt:message key="accounts.creditterm.shippingdetails"/></legend>
									<table id="hastab" class="width100">
										<thead>
											<tr> 
												<th><fmt:message key="accounts.creditterm.shippingname" /></th>
												<th><fmt:message key="accounts.creditterm.contactperson" /></th>
												<th><fmt:message key="accounts.creditterm.contactno" /></th>
												<th><fmt:message key="accounts.creditterm.address" /></th> 
												<th style="width: 0.01%;display: none;"><fmt:message key="accounts.common.label.options" /></th>
											</tr>
										</thead>
										<tbody class="customer-tab">
											<c:forEach var="SHIPPING_DETAIL" items="${CREDIT_TERM_INFORMATION.shippingDetails}" varStatus="status">
												<tr class="customer-rowid" id="customer-fieldrow_${status.index+1}"> 
													<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td>
													<td>
														<input type="text" id="contactName_${status.index+1}" value="${SHIPPING_DETAIL.name}" class="contactname"/>
													</td>
													<td>
														<input type="text" id="contactPerson_${status.index+1}" value="${SHIPPING_DETAIL.contactPerson}" class="contactperson"/>
													</td>
													<td>
														<input type="text" id="contactNo_${status.index+1}" value="${SHIPPING_DETAIL.contactNo}"/>
													</td>
													<td>
														<input type="text" id="customeraddress_${status.index+1}" value="${SHIPPING_DETAIL.address}"/>
													</td>
													<td style="display: none;">
														<input type="hidden" id="shippingId_${status.index+1}" value="${SHIPPING_DETAIL.shippingId}"/>
													</td>
													<td style="width:0.01%;display: none;" class="opn_td" id="option_${status.index+1}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
													</td>   
												</tr>
											</c:forEach>  
										</tbody>
									</table>
								</fieldset>	
							</div>
						</form>
					</div> 
				</div>
	<div class="clearfix"></div> 
	<div id="sub-content" class="width100 float-left"> 
	</div> 
	<div class="clearfix"></div>  
  </div>
</div> 
<div class="clearfix"></div>  