<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.divstyle {
	padding: 3px;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
	position: relative;
	top: 7px;
}

.divpadding {
	padding-bottom: 7px;
}
</style>
<script type="text/javascript">
var tempid="";
var slidetab="";
var requestDetails=""; 
var paymentRequestId = 0;
$(function(){ 
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	});  
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function callUseCaseApprovalBusinessProcessFromWorkflow(messageId,returnVO){
	var paymentRequestId=Number($('#paymentRequestId').val());
	if(returnVO.returnStatusName=='DeleteApproved')
		deletePaymentRequestApproval(paymentRequestId,messageId);
}
function deletePaymentRequestApproval(paymentRequestId,messageId){
	$.ajax({
		type: "POST",  
		url: "<%=request.getContextPath()%>/delete_paymentrequest.action",  
     	data: {paymentRequestId: paymentRequestId,messageId: messageId},  
     	async: false,
		dataType: "html",
		cache: false,
		success: function(result){  
			return false;
     	},
     	error:function(result){
     		return false;
        } 
	}); 
	return false;
} 
</script>
<div id="main-content">
	<div id="request_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> payment
			request
		</div> 
		<form name="requestvalidation" id="requestvalidation">
			<div class="portlet-content">
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="paymentRequestId" name="paymentRequestId"
					value="${PAYMENT_REQUEST.paymentRequestId}" />
				<div class="width100 float-left" id="hrm">
					<div class="tempresult" style="display: none;"></div>
					<div id="page-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div class="width45 float-left" id="hrm">
						<fieldset style="min-height: 80px;">
							<div class="divstyle">
								<label class="width20">Request No.
								</label> <span class="spanfont">${PAYMENT_REQUEST.paymentReqNo}</span>
							</div>
							<div class="divstyle">
								<label class="width20">Request Date
								</label>
								<c:set var="requestDate" value="${PAYMENT_REQUEST.requestDate}" />
								<span class="spanfont"> <%=DateFormat.convertDateToString(pageContext.getAttribute(
					"requestDate").toString())%>
								</span>
							</div>
							<div class="divstyle">
								<label class="width20">Person</label>
								<c:set var="personName" value="${PAYMENT_REQUEST.personName}"/>
								<c:if test="${PAYMENT_REQUEST.personByPersonId ne null && PAYMENT_REQUEST.personByPersonId.personId gt 0}">
									<c:set var="personName" value="${PAYMENT_REQUEST.personByPersonId.firstName} ${PAYMENT_REQUEST.personByPersonId.lastName}"/>
								</c:if>
								<span class="spanfont">${personName}</span>
							</div>
						</fieldset>
					</div>
					<div class="width45 float-right" id="hrm">
						<fieldset style="min-height: 80px;">
							<div class="divstyle" style="display: none;">
								<label class="width20 tooltip">Status<span
									class="mandatory">*</span>
								</label> <span> <c:set var="payrequest"
										value="${PAYMENT_REQUEST.status}" /> <%=Constants.Accounts.PaymentRequestStatus.get(Byte
					.valueOf(pageContext.getAttribute("payrequest").toString()))%>
								</span>
							</div> 
							<div class="divstyle">
								<label class="width20">Description</label>
								<span class="spanfont">${PAYMENT_REQUEST.description}</span>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="hrm" class="portlet-content width100 invoice_lines">
				<fieldset id="hrm">
					<legend>Payment Request Detail</legend>
					<div class="portlet-content">
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr> 
										<th style="width: 5%;">Category</th> 
										<th style="width: 3%;">Payment Mode</th>
										<th style="width: 3%;">Amount</th>
										<th style="width: 5%;">Description</th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${PAYMENT_REQUEST_DETAIL ne null && PAYMENT_REQUEST_DETAIL ne ''}">
											<c:forEach var="REQUEST_DETAIL"
												items="${PAYMENT_REQUEST_DETAIL}"
												varStatus="status">
												<tr class="rowid splrowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td>${REQUEST_DETAIL.lookupDetail.displayName}</td> 
													<td>${REQUEST_DETAIL.paymentModeStr}</td> 
													<td style="text-align: right;">
														${REQUEST_DETAIL.amount}</td> 
													<td>${REQUEST_DETAIL.description} <input type="hidden"
														id="paymentRequestId_${status.index+1}"
														value="${REQUEST_DETAIL.paymentRequestDetailId}" /></td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
		</form>
		<div class="width30 float-right"
			style="font-weight: bold; display: none;">
			<span>Total: </span> <span id="totalInvoiceAmount">${requestScope.totalAmount}</span>
		</div>
		<div class="clearfix"></div> 
	</div>
</div>