<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.quantityerr{ font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<script type="text/javascript">
var creditDetails = "";
$(function(){
	 
	 
	if($('#creditId').val()>0){
		$('.rowid').each(function(){
			var rowId=getRowId($(this).attr('id'));  
			$('#creditType_'+rowId).val($('#tempcreditType_'+rowId).val()); 
		});
	} 
    
	$('input,select').attr('disabled', true);
	$('.sales_delivery_popup').click(function(){  
 		$('.ui-dialog-titlebar').remove();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_delivery_note_popup.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		}); 
		return false; 
	});

	 $('#sales-delivery-popup').live('click',function(){  
    	 $('#common-popup').dialog('close'); 
     });
	

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.delrow').live('click',function(){ 
		 slidetab = $(this).parent().parent().get(0);  
     	 $(slidetab).remove();  
     	 var i=1;
     	 var trSize = $(".tab>tr").size();
     	 if(trSize > 0){
     		 $('.rowid').each(function(){   
       			var rowId=getRowId($(this).attr('id')); 
       		 	$('#lineId_'+rowId).html(i);
     			i=i+1; 
     		 });  
         }else {
        	 $('.credit_note_details').hide(); 
        	 { 
   				$('#currencyId').val('');
   				$('#currencyCode').val('');
  				$('#customerName').val('');
  				$('#salesDeliveryNumber').val('');
  				$('#customerNumber').val('');
 				$('#customerId').val('');
  			} 
         } 
	 }); 

	$('#credit_note_save').click(function(){  
		creditDetails = "";
		var creditId = Number($('#creditId').val());
		var creditNumber = $('#creditNumber').val();
		var creditDate = $('#creditDate').val();
		var currencyId = Number($('#currencyId').val());
		var customerId = Number($('#customerId').val());
		var salesDeliveryNoteId = Number($('#salesDeliveryNoteId').val());
	 	var description=$('#description').val(); 
	 	if($jquery("#creditnotesValidation").validationEngine('validate')){
 			creditDetails = getCreditDetails();   
 			if(creditDetails!=""){
				$.ajax({
					type:"POST",
					url:'<%=request.getContextPath()%>/credtnote_save.action',
				 	async: false,
				 	data:{  date: creditDate, creditId: creditId, creditNumber: creditNumber, currencyId: currencyId, customerId: customerId,
				 			salesDeliveryNoteId: salesDeliveryNoteId, description: description, creditDetails: creditDetails
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){ 
	 					 if(response.returnMessage=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getcredit_listdetails.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove();  
										$("#main-wrapper").html(result);
										if (creditId > 0)
											$('#success_message').hide().html("Record updated").slideDown(1000);
										else
											$('#success_message').hide().html("Record created").slideDown(1000);
										$('#success_message').delay(2000).slideUp();
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					}
				 });
			}else{
				$('#page-error').hide().html("Please enter credit details.").slideDown(1000);
				 $('#page-error').delay(2000).slideUp();
				 return false;
			} 
		} 
		else{
		 return false;
	 	}
	 });

	var getCreditDetails=function(){
		 var productArray=new Array();  
		 var inwardArray=new Array(); 
		 var inwardedArray = new Array();
 		 var descArray=new Array(); 
		 var deliveryArray = new Array();
		 var creditDetailArray = new Array();
		 var creditDetail="";
		 $('.rowid').each(function(){
		 	 var rowId = getRowId($(this).attr('id')); 
			 var productId = $('#productid_'+rowId).val(); 
			 var inwardQuantity = Number($('#inwardQuantity_'+rowId).val());
			 var inwardedQuantity  = Number($('#inwardedQuantity_'+rowId).text());
			 var description = $('#description_'+rowId).val();  
			 var salesDeliveryDetailId = Number($('#salesDeliveryDetailId_'+rowId).val());  
			 var creditDetaiId = Number(0);  
 			 if(inwardQuantity > 0){  
 				productArray.push(productId);  
 				inwardArray.push(inwardQuantity);
 				inwardedArray.push(inwardedQuantity);
 				deliveryArray.push(salesDeliveryDetailId);  
 				creditDetailArray.push(creditDetaiId);  
 				if(description!="")
 					descArray.push(description);
 				else
 					descArray.push("##");
			 }  
		  });  
		  for(var j=0; j < inwardArray.length; j++){  
			  creditDetail += inwardArray[j]+"__"+descArray[j]+"__"+deliveryArray[j]+"__"+creditDetailArray[j]+"__"+inwardedArray[j];
			if(j==inwardArray.length-1){   
			} 
			else{
				creditDetail += "#@";
			}
		 }   
 		 return creditDetail;
	 };

	 $('#credit_note_discard').click(function(){  
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcredit_listdetails.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#main-wrapper").html(result); 
			}
		 });
	 });

	 $('.inwardQuantity').live('change',function(){   
		 $('.quantityerr').remove(); 
 	 	 var rowId = getRowId($(this).attr('id')); 
 	 	 var inwardQty =  Number($('#inwardQuantity_'+rowId).val());
 	 	 var inwardedQty =  Number($('#inwardedQuantity_'+rowId).text());
 	 	 var deliveredQuantity = Number($('#deliveredQuantity_'+rowId).text());  
  	 	 if ((inwardQty + inwardedQty) > deliveredQuantity) {
 	 		$(this).parent().append("<span class='quantityerr' id=inverror_"+rowId+">Inward Qty should be lt or eq delivery qty.</span>"); 
 	 		$('#inwardQuantity_'+rowId).val($('#tempInwardQuantity_'+rowId).val());
 	 	 } 
 		 return false;
 	 });

 	 if($('#creditId').val()){  
 		$('.credit_note_details').show();
 	 }
	 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function loadDeliveryDetails(salesDeliveryNoteId){
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_sales_delivery_detail.action", 
	 	async: false,  
	 	data: {salesDeliveryNoteId: salesDeliveryNoteId},
	 	dataType: "json",
	    cache: false,
		success:function(response){ 
			{ 
				$('#salesDeliveryNoteId').val(response.salesDeliveryNoteVO.salesDeliveryNoteId);
  				$('#currencyId').val(response.salesDeliveryNoteVO.currencyId);
  				$('#currencyCode').val(response.salesDeliveryNoteVO.currencyCode);
 				$('#customerName').val(response.salesDeliveryNoteVO.customerName);
 				$('#salesDeliveryNumber').val(response.salesDeliveryNoteVO.referenceNumber);
 				$('#customerId').val(response.salesDeliveryNoteVO.customerId);
 			} 
			$('.tab').html('');
 			var counter = 0; 
 			var invoiceQuantity = '';
 			var unitRate = 0; 
 			$(response.salesDeliveryNoteVO.salesDeliveryDetailVOs)
			.each(
					function(index) {
						counter += 1; 
						var inwardedQuantity = '';
			 			var inwardQuantity = '';
						if(null!=response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].invoiceQuantity) { 
							invoiceQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].invoiceQuantity;
			 	 		} 
						unitRate = Number(response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].unitRate).toFixed(2);
						if(null!=response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardedQuantity) { 
							inwardedQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardedQuantity;
							inwardQuantity = response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].inwardQuantity;
							if(inwardQuantity == null)
								inwardQuantity = '';
 			 	 		} 
						$('.tab')
						.append(
						
							"<tr id='fieldrow_"+counter+"' class='rowid'>"
								+"<td id='lineId_"+counter+"' style='display:none;'>"+counter+"</td>"
								+"<td>"
									+"<input type='hidden' id='productid_"+counter+"'"
									+" value='"+response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].productId+"' name='productId'/>"
									+"<span id='product_"+counter+"'>"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].productName
									+"</span>"
								+"</td>" 
								+"<td>" 
									+"<input type='hidden' id='shelfId_"+counter+"'"
									+" value='"+response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].shelfId+"' name='shelfId'/>"
									+"<span id=store_"+counter+">"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].storeName
									+"</span>" 
								+"</td>"
								+"<td id='deliveredQuantity_"+counter+"'>"+ 
									response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].deliveredQuantity
								+"</td>"
								+"<td id='invoiceQuantity_"+counter+"'>"+ 
									invoiceQuantity
								+"</td>"
								+"<td>"
									+"<span style='float: right;' id='unitRate_"+counter+"'>"+
										unitRate
									+"</span>" 
								+"</td>"  
								+"<td>"+
									inwardedQuantity
								+"</td>"
								+"<td>"
									+"<input type='text' id='inwardQuantity_"+counter+"' name='inwardQuantity' value='"+inwardQuantity+"' class='inwardQuantity'>"
									+"<input type='hidden' id='tempInwardQuantity_"+counter+"' name='tempInwardQuantity' value='"+inwardQuantity+"' class='tempInwardQuantity'>"
								+"</td>"
								+"<td>"
									+"<input type='text' id='description_"+counter+"' name='description'>"
								+"</td>"
								+"<td>"
								   +"<a class='btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow' id='DeleteImage_"+counter+"'"
								   		+"style='cursor:pointer;' title='Delete Record'>"
										+"<span class='ui-icon ui-icon-circle-close'></span>"
								   +"</a>" 
									+"<input type='hidden' id='salesDeliveryDetailId_"+counter+"' value='"+
										response.salesDeliveryNoteVO.salesDeliveryDetailVOs[index].salesDeliveryDetailId
									+"' name='salesDeliveryDetailId'>"
									+"<input type='hidden' id='creditDetaiId_"+counter+"' name='creditDetaiId'>"
								+"</td>"
							+"</tr>"
					);
			});
			$('.credit_note_details').show(); 
		}
	});  
	return false;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>credit note</div>	
		  <form name="creditnotesValidation" id="creditnotesValidation" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div> 
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height: 80px;">   
							<div>
								<label class="width30">Customer Name<span style="color: red;">*</span></label>
								<input type="hidden" id="customerId" name="customerId"/> 
								<input type="hidden" id="currencyId" name="currencyId"/> 
								<input type="text" readonly="readonly" name="customerName"  id="customerName" class="width50"
								value="${CREDIT_INFO.customer.personByPersonId.firstName} ${CREDIT_INFO.customer.personByPersonId.lastName}${CREDIT_INFO.customer.cmpDeptLocation.company.companyName}${CREDIT_INFO.customer.company.companyName}"/>  
							</div> 
							<div>
								<label class="width30" for="description">Description</label> 
								<textarea class="width50" id="description">${CREDIT_INFO.description}</textarea>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height: 80px;">
 							<div>
								<label class="width30">Credit Number<span style="color: red;">*</span></label> 
								<input type="hidden" name="creditId" id="creditId"/>
								<input type="text" readonly="readonly" name="creditNumber" id="creditNumber" class="width50"
									 value="${CREDIT_INFO.creditNumber}"/>
 							</div>  
							<div>
								<label class="width30" for="creditDate">Date<span style="color: red;">*</span></label> 
								<input type="text" readonly="readonly" name="creditDate" id="creditDate" value="${CREDIT_INFO.date}"
								class="width50 validate[required]"/>
							</div> 
							<div>
								<label class="width30">Delivery Number<span style="color: red;">*</span></label>  
								<input type="hidden" id="salesDeliveryNoteId" name="salesDeliveryNoteId"/>
 							 	<input type="text" readonly="readonly" name="salesDeliveryNumber" value="${CREDIT_INFO.salesReference}"
 							 	id="salesDeliveryNumber" class="width50"/> 
								 
							</div>  
						</fieldset>
					</div> 
			</div>  
		</div>  
		<div class="clearfix"></div>  
		<div class="portlet-content class90 credit_note_details" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
				<legend>Credit Details<span
									class="mandatory">*</span></legend>
 				 
					<div id="hrm" class="hastable width100">   
						<table id="hastab" class="width100"> 
							<thead>
							   <tr>   
									<th class="width10">Product</th> 
								    <th class="width5">Store</th> 
								    <th class="width5">Deliverd Qty</th>   
								    <th class="width5">Unit Rate</th>  
									<th class="width5">Inward Qty</th>    
							  </tr>
							</thead> 
							<tbody class="tab"> 
								<c:forEach var="detail" items="${CREDIT_DETAIL_INFO}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td> 
										<td><span class="float-left width60"
											id="product_${status.index+1}">${detail.productName}</span>
										</td>
										<td>
										<span class="float-left width60" id="storeName_${status.index+1}">${detail.shelfName}</span>
										</td>  
										<td>${detail.deliveryQuantity}</td>
										<td>${detail.unitRate} </td>
										<td>${detail.returnQuantity}</td> 
									</tr>
								</c:forEach>
							</tbody>
						</table> 
					</div> 
			</fieldset>
		</div> 
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right discard"
				id="credit_note_discard" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right save"
				id="credit_note_save" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
 		</div>
		<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div>  
		<div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
  	</form>
  </div> 
</div>