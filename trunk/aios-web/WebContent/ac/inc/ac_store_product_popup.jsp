<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
 var oTable; var selectRow=""; var aSelected = []; var aData="";
	$(function() {
		oTable = $('#ProductStoreList')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_store_product_jsonlist.action", 
							"aoColumns" : [  {
								"mDataProp" : "productName"
							 },{ 
								"mDataProp" : "storeName"
							 },{
								"mDataProp" : "unitRate" 
							 },{
								"mDataProp" : "availableQuantity" 
							 } ], 
							 "sScrollY": $("#main-content").height() - 235,
						});

		/* Click event handler */
		$('#ProductStoreList tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					commonProductStockPopup(aData, $(
							'#currentRowId').val());
					$('#product-stock-popup').trigger('click');
				});

		/* Click event handler */
		$('#ProductStoreList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
		</div>
		<div class="portlet-content">
			<div id="product_json_list">
				<table id="ProductStoreList" class="display">
					<thead>
						<tr>
 							<th>Product</th>
							<th>Store</th>
							<th>Unit Rate</th>
							<th>Quantity</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="product-stock-popup">close</div>
			</div>
		</div>
		<input type="hidden" id="currentRowId" value="${requestScope.rowId}" />
		<input type="hidden" id="productId"
			value="${requestScope.productId}" />
	</div>
</div>