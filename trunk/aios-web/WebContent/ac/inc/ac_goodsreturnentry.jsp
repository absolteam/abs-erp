<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style type="text/css">
.alignDiv {
	padding: 3px 0 3px 0;
}
</style>
<script type="text/javascript">
var slidetab = "";
var returnDetails = "";
$(function(){
	
	$jquery("#returnnotesValidation").validationEngine('attach');  
	
	$('.delrow').click(function(){ 
		slidetab=$(this).parent().parent().get(0);  
        $(slidetab).remove();  
      	var i=1;
   		$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 }); 
	 });

	$('.packageUnit').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		$('#baseUnitConversion_'+rowId).text('');
		$('#quantity_'+rowId).val('');
		var packageUnit = Number($(this).val()); 
		var packageType = Number($('#packageType_'+rowId).val()); 
		if(packageType == -1 && packageUnit > 0){
			$('#quantity_'+rowId).val(packageUnit); 
			var quantity = Number($('#quantity_'+rowId).val());
			var unitrate = Number($('#unitrateH_'+rowId).val());
	 		if(quantity > 0 && unitrate > 0){ 
				var totalAmount = Number(quantity * unitrate); 
				$('#totalamount_'+rowId).text(Number(totalAmount).toFixed(3));
			}else{
				$('#totalamount_'+rowId).text('');
			} 
		}
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		} 
		return false;
	}); 
	
	$('.quantity').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var quantity=Number($(this).val());
		var total=Number(0);
		var unitrate=Number($('#unitrateH_'+rowId).val());  
		var packageType = Number($('#packageType_'+rowId).val());   
		if(quantity > 0 && packageType > 0){ 
			getProductConversionUnit(rowId);
		}else{
			$('#packageUnit_'+rowId).val(quantity); 
		}
		if(quantity>0 && unitrate>0 ){
			total=Number(unitrate*quantity).toFixed(3);
			$('#totalamount_'+rowId).text(total); 
		} 
		return false;
	}); 
	
	 $('.returnssave').click(function(){ 
		 if($jquery("#returnnotesValidation").validationEngine('validate')){  
 			 var returnId = Number($('#returnId').val());
  			 var supplierId = Number($('#supplierId').val()); 
 			 var returnDate = $('#returnDate').val(); 
 			 var description = $('#description').val(); 
 			 var grnReceiveId = Number($('#grnReceiveId').val());
			 returnDetails = getReturnDetails();   
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_goodsreturns.action", 
				 	async: false,
				 	data:{returnId: returnId, supplierId: supplierId, returnDate: returnDate, description: description,
				 			returnDetails: returnDetails, receiveId: grnReceiveId},
				    dataType: "json",
				    cache: false,
					success:function(response){
 						 if(response.returnMessage=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/getgoods_return.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove();   
										$("#main-wrapper").html(result); 
									}
							 });
							 if(returnId > 0)
								$('#success_message').hide().html("Record updated.").slideDown(1000); 
							 else
								 $('#success_message').hide().html("Record created.").slideDown(1000); 
							$('#success_message').hide().delay(2000);
						 }
						 else{
							 $('#returns-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#returns-error').hide().delay(2000);
							 return false;
						 }
					},
					error:function(result){  
						$('#returns-error').hide().html("System error.").slideDown(1000);
						$('#returns-error').hide().delay(2000);
					}
			 }); 
		 }
		 return false;
	 });
	 
	 $('.goodsreceive-common-popup').click(function(){  
			$('.ui-dialog-titlebar').remove(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_closed_receivenotes.action", 
			 	async: false,  
	 		    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);  
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0);
				} 
			});  
			return false;
		});
	 
	 $('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	var getReturnDetails = function(){
		returnDetails="";
		var product=new Array();
		var purchase= new Array();
 		var returndetail=new Array();
 		var quantity = new Array();
 		var receiveDetailArray = new Array();
 		var packageDetailArray = new Array();
 		var packageUnitArray = new Array();
 		$('.rowid').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var productId=Number($('#productid_'+rowId).val());
 			var returnQuantity=Number($('#quantity_'+rowId).val());
			var purchaseId=Number($('#purchaseId_'+rowId).val());  
			var returnLineId=Number($('#returnLineId_'+rowId).val()); 
			var receiveDetailId = Number($('#receiveDetailId_'+rowId).val()); 
			var packageDetailId = Number($(
					'#packageType_' + rowId).val());
			var packageUnit = Number($(
					'#packageUnit_' + rowId).val());
			if(typeof productId != 'undefined' && productId > 0){
				product.push(productId);
				quantity.push(returnQuantity);
				returndetail.push(returnLineId);
				purchase.push(purchaseId); 
				receiveDetailArray.push(receiveDetailId);
				packageDetailArray.push(packageDetailId);
				packageUnitArray.push(packageUnit);
			} 
		});
		for(var j=0;j<product.length;j++){ 
			returnDetails += product[j]+"__"+quantity[j]+"__"+returndetail[j]+"__"+purchase[j]+"__"+receiveDetailArray[j]+"__"+packageDetailArray[j]
				+"__"+packageUnitArray[j];
			if(j==product.length-1){   
			} 
			else{
				returnDetails+="#@";
			}
		} 
		return returnDetails;
	 };
	 
	$('.returnsdiscard').click(function(){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getgoods_return.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
					$("#main-wrapper").html(result);
				}
			});
		});
	
		var returnSize = Number($(".tab>tr").size());
		if(returnSize > 0){
			$('.goodsreturn_detail').show();
			$('#returnDate').datepick({showTrigger: '#calImg'}); 
		}else{
			$('#returnDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
		}
	});
	
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	
	function closedReceivePopup(aData){
		$('#supplierId').val(aData.supplierId);
		$('#supplierName').val(aData.supplierName);
		$('#grnNumber').val(aData.receiveNumber); 
		$('#grnReceiveId').val(aData.receiveId); 
		getRecevieNoteDetails(aData.receiveId);
	}
	
	function getRecevieNoteDetails(receiveId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receive_notes_details.action",
			async : false,
			dataType : "html",
			data:{receiveId: receiveId},
			cache : false,
			success : function(result) { 
				$(".tab").html(result);
				$('.returnedLabel').show();
				$('.goodsreturn_detail').show();
			}
		});
		return false;
	}
	
	function getProductBaseUnit(rowId){
		$('#baseUnitConversion_'+rowId).text('');
		var packageQuantity = Number($('#packageUnit_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		var basePrice = Number(0);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#quantity_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);  
 				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity); 
			} 
		});  
 		var unitrate = Number($('#unitrateH_'+rowId).val());
 		if(packageQuantity > 0 && unitrate > 0){ 
			var totalAmount = Number(packageQuantity * unitrate); 
			$('#totalamount_'+rowId).text(Number(totalAmount).toFixed(3));
		}else{
			$('#totalamount_'+rowId).text('');
		} 
		return false;
	}
	
	function getProductConversionUnit(rowId){
 		var packageQuantity = Number($('#quantity_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
 			} 
		});  
		return false;
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Goods
			Return
		</div> 
		<form name="returnnotesValidation" id="returnnotesValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="returns-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="returnId" name="returnId" value="${GOODS_RETURN.goodsReturnId}" /> 
				<input type="hidden" id="grnReceiveId" value="${GOODS_RETURN.receive.receiveId}"/>
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48">
						<fieldset style="min-height: 80px;">
							<div>
								<label class="width30">Supplier Name</label> 
								<input type="text" readonly="readonly" class="width50" id="supplierName" value="${GOODS_RETURN.supplierName}" />
								<input type="hidden" id="supplierId" value="${GOODS_RETURN.supplierId}"/>
							</div>
							<div>
								<label class="width30">Description</label>
								<textarea class="width51">${GOODS_RETURN.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 80px;">
							<div>
								<label class="width30" for="returnNumber"> Return Number<span
									class="mandatory">*</span>
								</label>
								<c:choose>
									<c:when
										test="${GOODS_RETURN.returnNumber ne null && GOODS_RETURN.returnNumber ne ''}">
										<input type="text" id="returnNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${GOODS_RETURN.returnNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" id="returnNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${requestScope.returnNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="returnDate">Return Date<span
									class="mandatory">*</span>
								</label> <input type="text" id="returnDate" readonly="readonly"
									value="${GOODS_RETURN.goodsReturnDate}"
									class="width50 validate[required]" />
							</div> 
							<c:if test="${GOODS_RETURN.goodsReturnId eq null || GOODS_RETURN.goodsReturnId eq 0
							 || GOODS_RETURN.grnNumber ne null && GOODS_RETURN.grnNumber ne ''}">
								<div>
									<label class="width30">Goods Receive<span
										class="mandatory">*</span>
									</label> <input type="text" id="grnNumber"
										class="width50 validate[required]" readonly="readonly"
										value="${GOODS_RETURN.grnNumber}" /> 
	 									<span class="button"> <a style="cursor: pointer;" 
										class="btn ui-state-default ui-corner-all goodsreceive-common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
	 							</div> 
							</c:if>
						</fieldset>
					</div>

				</div>
				<div class="clearfix"></div>
			</div>
 			<div class="portlet-content goodsreturn_detail"
					style="margin-top: 5px; display: none;" id="hrm">
					<fieldset>
						<legend>
							 Return Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="warning_message"
							class="response-msg notice ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width5">Product Code</th>
	 									<th class="width10">Product</th> 
	 									<th class="width5">Receive Qty</th>  
	 									<th class="width5 returnedLabel" style="display: none;">Returned Qty</th>  
	 									<th class="width5">Packaging</th>  
	 									<th class="width5">Return Qty</th>  
	 									<th class="width5">Base Qty</th>  
	 									<th class="width5">Unit Rate</th> 
	 									<th class="width5">Total</th> 
										<th style="width: 0.01%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:if test="${GOODS_RETURN.goodsReturnDetailVOs ne null &&  GOODS_RETURN.goodsReturnDetailVOs ne ''}">
										<c:forEach var="RETURN_DETAIL"
											items="${GOODS_RETURN.goodsReturnDetailVOs}" varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
												<td>
													<span>${RETURN_DETAIL.product.code}</span>
												</td>
												<td><span>${RETURN_DETAIL.product.productName}</span>
													<input type="hidden" id="productid_${status.index+1}"
													value="${RETURN_DETAIL.product.productId}" /></td> 
												<td><span>${RETURN_DETAIL.receiveQty}</span></td> 
												<td>
													${RETURN_DETAIL.conversionUnitName}
													<input type="hidden" id="packageType_${status.index+1}" value="${RETURN_DETAIL.packageDetailId}"  
														name="packageType" class="packageType"/>
												</td>
												<td><input type="text" value="${RETURN_DETAIL.packageUnit}"
													class="packageUnit validate[optional,custom[number]]"
													id="packageUnit_${status.index+1}" />
												</td>
												<td><input type="hidden"
													class="quantity validate[optional,custom[number]]"
													id="quantity_${status.index+1}"
													value="${RETURN_DETAIL.returnQty}"/>
													<span id="baseUnitConversion_${status.index+1}" class="width10" 
														style="display: none;">${RETURN_DETAIL.baseUnitName}</span>
													<span id="baseDisplayQty_${status.index+1}">${RETURN_DETAIL.baseQuantity}</span>
												</td>
												<td><span class="float-right">${RETURN_DETAIL.unitRate}</span></td>
												<td><span class="float-right"  id="totalamount_${status.index+1}">${RETURN_DETAIL.totalAmount}</span></td>
												<td style="display: none;"><input type="hidden"
													id="returnLineId_${status.index+1}"
													value="${RETURN_DETAIL.returnDetailId}" /> <input
													type="hidden" id="purchaseId_${status.index+1}"
													value="${RETURN_DETAIL.purchase.purchaseId}" /> <input
													type="hidden" id="receiveDetailId_${status.index+1}"
													value="${RETURN_DETAIL.receiveDetail.receiveDetailId}" />
													<input type="hidden" value="${RETURN_DETAIL.unitRate}"
													id="unitrateH_${status.index+1}" />
												</td>
												<td style="width: 0.01%;" class="opn_td"
													id="option_${status.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${status.index+1}" style="cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a></td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
 			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div
					class="portlet-header ui-widget-header float-right returnsdiscard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right returnssave"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
		</form>
		<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
	</div>
</div>