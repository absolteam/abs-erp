<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
</style>
<script type="text/javascript"> 
var productId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
var productName="";
$(function(){
	
	$('.formError').remove();
	$('#Product').dataTable({ 
		"sAjaxSource": "product_list_assetusage_json.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Product_ID", "bVisible": false},
			{ "sTitle": "Product Code"},
			{ "sTitle": "Product Name"},
			{ "sTitle": 'Unit Name'}, 
			{ "sTitle": 'Item Type'},
			{ "sTitle": 'Item Nature'}, 
			{ "sTitle": 'Status'},
		], 
		"sScrollY": $("#main-content").height() - 325,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	
	  
	

	//init datatable
	oTable = $('#Product').dataTable();
	 
	/* Click event handler */
	$('#Product tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          productId=aData[0];  
	          productName=aData[1] +" - "+ aData[2];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          productId=aData[0];  
	          productName=aData[1] +" - "+ aData[2]; 
	      }
	});
	
	$('#Product tbody tr').live('dblclick', function () { 
		 if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          productId=aData[0];  
	          productName=aData[1] +" - "+ aData[2];
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          productId=aData[0];  
	          productName=aData[1] +" - "+ aData[2]; 
	      }
		 
		 productPopupResult(productId,productName);
		$('#common-popup').dialog("close");


	});
	$('#asset-list-close').click(function () { 
		$('#common-popup').dialog("close");
	});
	
	
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset</div>	 	 
		<div class="portlet-content"> 
		 	<div id="rightclickarea">
			 	<div id="journal_accounts">
						<table class="display" id="Product"></table>
				</div> 
			</div>		
			
		</div>
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="asset-list-close"><fmt:message key="common.button.close"/></div>
	</div>
</div>