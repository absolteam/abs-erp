<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
}

.returnerr {
	font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<div id="hrm" class="portlet-content width100 purchaseline-info"
	style="height: 90%;">
	<div class="portlet-content">
		<div id="page-error-popup"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<div id="warning_message-popup"
			class="response-msg notice ui-corner-all width90"
			style="display: none;"></div>
		<div class="width30">
			<label style="width: 23% !important;" for="continuousPurchase">Continuous
				PO</label>
			<c:choose>
				<c:when
					test="${requestScope.continuousPurchase ne null && requestScope.continuousPurchase eq true}">
					<input type="checkbox" name="continuousPurchase"
						id="continuousPurchase" class="width3" checked="checked"
						style="position: relative; top: -2px;" />
				</c:when>
				<c:otherwise>
					<input type="checkbox" name="continuousPurchase"
						id="continuousPurchase" class="width3"
						style="position: relative; top: -2px;" />
				</c:otherwise>
			</c:choose>
		</div>
		<div id="hrm" class="hastable width100 batch_receive"
			style="display: none;">
			<fieldset>
				<legend>Batch Receive</legend>
				<table id="hastab-2" class="width100">
				<thead>
					<tr>
						<th>Product Code</th>
						<th>Product Name</th>
						<th>PO.Qty</th>
						<th>Received Qty</th>
						<th>Non Received Qty</th>
 						<th>Receive</th>
						<th>Unit Rate</th>
						<th>Total</th>
						<th>Store</th>
						<th>Description</th>
						<th><fmt:message key="accounts.common.label.options" /></th>
					</tr>
				</thead>
				<tbody class="tab-2">
					<c:forEach var="bean" varStatus="status1"
						items="${PURCHASE_DETAIL_BYID}">
						<tr class="rowidporcv" id="fieldrowporcv_${status1.index+1}">
							<td style="display: none;" id="lineIdporcv_${status1.index+1}">${status1.index+1}</td>
							<td id="productcodercv_${status1.index+1}">${bean.product.code}
							</td>
							<td id="productnamercv_${status1.index+1}">
								${bean.product.productName}</td> 
							<td id="purchaseqtyrcv_${status1.index+1}">${bean.purchaseQty}</td> 
							<td>
								<c:choose>
									<c:when test="${bean.receivedQty ne null &&  bean.receivedQty gt 0}">
										<span id="receivedqtyrcv_${status1.index+1}">${bean.receivedQty}</span>
									</c:when>
									<c:otherwise>
										<span id="receivedqtyrcv_${status1.index+1}" style="display:none;">${bean.receivedQty}</span>
										-N/A- 
									</c:otherwise>
								</c:choose> 
							</td>
							<td>
								<c:choose>
									<c:when test="${bean.returnedQty ne null &&  bean.returnedQty gt 0}">
										<span id="returnedqtyrcv_${status1.index+1}">${bean.returnedQty}</span>
									</c:when>
									<c:otherwise>
										<span id="returnedqtyrcv_${status1.index+1}" style="display:none;">${bean.returnedQty}</span>
										-N/A-
									</c:otherwise>
								</c:choose>  
							</td> 
  							<td><input type="text"
								class="width90 receiveQtyrcv validate[optional,custom[number]]"
								style="border: 0px;" value="${bean.quantity}" name="receiveQtyrcv"
								id="receiveQtyrcv_${status1.index+1}" /> 
								<input type="hidden"
								value="${bean.quantity}" name="receiveHiddenQtyrcv"
								id="receiveHiddenQtyrcv_${status1.index+1}" />
							</td>  
							<td><input type="text"
								class="width90 unitRatercv validate[optional,custom[number]]"
								style="border: 0px;" value="${bean.unitRate}" name="unitRatercv"
								id="unitRatercv_${status1.index+1}" />
							</td>
							<td id="totalrcv_${status1.index+1}" style="text-align: right;">
								<c:set var="totalrcv" value="${bean.quantity * bean.unitRate}" />
								<%=AIOSCommons.formatAmount(pageContext
								.getAttribute("totalrcv"))%></td>
							<td><input type="hidden" name="storeDetailrcv"
								value="${bean.rackId}" id="storeDetailrcv_${status1.index+1}" /> <span
								id="racknamercv_${status1.index+1}">${bean.rackType}</span> <span
								class="button float-right"> <a style="cursor: pointer;"
									id="${status1.index+1}"
									class="btn ui-state-default ui-corner-all store-popuprcv width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
							<td><input type="text" name="linedescription"
								id="linedescriptionrcv_${status1.index+1}"
								value="${bean.description}" />
							</td>
							<td style="width: 0.01%;" class="opn_tdrcv"
								id="optionporcv_${status1.index+1}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_porcv"
								id="DeleteCImagercv_${status1.index+1}" style="cursor: pointer;"
								title="Delete Record"> <span
									class="ui-icon ui-icon-circle-close"></span> </a>
								<input type="hidden" id="returnQtyrcv_${status1.index+1}" name="returnQtyrcv" 
									value="${bean.returnQty}"/>
								<input type="hidden"
									name="receivelineidrcv" id="receivelineidrcv_${status1.index+1}" /> <input
								type="hidden" name="purchasePOIdrcv"
								id="purchasePOIdrcv_${status1.index+1}"
								value="${requestScope.purchaseId}" /> <input type="hidden"
								name="productidrcv" id="productidrcv_${status1.index+1}"
								value="${bean.product.productId}" /> <input type="hidden"
								name="purchaseLineIdrcv" id="purchaseLineIdrcv_${status1.index+1}"
								value="${bean.purchaseDetailId}" />
							</td> 
						</tr>
					</c:forEach>
				</tbody>
			</table> 
			</fieldset> 
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div
					class="portlet-header ui-widget-header float-left showReturnsTable"
					style="cursor: pointer;">
					Non Receive Update
				</div> 
			</div>
			<div id="hrm" class="hastable width50 batch_returns" style="display: none;">
				<fieldset>
					<legend>Goods Returns</legend>
					<table id="hastab-2" class="width100">
					<thead>
						<tr>
							<th>Product Code</th>
							<th>Product Name</th>
							<th>PO.Qty</th>
							<th>Batch Receive</th>
	 						<th>Non Received Qty</th>  
						</tr>
					</thead>
					<tbody class="tab-3">
						<c:forEach var="bean" varStatus="status2"
							items="${PURCHASE_DETAIL_BYID}">
							<tr class="rowidportn" id="fieldrowportn_${status2.index+1}">
								<td style="display: none;" id="lineIdportn_${status2.index+1}">${status2.index+1}</td>
								<td id="productcodertn_${status2.index+1}">${bean.product.code}
								</td>
								<td id="productnamertn_${status2.index+1}">
									${bean.product.productName}</td> 
								<td id="purchaseqtyrtn_${status2.index+1}">${bean.purchaseQty}</td>  
								<td id="receiveqtyrtn_${status2.index+1}">${bean.quantity}</td>  
								<td>
								<c:choose>
									<c:when test="${bean.returnQty ne null && bean.returnQty gt 0}">
										<input type="text"
											class="width90 returnQtyrtn validate[optional,custom[number]]"
											style="border: 0px;" value="${bean.returnQty}" name="returnQtyrtn"
											id="returnQtyrtn_${status2.index+1}" />
									</c:when>
									<c:otherwise>
										<input type="text"
											class="width90 returnQtyrtn validate[optional,custom[number]]"
											style="border: 0px;" name="returnQtyrtn"
											id="returnQtyrtn_${status2.index+1}" />
									</c:otherwise>
								</c:choose> 
								</td> 
							</tr>
						</c:forEach>
					</tbody>
				</table> 
				</fieldset>  
			</div>
		</div>
		<div class="clearfix"></div> 
		<div id="hrm" class="hastable width100 purchase_receive" style="display: none;"> 
			<input type="hidden" id="receiveLineId" value="${requestScope.rowId}" />
			<fieldset>
				<legend>Purchase Details</legend>
			<table id="hastab-1" class="width100">
				<thead>
					<tr>
						<th>Product Code</th>
						<th>Product</th>
						<th style="display: none;">UOM</th>
						<th>PO.Qty</th>
						<th>Received Qty</th>
						<th>Non Received Qty</th>
						<th>Receive Qty</th>
						<th>Unit Rate</th>
						<th>Total</th>
						<th>Store</th>
						<th class="returnDiv">Non Receive Qty</th>
						<th>Description</th>
						<th><fmt:message key="accounts.common.label.options" /></th>
					</tr>
				</thead>
				<tbody class="tab-1">
					<c:forEach var="bean" varStatus="status"
						items="${PURCHASE_DETAIL_BYID}">
						<tr class="rowidpo" id="fieldrowpo_${status.index+1}">
							<td style="display: none;" id="lineIdpo_${status.index+1}">${status.index+1}</td>
							<td id="productcode_${status.index+1}">${bean.product.code}
							</td>
							<td id="productname_${status.index+1}">
								${bean.product.productName}</td>
							<td style="display: none;" id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td>
							<td id="purchaseqty_${status.index+1}">${bean.purchaseQty}</td>
							<td>
								<c:choose>
									<c:when test="${bean.receivedQty ne null &&  bean.receivedQty gt 0}">
										<span id="receivedqty_${status.index+1}">${bean.receivedQty}</span>
									</c:when>
									<c:otherwise>
										<span id="receivedqty_${status.index+1}" style="display:none;">${bean.receivedQty}</span>
										-N/A-
									</c:otherwise>
								</c:choose>  
							</td>
							<td>
								<c:choose>
									<c:when test="${bean.returnedQty ne null &&  bean.returnedQty gt 0}">
										<span id="returnedqty_${status.index+1}">${bean.returnedQty}</span>
									</c:when>
									<c:otherwise>
										<span id="returnedqty_${status.index+1}" style="display:none;">${bean.returnedQty}</span>
										-N/A-
									</c:otherwise>
								</c:choose>  
							</td>
							<td><input type="text"
								class="width90 receiveQty validate[optional,custom[number]]"
								style="border: 0px;" value="${bean.quantity}" name="receiveQty"
								id="receiveQty_${status.index+1}" /> <input type="hidden"
								value="${bean.quantity}" name="receiveHiddenQty"
								id="receiveHiddenQty_${status.index+1}" />
							</td>
							<td><input type="text"
								class="width90 unitRate validate[optional,custom[number]]"
								style="border: 0px;" value="${bean.unitRate}" name="unitRate"
								id="unitRate_${status.index+1}" />
							</td>
							<td id="total_${status.index+1}" style="text-align: right;">
								<c:set var="total" value="${bean.quantity * bean.unitRate}" />
								<%=AIOSCommons.formatAmount(pageContext
						.getAttribute("total"))%></td>
							<td style="display: none"><input type="hidden"
								name="receivelineid" id="receivelineid_${status.index+1}" /> <input
								type="hidden" name="purchasePOId"
								id="purchasePOId_${status.index+1}"
								value="${requestScope.purchaseId}" /> <input type="hidden"
								name="productid" id="productid_${status.index+1}"
								value="${bean.product.productId}" /> <input type="hidden"
								name="purchaseLineId" id="purchaseLineId_${status.index+1}"
								value="${bean.purchaseDetailId}" />
							</td>
							<td><input type="hidden" name="storeDetail"
								value="${bean.rackId}" id="storeDetail_${status.index+1}" /> <span
								id="rackname_${status.index+1}">${bean.rackType}</span> <span
								class="button float-right"> <a style="cursor: pointer;"
									id="${status.index+1}"
									class="btn ui-state-default ui-corner-all store-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
							<td class="returnDiv"><span id="returnQty1_${status.index+1}">${bean.returnQty}</span>
								<input type="hidden" name="returnQty"
								id="returnQty_${status.index+1}" class="returnQty"
								value="${bean.returnQty}" />
							</td>
							<td><input type="text" name="linedescription"
								id="linedescription_${status.index+1}"
								value="${bean.description}" />
							</td>
							<td style="width: 0.01%;" class="opn_td"
								id="optionpo_${status.index+1}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_po"
								id="DeleteCImage_${status.index+1}" style="cursor: pointer;"
								title="Delete Record"> <span
									class="ui-icon ui-icon-circle-close"></span> </a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</fieldset>
		</div> 
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div
			class="portlet-header ui-widget-header float-left sessionReceiveSave"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div
			class="portlet-header ui-widget-header float-left closeReceiveDom"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$jquery("#receiveNoteValidation").validationEngine('attach');  
	continuousPurchase();
	
	$('#continuousPurchase').change(function(){
		continuousPurchase();
	});
	
	$('.batch_returns').show();
	
});
function continuousPurchase(){
	var continuousPO = $('#continuousPurchase').attr('checked');
	if(continuousPO ==  true){
		$('.returnDiv').hide();
		$('.batch_receive').show();
		$('.purchase_receive').hide();
	}else{
		$('.returnDiv').show();
		$('.batch_receive').hide();
		 $('.rowidpo').each(function(){ 
			 var rowId = getChildRowId($(this).attr('id'));
			 var receiveQty = Number($.trim($('#receiveQty_'+rowId).val()));
			 var poQty = Number($.trim($('#purchaseqty_'+rowId).text()));   
			 var returnedQty =  Number($.trim($('#returnedqty_'+rowId).text()));  
			 var receivedQty = Number($.trim($('#receivedqty_'+rowId).text()));
			 var totalAvailedQty = Number(receiveQty+receivedQty+returnedQty);   
			 var rtrnQty = Number(poQty) - Number(totalAvailedQty);
			 if(rtrnQty > 0){
				 $('#returnQty1_'+rowId).text(rtrnQty); 
	 			 $('#returnQty_'+rowId).val($.trim($('#returnQty1_'+rowId).text()));
			 } 
		 }); 
		$('.purchase_receive').show();
	}
	$('.batch_returns').show();
	return false;
}
function getChildRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
</script>