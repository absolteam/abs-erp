<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 var oTable; var selectRow=""; var aSelected = []; var aData="";
	$(function() {
		oTable = $('#RequisitionCommonList')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "getrequistion_issue_list.action",

							"aoColumns" : [ {
								"mDataProp" : "referenceNumber"
							}, {
								"mDataProp" : "date"
							}, {
								"mDataProp" : "locationName"
							}, {
								"mDataProp" : "personName"
							}, {
								"mDataProp" : "statusStr"
							}]
						});
		oTable.fnSort( [ [0,'desc'] ] );	
		/* Click event handler */
		$('#RequisitionCommonList tbody tr').live(
				'dblclick',
				function() {
					aData = oTable.fnGetData(this);
					commonRequisitionPopup(aData);
					$('#requisition-common-popup').trigger('click');
					return false;
				});

		/* Click event handler */
		$('#RequisitionCommonList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
		
		$('#requisition-common-popup').click(function () { 
			$('#common-popup').dialog("close");
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>item requisition
		</div>
		<div class="portlet-content">
			<div id="req_json_list">
				<table id="RequisitionCommonList" class="display">
					<thead>
						<tr>
							<th>Reference</th>
							<th>Date</th>
							<th>Location</th>
							<th>Request Person</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="requisition-common-popup">close</div>
			</div>
		</div> 
	</div>
</div>