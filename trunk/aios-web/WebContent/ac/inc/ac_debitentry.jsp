<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.width96{width:96%!important;}
</style>
<script type="text/javascript">
var debitDetails="";

$(function(){
	 
	$jquery("#debitenotesValidation").validationEngine('attach'); 
	
	if($('#debitId').val()>0)
		$('.recqty').remove();
	
	$('#debitDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
    
	$('.returns-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		var page="debitNote";
		$('#page').val('debitNote');
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/showreturn_goods.action", 
		 	async: false, 
		 	data:{showPage:page},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open');
				 $(
 							$(
 									$('#common-popup')
 											.parent())
 									.get(0)).css('top',
 							0); 
			} 
		}); 
			return false; 
	});

	$('#goodsreturn-common-popup').live('click',function(){
		$('#common-popup').dialog('close');
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.returnqty').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var unitrate=Number($jquery('#unitrate_'+rowId).text());
		var returnQty=Number($('#returnQty_'+rowId).val());
		$('#total_'+rowId).text(Number(unitrate*returnQty));
	});

	$('.debit_save').click(function(){  
		var debitId=Number($('#debitId').val());
		var debitNumber=$('#debitNumber').val();
		var debitDate=$('#debitDate').val();
		var returnId=Number($('#returnId').val());
	 	var description=$('#description').val();
	 	var exchangeFlag = $('#exchangeFlag').attr('checked');
 		debitDetails=getDebitDetails();    
		if($jquery("#debitenotesValidation").validationEngine('validate')){
 		 $.ajax({
				type:"POST",
				url:'<%=request.getContextPath()%>/debit_save.action',
			 	async: false,
			 	data:{ date:debitDate, returnId:returnId, debitId:debitId, debitNumber:debitNumber, description:description,
			 		   exchangeFlag: exchangeFlag, debitDetails:debitDetails},
			    dataType: "json",
			    cache: false,
				success:function(response){ 
 					 if(response.returnMessage=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/getdebit_listdetails.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record created.").slideDown(1000);
								}
						 });
					 }
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 return false;
					 }
				}
			 });
		} 
		else{
		 return false;
	 	}
	 });

	var getDebitDetails=function(){
		 var productid=new Array();  
		 var returnqty=new Array(); 
		 var debitlineid=new Array();  
		 var amount=new Array();  
		 var returnDetailArray=new Array(); 
		 debitDetails="";
		 $('.rowid').each(function(){
		 	 var rowId=getRowId($(this).attr('id')); 
			 var productId = Number($('#productid_'+rowId).val());
			 var returnQty = Number($('#returnqty_'+rowId).val()); 
			 var amountVal = Number($jquery('#unitrate_'+rowId).val());
			 var debitLineId = Number($('#debitLineId_'+rowId).val());   
			 var returnDetailId = Number($('#returnDetailId_'+rowId).val());   
			 if(typeof productId!='undefined' &&
					typeof returnQty!='undefined' && productId!='' && returnQty!=''
					&& typeof amountVal!='undefined' && amountVal!=''){  
				returnqty.push(returnQty); 
				productid.push(productId);
				amount.push(amountVal);
				debitlineid.push(debitLineId);   
				returnDetailArray.push(returnDetailId);
			 }  
		  });  
		  for(var j=0;j<productid.length;j++){  
			debitDetails+=productid[j]+"__"+returnqty[j]+"__"+amount[j]+"__"+debitlineid[j]+"__"+returnDetailArray[j];
			if(j==productid.length-1){   
			} 
			else{
				debitDetails+="#@";
			}
		 }    
		 return debitDetails;
	 };

	 $('.debit_discard').click(function(){  
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getdebit_listdetails.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();  
					$("#main-wrapper").html(result); 
				}
		 });
	 });
	 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function getAllReturns(returnId){
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_return_detail.action", 
		 	async: false, 
		 	data:{returnId:returnId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
		 		$(".tab").html(result);  
		 		$('.debit-details').show();
			} 
		}); 
		return false;
}

function commonGoodsReturnPopup(returnId, returnNumber, supplierName) {
	$('#returnId').val(returnId); 
	$('#returnNumber').val(returnNumber); 
	$('#supplierName').val(supplierName); 
	getAllReturns(returnId);
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>debit note</div>
		  <form name="debitenotesValidation" style="position: relative;" id="debitenotesValidation"> 
			<div class="portlet-content">  
			<input type="hidden" id="page"/>
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div> 
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height: 90px;">  
							<div style="display: none;">
								<label class="width30" for="supplierName">Supplier Number</label> 
								<input type="text" readonly="readonly" name="supplierNumber" id="supplierNumber" value="${DEBIT_INFO.goodsReturn.purchase.supplier.supplierNumber}" class="width50"/> 
							</div> 
							<div>
								<label class="width30" for="siteName">Supplier Name<span
									style="color: red;">*</span></label> 
								<input type="text" readonly="readonly" name="supplierName" id="supplierName" class="validate[required] width50"
									 value="${DEBIT_INFO.goodsReturn.purchase.supplier.person.firstName} ${DEBIT_INFO.goodsReturn.purchase.supplier.person.firstName}${DEBIT_INFO.goodsReturn.purchase.supplier.cmpDeptLocation.company.companyName}"/> 
							</div> 
							 <div>
								<label class="width30" for="description">Description</label> 
								<textarea rows="2" class="float-left width51" id="description">${DEBIT_INFO.description}</textarea> 
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height: 90px;"> 
							<div>
								<label class="width30" for="debitId">Debit Number</label> 
								<input type="hidden" name="debitId" id="debitId" value="${DEBIT_INFO.debitId}"/>
								<c:choose>
									<c:when test="${DEBIT_INFO.debitId ne null && DEBIT_INFO.debitId ne ''}">
										<input type="text" readonly="readonly" name="debitNumber" id="debitNumber" class="width50 validate[required]" value="${DEBIT_INFO.debitNumber}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="debitNumber" id="debitNumber" class="width50 validate[required]" value="${DEBIT_NUMBER}"/>
									</c:otherwise>
								</c:choose> 
							</div>  
							<div>
								<label class="width30" for="debitDate">Date<span
									style="color: red;">*</span></label> 
								<c:choose>
									<c:when test="${DEBIT_INFO.date ne null && DEBIT_INFO.date ne ''}">
										<c:set var="date" value="${DEBIT_INFO.date}"/>  
										<% 
											String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
										%>
										<input type="text" readonly="readonly" name="debitDate" id="debitDate" class="width50 validate[required]" value="<%=date%>"/> 
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="debitDate" id="debitDate" class="width50 validate[required]"/>
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="width30" for="returnNumber">Return Number<span
									style="color: red;">*</span></label> 
								<input type="hidden" name="returnId" id="returnId" value="${DEBIT_INFO.goodsReturn.returnId}"/>
								<c:choose>
									<c:when test="${DEBIT_INFO.goodsReturn.returnId ne null && DEBIT_INFO.goodsReturn.returnId ne ''}">
										<input type="text" readonly="readonly" name="returnNumber" id="returnNumber" class="width50 validate[required]" value="${DEBIT_INFO.goodsReturn.returnNumber}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="returnNumber" id="returnNumber" class="width50 validate[required]"/>
										<span class="button">
											<a style="cursor: pointer;" id="receive" class="btn ui-state-default ui-corner-all returns-common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span>  
									</c:otherwise>
								</c:choose> 
							</div>  
							<fieldset> 
								<div class="width38 float-left">
									<input type="radio" id="exchangeFlag" name="returnAction" class="float-left"/> 
									<label for="exchangeFlag" style="position: relative; top:3px; font-size:13px;">Exchange</label>
								</div>
								<div class="width48 float-left">
									<input type="radio" id="returnsFlag" name="returnAction" class="float-left"/>  
									<label for="returnsFlag" style="position: relative; top:3px; font-size:13px;">Returns</label>
								</div>
							</fieldset>
						</fieldset>
					</div> 
			</div>  
		</div>  
		<div class="clearfix"></div> 
		<div class="portlet-content debit-details" style="margin-top:10px;display: none;" id="hrm"> 
 			<fieldset>
 				<legend>Debit Details<span
									class="mandatory">*</span></legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100"  >   
						 <table id="hastab" class="width100"> 
							<thead>
							   <tr>   
								    <th class="width5">Product Code</th> 
								    <th class="width10">Product Name</th>  
								    <th class="width10">Packaging</th>  
								    <th class="width10">Return Qty</th>  
								    <th class="width5">Base Qty</th> 
								    <th class="width5">Unit Rate</th> 
								    <th class="width5">Total Amount</th> 
 									<th style="display: none;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
							<tbody class="tab"> 
							<c:choose>
							<c:when test="${DEBIT_DETAIL_INFO ne null && DEBIT_DETAIL_INFO ne '' && fn:length(DEBIT_DETAIL_INFO)>0}">	
								<c:forEach var="bean" items="${DEBIT_DETAIL_INFO}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
										<td id="productcode_${status.index+1}"> 
											${bean.product.code}
										</td>
										<td id="productname_${status.index+1}"> 
											${bean.product.productName}
										</td> 
										<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
										<td id="unitrate_${status.index+1}" style="text-align: right;">
											<c:out value=" ${bean.amount/bean.returnQty}"/>
										</td> 
										<td id="returnqty_${status.index+1}">${bean.returnQty}</td>   
										<td id="total_${status.index+1}" style="text-align: right;">
											 <c:out value="${bean.amount}"/>
										</td>  
										<td style="display:none"> 
											<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/>
											<input type="hidden" name="debitLineId" id="debitLineId_${status.index+1}" value="${bean.debitDetailId}"/> 
											<input type="hidden" name="returnDetailId" id="returnDetailId_${status.index+1}" value="${bean.goodsReturnDetail.returnDetailId}"/> 
										</td>  
										<td>
											<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span>
										    </a>	
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
										   </a> 
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
										   </a>
										   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
												<span class="processing"></span>
										   </a>
										</td> 
									</tr>
								</c:forEach> 
							</c:when>
							<c:otherwise>
								<c:forEach var="i" begin="1" end="2" step="1" varStatus="status1"> 
									<tr class="rowid" id="fieldrow_${status1.index+1}" style="height:23px;"> 
										<c:forEach var="j" begin="1" end="6" step="1" varStatus="status"> 
											<td></td>
										</c:forEach> 
								</c:forEach> 
							</c:otherwise>
							</c:choose>
					 </tbody>
				</table>
			</div> 
		</fieldset>
	</div>  
	<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right debit_discard" id="discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right debit_save" id="save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>
		<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div>  
		<div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>  
  </form>
  </div> 
</div>