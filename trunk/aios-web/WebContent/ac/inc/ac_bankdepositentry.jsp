<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var tempactionname="";
var accountTypeId=0;
var depositDetails="";
$(function(){  
	$('#depositDate').datepick({defaultDate: '0', selectDefaultDate: true,maxDate:0, showTrigger: '#calImg'});

	  $('#customer-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

	     $('#supplier-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

	     $('#bankreceipts-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });
	  
	    $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 	$('#selectedMonth,#linkedMonth').change();
	 	$('#l10nLanguage,#rtlLanguage').change();
	 	if ($.browser.msie) {
	 	        $('#themeRollerSelect option:not(:selected)').remove();
	 	}
	 	$('#dateFrom,#dateTo').datepick({
	 	 	onSelect: customRange, showTrigger: '#calImg'}); 
	 	
	$('.bankaccount-common-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
	    $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getbankaccount_details.action", 
		 	async: false,  
		 	data : {showPage : "bankdeposit"}, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);    
				 $($($('#common-popup').parent()).get(0)).css('top',0);
			} 
		});  
		return false;
	});
	
	$('.bankdeposit-depositer-popup').click(function(){  
	    $('.ui-dialog-titlebar').remove();   
	    $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data : {personTypes: "1"}, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);   
				 $('#common-popup').dialog('open');
				 $($($('#common-popup').parent()).get(0)).css('top',0);
			} 
		});  
		return false;
	});
	
	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800,  
		 top: 0,
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('#person-list-close').live('click',function(){ 
		$('#common-popup').dialog('close');
	}); 
	
	$('#receiptsSource').change(function(){
		$('#supplierName').val(''); 
		if($(this).val()!=null && $(this).val()!=''){
			$('#supplierName').removeClass('width60').addClass('width50');
				$('.supplier-popup-span').show();
		} else {
			$('#supplierName').removeClass('width50').addClass('width60');
			$('.supplier-popup-span').hide();
		}
	});

	$('.delrow').live('click',function(){ 
		 slidetab = "";
		 slidetab = $(this).parent().parent().get(0);  
     	 $(slidetab).remove();  
     	 var i=1;
  	 	 $('.rowid').each(function(){   
  		 var rowId=getRowId($(this).attr('id')); 
  		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });
		
	$('#deposit_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/bank_deposit_list.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	}); 
	 
	$('#receiptsTypeId').change(function(){
		var receiptsTypeId = $(this).val();
		if(receiptsTypeId !=null && receiptsTypeId !=''){
			$('#receiptsId').val('');
			$('#receiptsNo').val('');
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/bank_receipts_byreceiptstype.action",
				data : {receiptTypeId: receiptsTypeId}, 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('#payment_list_div').html(result);
				}
			}); 
		} else
			return false;
	}); 
	
	$('#show_receipt').click(function(){ 
		var receiptType = Number($('#receiptType').val()); 
		if(receiptType >0){
			if(receiptType == 2){ 
				getPOSReceiptDepositDetail();
			}else{ 
				getReceiptDepositDetail();
			}
		} 
	}); 

	$('#receiptType').change(function(){
		var receiptType = Number($('#receiptType').val()); 
		if(receiptType == 2){
			$('.branchdiv').show(); 
			$('.receiptdiv').hide();
		}else{
			$('.branchdiv').hide(); 
			$('.receiptdiv').show();
		}
	});
	
	 $jquery("#bankDepositForm").validationEngine('attach'); 

	$('#deposit_save').click(function(){ 
		if($jquery("#bankDepositForm").validationEngine('validate')){
		var depositDocs = $('#depositDocs').html(); 
		var bankDepositId = Number($('#bankDepositId').val());
		var bankDepositNumber = Number($('#bankDepositNumber').val());
		var depositDate = $('#depositDate').val();
		var description = $('#description').val();
		var depositerId = $('#depositerId').val(); 
		var bankAccountId = Number($('#bankAccountId').val());
		depositDetails = getBankDepositDetails(); 
		
		if(depositDetails!=null && depositDetails!=""){
			if(depositDocs!=null && depositDocs.trim()!=''){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_bank_deposit.action", 
			 	async: false, 
			 	data:{	bankDepositId: bankDepositId, bankDepositNumber: bankDepositNumber, date: depositDate, description: description,
			 			depositerId: depositerId, bankAccountId: bankAccountId, bankDepositDetails: depositDetails},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html();  
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/bank_deposit_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record Created.").slideDown(1000);
									$('#success_message').delay(2000).hide();
								}
						 });
						 return false;
					 } 
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
			}else{
				$('#page-error').hide().html("Please upload some documents.").slideDown(1000);
				$('#page-error').delay(2000).slideUp();
				return false;
			} 
		}else{
			$('#page-error').hide().html("Please enter deposit details.").slideDown(1000);
			$('#page-error').delay(2000).slideUp();
			return false;
		} 
		}else
			return false;
		return false;
	});

	var getBankDepositDetails = function(){  
  		var bankReceiptsDetailArray = new Array();
 		var amountArray = new Array();
  		var descArray = new Array();
 		var useCaseArray = new Array();
 		var recordIdArray =new Array();
 		depositDetails = "";  
 		$('.rowid').each(function(){ 
 			 var rowId = getRowId($(this).attr('id'));  
 			 var checkedReceipt = $('#checkbankreceipt_'+rowId).attr('checked');
 			 if(checkedReceipt ==  true){
 				 var amount = $('#amount_'+rowId).text().trim();   
 	 			 var lineDescription = $('#description_'+rowId).val(); 
 	  			 var bankReceiptsDetailId =  Number($('#bankReceiptsDetailId_'+rowId).val());  
 	  			 var useCase = $('#bankReceiptsUseCase_'+rowId).val();
 	  			 var recordId = Number($('#bankReceiptsRecordId_'+rowId).val()); 
  				 amountArray.push(amount);
 				 bankReceiptsDetailArray.push(bankReceiptsDetailId);
  				 if(lineDescription!=null && lineDescription!="")
 					descArray.push(lineDescription);
 				 else
 					descArray.push("##"); 
  				 if(useCase!=null && useCase!="")
  					useCaseArray.push(useCase);
 				 else
 					useCaseArray.push("##"); 
  				 recordIdArray.push(recordId);
 	 			 
 			 } 
 		});
  		for(var j=0; j < amountArray.length; j++){ 
  			depositDetails += amountArray[j]+"__"+descArray[j]
  								+"__"+bankReceiptsDetailArray[j]+ "__"+useCaseArray[j]+"__"+recordIdArray[j];
 			if(j == amountArray.length-1 ){   
 			} 
 			else{
 				depositDetails += "@@";
 			}
 		} 
 		return depositDetails;
 	}; 

	var bankDepositId=$('#bankDepositId').val();		
	 $('#deposit_document_information').click(function(){
			if(bankDepositId>0){
				AIOS_Uploader.openFileUploader("doc","depositDocs","BankDeposit",bankDepositId,"BankDepositDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","depositDocs","BankDeposit","-1","BankDepositDocuments");
			}
		});
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function showBankReceiptsDetail(bankReceiptsId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/bank_receipts_byreceipts.action",
					data : {
						bankReceiptsId : bankReceiptsId
					},
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$('#payment_list_div').html(result);
					}
				});
	}
	function personPopupResult(personid, personname, commonParam) { 
		$('#depositerId').val(personid);
		$('#depositerName').val(personname); 
		$('#common-popup').dialog("close");
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#supplierId').val(customerId);
		$('#supplierName').val(customerName);
		$('#codeCombinationId').val(combinationId);
		$('#codeCombination').val(accountCode);
	}

	function commonSupplierPopup(supplierId, supplierName, combinationId,
			accountCode, commonParam) {
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
		$('#codeCombinationId').val(combinationId);
		$('#codeCombination').val(accountCode);
	}
	function bankReceiptsPaidPopup(bankReceiptsId, receiptNumber) {
		$('#receiptsId').val(bankReceiptsId);
		$('#receiptsNo').val(receiptNumber);
		showBankReceiptsDetail(bankReceiptsId);
	}
	
	function getPOSReceiptDepositDetail(){
		var dateFrom = $('.datefrom').val();
		var dateTo = $('.dateto').val();
		var receiptType = Number($('#receiptType').val());
		var storeId = Number($('#storeDetailId').val()); 
		var paymentMode = Number($('#paymentModeBr').val()); 
		if(receiptType > 0){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_posreceipts_byreceiptstype.action",
				data : {
					dateFrom : dateFrom, dateTo: dateTo, storeId: storeId, paymentMode: paymentMode
				},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#payment_list_div').html(result);
					return false;
				}
			});	
		} return false;
	}
	
	function getReceiptDepositDetail(){
		var dateFrom = $('.datefrom').val();
		var dateTo = $('.dateto').val();
		var receiptType = Number($('#receiptType').val());
		var paymentMode = $('#paymentMode').val();
		if(receiptType > 0){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_bankreceipts_byreceiptstype.action",
				data : {
					dateFrom : dateFrom, dateTo: dateTo, paymentMode: paymentMode
				},
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#payment_list_div').html(result);
					return false;
				}
			});	
		} return false;
	}
	
	function checkLinkedDays() {
		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}
	
	function customRange(dates) {
 		if (this.id == 'dateFrom') {
			$('#dateTo').datepick('option', 'minDate', dates[0] || null);  
		} else {
			$('#dateFrom').datepick('option', 'maxDate', dates[0] || null); 
		}
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Bank
			Deposits
		</div> 
		<div class="width100" id="hrm">
			<form id="bankDepositForm" style="position: relative;">
				<div class="tempresult" style="display: none;"></div>
				<div style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;" 
					class="response-msg error ui-corner-all" id="page-error"></div> 
				<div class="width50 float-left" id="hrm">
					<fieldset style="min-height: 190px;">
						<input type="hidden" id="bankDepositId"
							value="${BANK_DEPOSIT.bankDepositId}" />
						<div>
							<label class="width30">Deposit Number<span
								class="mandatory">*</span>
							</label> <input type="text" name="bankDepositNumber"
								value="${requestScope.bankDepositNumber}" id="bankDepositNumber"
								readonly="readonly" class="width50 validate[required]" />
						</div>
						<div>
							<label class="width30">Deposit Date<span
								class="mandatory">*</span>
							</label>
							<c:choose>
								<c:when
									test="${BANK_DEPOSIT.date ne null && BANK_DEPOSIT.date ne ''}">
									<c:set var="payDate" value="${BANK_DEPOSIT.date}" />
									<%
										String date = DateFormat.convertDateToString(pageContext
														.getAttribute("payDate").toString());
									%>
									<input type="text" name="depositDate" value="<%=date%>"
										id="depositDate" readonly="readonly"
										class="width50 validate[required]" />
								</c:when>
								<c:otherwise>
									<input type="text" name="depositDate" id="depositDate"
										readonly="readonly" class="width50 validate[required]" />
								</c:otherwise>
							</c:choose>
						</div>
						<div>
							<label class="width30" for="depositerName">Depositer<span
								class="mandatory">*</span>
							</label> <input type="hidden" id="depositerId" name="depositerId" /> <input
								type="text" readonly="readonly" name="depositerName"
								id="depositerName"
								class="width50 validate[required] depositerName" /> <span
								class="button"> <a style="cursor: pointer;"
								id="depositer"
								class="btn ui-state-default ui-corner-all 
										bankdeposit-depositer-popup width100">
									<span class="ui-icon ui-icon-newwin"></span> </a> </span>
						</div> 
						<div>
							<label class="width30" for="bankAccount">Bank Account<span
								class="mandatory">*</span></label>
							<input type="hidden" id="bankAccountId" />  
							<input type="text" readonly="readonly" name="bankAccount"
								id="bankAccount" class="width50 validate[required] bankAccount" />
							<span class="button"> 
								<a class="btn ui-state-default ui-corner-all bankaccount-common-popup width100" 
									style="cursor: pointer;">
								<span class="ui-icon ui-icon-newwin"> </span> </a> 
							</span>
						</div> 
						<div class="width98">
							<fieldset>
								<div>
									<label class="width30" for="receiptType">Receipt Type<span
										class="mandatory">*</span></label> 
									<select name="receiptType" id="receiptType" class="width50">
										<option value="">Select</option>
										<c:forEach items="${RECEIPT_TYPES}" var="receiptType">
											<option value="${receiptType.key}">${receiptType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div style="display: none;" class="receiptdiv">
									<label class="width30">Payment Mode</label> <select
										name="paymentMode" class="paymentMode width50" id="paymentMode">
										<option value="">Select</option>
										<c:choose>
											<c:when
												test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
												<c:forEach items="${MODE_OF_PAYMENT}" var="MODE">
													<option value="${MODE.key}">${MODE.value}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
								</div>
								<div style="display: none;" class="branchdiv">
									<label class="width30" for="storeDetail">Branch</label>
									<input type="hidden" id="storeDetailIdTemp" /> <select
										name="storeDetailId" id="storeDetailId" class="width51">
										<option value="-1">All</option>
										<c:forEach var="store" items="${STORE_DETAILS}">
											<option value="${store.storeId}">${store.storeName}</option>
										</c:forEach>
									</select>
								</div>
								<div style="display: none;" class="branchdiv">
									<label class="width30">Payment Mode</label>
									<select
										name="paymentModeBr" id="paymentModeBr" class="width51">
										<option value="-1">All</option>
										<c:forEach items="${POSRECEIPT_TYPES}" var="paymentmode">
											<option value="${paymentmode.key}">${paymentmode.value}</option>
										</c:forEach>
									</select>
								</div>
								<div class="float-left width50">
									<label class="width30">Date From</label>
									<input type="text" readonly="readonly" class="datefrom width60" id="dateFrom">
								</div>
								<div class="float-left width48">
									<label class="width30">Date To</label>
									<input type="text" readonly="readonly" class="dateto width60" id="dateTo">
								</div>
								<div
									class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
									style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
									<div class="portlet-header ui-widget-header float-right"
										id="show_receipt" style="cursor: pointer; color: #fff;">
										show receipts
									</div> 
								</div>
							</fieldset>
						</div> 
					</fieldset>
				</div>
				<div class="width30 float-left" id="hrm" style="display: none;">
					<fieldset style="min-height: 220px;"> 
						<div>
							<label class="width30" for="receiptsSource">Source</label> <select
								name="receiptsSource" id="receiptsSource" class="width62">
								<option value="">Select</option>
								<c:forEach items="${RECEIPT_SOURCE}" var="SOURCE">
									<option value="${SOURCE.key}">${SOURCE.value}</option>
								</c:forEach>
							</select>
						</div>
						<div>
							<label class="width30">Receiver</label> <input type="text"
								name="supplierName" id="supplierName" class="width60"
								readonly="readonly" /> <span
								class="button width10 float-right supplier-popup-span"
								style="display: none; position: relative; top: 7px; right: 13px;">
								<a style="cursor: pointer;" id="supplier"
								class="btn ui-state-default ui-corner-all bankdeposit-common-popup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
								type="hidden" readonly="readonly" name="supplierId"
								id="supplierId" />
						</div>
						<div>
							<label class="width30 tooltip">Description</label>
							<textarea rows="2" id="description" class="width61 float-left">${DIRECT_PAYMENT.description}</textarea>
						</div>
					</fieldset>
				</div>
				<div class="width48 float-left" id="hrm">
					<fieldset style="height: 190px;">
						<div>
							<div id="deposit_document_information"
								style="cursor: pointer; color: blue;">
								<u>Upload document here<span class="mandatory">*</span>
								</u>
							</div>
							<div
								style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
								<span id="depositDocs"></span>
							</div>
						</div>
					</fieldset>
				</div>
				<div id="payment_list_div"></div>
			</form>
		</div>
		<div class="clearfix"></div>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right"
				id="deposit_discard" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right"
				id="deposit_save" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span>
				</a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>