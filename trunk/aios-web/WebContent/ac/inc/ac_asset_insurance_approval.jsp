<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accessCode = "";
$(function(){ 
	 
	 
	 $('.insurancetype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.policytype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.provider-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('#paymentMode').change(function(){  
		$('.mode').hide(); 
		$('#accountNumber').val(''); 
		$('#bankAccountId').val(''); 
		$('#cash_bank_account').val(''); 
		$('#cashAccount').val('');
		$('#'+$('#paymentMode option:selected').text()).show();
	 });

	 $('.paymentcheque-common-popup').click(function(){  
		    $('.ui-dialog-titlebar').remove();   
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/getbankaccountwithcheque_details.action", 
				 	async: false,  
	 			    dataType: "html",
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);   
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0); 
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				return false;
			});

	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
 	 		if(accessCode=="ASSET_INSURANCE_TYPE"){
				$('#insuranceType').html("");
				$('#insuranceType').append("<option value=''>Select</option>");
				loadLookupList("insuranceType"); 
			} else if(accessCode=="ASSET_INSURANCE_POLICYTYPE"){
				$('#policyType').html("");
				$('#policyType').append("<option value=''>Select</option>");
				loadLookupList("policyType"); 
			}  else if(accessCode=="ASSET_INSURANCE_PROVIDER"){
				$('#provider').html("");
				$('#provider').append("<option value=''>Select</option>");
				loadLookupList("provider"); 
			}  
		});
		
	 $('input,select').attr('disabled', true);

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });
	 
		$('.common-popup').click(
						function() {
							$('.ui-dialog-titlebar').remove();
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/show_common_asset_popup.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			top : 0,
			modal : true
		}); 

		//Combination pop-up config
	    $('.codecombination-popup').click(function(){ 
		      tempid=$(this).parent().get(0);   
		      combinationType = $(this).attr('id');
		      $('.ui-dialog-titlebar').remove();   
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/combination_treeview.action", 
				 	async: false,  
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $('.codecombination-result').html(result);  
					},
					error:function(result){ 
						 $('.codecombination-result').html(result); 
					}
				});  
		});
	     
	     $('#codecombination-popup').dialog({
				autoOpen: false,
				minwidth: 'auto',
				width:800, 
				bgiframe: false,
				modal: true 
			});

	     if(Number($('#assetInsuranceId').val())> 0){
			 $('#insuranceType').val("${ASSET_INSURANCE.lookupDetailByInsuranceType.lookupDetailId}");
			 $('#policyType').val("${ASSET_INSURANCE.lookupDetailByPolicyType.lookupDetailId}");
			 $('#provider').val("${ASSET_INSURANCE.lookupDetailByProvider.lookupDetailId}");
			 $('#insurancePeriod').val("${ASSET_INSURANCE.insurancePeriod}");
			 if(Number($('#cashAccount').val())> 0){
				 $('#Cash').show();
				 $('#paymentMode').val(0);
			 }
			 else{
				 $('#Bank').show();
				 $('#paymentMode').val(1);
			 }
		 }
	});
function insuranceDiscard(message){ 
 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_insurance.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();
				$("#main-wrapper").html(result);
				if (message != null && message != '') {
					$('#success_message').hide().html(message).slideDown(1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
	function commonAssetPopup(assetId, assetName) {
		$('#assetCreationId').val(assetId);
		$('#assetCreationName').val(assetName);
	}
	function setCombination(combinationTreeId, combinationTree){
 		$('#cashAccount').val(combinationTreeId);
		$('#cash_bank_account').val(combinationTree); 
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			insurance
		</div> 
		<form name="asset_insurance" id="asset_insurance" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetInsuranceId" name="assetInsuranceId"
					value="${ASSET_INSURANCE.assetInsuranceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 265px;">
							<div>
								<label class="width30" for="provider">Provider<span
									class="mandatory">*</span> </label> <select name="provider"
									id="provider" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="provider" items="${PROVIDER}">
										<option value="${provider.lookupDetailId}">${provider.displayName}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="premium">Premium<span
									class="mandatory">*</span> </label> <input type="text" id="premium"
									class="width50 validate[required,custom[number]]"
									value="${ASSET_INSURANCE.premium}">
							</div>
							<div>
								<label class="width30" for="contactPerson">Contact
									Person</label> <input type="text" id="contactPerson" class="width50"
									value="${ASSET_INSURANCE.contactPerson}">
							</div>
							<div>
								<label class="width30" for="contactNumber">Contact
									Number</label> <input type="text" id="contactNumber" class="width50"
									value="${ASSET_INSURANCE.contactNumber}">
							</div>
							<div>
								<label class="width30" for="brokerName">Broker</label> <input
									type="text" id="brokerName" class="width50"
									value="${ASSET_INSURANCE.brokerName}">
							</div>
							<div>
								<label class="width30" for="brokerNumber">Broker Number</label>
								<input type="text" id="brokerNumber" class="width50"
									value="${ASSET_INSURANCE.brokerNumber}">
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_INSURANCE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 265px;">
							<div>
								<label class="width30" for="assetCreationName">Asset
									Name<span class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="assetCreationName"
									value="${ASSET_INSURANCE.assetCreation.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="assetCreationId"
									value="${ASSET_INSURANCE.assetCreation.assetCreationId}" /> 
							</div>
							<div>
								<label class="width30" for="policyNumber">Policy Number<span
									class="mandatory">*</span> </label> <input type="text"
									id="policyNumber" class="width50 validate[required]"
									value="${ASSET_INSURANCE.policyNumber}">
							</div>
							<div>
								<label class="width30" for="startDate">Start Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_INSURANCE.startDate ne null && ASSET_INSURANCE.startDate ne ''}">
										<c:set var="startDate" value="${ASSET_INSURANCE.startDate}" />
										<%
											String fromDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"startDate").toString());
										%>
										<input type="text" id="startDate"
											class="width50 validate[required]" value="<%=fromDate%>"
											readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="startDate"
											class="width50 validate[required]" readonly="readonly">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="endDate">End Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_INSURANCE.endDate ne null && ASSET_INSURANCE.endDate ne ''}">
										<c:set var="endDate" value="${ASSET_INSURANCE.endDate}" />
										<%
											String toDate = DateFormat.convertDateToString(pageContext
															.getAttribute("endDate").toString());
										%>
										<input type="text" id="endDate"
											class="width50 validate[required]" value="<%=toDate%>"
											readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="endDate"
											class="width50 validate[required]" readonly="readonly">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="insuranceType">Insurance
									Type<span class="mandatory">*</span> </label> <select
									name="insuranceType" id="insuranceType"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="insuranceType" items="${INSURANCE_TYPE}">
										<option value="${insuranceType.lookupDetailId}">${insuranceType.displayName}</option>
									</c:forEach>
								</select> 
							</div>
							<div>
								<label class="width30" for="insurancePeriod">Insurance
									Period<span class="mandatory">*</span> </label> <select
									name="serviceLevel" id="insurancePeriod"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="insurancePeriod" items="${INSURANCE_PERIOD}">
										<option value="${insurancePeriod.key}">${insurancePeriod.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceLevel"
									value="${ASSET_INSURANCE.insurancePeriod}" />
							</div>

							<div>
								<label class="width30" for="policyType">Policy Type<span
									class="mandatory">*</span> </label> <select name="policyType"
									id="policyType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="policyType" items="${POLICY_TYPE}">
										<option value="${policyType.lookupDetailId}">${policyType.displayName}</option>
									</c:forEach>
								</select>  
							</div>
							<div>
								<label class="width30">Payment Mode<span
									style="color: red;">*</span> </label>
								<select name="paymentMode" class="width51 validate[required]" id="paymentMode">
									<option value="">Select</option>  
									<option value="0">Cash</option>  
									<option value="1">Bank</option>  
								</select> 
							</div>
							<div id="Bank" class="mode" style="display: none;">
								<div>
								<label class="width30">Account No<span class="mandatory">*</span></label>
								<input type="text" id="accountNumber" class="width50 accountNumber validate[required]" readonly="readonly" name="accountNumber" value="${ASSET_INSURANCE.chequeBook.bankAccount.bank.bankName} [${ASSET_INSURANCE.chequeBook.bankAccount.accountNumber}]"/> 
								 
								<input type="hidden" readonly="readonly" name="bankAccountId" id="bankAccountId" value="${ASSET_INSURANCE.chequeBook.bankAccount.bankAccountId}"/>
							</div> 
							<div>
								<label class="width30">Cheque No<span class="mandatory">*</span></label>
								<input type="text" id="chequeNumber" readonly="readonly" class="width50 chequeNumber validate[required]"
									 name="chequeNumber" value="${ASSET_INSURANCE.chequeNumber}"/>
								<input type="hidden" id="chequeId" value="${ASSET_INSURANCE.chequeBook.chequeBookId}"/>
							</div>
							<div>
								<label class="width30">Cheque Date<span class="mandatory">*</span></label>
									<c:choose>
										<c:when test="${ASSET_INSURANCE.chequeDate ne null && ASSET_INSURANCE.chequeDate ne ''}">
											<c:set var="cheqDate" value="${ASSET_INSURANCE.chequeDate}"/>  
											<%String cheqDate = DateFormat.convertDateToString(pageContext.getAttribute("cheqDate").toString());%>
											<input type="text" name="chequeDate" value="<%=cheqDate%>" 
												id="chequeDate" readonly="readonly" class="width50 validate[required]"> 
										</c:when>  
										<c:otherwise>  
											<input type="text" name="chequeDate" 
												id="chequeDate" readonly="readonly" class="width50 validate[required]"> 
										</c:otherwise>
								</c:choose>  
							 </div>
						 </div> 
						  <div id="Cash" class="mode" style="display: none;">
								<label class="width30">Cash Account<span
									style="color: red;">*</span> </label> <input type="hidden"
									id="cashAccount" name="cashAccount"
									value="${ASSET_INSURANCE.combination.combinationId}" />
								<c:choose>
									<c:when
										test="${ASSET_INSURANCE.combination ne null && ASSET_INSURANCE.combination.combinationId > 0}">
										<input type="text" id="cash_bank_account"
											value="${ASSET_INSURANCE.combination.accountByNaturalAccountId.account}.${ASSET_INSURANCE.combination.accountByAnalysisAccountId.account}"
											class="width50 validate[required]" readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="cash_bank_account"
											class="width50 validate[required]" readonly="readonly"> 
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div>