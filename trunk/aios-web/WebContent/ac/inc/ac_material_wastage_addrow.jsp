<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="hidden" name="productId" id="productid_${requestScope.rowId}" />
		<span id="product_${requestScope.rowId}" class="width60 float-left"></span> 
		<span class="width10 float-right" id="unitCode_${requestScope.rowId}" style="position: relative;"></span>
		<span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_product_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	<input type="hidden" name="shelfId" id="shelfId_${requestScope.rowId}"/>
	<input type="hidden" name="storeId" id="storeId_${requestScope.rowId}"/>
	<input type="hidden" name="productType" id="productType_${requestScope.rowId}"/>
	</td>
	<td>
		<select name="wastageType" id="wastageType_${requestScope.rowId}"
			class="width70 wastageType">
			<option value="">Select</option>
			<c:forEach var="wastageType" items="${WASTAGE_TYPE}">
				<option value="${wastageType.lookupDetailId}">${wastageType.displayName}</option>
			</c:forEach>
		</select> 
		 <span
		class="button float-right"> <a style="cursor: pointer;"
			id="MATERIAL_WASTAGE_TYPE_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_wastagetype_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="hidden" name="wastagepersonId"
		id="wastagepersonId_${requestScope.rowId}" /> <span id="wastageperson_${requestScope.rowId}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="prsnID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_wastageperson_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><select name="packageType" id="packageType_${requestScope.rowId}"
		class="packageType">
			<option value="">Select</option>
	</select></td>
	<td><input type="text"
		class="width96 packageUnit validate[optional,custom[number]]"
		id="packageUnit_${requestScope.rowId}" /></td>
	<td><input type="hidden" name="wastageQuantity"
		id="wastageQuantity_${requestScope.rowId}"
		class="wastageQuantity width98 validate[optional,custom[number]]">
		<input type="hidden" id="availableQunatity_${requestScope.rowId}"/>
		<span id="baseUnitConversion_${requestScope.rowId}" class="width10" style="display: none;"></span>
		<span id="baseDisplayQty_${requestScope.rowId}"></span>
	</td>
	<td><input type="text" name="description" id="description_${requestScope.rowId}"
		class="description width98" /></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;" title="Working"> <span
			class="processing"></span> </a> <input type="hidden"
		name="materialWastageDetailId" id="materialWastageDetailId_${requestScope.rowId}" /></td>
</tr>