<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 var oTable1; var selectRow1=""; var aSelected1 = []; var aData1="";
	$(function() {
		
		loadProductJsonData();
		/* Click event handler */
		$('#SalesProductCommonList tbody tr').live(
				'dblclick',
				function() {
					aData1 = oTable1.fnGetData(this);
					commonProductPopup(null, aData1);
					return false;
				});

		/* Click event handler */
		$('#SalesProductCommonList tbody tr').live('click', function() {
			if ($(this).hasClass('row_selected')) {
				$(this).addClass('row_selected');
 			} else {
				oTable1.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
 			}
 		});
		
	
	});
	
	function loadProductJsonData(){
		oTable1 = $('#SalesProductCommonList')
		.dataTable(
				{
					"bJQueryUI" : true,
					"sPaginationType" : "full_numbers",
					"bFilter" : true,
					"bInfo" : true,
					"bSortClasses" : true,
					"bLengthChange" : true,
					"bProcessing" : true,
					"bDestroy" : true,
					"iDisplayLength" : 10,
					"sAjaxSource" : "show_sales_product_common_jsonlist.action",

					"aoColumns" : [ {
						"mDataProp" : "code"
					}, {
						"mDataProp" : "productName"
					}, {
						"mDataProp" : "unitName"
					}, {
						"mDataProp" : "costingMethod"
					} ]
				});
	}
</script>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
		</div>
		<div class="portlet-content">
			<div id="product_json_list">
				<table id="SalesProductCommonList" class="display">
					<thead>
						<tr>
							<th>Code</th>
							<th>Product</th>
							<th>Unit Type</th>
							<th>Costing Method</th>
						</tr>
					</thead>
				</table>
			</div>
			
		</div>
	</div>
