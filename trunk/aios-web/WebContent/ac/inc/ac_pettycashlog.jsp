<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.flag {background: url(./images/checked.png); background-repeat: no-repeat; height: 30px; width:20px; float: left;}
</style>
<div id="hrm" class="hastable width100"  >  
 	<table id="hastab" class="width100"> 
		<thead>
		   <tr>  
			    <th>Voucher No</th>   
			    <th>Date</th>  
			    <th>Type</th> 
			    <th>Employee</th> 
			    <th>Expense COA</th> 
			    <th>Narration</th> 
			    <th>Cash In</th>  
			    <th>Cash Out</th> 
			    <th>Expense</th> 
			    <th>Remaining Balance</th>  
			    <th>Bill No</th> 
			    <th>Settle With</th> 
			    <th>Settled</th> 
			    <th>Created By</th> 
			    <th>Closed</th> 
		  </tr>
		</thead> 
		<tbody class="tab">  
			<c:forEach var="LOG_INFO" items="${PETTY_CASH_LOG}" varStatus="status">
				<tr class="rowid" id="fieldrow_${status.index+1}"> 
				<td>${LOG_INFO.pettyCashNo}</td>
				<td>${LOG_INFO.pettyCashDate}</td> 
				<td>${LOG_INFO.pettyCashTypeName}</td>
				<td>
					${LOG_INFO.employeeName}</td>
				<td>
					${LOG_INFO.expenseAccount}
				</td>
				<td>${LOG_INFO.description}</td>
				<td>
					 <c:if test="${LOG_INFO.cashIn ne null && LOG_INFO.cashIn ne '' && LOG_INFO.cashIn ne 0}">
					 	<c:set var="cashin" value="${LOG_INFO.cashIn}"/>
					 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("cashin"))%>
					 </c:if>
				</td>
				<td>
					<c:if test="${LOG_INFO.cashOut ne null && LOG_INFO.cashOut ne '' && LOG_INFO.cashOut ne 0}">
					 	<c:set var="cashout" value="${LOG_INFO.cashOut}"/>
					 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("cashout"))%>
					 </c:if>
				</td> 
				<td>
					<c:if test="${LOG_INFO.expenseAmount ne null && LOG_INFO.expenseAmount ne '' && LOG_INFO.expenseAmount ne 0}">
					 	<c:set var="expenseAmount" value="${LOG_INFO.expenseAmount}"/>
					 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("expenseAmount"))%>
					 </c:if>
				</td>
				<td>
					<c:if test="${LOG_INFO.remainingBalance ne null && LOG_INFO.remainingBalance ne '' && LOG_INFO.remainingBalance ne 0}">
					 	<c:set var="remainingBalance" value="${LOG_INFO.remainingBalance}"/>
					 	<%=AIOSCommons.formatAmount(pageContext.getAttribute("remainingBalance"))%>
					 </c:if>
				</td>
				<td>${LOG_INFO.invoiceNumber}</td>
				<td>${LOG_INFO.pettyCash.pettyCashNo}</td>
				<td>
					<c:if test="${LOG_INFO.settelmentFlag}">
						<span class="flag"></span>
					</c:if>
				</td>
				<td>${LOG_INFO.personByCreatedBy.firstName} ${LOG_INFO.personByCreatedBy.lastName}</td>
				<td>
					<c:if test="${LOG_INFO.closedFlag}">
						<span class="flag"></span>
					</c:if>
				</td> 
			</tr>
			</c:forEach>  
		 </tbody>
	</table>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right petty_log_discard" id="petty_log_discard" >Close</div>
		</div> 
</div> 