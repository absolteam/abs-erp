<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var salesDeliveryDetails="";
var chargesDetails = "";
var packingDetails = "";
var tempid = "";
var accessCode = "";
var sectionRowId = 0;
var tableRowId = 0;
$(function(){   
	 
	 

	 $('input,select').attr('disabled', true);

	 $('.common_person_list').click(function(){  
			$('.ui-dialog-titlebar').remove();   
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data: {  personTypes: "1"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/deliverynote_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrowscharges').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabcharges>.rowidcharges:last").attr('id'))); 
		  id=id+1;  
		  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/deliverynote_charges_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabcharges tr:last').before(result);
					 if($(".tabcharges").height()>255)
						 $(".tabcharges").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidcharges').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#chargesLineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrowspacks').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabpacks>.rowidpacks:last").attr('id'))); 
		  id=id+1;  
		  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/deliverynote_packs_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabpacks tr:last').before(result);
					 if($(".tabpacks").height()>255)
						 $(".tabpacks").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidpacks').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#packingLineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });


	 $('.shippingmethod-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id");  
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.shippingterm-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.chargestype-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 var str = $(this).attr("id");
			 accessCode = str.substring(0, str.lastIndexOf("_"));
			 sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
		    $.ajax({
		         type:"POST",
		         url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		         data:{accessCode: accessCode},
		         async: false,
		         dataType: "html",
		         cache: false,
		         success:function(result){ 
		              $('.common-result').html(result);
		              $('#common-popup').dialog('open');
						   $($($('#common-popup').parent()).get(0)).css('top',0);
		              return false;
		         },
		         error:function(result){
		              $('.common-result').html(result);
		         }
		     });
		 return false;
		});	
		
	//Lookup Data Roload call
	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SHIPPING_TERMS"){
			$('#shippingTerm').html("");
			$('#shippingTerm').append("<option value=''>Select</option>");
			loadLookupList("shippingTerm"); 
		} else if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingMethod').html("");
			$('#shippingMethod').append("<option value=''>Select</option>");
			loadLookupList("shippingMethod"); 
		}  else if(accessCode=="CHARGES_TYPE"){
			$('#chargesTypeId_'+sectionRowId).html("");
			$('#chargesTypeId_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("chargesTypeId_"+sectionRowId); 
		}  
	});

	  

 	//Combination pop-up config
  	$('.codecombination-popup').live('click',function(){ 
  	    tempid=$(this).parent().get(0);  
  	    $('.ui-dialog-titlebar').remove();   
  	 	$.ajax({
  			type:"POST",
  			url:"<%=request.getContextPath()%>/combination_treeview.action", 
  		 	async: false, 
  		    dataType: "html",
  		    cache: false,
  			success:function(result){   
  				 $('.codecombination-result').html(result);  
  			},
  			error:function(result){ 
  				 $('.codecombination-result').html(result); 
  			}
  		});  
  		return false;
  	});
  	
  	 $('#codecombination-popup').dialog({
  			autoOpen: false,
  			minwidth: 'auto',
  			width:800, 
  			bgiframe: false,
  			modal: true
  	 });

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.delrowcharges').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidcharges').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#chargesLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	 $('.delrowpacks').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidpacks').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#packingLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('#customer-common-popup').live('click',function(){  
    	$('#common-popup').dialog('close'); 
     });

	 $('#product-stock-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 }); 

     $('#store-common-popup').live('click',function(){  
	    $('#common-popup').dialog('close'); 
     });

	 $('#price-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 }); 

	 $('#sales-order-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 });
  
  	$('.show_customer_list').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false,  
		 	data: { pageInfo: "sales_delivery"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

  $('.show_salesorder_list').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_sales_orders.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	$('.show_product_list_delivery_note').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]);  
	 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_popup.action", 
		 	async: false,  
		 	data: {itemType: "I", rowId: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.show_store_list_sales_delivery').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]); 
		var productId = Number($('#productid_'+tableRowId).val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_common_popup.action", 
		 	async: false,  
		 	data: { rowId: tableRowId, productId: productId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0); 
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.show_productpricing_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]);  
		var productId = Number($('#productid_'+tableRowId).val());
		var storeId = Number($('#storeid_'+tableRowId).val()); 
		if(productId > 0){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_pricing.action", 
			 	async: false,  
			 	data: {productId: productId, storeId: storeId, rowId: tableRowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			}); 
		} 
		return false;
	});
 	

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	
	 
	
	$('.productQty').live('change',function(){   
 		var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($('#amount_'+rowId).val());
 	 	var productQty = Number($('#productQty_'+rowId).val());
 	 	$('#allowSave_'+rowId).val('true');
 	 	if(amount > 0 && productQty > 0){
 	 		var discount = Number($('#discount_'+rowId).val());
 	 		if(discount > 0){ 
 	 			var totalAmount = Number(amount * productQty); 
 	 			var basePrice = Number($('#basePrice_'+rowId).val());
 	 		 	var standardPrice = Number($('#standardPrice_'+rowId).val());
 	 			var discountAmount = 0; 
 	 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 	 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 			if(typeof pecentageMode == 'undefined' || typeof amountMode == 'undefined'){
 	 				pecentageMode = $('#chargesmodeDetail_'+rowId).val();
 	 				if(pecentageMode == "true"){
 	 					pecentageMode =  true;
 	 					amountMode = false;
 	 				} 
 	 				else{
 	 					pecentageMode = false;
 	 					amountMode = true;
 	 				} 
 	 			} 
 	 			if(pecentageMode == true) { 
 	 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 	 			totalAmount -= discountAmount;
 	 	 		}else if(amountMode == true){
 	 	 			totalAmount -= discount;
 	 	 	 	}
 	 			var averagePrice = Number(totalAmount / productQty);  
 	 	 		if(averagePrice < basePrice){ 
 	 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 	 				$('#allowSave_'+rowId).val('false');
 		 		}
 	 	 		else if(averagePrice < standardPrice){
 	 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 	 		}else
 	 	 			$('#fieldrow_'+rowId).css('background', '#fff');
  	 	 		$('#totalAmount_'+rowId).text(Number(totalAmount).toFixed(2));
 	 	 	}else
 	 			$('#totalAmount_'+rowId).text(Number(amount * productQty)); 
 	 	} 
 	 	calculateTotalSales();
 	 	triggerAddRow(rowId);
 		return false;
 	 });

 	$('.amount').live('change',function(){ 
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($('#amount_'+rowId).val());
 	 	var productQty = Number($('#productQty_'+rowId).val());
 	 	$('#allowSave_'+rowId).val('true');
 	 	if(amount > 0 && productQty > 0){
 	 		var discount = Number($('#discount_'+rowId).val());
 	 		if(discount > 0){
 	 			var totalAmount = Number(amount * productQty); 
 	 			var basePrice = Number($('#basePrice_'+rowId).val());
 	 		 	var standardPrice = Number($('#standardPrice_'+rowId).val());
 	 			var discountAmount = 0; 
 	 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 	 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 			if(pecentageMode == true) { 
 	 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 	 			totalAmount -= discountAmount;
 	 	 		}else if(amountMode == true){
 	 	 			totalAmount -= discount;
 	 	 	 	}
 	 			var averagePrice = Number(totalAmount / productQty);  
 	 	 		if(averagePrice < basePrice){ 
 	 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 	 				$('#allowSave_'+rowId).val('false');
 		 		}
 	 	 		else if(averagePrice < standardPrice){
 	 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 	 		}else
 	 	 			$('#fieldrow_'+rowId).css('background', '#fff');
  	 	 		$('#totalAmount_'+rowId).text(Number(totalAmount).toFixed(2));
 	 	 	}else
 	 			$('#totalAmount_'+rowId).text(Number(amount * productQty));
 	 		calculateTotalSales();
 	 		triggerAddRow(rowId);
 	 	} 
 		return false;
 	 });

 	$('.chargestypeid').live('change',function(){ 
 		triggerChargesAddRow(getRowId($(this).attr('id')));
 		return false;
 	 });

 	$('.packingType').live('change',function(){ 
 		triggerPacksAddRow(getRowId($(this).attr('id')));
 		return false;
 	 });	

 	$('.chargesmode').live('change',function(){ 
 		triggerChargesAddRow(getRowId($(this).attr('id')));
 		return false;
 	 });

 	$('.charges').live('change',function(){ 
 		triggerChargesAddRow(getRowId($(this).attr('id')));
 		return false;
 	 }); 

 	$('.chargesmodeDetail').live('change',function(){  
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($('#amount_'+rowId).val());
 	 	var discount = Number($('#discount_'+rowId).val()); 
 	 	var productQty = Number($('#productQty_'+rowId).val());
 	 	$('#allowSave_'+rowId).val('true');
 	 	if(amount > 0 && discount > 0) {
 	 		var basePrice = Number($('#basePrice_'+rowId).val());
	 		var standardPrice = Number($('#standardPrice_'+rowId).val());
 	 		var totalAmount = Number(amount * productQty); 
 			var discountAmount = 0;  
 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked'); 
 			if(pecentageMode == true) { 
 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 			totalAmount -= discountAmount;
 	 		}else{
 	 			totalAmount -= discount;
 	 	 	}
 			var averagePrice = Number(totalAmount / productQty);  
 	 		if(averagePrice < basePrice){ 
 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 				$('#allowSave_'+rowId).val('false');
	 		}
 	 		else if(averagePrice < standardPrice){
 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 		}else
 	 			$('#fieldrow_'+rowId).css('background', '#fff');
  	 		$('#totalAmount_'+rowId).text(Number(totalAmount).toFixed(2));
 	 	} else{
 	 		$('#totalAmount_'+rowId).text(Number(productQty * amount).toFixed(2));
 	 	} 
 	 	calculateTotalSales();
 	 	triggerAddRow(rowId);
 	});

 	$('.discount').live('change',function(){  
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($('#amount_'+rowId).val());
 	 	var discount = Number($('#discount_'+rowId).val()); 
 	 	var productQty = Number($('#productQty_'+rowId).val());
 	 	$('#allowSave_'+rowId).val('true');
 	 	if(amount > 0 && discount > 0) {
 	 		var totalAmount = Number(amount * productQty); 
 	 		var basePrice = Number($('#basePrice_'+rowId).val());
	 		var standardPrice = Number($('#standardPrice_'+rowId).val());
 			var discountAmount = 0;  
 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 			if(pecentageMode == true) { 
 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 			totalAmount -= discountAmount;
 	 		}else{
 	 			totalAmount -= discount;
 	 	 	}
  	 		$('#totalAmount_'+rowId).text(Number(totalAmount).toFixed(2));
  	 		var averagePrice = Number(totalAmount / productQty);  
 	 		if(averagePrice < basePrice){ 
 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 				$('#allowSave_'+rowId).val('false');
	 		}
 	 		else if(averagePrice < standardPrice){
 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 		}else
 	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 	} else{
 	 		$('#totalAmount_'+rowId).text(Number(productQty * amount).toFixed(2));
 	 	} 
 	 	calculateTotalSales();
 	 	triggerAddRow(rowId);
 	});
 	
 	if (Number($('#salesDeliveryNoteId').val()) > 0) {
 		$('#currency').val($('#tempCurrency').val());
 		$('#shippingMethod').val($('#tempShippingMethod').val());
 		$('#shippingTerm').val($('#tempShippingTerm').val());
  		$('#status').val($('#tempStatus').val());
 		$('#shippingDetail').val($('#tempShippingDetail').val());
 		$('#paymentMode').val($('#tempPaymentMode').val());
 		{
 			$('.rowid').each(function(){
 				var rowId = getRowId($(this).attr('id')); 
 				$('#chargesmodeDetail_'+rowId).val($('#tempChargesModeDetail_'+rowId).val()); 
 			}); 
 	 	}
 	 	{
 	 		$('.rowidcharges').each(function(){
 				var rowId = getRowId($(this).attr('id')); 
 				$('#chargesTypeId_'+rowId).val($('#tempChargesTypeId_'+rowId).val()); 
 				$('#chargesMode_'+rowId).val($('#tempChargesMode_'+rowId).val()); 
 			});
 	 	}
 	 	{
 	 		$('.rowidpacks').each(function(){
 				var rowId = getRowId($(this).attr('id')); 
 				$('#packingType_'+rowId).val($('#tempPackingType_'+rowId).val()); 
  			});
 	 	}
		{
			calculateTotalSales();
		}
 	 	if(Number($('#salesOrderId').val()) > 0){ 
 	 	 	$('.show_product_list_delivery_note, .chargestype-lookup, .show_salesorder_list, .show_customer_list, .common_person_list, .show_productpricing_list').remove();  
 	 	 	$('.amount')
	    	 .attr("disabled", true);
 	 	 	$('.rowidcharges').find(":input")
 	    	 .attr("disabled", true); 
 		} 
	}else {
		$('#status').val(7); 
 		$('#currency').val($('#defaultCurrency').val()); 
 	}
});

function triggerPriceCalc(rowId){
  	var amount = Number($('#amount_'+rowId).val());
 	var discount = Number($('#discount_'+rowId).val());
 	var chargesmodeDetail = $('#chargesmodeDetail_'+rowId).val();
 	var productQty = Number($('#productQty_'+rowId).val());
 	if(chargesmodeDetail!='' && amount > 0 && discount > 0) {
 		var totalAmount = Number(amount * productQty); 
		var discountAmount = 0; 
 	 	var chargesMode = $('#chargesmodeDetail_'+rowId).val();
 		if(chargesMode == "true") { 
 			discountAmount = Number(Number(amount * discount) / 100);
 			totalAmount -= discountAmount;
 		}else{
 			totalAmount -= discount;
 	 	}
 		$('#totalAmount_'+rowId).text(Number(totalAmount).toFixed(2));
 	}else{
 		$('#totalAmount_'+rowId).text(Number(productQty * amount).toFixed(2));
 	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function manupulateLastRow(){
	var hiddenSize=0;
	var tdSize = 0;
	var actualSize = 0;
	{
		hiddenSize=0;
		$($(".tab>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		tdSize=$($(".tab>tr:first").children()).size();
		actualSize=Number(tdSize-hiddenSize);  
		
		$('.tab>tr:last').removeAttr('id');  
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
		$($('.tab>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}  
	{
		hiddenSize=0;
		tdSize = 0;
		actualSize = 0;
		$($(".tabcharges>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		tdSize=$($(".tabcharges>tr:first").children()).size();
		actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabcharges>tr:last').removeAttr('id');  
		$('.tabcharges>tr:last').removeClass('rowidcharges').addClass('lastrow');  
		$($('.tabcharges>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabcharges>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}
	{
		hiddenSize=0;
		tdSize = 0;
		actualSize = 0;
		$($(".tabpacks>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		tdSize=$($(".tabpacks>tr:first").children()).size();
		actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabpacks>tr:last').removeAttr('id');  
		$('.tabpacks>tr:last').removeClass('rowidpacks').addClass('lastrow');  
		$($('.tabpacks>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabpacks>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}
}
 
function triggerAddRow(rowId){  
	 
	var productid = $('#productid_'+rowId).val();  
	var productQty = $('#productQty_'+rowId).val(); 
	var amount = Number($('#amount_'+rowId).val()); 
 	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid!=null && productid!=""
			&& productQty!=null && productQty!="" && amount > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}

function triggerChargesAddRow(rowId){  
	var chargesTypeId = $('#chargesTypeId_'+rowId).val();  
 	var charges = $('#charges_'+rowId).val();
	var nexttab=$('#fieldrowcharges_'+rowId).next();  
	if(chargesTypeId!=null && chargesTypeId!="" 
			&& charges!=null && charges!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImagecharges_'+rowId).show();
		$('.addrowscharges').trigger('click');
	} 
} 

function triggerPacksAddRow(rowId){  
	var packingType = $('#packingType_'+rowId).val();  
	var nexttab=$('#fieldrowpacks_'+rowId).next();  
	if(packingType!=null && packingType!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImagepacks_'+rowId).show();
		$('.addrowspacks').trigger('click');
	} 
}
function personPopupResult(personId, personName, commonParam){  
	$('#salesPerson').val(personName);
	$('#salesPersonId').val(personId); 
}
function commonProductPricePopup(productPricingCalcId, calculationTypeCode, 
		calculationSubTypeCode, salesAmount, rowId) {  
	$('#amount_' + rowId).val(salesAmount);
 	triggerPriceCalc(rowId);
	return false;
}
function showShippingSite(customerId) {
	var customerId = $('#customerId').val(); 
	$('#shippingDetail').html('');
	$('#shippingDetail')
	.append(
			'<option value="">Select</option>');
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_shipping_site.action", 
	 	async: false,  
	 	data: {customerId: customerId},
	 	dataType: "json",
	    cache: false,
		success:function(response){   
		$(response.aaData)
			.each(
					function(index) {
				$('#shippingDetail')
					.append('<option value='
							+ response.aaData[index].shippingId
							+ '>' 
							+ response.aaData[index].name
							+ '</option>');
			}); 
			$('#shippingDetail').val(response.aaData[0].shippingId);
		} 
	});  
	return false;
}
function calculateTotalSales() { 
	var totalSales = 0;
	$('#totalSalesAmount').text('');
	$('.rowid1').each(function() {
		var rowId = getRowId($(this).attr('id'));
		var totalamount = Number($.trim($('#totalAmount_' + rowId).text()));
		totalSales = Number(totalSales + totalamount);
	});
	$('#totalSalesAmount').text(Number(totalSales).toFixed(2));
}
function loadSalesOrders(salesOrderId , salesOrder) {
	$('#salesOrderId').val(salesOrderId); 
	$('#salesOrder').val(salesOrder); 
	$('#status').val(7); 
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_sales_order_info.action",
					async : false,
					data : {
						salesOrderId : salesOrderId
					},
					dataType : "json",
					cache : false,
					success : function(response) {
						{
							$('#shippingDetail').html('');
							$('#shippingDetail').append(
									'<option value="">Select</option>');
							$('#salesOrderId').val(
									response.salesOrderVO.salesOrderId);
							$('#salesOrder').val(
									response.salesOrderVO.referenceNumber);
							$('#currency')
									.val(response.salesOrderVO.currencyId);
							$('#shippingMethod').val(
									response.salesOrderVO.shippingMethodId);
							$('#shippingTerm').val(
									response.salesOrderVO.shippingTermsId);
							$('#paymentMode').val(
									response.salesOrderVO.modeOfPayment);
							$('#salesPerson').val(
									response.salesOrderVO.salesReperesentive);
							$('#salesPersonId').val(
									response.salesOrderVO.salesReperesentiveId);
							$('#customerName').val(
									response.salesOrderVO.customerName);
							$('#customerId').val(
									response.salesOrderVO.customerId);
							$('#description').val(
									response.salesOrderVO.description);
							$('#continuousSales').attr('checked',response.salesOrderVO.continuousSales);
							$('#continuous-div').show();
							$(response.salesOrderVO.customer.shippingDetails)
									.each(
											function(sindex) {
												$('#shippingDetail')
														.append(
																'<option value='
							+ response.salesOrderVO.customer.shippingDetails[sindex].shippingId
							+ '>'
																		+ response.salesOrderVO.customer.shippingDetails[sindex].name
																		+ '</option>');
											});
							$('#shippingDetail').val(response.salesOrderVO.shippingDetailId);
						}
						$('.tab').html('');
						$('.tabcharges').html('');
						$('.options_').hide();
						{
							var counter = 0;
							$(response.salesOrderVO.salesOrderDetailVOs)
									.each(
											function(index) {
											if(response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs!=null){
												$(response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs).each(
														function(index1) {
															var deliveredOldQty = response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].deliveredQuantity; 
															if(deliveredOldQty == 0){
																deliveredOldQty = "-N/A-";
															} 
															$('.tab')
															.append("<tr class='salesDetail'>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].productName+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].storeName+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].orderQuantity+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+deliveredOldQty+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].remainingQuantity+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].deliveryQuantity+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span class='float-right'>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].unitRate+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].strDiscountMode+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span class='float-right'>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].discount+"</span>"
																	+ "</td>"
																	+ "<td>"
																		+ "<span class='float-right'>"+response.salesOrderVO.salesOrderDetailVOs[index].salesDeliveryDetailVOs[index1].total+"</span>"
																	+ "</td>"
																	+"</tr>");
															 
														});
											}	
											
												counter += 1;
												var storeId = 0;
												var shelfId = 0;
												var storeName = "";
												var isPercentage = "";
												var mode = "";
												var discount = "";
 												var orderQuantity = response.salesOrderVO.salesOrderDetailVOs[index].orderQuantity;
												var unitRate = response.salesOrderVO.salesOrderDetailVOs[index].unitRate;
												var totalAmount = Number(orderQuantity
														* unitRate);
												var deliveredQty = response.salesOrderVO.salesOrderDetailVOs[index].deliveredQty; 
												var remainingQty = response.salesOrderVO.salesOrderDetailVOs[index].remainingQtyQty; 
												 
												if(response.salesOrderVO.continuousSales!= null &&
															response.salesOrderVO.continuousSales == true){
													$('.delivered-div').show();
												}else{
													$('.delivered-div').hide();
												}
												if(deliveredQty == 0){
													deliveredQty = "-N/A-";
												}
												if (response.salesOrderVO.salesOrderDetailVOs[index].storeName != null) {
													storeId = response.salesOrderVO.salesOrderDetailVOs[index].storeId;
													shelfId = response.salesOrderVO.salesOrderDetailVOs[index].shelfId;
													storeName = response.salesOrderVO.salesOrderDetailVOs[index].storeName;
												}
												 
												if (response.salesOrderVO.salesOrderDetailVOs[index].isPercentage != null) { 
													discount = response.salesOrderVO.salesOrderDetailVOs[index].discount;
													var discountAmount = 0;
													if (response.salesOrderVO.salesOrderDetailVOs[index].isPercentage == true) {
														discountAmount = Number(totalAmount
																* discount) / 100;
														totalAmount -= discountAmount;
														isPercentage = "true";
														mode = "Percentage";
													} else {
														totalAmount -= discount;
														isPercentage = "false";
														mode = "Amount";
													}
												} else {
													$('#discount_' + counter)
															.hide();
												}
												totalAmount = Number(
														totalAmount).toFixed(2);
												$('.tab')
														.append(

																"<tr id='fieldrow_"+counter+"' class='rowid'>"
																		+ "<td id='lineId_"+counter+"' style='display:none;'>"
																		+ counter
																		+ "</td>"
																		+ "<td>"
																		+ "<input type='hidden' id='productid_"+counter+"'"
										+" value='"+response.salesOrderVO.salesOrderDetailVOs[index].product.productId+"' name='productId'/>"
																		+ "<span id='product_"+counter+"'>"
																		+ response.salesOrderVO.salesOrderDetailVOs[index].product.productName
																		+ "</span>"
																		+ "</td>"
																		+ "<td>"
																		+ "<input type='hidden' value='"+storeId+"' id='storeid_"+counter+"' name='storeId'>"
																		+ "<input type='hidden' id='shelfid_"+counter+"' value='"+shelfId+"' name='shelfId'>"
																		+ "<span id=store_"+counter+">"
																		+ storeName
																		+ "</span>"
																		+ "<span class='button float-right'>"
																		+ "<a class='show_store_list_sales_delivery btn ui-state-default ui-corner-all width100' id='storeID_"+counter+"'"
												 +"style='cursor: pointer;'>"
																		+ "<span class='ui-icon ui-icon-newwin'></span>"
																		+ "</a>"
																		+ "</span>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span>"+orderQuantity+"</span>"
																		+ "<input type='hidden' readOnly='readOnly' id='orderQty_"+counter+"' name='orderQty' value='"+orderQuantity+"'"
										+"class='orderQty width80'/>"
																		+ "</td>"
																		+ "<td class='delivered-div' style='display: none;'>"
																		+ "<span>"+deliveredQty+"</span>" 
																		+ "</td>"
																		+ "<td class='delivered-div' style='display: none;'>"
																		+ "<span>"+remainingQty+"</span>" 
																		+ "</td>"
																		+ "<td>"
																		+ "<input type='text' id='productQty_"+counter+"' name='productQty'"
										+"class='productQty validate[optional,custom[number]] width98'/>"
																		+ "<span class='button float-right'>"
																		+ "<a class='baseprice-popup btn ui-state-default ui-corner-all width100' id='showBasePrice_"+counter+"'"
												 +"style='cursor: pointer; position: relative; top: 7px; display: none;'>"
																		+ "<span class='ui-icon ui-icon-newwin'></span>"
																		+ "</a>"
																		+ "</span>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span class='float-right' id='amountstr_"+counter+"'>"+Number(unitRate).toFixed(2)+"</span>"
																		+ "<input type='hidden' name='amount' id='amount_"+counter+"' value='"+unitRate+"'"
										+"class='amount validate[optional,custom[number]] width80 right-align'/>" 
																		+ "<input type='hidden' name='basePrice' value="+ response.salesOrderVO.salesOrderDetailVOs[index].basePrice+" id='basePrice_"+counter+"'/>"
																		+ "<input type='hidden' name='standardPrice' value="+response.salesOrderVO.salesOrderDetailVOs[index].standardPrice+" id='standardPrice_"+counter+"'/>"
																		+ "<input type='hidden' name='allowSave' id='allowSave_"+counter+"'/>"
																		+ "<span class='button float-right'>"
																		+ "<a class='show_productpricing_list btn ui-state-default ui-corner-all width100' id='pricingDetail_"+counter+"'"
												 +"style='cursor: pointer; position: relative; top: 7px;display:none;'>"
																		+ "<span class='ui-icon ui-icon-newwin'></span>"
																		+ "</a>"
																		+ "</span>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span class='width98'>"
																		+ mode
																		+ "</span>"
																		+ "<input type='hidden' value='"+isPercentage+"' id='chargesmodeDetail_"+counter+"'"
										+" class='chargesmodeDetail' name='chargesmodeDetail'/>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span class='float-right'>"+Number(discount).toFixed(2)+"</span>"
																		+ "<input type='hidden' readOnly='readOnly' value='"+discount+"' id='discount_"+counter+"'"
										+" class='discount width98 validate[optional,custom[number]] right-align' name='discount' style='text-align:right;'/>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span style='float: right;' id='totalAmount_"+counter+"'>"
																		+ totalAmount
																		+ "</span>"
																		+ "</td>"
																		+ "<td style='display:none;'>"
																		+ "<input type='hidden' id='salesDeliveryDetailId_"+counter+"' name='salesDeliveryDetailId'>"
																		+ "<input type='hidden' id='salesOrderDetailId_"+counter+"' name='salesOrderDetailId' value='"+response.salesOrderVO.salesOrderDetailVOs[index].salesOrderDetailId+"'>"
																		+ "<input type='hidden' id='availableqty_"+counter+"' name='availableqty' value='"+response.salesOrderVO.salesOrderDetailVOs[index].availableQty+"'>"
																		+ "</td>"
																		+ "</tr>");
												
												if(response.salesOrderVO.continuousSales!= null &&
														response.salesOrderVO.continuousSales == true){
													$('.delivered-div').show();
												}else{
													$('.delivered-div').hide();
												}
											});
						}

						{
							if (response.salesOrderVO.salesOrderCharges != null
									&& response.salesOrderVO.salesOrderCharges != '') {
								var ccounter = 0;
								$(response.salesOrderVO.salesOrderCharges)
										.each(
												function(cindex) {
													ccounter += 1;
													var chargesType = response.salesOrderVO.salesOrderCharges[cindex].lookupDetail.displayName;
													var chargesTypeId = response.salesOrderVO.salesOrderCharges[cindex].lookupDetail.lookupDetailId;
													var isChargePercentage = "";
													var chargeMode = "";
													if (response.salesOrderVO.salesOrderCharges[cindex].isPercentage == true) {
														isChargePercentage = "true";
														chargeMode = "Percentage";
													} else {
														isChargePercentage = "false";
														chargeMode = "Amount";
													}
													var charges = response.salesOrderVO.salesOrderCharges[cindex].charges;
													$('.tabcharges')
															.append(
																	"<tr id='fieldrowcharges_"+ccounter+"' class='rowidcharges'>"
																			+ "<td id='chargesLineId_"+ccounter+"'>"
																			+ ccounter
																			+ "</td>"
																			+ "<td>"
																			+ "<span class='width98'>"
																			+ chargesType
																			+ "</span>"
																			+ "<input type='hidden' value='"+chargesTypeId+"' id='chargesTypeId_"+ccounter+"'"
											+" class='chargestypeid width90' name='chargesTypeId'/>"
																			+ "</td>"
																			+ "<td style='display: none;'>"
																			+ "<span class='width90'>"
																			+ chargeMode
																			+ "</span>"
																			+ "<input type='hidden' value='"+isChargePercentage+"' id='chargesMode_"+ccounter+"'"
											+" class='chargesmode' name='chargesMode'/>"
																			+ "</td>"
																			+ "<td>"
																			+"<span class='float-right'>"+Number(charges).toFixed(2)+"</span>"
																			+ "<input type='hidden' readOnly='readOnly' value='"+charges+"' id='charges_"+ccounter+"'"
											+" class='charges  validate[optional,custom[number]] width98 right-align' name='charges' style='text-align:right;'/>"
																			+ "</td>"
																			+ "<td>"
																			+"<span>"+response.salesOrderVO.salesOrderCharges[cindex].description+"</span>"
																			+ "<input type='hidden' value='"+response.salesOrderVO.salesOrderCharges[cindex].description+"'"
											+" id='description_"+ccounter+"' class='description width98' name='description'/>"
																			+ "</td>"
																			+ "<td style='display:none;'>"
																			+ "<input type='hidden' id='salesDeliveryChargeId_"+ccounter+"' name='salesDeliveryChargeId'>"
																			+ "</td>"
																			+ "</tr>");
												});
							} else {
								$('.deliveryChargesContent').remove();
							}
						}
					}
				});
				 $('.rowid').each(function(){   
					 var rowId=getRowId($(this).attr('id')); 
					 var orderQty = Number($('#orderQty_'+rowId).val());
					 var availableqty = Number($('#availableqty_'+rowId).val());
					 if(orderQty > availableqty)
						 $('#fieldrow_'+rowId).css('background','#FFFF00');
				 });  
		return false;
	}
	function setCombination(combinationTreeId, combinationTree) {
		var idVals = $(tempid).attr('id').split("_");
		var rowids = Number(idVals[1]);
		$('#combinationId_' + rowids).val(combinationTreeId);
		$('#codeCombination_' + rowids).text(combinationTree);
		$('#codeCombination_' + rowids).text().replace(/[\s\n\r]+/g, ' ')
				.trim();
		triggerAddRow(rowids);
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName);
		showShippingSite(customerId);
	}

	function commonProductPopup(aData, rowId) {
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.productName);
		$('#storeid_' + rowId).val(aData.storeId);
		$('#store_' + rowId).html(aData.storeName);
		$('#shelfid_' + rowId).val(aData.shelfId);  
		$('#basePrice_' + rowId).val(aData.costPrice); 
		$('#standardPrice_' + rowId).val(aData.standardPrice); 
		var customerId = Number($('#customerId').val()); 
		if(customerId > 0){
			checkCustomerStoreSalesPrice(aData.productId, customerId, aData.unitRate, aData.storeId, rowId);
		}else{
			$('#amount_' + rowId).val(aData.unitRate); 
			$('#amountstr_' + rowId).text(aData.unitRate); 
			
		} 
		calculateTotalSales();
		$('#product-common-popup').trigger('click');
		return false;
	}
	
	function checkCustomerStoreSalesPrice(productId, customerId, finalUnitPrice, storeId, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcustomer_store_salesprice.action",
				data : {
					productId : productId, customerId: customerId, storeId: storeId
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) { 
					if(response.sellingPrice != null && response.sellingPrice != 0){
						$('#amount_' + rowId).val(response.sellingPrice); 
						$('#amountstr_' + rowId).text(response.sellingPrice); 
					}else{
						$('#amount_' + rowId).val(finalUnitPrice); 
						$('#amountstr_' + rowId).text(aData.unitRate); 
					}
				}
			});
		calculateTotalSales();
	}
	
	function commonStorePopup(storeId, storeName, shelfId, rowId) {
		$('#storeid_' + rowId).val(storeId);
		$('#store_' + rowId).html(storeName); 
		calculateTotalSales();
		$('.baseprice-popup').trigger('click');
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> 
			Delivery Note
		</div> 
		<form id="deliveryNoteValidation" method="POST" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="salesDeliveryNoteId"
					name="salesDeliveryNoteId"
					value="${DELIVERY_NOTE.salesDeliveryNoteId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 180px;">
							<div>
								<label class="width30">Sales Person</label>
								<c:choose>
									<c:when
										test="${DELIVERY_NOTE.personBySalesPersonId ne null && DELIVERY_NOTE.personBySalesPersonId ne ''}">
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50"
											value="${DELIVERY_NOTE.personBySalesPersonId.firstName } ${DELIVERY_NOTE.personBySalesPersonId.lastName}" />
										<input type="hidden" readonly="readonly" name="salesPersonId" id="salesPersonId"
												value="${DELIVERY_NOTE.personBySalesPersonId.personId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50" value="${requestScope.personName}"/>
										<input type="hidden" readonly="readonly" name="salesPersonId" id="salesPersonId"
												value="${requestScope.salesPersonId}" />
									</c:otherwise>
								</c:choose>  
							</div>
							<div>
								<label class="width30"> Payment Mode</label> <select
									name="paymentMode" id="paymentMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="paymentMode" items="${PAYMENT_MODE}">
										<option value="${paymentMode.key}">${paymentMode.value}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempPaymentMode"
									id="tempPaymentMode"
									value="${DELIVERY_NOTE.modeOfPayment}" />
							</div>
							<div>
								<label class="width30"> Shipping Method
									</label> <select
									name="shippingMethod" id="shippingMethod" class="width51">
									<option value="">Select</option>
									<c:forEach var="METHOD" items="${SHIPPING_METHOD}">
										<option value="${METHOD.lookupDetailId}">${METHOD.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempShippingMethod"
									id="tempShippingMethod"
									value="${DELIVERY_NOTE.lookupDetailByShippingMethod.lookupDetailId}" /> 
							</div>
							<div>
								<label class="width30"> Shipping Terms</label> <select
									name="shippingTerm" id="shippingTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${SHIPPING_TERMS}">
										<option value="${TERMS.lookupDetailId}">${TERMS.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempShippingTerm"
									id="tempShippingTerm"
									value="${DELIVERY_NOTE.lookupDetailByShippingTerm.lookupDetailId}" /> 
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${DELIVERY_NOTE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 180px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.</label> <span
									style="font-weight: normal;"> <c:choose>
										<c:when
											test="${DELIVERY_NOTE.salesDeliveryNoteId ne null 
												&& DELIVERY_NOTE.salesDeliveryNoteId gt 0}">
												<input type="text" value="${DELIVERY_NOTE.referenceNumber}" class="width50" readonly="readonly"/>
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${DELIVERY_NOTE.referenceNumber}"
									name="tempreferenceNumber" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30">Sales Order</label> <input type="text"
									readonly="readonly" name="salesOrder"
									value="${DELIVERY_NOTE.salesOrder.referenceNumber}"
									id="salesOrder" class="width50" /> <input type="hidden"
									id="salesOrderId" name="salesOrderId"
									value="${DELIVERY_NOTE.salesOrder.salesOrderId}" />  
							</div>
							<div>
								<label class="width30"> Delivery Date </label>
								<c:choose>
									<c:when
										test="${DELIVERY_NOTE.deliveryDate ne null && DELIVERY_NOTE.deliveryDate ne ''}">
										<c:set var="deliveryDate"
											value="${DELIVERY_NOTE.deliveryDate}" />
										<input name="deliveryDate" type="text" readonly="readonly"
											id="deliveryDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("deliveryDate").toString())%>"
											class="deliveryDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="deliveryDate" type="text" readonly="readonly"
											id="deliveryDate"
											class="deliveryDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Currency</label> <select name="currency"
									id="currency" class="width51">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${DELIVERY_NOTE.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div> 
							<div>
								<label class="width30">Customer Name</label>
								<c:choose>
									<c:when
										test="${DELIVERY_NOTE.customer.personByPersonId ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${DELIVERY_NOTE.customer.personByPersonId.firstName} ${DELIVERY_NOTE.customer.personByPersonId.lastName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:when test="${DELIVERY_NOTE.customer.company ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${DELIVERY_NOTE.customer.company.companyName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:when
										test="${DELIVERY_NOTE.customer.cmpDeptLocation ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${DELIVERY_NOTE.customer.cmpDeptLocation.company.companyName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="customerName"
											id="customerName" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
								<input type="hidden" id="customerId" name="customerId"
									value="${DELIVERY_NOTE.customer.customerId}" />  
							</div>
							<div>
								<label class="width30"> Shipping Address</label> <select
									name="shippingDetail" id="shippingDetail" class="width51 validate[required]">
									<option value="">Select</option>
									<c:if
										test="${DELIVERY_NOTE.customer ne null &&  DELIVERY_NOTE.customer ne ''}">
										<c:forEach var="SHIPPING"
											items="${DELIVERY_NOTE.customer.shippingDetails}">
											<option value="${SHIPPING.shippingId}">${SHIPPING.name}</option>
										</c:forEach>
									</c:if>
								</select> <input type="hidden" id="tempShippingDetail"
									name="tempShippingDetail"
									value="${DELIVERY_NOTE.shippingDetail.shippingId}" />
							</div> 
							<div style="display: none;" id="continuous-div">
								<label class="width30" for="continuousSales">Continuous Sales</label>  
								<c:choose>
									<c:when test="${DELIVERY_NOTE.salesOrder.continuousSales ne null && DELIVERY_NOTE.salesOrder.continuousSales eq true}">
										<input type="checkbox" name="continuousSales" id="continuousSales" class="width3" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="continuousSales" id="continuousSales" class="width3" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Status</label> <select name="status"
									id="status" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${DELIVERY_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus" name="lookupDetailId"
									value="${DELIVERY_NOTE.status}" />
							</div> 
						</fieldset>
					</div>

				</div>
				<div class="clearfix"></div>
				<div id="line-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;"> 
					<fieldset>
						<legend>Delivery Detail</legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width5">Product</th>
										<th class="width5">Store</th>
										<th class="width5">Order.Qty</th>
										<th class="delivered-div width5" 
											style="display:none;">Delivered.Qty</th>
										<th class="delivered-div width5" 
											style="display:none;">Remaining.Qty</th>
										<th class="width5">Delivery.Qty</th>
										<th class="width5">Price</th>
										<th class="width5">Mode</th>
										<th class="width5">Discount</th>
										<th class="width5">Total</th>
										<th style="width: 1%; display: none;" class="options options_"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${DELIVERY_NOTE.salesDeliveryDetailVOs}"
										varStatus="status">
										<tr class="rowid1" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.productName}</span> 
											</td>
											<td><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.shelf.shelf.aisle.store.storeId}" />
												<input type="hidden" name="shelfId"
												id="shelfid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" />
												<span
												id="store_${status.index+1}">
													${DETAIL.shelf.shelf.aisle.store.storeName}>>${DETAIL.shelf.shelf.aisle.sectionName}>>${DETAIL.shelf.shelf.name}>>${DETAIL.shelf.name}
												</span> 
											</td>
											<td>
											<c:choose>
												<c:when test="${DETAIL.orderQuantity ne null && DETAIL.orderQuantity gt 0}">
													<input type="text" name="orderQty" id="orderQty_${status.index+1}"
														value="${DETAIL.orderQuantity}" class="orderQty width80">
												</c:when>
												<c:otherwise>
													<input type="text" name="orderQty" id="orderQty_${status.index+1}" class="orderQty width80">
												</c:otherwise>
											</c:choose> 
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}"
												value="${DETAIL.deliveryQuantity}"
												class="productQty validate[optional,custom[number]] width98">  
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="amount validate[optional,custom[number]] width50 right-align">   
												<input type="hidden" name="basePrice" id="basePrice_${status.index+1}" value="${DETAIL.basePrice}"/> 
												<input type="hidden" name="standardPrice" id="standardPrice_${status.index+1}" value="${DETAIL.standardPrice}"/>
												<input type="hidden" name="allowSave" id="allowSave_${status.index+1}" value="true"/> 
											</td>
											<td> 
												<c:choose> 
													<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq true}">
														<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																 id="pecentagemode_${status.index+1}" checked="checked"/>
															<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
															<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																id="amountmode_${status.index+1}" style="width:5px;"/>
															<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
													</c:when> 
													<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq false && DETAIL.discount gt 0}">
														<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																 id="pecentagemode_${status.index+1}"/>
															<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
															<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																id="amountmode_${status.index+1}" value="false" style="width:5px;" checked="checked"/>
															<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
													</c:when>
													<c:otherwise>
														<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																 id="pecentagemode_${status.index+1}" value="true"/>
															<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
															<div class="float-left">
															<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																id="amountmode_${status.index+1}" value="false" style="width:5px;"/>
															<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
													</c:otherwise> 
												</c:choose> 
											</td>
											<td>
												<c:choose>
													<c:when test="${DETAIL.discount ne null && DETAIL.discount gt 0}">
														<input type="text" name="discount" id="discount_${status.index+1}" value="${DETAIL.discount}"
														 style="text-align: right;" class="discount validate[optional,custom[number]] width98 right-align">
													</c:when>
													<c:otherwise>
														<input type="text" name="discount" id="discount_${status.index+1}"style="text-align: right;"
														 class="discount validate[optional,custom[number]] width98 right-align">
													</c:otherwise>
												</c:choose> 
											</td>
											<td> <span
												id="totalAmount_${status.index+1}" style="float: right;">${DETAIL.total}</span>
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="salesDeliveryDetailId"
												id="salesDeliveryDetailId_${status.index+1}"
												value="${DETAIL.salesDeliveryDetailId}" />
												<input type="hidden" name="salesOrderDetailId"
													id="salesOrderDetailId_${status.index+1}" value="${DETAIL.salesOrderDetail.salesOrderDetailId}"/>
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
				<div class="clearfix"></div>
				<div class="float-right width18" style="font-weight: bold;">
					<span style="font-weight: bold;">Total: </span> <span
						id="totalSalesAmount"></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<c:if test="${DELIVERY_NOTE.salesDeliveryCharges ne null && fn:length(DELIVERY_NOTE.salesDeliveryCharges)>0}"> 
				<div class="portlet-content class90 deliveryChargesContent" id="hrm"
					style="margin-top: 5px;">
					<fieldset>
						<legend>Addtional Charges</legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab_charges" class="width100">
								<thead>
									<tr>
										<th style="width: 1%">L.NO</th>
										<th class="width10">Charges Type</th>
										<th style="width: 5%; display: none;">Mode</th>
										<th style="width: 5%;">Charges</th>
										<th class="width10">Description</th>
										<th style="width: 1%; display: none;" class="options options_"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tabcharges">
									<c:forEach var="CHARGES"
										items="${DELIVERY_NOTE.salesDeliveryCharges}"
										varStatus="status">
										<tr class="rowidcharges1" id="fieldrowcharges_${status.index+1}">
											<td id="chargesLineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="tempchargesTypeId"
												id="tempChargesTypeId_${status.index+1}"
												value="${CHARGES.lookupDetail.lookupDetailId}" /> <select
												name="chargesTypeId" id="chargesTypeId_${status.index+1}"
												class="chargestypeid width70">
													<option value="">Select</option>
													<c:forEach var="TYPE" items="${CHARGES_TYPE}">
														<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
													</c:forEach>
											</select> 
											</td>
											<td style="display: none;"><input type="hidden" name="tempChargesMode"
												id="tempChargesMode_${status.index+1}"
												value="${CHARGES.isPercentage}" /> <select
												name="chargesMode" id="chargesMode_${status.index+1}"
												class="chargesmode width90">
													<option value="">Select</option>
													<option value="true">Percentage</option>
													<option value="false">Amount</option>
											</select>
											</td>
											<td><input type="text" name="charges"
												id="charges_${status.index+1}" value="${CHARGES.charges}"
												class="charges validate[optional,custom[number]] width98" style="text-align: right;">
											</td>
											<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${CHARGES.description}" class="description width98">
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="optioncharges_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
												id="AddImagecharges_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
												id="EditImagecharges_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
												id="DeleteImagecharges_${status.index+1}"
												style="cursor: pointer;" title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImagecharges_${status.index+1}"
												style="display: none;" title="Working"> <span
													class="processing"></span> </a> <input type="hidden"
												name="salesDeliveryChargeId"
												id="salesDeliveryChargeId_${status.index+1}"
												value="${CHARGES.salesDeliveryChargeId}" />
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</c:if>
			<div class="clearfix"></div>
			<c:if test="${DELIVERY_NOTE.salesDeliveryPacks ne null && fn:length(DELIVERY_NOTE.salesDeliveryPacks)>0}"> 
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 5px;">
					<fieldset>
						<legend>Packing Details</legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab_packing" class="width100">
								<thead>
									<tr>
										<th style="width: 3%">Ref.NO</th>
										<th style="width: 5%;">Type</th>
										<th style="width: 3%;">Pack Size</th>
										<th style="width: 5%;">Gross Weight</th>
										<th style="width: 5%;">Dispatcher</th>
										<th class="width10">Description</th>
										<th style="width: 1%; display: none;" class="options"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tabpacks">
									<c:forEach var="PACKS"
										items="${DELIVERY_NOTE.salesDeliveryPacks}"
										varStatus="pkstatus">
										<tr class="rowidpacks1" id="fieldrowpacks_${pkstatus.index+1}">
											<td id="packingLineId_${pkstatus.index+1}"
												style="display: none;">${pkstatus.index+1}</td>
											<td><input type="text" name="packReferenceNumber"
												id="referenceNumber_${pkstatus.index+1}"
												value="${PACKS.referenceNumber}"
												class="packReferenceNumber width98">
											</td>
											<td><input type="hidden" name="tempPackingType"
												id="tempPackingType_${pkstatus.index+1}"
												value="${PACKS.lookupDetailByProductUnit.lookupDetailId}" /> <select
												name="packingType" id="packingType_${pkstatus.index+1}"
												class="packingType width90">
													<option value="">Select</option>
													<c:forEach var="TYPE" items="${PRODUCT_UNIT}">
														<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
													</c:forEach>
											</select>
											</td>
											<td><input type="text" name="noOfPacks"
												id="noOfPacks_${pkstatus.index+1}" value="${PACKS.noOfPacks}"
												class="noOfPacks width98">
											</td>
											<td><input type="text" name="grossWeight"
												id="grossWeight_${pkstatus.index+1}"
												value="${PACKS.grossWeight}" class="grossWeight width98">
											</td>
											<td><input type="text" name="dispatcher"
												id="dispatcher_${pkstatus.index+1}"
												value="${PACKS.dispatcher}" class="dispatcher width98">
											</td>
											<td><input type="text" name="description"
												id="description_${pkstatus.index+1}"
												value="${PACKS.description}" class="description width98">
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="optioncharges_${pkstatus.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatapacks"
												id="AddImagepacks_${pkstatus.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatapacks"
												id="EditImagepacks_${pkstatus.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowpacks"
												id="DeleteImagepacks_${pkstatus.index+1}"
												style="cursor: pointer;" title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImagepacks_${pkstatus.index+1}"
												style="display: none;" title="Working"> <span
													class="processing"></span> </a> <input type="hidden"
												name="salesDeliveryPackId"
												id="salesDeliveryPackId_${pkstatus.index+1}"
												value="${PACKS.salesDeliveryPackId}" />
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</c:if>
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div> 