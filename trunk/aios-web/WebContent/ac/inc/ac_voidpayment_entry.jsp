<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
 $(function(){
	 
	 $jquery("#void_payment_entry").validationEngine('attach'); 
     $('#void_discard').click(function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_all_voidpayment.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
	 });

     $('#payment-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	});
	
	$('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_allcheque_payments.action", 
		 	async: false,  
 		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);   
				 $('#common-popup').dialog('open'); 
					 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0); 
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});	 

	$('#void_save').click(function(){  
		$('.response-msg').hide();
		if($jquery("#void_payment_entry").validationEngine('validate')){
		var directPaymentId = Number($('#directPaymentId').val()); 
		var paymentId = Number($('#paymentId').val()); 
		var voidPaymentId =  Number($('#voidPaymentId').val()); 
		var description =$('#description').val(); 
		var referenceNumber = $('#referenceNumber').val(); 
		var voidDate = $('#voidDate').val();
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/save_voidpayment.action", 
	     	async: false,
	     	data:{
	     			directPaymentId : directPaymentId, paymentId: paymentId, description: description, voidPaymentId: voidPaymentId,
	     			referenceNumber: referenceNumber, voidDate: voidDate
	     		 },
			dataType: "json",
			cache: false,
			success: function(response){  
				if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
					$.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_all_voidpayment.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$(
																			'#common-popup')
																			.dialog(
																					'destroy');
																	$(
																			'#common-popup')
																			.remove();
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	if (voidPaymentId > 0)
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record updated")
																				.slideDown(
																						1000);
																	else
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						"Record created")
																				.slideDown(
																						1000);
																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																	$
																			.scrollTo(
																					0,
																					300);
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else {
								return false;
							}
						});
	if(Number($('#voidPaymentId').val())>0){
		$('#voidDate').datepick(); 
	} 	
	 else{
		 $('#voidDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	 }
 });
	function commonPaymentPopup(directPaymentId, paymentId, chequeBookNo,
			paymentType, paymentNumber, chequeNo, accountNumber, bankName,
			payeeName) {
		$('#paymentNo').val(paymentNumber);
		$('#chequeNo').val(chequeNo);
		$('#accountNumber').val(accountNumber);
		$('#bankAccount').val(bankName + "[" + accountNumber + "]");
		$('#payeeName').val(payeeName);
		$('#chequeBookNo').val(chequeBookNo);
		$('#directPaymentId').val(directPaymentId);
		$('#paymentId').val(paymentId);
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>void
			cheque
		</div> 
		<form name="void_payment_entry" id="void_payment_entry"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
				<input type="hidden" id="voidPaymentId" name="voidPaymentId"
					value="${VOID_PAYMENT.voidPaymentId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 130px;">
							<div>
								<label for="chequeBookNo" class="width30">Cheque Book No</label> <input
									type="text" name="chequeBookNo" id="chequeBookNo" tabindex="3"
									value="${VOID_PAYMENT.chequeBookNo}" class="width50"
									readonly="readonly" />
							</div>
							<div>
								<label for="payeeName" class="width30">Payee</label> <input type="text"
									name="payeeName" id="payeeName" tabindex="5" class="width50"
									readonly="readonly" value="${VOID_PAYMENT.payeeName}" />
							</div>
							<div>
								<label for="description" class="width30">Description</label>
								<textarea rows="2" class="width50" tabindex="6" id="description">${VOID_PAYMENT.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 130px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${VOID_PAYMENT.referenceNumber ne null && VOID_PAYMENT.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${VOID_PAYMENT.referenceNumber}" class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30">Void Date<span
									style="color: red;">*</span> </label>  
								 <input type="text" readonly="readonly" id="voidDate" name="voidDate"
									value="${VOID_PAYMENT.voidDateStr}" class="validate[required] width50" />
							</div>  
							<div>
								<label for="paymentNo" class="width30">Voucher Number<span
									style="color: red;">*</span> </label> <input type="text"
									readonly="readonly" name="paymentNo" id="paymentNo"
									tabindex="1" class="width50 validate[required]"
									value="${VOID_PAYMENT.paymentNumber}" />
								<c:if
									test="${VOID_PAYMENT.voidPaymentId eq null || VOID_PAYMENT.voidPaymentId le 0}">
									<span class="button"> <a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</c:if>
								<input type="hidden" readonly="readonly" name="paymentId"
									id="paymentId" value="${VOID_PAYMENT.paymentId}" /> <input
									type="hidden" readonly="readonly" name="directPaymentId"
									id="directPaymentId" value="${VOID_PAYMENT.directPaymentId}" />
							</div>
							<div>
								<label for="bankAccount" class="width30">Bank Account</label> <input type="text"
									name="bankAccount" id="bankAccount" readonly="readonly"
									value="${VOID_PAYMENT.bankAccount.accountNumber}" tabindex="2"
									class="width50" />
							</div> 
							<div>
								<label for="chequeNo" class="width30">Cheque No</label> <input type="text"
									name="chequeNo" id="chequeNo" tabindex="4" class="width50"
									value="${VOID_PAYMENT.chequeNo}" readonly="readonly" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="void_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="void_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>