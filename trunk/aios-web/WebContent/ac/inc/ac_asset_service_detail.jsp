<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript">
var currentId=""; var tempvar=""; var idarray=""; var rowid="";
$(function(){ 
	
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',25+"%");
	 $($($('#common-popup').parent()).get(0)).css('top',20+"%");
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	$("#tree_bank").treeview({
		collapsed: true,
		animated: "medium",
		control:"#sidetreecontrol",
		persist: "location"
	}); 

	$(".serviceDatepopup").click(function(){
		var rowId = $(this).attr('id').split("_")[1]; 
		$("#serviceDate").val($(this).html());
		$("#assetServiceDetailId").val($("#assetServiceDetailId_"+rowId).val());
		$("#assetName").val($("#assetName_"+rowId).val());
		$("#serviceNumber").val($("#serviceNumber_"+rowId).val());
		$('#common-popup').dialog('close'); 
	});
	
	$('.close').click(function(){
		 $('#common-popup').dialog('close'); 
	});
});
</script>
<div class="portlet-content">  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Asset Service Details
</div>
<ul id="tree_bank">
	<li><strong>Service Details</strong></li> 
	<c:choose>
		<c:when test="${requestScope.ASSET_SERVICES ne null && requestScope.ASSET_SERVICES ne''}">
			<c:forEach items="${ASSET_SERVICES}" var="assetService" varStatus="status1"> 
				<li id="service_${status1.index+1}">
					<span id="assetCreationName_${status1.index+1}" class="assetCreationName">${assetService.assetCreation.assetName}(${assetService.serviceNumber})</span>
					<ul> 
						<c:forEach items="${assetService.assetServiceDetails}" var="serviceDetail" varStatus="status"> 
							<input type="hidden" id="assetServiceDetailId_${status.index+1}" value="${serviceDetail.assetServiceDetailId}"/>
							<input type="hidden" id="assetName_${status.index+1}" value="${assetService.assetCreation.assetName}"/>
							<input type="hidden" id="serviceNumber_${status.index+1}" value="${assetService.serviceNumber}"/>
							<li style="cursor: pointer;"> 
								<c:set var="serviceDate" value="${serviceDetail.serviceDate}"/> 
								<% String serviceDate = pageContext.getAttribute("serviceDate").toString();
									serviceDate = DateFormat.convertDateToString(serviceDate).trim(); %>
								<span id="serviceDate_${status.index+1}" class="serviceDatepopup width30"><%=serviceDate%></span>
							</li> 
 						</c:forEach> 
					</ul>
				</li>	
			</c:forEach> 
		</c:when>
	</c:choose>		
</ul>  
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
	<div class="portlet-header ui-widget-header float-right close" style="cursor:pointer;">close</div>  
</div>  
</div>  
	