<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${CUSTOMER_INVOICE ne null && fn:length(CUSTOMER_INVOICE) > 0}">
	<fieldset>
		<legend>Active Customer Invoice</legend>
		<c:forEach var="invoice" items="${CUSTOMER_INVOICE}" varStatus="status">
			<div class="width20 float-left">
				<input type="checkbox" class="salesactiveInvoice float-left" style="top: 3px; position: relative;"
					 name="salesactiveInvoice_${invoice.salesInvoiceId}" id="salesactiveInvoice_${invoice.salesInvoiceId}"/>
				<label class="width90 float-right" for="salesactiveInvoice_${invoice.salesInvoiceId}">${invoice.referenceNumber}</label>
			</div>
		</c:forEach>
	</fieldset>
</c:if>