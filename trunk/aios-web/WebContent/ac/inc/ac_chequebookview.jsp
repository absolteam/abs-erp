<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
$(function() {
	$jquery("#cheequebook_entry_validate").validationEngine('attach'); 
	
	$('.chequebook-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getbankaccount_details.action", 
		 	async: false,  
		 	data : {showPage : "chequeBook"}, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);   
				 $($($('#common-popup').parent()).get(0)).css('top',0);
			} 
		});  
		return false;
	});
	
	$('#startingNo').live('change',function(){
		$('#notifyNo').val('');
		var startingNo = Number($('#startingNo').val());
		var endingNo = Number($('#endingNo').val());
		if(Number(endingNo - startingNo) > 0) {
 			$('#totalLeafs').val(Number(endingNo - startingNo) + 1);
			$('#notifyNo').val(Number($('#endingNo').val()) - 10);
		} else{
			$('#totalLeafs').val('');
		}
		return false;
	});
	
	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});	
	
	$('#endingNo').live('change',function(){
		$('#notifyNo').val('');
		var startingNo = Number($('#startingNo').val());
		var endingNo = Number($('#endingNo').val()); 
		if(Number(endingNo - startingNo) > 0) {
			$('#totalLeafs').val(Number(endingNo - startingNo) + 1);
			$('#notifyNo').val(Number($('#endingNo').val()) - 10);
		} else{
			$('#totalLeafs').val('');
		}
		return false;
	});

	$('#notifyNo').live('change',function(){
		var endingNo = Number($('#endingNo').val()); 
		var notifyNo = Number($('#notifyNo').val()); 
		var startingNo = Number($('#startingNo').val());
		if(Number(endingNo < notifyNo) || Number(startingNo > notifyNo)) {
			$('#notifyNo').val('');
		}  
		return false;
	});

	$('.chequeBookSave').click(function(){  
		$('.response-msg').hide();
		if($jquery("#cheequebook_entry_validate").validationEngine('validate')){
	 		var startingNo = $('#startingNo').val();
			var endingNo = $('#endingNo').val();
			var notifyNo = Number($('#notifyNo').val());
			var description =$('#description').val();
			var bankAccountId = $('#accountId').val();
			var chequeNo = $('#chequeNo').val();
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_chequebook.action", 
		     	async: false,
		     	data:{
			     		chequeBookId: chequeBookId, startingNo : startingNo, endingNo: endingNo, notifyNo: notifyNo,
		     		  	bankAccountId: bankAccountId, chequeNo: chequeNo, description: description
		     		 },
				dataType: "html",
				cache: false,
				success: function(result){ 
					$(".tempresult").html(result);  
					var message=$('.tempresult').html().toLowerCase().trim(); 
					if(message!=null && message=="success"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_allchequebook.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){ 
								$('#common-popup').dialog('destroy');		
								$('#common-popup').remove();   
								$("#main-wrapper").html(result);   
								if(chequeBookId == 0)
									$('#success_message').hide().html("Record created.").slideDown(1000); 
								else
									$('#success_message').hide().html("Record updated.").slideDown(1000); 
								$('#success_message').delay(2000).slideUp();
 							} 		
						}); 
					}
					else{
						$('#error_message').hide().html(message).slideDown(1000);
						$('#error_message').delay(2000).slideUp();
						return false;
					} 
				} 		
			});
		}else{
			return false;
		} 
	}); 

	$('.cancel_book').click(function(){  
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_allchequebook.action", 
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove(); 
				$("#main-wrapper").html(result);    
			} 		
		}); 
	});
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>cheque
			book
		</div> 
		<form name="cheequebook_entry" id="cheequebook_entry_validate"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="purchaseId" name="purchaseId"
					value="${PURCHASE_INFO.purchaseId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 110px;">
							<div>
								<label for="totalLeafs">Total Leaves</label> 
								 <c:choose>
								 	<c:when test="${CHEQUE_BOOK.bankAccount ne null && CHEQUE_BOOK.bankAccount ne ''}">
										<input type="text"
											readonly="readonly" name="totalLeafs" id="totalLeafs"
											tabindex="6" class="width50"
											value="${CHEQUE_BOOK.endingNo - CHEQUE_BOOK.startingNo +1}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="totalLeafs" id="totalLeafs" tabindex="6" class="width50"/>
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label for="notifyNo">Notify At</label> <input type="text"
									name="notifyNo" id="notifyNo" tabindex="7" class="width50"
									value="${CHEQUE_BOOK.notifyNo}" />
							</div>
							<div>
								<label for="description">Description</label>
								<textarea rows="2" class="width50" tabindex="8" id="description">${CHEQUE_BOOK.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 110px;">
							<div>
								<label for="bankAccount">Bank Account<span
									class="mandatory">*</span></label> 
								 <c:choose>
								 	<c:when test="${CHEQUE_BOOK.bankAccount ne null && CHEQUE_BOOK.bankAccount ne ''}">
								 		<input type="text"  readonly="readonly" name="accountNumber" id="accountNumber" class="width50 validate[required]"
										tabindex="1" value="${CHEQUE_BOOK.bankAccount.bank.bankName}(${CHEQUE_BOOK.bankAccount.accountNumber})"/> 
								 	</c:when>
								 	<c:otherwise>
								 		<input type="text"  readonly="readonly" name="accountNumber" id="accountNumber" tabindex="1"
								 		class="width50 validate[required]"/> 
								 		<span class="button"> <a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all chequebook-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								 	</c:otherwise>
								 </c:choose>
								<input type="hidden"
									readonly="readonly" name="accountId" id="accountId"
									value="${CHEQUE_BOOK.bankAccount.bankAccountId}" /> 
							</div>
							<div>
								<label for="chequeNo">Cheque Book No.<span
									class="mandatory">*</span></label> <input type="text"
									name="chequeNo" id="chequeNo" tabindex="2"
									class="width50 validate[required,custom[onlyNumber]]"
									value="${CHEQUE_BOOK.chequeBookNo}" />
							</div>
							<div>
								<label for="startingNo">Starting Leaf<span
									class="mandatory">*</span></label> <input type="text"
									name="startingNo" id="startingNo" tabindex="3"
									class="width50 validate[required,custom[onlyNumber]]"
									value="${CHEQUE_BOOK.startingNo}" />
							</div>
							<div>
								<label for="endingNo">Ending Leaf<span
									class="mandatory">*</span></label> <input type="text"
									name="endingNo" id="endingNo" tabindex="4"
									class="width50 validate[required,custom[onlyNumber]]"
									value="${CHEQUE_BOOK.endingNo}" />
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div> 
				
			</div>
		</form>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
	</div>
</div>