<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="hidden" name="productId"
		id="productid_${requestScope.rowId}" /> <span
		id="product_${requestScope.rowId}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all material_product_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" name="productQty"
		id="productQty_${requestScope.rowId}"
		class="productQty validate[optional,custom[number]] width80">
		<input type="hidden" name="totalAvailQty"
		id="totalAvailQty_${requestScope.rowId}">
	</td>
	<td style="display: none;"><input type="hidden" name="storeFromId"
		id="storeFromId_${requestScope.rowId}"
		value="${DETAIL.store.storeId}" />
	</td>
	<td><select name="transferType"
		id="transferType_${requestScope.rowId}" class="width80 transferType">
			<option value="">Select</option>
			<c:forEach var="transferType" items="${TRANSFER_TYPE}">
				<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;"
			id="MATERIAL_TRANSFER_TYPE_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all transfer-type-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td style="display: none;"><select name="transferReason"
		id="transferReason_${requestScope.rowId}"
		class="width80 transferReason">
			<option value="">Select</option>
			<c:forEach var="transferReason" items="${TRANSFER_REASON}">
				<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;"
			id="MATERIAL_TRANSFER_REASON_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all transfer-reason-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="hidden" name="shelId"
		id="shelfId_${requestScope.rowId}"
		value="${DETAIL.shelf.shelfId}" /> <span
		id="shelf_${requestScope.rowId}">${DETAIL.shelf.name}</span>
		<span class="button float-right"> <a style="cursor: pointer;"
			id="shelfID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all store-shelf-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td> 
	<td style="display: none;"><input type="text" name="amount"
		id="amount_${requestScope.rowId}" readonly="readonly"
		style="text-align: right;"
		class="amount width98 validate[optional,custom[number]] right-align">
	</td>
	<td style="display: none;"><span id="totalAmount_${requestScope.rowId}"
		style="float: right;"></span></td>
	<td><input type="text" name="linesDescription"
		id="linesDescription_${requestScope.rowId}" class="width98 linesDescription"
		maxlength="150"></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="materialTransferDetailId"
		id="materialTransferDetailId_${requestScope.rowId}" />
	</td>
</tr>