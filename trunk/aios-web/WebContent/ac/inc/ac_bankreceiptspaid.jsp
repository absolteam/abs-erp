<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
 <script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	$('#example').dataTable({ 
		"sAjaxSource": "show_paid_bank_json_receipts.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": 'BankReceipts', "bVisible": false},
			{ "sTitle": 'Receipts No'},
			{ "sTitle": 'Receipt Type'},  
			{ "sTitle": 'Currency'},  
			{ "sTitle": 'Date'},
			{ "sTitle": 'Ref.No'},
			{ "sTitle": 'Receiver'},
		], 
		"sScrollY": $("#main-content").height() - 235,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#example').dataTable();
	 
	/* Click event handler */
	$('#example tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this);
		bankReceiptsPaidPopup(aData[0], aData[1]);
		$('#bankreceipts-common-popup').trigger('click');
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>bank receipts</div>	 	 
		<div class="portlet-content"> 
 	   	    <div id="rightclickarea">
			 	<div id="bank_receipts">
					<table class="display" id="example"></table>
				</div> 
			</div>	 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
 				<div class="portlet-header ui-widget-header float-right" id="bankreceipts-common-popup">close</div>	 
			</div>
		</div>
	</div>
</div>