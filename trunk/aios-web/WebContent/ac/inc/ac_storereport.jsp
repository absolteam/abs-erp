<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Store Detail</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
.block-seperator{
	width:98%;
	float:left;
	border: 1px solid;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>STORE DETAILS</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<c:forEach var="STORE_INFO" items="${STORE_DETAILS}">
			<div class="width40 float_right">
				<div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Storage Type<span class="float_right">:</span> </span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
						 ${STORE_INFO.lookupDetailsByStorageType.displayName}
					</span>
				</div>
				<div class="width98 div_text_info">
					<span class="float_left left_align width28 text-bold">Store Manager<span class="float_right">:</span></span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
						${STORE_INFO.person.firstName} ${STORE_INFO.person.lastName}
					</span>  		
				</div> 
			</div>  
			<div class="width50 float_left">
				<div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Store Reference<span class="float_right">:</span></span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
						 ${STORE_INFO.storeNumber}
					</span> 
				</div>
				<div class="width100 div_text_info">
					<span class="float_left left_align width28 text-bold">Store Name <span class="float_right">:</span></span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
						${STORE_INFO.storeName}
					</span>
				</div>
			</div> 
		</c:forEach>  
	</div>
	<div class="clear-fix"></div>  
	<div class="block-seperator"></div>  
	</body>
</html>