<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#AssetCheckOutCheckInPopup').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_json_asset_checkout_checkin.action",

		"aoColumns" : [ { 
			"mDataProp" : "assetNumber"
		 }, {
			"mDataProp" : "assetName"
		 }, {
			"mDataProp" : "checkOutNumber"
		 },{
			"mDataProp" : "outDue"
		 },{
			"mDataProp" : "outDate"
		 },{
			"mDataProp" : "checkOutTo"
		 } ] 
	}); 
	 
	/* Click event handler */
	$('#AssetCheckOutCheckInPopup tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		assetCheckOutDetail(aData);  
		$('#asset-checkout-close').trigger('click');
  	});

	$('#AssetCheckOutCheckInPopup tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) 
			  $(this).addClass('row_selected');
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
 	      }
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Asset Checkout</div>	 	 
		<div class="portlet-content"> 
  			 	<div id="asset_popup_list">
					<table id="AssetCheckOutCheckInPopup" class="display">
						<thead>
							<tr>
								<th>Asset Number</th> 
								<th>Name</th> 
								<th>CheckOut Number</th> 
								<th>Due</th> 
								<th>CheckOut Date</th> 
								<th>CheckOut To</th>  
							</tr>
						</thead> 
					</table> 
 			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="asset-checkout-close" >close</div> 
 			</div>
		</div>
	</div>
</div>