<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
}); 
</script>
<div id="main-content">
	<div id="fy_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.coa.label.chartofaccounts"/>
		</div> 
		<div class="portlet-content"> 
			<div class="page-error response-msg error ui-corner-all" style="display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;">
				<input type="hidden" name="accountId" id="accountId" value="${ACCOUNT_DETAILS.accountId}"/>  
				<div class="width60" id="hrm">
					<fieldset>
						<legend><fmt:message key="accounts.coa.label.segmentdetails"/></legend>
						<div class="divpadding">
							<label class="width30"><fmt:message key="accounts.coa.label.segmentname"/><span style="color: red;">*</span></label>
							 <span class="width60 spanfont">${ACCOUNT_DETAILS.segmentName}</span>
						</div>
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm"> 
				<fieldset>
						<legend><fmt:message key="accounts.coa.label.accountdetails"/><span style="color: red;">*</span></legend>
						<div id="hrm" class="hastable width100">
							<div id="lineerror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
							<div id="warning_message" class="response-msg notice ui-corner-all"  style="width:90%; display:none;"></div>
							<input type="hidden" name="childCount" id="childCount"/>  
							<table id="hastab" class="width100">	
								<thead>
									<tr> 
										<th><fmt:message key="accounts.coa.label.accountcode"/></th> 
										<th><fmt:message key="accounts.coa.label.description"/></th> 
										<th><fmt:message key="accounts.coa.label.accounttype"/></th> 
										<th>Sub Type</th> 
									</tr>
								</thead>
								<tbody class="tab">  
									<tr id="fieldrow_${i}" class="rowid">		  	
										<td id="code_${i}">${ACCOUNT_DETAILS.accountCode}</td>
										<td id="description_${i}">${ACCOUNT_DETAILS.accountDescription}</td>
										<td id="accounttype_${i}">${ACCOUNT_DETAILS.accountType}</td> 
										<td id="subTypeName_${i}">${ACCOUNT_DETAILS.lookupDisplayName}</td> 
										<td style="display:none;" id="accounttypeid_${i}">${ACCOUNT_DETAILS.accountTypeId}</td> 
										<td style="display:none;" id="accountid_${i}">${ACCOUNT_DETAILS.accountId}</td>  
										<td style="display:none;" id="lineId_${i}">${i}</td>  
									</tr> 		 									 
						 		</tbody>
							</table>
						</div>
					</fieldset>	 
				</div>
			</form>
		</div> 
	</div>		
</div>
<div class="clearfix"></div>  