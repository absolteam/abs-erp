<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>General Ledger</title>
<link href="${pageContext.request.contextPath}/css/print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
}

td {
	border: 1px solid #000;
	height: 28px;
}

.bottomTD {
	border-bottom: 3px double #000;
}

.heading {
	padding: 10px;
	font-size: 20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica,
		sans-serif;
	text-align: center;
	color: #000;
	font-weight: bold;
	width: 100%;
	float: right;
}
.align-left{text-align: left!important;}
.lstln td{  
	border-top: 3px double #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px double #000; 
 }
</style>
<body onload="window.print();"
	style="margin-top: 15px; font-size: 12px;">
	<div class="width100">
		<span class="title-heading"
			style="font-family: book; font-style: italic; text-transform: inherit !important; 
				font-size: 28px !important; color: black;"><strong>${THIS.companyName}</strong>
		</span>
	</div>

	<div style="clear: both;"></div>
	<div class="width98">
		<span class="heading" style="font-family: book;"><span><u>GENERAL
					LEDGER<br/><c:if test="${LEDGER_ACCOUNT.fromDate ne null && LEDGER_ACCOUNT.fromDate ne ''}">
			 FROM ${LEDGER_ACCOUNT.fromDate}  
		</c:if>  
		<c:if test="${LEDGER_ACCOUNT.toDate ne null && LEDGER_ACCOUNT.toDate ne ''}">
			 TO ${LEDGER_ACCOUNT.toDate}  
		</c:if> </u> </span> </span>
	</div>
	<div style="clear: both;"></div>
	<div class="clear-fix"></div>
	<div
		style="position: relative; top: 1px; height: 100%; border-style: none; border-width: 1px; 
			-moz-border-radius: 0px; -webkit-border-radius: 0px; border-radius: 0px;"
		class="width100 float_left">
		<table style="width: 99%; margin: 0 auto;"> 
			<tr style="background-color: #f5ecce;">
				<td colspan="10" class="align-left" style="font-weight: bold;">${LEDGER_ACCOUNT.accountDescription}</td>
			</tr>
			<tr>
				<th style="width:1%;">REFERENCE</th> 
				<th style="width:1%;">DATE</th> 
				<th style="width:3%;">COA CODE</th>
				<th style="width:10%;">COA DESCRIPTION</th>
				<th style="width:3%;">TRANSACTION TYPE</th>
				<th style="width:2%;">TRANSACTION ID</th>
				<th style="width:10%;">DESCRIPTION</th>
				<th style="width:3%;">DEBIT</th>
				<th style="width:3%;">CREDIT</th>
				<th style="width:3%;">BALANCE</th>
			</tr> 
			<c:forEach items="${LEDGER_ACCOUNT.transactionVOs}"
				var="head"> 
				<c:forEach items="${head.transactionDetailVOs}"
					var="detail">
					<tr>
						<td class="align-left">${detail.journalNumber}</td> 
						<td class="align-left">${detail.transactionDate}</td> 
						<td class="align-left">${detail.accountCode}</td> 
						<td class="align-left">${detail.accountDescription}</td> 
						<td class="align-left">${detail.transactionRef}</td>
						<td class="align-left">${detail.transactionReference}</td>
						<td class="align-left">${detail.description}</td>
						<td>${detail.debitAmount}</td>
						<td>${detail.creditAmount}</td> 
						<td>${detail.closingBalance}</td> 
					</tr>
				</c:forEach> 
			</c:forEach>  
			<tr  class="lstln">
				<td colspan="7">TOTAL</td>
				<td>${LEDGER_ACCOUNT.debitAmount}</td>
				<td>${LEDGER_ACCOUNT.creditAmount}</td> 
				<td>${LEDGER_ACCOUNT.closingBalance}</td> 
			</tr>
		</table>
	</div>
	<div class="clear-fix"></div>
</body>
</html>