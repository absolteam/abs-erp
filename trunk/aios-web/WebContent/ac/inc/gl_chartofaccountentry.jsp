<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.opn_td{
	width:6%;
}
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var addedCOA = 0;
var accessCode=null;

$(function(){
	if($('.save').attr('id')=="edit"){
		$('.addrows').remove();
	} 
	if($('.save').attr('id')=="add-customise"){
		$('.skip').show(); 
		if($('.save').attr('id')=="add-template"){
			$('.skips').removeClass('float-left').addClass('float-right');
			$('.discard').remove();
		}
	}else if($('.save').attr('id')=="add-template"){
		$('.skip').remove();
		$('.discard').removeAttr('id');
		$('.discard').removeClass('discard').addClass('skip').html("Skip");
	}
	else{
		$('.skip').remove();
	}

	$jquery("#coavalidate-form").validationEngine('attach');
 
	 
	$('#segmentId').val($('#tempSegmentId').val());
	var tempvar=$('.tab>tr:last').attr('id'); 
	var idval=tempvar.split("_");
	var rowid=Number(idval[1]); 
	$('#DeleteImage_'+rowid).hide(); 

	 
	if($('.save').attr('id')=='edit'){
		$('#segmentId').attr('disabled',true);
	}else{manupulateLastRow();}

	 
	 
	$('.addData').click(function(){
		if($jquery("#coavalidate-form").validationEngine('validate')){  
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addadd";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 var segmentId = Number($('#segmentId').val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/chart_of_account_addentry.action",
			data:{id:rowid, showPage:showPage, segmentId: segmentId},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#accountCode').focus();  
			}
		 });
		}
		else{
			return false;
		}
		return false;		
	});

	$('.editData').click(function(){
		if($jquery("#coavalidate-form").validationEngine('validate')){  
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var showPage="addedit";
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).hide();
			 $("#WorkingImage_"+rowid).show(); 
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/chart_of_account_addentry.action",
				data:{id:rowid,showPage:showPage},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
				 	$('#accountCode').val($('#code_'+rowid).text());
				 	$('#accountDescription').val($('#description_'+rowid).text()); 
				 	$('#accountTypeId').val($('#accounttypeid_'+rowid).text());
				 	$('#subType').val($('#subTypeId_'+rowid).val());
				 	if($('#segmentId').val()==3){
				 		$('#accountTypeId').addClass('validate[required]');
				 	}
				 	else{
				 		$('#accountTypeId').removeClass('validate[required]');
				 	}
				 	$('#accountCode').focus();
				}
			 });
			}
			else{
				return false;
			}	
		return false;	
	 });

	 $(".delrow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0); 

		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var code=$('#code_'+rowid).text().trim();
		 if(code!=null && code!=""){
			 var flag=false; 
			 $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/chart_of_account_deletelineentry.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result); 
						 if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null ||$('#returnMsg').html()=="Success")
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#lineerror").hide().html($('#returnMsg').html().slideDown());
						$("#lineerror").delay(3000).slideUp(); 
						$('.tempresult').html(result);   
						return false;
					}
						
				 });
				 if(flag==true){ 
		        		 var childCount=Number($('#childCount').val());
		        		 if(childCount > 0){
							childCount=childCount-1;
							$('#childCount').val(childCount); 
		        		 } 
		        		 $(this).parent().parent('tr').remove(); 
		        		//To reset sequence 
		        		var i=0;
		     			$('.rowid').each(function(){  
							i=i+1;
							$($(this).children().get(5)).text(i); 
	   					 });  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
		 return false;
	 });

	 $('.editeditData').click(function(){
		 if($jquery("#coavalidate-form").validationEngine('validate')){  
			 $('.error').hide(); 
			 slidetab=$(this).parent().parent().get(0);   
			 var tempvar=$(slidetab).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]);  
			 var showPage="editedit";
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).hide();
			 $("#WorkingImage_"+rowid).show();  
			 var segmentId = Number($('#segmentId').val());
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/chart_of_account_addentry.action",
				data:{id:rowid, showPage:showPage, segmentId: segmentId},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
				 	$(result).insertAfter(slidetab); 
					$('#accountCode').val($('#code_'+rowid).text()); 
					$('#accountCode').attr('disabled',true);
				 	$('#accountDescription').val($('#description_'+rowid).text()); 
				 	$('#accountTypeId').val($('#accounttypeid_'+rowid).text());
				 	$('#subType').val($('#subTypeId_'+rowid).val());
				}
			 });  
		 }
		 else{
			 return false;
		 }
		 return false;
	 });

	 $('.save').click(function(){ 
		var id=$('.save').attr('id'); 
		var rowid=1;
		 var url_action="";
		 var accountId=0;
		 var accountCode="";
		 var accountDescription="";
		 var accountTypeId=0;
		 var segmentId=0;
		 var recordMessage = "Record created.";
		 if(id=="add" || id=="add-customise" || id=="add-template"){
			url_action="chart_of_account_save";
		 }
		 else{
			url_action="chart_of_account_update";
			accountCode=$('#code_'+rowid).text();
			accountDescription=$('#description_'+rowid).text(); 
			accountTypeId=$('#accounttypeid_'+rowid).text();
			segmentId=$('#segmentId').val();
			accountId=$('#accountid_'+rowid).text(); 
			recordMessage = "Record updated.";
		}
		 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+url_action+".action", 
			 	async: false,
			 	data:{	accountCode:accountCode,accountDescription:accountDescription,
				 		accountTypeId:accountTypeId,segmentId:segmentId,accountId:accountId
				 	 },
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $(".tempresultfinal").html(result);
					 var message=$('#returnMessage').html(); 
					  
					 if(message.trim()=="Success" && (id=="add" || id=="edit")){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/chart_of_accounts_retrieve.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(recordMessage).slideDown(1000);
								}
						 });
					 }
					 else if(message.trim()=="Success" && (id=="add-customise" || id=="add-template")){ 
						 var showPage="";
						 if(id=="add-customise")
							 showPage="add-customise";
						 else
							 showPage="add-template";
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/customise_combination.action", 
					     	async: false,
					     	data:{showPage:showPage},
							dataType: "html",
							cache: false,
							success: function(result){  
								$("#DOMWindow").html(result);  
							} 		
						});  
					 } 
					 else{
						 $('.page-error').hide().html("Please enter Account details").slideDown(1000);
						 return false;
					 }
				}
			 });
	 });

	 $('.discard').click(function(){
		 var id=$('.save').attr('id');  
		 if(id=="add" || id=="edit"){
			url_action="chart_of_account_discard";
		 }
		 else{ 
			url_action="show_setup_wizard"; 
		 }
		 $.ajax({
				type:"POST", 
				url:"<%=request.getContextPath()%>/"+url_action+".action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);
				}
			});
			return false;
	 });
	 
	 $('.skip').click(function(){ 
		var showPage="";
		if($('.save').attr('id')=="add-customise")
			showPage="add-customise";
		else
			showPage="add-template"; 
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/customise_combination.action", 
	     	async: false,
	     	data:{showPage:showPage},
			dataType: "html",
			cache: false,
			success: function(result){  
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#DOMWindow").html(result); 
				$.scrollTo(0,300);
			} 		
		});  
	 });
	 
	 $(document).keyup(function(e) { 
		 var id=$('.save').attr('id'); 
		  if (e.keyCode == 27 && id=="add-customise"){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#common-popup').dialog('destroy');		
						$('#common-popup').remove(); 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						 $('#common-popup').dialog('destroy');		
						 $('#common-popup').remove(); 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove(); 
						
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	}); 

	//add rows manipulations
		$('.addrows').click(function(){ 
			var id=Number(1);
			var i=Number(0);
			$('.rowid').each(function(){
				id=id+1; 
			});  
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/chart_of_account_addrow.action", 
			 	async: false,
			 	data:{id:id},
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('.tab tr:last').before(result);
					//if($(".tab").height()<255)
						// $(".tab").animate({height:'+=20',maxHeight:'255'},0);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
					i=0;
					 $('.rowid').each(function(){  
							i=i+1;
							var rowid=Number($(this).attr('id').split('_')[1]);
	 						$('#lineId_'+rowid).text(i);
	 					});  
				}
			});
		});
	
		 $('.subtype-lookup').live('click',function(){ 
	         $('.ui-dialog-titlebar').remove(); 
	         $('#common-popup').dialog('open');
	         accessCode=$(this).attr("id"); 
	            $.ajax({
	               type:"POST",
	               url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	               data:{accessCode:accessCode},
	               async: false,
	               dataType: "html",
	               cache: false,
	               success:function(result){
	                    $('.common-result').html(result);
	                    return false;
	               },
	               error:function(result){
	                    $('.common-result').html(result);
	               }
	           });
	            return false;
	   	}); 
	   
		$('#common-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			height:550,
			bgiframe: false,
			modal: true 
		});

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){ 
	 		
			if(accessCode=="ACCOUNT_SUB_TYPE"){
				$('#subType').html("");
				$('#subType').append("<option value=''>--Select--</option>");
				loadLookupList("subType");
				
			}
			return false;
		});	
});
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action", 
			data:{accessCode:accessCode},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){
				
				$(response.lookupDetails)
						.each(
								function(index) {
									$('#'+id)
											.append(
													'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
															+ response.lookupDetails[index].displayName
															+ '</option>');
					});
			}
		});
}
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:20px;\"></td>");
	}
}
function triggerAddRow(rowId) { 
	var nexttab = $('#fieldrow_' + rowId).next();
 	if ($(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
 	}
}
</script>

<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
	<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.coa.label.chartofaccounts"/>
	</div> 
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div class="portlet-content">
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div style="display:none;" class="tempresultfinal">
				<c:if test="${requestScope.bean != null}">
					<input type="hidden" id="sqlReturnStatus" value="${bean.sqlReturnStatus}" />
					 <c:choose>
						 <c:when test="${bean.sqlReturnStatus == 1}">
							<div class="response-msg success ui-corner-all">${bean.sqlReturnMessage}</div>
						</c:when>
						<c:when test="${bean.sqlReturnStatus != 1}">
							<div class="response-msg error ui-corner-all">${bean.sqlReturnMessage}</div>
						</c:when>
					</c:choose>
				</c:if>    
			</div>	 	 
			<div class="page-error response-msg error ui-corner-all" style="display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;">
				<input type="hidden" name="accountId" id="accountId" value="${ACCOUNT_DETAILS.accountId}"/>  
				<div class="width60" id="hrm">
					<fieldset>
						<legend><fmt:message key="accounts.coa.label.segmentdetails"/></legend>
						<div><label class="width30"><fmt:message key="accounts.coa.label.segmentname"/><span style="color: red;">*</span></label>
							<select class="width40 validate[required]" name="segmentId" id="segmentId">
								<option value="">Select</option> 
 									<c:forEach items="${SEGMENT_LIST}" var="segment" >
										<option value="${segment.segmentId}" style="font-weight: bold;">${segment.segmentName}</option>
										<c:if test="${segment.segmentVOs ne null && fn:length(segment.segmentVOs)>0}">
											<c:forEach items="${segment.segmentVOs}" var="child" >
												<option value="${child.segmentId}" style="margin-left: 8px;">${child.segmentName}</option>
											</c:forEach>
										</c:if>
									</c:forEach>
 							</select>  
						</div>
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm"> 
				<fieldset>
						<legend><fmt:message key="accounts.coa.label.accountdetails"/><span style="color: red;">*</span></legend>
						<div id="hrm" class="hastable width100">
							<div id="lineerror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
							<div id="warning_message" class="response-msg notice ui-corner-all"  style="width:90%; display:none;"></div>
							<input type="hidden" name="childCount" id="childCount"/>  
							<table id="hastab" class="width100">	
								<thead>
									<tr> 
										<th><fmt:message key="accounts.coa.label.accountcode"/></th> 
										<th><fmt:message key="accounts.coa.label.description"/></th> 
										<th><fmt:message key="accounts.coa.label.accounttype"/></th> 
										<th>Sub Type</th>
										<th style="width:10%;"><fmt:message key="accounts.common.label.options"/></th>
									</tr>
								</thead>
								<tbody class="tab"> 
									<c:set var="i" value="1"/>
									<c:choose>
										<c:when test="${ACCOUNT_DETAILS ne null}">
											<input type="hidden" id="tempSegmentId" value="${ACCOUNT_DETAILS.segmentId}"/>
											<tr id="fieldrow_${i}" class="rowid">		  	
												<td id="code_${i}">${ACCOUNT_DETAILS.accountCode}</td>
												<td id="description_${i}">${ACCOUNT_DETAILS.accountDescription}</td>
												<td id="accounttype_${i}">${ACCOUNT_DETAILS.accountType}</td> 
												<td id="subTypeName_${i}">${ACCOUNT_DETAILS.lookupDisplayName}</td> 
												<td style="display:none;" id="accounttypeid_${i}">${ACCOUNT_DETAILS.accountTypeId}</td> 
												<td style="display:none;" id="accountid_${i}">${ACCOUNT_DETAILS.accountId}</td>  
												<td style="display:none;" id="lineId_${i}">${i}</td> 
												<td>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" style="cursor:pointer;display:none;" id="AddImage_${i}" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editeditData"  id="EditImage_${i}" style="cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editdelrow"  id="DeleteImage_${i}" style="cursor:pointer;display:none;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
													  <input type="hidden" name="subTypeId" id="subTypeId_${i}" 
													  value="${ACCOUNT_DETAILS.lookupDetailId}">
												</td>  
											</tr>
										</c:when>
										<c:otherwise>
											<c:forEach var="i" begin="1" end="2" step="1" varStatus ="status"> 
												<tr id="fieldrow_${i}" class="rowid"> 	
													<td id="code_${i}"></td>
													<td id="description_${i}"></td>
													<td id="accounttype_${i}"></td> 
													<td id="subTypeName_${i}"></td>
													<td style="display:none;" id="accounttypeid_${i}"></td>
													<td style="display:none;" id="accountid_${i}"></td>  
													<td style="display:none;" id="lineId_${i}">${i}</td>
													<td id="option_${i}">
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="cursor:pointer;" title="Add Record">
								 								<span class="ui-icon ui-icon-plus"></span>
														  </a>	
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
																<span class="ui-icon ui-icon-wrench"></span>
														  </a> 
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none; cursor:pointer;" title="Delete Record">
																<span class="ui-icon ui-icon-circle-close"></span>
														  </a>
														  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
																<span class="processing"></span>
														  </a>
														  <input type="hidden" name="subTypeId" id="subTypeId_${i}" 
													 	 value="">
													</td>  
												</tr>
											</c:forEach>	
										</c:otherwise>
									</c:choose> 						 									 
						 		</tbody>
							</table>
						</div>
					</fieldset>	
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="discard"><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save" id="${requestScope.showPage}"><fmt:message key="accounts.common.button.save"/></div>
					</div> 
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
					</div>
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
						<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
					</div>
				</div>
			</form>
		</div>	
			<div
					style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
					class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
					tabindex="-1" role="dialog"
					aria-labelledby="ui-dialog-title-dialog">
					<div
						class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
						unselectable="on" style="-moz-user-select: none;">
						<span class="ui-dialog-title" id="ui-dialog-title-dialog"
							unselectable="on" style="-moz-user-select: none;">Dialog
							Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
							role="button" unselectable="on" style="-moz-user-select: none;"><span
							class="ui-icon ui-icon-closethick" unselectable="on"
							style="-moz-user-select: none;">close</span> </a>
					</div>
					<div id="common-popup"
						class="ui-dialog-content ui-widget-content"
						style="height: auto; min-height: 48px; width: auto;">
						<div class="common-result width100"></div>
						<div
							class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
							<button type="button" class="ui-state-default ui-corner-all">Ok</button>
							<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
						</div>
					</div>
				</div>
		
	</div>		
</div>