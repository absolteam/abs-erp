 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%>   
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			payment term details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="receivelist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${PAYMENT_TERM_INFO}" var="bean" varStatus="status">  
	     		<option value="${bean.creditTermId}|${bean.discountDays}|${bean.discount}">${bean.name}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 payment_term_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width30 float-left"><span>Term Name</span></li>  
   				<li class="width20 float-left"><span>Discount Days</span></li>   
   				<li class="width20 float-left"><span>Discount</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${PAYMENT_TERM_INFO}" var="bean" varStatus="status">
		     		<li id="paymenttermlist_${status.index+1}" class="payment_term_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.creditTermId}" id="paymenttermidbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.name}" id="paymenttermnamebyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.discountDays}" id="discountdaysid_${status.index+1}">  
		     			<input type="hidden" value="${bean.discount}" id="discountid_${status.index+1}"> 
		     			<span id="pname_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.name}</span>
		     			<span id="pddays_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.discountDays}</span> 
		     			<span id="pdiscount_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.discount}
		     				<span id="termNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript"> 
 var currentId="";
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',500);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',0);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var termdetails=$(this).val();
		       var termarray=termdetails.split("|");
		       $('#paymentTermId').val(termarray[0]);  
		       $('#paymenttermname').val($(".autoComplete option:selected").text()); 
		       $('#discountDays').val(termarray[1]); 
		       $('#discount').val(termarray[2]);   
		       $('#common-popup').dialog("close");  
	   } 
	}); 
	 
	 $('.payment_term_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.payment_term_list_li').each(function(){   
				 tempvar=$(this).attr('id'); 
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#termNameselect_'+rowid).hasClass('selected')){ 
					 $('#termNameselect_'+rowid).removeClass('selected');
				 }
			});  
			tempvar=$(this).attr('id');   
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);
			$('#termNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.payment_term_list_li').live('dblclick',function(){ 
			var tempvar="";var idarray = ""; var rowid="";
			 currentId=$(this);  
			 tempvar=$(currentId).attr('id');   
			 idarray = tempvar.split('_'); 
			 rowid=Number(idarray[1]);  
			 $('#paymentTermId').val($('#paymenttermidbyid_'+rowid).val());
			 $('#paymenttermname').val($('#paymenttermnamebyid_'+rowid).val());  
	         $('#discountDays').val($("#discountdaysid_"+rowid).val());
	         $('#discount').val($("#discountid_"+rowid).val());  
			 $('#common-popup').dialog('close');  
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 });
  
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.payment_term_list ul li{
		padding:3px;
	}
	.payment_term_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>