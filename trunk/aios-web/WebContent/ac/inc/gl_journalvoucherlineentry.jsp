<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tr>
	<td colspan="8" class="tdidentity" id="childRowId_${rowId}">
		<div class="width48 float-right" id="hrm">
			<fieldset>					 
				<legend><fmt:message key="accounts.journal.label.journaldetails"/></legend>
					<div>
						<label>Amount<span style="color: red;">*</span></label>
						<input type="text" name="linesAmount" id="linesAmount" style="text-align:right; " class="width40 right-align validate[optional,custom[onlyFloat]]">
					</div>
					<div>
						<label><fmt:message key="accounts.journal.label.desc"/></label>
						<input type="text" name="linesDescription" id="linesDescription" class="width40" maxlength="150">
					</div>
			</fieldset>  
			<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>    
		 </div>			 
		 <div class="width50 float-left" id="hrm">
				<fieldset>
					<legend><fmt:message key="accounts.journal.label.journaldetails"/></legend> 
					<div><label><fmt:message key="accounts.jv.label.jvlinno"/></label><input type="text" name="lineNumber" id="lineNumber" class="width20 validate[required]" readonly="readonly"></div>
					<div><label><fmt:message key="accounts.jv.label.jvcc"/><span style="color: red;">*</span></label>
						 <input type="hidden" name="combinationId" id="combinationId"/> 
						<input type="text" name="codeCombination" readonly="readonly" id="codeCombination" class="width40 validate[required] ">
						<span class="button">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup width100"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
					</div>
					<div>
						<label>Transaction<span style="color: red;">*</span></label>
						 <select name="isDebit" id="isDebit" class="width40 validate[required]">
						 	<option value="">Select</option>
						 	<option value="D">Debit</option>
						 	<option value="C">Credit</option>
						 </select>
					</div> 
				</fieldset> 
		 </div>  
 		 <input type="hidden" name="codeCombinationdesc" id="codeCombinationdesc"/> 
		 <div class="clearfix"></div> 
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"> 
		 	<input type="hidden" id="showPage" value="${showPage}"/>
			<div class="portlet-header ui-widget-header float-right rowcancel" id="cancel_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right rowsave" id="rowsave_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>
		 <div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 </div>
	</td>  
</tr>
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0);
function callIsDebit(transaction){
	if(transaction!=null && transaction!=""){
		if(transaction=="D" && transaction!="C")
			return 1;
		else
			return 0;
	}
	else{
		return false;
	}
}
$(function(){  
	$('.rowcancel').click(function(){ 
		$('.formError').remove();	
		//Destroy popup
	 	$('#codecombination-popup').dialog('destroy');		
  		$('#codecombination-popup').remove(); 
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if(showPage=="editedit"){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			if(Number($('#journalid_'+rowid).val())>0){
				$("#DeleteImage_"+rowid).hide();
			}else
				$("#DeleteImage_"+rowid).show();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 } 
		i=0;
		$('.rowid').each(function(){
			i=i+1;
		}); 
		$('#DeleteImage_'+i).hide(); 
	});

	$('.rowsave').click(function(){
		 //Find the Id for fetch the value from Id
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	
		 var combinationId=$('#combinationId').val();
		 var transaction=$('#isDebit').val(); 
		 var isDebit=callIsDebit(transaction);
		 var linesAmount=$('#linesAmount').val();
		 var linesDescription=$('#linesDescription').val();
		 var showPage=$('#showPage').val(); 
		 var url_action="";
		 var lineId=$('#lineId_'+rowid).text();   
		 var jounralId=Number($('#journalId').val()); 
		 var jounralLineId=Number($('#journalid_'+rowid).val()); 
		 if(showPage=="editedit")
			 url_action="journalvoucher_editedit_linesave";
		 else if(showPage=="editadd")
			 url_action="journalvoucher_editadd_linesave";
		 else if(showPage=="addedit")
			 url_action="journalvoucher_addedit_linesave";
		 else if(showPage=="addadd")
			 url_action="journalvoucher_addadd_linesave";
		 else{
			 $('#othererror').hide().html("There is no Action mapped.").slideDown(1000);
			// return false;
		 }
		 if(isDebit>0)
			 transactionFlag=1;
		 else
			 transactionFlag=0; 
		 
		 if($("#addMasterJournalValidation").validationEngine({returnIsValid:true})){      
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false, 
				 	data:{	combinationId:combinationId, transactionFlag:transactionFlag, linesAmount:linesAmount, linesDecription:linesDescription, 
					 		id:rowid, showPage:showPage, jounralId:jounralId, journalLineId:jounralLineId
					 	 },
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tempresult').html(result); 
					     if(result!=null) {
                             flag = true;

                              addedVouchers = addedVouchers + 1;
 	   							if(addedVouchers == 2) {	
 	   							i=0;
	 	   						$('.rowid').each(function(){
	 	   							i=i+1;
	 	   						}); 
	 	   						$('#DeleteImage_'+i).show(); 
	 	   							$('.addrows').trigger('click');
	 	   							
	 	   							addedVouchers = 1;
 	   							}
					     } 
				 	},  
				 	error:function(result){
				 	  $('.tempresult').html(result);
                     $("#othererror").html($('#returnMsg').html()); 
                     return false;
				 	} 
	          });
			 if(flag==true){  
				 	//Destroy popup
					$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove(); 
		        	//Bussiness parameter 
	        		$('#codecombination_'+rowid).text($('#codeCombination').val()); 
	        		$('#transactionflag_'+rowid).text($('#isDebit :selected').text());  
	        		$('#amount_'+rowid).text(linesAmount);  
	        		$('#linedecription_'+rowid).text(linesDescription);  
	        		$('#combinationid_'+rowid).val(combinationId);
					//Button(action) hide & show 
	        		if(Number($('#journalid_'+rowid).val())>0){
	        			$("#AddImage_"+rowid).hide();
	        			$("#EditImage_"+rowid).show();
	        			$("#DeleteImage_"+rowid).hide();
	        			$("#WorkingImage_"+rowid).hide(); 
	        		 }
	        		 else{
	        			 $("#AddImage_"+rowid).hide();
	        			 $("#EditImage_"+rowid).show();
	        			 $("#DeleteImage_"+rowid).show();
	        			 $("#WorkingImage_"+rowid).hide(); 
	        		 }
	        		 
	        		$("#childRowId_"+rowid).remove();
					//Row count+1
					var childCount=Number($('#childCount').val());
					childCount=childCount+1;
					$('#childCount').val(childCount);

					var debitTotal=Number(0);
					var k=0;
					$('.rowid').each(function(){ 
						k=k+1;
						if($('#transactionflag_'+k).text().trim()=="Debit"){
							var tempdebitTotal=$('#amount_'+k).text().trim().split(',').join(''); 
							debitTotal=Number(debitTotal)+Number(tempdebitTotal);
						 } 
					}); 
					$('.debitTotal').text(Number(debitTotal).toFixed(2)); 
					var creditTotal=Number(0);
					k=0;
					$('.rowid').each(function(){ 
						k=k+1;
						if($('#transactionflag_'+k).text().trim()=="Credit"){
							var tempCreditTotal=$('#amount_'+k).text().trim().split(',').join(''); 
							creditTotal=Number(creditTotal)+Number(tempCreditTotal);
						 } 
					}); 
					$('.creditTotal').text(Number(creditTotal).toFixed(2)); 
	        	}
		 }
		 else{
			 return false;
		 } 
	});


	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	    tempid=$(this).parent().get(0); 
	    $('.ui-dialog-titlebar').remove();   
	 	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_treeview.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){   
				 $('.codecombination-result').html(result);  
			},
			error:function(result){ 
				 $('.codecombination-result').html(result); 
			}
		});  
	});
	
	 $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true
	 });
});	
function setCombination(combinationTreeId,combinationTree){
	$('#combinationId').val(combinationTreeId);
	$('#codeCombination').val(combinationTree);
} 
</script>