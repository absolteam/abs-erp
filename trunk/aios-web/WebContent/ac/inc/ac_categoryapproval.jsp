<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<script type="text/javascript">
$(function(){ 
	
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
	
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	});  
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>category</div> 
		  <form name="category_details" id="category_details" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height:50px;"> 
							<div class="divpadding">
								<label class="width30" for="fromDate">From Date</label> 
								<span
									class="width60 spanfont">${CATEGORY_INFO.start}</span> 
							</div> 
							<div class="divpadding">
								<label class="width30" for="toDate">To Date</label> 
								<span
									class="width60 spanfont">${CATEGORY_INFO.end}</span> 
							</div>  
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height:50px;"> 
							<div class="divpadding">
								<label class="width30" for="categoryName">Category Name</label> 
								<input type="hidden" id="categoryId" name="categoryId" value="${CATEGORY_INFO.categoryId}"/>
								<span
									class="width60 spanfont">${CATEGORY_INFO.name}</span> 
							</div> 
							<div class="divpadding">
								<label class="width30" for="noOfDays">Status</label> 
								<span
									class="width60 spanfont">${CATEGORY_INFO.statusStr}</span>  
							</div>  
						</fieldset>
					</div> 
			</div>  
		</div>    
		<div class="clearfix"></div>  
  </form>
  </div> 
</div>
<div class="clearfix"></div>  