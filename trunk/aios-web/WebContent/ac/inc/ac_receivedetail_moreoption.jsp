<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
</style>
<div id="returnMessageErr" style="display: none;">${requestScope.RETURN_MESSAGE}</div>
<div id="hrm" class="hastable width100 purchase_bacthreceive">
	<div id="receivedetail-moreerror"
					class="response-msg error ui-corner-all width90"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
	<input type="hidden" id="receiveLineId" value="${requestScope.rowId}" />
	<input type="hidden" id="purchasePOId" value="${requestScope.purchaseId}" />
	<fieldset>
		<legend>Batch Receive Details</legend>
		<table id="hastab-1" class="width100">
			<thead>
				<tr>
					<th>Product Code</th>
					<th>Product</th>
					<th style="display: none;">UOM</th>
					<th>PO.Qty</th>
					<th>Received Qty</th>
					<th>Non Received Qty</th>
					<th>Packaging</th>
					<th>Quantity</th>
					<th>Base Qty</th>
					<th>Unit Rate</th>
					<th>Batch NO</th>
					<th>Expiry</th>
					<th>Total</th>
					<th>Store</th>
					<th>Description</th> 
					<th><fmt:message key="accounts.common.label.options" /></th>
				</tr>
			</thead>
			<tbody class="tab-1">
				<c:forEach var="bean" varStatus="status"
					items="${RECEIVE_DETAILINFO}">
					<tr class="rowidpo" id="fieldrowpo_${status.index+1}">
						<td style="display: none;" id="lineIdpo_${status.index+1}">${status.index+1}</td>
						<td id="productcode_${status.index+1}">${bean.product.code}</td>
						<td id="productname_${status.index+1}">
							<span class="width60 float-left">${bean.product.productName}</span>
							<span class="width10 float-right">${bean.unitCode}</span>
						</td>
						<td style="display: none;" id="uom_${status.index+1}"></td>
						<td id="purchaseqty_${status.index+1}">${bean.purchaseQty}</td>
						<td><c:choose>
								<c:when
									test="${bean.receivedQty ne null &&  bean.receivedQty gt 0}">
									<span id="receivedqty_${status.index+1}">${bean.receivedQty}</span>
								</c:when>
								<c:otherwise>
									<span id="receivedqty_${status.index+1}" style="display: none;">${bean.receivedQty}</span>
										-N/A-
									</c:otherwise>
							</c:choose>
						</td>
						<td><c:choose>
								<c:when
									test="${bean.returnedQty ne null &&  bean.returnedQty gt 0}">
									<span id="returnedqty_${status.index+1}">${bean.returnedQty}</span>
								</c:when>
								<c:otherwise>
									<span id="returnedqty_${status.index+1}" style="display: none;">${bean.returnedQty}</span>
										-N/A-
									</c:otherwise>
							</c:choose>
						</td> 
						<td> 
							<select name="grnpackageType" id="grnpackageType_${status.index+1}" class="grnpackageType"> 
								<option value="-1">Select</option>
								<c:forEach var="packageType" items="${bean.productPackageVOs}">
									<c:choose>
										<c:when test="${packageType.productPackageId gt 0}">
											<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
											 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
											 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
											 	</c:forEach> 
											 </optgroup>
										</c:when> 
									</c:choose> 
								</c:forEach>
							</select>  
							<input type="hidden" name="tempgrnpackageType" id="tempgrnpackageType_${status.index+1}" value="${bean.packageDetailId}"/>
						</td>
						<td><input type="text"
							class="width90 grnPackageUnit validate[optional,custom[number]]"
							value="${bean.packageUnit}" name="grnPackageUnit"
							id="grnPackageUnit_${status.index+1}" /> 
						</td>
						<td><input type="hidden"
							class="width90 receiveQtybatch validate[optional,custom[number]]"
							value="${bean.quantity}" name="receiveQty"
							id="receiveQty_${status.index+1}" /> <input type="hidden"
							value="${bean.quantity}" name="receiveHiddenQty"
							id="receiveHiddenQty_${status.index+1}" />
							<span id="grnbaseUnitConversion_${status.index+1}" class="width15" style="display: none;">${bean.baseUnitName}</span>
							<span id="baseDisplayQty_${status.index+1}">${bean.baseQuantity}</span>
							<c:choose>
								<c:when test="${bean.baseQuantity ne null && bean.baseQuantity ne ''}">
									<input type="hidden" value="${bean.baseQuantity}" name="receiveMaxQty"
										id="receiveMaxQty_${status.index+1}" />
								</c:when>
								<c:otherwise>
									<input type="hidden" value="${bean.quantity}" name="receiveMaxQty"
										id="receiveMaxQty_${status.index+1}" />
								</c:otherwise>
							</c:choose> 
						</td>
						<td><input type="text" style="text-align: right;"
							class="width90 unitRate validate[optional,custom[number]]"
							value="${bean.unitRate}" name="unitRate"
							id="unitRate_${status.index+1}" />
							<input type="text" style="display: none;" class="totalAmount" id="totalAmount_${status.index+1}"
									value="${bean.quantity * bean.unitRate}" name="totalAmount"/>
						</td>
						<td><input type="text" class="width96 batchNumber"
							value="${bean.batchNumber}"
							id="batchNumber_${status.index+1}" maxlength="40" /></td>
						<td><input type="text" readonly="readonly"
							class="width96 expiryBatchDate" value="${bean.expiryDate}"
							id="expiryBatchDate_${status.index+1}" /></td>
						<td id="total_${status.index+1}" style="text-align: right;"></td>
						<td style="display: none"><input type="hidden" value="${bean.receiveDetailId}"
							name="receivelineid" id="receivelineid_${status.index+1}" /> <input
							type="hidden" name="purchasePOId"
							id="purchasePOId_${status.index+1}"
							value="${bean.purchaseId}" /> <input type="hidden"
							name="productid" id="productid_${status.index+1}"
							value="${bean.product.productId}" /> <input type="hidden"
							name="purchaseLineId" id="purchaseLineId_${status.index+1}"
							value="${bean.purchaseDetailId}" /> 
						</td>
						<td><input type="hidden" name="storeDetail"
							value="${bean.shelfId}" id="storeDetail_${status.index+1}" /> <span
							id="rackname_${status.index+1}">${bean.storeName}</span> <span
							class="button float-right"> <a style="cursor: pointer;"
								id="${status.index+1}"
								class="btn ui-state-default ui-corner-all store-popup width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
						<td class="returnDiv" style="display: none;"><span id="returnQty1_${status.index+1}">${bean.returnQty}</span>
							<input type="hidden" name="returnQty"
							id="returnQty_${status.index+1}" class="returnQty"
							value="${bean.returnQty}" />
						</td>
						<td><input type="text" name="linedescription"
							id="linedescription_${status.index+1}"
							value="${bean.description}" />
						</td>
						<td style="width: 0.01%;" class="opn_td"
							id="optionpo_${status.index+1}"><a
							class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_batchpo"
							id="DeleteCImage_${status.index+1}" style="cursor: pointer;"
							title="Delete Record"> <span
								class="ui-icon ui-icon-circle-close"></span> </a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</fieldset>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div
			class="portlet-header ui-widget-header float-left sessionReceiveBatchSave"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div
			class="portlet-header ui-widget-header float-left closeReceiveDom"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() { 
		$jquery(".unitRate,.totalAmount").number(true,3); 
		$('.expiryBatchDate').datepick(); 
		$('.rowidpo').each(function(){   
			var rowId = getRowId($(this).attr('id'));
  			$('#grnpackageType_'+rowId).val($('#tempgrnpackageType_'+rowId).val());
  			$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
		});
	});
</script>