<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <div class="selectable" id="hrm">
     <div class="float-left width100" style="background-color:#dfdfdf;padding:5px;">
     	<div class="width48 float-left"><span>Product Name</span></div>
     	<div class="width50 float-left"><span>UOM</span></div>
     </div>	
	<c:forEach items="${PRODUCT_INFO}" var="bean" varStatus="status">
	    <div class="selectlist float-left width100">   
	     	  <div class="width48 float-left" style="padding:1% 0  1% 0 ;"><span>${bean.productName}</span></div>
	     	  <div class="width50 float-left" style="padding:1% 0  1% 0 ;"><span>${bean.lookupDetailByProductUnit.displayName}</span></div>
	     	  <input type="hidden" name="accountId" value="${bean.productId}"/>
	     </div>
	</c:forEach>
 </div> 
<script type="text/javascript">
var temp="";
$(function(){
	   $(".ui-dialog-titlebar").remove();
	   $(".selectlist").click(function(){
          $(temp).css('background-color','#fff');
          $(this).css('background-color','#F39814');
          temp=this;
      });
	   $(".selectlist").mousedown(function(){
          $(this).css('background-color','#FECA40');
      });
	  $('.selectlist').click(function(){ 
		  $('#productName').val($($($(this).children().get(0)).children().get(0)).html()); 
		  $('#uom').val($($($(this).children().get(1)).children().get(0)).html()); 
		  $('#productId').val($($(this).children().get(2)).val()); 
  	  });  
}); 

</script>