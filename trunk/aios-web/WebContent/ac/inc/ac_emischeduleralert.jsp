<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@page import="java.text.DecimalFormat;" %>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<style>
.currentpayment{
	background: #EB7A8D;
}
.futurepayment{
	background: #AFFAD9;
}
.paid{
	background: #FFFF00;
}
.unpaid{
	background: #BF6D24;
}
</style>
<script type="text/javascript">
var emifrequency="";
var ratetype="";
var ratevariance=""; 
var emiPayments="";
var emihead=0;
$(function(){ 
	convertChar();
	colorTable();
	if(currentfield>1){
		checkUnPaid();
	} 

	$('.addpayement').click(function(){
		var rowId=getRowId($(this).attr('id')); 
		emiPayments="";
		var loanId=$('#loanId').val();
		var dueDate=$('#dueDate_'+rowId).val();
		emiPayments=getEmiPayments(rowId);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getemi_paymentview.action", 
		 	async: false,
		 	data:{loanId: loanId, dueDate:dueDate, attribute1:emiPayments},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$("#main-wrapper").html(result);  
			}
		});
	});

	var getEmiPayments =function(rowId){
		var amountArray=new Array();
		var paymentName=new Array(); 
		var loanAmount=0;
		var totalSize=$('#fieldrow_'+rowId+'>td').size();
		totalSize-=3; 
		for(var i=2;i<totalSize;i++){ 
			amountArray.push($($($($($('#fieldrow_'+rowId).children()).get(i)).children()).get(0)).val());
		} 
		$('.emihead').each(function(){
			paymentName.push($(this).text().trim($(this).text()));
		});
		for(var j=0;j<paymentName.length;j++){ 
			loanAmount=amountArray[j].replace(/,/gi,'');
			emiPayments+=paymentName[j]+"#@"+loanAmount;
			if(j==paymentName.length-1){   
			} 
			else{
				emiPayments+="##@";
			}
		} 
		return emiPayments;
	};

	// File upload
	 $('#emi_document_information').click(function(){ 
			AIOS_Uploader.openFileUploader("doc","emiDocs","Emi","-1","EmiDocuments"); 
	 });
		
	 $('#emi_image_information').click(function(){ 
		AIOS_Uploader.openFileUploader("img","emiImages","Emi","-1","EmiImages"); 
	});
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function trim(str) {
    return str.replace(/^\s+|\s+$/g,"");
}
function convertChar(){
	if($('#emiFrequency').val()=='M')
		 emifrequency="Monthly";
	 else if($('#emiFrequency').val()=='D')
		 emifrequency="Daily";
	 else if($('#emiFrequency').val()=='W')
		 emifrequency="Weekly";
	 else if($('#emiFrequency').val()=='Q')
		 emifrequency="Quaterly";
	 else if($('#emiFrequency').val()=='H')
		 emifrequency="SemiAnually";
	 else if($('#emiFrequency').val()=='Y')
		 emifrequency="Anually";
	 $('#emiFrequency').val(emifrequency);  

	 if($('#rateVariance').val()=='F')
		 ratetype="Flat";
	 else if($('#rateVariance').val()=='D')
		 ratetype="Diminishing";  
	 if($('#rateType').val()=='F')
		 ratevariance="Fixed";
	 else if($('#rateType').val()=='V')
		 ratevariance="Vary"; 
	 else if($('#rateType').val()=='M')
		 ratevariance="Mixed";
	 $('#rateVariance').val(ratetype);  
	 $('#rateType').val(ratevariance);
	 if($('#loanType').val()=='P')
    	 $('#loanType').val("Predictable");
     else
    	 $('#loanType').val("Unpredictable");
}
function colorTable(){
	$('.rowid').each(function(){
		var rowId=getRowId($(this).attr('id'));
		if($('#status_'+rowId).val()==1){
			$('#fieldrow_'+rowId).addClass('paid');
			$('#fieldrow_'+rowId+'>td>input').addClass('paid');
		}
		else if($('#status_'+rowId).val()==2){
			currentfield=getRowId($('#status_'+rowId).attr('id'));
			$('#fieldrow_'+rowId).addClass('currentpayment');
			$('#fieldrow_'+rowId+'>td>input').addClass('currentpayment'); 
			$('#fieldrow_'+rowId+':last').append("<td style=\"height:25px; width:2%!important;cursor:pointer;font-weight:bold;\" class=\"addpayement\" id=addpayement_"+rowId+">Pay</td>");
		}
		else{
			console.debug("inside");
			$('#fieldrow_'+rowId).addClass('futurepayment');
			$('#fieldrow_'+rowId+'>td>input').addClass('futurepayment');
		}
	}); 
}
function checkUnPaid(){
	for(var i=1;i<currentfield;i++){
		if($('#status_'+i).val()==0){
			$('#fieldrow_'+i).removeClass('paid').addClass('unpaid');
			$('#fieldrow_'+i+'>td>input').removeClass('paid').addClass('unpaid');
		}
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>emi details</div>	
		  <form name="loanentry_details" id="loanentry_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="loanId" name="loanId" value="${ALERT_LOAN_DETAILS.loanId}"/>
				<div class="width100 float-left" id="hrm"> 
					<div class="width60 float-left"> 
					<fieldset style="height: 142px;">
						<legend>EMI Detail</legend>
						<div class="width100 float-left">
							<div class="width48 float-right"> 
								<div>
									<label class="width30" for="rateType">Rate Type</label>
									<input type="text" readonly="readonly" name="rateType" id="rateType" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.rateType}"/>
								</div> 
								<div>
									<label class="width30" for="rateType">Rate Variance</label>
									<input type="text" readonly="readonly" name="rateVariance" id="rateVariance" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.rateVariance}"/>
								</div> 
								<div>
									<label class="width30" for="emiFrequency">EMI Frequency</label>
									<input type="text" readonly="readonly" name="emiFrequency" id="emiFrequency" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.emiFrequency}"/>
								</div> 
								<div>
									<label class="width30" for="emiDueDate">First Due</label>
									<c:set var="duedates" value="${ALERT_LOAN_DETAILS.emiFirstDuedate}"/>
									 <%
									 	String duedates=DateFormat.convertDateToString(pageContext.getAttribute("duedates").toString()); 
									 %>
									 <input type="text" readonly="readonly" name="emiFrequency" id="emiFrequency" class="width50 validate[required]" value="<%=duedates%>"/>
								</div>  
							</div>
							<div class="width50 float-left">
								<div>
									<label class="width30" for="loanNumber">Loan Number</label>
									<input type="text" readonly="readonly" name="loanNumber" id="loanNumber" tabindex="1" 
										class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.loanNumber}"/> 
								</div> 
								<div style="display:none;">
									<label class="width30" for="currency">Currency</label>
									<input type="text" readonly="readonly" name="currency" id="currency" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.currency.currencyPool.code}"/>
								</div> 
								<div>
									<label class="width30" for="loanType">Loan Type</label>
									<input type="text" readonly="readonly" name="loanType" id="loanType" tabindex="2" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.loanType}"/>
								</div>
								<div>
									<label class="width30" for="approvalAmount">Approval Amount</label>
									<c:set var="loanamount" value="${ALERT_LOAN_DETAILS.amount}"/>
						     		<%
						     			DecimalFormat decimalFormat=new DecimalFormat();
						     			String loanamount=decimalFormat.format(pageContext.getAttribute("loanamount"));
						     			loanamount = AIOSCommons.formatAmount(pageContext.getAttribute("loanamount"));
						     			pageContext.setAttribute("loanamount", loanamount); 
						     		%>
									<input type="text" readonly="readonly" name="approvalAmount" id="approvalAmount" tabindex="3" class="width50 validate[required]" value="${loanamount}"/>
								</div> 
								<div style="margin-top: 3px;">
									<label class="width30" for="accountNumber">Account Number</label>
									<input type="text" readonly="readonly" name="accountNumber" id="accountNumber" tabindex="4" class="width50 validate[required]" value="${ALERT_LOAN_DETAILS.bankAccount.accountNumber}"/>
								</div>  
							</div>
						</div>  
					</fieldset> 
					</div>
					<div class="width40 float-left">
		 				<fieldset style="height: 150px;">
						<legend>Uploads</legend> 
						<div id="hrm" Style="margin-bottom: 10px;">
							<div id="UploadDmsDiv" class="width100">
								
								<div style="height: 65px;">									
									
									<div id="emi_image_information" style="cursor: pointer; color: blue;">
										<u>Upload image here</u>
									</div>
									<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
										<span id="emiImages"></span>
									</div>
									
								</div>	
								<div style="height: 65px;">									
									
									<div id="emi_document_information" style="cursor: pointer; color: blue;">
										<u>Upload document here</u>
									</div>
									<div style="padding-top: 10px; margin-top:3px; height: 85px; overflow: auto;">
										<span id="emiDocs"></span>
									</div>
									
								</div>						
							</div>
							</div>
						</fieldset>	
					</div> 
				</div>   
				<div class="clearfix"></div> 
 				<div class="portlet-content" id="hrm">
 					<fieldset>
						<legend>EMI Schedule</legend>
						<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
						<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div> 
						<div id="hrm" class="hastable width100"  >  
							<input type="hidden" name="childCount" id="childCount" value=""/>  
							<table id="hastab" class="width100"> 
								<thead>
							   		<tr> 
										<th style="width:1%">Line No</th> 
								   	 	<th style="width:6%">Due Date</th>  
								   	 	<c:choose>
					   	 					<c:when test="${requestScope.EMI_DETAIL_PAYMENT ne null && requestScope.EMI_DETAIL_PAYMENT ne '' && fn:length(requestScope.EMI_DETAIL_PAYMENT)>0}">
					   	 						<c:forEach var="beans" items="${EMI_DETAIL_HEAD}" varStatus="statush">
					   	 							<th class="emihead" id="head_${statush.index+1}" style="width:5%">${beans.name}</th>
					   	 						</c:forEach>
					   	 					</c:when>
					   	 					<c:otherwise> 
					   	 						<c:set var="i" value="${1}"/>
												<th class="emihead" id="head_${i}" style="width:5%">Instalment</th>  
												<c:set var="i" value="${i+1}"/>
								   	 			<th class="emihead" id="head_${i}" style="width:5%">Interest</th>   
								   	 			<c:if test="${requestScope.ALERT_LOAN_DETAILS.eibor ne null && requestScope.ALERT_LOAN_DETAILS.eibor ne ''}">
								   	 				<c:set var="i" value="${i+1}"/>
								   	 				<th class="eiborhead emihead" id="head_${i}" style="width:5%">EIBOR</th> 
								   	 			</c:if>
								   	 			<c:choose>
										   	 		<c:when test="${requestScope.LOAN_CHARGES_DETAIL ne null && requestScope.LOAN_CHARGES_DETAIL ne ''}">
										   	 			<c:forEach var="cbean" items="${LOAN_CHARGES_DETAIL}">
										   	 				<c:set var="i" value="${i+1}"/>
										   	 				<th class="insurancehead emihead" id="head_${i}" style="width:5%">${cbean.name}</th>
										   	 			</c:forEach>
										   	 		</c:when>
										   	 	</c:choose>
										   	 	<c:set var="i" value="${i+1}"/>
								   	 			<th id="head_${i}" style="width:5%" class="emihead">Depreciation</th>
								   	 			<c:set var="i" value="${i+1}"/>
								   	 			<th id="head_${i}" style="width:5%" class="emihead">Cumulative</th>  
								   	 			<c:set var="i" value="${i+1}"/>  
								   	 			<th id="head_${i}" style="width:5%" class="emihead">Balance</th>   
											</c:otherwise>	 
					   	 			  </c:choose> 
					   	 			  <th style="width:5%">Description</th>	
						 		 </tr>
								</thead> 
								<tbody class="tab">  
									<c:choose>
										<c:when test="${requestScope.EMI_DETAIL_PAYMENT ne null && requestScope.EMI_DETAIL_PAYMENT ne '' && fn:length(requestScope.EMI_DETAIL_PAYMENT)>0}">
											<c:forEach var="emibean" items="${EMI_DETAIL_PAYMENT}" varStatus="estatus"> 
												<tr class="paid">
													<td id="lineId_${estatus.index+1}">${estatus.index+1}</td> 
													<td class="paid"> 
														<input type="text" name="emiDueDate" class="emiDueDate width95 paid" readonly="readonly" id="dueDate_${estatus.index+1}" 
															style="border:0px;" value="${emibean.emiDueDate}"/>
													</td> 
													<c:choose>
								   	 					<c:when test="${requestScope.EMI_DETAIL_HEAD ne null && requestScope.EMI_DETAIL_HEAD ne ''}">
								   	 						<c:forEach var="beans" items="${EMI_DETAIL_HEAD}">
								   	 							<td class="paid">
								   	 								<c:set var="paidAmount" value="${beans.amount}"/>
																	<%String paidAmount = AIOSCommons.formatAmount(pageContext.getAttribute("paidAmount"));
																					pageContext.setAttribute("paidAmount", paidAmount);%>
								   	 								<input type="text" value="${paidAmount}" class="width95 paid" readonly="readonly" style="border:0px;text-align: right;"/> 
								   	 							</td> 
								   	 						</c:forEach>
								   	 					</c:when>
								   	 				</c:choose>
								   	 				<td class="paid">
														<input type="text" readonly="readonly" name="description" class="description width95 paid" id="description_${estatus.index+1}" value="${emibean.description}" style="border:0px;"/>
													</td> 
												</tr> 
											</c:forEach>
										</c:when>
									</c:choose>	
									<c:choose>
										<c:when test="${requestScope.LOAN_SCHEDULE ne null && requestScope.LOAN_SCHEDULE ne '' && fn:length(requestScope.LOAN_SCHEDULE)>0}">
											<c:forEach var="bean" items="${LOAN_SCHEDULE}" varStatus ="status"> 
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}">${bean.lineNumber}</td> 
													<td> 
														<input type="text" name="emiDueDate" class="emiDueDate width95" readonly="readonly" id="dueDate_${status.index+1}" 
															style="border:0px;" value="${bean.emiDueDate}"/>
													</td> 
													<td>
															<c:set var="instalment" value="${bean.instalment}"/>
															<%String instalment = AIOSCommons.formatAmount(pageContext.getAttribute("instalment"));
																			pageContext.setAttribute("instalment", instalment);%>
															<input type="text" style="text-align:right;border:0px;" readonly="readonly" class="width95 principal extend" 
																id="instalment_${status.index+1}" value="${instalment}"/>
														</td> 
														<td>
															<c:set var="interest" value="${bean.interest}"/>
															<%String interest = AIOSCommons.formatAmount(pageContext.getAttribute("interest"));
																			pageContext.setAttribute("interest", interest);%>
															<input type="text" style="text-align:right;border:0px;width:92%;" readonly="readonly" class="interest extend" id="interest_${status.index+1}"
																value="${interest}" />
														</td>
														<c:choose>
															<c:when test="${bean.eiborAmount ne 0 && bean.eiborAmount!= 0}">
																<td>
																	<c:set var="eiborAmount" value="${bean.eiborAmount}"/>
																	<%String eiborAmount = AIOSCommons.formatAmount(pageContext.getAttribute("eiborAmount"));
																					pageContext.setAttribute("eiborAmount", eiborAmount);%>
																	<input type="text" style="text-align:right;border:0px;" readonly="readonly" class="width95 eiborAmount extend" id="eiborAmount_${status.index+1}"
																		value="${eiborAmount}" />
																</td>
															</c:when> 
														</c:choose> 
														<c:choose>
												   	 		<c:when test="${bean.loanCharges ne null && bean.loanCharges ne ''}">
												   	 			<c:forEach var="cbean" items="${bean.loanCharges}"> 
																	<td> 
																		<c:set var="insurance" value="${cbean.value}"/>
																		<%String insurance = AIOSCommons.formatAmount(pageContext.getAttribute("insurance"));
																					pageContext.setAttribute("insurance", insurance);%>
																		<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width90 insurance extend" id="insurance_${status.index+1}"
																			value="${insurance}"/>
																	 </td>  
												   	 			</c:forEach>
												   	 		</c:when>
												   	 	</c:choose>
														<td>
															<c:set var="emiDepreciation" value="${bean.emiDepreciation}"/>
															<%String emiDepreciation = AIOSCommons.formatAmount(pageContext.getAttribute("emiDepreciation"));
																			pageContext.setAttribute("emiDepreciation", emiDepreciation);%>
															<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width95 depreciation" id="depreciation_${status.index+1}"
																value="${emiDepreciation}"/>
														</td>
														<td>
															<c:set var="emiCumulative" value="${bean.emiCumulative}"/>
															<%String emiCumulative = AIOSCommons.formatAmount(pageContext.getAttribute("emiCumulative"));
																			pageContext.setAttribute("emiCumulative", emiCumulative);%>
															<input type="text" readonly="readonly" style="text-align:right;border:0px;"  class="width95 cumulative" id="cumulative_${status.index+1}"
																value="${emiCumulative}"/>
														</td>
														<td>
														<c:set var="amount" value="${bean.amount}"/>
															<%String amount = AIOSCommons.formatAmount(pageContext.getAttribute("amount"));
																			pageContext.setAttribute("amount", amount);%>
															<input type="text" readonly="readonly" style="text-align:right;border:0px;" class="width95 balance" id="balance_${status.index+1}"
																value="${amount}"/>
														</td>  
													<td>
														<input type="text" readonly="readonly" name="description" class="description width95" id="description_${status.index+1}" value="${bean.description}" style="border:0px;"/>
													</td> 
													<td style="display:none;">
														<input type="text" name="status" class="status width95" id="status_${status.index+1}" value="${bean.paidStatus}" style="border:0px;"/>
													</td>
												</tr>
											</c:forEach> 
										</c:when>  
									</c:choose>
								</tbody>
							</table>
						</div> 
					</fieldset> 
 				</div>  
		</div>   
  </form>
  </div> 
</div> 