<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accessCode = "";
$(function(){ 
	  
	 

	 if(Number($('#assetClaimInsuranceId').val())> 0){
 		 $('#claimStatus').val("${ASSET_CLAIM_INSURANCE.status}");
		 $('#payOutNature').val("${ASSET_CLAIM_INSURANCE.lookupDetail.lookupDetailId}");
		 $('#paymentMode').val("${ASSET_CLAIM_INSURANCE.modeOfPayment}");
	 }else
		 $('#claimStatus').val(1);
		 

 	$('input,select').attr('disabled', true);

		$('.common-popup-claiminsurance').click(
						function() {
							$('.ui-dialog-titlebar').remove();
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/show_common_asset_insurance.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			top : 0,
			modal : true
		}); 

		$('#commom-insurance-close')
		.live(
				'click',
				function() {
					$('#common-popup').dialog('close');
				});
	});
function claimInsuranceDiscard(message){ 
 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_claim_insurance.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$('#common-popup').dialog('destroy');
						$('#common-popup').remove();
						$("#main-wrapper").html(result);
						if (message != null && message != '') {
							$('#success_message').hide().html(message)
									.slideDown(1000);
							$('#success_message').delay(2000).slideUp();
						}
					}
				});
	}

	function assetInsurancePopupResult(assetInsuranceId, policyNumber) {
 		$('#assetInsuranceId').val(assetInsuranceId);
		$('#policyNumber').val(policyNumber);
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			claim insurance
		</div> 
		<form name="asset_claim_insurance" id="asset_claim_insurance" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetClaimInsuranceId" name="assetClaimInsuranceId"
					value="${ASSET_CLAIM_INSURANCE.assetClaimInsuranceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="claimStatus">Status<span
									class="mandatory">*</span> </label> <select name="claimStatus"
									id="claimStatus" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="claimStatus" items="${CLAIM_STATUS}">
										<option value="${claimStatus.key}">${claimStatus.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="claimExpense">Claim Expense
								</label> <input type="text" id="claimExpense"
									class="width50 validate[optional,custom[number]]"
									value="${ASSET_CLAIM_INSURANCE.claimExpenses}">
							</div>
							<div style="display: none;">
								<div>
									<label class="width30" for="payOutNature">PayOut Nature<span
										class="mandatory">*</span> </label> <select name="payOutNature"
										id="payOutNature" class="width51">
										<option value="">Select</option>
										<c:forEach var="payOutNature" items="${PAYOUT_NATURE}">
											<option value="${payOutNature.lookupDetailId}">${payOutNature.displayName}</option>
										</c:forEach>
									</select> 
								</div>
								<div>
									<label class="width30" for="paymentMode">Payment Mode </label>
									<select name="paymentMode" id="paymentMode" class="width51">
										<option value="">Select</option>
										<c:forEach var="paymentMode" items="${PAYMENT_MODE}">
											<option value="${paymentMode.key}">${paymentMode.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label class="width30" for="payOutAmount">PayOut Amount</label>
									<input type="text" id="payOutAmount"
										class="width50 validate[optional,custom[number]]"
										value="${ASSET_CLAIM_INSURANCE.payoutAmount}">
								</div>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_CLAIM_INSURANCE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="referenceNumber">Reference
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CLAIM_INSURANCE.referenceNumber ne null && ASSET_CLAIM_INSURANCE.referenceNumber ne ''}">
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${ASSET_CLAIM_INSURANCE.referenceNumber}">
									</c:when>
									<c:otherwise>
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${requestScope.referenceNumber}">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="policyNumber">Policy Number<span
									class="mandatory">*</span> </label> <input type="text"
									id="policyNumber" class="width50 validate[required]"
									readonly="readonly"
									value="${ASSET_CLAIM_INSURANCE.assetInsurance.policyNumber}">
							 <input
									type="hidden" id="assetInsuranceId"
									value="${ASSET_CLAIM_INSURANCE.assetInsurance.assetInsuranceId}">
							</div>
							<div>
								<label class="width30" for="claimDate">Claim Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CLAIM_INSURANCE.claimDate ne null && ASSET_CLAIM_INSURANCE.claimDate ne ''}">
										<c:set var="claimDate"
											value="${ASSET_CLAIM_INSURANCE.claimDate}" />
										<%
											String claimDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"claimDate").toString());
										%>
										<input type="text" id="claimDate"
											class="width50 validate[required]" value="<%=claimDate%>"
											readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="claimDate"
											class="width50 validate[required]" readonly="readonly">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="claimAmount">Claim Amount <span
									class="mandatory">*</span> </label> <input type="text" id="claimAmount"
									class="width50 validate[required,custom[number]]"
									value="${ASSET_CLAIM_INSURANCE.claimAmount}">
							</div> 
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div> 