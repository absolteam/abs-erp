 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:forEach var="bean" varStatus="status" items="${PURCHASE_DETAILVO_INFO}"> 
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
		<td id="productcode_${status.index+1}"> 
			${bean.product.code}
		</td>
		<td id="productname_${status.index+1}"> 
			${bean.product.productName}
		</td> 
		<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
		<td id="purchaseqty_${status.index+1}">${bean.quantity}</td>
		<td id="returnqty_${status.index+1}" >${bean.returnQty}</td>
		<td id="receiveqty_${status.index+1}"> 
			<input type="text" class="width90 quantity validate[optional,custom[onlyFloat]]" style="border:0px;"
				 value="${bean.quantity-bean.returnQty}" name="receiveQty" id="receiveQty_${status.index+1}"/>
		</td>  
		<td id="unitrate_${status.index+1}" >
			<input type="text" class="width90 unitrate validate[optional,custom[onlyFloat]]" style="border:0px; text-align: right;" readonly="readonly" value="${bean.unitRate}" name="unitrate" id="unitrateQty_${status.index+1}"/>
		</td>  
		<td id="total_${status.index+1}" style="text-align: right;">
			 ${(bean.quantity-bean.returnQty)*bean.unitRate}
		</td>  
		<td style="display:none">
			<input type="hidden" name="receivelineid" id="receivelineid_${status.index+1}"/>
			<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
		</td> 
		<td>
			<select name="storeDetail" id="storeDetail_${status.index+1}" class="storeDetail"> 
				<c:choose>
					<c:when test="${STORE_INFO ne null && STORE_INFO ne ''}">
						<c:forEach var="bean" items="${STORE_INFO}">
							<optgroup label="${bean.storeNumber}">
								<c:forEach var="aisle" items="${AISLE_INFO}">
									<c:if test="${bean.storeId==aisle.store.storeId}">
										<option value="${aisle.aisleId}" style="margin-left:15px;">${aisle.aisleNumber}</option> 
									</c:if>  
								</c:forEach>
							</optgroup> 
						</c:forEach> 
					</c:when>
				</c:choose>
			</select>
		</td> 
		<td>
		 <input type="text" name="linedescription" id="linedescription_${status.index+1}"/>
		</td>
		<td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
				<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
		</td>   
	</tr>
</c:forEach>
<script type="text/javascript">
$(function(){ 
	$('#returnDetail').val("${RETURN_NUMBER}");
	calculateTotalReceive();
});
</script>