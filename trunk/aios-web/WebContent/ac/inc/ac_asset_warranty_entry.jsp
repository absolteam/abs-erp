<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accessCode = "";
$(function(){ 
	$('#fromDate,#endDate').datepick();

	 $jquery("#asset_warranty_validation").validationEngine('attach'); 

	 $('.warrentytype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('#asset-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="ASSET_WARRANTY_TYPE"){
				$('#warrantyType').html("");
				$('#warrantyType').append("<option value=''>Select</option>");
				loadLookupList("warrantyType"); 
			}  
		});

	 	$('.asset_warranty_save').click(function(){   
			 if($jquery("#asset_warranty_validation").validationEngine('validate')){
				var warrantyType = Number($('#warrantyType').val()); 
				var assetId = Number($('#assetCreationId').val()); 
				var startDate = $('#fromDate').val(); 
				var endDate = $('#endDate').val(); 
				var warrantyNumber =$('#warrantyNumber').val();  
				var description =$('#description').val();  
				var assetWarrantyId = Number($('#assetWarrantyId').val());  
				var message = "Record created.";
				if (assetWarrantyId > 0)
					message = "Record updated.";
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/save_asset_warranty.action", 
		     	async: false,
		     	data:{
		     			assetWarrantyId : assetWarrantyId, assetId: assetId, warrantyType: warrantyType, startDate: startDate,
		     			endDate: endDate, warrantyNumber: warrantyNumber, description: description
		     		 },
				dataType: "json",
				cache: false,
				success: function(response){  
					if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/show_asset_warranty.action",
																async : false,
																dataType : "html",
																cache : false,
																success : function(
																		result) {
																	$('#common-popup').dialog('destroy');
																	$('#common-popup').remove();
																	$(
																			"#main-wrapper")
																			.html(
																					result);
																	if (assetWarrantyId > 0)
																		$(
																				'#success_message')
																				.hide()
																				.html(	
																						message)
																				.slideDown(
																						1000);
																	else
																		$(
																				'#success_message')
																				.hide()
																				.html(
																						message)
																				.slideDown(
																						1000);

																	$(
																			'#success_message')
																			.delay(
																					2000)
																			.slideUp();
																}
															});
												} else {
													$('#error_message')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													$('#error_message').delay(
															2000).slideUp();
													return false;
												}
											}
										});
							} else
								return false;
						return false;
						});

		$('.asset_warranty_discard').click(function(){ 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_asset_warranty.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) { 
												$('#common-popup').dialog('destroy');
												$('#common-popup').remove();
												$("#main-wrapper").html(result);
											}
										});
							});

		$('.common-popup').click(
				function() {
					$('.ui-dialog-titlebar').remove();
					$
							.ajax({
								type : "POST",
								url : "<%=request.getContextPath()%>/show_common_asset_popup.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) {
												$('#common-popup').dialog('open');
												$(
														$(
																$('#common-popup')
																		.parent())
																.get(0)).css('top',
														0);
												$('.common-result').html(result);
											},
											error : function(result) {
												$('.common-result').html(result);
											}
										});
								return false;
							});

			$('#common-popup').dialog({
				autoOpen : false,
				minwidth : 'auto',
				width : 800,
				bgiframe : false,
				overflow : 'hidden',
				top : 0,
				modal : true
			});
			if(Number($('#assetWarrantyId').val())> 0){
				$('#warrantyType').val($('#tempwarrantyType').val());
			}
});
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationName').val(assetName);
	$('#assetCreationId').val(assetId);
}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			warranty
		</div> 
		<form id="asset_warranty_validation" style="position: relative;">
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 120px;">
								<div>
									<label for="assetName">Asset<span style="color: red;">*</span>
									</label> <input type="text" name="assetCreationName" value="${ASSET_WARRANTY.assetName}"
										id="assetCreationName" tabindex="5"
										class="width50 validate[required]" readonly="readonly" /> <input value="${ASSET_WARRANTY.assetId}"
										type="hidden" name="assetCreationId" id="assetCreationId" />
										<input value="${ASSET_WARRANTY.assetWarrantyId}"
										type="hidden" name="assetWarrantyId" id="assetWarrantyId" />
									<span class="button"> <a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="6" id="description">${ASSET_WARRANTY.description}</textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset style="min-height: 120px;">
								<div>
									<label for="warrantyNumber">Warranty Number<span
										style="color: red;">*</span> </label> <input type="text" value="${ASSET_WARRANTY.warrantyNumber}"
										name="warrantyNumber" id="warrantyNumber" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="warrantyType">Warranty Type<span
										style="color: red;">*</span> </label> <select name="warrantyType"
										id="warrantyType" class="width51 validate[required]"
										tabindex="2">
										<option value="">Select</option>
										<c:forEach var="warrantyType" items="${WARRANTY_TYPE}">
											<option value="${warrantyType.lookupDetailId}">${warrantyType.displayName}</option>
										</c:forEach>
									</select>
									<input type="hidden" id="tempwarrantyType" value="${ASSET_WARRANTY.warrantyTypeId}"/>
									<span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_WARRANTY_TYPE"
									class="btn ui-state-default ui-corner-all warrentytype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label for="fromDate">From Date<span
										style="color: red;">*</span> </label> <input type="text" value="${ASSET_WARRANTY.fromDate}"
										name="fromDate" id="fromDate" tabindex="3"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="endDate">End Date<span style="color: red;">*</span>
									</label> <input type="text" name="endDate" id="endDate" tabindex="4" value="${ASSET_WARRANTY.toDate}"
										class="width50 validate[required]" readonly="readonly" />
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div>
					<div
						class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
						<div style="display: none;"
							class="portlet-header ui-widget-header float-right" id="delete">
							<fmt:message key="accounts.common.button.delete" />
						</div>
						<div style="display: none;"
							class="portlet-header ui-widget-header float-right" id="edit">
							<fmt:message key="accounts.common.button.edit" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right asset_warranty_discard">
							<fmt:message key="accounts.common.button.cancel" />
						</div>
						<div
							class="portlet-header ui-widget-header float-right asset_warranty_save">
							<fmt:message key="accounts.common.button.save" />
						</div>
					</div>
				</div>
			</form>
				<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
		</div></div>