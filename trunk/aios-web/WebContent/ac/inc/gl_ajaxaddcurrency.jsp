<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<td colspan="4" class="tdidentity">
	<div id="errMsg"></div>
	<form name="currencyAdd" id="currencyAddValidate" style="position: relative;">
		<div class="float-left width50" id="hrm">
			<fieldset>
				<legend>
					<fmt:message key="accounts.currencymaster.label.createcurrency" />
				</legend>
				<div>
					<label class="width20"><fmt:message
							key="accounts.currencymaster.label.code" /><span
						style="color: red;">*</span>
					</label> <select name="curId" tabindex="1"
						class="width30 validate[required]" id="selCode">
						<option value="">Select</option>
						<c:if test="${CURRENCYINFO ne null }">
							<c:forEach items="${CURRENCYINFO}" var="bean">
								<option value="${bean.currencyId}">${bean.currencyCountry}--${bean.currencyCode}</option>
							</c:forEach>
						</c:if>
					</select>
				</div> 
				<div>
					<label for="curEnable" class="width20"><fmt:message
							key="accounts.currencymaster.label.enable" />
					</label> <input type="checkbox" tabindex="3" name="curEnable"
						checked="checked" id="curEnable" style="width: 2%;">
				</div>
				<div>
					<label class="width20">Default Currency</label> <input
						type="checkbox" tabindex="2" name="defaultCurrency"
						id="defaultCurrency" style="width: 2%;">
				</div>
				<div id="othererror" class="response-msg error ui-corner-all"
					style="width: 80%; display: none;">${requestScope.errMsg}</div>
				<input type="hidden" name="manipulationFlag" id="manipulationFlag"
					value="${requestScope.manipulationFlag}" />
			</fieldset>
		</div>
		<div class="clearfix"></div>

		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px 25px 10px 10px;">
			<div class="portlet-header ui-widget-header float-right cancel"
				style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right"
				id="add_currency" style="cursor: pointer;">
				<fmt:message key="accounts.currencymaster.button.save" />
			</div>

		</div>
		<input type="hidden" id="default_currency" value="${DEFAULT_CURRENCY}" />
		<input type="hidden" id="pageId" />
	</form></td>
<script type="text/javascript"> 
	//on focus
	 $('#selCode').focus();
	 
	 $(function(){
		 $jquery("#currencyAddValidate").validationEngine('attach');
		 
		 $('.formError').remove(); 
 		  $('.cancel').click(function(){  
			  $('.formError').remove();
			  $($($(slidetab).children().get(4)).children().get(0)).show();
	          $($($(slidetab).children().get(4)).children().get(2)).show(); 
	          $($($(slidetab).children().get(4)).children().get(3)).hide(); 
	          if($("#manipulationFlag").val()=="A"){
	        	$($($(slidetab).children().get(4)).children().get(0)).show();
	          	$($($(slidetab).children().get(4)).children().get(1)).hide();
	          	//$('#DeleteImage_'+i).hide(); 
	          }
	          else{
		          $($($(slidetab).children().get(4)).children().get(1)).show();
	        	  $($($(slidetab).children().get(4)).children().get(0)).hide();
	        	  $($($(slidetab).children().get(4)).children().get(2)).hide(); 
	          }
	          $('.tdidentity').remove(); 	
		  });
		 
		  $('#add_currency').click(function(){   
			var curId=$('#selCode option:selected').val();
			var curIdVal=$('#selCode option:selected').text();
		    var curEnable=$('#curEnable').attr('checked');
		    var defaultCurrency=$('#defaultCurrency').attr('checked'); 
            var manipulationFlag=$('#manipulationFlag').val();
            var defaultflag=true;  
			 if(defaultCurrency==true){
				 $('.rowid').each(function(){ 
	            	var rowid=$(this).attr('id'); 
					var idarray=rowid.split('_');
					var idval=idarray[1];  
					var exitcurflag=$('#defaultcurrencies_'+idval).text().trim().toLowerCase(); 
					if(defaultflag==true){
		            	if($('#default_currency').val()=="true"){  
	                    	defaultflag=false; 
	       			 	}else{ 
	                    	if(exitcurflag=='yes' && Number($('#pageId').val())!=idval){
	                    		defaultflag=false; 
	                        }  
		       			} 
	            	}  
				 });
			 }  
          var lineId=$($($(slidetab).children().get(3)).children().get(1)).val(); 
          if($jquery("#currencyAddValidate").validationEngine('validate')){  
 	          if(defaultflag==true){
	  	         var flag=false;	
	              $.ajax({
	                     type:"POST",
	                     url:"<%=request.getContextPath()%>/gl_currency_addlines.action",
												async : false,
												data : {
													currencyId : curId,
													currencyStatus : curEnable,
													id : lineId,
													manipulationFlag : manipulationFlag,
													defaultCurrency : defaultCurrency
												},
												dataType : "html",
												cache : false,
												success : function(result) {
													$('.formError').hide();
													$('#othererror').hide();
													$('.tempresult').html(
															result);
													if (result != null) {
														if ($('#returnMsg')
																.html() == ''
																|| $(
																		'#returnMsg')
																		.html() == null)
															flag = true;
														else {
															flag = false;
															$("#othererror")
																	.html(
																			$(
																					'#returnMsg')
																					.html());
															$("#othererror")
																	.hide()
																	.slideDown();
														}
													}
												},
												error : function(result) {
													$('.tempresult').html(
															result);
													$("#othererror").html(
															$('#returnMsg')
																	.html());
													$("#othererror").hide()
															.slideDown();
													return false;
												}
											});

									if (flag == true) {
										$($(slidetab).children().get(0)).text(
												curIdVal);
										$(
												$($(slidetab).children().get(3))
														.children().get(1))
												.val(curId);
										if ($('#defaultCurrency').attr(
												'checked')) {
											$($(slidetab).children().get(1))
													.text("YES");
										} else {
											$($(slidetab).children().get(1))
													.text("NO");
										}
										if ($('#curEnable').attr('checked')) {
											$($(slidetab).children().get(2))
													.text("YES");
										} else {
											$($(slidetab).children().get(2))
													.text("NO");
										}

										var childCount = Number($('#childCount')
												.val());
										childCount = childCount + 1;
										$('#childCount').val(childCount);

										$(
												$($(slidetab).children().get(4))
														.children().get(0))
												.hide();
										$(
												$($(slidetab).children().get(4))
														.children().get(2))
												.show();
										$(
												$($(slidetab).children().get(4))
														.children().get(1))
												.show();
										$(
												$($(slidetab).children().get(4))
														.children().get(3))
												.hide();

										$('.tdidentity').remove();

										addedCurrency = addedCurrency + 1;
										if (addedCurrency == 2) {
											i = 0;
											$('.rowid').each(function() {
												i = i + 1;
											});
											$('#DeleteImage_' + i).show();
											$('.addrows').trigger('click');
											addedCurrency = 1;
										}
									}
								} else {
									$("#othererror").hide().html(
											"Default currenct already exits")
											.slideDown();
									return false;
								}
							} else {
								return false;
							}
						});
	});
</script>
