<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<td colspan="4" class="tdidentity" id="childRowId_${rowId}">
	<div class="float-right width48" style="margin-top: 5px;">
		<div id="othererror" class="response-msg error ui-corner-all width60"
			style="display: none;"></div>
	</div>
	<div class="float-left width50" id="hrm">
		<fieldset>
			<legend>
				<fmt:message key="accounts.coa.label.accountdetails" />
			</legend>
			<div>
				<label class="width30"><fmt:message
						key="accounts.coa.label.accountcode" /><span style="color: red;">*</span>
				</label> <input type="text" name="accountCode" id="accountCode"
					class="width50 validate[required,custom[onlyNumber]]" maxlength="8">
			</div>
			<div>
				<label class="width30"><fmt:message
						key="accounts.coa.label.description" /><span style="color: red;">*</span>
				</label> <input type="text" name="accountDescription"
					id="accountDescription" class="width50 validate[required]">
			</div>
			<div class="naturalType">
				<label class="width30"><fmt:message
						key="accounts.coa.label.accounttype" />
				</label> <select name="accountTypeId" id="accountTypeId" class="width51">
					<option value="">Select</option>
					<option value="1">Assets</option>
					<option value="2">Liabilities</option>
					<option value="3">Revenue</option>
					<option value="4">Expenses</option>
					<option value="5">Owners Equity</option>
				</select>
			</div>
		</fieldset>

		<input type="hidden" id="showPage" value="${showPage}" />
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right">
		<div class="portlet-header ui-widget-header float-right rowcancel"
			id="cancel_${rowId}" style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
		<div class="portlet-header ui-widget-header float-right rowsave"
			id="rowsave_${rowId}" style="cursor: pointer;">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
	<div style="display:none;" class="tempresultfinal"></div>
</td>
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0);
$(function(){ 
	$jquery("#coavalidate-form").validationEngine('attach');
	if(Number($('#segmentId').val()!=3))
		$('#accountTypeId').attr('disabled',true); 
	else
		$('#accountTypeId').attr('disabled',false);
	$('.rowcancel').click(function(){ 
		$('.formError').remove();	
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if($('#accountid_'+rowid).text()>0){
			$("#AddCOAImage_"+rowid).hide();
			$("#EditCOAImage_"+rowid).show();
			$("#DeleteCOAImage_"+rowid).show();
			$("#WorkingCOAImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddCOAImage_"+rowid).hide();
			 $("#EditCOAImage_"+rowid).show();
			 $("#DeleteCOAImage_"+rowid).show();
			 $("#WorkingCOAImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddCOAImage_"+rowid).show();
			 $("#EditCOAImage_"+rowid).hide();
			 $("#DeleteCOAImage_"+rowid).show();
			 $("#WorkingCOAImage_"+rowid).hide(); 
		 }
		i=0;
		$('.rowidCOA').each(function(){
			i=i+1;
		}); 
		$('#DeleteImage_'+i).hide(); 
	});

	$('.rowsave').click(function(){
		$('.error').hide();
		 //Find the Id for fetch the value from Id
		if($jquery("#coavalidate-form").validationEngine('validate')){      
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	
		 var accountCode=$('#accountCode').val();
		 if(accountCode==null || accountCode.trim()==''){
			 $("#othererror").hide().html("Enter Account Code").slideDown(1000);
			return false;
		}
		 var accountDescription=$('#accountDescription').val();
		 if(accountDescription==null || accountDescription.trim()==''){
			 $("#othererror").hide().html("Enter Account Description").slideDown(1000);
			return false;
		}
		 var accountTypeId=Number($('#accountTypeId').val());
		 var segmentId=$('#segmentId').val();
		 var showPage=$('#showPage').val();
		 var accountId=0;
		 var url_action="";
		 var lineId=$('#lineId_'+rowid).text(); 
		 
		 if($('#accountid_'+rowid).text()>0){
			 accountId=$('#accountid_'+rowid).text();
			 url_action="chart_of_account_update_linesave";
		 }
		 else if(showPage=="addadd"){
			 url_action="chart_of_account_add_linesave";
		 }
		 else if(showPage=="addedit"){
			 url_action="chart_of_account_addedit_linesave";
		 }
		 var implementationId=1; 
		 var accountType="";
		 if(accountTypeId>0)
			 accountType=$('#accountTypeId :selected').text();
		
		
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"
													+ url_action + ".action",
											async : false,
											data : {
												segmentId : segmentId,
												implementationId : implementationId,
												accountCode : accountCode,
												accountDescription : accountDescription,
												accountId : accountId,
												accountTypeId : accountTypeId,
												id : rowid,
												showPage : showPage,
												id : lineId
											},
											dataType : "html",
											cache : false,
											success : function(result) {
												$('.tempresultfinal').html(
														result);
												var message = $('#returnMsg')
														.html().trim();
												if (message == "Success") {
													flag = true;
												} else {
													flag = false;
													$("#othererror").hide()
															.html(message)
															.slideDown();
													return false;
												}
											},
											error : function(result) {
												$('.tempresultfinal').html(
														result);
												$("#othererror").html(
														$('#returnMsg').html());
												return false;
											}
										});
								if (flag == true) {
									//Bussiness parameter
									$('#code_' + rowid).text(accountCode);
									$('#description_' + rowid).text(
											accountDescription);
									$('#accounttype_' + rowid)
											.text(accountType);
									$('#accounttypeid_' + rowid).text(
											accountTypeId);
									//Button(action) hide & show 
									if ($('#accountid_' + rowid).text() > 0) {
										$("#AddCOAImage_" + rowid).hide();
										$("#EditCOAImage_" + rowid).show();
										$("#DeleteCOAImage_" + rowid).hide();
										$("#WorkingCOAImage_" + rowid).hide();
									} else {
										$("#AddCOAImage_" + rowid).hide();
										$("#EditCOAImage_" + rowid).show();
										$("#DeleteCOAImage_" + rowid).show();
										$("#WorkingCOAImage_" + rowid).hide();
									} 
									$("#childRowId_" + rowid).remove();
									triggerCOAAddRow(rowid);
									//Row count+1
									var childCount = Number($('#childCount')
											.val());
									childCount = childCount + 1;
									$('#childCount').val(childCount);
								}
							} else {
								return false;
							}
						});
	});
</script>