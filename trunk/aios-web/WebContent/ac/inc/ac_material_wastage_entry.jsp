<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.wastagesType>label{float: none !important;}
</style>
<script type="text/javascript">
var slidetab = "";
var wastageDetails = [];
var accessCode = "";
var detailRowId = 0;
$(function(){
	manupulateLastRow();
	$jquery("#materialWastageValidation").validationEngine('attach');  
	 
	$('#wastageDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	
	 $('#wastage_discard').click(function(event){  
		 materialWastageDiscard("");
		 return false;
	 });

	 $('#wastage_save').click(function(){
		 wastageDetails = [];
		 if($jquery("#materialWastageValidation").validationEngine('validate')){
 		 		
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var wastageDate = $('#wastageDate').val(); 
	 			var personId = Number($('#personId').val());   
	 			var materialWastageId = Number($('#materialWastageId').val());   
	 			var description=$('#description').val();   
	 			var productionTypeWastage = $('#productionTypeWastage').attr('checked'); 
	 			 
	 			wastageDetails = getWastageDetails();  
	 			 
	   			if(wastageDetails!=null && wastageDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/material_wastage_save.action", 
	 				 	async: false, 
	 				 	data:{	materialWastageId: materialWastageId, referenceNumber: referenceNumber, wastageDate: wastageDate, personId: personId,
	 				 			productionTypeWastage: productionTypeWastage, description: description, wastageDetails: JSON.stringify(wastageDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							materialWastageDiscard(materialWastageId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown();
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter material wastage details.").slideDown();
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 

	  var getWastageDetails = function(){
		  wastageDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val());  
				var shelfId = Number($('#shelfId_'+rowId).val());  
				var storeId = Number($('#storeId_'+rowId).val());  
				var wastageType = Number($('#wastageType_'+rowId).val()); 
				var wastagepersonId = Number($('#wastagepersonId_'+rowId).val()); 
				var wastageQuantity = Number($('#wastageQuantity_'+rowId).val());  
				var materialWastageDetailId =  Number($('#materialWastageDetailId_'+rowId).val()); 
				var packageDetailId = Number($(
						'#packageType_' + rowId).val());
				var packageUnit = Number($(
						'#packageUnit_' + rowId).val());
				if(typeof productId != 'undefined' && productId > 0 && wastageType > 0 && wastageQuantity > 0){ 
					wastageDetails.push({
						"productId" : productId,
						"shelfId" : shelfId,
						"storeId" : storeId,
						"wastageType" : wastageType,
						"wastagePerson" : wastagepersonId,
						"wastageQuantity" : wastageQuantity, 
						"packageType": packageDetailId,
						"packageUnit" : packageUnit,
						"materialWastageDetailId": materialWastageDetailId
					});
				} 
			});  
			return wastageDetails;
		 }; 


	 $('.show_wastagetype_list').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        detailRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	  				  $($($('#common-popup').parent()).get(0)).css('z-index',100000);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	
	 
		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="MATERIAL_WASTAGE_TYPE"){
	 			$('#wastageType_'+detailRowId).html("");
				$('#wastageType_'+detailRowId).append("<option value=''>Select</option>");
				loadLookupList("wastageType_"+detailRowId); 
			} 
		});
	
	 $('.show_product_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		var tableRowId = getRowId($(this).attr('id'));
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_popup.action", 
		 	async: false,  
		 	data: {itemType: "I", rowId: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#product-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/material_wastage_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.packageType').live('change',function(){
			var rowId=getRowId($(this).attr('id')); 
			$('#wastageQuantity_'+rowId).val('');
			var packageUnit = Number($('#packageUnit_'+rowId).val());
			var packageType = Number($('#packageType_'+rowId).val());  
	 		if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			} else if(packageUnit > 0 && packageType == -1){
				$('#wastageQuantity_'+rowId).val(packageUnit); 
				triggerAddRow(rowId);
			} 
			return false;
		});
		
	$('.packageUnit').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		$('#baseUnitConversion_'+rowId).text('');
		$('#wastageQuantity_'+rowId).val('');
		var packageUnit = Number($(this).val()); 
		var availableQuantity = Number($('#availableQunatity_'+rowId).val());
		var packageType = Number($('#packageType_'+rowId).val()); 
		var productType = $('#productType_'+rowId).val();
		if(packageType == -1 && packageUnit > 0){   
			if(packageUnit <= availableQuantity || productType == 'C'){
				$('#wastageQuantity_'+rowId).val(packageUnit);
				triggerAddRow(rowId);
			}else{
				$('#wastageQuantity_'+rowId).val('');
				$('#wastageQuantity_'+rowId).parent().append("<span class='wastageerr' style='font-size: 9px; color: #d14836;'"
	 	 				+"id=wastageerr_"+rowId+">Wastage qty should be lt or eq available qty.</span>"); 
			} 
		}
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		} 
		return false;
	}); 
		
 	$('.wastageQuantity').live('change',function(){   
 		$('.wastageerr').remove();
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var wastageQuantity = Number($('#wastageQuantity_'+rowId).val());
 	 	var availableQuantity = Number($('#availableQunatity_'+rowId).val());
 	 	var productType = $('#productType_'+rowId).val();
 	 	var packageType = Number($('#packageType_'+rowId).val()); 
 	 	if((wastageQuantity <= availableQuantity || productType == 'C') && packageType == -1){
			$('#packageUnit_'+rowId).val(productQty);
		}else if((wastageQuantity <= availableQuantity || productType == 'C') && packageType > 0){
 	 		getProductConversionUnit(rowId);
 	 		triggerAddRow(rowId);
 	 	}else{
 	 		$('#wastageQuantity_'+rowId).val('');
 	 		$(this).parent().append("<span class='wastageerr' style='font-size: 9px; color: #d14836;'"
 	 				+"id=wastageerr_"+rowId+">Wastage qty should be lt or eq available qty.</span>"); 
 	 	} 
 	 	return false;
	});
	
 	$('.show_wastageperson_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
		var tableRowId = getRowId($(this).attr('id'));
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list_detail.action", 
		 	async: false,  
		 	data: {personTypes: personTypes, id: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
 	
 	$('.wastagesTypeRadio').click(function(){
 		$('.wastagesTypeRadio').attr('disabled', true);
 		$('#materialWastageValidation').show();
 	});
 	
 	$('#product-stock-popup').live('click',function(){
 		$('#common-popup').dialog("close");
 	});
 	
 	$('.rowid').each(function(){
 		var rowId= getRowId($(this).attr('id'));
 		$('#wastageType_'+rowId).val($('#tempwastagetypeId_'+rowId).val());
  	   	$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
 	});
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#personName').val(personname); 
	$('#personId').val(personid); 
	$('#common-popup').dialog("close");
}
function personDetailPopupResult(personid, personname, commonParam, id){
	$('#wastageperson_'+id).html(personname); 
	$('#wastagepersonId_'+id).val(personid); 
	$('#common-popup').dialog("close"); 
}
function commonProductPopup(aData, rowId) {
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName+" ["+aData.productCode+"]");  
	$('#unitCode_' + rowId).html(aData.unitCode);  
	$('#shelfId_' + rowId).val(aData.shelfId);  
	$('#storeId_' + rowId).val(aData.storeId);  
	$('#productType_' + rowId).val(aData.productType);  
	$('#availableQunatity_' + rowId).val(aData.availableQuantity);  
	getProductPackagings("packageType",aData.productId, rowId);
	$('#common-popup').dialog("close");
	return false;
}
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
	var wastageType = Number($('#wastageType_'+rowId).val());  
	var wastageQuantity = Number($('#wastageQuantity_'+rowId).val());  
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid > 0 && wastageQuantity > 0  
			&& wastageType > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function materialWastageDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_material_wastage.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
} 
function getProductPackagings(idName, productId, rowId){
	$('#'+idName+'_'+rowId).html('');
	$('#'+idName+'_'+rowId)
	.append(
			'<option value="-1">Select</option>');
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
	 	async: false,  
	 	data:{productId: productId},
	    dataType: "json",
	    cache: false,
		success:function(response){   
			if(response.productPackageVOs != null){ 
				$.each(response.productPackageVOs, function (index) {
					if(response.productPackageVOs[index].productPackageId == -1){ 
						$('#'+idName+'_'+rowId)
						.append('<option value='
								+ response.productPackageVOs[index].productPackageId
								+ '>' 
								+ response.productPackageVOs[index].packageName
								+ '</option>');
					}else{ 
						var optgroup = $('<optgroup>');
			            optgroup.attr('label',response.productPackageVOs[index].packageName);
			            optgroup.css('color', '#c85f1f'); 
			             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
			                var option = $("<option></option>");
			                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
			                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
			                option.css('color', '#000'); 
			                option.css('margin-left', '10px'); 
			                optgroup.append(option);
			             });
			             $('#'+idName+'_'+rowId).append(optgroup);
					}  
				}); 
				$('#'+idName+'_'+rowId).multiselect('refresh'); 
 			} 
		} 
	});  
	return false;
}
function getProductBaseUnit(rowId){
	$('#baseUnitConversion_'+rowId).text('');
	var packageQuantity = Number($('#packageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	var basePrice = Number($('#baseSellingPrice_' + rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){  
			$('#wastageQuantity_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
			$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
			$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
			var wastageQuantity = Number($('#wastageQuantity_'+rowId).val());
			var availableQunatity = Number($('#availableQunatity_'+rowId).val());
			var productType = $('#productType_'+rowId).val();
			if(wastageQuantity <= availableQunatity || productType == 'C'){
 				triggerAddRow(rowId);
			}else{
				$('#wastageQuantity_'+rowId).val('');
				$('#wastageQuantity_'+rowId).parent().append("<span class='wastageerr' style='font-size: 9px; color: #d14836;'"
	 	 				+"id=wastageerr_"+rowId+">Wastage qty should be lt or eq available qty.</span>"); 
			} 
 		} 
	}); 
	return false;
}
function getProductConversionUnit(rowId){
	var packageQuantity = Number($('#wastageQuantity_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
		} 
	});   
	return false;
}
function loadLookupList(id){
	 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/load_lookup_detail.action",
		data : {
			accessCode : accessCode
		},
		async : false,
		dataType : "json",
		cache : false,
		success : function(response) {

			$(response.lookupDetails)
					.each(
							function(index) {
								$('#' + id)
										.append(
												'<option value='
			+ response.lookupDetails[index].lookupDetailId
			+ '>'
														+ response.lookupDetails[index].displayName
														+ '</option>');
							});
		}
	});
	triggerAddRow(detailRowId);
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material wastage
		</div> 
		<c:set var="showhide" value="none"/>
		<div id="hrm" class="width100" style="margin-bottom:5px;">
			<fieldset>
				<legend>Wastage Type</legend>
				<c:choose> 
					<c:when test="${WASTAGE_DETAIL.wastageType eq 1}"> 
						<c:set var="showhide" value="show"/>
						<div class="float-left width30">
							<input name="wastagesType" id="productionTypeWastage" type="radio"
								class="wastagesTypeRadio" checked="checked" disabled="disabled">
							<span class="wastagesType"><label>Production Wastage</label></span> 
						</div>
						<div class="float-left width30">
							<input name="wastagesType" id="generalTypeWastage" type="radio" class="wastagesTypeRadio" disabled="disabled"> 
							<span class="wastagesType"><label>General Wastage</label></span>
						</div> 
					</c:when>
					<c:when test="${WASTAGE_DETAIL.wastageType eq 2}"> 
						<c:set var="showhide" value="show"/>
						<div class="float-left width30">
							<input name="wastagesType"type="radio"
								class="wastagesTypeRadio" disabled="disabled">
							<span class="wastagesType"><label>Production Wastage</label></span> 
						</div>
						<div class="float-left width30">
							<input name="wastagesType" id="generalTypeWastage" type="radio"  checked="checked"
								class="wastagesTypeRadio" disabled="disabled"> 
							<span class="wastagesType"><label>General Wastage</label></span>
						</div> 
					</c:when>
					<c:otherwise>
						<div class="float-left width30">
							<input name="wastagesType" id="productionTypeWastage" type="radio" class="wastagesTypeRadio">
							<span class="wastagesType"><label for="productionTypeWastage">Production Wastage</label></span>
						</div>
						<div class="float-left width30">
							<input name="wastagesType" id="generalTypeWastage" type="radio" class="wastagesTypeRadio"> 
							<span class="wastagesType"><label for="generalTypeWastage">General Wastage</label></span></div> 
					</c:otherwise> 
				</c:choose> 
			</fieldset>
		</div>
		<form name="materialWastageValidation" id="materialWastageValidation"
			style="position: relative; display: ${showhide};">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<input type="hidden" id="materialWastageId" value="${WASTAGE_DETAIL.materialWastageId}"/>
 				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 95px;"> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									class="width51">${WASTAGE_DETAIL.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${WASTAGE_DETAIL.referenceNumber ne null && WASTAGE_DETAIL.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${WASTAGE_DETAIL.referenceNumber}" 	class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30"> Date<span
									style="color: red;">*</span> </label>  
								<input type="text" readonly="readonly" id="wastageDate" name="wastageDate"  
									value="${MATERIAL_DETAIL.date}" class="validate[required] width50" /> 
							</div> 
							<div>
								<label class="width30"> Person<span
									style="color: red;">*</span> </label> 
								<c:choose>
									<c:when
									test="${WASTAGE_DETAIL.person ne null && WASTAGE_DETAIL.person ne ''}"> 
										<input type="text" readonly="readonly" id="personName" name="personName" 	
											value="${WASTAGE_DETAIL.person.firstName} ${WASTAGE_DETAIL.person.lastName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${WASTAGE_DETAIL.person.personId}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="personName" name="personName"  
											value="${requestScope.personName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${requestScope.personId}"/>
									</c:otherwise>
								</c:choose>  
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Material Wastage Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th class="width10">Wastage Type</th>
										<th class="width5">Person</th>  
										<th class="width5">Packaging</th>
										<th class="width5">Quantity</th>
										<th class="width5">Base Qty</th> 
										<th class="width5">Description</th>  
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${WASTAGE_DETAIL.materialWastageDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> 
												<input type="hidden" name="productType"
												id="productType_${status.index+1}" value="${DETAIL.productType}"/><span class="width60 float-left"
												id="product_${status.index+1}">${DETAIL.product.productName} [${DETAIL.product.code}]</span>
												<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												<input type="hidden" name="shelfId" id="shelfId_${status.index+1}" value="${DETAIL.shelf.shelfId}" />
												<input type="hidden" name="storeId" id="storeId_${status.index+1}" value="${DETAIL.store.storeId}" />
											</td> 
											<td>
												<input type="hidden" name="tempwastagetypeId" id="tempwastagetypeId_${status.index+1}"
												value="${DETAIL.lookupDetail.lookupDetailId}" />
												<select name="wastageType" id="wastageType_${status.index+1}"
													class="width70 wastageType">
													<option value="">Select</option>
													<c:forEach var="wastageType" items="${WASTAGE_TYPE}">
														<option value="${wastageType.lookupDetailId}">${wastageType.displayName}</option>
													</c:forEach>
												</select> 
												<span class="button float-right"> <a
													style="cursor: pointer;" id="MATERIAL_WASTAGE_TYPE_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_wastagetype_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<input type="hidden" name="wastagepersonId" id="wastagepersonId_${status.index+1}"
												value="${DETAIL.person.personId}" />
												<span id="wastageperson_${status.index+1}">${DETAIL.person.firstName} ${DETAIL.person.lastName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prsnID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_wastageperson_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
										</td>
										<td><input type="text"
											class="width96 packageUnit validate[optional,custom[number]]"
											id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
										</td>  
										<td><input type="hidden" name="wastageQuantity"
												id="wastageQuantity_${status.index+1}" value="${DETAIL.wastageQuantity}" 
												class="wastageQuantity width98 validate[optional,custom[number]]">
												<input type="hidden" id="availableQunatity_${status.index+1}" value="${DETAIL.availableQuantity}"/>
												<span id="baseUnitConversion_${status.index+1}" class="width10" style="display: none;"></span>
												<span id="baseDisplayQty_${status.index+1}">${DETAIL.wastageQuantity}</span>
										</td> 
										<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${DETAIL.description}" class="description width98"/>
										</td>
										<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="materialWastageDetailId"
												id="materialWastageDetailId_${status.index+1}"
												value="${DETAIL.materialWastageDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(WASTAGE_DETAIL.materialWastageDetailVOs)+1}"
										end="${fn:length(WASTAGE_DETAIL.materialWastageDetailVOs)+2}"
										step="1" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> 
												<input type="hidden" name="productType"
												id="productType_${i}" /> <span id="product_${i}" class="width60 float-left"></span> 
												<span class="width10 float-right" id="unitCode_${i}"
													style="position: relative;"></span>
												<span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show_product_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												<input type="hidden" name="shelfId" id="shelfId_${i}"/>
												<input type="hidden" name="storeId" id="storeId_${i}"/>
											</td> 
											<td> 
												<select name="wastageType" id="wastageType_${i}"
													class="width70 wastageType">
													<option value="">Select</option>
													<c:forEach var="wastageType" items="${WASTAGE_TYPE}">
														<option value="${wastageType.lookupDetailId}">${wastageType.displayName}</option>
													</c:forEach>
												</select> 
												<span class="button float-right"> <a
													style="cursor: pointer;"  id="MATERIAL_WASTAGE_TYPE_${i}"
													class="btn ui-state-default ui-corner-all show_wastagetype_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<input type="hidden" name="wastagepersonId" id="wastagepersonId_${i}"/>
												<span id="wastageperson_${i}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prsnID_${i}"
													class="btn ui-state-default ui-corner-all show_wastageperson_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${i}" class="packageType">
													<option value="">Select</option>
												</select> 
											</td> 
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${i}"  />
	 										</td>
											<td><input type="hidden" name="wastageQuantity"id="wastageQuantity_${i}"
												class="wastageQuantity width98 validate[optional,custom[number]]">
												<input type="hidden" id="availableQunatity_${i}"/>
												<span id="baseUnitConversion_${i}" class="width10" style="display: none;"></span>
												<span id="baseDisplayQty_${i}"></span>
											</td> 
											<td><input type="text" name="description"
												id="description_${i}" class="description width98"/>
											</td> 
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="materialWastageDetailId"
												id="materialWastageDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="wastage_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<c:if test="${WASTAGE_DETAIL eq null || WASTAGE_DETAIL.status eq true}">
					<div class="portlet-header ui-widget-header float-right save"
						id="wastage_save" style="cursor: pointer;">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</c:if> 
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>