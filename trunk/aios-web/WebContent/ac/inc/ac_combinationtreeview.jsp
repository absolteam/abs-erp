<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<style>
.ui-autocomplete-input {
	min-height: 27px !important;
	width: 70%!important;
}
.combination-error{
	color: #d80036;
	font-size: 9px !important;
}
</style>
<script type="text/javascript">
var tree;
$(function(){  
	$jquery("#combination_treeview").fancytree({ 
 		extensions: ['filter'],  
 		quicksearch: true, 
 		icons: true,  
 		minExpandLevel: 1,
		debugLevel: 0,
		selectMode: 1,
		dblclick: function(event, data) {
			combinationTreeReturn(data.node); 
	    },
 		source: 
			{url: "<%=request.getContextPath()%>/show_combination_tree.action"},  
 		filter: {
 	        autoApply: true,
  	        fuzzy: false,  
 	        highlight: true, 
 	        mode: "hide" 
 	    },postProcess: function(event, data) {  
 	    	tree = $jquery("#combination_treeview").fancytree("getTree");
 	    	$('.ui-autocomplete-input').attr('name', 'search'); 
 	    	callRecursiveData(data.response);
 	  	},
	}); 
	
	$jquery("input[name=search]").live('keyup',function(e){ 
		if(Number($(this).val().length) > 3){
			var n,
	        opts = {
	          autoExpand: true,
	          leavesOnly: false
	        },
	        match = $(this).val();

	      if(e && e.which === $jquery.ui.keyCode.ESCAPE || $jquery.trim(match) === ""){
	        $("button#btnResetSearch").click();
	        return;
	      }
		  
	      // Pass function to perform match
	      n = tree.filterNodes(match, opts); 
	      
	      $("button#btnResetSearch").attr("disabled", false);
	      $jquery("span#matches").text("(" + n + " matches)");
		} 
		if(e && e.which === $jquery.ui.keyCode.ESCAPE || $jquery.trim($(this).val()) === ""){
	        $("button#btnResetSearch").click();
	        return;
	    }
    }).focus(); 
	
	$('#codecombination-popup').dialog('open');
	$('#codecombination-popup').css('overflow','hidden');  
 	$($($('#codecombination-popup').parent()).get(0)).css('top',0);
	
	$('.treecancel').click(function(){ 
		$('#codecombination-popup').dialog('close');
	});
	
	$("button#btnResetSearch").click(function(e){
      	$("input[name=search]").val("");
      	$("span#matches").text("");
      	tree.clearFilter();
    }).attr("disabled", true);
	
	$('.combinationSearch').comboboxpos({ 
	    selected: function(event, ui){  
	    	var combination = $.trim($(this).val()); 
	    	if(null != combination && combination != ''){
	    		var accessId = getLastId($.trim($(this).val()));
 	    		if(accessId > 2){ 
	    			var combinationAccount= $.trim($(".combinationSearch option:selected").text()); 
	    			var combinationViewId = getFirstId($.trim($(this).val()));
 	    			setCombination(combinationViewId,combinationAccount);
	    		    $('#codecombination-popup').dialog('close');
	    		}else{
	    			$('#popup-error').hide().html("Transaction can't be created under this combination.").slideDown(1000);
	    			$('#popup-error').delay(2000).slideUp();
	    		}
	    	}
       	}
	});
	
	$('.ui-autocomplete-input').next('button').remove();
});
function combinationTreeReturn(node){
 	var accessId = getLastId(node.refKey);
 	if(accessId > 2){ 
 		var combinationViewId = getFirstId(node.key);
 		setCombination(combinationViewId,node.title);
	    $('#codecombination-popup').dialog('close');
	}else{
		$('#popup-error').hide().html("Transaction can't be created under this combination.").slideDown(1000);
		$('#popup-error').delay(2000).slideUp();
	}
	return false;
}
function callRecursiveData(data){  
   	$.each(data, function(i, item) {  
  		$('#combinationSearch')
			.append(
					'<option value="'+data[i].key+'#'+data[i].refKey+'">'+data[i].title+'</option>');
  		callRecursiveData(data[i].children);
	});  
} 
function getFirstId(id) {
	var idval = id.split('#');
	var rowId = Number(idval[0]);
	return rowId;
}
function getLastId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="ac.combination.combinationtreeview"/></div>	 
			<div class="portlet-content">  
				<div id="popup-error" class="response-msg error ui-corner-all" 
					style="display: none; width: 200px;"></div>  
				<div id="popup-success" class="response-msg success ui-corner-all" style="display:none;"></div>  
			  	<div class="width100 float-left" style="height: 400px; overflow-y :auto;">  
			  		<div class="float-right  width48">  
			  			<label class="width10" style="float: none; font-weight: bold;">Search:</label> 
						<select name="combinationSearch" id="combinationSearch" class="combinationSearch">
		 				</select>
						<button id="btnResetSearch" style="float: none; position: relative; 
							right: 5px; top:1px; min-height: 34px; font-weight: bold;">&times;</button>
						<span id="matches" style="display: none;"></span>
			  		</div>
					<div class="float-left  width50">   
						 <div id="combination_treeview"></div> 
					</div>  
				</div>  
			</div>  
			<div class="clearfix"></div>  
			<div>  
				<div class="portlet-header ui-widget-header float-right treecancel" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>  
			</div>  
  	</div> 
</div>