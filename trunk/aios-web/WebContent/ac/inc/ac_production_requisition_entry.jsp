<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var slidetab = "";
var requisitionDetails = [];
$(function(){
	manupulateLastRow();
	$jquery("#productionRequisitionValidation").validationEngine('attach');  
	 
	 $('#production_discard').click(function(event){  
		 productionRequisitionDiscard("");
		 return false;
	 }); 
	 
	 $('#production_save').click(function(){
		 idleMixDetails = [];
		 if($jquery("#productionRequisitionValidation").validationEngine('validate')){ 
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var productionRequisitionId = Number($('#productionRequisitionId').val()); 
	 			var personId = Number($('#personId').val());   
	 			var description=$('#description').val();  
	 			var requistionDate = $('#requistionDate').val();   
	 			requisitionDetails = getRequisitionDetails();  
	 			 
	   			if(requisitionDetails!=null && requisitionDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/production_requisition_save.action", 
	 				 	async: false, 
	 				 	data:{	productionRequisitionId: productionRequisitionId, referenceNumber: referenceNumber, personId: personId,
	 				 			requistionDate: requistionDate, description: description, requisitionDetails: JSON.stringify(requisitionDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							productionRequisitionDiscard(productionRequisitionId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown();
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter production requisition details.").slideDown();
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 

	  var getRequisitionDetails = function(){
		  requisitionDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val());
				var quantity = Number($('#quantity_'+rowId).val());
 				var shelfId = Number($('#shelfid_'+rowId).val());  
				var productionRequisitionDetailId =  Number($('#productionRequisitionDetailId_'+rowId).val()); 
				var description = $('#description_'+rowId).val(); 
				 var packageType = Number($('#packageType_'+rowId).val());
	 			 var packageUnit = Number($('#packageUnit_'+rowId).val());
				if(typeof productId != 'undefined' && productId > 0 && packageUnit > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"quantity" : quantity, 
						"shelfId" : shelfId, 
						"productionRequisitionDetailId": productionRequisitionDetailId,
						"description" : description,
						"packageType" : packageType,
						"packageUnit" : packageUnit
					});   
					requisitionDetails.push({
						"requisitionDetails" : jsonData
					});
				} 
			});  
			return requisitionDetails;
		 }; 
	 
	 $('.finished-product-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));  
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_workinprocess_product_popup.action", 
			 	async: false,   
			 	data:{id:rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	 
	 $('.show_store_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id')); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_session_common.action", 
			 	async: false,  
			 	data: {rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0); 
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		 });
	 
	 $('.rackid').live(
				'dblclick',
				function() {
					currentId = $(this);
					var tempvar = $(currentId).attr('id');
					var idarray = tempvar.split('_');
					var rowid = Number(idarray[1]);
					var storeRowId = Number($('#store_row_id').html());  
					$('#shelfid_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#shelf_'+storeRowId).html($('#rackstorename_' + rowid).html());  
					$('#common-popup').dialog('close');
					return false;
		});

	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#product-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/production_requisition_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.packageType').live('change',function(){
			var rowId=getRowId($(this).attr('id')); 
			$('#quantity_'+rowId).val('');
			var packageUnit = Number($('#packageUnit_'+rowId).val());
			var packageType = Number($('#packageType_'+rowId).val());  
 			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			} else if(packageUnit > 0 && packageType == -1){
				$('#quantity_'+rowId).val(packageUnit); 
				triggerAddRow(rowId);
			} 
			return false;
		});
		
		$('.packageUnit').live('change',function(){
			var rowId=getRowId($(this).attr('id'));
			$('#baseUnitConversion_'+rowId).text('');
			$('#quantity_'+rowId).val('');
			var packageUnit = Number($(this).val()); 
			var packageType = Number($('#packageType_'+rowId).val()); 
 			if(packageType == -1 && packageUnit > 0){  
		 	 	$('#quantity_'+rowId).val(packageUnit);
 			}
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			}
			triggerAddRow(rowId);
			return false;
		}); 
		
 	$('.quantity').live('change',function(){  
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var productQty = Number($('#quantity_'+rowId).val());   
 	 	var packageType = Number($('#packageType_'+rowId).val()); 
 	 	if(productQty > 0 && packageType == -1){
			$('#packageUnit_'+rowId).val(productQty);
		}else if(productQty > 0 && packageType > 0){ 
			getProductConversionUnit(rowId);
		} 
 	 	triggerAddRow(rowId);
 	 	return false;
	});
	
 	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	});  
 	
 	if(Number($('#productionRequisitionId').val())>0){
		 $('#requistionDate').datepick(); 
		 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
 	   		 $('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
		 }); 
	}else{
		 $('#requistionDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
	} 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#personName').val(personname); 
	$('#personId').val(personid); 
	$('#common-popup').dialog("close");
} 
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
	var quantity = Number($('#packageUnit_'+rowId).val());  
 	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid > 0 && quantity > 0  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function productionRequisitionDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_production_requisition.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
} 
function finishedProductPopup(aaData, rowId){
	$('#product_'+rowId).html(aaData.code+" -- "+aaData.productName);
	$('#productid_'+rowId).val(aaData.productId); 
	$('#unitCode_'+rowId).val(aaData.unitCode);
	if(aaData.storeId != null && Number(aaData.shelfId)>0){
		$('#shelfid_'+rowId).val(aaData.shelfId); 
		$('#shelf_'+rowId).text(aaData.storeName); 
	}else{
		$('#shelfid_'+rowId).val(''); 
		$('#shelf_'+rowId).text(''); 
	}
	getProductPackagings("packageType",aData.productId, rowId);
}

function getProductPackagings(idName, productId, rowId){
	$('#'+idName+'_'+rowId).html('');
	$('#'+idName+'_'+rowId)
	.append(
			'<option value="-1">Select</option>');
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
	 	async: false,  
	 	data:{productId: productId},
	    dataType: "json",
	    cache: false,
		success:function(response){   
			if(response.productPackageVOs != null){ 
				$.each(response.productPackageVOs, function (index) {
					if(response.productPackageVOs[index].productPackageId == -1){ 
						$('#'+idName+'_'+rowId)
						.append('<option value='
								+ response.productPackageVOs[index].productPackageId
								+ '>' 
								+ response.productPackageVOs[index].packageName
								+ '</option>');
					}else{ 
						var optgroup = $('<optgroup>');
			            optgroup.attr('label',response.productPackageVOs[index].packageName);
			            optgroup.css('color', '#c85f1f'); 
			             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
			                var option = $("<option></option>");
			                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
			                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
			                option.css('color', '#000'); 
			                option.css('margin-left', '10px'); 
			                optgroup.append(option);
			             });
			             $('#'+idName+'_'+rowId).append(optgroup);
					}  
				}); 
				$('#'+idName+'_'+rowId).multiselect('refresh'); 
 			} 
		} 
	});  
	return false;
}

function getProductBaseUnit(rowId){
	$('#baseUnitConversion_'+rowId).text('');
	var packageQuantity = Number($('#packageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	var basePrice = Number(0);
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#quantity_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
			$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
			$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
 		} 
	}); 
	return false;
}
function getProductConversionUnit(rowId){
	var packageQuantity = Number($('#quantity_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
		} 
	});   
	return false;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> production requisition
		</div> 
		<form name="productionRequisitionValidation" id="productionRequisitionValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<input type="hidden" id="productionRequisitionId" value="${PRODUCTION_REQUISITION.productionRequisitionId}"/> 
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 95px;">  
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									class="width51">${PRODUCTION_REQUISITION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${PRODUCTION_REQUISITION.referenceNumber ne null && PRODUCTION_REQUISITION.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${PRODUCTION_REQUISITION.referenceNumber}" 	class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>  
							<div>
								<label class="width30">Requisition Date </label>
								<input type="text" readonly="readonly" id="requistionDate" name="requistionDate"
									value="${PRODUCTION_REQUISITION.requisitionDateStr}" class="validate[required] width50" />
							</div>
							<div>
								<label class="width30"> Person<span
									style="color: red;">*</span> </label> 
								<c:choose>
									<c:when
									test="${PRODUCTION_REQUISITION.person ne null && PRODUCTION_REQUISITION.person ne ''}"> 
										<input type="text" readonly="readonly" id="personName" name="personName" 	
											value="${PRODUCTION_REQUISITION.person.firstName} ${PRODUCTION_REQUISITION.person.lastName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${PRODUCTION_REQUISITION.person.personId}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="personName" name="personName"  
											value="${requestScope.personName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${requestScope.personId}"/>
									</c:otherwise>
								</c:choose>  
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>  
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Requisition Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th class="width10">Shelf</th>
										<th style="width: 8%;">Packaging</th> 
										<th class="width5">Quantity</th>
										<th class="width5">Base Qty</th>
 										<th class="width5">Description</th>  
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${PRODUCTION_REQUISITION.productionRequisitionDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span class="width60 float-left"
												id="product_${status.index+1}">${DETAIL.product.productName} [${DETAIL.product.code}]</span>
												<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all finished-product-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="hidden" name="shelfId"
												id="shelfid_${status.index+1}"
												value="${DETAIL.shelf.shelfId}" /> <span
												id="shelf_${status.index+1}">${DETAIL.storeName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="shelfID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_store_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>  
											<td>
												<select name="packageType" id="packageType_${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
										</td>
										<td><input type="text"
											class="width96 packageUnit validate[optional,custom[number]]"
											id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
										</td> 
										<td><input type="hidden" name="quantity"
											id="quantity_${status.index+1}" value="${DETAIL.baseQuantity}"
											class="quantity validate[optional,custom[number]] width80">
											<span id="baseUnitConversion_${status.index+1}" class="width10" 
												style="display: none;">${DETAIL.baseUnitName}</span>
											<span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
										</td>  
										<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${DETAIL.description}" class="description width98"/>
										</td>
										<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productionRequisitionDetailId"
												id="productionRequisitionDetailId_${status.index+1}"
												value="${DETAIL.productionRequisitionDetailId}" />
										</td>
									</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(PRODUCTION_REQUISITION.productionRequisitionDetailVOs)+1}"
										end="${fn:length(PRODUCTION_REQUISITION.productionRequisitionDetailVOs)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}" class="width60 float-left"></span> 
												<span class="width10 float-right" id="unitCode_${i}"
													style="position: relative;"></span>
												<span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all finished-product-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="hidden" name="shelfId"
												id="shelfid_${i}"/> <span id="shelf_${i}"></span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="shelfID_${i}"
													class="btn ui-state-default ui-corner-all show_store_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td>
												<select name="packageType" id="packageType_${i}" class="packageType">
													<option value="">Select</option>
												</select> 
											</td> 
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${i}"  />
	 										</td>
											<td><input type="hidden" name="quantity"
												id="quantity_${i}"
												class="quantity validate[optional,custom[number]] width80">
												<span id="baseUnitConversion_${status.index+1}" class="width10" 
													style="display: none;"></span>
												<span id="baseDisplayQty_${i}"></span>
											</td> 
											<td><input type="text" name="description"
												id="description_${i}" class="description width98">
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productionRequisitionDetailId"
												id="productionRequisitionDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="production_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="production_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>