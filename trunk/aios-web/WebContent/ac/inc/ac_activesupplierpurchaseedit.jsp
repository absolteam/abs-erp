<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend><fmt:message key="accounts.grn.label.activepurchaseorder"/></legend>
	<div>
		<c:forEach var="ACTIVE_PO" items="${PURCHASE_INFO}">
			<label for="purchase_${ACTIVE_PO.purchaseId}" class="width10">
				<c:if test="${ACTIVE_PO.editFlag eq true}">
					<input type="checkbox" checked="checked" name="activePONumber" class="activePONumber" id="purchase_${ACTIVE_PO.purchaseId}"/>
				</c:if>
				<c:if test="${ACTIVE_PO.editFlag eq false}">
					<input type="checkbox" name="activePONumber" class="activePONumber" id="purchase_${ACTIVE_PO.purchaseId}"/>
				</c:if>
				${ACTIVE_PO.purchaseNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	$('.activePONumber').each(function(){
		if($(this).attr('checked')){
			validatePurchaseInfo($(this));   
		}
	});
	
	$('.activePONumber').click(function(){   
		validatePurchaseInfo($(this));   
	});
});
</script>