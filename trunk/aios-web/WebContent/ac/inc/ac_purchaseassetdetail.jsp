<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#StockDetailList').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 5,
		"sAjaxSource" : "show_assetreceive_details.action",

		"aoColumns" : [  {
			"mDataProp" : "productCode"
		 },{
			"mDataProp" : "productName"
		 },{ 
			"mDataProp" : "storeName"
		 },{
			"mDataProp" : "unitRate" 
		 },{
			"mDataProp" : "availableQuantity" 
		 } ], 
	});	

	 $('#StockDetailList tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) 
			  $(this).addClass('row_selected'); 
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	      }
 	}); 
	 
	/* Click event handler */
	$('#StockDetailList tbody tr').live('dblclick', function () {  
		 aData = oTable.fnGetData(this);
		 commonStockDetail(aData.stockId, aData.productName); 
  	});
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>stock
			details
		</div>
		<div class="portlet-content">
			<div id="stock_detail_list">
				<table id="StockDetailList" class="display">
					<thead>
						<tr> 
							<th>Product Code</th>
							<th>Product Name</th>
							<th>Store</th>
							<th>Unit Rate</th>
 							<th>Quantity</th>
						</tr>
					</thead>

				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="assetstock_popupclose">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
			</div>
		</div>
	</div>
</div>