<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
	
	$('input, select').attr('disabled', true);
	$('.rowid').each(function(){
		var id = Number($(this).attr('id').split('_')[1]);  
		$('#transferType_'+id).val($('#tempTransferType_'+id).val());
		$('#transferReason_'+id).val($('#tempTransferReason_'+id).val());
	});
}); 
</script>
<div id="main-content">
	<div id="mt_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material
			transfer
		</div> 
		<form name="materialTransferValidation"
			id="materialTransferValidation" style="position: relative;">
			<input type="hidden" id="materialTransferId" name="materialTransferId" 
				value="${MATERIAL_TRANSFER.materialTransferId}"/>
			<div class="portlet-content"> 
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 90px;"> 
							<div>
								<label class="width30" for="description"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${MATERIAL_TRANSFER.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30"> Reference No. </label>
								<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${MATERIAL_TRANSFER.referenceNumber} "
											class="validate[required] width50" />
							</div>
							<div>
								<label class="width30" for="transferDate"> Transfer Date</label>
								<c:set var="transferDate"
									value="${MATERIAL_TRANSFER.transferDate}" />
								<input name="transferDate" type="text" readonly="readonly"
											id="transferDate"
											value="<%=DateFormat.convertDateToString(pageContext
										.getAttribute("transferDate").toString())%>"
											class="transferDate validate[required] width50">
							</div>
							<div>
								<label class="width30" for="transferPerson">Transfer
									Person</label> 
									<input type="text" readonly="readonly" name="transferPerson" id="transferPerson" class="width50 validate[required]"
										value="${MATERIAL_TRANSFER.personByTransferPerson.firstName } ${MATERIAL_TRANSFER.personByTransferPerson.lastName }" />
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="requisition-info" id="hrm">
					<div class="portlet-content class90" id="hrm"
						style="margin-top: 10px;">
						<fieldset>
							<legend>
								Material Transfer Detail
							</legend>
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th class="width10">Product</th>
											<th class="width5">Quantity</th>
											<th class="width10">Transfer Type</th>
											<th class="width10">Reason</th>
											<th class="width10">Transfer Store</th> 
											<th class="width5" style="display: none;">Price</th>
											<th class="width5" style="display: none;" >Total</th>
											<th class="width10"><fmt:message
													key="accounts.journal.label.desc" /></th>
											<th style="width: 1%; display: none;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:forEach var="DETAIL"
											items="${MATERIAL_TRANSFER.materialTransferDetails}"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
												<td><input type="hidden" name="productId"
													id="productid_${status.index+1}"
													value="${DETAIL.product.productId}" /> <span
													id="product_${status.index+1}">${DETAIL.product.productName}</span> 
												</td>
												<td><input type="text" name="productQty"
													id="productQty_${status.index+1}"
													value="${DETAIL.quantity}"
													class="productQty validate[optional,custom[number]] width80">
													<input type="hidden" name="totalAvailQty"
													id="totalAvailQty_${status.index+1}">
												</td>
												<td style="display: none;"><input type="hidden"
													name="storeFromId" id="storeFromId_${status.index+1}"
													value="${DETAIL.shelfByFromShelfId.shelfId}" />
												</td>
												<td ><select name="transferType"
													id="transferType_${status.index+1}"
													class="width75 transferType">
														<option value="">Select</option>
														<c:forEach var="transferType" items="${TRANSFER_TYPE}">
															<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
														</c:forEach>
												</select> <input type="hidden"
													id="tempTransferType_${status.index+1}"
													value="${DETAIL.lookupDetailByTransferType.lookupDetailId}" /> 
												</td>
												<td><select name="transferReason"
													id="transferReason_${status.index+1}"
													class="width75 transferReason">
														<option value="">Select</option>
														<c:forEach var="transferReason" items="${TRANSFER_REASON}">
															<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
														</c:forEach>
												</select> <input type="hidden"
													id="tempTransferReason_${status.index+1}"
													value="${DETAIL.lookupDetailByTransferReason.lookupDetailId}" /> 
												</td>
												<td><input type="hidden" name="shelfId"
													id="shelfId_${status.index+1}"
													value="${DETAIL.shelfByShelfId.shelfId}" /> <span
													id="shelf_${status.index+1}">${DETAIL.shelfByShelfId.name}</span> 
												</td> 
												<td style="display: none;"><input type="text" name="amount"
													id="amount_${status.index+1}" value="${DETAIL.unitRate}"
													style="text-align: right;" readonly="readonly"
													class="amount width98 validate[optional,custom[number]] right-align">
												</td>
												<td style="display: none;"><c:set var="totalAmount"
														value="${DETAIL.quantity * DETAIL.unitRate}" /> <span
													id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
												</td>
												<td><input type="text" name="linesDescription"
													id="linesDescription_${status.index+1}"
													value="${DETAIL.description}" class="width98 linesDescription"
													maxlength="150">
												</td>
												<td style="width: 1%; display: none;" class="opn_td"
													id="option_${status.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${status.index+1}"
													style="cursor: pointer; display: none;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${status.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${status.index+1}" style="cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${status.index+1}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="materialTransferDetailId"
													id="materialTransferDetailId_${status.index+1}"
													value="${DETAIL.materialTransferDetailId}" />
												</td>
											</tr>
										</c:forEach> 
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</div> 
			</div> 
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div>