<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/michael-multiselect/js/ui.multiselect.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/js/michael-multiselect/css/ui.multiselect.css" />

<style type="text/css">
#purchase_print {
	overflow: hidden;
} 
.portlet-content{padding:1px;}
.buttons { margin:4px;}

.ui-widget-header{
	padding:4px;
}

	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}

.buttons {
	margin: 4px;
}

#filter-form td {
	padding: 5px!important;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 90%;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript"> 

var oTable; var selectRow=""; var aSelected = []; var aData="";
var supplierId = 0; var productId = 0; var datefrom = ""; var dateto ="";
$(function(){  
	
	 $('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	 $('#selectedMonth,#linkedMonth').change();
	 $('#l10nLanguage,#rtlLanguage').change();
	 if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	 }
	 $('#fromDate,#endDate').datepick({
	 	onSelect: customRange, showTrigger: '#calImg'}); 
	 
	 $("#productId,#supplierId").multiselect();
	 		
	 $(".print-call").click(function(){
		datefrom = $('#fromDate').val();
		dateto = $('#endDate').val();   
		var products = generateDataToSendToDB("productId"); 
		var suppliers = generateDataToSendToDB("supplierId"); 
		if((products != null  && products!= '') || (suppliers != null  && suppliers!= '' )){
			window.open('<%=request.getContextPath()%>/getpurchase_comparisonreport.action?fromDate='+ datefrom + '&toDate=' + dateto + '&products='+ products
					+'&suppliers='+suppliers,'_blank','width=1100,height=700,scrollbars=yes,left=100px,top=2px');
		} else{
			alert("Please select either product or supplier for comparison");
			return false;
		}
	 }); 
}); 
	
	
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth){
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRange(dates) {
	if (this.id == 'fromDate') { 
		$('#endDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#fromDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
}	
function generateDataToSendToDB(source){
  	var jsonData = []; 
	$('#'+source+' option:selected').each(function(){
 		jsonData.push(Number($(this).val())); 
 	}); 
	if(null != jsonData && jsonData.length > 0)
 		return JSON.stringify(jsonData);
	else
		return null;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Purchase Comparison
		</div>
		<div class="portlet-content">
			<form id="filter-form">
			<div class="width100 float-left">
				<table width="90%" > 
					<tr> 
						<td class="width10">From Date</td>
						<td>
							<div>
								<input type="text" class="width30"
								name="fromDate" class="fromDate" id="fromDate" readonly="readonly">
							</div>
						</td>
					</tr>
					<tr>
						<td class="width10">To Date</td>
						<td>
							<div>
								 <input type="text"  class="width30"
								name="toDate" class="toDate" id="endDate" readonly="readonly">
							</div>
						</td>
					</tr>
					
				</table> 
			</div>
			</form>
			<div class="width100 float-left" style="padding: 5px;"> 
				<div class="width50 float-left">
					<label class="width100" style="font-weight: bold;">Product</label>
					<select id="productId" class="product width100" name="product" multiple="multiple">
						<c:forEach var="product" items="${PRODUCT_INFO}">
							<option value="${product.productId}">${product.code} -- ${product.productName}</option>						
						</c:forEach> 
					</select> 
				</div> 
				<div class="width48 float-right">
					<label class="width100" style="font-weight: bold;">Supplier</label>
					<select id="supplierId" class="supplier width100" name="supplier" multiple="multiple">
						<c:forEach var="supplier" items="${SUPPLIER_INFO}">
							<option value="${supplier.supplierId}">${supplier.supplierName}</option>						
						</c:forEach> 
					</select>	
					
				</div> 
			</div>  
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right "> 
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>