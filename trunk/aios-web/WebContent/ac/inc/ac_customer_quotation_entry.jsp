<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var quotationDetails="";
var chargesDetails = "";
var tempid = "";
var accessCode = "";
var sectionRowId = 0;
var tableRowId = 0;
var globalRowId=null;
$(function(){   
	manupulateLastRow();
	$jquery("#customerQuotationValidation").validationEngine('attach'); 
	$jquery(".amount,.totalAmountH").number(true, 2);
	$('#quotationDate,#expiryDate,#shippingDate').datepick({
		onSelect: customRange, showTrigger: '#calImg'});   
	
	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_customer_quotation.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#sales-quotation-product-inline-popup').dialog('destroy');		
					$('#sales-quotation-product-inline-popup').remove(); 
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
					$('#baseprice-popup-dialog').dialog('destroy');		
					$('#baseprice-popup-dialog').remove(); 
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 }); 
	 
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_quotation_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrowscharges').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabcharges>.rowidcharges:last").attr('id'))); 
		  id=id+1;  
		  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_quotation_charges_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabcharges tr:last').before(result);
					 if($(".tabcharges").height()>255)
						 $(".tabcharges").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidcharges').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#chargesLineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('#quotation_save').click(function(){
		 quotationDetails = "";
		 chargesDetails = "";
		 if($jquery("#customerQuotationValidation").validationEngine('validate')){
 		 		
	 			var customerQuotationId = Number($('#customerQuotationId').val()); 
	 			var referenceNumber = $('#tempreferenceNumber').val();
	 			var quotationDate = $('#quotationDate').val(); 
	 			var expiryDate = $('#expiryDate').val();  
	 			var shippingDate = $('#shippingDate').val();  
	 			var shippingMethod = Number($('#shippingMethod').val());  
	 			var shippingTerm = Number($('#shippingTerm').val());  
 	 			var customerId = Number($('#customerId').val());  
	 			var personId = Number($('#personId').val()); 
	 			var paymentMode =  Number($('#paymentMode').val()); 
	 			var status = Number($('#status').val()); 
	 			var description=$('#description').val();  
	 			var creditTerm = Number($('#creditTerm').val());
	 			quotationDetails = getQuotationDetails();   
	   			if(quotationDetails!=null && quotationDetails!=""){
	   				chargesDetails = getQuotationCharges();  
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_customer_quotation.action", 
	 				 	async: false, 
	 				 	data:{	customerQuotationId: customerQuotationId, referenceNumber: referenceNumber, quotationDate: quotationDate, 
		 				 	 	expiryDate: expiryDate, shippingDate: shippingDate, shippingMethod: shippingMethod, shippingTerm : shippingTerm,
		 				 	 	status: status, customerId: customerId, paymentMode: paymentMode,
	 				 			personId: personId, description: description, chargesDetails: chargesDetails, quotationDetails: quotationDetails,
	 				 			creditTermId:creditTerm
	 					 	 },
	 				    dataType: "html",
	 				    cache: false,
	 					success:function(result){   
	 						 $(".tempresult").html(result);
	 						 var message=$.trim($('.tempresult').html());  
	 						 if(message=="SUCCESS"){
	 							 $.ajax({
	 									type:"POST",
	 									url:"<%=request.getContextPath()%>/show_customer_quotation.action", 
	 								 	async: false,
	 								    dataType: "html",
	 								    cache: false,
	 									success:function(result){
	 										$('#sales-quotation-product-inline-popup').dialog('destroy');		
	 										$('#sales-quotation-product-inline-popup').remove(); 
	 										$('#common-popup').dialog('destroy');		
	 										$('#common-popup').remove();  
	 										$('#baseprice-popup-dialog').dialog('destroy');		
	 										$('#baseprice-popup-dialog').remove(); 
	 										$("#main-wrapper").html(result); 
	 										if(customerQuotationId==0)
	 											$('#success_message').hide().html("Record created.").slideDown(1000);
	 										else
	 											$('#success_message').hide().html("Record updated.").slideDown(1000);
	 										$('#success_message').delay(2000).slideUp();
	 									}
	 							 });
	 						 } 
	 						 else{
	 							 $('#page-error').hide().html(message).slideDown(1000);
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					},
	 					error:function(result){  
	 						$('#page-error').hide().html("Internal error.").slideDown(1000);
	 						$('#page-error').delay(2000).slideUp();
	 					}
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter Quotation details.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	});

	 var getQuotationDetails = function(){ 
		 var productArray = new Array();
		 var quantityArray = new Array();
	 	 var amountArray = new Array(); 
	 	 var modeArray = new Array(); 
	 	 var discountArray = new Array(); 
	 	 var detailIdArray = new Array(); 
	 	 var storeArray = new Array(); 
	 	 var productPackageArray = new Array();
	 	 var packageUnitArray = new Array();
 	 	 var quotationDetail = ""; 
 		 $('.rowid').each(function(){  
 			 var rowId = getRowId($(this).attr('id'));  
 			 var productId = $('#productid_'+rowId).val();  
 			 var storeId = Number($('#storeid_'+rowId).val()); 
 			 var quantity = $('#productQty_'+rowId).val();  
  			 var amount = $jquery('#amount_'+rowId).val();    
 			 var amountMode=$('#amountmode_'+rowId).attr('checked');
 			 var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 			 var productPackageDetailId = Number($('#packageType_'+rowId).val());
 			 var packageUnit = Number($('#packageUnit_'+rowId).val());
 			 var discount=Number($('#discount_'+rowId).val());
 			 var customerQuotationDetailId = Number($('#customerQuotationDetailId_'+rowId).val());
 			
 			 if(typeof productId != 'undefined' && productId!=null && 
 					productId!="" && quantity!='' && amount!=''){
 				productArray.push(productId); 
 				storeArray.push(storeId);
 				quantityArray.push(quantity);
 				amountArray.push(amount);
  				if(amountMode == false && pecentageMode==false)
  					modeArray.push("##");
 				else if(amountMode == true)
 					modeArray.push('A'); 
 				else if(pecentageMode == true)
 					modeArray.push('P'); 
  				discountArray.push(discount); 
  				productPackageArray.push(productPackageDetailId);
  				packageUnitArray.push(packageUnit);
  				detailIdArray.push(customerQuotationDetailId);  
 			 } 
 		});
 		 
  		for(var j=0;j<productArray.length;j++){ 
  			quotationDetail += productArray[j]+"__"+storeArray[j]+"__"+quantityArray[j]+"__"+amountArray[j]+"__"+
  								modeArray[j]+"__"+discountArray[j]+"__"+productPackageArray[j]+"__"+packageUnitArray[j]+"__"+detailIdArray[j];
 			if(j==productArray.length-1){   
 			} 
 			else{
 				quotationDetail += "@#";
 			}
 		} 
  		 
 		return quotationDetail; 
 	 }; 

 	 var getQuotationCharges = function(){ 
		 var chargesTypeArray = new Array(); 
	 	 var chargesArray = new Array(); 
	 	 var descriptionArray = new Array(); 
 	 	 var chargesIdArray = new Array(); 
 	 	 var chargesDetail = ""; 
 		 $('.rowidcharges').each(function(){  
 			 var rowId = getRowId($(this).attr('id'));  
 			 var chargesTypeId = $('#chargesTypeId_'+rowId).val();   
 			 var charges = Number($('#charges_'+rowId).val());  
 			 var description = $('#description_'+rowId).val();     
 			 var customerQuotationChargesId = Number($('#customerQuotationChargesId_'+rowId).val());
 			
 			 if(typeof chargesTypeId != 'undefined' && chargesTypeId!=null && 
 					chargesTypeId!="" && charges>0){
 				chargesTypeArray.push(chargesTypeId);  
 				chargesArray.push(charges);  
  				if(description ==null || description=="")
  					descriptionArray.push("##");
 				else 
 					descriptionArray.push(description); 
  				chargesIdArray.push(customerQuotationChargesId);  
 			 }   
 		});
  		for(var j=0;j<chargesTypeArray.length;j++){ 
  			chargesDetail += chargesTypeArray[j]+"__"+chargesArray[j]+"__"+descriptionArray[j]+"__"+
  							chargesIdArray[j];
 			if(j==chargesTypeArray.length-1){   
 			} 
 			else{
 				chargesDetail += "@#";
 			}
 		} 
  		 
 		return chargesDetail; 
 	 };

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.delrowcharges').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidcharges').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#chargesLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	  $('#customer-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

	  $('#product-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('#price-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('.common_person_list').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data: {  personTypes: "1"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				} 
			});  
			return false;
		});

	  $('.show_customer_list').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
			 	async: false,  
			 	data: { pageInfo: "customer_quotation"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				} 
			});  
			return false;
		});
	  
	  {
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_sales_product_common_popup_preload.action", 
			 	async: false,  
			 	data: {itemType: "I"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					$('#sales-quotation-product-inline-popup').html(result);  
				} 
			});
		}
	  
	  $('.show_product_list_cstquotation').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			globalRowId = Number($(this).attr('id').split("_")[1]);  
			$('#sales-quotation-product-inline-popup').dialog('open'); 
			return false;
		});
 
	  $('#sales-quotation-product-inline-popup').dialog({
			autoOpen : false,
			width : 800,
			height : 600,
			bgiframe : true,
			modal : true,
			buttons : {
				"Close" : function() {
					$(this).dialog("close");
				}
			}
		});

	  $('.show_store_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			tableRowId = Number($(this).attr('id').split("_")[1]); 
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
			 	async: false,  
			 	data: { rowId: tableRowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0); 
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	  $('.show_productpricing_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			tableRowId = Number($(this).attr('id').split("_")[1]);  
			var productId = Number($('#productid_'+tableRowId).val());
			var storeId = Number($('#storeid_'+tableRowId).val()); 
			var customerId = Number($('#customerId').val()); 
			if(productId > 0){
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_common_product_pricing.action", 
				 	async: false,  
				 	data: {productId: productId, storeId: storeId, rowId: tableRowId, customerId: customerId},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						 $('.common-result').html(result);  
						 $('#common-popup').dialog('open'); 
						 $( $(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
					} 
				}); 
			} 
			return false;
		});
 

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.shippingmethod-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id");  
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             } 
	         });
	   return false;
	});	

	 $('.shippingterm-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             }
	         });
	   return false;
	});	

	 $('.chargestype-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 var str = $(this).attr("id");
			 accessCode = str.substring(0, str.lastIndexOf("_"));
			 sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
		    $.ajax({
		         type:"POST",
		         url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		         data:{accessCode: accessCode},
		         async: false,
		         dataType: "html",
		         cache: false,
		         success:function(result){ 
		              $('.common-result').html(result);
		              $('#common-popup').dialog('open');
						   $($($('#common-popup').parent()).get(0)).css('top',0);
		              return false;
		         } 
		     });
		 return false;
		});	
		
	//Lookup Data Roload call
	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SHIPPING_TERMS"){
			$('#shippingTerm').html("");
			$('#shippingTerm').append("<option value=''>Select</option>");
			loadLookupList("shippingTerm"); 
		} else if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingMethod').html("");
			$('#shippingMethod').append("<option value=''>Select</option>");
			loadLookupList("shippingMethod"); 
		}  else if(accessCode=="CHARGES_TYPE"){
			$('#chargesTypeId_'+sectionRowId).html("");
			$('#chargesTypeId_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("chargesTypeId_"+sectionRowId); 
		}  
	});

	$('.baseprice-popup').click(function(){ 
		$('.ui-dialog-titlebar').remove(); 
		var productId = Number($('#productid_'+tableRowId).val());
		var storeId = Number($('#storeid_'+tableRowId).val());  
		$('#amount_' + tableRowId).val('');
		$('#productPricingDetailId_' + tableRowId).val('');
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_stock_details.action",
											async : false,
											data : {
												productId : productId,
												storeId : storeId
											},
											dataType : "json",
											cache : false,
											success : function(response) {
												if (null!=response.stockVO &&
														response.stockVO.availableQuantity != null
														&& response.stockVO.availableQuantity != '') {
													$('#productCode')
															.text(
																	response.stockVO.productName);
													$('#unitName')
															.text(
																	response.stockVO.productUnit);
													$('#costingType')
															.text(
																	response.stockVO.costingType);
													$('#storeQuantity')
															.text(
																	response.stockVO.availableQuantity);
													//$('#productQty_' + tableRowId).val(response.stockVO.availableQuantity);
													$jquery('#amount_' + tableRowId)
															.val(
																	response.stockVO.unitRate);

													var amount = Number($jquery(
															'#amount_' + tableRowId)
															.val());
													var productQty = Number($(
															'#productQty_'
																	+ tableRowId)
															.val());
													if (amount > 0
															&& productQty > 0) {
														var discount = Number($(
																'#discount_'
																		+ tableRowId)
																.val());
														if (discount > 0) {
															var totalAmount = Number(amount
																	* productQty);
															var discountAmount = 0;
															var chargesMode = $(
																	'#chargesmodeDetail_'
																			+ tableRowId)
																	.val();
															if (chargesMode == "true") {
																discountAmount = Number(Number(amount
																		* discount) / 100);
																totalAmount -= discountAmount;
															} else {
																totalAmount -= discount;
															}
															
															$jquery('#totalAmountH_'
																			+ tableRowId)
																	.val(totalAmount);
															$('#totalAmount_'
																			+ tableRowId)
																	.text($('#totalAmountH_'
																			+ tableRowId)
																	.val());
														} else{
															$jquery('#totalAmountH_'
																	+ tableRowId)
															.val(Number(amount
																	* productQty));
															$('#totalAmount_'
																	+ tableRowId)
															.text($('#totalAmountH_'
																	+ tableRowId)
															.val());
														}  
													}
													triggerAddRow(tableRowId);
													return false;
												} else {
													$('#line-error')
															.hide()
															.html(
																	"No product available for "
																			+ $(
																					'#product_'
																							+ tableRowId)
																					.text()
																			+ " on store "
																			+ $(
																					'#store_'
																							+ tableRowId)
																					.text())
															.slideDown();
													$('#line-error')
															.delay(2000)
															.slideUp(); 
													triggerAddRow(tableRowId);
													return false;
												}
												return false;
											} 
										}); 
								return false; 
						});

		$('#baseprice-popup-dialog').dialog({
			autoOpen : false,
			width : 400,
			height : 200,
			bgiframe : true,
			modal : true,
			buttons : {
				"Close" : function() {
					$('#baseprice-popup-dialog').dialog("close");
				}
			}
		});
		
		$('.packageType').live('change',function(){
			var rowId=getRowId($(this).attr('id')); 
			$('#productQty_'+rowId).val('');
			$('#baseDisplayQty_'+rowId).html('');
			var packageUnit = Number($('#packageUnit_'+rowId).val());
			var packageType = Number($('#packageType_'+rowId).val());   
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			} 
			triggerAddRow(rowId);
			return false;
		});
		
		$('.packageUnit').live('change',function(){
			var rowId=getRowId($(this).attr('id'));
			$('#baseUnitConversion_'+rowId).text('');
			$('#productQty_'+rowId).val('');
			var packageUnit = Number($(this).val()); 
			var packageType = Number($('#packageType_'+rowId).val()); 
			if(packageType == -1 && packageUnit > 0){
				$('#productQty_'+rowId).val(packageUnit);
				var discount = Number($('#discount_' + rowId).val());
				var amount = Number($jquery('#amount_' + rowId).val());
				if (discount > 0) {
					var totalAmount = Number(amount * packageUnit);
					var discountAmount = 0;
					var amountMode=$('#amountmode_'+rowId).attr('checked');
		 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
					if (pecentageMode == true) {
						discountAmount = Number(Number(totalAmount
								* discount) / 100);
						totalAmount -= discountAmount;
					} else if (amountMode == true) {
						totalAmount -= discount;
					}
					$jquery('#totalAmountH_' + rowId).val(totalAmount);
					$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
				} else{
					$jquery('#totalAmountH_' + rowId).val(amount * packageUnit);
					$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
				} 	
			}
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			}
			triggerAddRow(rowId);
 			return false;
		}); 

		$('.productQty').live(
				'change',
				function() {
					var rowId = getRowId($(this).attr('id'));
					var amount = Number($jquery('#amount_' + rowId).val());
					var productQty = Number($('#productQty_' + rowId).val());
					var packageType = Number($('#packageType_'+rowId).val());   
					if (amount > 0 && productQty > 0) {
						var discount = Number($('#discount_' + rowId).val());
						if (discount > 0) {
							var totalAmount = Number(amount * productQty).toFixed(2);
							var discountAmount = 0;
							var amountMode=$('#amountmode_'+rowId).attr('checked');
				 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
							if (pecentageMode == true) {
								discountAmount = Number(Number(totalAmount
										* discount) / 100);
								totalAmount -= discountAmount;
							} else if (amountMode == true) {
								totalAmount -= discount;
							}
							$jquery('#totalAmountH_' + rowId).val(totalAmount);
							$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
						} else{
							$jquery('#totalAmountH_' + rowId).val(amount * productQty);
							$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
						}  
						if(productQty > 0 && packageType > 0){ 
							getProductConversionUnit(rowId);
						}else{
							$('#packageUnit_'+rowId).val(productQty);
						}
					} 
					triggerAddRow(rowId);
					return false;
				});

		$('.amount').live(
				'change',
				function() {
					var rowId = getRowId($(this).attr('id'));
					var amount = $jquery('#amount_' + rowId).val();
					var productQty = Number($('#packageUnit_' + rowId).val());
					if (amount > 0 && productQty > 0) {
						$('#baseSellingPrice_' + rowId).val('');
						var discount = Number($('#discount_' + rowId).val());
						if (discount > 0) {
							var totalAmount = Number(amount * productQty);
							var discountAmount = 0;
							var amountMode=$('#amountmode_'+rowId).attr('checked');
				 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
							if (pecentageMode == true) {
								discountAmount = Number(Number(totalAmount
										* discount) / 100);
								totalAmount -= discountAmount;
							} else if (amountMode == true) {
								totalAmount -= discount;
							}
							$jquery('#totalAmountH_' + rowId).val(totalAmount);
							$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
						} else{
							$jquery('#totalAmountH_' + rowId).val(amount * productQty);
							$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
						}  
					}
					triggerAddRow(rowId);
					return false;
				});

		$('.chargestypeid').live('change', function() {
			triggerChargesAddRow(getRowId($(this).attr('id')));
			return false;
		}); 

		$('.charges').live('change', function() {
			triggerChargesAddRow(getRowId($(this).attr('id')));
			return false;
		});

		$('.chargesmodeDetail')
				.live(
						'change',
						function() {
							var rowId = getRowId($(this).attr('id'));
							var amount = Number($jquery('#amount_' + rowId).val());
							var discount = Number($('#discount_' + rowId).val());
							var amountMode=$('#amountmode_'+rowId).attr('checked');
				 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
							var productQty = Number($('#packageUnit_' + rowId)
									.val());
							if (amount > 0
									&& discount > 0) {
								var totalAmount = Number(amount * productQty);
								var discountAmount = 0; 
								if (pecentageMode == true) {
									discountAmount = Number(Number(totalAmount
											* discount) / 100);
									totalAmount -= discountAmount;
								} else if (amountMode == true) {
									totalAmount -= discount;
								}
								$jquery('#totalAmountH_' + rowId).val(totalAmount);
								$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
							} else {
								$jquery('#totalAmountH_' + rowId).val(amount * productQty);
								$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
							}
						});

		$('.discount')
				.live(
						'change',
						function() {
							var rowId = getRowId($(this).attr('id'));
							var amount = Number($jquery('#amount_' + rowId).val());
							var discount = Number($('#discount_' + rowId).val());
							var amountMode=$('#amountmode_'+rowId).attr('checked');
				 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
				 			
							var productQty = Number($('#packageUnit_' + rowId)
									.val());
							
							if (amount > 0
									&& discount > 0) { 
								var totalAmount = Number(amount * productQty);
								var discountAmount = 0; 
								if (pecentageMode == true) {
									discountAmount = Number(Number(totalAmount
											* discount) / 100);
									totalAmount -= discountAmount;
								} else if (amountMode == true) {
									totalAmount -= discount;
								}
								$jquery('#totalAmountH_' + rowId).val(totalAmount);
								$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
							} else {
								$jquery('#totalAmountH_' + rowId).val(amount * productQty);
								$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
							}
						});

		if (Number($('#customerQuotationId').val()) > 0) {
			$('#shippingMethod').val($('#tempShippingMethod').val());
			$('#shippingTerm').val($('#tempShippingTerm').val());
			$('#status').val($('#tempStatus').val());
			$('#paymentMode').val($('#tempPaymentMode').val());
			$('#creditTerm').val($('#termpCreditTermId').val()); 
			{
				$('.rowid')
						.each(
								function() {
									var rowId = getRowId($(this).attr('id'));
									$('#packageType_' + rowId)
									.val(
											$(
													'#temppackageType_'
															+ rowId)
													.val());
									$('#totalAmount_' + rowId)
											.text(
													$(
															'#totalAmountH_'
																	+ rowId)
															.val());
									$('#chargesmodeDetail_' + rowId)
									.val(
											$(
													'#tempChargesModeDetail_'
															+ rowId)
													.val());
								});
			}
			{
				$('.rowidcharges').each(
						function() {
							var rowId = getRowId($(this).attr('id'));
							$('#chargesTypeId_' + rowId).val(
									$('#tempChargesTypeId_' + rowId).val());
							$('#chargesMode_' + rowId).val(
									$('#tempChargesMode_' + rowId).val());
						});
			}
		}else
			$('#status').val(7);
	});
	
	function getProductBaseUnit(rowId){
		$('#baseUnitConversion_'+rowId).text('');
		var packageQuantity = Number($('#packageUnit_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		var basePrice = Number($('#baseSellingPrice_' + rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice,  productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#productQty_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName); 
				if(response.productPackagingDetailVO.basePrice != null && response.productPackagingDetailVO.basePrice > 0){
					$jquery('#amount_' + rowId).val(response.productPackagingDetailVO.basePrice);
				} 
 				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
			} 
		});  
  		var amount = Number($jquery('#amount_' + rowId).val());
		var discount = Number($('#discount_' + rowId).val());
 		if (discount > 0) {
			var totalAmount = Number(amount * packageQuantity);
			var discountAmount = 0;
			var amountMode=$('#amountmode_'+rowId).attr('checked');
 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
			if (pecentageMode == true) {
				discountAmount = Number(Number(totalAmount
						* discount) / 100);
				totalAmount -= discountAmount;
			} else if (amountMode == true) {
				totalAmount -= discount;
			}
			$jquery('#totalAmountH_' + rowId).val(totalAmount);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val());  
		} else{
			$jquery('#totalAmountH_' + rowId).val(amount * packageQuantity);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val());  
		} 
		return false;
	}
	
	function getProductConversionUnit(rowId){
 		var packageQuantity = Number($('#productQty_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
 			} 
		});   
		return false;
	}
	
	function triggerPriceCalc(rowId) {
		var amount = Number($jquery('#amount_' + rowId).val());
		var discount = Number($('#discount_' + rowId).val());
		var chargesmodeDetail = $('#chargesmodeDetail_' + rowId).val();
		var productQty = Number($('#productQty_' + rowId).val());
		if (chargesmodeDetail != '' && amount > 0 && discount > 0) {
			var totalAmount = Number(amount * productQty);
			var discountAmount = 0;
			var chargesMode = $('#chargesmodeDetail_' + rowId).val();
			if (chargesMode == "true") {
				discountAmount = Number(Number(amount * discount) / 100);
				totalAmount -= discountAmount;
			} else {
				totalAmount -= discount;
			}
			$jquery('#totalAmountH_' + rowId).val(totalAmount);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
		} else {
			$jquery('#totalAmountH_' + rowId).val(amount * productQty);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
		}
		triggerAddRow(rowId);
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		var tdSize = 0;
		var actualSize = 0;
		{
			hiddenSize = 0;
			$($(".tab>tr:first").children()).each(function() {
				if ($(this).is(':hidden')) {
					++hiddenSize;
				}
			});
			tdSize = $($(".tab>tr:first").children()).size();
			actualSize = Number(tdSize - hiddenSize);

			$('.tab>tr:last').removeAttr('id');
			$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
			$($('.tab>tr:last').children()).remove();
			for ( var i = 0; i < actualSize; i++) {
				$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
			}
		}
		{
			hiddenSize = 0;
			tdSize = 0;
			actualSize = 0;
			$($(".tabcharges>tr:first").children()).each(function() {
				if ($(this).is(':hidden')) {
					++hiddenSize;
				}
			});
			tdSize = $($(".tabcharges>tr:first").children()).size();
			actualSize = Number(tdSize - hiddenSize);

			$('.tabcharges>tr:last').removeAttr('id');
			$('.tabcharges>tr:last').removeClass('rowidcharges').addClass(
					'lastrow');
			$($('.tabcharges>tr:last').children()).remove();
			for ( var i = 0; i < actualSize; i++) {
				$('.tabcharges>tr:last').append(
						"<td style=\"height:25px;\"></td>");
			}
		}
	}

	function customRange(dates) {
		if (this.id == 'quotationDate') {
			$('#expiryDate').datepick('option', 'minDate', dates[0] || null);
			if ($('#quotationDate').val() != "") {
				addPeriods();
			}
		} else {
			$('#quotationDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
	function addPeriods() {
		var date = new Date($('#quotationDate').datepick('getDate')[0].getTime());
		$.datepick.add(date, parseInt(90, 10), 'd');
		$('#expiryDate').val($.datepick.formatDate(date));
	}
	
	function triggerAddRow(rowId) { 
		var productid = Number($('#productid_' + rowId).val());
		var productQty = Number($('#productQty_' + rowId).val());
		var amount = Number($('#amount_' + rowId).val()); 
		var nexttab = $('#fieldrow_' + rowId).next();
		if (productid > 0 && productQty > 0
				&& amount > 0 
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function triggerChargesAddRow(rowId) {  
		var chargesTypeId = $('#chargesTypeId_' + rowId).val(); 
		var charges = $('#charges_' + rowId).val();
		var nexttab = $('#fieldrowcharges_' + rowId).next();
		if (chargesTypeId != null && chargesTypeId != "" && charges != null && charges != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImagecharges_' + rowId).show();
			$('.addrowscharges').trigger('click');
		}
	}

	function personPopupResult(personId, personName, commonParam) {
		$('#salesPerson').val(personName);
		$('#personId').val(personId);
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName);
 	}

	function commonProductPopup(rowId, aData) {
		if(rowId==null || row==0)
			rowId=globalRowId;
		
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.code +" - "+aData.productName); 
		$('#unitCode_' + rowId).html(aData.unitCode); 
		$('#productPricingDetailId_' + rowId).val(aData.productPricingCalcId); 
		$('#basePrice_' + rowId).val(aData.basePrice);   
		$('#standardPrice_' + rowId).val(aData.suggestedPrice); 
		var customerId = Number($('#customerId').val());
		
		{
			getProductPackagings("packageType",aData.productId, rowId);
		}
		
		{
			if(customerId > 0){
				checkCustomerSalesPrice(aData.productId, customerId, aData.sellingPrice, rowId);
			}else{
				$('#amount_' + rowId).val(aData.sellingPrice); 
				$('#baseSellingPrice_' + rowId).val(aData.sellingPrice); 
			} 
			$('#sales-quotation-product-inline-popup').dialog("close");
			triggerAddRow(rowId);
		} 
		return false;
	}
	
	function getProductPackagings(idName, productId, rowId){
		$('#'+idName+'_'+rowId).html('');
		$('#'+idName+'_'+rowId)
		.append(
				'<option value="-1">Select</option>');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.productPackageVOs != null){ 
					$.each(response.productPackageVOs, function (index) {
						if(response.productPackageVOs[index].productPackageId == -1){ 
							$('#'+idName+'_'+rowId)
							.append('<option value='
									+ response.productPackageVOs[index].productPackageId
									+ '>' 
									+ response.productPackageVOs[index].packageName
									+ '</option>');
						}else{ 
							var optgroup = $('<optgroup>');
				            optgroup.attr('label',response.productPackageVOs[index].packageName);
				            optgroup.css('color', '#c85f1f'); 
				             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
				                var option = $("<option></option>");
				                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
				                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
				                option.css('color', '#000'); 
				                option.css('margin-left', '10px'); 
				                optgroup.append(option);
				             });
				             $('#'+idName+'_'+rowId).append(optgroup);
						}  
					}); 
					$('#'+idName+'_'+rowId).multiselect('refresh'); 
 				} 
			} 
		});  
		return false;
	}
	
	function checkCustomerSalesPrice(productId, customerId, finalUnitPrice, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcustomer_salesprice.action",
				data : {
					productId : productId, customerId: customerId
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) { 
					if(response.sellingPrice != null && response.sellingPrice != 0){
						$jquery('#amount_' + rowId).val(response.sellingPrice); 
						$('#baseSellingPrice_' + rowId).val(response.sellingPrice); 
					}else{
						$jquery('#amount_' + rowId).val(finalUnitPrice); 
						$('#baseSellingPrice_' + rowId).val(finalUnitPrice); 
					}
				}
			});
	}

	function commonStorePopup(storeId, storeName, rowId) { 
		$('#storeid_' + rowId).val(storeId);
		$('#store_' + rowId).html(storeName);
		$('.baseprice-popup').trigger('click');
		return false;
	}

	function commonProductPricePopup(productPricingCalcId, calculationTypeCode, 
				calculationSubTypeCode, salesAmount, rowId) {  
		$('#amount_' + rowId).val(salesAmount);
		$('#baseSellingPrice_' + rowId).val(salesAmount); 
 		triggerPriceCalc(rowId);
		return false;
 	}
 	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Quotation
		</div> 
		<form name="customerQuotationValidation"
			id="customerQuotationValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="customerQuotationId"
					name="customerQuotationId"
					value="${CUSTOMER_QUOTATION.customerQuotationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="height: 160px;">
							<div>
								<label class="width30">Sales Person</label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.person ne null && CUSTOMER_QUOTATION.person ne ''}">
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50"
											value="${CUSTOMER_QUOTATION.person.firstName } ${CUSTOMER_QUOTATION.person.lastName}" />
										<input type="hidden" readonly="readonly" name="personId" id="personId"
												value="${CUSTOMER_QUOTATION.person.personId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50" value="${requestScope.personName}"/>
										<input type="hidden" readonly="readonly" name="personId" id="personId"
												value="${requestScope.personId}" />
									</c:otherwise>
								</c:choose>
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common_person_list width100">
										<span class="ui-icon ui-icon-newwin"></span> </a> </span> 
							</div> 
							<div>
								<label class="width30">Credit Term</label> <select
									name="creditTerm" id="creditTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${CREDIT_TERM}">
										<option value="${TERMS.creditTermId}">${TERMS.name}</option>
									</c:forEach>
								</select> <input type="hidden" id="termpCreditTermId"
									name="termpCreditTermId"
									value="${CUSTOMER_QUOTATION.creditTerm.creditTermId}" />
							</div>
							<div>
								<label class="width30"> Payment Mode</label> <select
									name="paymentMode" id="paymentMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="MODE" items="${PAYMENT_MODE}">
										<option value="${MODE.key}">${MODE.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempPaymentMode"
									value="${CUSTOMER_QUOTATION.modeOfPayment}" />
							</div> 
							<div>
								<label class="width30"> Shipping Method</label> <select
									name="shippingMethod" id="shippingMethod" class="width51">
									<option value="">Select</option>
									<c:forEach var="METHOD" items="${SHIPPING_METHOD}">
										<option value="${METHOD.lookupDetailId}">${METHOD.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingMethod"
									value="${CUSTOMER_QUOTATION.lookupDetailByShippingMethod.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SHIPPING_SOURCE"
											class="btn ui-state-default ui-corner-all shippingmethod-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"> Shipping Terms</label> <select
									name="shippingTerm" id="shippingTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${SHIPPING_TERMS}">
										<option value="${TERMS.lookupDetailId}">${TERMS.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingTerm"
									value="${CUSTOMER_QUOTATION.lookupDetailByShippingTerm.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SHIPPING_TERMS"
											class="btn ui-state-default ui-corner-all shippingterm-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${CUSTOMER_QUOTATION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="height: 160px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${CUSTOMER_QUOTATION.referenceNo ne null && CUSTOMER_QUOTATION.referenceNo ne ''}">
												${CUSTOMER_QUOTATION.referenceNo} 
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${CUSTOMER_QUOTATION.referenceNo}" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Quotation Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.date ne null && CUSTOMER_QUOTATION.date ne ''}">
										<c:set var="quotationDate" value="${CUSTOMER_QUOTATION.date}" />
										<input name="quotationDate" type="text" readonly="readonly"
											id="quotationDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("quotationDate").toString())%>"
											class="quotationDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="quotationDate" type="text" readonly="readonly"
											id="quotationDate"
											class="quotationDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Expiry Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.expiryDate ne null && CUSTOMER_QUOTATION.expiryDate ne ''}">
										<c:set var="expiryDate"
											value="${CUSTOMER_QUOTATION.expiryDate}" />
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("expiryDate").toString())%>"
											class="expiryDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate" class="expiryDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Status<span
									style="color: red;">*</span></label> <select name="status"
									id="status" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${QUOTATION_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus"
									value="${CUSTOMER_QUOTATION.status}" />
							</div>
							<div>
								<label class="width30">Customer</label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.customer ne null && CUSTOMER_QUOTATION.customer ne ''}">
										<c:set var="customerName" />
										<c:choose>
											<c:when
												test="${CUSTOMER_QUOTATION.customer.personByPersonId ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.personByPersonId.firstName} ${CUSTOMER_QUOTATION.customer.personByPersonId.lastName}" />
											</c:when>
											<c:when test="${CUSTOMER_QUOTATION.customer.company ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.company.companyName}" />
											</c:when>
											<c:when
												test="${CUSTOMER_QUOTATION.customer.cmpDeptLocation ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.cmpDeptLocation.company.companyName}" />
											</c:when>
										</c:choose>
										<input type="text" name="customerName" id="customerName"
											value="${customerName}" class="width50" />
										<input type="hidden" id="customerId" name="customerId"
											value="${CUSTOMER_QUOTATION.customer.customerId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="customerName"
											id="customerName" class="width50" />
										<input type="hidden" id="customerId" name="customerId" />
									</c:otherwise>
								</c:choose>
								<span class="button"> <a
									style="cursor: pointer;" id="customer"
									class="btn ui-state-default ui-corner-all show_customer_list width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"> Shipping Date </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.shippingDate ne null && CUSTOMER_QUOTATION.shippingDate ne ''}">
										<c:set var="shippingDate"
											value="${CUSTOMER_QUOTATION.shippingDate}" />
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("shippingDate").toString())%>"
											class="shippingDate width50">
									</c:when>
									<c:otherwise>
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											class="shippingDate width50">
									</c:otherwise>
								</c:choose>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="line-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>Quotation Detail<span
									class="mandatory">*</span></legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 6%;">Product</th>
										<th style="width: 1%; display: none;">Store</th>
										<th style="width: 4%;">Packaging</th> 
 										<th style="width: 5%;">Quantity</th>
										<th style="width: 5%;">Base Qty</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 3%;">Discount Mode</th>
										<th style="width: 3%;">Discount</th>
										<th style="width: 3%;">Total</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${CUSTOMER_QUOTATION.customerQuotationDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span class="width60 float-left"
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span>
												<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list_cstquotation width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.store.storeId}" /> <span
												id="store_${status.index+1}">${DETAIL.store.storeName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_store_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
											</td>
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
											</td> 
											<td>
												<input type="hidden" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.baseQuantity}"
												class="productQty width80">  
												 <span id="baseUnitConversion_${status.index+1}" 
												 	class="width10" style="display: none;">${DETAIL.baseUnitName}</span>
												 <span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}" style="text-align: right;"
												class="amount width60 right-align"> 
												<input type="hidden" id="baseSellingPrice_${status.index+1}" value="${DETAIL.unitRate}"/>
												<span class="button float-right"> <a
													style="cursor: pointer; position: relative; top: 7px;"
													id="pricingDetail_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_productpricing_list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
 											</td>
											<td><input type="hidden" name="tempChargesModeDetail"
												id="tempChargesModeDetail_${status.index+1}"
												value="${DETAIL.isPercentage}" /> <c:set var="isPercentage"
													value="${DETAIL.isPercentage}" />  
											<c:choose>
												<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq true}">
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}" checked="checked"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" style="width:5px;"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:when>
												<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq false}">
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" value="false" style="width:5px;" checked="checked"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:when>
												<c:otherwise>
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}" value="true"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" value="false" style="width:5px;"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:otherwise> 
											</c:choose> 
											</td>
											<td><input type="text" name="discount"
												id="discount_${status.index+1}" value="${DETAIL.discount}"
												style="text-align: right;"
												class="discount width90 right-align"> <c:set
													var="discount" value="${DETAIL.discount}" />
											</td>
											<td><c:set var="totalAmount"
													value="${DETAIL.packageUnit * DETAIL.unitRate}" /> <%
									 		double discount = 0;
									 		double totalAmount = Double.parseDouble(pageContext
									 				.getAttribute("totalAmount").toString());
									 		if (null != pageContext.getAttribute("isPercentage")) {
									 			if (("true").equals(pageContext
									 					.getAttribute("isPercentage").toString())) {
									 				discount = Double.parseDouble(pageContext.getAttribute(
									 						"discount").toString());
									 				discount = (totalAmount * discount) / 100;
									 				totalAmount -= discount;
									 			} else {
									 				discount = Double.parseDouble(pageContext.getAttribute(
									 						"discount").toString());
									 				totalAmount -= discount;
									 			}
									 		}%> 
									 		<span id="totalAmount_${status.index+1}" style="float: right;"></span>
									 		<input type="text" style="display: none" id="totalAmountH_${status.index+1}" value="<%=totalAmount%>"
									 			class="totalAmountH"/>
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="customerQuotationDetailId"
												id="customerQuotationDetailId_${status.index+1}"
												value="${DETAIL.customerQuotationDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(CUSTOMER_QUOTATION.customerQuotationDetailVOs)+1}"
										end="${fn:length(CUSTOMER_QUOTATION.customerQuotationDetailVOs)+2}"
										step="1" varStatus="status">
										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}" class="width60 float-left"></span> 
												 <span class="width10 float-right" id="unitCode_${i}"
												 	 style="position: relative;"></span>
												 <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show_product_list_cstquotation width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
											</td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${i}" /> <span id="store_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="storeID_${i}"
													class="btn ui-state-default ui-corner-all show_store_list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${i}" class="packageType">
													<option value="">Select</option>
												</select> 
											</td> 
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${i}"  />
	 										</td>
											<td><input type="hidden" name="productQty"
												id="productQty_${i}" class="productQty validate[optional,custom[number]] width80"> <span
												class="button float-right"> <a
													style="cursor: pointer; position: relative; top: 7px; display: none;"
													id="showBasePrice_${i}"
													class="btn ui-state-default ui-corner-all baseprice-popup width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
												<span id="baseUnitConversion_${i}" class="width10" style="display: none;"></span>
												 <span id="baseDisplayQty_${i}"></span>
											</td>
											<td><input type="text" name="amount" id="amount_${i}" style="text-align: right;"
												class="amount validate[optional,custom[number]] width60 right-align">  <span
												class="button float-right"> <a
													style="cursor: pointer; position: relative; top: 7px;"
													id="pricingDetail_${i}"
													class="btn ui-state-default ui-corner-all show_productpricing_list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
												<input type="hidden" id="baseSellingPrice_${i}"/>
											</td>
											<td>
												<div class="float-left">
												<input type="radio" name="chargesmodeDetail_${i}" class="chargesmodeDetail float-left" style="width:5px;"
													 id="pecentagemode_${i}" value="true"/>
												<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
												<div class="float-left">
												<input type="radio" name="chargesmodeDetail_${i}" class="chargesmodeDetail float-left" 
													id="amountmode_${i}" value="false" style="width:5px;"/>
												<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
											</td>
											<td><input type="text" name="discount"
												id="discount_${i}" style="text-align: right;"
												class="discount width90 validate[optional,custom[number]] right-align">
											</td>
											<td>
												<span id="totalAmount_${i}" style="float: right;"></span>
												<input type="text" style="display: none" id="totalAmountH_${i}" class="totalAmountH"/>
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="customerQuotationDetailId"
												id="customerQuotationDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90" id="hrm"
				style="margin-top: 5px;">
				<fieldset>
					<legend>Quotation Charges</legend>
					<div id="hrm" class="hastable width100">
						<table id="hastab_charges" class="width100">
							<thead>
								<tr>
									<th style="width: 1%">L.NO</th>
									<th class="width10">Charges Type</th>
									<th style="width: 5%; display: none;">Mode</th>
									<th style="width: 5%;">Charges</th>
									<th class="width10">Description</th>
									<th style="width: 1%;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tabcharges">
								<c:forEach var="CHARGES"
									items="${CUSTOMER_QUOTATION.customerQuotationCharges}"
									varStatus="status">
									<tr class="rowidcharges" id="fieldrowcharges_${status.index+1}">
										<td id="chargesLineId_${status.index+1}">${status.index+1}</td>
										<td><input type="hidden" name="tempchargesTypeId"
											id="tempChargesTypeId_${status.index+1}"
											value="${CHARGES.lookupDetail.lookupDetailId}" /> <select
											name="chargesTypeId" id="chargesTypeId_${status.index+1}"
											class="chargestypeid width70">
												<option value="">Select</option>
												<c:forEach var="TYPE" items="${CHARGES_TYPE}">
													<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
												</c:forEach>
										</select>
										<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="CHARGES_TYPE_${status.index+1}"
											class="btn ui-state-default ui-corner-all chargestype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										</td>
										<td style="display: none;"><input type="hidden" name="tempChargesMode"
											id="tempChargesMode_${status.index+1}"
											value="${CHARGES.isPercentage}" /> <select
											name="chargesMode" id="chargesMode_${status.index+1}"
											class="chargesmode width90">
												<option value="">Select</option>
												<option value="true">Percentage</option>
												<option value="false">Amount</option>
										</select>
										</td>
										<td><input type="text" name="charges"
											id="charges_${status.index+1}" value="${CHARGES.charges}"
											class="charges validate[optional,custom[number]] width98" style="text-align: right;">
										</td>
										<td><input type="text" name="description"
											id="description_${status.index+1}"
											value="${CHARGES.description}" class="description width98">
										</td>
										<td style="width: 1%;" class="opn_td"
											id="optioncharges_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
											id="AddImagecharges_${status.index+1}"
											style="cursor: pointer; display: none;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
											id="EditImagecharges_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
											id="DeleteImagecharges_${status.index+1}"
											style="cursor: pointer;" title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImagecharges_${status.index+1}"
											style="display: none;" title="Working"> <span
												class="processing"></span> </a> <input type="hidden"
											name="customerQuotationChargesId"
											id="customerQuotationChargesId_${status.index+1}"
											value="${CHARGES.customerQuotationChargeId}" />
										</td>
									</tr>
								</c:forEach>
								<c:forEach var="i"
									begin="${fn:length(CUSTOMER_QUOTATION.customerQuotationCharges)+1}"
									end="${fn:length(CUSTOMER_QUOTATION.customerQuotationCharges)+2}"
									step="1" varStatus="status">

									<tr class="rowidcharges" id="fieldrowcharges_${i}">
										<td id="chargesLineId_${i}">${i}</td>
										<td><select name="chargesTypeId" id="chargesTypeId_${i}"
											class="chargestypeid width70">
												<option value="">Select</option>
												<c:forEach var="TYPE" items="${CHARGES_TYPE}">
													<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
												</c:forEach>
										</select>
										<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="CHARGES_TYPE_${i}"
											class="btn ui-state-default ui-corner-all chargestype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										</td>
										<td style="display: none;"><select name="chargesMode" id="chargesMode_${i}"
											class="chargesmode width90">
												<option value="">Select</option>
												<option value="true">Percentage</option>
												<option value="false">Amount</option>
										</select> 
										</td>
										<td><input type="text" name="charges" id="charges_${i}"
											class="charges validate[optional,custom[number]] width98" style="text-align: right;">
										</td>
										<td><input type="text" name="description"
											id="description_${i}" class="description width98">
										</td>
										<td style="width: 1%;" class="opn_td" id="optioncharges_${i}">
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
											id="AddImagecharges_${i}"
											style="cursor: pointer; display: none;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
											id="EditImagecharges_${i}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
											id="DeleteImagecharges_${i}"
											style="cursor: pointer; display: none;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImagecharges_${i}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" name="customerQuotationChargesId"
											id="customerQuotationChargesId_${i}" />
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div> 
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="quotation_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
				<div
					class="portlet-header ui-widget-header float-left addrowscharges"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="sales-quotation-product-inline-popup" style="display: none;"
					class="width100 float-left" id="hrm">
					
			</div>
			<div class="clearfix"></div>
			<div id="baseprice-popup-dialog" style="display: none;">
				<div class="width100 float-left" id="hrm" style="padding: 5px;">
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Product</label> <span id="productCode"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Unit Name</label> <span id="unitName"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Costing Type</label> <span id="costingType"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Store Quantity</label> <span
							id="storeQuantity"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Base Price</label> <span id="basePrice"></span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>