<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var tempid = "";
var deliveryDetails = "";
$(function(){    
	$jquery("#invoiceContinuousValidation").validationEngine('attach'); 
	$jquery('.invoiceAmount, .paidInvoice, .totalInvoice').number( true, 3 );
	$('#invoiceDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	
	$('.supplierinvoice-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,   
		 	data:{pageInfo : "invoice"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0); 
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	$('#invoiceAmount').change(function(){
		 var invoiceAmount = Number($jquery('#invoiceAmount').val());
  		 var pendedInvoice = Number($jquery('#pendedInvoice').val());
		 if(invoiceAmount > pendedInvoice){
			 $('#invoiceAmount').removeClass('validate[required,custom[number]]').addClass('validate[required,custom[number],max['+pendedInvoice+']]'); 
		 }else{
			 $('#invoiceAmount').removeClass('validate[required,custom[number],max['+pendedInvoice+']]').addClass('validate[required,custom[number]]'); 
		 } 
	  });

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('#purchase_invoice_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_continuous_purchase_invoice.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove(); 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result); 
			} 
		});  
	 }); 
	
	$('#purchase_invoice_save').click(function(){ 
 	 	if($jquery("#invoiceContinuousValidation").validationEngine('validate')){
	 	 		var continousInvoiceId = Number($('#continousInvoiceId').val());
	 	 		var invoiceId = Number($('#invoiceId').val());
 				var date = $('#invoiceDate').val();   
				var description = $('#description').val(); 
				var supplierId = $('#supplierId').val();
				var status = $('#status').val();
				var invoiceAmount = $jquery('#invoiceAmount').val();
		 		var totalInvoice = $jquery('#totalInvoice').val();
		 		var currencyId = Number($('#currencyId').val());
 	 			$.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/save_continuous_purchaseinvoice.action", 
 				 	async: false, 
 				 	data:{ supplierId: supplierId, invoiceAmount: invoiceAmount,  date: date, continousInvoiceId: continousInvoiceId,
 				 	   	   invoiceId: invoiceId, description: description, totalInvoice: totalInvoice, status: status, currencyId: currencyId
 				 	 	 },
 				    dataType: "json",
 				    cache: false,
 					success:function(response){   
 						 if(response.returnMessage=="SUCCESS"){
	 						var message ="Record udpated."; 
 							 $.ajax({
 									type:"POST",
 									url:"<%=request.getContextPath()%>/show_continuous_purchase_invoice.action", 
 								 	async: false,
 								    dataType: "html",
 								    cache: false,
 									success:function(result){
 										$('#common-popup').dialog('destroy');		
 										$('#common-popup').remove();   
 										$('#DOMWindow').remove();
 										$('#DOMWindowOverlay').remove();
 										$("#main-wrapper").html(result); 
 										$('#success_message').hide().html(message).slideDown(1000); 
 										$('#success_message').delay(2000).slideUp();
 									}
 							 });
 						 } 
 						 else{
 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
 							 $('#page-error').delay(2000).slideUp();
 							 return false;
 						 }
 					},
 					error:function(response){  
 						$('#page-error').hide().html("Internal error.").slideDown(1000);
 						$('#page-error').delay(2000).slideUp();
 					}
 				}); 
	 		}else{
	 			return false;
	 		}
	 	}); 
	$('#currencyId').val($('#tempCurrency').val());
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			 Continuous Invoice
		</div> 
		<form name="invoiceContinuousValidation" id="invoiceContinuousValidation" style="position: relative;"> 
		<div class="portlet-content">  
		  	<div class="tempresult" style="display:none;"></div>
		  	<input type="hidden" id="continousInvoiceId" name="continousInvoiceId" value="${INVOICE_INFO.invoiceId}"/>
		  	<input type="hidden" id="invoiceId" name="invoiceId" value="${INVOICE_INFO.invoice.invoiceId}"/>
			<div class="width100 float-left" id="hrm"> 
				<div class="tempresult" style="display: none;"></div>
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width48 float-left" id="hrm">
					<fieldset style="min-height: 130px;">  			 
						<div>
							<label class="width30">Invoice No.<span class="mandatory">*</span></label>  
 							<input type="text" name="invoiceNumber" readonly="readonly" value="${requestScope.invoiceNumber}" 
								id="invoiceNumber" class="width50 validate[required]"/>  
						</div> 
						<div>
							<label class="width30">Invoice Date<span class="mandatory">*</span></label> 
							<input type="text" name="invoiceDate"
									id="invoiceDate" readonly="readonly" class="width50 invoiceAmount validate[required]"> 
						</div> 
						<div>
							<label class="width30">Currency
								<span class="mandatory">*</span></label> 
							<select name="currency" id="currencyId" class="width51 validate[required]" disabled="disabled">
								<option value="">Select</option>
								<c:forEach var="currency" items="${CURRENCY_INFO}">
									<option value="${currency.currencyId}">${currency.currencyPool.code}</option>
								</c:forEach>
							</select>
							<input type="hidden" id="defaultCurrency" value="${DEFAULT_CURRENCY}"/>
							<input type="hidden" id="tempCurrency" value="${INVOICE_INFO.currency.currencyId}"/>
							<input type="hidden" id="oldCurrency" value="${INVOICE_INFO.currency.currencyId}"/>
						</div>	  
						<div>
							<label class="width30">Total Invoice</label> 
							<input type="text" name="totalInvoice" value="${INVOICE_INFO.totalInvoice}"
									id="totalInvoice" readonly="readonly" class="width50 totalInvoice"> 
						</div>	
						<div>
							<label class="width30">Paid Amount<span class="mandatory">*</span></label> 
							<input type="text" name="paidInvoice" value="${INVOICE_INFO.invoiceAmount}" readonly="readonly"
									id="paidInvoice" class="width50 paidInvoice"> 
							<input type="hidden" name="pendedInvoice" value="${INVOICE_INFO.pendingInvoice}"id="pendedInvoice"> 
						</div>	 
						<div>
							<label class="width30">Invoice Amount<span class="mandatory">*</span></label> 
							<input type="text" name="invoiceAmount" value="${INVOICE_INFO.pendingInvoice}"
									id="invoiceAmount" class="width50 invoiceAmount validate[required,custom[number]]"> 
						</div>	
					</fieldset>
				</div>
				<div class="width48 float-right" id="hrm">
					<fieldset style="min-height: 130px;">  
						 <div>
							<label class="width30">Supplier<span class="mandatory">*</span></label> 
							<input type="text" name="supplierName" value="${INVOICE_INFO.supplierName}"  class="width50 validate[required]"
									readonly="readonly" id="supplierName"/>  
							<input type="hidden" readonly="readonly" name="supplierId" id="supplierId" value="${INVOICE_INFO.supplier.supplierId}"/>
						</div> 
						<div>
							<label class="width30 tooltip">Status</label>
							<select name="status" id="status" class="width51 validate[required]">
								<c:forEach var="STATUS" items="${INVOICE_STATUS}">
									<option value="${STATUS.key}">${STATUS.value}</option>
								</c:forEach>
							</select>
 						</div>
						<div>
							<label class="width30 tooltip">Description</label>
							<textarea id="description" class="width51"></textarea>
						</div> 
					</fieldset>
				</div> 
			</div> 
			</div>
			<div class="clearfix"></div>  
			<div id="hrm" class="portlet-content width100 invoice_lines"> 
				<fieldset id="hrm"> 	
					<legend>Invoice Detail<span
									class="mandatory">*</span></legend>
					<div class="portlet-content"> 
					<div id="hrm" class="hastable width100"  > 
					 <table id="hastab" class="width100"> 
						<thead>
						   <tr>   
					   	    <th style="width:5%;">Receive NO.</th>
					   	    <th style="width:5%;">Ref.NO</th>   
					   	    <th style="width:5%;">Receive Date</th>   
					   	    <th style="width:5%;">Receive Qty.</th>  
					   	    <th style="width:5%;">Total Amount</th> 
						    <th style="width:5%;">Description</th> 
							<th style="width:1%; display : none;"><fmt:message key="accounts.common.label.options"/></th> 
						  </tr>
						</thead> 
						<tbody class="tab">  
							<c:choose>
								<c:when test="${INVOICE_INFO.receiveVOList ne null && INVOICE_INFO.receiveVOList ne ''}">
									<c:forEach var="INVOICE_RECEIVE" items="${INVOICE_INFO.receiveVOList}" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${status.index+1}"> 
											<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td> 
											<td>
												<input type="hidden" class="receiveId" id="receiveId_${status.index+1}" value="${INVOICE_RECEIVE.receiveId}" />
												${INVOICE_RECEIVE.receiveNumber}
											</td> 
											<td>
												${INVOICE_RECEIVE.referenceNo} 
											</td>
											<td>
												${INVOICE_RECEIVE.strReceiveDate}
											</td> 
											<td>
												${INVOICE_RECEIVE.totalReceiveQty}
											</td>  
											<td style="text-align: right;"> 
												<c:set var="totalamt" value="${INVOICE_RECEIVE.totalReceiveAmount}"/>
												<%= AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
											</td> 
											<td>
										 		 ${INVOICE_RECEIVE.description}  
											</td> 
										 	<td style="width:0.01%; display: none;" class="opn_td" id="option_${status.index+1}"> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv" id="EditImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											  </a> 
											  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowinv" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											  </a> 
											</td>   
										</tr>
									</c:forEach>
								</c:when>
							</c:choose> 
						</tbody>
					</table>
					</div>
					</div>
				</fieldset>
			</div>  
		</form>  
		<div class="clearfix"></div>
		<div id="temp-result" style="display:none;"></div>
		<div class="width30 float-right" style="font-weight: bold; display: none;">
			<span>Total: </span>
			<span id="totalInvoiceAmount"></span>
		</div>
		<div class="clearfix"></div>
		<span class="callJq"></span>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
			<div class="portlet-header ui-widget-header float-right" id="purchase_invoice_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>  
			<div class="portlet-header ui-widget-header float-right" id="purchase_invoice_save" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		 </div> 
		<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
			<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div> 
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div> 
	</div>
</div>