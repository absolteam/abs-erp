<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
select.width51 {
	width: 51%;
}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var serviceCharges="";
var accessCode = "";
var sectionRowId = 0;
$(function(){  

	 
	$('.chargesType').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	});

	$('.amount').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	}); 

	$('input,select').attr('disabled',true);
	
	$('.assetcost-lookup').live('click',function(){
		$('.ui-dialog-titlebar').remove(); 
		var str = $(this).attr("id");
		accessCode = str.substring(0, str.lastIndexOf("_"));
		sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
		$.ajax({
		        type:"POST",
		        url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		        data:{accessCode: accessCode},
		        async: false,
		        dataType: "html",
		        cache: false,
		        success:function(result){ 
		             $('.common-result').html(result);
		             $('#common-popup').dialog('open');
						   $($($('#common-popup').parent()).get(0)).css('top',0);
		             return false;
		        },
		        error:function(result){
		             $('.common-result').html(result);
		        }
		    });
			return false;
		});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="ASSET_SERVICE_COST_TYPE"){
			$('#chargesType_'+sectionRowId).html("");
			$('#chargesType_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("chargesType_"+sectionRowId); 
		}  
	});

	$('.addrows').click(function(){ 
		  var i = Number(1); 
		  var id = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id = id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/asset_service_charge_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	$('.delrow').live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);  
      	$(slidetab).remove();  
      	var i=1;
   	 	$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	 });

	 $('#service_charge_discard').click(function(){ 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_service_charge.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
			}
		 });
	 });

	 $('#service_charge_save').click(function(){   
		 if($jquery("#asset_service_charge").validationEngine('validate')){
 			var assetServiceDetailId = Number($('#assetServiceDetailId').val());   
			var page= $('#page').val();
			serviceCharges = getServiceCharges(); 
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_asset_service_charge.action", 
				 	async: false,
				 	data:{
				 			assetServiceDetailId: assetServiceDetailId, serviceCharges: serviceCharges, page: page
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){  
						 if(response.returnMessage == "SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/show_asset_service_charge.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$("#main-wrapper").html(result); 
										if(page == "edit")
											$('#success_message').hide().html("Record updated successfully.").slideDown(1000);
										else
											$('#success_message').hide().html("Record created successfully.").slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error!!!").slideDown(1000);
					}
			 }); 
		 }else{return false;}
	 });

	 var getServiceCharges = function(){
		serviceCharges = ""; 
		var chargesTypeArray = new Array();
		var amountArray = new Array(); 
		var descriptionArray = new Array(); 
		var assetServiceChargeIdArray =new Array(); 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var chargesType = $('#chargesType_'+rowId).val();
			var amount = $('#amount_'+rowId).val(); 
			var description = $('#description_'+rowId).val();
			var assetServiceChargeId = Number($('#assetServiceChargeId_'+rowId).val());   
			if(typeof chargesType != 'undefined' && chargesType!=null && chargesType!="" && amount!=null && amount!=""){
				chargesTypeArray.push(chargesType);
				amountArray.push(amount);  
 				if(description!=null && description!="")
					descriptionArray.push(description);
				else
					descriptionArray.push("##");
 				assetServiceChargeIdArray.push(assetServiceChargeId); 
			} 
		});
		for(var j=0;j<chargesTypeArray.length;j++){ 
			serviceCharges += chargesTypeArray[j]+"__"+amountArray[j]+"__"+descriptionArray[j]+"__"+assetServiceChargeIdArray[j];
			if(j==chargesTypeArray.length-1){   
			} 
			else{
				serviceCharges += "#@";
			}
		} 
		return serviceCharges;
	 };

	 $('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_service_details.action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
				return false;
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 

	if(Number($('#assetServiceDetailId').val())>0){ 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			$('#chargesType_'+rowId).val($('#tempChargesType_'+rowId).val()); 
		});
	}
	
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var chargesType=$('#chargesType_'+rowId).val();  
	 var amount=$('#amount_'+rowId).val(); 
	 var nexttab=$('#fieldrow_'+rowId).next(); 
	if(chargesType!=null && chargesType!=""
			&& amount!=null && amount!=""  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');  
	}
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			service charges
		</div> 
		<form name="asset_service_charge" id="asset_service_charge" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetServiceDetailId" name="assetServiceDetailId"
					value="${SERVICE_CHARGE.assetServiceDetailId}" />
				<input type="hidden" id="page" name="page" value="${requestScope.page}" />
				<div class="width100 float-left" id="hrm">
					<div class="width50 float-left">
						<fieldset>
							<div>
								<label class="width30" for="serviceNumber">Service Number<span
									class="mandatory">*</span> </label> 
								<input type="text" readonly="readonly" name="serviceNumber"
									id="serviceNumber" class="width50 validate[required]"
									value="${SERVICE_CHARGE.serviceNumber}" />  
							</div>
							<div>
								<label class="width30" for="serviceDate">Asset Name<span
									class="mandatory">*</span></label> 
								<input type="text" readonly="readonly" id="assetName"
									value="${SERVICE_CHARGE.assetName}" class="width50 validate[required]"/> 
							</div>
							<div>
								<label class="width30" for="serviceDate">Service Date</label> 
								<input type="text" readonly="readonly" id="serviceDate"
									value="${SERVICE_CHARGE.serviceDate}" class="width50 validate[required]"/> 
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Charges Detail </legend> 
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr> 
									<th style="width: 2%">Line No</th>
									<th class="width10">Charges Type</th>
									<th class="width10">Amount</th>
									<th class="width15">Description</th>  
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail" items="${SERVICE_CHARGE.assetServiceCharges}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}">${status.index+1}</td> 
										<td>
											<select name="chargesType" id="chargesType_${status.index+1}" class="width70 chargesType">
												<option value="">Select</option>
												<c:forEach var="chargesType" items="${COST_TYPES}">
													<option value="${chargesType.lookupDetailId}">${chargesType.displayName}</option>
												</c:forEach>
											</select> 
											<input type="hidden"
												id="tempChargesType_${status.index+1}" value="${detail.lookupDetail.lookupDetailId}"/>
										</td>
										<td>
											<input type="text"
												class="width96 amount validate[optional,custom[number]]" id="amount_${status.index+1}" style=" text-align: right;"
												value="${detail.amount}"/>
										</td> 
										<td>
											<input type="text"
												class="width96 linedescription"
												id="description_${status.index+1}" value="${detail.description}"
												style="" />
											<input type="hidden"
												id="assetServiceChargeId_${status.index+1}" value="${detail.assetServiceChargeId}" /></td>
										 
									</tr>
								</c:forEach> 
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="width30 float-right" style="font-weight: bold;">
				<span>Total: </span> <span id="totalAmount"></span>
			</div>
			<div class="clearfix"></div>  
		</form>
	</div>
</div>
<div class="clearfix"></div>