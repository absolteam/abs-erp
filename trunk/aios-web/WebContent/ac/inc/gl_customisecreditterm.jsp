<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
}

.ui-dialog {
	z-index: 99999 !important;
}
</style>
<script type="text/javascript">
var accountTypeId=0;
var slidetab;
var tempid;
$(function(){
	 $jquery("#coavalidate-form").validationEngine('attach'); 
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0); 
	      $('.ui-dialog-titlebar').remove();   
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
	
	$('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: false
	}); 
	 
	 $('.discard').click(function(){
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);
			},
			error: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		});
		 return false;
	 });
	 
	 $(document).keyup(function(e) { 
		 var id=$('.save').attr('id'); 
		  if (e.keyCode == 27 && id=="add-customise"){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$('#codecombination-popup').dialog('destroy');		
				  		$('#codecombination-popup').remove(); 
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	});  
	 
	 $('.skip').click(function(){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customise_pointofsale_control.action", 
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){ 
					$('#codecombination-popup').dialog('destroy');		
			  		$('#codecombination-popup').remove(); 
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
	  });
	 
	 $('.save').click(function(){ 
		 $('.error').hide();
		 var salesDiscountControlId=Number($('#salesDiscountControlId').val());
		 var purchaseDiscountControlId=Number($('#purchaseDiscountControlId').val());
		 var salesPenaltyControlId = Number($('#salesPenaltyControlId').val());
		 var purchasePenaltyControlId = Number($('#purchasePenaltyControlId').val());
		
		if($jquery("#coavalidate-form").validationEngine('validate')){

			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customise_credittermcontrol.action",
				data:{salesDiscountControlId: salesDiscountControlId, purchaseDiscountControlId: purchaseDiscountControlId,
						salesPenaltyControlId: salesPenaltyControlId, purchasePenaltyControlId: purchasePenaltyControlId},
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$(".tempresult").html(result);
					var message=$('#returnMsg').html(); 
					if(message.trim()=="Success"){  
						 $.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/customise_pointofsale_control.action", 
					     	async: false, 
							dataType: "html",
							cache: false,
							success: function(result){  
								$('#codecombination-popup').dialog('destroy');		
						  		$('#codecombination-popup').remove(); 
								$("#DOMWindow").html(result); 
								$('#success_message').hide().html(message).slideDown(1000);
								$.scrollTo(0,300);
							} 		
						});  
					 }
					else{
						$('.page-error').hide().html($('#returnMsg').html()).slideDown(1000);
						return false;
					}
				}
			 });
		}
		else{
			return false;
		}
	 });
	 return false;
});
function setCombination(combinationTreeId, combinationTree){  
	tempid=$(tempid).attr('id');  
	if(typeof tempid!='undefined'&& tempid=="accountSalesDiscountControlCode"){
		$('#salesDiscountControlId').val(combinationTreeId); 
		$('#salesDiscountControl').val(combinationTree);
	}
	else if(typeof tempid!='undefined'&& tempid=="accountPurchaseDiscountControlCode"){
		$('#purchaseDiscountControlId').val(combinationTreeId); 
		$('#purchaseDiscountControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountSalesPenaltyControlCode"){
		$('#salesPenaltyControlId').val(combinationTreeId); 
		$('#salesPenaltyControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountPurchasePenaltyControlCode"){
		$('#purchasePenaltyControlId').val(combinationTreeId); 
		$('#purchasePenaltyControl').val(combinationTree);
	} 
} 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Create
			Credit Terms Control
		</div>
		<div class="portlet-content">
			<div class="tempresult" style="display: none;"></div>
			<div id="success-msg" class="response-msg success ui-corner-all"
				style="width: 90%; display: none;"></div>
			<div class="page-error response-msg error ui-corner-all"
				style="width: 80% !important; display: none;"></div>
			<form id="coavalidate-form" name="coaentrydetails"
				style="position: relative;">
				<div class="width60" id="hrm">
					<fieldset>
						<legend>Credit Terms Account</legend>
						<div>
							<label class="width30">Sales Discount<span
								style="color: red;">*</span>
							</label> <input type="hidden" name="salesDiscountControlId"
								id="salesDiscountControlId" value="${SALES_DISCOUNT.combinationId}"/>
								<c:choose>
									<c:when test="${SALES_DISCOUNT.accountByCostcenterAccountId.account ne null}">
										<input type="text" name="salesDiscountControl" id="salesDiscountControl" value="${SALES_DISCOUNT.accountByCostcenterAccountId.account}.${SALES_DISCOUNT.accountByNaturalAccountId.account}.${SALES_DISCOUNT.accountByAnalysisAccountId.account}"
											readonly="readonly" class="width48 validate[required]">
									</c:when>
									<c:otherwise>
										 <input type="text"
									name="salesDiscountControl" id="salesDiscountControl"
									readonly="readonly" class="width48 validate[required]">
									</c:otherwise>
								</c:choose>  
								
							<span class="button" id="accountSalesDiscountControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div>
						<div>
							<label class="width30">Purchase Discount<span
								style="color: red;">*</span>
							</label> <input type="hidden" name="purchaseDiscountControlId"
								id="purchaseDiscountControlId" value="${PURCHASE_DISCOUNT.combinationId}"/> 
								<c:choose>
									<c:when test="${PURCHASE_DISCOUNT.accountByCostcenterAccountId.account ne null}">
										<input type="text" name="purchaseDiscountControl" id="purchaseDiscountControl" value="${PURCHASE_DISCOUNT.accountByCostcenterAccountId.account}.${PURCHASE_DISCOUNT.accountByNaturalAccountId.account}.${PURCHASE_DISCOUNT.accountByAnalysisAccountId.account}"
											readonly="readonly" class="width48 validate[required]">
									</c:when>
									<c:otherwise>
										 <input type="text"
									name="purchaseDiscountControl" id="purchaseDiscountControl"
									readonly="readonly" class="width48 validate[required]">
									</c:otherwise>
								</c:choose>  
							<span class="button" id="accountPurchaseDiscountControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div>
						<div>
							<label class="width30">Sales Penalty<span
								style="color: red;">*</span>
							</label> <input type="hidden" name="salesPenaltyControlId"
								id="salesPenaltyControlId" value="${SALES_PENALTY.combinationId}"/>
								<c:choose>
									<c:when test="${SALES_PENALTY.accountByCostcenterAccountId.account ne null}">
										<input type="text" name="salesPenaltyControl" id="salesPenaltyControl" value="${SALES_PENALTY.accountByCostcenterAccountId.account}.${SALES_PENALTY.accountByNaturalAccountId.account}.${SALES_PENALTY.accountByAnalysisAccountId.account}"
											readonly="readonly" class="width48 validate[required]">
									</c:when>
									<c:otherwise>
										 <input type="text"
									name="salesPenaltyControl" id="salesPenaltyControl"
									readonly="readonly" class="width48 validate[required]">
									</c:otherwise>
								</c:choose>  
							<span class="button" id="accountSalesPenaltyControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div>
						<div>
							<label class="width30">Purchase Penalty<span
								style="color: red;">*</span>
							</label> <input type="hidden" name="purchasePenaltyControlId"
								id="purchasePenaltyControlId" value="${PURCHASE_PENALTY.combinationId}"/> 
								<c:choose>
									<c:when test="${PURCHASE_PENALTY.accountByCostcenterAccountId.account ne null}">
										<input type="text" name="purchasePenaltyControl" id="purchasePenaltyControl" value="${PURCHASE_PENALTY.accountByCostcenterAccountId.account}.${PURCHASE_PENALTY.accountByNaturalAccountId.account}.${PURCHASE_PENALTY.accountByAnalysisAccountId.account}"
											readonly="readonly" class="width48 validate[required]">
									</c:when>
									<c:otherwise>
										 <input type="text"
									name="purchasePenaltyControl" id="purchasePenaltyControl"
									readonly="readonly" class="width48 validate[required]">
									</c:otherwise>
								</c:choose>   
							<span class="button" id="accountPurchasePenaltyControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div>
					</fieldset>
				</div>
				<div class="clearfix"></div>
				<div class="accountcreditterm_controlcode"
					style="font-weight: bold;"></div>
				<div class="clearfix"></div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
					<div class="portlet-header ui-widget-header float-right discard"
						id="cancel">
						<fmt:message key="accounts.common.button.cancel" />
					</div>
					<div class="portlet-header ui-widget-header float-right save"
						id="add">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip">
					<div class="portlet-header ui-widget-header float-left skip"
						style="cursor: pointer;">skip</div>
				</div>
			</form>
		</div>
	</div>
	<div
		style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span>
			</a>
		</div>
		<div id="codecombination-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>