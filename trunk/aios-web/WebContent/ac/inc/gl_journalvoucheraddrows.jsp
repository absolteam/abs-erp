 <tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}">${rowId}</td>
	<td class="codecombination_info" id="codecombination_${rowId}" style="cursor: pointer;">
		<input type="hidden" name="combinationId_${rowId}" id="combinationId_${rowId}"/> 
		<input type="text" name="codeCombination_${rowId}" readonly="readonly" id="codeCombination_${rowId}" class="codeComb width80 ">
		<span class="button" id="codeID_${rowId}">
			<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all codecombination-popup-transaction width100"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span>
	</td>
	<td id="transactionflag_${rowId}"> 
		<input type="text" name="debitAmount" id="debitAmount_${rowId}" style="text-align:right;"
		 class="debitAMOUNT width97 right-align validate[optional,custom[number]]">
	</td>
	<td id="amount_${rowId}" style="text-align; right;">
		<input type="text" name="linesAmount" id="linesAmount_${rowId}" style="text-align:right;"
		 class="linesAMOUNT width97 right-align validate[optional,custom[number]]">
	</td> 
	<td id="linedecription_${rowId}">
		<input type="text" name="linesDescription" id="linesDescription_${rowId}" class="width98" maxlength="150">
	</td>
	<td style="display:none">
		<input type="hidden" name="transactionDetailId" id="transactionDetailId_${rowId}"/>
		<input type="hidden" name="combinationid" id="combinationid_${rowId}"/> 
	</td> 
	 <td style="width:1%;" class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${rowId}" style="cursor:pointer;display:none;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${rowId}" style="cursor:pointer;display:none;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td> 
	<td style="display:none;" id="codecombination_desc_${rowId}"></td>  
</tr> 
<script type="text/javascript">
$(function(){  
	$jquery("#addMasterJournalValidation").validationEngine('attach');
	$jquery(".debitAMOUNT,.linesAMOUNT").number( true, 3 );
});
</script>