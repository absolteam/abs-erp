<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<td colspan="5" class="tdidentity" id="childRowId_${rowId}">
	<div class="float-right width48" style="margin-top: 5px;">
		<div id="othererror" class="response-msg error ui-corner-all width60"
			style="display: none;"></div>
	</div>
	<div class="float-left width100" id="hrm">
		<fieldset>
			<legend>
				<fmt:message key="accounts.coa.label.accountdetails" />
			</legend>
			<div class="width50 float-left">
				<div class="width100 float-left">
					<label class="width30"><fmt:message
							key="accounts.coa.label.accountcode" /><span style="color: red;">*</span>
					</label> <input type="text" name="accountCode" id="accountCode"
						class="width50 validate[required,custom[onlyNumber]]" maxlength="8">
				</div>
				<div class="width100 float-left">
					<label class="width30"><fmt:message
							key="accounts.coa.label.description" /><span style="color: red;">*</span>
					</label> <input type="text" name="accountDescription"
						id="accountDescription" class="width50 validate[required]">
				</div>
			</div>
			<div class="width50 float-left">
				<div class="naturalType width100 float-left">
					<c:set var="validateclass" value=""/>
					<c:set var="spanred" value="none"/>
					<c:if test="${ACCOUNT_TYPES ne null && fn:length(ACCOUNT_TYPES)>0}">
						<c:set var="validateclass" value="validate[required]"/>
						<c:set var="spanred" value="inline"/>
					</c:if>
					<label class="width30"><fmt:message
							key="accounts.coa.label.accounttype" />
							<span style="color: red; display: ${spanred}">*</span>
					</label>  
					<select name="accountTypeId" id="accountTypeId" class="width50 ${validateclass}">
						<option value="">Select</option>
						<c:forEach items="${ACCOUNT_TYPES}" var="accountType">
							<option value="${accountType.key}">${accountType.value}</option>
						</c:forEach> 
					</select>
				</div>
				<div class="naturalType width100 float-left">
					<label class="width30">Sub Type</label>
					<div>
						<select id="subType"
							name="subType"
							class="width50">
							<option value="">Select</option>
							<c:forEach items="${SUB_TYPES}" var="nltlst">
								<option value="${nltlst.lookupDetailId}">${nltlst.displayName}
								</option>
							</c:forEach>
						</select> <span class="button" id="ACCOUNT_SUB_TYPE_1"
							style="position: relative; "> <a
							style="cursor: pointer;" id="ACCOUNT_SUB_TYPE"
							class="btn ui-state-default ui-corner-all subtype-lookup width100">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
							type="hidden" name="subTypeTemp" id="subTypeTemp"
							value="" />
					</div>
				</div>
			</div>
		</fieldset>

		<input type="hidden" id="showPage" value="${showPage}" />
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right">
		<div class="portlet-header ui-widget-header float-right rowcancel"
			id="cancel_${rowId}" style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
		<div class="portlet-header ui-widget-header float-right rowsave"
			id="rowsave_${rowId}" style="cursor: pointer;">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
</td>

<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0);
$(function(){ 
	$jquery("#coavalidate-form").validationEngine('attach');  
	
	showSystemAccountCode();
	
	$('.rowcancel').click(function(){ 
		$('.formError').remove();	
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if($('#accountid_'+rowid).text()>0){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			$("#DeleteImage_"+rowid).show();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		i=0;
		$('.rowid').each(function(){
			i=i+1;
		}); 
		$('#DeleteImage_'+i).hide(); 
	});

	$('.rowsave').click(function(){
		$('.error').hide();
		 //Find the Id for fetch the value from Id
		if($jquery("#coavalidate-form").validationEngine('validate')){      
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	
		 var accountCode=$('#accountCode').val();
		 if(accountCode==null || accountCode.trim()==''){
			 $("#othererror").hide().html("Enter Account Code").slideDown(1000);
			return false;
		}
		 var accountDescription=$('#accountDescription').val();
		 if(accountDescription==null || accountDescription.trim()==''){
			 $("#othererror").hide().html("Enter Account Description").slideDown(1000);
			return false;
		}
		 var accountTypeId=Number($('#accountTypeId').val());
		 var segmentId=$('#segmentId').val();
		 var showPage=$('#showPage').val();
		 var accountId=0;
		 var url_action="";
		 var lineId=$('#lineId_'+rowid).text(); 
		 
		 if($('#accountid_'+rowid).text()>0){
			 accountId=$('#accountid_'+rowid).text();
			 url_action="chart_of_account_update_linesave";
		 }
		 else if(showPage=="addadd"){
			 url_action="chart_of_account_add_linesave";
		 }
		 else if(showPage=="addedit"){
			 url_action="chart_of_account_addedit_linesave";
		 }
		 var implementationId=1; 
		 var accountType="";
		 if(accountTypeId>0)
			 accountType=$('#accountTypeId :selected').text();
		
		var subType =Number($('#subType :selected').val());
		var subTypeName =$('#subType :selected').text();
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"
													+ url_action + ".action",
											async : false,
											data : {
												segmentId : segmentId,
												implementationId : implementationId,
												accountCode : accountCode,
												accountDescription : accountDescription,
												accountId : accountId,
												accountTypeId : accountTypeId,
												id : rowid,
												showPage : showPage,
												id : lineId,
												subType:subType
											},
											dataType : "html",
											cache : false,
											success : function(result) {
												$('.tempresultfinal').html(
														result);
												var message = $('#returnMsg')
														.html().trim();
												if (message == "Success") {
													flag = true;
												} else {
													flag = false;
													$("#othererror").hide()
															.html(message)
															.slideDown();
													return false;
												}
											},
											error : function(result) {
												$('.tempresultfinal').html(
														result);
												$("#othererror").html(
														$('#returnMsg').html());
												return false;
											}
										});
								if (flag == true) {
									//Bussiness parameter
									$('#code_' + rowid).text(accountCode);
									$('#description_' + rowid).text(
											accountDescription);
									$('#accounttype_' + rowid)
											.text(accountType);
									$('#accounttypeid_' + rowid).text(
											accountTypeId);
									$('#subTypeName_' + rowid).text(
											subTypeName);
									$('#subTypeId_' + rowid).val(
											subType);
									//Button(action) hide & show 
									if ($('#accountid_' + rowid).text() > 0) {
										$("#AddImage_" + rowid).hide();
										$("#EditImage_" + rowid).show();
										$("#DeleteImage_" + rowid).hide();
										$("#WorkingImage_" + rowid).hide();
									} else {
										$("#AddImage_" + rowid).hide();
										$("#EditImage_" + rowid).show();
										$("#DeleteImage_" + rowid).show();
										$("#WorkingImage_" + rowid).hide();
									} 
									$("#childRowId_" + rowid).remove();
									triggerAddRow(rowid);
									//Row count+1
									var childCount = Number($('#childCount')
											.val());
									childCount = childCount + 1;
									$('#childCount').val(childCount);
								}
							} else {
								return false;
							}
						});
	
	
	});
	
function showSystemAccountCode(){
	var tempvar=$('.tdidentity').attr("id");
	var idarray = tempvar.split('_');
	var rowid=Number(idarray[1]); 
	var segmentId = Number($('#segmentId').val());
	if(segmentId > 0){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_chartofaccount_systemcode.action",
			data:{id:rowid, segmentId: segmentId},
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				if(response != null && response.systemAccountCode != null){
					$('#accountCode').val(response.systemAccountCode);
				} 
			}
		 });
	} 
}
</script>