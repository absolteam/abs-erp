<tr class="rowidmrp" id="fieldrowmrp_${rowId}">
	<td id="lineIdmrp_${rowId}" style="display: none;">${rowId}</td>
	<td><select name="itemnaturemrp" class="width98 itemnatureMrp"
		id="itemnaturemrp_${rowId}">
			<option value="A">Asset</option>
			<option value="E">Expense</option>
			<option value="I" selected="selected">Inventory</option>
	</select> <input type="hidden" id="tempitemnature_${rowId}" name="itemnature" />
	</td>
	<td><span class="float-left width60" id="productmrp_${rowId}"></span>
		<span class="width10 float-right" id="unitCodemrp_${rowId}"
							style="position: relative;"></span>
		<span class="button float-right prod" style="height: 16px;"> <a
			style="cursor: pointer; cursor: pointer; height: 100%;"
			id="productlinemrp_${rowId}"
			class="btn ui-state-default ui-corner-all purchaseproduct-common-popupmrp width100 float-right">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input type="hidden"
		id="productidmrp_${rowId}" />
	</td>
	<td class="hidemandatorymrp"><span class="float-left width60"
		id="storeNamemrp_${rowId}"></span> <input type="hidden" name="storeId"
		id="storeIdmrp_${rowId}" /> <input type="hidden" name="shelfId"
		id="shelfIdmrp_${rowId}" /> <span class="button float-right"> <a
			style="cursor: pointer;" id="prodIDStrmrp_${rowId}"
			class="btn ui-state-default ui-corner-all purchase-store-popupmrp width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td id="uommrp_${rowId}" class="uom" style="display: none;"></td>
	<td>
		<select name="packageTypemrp" id="packageTypemrp_${rowId}" class="packageTypemrp">
			<option value="-1">Select</option>
		</select> 
		<input type="hidden" name="temppackageTypemrp" id="temppackageTypemrp_${rowId}"/> 
	</td> 
	<td><input type="text" 
			class="width96 packageUnitmrp validate[optional,custom[number]]" id="packageUnitmrp_${rowId}"/> 
	</td>  
	<td><input type="hidden"
		class="width96 quantitymrp validate[optional,custom[number]]"
		id="quantitymrp_${rowId}" />
		<span id="baseUnitConversionmrp_${rowId}" class="width25" style="display: none;"></span>
		<span id="baseDisplayQtymrp_${rowId}"></span>
	</td>
	<td><input type="text"
		class="width96 unitratemrp validate[optional,custom[number]]"
		id="unitratemrp_${rowId}" style="text-align: right;" />
		<input type="text" style="display: none;" name="totalAmountHmrp" readonly="readonly" id="totalAmountHmrp_${rowId}" 
			class="totalAmountHmrp"/> 
		<input type="hidden" id="basePurchasePricemrp_${rowId}"/>
	</td>
	<td><input type="text" class="width96 batchNumbermrp"
		id="batchNumbermrp_${rowId}" maxlength="40"/>
	</td>
	<td><input type="text" readonly="readonly"
		class="width96 expiryBatchDatemrp" id="expiryBatchDatemrp_${rowId}" />
	</td>
	<td class="totalamountmrp" id="totalamountmrp_${rowId}"
		style="text-align: right;"></td>
	<td><input type="text" name="linedescriptionmrp"
		id="linedescriptionmrp_${rowId}" />
	</td>
	<td style="display: none;"><input type="hidden"
		id="purchaseLineIdmrp_${rowId}" /> <input type="hidden"
		id="requisitionLineIdmrp_${rowId}" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowmrp"
		id="DeleteImageMrp_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImageMrp_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a>
	</td>
</tr> 
<script type="text/javascript">
$(function(){
	$('.expiryBatchDatemrp').datepick();
	if (Number($('#supplierId').val()) > 0)
		$('.hidemandatorymrp').hide();
	$jquery(".unitratemrp,.totalAmountHmrp").number(true,3); 
});
</script>