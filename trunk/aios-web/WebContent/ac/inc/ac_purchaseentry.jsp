<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script src="fileupload/FileUploader.js"></script>
<style>
.ui-button{
 background: #d14836 none repeat scroll 0 0 !important;
 border: 1px solid transparent !important;
 border-radius: 2px !important;
 color: #fff !important;
 margin-left: 4px !important;
 text-shadow: none !important;
 font-size : 0.9em!important;
 font-weight : bold!important;
}.ui-button span{font-weight : bold!important;}
</style>
<script type="text/javascript">
var idname="";
var slidetab="";
var actionname="";
var purchaseDetails="";
var accessCode = "";
var globalRowId=null;
var productItemType=null;
var purchsaeDetailJS = [];
var batchProduct = false;
$(function(){   
	$jquery("#purchaseentry_details").validationEngine('attach'); 
	$jquery(".unitrate,.totalAmountH,.totalPurchaseAmountH").number(true,3); 
	var purchaseId = Number($('#purchaseId').val());   
	 $('#purchase_document_information').click(function(){
			if(purchaseId>0){
				AIOS_Uploader.openFileUploader("doc","purchaseDocs","Purchase",purchaseId,"PurchaseDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","purchaseDocs","Purchase","-1","PurchaseDocuments");
			}
		});
	 
	 if(purchaseId>0)
		populateUploadsPane("doc","purchaseDocs","Purchase",purchaseId);	
	 
	 //Supplier being made mandatory to avoid the confution with Maghweer stables
	 	var compKey='${THIS.companyKey}';
 		if(compKey=='maghaweer' ){
 			$('#supplierName').addClass('validate[required]');
 		}
	 
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#purchaseDate,#expiryDate,#deliveryDate').datepick({
	 onSelect: customRange, showTrigger: '#calImg'});  
	
	manupulateLastRow();

	$('#person-list-close').live('click',function(){
		$('#common-popup').dialog('close');
	});

	$('#product-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	});
 
	$('#supplier-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	});

	$('#quotation-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	});
	  
	 $('.closeBacthPurchase').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
	 });
	   
	$('.quotation-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_commom_quotation_popup.action", 
		 	async: false,  
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.supplierpurchase-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,  
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.contactpurchase-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data:{personTypes: "1"},
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	{
		var itemType="I";
		var rowId=Number(1);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchase_product_common_popup_preload.action", 
		 	async: false,  
		 	data:{rowId: rowId, itemType: itemType, pageInfo: "purchase"},
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#product-inline-popup').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
	}
	
	$('.purchaseproduct-common-popup').live('click',function(){  
		batchProduct = false;
		$('.ui-dialog-titlebar').remove(); 
		globalRowId=getRowId($(this).attr('id'));
		productItemType=$('#itemnature_'+rowId).val(); 
		$('#product-inline-popup').dialog('open'); 
		$('#productmrp').val("false");
		return false;
	}); 
	
	$('.purchaseproduct-common-popupmrp').live('click',function(){  
		batchProduct =  true;
		$('.ui-dialog-titlebar').remove(); 
		globalRowId=getRowId($(this).attr('id'));
		productItemType=$('#itemnaturemrp_'+rowId).val(); 
		$('#product-inline-popup').dialog('open'); 
		$('#productmrp').val("true");
		$('#DOMWindowOverlay').css('z-index',1000);
		$('#DOMWindow').css('z-index',1000);
 		return false;
	});
	
	
	$('#product-inline-popup').dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			},"Add Product" : function() {
				addProductWin();
			}
		}
	});
	
	$('.confirmBacthPurchase').live('click',function(){  
  		purchsaeDetailJS = getPurchaseProductBatchDetails(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchsedetail_option.action", 
		 	async: false,  
		 	data:{purchaseDetails: JSON.stringify(purchsaeDetailJS)},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('.tab').html(result);
			} 
		}); 
		return false; 
	}); 
	
	var getPurchaseProductBatchDetails = function(){
		purchsaeDetailJS = []; 
		$('.rowidmrp').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = $('#productidmrp_' + rowId).val();
			var unitRate = $jquery('#unitratemrp_' + rowId).val();
			var productQty = $('#quantitymrp_' + rowId).val();
			var batchNumber = $('#batchNumbermrp_' + rowId).val();
			var expiryDate = $('#expiryBatchDatemrp_' + rowId).val();
			var purchaseDetailId = Number($(
					'#purchaseLineIdmrp_' + rowId).val());
 			var linesDescription = $('#linedescriptionmrp_' + rowId)
					.val();
			var shelfId = Number($(
					'#shelfIdmrp_' + rowId).val());
			var requisitionDetailId = Number($(
					'#requisitionLineIdmrp_' + rowId).val());
			var packageDetailId = Number($(
					'#packageTypemrp_' + rowId).val());
			var packageUnit= Number($(
					'#packageUnitmrp_' + rowId).val());
			if(typeof productId != 'undefined' && productId > 0 && productQty > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"productQty" : productQty, 
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"requisitionDetailId": requisitionDetailId,
					"packageDetailId": packageDetailId,
					"packageUnit": packageUnit,
					"purchaseDetailId": purchaseDetailId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	}; 
	
	$('.moreoption').live('click',function(){  
  		purchsaeDetailJS = getPurchaseProductDetails(); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchsedetail_moreoption.action", 
		 	async: false,  
		 	data:{purchaseDetails: JSON.stringify(purchsaeDetailJS)},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		}); 
		return false; 
	});  
	
	var getPurchaseProductDetails = function(){
		purchsaeDetailJS = []; 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = $('#productid_' + rowId).val();
			var unitRate = $jquery('#unitrate_' + rowId).val();
			var productQty = $('#quantity_' + rowId).val();
			var purchaseDetailId = Number($(
					'#purchaseLineId_' + rowId).val());
			var batchNumber = $('#batchNumber_' + rowId).val();
			var expiryDate = $('#expiryBatchDate_' + rowId).val();
 			var linesDescription = $('#linedescription_' + rowId)
					.val();
			var shelfId = Number($(
					'#shelfId_' + rowId).val());
			var requisitionDetailId = Number($(
					'#requisitionLineId_' + rowId).val());
			var packageDetailId = Number($(
					'#packageType_' + rowId).val());
			var packageUnit = Number($(
					'#packageUnit_' + rowId).val());
			if(typeof productId != 'undefined' && productId > 0 && productQty > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"productQty" : productQty, 
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"requisitionDetailId": requisitionDetailId,
					"packageDetailId": packageDetailId,
					"packageUnit" : packageUnit,
					"purchaseDetailId": purchaseDetailId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	}; 
	 
	 $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	 $('.shipping-term-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        accessCode = $(this).attr("id");
 	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="SHIPPING_TERMS"){
				$('#shippingTerm').html("");
				$('#shippingTerm').append("<option value=''>Select</option>");
				loadLookupList("shippingTerm"); 
			} 
		});
		
	$('.itemnature').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	});
	
	$('.packageUnitmrp').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		$('#baseUnitConversionmrp_'+rowId).text('');
		var packageUnit = Number($(this).val()); 
		var packageType = Number($('#packageTypemrp_'+rowId).val());   
		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		if(packageType == -1 && packageUnit > 0){
			$('#quantitymrp_'+rowId).val(packageUnit);
			if(unitrate > 0 ){
				var total = Number(unitrate * packageUnit);
				$jquery('#totalAmountHmrp_'+rowId).val(total);
				$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val()); 
			}  
		}
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnitMrp(rowId);
		} 
		triggerAddRowMrp(rowId);
		return false;
	});
	
	$('.unitratemrp').live('change',function(){
 		var rowId=getRowId($(this).attr('id'));
 		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		var quantity=Number($('#packageUnitmrp_'+rowId).val());
		var total=Number(0);  
		if(unitrate>0 && quantity>0){
			total = Number(unitrate * quantity);
			$jquery('#totalAmountHmrp_'+rowId).val(total);
			$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
			triggerAddRowMrp(rowId);
		} 
		return false;
	});
	
	$('.packageTypemrp').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		$('#quantitymrp_'+rowId).val('');
		var packageUnit = Number($('#packageUnitmrp_'+rowId).val());
		var packageType = Number($('#packageTypemrp_'+rowId).val());   
		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnitMrp(rowId);
		}  else if(packageUnit > 0 && packageType == -1){
			$('#quantitymrp_'+rowId).val(packageUnit);
			if(unitrate > 0 ){
				total = Number(unitrate * packageUnit);
				$jquery('#totalAmountHmrp_'+rowId).val(total);
				$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val()); 
			}  
		}
		return false;
	});
	
	$('.quantitymrp').live('change',function(){ 
		var rowId=getRowId($(this).attr('id'));
		var quantity = Number($(this).val());
		var total=Number(0);
		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		var packageType = Number($('#packageTypemrp_'+rowId).val());   
		if(quantity > 0 && packageType == -1){
			$('#packageUnitmrp_'+rowId).val(quantity);
		}else if(quantity > 0 && packageType > 0){ 
			getProductConversionUnitMrp(rowId);
		}
		if(quantity>0 && unitrate>0 ){
			total = Number(quantity * unitrate);
			$jquery('#totalAmountHmrp_'+rowId).val(total);
			$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
			triggerAddRowMrp(rowId);
		} 
		return false;
	});
	
	$('.unitrate').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		$('#basePurchasePrice_'+rowId).val('');
		var quantity = Number($('#packageUnit_'+rowId).val());
		var total = Number(0);   
		if(unitrate > 0 && quantity > 0){
			total = Number(unitrate * quantity);
			$jquery('#totalAmountH_'+rowId).val(total);
			$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
			triggerAddRow(rowId);
		}
		calculateTotalPurchase();
		return false;
	});

	$('.quantity').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var quantity=Number($(this).val());
		var total=Number(0);
		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		var packageType = Number($('#packageType_'+rowId).val());   
		if(quantity > 0 && packageType == -1){
			$('#packageUnit_'+rowId).val(quantity);
		}else if(quantity > 0 && packageType > 0){ 
			getProductConversionUnit(rowId);
		}
		if(quantity>0 && unitrate>0 ){
			total = Number(unitrate * quantity);
			$jquery('#totalAmountH_'+rowId).val(total);
			$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
			triggerAddRow(rowId);
		}
		calculateTotalPurchase();
		return false;
	}); 

	$('.packageUnit').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		$('#baseUnitConversion_'+rowId).text('');
		$('#quantity_'+rowId).val('');
		var packageUnit = Number($(this).val()); 
		var packageType = Number($('#packageType_'+rowId).val()); 
		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		var total=Number(0); 
		if(packageType == -1 && packageUnit > 0){
			$('#quantity_'+rowId).val(packageUnit);
			if(unitrate > 0 ){
				total = Number(unitrate * packageUnit);
				$jquery('#totalAmountH_'+rowId).val(total);
				$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
				triggerAddRow(rowId);
			} 
			calculateTotalPurchase();
		}
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		}
		triggerAddRow(rowId);
		return false;
	}); 
	
	$('.packageType').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		$('#quantity_'+rowId).val('');
		var packageUnit = Number($('#packageUnit_'+rowId).val());
		var packageType = Number($('#packageType_'+rowId).val());  
		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		} else if(packageUnit > 0 && packageType == -1){
			$('#quantity_'+rowId).val(packageUnit);
			if(unitrate > 0 ){
				total = Number(unitrate * packageUnit);
				$jquery('#totalAmountH_'+rowId).val(total);
				$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
				triggerAddRow(rowId);
			} 
			calculateTotalPurchase();
		}
		return false;
	}); 
	
	 $('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/purchase_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 
	 
	 $('.addrowsmrp').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabMrp>.rowidmrp:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/purchase_addrowmrp.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabMrp tr:last').before(result);
					 if($(".tabMrp").height()>255)
						 $(".tabMrp").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidmrp').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdmrp_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
 		 calculateTotalPurchase();
 		 return false;
	 }); 

	 if($('#purchaseId').val()>0){ 
			$('#currencyId').val($('#tempcurrencyId').val());
			$('#termId').val($('#temptermId').val()); 
			if($('#quotationId').val()>0){
				$('#currencyId').attr('disabled',true);
				$('#supplier').hide();
				$('.quoteprod').remove();
				
				$('.itemnature').attr("disabled",true); 
				$('.unitrate').attr('readOnly',true);
				$('.quantity').attr('readOnly',true);
				$('.rowid').each(function(){   
					var rowId = getRowId($(this).attr('id'));
					$('#itemnature_'+rowId).hide();  
					$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val()); 
					$('#itemtype_'+rowId).text($('#itemnature_'+rowId+ " :selected").text()); 
					var nexttab = $('#fieldrow_' + rowId).next(); 
					if (
							$(nexttab).hasClass('lastrow')) {
						$(this).remove();
					} 
				});
			}
			if($('#requisitionId').val()>0){ 
				$('.quoteprod').remove();
				$('.rowid').each(function(){   
					var rowId = getRowId($(this).attr('id'));
					$('#itemnature_'+rowId).hide();  
					$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val()); 
					$('#itemtype_'+rowId).text($('#itemnature_'+rowId+ " :selected").text()); 
					var nexttab = $('#fieldrow_' + rowId).next(); 
					if (
							$(nexttab).hasClass('lastrow')) {
						$(this).remove();
					} 
				});
			}
			var totalPurchase = 0;
			$('.rowid').each(function(){
				var rowId=getRowId($(this).attr('id'));
				$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val());
				$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
				$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val()); 
				var totalamount = Number($jquery('#totalAmountH_'+rowId).val());
	  	 		totalPurchase = Number(totalPurchase+totalamount); 
			});
			$jquery('.totalPurchaseAmountH').val(totalPurchase);
			$('#totalPurchaseAmount').text($('.totalPurchaseAmountH').val());
			$('#status').val($('#tempstatus').val());
			$('#shippingTerm').val($('#tempshippingTerm').val());
			$('#paymentMode').val($('#temppaymentMode').val());  
		}else{ 
			$('#status').val(1);
			var default_currency=$('#default_currency').val();
			$('#currencyId option').each(function(){  
				var curval=$(this).val();
				if(curval==default_currency){   
					$('#currencyId option[value='+$(this).val()+']').attr("selected","selected"); 
					return false;
				}
			});
		} 

	 $('.pdiscard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpurchaseorder.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
					$("#main-wrapper").html(result);  
				}
		 });
	 });

	 $('.save').click(function(){ 
		 if($jquery("#purchaseentry_details").validationEngine('validate')){
 			 var purchaseId=Number($('#purchaseId').val()); 
			 
			 var quotationId=Number($('#quotationId').val());
			 var requisitionId = Number($('#requisitionId').val());
			 var purchaseDate=$('#purchaseDate').val();
			 var currencyId=$('#currencyId').val();
			 var supplierId=Number($('#supplierId').val()); 
			 if(supplierId==0){
				 var shelfIds = new Array();
					$('.rowid').each(
							function() {
								var rowId = getRowId($(this).attr('id'));
								var shelfId = Number($('#shelfId_' + rowId).val());
								if(shelfId!=null && shelfId>0)
								shelfIds.push(shelfId);
					});
					if(shelfIds.length==0){
						$('#page-error').hide().html(
							"Please choose store information for each purchase detail.").slideDown(
									1000);
						$('#page-error').delay(
								2000).slideUp();
						return false;
					}
			 }
			 var termId=Number($('#termId').val());
			 var expiryDate=$('#expiryDate').val();
			 var description = $("#description").val();
			 var purchaseNumber=$('#purchaseNumber').val();
			 var purchaseManagerId = Number($('#purchaseManagerId').val());
			 var shippingTermId = Number($('#shippingTerm').val());
			 var paymentModeId = Number($('#paymentMode').val());
			 var status = $('#status').val();
			 var deliveryAddress = $('#deliveryAddress').val();
			 var deliveryDate = $('#deliveryDate').val();
			 var continuousPurchase = $('#continuousPurchase').attr('checked');
			 purchsaeDetailJS = getPurchaseProductDetails(); 
			 if(purchsaeDetailJS!=null && purchsaeDetailJS!=""){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/savepurchaseorder.action", 
					 	async: false,
					 	data : { purchaseId: purchaseId, quotationId: quotationId, purchaseDate: purchaseDate, currencyId: currencyId, 
						 		 paymentModeId: paymentModeId, supplierId: supplierId, paymentTermId: termId, expiryDate: expiryDate, status: status,
						 		 purchaseDetails: JSON.stringify(purchsaeDetailJS), purchaseManagerId: purchaseManagerId, shippingTermId: shippingTermId,
						 		 description:description, purchaseNumber:purchaseNumber, deliveryAddress: deliveryAddress, deliveryDate: deliveryDate,
						 		 continuousPurchase: continuousPurchase, requisitionId: requisitionId
						 		},
					    dataType: "json",
					    cache: false,
						success:function(response){
							if(response.returnMessage == "SUCCESS"){
								$('#common-popup').dialog('destroy');		
								$('#common-popup').remove();   
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/getpurchaseorder.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(
												result) {
											$(
													"#main-wrapper")
													.html(
															result);
											if (purchaseId > 0)
												$(
														'#success_message')
														.hide()
														.html(
																"Record updated")
														.slideDown(
																1000);
											else
												$(
														'#success_message')
														.hide()
														.html(
																"Record created")
														.slideDown(
																1000);
											$(
													'#success_message')
													.delay(
															2000)
													.slideUp();
										}
									});
								} else {
									$('#page-error')
											.hide()
											.html(
													response.returnMessage)
											.slideDown(1000);
									$('#page-error').delay(
											2000).slideUp();
									if(null!=response.duplicateProductIds && response.duplicateProductIds !=""){
										var duplicateProducts = response.duplicateProductIds.split(',');
										var duplicateArray = new Array();
 										for (var i = 0; i < duplicateProducts.length; i++) {
 											if(Number(duplicateProducts[i] > 0)){
 												duplicateArray.push(Number(duplicateProducts[i]));
 											} 
										}  
 										$('.rowid').each(
											function() {
											var rowId = getRowId($(this).attr('id'));
											var productId = Number($('#productid_' + rowId).val());
 											if($jquery.inArray(productId, duplicateArray) != -1){
 												$('#fieldrow_'+rowId).css('background-color','#fff636');
 											}
										});
									}
									return false;
								}
							} 
						});
				} else {
					$('#page-error').hide().html(
							"Please enter purchase details.")
							.slideDown(1000);
					$('#page-error').delay(2000).slideUp();
					return false;
				}
			} else {
				return false;
			}
		}); 
		
		$('.purchase-store-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_session_common.action", 
			 	async: false,  
			 	data:{rowId : rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.store-result').html(result);  
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});
		
		$('.purchase-store-popupmrp').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_session_commonmrp.action", 
			 	async: false,  
			 	data:{rowId : rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.store-result').html(result);  
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});
		
		$('.requisition-purchase-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_purchase_requisition_popup.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);  
					$('#common-popup').dialog('open');
	  				$($($('#common-popup').parent()).get(0)).css('top',0);
	                return false;
				} 
			});  
			return false;
		});

		$('#store-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 zIndex:100000,
			 modal: true 
		});  
			
		$('.rackidmrp').live(
				'dblclick',
				function() { 
					currentId = $(this);
					tempvar = $(currentId).attr('id');
					idarray = tempvar.split('_');
					rowid = Number(idarray[1]);
					var storeRowId = Number($('#store_row_idmrp').html()); 
					$('#storeDetailmrp_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#racknamemrp_'+storeRowId).text($('#rack_' + rowid).html()); 
					$('#storeNamemrp_'+storeRowId).text($('#rackstorename_' + rowid).text()); 
					$('#storedmrp_'+storeRowId).val($('#rackstoreid_' + rowid).html());
					$('#shelfIdmrp_'+storeRowId).val($('#rackidmrp_' + rowid).html());  
					$('#store-popup').dialog('close');
					$('#common-popup').dialog('close');
					return false;
		});
		
		$('.rackid').live(
				'dblclick',
				function() { 
					currentId = $(this);
					tempvar = $(currentId).attr('id');
					idarray = tempvar.split('_');
					rowid = Number(idarray[1]);
					var storeRowId = Number($('#store_row_id').html()); 
					$('#storeDetail_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#rackname_'+storeRowId).text($('#rack_' + rowid).html()); 
					$('#storeName_'+storeRowId).text($('#rackstorename_' + rowid).text()); 
					$('#stored_'+storeRowId).val($('#rackstoreid_' + rowid).html());
					$('#shelfId_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#shelfName').val($('#rack_' + rowid).html()); 
					$('#store-popup').dialog('close');
					$('#common-popup').dialog('close');
					return false;
		});
		
	});
	
	function purchaseProductBatches(){ 
		purchsaeDetailJS = []; 
		$('.rowidmrp').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = $('#productidmrp_' + rowId).val();
			var unitRate = $jquery('#unitratemrp_' + rowId).val();
			var productQty = $('#quantitymrp_' + rowId).val();
			var batchNumber = $('#batchNumbermrp_' + rowId).val();
			var expiryDate = $('#expiryBatchDatemrp_' + rowId).val();
			var purchaseDetailId = Number($(
					'#purchaseLineIdmrp_' + rowId).val());
 			var linesDescription = $('#linedescriptionmrp_' + rowId)
					.val();
			var shelfId = Number($(
					'#shelfIdmrp_' + rowId).val());
			var requisitionDetailId = Number($(
					'#requisitionLineIdmrp_' + rowId).val());
			var packageDetailId = Number($(
					'#packageTypemrp_' + rowId).val());
			var packageUnit = Number($(
					'#packageUnitmrp_' + rowId).val());
			
			if(typeof productId != 'undefined' && productId > 0 && productQty > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"productQty" : productQty, 
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"requisitionDetailId": requisitionDetailId,
					"packageDetailId": packageDetailId,
					"packageUnit" : packageUnit,
					"purchaseDetailId": purchaseDetailId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	}
	 
	function addProductWin() {
		if(batchProduct == true){
			purchsaeDetailJS = purchaseProductBatches(); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_purchsedetail_option.action", 
			 	async: false,  
			 	data:{purchaseDetails: JSON.stringify(purchsaeDetailJS)},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$('.tab').html(result);
				} 
			}); 	
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/product_addentry_popup.action", 
		 	async: false,  
		 	data:{productId: Number(0)},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$('#product-inline-popup').dialog("close");
				$('#temp-result').html(result); 
				$('.callJq').trigger('click');  
			}
		}); 
		return false; 
	}
	
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}
	function customRange(dates) {
		if (this.id == 'purchaseDate') {
			$('#expiryDate').datepick('option', 'minDate', dates[0] || null);
			if ($('#purchaseDate').val() != "") {
				addPeriods();
			}
		} else {
			$('#purchaseDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function callClearFields() {
		$('.supp-pop').show();
		$('.site-pop').show();
		$('#currencyId').attr('disabled', false);
		$('#quotationId').val("");
		$('#quotationNumber').val("");
		$('.prod').show();
		$('#currencyId').val("");
		$('#supplierId').val("");
		$('#siteId').val("");
		$('#supplierName').val("");
		$('#siteName').val("");
	}
	function triggerAddRow(rowId) {
		var itemNature = $('#itemnature_' + rowId).val();
		var productid = $('#productid_' + rowId).val();
		var unitrate = $('#unitrate_' + rowId).val();
		var quantity = $('#quantity_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (itemNature != null && itemNature != "" && productid != null
				&& productid != "" && unitrate != null && unitrate != ""
				&& quantity != null && quantity != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}
	function triggerAddRowMrp(rowId) {
		var itemNature = $('#itemnaturemrp_' + rowId).val();
		var productid = $('#productidmrp_' + rowId).val();
		var unitrate = $('#unitratemrp_' + rowId).val();
		var quantity = $('#quantitymrp_' + rowId).val();
		var nexttab = $('#fieldrowmrp_' + rowId).next();
		if (itemNature != null && itemNature != "" && productid != null
				&& productid != "" && unitrate != null && unitrate != ""
				&& quantity != null && quantity != ""
				&& $(nexttab).hasClass('lastrowmrp')) {
			$('#DeleteImagemrp_' + rowId).show();
			$('.addrowsmrp').trigger('click');
		}
	}
	function addPeriods() {
		var date = new Date($('#purchaseDate').datepick('getDate')[0].getTime());
		$.datepick.add(date, parseInt(365, 10), 'd');
		$('#expiryDate').val($.datepick.formatDate(date));
	}
	function calculateTotalPurchase() { 
		var totalPurchase = 0;
		$('#totalPurchaseAmount').text('');
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var totalamount = Number($jquery('#totalAmountH_' + rowId).val());
			totalPurchase = Number(totalPurchase + totalamount);
		});
		$jquery('.totalPurchaseAmountH').val(totalPurchase);
		$('#totalPurchaseAmount').text($('.totalPurchaseAmountH').val());
	}
	function personPopupResult(personid, personname, commonParam) {
		$('#purchaseManagerId').val(personid);
		$('#purchaseManager').val(personname);
		$('#common-popup').dialog("close");
	}
	function commonProductPopupMrp(aData, rowId){
		if(rowId==null || row==0)
			rowId=globalRowId;
		$('#productidmrp_' + rowId).val(aData.productId);
		$('#productmrp_' + rowId).html(aData.code +" - "+ aData.productName);
		$('#unitCodemrp_' + rowId).html(aData.unitCode);
		$('#storeNamemrp_' + rowId).html(aData.storeName);
		$('#shelfIdmrp_' + rowId).val(aData.shelfId);
		$('#storeIdmrp_' + rowId).val(aData.storeId);
		
		{
			getProductPackagings("packageTypemrp",aData.productId, rowId);
		}
		
		{
			if(aData.basePrice != null  && aData.basePrice > 0){
				$jquery('#unitratemrp_' + rowId).val(aData.basePrice);
				$('#basePurchasePricemrp_' + rowId).val(aData.basePrice);
				var quantity = Number($('#quantitymrp_'+rowId).val());
				var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
				if(quantity > 0 && unitrate>0){
					var totalAmount = Number(quantity * unitrate); 
					$jquery('#totalAmountHmrp_'+rowId).val(totalAmount);
					$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
				}else{
					$('#totalamountmrp_'+rowId).text('');
				}
	 		}else{
				getProductUnitRateMrp(aData.productId, rowId);
			}  
		} 
		
		$('#product-inline-popup').dialog("close");

		return false;
	}
	function commonProductPopup(productId, productName, rowId, itemSubType, aData) {
		
		if(rowId==null || row==0)
			rowId=globalRowId;
		
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.code +" - "+ aData.productName);
		$('#unitCode_' + rowId).html(aData.unitCode);
		$('#storeName_' + rowId).html(aData.storeName);
		$('#shelfId_' + rowId).val(aData.shelfId);
		$('#storeId_' + rowId).val(aData.storeId);
		$('#baseUnitConversion_'+rowId).text('');
		{
			getProductPackagings("packageType",aData.productId, rowId);
		}
		
		{
			if(aData.basePrice != null  && aData.basePrice > 0){
				$jquery('#unitrate_' + rowId).val(aData.basePrice);
				$('#basePurchasePrice_' + rowId).val(aData.basePrice);
				var quantity = Number($('#quantity_'+rowId).val());
				var unitrate = Number($jquery('#unitrate_'+rowId).val());
				if(quantity > 0 && unitrate>0){
					var totalAmount = Number(quantity * unitrate); 
					$jquery('#totalAmountH_'+rowId).val(totalAmount);
					$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
 				}else{
					$('#totalamount_'+rowId).text('');
				}
				calculateTotalPurchase();
			}else{
				getProductUnitRate(aData.productId, rowId);
			} 	
		}
		 
		$('.tab>tr').css("background-color", "#FFF");  
		$('.rowid').each(
			function() {
			var rowDId = getRowId($(this).attr('id'));
			var productDId = $('#productid_' + rowDId).val();
			if(productDId == productId && rowId!=rowDId){
				 $('#line-error').hide().html('Product Duplicated, Please Check.').slideDown(1000);
				 $('#line-error').hide().delay(2000).slideUp();
				 
				 $("#fieldrow_" + rowDId).css("background-color", "#FFF636");
				 $("#fieldrow_" + rowId).css("background-color", "#FFF636");
				 
				 return false;
			}
		});	 
		
		$('#product-inline-popup').dialog("close");

		return false;
	}
	
	function getProductPackagings(idName, productId, rowId){
		$('#'+idName+'_'+rowId).html('');
		$('#'+idName+'_'+rowId)
		.append(
				'<option value="-1">Select</option>');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.productPackageVOs != null){ 
					$.each(response.productPackageVOs, function (index) {
						if(response.productPackageVOs[index].productPackageId == -1){ 
							$('#'+idName+'_'+rowId)
							.append('<option value='
									+ response.productPackageVOs[index].productPackageId
									+ '>' 
									+ response.productPackageVOs[index].packageName
									+ '</option>');
						}else{ 
							var optgroup = $('<optgroup>');
				            optgroup.attr('label',response.productPackageVOs[index].packageName);
				            optgroup.css('color', '#c85f1f'); 
				             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
				                var option = $("<option></option>");
				                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
				                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
				                option.css('color', '#000'); 
				                option.css('margin-left', '10px'); 
				                optgroup.append(option);
				             });
				             $('#'+idName+'_'+rowId).append(optgroup);
						}  
					}); 
					$('#'+idName+'_'+rowId).multiselect('refresh'); 
 				} 
			} 
		});  
		return false;
	}
	
	function getProductBaseUnit(rowId){
		$('#baseUnitConversion_'+rowId).text('');
		var packageQuantity = Number($('#packageUnit_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		var basePrice = Number($('#basePurchasePrice_' + rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#quantity_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
				if(response.productPackagingDetailVO.basePrice != null && response.productPackagingDetailVO.basePrice > 0){
					$jquery('#unitrate_' + rowId).val(response.productPackagingDetailVO.basePrice);
				} 
 				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
 			} 
		});  
 		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		if(packageQuantity > 0 && unitrate>0){
			var totalAmount = Number(packageQuantity * unitrate); 
			$jquery('#totalAmountH_'+rowId).val(totalAmount);
			$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
		}else{
			$('#totalamount_'+rowId).text('');
		} 
		calculateTotalPurchase(); 
		return false;
	}
	
	function getProductConversionUnit(rowId){
 		var packageQuantity = Number($('#quantity_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
 			} 
		});   
		return false;
	}
	
	function getProductConversionUnitMrp(rowId){
		var packageQuantity = Number($('#quantitymrp_'+rowId).val());
		var productPackageDetailId = Number($('#packageTypemrp_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#packageUnitmrp_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
 			} 
		});   
		return false;
	}
	
	function getProductBaseUnitMrp(rowId){
		$('#baseUnitConversion_'+rowId).text('');
		var packageQuantity = Number($('#packageUnitmrp_'+rowId).val());
		var productPackageDetailId = Number($('#packageTypemrp_'+rowId).val());  
		var basePrice = Number($('#basePurchasePricemrp_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#quantitymrp_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseUnitConversionmrp_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
				if(response.productPackagingDetailVO.basePrice != null && response.productPackagingDetailVO.basePrice > 0){
					$jquery('#unitratemrp_' + rowId).val(response.productPackagingDetailVO.basePrice);
				} 
 				$('#baseDisplayQtymrp_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
			} 
		});  
		var quantity = Number($('#quantitymrp_'+rowId).val());
		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		if(quantity > 0 && unitrate>0){
			var totalAmount = Number(packageQuantity * unitrate); 
			$jquery('#totalAmountHmrp_'+rowId).val(totalAmount);
			$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
		}else{
			$('#totalamountmrp_'+rowId).text('');
		} 
		return false;
	}
	
	function getProductUnitRate(productId, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_unitrate.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.unitRate != null  && response.unitRate > 0){
					$jquery('#unitrate_' + rowId).val(response.unitRate); 
					$('#basePurchasePrice_' + rowId).val(response.unitRate); 
				} 
			} 
		});  
		var quantity = Number($('#quantity_'+rowId).val());
		var unitrate = Number($jquery('#unitrate_'+rowId).val());
		if(quantity > 0 && unitrate>0){
			var totalAmount = Number(quantity * unitrate); 
			$jquery('#totalAmountH_'+rowId).val(totalAmount);
			$('#totalamount_'+rowId).text($('#totalAmountH_'+rowId).val());
		}else{
			$('#totalamount_'+rowId).text('');
		}
		calculateTotalPurchase();

		return false;
	}
	function getProductUnitRateMrp(productId, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_unitrate.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.unitRate != null  && response.unitRate > 0){
					$jquery('#unitratemrp_' + rowId).val(response.unitRate); 
					$('#basePurchasePricemrp_' + rowId).val(response.unitRate); 
				} 
			} 
		});  
		var quantity = Number($('#quantitymrp_'+rowId).val());
		var unitrate = Number($jquery('#unitratemrp_'+rowId).val());
		if(quantity > 0 && unitrate>0){
			var totalAmount = Number(quantity * unitrate); 
			$jquery('#totalAmountHmrp_'+rowId).val(totalAmount);
			$('#totalamountmrp_'+rowId).text($('#totalAmountHmrp_'+rowId).val());
 		}else{
			$('#totalamountmrp_'+rowId).text('');
		} 
		return false;
	}
	function commonSupplierPopup(supplierId, supplierName,
			combinationId, accountCode, param){
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
		$('#termId').val(param.creditTermId);
		$('.hidemandatory').remove();
		$('.purchase-store-popup').remove();
		$('#continuous-div').show();
		return false;
	}
	function commonQuotationPopup(quotationId, currencyId,
			quotationNumber, supplierName, supplierId,creditTermId){
		$('#quotationId').val(quotationId);
		$('#currencyId').val(currencyId);
		$('#currencyId').attr('disabled', true);
		$('#quotationNumber').val(quotationNumber);
		$('#supplierName').val(supplierName);
		$('#supplierId').val(supplierId);
		$('#termId').val(creditTermId);
		$('#supplier').hide();
		$('.requisition-purchase-popup').remove();
		callQuotationDetails(quotationId);
	}
	function commonPORequisitionPopup(aData){
		$('#requisitionNumber').val(aData.referenceNumber);
		$('#requisitionId').val(aData.requisitionId);
		$('.quotation-common-popup').remove();
		callPORequisitionDetails(aData.requisitionId);
	}
	function callPORequisitionDetails(requisitionId){ 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getpurchase_requisition_detail.action",
			async : false,
			data : {
				requisitionId : requisitionId
			},
			dataType : "html",
			cache : false,
			success : function(result) {
				$('.tab').html(result); 
			} 
		});
	}
	function callQuotationDetails(quotationId){ 
		var rowId=Number(0);
		var  itemNature='D'; 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/showquotation_detail.action", 
			 	async: false,  
			 	data:{quotationId:quotationId, rowId:rowId, itemType:itemNature},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.quotation_detail').html(result);  
				} 
			});  
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>purchase
			order
		</div> 
		<form name="purchaseentry_details" id="purchaseentry_details" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="tempresult" style="display: none;"></div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
				<input type="hidden" id="purchaseId" name="purchaseId"
					value="${PURCHASE_INFO.purchaseId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 250px;">
							<div style="display: none;">
								<label class="width30" for="supplierName">Supplier
									Number </label> <input type="hidden"
									id="supplierId" name="supplierId"
									value="${PURCHASE_INFO.supplier.supplierId}" /> <input
									type="text" readonly="readonly" name="supplierNumber"
									id="supplierNumber"
									value="${PURCHASE_INFO.supplier.supplierNumber}"
									class="width50" />
							</div>
							<div>
								<label class="width30 tooltip">Supplier Name </label> <input type="text"
									name="supplierName"
									value="${PURCHASE_INFO.supplier.person.firstName} ${PURCHASE_INFO.supplier.person.lastName}${PURCHASE_INFO.supplier.cmpDeptLocation.company.companyName}${PURCHASE_INFO.supplier.company.companyName}"
									id="supplierName" class="width50" TABINDEX=6
									readonly="readonly"> <span class="button supp-pop">
									<a style="cursor: pointer;" id="supplier"
									class="btn ui-state-default ui-corner-all supplierpurchase-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="termId">Credit Terms</label> <select
									name="termId" id="termId" style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${PAYMENT_DETAIL ne null && PAYMENT_DETAIL ne ''}">
											<c:forEach var="pbean" items="${PAYMENT_DETAIL}">
												<option value="${pbean.creditTermId}">${pbean.name}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="temptermId" name="temptermId"
									value="${PURCHASE_INFO.creditTerm.creditTermId}" />
							</div>

							<div>
								<label class="width30" for="shippingTerm">Shipping Terms</label>
								<select name="shippingTerm" id="shippingTerm"
									style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${SHIPPING_TERMS ne null && SHIPPING_TERMS ne ''}">
											<c:forEach var="ship" items="${SHIPPING_TERMS}">
												<option value="${ship.lookupDetailId}">${ship.displayName}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="tempshippingTerm"
									name="tempshippingTerm"
									value="${PURCHASE_INFO.lookupDetail.lookupDetailId}" /> <span
									class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="SHIPPING_TERMS"
									class="btn ui-state-default ui-corner-all shipping-term-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>

							<div>
								<label class="width30" for="paymentMode">Payment Mode</label> <select
									name="paymentMode" id="paymentMode" style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
											<c:forEach var="payment" items="${MODE_OF_PAYMENT}">
												<option value="${payment.key}">${payment.value}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="temppaymentMode"
									name="temppaymentMode" value="${PURCHASE_INFO.modeOfPayment}" />
							</div>
							<div>
								<label class="width30">Contact</label> <input type="hidden"
									id="purchaseManagerId" name="purchaseManagerId"
									value="${PURCHASE_INFO.personByPurchaseManager.personId}" /> <input type="text"
									id="purchaseManager" name="purchaseManager" class="width50"
									value="${PURCHASE_INFO.personByPurchaseManager.firstName} ${PURCHASE_INFO.personByPurchaseManager.lastName}" />
								<span class="button"> <a style="cursor: pointer;"
									id="contactPerson"
									class="btn ui-state-default ui-corner-all contactpurchase-common-popup width100">
										<span class="ui-icon ui-icon-newwin"></span> </a> </span>
							</div>
							<div>
								<label class="width30" for="deliveryDate">Delivery Date</label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.deliveryDate ne null && PURCHASE_INFO.deliveryDate ne ''}">
										<c:set var="deliveryDate"
											value="${PURCHASE_INFO.deliveryDate}" />
										<%
											String deliveryDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"deliveryDate").toString());
										%>
										<input type="text" readonly="readonly" name="deliveryDate"
											id="deliveryDate" class="width50" value="<%=deliveryDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="deliveryDate"
											id="deliveryDate" class="width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Delivery Address</label>
								<textarea id="deliveryAddress" name="deliveryAddress"
									class="width50">${PURCHASE_INFO.deliveryAddress}</textarea>
							</div>
							<div class="width100 float-left ">									
							
								<div id="purchase_document_information" class="width90" style="cursor: pointer; color: blue;">
									<u>Upload Documents Here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px;" class="width90">
									<span id="purchaseDocs"></span>
								</div>
								
							</div>		
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 250px;">
							<div>
								<label class="width30" for="purchaseNumber">Purchase
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.purchaseId ne null && PURCHASE_INFO.purchaseId ne ''}">
										<input type="text" readonly="readonly" name="purchaseNumber"
											id="purchaseNumber" class="width50 validate[required]"
											value="${PURCHASE_INFO.purchaseNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="purchaseNumber"
											id="purchaseNumber" class="width50 validate[required]"
											value="${PURCHASE_NUMBER}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="quotationNumber">Quotation</label> <input
									type="hidden" id="quotationId" name="quotationId"
									value="${PURCHASE_INFO.quotation.quotationId}" /> <input
									type="text" readonly="readonly" name="quotationNumber"
									id="quotationNumber" class="width50"
									value="${PURCHASE_INFO.quotation.quotationNumber}" />
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.purchaseId eq null || PURCHASE_INFO.purchaseId ne '' && PURCHASE_INFO.purchaseId eq 0}">
										<span class="button"> <a style="cursor: pointer;"
											id="quotation"
											class="btn ui-state-default ui-corner-all quotation-common-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</c:when>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="requisitionNumber">Requisition</label> <input
									type="hidden" id="requisitionId" name="requisitionId"
									value="${PURCHASE_INFO.requisition.requisitionId}" /> <input
									type="text" readonly="readonly" name="requisitionNumber"
									id="requisitionNumber" class="width50"
									value="${PURCHASE_INFO.requisition.referenceNumber}" />
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.purchaseId eq null || PURCHASE_INFO.purchaseId ne '' && PURCHASE_INFO.purchaseId eq 0}">
										<span class="button"> <a style="cursor: pointer;"
											id="quotation"
											class="btn ui-state-default ui-corner-all requisition-purchase-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</c:when>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="currencyId">Currency<span
									class="mandatory">*</span> </label> <select name="currencyId"
									id="currencyId" class="validate[required]" style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${requestScope.CURRENCY_DETAIL ne null && requestScope.CURRENCY_DETAIL ne ''}">
											<c:forEach var="bean" items="${CURRENCY_DETAIL}">
												<option value="${bean.currencyId}">${bean.currencyPool.code}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="default_currency"
									name="default_currency" value="${DEFAULT_CURRENCY}" /> <input
									type="hidden" id="tempcurrencyId" name="tempcurrencyId"
									value="${PURCHASE_INFO.currency.currencyId}" />
							</div>
							<div>
								<label class="width30" for="purchaseDate">Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.date ne null && PURCHASE_INFO.date ne ''}">
										<c:set var="date" value="${PURCHASE_INFO.date}" />
										<%
											String date = DateFormat.convertDateToString(pageContext
															.getAttribute("date").toString());
										%>
										<input type="text" readonly="readonly" name="purchaseDate"
											id="purchaseDate" class="width50 validate[required]"
											value="<%=date%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="purchaseDate"
											id="purchaseDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="expiryDate">Expiry Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.expiryDate ne null && PURCHASE_INFO.expiryDate ne ''}">
										<c:set var="expiryDate" value="${PURCHASE_INFO.expiryDate}" />
										<%
											String expiryDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"expiryDate").toString());
										%>
										<input type="text" readonly="readonly" name="expiryDate"
											id="expiryDate" class="width50 validate[required]"
											value="<%=expiryDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="expiryDate"
											id="expiryDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div style="display: none;" id="continuous-div">
								<label class="width30" for="continuousPurchase">Continuous PO</label>  
								<c:choose>
									<c:when test="${PURCHASE_INFO.continuousPurchase ne null && PURCHASE_INFO.continuousPurchase eq true}">
										<input type="checkbox" name="continuousPurchase" id="continuousPurchase" class="width3" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="continuousPurchase" id="continuousPurchase" class="width3" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Status</label> <select name="status"
									id="status" class="validate[required]" style="width: 51%;">
									<option>Select</option>
									<c:forEach var="poStatus" items="${PO_STATUS}">
										<option value="${poStatus.key}">${poStatus.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempstatus" name="tempstatusId"
									value="${PURCHASE_INFO.status}" />
							</div>
							<div>
								<label class="width30 tooltip">Description</label>
								<textarea id="description" class="width50">${PURCHASE_INFO.description}</textarea>
							</div>
							
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Purchase Detail<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all"
						style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all"
						style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
					<div id="hrm" class="hastable width100"> 
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Item Type</th>
									<th style="width: 10%">Product</th>
									<th style="width: 8%">Store<span 
										class="mandatory hidemandatory">*</span></th>
									<th style="display: none;">UOM</th>
									<th style="width: 8%;">Packaging</th> 
									<th style="width: 5%">Quantity</th>
									<th style="width: 4%">Base Qty</th>
									<th style="width: 5%">Unit Rate</th>
									<th style="width: 4%">Total Amount</th>
									<th style="width: 8%">Description</th>
									<th style="width: 2%">MORE</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail" items="${PURCHASE_DETAIL_INFO}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
										<td id="itemtype_${status.index+1}"><select
											name="itemnature" class="width98 itemnature"
											id="itemnature_${status.index+1}">
												<option value="A">Asset</option>
												<option value="E">Expense</option>
												<option value="I">Inventory</option>
										</select> <input type="hidden" id="tempitemnature_${status.index+1}"
											name="itemnature" value="${detail.product.itemType}" /></td>
										<td><span class="float-left width60"
											id="product_${status.index+1}">${detail.product.code} - ${detail.product.productName}</span>
											<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${detail.product.lookupDetailByProductUnit.accessCode}</span>
											<span class="button float-right prod quoteprod"
											style="height: 16px;"> <a
												style="cursor: pointer; height: 100%;"
												id="productline_${status.index+1}"
												class="btn ui-state-default ui-corner-all purchaseproduct-common-popup width100 float-right">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" id="productid_${status.index+1}"
											value="${detail.product.productId}"  />
										</td>
										<td>
										<span class="float-left width60" id="storeName_${status.index+1}"> 
											<c:if test="${detail.shelf ne null && detail.shelf ne ''}">
												${detail.shelf.shelf.aisle.store.storeName} >> ${detail.shelf.shelf.aisle.sectionName} >> ${detail.shelf.shelf.name} >> ${detail.shelf.name}
											</c:if> 
										</span>
											<input type="hidden" name="storeId"
											id="storeId_${status.index+1}"
											value="" /> 
											<input type="hidden" name="shelfId"
											id="shelfId_${status.index+1}"
											value="${detail.shelf.shelfId}" /> 
											<span
											class="button float-right"> <a
												style="cursor: pointer;" id="prodIDStr_${status.index+1}"
												class="btn ui-state-default ui-corner-all purchase-store-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										</td>
										<td id="uom_${status.index+1}" class="uom"
											style="display: none;"></td>
										<td>
											<select name="packageType" id="packageType_${status.index+1}" class="packageType">
												<option value="-1">Select</option>
												<c:forEach var="packageType" items="${detail.productPackageVOs}">
													<c:choose>
														<c:when test="${packageType.productPackageId gt 0}">
															<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
															 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
															 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
															 	</c:forEach> 
															 </optgroup>
														</c:when> 
													</c:choose> 
												</c:forEach>
											</select>
											<input type="hidden" id="temppackageType_${status.index+1}" value="${detail.packageDetailId}"/>
										</td>
										<td><input type="text"
											class="width96 packageUnit validate[optional,custom[number]]"
											id="packageUnit_${status.index+1}" value="${detail.packageUnit}" /> 
										</td> 
										<td><input type="hidden"
											class="width70 quantity validate[optional,custom[number]]"
											id="quantity_${status.index+1}" value="${detail.quantity}" />
											<span id="baseUnitConversion_${status.index+1}" class="width10" 
												style="display: none;">${detail.baseUnitName}</span>
											<span id="baseDisplayQty_${status.index+1}">${detail.baseQuantity}</span>
										</td>
										<td><input type="text"
											class="width96 unitrate validate[optional,custom[number]]"
											id="unitrate_${status.index+1}" value="${detail.unitRate}"
											style=" text-align: right;" />
											<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${status.index+1}" 
												class="totalAmountH" value="${detail.unitRate * detail.packageUnit}"/>
											<input type="hidden" id="basePurchasePrice_${status.index+1}"/>
										</td>
										<td class="totalamount" id="totalamount_${status.index+1}"
											style="text-align: right;"></td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status.index+1}"
											value="${detail.description}" /></td>
										<td style="display: none;"><input type="hidden"
											id="purchaseLineId_${status.index+1}" 
											value="${detail.purchaseDetailId}" />
											<input type="hidden" id="requisitionLineId_${status.index+1}" 
												value="${detail.requisitionDetail.requisitionDetailId}"/>
											<input type="text" value="${detail.batchNumber}"
											class="width96 batchNumber" id="batchNumber_${status.index+1}" /> <input
											type="text" readonly="readonly" class="width96 expiryBatchDate"
											id="expiryBatchDate_${status.index+1}" value="${detail.expiryDate}"/>
										</td>
										<td>
											<div id="moreoption_${status.index+1}" style="cursor: pointer; text-align: center; background-color: #3ADF00;" 
												class="portlet-header ui-widget-header float-left moreoption">
												MORE OPTION
											</div>
										</td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a></td>
									</tr>
								</c:forEach>
								<c:forEach var="i" begin="${fn:length(PURCHASE_DETAIL_INFO)+1}"
									end="${fn:length(PURCHASE_DETAIL_INFO)+2}" step="1"
									varStatus="status1">
									<tr class="rowid" id="fieldrow_${i}">
										<td id="lineId_${i}" style="display: none;">${i}</td>
										<td><select name="itemnature" class="width98 itemnature"
											id="itemnature_${i}">
												<option value="A">Asset</option>
												<option value="E">Expense</option>
												<option value="I" selected="selected">Inventory</option>
										</select> <input type="hidden" id="tempitemnature_${i}"
											name="itemnature" /></td>
										<td><span class="float-left width60" id="product_${i}"></span>
											<span class="width10 float-right" id="unitCode_${i}"
												 	 style="position: relative;"></span>
											<span class="button float-right prod" style="height: 16px;">
												<a style="cursor: pointer; cursor: pointer; height: 100%;"
												id="productline_${i}"
												class="btn ui-state-default ui-corner-all purchaseproduct-common-popup width100 float-right">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" id="productid_${i}"  /></td>
										<td>
										<span class="float-left width60" id="storeName_${i}"></span>
												<input type="hidden" name="storeId"
												id="storeId_${i}"
												value="" /> 
												<input type="hidden" name="shelfId"
												id="shelfId_${i}"
												 /> 
												<span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodIDStr_${i}"
													class="btn ui-state-default ui-corner-all purchase-store-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										</td>
										<td id="uom_${i}" class="uom" style="display: none;"></td>
										<td>
											<select name="packageType" id="packageType_${i}" class="packageType">
												<option value="">Select</option>
											</select> 
										</td> 
										<td><input type="text"
											class="width96 packageUnit validate[optional,custom[number]]"
											id="packageUnit_${i}"  />
 										</td>
										<td><input type="hidden"
											class="width70 quantity validate[optional,custom[number]]"
											id="quantity_${i}"  />
											<span id="baseUnitConversion_${i}" class="width10" style="display: none;"></span>
											<span id="baseDisplayQty_${i}"></span> 
 										</td>
										<td><input type="text"
											class="width96 unitrate validate[optional,custom[number]]"
											id="unitrate_${i}" style=" text-align: right;" />
											<input type="text" style="display: none;" name="totalAmountH" readonly="readonly" id="totalAmountH_${i}" 
												class="totalAmountH"/> 
											<input type="hidden" id="basePurchasePrice_${i}"/>
										</td>
										<td class="totalamount" id="totalamount_${i}"
											style="text-align: right;"></td>
										<td><input type="text" name="linedescription"
											id="linedescription_${i}" /></td>
										<td style="display: none;"><input type="hidden"
											id="purchaseLineId_${i}"  />
											<input type="hidden" id="requisitionLineId_${i}"/>
											<input type="text"
											class="width96 batchNumber" id="batchNumber_${i}" /> <input
											type="text" readonly="readonly" class="width96 expiryBatchDate"
											id="expiryBatchDate_${i}" />
										</td>
										<td>
											<div id="moreoption_${i}" style="cursor: pointer; text-align: center; background-color: #3ADF00;" 
												class="portlet-header ui-widget-header float-left moreoption">
												MORE OPTION
											</div>
										</td>
										<td style="width: 0.01%;" class="opn_td" id="option_${i}">
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${i}" style="display: none; cursor: pointer;"
											title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${i}" style="display: none; cursor: pointer;"
											title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${i}" style="display: none; cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${i}" style="display: none;" title="Working">
												<span class="processing"></span> </a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="float-right width30" style="font-weight: bold;">
				<span style="font-weight: bold;">Total: </span> <span
					id="totalPurchaseAmount"></span>
				<input type="text" style="display: none;" name="totalPurchaseAmountH" readonly="readonly" class="totalPurchaseAmountH"/> 
			</div>
			<div class="clearfix"></div>

			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right pdiscard"
					id="discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrowsmrp"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="product-inline-popup" style="display: none;"
				class="width100 float-left" id="hrm">
				
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div id="temp-result" style="display: none;"></div>
			<span class="callJq"></span>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>