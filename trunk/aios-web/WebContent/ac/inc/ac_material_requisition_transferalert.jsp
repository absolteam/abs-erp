<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var transferPerson = Number(0);
var materialTransferId = Number(0);
var slidetab = "";
var transferDetails = [];
var detailRowId = 0;
var accessCode = "";
var storeAccess ="";
var storeId = 0;
$(function(){
	 
	$jquery("#materialTransferValidation").validationEngine('attach');   

	$('#transferDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	
	 $('#material_transfer_discard').click(function(event){  
		 materialTransferDiscard("");
		 return false;
	 });

	 $('#material_transfer_save').click(function(){
		 transferDetails = [];
		 requisitionDetails = [];
		 if($jquery("#materialTransferValidation").validationEngine('validate')){
 		 		
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var transferDate = $('#transferDate').val(); 
 	 			var description=$('#description').val();  
 	 			var materialTransferId = Number($('#materialTransferId').val());
 	 			transferDetails = getTransferDetails();  
 	 	 		var checkStore = validateTransfers(); 
 	 	 		if(checkStore == true){
 	 	 			if(transferDetails!=null && transferDetails!=""){
 		 				$.ajax({
 		 					type:"POST",
 		 					url:"<%=request.getContextPath()%>/save_material_transfer.action", 
 		 				 	async: false, 
 		 				 	data:{	materialTransferId: materialTransferId, referenceNumber: referenceNumber, transferDate: transferDate, 
 		 				 			transferPerson: transferPerson, description: description, transferDetails: JSON.stringify(transferDetails),
 		 				 			storeId: storeId
 		 					 	 },
 		 				    dataType: "json",
 		 				    cache: false,
 		 					success:function(response){   
 		 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
 		 							materialTransferDiscard(materialTransferId > 0 ? "Record updated.":"Record created.");
 		 						 else{
 		 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
 		 							 $('#page-error').delay(2000).slideUp();
 		 							 return false;
 		 						 }
 		 					},
 		 					error:function(result){  
 		 						$('#page-error').hide().html("Internal error.").slideDown(1000);
 		 						$('#page-error').delay(2000).slideUp();
 		 					}
 		 				}); 
 		 			}else{
 		 				$('#page-error').hide().html("Please enter material transfer details.").slideDown(1000);
 		 				$('#page-error').delay(2000).slideUp();
 		 				return false;
 		 			}
 	 	 		}else{
 	 	 			$('#page-error').hide().html("Product can't be transfered in same shelf.").slideDown(1000);
		 			$('#page-error').delay(2000).slideUp();
		 			return false;
 	 	 		} 
	 		}else{
	 			return false;
	 		}
	 	}); 

	 var validateTransfers = function(){
		 var checkStore = true;
		 $('.rowid').each(function(){ 
			 var rowId = getRowId($(this).attr('id'));  
			 var storeFromId = Number($('#storeFromId_'+rowId).val());  
			 var shelfId = Number($('#shelfId_'+rowId).val());  
			 if(storeFromId > 0 && shelfId > 0 && storeFromId == shelfId){
				 checkStore = false;
				 return checkStore;
			 }
		 });
		 return checkStore;
	 };
	 
	  var getTransferDetails = function(){
		  transferDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val()); 
				var storeFromId = Number($('#storeFromId_'+rowId).val());  
				var shelfId = Number($('#shelfId_'+rowId).val()); 
 				var productQty = Number($('#productQty_'+rowId).val()); 
				var amount = Number($('#amount_'+rowId).val()); 
				var transferType = Number($('#transferType_'+rowId).val()); 
				var transferReason = Number($('#transferReason_'+rowId).val()); 
				var linesDescription = $.trim($('#linesDescription_'+rowId).val());  
				var materialTransferDetailId =  Number($('#materialTransferDetailId_'+rowId).val()); 
				var materialRequisitionDetailId =  Number($('#materialRequisitionDetailId_'+rowId).val()); 
				if(typeof productId != 'undefined' && productId > 0 && shelfId > 0 && productQty > 0
						 && transferType > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"transferType" : transferType,
						"transferReason" : transferReason,
						"storeFromId" : storeFromId,
						"shelfId" : shelfId,
						"productQty" : productQty,
						"amount": amount,
						"linesDescription": linesDescription,
						"materialTransferDetailId": materialTransferDetailId,
						"materialRequisitionDetailId" : materialRequisitionDetailId
					});   
					transferDetails.push({
						"transferDetails" : jsonData
					});
				} 
			});  
			return transferDetails;
		 };

	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#product-stock-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 });

	 $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });

	 $('.material_product_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_stock_popup.action", 
			 	async: false,  
			 	data: {itemType: 'I',rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('.store-shelf-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));  
			var storeRId = Number($('#storeRId').val());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_materialtransfer.action", 
			 	async: false,  
			 	data:{rowId : rowId, storeId: storeRId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
 					$('.store-result').html(result);   
					$($($('#common-popup').parent()).get(0)).css('top',0);
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});

		$('#store-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});   

	 $('.store-from-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
			var productId = Number($('#productid_'+rowId).val());
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_store_product_popup.action", 
				 	async: false,   
				    dataType: "html",
				    data: {rowId: rowId, productId: productId},
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);   
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0); 
						$(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('z-index',100000);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				return false;
			});

	 $('.reqtransfer-store-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
			 	async: false,  
			 	data: {rowId: Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
		
	 
	 
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	 $('.transfer-type-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        detailRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	  				  $($($('#common-popup').parent()).get(0)).css('z-index',100000);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

	 $('.transfer-reason-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        detailRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	  				  $($($('#common-popup').parent()).get(0)).css('z-index',100000);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

	 if(Number($('#materialTransferId').val())>0){
		 transferPerson = '${MATERIAL_TRANSFER.personByTransferPerson.personId}';
	 }else{
		 transferPerson = '${requestScope.transferPerson}';
	 }  
	 
	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="MATERIAL_TRANSFER_TYPE"){
	 			$('#transferType_'+detailRowId).html("");
				$('#transferType_'+detailRowId).append("<option value=''>Select</option>");
				loadLookupList("transferType_"+detailRowId); 
			}  
	 		else if(accessCode=="MATERIAL_TRANSFER_REASON"){
	 			$('#transferReason_'+detailRowId).html("");
				$('#transferReason_'+detailRowId).append("<option value=''>Select</option>");
				loadLookupList("transferReason_"+detailRowId); 
			}  
		});  

	 $('.transfer-person-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			var personTypes="1";
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data: {personTypes: personTypes},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#transferPerson').val(personname);
	transferPerson = personid;
	$('#common-popup').dialog("close");
}
function commonProductPopup(aData, rowId) {
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName);
 	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
	$('#productQty_' + rowId).val(aData.availableQuantity);
	$('#amount_' + rowId).val(aData.unitRate); 
	$('#storeFromId_' + rowId).val(aData.shelfId);
	$('.amount').trigger('change');
}
function commonProductStockPopup(aData, rowId) {
	$('#productQty_' + rowId).val('');
  	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
  	if(aData.availableQuantity <= Number($('#requisitionQty_' + rowId).val()))
		$('#productQty_' + rowId).val(aData.availableQuantity);
	$('#amount_' + rowId).val(aData.unitRate); 
	$('#storeFromName_' + rowId).text(aData.storeName);
	$('#storeFromId_' + rowId).val(aData.shelfId);
	$('.amount').trigger('change');
} 
function materialTransferDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_material_transfers.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();
			$('#store-popup').dialog('destroy');		
			$('#store-popup').remove();  
			$("#main-wrapper").html(result);
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
		triggerAddRow(detailRowId);
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material
			transfer
		</div> 
		<form name="materialTransferValidation"
			id="materialTransferValidation" style="position: relative;">
			<input type="hidden" id="materialTransferId" name="materialTransferId" />
			<input type="hidden" id="alertId" name="alertId" value="${requestScope.alertId}" />
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30" for="searchLocation">Store </label> <input
									type="text" readonly="readonly" name="store" id="store"
									class="width50" value="${MATERIAL_REQUISITION.storeName}"/>  
									<input type="hidden" id="storeRId" value="${MATERIAL_REQUISITION.store.storeId}"/>
							</div>
							<div>
								<label class="width30" for="description"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${MATERIAL_REQUISITION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
							</div>
							<div>
								<label class="width30" for="transferDate"> Transfer Date<span
									style="color: red;">*</span> </label>
								<input name="transferDate" type="text" readonly="readonly"
									id="transferDate" class="transferDate validate[required] width50">
							</div>
							<div>
								<label class="width30" for="transferPerson">Transfer
									Person<span style="color: red;">*</span> </label> 
									<input type="text" readonly="readonly" name="transferPerson" id="transferPerson" 
												class="width50 validate[required]" value="${personName}"/> 
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all transfer-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="requisition-info" id="hrm">
					<div class="portlet-content class90" id="hrm"
						style="margin-top: 10px;">
						<fieldset>
							<legend>
								Material Transfer Detail<span class="mandatory">*</span>
							</legend>
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th class="width10">Product</th>
											<th class="width5">Store</th>
											<th class="width5">Requisition Quantity</th>
											<th class="width5">Transfer Quantity</th>
											<th class="width10">Transfer Type</th>
											<th class="width10" style="display: none;">Reason</th>
											<th class="width10">Transfer Shelf</th> 
											<th class="width5" style="display: none;">Price</th>
											<th class="width5" style="display: none;" >Total</th>
											<th class="width10"><fmt:message
													key="accounts.journal.label.desc" /></th>
											<th style="width: 1%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:forEach var="DETAIL"
											items="${MATERIAL_REQUISITION.materialRequisitionDetailVOs}"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
												<td><input type="hidden" name="productId"
													id="productid_${status.index+1}"
													value="${DETAIL.product.productId}" /> <span
													id="product_${status.index+1}">${DETAIL.product.productName}</span>
													<span class="button float-right"> <a
														style="cursor: pointer;" id="prodID_${status.index+1}"
														class="btn ui-state-default ui-corner-all store-from-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td>
													<input type="hidden" id="storeFromId_${status.index+1}"/>
													<span id="storeFromName_${status.index+1}"></span>
												</td>
												<td><input type="hidden" name="requisitionQty"
													id="requisitionQty_${status.index+1}"
													value="${DETAIL.quantity}"
													class="requisitionQty"> 
													<span>${DETAIL.quantity}</span>
												</td>
												<td><input type="text" name="productQty"
													id="productQty_${status.index+1}" class="productQty validate[optional,custom[number]] width80">
													<input type="hidden" name="totalAvailQty"
													id="totalAvailQty_${status.index+1}">
												</td>
												<td style="display: none;"><input type="hidden"
													name="storeFromId" id="storeFromId_${status.index+1}" />
												</td>
												<td ><select name="transferType"
													id="transferType_${status.index+1}"
													class="width70 transferType">
														<option value="">Select</option>
														<c:forEach var="transferType" items="${TRANSFER_TYPE}">
															<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
														</c:forEach>
												</select>  
													<span class="button" style="position: relative;"> <a
														style="cursor: pointer;"
														id="MATERIAL_TRANSFER_TYPE_${status.index+1}"
														class="btn ui-state-default ui-corner-all transfer-type-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td style="display: none;"><select name="transferReason"
													id="transferReason_${status.index+1}"
													class="width70 transferReason">
														<option value="">Select</option>
														<c:forEach var="transferReason" items="${TRANSFER_REASON}">
															<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
														</c:forEach>
												</select> 
													<span class="button" style="position: relative;"> <a
														style="cursor: pointer;"
														id="MATERIAL_TRANSFER_REASON_${status.index+1}"
														class="btn ui-state-default ui-corner-all transfer-reason-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="hidden" name="shelfId"
													id="shelfId_${status.index+1}"/> <span
													id="shelf_${status.index+1}"></span>
													<span class="button float-right"> <a
														style="cursor: pointer;" id="shelfID_${status.index+1}"
														class="btn ui-state-default ui-corner-all store-shelf-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td> 
												<td style="display: none;"><input type="text" name="amount"
													id="amount_${status.index+1}" 
													style="text-align: right;" readonly="readonly"
													class="amount width98 validate[optional,custom[number]] right-align">
												</td>
												<td style="display: none;"><c:set var="totalAmount"/> <span
													id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
												</td>
												<td><input type="text" name="linesDescription"
													id="linesDescription_${status.index+1}"
													value="${DETAIL.description}" class="width98 linesDescription"
													maxlength="150">
												</td>
												<td style="width: 1%;" class="opn_td"
													id="option_${status.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${status.index+1}"
													style="cursor: pointer; display: none;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${status.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${status.index+1}" style="cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${status.index+1}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="materialTransferDetailId"
													id="materialTransferDetailId_${status.index+1}"/>
													<input value="${DETAIL.materialRequisitionDetailId}"
													type="hidden" name="materialRequisitionDetailId"
													id="materialRequisitionDetailId_${status.index+1}"/>
												</td>
											</tr>
										</c:forEach>
										 
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div> 
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="material_transfer_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="material_transfer_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<span class="callJq"></span>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>