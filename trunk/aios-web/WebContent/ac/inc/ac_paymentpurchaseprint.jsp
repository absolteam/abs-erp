<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>PURCHASE PAYMENT VOUCHER</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PURCHASE PAYMENT VOUCHER</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width30 text-bold">VOUCHER<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${PAYMENT_INFO_PRINT.paymentNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width30 text-bold">DATE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="date" value="${PAYMENT_INFO_PRINT.date}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
				</span>  		
			</div> 
		</div> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width30 text-bold">NAME <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:choose>
						<c:when test="${PAYMENT_INFO_PRINT.supplier.person ne null && PAYMENT_INFO_PRINT.supplier.person ne ''}">
							${PAYMENT_INFO_PRINT.supplier.person.firstName} ${PAYMENT_INFO_PRINT.supplier.person.lastName} 
							 <c:set var="supplierType" value="Person"/>
						</c:when>
						<c:when test="${PAYMENT_INFO_PRINT.supplier.company ne null && PAYMENT_INFO_PRINT.supplier.company ne ''}">
							${PAYMENT_INFO_PRINT.supplier.company.companyName}
							 <c:set var="supplierType" value="Company"/>
						</c:when>
						<c:otherwise>
							${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyName}
							<c:set var="supplierType" value="Company"/>
						</c:otherwise>
					</c:choose>
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width30 text-bold">ADDRESS <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:choose> 
						<c:when test="${PAYMENT_INFO_PRINT.supplier.person ne null && PAYMENT_INFO_PRINT.supplier.person ne ''}">
							<c:choose> 
								<c:when test="${PAYMENT_INFO_PRINT.supplier.person.postboxNumber  ne null && PAYMENT_INFO_PRINT.supplier.person.postboxNumber ne '' }">
									P.O.Box: ${PAYMENT_INFO_PRINT.supplier.person.postboxNumber}
									<c:choose>
										<c:when test="${PAYMENT_INFO_PRINT.supplier.person.residentialAddress  ne null && PAYMENT_INFO_PRINT.supplier.person.residentialAddress ne '' }">
											, ${PAYMENT_INFO_PRINT.supplier.person.residentialAddress}  
										</c:when>	
									</c:choose>
								</c:when> 
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:otherwise> 
							<c:choose> 
								<c:when test="${PAYMENT_INFO_PRINT.supplier.company ne null && PAYMENT_INFO_PRINT.supplier.company ne ''}">
									P.O.Box: ${PAYMENT_INFO_PRINT.supplier.company.companyPobox}
									<c:choose>
										<c:when test="${PAYMENT_INFO_PRINT.supplier.company.companyAddress ne null && PAYMENT_INFO_PRINT.supplier.company.companyAddress ne ''}">
											, ${PAYMENT_INFO_PRINT.supplier.company.companyAddress} 
										</c:when> 
									</c:choose>
								</c:when>
								<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox ne ''}">
									P.O.Box: ${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox}
									<c:choose>
										<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress ne ''}">
											, ${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress} 
										</c:when> 
									</c:choose>
								</c:when> 
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:otherwise>	 
					</c:choose>			
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width30 text-bold">MOBILE NO <span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:choose>
						<c:when test="${PAYMENT_INFO_PRINT.supplier.person ne null && PAYMENT_INFO_PRINT.supplier.person ne ''}">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.supplier.person.mobile ne null && PAYMENT_INFO_PRINT.supplier.person.mobile ne ''}">
									${PAYMENT_INFO_PRINT.supplier.person.mobile} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose> 
						</c:when>
						<c:when test="${PAYMENT_INFO_PRINT.supplier.company ne null && PAYMENT_INFO_PRINT.supplier.company ne ''}">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.supplier.company.companyPhone ne null && PAYMENT_INFO_PRINT.supplier.company.companyPhone ne ''}">
									${PAYMENT_INFO_PRINT.supplier.company.companyPhone} 
								</c:when> 
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company ne ''}">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone ne ''}">
									${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone} 
								</c:when> 
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose>
				</span>
				</div>
				 
				<c:choose>  
				<c:when test="${PAYMENT_INFO_PRINT.bankAccount ne null && PAYMENT_INFO_PRINT.bankAccount ne ''}">
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">BANK<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${PAYMENT_INFO_PRINT.bankAccount.bank.bankName}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">ACCOUNT NO.<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${PAYMENT_INFO_PRINT.bankAccount.accountNumber}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CHEQUE NO.<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.chequeNumber ne null && PAYMENT_INFO_PRINT.chequeNumber ne ''}">
									${PAYMENT_INFO_PRINT.chequeNumber}
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CHEQUE DATE<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.chequeDate ne null && PAYMENT_INFO_PRINT.chequeDate ne ''}">
									<c:set var="chequeDate" value="${PAYMENT_INFO_PRINT.chequeDate}"/>  
									<%=DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString())%> 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</c:when>
				<c:otherwise>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CASH ACCOUNT<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.combination.accountByAnalysisAccountId ne null &&
									PAYMENT_INFO_PRINT.combination.accountByAnalysisAccountId ne ''}">
										${PAYMENT_INFO_PRINT.combination.accountByNaturalAccountId.account}.${PAYMENT_INFO_PRINT.combination.accountByAnalysisAccountId.account}
								</c:when>
								<c:otherwise>
									${PAYMENT_INFO_PRINT.combination.accountByNaturalAccountId.account}
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</c:otherwise>
			</c:choose> 
			<div class="width100 div_text_info">
				<c:choose>
					<c:when test="${PAYMENT_INFO_PRINT.supplier.person ne null && PAYMENT_INFO_PRINT.supplier.person ne ''}">
						<c:choose>
							<c:when test="${PAYMENT_INFO_PRINT.supplier.person.telephone ne null && PAYMENT_INFO_PRINT.supplier.person.telephone ne ''}">
							 	<span class="float_left left_align width30 text-bold">TEL NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${PAYMENT_INFO_PRINT.supplier.person.telephone}</span>
							</c:when>
							<c:otherwise>
								<span class="float_left left_align width30 text-bold">TEL NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose> 
					</c:when>
					<c:when test="${PAYMENT_INFO_PRINT.supplier.company ne null && PAYMENT_INFO_PRINT.supplier.company ne ''}">
						<c:choose>
							<c:when test="${PAYMENT_INFO_PRINT.supplier.company.companyFax ne null && PAYMENT_INFO_PRINT.supplier.company.companyFax ne ''}">
								<span class="float_left left_align width30 text-bold">FAX NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${PAYMENT_INFO_PRINT.supplier.company.companyFax}</span>
							</c:when> 
							<c:otherwise>
								<span class="float_left left_align width30 text-bold">FAX NO <span class="float_right">:</span></span> 
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>	
					</c:when>  
					<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company ne ''}">
						<c:choose>
							<c:when test="${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax ne null && PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax ne ''}">
								<span class="float_left left_align width30 text-bold">FAX NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${PAYMENT_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax}</span>
							</c:when> 
							<c:otherwise>
								<span class="float_left left_align width30 text-bold">FAX NO <span class="float_right">:</span></span> 
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>	
					</c:when>  
				</c:choose> 
			</div> 
		</div> 
	</div>
	<div style=" position:relative; clear: both; top: 10px; margin-bottom:10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<table class="width100">
			<tr> 
				<th style="width:2%;">S.NO:</th>
				<th class="width10">INVOICE NO</th>
				<th class="width30">DESCRIPTION</th> 
				<th class="width10">QUANTITY</th>
				<th class="width10">AMOUNT</th> 
			</tr>
			<tbody>  
				<c:choose>
					<c:when test="${PAYMENT_INVOICE_INFO_PRINT ne null && PAYMENT_INVOICE_INFO_PRINT ne ''}">
						<c:set var="totalAmount" value="0"/>
						<tr style="height: 25px;">
							<c:forEach begin="0" end="4" step="1">
								<td/>
							</c:forEach>
						</tr>
						<c:forEach items="${PAYMENT_INVOICE_INFO_PRINT}" var="INVOICE_DETAIL" varStatus="status"> 
							<tr>
								<td>${status.index+1}</td>
								<td>${INVOICE_DETAIL.invoiceNumber}</td>
								<td>${INVOICE_DETAIL.description}</td>
								<td>
									<c:set var="totalInvoiceQty" value="${INVOICE_DETAIL.totalInvoiceQty}"/> 
									<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalInvoiceQty"))%>  
								</td> 
								<td  class="right_align">
									<c:set var="totalInvoiceAmount" value="${INVOICE_DETAIL.totalInvoiceAmount}"/>
									<c:set var="totalAmount" value="${(totalAmount)+INVOICE_DETAIL.totalInvoiceAmount}"/>
									<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalInvoiceAmount"))%>  
								</td> 
							</tr>
						</c:forEach> 
						<c:if test="${fn:length(PAYMENT_INFO_PRINT.paymentDetails)<5}">
							<c:set var="emptyLoop" value="${5 - fn:length(PAYMENT_INFO_PRINT.paymentDetails)}"/> 
							<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
								<tr style="height: 25px;">
									<c:forEach begin="0" end="4" step="1">
										<td/>
									</c:forEach>
								</tr>
							</c:forEach>
						</c:if> 
						<tr>
							<td colspan="4" class="bottomTD left_align" style="font-weight: bold;">TOTAL AMOUNT</td> 
							<td class="right_align bottomTD" style="font-weight: bold;">
								<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount")) %> 
							</td>
						</tr>
						<tr>
							<c:choose>
								<c:when test="${PAYMENT_INFO_PRINT.discountReceived ne null && PAYMENT_INFO_PRINT.discountReceived ne '' }">
									  <c:set var="discountAmount" value="${PAYMENT_INFO_PRINT.discountReceived}"/>  	 
								</c:when> 
								<c:otherwise>
									<c:set var="discountAmount" value="0"/>  
								</c:otherwise>	
							</c:choose> 
							<%
								double netAmount = (Double.valueOf(pageContext.getAttribute("totalAmount").toString()));
								double discount = (Double.valueOf(pageContext.getAttribute("discountAmount").toString()));
								if(discount > 0){
									netAmount -= discount;
								}
							%>
							<td colspan="3" class="left_align"><span style="font-weight: bold;"> AMOUNT IN WORDS : </span>
								 <%	String decimalAmount = String.valueOf(AIOSCommons
								 					.formatAmount(netAmount));
								 			decimalAmount = decimalAmount.substring(decimalAmount
								 					.lastIndexOf('.') + 1);
								 			String amountInWords = AIOSCommons
								 					.convertAmountToWords(netAmount);
								 			if (Double.parseDouble(decimalAmount) > 0) {
								 				amountInWords = amountInWords.concat(" and ") 
								 						.concat(AIOSCommons
								 								.convertAmountToWords(decimalAmount))
								 						.concat(" Fils ");
								 				String replaceWord = "Only";
								 				amountInWords = amountInWords.replaceAll(replaceWord,
								 						"");
								 				amountInWords = amountInWords.concat(" " + replaceWord);
								 			}
								 %> <%=amountInWords%>
							</td>
							<td style="font-weight: bold;">
								<table>
									<tr>
										<td style="border: 0px; border-bottom: 2px double #000; border-width: 3px;">DISCOUNT</td>
									</tr>
									<tr>
										<td style="border: 0px;">NET AMOUNT</td> 
									</tr> 
								</table> 
							 </td>
							<td style="font-weight: bold;"> 
								<table>
									<tr>
										<td style="border: 0px; border-bottom: 2px double #000; border-width: 3px;" class="right_align">  
											 <c:if test="${discountAmount gt 0}">
													<c:set var="discountAmount" value="${PAYMENT_INFO_PRINT.discountReceived}"/> 
													<c:out value="${discountAmount}"/>
											</c:if>
											<c:if test="${discountAmount eq 0}">
												<span style="visibility: hidden;">-N/A-</span>
											</c:if>	  
										</td> 
									</tr>
									<tr> 
										<td style="border: 0px;" class="right_align"><%=AIOSCommons.formatAmount(netAmount) %> </td> 
									</tr> 
								</table>  
							</td>
						</tr>
					</c:when>
				</c:choose> 
			</tbody>
		</table>
	</div> 
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">DESCRIPTION<span class="float_right">:</span> </span>
		<span class="span_border left_align text_info" style="width:77%;display: -moz-inline-stack;">
			<c:choose>
				<c:when test="${PAYMENT_INFO_PRINT.description ne null && PAYMENT_INFO_PRINT.description ne ''}">
					${PAYMENT_INFO_PRINT.description}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose>  
		</span>
	</div>
	<div class="width100 float_left div_text_info" style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span> 
			</div> 
			<div class="width40 float_left">
				<span>Recommended by:</span> 
			</div>
			<div class="width30 float_left">
				<span>Approved by:</span> 
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
		</div>
	</div>
	<div class="width100 float_left div_text_info" style="margin-top: 20px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Received by:</span> 
			</div>  
		</div>
	</div>
	</body>
</html>