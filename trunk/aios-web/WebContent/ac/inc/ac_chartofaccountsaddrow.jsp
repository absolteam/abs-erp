<tr id="fieldrow_${rowId}" class="rowidCOA"> 	
	<td id="code_${rowId}"></td>
	<td id="description_${rowId}"></td>
	<td id="accounttype_${rowId}"></td> 
	<td style="display:none;" id="accounttypeid_${rowId}"></td>
	<td style="display:none;" id="accountid_${rowId}"></td>  
	<td style="display:none;" id="lineId_${rowId}">${rowId}</td>
	<td class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addCOAData" id="AddCOAImage_${rowId}" style="cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editCOAData" id="EditCOAImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delCOArow" id="DeleteCOAImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingCOAImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>  
</tr>