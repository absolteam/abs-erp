<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var assetInsuranceId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	 
	 oTable = $('#AssetInsuranceClainPopup').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allasset_insurance.action",

		"aoColumns" : [ { 
			"mDataProp" : "assetName"
		 },{ 
			"mDataProp" : "insuranceType"
		 },{ 
			"mDataProp" : "policyNumber"
		 },{
			"mDataProp" : "policyType"
		 }, {
			"mDataProp" : "provider"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 },{
			"mDataProp" : "insurancePeriodName"
		 }]
	});	  
	 
	/* Click event handler */
	$('#AssetInsuranceClainPopup tbody tr').live('dblclick', function () {   
		aData = oTable.fnGetData(this); 
 		 assetInsurancePopupResult(aData.assetInsuranceId, aData.policyNumber);
		 $('#common-popup').dialog("close");
  	});

	$('#AssetInsuranceClainPopup tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) 
			  $(this).addClass('row_selected');
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	      }
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset insurance</div>	 	 
		<div class="portlet-content"> 
 			 	<div id="asset_service_list">
					<table id="AssetInsuranceClainPopup" class="display">
						<thead>
							<tr>
								<th>Asset Name</th> 
								<th>Insurance Type</th> 
								<th>Policy NO</th> 
								<th>Policy Type</th> 
								<th>Provider</th> 
								<th>Start Date</th> 
								<th>End Date</th>  
								<th>Period</th>  
							</tr>
						</thead> 
					</table> 
 			</div>	 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="commom-insurance-close" >close</div> 
 			</div>
		</div>
	</div>
</div>