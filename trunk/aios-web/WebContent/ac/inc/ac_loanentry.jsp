<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="java.text.DecimalFormat;" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.opn_td{
	width:6%;
}
.width35{
	width:35%!important;
}
.width25{
	width:25%!important;
}
.width15{
	width:15%!important;
}
.margintop3{
	margin-top:3px;
}
#common-popup{
	overflow: hidden;
}
.width61{
	width:61% !important;
}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";  
var totalDayDiff=0;
var tempid="";
var loanLineDetail=""; var actionname="";
var accountTypeId=0;
var providerdetails=""; var loanChargesDetail="";
var isMeturity=false;
$(function(){ 
	var attrId=$('.save').attr('id').trim();
	if(attrId=='edit'){
		$('#emiFrequency').val($('#tempemifrequency').val());
		$('#emiType').val($('#tempemitype').val());
		$('#loanCurrency').val($('#tempcurrencyid').val());
		$('#interestType').val($('#tempratetype').val());
		$('#rateVariance').val($('#tempratevariance').val());
		$('#loanType').val($('#temploantype').val());  
		if($('#temploantype').val()=='C' || $('#temploantype').val()=='O'){
			$('#eiborType').addClass('validate[required]'); 
		}
		else{
			$('#eiborType').removeClass('validate[required]');
			$('#eiborRate').removeClass('validate[required,custom[onlyFloat,percentRange]]');
		} 
		
		$('.chargesType').each(function(){
			var rowId=getRowId($(this).attr('id')); 
			if(typeof($('#temptypeId_'+rowId).val()!='undefined') && $('#temptypeId_'+rowId).val()!=null && $('#temptypeId_'+rowId).val()!=""){
				$(this).val($('#temptypeId_'+rowId).val());
			}
	 	});
	}
	 
	if(Number($('#loanId').val())==0){
		var default_currency=$('#default_currency').val();
		$('#loanCurrency option').each(function(){  
			var curval=$(this).val();
			if(curval==default_currency){   
				$('#loanCurrency option[value='+$(this).val()+']').attr("selected","selected"); 
				return false;
			}
		});
	}
	 
	manupulateLastRow();
	$("#loanentry_details").validationEngine({  
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction();} 
	}); 

	 
	
	$('#sancationDate,#maturityDate').datepick({
		 onSelect: customRange,showTrigger: '#calImg'});   
	$('#emiDueDate').datepick(); 
	 $('#startPicker,#endPicker').datepick({
		 onSelect: customRanges, showTrigger: '#calImg'}); 
	  
	$('.lineamount').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		$('#linedecription_'+rowId).focus(); 
		triggerAddRow($('#linedate_'+rowId).val(),rowId);
	});
	
	$('.linedecription').blur(function(){
		//var rowId=getRowId($(this).attr('id')); 
	}); 

	$('.chargesType').live('change',function(){  
		if($(this).val()!="" && $(this).val()=='A'){ 
			var rowIds=getRowId($(this).attr('id'));  
			$('#chargesValue_'+rowIds).val('');
			$('#chargesValue_'+rowIds).removeClass('validate[optional,custom[onlyFloat,percentRange]]');
			$('#chargesValue_'+rowIds).addClass('validate[optional,custom[onlyFloat]]');
		}else  if($(this).val()!="" && $(this).val()=="I"){  
			var rowIds=getRowId($(this).attr('id'));
			$('#chargesValue_'+rowIds).val('');
			$('#chargesValue_'+rowIds).removeClass('validate[optional,custom[onlyFloat]]');
			$('#chargesValue_'+rowIds).addClass('validate[optional,custom[onlyFloat,percentRange]]');
		}
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerChargesAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	$('.chargesName').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerChargesAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});

	$('.chargesValue').live('change',function(){
		if($(this).val()!=""){
			var rowId=getRowId($(this).attr('id')); 
			triggerChargesAddRow(rowId);
			return false;
		} 
		else{
			return false;
		}
	});
	
	/*$('#emiType').change(function(){
		var emiType=$(this).val();
		if(emiType!=null && emiType!=""){
			if(emiType=='P'){
				$('.emidue').each(function(){
					$($(this).show().children().get(1)).val(""); 
					$($(this).show().children().get(1)).addClass('validate[required]'); 
				});
				$($('.unpredictdue').hide().children().get(1)).removeClass('validate[required,custom[onlyNumber]]');
			}
			else{
				$('.emidue').each(function(){
					$($(this).hide().children().get(1)).removeClass('validate[required]');
				}); 
				$('#totalEmi').val("");
				$($('.unpredictdue').show().children().get(1)).addClass('validate[required,custom[onlyNumber]]');	
			}
		}
	});*/
	
	/*$('#interestType').change(function(){
		var interestType=$(this).val();
		if(interestType!=null && interestType!=""){ 
			$($('.interestdiv').children().get(1)).val(""); 
			if(interestType=='V' || interestType=='M'){  
				$($('.interestdiv').show().children().get(1)).addClass(' validate[required,custom[onlyFloat,percentRange]]');    
			}
			else{
				$($('.interestdiv').hide().children().get(1)).removeClass(' validate[required,custom[onlyFloat,percentRange]]');   
			}
		} 
	});*/
	
	$('.save').click(function(){  
		if($("#loanentry_details").validationEngine({returnIsValid:true})){  
			var page="";
			loanLineDetail=""; loanChargesDetail="";
			var loanId=Number($('#loanId').val());
			var loanNumber=$('#loanNumber').val();
			var loanType=$('#loanType').val();
			var sancationDate=$('#sancationDate').val();
			//var bankId=$('#bankId').val();
			var accountId=$('#accountId').val();
			var loanAmount=$('#loanAmount').val();
			loanAmount=loanAmount.replace(/,/gi,'');
			var eiborId=Number($('#eiborId').val()); 
			var interestType=$('#interestType').val();
			//var insurance=Number($('#insurance').val());
			var finalInterest=$('#finalInterest').val();
			var leadyDay=$('#leadyDay').val();
			var emiFrequency=$('#emiFrequency').val();
			var emiStartDate=$('#startPicker').val();
			var emiEndDate=$('#endPicker').val();
			//var emiPrincipal=$('#emiPrincipal').val();
			var description=$('#description').val();
			var providerName=$('#providerName').val();
			var mobileNumber=$('#mobileNumber').val();
			var landLine=$('#landLine').val();
			var addressDetails=$('#addressDetails').val();
			var maturityDate=$('#maturityDate').val();
			//var emiCombinationId=$('#emiCombinationId').val();
			var loanCombinationId=$('#loanCombinationId').val();
			//var otherCombinationId=$('#otherCombinationId').val();
			var emiType=$('#emiType').val();
			var interest=$('#interest').val();
			var emailAddress=$('#emailAddress').val();
			var loanCurrency=$('#loanCurrency').val();
			//var totalEmi=Number($('#totalEmi').val());
			var interestExpensesCombinationId=$('#interestExpensesCombinationId').val();
			//var interestPayableCombinationId=$('#interestPayableCombinationId').val();
			var shortTermCombinationId=$('#shortTermCombinationId').val();
			var emiDueDate=$('#emiDueDate').val();
			var rateVariance=$('#rateVariance').val(); 
			loanLineDetail=getLoanLineDetails();  
			loanChargesDetail=getLoanChargesDetails();
			if(loanLineDetail!=null && loanLineDetail!=""){ 
			}
			else{ 
				createLoanLines(sancationDate,loanAmount,description);
			} 
			if(loanId>0)
				 page="update";
			else
				page="add"; 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/saveloandetails.action", 
			 	async: false, 
			 	data:{ loanId:loanId, loanNumber:loanNumber, loanType:loanType, sancationDate:sancationDate, maturityDate:maturityDate, 
				 	   loanAmount:loanAmount, eiborId:eiborId, interestType:interestType, finalInterest:finalInterest, leadyDay:leadyDay, page:page,
				 	   emiFrequency:emiFrequency, emiStartDate:emiStartDate, emiEndDate:emiEndDate,loanLineDetail:loanLineDetail, description:description,
				 	   providerName:providerName, mobileNumber:mobileNumber, accountId:accountId, landLine:landLine, addressDetails:addressDetails,
				 	   loanCombinationId:loanCombinationId, emiType:emiType, emailAddress:emailAddress, currencyId:loanCurrency, interestRate:interest,
				 	   interestExpensesCombinationId:interestExpensesCombinationId, shortTermCombinationId:shortTermCombinationId, emiDueDate:emiDueDate,
				 	   rateVariance:rateVariance, loanChargesDetail:loanChargesDetail},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('#return_message').html();  
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/getloan_details.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove(); 
				  					 $('#codecombination-popup').dialog('destroy');		
				  					 $('#codecombination-popup').remove(); 
				  					$('.providerx-popup').dialog('destroy');		
				  					$('.providerx-popup').remove();  
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html(message).slideDown(1000);
								}
						 });
					 }else if(message.trim()=="PAYMENTS"){
						 loanId=$('#tempLoanId').val(); 
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/loan_payment_entry.action", 
							 	async: false,
							 	data:{loanId:loanId},
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove(); 
				  					 $('#codecombination-popup').dialog('destroy');		
				  					 $('#codecombination-popup').remove(); 
				  					$('.providerx-popup').dialog('destroy');		
				  					$('.providerx-popup').remove();  
									$("#main-wrapper").html(result);  
								}
						 });
					 }
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Server goes wrong").slideDown(1000);
				}
			});  
		}
		else{
			return false;
		}
	});
	
	$('.discard').live('click',function(){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getloan_details.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
  					$('#common-popup').remove(); 
  					$('.providerx-popup').dialog('destroy');		
  					$('.providerx-popup').remove();  
  					$('#codecombination-popup').dialog('destroy');		
  					$('#codecombination-popup').remove();   
					$("#main-wrapper").html(result);  
					return false;
				}
		 });
		 return false;
	});
	
	var createLoanLines=function(date,amount,description){
		if(description!=null && description!=""){
		}else{
			description="Loan Detail entry";
		}
		loanLineDetail=date+"__"+amount+"__"+description+"__"+0+"@@";
		return loanLineDetail;
	};
	 
	var getLoanLineDetails=function(){
		var loanDetailDate=new Array();
		var loanDetailAmount=new Array();
		var loanDetailDesc=new Array();
		var loanLineId=new Array(); 
		$('.rowid').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var lineDate=$('#linedate_'+rowId).val();
			var amount=$('#amount_'+rowId).val();
			var linedesc=$('#linedecription_'+rowId).val(); 
			if(linedesc!=null && linedesc!=""){
			}else{
				linedesc="Loan Detail entry";
			}
			var lineId=Number($('#loanLineId_'+rowId).val()); 
			if(typeof lineDate != 'undefined' && lineDate!=null && lineDate!="" && amount!=null && amount!=""){
				loanDetailDate.push(lineDate);
				loanDetailAmount.push(amount);
				loanDetailDesc.push(linedesc);
				loanLineId.push(lineId);
			}
		});
		for(var j=0;j<loanDetailDate.length;j++){ 
			loanLineDetail+=loanDetailDate[j]+"__"+loanDetailAmount[j]+"__"+loanDetailDesc[j]+"__"+loanLineId[j];
			if(j==loanDetailDate.length-1){   
			} 
			else{
				loanLineDetail+="@@";
			}
		} 
		return loanLineDetail;
	};

	var getLoanChargesDetails = function(){
		var chargesType=new Array();
		var chargesName=new Array();
		var chargesValue=new Array();
		var loanChargesId=new Array();
		var otherCharges=new Array();
		var paymentflag=new Array();
		$('.rowidc').each(function(){
			var rowId=getRowId($(this).attr('id')); 
			var typec=$('#chargesType_'+rowId).val();
			var namec=$('#chargesName_'+rowId).val();
			var valuec=$('#chargesValue_'+rowId).val(); 
			var otherChargesCode=$('#otherChargesCombinationId_'+rowId).val();
			var chargesid=Number($('#loanLineChargesId_'+rowId).val()); 
			var paymentFlagId=$('#paymentFlag_'+rowId).val(); 
			if(typeof typec != 'undefined' && typec!=null && typec!="" && namec!=null && namec!="" 
				&& valuec!=null && valuec!="" && otherChargesCode!=null && otherChargesCode!=""){
					chargesType.push(typec);
					chargesName.push(namec);
					chargesValue.push(valuec);
					loanChargesId.push(chargesid);
					otherCharges.push(otherChargesCode);
					paymentflag.push(paymentFlagId);
			}
		});
		for(var j=0;j<chargesType.length;j++){ 
			loanChargesDetail+=chargesType[j]+"__"+chargesName[j]+"__"+chargesValue[j]+"__"+loanChargesId[j]+"__"+otherCharges[j]+"__"+paymentflag[j];
			if(j==chargesType.length-1){   
			} 
			else{
				loanChargesDetail+="@@";
			}
		}  
		return loanChargesDetail;
	};
	
	 $('.common-popup').click(function(){ 
	       tempid=$(this).attr('id'); 
	       $('.ui-dialog-titlebar').remove();  
	       var currentaction="";
	       var bankId=Number(0);
	       if(tempid=="bankid"){
	    	   $('#accountId').val("");
	    	   $('#accountNumber').val("");
	    	   $('#eiborId').val("");
	    	   $('#eiborType').val("");
	    	   currentaction="getbankdetails";
	       }
	       else if(tempid=="accountid"){
	    	   bankId=Number($('#bankId').val());
	    	   currentaction="getbankaccount_details";
	       }
	       else if(tempid=="eiborid"){
	    	   bankId=0;//$('#bankId').val();
	    	   currentaction="geteibor_details";
	       }
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+currentaction+".action", 
			 	async: false, 
			 	data:{bankId:bankId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.popup-result').html(result);  
				},
				error:function(result){  
					 $('.popup-result').html(result); 
				}
			});  
		});
		 $('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
	
	$('.linedate').live('focus',function(){  
		var rowId=getRowId($(this).attr('id')); 
		$('#linedate_'+rowId).datepick({ 
			minDate:totalDayDiff, onSelect: function(dates){covertDate(dates,$(this));}, 
		    showTrigger: '#calImg'}); 
	}); 
	
	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 });
	
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/loan_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	$('.addrowsc').live('click',function(){  
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabc>.rowidc:last").attr('id'))); 
		  id=id+1;   
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/loan_othercharges_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabc tr:last').before(result);
					 if($(".tabc").height()>255)
						 $(".tabc").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidc').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdc_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
					 return false;
				}
			});
		  return false;
	 }); 
	
	$('.provider-popup').click(function(){
		$('.ui-dialog-titlebar').remove();  
		 $('.formError').hide(); 
		$('.providerx-popup').dialog('open'); 
	});
	
	// management Dialog			
 	$('.providerx-popup').dialog({
 		autoOpen: false,
 		width: 500, 
 		bgiframe: true,
 		modal: true,
 		buttons: {
 			"Close": function(){  
 				$('#providerDetails').val("");
 				if($('#providerName').val()!=null && $('#providerName').val()!=''){
 					providerdetails=providerdetails+" "+$('#providerName').val()+" | ";
 				}
 				if($('#mobileNumber').val()!=null && $('#mobileNumber').val()!=''){
 					providerdetails=providerdetails+" "+$('#mobileNumber').val()+" | ";
 				}
 				if($('#landLine').val()!=null && $('#landLine').val()!=''){
 					providerdetails=providerdetails+" "+$('#landLine').val()+" | ";
 				}
 				if($('#emailAddress').val()!=null && $('#emailAddress').val()!=''){
 					providerdetails=providerdetails+" "+$('#emailAddress').val()+" | ";
 				}
 				if($('#addressDetails').val()!=null && $('#addressDetails').val()!=''){
 					providerdetails=providerdetails+" "+$('#addressDetails').val();
 				}
 				$('#providerDetails').val(providerdetails);
 				$(this).dialog("close"); ;
 			}, 
		 	"Clear": function() {  
				$('#addressDetails').val("");
				$('#providerName').val("");
				$('#mobileNumber').val("");
				$('#landLine').val(""); 
				$('#emailAddress').val(""); 
				$('#providerDetails').val("");
				$(this).dialog("close");
			} 
 		}
 	});
	
 	//Combination pop-up config
    $('.codecombination-popup').live('click',function(){
        tempid=$(this).parent().get(0);
        accountTypeId=0; 
        actionname="";
        $('.ui-dialog-titlebar').remove();  
        actionname=$(tempid).attr('id');
        var idVal=actionname.split('_');
        if(typeof(idVal[1]!='undefined') && idVal[1]!=null && idVal[1]!=""){
      	  actionname=idVal[0];
        }
        else{
      	  actionname=$(tempid).attr('id');
        }  
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/combination_treeview.action",
             async: false, 
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.codecombination-result').html(result);
                  return false;
             },
             error:function(result){
                  $('.codecombination-result').html(result);
             }
         });
         return false;
    }); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
		});
     
     $('#loanType').change(function(){
    	 var loanType=$(this).val().toLowerCase();  
    	 if(loanType=='p' || loanType=='c' || loanType=='a'){ 
    		 $('#eiborType').removeClass('validate[required]');
    		 $('#eiborRate').removeClass('validate[required,custom[onlyFloat,percentRange]]');
    		 $('#emiType').val('P');
    	 }
    	 else{ 
    		 $('#eiborType').addClass('validate[required]');
    		 $('#eiborRate').addClass('validate[required,custom[onlyFloat,percentRange]]');
    		 $('#emiType').val("");
    	 }
     }); 
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
	manupulateChargesLastRow();
}
function manupulateChargesLastRow(){
	var hiddenSize=0;
	$($(".tabc>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tabc>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tabc>tr:last').removeAttr('id');  
	$('.tabc>tr:last').removeClass('rowidc').addClass('lastrow');  
	$($('.tabc>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tabc>tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function customRange(dates){ 
	var fromFormat='dd-MMM-yyyy';
	var toFormat='yyyyMMdd';
	var  validDate=Number(formatDate(new Date(getDateFromFormat($("#sancationDate").val(),fromFormat)),toFormat)); 
	var splitdates=validDate.toString();
	var splityr=splitdates.substring(0,4);
	var splitmn=splitdates.substring(4,6);
	var splitdy=splitdates.substring(6,8);
	var approvalDate=new Date(splityr+"-"+splitmn+"-"+splitdy);
	var today=new Date();
	var diff = new Date(approvalDate - today);
	totalDayDiff = parseInt(diff/1000/60/60/24)+1;
	$('.linedate').val("");   
	if (this.id == 'sancationDate') {
		$('#maturityDate').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#sancationDate').datepick('option', 'maxDate', dates[0] || null);  
	} 
	isLongTermPlan();
} 
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(currentLnDate,rowId){  
	 var lineAmount=$('#amount_'+rowId).val();  
	 var nexttab=$('#fieldrow_'+rowId).next(); 
	if(currentLnDate!=null && currentLnDate!=""
			&& lineAmount!=null && lineAmount!="" && $(nexttab).hasClass('lastrow')){
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	}
}
function triggerChargesAddRow(rowId){  
	var chargesType=$('#chargesType_'+rowId).val();  
	var chargesName=$('#chargesName_'+rowId).val();  
	var chargesValue=$('#chargesValue_'+rowId).val();  
	var othercharges=$('#otherChargesCombinationId_'+rowId).val();
	var nexttab=$('#fieldrowc_'+rowId).next();  
	if(chargesType!=null && chargesType!=""
			&& chargesName!=null && chargesName!="" && chargesValue!=null && chargesValue!=""
				&& othercharges!=null && othercharges!=""  && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImageC_'+rowId).show();
		$('.addrowsc').trigger('click');
		return false;
	}else{
		return false;
	} 
}
function covertDate(date, id){ 
	var rowId=getRowId($(id).attr('id'));  
	var dates = new Date(date.toString());
    var curr_date = dates.getDate();
    var curr_month = dates.getMonth() + 1;
    var curr_year = dates.getFullYear();
    var currentLnDate=validDate=curr_date + "-" + curr_month + "-" + curr_year; 
    if(currentLnDate!=""){
    	$('#amount_'+rowId).focus();
    }
    triggerAddRow(currentLnDate,rowId);
}
function checkLinkedDays() {
    var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
    $('#selectedDay option:gt(27)').attr('disabled', false);
    $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
    if ($('#selectedDay').val() > daysInMonth){
        $('#selectedDay').val(daysInMonth);
    }
} 
function customRanges(dates) {
	if (this.id == 'startPicker') {
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);  
	}
	else{
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);  
	} 
}

function setCombination(combinationTreeId,combinationTree){
	if(actionname=="loanaccount"){
		  $('#loanCombinationId').val(combinationTreeId); 
        $('.loanaccount').html(combinationTree); 
  }
  else if(actionname=="emiaccount"){
  	  $('#emiCombinationId').val(combinationTreeId); 
        $('.emiaccount').html(combinationTree); 
  }  
  else if(actionname=="otherchargescode"){
      var tempvar =$(tempid).attr('id');	
      var idVal=tempvar.split("_");
      var rowid=Number(idVal[1]); 
  	  $('#otherChargesCombinationId_'+rowid).val(combinationTreeId); 
      $('#otherChargesCode_'+rowid).html(combinationTree); 
      triggerChargesAddRow(rowid);
  }   
  else if(actionname=="interestexpenses"){
	  $('#interestExpensesCombinationId').val(combinationTreeId); 
      $('.interestexpenses').html(combinationTree); 
  } 
  else if(actionname=="interestpayable"){
  	  $('#interestPayableCombinationId').val(combinationTreeId); 
      $('.interestpayable').html(combinationTree); 
  } 
  else if(actionname=="shorttermloan"){
  	  $('#shortTermCombinationId').val(combinationTreeId); 
      $('.shorttermloan').html(combinationTree); 
  } 
	actionname="";
} 
var fromFormat='dd-MMM-yyyy';
var toFormat='MM/dd/y';
function isLongTermPlan() {
	var tempdate1 = formatDate(new Date(getDateFromFormat($("#maturityDate").val(),fromFormat)),toFormat);
	var tempdate2 = formatDate(new Date(getDateFromFormat($("#financialEndDate").val(),fromFormat)),toFormat);
	var date1=new Date(tempdate1);
	var date2=new Date(tempdate2);
	var timeDiff = date2.getTime() - date1.getTime();
	//alert(timeDiff);
	if(timeDiff<0){
		$('#longTerm-div').show();
	}else{
		$('#longTerm-div').hide();
		$('#loanCombinationId').val(0);
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>loan information</div>	
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10">Comment From   :<span> ${COMMENT_IFNO.person.firstName} ${COMMENT_IFNO.person.lastName}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>	
		  <form name="loanentry_details" id="loanentry_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="loanId" name="loanId" value="${LOAN_INFORMATION.loanId}"/>
				<div class="width100 float-left" id="hrm"> 
					<div class="width100 float-left">
						<fieldset class="width58 float-left" style="height:175px;"> 
								<div class="width50 float-right">
									<div class="width90 float-left"> 
										<label class="width20" for="loanCurrency">Currency</label>
										<select name="loanCurrency" id="loanCurrency" class="width60 validate[required]" tabindex="8">
											 <option value="">Select</option>
											 <c:forEach var="bean" items="${LOAN_CURRENCY_DETAILS}">
											 	<option value="${bean.currencyId}">${bean.currencyPool.code}</option>
											 </c:forEach>
										</select>
										<input type="hidden" readonly="readonly" name="tempcurrencyid" id="tempcurrencyid"
											value="${LOAN_INFORMATION.currency.currencyId}"/>
										<input type="hidden" readonly="readonly" name="default_currency" id="default_currency" value="${DEFAULT_CURRENCY}"/>	
									</div> 
									<div class="width90 float-left">
										<label class="width20" for="sancationDate">Approval Date</label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null &&  LOAN_INFORMATION.loanId ne 0}">
												<c:set var="sanctionDate" value="${LOAN_INFORMATION.sanctionDate}"/>  
												<%String sanctionDate = DateFormat.convertDateToString(pageContext.getAttribute("sanctionDate").toString());%>
												<input type="text" readonly="readonly" name="sancationDate" id="sancationDate" tabindex="3" class="width60 validate[required]" 
												value="<%=sanctionDate%>"/>
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="sancationDate" id="sancationDate" tabindex="3" class="width60 validate[required]"/>
											</c:otherwise>
										</c:choose> 
									</div>   
									<div class="width90 float-left">
										<label class="width20" for="maturityDate">Maturity Date</label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null &&  LOAN_INFORMATION.loanId ne 0}">
												<c:set var="maturityDate" value="${LOAN_INFORMATION.maturityDate}"/>  
												<%String maturityDate = DateFormat.convertDateToString(pageContext.getAttribute("maturityDate").toString());%>
												<input type="text" readonly="readonly" name="maturityDate" id="maturityDate" tabindex="3" class="width60 validate[required]" 
												value="<%=maturityDate%>"/>
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="maturityDate" id="maturityDate" tabindex="4" class="width60 validate[required]"/>
											</c:otherwise>
										</c:choose> 
										<input type="hidden" name="financialEndDate" id="financialEndDate" tabindex="4" class="width60" value="${FINANCIAL_YEAR_ENDDATE}"/>
									</div>  
									<div class="width90 float-left">
										<label class="width20" for="loanAmount">Loan Amount</label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.amount ne null && LOAN_INFORMATION.amount gt 0}">
												<c:set var="loanamount" value="${LOAN_INFORMATION.amount}"/>
												<%
													DecimalFormat decimalFormat=new DecimalFormat();
													String loanamount=decimalFormat.format(pageContext.getAttribute("loanamount")); 
													loanamount = AIOSCommons.formatAmount(pageContext.getAttribute("loanamount"));
													pageContext.setAttribute("loanamount", loanamount); 
												%>
												<input type="text" name="loanAmount" id="loanAmount" tabindex="7" class="width60 validate[required]"
											value="${loanamount}"/>
											</c:when> 
											<c:otherwise>
												<input type="text" name="loanAmount" id="loanAmount" tabindex="7" class="width60 validate[required]"/>
											</c:otherwise>
										</c:choose> 
									</div> 
									<div class="width90 float-left">
										<label class="width20" for="providerDetails">Provider Details</label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null && LOAN_INFORMATION.loanId ne 0}">
												<c:set var="provider" value="${providerName} ${mobileNumber} ${landLine} ${emailAddress} ${addressDetails}"/> 
												<input type="text" name="providerDetails" readonly="readonly" id="providerDetails"  tabindex="24" class="width60"
													value="${provider}"/>
											</c:when>
											<c:otherwise>
												<input type="text" name="providerDetails" readonly="readonly" id="providerDetails" tabindex="24" class="width60"/>
											</c:otherwise>
										</c:choose> 
										<span class="button" style="position: relative; margin :-17px 27px 0 0; float: right;">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all provider-popup width100"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span>
										<input type="hidden" name="tempproviderdetails" id="tempproviderdetails" value="${LOAN_INFORMATION.providerDetails}"/>  
									</div> 
								</div> 
								<div class="width50 float-left">
									<div class="width90 float-left">
										<label class="width20" for="loanNumber">Loan Number</label>
										<input type="text" name="loanNumber" id="loanNumber" tabindex="1" class="width70 validate[required]" 
											value="${LOAN_INFORMATION.loanNumber}"/>
									</div> 
									<div class="width90 float-left" style="height:27px;">
										<label class="width20" for="accountNumber">Account No</label>
										<input type="text" readonly="readonly" name="accountNumber" tabindex="5"  id="accountNumber" 
											class="width70 validate[required]" value="${LOAN_INFORMATION.bankAccount.accountNumber}"/>
										<span class="button" style="position: relative!important;top:-17px!important; right:14px!important; float: right;">
											<a style="cursor: pointer;" id="accountid" class="btn ui-state-default ui-corner-all common-popup width100"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span>
										<input type="hidden" readonly="readonly" name="accountId" id="accountId"
											value="${LOAN_INFORMATION.bankAccount.bankAccountId}"/>
									</div>
									<div class="width90 float-left">
											<label class="width20" for="loanType">Facility Type</label>
											<select name="loanType" id="loanType" tabindex="2" class="width70 validate[required]">
												<option value="">Select</option>
												<c:forEach var="entry" items="${LOAN_TYPE}">
													<option value="${entry.key}">${entry.value}</option>
												</c:forEach>
											</select>
											<input type="hidden" name="temploantype" id="temploantype" value="${LOAN_INFORMATION.loanType}"/>
										</div>  
									<div class="width90 float-left">
										<label class="width20" for="interestType">Interest Type</label>
										<select name="interestType" id="interestType" class="width70 validate[required]" tabindex="10">
											<option value="">Select</option>
											<option value="F">Fixed</option>
											<option value="V">Vary</option> 
											<option value="M">Mixed</option> 
										</select>
										<input type="hidden" readonly="readonly" name="tempratetype" id="tempratetype"
											value="${LOAN_INFORMATION.rateType}"/>
									</div>  
									<div class="width90 float-left">
										<label class="width20" for="rateVariance">Interest Method</label>
										<select name="rateVariance" id="rateVariance" class="width70 validate[required]" tabindex="9">
											<option value="">Select</option>
											<option value="F">Flat</option>
											<option value="D">Diminish</option> 
										</select>
										<input type="hidden" readonly="readonly" name="tempratevariance" id="tempratevariance"
											value="${LOAN_INFORMATION.rateVariance}"/>
									</div> 
								</div>
						</fieldset>
						<fieldset style="width:32%;height:175px;" class="float-right"> 
							<div class="width90 float-left">
								<label class="width25" for="emiType">EMI Type</label>
								<select name="emiType" id="emiType" class="width61 validate[required]" tabindex="15">
									<option value="">Select</option>
									<option value="P">Predictable</option>
									<option value="U">UnPredictable</option>
								</select>
								<input type="hidden" readonly="readonly" name="tempemitype" id="tempemitype" value="${LOAN_INFORMATION.emiType}"/>
							</div>  
							<div class="width90 float-left emidue">
								<label class="width25" for="startPicker">EMI From</label>
								<c:choose>
									<c:when test="${LOAN_INFORMATION.emiStartDate ne null &&  LOAN_INFORMATION.emiStartDate ne ''}">
										<c:set var="emiStartDate" value="${LOAN_INFORMATION.emiStartDate}"/>  
										<%String emiStartDate = DateFormat.convertDateToString(pageContext.getAttribute("emiStartDate").toString());%>
										<input type="text" name="emiStartDate" id="startPicker" tabindex="17" class="width60 validate[required]" 
										value="<%=emiStartDate%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="emiStartDate" id="startPicker" tabindex="17" class="width60 validate[required]"/>
									</c:otherwise>
								</c:choose>  
							</div>  
							<div class="width90 float-left emidue">
								<label class="width25" for="endPicker">EMI To</label>
								<c:choose>
									<c:when test="${LOAN_INFORMATION.emiEndDate ne null &&  LOAN_INFORMATION.emiEndDate ne ''}">
										<c:set var="emiEndDate" value="${LOAN_INFORMATION.emiEndDate}"/>  
										<%String emiEndDate = DateFormat.convertDateToString(pageContext.getAttribute("emiEndDate").toString());%>
										<input type="text" name="emiToDate" readonly="readonly" id="endPicker" tabindex="18" class="width60 validate[required]" 
										value="<%=emiEndDate%>"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="emiToDate" id="endPicker" tabindex="18" class="width60 validate[required]"/>
									</c:otherwise>
								</c:choose>  
							</div> 
							<div class="width90 float-left">
								<label class="width25" for="leadyDay">Lead Day</label>
								<input type="text" name="leadyDay" id="leadyDay" tabindex="23" class="width60 validate[required,custom[onlyNumber]]"
									value="${LOAN_INFORMATION.leadDay}"/>
							</div> 
							<div class="width90 float-left">
								<label class="width25" for="emiFrequency">EMI Frequency</label>
								<select name="emiFrequency" id="emiFrequency" class="width61 validate[required]" tabindex="16">
									<option value="">Select</option>
									<c:forEach var="entry" items="${LOAN_FREQUENCY}">
										<option value="${entry.key}">${entry.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" readonly="readonly" name="tempemifrequency" id="tempemifrequency" 
									value="${LOAN_INFORMATION.emiFrequency}"/>
							</div> 
							<div class="width90 float-left">
									<label class="width25" for="emiDueDate">EMI Due Date</label>
									<c:choose>
										<c:when test="${LOAN_INFORMATION.emiFirstDuedate ne null &&  LOAN_INFORMATION.loanId ne ''}">
											<c:set var="emiFirstDuedate" value="${LOAN_INFORMATION.emiFirstDuedate}"/>  
											<%String emiFirstDuedate = DateFormat.convertDateToString(pageContext.getAttribute("emiFirstDuedate").toString());%>
											<input type="text" readonly="readonly" name="emiDueDate" id="emiDueDate" tabindex="19" class="width60 validate[required]" 
											value="<%=emiFirstDuedate%>"/>
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="emiDueDate" id="emiDueDate" tabindex="19" class="width60 validate[required]"/>
										</c:otherwise>
									</c:choose>   
								</div>  
						</fieldset> 
						<div class="width100 float-left">
							<div class="width25 float-left" style="display:none;">
								<label class="width25" for="bankName">Bank Details</label>
								<input type="text" readonly="readonly" name="bankName" id="bankName" tabindex="6" class="width60 validate[required]"
									value="${LOAN_INFORMATION.bankAccount.bank.bankName}"/>
								<span class="button" style="display:none;position: relative!important;top:-17px!important; right:24px!important; float: right;">
									<a style="cursor: pointer;" id="bankid" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<input type="hidden" readonly="readonly" name="bankId" id="bankId"
									value="${LOAN_INFORMATION.bankAccount.bank.bankId}"/>
							</div> 
						</div> 
							<fieldset style="position: relative; top:10px; width:95%;">
								<legend>Interest Information</legend>
								<div class="width100 float-left">
									<div class="width30 float-left">
										<label class="width15" for="eiborType">EIBOR</label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.eibor ne null && LOAN_INFORMATION.eibor ne '' }">
												<c:set var="eiborType" value="${LOAN_INFORMATION.eibor.eiborType}"/>  
												<%Object object=Constants.Accounts.EiborType.get((Character)pageContext.getAttribute("eiborType"));%> 
												<input type="text" readonly="readonly" name="eiborType" id="eiborType" tabindex="13" class="width60 validate[required]"
													value="<%=object%>"/>
												<span class="button">
													<a style="cursor: pointer;" id="eiborid" class="btn ui-state-default ui-corner-all common-popup width100"> 
														<span class="ui-icon ui-icon-newwin"> 
														</span> 
													</a>
												</span>
												<input type="hidden" readonly="readonly" name="eiborId" id="eiborId" value="${LOAN_INFORMATION.eibor.eiborId}"/> 
											</c:when>
											<c:otherwise>
												<input type="text" readonly="readonly" name="eiborType" id="eiborType" tabindex="13" class="width60 validate[required]"/>
												<span class="button">
													<a style="cursor: pointer;" id="eiborid" class="btn ui-state-default ui-corner-all common-popup width100"> 
														<span class="ui-icon ui-icon-newwin"> 
														</span> 
													</a>
												</span>
												<input type="hidden" readonly="readonly" name="eiborId" id="eiborId"/> 
											</c:otherwise>
										</c:choose> 
									</div> 
									<div class="width30 float-left">
											<label class="width25" for="interest">Min.Interest</label>
											<input type="text" name="interest" id="interest" tabindex="22" class="width60"
											value="${LOAN_INFORMATION.rate}"/>
									</div>  
									<div class="width30 float-left">
										<label class="width25" for="finalInterest">Max Interest</label>
										<input type="text" name="finalInterest" id="finalInterest" class="width60 validate[required,custom[onlyFloat,percentRange]]"
											 tabindex="12" value="${LOAN_INFORMATION.finalRate}"/>
									</div>  
									<div class="width25 float-left" style="display:none;">
										<label class="width25" for="eiborRate">EIBOR Rate</label>
										<input type="text" readonly="readonly" name="eiborRate" id="eiborRate" tabindex="14" 
											class="width60 validate[required,custom[onlyFloat,percentRange]]" value="${LOAN_INFORMATION.eibor.eiborRate}"/>
									</div> 
								</div>
							</fieldset> 
							<fieldset style="position: relative; top:10px; width:95%">
								<legend>Account Combination</legend>
								<div class="width100 float-left">
									<div class="width30 float-left">
										<label class="width40">Short Term Loan<span class="mandatory">*</span></label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null && LOAN_INFORMATION.loanId ne 0}">
												<span style="height: 20px; width:43%!important;" class="float-left shorttermloan">
													${LOAN_INFORMATION.combinationByShortTermAccountId.accountByNaturalAccountId.code}
													[${LOAN_INFORMATION.combinationByShortTermAccountId.accountByNaturalAccountId.account}]
												</span>
											</c:when>
											<c:otherwise>
												<span style="height: 20px;width:43%!important" class="float-left width20 shorttermloan"></span>
											</c:otherwise>
										</c:choose>   
										<span class="button float-right" id="shorttermloan"  style="position: relative; top:6px; width:11%!important;">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span> 
										<input type="hidden" name="shortTermCombinationId" id="shortTermCombinationId" 
											value="${LOAN_INFORMATION.combinationByShortTermAccountId.combinationId}"/>  
									</div> 
									<div class="width30 float-left" id="longTerm-div" style="display: none;">
										<label class="width40">Long Term Loan<span class="mandatory">*</span></label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null && LOAN_INFORMATION.loanId ne 0}">
												<span style="height: 20px; width:43%!important" class="float-left loanaccount">
													${LOAN_INFORMATION.combinationByLoanAccountId.accountByNaturalAccountId.code}
													[${LOAN_INFORMATION.combinationByLoanAccountId.accountByNaturalAccountId.account}]
												</span>
											</c:when>
											<c:otherwise>
												<span style="height: 20px; width:43%!important;" class="float-left loanaccount"></span>
											</c:otherwise>
										</c:choose> 
										<span class="button float-right" id="loanaccount"  style="position: relative; top:6px;width:11%!important">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span> 
										<input type="hidden" name="loanCombinationId" id="loanCombinationId" 
											value="${LOAN_INFORMATION.combinationByLoanAccountId.combinationId}"/>  
									</div>
									<div class="width30 float-left">
										<label class="width40">Interest Expenses<span class="mandatory">*</span></label>
										<c:choose>
											<c:when test="${LOAN_INFORMATION.loanId ne null && LOAN_INFORMATION.loanId ne 0}">
												<span style="height: 20px;width:43%!important;" class="float-left interestexpenses">
													${LOAN_INFORMATION.combinationByInterestExpenses.accountByNaturalAccountId.code}
													[${LOAN_INFORMATION.combinationByInterestExpenses.accountByNaturalAccountId.account}]
												</span>
											</c:when>
											<c:otherwise>
												<span style="height: 20px;width:43%!important;" class="float-left interestexpenses"></span>
											</c:otherwise>
										</c:choose>  
										<span class="button float-right" id="interestexpenses"  style="position: relative; top:6px; width:11%!important;">
											<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
												<span class="ui-icon ui-icon-newwin"> 
												</span> 
											</a>
										</span> 
										<input type="hidden" name="interestExpensesCombinationId" id="interestExpensesCombinationId"
											value="${LOAN_INFORMATION.combinationByInterestExpenses.combinationId}"/>  
									</div>
								</div> 
							</fieldset>  
							<fieldset style="position: relative; top:10px; width:95%;">
								<legend>Description</legend>
								<div> 
									<textarea tabindex="22" class="width98" id="description" style="height:40%">${LOAN_INFORMATION.description}</textarea>
								</div>
							</fieldset> 
							<fieldset style="position: relative; top:10px; width:95%;">
			 					<legend>Charges Information</legend> 
						 			<div  id="line-error-charges" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
						 			<div id="warning_message-charges" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
										<div id="hrm" class="hastable width100"  >  
											<input type="hidden" name="childCountCharges" id="childCountCharges" value="${LOAN_CHARGES_INFORMATION}"/>  
											<table id="hastab1" class="width100"> 
												<thead>
												   <tr> 
														<th style="width:5%">Type</th> 
													    <th style="width:5%">Name</th>  
													    <th style="width:5%">Value</th>  
													    <th style="width:5%">Combination</th>  
														<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
												  </tr>
												</thead> 
												<tbody class="tabc">
													<c:choose>
														<c:when test="${LOAN_CHARGES_INFORMATION ne null && LOAN_CHARGES_INFORMATION ne '' && fn:length(LOAN_CHARGES_INFORMATION)>0}">
															<c:forEach var="bean" items="${LOAN_CHARGES_INFORMATION}" varStatus="status1">
																<tr class="rowidc" id="fieldrowc_${status1.index+1}">
																	<td id="lineIdc_${status1.index+1}" style="display: none;">${status1.index+1}</td>
																	<td>
																		<select class="width90 chargesType" id="chargesType_${status1.index+1}"style="border:0px;">
																			<option value="">Select</option>
																			<option value="A">Amount</option>
																			<option value="I">Incremental</option>
																		</select>
																		<input type="hidden" id="temptypeId_${status1.index+1}" value="${bean.type}" style="border:0px;"/>
																	</td> 
																	<td>
																		 <input type="text" class="width80 chargesName" id="chargesName_${status1.index+1}" value="${bean.name}" style="border:0px;"/>
																	</td> 
																	<td>
																		<input type="text" class="width70 chargesValue" id="chargesValue_${status1.index+1}" value="${bean.value}" style="border:0px;text-align: right;"/>
																	</td> 
																	<td>
																		<span class="otherChargesCode width50" id="otherChargesCode_${status1.index+1}">
																			${bean.combination.accountByNaturalAccountId.code}
																				[${bean.combination.accountByNaturalAccountId.account}]
																		</span>
																		<span class="button float-right width20" id="otherchargescode_${status1.index+1}"  style="position: relative; top:6px;">
																			<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
																				<span class="ui-icon ui-icon-newwin"> 
																				</span> 
																			</a>
																		</span> 
																		<input type="hidden" name="otherChargesCombinationId" id="otherChargesCombinationId_${status1.index+1}"
																			value="${bean.combination.combinationId}"/> 
																	</td> 
																	<td style="display:none;">
																		<input type="hidden" id="paymentFlag_${status1.index+1}" value="${bean.paymentFlag}"/>
																		<input type="hidden" id="loanLineChargesId_${status1.index+1}" value="${bean.otherChargeId}"/>
																	</td> 
																	 <td style="width:0.01%;" class="opn_td" id="option_${status1.index+1}">
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatac" id="AddImageC_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												 								<span class="ui-icon ui-icon-plus"></span>
																		  </a>	
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatac" id="EditImageC_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
																				<span class="ui-icon ui-icon-wrench"></span>
																		  </a> 
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowc" id="DeleteImageC_${status1.index+1}" style="cursor:pointer;display:none;" title="Delete Record">
																				<span class="ui-icon ui-icon-circle-close"></span>
																		  </a>
																		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${status1.index+1}" style="display:none;" title="Working">
																				<span class="processing"></span>
																		  </a>
																	</td>
																</tr>
															</c:forEach>  
														</c:when>
													</c:choose>  
													<c:forEach var="i" begin="${fn:length(LOAN_CHARGES_INFORMATION)+1}" end="${fn:length(LOAN_CHARGES_INFORMATION)+2}" step="1" varStatus="status"> 
														<tr class="rowidc" id="fieldrowc_${i}">
															<td id="lineIdc_${i}" style="display:none;">${i}</td>
															<td>
																<select class="width90 chargesType" id="chargesType_${i}"style="border:0px;">
																	<option value="">Select</option>
																	<option value="A">Amount</option>
																	<option value="I">Incremental</option>
																</select>
															</td> 
															<td>
																<input type="text" class="width80 chargesName" id="chargesName_${i}" style="border:0px;"/>
															</td> 
															<td>
																<input type="text" class="width70 chargesValue" id="chargesValue_${i}" style="border:0px;text-align: right;"/>
															</td> 
															<td>
																<span class="otherChargesCode width50" id="otherChargesCode_${i}"></span>
																<span class="button float-right width20" id="otherchargescode_${i}"  style="position: relative; top:6px;">
																	<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
																		<span class="ui-icon ui-icon-newwin"> 
																		</span> 
																	</a>
																</span> 
																<input type="hidden" name="otherChargesCombinationId" id="otherChargesCombinationId_${i}"
																	value=""/> 
															</td> 
															<td style="display:none;">
																<input type="hidden" id="paymentFlag_${status1.index+1}" value="0"/>
																<input type="hidden" id="loanLineChargesId_${i}" style="border:0px;"/>
															</td> 
															 <td style="width:0.01%;" class="opn_td" id="optionc_${i}">
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${i}" style="display:none;cursor:pointer;" title="Add Record">
										 								<span class="ui-icon ui-icon-plus"></span>
																  </a>	
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${i}" style="display:none; cursor:pointer;" title="Edit Record">
																		<span class="ui-icon ui-icon-wrench"></span>
																  </a> 
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${i}" style="display:none;cursor:pointer;" title="Delete Record">
																		<span class="ui-icon ui-icon-circle-close"></span>
																  </a>
																  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${i}" style="display:none;" title="Working">
																		<span class="processing"></span>
																  </a>
															</td>   
														</tr>
												</c:forEach>
										 </tbody>
									</table>
								</div> 
							</fieldset> 
						<div class="providerx-popup" id="hrm" class="width100 float-left">
							<div>
								<label for="providerName" class="width15">Name</label>
								<input type="text" name="providerName" id="providerName" value="${providerName}" class="width60"/>
							</div>
							<div>
								<label for="mobileNumber" class="width15">Mobile</label>
								<input type="text" name="mobileNumber" id="mobileNumber" class="width60" value="${mobileNumber}"/>
							</div>
							<div>
								<label for="landLine" class="width15">Land Line</label>
								<input type="text" name="landLine" id="landLine" class="width60" value="${landLine}"/>
							</div>
							<div>
								<label for="emailAddress" class="width15">E-Mail</label>
								<input type="text" name="emailAddress" id="emailAddress" class="width60" value="${emailAddress}"/>
							</div>
							<div>
								<label for="addressDetails" class="width15">Address</label>
								<textarea name="addressDetails" id="addressDetails" class="width60">${addressDetails}</textarea>
							</div>
						</div>
				 </div> 
 			</div>	
			<div class="clearfix"></div> 
 			<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
 				<legend>Disbursement Details</legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100"  >  
						 <input type="hidden" name="childCount" id="childCount" value="${LOAN_DETAIL_LINE_SIZE}"/>  
						<table id="hastab" class="width100"> 
							<thead>
							   <tr> 
									<th style="width:1%">Line No</th> 
								    <th style="width:5%">Disbursement Date</th>  
								    <th style="width:5%">Amount</th>  
								    <th style="width:5%">Description</th> 
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
							<tbody class="tab">
							<c:choose>
								<c:when test="${LOAN_DETAIL_INFORMATION ne null && LOAN_DETAIL_INFORMATION ne '' && fn:length(LOAN_DETAIL_INFORMATION)>0}">
									<c:forEach var="bean" items="${LOAN_DETAIL_INFORMATION}" varStatus="status1">
										<tr class="rowid" id="fieldrow_${status1.index+1}">
											<td id="lineId_${status1.index+1}">${status1.index+1}</td>
											<td>
												<c:set var="date" value="${bean.date}"/>
												<%String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString()); %>
												<input type="text" readonly="readonly" class="width50 linedate" value="<%=date%>" id="linedate_${status1.index+1}" style="border:0px;"/>
											</td> 
											<td>
												<input type="text" class="width70 lineamount" id="amount_${status1.index+1}" value="${bean.amount}" style="border:0px;text-align: right;"/>
											</td> 
											<td>
												<input type="text" class="width98 linedecription" id="linedecription_${status1.index+1}" value="${bean.description}" style="border:0px;"/>
											</td> 
											<td style="display:none;">
												<input type="hidden" id="loanLineId_${status1.index+1}" value="${bean.loanDetailId}" style="border:0px;"/>
											</td> 
											 <td style="width:0.01%;" class="opn_td" id="option_${status1.index+1}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status1.index+1}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>
										</tr>
									</c:forEach>  
								</c:when>
							</c:choose> 
							<c:forEach var="i" begin="${fn:length(LOAN_DETAIL_INFORMATION)+1}" end="${fn:length(LOAN_DETAIL_INFORMATION)+2}" step="1" varStatus ="status"> 
								<tr class="rowid" id="fieldrow_${i}">
									<td id="lineId_${i}">${i}</td>
									<td>
										<input type="text" readonly="readonly" class="width50 linedate" id="linedate_${i}" style="border:0px;"/>
									</td> 
									<td>
										<input type="text" class="width70 lineamount" id="amount_${i}" style="border:0px;text-align: right;"/>
									</td> 
									<td>
										<input type="text" class="width98 linedecription" id="linedecription_${i}" style="border:0px;"/>
									</td> 
									<td style="display:none;">
												<input type="hidden" id="loanLineId_${i}" style="border:0px;"/>
									</td> 
									 <td style="width:0.01%;" class="opn_td" id="option_${i}">
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="display:none;cursor:pointer;" title="Add Record">
				 								<span class="ui-icon ui-icon-plus"></span>
										  </a>	
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
										  </a> 
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none;cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
										  </a>
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
										  </a>
									</td>   
								</tr>
							</c:forEach>
					 </tbody>
				</table>
			</div> 
		</fieldset>
	</div>  
	</div>  
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
		<div class="portlet-header ui-widget-header float-right save" id="${showPage}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	 </div> 
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		<div class="portlet-header ui-widget-header float-left addrowsc" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div>  
	<div class="clearfix"></div> 
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="popup-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
	<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>
  </form>
  </div> 
</div>