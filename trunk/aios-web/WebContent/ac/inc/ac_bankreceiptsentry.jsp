<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var bankReceiptsDetail = "";
var accessCode= "";
var slidetab = ""; var tempid = ""; var accountTypeId = 0;
var actionname = "";
var clearingImplAccount = false;
var pdcImplAccount =false;
var sectionRowId = 0;
var payeetype = "";
var getBankReceiptsDetails =null;
$(function(){
	$jquery(".amount,.totalReceiptAmountH").number(true, 2); 
	if(Number($('#bankReceiptsId').val() > 0) || '${RECEIPT_FROM}'!='RECEIPT'){  
		$('#receiptsTypeId').val($('#receiptsTypedb').val());
		$('#currencyId').val($('#currencydb').val()); 
		if("${BANK_RECEIPTS.supplier}" != null && "${BANK_RECEIPTS.supplier}" !=""){
			payeetype = "SPL"; 
			$('#supplierId').val("${BANK_RECEIPTS.supplier.supplierId}"); 
		} else if("${BANK_RECEIPTS.customer}" != null && "${BANK_RECEIPTS.customer}" !=""){
			payeetype = "CST"; 
			$('#supplierId').val("${BANK_RECEIPTS.customer.customerId}");
		}else if("${BANK_RECEIPTS.personByPersonId}" != null && "${BANK_RECEIPTS.personByPersonId}" !=""){
			payeetype = "EMP"; 
			$('#supplierId').val("${BANK_RECEIPTS.personByPersonId.personId}");
		}else if("${BANK_RECEIPTS.others}" != null && "${BANK_RECEIPTS.others}" !=""){
			payeetype = "OT"; 
			$('#supplierName').attr('readOnly',false);
			$('#supplier').hide();
			$('#expensecode').show();
		} else{
			payeetype = "OT"; 
			$('#supplierName').attr('readOnly',false);
			$('#supplier').hide();
			$('#expensecode').show();
		} 
		$('#payeeType option').each(function(){
			var thisval = $(this).val();
			var thisarray = thisval.split('@@'); 
			if(thisarray[1]==payeetype){
				$('#payeeType option[value='+$(this).val()+']').attr("selected","selected"); 
				return false; 
			}
		}); 

		$('.paymentMode').each(function(){
			var rowId = getRowId($(this).attr('id')); 
			var paymodedb = $('#paymentModedb_'+rowId).val();
			if(typeof (paymodedb)!='undefined'){
				$('#paymentMode_'+rowId+' option').each(function(){
					var thisval = $(this).val();
					var thisarray = thisval.split('@@'); 
					if(thisarray[1] == 'CSH'){
						if(thisarray[0] == paymodedb){
							$('#cashcode_'+rowId).show();
						}
					} 
					if(thisarray[0]==paymodedb){
						$('#paymentMode_'+rowId+' option[value='+$(this).val()+']').attr("selected","selected"); 
						return false; 
					}
				}); 
			} 
		});	
 
		$('.tempreferenceDetail').each(function(){
			var rowId = getRowId($(this).attr('id')); 
			$('#referenceDetail_'+rowId).val($('#tempreferenceDetail_'+rowId).val()); 
		});	
		
		$('.paymentStatus').each(function(){
			var rowId = getRowId($(this).attr('id')); 
			var paymentstatusdb = $('#paymentStatusdb_'+rowId).val();
			if(typeof (paymentstatusdb)!='undefined'){
				$('#paymentStatus_'+rowId+' option').each(function(){
					var thisval = $(this).val();
					var thisarray = thisval.split('@@'); 
					if(thisarray[0]==paymentstatusdb){
						$('#paymentStatus_'+rowId+' option[value='+$(this).val()+']').attr("selected","selected"); 
						return false; 
					}
				}); 
			} 
		});	
		
		if('${RECEIPT_FROM}'!='RECEIPT'){
			$('#receiptsDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'}); 
			formDisabled();
		}else{
			$('#receiptsDate').datepick(); 
		}
		calculateTotalReceipt();
	}else{
		$('#currencyId').val($('#defaultCurrency').val()); 
		$('#receiptsDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});  
	} 
	 
	$jquery("#bankReceiptsForm").validationEngine('attach'); 
	
	if('${RECEIPT_FROM}'=='RECEIPT') {
		$('.institutionDate').datepick({onSelect: validateCheque, showTrigger: '#calImg'}); 
		manupulateLastRow();
	}

	if('${RECEIPT_FROM}'=='RELEASE') {
		$('.institutionDate').datepick({onSelect: validateCheque, showTrigger: '#calImg'});  
	}


	$('.bankreceipts-lookup').click(function(){
         $('.ui-dialog-titlebar').remove(); 
         accessCode=$(this).attr("id"); 
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/add_lookup_detail.action",
              data:{accessCode: accessCode},
              async: false,
              dataType: "html",
              cache: false,
              success:function(result){ 
                   $('.common-result').html(result);
                   $('#common-popup').dialog('open');
   				   $($($('#common-popup').parent()).get(0)).css('top',0);
                   return false;
              },
              error:function(result){
                   $('.common-result').html(result);
              }
          });
           return false;
  	});	 

	$('.reference_category_lookup').live('click',function(){
        $('.ui-dialog-titlebar').remove(); 
        var str = $(this).attr("id");
        accessCode = str.substring(0, str.lastIndexOf("_"));
        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
		if(accessCode=="RECEIPT_TYPES"){
			$('#receiptsTypeId').html("");
			$('#receiptsTypeId').append("<option value=''>Select</option>");
			loadLookupList("receiptsTypeId"); 
		} else if(accessCode=="BANK_RECEIPT_REFERENCE"){
			$('#referenceDetail_'+sectionRowId).html("");
			$('#referenceDetail_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("referenceDetail_"+sectionRowId); 
		}   
	});
	 
	//Combination pop-up config
    $('.codecombination-popup').live('click',function(){
        tempid=$(this).parent().get(0);
        accountTypeId=0; 
        actionname="";
        $('.codeCombinationformError').remove();
        $('.ui-dialog-titlebar').remove();  
        actionname=$(tempid).attr('id');
        var idVal=actionname.split('_');
        if(typeof(idVal[1]!='undefined') && idVal[1]!=null && idVal[1]!=""){
      	  actionname=idVal[0];
        }
        else{
      	  actionname=$(tempid).attr('id');
        }  
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/combination_treeview.action",
             async: false, 
             dataType: "html",
             cache: false,
             success:function(result){
                  $('.codecombination-result').html(result); 
             },
             error:function(result){
                  $('.codecombination-result').html(result);
             }
         });
         return false;
    }); 
     
     $('#codecombination-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800, 
			bgiframe: false,
			modal: true 
	 });

     $('#customer-common-popup').live('click',function(){  
    	 $('#common-popup').dialog('close'); 
     });

     $('#supplier-common-popup').live('click',function(){  
    	 $('#common-popup').dialog('close'); 
     });

     $('.receiptsupplier-common-popup').click(function(){  
  	    $('.ui-dialog-titlebar').remove();  
  	    var idname= "";
  	    idname = $(this).attr('id'); 
  	    var rowId=-1; 
  	    if(idname == "supplier"){ 
  			var payeetypes = $('#payeeType').val();
  			var payTypeArray = payeetypes.split('@@'); 
  			if(payTypeArray[1]=="CST")  
  				actionname="show_customer_common_popup";
  			else if(payTypeArray[1]=="SPL") 
  				actionname="show_common_supplier_popup";
  			else if(payTypeArray[1]=="EMP")
  				actionname="common_person_list"; 
  		}  
   		$.ajax({
  				type:"POST",
  				url:"<%=request.getContextPath()%>/"+actionname+".action", 
  			 	async: false,  
  			 	data : {pageInfo : "", id: rowId, personTypes: "1"}, 
  			    dataType: "html",
  			    cache: false,
  				success:function(result){  
  					 $('.common-result').html(result);  
  					$('#common-popup').dialog('open'); 
  					 $(
  							$(
  									$('#common-popup')
  											.parent())
  									.get(0)).css('top',
  							0); 
  				},
  				error:function(result){  
  					 $('.common-result').html(result);  
  				}
  			});  
  			return false;
  		});

		
 		$('#common-popup').dialog({
 			 autoOpen: false,
 			 minwidth: 'auto',
 			 width:800, 
 			 bgiframe: false,
 			 overflow:'hidden',
 			 modal: true 
 		});

     $('#payeeType').change(function(){
		$('#supplierName').val(''); 
		$('#codeCombination').val(''); 
		$('#codeCombinationId').val(''); 
		if($(this).val()!=null && $(this).val()!=''){
			var payeetypes = $('#payeeType').val();
			var payTypeArray = payeetypes.split('@@');  
			if(payTypeArray[1]=="OT"){
				$('.supplier-popup-span').hide();
				$('#expensecode').show();
				$('#supplierName').attr('readOnly',false);
			} else {
				$('.supplier-popup-span').show();
				$('#expensecode').hide();
				$('#supplierName').attr('readOnly',true);
			}
		}
	}); 

     $('.delrow').live('click',function(){ 
		 slidetab="";
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
   	 	 $('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });   
	 });

     $('.amount').live('change',function(){ 
		triggerAddRow(getRowId($(this).attr('id')));
		return false;
	});
     
     $('.paymentMode').live('change',function(){   
 		var paymode = $(this).val();
 		var modeArray = paymode.split('@@');
 		var rowId=getRowId($(this).attr('id'));  
 		$('#institutionName_'+rowId+',#institutionRef_'+rowId+',#institutionDate_'+rowId+',#paymentStatus_'+rowId).val('');
 		$('#institutionName_'+rowId+',#institutionRef_'+rowId+',#institutionDate_'+rowId).attr('disabled', false); 
 		if(modeArray[1]=='CSH'){ 
 	 		$('#cashcode_'+rowId).show();
 	 		$('#institutionName_'+rowId+',#institutionRef_'+rowId+',#institutionDate_'+rowId).attr('disabled', true); 
 	 	}else if(modeArray[1]=='CCD' || modeArray[1]=='DCD'){ 
  	 		$('#institutionDate_'+rowId).attr('disabled', true); 
 	 	} 
 		triggerAddRow(getRowId($(this).attr('id')));
 		return false;
 	});

     clearingImplAccount = '${CLEARING_ACCOUNT}';
     pdcImplAccount = '${PDC_RECEIVED}';

     $('.paymentStatus').live('change',function(){  
  		var paystatus = $(this).val();
  		var statusArray = paystatus.split('@@');
  		var rowId=getRowId($(this).attr('id'));  
  		var paymode = $('#paymentMode_'+rowId).val();
  		var modeArray = paymode.split('@@');   
  		if(statusArray[1]=='PDC'){  
  	  		if(modeArray[1]=='CHQ' || modeArray[1]=='WRT'){ 
  	 			$(this).val('');
  	 		} else { 
  	 			validateChequeDate($('#institutionDate_'+rowId).val());
  	 			if(Number($('#pdcYr').val()) >= 0){
  	 				if(modeArray[1]=='CHQ' || modeArray[1]=='WRT'){  
  	 		 			if(pdcImplAccount == "true" || pdcImplAccount == true){
	  	 		 			if(Number($('#pdcMonth').val()) > 0){
	  	  	 					$('#paymentStatus_'+rowId).val($(this).val());
	  	  	 				} else {
	  	  	 					$(this).val('');
	  	  	  	 			}
  	 		 			}else{
  	 		 				$(this).val('');
  	  	 		 		}
  	 		 		}	 
  	 				triggerAddRow(rowId);
  	 			} else {
  	 				$(this).val('');
  	 				$('#line-error')
					.hide()
					.html("Please configure PDC Account in setup wizard.")
					.slideDown(1000);
			$('#line-error').delay(
					2000).slideUp();
			return false;
  	  	 		}
  	  	 	} 
  	 	} else { 
  	 		if(modeArray[1]=='CHQ' || modeArray[1]=='WRT'){   
  	 			if(clearingImplAccount == "true" || clearingImplAccount == true){ 
  	 				validateChequeDate($('#institutionDate_'+rowId).val());
  	  	 			if(Number($('#pdcYr').val()) >= 0){
  	  	 				if(Number($('#pdcMonth').val()) > 0){
  	  	 					$('#paymentStatus_'+rowId+' option').each(function(){
  	  	  	 					var thisval = $(this).val();
  	  	  	 					statusArray = thisval.split('@@');
  	  	  	 					if(statusArray[1] == 'PDC')
  	  	  	 						$('#paymentStatus_'+rowId).val(thisval);
  	  	  	 			    });
  	  	 				}  
  	  	 			} 
  	 			} else{
  	 				$('#paymentStatus_'+rowId).val('');
  	 				$('#line-error')
					.hide()
					.html("Please configure Clearing Account in setup wizard.")
					.slideDown(1000);
			$('#line-error').delay(
					2000).slideUp();
			return false;
  	  	 		}
  	  	 	}
  	 		triggerAddRow(rowId);
  	  	 } 
  		return false;
  	});
     
     
     $('.salesactiveInvoice').live('change',function(){
 		var disabledflag = false;
 		$('.salesactiveInvoice').each(function(){
 			 if($(this).attr('checked') == false && disabledflag != true){
 				 disabledflag = false; 
 			 } 
 			 else{
 				 disabledflag = true;
 			 } 
 		});
 		$('#currencyId').attr('disabled', disabledflag);
 		var salesInvoiceId = getRowId($(this).attr('id'));
 		
 		if($(this).attr('checked') == true){
  			$.ajax({
 				type:"POST",
 				url:"<%=request.getContextPath()%>/show_salesinvoicedata_receiver.action", 
 			 	async: false,
 			 	data:{salesInvoiceId: salesInvoiceId},
 			    dataType: "json",
 			    cache: false,
 				success:function(response){ 
 					 if(response != null && response.slInvoiceVO != null){
 						 var rowId = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
 						 $('#amount_'+rowId).attr('readonly',true);
  						 $('#amount_'+rowId).val(response.slInvoiceVO.invoiceAmount);
 						 $('#linedescription_'+rowId).val(response.slInvoiceVO.description);
 						 $('#recordId_'+rowId).val(response.slInvoiceVO.salesInvoiceId);
 						 $('#useCase_'+rowId).val(response.slInvoiceVO.useCase);
 						 triggerAddRowWithoutCheck(rowId);
 					 }
 				}
 			});
 			return false;
 		} else{
 			var deleteRowId = Number(0);
 			$('.rowid').each(function(){   
 	  		 	var rowId=getRowId($(this).attr('id')); 
 	  		 	var recordId = Number($('#recordId_'+rowId).val()); 
 	  		 	if(recordId == salesInvoiceId){
 	  		 		deleteRowId = rowId; 
 	  		 	}
 			}); 
 	     	$('#fieldrow_'+deleteRowId).remove();  
 	     	var i=1;
 	   	 	$('.rowid').each(function(){   
 	   		 	var rowId=getRowId($(this).attr('id')); 
 	   		 	$('#lineId_'+rowId).html(i);
 				 i=i+1; 
 			});   
  	     	return false;
 		} 
 	}); 

     $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/bankreceipts_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 

     $('#receipts_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receipts_voucher.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#codecombination-popup').dialog('destroy');		
				$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});
     
     $('#receipts_save_print').click(function(){
    	 receiptPayment(true);
     });
     $('#receipts_save').click(function(){
    	receiptPayment(false);
	 });

		getBankReceiptsDetails = function() {
			var amountarr = new Array();
			var descarr = new Array();
			var modearr = new Array();
			var bankReceiptsLines = new Array();
			var namearr = new Array();
			var namerefarr = new Array();
			var datearr = new Array();
			var refarr = new Array();
			var combinationArr = new Array();
			var codeArr = new Array(); 
			var recordIdArray = new Array();
			var useCaseArray = new Array();
			var receiptsDetail = "";
			var flagArr = new Array();
			$('.rowid')
					.each(
							function() {
								var rowId = getRowId($(this).attr('id'));
								var payentMode = $('#paymentMode_' + rowId)
										.val();
								var payTypeArray = payentMode.split('@@');
								payentMode = payTypeArray[0];
								var accessCode = payTypeArray[1];
								var amount = $jquery('#amount_' + rowId).val();
								var lineDescription = $(
										'#linedescription_' + rowId).val();
								var institutionName = $(
										'#institutionName_' + rowId).val();
								var institutionRef = $(
										'#institutionRef_' + rowId).val();
								var institutionDate = $(
										'#institutionDate_' + rowId).val();
								var referenceDetail = Number($(
										'#referenceDetail_' + rowId).val());
								var changeFlag = Number($(
										'#changeFlag_' + rowId).val());
								var bankReceiptsLineId = Number($(
										'#bankReceiptsLineId_' + rowId).val());
								var combinationId = Number($(
										'#combinationId_' + rowId).val()); 
								var recordId = Number($(
										'#recordId_' + rowId).val());
								var useCase = $('#useCase_' + rowId)
										.val();
								if (typeof amount != 'undefined'
										&& amount != null && amount != ""
										&& payentMode != null
										&& payentMode != "") {
									modearr.push(payentMode);
									amountarr.push(amount);
									if (typeof institutionName == 'undefined'
											|| institutionName == null
											|| institutionName == "")
										namearr.push("##");
									else
										namearr.push(institutionName);
									if (typeof institutionRef == 'undefined'
											|| institutionRef == null
											|| institutionRef == "")
										namerefarr.push("##");
									else
										namerefarr.push(institutionRef);
									if (typeof institutionDate == 'undefined'
											|| institutionDate == null
											|| institutionDate == "")
										datearr.push("##");
									else
										datearr.push(institutionDate);
									refarr.push(referenceDetail); 
									if (lineDescription != null
											&& lineDescription != "")
										descarr.push(lineDescription);
									else
										descarr.push("##");
									combinationArr.push(combinationId);
									bankReceiptsLines.push(bankReceiptsLineId);
									codeArr.push(accessCode); 
									flagArr.push(changeFlag);
									recordIdArray.push(recordId);
									useCaseArray.push(useCase);
								}
							});
			for ( var j = 0; j < modearr.length; j++) {
				receiptsDetail += modearr[j] + "__" + amountarr[j] + "__"
						+ descarr[j] + "__" + namearr[j] + "__" + namerefarr[j]
						+ "__" + datearr[j] + "__" + refarr[j] + "__"
						+ combinationArr[j] + "__" + bankReceiptsLines[j]
						+ "__" + codeArr[j] + "__" +
						+ flagArr[j] + "__" + recordIdArray[j] + "__"
						+ useCaseArray[j];
				if (j == modearr.length - 1) {
				} else {
					receiptsDetail += "@@";
				}
			}
			return receiptsDetail;
		};
	});
	function receiptPayment(printFlag){
		bankReceiptsDetail="";
    	if($jquery("#bankReceiptsForm").validationEngine('validate')){
  			var bankReceiptsId = Number($('#bankReceiptsId').val()); 
 			var receiptsNo = $('#receiptsNo').val();
 			var receiptsDate = $('#receiptsDate').val();
 			var currencyId = $('#currencyId').val();
 			var referenceNo = $('#referenceNo').val();
 			var supplierId = Number($('#supplierId').val()); 
 			var codeCombinationId = Number($('#codeCombinationId').val());
   			var vendorName = $('#supplierName').val();
  			var alertId = Number($('#alertId').val());
 			var payeeType = $('#payeeType').val();
 			var payTypeArray = payeeType.split('@@');
 			payeeType = payTypeArray[1];
 			var description=$('#description').val();  
 			var receiptsTypeId = $('#receiptsTypeId').val();
 		 	var useCaseName = $('#useCaseName').val();
 		 	var useCaseId = Number($('#useCaseId').val()); 
 			bankReceiptsDetail = getBankReceiptsDetails();  
   			if(bankReceiptsDetail!=null && bankReceiptsDetail!=""){
 				$.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/save_bankreceipts_info.action", 
 				 	async: false, 
 				 	data:{	bankReceiptsId: bankReceiptsId, receiptNo: receiptsNo, currencyId: currencyId, description: description,
 					 		vendorName: vendorName, referenceNo: referenceNo, supplierId: supplierId, receiptsDate: receiptsDate,
 					 		payeeType: payeeType, combinationId: codeCombinationId, bankReceiptLines: bankReceiptsDetail, 
 					 		receiptsTypeId: receiptsTypeId, alertId: alertId, useCaseId: useCaseId, useCaseName: useCaseName
 					 	 },
 				     dataType: "json",
 				    cache: false,
 					success:function(response){  
 						 var message=response.returnMessage;  
 						 if(message == "SUCCESS"){ 
 							 $.ajax({
 									type:"POST",
 									url:"<%=request.getContextPath()%>/show_receipts_voucher.action",
																	async : false,
																	dataType : "html",
																	cache : false,
																	success : function(
																			result) {
																		$(
																				'#common-popup')
																				.dialog(
																						'destroy');
																		$(
																				'#common-popup')
																				.remove();
																		$(
																				'#codecombination-popup')
																				.dialog(
																						'destroy');
																		$(
																				'#codecombination-popup')
																				.remove();
																		$(
																				"#main-wrapper")
																				.html(
																						result);
																		if (bankReceiptsId == 0)
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record created.")
																					.slideDown(
																							1000);
																		else
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record updated.")
																					.slideDown(
																							1000);
																		$(
																				'#success_message')
																				.delay(
																						2000)
																				.slideUp();
																	}
																});
													} else {
														$('#page-error')
																.hide()
																.html(message)
																.slideDown(1000);
														$('#page-error').delay(
																2000).slideUp();
														return false;
													}
												},
												error : function(result) {
													$('#page-error')
															.hide()
															.html(
																	"Internal error.")
															.slideDown(1000);
													$('#page-error')
															.delay(2000)
															.slideUp();
												}
											});
								} else {
									$('#page-error')
											.hide()
											.html(
													"Please enter receipt details.")
											.slideDown(1000);
									$('#page-error').delay(2000).slideUp();
									return false;
								}
							} else {
								return false;
							}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function triggerAddRow(rowId) {
		var paymentmode = $('#paymentMode_' + rowId).val();
		var amount = $('#amount_' + rowId).val(); 
		var nexttab = $('#fieldrow_' + rowId).next(); 
		calculateTotalReceipt(); 
		if (paymentmode != null && paymentmode != "" && amount != null
				&& amount != "" && $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
		$('#changeFlag_' + rowId).val(Number(1)); 
	}
	function triggerAddRowWithoutCheck(rowId) { 
		calculateTotalReceipt(); 
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
 	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:20px;\"></td>");
		}
	}
	function setCombination(combinationTreeId, combinationTree) {
		if (actionname == "expensecode") {
			$('#codeCombinationId').val(combinationTreeId);
			$('.codeCombination').val(combinationTree);
		} else if (actionname == "cashcode") {
			var rowId = getRowId($(tempid).attr('id'));
			$('#combinationId_' + rowId).val(combinationTreeId);
			$('#combinationlines_' + rowId).val(combinationTree);
			triggerAddRow(rowId);
			return false;
		}
		actionname = "";
	}
	function validateChequeDate(chequeDate) {
		fromDate = $('#periodDate').val();
		putYears = ('#pdcYr');
		putMonths = $('#pdcMonth');
		putDays = $('#pdcDate');
		toDate = chequeDate;
		calculateDate(fromDate, toDate, putYears, putMonths, putDays);
	}
	
	function validateCheque(){
		var rowId=getRowId($(this).attr('id'));   
    	var institutionDate = $('#institutionDate_'+rowId).val();
    	var paymode = $('#paymentMode_'+rowId).val();
  		var modeArray = paymode.split('@@');
  		$('#cashcode_'+rowId).show();
  		if(modeArray[1]=='CHQ'){ 
  			$.ajax({
  				type:"POST",
  				url:"<%=request.getContextPath()%>/validate_receipt_pdccheque.action", 
  			 	async: false,  
  			 	data : {institutionDate: institutionDate}, 
  			    dataType: "json",
  			    cache: false,
  				success:function(response){  
  					 if(response != null && response.bankReceiptsObj != null){
  						$('#combinationId_'+rowId).val(response.bankReceiptsObj.combinationId);
  						$('#combinationlines_'+rowId).val(response.bankReceiptsObj.combinationAccount);
  						$('#cashcode_'+rowId).hide();
  					 }
  				} 
  			}); 
  		} 
	}
	
	function formDisabled() {
		//$('#bankReceiptsForm').attr("disabled",true);
		$("#bankReceiptsForm :input").attr("readOnly", true);
		$("select").each(function() {
			$(this).attr("disabled", true);
		});

		$(".paymentStatus,.linedescription,.description,.paymentMode,#receiptsTypeId,#referenceNo").attr("disabled",
				false);
		$(".paymentStatus,.linedescription,.description,#referenceNo").attr("readOnly",
				false);

		if ('${RECEIPT_FROM}' == 'RELEASE' || '${RECEIPT_FROM}' == 'SALESINVOICE' || '${RECEIPT_FROM}' == 'PROJECTPAYMENT') {
			$("tr :input").attr("readOnly", false);
			$("tr :input").attr("disabled", false);
		}
		calculateTotalReceipt();
	}
	function personPopupResult(personid, personname, commonParam) {
		$('#supplierId').val(personid);
		$('#supplierName').val(personname);
		$('#common-popup').dialog("close");
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getperson_combination_details.action",  
		 	async: false,
		 	data:{personId: personid},
		    dataType: "json",
		    cache: false,
			success:function(response){  
				$('#codeCombinationId').val(response.personObject.combinationId);
				$('#codeCombination').val(response.personObject.accountCode);
			} 
		});
		return false;
	} 
	
	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, commonParam) {
		$('#supplierId').val(customerId);
		$('#supplierName').val(customerName);
		$('#codeCombinationId').val(combinationId);
		$('#codeCombination').val(accountCode); 
		var currencyId = Number($('#currencyId').val()); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_activeinvoices.action",
			async : false,
			data : {
				customerId : customerId, currencyId: currencyId
			},
			dataType : "html",
			cache : false,
			success : function(result) {
				$(".customerinvoice").html(result);
 			}
		});
		return false;
	}

	function commonSupplierPopup(supplierId, supplierName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
		$('#codeCombinationId').val(combinationId);
		$('#codeCombination').val(accountCode);
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}

	function calculateTotalReceipt() {
		var totalReceipt = 0;
		$('#totalReceiptAmount').text('');
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var amount = Number($.trim($jquery('#amount_' + rowId).val()));
			totalReceipt = Number(totalReceipt + amount);
		});
		$jquery('.totalReceiptAmountH').val(totalReceipt);
		$('#totalReceiptAmount').text($('.totalReceiptAmountH').val());
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			receipts
		</div> 
		<form name="bankReceiptsForm" id="bankReceiptsForm" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="tempresult" style="display: none;"></div>
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<input type="hidden" name="alertId" id="alertId"
							value="${alertId}" />
						<input type="hidden" name="useCaseName" id="useCaseName"
							value="${BANK_RECEIPTS.useCase}" />
						<input type="hidden" name="useCaseId" id="useCaseId"
							value="${BANK_RECEIPTS.recordId}" />
						<fieldset>
							<div>
								<label class="width30" for="payeeType">Receipts Source<span
									class="mandatory">*</span> </label> <select name="payeeType"
									id="payeeType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach items="${RECEIPT_SOURCE}" var="SOURCE">
										<option value="${SOURCE.key}">${SOURCE.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30">Receiver</label>
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.supplier.person ne null && BANK_RECEIPTS.supplier.person ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.supplier.person.firstName} ${ BANK_RECEIPTS.supplier.person.lastName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.supplier.company ne null && BANK_RECEIPTS.supplier.company ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.supplier.company.companyName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.supplier.cmpDeptLocation ne null && BANK_RECEIPTS.supplier.cmpDeptLocation ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.supplier.cmpDeptLocation.company.companyName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.customer.personByPersonId ne null && BANK_RECEIPTS.customer.personByPersonId ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.customer.personByPersonId.firstName} ${BANK_RECEIPTS.customer.personByPersonId.lastName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.customer.company ne null && BANK_RECEIPTS.customer.company ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.customer.company.companyName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.customer.cmpDeptLocation ne null && BANK_RECEIPTS.customer.cmpDeptLocation ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.customer.cmpDeptLocation.company.companyName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.personByPersonId ne null && BANK_RECEIPTS.personByPersonId ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.personByPersonId.firstName} ${BANK_RECEIPTS.personByPersonId.lastName}"
											id="supplierName" class="width50" readonly="readonly" />
									</c:when>
									<c:when
										test="${BANK_RECEIPTS.others ne null && BANK_RECEIPTS.others ne '' }">
										<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.others}" id="supplierName"
											class="width50" readonly="readonly" />
									</c:when>
									<c:otherwise>
										<input type="text" name="supplierName" id="supplierName"
											class="width50" readonly="readonly" />
									</c:otherwise>
								</c:choose>
								<span class="button width10 float-right supplier-popup-span"
									style="display: none; position: relative !important; top: 8px !important; right: 34px !important;">
									<a style="cursor: pointer;" id="supplier"
									class="btn ui-state-default ui-corner-all receiptsupplier-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="supplierId"
									id="supplierId" />
							</div>
							<div>
								<c:set var="otherstype" value="show"/>
								<label class="width30">Account Code<span
									class="mandatory">*</span> </label> <input type="hidden"
									name="combinationId" id="codeCombinationId"
									value="${BANK_RECEIPTS.combination.combinationId}" />
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.combination.combinationId ne null && BANK_RECEIPTS.combination.combinationId ne ''}">
										<c:choose>
											<c:when
												test="${BANK_RECEIPTS.combination.accountByAnalysisAccountId ne null  &&
														 BANK_RECEIPTS.combination.accountByAnalysisAccountId ne ''}">
												<input type="text" name="codeCombination"
													readonly="readonly" id="codeCombination"
													value="${BANK_RECEIPTS.combination.accountByAnalysisAccountId.code}[${BANK_RECEIPTS.combination.accountByAnalysisAccountId.account}]"
													class="codeCombination width50 validate[required]">
											</c:when>
											<c:otherwise>
												<input type="text" name="codeCombination"
													readonly="readonly" id="codeCombination"
													value="${BANK_RECEIPTS.combination.accountByNaturalAccountId.code}[${BANK_RECEIPTS.combination.accountByNaturalAccountId.account}]"
													class="codeCombination width50 validate[required]">
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<input type="text" name="codeCombination" readonly="readonly"
											id="codeCombination"
											class="codeCombination width50 validate[required]">
									</c:otherwise>
								</c:choose>  
								<span class="button width10" id="expensecode" style="display: none;" > <a
									style="cursor: pointer;" id="combination"
									class="btn ui-state-default ui-corner-all codecombination-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div>
							<div>
								<label class="width30">Description</label>
								<textarea rows="2" id="description" class="width50 description">${BANK_RECEIPTS.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset>
							<div>
								<label class="width30" for="receiptsNo">Receipt Number<span
									class="mandatory">*</span> </label> <input type="hidden"
									name="bankReceiptsId" id="bankReceiptsId"
									value="${BANK_RECEIPTS.bankReceiptsId}" />
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.bankReceiptsId ne null && BANK_RECEIPTS.bankReceiptsId ne ''}">
										<input type="text" readonly="readonly" name="receiptsNo"
											id="receiptsNo" class="width50 validate[required]"
											value="${BANK_RECEIPTS.receiptsNo}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="receiptsNo"
											id="receiptsNo" class="width50 validate[required]"
											value="${requestScope.BANK_RECEIPTS_NO}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="receiptsDate">Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.receiptsDate ne null && BANK_RECEIPTS.receiptsDate ne ''}">
										<c:set var="receiptsDate"
											value="${BANK_RECEIPTS.receiptsDate}" />
										<%
											String date = DateFormat.convertDateToString(pageContext
															.getAttribute("receiptsDate").toString());
										%>
										<input type="text" readonly="readonly" name="receiptsDate"
											id="receiptsDate" class="width50 validate[required]"
											value="<%=date%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="receiptsDate"
											id="receiptsDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="receiptsTypeId">Receipts
									Type<span class="mandatory">*</span> </label> <input type="hidden"
									id="receiptsTypedb"
									value="${BANK_RECEIPTS.lookupDetail.lookupDetailId}" /> <select
									name="receiptsType" id="receiptsTypeId"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="TYPES" items="${RECEIPT_TYPES}">
										<option value="${TYPES.lookupDetailId}">${TYPES.displayName}</option>
									</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="RECEIPT_TYPES"
									class="btn ui-state-default ui-corner-all bankreceipts-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="currencyId">Currency<span
									class="mandatory">*</span> </label> <input type="hidden"
									id="currencydb" value="${BANK_RECEIPTS.currency.currencyId}" />
								<select name="currencyId" id="currencyId"
									class="width51 validate[required]">
									<c:forEach var="CURRENCY" items="${CURRENCY_LIST}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="referenceNo">Ref.No</label> <input
									type="text" id="referenceNo"
									value="${BANK_RECEIPTS.referenceNo}" class="width50" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<input type="hidden" id="defaultCurrency" value="${DEFAULT_CURRENCY}" />
			<c:choose>
				<c:when test="${PERIOD_INFO ne null && PERIOD_INFO ne ''}">
					<c:set var="periodDate" value="${PERIOD_INFO}" />
					<%
						String periodDate = DateFormat.convertDateToString(pageContext
								.getAttribute("periodDate").toString());
					%>
				<input type="hidden" id="periodDate" value="<%=periodDate%>" /> 
				</c:when>
				<c:otherwise>
					<input type="hidden" id="periodDate" value="" />  
				</c:otherwise>
			</c:choose>
			 <input type="hidden" id="pdcMonth" />
			 <input
					type="hidden" id="pdcYr" />
			<input type="hidden" id="pdcDate" />
			<div class="clearfix"></div>
			<div class="width100 float-left customerinvoice" id="hrm"></div>	
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Receipts Detail<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Payment Mode</th>
									<th style="width: 5%">Institution Name</th>
									<th style="width: 5%">Instrument No</th>
									<th style="width: 5%">Instrument Date</th>
									<th style="display: none;">Reference</th>
									<th style="width: 5%">Account Code</th>
									<th style="width: 5%">Amount</th>
									<th style="display: none;">Status</th>
									<th style="width: 5%">Description</th>
									<th style="width: 2%"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.bankReceiptsDetails ne null && BANK_RECEIPTS.bankReceiptsDetails ne ''}">
										<c:forEach var="RECEIPTS_DETAILS"
											items="${BANK_RECEIPTS.bankReceiptsDetails}"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
												<td><select name="paymentMode" class="paymentMode"
													id="paymentMode_${status.index+1}">
														<option value="">Select</option>
														<c:choose>
															<c:when
																test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
																<c:forEach items="${MODE_OF_PAYMENT}" var="MODE">
																	<option
																		value="${MODE.key}">${MODE.value}</option>
																</c:forEach>
															</c:when>
														</c:choose>
												</select> <input type="hidden" id="paymentModedb_${status.index+1}"
													value="${RECEIPTS_DETAILS.paymentMode}" />
												</td>
												<td><input type="text" name="institutionName"
													id="institutionName_${status.index+1}"
													class="institutionName"
													value="${RECEIPTS_DETAILS.institutionName}" />
												</td>
												<td><input type="text" name="institutionRef"
													id="institutionRef_${status.index+1}"
													class="institutionRef"
													value="${RECEIPTS_DETAILS.bankRefNo}" />
												</td>
												<td><c:choose>
														<c:when
															test="${RECEIPTS_DETAILS.chequeDate ne null && RECEIPTS_DETAILS.chequeDate ne ''}">
															<c:set var="chequeDate"
																value="${RECEIPTS_DETAILS.chequeDate}" />
															<%
																String chequeDate = DateFormat
																							.convertDateToString(pageContext
																									.getAttribute("chequeDate")
																									.toString());
															%>
															<input type="text" name="institutionDate"
																readonly="readonly"
																id="institutionDate_${status.index+1}"
																class="institutionDate" value="<%=chequeDate%>" />
														</c:when>
														<c:otherwise>
															<input type="text" name="institutionDate"
																readonly="readonly"
																id="institutionDate_${status.index+1}"
																class="institutionDate" />
														</c:otherwise>
													</c:choose>
												</td>
												<td style="display: none;">
													<select name="referenceDetail" class="referenceDetail"
													id="referenceDetail_${status.index+1}">
														<option value="">Select</option>
														<c:choose>
															<c:when
																test="${RECEIPT_REFERENCE ne null && RECEIPT_REFERENCE ne ''}">
																<c:forEach items="${RECEIPT_REFERENCE}" var="reference">
																	<option
																		value="${reference.lookupDetailId}">${reference.displayName}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
													<span
															class="button" style="position: relative;"> <a
																style="cursor: pointer;" id="BANK_RECEIPT_REFERENCE_${status.index+1}"
																class="btn ui-state-default ui-corner-all reference_category_lookup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
													<input type="hidden"
													id="tempreferenceDetail_${status.index+1}"
													class="tempreferenceDetail"
													value="${RECEIPTS_DETAILS.lookupDetail.lookupDetailId}" />
												</td>
												<td><c:choose>
														<c:when
															test="${RECEIPTS_DETAILS.combination.accountByAnalysisAccountId ne null && RECEIPTS_DETAILS.combination.accountByAnalysisAccountId ne ''}">
															<input type="text" readonly="readonly"
																class="combinationlines width60"
																id="combinationlines_${status.index+1}"
																value="${RECEIPTS_DETAILS.combination.accountByAnalysisAccountId.account}" />
														</c:when>
														<c:otherwise>
															<input type="text" readonly="readonly"
																class="combinationlines width60"
																id="combinationlines_${status.index+1}"
																value="${RECEIPTS_DETAILS.combination.accountByNaturalAccountId.account}" />
														</c:otherwise>
													</c:choose> <input type="hidden" id="combinationId_${status.index+1}"
													value="${RECEIPTS_DETAILS.combination.combinationId}" /> <span
													class="button width10 float-right"
													id="cashcode_${status.index+1}"
													style="display: none; position: relative; top: 6px; right: 8px;">
														<a style="cursor: pointer;"
														id="combination_${status.index+1}"
														class="btn ui-state-default ui-corner-all codecombination-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="text"
													class="amount validate[optional,custom[number]]"
													id="amount_${status.index+1}" style="text-align: right;"
													value="${RECEIPTS_DETAILS.amount}" />
												</td>
												<td style="display: none;"><select name="paymentStatus" class="paymentStatus"
													id="paymentStatus_${status.index+1}">
														<option value="">Select</option>
														<c:choose>
															<c:when
																test="${PAYMENT_STATUS ne null && PAYMENT_STATUS ne ''}">
																<c:forEach items="${PAYMENT_STATUS}" var="PAY_STATUS">
																	<option
																		value="${PAY_STATUS.key}">${PAY_STATUS.value}</option>
																</c:forEach>
															</c:when>
														</c:choose>
												</select> <input type="hidden" id="paymentStatusdb_${status.index+1}"
													value="${RECEIPTS_DETAILS.paymentStatus}" />
												</td>
												<td><input type="text" name="linedescription"
													class="linedescription"
													id="linedescription_${status.index+1}"
													value="${RECEIPTS_DETAILS.description}" />
												</td>
												<td style="display: none;"><input type="hidden"
													id="bankReceiptsLineId_${status.index+1}"
													value="${RECEIPTS_DETAILS.bankReceiptsDetailId}" /> <input
													type="hidden" id="changeFlag_${status.index+1}" /> <input
													type="hidden" name="recordId"
													id="recordId_${status.index+1}"
													value="${RECEIPTS_DETAILS.recordId}" />
													<input type="hidden" name="useCase"
													id="useCase_${status.index+1}"
													value="${RECEIPTS_DETAILS.useCase}" />
												</td>
												<td style="width: 0.01%;" class="opn_td"
													id="option_${status.index+1}"><c:if
														test="${RECEIPT_FROM eq 'RECEIPT'}">
														<a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
															id="DeleteImage_${status.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</c:when>
								</c:choose>
								<c:if test="${RECEIPT_FROM eq 'RECEIPT'}">
									<c:forEach var="i"
										begin="${fn:length(BANK_RECEIPTS.bankReceiptsDetails)+0}"
										end="${fn:length(BANK_RECEIPTS.bankReceiptsDetails)+1}"
										step="1" varStatus="status1">
										<tr class="rowid" id="fieldrow_${status1.index+1}">
											<td id="lineId_${status1.index+1}" style="display: none;">${status1.index+1}</td>
											<td><select name="paymentMode" class="paymentMode"
												id="paymentMode_${status1.index+1}">
													<option value="">Select</option>
													<c:choose>
														<c:when
															test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
															<c:forEach items="${MODE_OF_PAYMENT}" var="MODE">
																<option
																	value="${MODE.key}">${MODE.value}</option>
															</c:forEach>
														</c:when>
													</c:choose>
											</select>
											</td>
											<td><input type="text" name="institutionName"
												id="institutionName_${status1.index+1}"
												class="institutionName">
											</td>
											<td><input type="text" name="institutionRef"
												id="institutionRef_${status1.index+1}"
												class="institutionRef">
											</td>
											<td><input type="text" name="institutionDate"
												readonly="readonly" id="institutionDate_${status1.index+1}"
												class="institutionDate">
											</td>
											<td style="display: none;"> 
												<select name="referenceDetail" class="referenceDetail"
													id="referenceDetail_${status1.index+1}">
														<option value="">Select</option>
														<c:choose>
															<c:when
																test="${RECEIPT_REFERENCE ne null && RECEIPT_REFERENCE ne ''}">
																<c:forEach items="${RECEIPT_REFERENCE}" var="reference">
																	<option
																		value="${reference.lookupDetailId}">${reference.displayName}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select> 
													<span
															class="button" style="position: relative;"> <a
																style="cursor: pointer;" id="BANK_RECEIPT_REFERENCE_${status.index+1}"
																class="btn ui-state-default ui-corner-all reference_category_lookup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
 											</td>
											<td><input type="text" readonly="readonly" value="${requestScope.COMBINATION_LINECODE}"
												class="combinationlines width60"
												id="combinationlines_${status1.index+1}" /> <input value="${requestScope.COMBINATION_LINEID}"
												type="hidden" id="combinationId_${status1.index+1}" /> <span
												class="button width10 float-right"
												id="cashcode_${status1.index+1}"
												style="position: relative; top: 6px; right: 8px;">
													<a style="cursor: pointer;"
													id="combination_${status1.index+1}"
													class="btn ui-state-default ui-corner-all codecombination-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text"
												class="amount validate[optional,custom[number]]"
												id="amount_${status1.index+1}" style="text-align: right;" />
											</td>
											<td style="display: none;"><select name="paymentStatus" class="paymentStatus"
												id="paymentStatus_${status1.index+1}">
													<option value="">Select</option>
													<c:choose>
														<c:when
															test="${PAYMENT_STATUS ne null && PAYMENT_STATUS ne ''}">
															<c:forEach items="${PAYMENT_STATUS}" var="PAY_STATUS">
																<option
																	value="${PAY_STATUS.key}">${PAY_STATUS.value}</option>
															</c:forEach>
														</c:when>
													</c:choose>
											</select>
											</td>
											<td><input type="text" name="linedescription"
												id="linedescription_${status1.index+1}" />
											</td>
											<td style="display: none;"><input type="hidden"
												id="bankReceiptsLineId_${status1.index+1}" /> <input
												type="hidden" id="changeFlag_${status1.index+1}" /> <input
												type="hidden" name="recordId"
												id="recordId_${status1.index+1}" /> <input
												type="hidden" name="useCase"
												id="useCase_${status1.index+1}" />
											</td>
											<td style="width: 0.01%;" class="opn_td"
												id="option_${status1.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status1.index+1}"
												style="display: none; cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a>
											</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="width35 float-right" style="font-weight: bold;">
				<span style="font-weight: bold;">Total: </span> <span
					id="totalReceiptAmount"></span>
				<input type="text" class="totalReceiptAmountH" style="display: none;" readonly="readonly"/>	
			</div>
			<div class="clearfix"></div>
 			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="receipts_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="receipts_save_print" style="cursor: pointer; display: none;">Save & Print
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="receipts_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>