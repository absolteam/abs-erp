<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend>Active Material Requisition</legend>
	<div>
		<c:forEach var="ACTIVE_REQUISITION" items="${MATERIAL_REQUISITION_INFO}">
			<label for="requisition_${ACTIVE_REQUISITION.materialRequisitionId}" class="width10">
				<input type="checkbox" name="activeRQNumber" class="activeRQNumber" id="requisition_${ACTIVE_REQUISITION.materialRequisitionId}"/>
				${ACTIVE_REQUISITION.referenceNumber}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultActiveRQSelect($('.activeRQNumber').attr('id'));
	$('.activeRQNumber').click(function(){   
		validatRequisitionInfo($(this));   
	});
});
</script>