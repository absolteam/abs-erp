<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.ui-widget-overlay,.ui-dialog {
	z-index: 100000 !important;
}
</style>
<div id="hrm" class="portlet-content width100 invoiceline-info"
	style="height: 90%;">
	<div class="portlet-content">
		<div id="page-error-popup"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<form id="productCalcValidation" method="POST"
			style="position: relative;">
			<div id="warning_message-popup"
				class="response-msg notice ui-corner-all width90"
				style="display: none;"></div>
			<input type="hidden" id="parentId" value="${requestScope.parentId}" />
			<div id="hrm" class="hastable width100">
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th>Pricing Type</th>
							<th>Calculation Type</th>
							<th>Sub Type</th>
							<th>Price</th>
							<th>Calculated Price</th>
							<c:if test="${sessionScope.unified_price eq 'true'}">
								<th>Store</th>
							</c:if>
							<th>Customer</th>
							<th style="display: none;">Default</th>
							<th>Description</th>
							<th><fmt:message key="accounts.common.label.options" />
							</th>
						</tr>
					</thead>
					<tbody class="tabcalc">
						<c:forEach var="CALCULATION" varStatus="status"
							items="${PRICING_CALCULATIONS}">
							<tr class="rowidcalc" id="fieldrowcalc_${status.index+1}">
								<td style="display: none;" id="lineIdcalc_${status.index+1}">${status.index+1}</td>
								<td><select name="pricingType"
									id="pricingType_${status.index+1}"
									class="existingPriceType pricingType width70">
										<option value="">Select</option>
										<c:forEach var="TYPE" items="${PRICING_TYPE}">
											<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
										</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
										style="cursor: pointer;"
										id="PRODUCT_PRICING_LEVEL_${status.index+1}"
										class="btn ui-state-default ui-corner-all product-pricing-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" id="tempPricingType_${status.index+1}"
									value="${CALCULATION.lookupDetail.lookupDetailId}" /></td>
								<td><select name="calculationType"
									id="calculationType_${status.index+1}"
									class="existingCalculationType 
								calculationType width98">
										<option value="">Select</option>
										<c:forEach var="CAL_TYPE" items="${CALCULATION_TYPE}">
											<option value="${CAL_TYPE.key}">${CAL_TYPE.value}</option>
										</c:forEach>
								</select> <input type="hidden" id="tempCalculationType_${status.index+1}"
									value="${CALCULATION.calculationType}" /></td>
								<td><select name="calculationSubType"
									id="calculationSubType_${status.index+1}"
									class="existingCalculationSubType
								 calculationSubType width98">
										<option value="">Select</option>
										<c:forEach var="SUBTYPE" items="${CALCULATION_SUB_TYPE}">
											<option value="${SUBTYPE.key}">${SUBTYPE.value}</option>
										</c:forEach>
								</select> <input type="hidden"
									id="tempCalculationSubType_${status.index+1}"
									value="${CALCULATION.calculationSubType}" /></td>
								<td><input type="text" id="priceValue_${status.index+1}"
									name="priceValue"
									class="priceValue width98 validate[optional,custom[number]]"
									style="text-align: right;" value="${CALCULATION.salePrice}" />
								</td>
								<td id="calculatedPrice_${status.index+1}"
									style="text-align: right;">${CALCULATION.salePriceAmount}</td>
								<c:if test="${sessionScope.unified_price eq 'true'}">
									<td>
										<span id="store_${status.index+1}">${CALCULATION.storeName}</span> 
									</td>
								</c:if>
								<td> 
									<span>${CALCULATION.customerName}</span>
									<div id="customerInfo_${status.index+1}">
										<div id="div_${status.index+1}${CALCULATION.customer.customerId}${status.count}" 
											class="customerObject_${status.index+1}">
												<input type="hidden" value="${CALCULATION.customer.customerId}"
													id="customerId_${status.index+1}${CALCULATION.customer.customerId}${status.count}"/>
										</div>
									</div>
								</td>
								<td style="display: none;">
									<input type="checkbox" id="defaultPrice_${status.index+1}"
										name="defaultPrice" class="defaultPrice width98" /></td>
								<td><input type="text" name="linedescription"
									id="linedescription_${status.index+1}"
									value="${CALCULATION.description}" /></td>
								<td style="width: 0.01%;" class="opn_td"
									id="optionrc_${status.index+1}"><a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_pc"
									id="DeleteCImage_${status.index+1}" style="cursor: pointer;"
									title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <input
									type="hidden" id="productPricingCalcId_${status.index+1}"
									value="${CALCULATION.productPricingCalcId}" />
									<input type="hidden" name="storeId" id="storeid_${status.index+1}"
										value="${CALCULATION.store.storeId}" /> 
									</td>
							</tr>
						</c:forEach>
						<c:forEach var="i" begin="${fn:length(PRICING_CALCULATIONS)+1}"
							end="${fn:length(PRICING_CALCULATIONS)+2}" step="1">
							<tr class="rowidcalc" id="fieldrowcalc_${i}">
								<td style="display: none;" id="lineIdcalc_${i}">${i}</td>
								<td><select name="pricingType" id="pricingType_${i}"
									class="pricingType width70">
										<option value="">Select</option>
										<c:forEach var="TYPE" items="${PRICING_TYPE}">
											<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
										</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="PRODUCT_PRICING_LEVEL_${i}"
										class="btn ui-state-default ui-corner-all product-pricing-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
								<td><select name="calculationType"
									id="calculationType_${i}" class="calculationType width98">
										<option value="">Select</option>
										<c:forEach var="CAL_TYPE" items="${CALCULATION_TYPE}">
											<option value="${CAL_TYPE.key}">${CAL_TYPE.value}</option>
										</c:forEach>
								</select></td>
								<td><select name="calculationSubType"
									id="calculationSubType_${i}" class="calculationSubType width98">
										<option value="">Select</option>
										<c:forEach var="SUBTYPE" items="${CALCULATION_SUB_TYPE}">
											<option value="${SUBTYPE.key}">${SUBTYPE.value}</option>
										</c:forEach>
								</select></td>
								<td><input type="text" id="priceValue_${i}"
									name="priceValue" style="text-align: right;"
									class="priceValue width98 validate[optional,custom[number]]" />
								</td>
								<td id="calculatedPrice_${i}" style="text-align: right;"></td>
								<c:if test="${sessionScope.unified_price eq 'true'}">
									<td>
										<span id="store_${i}"></span> <span class="button float-right">
											<a style="cursor: pointer;" id="storeID_${i}"
											class="btn ui-state-default ui-corner-all show-store-list width100">
												<span class="ui-icon ui-icon-newwin"></span> </a> </span></td>
								</c:if>
								<td> 
									<span class="button float-right">
										<a style="cursor: pointer;" id="customerID_${i}"
										class="btn ui-state-default ui-corner-all show-customer-list width100">
											<span class="ui-icon ui-icon-newwin"></span> </a> </span>	
											<div id="customerInfo_${i}" class="customerInfos"></div>
								</td>
								<td style="display: none;"><input type="checkbox" id="defaultPrice_${i}"
									name="defaultPrice" class="defaultPrice"/></td>
								<td><input type="text" name="linedescription"
									id="linedescription_${i}" value="${bean.description}" /></td>
								<td style="width: 0.01%;" class="opn_td" id="optionrc_${i}">
									<a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_pc"
									id="DeleteCImage_${i}" style="cursor: pointer; display: none;"
									title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <input
									type="hidden" id="productPricingCalcId_${i}" />
									<input type="hidden" name="storeId" id="storeid_${i}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</form>
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div class="portlet-header ui-widget-header float-left sessionPricingSave"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div class="portlet-header ui-widget-header float-left pricecloseDom"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$jquery("#productCalcValidation").validationEngine('attach');
		$('.existingCalculationSubType')
		.each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var tempCalculationSubType = $(
							'#tempCalculationSubType_' + rowId).val(); 
					if (tempCalculationSubType != null
							&& tempCalculationSubType != '' && tempCalculationSubType > 0) { 
						$('#calculationSubType_' + rowId).val(tempCalculationSubType); 
					} else {
						$('#calculationSubType_' + rowId).attr(
								'disabled', true);
					}
				});
		
		$('.existingPriceType').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var tempPricingType = $('#tempPricingType_' + rowId).val();
			$('#pricingType_' + rowId + " option").each(function() {
				if ($(this).val() == tempPricingType) {
					$('#pricingType_' + rowId).val(tempPricingType);
				}
			});
		});

		$('.existingCalculationType').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var tempCalculationType = $('#tempCalculationType_' + rowId).val();
			$('#calculationType_' + rowId).val(tempCalculationType); 
			calculatePriceValue(rowId);
		});
		
		manupulateCalcLastRow(); 
		
	});
	function manupulateCalcLastRow() {
		var hiddenSize1=0;
		var tdSize1 = 0;
		var actualSize1 = 0; 
		hiddenSize1=0;
		
		$($(".tabcalc>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize1;
			}
		}); 
		tdSize1=$($(".tabcalc>tr:first").children()).size();
		actualSize1=Number(tdSize1-hiddenSize1);  
		
		$('.tabcalc>tr:last').removeAttr('id');
		$('.tabcalc>tr:last').removeClass('rowidcalc').addClass('lastrow');
		$($('.tabcalc>tr:last').children()).remove();
		for(var i=0;i<actualSize;i++){
			$('.tabcalc>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
</script>
