<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<option value="">Select</option>
<c:choose>
	<c:when test="${PRODUCT_SUBCATEGORIES ne null && PRODUCT_SUBCATEGORIES ne '' }">
			<c:forEach var="bean" items="${PRODUCT_SUBCATEGORIES}">
				<option value="${bean.key}">${bean.value}</option>
			</c:forEach>
	</c:when> 
</c:choose>
<script type="text/javascript">
$(function(){
	$('#itemCategoryId option').each(function(){
		var categoryval="";
		var catarray=$(this).text().split("_"); 
		if(catarray.length>0){
			for(var i=0;i<catarray.length;i++){
				categoryval=categoryval+" "+catarray[i];
			} 
			$(this).text(categoryval); 
	    } 
	});
});
</script>