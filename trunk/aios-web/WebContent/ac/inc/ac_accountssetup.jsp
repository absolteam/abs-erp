<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/wizard-js/financialyear.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/wizard-js/chartofaccount.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/wizard-js/combination.js"></script>
<style>
.opn_td{
	width:6%;
}
.calendarflag{
	display: none;font-size: small;color: red;
}
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
} 
.ui-dialog{
	z-index: 99999!important;
	overflow-x: hidden!important;
}
#common-popup{overflow-x: hidden!important;}
.ui-autocomplete-input {min-height:27px!important;} 
</style>  
<div id="main-content">
	<div class="tempresult" style="display:none;"></div>
	<div class="tempresultfinal" style="display:none;"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Accounts Setup
		</div>
	<div id="setup_success_message" class="response-msg success ui-corner-all width90" style="display:none;"></div> 
	<div id="setup_error_message" class="response-msg error ui-corner-all width90" style="display:none;"></div> 
	<div class="clearfix"></div> 
	<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all width99" Style="float:left;*float:none;">
		<ul
			class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
			<li
				class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a
				href="#tabs-1">Financial Year</a>
			</li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected"
				id="identitytab"><a href="#tabs-2">Chart Of Accounts</a>
			</li>
			<li class="ui-state-default ui-corner-top ui-tabs-selected"
				id="identitytab"><a href="#tabs-3">Combination</a>
			</li>
		</ul>
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom financial_year_setup"
			id="tabs-1">
			<form name="newFields" id="editCalendarVal" style="position: relative;"> 
			<input type="hidden" name="page" id="page-val" value="${showPage}"/>
			<input type="hidden" name="tempcalendarName" id="tempcalendarName" value="${CALENDAR_DETAILS.name}"/>
			<input type="hidden" name="financial-status" id="financial-status" value="${FINANCIAL_STATUS}"/>
			<div class="portlet-content width100">  
			 	<input type="hidden" name="childCount" id="childCount"  value="${fn:length(PERIOD_DETAILS)}"/> 
			 	<div class="width100" id="hrm">  
					<div class="width48 float-right">
						 <div>
							<label class="width30">From Date<span style="color:red;">*</span></label>  
							<input type="text" tabindex="2" name="startDate" readonly="readonly" value="${CALENDAR_DETAILS.startTime}"
								 id="startPicker" class="width50 validate[required]">
						</div>
						<div>
							<label class="width30">To Date<span style="color:red;">*</span></label>  
							<input id="endPicker" type="text" class="width50 validate[required]" readonly="readonly" name="endDate" 
								tabindex="3" value="${CALENDAR_DETAILS.endTime}">
						</div>
					</div>
					<div class="width50 float-left"> 
							<div>
								<label class="width30">Financial Name<span style="color:red;">*</span></label>  
									<input type="text" tabindex="1" name="calendarName" value="${CALENDAR_DETAILS.name}" id="calendarName"
										 class="width50 validate[required]">
									<input type="hidden" name="editCalHederId" value="${CALENDAR_DETAILS.calendarId}" id="calHeaderId"/> 
									<span class="calendarflag"></span>
							</div>   
							<div>
								<label class="width30">Year Status<span style="color:red;">*</span></label>
								<select name="status" id="status" class="width51 validate[required]"> 
									<option value="1" selected="selected">Active</option>
									<option value="2">Inactive</option>
								</select>
								<input type="hidden" name="tempstatus" id="tempstatus" value="${CALENDAR_DETAILS.status}"/>
							</div> 
					</div> 
				</div>
			</div>	
			<div class="clearfix"></div> 
			<div id="temperror" class="response-msg error ui-corner-all" style="width:80%; display:none"></div>   
			<div class="portlet-content">  
			<div id="hrm" class="hastable width100">  
			<fieldset>
				<legend>Period Details<span
											class="mandatory">*</span></legend> 
			 	<div  class="childCountErr response-msg error ui-corner-all" style="width:80%; display:none;"></div> 
				<div id="wrgNotice" class="response-msg notice ui-corner-all"  style="width:80%; display:none;"></div>
				<input type="hidden" name="trnValue" value="" readonly="readonly" id="transURN"/> 
				<input type="hidden" name="openFlag" id="openFlag"  value="0"/>  
				<table id="hastab" class="width100"> 
					<thead>
						<tr>   
							<th class="width20"><fmt:message key="accounts.calendar.label.periodname"/></th>
							<th class="width5"><fmt:message key="accounts.calendar.label.fromdate"/></th>
							<th class="width5"><fmt:message key="accounts.calendar.label.todate"/></th>
							<th class="width5"><fmt:message key="accounts.calendar.label.adjustingperiod"/></th>
							<th class="width5">Period Status</th>
							<th style="width:2%;"><fmt:message key="accounts.calendar.label.options"/></th>
						</tr>
					</thead>
					<tbody class="tabFinancial">
					 <c:forEach items="${requestScope.PERIOD_DETAILS}" var="result" varStatus="status">                         
				        <tr id="fieldrow_${status.index+1}" class="rowFinancialid">
				            <td class="width20" id="name_${status.index+1}">${result.name}</td>
				            <td class="width5" id="startTime_${status.index+1}">${result.startTime}</td>
				           	<td class="width5" id="endTime_${status.index+1}">${result.endTime}</td>
				            <td class="width5" id="extention_${status.index+1}">${result.extention}</td>
				            <c:choose>
				            	<c:when test="${result.status eq 1}">
				            		<td class="width5" id="periodstatus_${status.index+1}">Open</td>
				            	</c:when>
				            	<c:when test="${result.status eq 2}">
				            		<td class="width5" id="periodstatus_${status.index+1}">Close</td>
				            	</c:when>
				            	<c:when test="${result.status eq 3}">
				            		<td class="width5" id="periodstatus_${status.index+1}">Inactive</td>
				            	</c:when>
				            	<c:otherwise>
				            		<td class="width5" id="periodstatus_${status.index+1}">Temporary Closed</td>
				            	</c:otherwise>
				            </c:choose> 
				            <td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
				            <c:choose>
				            	<c:when test="${result.status ne 2}">
						             <td style="width:2%;"> 
						                		<input type="hidden" name="periodId_${status.index+1}" id="periodId_${status.index+1}"  value="${result.periodId}"/> 
						                		<input type="hidden" name="statusline_${status.index+1}" id="statusline_${status.index+1}"  value="${result.status}"/>  
					                     		<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFinancialData" id="AddFinancialImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
											 	 <span class="ui-icon ui-icon-plus"></span>
											   </a>
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFinancialData" id="EditFinancialImage_${status.index+1}" style="cursor:pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span>
											   </a> 
											   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFinancialrow" id="DeleteFinancialImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
													<span class="ui-icon ui-icon-circle-close"></span>
											   </a>
											    <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"  id="WorkingFinancialImage_${status.index+1}" style="display:none;cursor:pointer;" title="Working">
													<span class="processing"></span>
											   </a>
						                  
									</td> 
							</c:when>
							<c:otherwise>
								<td style="width:2%;"> </td>
							</c:otherwise>
							</c:choose>
				        </tr> 
				    </c:forEach>
				    <c:forEach var="i" begin="${fn:length(PERIOD_DETAILS)+1}" end="${fn:length(PERIOD_DETAILS)+2}" step="1" varStatus ="status">  
						<tr id="fieldrow_${i}" class="rowFinancialid"> 
							<td class="width20" id="name_${i}"></td>
				            <td class="width5" id="startTime_${i}"></td>
				           	<td class="width5" id="endTime_${i}"></td>
				            <td class="width5" id="extention_${i}"></td>
				            <td class="width5" id="periodstatus_${i}"></td>
				            <td style="display:none;" id="lineId_${i}">${i}</td> 
					 		<td  style="width:2%;" id="option_${i}">
					 			<input type="hidden" name="periodId_${i}" id="periodId_${i}"  value="0"/>   
					 			<input type="hidden" name="statusline" id="statusline_${i}"/>  
							  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFinancialData" id="AddFinancialImage_${i}"  title="Add Record">
							 	 <span class="ui-icon ui-icon-plus"></span>
							   </a>	
							   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFinancialData"  id="EditFinancialImage_${i}" style="display:none; cursor: pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span>
								</a> 
								<a style="cursor: pointer; display: none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFinancialrow"  id="DeleteFinancialImage_${i}" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
								</a>
								<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingFinancialImage_${i}" style="display:none;" title="Working">
									<span class="processing"></span>
								</a>
							</td> 
							
						</tr>
				 	</c:forEach>
				 </tbody>
			   </table>
		   </fieldset>
		  </div>  
		 </div>  
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right cancel_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>
		<div class="portlet-header ui-widget-header float-right delete_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.delete"/></div> 
		<div class="portlet-header ui-widget-header float-right save_calendar" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>
		<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
				<div class="portlet-header ui-widget-header float-left addFinancialrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
		</div> 
		<input type="hidden" name="availflag" id="availflag" value="1"/>
		</form>
		</div>
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-2">
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;">
				<input type="hidden" name="accountId" id="accountId" value="${bean.accountId}"/>  
				<div class="width60" id="hrm"> 
					<div><label class="width30"><fmt:message key="accounts.coa.label.segmentname"/><span style="color: red;">*</span></label>
						<select class="width40 validate[required]" name="segmentId" id="segmentId">
							<option value="">Select</option> 
							<c:forEach items="${SEGMENT_LIST}" var="segment" >
							<option value="${segment.segmentId}" style="font-weight: bold;">${segment.segmentName}</option>
							<c:if test="${segment.segmentVOs ne null && fn:length(segment.segmentVOs)>0}">
								<c:forEach items="${segment.segmentVOs}" var="child" >
									<option value="${child.segmentId}" style="margin-left: 8px;">${child.segmentName}</option>
								</c:forEach>
							</c:if>
							</c:forEach>
						</select>  
					</div> 
				</div> 
				<div class="clearfix"></div>  
				<div class="portlet-content class98" id="hrm"> 
				<fieldset>
						<legend><fmt:message key="accounts.coa.label.accountdetails"/><span style="color: red;">*</span></legend>
						<div id="hrm" class="hastable width100">
							<div id="lineerror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
							<div id="warning_message" class="response-msg notice ui-corner-all"  style="width:90%; display:none;"></div>
							<input type="hidden" name="childCount" id="childCount"/>  
							<table id="hastab" class="width100">	
								<thead>
									<tr> 
										<th><fmt:message key="accounts.coa.label.accountcode"/></th> 
										<th><fmt:message key="accounts.coa.label.description"/></th> 
										<th><fmt:message key="accounts.coa.label.accounttype"/></th> 
										<th style="width:10%;"><fmt:message key="accounts.common.label.options"/></th>
									</tr>
								</thead>
								<tbody class="tabCOA"> 
									<c:set var="i" value="1"/> 
									<c:forEach var="i" begin="1" end="2" step="1" varStatus ="status"> 
										<tr id="fieldrow_${i}" class="rowidCOA"> 	
											<td id="code_${i}"></td>
											<td id="description_${i}"></td>
											<td id="accounttype_${i}"></td> 
											<td style="display:none;" id="accounttypeid_${i}"></td>
											<td style="display:none;" id="accountid_${i}"></td>  
											<td style="display:none;" id="lineCOAId_${i}">${i}</td>
											<td id="option_${i}">
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addCOAData" id="AddCOAImage_${i}" style="cursor:pointer;" title="Add Record">
						 								<span class="ui-icon ui-icon-plus"></span>
												  </a>	
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editCOAData" id="EditCOAImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delCOArow" id="DeleteCOAImage_${i}" style="display:none; cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												  </a>
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingCOAImage_${i}" style="display:none;" title="Working">
														<span class="processing"></span>
												  </a>
											</td>  
										</tr>
									</c:forEach> 	 									 
						 		</tbody>
							</table>
						</div>
					</fieldset>	
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="${requestScope.showPage}"><fmt:message key="accounts.common.button.cancel"/></div> 
						<div class="portlet-header ui-widget-header float-right save_chartofaccounts" id="${requestScope.showPage}"><fmt:message key="accounts.common.button.save"/></div>
					</div> 
					<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
						<div class="portlet-header ui-widget-header float-left addCOArows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
					</div> 
				</div>
			</form>
		</div>
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
			id="tabs-3">
			 
			<div class="portlet-content">  
			 <div id="temp-result" style="display: none;"></div> 
			  <div class="width100 float-left">  
			  		<div class="float-right  width30">  
			  			 
			  		</div>
					<div class="float-left  width50">   
						<div class="portlet-content width48 float-left">
				 			<div id="temp-result" style="display: none;"></div>
							<div id="success_message" class="response-msg success ui-corner-all" 
								style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
							<div id="error_message" class="response-msg error ui-corner-all" 
								style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>  
							<div id="combination_treeview"></div> 
							<input type="hidden" id="copyCombinationId"/>
							<input type="hidden" id="copySegmentId"/>
							<div style="display: none;"
							class="portlet-header ui-widget-header float-right" id="add">
							add</div>
							<div style="display: none;"
								class="portlet-header ui-widget-header float-right" id="copy">
								copy</div>
							<div style="display: none;"
								class="portlet-header ui-widget-header float-right" id="paste">
								paste</div>
							<div style="display: none;"
								class="portlet-header ui-widget-header float-right" id="delete">
								delete</div>
							<div style="display: none;"
								class="portlet-header ui-widget-header float-right"
								id="account-popup-close">close</div>
							<div class="clearfix"></div> 
						</div> 
					</div> 
				</div>  
			</div> 
			<div style="display: none;" class="portlet-header ui-widget-header float-right" id="create">
				create
			</div> 
			<div style="display: none;"
			class="portlet-header ui-widget-header float-right"
			id="account-popup-close">close</div>
			<div class="clearfix"></div>  
			<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a  class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
				<div class="popup-result width100"></div>
				<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
			</div>  
		  
		</div>
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
		<div class="portlet-header ui-widget-header float-left skip_accounts_setup" style="cursor:pointer;">skip</div> 
	</div>
</div>
</div>