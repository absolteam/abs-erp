<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	var customerUpdateId = 0;
	$(function() {
		oTable = $('#CustomerCommonPopup')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_customer_poscommon_jsonlist.action?pageInfo="
									+ $('#pageInfo').val(),

							"aoColumns" : [ {
								"mDataProp" : "customerNumber"
							}, {
								"mDataProp" : "customerName"
							}, {
								"mDataProp" : "mobileNumber"
							}, {
								"mDataProp" : "residentialAddress"
							} ]
						});
		
		/* Click event handler */
		$('#CustomerCommonPopup tbody tr').live('click', function () {  
	 		  if ( $(this).hasClass('row_selected') ) {
				  $(this).addClass('row_selected');
				  aData = oTable.fnGetData(this);
				  customerUpdateId = aData.customerId;
		      }
		      else {
		          oTable.$('tr.row_selected').removeClass('row_selected');
		          $(this).addClass('row_selected');
		          aData = oTable.fnGetData(this);
		          customerUpdateId = aData.customerId;
		      }
	  	});

		/* Click event handler */
		$('#CustomerCommonPopup tbody tr').live(
				'dblclick',
				function() {
 					aData = oTable.fnGetData(this);
					commonCustomerPopup(aData.customerId, aData.customerName,
							aData.combinationId, aData.accountCode,
							aData.creditTermId, null);
					$('#customer-common-popup').trigger('click');
					return false;
				}); 
		
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>customer
		</div>
		<div class="portlet-content">
			<div id="customer_common_json_list">
				<table id="CustomerCommonPopup" class="display">
					<thead>
						<tr>
							<th>Ref.NO</th>
							<th>Name</th>
							<th>Telephone</th>
							<th>Address</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="customer-common-popup">close</div>
				<div class="portlet-header ui-widget-header float-right"
					id="customer-common-edit-popup">edit</div> 
				<div class="portlet-header ui-widget-header float-right"
					id="customer-common-create-popup">add</div>  
			</div>
			<input type="hidden" id="pageInfo" value="${requestScope.pageInfo}" /> 
		</div>
	</div>
</div>