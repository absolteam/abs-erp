<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var actionname="";
var assetDetails="";
var accessCode = "";
var sectionRowId = 0;
var assetClassType ="";
$(function(){  

	manupulateLastRow();
	$jquery("#asset_creations").validationEngine('attach');  

	$("#commenceDate").datepick();
	
	$('.costType').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	});

	$('.amount').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	}); 

	$('#ownerType').live('change',function(){
		 $('#ownerName').val('');
		 $('#ownerId').val('');
	});

	$('.assetsubclass-lookup').click(function(){
		if(Number($('#assetClass').val())>0){
			 $('.ui-dialog-titlebar').remove();  
			 if(Number($('#assetClass').val()) == 9)
		        	accessCode = "SUB_CLASS_TANAGIBLE";
		        else
		        	accessCode = "SUB_CLASS_INTANAGIBLE"; 
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.common-result').html(result);
		                  $('#common-popup').dialog('open');
		  				   $($($('#common-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.common-result').html(result);
		             }
		         });
		} 
        return false;
 	});	 

	$('.assettype-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id"); 
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.common-result').html(result);
		                  $('#common-popup').dialog('open');
		  				   $($($('#common-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.common-result').html(result);
		             }
		         });
        return false;
 	});	

	$('.assettype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
		   return false;
		});	

		$('.assetcategory-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
		  return false;
		});	

		$('.paymentAssetDetail').live('click',function(){
 			 $('.ui-dialog-titlebar').remove(); 
			 var rowId = getRowId($(this).attr("id")); 
		     $.ajax({
		            type:"POST",
		            url:"<%=request.getContextPath()%>/show_common_directpaymentdetails.action",
		            data:{id: rowId},
		            async: false,
		            dataType: "html",
		            cache: false,
		            success:function(result){ 
		                 $('.common-result').html(result);
		                 $('#common-popup').dialog('open');
		 				 $($($('#common-popup').parent()).get(0)).css('top',0);
		                 return false;
		            },
		            error:function(result){
		                 $('.common-result').html(result);
		            }
		        });
			 return false;
		});

		$('#direct_payment_popupclose').live('click',function(){
			$('#common-popup').dialog('close');
			return false;
		});
		
		$('.assetcost-lookup').live('click',function(){
				 $('.ui-dialog-titlebar').remove(); 
		 var str = $(this).attr("id");
	    	 accessCode = str.substring(0, str.lastIndexOf("_"));
	    	 sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
		 return false;
		});	
	
	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SUB_CLASS_TANAGIBLE" || accessCode=="SUB_CLASS_INTANAGIBLE"){
			$('#assetSubClass').html("");
			$('#assetSubClass').append("<option value=''>Select</option>");
			loadLookupList("assetSubClass"); 
		} else if(accessCode=="ASSET_TYPE"){
			$('#assetType').html("");
			$('#assetType').append("<option value=''>Select</option>");
			loadLookupList("assetType"); 
		}  else if(accessCode=="ASSET_CATEGORY"){
			$('#category').html("");
			$('#category').append("<option value=''>Select</option>");
			loadLookupList("category"); 
		} else if(accessCode=="ASSET_COST_TYPE"){
			$('#costType_'+sectionRowId).html("");
			$('#costType_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("costType_"+sectionRowId); 
		}  
	});

	$('#assetClass').change(function(){
		 var assetClass = $(this).val();
		 
		 $('#assetSubClass').html('');
			$('#assetSubClass')
			.append(
					'<option value="">Select</option>');
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_asset_subclass.action", 
			 	async: false,
			 	data:{assetClass: assetClass},
			    dataType: "json",
			    cache: false,
				success:function(response){ 
					$(response.aaData)
					.each(
							function(index) {
						$('#assetSubClass')
							.append('<option value='
									+ response.aaData[index].lookupDetailId
									+ '>' 
									+ response.aaData[index].displayName
									+ '</option>');
					}); 
				}
			});
	});

	$('.addrows').click(function(){ 
		  var i = Number(1); 
		  var id = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id = id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/asset_creation_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	$('.delrow').live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);  
      	$(slidetab).remove();  
      	var i=1;
   	 	$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });   
  		 calculateTotalAmount();
	 });

	 $('#asset_discard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_asset_list.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('#common-popup').dialog('destroy');		
					 $('#common-popup').remove(); 
					$("#main-wrapper").html(result);  
				}
		 });
	 });

	 $('#asset_save').click(function(){   
		if($jquery("#asset_creations").validationEngine('validate')){
 			var assetCreationId = Number($('#assetCreationId').val());   
			var assetNumber = $('#assetNumber').val();
			var assetClass = Number($('#assetClass').val());
			var assetSubClass = Number($('#assetSubClass').val());
			var assetType = Number($('#assetType').val()); 
	 		var manufacturer = $('#manufacturer').val();
	 		var brand = $('#brand').val();
	 		var assetName= $('#assetName').val();
	 		var assetMake = $('#assetMake').val();
	 		var assetModel = $('#assetModel').val();
	 		var serialNumber = $('#serialNumber').val();
	 		var assetColor = Number($('#assetColor').val()); 
	 		var assetCondition = Number($('#assetCondition').val()); 
	 		var category = Number($('#category').val()); 
			var description = $("#description").val(); 
 			var productId = Number($('#productId').val());
			var yearMake = $("#yearMake").val(); 
			var assetVin = $("#assetVin").val();  
			var plateNumber = $("#plateNumber").val();  
			var tcNumber = $("#tcNumber").val();  
			var plateColor = Number($("#plateColor").val());  
			var plateType = Number($("#plateType").val());
			var ownerType = $("#ownerType").val();
			var ownerId = Number($("#ownerId").val());
			var commenceDate = $("#commenceDate").val();
			var depreciationMethod = Number($("#depreciationMethod").val());
			var decliningVariance = Number($('#decliningVariance').val());
			var depreciationConvention = Number($("#convention").val());
			var scrapValue = Number($("#scrapValue").val()); 
			var usefulLife = Number($("#usefulLife").val()); 
			assetDetails = getAssetDetails(); 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_asset_creation.action", 
				 	async: false,
				 	data:{
					 		assetCreationId: assetCreationId, assetNumber: assetNumber, assetClass: assetClass, assetSubClass: assetSubClass, usefulLife: usefulLife,
				 			assetType: assetType, manufacturer: manufacturer, brand: brand, assetMake: assetMake, assetVin: assetVin, ownerType: ownerType,
				 			assetModel: assetModel, serialNumber: serialNumber, assetColor: assetColor, assetCondition: assetCondition, ownerId: ownerId, category: category, 
				 			productId: productId, description:description, yearMake: yearMake, assetName: assetName, decliningVariance: decliningVariance,
				 			commenceDate: commenceDate, depreciationMethod: depreciationMethod, depreciationConvention: depreciationConvention, scrapValue: scrapValue,
				 			plateNumber: plateNumber, tcNumber: tcNumber, plateColor: plateColor, plateType: plateType, assetDetails: assetDetails
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){  
						 if(response.returnMessage == "SUCCESS"){
							 var message = "Record created.";
							 if(assetCreationId > 0)
								 message = "Record updated.";
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/show_asset_list.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove(); 
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html(message).slideDown(1000);
										$('#success_message').delay(2000).slideUp();
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error!!!").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
			 }); 
		 }else{return false;}
		 return false;
	 });

	 var getAssetDetails = function(){
		assetDetails = "";
		var costTypeArray = new Array();
		var paymentDetailArray = new Array();
		var descriptionArray = new Array(); 
		var assetDetailIdArray =new Array(); 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var costType = $('#costType_'+rowId).val();
 			var linedescription = $('#linedescription_'+rowId).val();
			var assetDetailId = Number($('#assetDetailId_'+rowId).val());  
		 	var paymentDetailId =  Number($('#paymentDetailId_'+rowId).val()); 
			if(typeof costType != 'undefined' && costType!=null && costType!="" && paymentDetailId>0){
				costTypeArray.push(costType);
				paymentDetailArray.push(paymentDetailId); 
				if(linedescription!=null && linedescription!="")
					descriptionArray.push(linedescription);
				else
					descriptionArray.push("##");
				assetDetailIdArray.push(assetDetailId);
			} 
		});
		for(var j=0;j<costTypeArray.length;j++){ 
			assetDetails += costTypeArray[j]+"__"+paymentDetailArray[j]+"__"+descriptionArray[j]+"__"+assetDetailIdArray[j];
			if(j==costTypeArray.length-1){   
			} 
			else{
				assetDetails += "#@";
			}
		} 
		return assetDetails;
	 };

	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	 $('.common_person_company_list').click(function(){  
		 $('.ui-dialog-titlebar').remove(); 
		 var ownerType =  $('#ownerType').val();  
		 if(ownerType == 'P')
			actionName = "common_person_list";
		 else
			actionName = "common_company_list";
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+actionName+".action",
				async : false,
				data: {personTypes: "ALL"},
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup').dialog('open');
		  			$($($('#common-popup').parent()).get(0)).css('top',0);
					$('.common-result').html(result);
				},
				error : function(result) {
					$('.common-result').html(result);
				}
			});
			return false;
		});  

	 $('#product-stock-popup').live('click',function(){ 
		 $('#common-popup').dialog('close');
	 });
	 
	 $('.show_asset_product_detail').click(function(){  
		$('.ui-dialog-titlebar').remove();  
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/product_list_assetusage.action",
			async : false,
			dataType : "html",
			data : {itemType : "A"},
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('open');
	  			$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);
			},
			error : function(result) {
				$('.common-result').html(result);
			}
		 });
		return false;
	  });

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			top : 0,
			modal : true
		});

		if (Number($('#assetCreationId').val()) > 0) {
			$('#assetClass').val($('#tempAssetClass').val());
			$('#assetSubClass').val($('#tempAssetSubClass').val());
			$('#assetType').val($('#tempAssetType').val());
			$('#ownerType').val($('#tempOwnerType').val());
			$('#assetColor').val($('#tempAssetColor').val());
			$('#assetCondition').val($('#tempAssetCondition').val());
			$('#category').val($('#tempCategory').val());
			$('#plateColor').val($('#tempPlateColor').val());
			$('#plateType').val($('#tempPlateType').val());
			$('#convention').val($('#tempconvention').val());
			$('#depreciationMethod').val($('#tempdepreciationMethod').val());
			$('.rowid').each(function() {
				var rowId = getRowId($(this).attr('id'));
				$('#costType_' + rowId).val($('#tempcostType_' + rowId).val());
			});
		}

	});
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function triggerAddRow(rowId) {
		var costType = $('#costType_' + rowId).val();
		var amount = $('#amount_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (costType != null && costType != "" && amount != null
				&& amount != "" && $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
		calculateTotalAmount();
	}
	function calculateTotalAmount() {
		var totalCost = 0;
		$('#totalAmount').text('');
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var amount = Number($('#amount_' + rowId).val().trim());
			totalCost = Number(totalCost + amount);
		});
		$('#totalAmount').text(Number(totalCost).toFixed(2));
	}
	function personPopupResult(personid, personname, commonParam) {
		$('#ownerId').val(personid);
		$('#ownerName').val(personname);
		$('#common-popup').dialog("close");
	}
	function companyPopupResult(companyId, companyName, commonParam) {
		$('#ownerId').val(companyId);
		$('#ownerName').val(companyName);
		$('#common-popup').dialog("close");
	}
	
	function productPopupResult(productId, productName){ 
 		$('#productId').val(productId);
		$('#assetName').val(productName);
		$('#common-popup').dialog("close");
	}
	
	function commonDirectPaymentDetail(paymentDetailId, invoiceNumber, amount, currentRowId){
		$('#paymentDetail_'+currentRowId).val(invoiceNumber);
		$('#paymentDetailId_'+currentRowId).val(paymentDetailId);
		$('#amount_'+currentRowId).val(amount);
		triggerAddRow(currentRowId);
		$('#common-popup').dialog("close");
	}
	
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			creation
		</div> 
		<form name="asset_creations" id="asset_creations" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetCreationId" name="assetCreationId"
					value="${ASSET_CREATION.assetCreationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width40 float-left">
						<fieldset style="min-height: 235px;">
							<div>
								<label class="width30" for="assetNumber">Asset Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CREATION.assetCreationId ne null && ASSET_CREATION.assetCreationId ne ''}">
										<input type="text" readonly="readonly" name="assetNumber"
											id="assetNumber" class="width50 validate[required]"
											value="${ASSET_CREATION.assetNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="assetNumber"
											id="assetNumber" class="width50 validate[required]"
											value="${assetNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset Name<span
									class="mandatory">*</span> </label> <input type="text"
									id="assetName"
									value="${ASSET_CREATION.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="storeId"/> <input type="hidden"
									id="productId" value="${ASSET_CREATION.product.productId}"/><span
									class="button"> <a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all show_asset_product_detail width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetClass">Asset Class<span
									class="mandatory">*</span> </label> <select name="assetClass"
									id="assetClass" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="assetClass" items="${ASSET_CLASS}">
										<option value="${assetClass.key}">${assetClass.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetClass"
									value="${ASSET_CREATION.assetClass}" />
							</div>
							<div>
								<label class="width30" for="assetSubClass">Sub Class<span
									class="mandatory">*</span> </label> <select name="assetSubClass"
									id="assetSubClass" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="assetSubClass" items="${ASSET_SUB_CLASS}">
										<option value="${assetSubClass.lookupDetailId}">${assetSubClass.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetSubClass"
									value="${ASSET_CREATION.lookupDetailByAssetSubclass.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
									style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all assetsubclass-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetType">Asset Type<span
									class="mandatory">*</span> </label> <select name="assetType"
									id="assetType" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetType" items="${ASSET_TYPE}">
										<option value="${assetType.lookupDetailId}">${assetType.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetType"
									value="${ASSET_CREATION.lookupDetailByAssetType.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_TYPE"
									class="btn ui-state-default ui-corner-all assettype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetCondition">Asset
									Condition</label> <select name="assetCondition" id="assetCondition"
									class="width51">
									<option value="">Select</option>
									<c:forEach var="assetCondition" items="${ASSET_CONDITION}">
										<option value="${assetCondition.key}">${assetCondition.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempAssetCondition"
									value="${ASSET_CREATION.assetCondition}">
							</div>
							<div>
								<label class="width30" for="ownerType">Owner Type</label> <select
									name="ownerType" id="ownerType" class="width51">
									<option value="">Select</option>
									<option value="P">Person</option>
									<option value="C">Company</option>
								</select> <input type="hidden" value="${ASSET_CREATION.ownerType}"
									id="tempOwnerType" />
							</div>
							<div>
								<label class="width30" for="ownerName">Owner</label> <input
									type="text" readonly="readonly" id="ownerName"
									value="${ASSET_CREATION.ownerName}" class="width50" /> <input
									type="hidden" readonly="readonly" id="ownerId"
									value="${ASSET_CREATION.ownerId}" /> <span class="button">
									<a style="cursor: pointer;" 
									class="btn ui-state-default ui-corner-all common_person_company_list width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>

					<div class="width30 float-left">
						<fieldset style="min-height: 235px;">
							<div>
								<label class="width30" for="brand">Brand</label> <input
									type="text" name="brand" id="brand" class="width50"
									value="${ASSET_CREATION.brand}" />
							</div>
							<div>
								<label class="width30" for="manufacturer">Manufacturer</label> <input
									type="text" name="manufacturer" id="manufacturer"
									class="width50" value="${ASSET_CREATION.manufacturer}" />
							</div>
							<div>
								<label class="width30" for="assetMake">Asset Make</label> <input
									type="text" name="assetMake" id="assetMake"
									value="${ASSET_CREATION.assetMake}" class="width50" />
							</div>
							<div>
								<label class="width30" for="assetModel">Asset Model</label> <input
									type="text" name="assetModel"
									value="${ASSET_CREATION.assetModel}" id="assetModel"
									class="width50">
							</div>
							<div>
								<label class="width30" for="serialNumber">Serial No.</label> <input
									type="text" name="serialNumber" id="serialNumber"
									value="${ASSET_CREATION.serialNumber}" class="width50" />
							</div>
							<div style="display: none;">
								<label class="width30" for="assetColor">Asset Color</label> <select
									name="assetColor" id="assetColor" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetColor" items="${ASSET_COLOR}">
										<option value="${assetColor.key}">${assetColor.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempAssetColor"
									value="${ASSET_CREATION.assetColor}">
							</div>
							<div>
								<label class="width30" for="category">Category</label> <select
									name="category" id="category" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetCategory" items="${ASSET_CATEGORY}">
										<option value="${assetCategory.lookupDetailId}">${assetCategory.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCategory"
									value="${ASSET_CREATION.lookupDetailByCategory.lookupDetailId}">
								<span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_CATEGORY"
									class="btn ui-state-default ui-corner-all assetcategory-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_CREATION.description}</textarea>
							</div>
							<div id="asset_vehicle" style="display: none">
								<div>
									<label class="width30" for="yearMake">Make Year</label> <input
										type="text" name="yearMake" id="yearMake"
										value="${ASSET_CREATION.yearMake}" class="width50" />
								</div>
								<div>
									<label class="width30" for="assetVin">VIN</label> <input
										type="text" name="assetVin" id="assetVin"
										value="${ASSET_CREATION.assetVin}" class="width50" />
								</div>
								<div>
									<label class="width30" for="plateNumber">Plate No</label> <input
										type="text" name="plateNumber" id="plateNumber"
										value="${ASSET_CREATION.plateNumber}" class="width50" />
								</div>
								<div>
									<label class="width30" for="tcNumber">TC No</label> <input
										type="text" name="tcNumber" id="tcNumber"
										value="${ASSET_CREATION.tcNumber}" class="width50" />
								</div>
								<div>
									<label class="width30" for="plateColor">Plate Color</label> <select
										name="plateColor" id="plateColor" class="width50">
										<option value="">Select</option>
										<c:forEach var="assetColor" items="${ASSET_COLOR}">
											<option value="${assetColor.key}">${assetColor.value}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempPlateColor"
										value="${ASSET_CREATION.plateColor}">
								</div>
								<div>
									<label class="width30" for="plateType">Plate Type</label> <select
										name="plateType" id="plateType" class="width51">
										<option value="">Select</option>
										<c:forEach var="plateType" items="${PLATE_TYPE}">
											<option value="${plateType.key}">${plateType.value}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempPlateType"
										value="${ASSET_CREATION.plateType}">
								</div>
							</div>
						</fieldset>
					</div>
					<div class="width30 float-left">
						<fieldset style="min-height: 235px;"> 
							<div>
								<label class="width30" for="depreciationMethod">Dep.Method<span
									class="mandatory">*</span> </label> <select name="depreciationMethod"
									id="depreciationMethod" class="validate[required] width51">
									<option value="">Select</option>
									<c:forEach var="depreciationMethod"
										items="${DEPRECIATION_METHOD}">
										<option value="${depreciationMethod.key}">${depreciationMethod.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly"
									id="tempdepreciationMethod"
									value="${ASSET_CREATION.depreciationMethod}" />
							</div>
							<div>
								<label class="width30" for="decliningVariance">Variance</label>
								<input type="text" name="decliningVariance"
									id="decliningVariance"
									value="${ASSET_CREATION.decliningVariance}" class="width50 validate[optional,custom[number]]" />
							</div>
							<div>
								<label class="width30" for="commenceDate">Commence<span
									class="mandatory">*</span> </label> <input type="text"
									id="commenceDate" class="validate[required] width50"
									readonly="readonly" value="${ASSET_CREATION.depreciationDate}" />
							</div>
							<div>
								<label class="width30" for="Convention">Convention<span
									class="mandatory">*</span> </label> <select name="convention"
									id="convention" class="validate[required] width51">
									<option value="">Select</option>
									<c:forEach var="convention" items="${CONVENTION}">
										<option value="${convention.key}">${convention.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempconvention"
									value="${ASSET_CREATION.convention}" />
							</div>
							<div>
								<label class="width30" for="usefulLife">Useful Life<span
									class="mandatory">*</span> </label> <input type="text"
									name="usefulLife" id="usefulLife"
									value="${ASSET_CREATION.usefulLife}"
									class="width50 validate[required,custom[number]]" />
							</div> 
							<div>
								<label class="width30" for="scrapValue">Scrap Value</label> <input
									type="text" name="scrapValue" id="scrapValue"
									value="${ASSET_CREATION.scrapValue}"
									class="width50 validate[optional,custom[number]]" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Asset Detail</legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 1%">Line No</th>
									<th style="width: 5%">Cost Type</th>
									<th style="width: 5%">Invoice</th>
									<th style="width: 5%">Amount</th>
									<th style="width: 10%">Description</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail" items="${ASSET_CREATION.assetDetails}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}">${status.index+1}</td>
										<td><select name="costType" class="width70 costType"
											id="costType_${status.index+1}">
												<option value="">Select</option>
												<c:forEach var="costType" items="${COST_TYPE}">
													<option value="${costType.lookupDetailId}">${costType.displayName}</option>
												</c:forEach>
										</select> <input type="hidden" id="tempcostType_${status.index+1}"
											value="${detail.lookupDetail.lookupDetailId}" /> <span
											class="button" style="position: relative;"> <a
												style="cursor: pointer;"
												id="ASSET_COST_TYPE_${status.index+1}"
												class="btn ui-state-default ui-corner-all assetcost-lookup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
										<td>
											<input type="text"
												class="width70" readonly="readonly"
												id="paymentDetail_${status.index+1}" value="${detail.directPaymentDetail.invoiceNumber}" />
											<input type="hidden" readonly="readonly"
												id="paymentDetailId_${status.index+1}" value="${detail.directPaymentDetail.directPaymentDetailId}"/>
											<span class="button" style="position: relative;"> <a
													style="cursor: pointer;" id="paymentAssetDetail_${status.index+1}"
													class="btn ui-state-default ui-corner-all paymentAssetDetail width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a>
											 </span>
										</td>
										<td><input type="text" readonly="readonly"
											class="width96 amount"
											id="amount_${status.index+1}" value="${detail.directPaymentDetail.amount}"
											style="text-align: right;" /></td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status.index+1}"
											value="${detail.description}" /> <input type="hidden"
											id="assetDetailId_${status.index+1}"
											value="${detail.assetDetailId}" /></td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a>
										</td>
									</tr>
								</c:forEach>
								<c:forEach var="i"
									begin="${fn:length(ASSET_CREATION.assetDetails)+1}"
									end="${fn:length(ASSET_CREATION.assetDetails)+2}" step="1"
									varStatus="status1">
									<tr class="rowid" id="fieldrow_${i}">
										<td id="lineId_${i}">${i}</td>
										<td><select name="costType" class="width70 costType"
											id="costType_${i}">
												<option value="">Select</option>
												<c:forEach var="costType" items="${COST_TYPE}">
													<option value="${costType.lookupDetailId}">${costType.displayName}</option>
												</c:forEach>
										</select> <input type="hidden" id="tempcostType_${i}" /> <span
											class="button" style="position: relative;"> <a
												style="cursor: pointer;" id="ASSET_COST_TYPE_${i}"
												class="btn ui-state-default ui-corner-all assetcost-lookup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
										<td><input type="text" readonly="readonly"
											class="width70 paymentDetail"
											id="paymentDetail_${i}" />
											<input type="hidden"
												id="paymentDetailId_${i}" />
											<span class="button" style="position: relative;"> <a
													style="cursor: pointer;" id="paymentAssetDetail_${i}"
													class="btn ui-state-default ui-corner-all paymentAssetDetail width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a>
											 </span>
										</td>
										<td><input type="text" readonly="readonly"
											class="width96 amount"
											id="amount_${i}" style="text-align: right;" />
										</td>
										<td><input type="text" name="linedescription"
											id="linedescription_${i}" /> <input type="hidden"
											id="assetDetailId_${i}" /></td>
										<td style="width: 0.01%;" class="opn_td" id="option_${i}">
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${i}" style="display: none; cursor: pointer;"
											title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${i}" style="display: none; cursor: pointer;"
											title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${i}" style="display: none; cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${i}" style="display: none;" title="Working">
												<span class="processing"></span> </a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="width30 float-right" style="font-weight: bold;">
				<span>Total: </span> <span id="totalAmount"></span>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="asset_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="asset_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>