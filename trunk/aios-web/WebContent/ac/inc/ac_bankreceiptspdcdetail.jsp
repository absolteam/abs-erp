<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
var bankReceiptsDetail = "";
$(function(){
	$('.paymentStatus').each(function(){
		var rowId = getRowId($(this).attr('id')); 
		var removeArrb = "PDC"; 
		$('#paymentStatus_'+rowId+' option').each(function(){
			var thisval = $(this).val();
			var thisarray = thisval.split('@@'); 
			if(thisarray[1] == removeArrb){
				$('#paymentStatus_'+rowId+' option[value='+$(this).val()+']').remove(); 
				return false; 
			}
		});  
	});	
	$('#receipts_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receipts_voucher.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});

	$('#receipts_save').click(function(){
    	bankReceiptsDetail = ""; 
		var bankReceiptsId = Number($('#bankReceiptsId').val());  
		bankReceiptsDetail = getBankReceiptsDetails();  
 		if(bankReceiptsDetail!=null && bankReceiptsDetail!=""){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_bankreceipts_pdcinfo.action", 
			 	async: false, 
			 	data:{bankReceiptsId: bankReceiptsId, bankReceiptLines: bankReceiptsDetail, alertId: alertId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $(".tempresult").html(result);
					 var message=$('.tempresult').html();  
					 if(message.trim()=="SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_receipts_voucher.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
				  					$('#common-popup').remove();  
				  					$('#codecombination-popup').dialog('destroy');		
				  					$('#codecombination-popup').remove(); 
									$("#main-wrapper").html(result); 
									$('#success_message').hide().html("Record updated.").slideDown(1000);
									$('#success_message').delay(3000).slideUp();
								}
						 });
					 } 
					 else{
						 $('#page-error').hide().html(message).slideDown(1000);
						 $('#page-error').delay(3000).slideUp();
						 return false;
					 }
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error..").slideDown(1000);
					$('#page-error').delay(3000).slideUp();
				}
			}); 
		}else{
			$('#page-error').hide().html("Please enter receipts detail info.").slideDown(1000);
			$('#page-error').delay(3000).slideUp();
			return false;
		} 
 	});

     var getBankReceiptsDetails = function(){  
 		var statusArr = new Array();
   		var bankReceiptsLines = new Array();
 		var receiptsDetail = "";
 		var descarr = new Array();
 		$('.rowid').each(function(){ 
 			 var rowId = getRowId($(this).attr('id'));   
 			 var statusval = $('#paymentStatus_'+rowId).val();   
 			 if(statusval!=null && statusval!=''){
				var lineDescription=$('#linedescription_'+rowId).val(); 
				var statusArray = statusval.split('@@');
	 			statusval = statusArray[0];
  				if(lineDescription!=null && lineDescription!="")
 					 descarr.push(lineDescription);
 				else
 					 descarr.push("##"); 
 				bankReceiptsLines.push(bankReceiptsLineId); 
 				statusArr.push(statusval); 
  	 		 } 
 		});
  		for(var j=0;j<statusArr.length;j++){ 
 			receiptsDetail += statusArr[j]+"__"+descarr[j]+"__"+bankReceiptsLines[j];
 			if(j==modearr.length-1){   
 			} 
 			else{
 				receiptsDetail += "@@";
 			}
 		}  
 		return receiptsDetail;
 	}; 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>bank receipts information</div>
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>	
		  <form name="bankReceiptsForm" id="bankReceiptsForm"> 
			<div class="portlet-content">  
 			 <div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div> 
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right">  
						<input type="hidden" name="alertId" id="alertId" value="${alertId}"/>  
						<fieldset>  
							<div>
								<label class="width30" for="receiptsSource">Receipts Source</label> 
								<span class="width50">${BANK_RECEIPTS.lookupDetail.displayName}</span> 
							</div> 
							<div>
								<label class="width30">Receiver</label> 
								<c:choose>
									<c:when test="${BANK_RECEIPTS.supplier.person ne null && BANK_RECEIPTS.supplier.person ne '' }"> 
											<span class="width50">${BANK_RECEIPTS.supplier.person.firstName} ${ BANK_RECEIPTS.supplier.person.lastName}" </span> 
									</c:when>
									<c:when test="${BANK_RECEIPTS.supplier.company ne null && BANK_RECEIPTS.supplier.company ne '' }">
										<span class="width50">${BANK_RECEIPTS.supplier.company.companyName}</span>  
									</c:when>
									<c:when test="${BANK_RECEIPTS.supplier.cmpDeptLocation ne null && BANK_RECEIPTS.supplier.cmpDeptLocation ne '' }">
										<span class="width50">${BANK_RECEIPTS.supplier.cmpDeptLocation.company.companyName}</span>  
									</c:when> 
									<c:when test="${BANK_RECEIPTS.customer.personByPersonId ne null && BANK_RECEIPTS.customer.personByPersonId ne '' }"> 
										<span class="width50">${BANK_RECEIPTS.customer.personByPersonId.firstName} ${BANK_RECEIPTS.customer.personByPersonId.lastName}</span>  
									</c:when>
									<c:when test="${BANK_RECEIPTS.customer.company ne null && BANK_RECEIPTS.customer.company ne '' }">
										<span class="width50">${BANK_RECEIPTS.customer.company.companyName}</span>   
									</c:when>
									<c:when test="${BANK_RECEIPTS.customer.cmpDeptLocation ne null && BANK_RECEIPTS.customer.cmpDeptLocation ne '' }">
										<span class="width50">${BANK_RECEIPTS.customer.cmpDeptLocation.company.companyName}</span>   
									</c:when>
									<c:when test="${BANK_RECEIPTS.person ne null && BANK_RECEIPTS.person ne '' }">
			 							<span class="width50">${BANK_RECEIPTS.person.firstName} ${BANK_RECEIPTS.person.lastName}</span>   
									</c:when>
									<c:when test="${BANK_RECEIPTS.others ne null && BANK_RECEIPTS.others ne '' }"> 
										<span class="width50">${BANK_RECEIPTS.others}</span> 
									</c:when> 
								</c:choose>   
							</div> 
							<div>
								<label class="width30">Description</label>
								<span class="width50">${BANK_RECEIPTS.description}</span>
							</div>
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset> 
							<div>
								<label class="width30" for="receiptsNo">Receipt Number</label> 
								<input type="hidden" name="bankReceiptsId" id="bankReceiptsId" value="${BANK_RECEIPTS.bankReceiptsId}"/>
								<span class="width50">${BANK_RECEIPTS.receiptsNo}</span>  
							</div>  
							<div>
								<label class="width30" for="receiptsDate">Date</label> 
								<c:set var="receiptsDate" value="${BANK_RECEIPTS.receiptsDate}"/>  
								<span class="width50">
										<%= DateFormat.convertDateToString(pageContext.getAttribute("receiptsDate").toString())%>
								</span>   
							</div> 
							<div>
								<label class="width30" for="receiptsTypeId">Receipts Type</label> 
								<input type="hidden" id="receiptsTypedb" value="${BANK_RECEIPTS.lookupDetail.lookupDetailId}"/>
								<span class="width50">${BANK_RECEIPTS.lookupDetail.displayName}</span> 
							</div>  
							<div>
								<label class="width30" for="currencyId">Currency</label> 
								<input type="hidden" id="currencydb" value="${BANK_RECEIPTS.currency.currencyId}"/> 
								<span class="width50">${BANK_RECEIPTS.currency.currencyPool.code}</span> 
							</div>  
							<div>
								<label class="width30" for="referenceNo">Ref.No</label> 
								<span class="width50">${BANK_RECEIPTS.referenceNo}</span>  
							</div>
						</fieldset>
					</div> 
			</div>  
		</div>   
		<div class="clearfix"></div> 
		<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
 				<legend>Receipt Information</legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100">   
						 <table id="hastab" class="width100"> 
							<thead>
							   <tr>   
								    <th style="width:5%">Payment Mode</th> 
								    <th style="width:5%">Institution Name</th> 
								    <th style="width:5%">Cheque/TT/Card Ref</th>  
								    <th style="width:5%">Cheque/TT Date</th>  
								    <th style="width:5%">Reference</th> 
 								    <th style="width:5%">Amount</th> 
								    <th style="width:5%">Status</th> 
								    <th style="width:5%">Description</th> 
 							  </tr>
							</thead> 
							<tbody class="tab">  
								<c:choose>
									<c:when test="${BANK_RECEIPTS.bankReceiptsDetails ne null && BANK_RECEIPTS.bankReceiptsDetails ne ''}">
										<c:forEach var="RECEIPTS_DETAILS" items="${BANK_RECEIPTS.bankReceiptsDetails}" varStatus="status"> 
											<tr class="rowid" id="fieldrow_${status.index+1}"> 
												<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td> 
												<td>
													${RECEIPTS_DETAILS.lookupDetailByPaymentMode.displayName} 
													<input type="hidden" id="paymentModedb_${status.index+1}" value="${RECEIPTS_DETAILS.lookupDetailByPaymentMode.lookupDetailId}"/>
												</td>
												<td> 
													${RECEIPTS_DETAILS.institutionName} 
												</td>
												<td> 
													${RECEIPTS_DETAILS.bankRefNo} 
												</td>
												<td> 
													<c:set var="chequeDate" value="${RECEIPTS_DETAILS.chequeDate}"/>  
													<%= DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString())%> 
												</td>
												<td> 
													${RECEIPTS_DETAILS.referenceNo}
												</td> 
												<td>
													${RECEIPTS_DETAILS.amount}
												</td>  
												<td>
													<select name="paymentStatus" class="paymentStatus" id="paymentStatus_${status.index+1}">
														<option value="">Select</option>
														<c:choose>
													 		<c:when test="${PAYMENT_STATUS ne null && PAYMENT_STATUS ne ''}">
													 			<c:forEach items="${PAYMENT_STATUS}" var="PAY_STATUS">
													 				<option value="${PAY_STATUS.lookupDetailId}@@${PAY_STATUS.accessCode}">${PAY_STATUS.displayName}</option>
													 			</c:forEach> 
													 		</c:when>
													 	</c:choose> 
													</select>
													<input type="hidden" id="paymentStatusdb_${status.index+1}" value="${RECEIPTS_DETAILS.lookupDetailByPaymentStatus.lookupDetailId}"/>
												</td>
												<td>
												  <input type="text" name="linedescription" class="linedescription" id="linedescription_${status.index+1}" value="${RECEIPTS_DETAILS.description}"/>
												</td>
												<td style="display:none;">
													<input type="hidden" id="bankReceiptsLineId_${status.index+1}" value="${RECEIPTS_DETAILS.bankReceiptsDetailId}"/>  
													<input type="hidden" name="contractPaymentId" id="contractPaymentId_${status.index+1}" value="${RECEIPTS_DETAILS.contractPayment.contractPaymentId}"/> 
													<input type="hidden" name="releaseId" id="releaseId_${status.index+1}" value="${RECEIPTS_DETAILS.release.releaseId}"/> 
												</td>  
											</tr>
										</c:forEach> 
									</c:when>
								</c:choose> 
					 		</tbody>
						</table>
					</div> 
			</fieldset>
		</div>  
		<div class="clearfix"></div> 
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-right" id="receipts_discard" ><fmt:message key="accounts.common.button.cancel"/></div>
			<div class="portlet-header ui-widget-header float-right" id="receipts_save" ><fmt:message key="accounts.common.button.save"/></div> 
		</div>  
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div> 
  	</form>
  </div> 
</div>