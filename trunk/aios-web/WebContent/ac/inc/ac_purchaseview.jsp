<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
	<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var actionname="";
var purchaseDetails="";
var accessCode = "";
var globalRowId=null;
var productItemType=null;

$(function(){   
	
	var purchaseId = Number($('#purchaseId').val());   
	 $('#purchase_document_information').click(function(){
			if(purchaseId>0){
				AIOS_Uploader.openFileUploader("doc","purchaseDocs","Purchase",purchaseId,"PurchaseDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","purchaseDocs","Purchase","-1","PurchaseDocuments");
			}
		});
	 
	 if(purchaseId>0)
		populateUploadsPane("doc","purchaseDocs","Purchase",purchaseId);	
	$jquery("#purchaseentry_details").validationEngine('attach'); 
	 
	$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
	$('#selectedMonth,#linkedMonth').change();
	$('#l10nLanguage,#rtlLanguage').change();
	if ($.browser.msie) {
	        $('#themeRollerSelect option:not(:selected)').remove();
	}
	$('#purchaseDate,#expiryDate,#deliveryDate').datepick({
	 onSelect: customRange, showTrigger: '#calImg'}); 


	$('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	  $('#product-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
 
	  $('#supplier-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });

	  $('#quotation-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
	   
	$('.quotation-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_commom_quotation_popup.action", 
		 	async: false,  
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.supplierpurchase-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,  
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.contactpurchase-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data:{personTypes: "1"},
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	{
		var itemType="I";
		var rowId=Number(1);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchase_product_common_popup_preload.action", 
		 	async: false,  
		 	data:{rowId: rowId, itemType: itemType, pageInfo: "purchase"},
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#product-inline-popup').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
	}
	$('.purchaseproduct-common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove(); 
		globalRowId=getRowId($(this).attr('id'));
		productItemType=$('#itemnature_'+rowId).val();
		
		$('#product-inline-popup').dialog('open');
		<%-- $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchase_product_common_popup.action", 
		 	async: false,  
		 	data:{rowId: rowId, itemType: itemType, pageInfo: "purchase"},
 		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});   --%>
		return false;
	}); 
	
	
	
	$('#product-inline-popup').dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});

	 $('.shipping-term-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        accessCode = $(this).attr("id");
 	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="SHIPPING_TERMS"){
				$('#shippingTerm').html("");
				$('#shippingTerm').append("<option value=''>Select</option>");
				loadLookupList("shippingTerm"); 
			} 
		});
		
	$('.itemnature').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	});

	$('.unitrate').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var unitrate=Number($(this).val()).toFixed(2);
		$(this).val(unitrate);
		var quantity=Number($('#quantity_'+rowId).val());
		var total=Number(0);  
		if(quantity!=null && unitrate>0 && quantity>0){
			total=Number(unitrate*quantity).toFixed(2);
			$('#totalamount_'+rowId).text(total);
			triggerAddRow(rowId);
		}
		calculateTotalPurchase();
		return false;
	});

	$('.quantity').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var quantity=Number($(this).val());
		var total=Number(0);
		var unitrate=Number($('#unitrate_'+rowId).val());  
		if(unitrate!=null && quantity>0 && unitrate>0 ){
			total=Number(unitrate*quantity).toFixed(2);
			$('#totalamount_'+rowId).text(total);
			triggerAddRow(rowId);
		}
		calculateTotalPurchase();
		return false;
	});

	 $('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/purchase_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 });  
				}
			});
		  return false;
	 }); 

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
 		 calculateTotalPurchase();
 		 return false;
	 }); 

	 if($('#purchaseId').val()>0){
			calculateTotalPurchase();
			$('#currencyId').val($('#tempcurrencyId').val());
			$('#termId').val($('#temptermId').val()); 
			if($('#quotationId').val()>0){
				$('#currencyId').attr('disabled',true);
				$('#supplier').hide();
				$('.quoteprod').remove();
			}
			$('.rowid').each(function(){
				var rowId=getRowId($(this).attr('id'));
				$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val());
			});
			$('#status').val($('#tempstatus').val());
			$('#shippingTerm').val($('#tempshippingTerm').val());
			$('#paymentMode').val($('#temppaymentMode').val());
			if(Number($('#quotationId').val())>0){ 
				$('.itemnature').attr("disabled",true); 
				$('.unitrate').attr('readOnly',true);
				$('.quantity').attr('readOnly',true);
				$('.rowid').each(function(){   
					var rowId = getRowId($(this).attr('id'));
					$('#itemnature_'+rowId).hide(); 
					$('#itemtype_'+rowId).text($('#itemnature_'+rowId+" :selected").text()); 
					var nexttab = $('#fieldrow_' + rowId).next(); 
					if (
							$(nexttab).hasClass('lastrow')) {
						$(this).remove();
					}
					
				});
			}
		}else{ 
			$('#status').val(1);
			var default_currency=$('#default_currency').val();
			$('#currencyId option').each(function(){  
				var curval=$(this).val();
				if(curval==default_currency){   
					$('#currencyId option[value='+$(this).val()+']').attr("selected","selected"); 
					return false;
				}
			});
		} 

	 $('.pdiscard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/getpurchaseorder.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
					$("#main-wrapper").html(result);  
				}
		 });
	 });

	 $('.save').click(function(){ 
		 if($jquery("#purchaseentry_details").validationEngine('validate')){
 			 var purchaseId=Number($('#purchaseId').val()); 
			 
			 var quotationId=Number($('#quotationId').val());
			 var purchaseDate=$('#purchaseDate').val();
			 var currencyId=$('#currencyId').val();
			 var supplierId=Number($('#supplierId').val()); 
			 if(supplierId==0){
				 var shelfIds = new Array();
					$('.rowid').each(
							function() {
								var rowId = getRowId($(this).attr('id'));
								var shelfId = Number($('#shelfId_' + rowId).val());
								if(shelfId!=null && shelfId>0)
								shelfIds.push(shelfId);
					});
					if(shelfIds.length==0){
						$('#page-error').hide().html(
						"Please choose store information for each purchase detail.");
						return false;
					}
			 }
			 var termId=Number($('#termId').val());
			 var expiryDate=$('#expiryDate').val();
			 var description = $("#description").val();
			 var purchaseNumber=$('#purchaseNumber').val();
			 var purchaseManagerId = Number($('#purchaseManagerId').val());
			 var shippingTermId = Number($('#shippingTerm').val());
			 var paymentModeId = Number($('#paymentMode').val());
			 var status = $('#status').val();
			 var deliveryAddress = $('#deliveryAddress').val();
			 var deliveryDate = $('#deliveryDate').val();
			 var continuousPurchase = $('#continuousPurchase').attr('checked');
			 purchaseDetails=getPurchaseDetails();   
			if(purchaseDetails!=null && purchaseDetails!=""){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/savepurchaseorder.action", 
					 	async: false,
					 	data : { purchaseId:purchaseId, quotationId:quotationId, purchaseDate:purchaseDate, currencyId:currencyId, 
						 		 paymentModeId: paymentModeId, supplierId:supplierId, paymentTermId:termId, expiryDate:expiryDate, status:status,
						 		 purchaseDetails: purchaseDetails, purchaseManagerId: purchaseManagerId, shippingTermId: shippingTermId,
						 		 description:description, purchaseNumber:purchaseNumber, deliveryAddress: deliveryAddress, deliveryDate: deliveryDate,
						 		continuousPurchase: continuousPurchase
						 		},
					    dataType: "json",
					    cache: false,
						success:function(response){
							if(response.returnMessage == "SUCCESS"){
								$('#common-popup').dialog('destroy');		
								$('#common-popup').remove();   
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/getpurchaseorder.action",
																	async : false,
																	dataType : "html",
																	cache : false,
																	success : function(
																			result) {
																		$(
																				"#main-wrapper")
																				.html(
																						result);
																		if (purchaseId > 0)
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record updated")
																					.slideDown(
																							1000);
																		else
																			$(
																					'#success_message')
																					.hide()
																					.html(
																							"Record created")
																					.slideDown(
																							1000);
																		$(
																				'#success_message')
																				.delay(
																						2000)
																				.slideUp();
																	}
																});
													} else {
														$('#page-error')
																.hide()
																.html(
																		response.returnMessage)
																.slideDown(1000);
														$('#page-error').delay(
																2000).slideUp();
														return false;
													}
												} 
											});
								} else {
									$('#page-error').hide().html(
											"Please enter purchase details.")
											.slideDown(1000);
									$('#page-error').delay(2000).slideUp();
									return false;
								}
							} else {
								return false;
							}
						});

		var getPurchaseDetails = function() {
			purchaseDetails = "";
			var productid = new Array();
			var itemnatures = new Array();
			var unitrate = new Array();
			var totalunit = new Array();
			var lineid = new Array();
			var lineDescriptions = new Array();
			var shelfIds = new Array();
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var productId = $('#productid_' + rowId).val();
						var unitRate = $('#unitrate_' + rowId).val();
						var quantity = $('#quantity_' + rowId).val();
						var purchaseLineId = Number($(
								'#purchaseLineId_' + rowId).val());
						var itemnature = $('#itemnature_' + rowId).val();
						var linedescription = $('#linedescription_' + rowId)
								.val();
						var shelfId = Number($(
								'#shelfId_' + rowId).val());
						if (typeof productId != 'undefined'
								&& productId != null && productId != ""
								&& unitRate != null && unitRate != "") {
							productid.push(productId);
							unitrate.push(unitRate);
							totalunit.push(quantity);
							lineid.push(purchaseLineId);
							if (linedescription != null
									&& linedescription != "")
								lineDescriptions.push(linedescription);
							else
								lineDescriptions.push("##");
							itemnatures.push(itemnature);
							shelfIds.push(shelfId);

						}
					});
			for ( var j = 0; j < productid.length; j++) {
				purchaseDetails += productid[j] + "__" + unitrate[j] + "__"
						+ totalunit[j] + "__" + lineid[j] + "__"
						+ lineDescriptions[j] + "__" + itemnatures[j]+ "__" + shelfIds[j];
				if (j == productid.length - 1) {
				} else {
					purchaseDetails += "#@";
				}
			}
			return purchaseDetails;
		};
		
		
		
		$('.purchase-store-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_session_common.action", 
			 	async: false,  
			 	data:{rowId : rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.store-result').html(result);  
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});

		$('#store-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 zIndex:100000,
			 modal: true 
		});  
			
		$('.rackid').live(
				'dblclick',
				function() {
					currentId = $(this);
					tempvar = $(currentId).attr('id');
					idarray = tempvar.split('_');
					rowid = Number(idarray[1]);
						var storeRowId = Number($('#store_row_id').html()); 
					$('#storeDetail_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#rackname_'+storeRowId).text($('#rack_' + rowid).html()); 
					$('#storeName_'+storeRowId).text($('#rackstorename_' + rowid).text()); 
					$('#stored_'+storeRowId).val($('#rackstoreid_' + rowid).html());
					$('#shelfId_'+storeRowId).val($('#rackid_' + rowid).html()); 
					$('#shelfName').val($('#rack_' + rowid).html()); 
					$('#store-popup').dialog('close');
					$('#common-popup').dialog('close');
					return false;
		});
		
	});
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	//Prevent selection of invalid dates through the select controls
	function checkLinkedDays() {
		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}
	function customRange(dates) {
		if (this.id == 'purchaseDate') {
			$('#expiryDate').datepick('option', 'minDate', dates[0] || null);
			if ($('#purchaseDate').val() != "") {
				addPeriods();
			}
		} else {
			$('#purchaseDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function callClearFields() {
		$('.supp-pop').show();
		$('.site-pop').show();
		$('#currencyId').attr('disabled', false);
		$('#quotationId').val("");
		$('#quotationNumber').val("");
		$('.prod').show();
		$('#currencyId').val("");
		$('#supplierId').val("");
		$('#siteId').val("");
		$('#supplierName').val("");
		$('#siteName').val("");
	}
	function triggerAddRow(rowId) {
		var itemNature = $('#itemnature_' + rowId).val();
		var productid = $('#productid_' + rowId).val();
		var unitrate = $('#unitrate_' + rowId).val();
		var quantity = $('#quantity_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (itemNature != null && itemNature != "" && productid != null
				&& productid != "" && unitrate != null && unitrate != ""
				&& quantity != null && quantity != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}
	function addPeriods() {
		var date = new Date($('#purchaseDate').datepick('getDate')[0].getTime());
		$.datepick.add(date, parseInt(30, 10), 'd');
		$('#expiryDate').val($.datepick.formatDate(date));
	}
	function calculateTotalPurchase() { 
		var totalPurchase = 0;
		$('#totalPurchaseAmount').text('');
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var totalamount = Number($.trim($('#totalamount_' + rowId).html()));
			totalPurchase = Number(totalPurchase + totalamount);
		});
		$('#totalPurchaseAmount').text(Number(totalPurchase).toFixed(2));
	}
	function personPopupResult(personid, personname, commonParam) {
		$('#purchaseManagerId').val(personid);
		$('#purchaseManager').val(personname);
		$('#common-popup').dialog("close");
	}
	function commonProductPopup(productId, productName, rowId, itemSubType, aData) {
		if(rowId==null || row==0)
			rowId=globalRowId;
		
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.code +" - "+ aData.productName);
		$('#storeName_' + rowId).html(aData.storeName);
		$('#shelfId_' + rowId).val(aData.shelfId);
		$('#storeId_' + rowId).val(aData.storeId);
		if(aData.basePrice != null  && aData.basePrice > 0){
			$('#unitrate_' + rowId).val(aData.basePrice);
			var quantity = Number($('#quantity_'+rowId).val());
			var unitrate = Number($('#unitrate_'+rowId).val());
			if(quantity > 0 && unitrate>0){
				var totalAmount = Number(quantity * unitrate); 
				$('#totalamount_'+rowId).text(Number(totalAmount).toFixed(2));
			}else{
				$('#totalamount_'+rowId).text('');
			}
			calculateTotalPurchase();
		}else{
			getProductUnitRate(aData.productId, rowId);
		}  
		$('.rowid').each(
			function() {
			var rowDId = getRowId($(this).attr('id'));
			var productDId = $('#productid_' + rowDId).val();
			if(productDId == productId && rowId!=rowDId){
				 $('#line-error').hide().html('Product Duplicated, Please Check.').slideDown(1000);
				 $('#line-error').hide().delay(2000).slideUp();
				 return false;
			}
		});	 
		
		$('#product-inline-popup').dialog("close");

		return false;
	}
	function getProductUnitRate(productId, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_unitrate.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.unitRate != null  && response.unitRate > 0){
					$('#unitrate_' + rowId).val(response.unitRate); 
				} 
			} 
		});  
		var quantity = Number($('#quantity_'+rowId).val());
		var unitrate = Number($('#unitrate_'+rowId).val());
		if(quantity > 0 && unitrate>0){
			var totalAmount = Number(quantity * unitrate); 
			$('#totalamount_'+rowId).text(Number(totalAmount).toFixed(2));
		}else{
			$('#totalamount_'+rowId).text('');
		}
		calculateTotalPurchase();

		return false;
	}
	function commonSupplierPopup(supplierId, supplierName,
			combinationId, accountCode, param){
		$('#supplierId').val(supplierId);
		$('#supplierName').val(supplierName);
		$('.hidemandatory').remove();
		$('.purchase-store-popup').remove();
		$('#continuous-div').show();
		return false;
	}
	function commonQuotationPopup(quotationId, currencyId,
			quotationNumber, supplierName, supplierId){
		$('#quotationId').val(quotationId);
		$('#currencyId').val(currencyId);
		$('#currencyId').attr('disabled', true);
		$('#quotationNumber').val(quotationNumber);
		$('#supplierName').val(supplierName);
		$('#supplierId').val(supplierId);
		$('#supplier').hide();
		callQuotationDetails(quotationId);
	}
	function callQuotationDetails(quotationId){ 
		var rowId=Number(0);
		var  itemNature='D'; 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/showquotation_detail.action", 
			 	async: false,  
			 	data:{quotationId:quotationId, rowId:rowId, itemType:itemNature},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.quotation_detail').html(result);  
				},
				error:function(result){  
					 $('.quotation_detail').html(result); 
				}
			});  
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>purchase
			order
		</div> 
		<form name="purchaseentry_details" id="purchaseentry_details" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="purchaseId" name="purchaseId"
					value="${PURCHASE_INFO.purchaseId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 230px;">
							<div style="display: none;">
								<label class="width30" for="supplierName">Supplier
									Number </label> <input type="hidden"
									id="supplierId" name="supplierId"
									value="${PURCHASE_INFO.supplier.supplierId}" /> <input
									type="text" disabled="disabled" name="supplierNumber"
									id="supplierNumber"
									value="${PURCHASE_INFO.supplier.supplierNumber}"
									class="width50" />
							</div>
							<div>
								<label class="width30 tooltip">Supplier Name </label> <input type="text"
									name="supplierName"
									value="${PURCHASE_INFO.supplier.cmpDeptLocation.company.companyName}${PURCHASE_INFO.supplier.company.companyName}"
									id="supplierName" class="width50" TABINDEX=6
									disabled="disabled"> 
							</div>
							<div>
								<label class="width30" for="termId">Payment Terms</label> <select
									name="termId" id="termId" disabled="disabled" style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${PAYMENT_DETAIL ne null && PAYMENT_DETAIL ne ''}">
											<c:forEach var="pbean" items="${PAYMENT_DETAIL}">
												<option value="${pbean.creditTermId}">${pbean.name}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="temptermId" name="temptermId"
									value="${PURCHASE_INFO.creditTerm.creditTermId}" />
							</div>

							<div>
								<label class="width30" for="shippingTerm">Shipping Terms</label>
								<select name="shippingTerm" id="shippingTerm" disabled="disabled"
									style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${SHIPPING_TERMS ne null && SHIPPING_TERMS ne ''}">
											<c:forEach var="ship" items="${SHIPPING_TERMS}">
												<option value="${ship.lookupDetailId}">${ship.displayName}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" id="tempshippingTerm"
									name="tempshippingTerm" disabled="disabled"
									value="${PURCHASE_INFO.lookupDetail.lookupDetailId}" /> 
							</div>

							<div>
								<label class="width30" for="paymentMode">Payment Mode</label> <select
									name="paymentMode" id="paymentMode" disabled="disabled" style="width: 51%;">
									<option value="">Select</option>
									<c:choose>
										<c:when
											test="${MODE_OF_PAYMENT ne null && MODE_OF_PAYMENT ne ''}">
											<c:forEach var="payment" items="${MODE_OF_PAYMENT}">
												<option value="${payment.key}">${payment.value}</option>
											</c:forEach>
										</c:when>
									</c:choose>
								</select> <input type="hidden" disabled="disabled" id="temppaymentMode"
									name="temppaymentMode" value="${PURCHASE_INFO.modeOfPayment}" />
							</div>
							<div>
								<label class="width30">Contact</label> <input type="hidden"
									id="purchaseManagerId" name="purchaseManagerId"
									value="${PURCHASE_INFO.personByPurchaseManager.personId}" /> <input type="text"
									id="purchaseManager" name="purchaseManager" class="width50"
									value="${PURCHASE_INFO.personByPurchaseManager.firstName} ${PURCHASE_INFO.personByPurchaseManager.lastName}" />
							
							</div>
							<div>
								<label class="width30" for="deliveryDate">Delivery Date</label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.deliveryDate ne null && PURCHASE_INFO.deliveryDate ne ''}">
										<c:set var="deliveryDate"
											value="${PURCHASE_INFO.deliveryDate}" />
										<%
											String deliveryDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"deliveryDate").toString());
										%>
										<input type="text" disabled="disabled" name="deliveryDate"
											id="deliveryDate" class="width50" value="<%=deliveryDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" disabled="disabled" name="deliveryDate"
											id="deliveryDate" class="width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Delivery Address</label>
								<textarea id="deliveryAddress" name="deliveryAddress" disabled="disabled"
									class="width50">${PURCHASE_INFO.deliveryAddress}</textarea>
							</div>
							<div class="width100 float-left ">									
							
								<div id="purchase_document_information" class="width90" style="cursor: pointer; color: blue;">
									<u>Upload Documents Here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px;" class="width90">
									<span id="purchaseDocs"></span>
								</div>
								
							</div>		
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 230px;">
							<div>
								<label class="width30" for="purchaseNumber">Purchase
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.purchaseId ne null && PURCHASE_INFO.purchaseId ne ''}">
										<input type="text" disabled="disabled" name="purchaseNumber"
											id="purchaseNumber" class="width50 validate[required]"
											value="${PURCHASE_INFO.purchaseNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" disabled="disabled" name="purchaseNumber"
											id="purchaseNumber" class="width50 validate[required]"
											value="${PURCHASE_NUMBER}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="quotationNumber">Quotation</label> <input
									type="hidden" id="quotationId" name="quotationId"
									value="${PURCHASE_INFO.quotation.quotationId}" /> <input
									type="text" disabled="disabled" name="quotationNumber"
									id="quotationNumber" class="width50"
									value="${PURCHASE_INFO.quotation.quotationNumber}" />
								
							</div>
							<div>
								<label class="width30" for="currencyId">Currency<span
									class="mandatory">*</span> </label> <select name="currencyId"
									id="currencyId" class="validate[required]" disabled="disabled" style="width: 51%;">
									<option value="">Select</option>
									
								</select> <input type="hidden" id="default_currency"
									name="default_currency" value="${DEFAULT_CURRENCY}" /> <input
									type="hidden" id="tempcurrencyId" name="tempcurrencyId"
									value="${PURCHASE_INFO.currency.currencyId}" />
							</div>
							<div>
								<label class="width30" for="purchaseDate">Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.date ne null && PURCHASE_INFO.date ne ''}">
										<c:set var="date" value="${PURCHASE_INFO.date}" />
										<%
											String date = DateFormat.convertDateToString(pageContext
															.getAttribute("date").toString());
										%>
										<input type="text" disabled="disabled" name="purchaseDate"
											id="purchaseDate" class="width50 validate[required]"
											value="<%=date%>" />
									</c:when>
									<c:otherwise>
										<input type="text" disabled="disabled" name="purchaseDate"
											id="purchaseDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="expiryDate">Expiry Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${PURCHASE_INFO.expiryDate ne null && PURCHASE_INFO.expiryDate ne ''}">
										<c:set var="expiryDate" value="${PURCHASE_INFO.expiryDate}" />
										<%
											String expiryDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"expiryDate").toString());
										%>
										<input type="text" disabled="disabled" name="expiryDate"
											id="expiryDate" class="width50 validate[required]"
											value="<%=expiryDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" disabled="disabled" name="expiryDate"
											id="expiryDate" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
							</div>
							<div style="display: none;" id="continuous-div">
								<label class="width30" for="continuousPurchase">Continuous PO</label>  
								<c:choose>
									<c:when test="${PURCHASE_INFO.continuousPurchase ne null && PURCHASE_INFO.continuousPurchase eq true}">
										<input type="checkbox" disabled="disabled" name="continuousPurchase" id="continuousPurchase" class="width3" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" disabled="disabled" name="continuousPurchase" id="continuousPurchase" class="width3" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Status</label> <select name="status"
									id="status" class="validate[required]" disabled="disabled" style="width: 51%;">
									<option>Select</option>
									<c:forEach var="poStatus" items="${PO_STATUS}">
										<option value="${poStatus.key}">${poStatus.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempstatus" name="tempstatusId"
									value="${PURCHASE_INFO.status}" />
							</div>
							<div>
								<label class="width30 tooltip">Description</label>
								<textarea id="description" disabled="disabled" class="width50">${PURCHASE_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Purchase Detail<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100"> 
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Item Type</th>
									<th style="width: 15%">Product</th>
									<th style="width: 10%">Store<span 
										class="mandatory hidemandatory">*</span></th>
									<th style="display: none;">UOM</th>
									<th style="width: 5%">Unit Rate</th>
									<th style="width: 5%">Quantity</th>
									<th style="width: 5%">Total Amount</th>
									<th style="width: 10%">Description</th>
									
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail" items="${PURCHASE_DETAIL_INFO}"
									varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
										<td id="itemtype_${status.index+1}"><select
											name="itemnature" disabled="disabled" class="width98 itemnature"
											id="itemnature_${status.index+1}">
												<option value="A">Asset</option>
												<option value="E">Expense</option>
												<option value="I">Inventory</option>
										</select> <input type="hidden" id="tempitemnature_${status.index+1}"
											name="itemnature" value="${detail.product.itemType}" /></td>
										<td><span class="float-left width60"
											id="product_${status.index+1}">${detail.product.code} - ${detail.product.productName}</span>
											 <input
											type="hidden" id="productid_${status.index+1}"
											value="${detail.product.productId}"  />
										</td>
										<td>
										<span class="float-left width60" id="storeName_${status.index+1}">${detail.shelf.shelf.aisle.store.storeName} >> ${detail.shelf.shelf.aisle.sectionName} >> ${detail.shelf.shelf.name} >> ${detail.shelf.name}</span>
												<input type="hidden" name="storeId"
												id="storeId_${status.index+1}"
												value="" /> 
												<input type="hidden" name="shelfId"
												id="shelfId_${status.index+1}"
												value="${detail.shelf.shelfId}" /> 
												
										</td>
										<td id="uom_${status.index+1}" class="uom"
											style="display: none;"></td>
										<td><input type="text" disabled="disabled"
											class="width96 unitrate validate[optional,custom[number]]"
											id="unitrate_${status.index+1}" value="${detail.unitRate}"
											style=" text-align: right;" /></td>
										<td><input type="text" disabled="disabled"
											class="width96 quantity validate[optional,custom[number]]"
											id="quantity_${status.index+1}" value="${detail.quantity}"
											 /></td>
										<td class="totalamount" id="totalamount_${status.index+1}"
											style="text-align: right;"><c:out
												value="${detail.unitRate * detail.quantity}" /></td>
										<td><input type="text" disabled="disabled" name="linedescription"
											id="linedescription_${status.index+1}"
											value="${detail.description}" /></td>
										<td style="display: none;"><input type="hidden"
											id="purchaseLineId_${status.index+1}" 
											value="${detail.purchaseDetailId}" /></td>
										
									</tr>
								</c:forEach>
								
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="float-right width30" style="font-weight: bold;">
				<span style="font-weight: bold;">Total: </span> <span
					id="totalPurchaseAmount"></span>
			</div>
			<div class="clearfix"></div>

			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right pdiscard"
					id="discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="product-inline-popup" style="display: none;"
				class="width100 float-left" id="hrm">
				
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>