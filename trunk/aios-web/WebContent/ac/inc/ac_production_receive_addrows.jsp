<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="hidden" name="productId"
		id="productid_${requestScope.rowId}" /> <span
		id="product_${requestScope.rowId}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_product_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><input type="hidden" name="storeId"
		id="storeid_${requestScope.rowId}" /> <input type="hidden"
		name="storeDetail" id="storeDetail_${requestScope.rowId}" /> <span
		id="store_${requestScope.rowId}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="storeID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all store-common-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><input type="text" name="productQty"
		id="productQty_${requestScope.rowId}"
		class="productQty validate[optional,custom[number]] width80">
		</td>
	<td><input type="text" name="amount"
		id="amount_${requestScope.rowId}" style="text-align: right;"
		class="amount width98 validate[optional,custom[number]] right-align">
	</td>
	<td><span id="totalAmount_${requestScope.rowId}"
		style="float: right;"></span></td>
	<td><input type="text" name="linesDescription"
		id="linesDescription_${requestScope.rowId}" class="width98"
		maxlength="150"></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" name="productReceiveDetailId"
		id="productReceiveDetailId_${requestScope.rowId}" /></td>
</tr>