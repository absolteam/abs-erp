<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tr id="fieldrow_${rowId}" class="rowid"> 
	<td id="company_${rowId}">
		<select id="companyCode_${rowId}" class="accountcode companyclass_${rowId}" name="companyCode">
			<option value="">Select</option>
			<c:forEach items="${COMPANY_ACCOUNT.companyList}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td id="cost_${rowId}">
		<select id="costCode_${rowId}" class="accountcode costclass_${rowId}" name="costCode">
			<option value="">Select</option>
			<c:forEach items="${COST_ACCOUNT.costList}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td id="natural_${rowId}">
		<select id="naturalCode_${rowId}" class="accountcode naturalclass_${rowId}" name="naturalCode">
			<option value="">Select</option>
			<c:forEach items="${NATURAL_ACCOUNT.naturalList}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td id="analysis_${rowId}">
		<select id="analysisCode_${rowId}" class="accountcode analysisclass_${rowId}" name="analysisCode">
			<option value="">Select</option>
			<c:forEach items="${ANALYSIS_ACCOUNT.analysisList}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td id="buffer1_${rowId}">
		<select id="buffer1Code_${rowId}" class="accountcode buffer1class_${rowId}" name="buffer1Code">
			<option value="">Select</option>
			<c:forEach items="${BUFFER1_ACCOUNT.bufferList1}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td id="buffer2_${rowId}">
		<select id="buffer2Code_${rowId}" class="accountcode buffer2class_${rowId}" name="buffer2Code">
			<option value="">Select</option>
			<c:forEach items="${BUFFER2_ACCOUNT.bufferList1}" var="bean">
				<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
			</c:forEach>
		</select>
	</td>
	<td style="display:none;" id="lineId_${rowId}">${rowId}</td>
	<td style="display:none;" id="combinationid_${rowId}"></td>  
	<td style="display:none;">
		<input type="hidden" id="companyid_${rowId}" readonly="readonly">
		<input type="hidden" id="costid_${rowId}" readonly="readonly">
		<input type="hidden" id="naturalid_${rowId}" readonly="readonly">
		<input type="hidden" id="analysisid_${rowId}" readonly="readonly">
		<input type="hidden" id="buffer1id_${rowId}" readonly="readonly">
		<input type="hidden" id="buffer2id_${rowId}" readonly="readonly">
	</td>
	<td id="option_${i}">
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addRowData" id="AddImage_${rowId}" style="cursor:pointer;display:none;" title="Add Record">
				<span class="ui-icon ui-icon-plus"></span>
	  </a>	
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editRowData" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
	  </a> 
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delRowrow" id="DeleteImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
	  </a>
	  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
			<span class="processing"></span>
	  </a>
   </td> 
 </tr>	
<script type="text/javascript">
$(function(){
	
	 $(".accountcode").combobox({ 
         selected: function(event, ui){ 
	       	 slidetab=$(this).parent().parent().get(0);  
	  		 var nexttab=$(slidetab).next(); 
	  	  	 triggerAddRow(nexttab,slidetab); 
         }
     }); 
	 
	$('.addRowData').click(function(){
		if($("#combinationvalidate-form").validationEngine({returnIsValid:true})){   
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addadd";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_of_account_addlineentry.action",
			data:{id:rowid, showPage:showPage},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			}
		 });
		}
		else{
			return false;
		}		
	});

	$('.editRowData').click(function(){
		if($("#combinationvalidate-form").validationEngine({returnIsValid:true})){   
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addedit";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_of_account_addlineentry.action",
			data:{id:rowid, showPage:showPage},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#companyCode').val($('#company_'+rowid).text());
			 	$('#costCode').val($('#cost_'+rowid).text());
			 	$('#naturalCode').val($('#natural_'+rowid).text());
			 	$('#analyisCode').val($('#analysis_'+rowid).text());
			 	$('#bufferCode1').val($('#buffer1_'+rowid).text());
			 	$('#bufferCode2').val($('#buffer2_'+rowid).text());

			 	$('#companyId').val($('#companyid_'+rowid).text());
			 	$('#costAccountId').val($('#costid_'+rowid).text());
			 	$('#naturalAccountId').val($('#naturalid_'+rowid).text());
			 	$('#analyisAccountId').val($('#analysisid_'+rowid).text());
			 	$('#bufferAccountId1').val($('#buffer1id_'+rowid).text());
			 	$('#bufferAccountId2').val($('#buffer2id_'+rowid).text());
			}
		 });
		}
		else{
			return false;
		}		
	});

	$(".delRowrow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var companycode=$('#companyCode_'+rowid).val();
		 var costcode=$('#costCode_'+rowid).val();
		 var naturalcode=$('#naturalCode_'+rowid).val(); 
		 if(companycode!=null && companycode!="" && costcode!=null && costcode!="" && naturalcode!=null && naturalcode!=""){ 
			 var flag=true;  
				 if(flag==true){ 
	        		 var childCount=Number($('#childCount').val());
	        		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount); 
	        		 } 
	        		 $(this).parent().parent('tr').remove();  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
	 });
});
</script>