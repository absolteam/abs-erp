<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:choose>
	<c:when test="${RECEIPT_DETAILS ne null && fn:length(RECEIPT_DETAILS)>0}">
		<c:forEach items="${POS}" var="pos">
		<div class="clear-fix"></div>
		<c:set var="totalQuantity" value="${0}" />
		<c:set var="totalAmount" value="${0}" />
		<c:set var="totalDiscount" value="${0}" />
		<c:set var="totalUnitRate" value="${0}" />
		<c:set var="totalReceipt" value="${0}" />
		<div id="hrm" class="hastable width100">
			<table id="hastab" class="width100">
				<thead>
					<tr>
						<th>REFERENCE</th>
						<th>STORE</th>
						<th>PRODUCT</th>
						<th>QUANTITY</th>
						<th>UNIT RATE</th>
						<th>DISCOUNT</th>
						<th>TOTAL</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pos.value}" var="posHead">
						<c:forEach items="${posHead.pointOfSaleDetailVOs}" var="posdetail">
							<tr>
								<td>${posHead.referenceNumber}</td>
								<td>${posHead.storeName}</td>
								<td>${posdetail.productName}</td>
								<td>${posdetail.quantity}</td>
								<td style="text-align: right;">${posdetail.unitRate}</td>
								<td style="text-align: right;"><c:choose>
										<c:when
											test="${posdetail.discountValue ne null && posdetail.discountValue gt 0}">
											<c:choose>
												<c:when test="${posdetail.isPercentageDiscount eq true}">
													<span class="percentage-mode">(Percentage)</span>
												</c:when>
												<c:otherwise>
													<span class="percentage-mode">(Amount)</span>
												</c:otherwise>
											</c:choose>
															${posdetail.discount}
														</c:when>
										<c:otherwise>-N/A-</c:otherwise>
									</c:choose>
								</td>
								<td><span style="text-align: right; float: right;">${posdetail.totalPrice}</span>
								</td>
							</tr>
						</c:forEach>
						<c:set var="totalQuantity" value="${posHead.totalQuantity}" />
						<c:set var="totalAmount" value="${posHead.salesAmount}" />
						<c:set var="totalDiscount" value="${posHead.customerDiscount}" />
						<c:set var="totalUnitRate" value="${posHead.totalDue}" />
					</c:forEach>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td style="text-align: right; font-weight: bold;"><span
							style="font-size: 10px;">TOTAL QUANTITY:</span> ${totalQuantity}</td>
						<td style="text-align: right; font-weight: bold;"><span
							style="font-size: 10px;">TOTAL AMOUNT:</span> ${totalUnitRate}</td>
						<td style="text-align: right; font-weight: bold;"><span
							style="font-size: 10px;">TOTAL DISCOUNT:</span> ${totalDiscount}</td>
						<td style="text-align: right; font-weight: bold;"><span
							style="font-size: 10px;">TOTAL SALES:</span> <span
							style="text-align: right; font-weight: bold;">${totalAmount}</span>
						</td>
					</tr>
				</tbody>
			</table> 
		</div>
	</c:forEach>
	<div class="width30 float-left" style="margin-top: 5px;">
		<div style="font-weight: bold;">Receipt Summary</div>
		<div id="hrm" class="hastable width100"> 
			<table id="hastab" class="width100">
				<thead>
					<tr>
						<th class="width5">Receipt Type</th>
						<th class="width5">Amount</th>  
					</tr>
				</thead>
				<tbody class="tab">
					<c:forEach var="detail" items="${RECEIPT_DETAILS}"
						varStatus="status">
						<tr>
							<td>${detail.receiptTypeStr}</td>
							<td style="text-align: right;">${detail.receiptAmountStr}</td>
						</tr>
					</c:forEach>
				</tbody>		
			</table>
		</div> 
	</div>  
	</c:when> 
	<c:otherwise>
		<span style="color: #d71e23; font-size: smaller !important;">No
					Sales(s) found.</span>
	</c:otherwise>
</c:choose> 