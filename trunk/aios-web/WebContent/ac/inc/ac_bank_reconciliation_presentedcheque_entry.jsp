<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var presentedChequeDetails = [];
$(function(){    
	
	$jquery("#reconciliationValidation").validationEngine('attach');  
	$jquery(".statementBalance").number(true, 2);
	
	$('#transactionDate').datepick();  

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_bank_reconciliation_list.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });  
	
	 var getPresentedChequeDetails = function(){
			presentedChequeDetails = [];
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var reconciledChecked = $('#reconcile_'+rowId).attr('checked');
				if(reconciledChecked == true){
					var useCaseId = Number($('#useCaseId_' + rowId).val());
					var useCase = $('#useCase_' + rowId).val(); 
					presentedChequeDetails.push({
						"useCaseId" : useCaseId,
						"useCase" : useCase
					});
				}
			});
			return presentedChequeDetails;
		};
		
	 $('#reconciliation_save').click(function(){ 
		 if($jquery("#reconciliationValidation").validationEngine('validate')){ 
 			var reconciliationId = Number($('#reconciliationId').val()); 
 			var referenceNumber = $('#referenceNumber').val();
  			var bankAccountId =  $('#accountId').val();   
  			var statementReference = $('#statementReference').val();
 			var transactionDate = $('#transactionDate').val();
 			var description=$('#description').val();   
 			presentedChequeDetails = [];
 			presentedChequeDetails = getPresentedChequeDetails();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/save_presentedcheque_bankreconciliation.action", 
			 	async: false, 
			 	data:{	
			 			reconciliationId: reconciliationId, referenceNumber: referenceNumber, bankAccountId: bankAccountId,description: description,
			 			transactionDate: transactionDate, statementReference: statementReference, presentedChequeDetails: JSON.stringify(presentedChequeDetails)
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
 					 if(response.returnMessage == "SUCCESS"){
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_bank_reconciliation_list.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();   
									$("#main-wrapper").html(result); 
									if(reconciliationId==0)
										$('#success_message').hide().html("Record created.").slideDown(1000);
									else
										$('#success_message').hide().html("Record updated.").slideDown(1000);
									$('#success_message').delay(2000).slideUp();
								}
						 });
					 } 
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
				} 
			});  
 		}else{
 			return false;
 		}
	 });

	 $('#generate_statement').click(function(){  
		 if($jquery("#reconciliationValidation").validationEngine('validate')){ 
			 var reconciliationDate = $('#reconciliationDate').val();
			 var statementEndDate = $('#statementEndDate').val();
			 var bankAccountId = Number($('#accountId').val());
			 var statementBalance = Number($jquery('#statementBalance').val());
			 var statementReference = $('#statementReference').val();
			 var referenceNumber = $('#referenceNumber').val();
	 		 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_presentedcheques_reconciliations.action", 
				 	async: false,  
				 	data : {bankAccountId : bankAccountId, reconciliationDate: reconciliationDate, statementReference: statementReference,
					 		statementEndDate: statementEndDate, statementBalance: statementBalance, referenceNumber: referenceNumber},  
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $('.bank-reconciliation-detail').html(result);  
						 if(Number($('#processFlag').val()) == 1){
							 $('#reconciliation_save').show();
							 $('#print_statement').show();
					     } else{
					    	 $('#reconciliation_save').hide();
							 $('#print_statement').hide();
						  }
					},
					error:function(result){  
						 $('.bank-reconciliation-detail').html(result); 
					}
				});  
			}else
				 return false;
	 });
	 
	 $('#print_statement').click(function(){  
		 
		 window.open('<%=request.getContextPath()%>/show_reconciliation_print.action',
					'_blank','width=900,height=700,left=100px,top=2px');
			return false;
	 });

 	$('.bank-reconciliation-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getbankaccount_details.action", 
		 	async: false,
		 	data : {showPage : "bankRecon"}, 
		    dataType: "html",
		    cache: false,
			success:function(result){  
			 	$('.common-result').html(result);  
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('#common-popup').css('overflow-x', 'hidden');
			} 
		});  
		return false;
	});
 	
 	$('.statementBalance').change(function(){
 		var reconciliationId = Number($('#reconciliationId').val());
 		if(reconciliationId == 0)
 			$('.bank-reconciliation-detail').html('');
 		else{ 
 			var statementBalance = Number($jquery('#statementBalance').val());
 	 		$.ajax({
 				type:"POST",
 				url:"<%=request.getContextPath()%>/update_reconciliation_statementbalance.action",
				async : false,
				data : {
					statementBalance: statementBalance
				},
				dataType : "json",
				cache : false,
				success : function(response) {
					if (response != null
							&& response.reconciliationVO != null) {
						$('#paymentTotal')
								.html(
										response.reconciliationVO.paymentTotal);
						$('#receiptTotal')
								.html(
										response.reconciliationVO.receiptTotal);
						$('#accountBalanceStr')
								.html(
										response.reconciliationVO.accountBalanceStr);
						$('#statementBalanceStr')
								.html(
										response.reconciliationVO.statementBalanceStr);
						$('#differenceTotal')
								.html(
										response.reconciliationVO.differenceTotal);
					}
				}
			});
 			return false;
 		}
 	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
	
	$('.reconcile').live('change',function(){
		var index =  getRowId($(this).attr('id'));
		var reconciliationStatus = $('#reconcile_'+index).attr('checked');
		var flowId = Number($('#flowId_'+index).val());
		var statementBalance = Number($jquery('#statementBalance').val());
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_reconciliation_details.action",
										async : false,
										data : {
											index : index,
											reconciliationStatus : reconciliationStatus,
											flowId : flowId, statementBalance: statementBalance
										},
										dataType : "json",
										cache : false,
										success : function(response) {
											if (response != null
													&& response.reconciliationVO != null) {
												$('#paymentTotal')
														.html(
																response.reconciliationVO.paymentTotal);
												$('#receiptTotal')
														.html(
																response.reconciliationVO.receiptTotal);
												$('#accountBalanceStr')
														.html(
																response.reconciliationVO.accountBalanceStr);
												$('#statementBalanceStr')
														.html(
																response.reconciliationVO.statementBalanceStr);
												$('#differenceTotal')
														.html(
																response.reconciliationVO.differenceTotal);
											}
										}
									});
							return false;
						});

		var reconciliationId = Number($('#reconciliationId').val());
		$('#bank_reconciliation_document_information').click(
			function() {
			if (reconciliationId > 0) {
				AIOS_Uploader
						.openFileUploader("doc",
								"bankReconciliationDocs",
								"BankReconciliation", reconciliationId,
								"BankReconciliationDocuments");
			} else {
				AIOS_Uploader.openFileUploader("doc",
						"bankReconciliationDocs", "BankReconciliation",
						"-1", "BankReconciliationDocuments");
			}
		}); 
 		 
		$('#reconciliationDate,#statementEndDate').datepick({
			 onSelect: customRange, showTrigger: '#calImg'});  
		
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
		        $('#themeRollerSelect option:not(:selected)').remove();
		}
		 
	});
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	function checkLinkedDays() {
		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}
	function customRange(dates) {
		$('.bank-reconciliation-detail').html('');
		if (this.id == 'reconciliationDate') {
			$('#statementEndDate').datepick('option', 'minDate',
					dates[0] || null);
		} else {
			$('#reconciliationDate').datepick('option', 'maxDate',
					dates[0] || null);
		}
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Presented Cheque
		</div>
		<form name="reconciliationValidation" id="reconciliationValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="reconciliationId" name="reconciliationId"/>
				<div class="width100 float-left" id="hrm">
					<div class="width35 float-left" id="hrm">
						<fieldset style="min-height: 145px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<input type="text" id="referenceNumber"
											class="width50 validate[required]"
											value="${requestScope.referenceNumber}" readonly="readonly" />
							</div>
							<div>
								<label class="width30"> Date<span style="color: red;">*</span>
								</label>
								<input type="text" id="transactionDate"
											class="width50 validate[required]" readonly="readonly" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30">Bank Account<span
									style="color: red;">*</span> </label>
								<input type="text" readonly="readonly"
											name="bankAccountNumber" id="accountNumber"
											class="width50 validate[required]" />
								<input type="hidden" readonly="readonly" name="bankAccountId" id="accountId" />
								<span class="button"
									style="position: relative !important; top: 9px !important; right: 20px; float: right;">
									<a style="cursor: pointer;"
										class="btn ui-state-default ui-corner-all bank-reconciliation-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div>
							<div>
								<label class="width30">Start Date<span
									style="color: red;">*</span>
								</label> <input name="reconciliationDate" type="text" readonly="readonly"
									id="reconciliationDate" class="reconciliationDate validate[required] width50">
							</div>
							<div>
								<label class="width30">End Date<span style="color: red;">*</span>
								</label> <input name="statementEndDate" type="text" readonly="readonly" id="statementEndDate"
									class="statementEndDate validate[required] width50">
							</div>
							<div>
								<label class="width30">Statement Balance<span
									style="color: red;">*</span> </label> <input type="text"
									name="statementBalance" id="statementBalance" class="width50 validate[required] statementBalance" />
							</div>
						</fieldset>
					</div>
					<div class="width35 float-left" id="hrm">
						<fieldset style="min-height: 145px;">

							<div>
								<label class="width30">Statement Ref</label> 
								<input name="statementReference" type="text" id="statementReference" class="width60">
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width60"></textarea>
							</div>
						</fieldset>
					</div>
					<div class="width30 float-left" id="hrm">
						<fieldset style="min-height: 145px;">
							<div>
								<div id="bank_reconciliation_document_information"
									style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
								</div>
								<div
									style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
									<span id="bankReconciliationDocs"></span>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content bank-reconciliation-detail class90"
					id="hrm" style="margin-top: 10px;">  
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div> 
 				<div class="portlet-header ui-widget-header float-right generate"
					id="generate_statement" style="cursor: pointer;">generate</div>
  				<div class="portlet-header ui-widget-header float-right generate"
					id="print_statement" style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.print" />
				</div> 
				<div class="portlet-header ui-widget-header float-right save"
					id="reconciliation_save" style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.save" />
				</div>
				 
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>