 <%@page import="com.aiotech.aios.common.util.DateFormat"%>
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<style>
.ui-button{
position: relative; top: 2px; height: 24px;
}
</style>
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
	receive list
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="plist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${RECEIVE_ALL}" var="bean" varStatus="status">  
	     		<option value="${bean.receiveId}">${bean.receiveNumber}</option>
	     	</c:forEach>
     	</select>  
		<span style="margin-top:2px; cursor: pointer;" class="float-right">
     	<img width="24" height="24" src="images/icons/Glass.png" style="opacity: 0.97;"></span> 	</div> 
 	<div class="float-left width100 receive_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width20 float-left"><span>Receive No</span></li>  
   				<li class="width20 float-left"><span>Date</span></li>  
   				<li class="width30 float-left"><span>Supplier Name</span></li>  
   				<li class="width20 float-left"><span>Site</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${RECEIVE_ALL}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="receive_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.receiveId}" id="receivebyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.receiveNumber}" id="receivenumberbyid_${status.index+1}">  
		     			<span id="receivebynumber_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.receiveNumber}</span> 
		     			<c:set var="date" value="${bean.receiveDate}"/>
		     			<%
		     				String date=DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
		     			%>
		     			<span id="receivebydate_${status.index+1}" style="cursor: pointer;" class="float-left width20"><%=date%></span> 
		     			<span id="receivebysupplier_${status.index+1}" style="cursor: pointer;" class="float-left width30">-----</span>
		     			<span id="receivebysite_${status.index+1}" style="cursor: pointer;" class="float-left width20">-----
		     				<span id="receiveNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:88%;left:92%;">
		<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor: pointer; margin-top: 1px;">close</div>  
		<input type="hidden" name="page" id="page" value="${PAGE}"/>
	</div>
	
 </div> 
 <script type="text/javascript"> 
 $(function(){  
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open');  
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var receivedetails=$(this).val();
		       var receivearray=receivedetails.split("|"); 
		       $('#receiveNumber').val($(".autoComplete option:selected").text());
		       $('#receiveId').val(receivearray[0]);  
		       if($('#page').val()=="debitNote")
		       	  getAllReceives($('#receiveId').val());
		       $('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.receive_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.receive_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#receiveNameselect_'+rowid).hasClass('selected-')){ 
					 $('#receiveNameselect_'+rowid).removeClass('selected-');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#receiveNameselect_'+rowid).addClass('selected-');
		});
	 
	 $('.receive_list_li').live('dblclick',function(){ 
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#receiveId').val($('#receivebyid_'+rowid).val());
		  $('#receiveNumber').val($('#receivenumberbyid_'+rowid).val());  
		  if($('#receiveId').val()>0 && $('#page').val()=="debitNote"){ 
		      getAllReceives($('#receiveId').val()); 
		  }
		  $('#common-popup').dialog("close");   
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 }); 
function getAllReceives(receiveId){ 
	  $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receive_detail.action", 
		 	async: false, 
		 	data:{receiveId:receiveId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
		 		$(".tab").html(result);  
			} 
		}); 
}
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.receive_list ul li{
		padding:3px;
	}
	.receive_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected-{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>