<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fieldset> 
	<legend>Active Delivery Notes</legend>
	<div>
		<c:forEach var="ACTIVE_NOTE" items="${DELIVERY_NOTE_INFO}">
			<label for="activeNoteNumber_${ACTIVE_NOTE.salesDeliveryNoteId}" class="width10">
				<input type="checkbox" name="activeNoteNumber" class="activeNoteNumber" id="activeNoteNumber_${ACTIVE_NOTE.salesDeliveryNoteId}"/>
				${ACTIVE_NOTE.referenceNumber} ${ACTIVE_NOTE.description}
			</label>
		</c:forEach>
	</div>
</fieldset>
<script type="text/javascript">
$(function(){ 
	callDefaultNoteSelect($('.activeNoteNumber').attr('id'));
	$('.activeNoteNumber').click(function(){   
		validateNoteInfo($(this));   
	});
});
</script>