<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
	<script src="fileupload/FileUploader.js"></script>
	
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.ui-button{
 background: #d14836 none repeat scroll 0 0 !important;
 border: 1px solid transparent !important;
 border-radius: 2px !important;
 color: #fff !important;
 margin-left: 4px !important;
 text-shadow: none !important;
 font-size : 0.9em!important;
 font-weight : bold!important;
}.ui-button span{font-weight : bold!important;}
</style>
<script type="text/javascript">
var slidetab="";
var packagingDetails= [];
var tempid = ""; 
var accessCode = "";
var sectionRowId = 0;
$(function(){   
	manupulateLastRow();
	$jquery("#productPackageValidation").validationEngine('attach');  
 
	
	$('.product-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		var rowId = Number(0);  
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_package_popup.action", 
		 	async: false,   
		 	data: {id: rowId, itemType: ''},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	}); 
 	
 	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
 	
 	$('.product-unitlookup').live('click',function(){
        $('.ui-dialog-titlebar').remove(); 
        var str = $(this).attr("id");
        accessCode = str.substring(0, str.lastIndexOf("_"));
        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	
 	
 	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
		 if(accessCode=="PRODUCT_UNITNAME"){
			$('#productUnitId_'+sectionRowId).html("");
			$('#productUnitId_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("productUnitId_"+sectionRowId); 
		}   
	});
 	
 	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/product_packaging_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 
 	 
 	
 	 $('#packaging_discard').click(function(event){  
 		packagingDiscard("");
		return false;
	 });
 	 
 	 $('#packaging_save').click(function(){
 		packagingDetails = [];
		 if($jquery("#productPackageValidation").validationEngine('validate')){ 
	 			var referenceNumber = $('#referenceNumber').val(); 
	 			var packageName = $.trim($('#packageName').val());
 	 			var productId = Number($('#productId').val()); 
	 			var barcode = $('#barcode').val();   
	 			var length = Number($('#length').val());  
	 			var width = Number($('#width').val()); 
	 			var height = Number($('#height').val()); 
	 			var productPackageId = Number($('#productPackageId').val());   
	 			var netWeight= Number($('#netWeight').val());  
	 			var grossWeight = Number($('#grossWeight').val());  
	 			packagingDetails = getPackagingDetails();  
	   			if(packagingDetails!=null && packagingDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/product_package_save.action", 
	 				 	async: false, 
	 				 	data:{	productId: productId, referenceNumber: referenceNumber, barcode: barcode, length: length, width: width, 
	 				 			grossWeight: grossWeight, packageName: packageName, height: height, productPackageId: productPackageId, netWeight: netWeight,
	 				 			packagingDetails: JSON.stringify(packagingDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							packagingDiscard(productPackageId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown();
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter requisition details.").slideDown();
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 
 	 
 	  var getPackagingDetails = function(){
 		 packagingDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productUnitId = Number($('#productUnitId_'+rowId).val());  
				var quantity = Number($('#quantity_'+rowId).val()); 
				var productPackageDetailId =  Number($('#productPackageDetailId_'+rowId).val()); 
				if(typeof productUnitId != 'undefined' && productUnitId > 0 && quantity > 0){
					var jsonData = [];
					jsonData.push({
						"productUnitId" : productUnitId,
						"quantity" : quantity,  
						"productPackageDetailId": productPackageDetailId
					});   
					packagingDetails.push({
						"packagingDetails" : jsonData
					});
				} 
			});  
			return packagingDetails;
	 };  
	 
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
 	 
 	 
 	$('.quantity,productUnitId').live('change',function(){
 		var rowId = getRowId($(this).attr('id'));  
 		triggerAddRow(rowId);
 		return false;
	 }); 
 	
 	$('.product-unitlookup').live('click',function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result-product').html(result);
                  $('#common-popup-product').dialog('open');
  				   $($($('#common-popup-product').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result-product').html(result);
             }
         });
          return false;
 	});
	
 	if(Number($('#productPackageId').val()) > 0){
		 $('.rowid').each(function(){ 
			 var rowId = getRowId($(this).attr('id'));  
			 $('#productUnitId_'+rowId).val($('#tempproductUnitId_'+rowId).val());
		 }); 
	 } 
});	

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}

 
function commonProductPopup(productId, productName, rowId, itemSubType, code, unitName) {  
	$('#productId').val(productId);
	$('#productName').val(productName);   
	$('.baseUnitName').text(unitName);
	return false;
}
 
function triggerAddRow(rowId){  
	var productQty = Number($('#quantity_'+rowId).val());  
	var productUnitId = Number($('#productUnitId_'+rowId).val());  
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productUnitId > 0
			&& productQty > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
	return false;
}
function packagingDiscard(message){ 
	 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_product_packaging.action", 
 		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();  
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> product packaging
		</div> 
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		<form name="productPackageValidation" id="productPackageValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<input type="hidden" id="productPackageId" value="${PACKAGE_INFO.productPackageId}"/>
 				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 100px;"> 
							<div>
								<label class="width30">Barcode  </label> <input type="text"
									name="barcode" id="barcode" class="width50" value="${PACKAGE_INFO.barcode}" /> 
							</div> 
							 <div>
								<label class="width30">Net Weight  </label> <input type="text"
									name="netWeight" id="netWeight" class="width50" value="${PACKAGE_INFO.netWeight}" /> 
							</div> 	
							 <div>
								<label class="width30">Gross Weight  </label> <input type="text"
									name="grossWeight" id="grossWeight" class="width50"	value="${PACKAGE_INFO.grossWeight}" /> 
							</div> 	
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${PACKAGE_INFO.referenceNumber ne null && PACKAGE_INFO.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${PACKAGE_INFO.referenceNumber}" 	class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>  
							<div>
								<label class="width30">Package Name <span
									style="color: red;">*</span> </label>  <input type="text"
									name="packageName" id="packageName" class="width50" value="${PACKAGE_INFO.packageName}" /> 
							</div>
							<div>
								<label class="width30">Product<span
									style="color: red;">*</span> </label> <input type="text"
									readonly="readonly" name="productName" id="productName"
									class="width50 validate[required]"
									value="${PACKAGE_INFO.product.productName}" />
								<span class="button">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all product-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="productId"
									id="productId"
									value="${PACKAGE_INFO.product.productId}" />
								 <span class="baseUnitName">${PACKAGE_INFO.product.lookupDetailByProductUnit.displayName}</span>
							</div>  
							<div>
								<label class="width30">Dimension (L*W*H)  </label> <input type="text"
									name="length" id="length" class="width15 validate[optional,custom[number]]" value="${PACKAGE_INFO.length}" /> 
									<input type="text"
									name="width" id="width" class="width15 validate[optional,custom[number]]" value="${PACKAGE_INFO.width}" /> 
									<input type="text"
									name="height" id="height" class="width15 validate[optional,custom[number]]" value="${PACKAGE_INFO.height}" /> 
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Packaging Conversions<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width60">
								<thead>
									<tr>
										<th class="width10">Unit</th>
										<th class="width5">Quantity</th> 
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${PACKAGE_INFO.productPackageDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td>
												<select name="productUnitId" id="productUnitId_${status.index+1}" class="width70 productUnitId">
													<option value="">Select</option>
													<c:forEach var="productUnit" items="${PRODUCT_UNITS}">
														<option value="${productUnit.lookupDetailId}">${productUnit.displayName}</option>
													</c:forEach>
												</select>
												<input type="hidden" id="tempproductUnitId_${status.index+1}" value="${DETAIL.lookupDetail.lookupDetailId}"/>
											 <span
												class="button float-right"> <a
													style="cursor: pointer;" id="PRODUCT_UNITNAME_${status.index+1}"
													class="btn ui-state-default ui-corner-all product-unitlookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="text" name="quantity"
												id="quantity_${status.index+1}" value="${DETAIL.unitQuantity}"
												class="quantity validate[optional,custom[number]] width80">
											</td>   
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productPackageDetailId"
												id="productPackageDetailId_${status.index+1}" value="${DETAIL.productPackageDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(PACKAGE_INFO.productPackageDetailVOs)+1}"
										end="${fn:length(PACKAGE_INFO.productPackageDetailVOs)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td>
												<select name="productUnitId" id="productUnitId_${i}" class="width70 productUnitId">
													<option value="">Select</option>
													<c:forEach var="productUnit" items="${PRODUCT_UNITS}">
														<option value="${productUnit.lookupDetailId}">${productUnit.displayName}</option>
													</c:forEach>
												</select>
											 <span
												class="button float-right"> <a
													style="cursor: pointer;" id="PRODUCT_UNITNAME_${i}"
													class="btn ui-state-default ui-corner-all product-unitlookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="text" name="quantity"
												id="quantity_${i}"
												class="quantity validate[optional,custom[number]] width80">
											</td>  
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productPackageDetailId"
												id="productPackageDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="packaging_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="packaging_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>  
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>