<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
		Supplier Information
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="supplierlist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${SUPPLIER_INFO}" var="bean" varStatus="status">  
	     		<option value="${bean.supplierId}|${bean.supplierNumber}|${bean.supplierName}|${bean.creditTermId}|${bean.combinationId}">${bean.supplierName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer;" class="float-right">
     	<img width="24" height="24" src="images/icons/Glass.png" style="opacity: 0.97;"></span>
 	</div> 
 	
 	<div class="float-left width100 supplier_list" style="padding:2px;">  
 		<input type="hidden" value="${requestScope.PAGE_INFO}" id="pageInfo"/>
	     	<ul> 
   				<li class="width30 float-left"><span>Supplier Number</span></li>  
   				<li class="width30 float-left"><span>Name</span></li>  
   				<li class="width30 float-left"><span>Type</span></li>   
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${SUPPLIER_INFO}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="supplier_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.supplierId}" id="supplierbyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.supplierNumber}" id="supplierbynumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.supplierName}" id="supplierbynameid_${status.index+1}">
		     			<input type="hidden" value="${bean.creditTermId}" id="supplierbytermid_${status.index+1}">
		     			<input type="hidden" value="${bean.combinationId}" id="supplierbycombinationid_${status.index+1}">
		     			<span id="supplierbynumber_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.supplierNumber}</span>
		     			<span id="supplierbyname_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.supplierName}</span>
		     			 <span id="supplierbytype_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.type}
		     				<span id="supplierNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:88%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor: pointer; margin-top: 1px;">close</div>  
	</div>
 	
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',260);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',293);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){   
	    	   var supplierdetails=$(this).val();
		       var supplierarray=supplierdetails.split("|"); 
		       $('#supplierName').val($('.autoComplete option:selected').text());
		       $('#supplierNumber').val(supplierarray[1]); 
		       $('#supplierId').val(supplierarray[0]);
		       $('#termId').val(supplierarray[3]); 
		       $('#supplierCombinationId').val(supplierarray[4]);
		       if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="receive") 
					showActivePurchaseOrder($('#supplierId').val());
		       else if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="invoice")
			       showActiveReceiveNotes($('#supplierId').val()); 
		       else if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="payments")
			       showActiveInvoice($('#supplierId').val()); 
		       $('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.supplier_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.supplier_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#supplierNameselect_'+rowid).hasClass('selected')){ 
					 $('#supplierNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#supplierNameselect_'+rowid).addClass('selected');
			return false;
		});
	 
	 $('.supplier_list_li').live('dblclick',function(){  
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#supplierId').val($('#supplierbyid_'+rowid).val()); 
		  $('#supplierNumber').val($('#supplierbynumberid_'+rowid).val()); 
		  $('#supplierName').val($('#supplierbynameid_'+rowid).val()); 
		  $('#termId').val($('#supplierbytermid_'+rowid).val());
		  $('#supplierCombinationId').val($('#supplierbycombinationid_'+rowid).val()); 
		  if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="receive") 
				showActivePurchaseOrder($('#supplierId').val());
		  else if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="invoice")
		       showActiveReceiveNotes($('#supplierId').val()); 
		  else if($('#pageInfo').val()!=null && $('#pageInfo').val()!='' && $('#pageInfo').val()=="payments")
		       showActiveInvoice($('#supplierId').val()); 
		  $('#common-popup').dialog('close');  
		  return false;
	  });
	 $('#close').click(function(){
		 $('#common-popup').dialog('close'); 
	 });  
 }); 
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.supplier_list ul li{
		padding:3px;
	}
	.supplier_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
	.ui-button{
position: relative; top: 2px; height: 24px;
}
</style>