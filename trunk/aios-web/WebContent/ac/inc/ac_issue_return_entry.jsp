<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<script type="text/javascript">
var slidetab="";
var tempid = "";
var sectionRowId = 0;
var accessCode = "";
var requisitionDetails = "";
var transferDetails = "";
$(function(){   
	$jquery("#issueReturnValidation").validationEngine('attach');  

	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.editReturnData').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var issueRequistionId = $('#issueRequistionId_'+rowId).val();  
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_issue_requistion_details.action", 
		 	async: false,  
		 	data:{issueRequistionId: issueRequistionId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click');   
			} 
		});  
		return false;
	});
	
	$('.editTransferReturnData').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
    	 var materialTransferId = $('#materialTransferId_'+rowId).val();  
    	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_transfer_details.action", 
		 	async: false,  
		 	data:{materialTransferId: materialTransferId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click');   
			} 
		});  
		return false;
	});

	$('.material-condition-lookup').live('click',function(){
        $('.ui-dialog-titlebar').remove(); 
        var str = $(this).attr("id");
        accessCode = str.substring(0, str.lastIndexOf("_"));
        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
        $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				  $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="MATERIAL_CONDITION"){
			$('#condition_'+sectionRowId).html("");
			$('#condition_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("condition_"+sectionRowId); 
		} 
	});
	$('.delrowReturn').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
     	 var deleteId = getRowId($(slidetab).attr('id'));
     	 var deleteIssueId = $('#issueRequistionId_'+deleteId).val(); 
     	 $('.activeReturnNumber').each(function(){   
     		var issueId = getRowId($(this).attr('id')); 
     		 if(deleteIssueId == issueId){
				$(this).attr('checked',false);
				 return false;
			 }
     	 });
     	 $(slidetab).remove();  
     	 var i=1;
  	 	 $('.rowid').each(function(){   
  			 var rowId=getRowId($(this).attr('id')); 
  		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
  	 	if($('.tab tr').size()==0)
			$('.return_lines').hide();
		return false;
	 }); 

	$('.delrow_rt').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deleteIssueId = $('#issueRequistionID_'+idval).val();
		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidrt').each(function(){   
  			 var rowId=getRowId($(this).attr('id')); 
  		 	 $('#lineIdrt_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-1 tr').size()==0){ 
			 $('.activeReturnNumber').each(function(){   
	      		var issueId = getRowId($(this).attr('id'));  
	      		 if(deleteIssueId == issueId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId=getRowId($(this).attr('id')); 
			   		 	 var pageIssueId = $('#issueRequistionId_'+rowId).val();
			   		 	 if(pageIssueId == issueId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.return_lines').hide();
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 } 
		 return false;
	 });
	
	$('.delrow_rtntrns').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deleteTransferId = $('#materialTransferID_'+idval).val();
		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidrt').each(function(){   
 			 var rowId=getRowId($(this).attr('id')); 
 		 	 $('#lineIdrt_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-1 tr').size()==0){ 
			 $('.activeReturnNumber').each(function(){   
	      		var transferId = getRowId($(this).attr('id'));  
	      		 if(deleteTransferId == transferId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId = getRowId($(this).attr('id')); 
			   		 	 var pageTransferId = $('#materialTransferId_'+rowId).val();
			   		 	 if(pageTransferId == transferId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.return_lines').hide();
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 } 
		 return false;
	 });

	$('.returncloseDom,.returnTransferCloseDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
	});

	$('#department-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
 	});
	
	$('.sessionTransferReturnSave').live('click',function(){ 
		 transferDetails = getTransferDetails();   
		 var rowid = 1;
		 var materialTransferId = $('#materialTransferID_'+rowid).val();
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_material_transfer_session.action", 
		 	async: false,  
		 	data:{materialTransferId: materialTransferId, transferDetails: transferDetails},
		    dataType: "json",
		    cache: false,
			success:function(response){  
				 $('.tempresult').html(response);  
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
			} 
		});  
		return false;
	 });
	
	var getTransferDetails = function(){ 
		 var transferArray = new Array();
		 var returnArray = new Array();
		 var descArray = new Array();
		 var transferDetailArray = new Array();  
		 var conditionArray = new Array(); 
		 transferDetails = "";
		 $('.rowidrt').each(function(){
			 var rowid = getRowId($(this).attr('id'));  
			 var returnQty =  Number($('#returnQty_'+rowid).val());  
			 var materialTransferId = $('#materialTransferID_'+rowid).val();
			 var materialTransferDetailId = $('#materialTransferDetailId_'+rowid).val();
			 var description = $('#linedescription_'+rowid).val(); 
			 var materialCondition = Number($('#condition_'+rowid).val()); 
			 if(typeof materialTransferDetailId != 'undefined' && materialTransferDetailId!=null && materialTransferDetailId != ""){ 
				returnArray.push(returnQty);
				transferDetailArray.push(materialTransferDetailId);
				transferArray.push(materialTransferId);
				 if(description!=null && description!="")
					 descArray.push(description);
				 else
					 descArray.push("##");  
				 conditionArray.push(materialCondition);
			 }
		 });
		 for(var j=0;j<transferDetailArray.length;j++){ 
			 transferDetails += returnArray[j]+"__"+transferDetailArray[j]+"__"+descArray[j]+"__"+transferArray[j]+"__"+conditionArray[j];
				if(j!=transferDetailArray.length-1){   
					transferDetails += "#@";
				} 
			} 
			return transferDetails;
	 };
	
	$('.sessionReturnSave').live('click',function(){ 
		 requisitionDetails = getRequisitionDetails();   
		 var rowid = 1;
		 var issueRequistionId = $('#issueRequistionID_'+rowid).val();
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_issue_requisition_session.action", 
		 	async: false,  
		 	data:{issueRequistionId: issueRequistionId, requistionDetail: requisitionDetails},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.tempresult').html(result);  
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
			} 
		});  
		return false;
	 });

	 var getRequisitionDetails = function(){ 
		 var requisitionArray = new Array();
 		 var returnArray = new Array();
		 var descArray = new Array();
		 var requistionDetailArray = new Array();  
		 var conditionArray = new Array(); 
 		 requisitionDetails = "";
		 $('.rowidrt').each(function(){
			 var rowid = getRowId($(this).attr('id'));  
			 var returnQty =  Number($('#returnQty_'+rowid).val());  
			 var issueRequistionId = $('#issueRequistionID_'+rowid).val();
			 var issueRequistionDetailId = $('#issueRequistionDetailId_'+rowid).val();
			 var description = $('#linedescription_'+rowid).val(); 
			 var materialCondition = Number($('#condition_'+rowid).val()); 
 			 if(typeof issueRequistionDetailId != 'undefined' && issueRequistionDetailId!=null && issueRequistionDetailId!=""){ 
 				returnArray.push(returnQty);
 				requistionDetailArray.push(issueRequistionDetailId);
 				requisitionArray.push(issueRequistionId);
				 if(description!=null && description!="")
					 descArray.push(description);
				 else
					 descArray.push("##");  
				 conditionArray.push(materialCondition);
			 }
		 });
		 for(var j=0;j<requistionDetailArray.length;j++){ 
			 requisitionDetails+=returnArray[j]+"__"+requistionDetailArray[j]+"__"+descArray[j]+"__"+requisitionArray[j]+"__"+conditionArray[j];
				if(j==requistionDetailArray.length-1){   
				}
				else{
					requisitionDetails+="#@";
				}
			} 
			return requisitionDetails;
	 };

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_issue_returns.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });

	 $('#return_save').click(function(){
		 if($jquery("#issueReturnValidation").validationEngine('validate')){
 		 		
	 			var issueReturnId = Number($('#issueReturnId').val()); 
	 			var referenceNumber = $('#tempreferenceNumber').val();
	 			var returnDate = $('#returnDate').val();  
	 			var description = $('#description').val();  
	 			var locationId = Number($('#locationId').val());  
	 			var storeId = Number($('#storeId').val());  
	 			var requisitionArray = new Array();
				$('.activeReturnNumber').each(function(){
					if($(this).attr('checked')){ 
						 requisitionArray.push(getRowId($(this).attr('id')));
					 }
				}); 
				requisitionDetails = "";
				for(var i=0; i<requisitionArray.length;i++){
					if (i == requisitionArray.length - 1) {
						requisitionDetails += requisitionArray[i];
					} else {
						requisitionDetails += "#@";
					} 
				 } 
	   			 if(requisitionDetails != null && requisitionDetails !="" ){ 
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/save_issue_returns.action", 
	 				 	async: false, 
	 				 	data:{	issueReturnId: issueReturnId, referenceNumber: referenceNumber, returnDate: returnDate, cmpDeptLocationId: locationId,
	 				 			storeId: storeId, description: description, returnDetail: requisitionDetails
	 					 	 },
	 				    dataType: "html",
	 				    cache: false,
	 					success:function(result){   
	 						 $(".tempresult").html(result);
	 						 var message=$.trim($('.tempresult').html());  
	 						 if(message=="SUCCESS"){
	 							 $.ajax({
	 									type:"POST",
	 									url:"<%=request.getContextPath()%>/show_issue_returns.action", 
	 								 	async: false,
	 								    dataType: "html",
	 								    cache: false,
	 									success:function(result){
	 										$('#common-popup').dialog('destroy');		
	 										$('#common-popup').remove();  
	 										$('#DOMWindow').remove();
	 										$('#DOMWindowOverlay').remove();
	 										$("#main-wrapper").html(result); 
	 										if(issueReturnId==0)
	 											$('#success_message').hide().html("Record created.").slideDown(1000);
	 										else
	 											$('#success_message').hide().html("Record updated.").slideDown(1000);
	 										$('#success_message').delay(2000).slideUp();
	 									}
	 							 });
	 						 } 
	 						 else{
	 							 $('#page-error').hide().html(message).slideDown(1000);
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					},
	 					error:function(result){  
	 						$('#page-error').hide().html("Internal error.").slideDown(1000);
	 						$('#page-error').delay(2000).slideUp();
	 					}
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter return details.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 
	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

 	$('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		tempid = $(this).parent().get(0);   
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
	
	$('#storeId').change(function(){   
	 	var storeId = Number($(this).val());
	 	if(storeId > 0){
	 		$('.return_lines').hide();
	 		$('.locaction-div').hide();
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/material_transfer_bystore_list.action", 
			 	async: false, 
			 	data : {storeId: storeId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.return-info').html(result);   
					 if(typeof($('.activeReturnNumber').attr('id'))=="undefined"){   
						$('.return_lines').hide();
						$('.tab').html('');
						$('.return-info').find('div').css("color","#ca1e1e").html("No Transfer found in this store.");
					 } 
					 return false;
				} 
			}); 
	 	 }
	 	return false;
	 });
	 
 	 $('.productQty').live('change',function(){   
 		triggerAddRow(getRowId($(this).attr('id')));
 		return false;
 	 });

 	$('.amount').live('change',function(){ 
 		triggerAddRow(getRowId($(this).attr('id')));
 		return false;
 	 }); 

 	{
 		if (Number($('#issueReturnId').val()) > 0) {
 	 		$('#returnDate').datepick({
 	 			showTrigger: '#calImg'}); 
 	 	} else{
 	 		$('#returnDate').datepick({ 
 			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
 	 	}
 	} 
 	 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function callDefaultTransferSelect(materialTransferObj) {
	if (typeof materialTransferObj != 'undefined') {
		var id= Number(1); 
		var materialTransferId = getRowId(materialTransferObj); 
		$('#'+materialTransferObj).attr('checked',true); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_transfer_info.action", 
		 	async: false,  
		 	data:{materialTransferId: materialTransferId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.return_transfer_lines').show();
				$('.tab').html(result);  
				return false;
			} 
		});  
	}
	return false;
}
function validateTransferInfo(currentObj) {
	var materialTransferId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.return_transfer_lines').show();
		} else {
			$('.return_transfer_lines').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_transfer_info.action",
						async : false,
						data : {
							materialTransferId : materialTransferId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
							return false;
						}
					});
		} else {
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var removeMaterialTransferId = $(
								'#materialTransferId_' + rowId).val();
						if (removeMaterialTransferId == materialTransferId) {
							$('#fieldrow_' + rowId).remove();
						}
					});
			if ($('.tab tr').size() == 0)
				$('.return_transfer_lines').hide();
		}
		return false;
}
function departmentBranchPopupResult(locationId,locationName){
	$('.store-div').hide();
	$('.return_transfer_lines').hide();
	$('#locationId').val(locationId);
	$('#locationName').val(locationName);
	showRequistionByLocation(locationId);
}
function showRequistionByLocation(locationId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_issue_requistion_location.action", 
	 	async: false,  
	 	data:{locationId: locationId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.return-info').html(result);   
			 if(typeof($('.activeReturnNumber').attr('id'))=="undefined"){   
				$('.return_lines').hide();
				$('.tab').html('');
				$('.return-info').find('div').css("color","#ca1e1e").html("No Requisition found in this department.");
			 } 
			 return false;
		} 
	});  
	return false;
} 
function callDefaultIssueSelect(issueRequisitionObj) {
	if (typeof issueRequisitionObj != 'undefined') {
		var id= Number(1); 
		var issueRequistionId = getRowId(issueRequisitionObj); 
		$('#'+issueRequisitionObj).attr('checked',true); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_issuedetails_info.action", 
		 	async: false,  
		 	data:{issueRequistionId: issueRequistionId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.return_lines').show();
				$('.tab').html(result);  
				return false;
			} 
		});  
	}
	return false;
}
function validateIssueInfo(currentObj) {
	var issueRequistionId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.return_lines').show();
		} else {
			$('.return_lines').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_issuedetails_info.action",
						async : false,
						data : {
							issueRequistionId : issueRequistionId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
							return false;
						}
					});
		} else {
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var removeissueRequisitionId = $(
								'#issueRequistionId_' + rowId).val();
						if (removeissueRequisitionId == issueRequistionId) {
							$('#fieldrow_' + rowId).remove();
						}
					});
			if ($('.tab tr').size() == 0)
				$('.return_lines').hide();
		}
		return false;
	}

function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Issue
			Returns
		</div> 
		<form name="issueReturnValidation" id="issueReturnValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="issueReturnId" name="issueReturnId"
					value="${ISSUE_RETURN.issueReturnId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 100px;"> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" />
								</label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${ISSUE_RETURN.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 100px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span>
								</label> <span style="font-weight: normal;" id="referenceNumber">
									<c:choose>
										<c:when
											test="${ISSUE_RETURN.referenceNumber ne null && ISSUE_RETURN.referenceNumber ne ''}">
												${ISSUE_RETURN.referenceNumber} 
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${ISSUE_RETURN.referenceNumber}" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Return Date<span
									style="color: red;">*</span>
								</label>
								<c:choose>
									<c:when
										test="${ISSUE_RETURN.returnDate ne null && ISSUE_RETURN.returnDate ne ''}">
										<c:set var="returnDate" value="${ISSUE_RETURN.returnDate}" />
										<input name="returnDate" type="text" readonly="readonly"
											id="returnDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("returnDate").toString())%>"
											class="returnDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="returnDate" type="text" readonly="readonly"
											id="returnDate" class="returnDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
							<div class="locaction-div">
								<label class="width30">Location</label>
								<c:choose>
									<c:when
										test="${ISSUE_RETURN.cmpDeptLocation ne null && ISSUE_RETURN.cmpDeptLocation ne ''}">
										<input type="text" readonly="readonly" name="locationName"
											id="locationName" class="width50"
											value="${ISSUE_RETURN.cmpDeptLocation.location.locationName}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="locationName"
											id="locationName" class="width50" />
									</c:otherwise>
								</c:choose>
								<c:if
									test="${ISSUE_RETURN.issueReturnId eq null || ISSUE_RETURN.issueReturnId eq 0}">
									<span class="button">
										<a style="cursor: pointer;" id="location"
										class="btn ui-state-default ui-corner-all common-popup width100">
											<span class="ui-icon ui-icon-newwin"></span> </a> </span>
								</c:if>
								<input type="hidden" readonly="readonly" name="locationId"
									id="locationId"
									value="${ISSUE_RETURN.cmpDeptLocation.cmpDeptLocId}" />
							</div>
							<div class="store-div">
								<label class="width30">Store</label> 
								<c:choose>
									<c:when test="${ISSUE_RETURN.issueReturnId eq null || ISSUE_RETURN.issueReturnId eq 0}">
										<select name="storeName"
											id="storeId" class="width50">
											<option value="">Select</option>
											<c:forEach var="store" items="${STORE_DETAILS}">
												<option value="${store.storeId}">${store.storeName}</option>
											</c:forEach>
										</select>
									</c:when>
									<c:otherwise>
										<input type="text" name="storeName"
											id="storeName" class="width50" value="${ISSUE_RETURN.store.storeName}"/>
										<input type="text" name="storeId"
											id="storeId" class="width50" value="${ISSUE_RETURN.store.storeId}"/>
									</c:otherwise>
								</c:choose> 
							</div>
						</fieldset>
					</div>

				</div>
				<div class="clearfix"></div>
				<div class="return-info" id="hrm">
					<c:if test="${ISSUE_RETURN ne null && ISSUE_RETURN ne ''}">
						<fieldset>
							<legend>Issue Returns</legend>
							<div>
								<c:forEach var="ACTIVE_RETURNS" items="${ISSUE_RETURNS_INFO}">
									<label
										for="activeReturnNumber_${ACTIVE_RETURNS.issueRequistionId}"
										class="width10" id="returnLabel_${ACTIVE_RETURNS.referenceNo}">
										<input type="checkbox" name="activeReturnNumber"
										class="activeReturnNumber activeReturnNumberUpdate"
										id="activeIssueNumber_${ACTIVE_RETURNS.issueRequistionId}" />
										${ACTIVE_RETURNS.referenceNo} </label>
								</c:forEach>
							</div>
						</fieldset>
					</c:if>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90 return_transfer_lines" id="hrm"
					style="margin-top: 10px; display: none;">
					<fieldset>
						<legend>Transfer Return Detail<span
									class="mandatory">*</span></legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%;">Transfer No.</th> 
										<th style="width: 5%;">Transfer Date</th>
										<th style="width: 5%;">Transfer Quantity</th>
										<th style="width: 5%;">Total Amount</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tab"> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
				<div class="portlet-content class90 return_lines" id="hrm"
					style="margin-top: 10px; display: none;">
					<fieldset>
						<legend>Issue Return Detail<span
									class="mandatory">*</span></legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%;">Requisition No.</th> 
										<th style="width: 5%;">Issue Date</th>
										<th style="width: 5%;">Issued Quantity</th>
										<th style="width: 5%;">Total Amount</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" />
										</th>
									</tr>
								</thead>
								<tbody class="tab"> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div id="temp-result" style="display: none;"></div>
			<span class="callJq"></span>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="return_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span>
					</a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>