<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<fieldset style="height: auto !important;">
	<legend>Shelf Information</legend>
	<input type="hidden" id="binReferenceId"
		value="${requestScope.binReferenceId}" /> <input type="hidden"
		id="sectionReferenceId" value="${requestScope.sectionReferenceId}" />
	<div class="portlet-content">
		<div id="rack-page-error"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<div id="hrm" class="hastable width100">
			<table id="rack-hastab" class="width100">
				<thead>
					<tr>
						<th style="width: 3%">Shelf Name</th>
						<th style="width: 3%">Shelf Type</th>
						<th style="width: 3%">Dimension (l*b*h)</th>
						<th style="width: 0.3%">Options</th>
					</tr>
				</thead>
				<tbody class="racktab">
					<c:forEach var="rackDetail" items="${RACK_DETAILS}"
						varStatus="status">
						<tr class="rackrowid temprackrowid"
							id="rackfieldrow_${status.index+1}">
							<td><input type="text" class="width90 shelfName" value="${rackDetail.name}"
									name="shelfName" id="shelfName_${status.index+1}" />
							</td>
							<td style="display: none;" id="racklineId_${status.index+1}">${status.index+1}</td>
							<td><select name="rackType" id="rackType_${status.index+1}"
								class="width90">
									<option value="">Select</option>
									<c:forEach var="rackType" items="${RACK_TYPES}">
										<option value="${rackType.key}">${rackType.value}</option>
									</c:forEach>
							</select> <input type="hidden" id="temprackType_${status.index+1}"
								value="${rackDetail.shelfType}" /></td>
							<td><input type="text" class="width90 rackdimension"
								style="border: 0px;" name="rackdimension"
								id="rackdimension_${status.index+1}"
								value="${rackDetail.dimension}" /></td>
							<td style="width: 0.01%;" class="opn_td"
								id="option_${status.index+1}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackaddData"
								id="RackAddImage_${status.index+1}"
								style="display: none; cursor: pointer;" title="Add Record">
									<span class="ui-icon ui-icon-plus"></span> </a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackeditData"
								id="RackEditImage_${status.index+1}"
								style="display: none; cursor: pointer;" title="Edit Record">
									<span class="ui-icon ui-icon-wrench"></span> </a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackdelrow"
								id="RackDeleteImage_${status.index+1}" style="cursor: pointer;"
								title="Delete Record"> <span
									class="ui-icon ui-icon-circle-close"></span> </a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
								id="WorkingImage_${status.index+1}" style="display: none;"
								title="Working"> <span class="processing"></span> </a> <input
								type="hidden" name="rackid" value="${rackDetail.shelfId}"
								id="rackid_${status.index+1}" /> <input type="hidden"
								name="rackaisleId" value="${rackDetail.aisle.aisleId}"
								id="rackaisleId_${status.index+1}" /></td>
						</tr>
					</c:forEach>
					<c:forEach var="i" begin="${fn:length(RACK_DETAILS)+1}"
						end="${fn:length(RACK_DETAILS)+2}" step="1">
						<tr class="rackrowid" id="rackfieldrow_${i}">
							<td style="display: none;" id="racklineId_${i}">${i}</td>
							<td><input type="text" class="width90 shelfName"
									name="shelfName" id="shelfName_${i}" />
							</td>
							<td><select name="rackType" id="rackType_${i}"
								class="width90">
									<option value="">Select</option>
									<c:forEach var="rackType" items="${RACK_TYPES}">
										<option value="${rackType.key}">${rackType.value}</option>
									</c:forEach>
							</select>
							</td>
							<td><input type="text" class="width90 rackdimension"
								style="border: 0px;" name="rackdimension"
								id="rackdimension_${i}" /></td>
							<td style="width: 0.01%;" class="opn_td" id="option_${i}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackaddData"
								id="RackAddImage_${i}" style="display: none; cursor: pointer;"
								title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
							</a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackeditData"
								id="RackEditImage_${i}" style="display: none; cursor: pointer;"
								title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
							</a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip rackdelrow"
								id="RackDeleteImage_${i}"
								style="display: none; cursor: pointer;" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span> </a> <a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
								id="WorkingImage_${i}" style="display: none;" title="Working">
									<span class="processing"></span> </a> <input type="hidden"
								name="rackid" value="0" id="rackid_${i}" /> <input
								type="hidden" name="rackaisleId" value="0"
								id="rackaisleId_${i}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</fieldset>
<div class="clearfix"></div>
<div style="display: none;"
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
	<div class="portlet-header ui-widget-header float-left rackaddrows"
		style="cursor: pointer;">
		<fmt:message key="accounts.common.button.addrow" />
	</div>
</div>
<div
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
	style="margin: 10px;">
	<div class="portlet-header ui-widget-header float-right backToBin"
		style="cursor: pointer;">Back To Rack</div>
	<div class="portlet-header ui-widget-header float-right rack_save"
		style="cursor: pointer;">
		<fmt:message key="accounts.common.button.save" />
	</div>
</div>