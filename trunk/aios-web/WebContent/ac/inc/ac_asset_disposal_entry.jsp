<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var popval=""; 
$(function(){    
	$jquery("#asset_disposal").validationEngine('attach'); 

	$('#disposalDate').datepick();

	 $('#disposal_discard').click(function(){ 
		 showAssetDisposal("");
		 return false; 
	 });

	 $('#disposal_save').click(function(){   
		 if($jquery("#asset_disposal").validationEngine('validate')){
			var assetDisposalId = Number($('#assetDisposalId').val());   
			var referenceNumber = $('#referenceNumber').val();
			var assetId = Number($('#assetCreationId').val());
			var saleValue = Number($('#saleValue').val());
			var disposalMethod = Number($('#disposalMethod').val());
			var disposalType = Number($('#disposalType').val());
			var customerId = Number($('#customerId').val());
 	 		var disposalDate = $('#disposalDate').val();  
	 		var description = $('#description').val();
	 		var removalExpense = Number($('#customerd').val());
	 		var tradeAllowance = Number($('#customerd').val());
	 		var otherExpense = Number($('#otherExpense').val());
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_asset_disposal.action", 
				 	async: false,
				 	data:{
				 			assetId: assetId, assetDisposalId: assetDisposalId, referenceNumber: referenceNumber, description: description,
				 			saleValue: saleValue, disposalDate: disposalDate, disposalMethod: disposalMethod, disposalType: disposalType,
				 			customerId: customerId, removalExpense: removalExpense, tradeAllowance: tradeAllowance, otherExpense: otherExpense
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){  
						 if(response.returnMessage == "SUCCESS"){
							 showAssetDisposal(assetDisposalId > 0 ? "Record updated successfully."
										: "Record created successfully.");
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 return false;
						 }
					} 
			 }); 
		 }else{return false;}
	 }); 
	  
	 $('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		popval = $(this).attr('id');  
		var actionValue = "";
		if(popval == "asset")
			actionValue = "show_common_asset_popup"; 
		else
			actionValue = "show_customer";
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionValue+".action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 

	if(Number($('#assetDisposalId').val())>0){
		$('#disposalMethod').val("${ASSET_DISPOSAL.disposalMethod}"); 
		$('#disposalType').val("${ASSET_DISPOSAL.disposalType}"); 
	} 
	
});
function showAssetDisposal(message){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_disposal.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('destroy');
				$('#common-popup').remove();
				$("#main-wrapper").html(result);
				if (message != null && message != '') {
					$('#success_message').hide().html(message).slideDown(1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationId').val(assetId);
	$('#assetCreationName').val(assetName); 
	var disposalDate = $('#disposalDate').val();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_asset_value_detail.action",
					async : false,
					dataType : "json",
					data : {
						assetCreationId : assetId,
						commenceDate : disposalDate
					},
					cache : false,
					success : function(response) {

					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			disposal
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general
			information
		</div>
		<form name="asset_disposal" id="asset_disposal" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetDisposalId" name="assetDisposalId"
					value="${ASSET_DISPOSAL.assetDisposalId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 180px;">
							<div>
								<label class="width30" for="saleValue">Sale Value<span
									class="mandatory">*</span> </label> <input type="text" name="saleValue"
									id="saleValue"
									class="width50 validate[required,custom[number]]"
									readonly="readonly"
									value="${ASSET_DISPOSAL.receiptsDetail.amount}" /> <input
									type="hidden" id="receiptsId"
									value="${ASSET_DISPOSAL.receipts.receiptsId}" /> <span
									class="button"> <a style="cursor: pointer;"
									id="receipts"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="removalExpense">Removal
									Expense<span class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="removalExpense"
									value="${ASSET_DISPOSAL.directPaymentDetail.amount}"
									class="width50" /> <input type="hidden" id="removalExpenseId"
									value="${ASSET_DISPOSAL.directPaymentDetail.directPaymentDetailId}" />
								<span class="button"> <a style="cursor: pointer;"
									id="removalexpensepopup"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="tradeAllowance">Trade
									Allowance</label> <input type="text" name="tradeAllowance"
									id="tradeAllowance" readonly="readonly"
									class="width50 validate[optional,custom[number]]"/>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_DISPOSAL.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 180px;">
							<div>
								<label class="width30" for="referenceNumber">Reference <span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_DISPOSAL.referenceNumber ne null && ASSET_DISPOSAL.referenceNumber ne ''}">
										<input type="text" readonly="readonly" name="referenceNumber"
											id="referenceNumber" class="width50 validate[required]"
											value="${ASSET_DISPOSAL.referenceNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="referenceNumber"
											id="referenceNumber" class="width50 validate[required]"
											value="${requestScope.referenceNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset<span
									class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="assetCreationName"
									value="${ASSET_DISPOSAL.assetCreation.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="assetCreationId"
									value="${ASSET_DISPOSAL.assetCreation.assetCreationId}" /> <span
									class="button"> <a style="cursor: pointer;" id="asset"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="checkOutDate">Disposal Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_DISPOSAL.disposalDate ne null && ASSET_DISPOSAL.disposalDate ne ''}">
										<c:set var="disposalDate"
											value="${ASSET_DISPOSAL.disposalDate}" />
										<%
											String disposalDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"disposalDate").toString());
										%>
										<input type="text" readonly="readonly" name="referenceNumber"
											id="referenceNumber" class="width50 validate[required]"
											value="<%=disposalDate%>" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="disposalDate"
											class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>

							</div>
							<div>
								<label class="width30" for="disposalMethod">Disposal
									Method<span class="mandatory">*</span> </label> <select
									name="disposalMethod" id="disposalMethod"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="disposalMethod" items="${DISPOSAL_METHOD}">
										<option value="${disposalMethod.key}">${disposalMethod.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="disposalType">Disposal Type<span
									class="mandatory">*</span> </label> <select name="disposalMethod"
									id="disposalType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="disposalType" items="${DISPOSAL_TYPE}">
										<option value="${disposalType.key}">${disposalType.value}</option>
									</c:forEach>
								</select>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="disposal_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="disposal_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>