<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
  <script type="text/javascript">
	var oTable; var selectRow = ""; var aData = ""; var aSelected = [];
 	$(function() { 
		$('#QuotationPopupDT').dataTable({ 
			"sAjaxSource": "<%=request.getContextPath()%>/getAllQuotations.action",
		    "sPaginationType": "full_numbers",
		    "bJQueryUI": true, 
		    "iDisplayLength": 25,
			"aoColumns": [
				{ "sTitle": "quotationId", "bVisible": false},	
				{ "sTitle": "currencyId", "bVisible": false},	
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationNumber" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.tender.list.tenderNumber" />', "sWidth": "100px"},
				{ "sTitle": 'Requisition', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationDate" />', "sWidth": "100px"},
				{ "sTitle": '<fmt:message key="accounts.quotation.quotationExpiry" />', "sWidth": "100px"}, 
				{ "sTitle": 'Supplier', "sWidth": "100px"},
				{ "sTitle": "supplierId", "bVisible": false},
				{ "sTitle": "creditTerm", "bVisible": false}
			],
			"sScrollY": $("#main-content").height() - 235,
			"aaSorting": [[0, 'desc']], 
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
					$(nRow).addClass('row_selected');
				}
			}
		});	
	
		oTable = $('#QuotationPopupDT').dataTable();		 
		$('#QuotationPopupDT tbody tr').live('dblclick', function () {  
			 aData = oTable.fnGetData( this );
			commonQuotationPopup(aData[0], aData[1], aData[2], aData[7], aData[8], aData[9]); 
			$('#quotation-common-popup').trigger('click');
		});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"
		style="min-height: 99%;">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Quotation
		</div>
		<div id="rightclickarea">
			<div id="gridDiv">
				<table id="QuotationPopupDT" class="display"></table>
			</div>
		</div>

		<div style="position: relative;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
			<div class="portlet-header ui-widget-header float-right"
				id="quotation-common-popup">close</div>
		</div>
	</div>
</div>
