<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript">
	
	$(function(){
		
		$('.product-search').combobox({
			selected : function(event, ui) {
				var productId = Number($(this).val());
	 			var productName = $.trim($('.product-search :selected').text());
	 			var previewObject = {
						"productId" : productId,
						"productName" : productName 
				};
				addProductPreview(previewObject);
			}
		}); 
		
		var tdsize = $($("#subcategory-table>tbody>tr:first").children()).size();
		
		if (tdsize == 1) {
			$($("#subcategory-table>tbody>tr:first").children()).each(
					function() {
						$(this).addClass('float-left width25');
					});
		}
	});
	
</script>

<span class="line"></span>
<br/><br/>

<span class="heading_title" style="margin-left: -5px;">Choose Your Liking</span>
<fieldset style="overflow-y: auto; overflow-x: hidden; padding: 1px !important;">
	<div class="width100 content-item-table sub-category-panel" style="overflow: hidden !important;">
		<table id="subcategory-table" class="pos-item-table width100">
			<c:choose>
				<c:when
					test="${PRODUCT_SUB_CATEGORIES ne null && fn:length(PRODUCT_SUB_CATEGORIES) > 0}">
					<c:set var="row" value="0" />
					<c:forEach items="${PRODUCT_SUB_CATEGORIES}" var="sub_cat">						
						<div id="subCatBoxModel_${sub_cat.productCategoryId}" class="box-model subCategory" style="background: #9FF781;">
							<span class="subCategory">${sub_cat.categoryName}
								<input type="hidden"
								id="categoryId_${sub_cat.productCategoryId}"
								class="categoryId" /> </span>
								<img class="product_image" src="images/ordering-menu/company/${THIS.companyKey}/test_sub.jpg" />
								<%-- <img class="product_image" src="images/ordering-menu/company/${THIS.companyKey}/categories/cat_${sub_cat.productCategoryId}.png" /> --%>
						</div>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<div>
						<span style="color: red;">Sub-category not available.</span>
					</div>
				</c:otherwise>
			</c:choose>
		</table>
	</div>
</fieldset>