<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <link rel="stylesheet" media="all" type="text/css" href="${pageContext.request.contextPath}/css/treeview/jquery.treeview.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/treeview/jquery.treeview.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>

<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
$(document).ready(function(){
	$("#tree").ready(function(){
		$("#tree").treeview({
			collapsed: true,
			animated: "medium",
			control:"#sidetreecontrol",
			persist: "location"
		}); 
	}); 
});
</script>
<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	<form name="codecombination" id="codecombinationValidation">
		<div style="display:none;" class="tempresult"></div>
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="accounts.jv.label.jvcc"/></div>
	 	<div class="portlet-content"> 
			<div id="temperror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
			<ul id="tree">
				<li><strong>Account Codes</strong></li>
				<c:forEach items="${COMPANY_ACCOUNT.companyList}" var="cbean">
					<li>${cbean.code} (${cbean.account})
						<ul>
							<c:forEach items="${COST_ACCOUNT.costList}" var="beans">
								<li>${beans.code} (${beans.account})
									<%-- <ul>
										<c:forEach items="${NATURAL_ACCOUNT.naturalList}" var="bean">
											<li>${bean.code} (${bean.account})</li>
										</c:forEach>
									</ul> --%>
								</li>
							</c:forEach>
						</ul>
					</li>
				</c:forEach>
			</ul> 
		</div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right save"id="${requestScope.page}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div> 
	</div>
	<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div> 
	<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
		<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
	</div>
	</form>
</div>
</div>