<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript">
var slidetab = "";
var accessCode = "";
var sectionRowId = 0;
var productSplits = [];
var accessRightRowId =null;
$(function(){
	
	$jquery("#projectValidation").validationEngine('attach');
	
	
	
	$('.expenseDate').datepick({onSelect: function(){
		 var rId=getRowId($(this).attr('id')); 
			expensetriggerAddRow(rId);
		}, showTrigger: '#calImg'}); 
	
	 $('#project_discard').click(function(event){  
		 productSplitDiscard("");
		 return false;
	 });
	 
	
	 
	 
	 manupulateLastRow();

	 $('#project_save').click(function(){ 
		 if($jquery("#projectValidation").validationEngine('validate')){ 
			 var parentAmount = Number($('#parentAmount').val());
			 var totalAmount = Number($('#totalAmount').html());
			 if(parentAmount!=totalAmount){
				 $('#page-error').hide().html("Total ("+totalAmount+") is not equal to amount ("+parentAmount+"). It must to be equal").slideDown(1000);
				 $('#page-error').delay(3000).slideUp();
				 return false;
			 }
			 
	 		var storeId = Number($('#parentStoreId').val());
	 		var productId = Number($('#parentProductId').val());
	 		var productSplitId = Number($('#productSplitId').val());
 			var description=$('#description').val();  
 			productSplits = getProductSplits();
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/product_split_save.action", 
			 	async: false, 
			 	data:{	productSplitId:productSplitId,storeId:storeId,productId:productId,
			 		description:description,productSplits:JSON.stringify(productSplits)
			 			
				 	 },
			    dataType: "json",
			    cache: false,
				success:function(response){   
					 if(response.returnMessage=="SUCCESS"){
						 productSplitDiscard(response.productSplitId,(productSplitId > 0 ? "Record updated.":"Record created."));
					 }else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 };
				},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
		}else{
			return false;
		}
	 }); 
	 
	 var amount=0.0;
	 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			amount+= Number($('#amount_'+rowId).val());   
	 }); 
	 $('#totalAmount').html(amount);
	 $('#parentAmount').val(amount);
	 
	 var getProductSplits = function(){
		 productSplits = [];
		 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = Number($('#productId_'+rowId).val());  
			var storeId = Number($('#storeId_'+rowId).val());  
			var shelfId =  Number($('#shelfId_'+rowId).val()); 
 			var amount = $('#amount_'+rowId).val();    
 			var description=$('#inventoryDescription_'+rowId).val();
 			var productSplitDetailId = Number($('#productSplitDetailId_'+rowId).val());
			if(typeof productId != 'undefined' && productId > 0 && productId > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"storeId" : storeId,
					"description": description,
					"shelfId": shelfId,
					"amount" : amount,
					"productSplitDetailId" : productSplitDetailId,
				});   
				productSplits.push({
					"productSplits" : jsonData
				});
			} 
		});  
		return productSplits;
	 };
	 
	

	 
	 
	 
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/product_split_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  
		  return false;
	 }); 
	 
	
//---------------------Delete Row section-----------------	 

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 });   
	 }); 
	
//---------------------Change Trigger section-----------------
	 
	 
	 
	 $('#parentprodStockID').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_stock_popup.action", 
			 	async: false,  
			 	data: {rowId: Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	
	 $('#product-stock-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
	 
		$('.product-stock-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_stock_popup.action", 
			 	async: false,  
			 	data: {rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

		$('.splitproduct-common-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove(); 
			var rowId=getRowId($(this).attr('id'));
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_common_popup.action", 
			 	async: false,  
			 	data:{rowId: rowId,itemType:''},
	 		    dataType: "html",
			    cache: false,
				success:function(result){   
					$('.common-result').html(result);  
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0);
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;
		}); 
	
 	$('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
 	
 	$('.amount').live('change',function(){  
 		resetTotal();
 	}); 
 	
 	$('.store-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		var rowId = getRowId($(this).attr('id'));
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_store_session_common.action", 
		 	async: false,  
		 	data:{rowId : rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.store-result').html(result);  
			},
			error:function(result){  
				 $('.store-result').html(result); 
			}
		});  
		return false;
	});

	$('#store-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 zIndex:100000,
		 modal: true 
	});  
		
	$('.rackid').live(
			'dblclick',
			function() {
				currentId = $(this);
				tempvar = $(currentId).attr('id');
				idarray = tempvar.split('_');
				rowid = Number(idarray[1]);
					var storeRowId = Number($('#store_row_id').html()); 
				$('#storeName_'+storeRowId).val($('#rackstorename_' + rowid).text()); 
				$('#storeId_'+storeRowId).val($('#rackstoreid_' + rowid).html());
				$('#shelfId_'+storeRowId).val($('#rackid_' + rowid).html()); 
				$('#store-popup').dialog('close');
				$('#common-popup').dialog('close');
				triggerAddRow(storeRowId);
				return false;
	});
	if(Number($('#productSplitId').val()> 0)){    
 		
	}
});
function manupulateLastRow() {
	var hiddenSize = 0;
	$($(".tab>tr:first").children()).each(function() {
		if ($(this).is(':hidden')) {
			++hiddenSize;
		}
	});
	var tdSize = $($(".tab>tr:first").children()).size();
	var actualSize = Number(tdSize - hiddenSize);

	$('.tab>tr:last').removeAttr('id');
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
	$($('.tab>tr:last').children()).remove();
	for ( var i = 0; i < actualSize; i++) {
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	}
	

}


function triggerAddRow(rowId) {
	var productId= Number($('#productId_' + rowId).val());
	var shelfId= Number($('#shelfId_' + rowId).val());
	var nexttab = $('#fieldrow_' + rowId).next();
	if (productId!=null && productId!='' && productId>0 
			&& shelfId!=null && shelfId!='' && shelfId>0 && 
			$(nexttab).hasClass('lastrow')) {
		$('#DeleteImage_' + rowId).show();
		$('.addrows').trigger('click');
		
		calCulateAmount();
		
		resetTotal();
	}
}

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}

function calCulateAmount() {
	var length=0;
	$('.rowid').each(function(){ 
		var rowId = getRowId($(this).attr('id'));  
		var productId= Number($('#productId_' + rowId).val());
		var shelfId= Number($('#shelfId_' + rowId).val());
		if (productId!=null && productId!='' && productId>0 
				&& shelfId!=null && shelfId!='' && shelfId>0)
			length++;
			
 	}); 
	
	
	 var parentAmount=Number($('#parentAmount').val());
	 var perProd=parentAmount/Number(length);
	 $('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId= Number($('#productId_' + rowId).val());
			var shelfId= Number($('#shelfId_' + rowId).val());
			if (productId!=null && productId!='' && productId>0 
					&& shelfId!=null && shelfId!='' && shelfId>0)
					$('#amount_' + rowId).val(perProd);
			
	 }); 


}

function resetTotal(){
	 var amount=0.0;
	 $('.rowid').each(function(){ 
			var rd = getRowId($(this).attr('id'));  
			amount+= Number($('#amount_'+rd).val());   
	 }); 
	 $('#totalAmount').html(amount);
}

function commonProductPopup(pid,name,rowId){
	
	if(rowId!=null && rowId!='' && rowId>0){
		$('#productId_' + rowId).val(pid);
		$('#productName_' + rowId).html(name);
		$('#common-popup').dialog("close");
		triggerAddRow(rowId);
	}else{
		$('#parentProductId').val(aData.productId);
		$('#parentProductName').val(aData.productName);
		$('#parentStoreName').val(aData.storeName);
		$('#parentStoreId').val(aData.shelfId);
		$('#parentAmount').val(aData.unitRate); 
	}
}
function productSplitDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/product_split_retrieve.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$('#store-popup').dialog('destroy');
			$('#store-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}

</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Product Split
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.generalinfo" />
		</div>
		
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
					<div id="page-success"
					class="response-msg success ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<form name="projectValidation" id="projectValidation"
			style="position: relative;">
				<input type="hidden" id="productSplitId"
					value="${PRODUCT_SPLIT_INFO.productSplitId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-left width48">
						<fieldset style="min-height: 100px;">
							<div>
								<label class="width30" for="referenceNumber"> Refernce Number<span style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PRODUCT_SPLIT_INFO.referenceNumber ne null && PRODUCT_SPLIT_INFO.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${PRODUCT_SPLIT_INFO.referenceNumber}"
											class="validate[required] width60" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width60" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="product"> Product<span
									style="color: red;">*</span> </label> <input type="hidden"
									name="parentProductId" id="parentProductId"
									value="${PRODUCT_SPLIT_INFO.product.productId}" /> <input
									type="text" class="validate[required] width60"
									name="parentProductName" id="parentProductName"
									value="${PRODUCT_SPLIT_INFO.product.productName}" /> <span
									class="button float-right"> <a style="cursor: pointer;"
									id="parentprodStockID"
									class="btn ui-state-default ui-corner-all product-stock width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="store"> Store<span style="color: red;">*</span> </label>
								<input type="hidden" name="parentStoreId"
												id="parentStoreId"
												value="${PRODUCT_SPLIT_INFO.shelf.shelfId}" />
								<input type="text" name="parentStoreName"
								id="parentStoreName" class="validate[required] width60"
								value="${PRODUCT_SPLIT_INFO.storeName}" />
							</div>
							<div>
								<label class="width30" for="store">Amount<span style="color: red;">*</span> </label>
								<input type="text" name="parentAmount"
												id="parentAmount"
												value="" class="width60" />
							</div>
						</fieldset>
					</div>
					<div class="float-left  width48">
						<fieldset style="min-height: 100px;">
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									style="height: 70px;" class="width61">${PRODUCT_SPLIT_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
					
				</div>
			</form>
			</div>
			<div class="clearfix"></div>
			<div id="hrm" class="hastable width100">
				<fieldset style="min-height: 100px;">
					<legend>Splits Detail</legend>
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 10%;">Product</th>
										<th style="width: 10%;">Store</th>
										<th style="width: 5%;">Amount</th>
										<th style="width: 10%;"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${PRODUCT_SPLIT_DETAIL}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productId_${status.index+1}"
												value="${DETAIL.productSplitDetail.product.productId}" /> <span
												id="productName_${status.index+1}">${DETAIL.productSplitDetail.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all splitproduct-common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" class="width80" name="storeName"
												id="storeName_${status.index+1}"
												value="${DETAIL.productSplitDetail.store.storeName} >> ${DETAIL.productSplitDetail.shelf.name}" />
												<input type="hidden" name="storeId"
												id="storeId_${status.index+1}"
												value="${DETAIL.productSplitDetail.store.storeId}" /> 
												<input type="hidden" name="shelfId"
												id="shelfId_${status.index+1}"
												value="${DETAIL.productSplitDetail.shelf.shelfId}" /> 
												<span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodIDStr_${i}"
													class="btn ui-state-default ui-corner-all store-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.productSplitDetail.unitRate}"
												class="amount right-align validate[optional,custom[number]] width90"> 
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${status.index+1}"
												value="${DETAIL.productSplitDetail.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="projectInventoryId"
												id="productSplitDetailId_${status.index+1}"
												value="${DETAIL.productSplitDetail.productSplitDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(PRODUCT_SPLIT_DETAIL)+1}"
										end="${fn:length(PRODUCT_SPLIT_DETAIL)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productId_${i}" /> <span id="productName_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all splitproduct-common-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											
											<td><input type="text" class="width80" name="storeName"
												id="storeName_${i}" /><input type="hidden" name="storeName"
												id="storeId_${i}" /> 
												<input type="hidden" name="shelfId"
												id="shelfId_${i}"
												/> 
												<span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodIDStr_${i}"
													class="btn ui-state-default ui-corner-all store-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td><input type="text" name="amount"
												id="amount_${i}" class="amount right-align validate[optional,custom[number]] width90">
											</td>
											<td><input type="text" name="inventoryDescription"
												id="inventoryDescription_${i}" class="width98" maxlength="150">
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												 <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <input
												type="hidden" name="productSplitDetailId"
												id="productSplitDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<table id="hastab" class="width100">
								<tbody>
									<tr>
										<td colspan="3" style="text-align: right; font-weight: bold;">Total :</td>
										<td colspan="2" id="totalAmount" style="font-weight: bold;">0.0</td>
									</tr>
								</tbody>
							</table>
						</fieldset>
					</div>
					
				
				
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right"
					id="project_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="project_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			
			
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			
				
			
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
			
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<span class="callJq"></span>
		</div>
</div>
