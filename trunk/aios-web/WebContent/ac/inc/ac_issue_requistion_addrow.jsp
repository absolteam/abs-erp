<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td style="display: none;" id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><input type="hidden" name="productId" id="productid_${ROW_ID}" />
	<input type="hidden" name="productType" id="productType_${ROW_ID}" /> 
		<span id="product_${ROW_ID}" class="width60 float-left"></span> 
		<span class="width10 float-right" id="unitCode_${ROW_ID}" style="position: relative;"></span>
		<span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${ROW_ID}"
			class="btn ui-state-default ui-corner-all product-stock-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td id="uom_${ROW_ID}" style="display: none;"></td>
	<td id="costingType_${ROW_ID}" style="display: none;"></td>
	<td style="display: none;"><input type="hidden" name="shelfId"
		id="shelfid_${ROW_ID}" /> <span id="store_${ROW_ID}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="storeID_${ROW_ID}"
			class="btn ui-state-default ui-corner-all common-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><select name="packageType" id="packageType_${ROW_ID}"
		class="packageType">
			<option value="">Select</option>
	</select></td>
	<td><input type="text"
		class="width96 packageUnit validate[optional,custom[number]]"
		id="packageUnit_${ROW_ID}" /></td>
	<td><input type="hidden" name="productQty" id="productQty_${ROW_ID}"
		class="productQty validate[optional,custom[number]] width80">
		<input type="hidden" name="extproductQty" id="extproductQty_${ROW_ID}" />
		<input type="hidden" name="totalAvailQty" id="totalAvailQty_${ROW_ID}" />
		<span id="baseUnitConversion_${ROW_ID}" class="width10" 
			style="display: none;"></span>
		<span class="width10 float-right" id="unitCode_${ROW_ID}" style="position: relative;"></span>
	</td>
	<td><input type="text" name="amount" id="amount_${ROW_ID}"
		style="text-align: right;"
		class="amount width98 validate[optional,custom[number]] right-align"> 
		<input type="hidden" name="baseAmount" id="baseAmount_${ROW_ID}"/>
	</td>
	<td><span id="totalAmount_${ROW_ID}" style="float: right;"></span>
	<input type="text" style="display: none;" id="totalAmountH_${ROW_ID}" class="totalAmountH"/>
	</td>
	<td><input type="text" name="linesDescription"
		id="linesDescription_${ROW_ID}" class="width98" maxlength="150">
	</td>
	<td><select name="section" class="section width98"
		id="section_${ROW_ID}">
			<option value="">-Select-</option>
			<c:forEach var="sec" items="${SECTIONS}" varStatus="status1">
				<option value="${sec.lookupDetailId}">${sec.displayName}</option>
			</c:forEach>
	</select> <input type="hidden" name="sectionTemp" id="sectionTemp_${ROW_ID}"
		value="" class="sectionTemp width98" /></td>
	<td style="width: 1%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		name="issueRequistionDetailId" id="issueRequistionDetailId_${ROW_ID}" />
		<input type="hidden" name="requistionDetailId"
		id="requistionDetailId_${ROW_ID}" /> <input type="hidden"
		id="stock_${ROW_ID}"/>
		<input type="hidden" name="batchNumber" id="batchNumber_${ROW_ID}"/>
		<input type="hidden" name="expiryDate" id="expiryDate_${ROW_ID}" />
	</td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#requistionValidation").validationEngine('attach');
 		$jquery("#amount_${ROW_ID},#totalAmountH_${ROW_ID}").number(true, 2);
	});
</script>