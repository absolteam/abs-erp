<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.opn_td{
	width:6%;
}
.ui-button { margin-left: -1px; }
.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
.ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; width:90%}
.ui-button-icon-primary span{
	margin:-6px 0 0 -8px !important;
}
button{
	height:18px;
	position: absolute!important;
	float: right!important;
	margin:4px 0 0 -36px!important;
}
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
.ui-autocomplete{
	width:11%!important;
}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var addedCombinations = 0;

$(function(){
	if($('.save').attr('id')=="edit"){
		$('.addrows').remove();
	}
	
	if($('.save').attr('id')=="add-customise"){
		$('.skip').show();
	}
	else{
		$('.skip').remove();
	}

	$jquery("#combinationvalidate-form").validationEngine('attach');
	
	 
	if($('.save').attr('id')=="add" || $('.save').attr('id')=="add-customise"){
		manupulateLastRow();
	} 
	$('.addData').click(function(){
		 if($jquery("#combinationvalidate-form").validationEngine('validate')){   
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addadd";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 var getFlag=1;
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_of_account_addlineentry.action",
			data:{id:rowid, showPage:showPage, getFlag:getFlag},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			}
		 });
		}
		else{
			return false;
		}		
	});

	$('.editData').click(function(){
		if($jquery("#combinationvalidate-form").validationEngine('validate')){   
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="addedit";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 var getFlag=0;
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_of_account_addlineentry.action",
			data:{id:rowid, showPage:showPage, getFlag:getFlag},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#companyCode').val($('#company_'+rowid).text());
			 	$('#costCode').val($('#cost_'+rowid).text());
			 	$('#naturalCode').val($('#natural_'+rowid).text());
			 	$('#analyisCode').val($('#analysis_'+rowid).text());
			 	$('#bufferCode1').val($('#buffer1_'+rowid).text());
			 	$('#bufferCode2').val($('#buffer2_'+rowid).text());

			 	$('#companyId').val($('#companyid_'+rowid).text());
			 	$('#costAccountId').val($('#costid_'+rowid).text());
			 	$('#naturalAccountId').val($('#naturalid_'+rowid).text());
			 	$('#analyisAccountId').val($('#analysisid_'+rowid).text());
			 	$('#bufferAccountId1').val($('#buffer1id_'+rowid).text());
			 	$('#bufferAccountId2').val($('#buffer2id_'+rowid).text());
			}
		 });
		}
		else{
			return false;
		}		
	});

	$('.editeditData').click(function(){
 		if($jquery("#combinationvalidate-form").validationEngine('validate')){
		 $('.error').hide(); 
		 slidetab=$(this).parent().parent().get(0);   
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var showPage="editedit";
		 $("#AddImage_"+rowid).hide();
		 $("#EditImage_"+rowid).hide();
		 $("#DeleteImage_"+rowid).hide();
		 $("#WorkingImage_"+rowid).show(); 
		 var getFlag=1;
		 if($('#buffer1_'+rowid).text()!=[] &&  $('#buffer2_'+rowid).text()!=[]){
			 getFlag=1;
		 }
		 else{
			 getFlag=0;
		 }
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/combination_of_account_addlineentry.action",
			data:{id:rowid, showPage:showPage, getFlag:getFlag},
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
			 	$(result).insertAfter(slidetab); 
			 	$('#companyCode').val($('#company_'+rowid).text());
			 	$('#costCode').val($('#cost_'+rowid).text());
			 	$('#naturalCode').val($('#natural_'+rowid).text());
			 	$('#analyisCode').val($('#analysis_'+rowid).text());
			 	if(getFlag==0){
			 		$('#bufferCode1').val($('#buffer1_'+rowid).text());
			 		$('#bufferCode2').val($('#buffer2_'+rowid).text());
			 		$('#bufferAccountId1').val($('#buffer1id_'+rowid).val());
				 	$('#bufferAccountId2').val($('#buffer2id_'+rowid).val());
			 	}
			 	$('#companyId').val($('#companyid_'+rowid).val());
			 	$('#costAccountId').val($('#costid_'+rowid).val());
			 	$('#naturalAccountId').val($('#naturalid_'+rowid).val());
			 	$('#analyisAccountId').val($('#analysisid_'+rowid).val()); 
			}
		 });
		}
		else{
			return false;
		}		
	});

	$(".delrow").click(function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		//Find the Id for fetch the value from Id
		 var tempvar=$(slidetab).attr("id");  
		 var idarray = tempvar.split('_');
		 var rowid=Number(idarray[1]);  
		 var lineId=$('#lineId_'+rowid).text();
		 var companycode=$('#companyCode_'+rowid).val();
		 var costcode=$('#costCode_'+rowid).val();
		 var naturalcode=$('#naturalCode_'+rowid).val(); 
		 if(companycode!=null && companycode!="" && costcode!=null && costcode!="" && naturalcode!=null && naturalcode!=""){ 
			 var flag=true; 
			 <%-- $.ajax({
					 type : "POST",
					 url:"<%=request.getContextPath()%>/combination_of_account_deletelineentry.action", 
					 data:{id:lineId},
					 async: false,
				  dataType: "html",
					 cache: false,
				   success:function(result){ 
						 $('.formError').hide();  
						 $('.tempresult').html(result);   
						 if(result!=null){
						    	if($('#returnMsg').html() == '' || $('#returnMsg').html() == null)
	                              flag = true;
						    	else{	
							    	flag = false; 
							    	$("#lineerror").hide().html($('#returnMsg').html().slideDown()); 
						    	} 
						     }  
					},
					error:function(result){
						$("#temperror").hide().html($('#returnMsg').html().slideDown()); 
						 $('.tempresult').html(result);   
						 return false;
					}
						
				 }); --%>
				 if(flag==true){ 
	        		 var childCount=Number($('#childCount').val());
	        		 if(childCount > 0){
						childCount=childCount-1;
						$('#childCount').val(childCount); 
	        		 } 
	        		 $(this).parent().parent('tr').remove();  
				}
		 }
		 else{
			 $('#warning_message').hide().html("Please insert record to delete...").slideDown(1000);
			 return false;
		 }  
	 });

	 $('.save').click(function(){
		 var id=$('.save').attr('id'); 
		 var rowid=1;
		 var url_action="";
		 var companyId=0;
		 var costAccountId=0;
		 var naturalAccountId=0;
		 var analyisAccountId=0;
		 var bufferAccountId1=0;
		 var bufferAccountId2=0;
		 var combinationId=0;
		 if(id=="add" || id=="add-customise"){ 
			url_action="combination_of_account_save";  
		 }
		 else{
			url_action="combination_of_account_update"; 
		}
		 
		 var companyArray=new Array();
		 var costArray=new Array();
		 var naturalArray=new Array();
		 var analysisArray=new Array();
		 var buffer1Array=new Array();
		 var buffer2Array=new Array(); 
		 $('.rowid').each(function(){ 
			 var tempvar=$(this).attr("id");  
			 var idarray = tempvar.split('_');
			 var rowid=Number(idarray[1]); 
			 companyId=$('#companyCode_'+rowid).val();
			 costAccountId=$('#costCode_'+rowid).val();
			 naturalAccountId=$('#naturalCode_'+rowid).val();
			 analyisAccountId=$('#analysisCode_'+rowid).val();
			 bufferAccountId1=$('#buffer1Code_'+rowid).val();
			 bufferAccountId2=$('#buffer2Code_'+rowid).val(); 
			  if((typeof companyId != 'undefined') && (typeof costAccountId != 'undefined') && (typeof naturalAccountId != 'undefined')
					&& (companyId!="" && costAccountId!="" && naturalAccountId!="")){  
				  companyArray.push(companyId);
				  costArray.push(costAccountId);
				 naturalArray.push(naturalAccountId);
				 if(typeof analyisAccountId != 'undefined' && analyisAccountId!=''){
					 analysisArray.push(analyisAccountId);
				 }
				 else{ 
					 analysisArray.push(0);
				 }
				 if(typeof bufferAccountId1 != 'undefined' && bufferAccountId1!=''){
					 buffer1Array.push(bufferAccountId1);
				 }
				 else{ 
					 buffer1Array.push(0);
				 }
				 if(typeof bufferAccountId2 != 'undefined' && bufferAccountId2!=''){
					 buffer2Array.push(bufferAccountId2);
				 }
				 else{ 
					 buffer2Array.push(0);
				 } 
			 }  
		 }); 
		 var accountCodes="";  
		 for(var l=0;l<companyArray.length;l++){
			 accountCodes+=companyArray[l]+","+costArray[l]+","+naturalArray[l]+","+analysisArray[l]+","+buffer1Array[l]+","+buffer2Array[l];
			 if(l<companyArray.length-1){ 
				 accountCodes+="@@";
			  } 
		 }
		 if(id=="add" || id=="add-customise"){
			 companyId=Number(0);costAccountId=Number(0);naturalAccountId=Number(0);analyisAccountId=Number(0);bufferAccountId1=Number(0);bufferAccountId2=Number(0);combinationId=Number(0);
		 }
		 else if(id=="edit"){
			 companyId=companyArray[0];
			 costAccountId=costArray[0];
			 naturalAccountId=naturalArray[0];
			 analyisAccountId=analysisArray[0];
			 bufferAccountId1=buffer1Array[0];
			 bufferAccountId2=buffer2Array[0];
			 combinationId=$('#combinationid_1').text();
		 } 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+url_action+".action", 
		 	async: false,
		 	data:{	companyId:companyId, costAccountId:costAccountId, naturalAccountId:naturalAccountId, analyisAccountId:analyisAccountId,
					bufferAccountId1:bufferAccountId1, bufferAccountId2:bufferAccountId2, combinationId:combinationId, accountCodes:accountCodes
				 },
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $(".tempresult").html(result); 
				 var message=$('#returnMsg').html(); 
				 if(message.trim()=="Success" && (id=="add" || id=="edit")){
					 $.ajax({
							type:"POST",
							url:"<%=request.getContextPath()%>/combination_of_accounts_retrive.action", 
						 	async: false,
						    dataType: "html",
						    cache: false,
							success:function(result){
								$("#main-wrapper").html(result); 
								$('#success_message').hide().html(message).slideDown(1000);
							}
					 });
				 }
				 else if(message.trim()=="Success" && id=="add-customise"){ 
					 var showPage="add-customise";
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/customise_property_combination.action", 
				     	async: false,
				     	data:{showPage:showPage},
						dataType: "html",
						cache: false,
						success: function(result){  
							$("#DOMWindow").html(result); 
							$.scrollTo(0,300);
						} 		
					});  
				 }
				 else{
					 $('.temperror').hide().html(message).slideDown(1000);
					 return false;
				 }
			}
		 });
 	}); 
	 
	 $('.skip').click(function(){  
			$.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/customise_property_combination.action", 
		     	async: false, 
				dataType: "html",
				cache: false,
				success: function(result){  
					$("#DOMWindow").html(result); 
					$.scrollTo(0,300);
				} 		
			});  
	  });
	 
	 $(document).keyup(function(e) { 
		 var id=$('.save').attr('id'); 
		  if (e.keyCode == 27 && id=="add-customise"){ 
			  $.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
			     	async: false, 
					dataType: "html",
					cache: false,
					success: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						$("#main-wrapper").html(result);
					},
					error: function(result){ 
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove(); 
						$("#main-wrapper").html(result);  
					}
				});
		  } 
	}); 
      
      $('.discard').click(function(){
 		 var id=$('.save').attr('id'); 
 		 if(id=="add" || id=="edit"){
 			url_action="combination_of_account_discard";
 		 }
 		 else{
 			url_action="show_setup_wizard"; 
 		 }
 		 $.ajax({
 				type:"POST", 
 				url:"<%=request.getContextPath()%>/"+url_action+".action", 
 			 	async: false, 
 			    dataType: "html",
 			    cache: false,
 				success:function(result){
 					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$("#main-wrapper").html(result);
 				}
 			});
 	 });
      
      
      $('.addrows').click(function(){ 
		  var i=Number(0); 
		  var id=Number(0);
		  $('#warning_message').hide();
		  $('.rowid').each(function(){
			  var temparray=$(this).attr('id');
			  var idarray=temparray.split('_');
			  var rowid=Number(idarray[1]);  
			  id=Number($('#lineId_'+rowid).text());  
			  id+=1;
		  }); 
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_of_account_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
				}
			});
	 }); 
      
      $(".accountcode").combobox({ 
          selected: function(event, ui){ 
        	slidetab=$(this).parent().parent().get(0);  
   		  	var nexttab=$(slidetab).next(); 
   		    $('#warning_message').hide();
   	  		triggerAddRow(nexttab,slidetab); 
          }
      }); 
      
      if($('.save').attr('id')=="edit"){
    	  $('#options').remove();
	      $(".accountcode").each(function(){
	 		 slidetab=$(this).attr('id'); 
	 		 var idarray =slidetab.split('_');
	 	  	 var rowid=Number(idarray[1]); 
	 	  	 $('#companyCode_'+rowid+' option').each(function(){  
	 	 	  	 var tempCompanyCode=$('#tempcompanyCode_'+rowid).val().trim();
	 	 	  	 var tempcompanyId=$('#companyid_'+rowid).val();
	 	 	     var tempcompanyText=$(this).text(); 
	 			 if(tempcompanyText==tempCompanyCode){ 
	 				$('.companyclass_'+rowid).combobox('autocomplete', tempCompanyCode); 
					$('.companyclass_'+rowid+' option[value='+tempcompanyId+']').attr("selected","selected");  
	 			 }
	 	  	}); 
	 	  	$('#costCode_'+rowid+' option').each(function(){  
	 	 	  	 var tempcostCode=$('#tempcostCode_'+rowid).val().trim();
	 	 	  	 var tempCostId=$('#costid_'+rowid).val();
	 	 	     var tempcostText=$(this).text(); 
	 			 if(tempcostText==tempcostCode){ 
	 				$('.costclass_'+rowid).combobox('autocomplete', tempcostCode); 
	 				$('.costclass_'+rowid+' option[value='+tempCostId+']').attr("selected","selected");
	 			 }
	 	  	}); 
	 	  	
	 	  	$('#naturalCode_'+rowid+' option').each(function(){  
	 	 	  	 var tempnaturalCode=$('#tempnaturalCode_'+rowid).val().trim();
	 	 	  	 var tempNaturalId=$('#naturalid_'+rowid).val();
	 	 	     var tempnaturalText=$(this).text(); 
	 			 if(tempnaturalText==tempnaturalCode){ 
	 				$('.naturalclass_'+rowid).combobox('autocomplete', tempnaturalCode); 
	 				$('.naturalclass_'+rowid+' option[value='+tempNaturalId+']').attr("selected","selected");
	 			 }
	 	  	}); 
	 	  	
	 	  	$('#analysisCode_'+rowid+' option').each(function(){  
	 	 	  	 var tempanalysisCode=$('#tempanalyisCode_'+rowid).val().trim();
	 	 	  	 var tempAnalysisId=$('#analysisid_'+rowid).val();
	 	 	     var tempanalyisText=$(this).text(); 
	 			 if(tempanalyisText==tempanalysisCode){ 
	 				$('.analysisclass_'+rowid).combobox('autocomplete', tempanalysisCode); 
	 				$('.analysisclass_'+rowid+' option[value='+tempAnalysisId+']').attr("selected","selected");
	 			 }
	 	  	}); 
	 	  	
	 	  	$('#buffer1Code_'+rowid+' option').each(function(){  
	 	 	  	 var tempbufferCode=$('#tempbuffer1_'+rowid).val().trim();
	 	 	  	 var tempbufferId=$('#buffer1id_'+rowid).val();
	 	 	     var tempbufferText=$(this).text(); 
	 			 if(tempbufferText==tempbufferCode){ 
	 				$('.buffer1class_'+rowid).combobox('autocomplete', tempbufferCode); 
	 				$('.buffer1class_'+rowid+' option[value='+tempbufferId+']').attr("selected","selected");
	 			 }
	 	  	}); 
	 	  	
	 	  	
	 	  	$('#buffer2Code_'+rowid+' option').each(function(){  
	 	 	  	 var tempbufferCode=$('#tempbuffer2_'+rowid).val().trim();
	 	 	  	 var tempbufferId=$('#buffer2id_'+rowid).val();
	 	 	     var tempbufferText=$(this).text(); 
	 			 if(tempbufferText==tempbufferCode){ 
	 				$('.buffer2class_'+rowid).combobox('autocomplete', tempbufferCode); 
	 				$('.buffer2class_'+rowid+' option[value='+tempbufferId+']').attr("selected","selected");
	 			 }
	 	  	}); 
	 	  	
	 	 }); 
      } 
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid'); 
	$('tr:last').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function triggerAddRow(nexttab,slidetab){   
	var temparray=$(slidetab).attr('id'); 
	var idarray=temparray.split('_');
    var rowid=Number(idarray[1]);  
	var companyCode=$('#companyCode_'+rowid).val();
	var costCode=$('#costCode_'+rowid).val();
	var naturalCode=$('#naturalCode_'+rowid).val(); 
	if(companyCode!=null && companyCode!="" && 
			costCode!=null && costCode!="" &&
			naturalCode!=null && naturalCode!="" && 
			$(nexttab).hasClass('lastrow')){ 
		$('.buffer1class_'+rowid+" option").each(function(){
			if($(this).val()!=""){   
				if($(this).text()=="00000" || $(this).text()=="0000" || $(this).is(':contains("Default")') ||
						$(this).is(':contains("Buffer")')){
					var likevalue=$(this).text();
					$('.buffer1class_'+rowid).combobox('autocomplete', likevalue); 
					$('.buffer1class_'+rowid+' option[value='+$(this).val()+']').attr("selected","selected");
				} 
			} 
		}); 
		$('.buffer2class_'+rowid+" option").each(function(){
			if($(this).val()!=""){   
				if($(this).text()=="00000" || $(this).text()=="0000" || $(this).is(':contains("Default")') ||
						$(this).is(':contains("Buffer")')){
					var likevalue=$(this).text();
					$('.buffer2class_'+rowid).combobox('autocomplete', likevalue); 
					$('.buffer2class_'+rowid+' option[value='+$(this).val()+']').attr("selected","selected");
				} 
			} 
		});
		$('#DeleteImage_'+rowid).show();
		$('.addrows').trigger('click');
	}  
	return false;
}
</script>

<div id="main-content"> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
	<form name="codecombination" id="combinationvalidate-form" style="position: relative;">
		<div style="display:none;" class="tempresult"></div>
	 	<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="accounts.jv.label.jvcc"/></div>
	 	<div class="portlet-content"> 
			<div id="temperror" class="response-msg error ui-corner-all" style="width:90%; display:none"></div>  
			<div id="hrm" class="hastable width100">   
			 <div id="warning_message" class="response-msg notice ui-corner-all"  style="width:90%; display:none;"></div>
			<input type="hidden" name="trnValue" id="trnValue" readonly="readonly"/> 
			<input type="hidden" name="childCount" id="childCount"  value="0"/> 
			<table id="hastab" class="width100">	
				<thead class="chrome_tab">
				<tr>
					<th><fmt:message key="accounts.code.label.companyaccount"/></th>
					<th><fmt:message key="accounts.code.label.costaccount"/></th>
					<th><fmt:message key="accounts.code.label.naturalaccount"/></th>
					<th><fmt:message key="accounts.code.label.analysisaccount"/></th>
					<th><fmt:message key="accounts.code.label.buffer1"/></th>
					<th><fmt:message key="accounts.code.label.buffer2"/></th>  
					<th id="options"><fmt:message key="accounts.common.label.options"/></th>
				</tr>
			</thead>
			<tbody class="tab">
				 <c:set var="m" value="1"/>
				 <c:choose>
				 	<c:when test="${requestScope.COMBINATION_DETAILS ne null && requestScope.COMBINATION_DETAILS ne ''}"> 
						  <tr id="fieldrow_${m}" class="rowid"> 
								<td id="company_${m}">
									<select id="companyCode_${m}" class="accountcode companyclass_${m}" name="companyCode">
										<option value="">Select</option>
										<c:forEach items="${COMPANY_ACCOUNT.companyList}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.companyCode} (${COMBINATION_DETAILS.companyCodeDesc})" id="tempcompanyCode_${m}"/>
								</td>
								<td id="cost_${m}">
									<select id="costCode_${m}" class="accountcode costclass_${m}" name="costCode">
										<option value="">Select</option>
										<c:forEach items="${COST_ACCOUNT.costList}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.costCode} (${COMBINATION_DETAILS.costCodeDesc})" id="tempcostCode_${m}"/>
								</td>
								<td id="natural_${m}">
									<select id="naturalCode_${m}" class="accountcode naturalclass_${m}" name="naturalCode">
										<option value="">Select</option>
										<c:forEach items="${NATURAL_ACCOUNT.naturalList}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.naturalCode} (${COMBINATION_DETAILS.naturalCodeDesc})" id="tempnaturalCode_${m}"/>
								</td>
								<td id="analysis_${m}">
									<select id="analysisCode_${m}" class="accountcode analysisclass_${m}" name="analysisCode">
										<option value="">Select</option>
										<c:forEach items="${ANALYSIS_ACCOUNT.analysisList}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.analyisCode} (${COMBINATION_DETAILS.analyisCodeDesc})" id="tempanalyisCode_${m}"/>
								</td>
								<td id="buffer1_${m}">
									<select id="buffer1Code_${m}" class="accountcode buffer1class_${m}" name="buffer1Code">
										<option value="">Select</option>
										<c:forEach items="${BUFFER1_ACCOUNT.bufferList1}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.bufferCode1} (${COMBINATION_DETAILS.bufferCode1Desc})" id="tempbuffer1_${m}"/>
								</td>
								<td id="buffer2_${m}">
									<select id="buffer2Code_${m}" class="accountcode buffer2class_${m}" name="buffer2Code">
										<option value="">Select</option>
										<c:forEach items="${BUFFER2_ACCOUNT.bufferList1}" var="bean">
											<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
										</c:forEach>
									</select>
									<input type="hidden" value="${COMBINATION_DETAILS.bufferCode2} (${COMBINATION_DETAILS.bufferCode2Desc})" id="tempbuffer2_${m}"/>
								</td>
								<td style="display:none;" id="lineId_${m}">${m}</td>
								<td style="display:none;" id="combinationid_${m}">${COMBINATION_DETAILS.combinationId}</td>   
								<td style="display:none;">
						         <input type="hidden" id="companyid_${m}" value="${COMBINATION_DETAILS.companyId}" readonly="readonly">
						         <input type="hidden" id="costid_${m}" value="${COMBINATION_DETAILS.costAccountId}" readonly="readonly">
						         <input type="hidden" id="naturalid_${m}" value="${COMBINATION_DETAILS.naturalAccountId}" readonly="readonly">
						         <input type="hidden" id="analysisid_${m}" value="${COMBINATION_DETAILS.analyisAccountId}" readonly="readonly">
						         <input type="hidden" id="buffer1id_${m}" value="${COMBINATION_DETAILS.bufferAccountId1}" readonly="readonly">
						         <input type="hidden" id="buffer2id_${m}" value="${COMBINATION_DETAILS.bufferAccountId2}" readonly="readonly">
						        </td>
								<td id="option_${m}" style="display:none;">
								   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${m}" style="display:none; cursor:pointer;" title="Add Record">
		 								<span class="ui-icon ui-icon-plus"></span>
								  </a>	
								  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editeditData" id="EditImage_${m}" style="cursor:pointer;display:none" title="Edit Record">
										<span class="ui-icon ui-icon-wrench"></span>
								  </a> 
								  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${m}" style="display:none; cursor:pointer;" title="Delete Record">
										<span class="ui-icon ui-icon-circle-close"></span>
								  </a>
								  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${m}" style="display:none;" title="Working">
										<span class="processing"></span>
								  </a>
							   </td> 
						  </tr>	
				 	</c:when>
				 	<c:otherwise>
				 		<c:forEach var="i" begin="1" end="2" step="1" varStatus ="status"> 
							  <tr id="fieldrow_${i}" class="rowid"> 
									<td id="company_${i}">
										<select id="companyCode_${i}" class="accountcode companyclass_${i}" name="companyCode">
											<option value="">Select</option>
											<c:forEach items="${COMPANY_ACCOUNT.companyList}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td id="cost_${i}">
										<select id="costCode_${i}" class="accountcode costclass_${i}" name="costCode">
											<option value="">Select</option>
											<c:forEach items="${COST_ACCOUNT.costList}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td id="natural_${i}">
										<select id="naturalCode_${i}" class="accountcode naturalclass_${i}" name="naturalCode">
											<option value="">Select</option>
											<c:forEach items="${NATURAL_ACCOUNT.naturalList}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td id="analysis_${i}">
										<select id="analysisCode_${i}" class="accountcode analysisclass_${i}" name="analysisCode">
											<option value="">Select</option>
											<c:forEach items="${ANALYSIS_ACCOUNT.analysisList}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td id="buffer1_${i}">
										<select id="buffer1Code_${i}" class="accountcode buffer1class_${i}" name="buffer1Code">
											<option value="">Select</option>
											<c:forEach items="${BUFFER1_ACCOUNT.bufferList1}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td id="buffer2_${i}">
										<select id="buffer2Code_${i}" class="accountcode buffer2class_${i}" name="buffer2Code">
											<option value="">Select</option>
											<c:forEach items="${BUFFER2_ACCOUNT.bufferList1}" var="bean">
												<option value="${bean.accountId}">${bean.code} (${bean.account})</option>
											</c:forEach>
										</select>
									</td>
									<td style="display:none;" id="lineId_${i}">${i}</td>
									<td style="display:none;" id="combinationid_${i}"></td>  
									<td style="display:none;">
										<input type="hidden" id="companyid_${i}" readonly="readonly">
										<input type="hidden" id="costid_${i}" readonly="readonly">
										<input type="hidden" id="naturalid_${i}" readonly="readonly">
										<input type="hidden" id="analysisid_${i}" readonly="readonly">
										<input type="hidden" id="buffer1id_${i}" readonly="readonly">
										<input type="hidden" id="buffer2id_${i}" readonly="readonly">
									</td>
									<td id="option_${i}">
									   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="display:none; cursor:pointer;" title="Add Record">
			 								<span class="ui-icon ui-icon-plus"></span>
									  </a>	
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span>
									  </a> 
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none;cursor:pointer;" title="Delete Record">
											<span class="ui-icon ui-icon-circle-close"></span>
									  </a>
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
											<span class="processing"></span>
									  </a>
								   </td> 
							  </tr>	
						</c:forEach>
				 	</c:otherwise>
				 </c:choose> 
			 </tbody>
		</table>
		</div>  		 
		</div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div> 
		<div class="portlet-header ui-widget-header float-right save"id="${requestScope.page}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div> 
	</div>
	<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div> 
	<div style="display:none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons skip"> 
		<div class="portlet-header ui-widget-header float-left skip" style="cursor:pointer;">skip</div> 
	</div>
	</form>
</div>
</div>