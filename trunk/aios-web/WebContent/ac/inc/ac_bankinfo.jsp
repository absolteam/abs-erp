<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript">
 var startpick;
 var slidetab;
 var availflag=1;
 var calendarRecordsAdded = 0;
 var tempid="";
 var accountTypeId=0;
 var bankAccountString=""; 
 $(function(){ 
	 $('#accountCodeCheck').click(function() {
	        if (this.checked) {
	            $("#combination-div").hide();
	            $("#combinationId").val("");
	            
	        } else {
	            $("#combination-div").show();
	        }
	    }); 

	 manupulateLastRow();
	 
	 if($('#bankId').val()>0){
		 $('#accountCodeCheck-div').hide(); 
 		 $('.institutionType').attr('disabled', true); 
		 $('.institutionType').val($('#tempinstitutionType').val());  
	 } else{
		 $('.institutionType').val(Number(1));  
	}

	 $jquery("#bankaccount-validate").validationEngine('attach'); 
	

	 $('#institutionTypeId').change(function(){
		 if($(this).val()!=null && $(this).val()!= ''){
			 if(Number($(this).val()) == 5)
				 actionName = "debit_credit_card_header";
			 else
				 actionName = "get_bank_account_header";  
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/"+actionName+".action", 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
					 $('#hastab').html(result); 
					 manupulateLastRow();
					}
				 });
		 }  
		 return false;
	 }); 
	
	 $(".bankaddData").live('click',function(){  
		 if($jquery("#bankaccount-validate").validationEngine('validate')){
			 $('#institutionTypeId').attr('disabled',true);
 			 if(Number($('#combinationId').val())>0){
				 if($('#openFlag').val() == 0) {
					 slidetab=$(this).parent().parent().get(0);   
					//Find the Id for fetch the value from Id
					var tempvar=$(this).parent().attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					var institutionType = Number($('#institutionTypeId').val()); 
					var actionName = "";
					if(institutionType==5) 
						 actionName = "bank_debitcredit_add";
					else 
						 actionName = "bank_account_add"; 
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/"+actionName+".action", 
						data:{id:rowid, addEditFlag:"A"},
					 	async: false,
					    dataType: "html",
					    cache: false,
						success:function(result){
						 $(result).insertAfter(slidetab);
						 $('#accountNumber').focus();
						 $('#openFlag').val(1);
						}
					}); 
				 }else{
					return false;
				 }
			 }else{
				 $('#page-error').hide().html("Please Choose Account Code.").slideDown(100);
				 $('#page-error').delay(3000).slideUp();
				 return false;
			 } 
		 }
		 else{
			 return false;
		 }	
		 return false;
	});

	$(".bankeditData").live('click',function(){ 
		 
		 if($('#openFlag').val() == 0) { 
		 slidetab=$(this).parent().parent().get(0);  
		 
		 if($jquery("#bankaccount-validate").validationEngine('validate')){
			 if(Number($('#combinationId').val())>0){
			 if($('#availflag').val()==1){
				 
				//Find the Id for fetch the value from Id
					var tempvar=$(this).attr("id");
					var idarray = tempvar.split('_');
					var rowid=Number(idarray[1]); 
					$("#AddImage_"+rowid).hide();
					$("#EditImage_"+rowid).hide();
					$("#DeleteImage_"+rowid).hide();
					$("#WorkingImage_"+rowid).show(); 
					var institutionType = Number($('#institutionTypeId').val()); 
					var actionName = "";
					if(institutionType==5) 
						 actionName = "bank_debitcredit_add";
					else 
						 actionName = "bank_account_add"; 
			 $.ajax({
				 type : "POST",
				 url:"<%=request.getContextPath()%>/"+actionName+".action", 
				 data:{id:rowid,addEditFlag:"E"},
				 async: false,
			  	 dataType: "html",
				 cache: false,
			   	 success:function(result){ 
			   		$(result).insertAfter(slidetab); 
				    //Bussiness parameter  
					 $('#accountNumber').val($('#accountNumber_'+rowid).text().trim()); 
					 $('#holderName').val($('#accountHolder_'+rowid).text().trim()); 
		 			 $('#accountHolderId').val($('#holderidval_'+rowid).text().trim()); 
		 			 $('#branchContact').val($('#branchContact_'+rowid).val().trim()); 
		   			 $('#contactPerson').val($('#contactPerson_'+rowid).val().trim());  
			   		 if(actionName=="bank_account_add"){ 
			   			$('#accountType').val($('#typeidval_'+rowid).val().trim()); 
			   			$('#accountPurpose').val($('#accountPurpose_'+rowid).text().trim()); 
			   			$('#branchName').val($('#branchName_'+rowid).text().trim()); 
		    			$('#signatory1').val($('#sign1_'+rowid).val().trim());
		    			$('#signatory2').val($('#sign2_'+rowid).val().trim());
		    			$('#modeOfOperation').val($('#modeidval_'+rowid).val().trim());
		    			$('#currencyId').val($('#currencyidval_'+rowid).val().trim()); 
		    			$('#currencyidval').val(currencyId);  
			   			$('#address').val($('#address_'+rowid).val().trim());  
		    			$('#routingNumber').val($('#rountingNumber_'+rowid).text().trim()); 
			   			$('#iban').val($('#iban_'+rowid).text().trim());
			   			$('#swiftCode').val($('#swift_'+rowid).val().trim());  
			   			$('#creditLimit').val($.trim($('#creditLimit_'+rowid).text())); 
				   	  } else {
				   		 $('#cardType').val($('#typeidval_'+rowid).val().trim());  
				   		 $('#cardNumber').val($('#cardNumber_'+rowid).text().trim()); 
			   			 $('#accountPurpose').val($('#purpose_'+rowid).text().trim()); 
			   			 $('#description').val($('#description_'+rowid).val().trim()); 
			   			 $('#validFrom').val($('#validFrom_'+rowid).text().trim()); 
			 			 $('#validTo').val($('#validTo_'+rowid).text().trim());
			 			 $('#limit').val($('#limit_'+rowid).text().trim());
			 			 $('#cscNumber').val($('#csc_'+rowid).val().trim());
			 			 $('#pinNumber').val($('#pin_'+rowid).val().trim());
			 			 $('#issuingEntity').val($('#issuingid_'+rowid).val().trim());  
					  } 
					    $('#openFlag').val(1); 
					} 
				});
			 }
			else{
				$('#temperror').hide().html("Bank Account Number <span style='font-weight:bold; display: inline;'>\""+name+ "\"</span> is already is use !!!").slideDown(1000);
			}	
		 }else{
			 $('#page-error').hide().html("Please Choose Account Code.").slideDown(100);
			 $('#page-error').delay(3000).slideUp();
			 return false;
			}		
		 }
		 else{
			 return false;
		 }	
		 }else{
			 return false;
		 } 
		 return false;
	});
	
	 $(".bankdelrow").live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0); 
		//Find the Id for fetch the value from Id
		var tempvar=$(this).parent().attr("id"); 
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 
		var lineId=Number($('#lineId_'+rowid).val());
		var bankAccountId=$('#bankAccountId_'+rowid).val(); 
		var institutionType = Number($('#institutionTypeId').val()); 
		var actionName = "";
		if(institutionType==5) 
			 actionName = "bank_debitcredit_add";
		else 
			 actionName = "bank_account_add"; 
		$.ajax({
			 type : "POST",
			 url:"<%=request.getContextPath()%>/"+actionName+".action", 
			 data:{id:lineId, addEditFlag:"D"},
			 async: false,
		  dataType: "html",
			 cache: false,
		   success:function(result){ 
			     if(bankAccountId>0){
			    	 $('#bankAccountId').val($('#bankAccountId').val()+bankAccountId+"@");
			     }
				 $("#fieldrow_"+rowid).remove(); 
	        		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){  
						i=i+1;
						$($($(this).children().get(7)).children().get(1)).val(i); 
   					 }); 
			},
			error:function(result){
				$("#fieldrow_"+rowid).remove();		        		//To reset sequence 
	        		var i=0;
	     			$('.rowid').each(function(){  
						i=i+1;
						$($($(this).children().get(7)).children().get(1)).val(i); 
   					 }); 
				 return false;
			}
		 });
		 return false;	 
	 });

	 $('.save_bank').click(function(){
		 
		 if($jquery("#bankaccount-validate").validationEngine('validate')){
			 if(Number($('#bankId').val())>0){
				 saveBank();
			 }else{ 
				 if(Number($('#combinationId').val())>0){
					 saveBank();
				}
				else{
					$('#page-error').hide().html("Please Choose Account Code.").slideDown(100);
					$('#page-error').delay(3000).slideUp();
					return false;
			    } 
			 }
		 }else{
			 return false;
		 }
	 });
      
	 $(".cancel_bank").click(function(){
		 slidetab=$(this).parent().parent().get(0); 
		 $.ajax({
			 type:"POST",
			 url:"<%=request.getContextPath()%>/bank_list.action", 
			 //data:{trnValue:trnVal},
			 async: false,
			  dataType: "html",
			   cache: false,
			   success:function(result){  
				 $('.formError').hide(); 
				 	$('#transURN').val("");  
					$('#main-wrapper').html(result); 
					var tempsMsg=$('#sMsg').html();
					var tempeMsg=$('#eMsg').html(); 
				    if(tempsMsg!=""){
				       $('#sMsg').show(); 
				       $('#eMsg').hide();
				     }
				     else{ 
				    	$('#eMsg').show(); 
				    	$('#sMsg').hide();
				    }  
			 },
		     error:function(result){
				 $("#main-wrapper").html(result);
				 $(".success").hide();
				 $('.error').show(); 
				 $("#transURN").val(""); 
				 $("#trnValue").val("");
			 }
		 });
			 
		 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var rowid=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  rowid=rowid+1;  
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/bank_account_addrow.action",
					data:{id:rowid}, 
				 	async: false,
				    dataType: "html",
				    cache: false,
					success:function(result){
						$('.tab tr:last').before(result);
						 if($(".tab").height()>255)
							 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowid').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#lineId_'+rowId).html(i);
							 i=i+1; 
				 		 }); 
					} 
	  			
				});
			return false;
		});

	 $('#codecombination-popup').dialog({
			autoOpen: false,  
			width:800,  
			bgiframe: false,
			modal: true 
		});
	 $('.codecombination-popup').click(function(){  
		 $('.ui-dialog-titlebar').remove();  
           $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/combination_treeview.action",
              async: false, 
              dataType: "html",
              cache: false,
              success:function(result){
                   $('.codecombination-result').html(result);
              },
              error:function(result){
                   $('.codecombination-result').html(result);
              }
          });
	 });
	 $('.treecancel').click(function(){ 
		$('#codecombination-popup').dialog('close');
	 });

	 $('#bankName').change(function(){
		$('#tempbankname').val("###");
	 });
 });
function setCombination(combinationTreeId,combinationTree){
	$('#combinationId').val(combinationTreeId);
	$('.combinationValue').val(combinationTree); 
}
function saveBank(){ 
	 var bankId=Number($("#bankId").val()); 
	 var bankName=$("#bankName").val();  
	 var combinationId=Number($("#combinationId").val());
	 var bankAccountString=$("#bankAccountString").val();
	 var institutionType=Number($('#institutionTypeId').val());
	 var saveFlag = Number (0);
	 $('.rowid').each(function(){  
		var rowId = Number($(this).attr('id').split('_')[1]);
		var accountNumber = $.trim($('#accountNumber_'+rowId).text());
		if(accountNumber!=null && accountNumber!='')
			saveFlag = Number(1);
	 }); 
 	 if(saveFlag > 0){
		 $.ajax({
		  type : "POST",
		  url:"<%=request.getContextPath()%>/save_bank.action", 
		  data:{
			 bankId: bankId,
			 bankName: bankName, 
			 combinationId: combinationId,
			 institutionType: institutionType,
			 bankAccountString:bankAccountString
		   },
		   async: false,
		   dataType: "html",
	       cache: false,
		   success:function(result){  
			   $(".tempresult").html(result); 
			   var message = $.trim($('.tempresult').html());  
			   if(message=="SUCCESS"){
				   if (bankId == 0)
					   message = "Record created.";
				   else
					   message = "Record updated.";
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/bank_list.action",
											async : false,
											dataType : "html",
											cache : false,
											success : function(result) {
												$('#common-popup').dialog(
														'destroy');
												$('#common-popup').remove();
												$("#main-wrapper").html(result); 
												$('#success_message')
															.hide()
															.html(message)
															.slideDown(1000); 
												$('#success_message').delay(
														2000).slideUp();
											}
										});
							} else {
								$('#page-error').hide().html(message)
										.slideDown(1000);
								return false;
							}
						}
					});
		} else {
			$('#page-error').hide().html("Please enter account details.")
			.slideDown(1000);
			return false;
		}

	}
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	function triggerAddRow(rowId) {
		var nexttab = $('#fieldrow_' + rowId).next();
		if ($(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	
</script>

<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<form name="newFields" id="bankaccount-validate" style="position: relative;">
			<div class="mainhead portlet-header ui-widget-header">
				<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Bank
			</div> 
			<div class="portlet-content width100">
				<div id="hrm">
					<span style="color: red;"><c:out
							value="${requestScope.errMsg1}" /> </span>
				</div>
				<div class="tempresult" style="display: none;"></div>

				<input type="hidden" name="childCount" id="childCount"
					value="${fn:length(BANK_ACCOUNT_DETAILS)}" /> <input type="hidden"
					name="bankAccountString" id="bankAccountString" />
				<c:choose>
					<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
										&& COMMENT_IFNO.commentId gt 0}">
						<div class="width85 comment-style" id="hrm">
							<fieldset>
								<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
								<label class="width70">${COMMENT_IFNO.comment}</label>
							</fieldset>
						</div> 
					</c:when>
				</c:choose> 
				<div class="width100" id="hrm">
					<div id="page-error"
						class="response-msg error ui-corner-all width90"
						style="display: none"></div>
					<div class="width48 float-right">
						<fieldset style="min-height: 60px;">
							<div>
								<label class="width30">Account Code<span
									class="mandatory">*</span> </label> <input type="hidden"
									name="combinationId" id="combinationId"
									value="${BANK_DETAILS.combination.combinationId}" />
								<c:choose>
									<c:when
										test="${BANK_DETAILS.combination ne null && BANK_DETAILS.combination ne ''}">
										<input type="text" name="combinationValue" class="float-left width50 combinationValue validate[required]" readonly="readonly" value="${BANK_DETAILS.combination.accountByNaturalAccountId.code}[${BANK_DETAILS.combination.accountByNaturalAccountId.account}]"/>
									</c:when>
									<c:otherwise> 
										<input type="text" name="combinationValue" class="float-left width50 combinationValue validate[required]" readonly="readonly"/>
										<span class="button float-left width10" id="assetcode"
											style="position: relative; top: 6px;"> <a
											style="cursor: pointer;"
											class="btn ui-state-default ui-corner-all width100 codecombination-popup">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 60px;">
							<div>
								<label class="width30">Institution Type<span
									style="color: red">*</span>
								</label> <select name="institutionType" id="institutionTypeId"
									class="width51 validate[required] institutionType">
									<option value="">Select</option>
									<c:forEach var="institutionType" items="${INSTITUTION_TYPES}">
										<option value="${institutionType.key}">${institutionType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempinstitutionType"
									value="${BANK_DETAILS.institutionType}" />
							</div>
							<div>
								<label class="width30">Institution Name<span
									style="color: red">*</span> </label> <input type="hidden" name="bankId"
									value="${BANK_DETAILS.bankId}" id="bankId" /> <input
									type="text" tabindex="1" name="bankName"
									value="${BANK_DETAILS.bankName}" id="bankName"
									class="width50 validate[required]"> <input
									type="hidden" name="tempbankname" id="tempbankname" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="temperror" class="response-msg error ui-corner-all"
				style="width: 80%; display: none"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container width100">
				<div class="portlet-content">
					<div id="hrm" class="hastable width100">
						<fieldset>
							<legend>Account Information<span
									style="color: red">*</span></legend>
							<div class="childCountErr response-msg error ui-corner-all"
								style="width: 80%; display: none;"></div>
							<div id="wrgNotice" class="response-msg notice ui-corner-all"
								style="width: 80%; display: none;"></div>
							<input type="hidden" name="openFlag" id="openFlag" value="0" />
							<c:choose>
								<c:when test="${BANK_DETAILS.institutionType ne 5}">
									<table id="hastab" class="width100">
										<thead>
											<tr>
												<th class="width10">Account No.</th>
												<th class="width10">Account Holder</th>
												<th class="width10">Type</th>
												<th class="width10">Routing</th>
												<th class="width10">IBAN</th>
												<th class="width10">Currency</th>
												<th class="width10">Mode</th>
												<th class="width10">Purpose</th>
												<th class="width10">Branch Name</th>
												<th class="width10" style="display: none;">Credit Limit</th>
												<th class="width10"><fmt:message
														key="accounts.calendar.label.options" />
												</th>
											</tr>
										</thead>
										<tbody class="tab">
											<c:choose>
												<c:when
													test="${BANK_ACCOUNT_DETAILS ne null && fn:length(BANK_ACCOUNT_DETAILS) >0}">
													<c:forEach var="BANK_ACCOUNT"
														items="${BANK_ACCOUNT_DETAILS}" varStatus="status">
														<tr id="fieldrow_${status.index+1}" class="rowid">
															<td class="width10" id="accountNumber_${status.index+1}">${BANK_ACCOUNT.accountNumber}</td>
															<td class="width10" id="accountHolder_${status.index+1}">
																${BANK_ACCOUNT.bankAccountHolder}
															</td>
															<td class="width10" id="accountType_${status.index+1}">
																<c:choose>
																	<c:when
																		test="${BANK_ACCOUNT.accountType ne null && BANK_ACCOUNT.accountType eq 1 }">
																		 Savings
																	</c:when>
																	<c:when
																		test="${BANK_ACCOUNT.accountType ne null && BANK_ACCOUNT.accountType eq 2 }">
																		 Current
																	</c:when>
																	<c:otherwise>
																		Salary
																	</c:otherwise>
																</c:choose>
															</td>
															<td class="width10" id="rountingNumber_${status.index+1}">${BANK_ACCOUNT.rountingNumber}</td>
															<td class="width10" id="iban_${status.index+1}">${BANK_ACCOUNT.iban}</td>
															<td class="width10" id="currency_${status.index+1}">${BANK_ACCOUNT.currency.currencyPool.code}</td>
															<td class="width10" id="mode_${status.index+1}">
																${BANK_ACCOUNT.lookupDetailByOperationMode.displayName}
															</td>
															<td class="width10" id="accountPurpose_${status.index+1}">${BANK_ACCOUNT.accountPurpose}</td>
															<td class="width10" id="branchName_${status.index+1}">${BANK_ACCOUNT.branchName}</td>
															<td style="display: none;" class="width10" id="creditLimit_${status.index+1}">${BANK_ACCOUNT.creditLimit}</td>
															<td class="width10" id="option_${status.index+1}"><input
																type="hidden" id="bankAccountId_${status.index+1}"
																value="${BANK_ACCOUNT.bankAccountId}" /> <input
																type="hidden" id="lineId_${status.index+1}"
																value="${status.index+1}" /> <input type="hidden"
																id="swift_${status.index+1}"
																value="${BANK_ACCOUNT.swiftCode}" /> <input
																type="hidden" id="holderidval_${status.index+1}"
																value="${BANK_ACCOUNT.bankAccountHolder}" /> <input type="hidden"
																id="currencyidval_${status.index+1}"
																value="${BANK_ACCOUNT.currency.currencyId}" /> <input
																type="hidden" id="typeidval_${status.index+1}"
																value="${BANK_ACCOUNT.accountType}" />
																<input type="hidden" id="modeidval_${status.index+1}"
																value="${BANK_ACCOUNT.lookupDetailByOperationMode.lookupDetailId}" />
																<input type="hidden" id="sign1_${status.index+1}"
																value="${BANK_ACCOUNT.lookupDetailBySignatory1LookupId.lookupDetailId}" />
																<input type="hidden" id="sign2_${status.index+1}"
																value="${BANK_ACCOUNT.lookupDetailBySignatory2LookupId.lookupDetailId}" />
																<input type="hidden" id="address_${status.index+1}"
																value="${BANK_ACCOUNT.address}" /> <input type="hidden"
																id="branchContact_${status.index+1}"
																value="${BANK_ACCOUNT.branchContact}" /> <input
																type="hidden" id="contactPerson_${status.index+1}"
																value="${BANK_ACCOUNT.contactPerson}" /> <a
																class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
																id="EditImage_${status.index+1}"
																style="cursor: pointer;" title="Edit Record"> <span
																	class="ui-icon ui-icon-wrench"></span> </a> <a
																style="cursor: pointer;"
																class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
																id="DeleteImage_${status.index+1}" title="Delete Record">
																	<span class="ui-icon ui-icon-circle-close"></span> </a> <a
																href="#"
																class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
																id="WorkingImage_${status.index+1}"
																style="display: none;" title="Working"> <span
																	class="processing"></span> </a></td>
														</tr>
													</c:forEach>
												</c:when>
											</c:choose>
											<c:forEach var="i"
												begin="${fn:length(BANK_ACCOUNT_DETAILS)+1}"
												end="${fn:length(BANK_ACCOUNT_DETAILS)+2}" step="1"
												varStatus="status">
												<tr id="fieldrow_${i}" class="rowid">
													<td class="width10" id="accountNumber_${i}"></td>
													<td class="width10" id="accountHolder_${i}"></td>
													<td class="width10" id="accountType_${i}"></td>
													<td class="width10" id="rountingNumber_${i}"></td>
													<td class="width10" id="iban_${i}"></td>
													<td class="width10" id="currency_${i}"></td>
													<td class="width10" id="mode_${i}"></td>
													<td class="width10" id="accountPurpose_${i}"></td>
													<td class="width10" id="branchName_${i}"></td>
													<td style="display: none;" class="width10" id="creditLimit_${i}"></td>
													<td class="width10" id="option_${i}"><input
														type="hidden" id="bankAccountId_${i}" /> <input
														type="hidden" id="lineId_${i}" value="${i}" /> <input
														type="hidden" id="swift_${i}" /> <input type="hidden"
														id="holderidval_${i}" /> <input type="hidden"
														id="currencyidval_${i}" /> <input type="hidden"
														id="typeidval_${i}" /> <input type="hidden"
														id="modeidval_${i}" /> <input type="hidden"
														id="sign1_${i}" /> <input type="hidden" id="sign2_${i}" />
														<input type="hidden" id="address_${i}" /> <input
														type="hidden" id="branchContact_${i}" /> <input
														type="hidden" id="contactPerson_${i}" /> <a
														style="cursor: pointer;"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankaddData"
														id="AddImage_${i}" title="Add Record"> <span
															class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
														id="EditImage_${i}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														style="cursor: pointer; display: none;"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
														id="DeleteImage_${i}" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${i}" style="display: none;"
														title="Working"> <span class="processing"></span> </a></td>

												</tr>
											</c:forEach>
										</tbody>
									</table>
								</c:when>
								<c:otherwise>
									<table id="hastab" class="width100">
										<thead>
											<tr>
												<th class="width10">Account No.</th>
												<th class="width10">Account Holder</th>
												<th class="width10">Card Number</th>
												<th class="width10">Card Type</th>
												<th class="width10">Issuing Entity</th>
												<th class="width10">Valid from</th>
												<th class="width10">Valid to</th>
												<th class="width10">Limit</th>
												<th class="width10">Purpose</th>
												<th class="width10"><fmt:message
														key="accounts.calendar.label.options" />
												</th>
											</tr>
										</thead>
										<tbody class="tab">
											<c:forEach var="CARD_DETAILS" items="${BANK_ACCOUNT_DETAILS}"
												varStatus="status">
												<tr id="fieldrow_${status.index+1}" class="rowid">
													<td class="width10" id="accountNumber_${status.index+1}">${CARD_DETAILS.accountNumber}</td>
													<td class="width10" id="accountHolder_${status.index+1}">
														${CARD_DETAILS.bankAccountHolder}
													</td>
													<td class="width10" id="cardNumber_${status.index+1}">${CARD_DETAILS.cardNumber}</td>
													<td class="width10" id="cardType_${status.index+1}">${CARD_DETAILS.cardType}</td>
													<td class="width10" id="issuingEntity_${status.index+1}">${CARD_DETAILS.lookupDetailByCardIssuingEntity.displayName}</td>
													<td class="width10" id="validFrom_${status.index+1}">${CARD_DETAILS.validFrom}</td>
													<td class="width10" id="validTo_${status.index+1}">${CARD_DETAILS.validTo}</td>
													<td class="width10" id="limit_${status.index+1}">${CARD_DETAILS.cardLimit}</td>
													<td class="width10" id="purpose_${status.index+1}">${CARD_DETAILS.accountPurpose}</td>
													<td class="width10" id="option_${status.index+1}"><input
														type="hidden" id="bankAccountId_${status.index+1}"
														value="${CARD_DETAILS.bankAccountId}" /> <input
														type="hidden" id="lineId_${status.index+1}"
														value="${status.index+1}" /> <input type="hidden"
														id="csc_${status.index+1}"
														value="${CARD_DETAILS.csvNumber}" /> <input type="hidden"
														id="pin_${status.index+1}"
														value="${CARD_DETAILS.pinNumber}" /> <input type="hidden"
														id="typeidval_${status.index+1}"
														value="${CARD_DETAILS.cardType}" />
														<input type="hidden" id="issuingid_${status.index+1}"
														value="${CARD_DETAILS.lookupDetailByCardIssuingEntity.lookupDetailId}" />
														<input type="hidden" id="description_${status.index+1}"
														value="${CARD_DETAILS.description}" /> <input
														type="hidden" id="branchContact_${status.index+1}"
														value="${CARD_DETAILS.branchContact}" /> <input
														type="hidden" id="contactPerson_${status.index+1}"
														value="${CARD_DETAILS.contactPerson}" /> <input
														type="hidden" id="holderidval_${status.index+1}"
														value="${CARD_DETAILS.bankAccountHolder}" /> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
														id="EditImage_${status.index+1}" style="cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														style="cursor: pointer;"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
														id="DeleteImage_${status.index+1}" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span> </a> <a
														href="#"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a></td>

												</tr>
											</c:forEach>
											<c:forEach var="i"
												begin="${fn:length(BANK_ACCOUNT_DETAILS)+1}"
												end="${fn:length(BANK_ACCOUNT_DETAILS)+2}" step="1"
												varStatus="status">
												<tr id="fieldrow_${i}" class="rowid">
													<td class="width10" id="accountNumber_${i}"></td>
													<td class="width10" id="accountHolder_${i}"></td>
													<td class="width10" id="cardNumber_${i}"></td>
													<td class="width10" id="cardType_${i}"></td>
													<td class="width10" id="issuingEntity_${i}"></td>
													<td class="width10" id="validFrom_${i}"></td>
													<td class="width10" id="validTo_${i}"></td>
													<td class="width10" id="limit_${i}"></td>
													<td class="width10" id="purpose_${i}"></td>
													<td class="width10" id="option_${i}"><input
														type="hidden" id="bankAccountId_${i}" /> <input
														type="hidden" id="lineId_${i}" value="${i}" /> <input
														type="hidden" id="csc_${i}" /> <input type="hidden"
														id="pin_${i}" /> <input type="hidden" id="typeidval_${i}" />
														<input type="hidden" id="issuingid_${i}" /> <input
														type="hidden" id="description_${i}" /> <input
														type="hidden" id="branchContact_${i}" /> <input
														type="hidden" id="contactPerson_${i}" /> <a
														style="cursor: pointer;"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankaddData"
														id="AddImage_${i}" title="Add Record"> <span
															class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
														id="EditImage_${i}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														style="cursor: pointer; display: none;"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
														id="DeleteImage_${i}" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${i}" style="display: none;"
														title="Working"> <span class="processing"></span> </a></td>

												</tr>
											</c:forEach>
										</tbody>
									</table>
								</c:otherwise>
							</c:choose>
						</fieldset>
					</div>
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right cancel_bank"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save_bank"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<input type="hidden" name="availflag" id="availflag" value="1" />
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="width: auto; overflow: hidden;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>