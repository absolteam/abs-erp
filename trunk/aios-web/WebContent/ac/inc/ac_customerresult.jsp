<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
		Customer Information
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="customerlist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${CUSTOMER_INFO}" var="bean" varStatus="status">  
	     		<option value="${bean.customerId}|${bean.customerNumber}|${bean.customerName}">${bean.customerName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	
 	<div class="float-left width100 customer_list" style="padding:2px;">  
 		<input type="hidden" value="${requestScope.pageInfo}" id="pageInfo"/>
	     	<ul> 
   				<li class="width30 float-left"><span>Customer Number</span></li>  
   				<li class="width30 float-left"><span>Name</span></li>  
   				<li class="width30 float-left"><span>Type</span></li>   
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${CUSTOMER_INFO}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="customer_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.customerId}" id="customerbyid_${status.index+1}">  
		     			<input type="hidden" value="${bean.customerNumber}" id="customerbynumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.customerName}" id="customerbynameid_${status.index+1}"> 
		     			<span id="customerbynumber_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.customerNumber}</span>
		     			<span id="customerbyname_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.customerName}</span>
		     			 <span id="customerbytype_${status.index+1}" style="cursor: pointer;" class="float-left width30">${bean.type}
		     				<span id="customerNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 	
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',293);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	      selected: function(event, ui){   
    	   var customerdetails=$(this).val();
	       var customerarray=customerdetails.split("|"); 
	       $('#customerName').val($('.autoComplete option:selected').text());
	       $('#customerNumber').val(customerarray[1]); 
	       $('#customerId').val(customerarray[0]); 
	       if($('#pageInfo').val() == "sales_order" || $('#pageInfo').val() == "pos_invoice") {
			 showShippingSite();
	       }else if($('#pageInfo').val() == "sales_invoice") {
	    	   showActiveDeliveryNotes();
	       }
	       $('#common-popup').dialog("close");   
	   } 
	}); 
	 
	 $('.customer_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.customer_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#customerNameselect_'+rowid).hasClass('selected')){ 
					 $('#customerNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#customerNameselect_'+rowid).addClass('selected');
			return false;
		});
	 
	 $('.customer_list_li').live('dblclick',function(){  
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#customerId').val($('#customerbyid_'+rowid).val()); 
		  $('#customerNumber').val($('#customerbynumberid_'+rowid).val()); 
		  $('#customerName').val($('#customerbynameid_'+rowid).val());   
		  if($('#pageInfo').val() == "sales_order" || $('#pageInfo').val() == "pos_invoice") {
			  showShippingSite();
		  }else if($('#pageInfo').val() == "sales_invoice") {
	    	   showActiveDeliveryNotes();
	      }
		  $('#common-popup').dialog('close');  
		  return false;
	  });
	 $('#close').click(function(){
		 $('#common-popup').dialog('close'); 
	 });  
 }); 
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.customer_list ul li{
		padding:3px;
	}
	.customer_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>