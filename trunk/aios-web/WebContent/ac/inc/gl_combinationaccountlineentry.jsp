 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
 <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<tr style="background-color: #F8F8F8;">
	<td colspan="7" class="tdidentity" id="childRowId_${rowId}">
		<div class="width48 float-right" id="hrm">
			<fieldset>								 
					<legend><fmt:message key="accounts.code.label.codecreation"/></legend> 
					<div>
						<label class="width30">
							<fmt:message key="accounts.code.label.buffer1"/>
						</label>
						<input type="text" name="bufferCode1" id="bufferCode1" value="${account1.account}" readonly="readonly" class="width40">
						<input type="hidden" name="bufferAccountId1" id="bufferAccountId1" value="${account1.accountId}" readonly="readonly">
						<span class="button" style="margin-top:3px;">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
								<span class="ui-icon ui-icon-newwin"></span> 
							</a>
						</span>
					</div>
					<div>
						<label class="width30">
							<fmt:message key="accounts.code.label.buffer2"/>
						</label>
						<input type="text" name="bufferCode2" id="bufferCode2" value="${account2.account}"  readonly="readonly" class="width40">
						<input type="hidden" name="bufferAccountId2" id="bufferAccountId2" value="${account2.accountId}" readonly="readonly">
						<span class="button" style="margin-top:3px;">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
								<span class="ui-icon ui-icon-newwin"></span> 
							</a>
						</span>
					</div>			
			</fieldset> 
    		<div id="othererror" class="response-msg error ui-corner-all" style="width:80%; display:none;">${requestScope.errMsg}</div> 
		</div>			 
		<div class="float-left width50" id="hrm" style="margin-left:10px;">
			<fieldset>
				<legend><fmt:message key="accounts.code.label.codecreation"/></legend>  
				<div>
					<label class="width30">
						 <fmt:message key="accounts.code.label.companyaccount"/><span style="color:red;">*</span>
					</label>
					<input type="text" name="companyCode" id="companyCode" readonly="readonly" class="width40 validate[required]">
					<input type="hidden" name="companyId" id="companyId" readonly="readonly">
					<span class="button" style="margin-top:3px;">
						<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
							<span class="ui-icon ui-icon-newwin"></span> 
						</a>
					</span>
				</div>
				<div>
					<label class="width30">
						 <fmt:message key="accounts.code.label.costaccount"/><span style="color:red;">*</span>
					</label>
					<input type="text" name="costCode" id="costCode" readonly="readonly" class="width40 validate[required]">
					<input type="hidden" name="costAccountId" id="costAccountId" readonly="readonly">
					<span class="button" style="margin-top:3px;">
						<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
							<span class="ui-icon ui-icon-newwin"></span> 
						</a>
					</span>
				</div>
				<div>
					<label class="width30">
						 <fmt:message key="accounts.code.label.naturalaccount"/><span style="color:red;">*</span>
					</label>
					<input type="text" name="naturalCode" id="naturalCode" readonly="readonly" class="width40 validate[required]">
					<input type="hidden" name="naturalAccountId" id="naturalAccountId" readonly="readonly">
					<span class="button" style="margin-top:3px;">
						<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
							<span class="ui-icon ui-icon-newwin"></span> 
						</a>
					</span>
				</div>
				<div>
					<label class="width30">
						<fmt:message key="accounts.code.label.analysisaccount"/>
					</label>
					<input type="text" name="analyisCode" id="analyisCode" readonly="readonly" class="width40">
					<input type="hidden" name="analyisAccountId" id="analyisAccountId" readonly="readonly">
					<span class="button" style="margin-top:3px;">
						<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all ccid-popup width100"> 
							<span class="ui-icon ui-icon-newwin"></span> 
						</a>
					</span>
				</div> 
			</fieldset> 
		</div> 
		<div class="clearfix"></div> 
		<input type="hidden" id="showPage" value="${showPage}"/>
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"> 
			<div class="portlet-header ui-widget-header float-right rowcancel" id="cancel_${rowId}"  style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right rowsave"  id="rowsave_${rowId}"style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>
		<div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="ccid-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="ccid-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		</div>
	</td>
</tr>	
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0);
$(function(){
	$('.rowcancel').click(function(){ 
		$('.formError').remove();	
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if($('#combinationid_'+rowid).text()>0){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			$("#DeleteImage_"+rowid).hide();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 } 
	});

	$('.rowsave').click(function(){
		 //Find the Id for fetch the value from Id
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]); 

		 var lineId=$('#lineId_'+rowid).text(); 
		 var url_action="";
		 var combinationId=0; 
		
		 var companyId=$('#companyId').val();
		 var costAccountId=$('#costAccountId').val();
		 var naturalAccountId=$('#naturalAccountId').val();
		 var analyisAccountId=Number($('#analyisAccountId').val());
		 var bufferAccountId1=Number($('#bufferAccountId1').val());
		 var bufferAccountId2=Number($('#bufferAccountId2').val());
		 
		 var showPage=$('#showPage').val();
		 if($('#combinationid_'+rowid).text()>0){
			 combinationId=$('#combinationid_'+rowid).text();
			 url_action="combination_of_account_update_linesave";
		 }
		 else if(showPage=="addadd"){
			 url_action="combination_of_account_add_linesave";
		 }
		 else if(showPage=="addedit"){
			 url_action="combination_of_account_addedit_linesave";
		 }

		 if($("#combinationvalidate-form").validationEngine({returnIsValid:true})){
			 var flag=false; 
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false, 
				 	data:{	combinationId:combinationId, bufferAccountId1:bufferAccountId1, bufferAccountId2:bufferAccountId2, companyId:companyId,
							costAccountId:costAccountId, naturalAccountId:naturalAccountId, analyisAccountId:analyisAccountId, id:lineId
					 	 },
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tempresult').html(result); 
					     if(result!=null) {
                           flag = true;
                           addedCombinations = addedCombinations + 1;
	   						if(addedCombinations == 2) {								
	   							$('.addrows').trigger('click');
	   							addedCombinations = 1;
	   						}
					     } 
				 	},  
				 	error:function(result){
				 	  $('.tempresult').html(result);
                   	  $("#othererror").html($('#returnMsg').html()); 
                   	  return false;
				 	} 
	          });

			 if(flag==true){  
		        	//Bussiness parameter
	        		$('#company_'+rowid).text($('#companyCode').val()); 
	        		$('#cost_'+rowid).text($('#costCode').val()); 
	        		$('#natural_'+rowid).text($('#naturalCode').val());  
	        		$('#analysis_'+rowid).text($('#analyisCode').val());  
	        		$('#buffer1_'+rowid).text($('#bufferCode1').val());  
	        		$('#buffer2_'+rowid).text($('#bufferCode2').val());  

	        		$('#companyid_'+rowid).text($('#companyId').val()); 
	        		$('#costid_'+rowid).text($('#costAccountId').val()); 
	        		$('#naturalid_'+rowid).text($('#naturalAccountId').val());
	        		$('#analysisid_'+rowid).text($('#analyisAccountId').val());    
	        		$('#buffer1id_'+rowid).text($('#bufferAccountId1').val());    
	        		$('#buffer2id_'+rowid).text($('#bufferAccountId2').val());    
	        		
					//Button(action) hide & show 
	        		if(showPage=="editedit"){ 
	        			$("#AddImage_"+rowid).hide();
	        			$("#EditImage_"+rowid).show();
	        			$("#DeleteImage_"+rowid).hide();
	        			$("#WorkingImage_"+rowid).hide(); 
	        		 }
	        		 else if(showPage=="addedit"){
	        			 $("#AddImage_"+rowid).hide();
	        			 $("#EditImage_"+rowid).show();
	        			 $("#DeleteImage_"+rowid).show();
	        			 $("#WorkingImage_"+rowid).hide(); 
	        		 }
	        		 else{
	        			 $("#AddImage_"+rowid).hide();
	        			 $("#EditImage_"+rowid).show();
	        			 $("#DeleteImage_"+rowid).show();
	        			 $("#WorkingImage_"+rowid).hide(); 
	        		 } 
	        		$("#childRowId_"+rowid).remove();
					//Row count+1
					var childCount=Number($('#childCount').val());
					childCount=childCount+1;
					$('#childCount').val(childCount);
	        }
		 } 
		 else{
			 return false;
		 }   
	});

	$('.ccid-popup').click(function(){ 
	       tempid=$(this).parent().get(0);
			$('#ccid-popup').dialog('open');
			actionName=$($(tempid).siblings().get(1)).attr('id').toLowerCase(); 
			var segmentId="0";
			if(actionName=="companycode"){
				segmentId=1;
			}
			else if(actionName=="costcode"){
				segmentId=2;
			}
			else if(actionName=="naturalcode"){
				segmentId=3;
			}
			else if(actionName=="analyiscode"){
				segmentId=4;
			}
			else if(actionName=="buffercode1"){
				segmentId=5;
			}
			else if(actionName=="buffercode2"){
				segmentId=6;
			}
			actionName="getaccountcodes";	
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_of_"+actionName+".action", 
			 	async: false, 
			    dataType: "html",
			    data:{segmentId:segmentId},
			    cache: false,
				success:function(result){  
					 $('.ccid-result').html(result); 
				},
				error:function(result){ 
					 $('.ccid-result').html(result); 
				}
			}); 
	});
		
	 $('#ccid-popup').dialog({ 
			autoOpen: false,
			minwidth: 'auto',
			width:400,
			bgiframe: false,
			modal: false,
			buttons: {
				"Ok": function() { 
					$(this).dialog("close"); 
				}, 
				"Cancel": function() {  
					$(this).dialog("close"); 
				} 
			}	
		}); 
});
</script>