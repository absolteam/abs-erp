<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${MATERIAL_TRANSFER_BYID.referenceNumber}
	</td> 
	<td> 
		<c:set var="transferDate" value="${MATERIAL_TRANSFER_BYID.transferDate}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("transferDate").toString())%>  
	</td> 
	<td>
		 ${MATERIAL_TRANSFER_BYID.totalTransferedQty}
	</td>
	<td style="text-align: right;">
		${MATERIAL_TRANSFER_BYID.totalTransferAmount} 
	</td> 
	<td style="display:none;">
		<input type="hidden" id="materialTransferId_${rowId}" value="${MATERIAL_TRANSFER_BYID.materialTransferId}"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}"> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editTransferReturnData" id="EditImage_${rowId}" 
		  	style="cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowTransferReturn" id="DeleteImage_${rowId}" 
		  	style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a> 
	</td>   
</tr>