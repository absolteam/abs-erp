<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
 
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname="";
var accountTypeId=0; 
$(function(){ 
	$('#pay_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getpayment_detaillist.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				$("#main-wrapper").html(result);  
			}
		 }); 
	});

	$('#pay_print').click(function(){  
		var paymentId=Number($('#paymentId').val()); 
		window.location.reload();
   		window.open('payment_purchase_alertprint.action?paymentId='+ paymentId +'&format=HTML', '',
					'width=800,height=800,scrollbars=yes,left=100px'); 
	}); 

	$('#chequeNumber').change(function(){
		$('#chequeFlag').val(true); 
	});
}); 
</script>
<div id="main-content">
		<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container" style="min-height: 99%;">
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 Purchase Payment Information
			</div>	
			<div class="mainhead portlet-header ui-widget-header"><span style="display: none;" class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
				 General Information
			</div>	
			<div><form id="paymentCreation">
			<div style="width: 98%; margin: 10px;">
			<div>
			<div class="tempresult" style="display: none;"></div>
			<input type="hidden" id="chequeFlag" value="false"/>
			<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>   
				<div class="width45 float-left" id="hrm">
					<fieldset> 
						<input type="hidden" id="paymentId" value="${PAYMENT_INFO_ALERT.paymentId}">  					 
						<div style="padding: 3px;">
							<label class="width30">Payment NO</label> 
							<span class="width60 view">${PAYMENT_INFO_ALERT.paymentNumber}</span> 
						</div>  
						<div style="padding: 3px;">
							<label class="width30">Payment Mode</label> 
							${PAYMENT_INFO_ALERT.paymentAccess} 
						</div>
						<c:choose>
						 	<c:when test="${PAYMENT_INFO_ALERT.paymentCode eq 'CSH' || PAYMENT_INFO_ALERT.paymentCode eq 'WRT'}">
						 		<div class="cash" style="padding: 3px;">
									<label class="width30">Cash Account</label>
									<input type="hidden" name="combinationId" id="cashCombinationId" value="${PAYMENT_INFO_ALERT.combination.combinationId}"/> 
									<span class="width60 view">${PAYMENT_INFO_ALERT.combination.accountByNaturalAccountId.code}[${PAYMENT_INFO_ALERT.combination.accountByNaturalAccountId.account}]</span>
								</div>
						 	</c:when>
						 	<c:when test="${PAYMENT_INFO_ALERT.paymentCode eq 'CHQ'}">
						 		<div style="padding: 3px;">
									<label class="width30">Account No</label>
									<span class="width60">${PAYMENT_INFO_ALERT.bankAccount.bank.bankName} [${PAYMENT_INFO_ALERT.bankAccount.accountNumber}]</span>
									<input type="hidden" readonly="readonly" name="accountId" id="accountId" value="${PAYMENT_INFO_ALERT.bankAccount.bankAccountId}"/>
								</div>
								<div style="padding: 3px;">
									<label class="width30">Cheque No</label> 
									<span class="width60">${PAYMENT_INFO_ALERT.chequeNumber}</span>
								</div>
								<div style="padding: 3px;">
									<label class="width30">Cheque Date</label> 
									<span class="width60">${PAYMENT_INFO_ALERT.chequeDate}</span>
								</div>
						 	</c:when>
						 	<c:when test="${PAYMENT_INFO_ALERT.paymentCode eq 'CCD' || PAYMENT_INFO_ALERT.paymentCode eq 'DCD'}">
						 		<div style="padding: 3px;">
									<label class="width30">Card No</label>
									<span class="width60">${PAYMENT_INFO_ALERT.bankAccount.bank.bankName} [${PAYMENT_INFO_ALERT.bankAccount.cardNumber}]</span>
									<input type="hidden" readonly="readonly" name="cardAccountId" id="cardAccountId" value="${PAYMENT_INFO_ALERT.bankAccount.bankAccountId}"/>
								</div> 	 
						 	</c:when>
						 	<c:when test="${PAYMENT_INFO_ALERT.paymentCode eq 'IAT'}">
						 		<div style="padding: 3px;">
									<label class="width30">Transfer From</label>
									<span class="width60">${PAYMENT_INFO_ALERT.bankAccount.bank.bankName} [${PAYMENT_INFO_ALERT.bankAccount.accountNumber}]</span>
									<input type="hidden" readonly="readonly" name="transferAccountId" id="transferAccountId" value="${PAYMENT_INFO_ALERT.bankAccount.bankAccountId}"/>
								</div> 	 
						 	</c:when>
						 </c:choose> 
					</fieldset>
				</div>
				<div class="width45 float-right" id="hrm">
					<fieldset> 
						<div style="padding: 3px;">
							<label class="width30">Payment Date</label> 
							 <c:set var="payDate" value="${PAYMENT_INFO_ALERT.date}"/>  
							<%String date = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
							<span class="width60 view"><%=date%></span> 
						</div>  
						<div style="padding: 3px;">
							<label class="width30">Payable No.</label> 
							<span class="width60 view">${PAYMENT_INFO_ALERT.supplier.supplierNumber}</span>
						</div>
						<div style="padding: 3px;">
							<label class="width30">Payable</label> 
							<c:choose>
								<c:when test="${PAYMENT_INFO_ALERT.supplier.person ne null && PAYMENT_INFO_ALERT.purchase.supplier.person ne '' }">
									<span class="width60 view">${PAYMENT_INFO_ALERT.supplier.person.middleName} ${PAYMENT_INFO_ALERT.supplier.person.lastName}</span>
								</c:when>
								<c:when test="${PAYMENT_INFO_ALERT.supplier.company ne null && PAYMENT_INFO_ALERT.supplier.company ne '' }">
									<span class="width60 view">${PAYMENT_INFO_ALERT.supplier.company.companyName}</span>
								</c:when>
								<c:when test="${PAYMENT_INFO_ALERT.supplier.cmpDeptLocation ne null && PAYMENT_INFO_ALERT.supplier.cmpDeptLocation ne '' }">
									<span class="width60 view">${PAYMENT_INFO_ALERT.supplier.cmpDeptLocation.company.companyName}</span>
								</c:when>
								<c:otherwise>
									<span class="width60 view"></span>
								</c:otherwise>
							</c:choose> 
						</div>  
						 
					</fieldset>
				</div> 
			</div>
		
			<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
				<fieldset> 	
					<legend>Payment Information</legend>
					 <table id="hastab" class="width100"> 
								<thead>
								   <tr>   
								   	    <th style="width:1%">S.No</th>
								   	    <th>Product Code</th>   
									    <th>Product Description</th>  
									    <th>Units</th> 
									    <th>Quantity</th>  
									    <th>Price</th>   
									    <th>Total Amount</th>  
								  </tr>
								</thead> 
								<tbody class="tab">  
									<c:choose>
										<c:when test="${INVOICE_DETAIL_LIST ne null && INVOICE_DETAIL_LIST ne ''}">
											<c:forEach var="PAYMENT_DETAIL" items="${INVOICE_DETAIL_LIST}" varStatus="status"> 
												<tr class="rowid" id="fieldrow_${status.index+1}"> 
													<td id="lineId_${status.index+1}">${status.index+1}</td> 
													<td>
														${PAYMENT_DETAIL.product.code}
													</td> 
													 <td> 
														${PAYMENT_DETAIL.product.productName}
													</td>
													 <td> 
														${PAYMENT_DETAIL.product.lookupDetailByProductUnit.displayName}
													</td>
													<td>
														${PAYMENT_DETAIL.invoiceQty}
													</td> 
													<td style="text-align: right;"> 
														<c:set var="unitRate" value="${PAYMENT_DETAIL.receiveDetail.unitRate}"/>
														<%=AIOSCommons.formatAmount(pageContext.getAttribute("unitRate"))%>
													</td> 
													<td style="text-align: right;">
														<c:set var="totalAmount" value="${PAYMENT_DETAIL.invoiceQty * PAYMENT_DETAIL.receiveDetail.unitRate}"/>
														<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount"))%>
													</td> 
												</tr>
											</c:forEach>
										</c:when>
									</c:choose> 
									
						 </tbody>
					</table>
					</fieldset>
			</div>		
			</div> 
			</form> 
			</div> 
			<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
				<div class="portlet-header ui-widget-header float-right" id="pay_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
				<div class="portlet-header ui-widget-header float-right" id="pay_print" style="cursor:pointer;">Print</div>
			 </div>  
		</div>
</div>