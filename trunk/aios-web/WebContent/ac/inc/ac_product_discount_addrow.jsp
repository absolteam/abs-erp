<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}" style="display: none;">${rowId}</td>
	<td><input type="text"
		class="width96 flatpercentage validate[optional,custom[number]]"
		id="flatpercentage_${rowId}"  /></td>
	<td><input type="text"
		class="width96 flatamount validate[optional,custom[number]]"
		id="flatamount_${rowId}"  /></td>
	<td><input type="text"
		class="width96 salesamount validate[optional,custom[number]]"
		id="salesamount_${rowId}"
		style=" text-align: right;" /></td>
	<td style="display: none;"><input type="text"
		class="width96 discountamount validate[optional,custom[number]]"
		id="discountamount_${rowId}"
		style=" text-align: right;" /></td>
	<td style="display: none;"><input type="text"
		class="width96 discountpercentage validate[optional,custom[number]]"
		id="discountpercentage_${rowId}"
		style=" text-align: right;" /></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${rowId}" class="width96" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}"
		style="display: none; cursor: pointer;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" id="discountMethodId_${rowId}" /></td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#product_discount_details").validationEngine('attach'); 
	});
</script>