<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
$(function(){
	$('.product-search').combobox({
		selected : function(event, ui) {
			var productId = Number($(this).val());
 			var productName = $.trim($('.product-search :selected').text());
 			var previewObject = { 
					"productId" : productId,
					"productName" : productName 
			};
			addProductPreview(previewObject);
		}
	}); 
	var tdsize = $($("#subcategory-table>tbody>tr:first").children()).size();
	if (tdsize == 1) {
		$($("#subcategory-table>tbody>tr:first").children()).each(
				function() {
					$(this).addClass('float-left width25');
				});
	}
});
</script>
<fieldset
	style="max-height: 95px; overflow-y: auto; overflow-x: hidden; padding: 1px !important;">
	<legend>
		<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sub
		Category
	</legend>
	<div class="width100 content-item-table"
		style="overflow: hidden !important;">
		<table id="subcategory-table" class="pos-item-table width100">
			<c:choose>
				<c:when
					test="${PRODUCT_SUB_CATEGORIES ne null && fn:length(PRODUCT_SUB_CATEGORIES) > 0}">
					<c:set var="row" value="0" />
					<c:forEach begin="0" step="5" items="${PRODUCT_SUB_CATEGORIES}">
						<tr>
							<!-- TD 1 -->
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="subCategory">${PRODUCT_SUB_CATEGORIES[categoryRow+0].categoryName}
										<input type="hidden"
										id="categoryId_${PRODUCT_SUB_CATEGORIES[categoryRow+0].productCategoryId}"
										class="categoryId" /> </span>
								</div>
							</td>
							<!-- TD 2 -->
							<c:if test="${PRODUCT_SUB_CATEGORIES[categoryRow+1]!= null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="subCategory">${PRODUCT_SUB_CATEGORIES[categoryRow+1].categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUB_CATEGORIES[categoryRow+1].productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 3 -->
							<c:if test="${PRODUCT_SUB_CATEGORIES[categoryRow+2]!= null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="subCategory">${PRODUCT_SUB_CATEGORIES[categoryRow+2].categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUB_CATEGORIES[categoryRow+2].productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 4 -->
							<c:if test="${PRODUCT_SUB_CATEGORIES[categoryRow+3]!= null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="subCategory">${PRODUCT_SUB_CATEGORIES[categoryRow+3].categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUB_CATEGORIES[categoryRow+3].productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 5 -->
							<c:if test="${PRODUCT_SUB_CATEGORIES[categoryRow+4]!= null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="subCategory">${PRODUCT_SUB_CATEGORIES[categoryRow+4].categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUB_CATEGORIES[categoryRow+4].productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
							</c:if>
							<c:set var="categoryRow" value="${categoryRow + 5}" />
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<div>
						<span style="color: red;">No sub category has found.</span>
					</div>
				</c:otherwise>
			</c:choose>
		</table>
	</div>
</fieldset>