<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Purchase Invoice</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>PURCHASE INVOICE</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 150px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<div class="width40 float_right">
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">GRN<span class="float_right">:</span> </span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
						<span style="color: #CC2B2B;">
						 <c:choose>
						 	<c:when test="${RECEIVE_NUMBER ne null && RECEIVE_NUMBER ne ''}">
						 		${RECEIVE_NUMBER}
						 	</c:when>
						 	<c:otherwise>-N/A-</c:otherwise>
						 </c:choose>  </span>
					</span>
			</div>	
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">REF.NO<span class="float_right">:</span> </span>
					<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
						<span style="color: #CC2B2B;">
						 <c:choose>
						 	<c:when test="${REFERENCE_NUMBER ne null && REFERENCE_NUMBER ne ''}">
						 		${REFERENCE_NUMBER}
						 	</c:when>
						 	<c:otherwise>-N/A-</c:otherwise>
						 </c:choose>  </span>
					</span>
			</div>	
			 <div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">INV.NO.<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${INVOICE_INFO_PRINT.invoiceNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">DATE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="date" value="${INVOICE_INFO_PRINT.date}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
				</span>  		
			</div> 
		</div> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">NAME <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:choose>
						<c:when test="${INVOICE_INFO_PRINT.supplier.person ne null && INVOICE_INFO_PRINT.supplier.person ne ''}">
							${INVOICE_INFO_PRINT.supplier.person.firstName} ${INVOICE_INFO_PRINT.supplier.person.lastName} 
							 <c:set var="supplierType" value="Person"/>
						</c:when>
						<c:when test="${INVOICE_INFO_PRINT.supplier.company ne null && INVOICE_INFO_PRINT.supplier.company ne ''}">
							${INVOICE_INFO_PRINT.supplier.company.companyName}
							 	<c:set var="supplierType" value="Company"/>
						</c:when>
						<c:otherwise>
							${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyName}
							<c:set var="supplierType" value="Company"/>
						</c:otherwise>
					</c:choose>
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">ADDRESS <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:choose> 
						<c:when test="${INVOICE_INFO_PRINT.supplier.person ne null && INVOICE_INFO_PRINT.supplier.person ne ''}">
							<c:choose> 
								<c:when test="${INVOICE_INFO_PRINT.supplier.person.postboxNumber  ne null && INVOICE_INFO_PRINT.supplier.person.postboxNumber ne '' }">
									P.O.Box: ${INVOICE_INFO_PRINT.supplier.person.postboxNumber}
									<c:choose>
										<c:when test="${INVOICE_INFO_PRINT.supplier.person.residentialAddress  ne null && INVOICE_INFO_PRINT.supplier.person.residentialAddress ne '' }">
											, ${INVOICE_INFO_PRINT.supplier.person.residentialAddress}  
										</c:when>	
									</c:choose>
								</c:when> 
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:when test="${INVOICE_INFO_PRINT.supplier.company ne null && INVOICE_INFO_PRINT.supplier.company ne ''}">
							<c:choose> 
								<c:when test="${INVOICE_INFO_PRINT.supplier.company.companyPobox  ne null && INVOICE_INFO_PRINT.supplier.company.companyPobox ne '' }">
									P.O.Box: ${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox}
									<c:choose>
										<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress  ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress ne '' }">
											, ${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress}  
										</c:when>	
									</c:choose>
								</c:when> 
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:otherwise> 
							<c:choose> 
								<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox ne ''}">
									P.O.Box: ${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPobox}
									<c:choose>
										<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress ne ''}">
											, ${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyAddress} 
										</c:when> 
									</c:choose>
								</c:when> 
								<c:otherwise> 
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:otherwise>	 
					</c:choose>			
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">MOBILE NO <span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:choose>
						<c:when test="${INVOICE_INFO_PRINT.supplier.person ne null && INVOICE_INFO_PRINT.supplier.person ne ''}">
							<c:choose>
								<c:when test="${INVOICE_INFO_PRINT.supplier.person.mobile ne null && INVOICE_INFO_PRINT.supplier.person.mobile ne ''}">
									${INVOICE_INFO_PRINT.supplier.person.mobile} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose> 
						</c:when>
						<c:when test="${INVOICE_INFO_PRINT.supplier.company ne null && INVOICE_INFO_PRINT.supplier.company ne ''}">
							<c:choose>
								<c:when test="${INVOICE_INFO_PRINT.supplier.company.companyPhone ne null && INVOICE_INFO_PRINT.supplier.company.companyPhone ne ''}">
									${INVOICE_INFO_PRINT.supplier.company.companyPhone}
								</c:when> 
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company ne ''}">
							<c:choose>
								<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone ne ''}">
									${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyPhone} 
								</c:when> 
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose>
				</span>
			</div>
			<div class="width100 div_text_info"> 
				<c:choose>
					<c:when test="${INVOICE_INFO_PRINT.supplier.person ne null && INVOICE_INFO_PRINT.supplier.person ne ''}">
						<c:choose>
							<c:when test="${INVOICE_INFO_PRINT.supplier.person.telephone ne null && INVOICE_INFO_PRINT.supplier.person.telephone ne ''}">
							 	<span class="float_left left_align width28 text-bold">TEL NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${INVOICE_INFO_PRINT.supplier.person.telephone}</span>
							</c:when>
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">TEL NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose> 
					</c:when>
					<c:when test="${INVOICE_INFO_PRINT.supplier.company ne null && INVOICE_INFO_PRINT.supplier.company ne ''}">
						<c:choose>
							<c:when test="${INVOICE_INFO_PRINT.supplier.company.companyFax ne null && INVOICE_INFO_PRINT.supplier.company.companyFax ne ''}">
								<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${INVOICE_INFO_PRINT.supplier.company.companyFax}</span>
							</c:when> 
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span></span> 
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>	
					</c:when>  
					<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company ne ''}">
						<c:choose>
							<c:when test="${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax ne null && INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax ne ''}">
								<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span> </span>
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">${INVOICE_INFO_PRINT.supplier.cmpDeptLocation.company.companyFax}</span>
							</c:when> 
							<c:otherwise>
								<span class="float_left left_align width28 text-bold">FAX NO <span class="float_right">:</span></span> 
								<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">-N/A-</span>
							</c:otherwise>
						</c:choose>	
					</c:when>  
				</c:choose> 
			</div> 
		</div> 
	</div>
	<div style="margin-top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<table class="width100">
			<tr> 
				<th style="width:2%;">S.NO:</th>
				<th class="width10">CODE</th>
				<th class="width30">DESCRIPTION</th>
				<th class="width10">QTY</th>
				<th class="width10"></th>
			</tr>
			<tbody>  
				<c:choose>
					<c:when test="${INVOICE_DETAIL_INFO_PRINT ne null && INVOICE_DETAIL_INFO_PRINT ne ''}">
						<c:set var="totalAmount" value="0"/>
						<tr style="height: 25px;">
							<c:forEach begin="0" end="4" step="1">
								<td/>
							</c:forEach>
						</tr>
						<c:forEach items="${INVOICE_DETAIL_INFO_PRINT}" var="INVOICE_DETAIL" varStatus="status"> 
							<tr>
								<td>${status.index+1}</td>
								<td>${INVOICE_DETAIL.product.code}</td>
								<td>${INVOICE_DETAIL.product.productName} - ${INVOICE_DETAIL.product.lookupDetailByProductUnit.displayName}</td>
								<td>
									<c:choose>
										<c:when test="${INVOICE_DETAIL.invoiceQty ne null}">
											${INVOICE_DETAIL.invoiceQty}
										</c:when>
										<c:otherwise>
											${INVOICE_DETAIL.receiveDetail.receiveQty}
										</c:otherwise>
									</c:choose>
								</td> 
								<td></td> 
							</tr>
						</c:forEach> 
						<c:if test="${fn:length(INVOICE_INFO_PRINT.invoiceDetails)<5}">
							<c:set var="emptyLoop" value="${5 - fn:length(INVOICE_INFO_PRINT.invoiceDetails)}"/> 
							<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
								<tr style="height: 25px;">
									<c:forEach begin="0" end="4" step="1">
										<td/>
									</c:forEach>
								</tr>
							</c:forEach>
						</c:if> 
						<tr>
							<td colspan="4" class="bottomTD left_align" style="font-weight: bold;">INVOICE AMOUNT</td> 
							<td class="right_align bottomTD" style="font-weight: bold;">
								<c:set var="totalAmount" value="${INVOICE_INFO_PRINT.invoiceAmount}"/>
								<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalAmount")) %> 
							</td>
						</tr>
						<tr>
							 
							<%
								double netAmount = (Double.valueOf(pageContext.getAttribute("totalAmount").toString()));
							%>
							<td colspan="4" class="left_align"><span style="font-weight: bold;"> AMOUNT IN WORDS : </span>
								<%
								 	String decimalAmount = String.valueOf(AIOSCommons
								 					.formatAmount(netAmount));
								 			decimalAmount = decimalAmount.substring(decimalAmount
								 					.lastIndexOf('.') + 1);
								 			String amountInWords = AIOSCommons
								 					.convertAmountToWords(netAmount);
								 			if (Double.parseDouble(decimalAmount) > 0) {
								 				amountInWords = amountInWords.concat(" and ") 
								 						.concat(AIOSCommons
								 								.convertAmountToWords(decimalAmount))
								 						.concat(" Fils ");
								 				String replaceWord = "Only";
								 				amountInWords = amountInWords.replaceAll(replaceWord,
								 						"");
								 				amountInWords = amountInWords.concat(" " + replaceWord);
								 			}
								 %> <%=amountInWords%>
							</td> 
						</tr>
					</c:when>
				</c:choose> 
			</tbody>
		</table>
	</div> 
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">INV.DESCRIPTION<span class="float_right">:</span> </span>
		<span class="span_border left_align text_info" style="width:77%;display: -moz-inline-stack;">
			<c:choose>
				<c:when test="${INVOICE_INFO_PRINT.description ne null && INVOICE_INFO_PRINT.description ne ''}">
					${INVOICE_INFO_PRINT.description}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose>  
		</span>
	</div>
	<div class="width100 float_left div_text_info" style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span> 
			</div>  
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div> 
		</div>
	</div>
	</body>
</html>