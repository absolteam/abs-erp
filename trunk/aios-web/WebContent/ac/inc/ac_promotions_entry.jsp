<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var slidetab="";
var promotionMethods = "";
var promotionOptions = new Object();
var accessCode = "";
	$(function(){  
		 $jquery("#product_promotion_details").validationEngine('attach'); 

	manupulateLastRow();

	$('#fromDate,#toDate').datepick({
		showTrigger: '#calImg'}); 

	$('#promotion-discard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_promotions.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('destroy');		
											$('#common-popup').remove();   
											$("#main-wrapper").html(result);
										}
									});
						}); 
		

		$('.productdiscount-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id"); 
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.common-result').html(result);
		                  $('#common-popup').dialog('open');
		  				   $($($('#common-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.common-result').html(result);
		             }
		         });
	       return false;
		});	

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="PRODUCT_DISCOUNT_NAME"){
				$('#discountName').html("");
				$('#discountName').append("<option value=''>Select</option>");
				loadLookupList("discountName"); 
			}  
		});

		$('#common-popup').dialog({
			 autoOpen: false,
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$('#product-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });

		$('#customer-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });

		$('#promotionOption').change(function(){
			var promotionOption = $(this).val(); 
			$("#view_option").hide();
			if(promotionOption!=null && promotionOption!= ''){
				$(".common-result").html('');
				$('.ui-dialog-titlebar').remove();
				promotionOptions = [];
				var actionName = "";
				if(promotionOption == 1)
					actionName = "show_product_common_popup";
				else if(promotionOption == 2)
					actionName = "show_product_multiple_popup";
				else if(promotionOption == 3)
					actionName = "show_all_category_popup";
				else if(promotionOption == 4)
					actionName = "show_all_sub_category_popup";
				else if(promotionOption == 5)
					actionName = "common_multiselect_customer_list";
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/"+actionName+".action",
											async : false,
											dataType : "html",
											data : {rowId: Number(0), itemType: 'I'},
											cache : false,
											success : function(result) { 
												$(".common-result").html(result);
												$('#common-popup').dialog('open');
												$($($('#common-popup').parent()).get(0)).css('top',0);
											}
										});
							}
							return false;
						});

		$('.addrows')
				.click(
						function() {
							var i = Number(1);
							var id = Number(getRowId($(".tab>.rowid:last")
									.attr('id')));
							id = id + 1;
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/promotion_addrow.action", 
				 	async: false,
				 	data:{rowId: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tab tr:last').before(result);
						 if($(".tab").height()>255)
							 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowid').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#lineId_'+rowId).html(i);
							 i=i+1; 
				 		 }); 
					}
				});
			  return false;
		 }); 

		 $('.delrow').live('click',function(){ 
			 slidetab = $(this).parent().parent().get(0);  
	       	 $(slidetab).remove();  
	       	 var i=1;
	    	 $('.rowid').each(function(){   
	    		 var rowId = getRowId($(this).attr('id')); 
	    		 $('#lineId_'+rowId).html(i);
				 i=i+1; 
	 		 });   
		 });

		 $('.common-popup').live('click',function(){  
			   $('.ui-dialog-titlebar').remove(); 
 			//	var rowId = getRowId($(this).attr('id')); 
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_common_coupon_popup.action", 
				 	async: false,  
				 	data:{rowId: -1},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0);
						$('.common-result').html(result);  
					},
					error:function(result){  
						 $('.common-result').html(result); 
					}
				});  
				return false;
			});

		 

		 $('#promotion_option_popup').dialog({
		 		autoOpen: false,
		 		width: 800,
		 		height: 400,
		 		bgiframe: true,
		 		modal: true,
		 		buttons: {
		 			"OK": function(){  
		 				$(this).dialog("close"); 
		 			} 
		 		}
		 	});

		 $('#view_option').click(function(){   
 			  $('.ui-dialog-titlebar').remove();
				var htmlString = "";
							htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>"
									+ promotionOptions.selectiontitle + "</div>";
									var count = 0;
							
				$(promotionOptions.record)
									.each(
											function(index) {
												count += 1;
												htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
														+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
														+ count
														+ ".</span>"
														+ "<span class='width20 float-left'>"
														+ promotionOptions.record[index].recordName
														+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
											});

							$('#promotion_option_popup').dialog('open');

							$('#promotion_option_popup').html(htmlString);
						});

		 $('.deleteoption')
			.live('click',
					function() {
				var rowId = getRowId($(this).attr('id')); 
  				var count_index =  $('#countindex_'+rowId).attr('id'); 
				$('#div_'+rowId).remove(); 
				promotionOptions.record.splice(count_index.split('.')[0], 1); 
				var i = 0;
				$('.count_index')
				.each(
						function() {
							i = i +1;
					$(this).text(i+".");		
				});
				if(promotionOptions.record.length == 0){
					$('#view_option').hide();
					$('#promotionOption').val('');
					$('#promotion_option_popup').dialog("close");
				} 
				return false;
		 }); 

		$('#promotion-save')
				.click(
						function() {

							if($jquery("#product_promotion_details").validationEngine('validate')){
								if(promotionOptions!=null && promotionOptions!= ''){  
								var promotionId = Number($(
										'#promotionId').val());
								var promotionName = $('#promotionName').val(); 
								var discountOnPrice = Number($('#discountOnPrice').val());
								var status = Number($('#status').val());
								var rewardType =  Number($('#rewardType').val());
								var fromDate = $('#fromDate').val();
								var toDate = $('#toDate').val();
								var minimumSales = $('#minimumSales').val();
								var salesValue = Number($('#salesValue').val());
 								var promotionOption = Number($(
										'#promotionOption').val());
								var description = $('#description').val();
								promotionMethods = getPromotionMethods(); 
								 
 								if (promotionMethods != null
										&& promotionMethods != "") { 
									$
											.ajax({
												type : "POST",
												url : "<%=request.getContextPath()%>/save_promotion.action", 
						 	async: false,
						 	data : { promotionId: promotionId, promotionName: promotionName, rewardType: rewardType, discountOnPrice: discountOnPrice, 
						 			 status: status, fromDate: fromDate, toDate: toDate, minimumSales: minimumSales, rewardType: rewardType,
						 			 salesValue: salesValue, promotionOption: promotionOption, description: description,
						 			 promotionMethods: promotionMethods, aaData: JSON.stringify(promotionOptions)
							 	   },
						    dataType: "json",
						    cache: false,
							success:function(response){
								console.debug(response);
								if(response.returnMessage == "SUCCESS"){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();   
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/show_promotions.action",
																		async : false,
																		dataType : "html",
																		cache : false,
																		success : function(
																				result) {
																			$(
																					"#main-wrapper")
																					.html(
																							result);
																			if (promotionId > 0)
																				$(
																						'#success_message')
																						.hide()
																						.html(
																								"Record updated")
																						.slideDown(
																								1000);
																			else
																				$(
																						'#success_message')
																						.hide()
																						.html(
																								"Record created")
																						.slideDown(
																								1000);
																			$(
																					'#success_message')
																					.delay(
																							3000)
																					.slideUp();
																		}
																	});
														} else {
															$('#page-error')
																	.hide()
																	.html(
																			response.returnMessage)
																	.slideDown(
																			1000);
															$('#page-error')
																	.delay(3000)
																	.slideUp();
															return false;
														}
													},
													error : function(result) {
														$('#page-error')
																.hide()
																.html(
																		"Save failure")
																.slideDown(1000);
														$('#page-error').delay(
																3000).slideUp();
													}
												});
									} else {
										$('#page-error')
												.hide()
												.html(
														"Please enter promotion methods.")
												.slideDown(1000);
										$('#page-error').delay(3000).slideUp();
										return false;
									}
								} else {
									$('#page-error').hide().html(
											"Please enter promotion option.")
											.slideDown(1000);
									$('#page-error').delay(3000).slideUp();
									return false;
								}
							} else {
								return false;
							}
						});

		var getPromotionMethods = function() {
			promotionMethods = "";
			var promotionTypeArray = new Array();
			var calculationTypeArray = new Array();
			var promotionArray = new Array();
			var couponIdArray = new Array();
			var methodIdArray = new Array();  
			var promotionType = Number($('#rewardType')
					.val());
			var calculationType = $('#calculationType')
					.val();
			var promotion = Number($('#promotion').val());
			var couponId = Number($('#couponId').val());
			var promotionMethodId = Number($(
					'#promotionMethodId_1').val()); 
			if (promotionType > 0 || calculationType != ''
					|| promotion > 0 || couponId > 0) {
				promotionTypeArray.push(promotionType);
				if (calculationType != null
						&& calculationType != "")
					calculationTypeArray.push(calculationType);
				else
					calculationTypeArray.push("##");
				couponIdArray.push(couponId);
				promotionArray.push(promotion); 
				methodIdArray.push(promotionMethodId); 
			} 
			
			for ( var j = 0; j < promotionTypeArray.length; j++) {
				promotionMethods += promotionTypeArray[j] + "__"
						+ calculationTypeArray[j] + "__" + promotionArray[j]
						+ "__" + couponIdArray[j] + "__"
						+ methodIdArray[j];
				if (j == promotionTypeArray.length - 1) {
				} else {
					promotionMethods += "#@";
				}
			}
			return promotionMethods;
		};

		if (Number($('#promotionId').val()) > 0) {
			$('#rewardType').val($('#temppackTyp_1').val());
			$('#calculationType').val($('#tempcalculationType_1').val());
			if(Number($('#promotion_1').val())>0)
				$('#promotion').val($('#promotion_1').val());
			else
				$('#promotion').val($('#promotionPoints_1').val());
			$('#couponId').val($('#couponId_1').val());
			$('#coupon').val($('#coupon_1').val());
			$('#discountOnPrice').val($('#tempdiscountOnPrice').val());
			$('#minimumSales').val($('#tempminimumSales').val());
			$('#promotionOption').val($('#temppromotionOption').val());

			$('.rowid')
					.each(
							function() {
								var rowId = getRowId($(this).attr('id'));
								if (Number($('#promotionMethodId_' + rowId)
										.val() > 0)) {
									$('#promotionType_' + rowId).val(
											$('#temppackTyp').val());
									$('#calculationType_' + rowId).val(
											$('#tempcalculationType').val());
								}
							});
			var jsonData = [];

			$('.discountOptions').each(function() {
				var rowId = getRowId($(this).attr('id'));
				jsonData.push({
					"recordId" : Number($('#recordbyid_' + rowId).val()),
					"recordName" : $('#recordbynameid_' + rowId).val(),
					"entityName" : $('#recordbytableid_' + rowId).val()
				});
			});

			if ('${PROMOTION_INFO.status}' == "true")
				$('#status').val(1);
			else
				$('#status').val(0);
			$('#view_option').show();
			var selectiontitle = "";
			if ($('#promotionOption').val() == '1')
				selectiontitle = "Single Product Discount";
			else if ($('#promotionOption').val() == '2')
				selectiontitle = "Multiple Product Discount";
			else if ($('#promotionOption').val() == '5')
				selectiontitle = "Customer Discount";
			var jsonObject = {
				"selectiontitle" : selectiontitle,
				"record" : jsonData
			};
			setJsonDiscountData(jsonObject);
		}
	});

	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function triggerAddRow(rowId) {
		var nexttab = $('#fieldrow_' + rowId).next();
		if ($(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function selectedCustomerList(customerData) {  
		var jsonData = [];
		$(customerData)
		.each(
				function(index) {
					jsonData.push({ 
						"recordId" : Number(customerData[index].customerId),
						"recordName" : customerData[index].customerName,
						"entityName" : "Customer"
					}); 
				}); 
		var jsonObject = {
				"selectiontitle" : "Customer Discount",
				"record" : jsonData
			};
		setJsonDiscountData(jsonObject);
	}
	
	function commonProductPopup(productId, productName, rowId) { 
		var jsonObject = {
			"selectiontitle" : "Single Product Discount",
			"record" : [ {
				"recordId" : Number(productId),
				"recordName" : productName,
				"entityName" : "Product"
			} ]
		};
		setJsonDiscountData(jsonObject);
	}
	
	function setJsonDiscountData(jsonData) {
		promotionOptions = jsonData;
		if (promotionOptions.record.length > 0)
			$('#view_option').show();
	}
	function commonCouponMethod(couponId, couponName, params) { 
		$('#coupon').val(couponName);
		$('#couponId').val(couponId);
		$('#common-popup').dialog("close");
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			promotion
		</div> 
		<form name="product_promotion_details" id="product_promotion_details" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="promotionId" name="promotionId"
					value="${PROMOTION_INFO.promotionId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 195px;">
							<div>
								<div>
									<label class="width30" for="fromDate">From Date<span class="mandatory">*</span> </label>
									<c:choose>
										<c:when
											test="${PROMOTION_INFO.fromDate ne null && PROMOTION_INFO.fromDate ne ''}">
											<c:set var="fromDate" value="${PROMOTION_INFO.fromDate}" />
											<%
												String fromDate = DateFormat
																.convertDateToString(pageContext.getAttribute(
																		"fromDate").toString());
											%>
											<input type="text" readonly="readonly" name="fromDate"
												id="fromDate" class="width50 validate[required]" value="<%=fromDate%>" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="fromDate"
												id="fromDate" class="width50 validate[required]" />
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30" for="toDate">To Date<span class="mandatory">*</span> </label>
									<c:choose>
										<c:when
											test="${PROMOTION_INFO.toDate ne null && PROMOTION_INFO.toDate ne ''}">
											<c:set var="toDate" value="${PROMOTION_INFO.toDate}" />
											<%
												String toDate = DateFormat.convertDateToString(pageContext
																.getAttribute("toDate").toString());
											%>
											<input type="text" readonly="readonly" name="toDate"
												id="toDate" class="width50 validate[required]" value="<%=toDate%>" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="toDate"
												id="toDate" class="width50 validate[required]" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div>
								<label class="width30" for="promotionOption">Promotion
									Option<span class="mandatory">*</span> </label> <select
									name="promotionOption" id="promotionOption"
									class="width50 validate[required] float-left">
									<option value="">Select</option>
									<c:forEach var="promotionOption" items="${PROMOTION_OPTIONS}">
										<option value="${promotionOption.key}">${promotionOption.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="temppromotionOption"
									value="${PROMOTION_INFO.promotionOption}" /> <span
									style="cursor: pointer; position: relative; top: 8px; display: none; float: left;
											width:17px; height:17px; background-image: url('./images/icons/view.ico');background-repeat: no-repeat;" id="view_option"></span>
							</div>
							<div>
								<label class="width30" for="minimumSales">Minimum Sales</label>
								<select name="minimumSales" id="minimumSales" class="width51">
									<option value="">Select</option>
									<option value="Q">Quantity</option>
									<option value="A">Amount</option>
								</select> <input type="hidden" id="tempminimumSales"
									value="${PROMOTION_INFO.miniumSales}" />
							</div>
							<div>
								<label class="width30" for="salesValue">Minimum Value</label> <input
									type="text" name="salesValue" id="salesValue"
									value="${PROMOTION_INFO.salesValue}" class="width50" />
							</div>
							
							<div>
								<label class="width30">Description</label>
								<textarea id="description" name="description" class="width50">${PROMOTION_INFO.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 195px;">
							<div>
								<label class="width30" for="promotionName">Promotion
									Name <span class="mandatory">*</span> </label> <input type="text"
									name="promotionName" id="promotionName"
									value="${PROMOTION_INFO.promotionName}"
									class="width50 validate[required]" />
							</div> 
							
							<div>
								<label class="width30" for="discountOnPrice">Price Based<span
									class="mandatory">*</span> </label> <select name="discountOnPrice"
									id="discountOnPrice" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="discountOnPrice" items="${DISCOUNT_ON_PRICE}">
										<option value="${discountOnPrice.lookupDetailId}">${discountOnPrice.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempdiscountOnPrice"
									value="${PROMOTION_INFO.lookupDetail.lookupDetailId}">
							</div>
							
							<div>
								<label class="width30" for="rewardType">Reward Type<span
									class="mandatory">*</span> </label> <select name="rewardType"
									id="rewardType" class="width51">
									<option value="">Select</option>
									<c:forEach var="rewardType" items="${PROMOTION_REWARD_TYPES}">
										<option value="${rewardType.key}">${rewardType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="temppromotionType"
									value="${PROMOTION_INFO.rewardType}">
							</div>
							<div>
								<label class="width30" for="calculationType">Calculation Type</label>
								<select name="calculationType"
											class="width51 calculationType validate[optional]"
									id="calculationType">
										<option value="">Select</option>
										<option value="O">Off</option>
										<option value="E">Extra</option>
								</select> <input type="hidden" id="tempcalculationType"
									value="">
							</div>
							<div>
								<label class="width30" for="promotion">Promotion</label> 
								<input type="text" class="width50 promotion validate[optional,custom[number]]" id="promotion" /> 
							</div>
							<div>
								<label class="width30" for="coupon">Coupon</label> 
								<input type="text" class="width50 coupon validate[optional]" id="coupon"/> 
								<span class="button"> <a
												style="cursor: pointer;" id="couponPopup"
												class="btn ui-state-default ui-corner-all common-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								<input type="hidden" id="couponId" >
							</div>
							<div>
								<label class="width30" for="status">Status<span
									class="mandatory">*</span> </label> <select name="status" id="status"
									class="width51 validate[required]">
									<option value="">Select</option>
									<option value="1" selected="selected">Active</option>
									<option value="0">Inactive</option>
								</select> <input type="hidden" id="tempstatus"
									value="${PROMOTION_INFO.status}">
							</div> 
							
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top: 10px; display: none;" id="hrm">
				<fieldset>
					<legend>Promotion method<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<input type="hidden" name="childCount" id="childCount"
							value="${fn:length(PROMOTION_INFO.promotionMethods)}" />
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%;">Promotion Type</th>
									<th style="width: 5%">Calculation Type</th>
									<th style="width: 5%">Promotion</th>
									<th style="width: 5%">Coupon</th>
									<th style="width: 5%">Promotion Points</th>
									<th style="width: 5%">Description</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="promotionMethod"
									items="${PROMOTION_INFO.promotionMethods}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
										<td><select name="promotionType"
											class="width96 promotionType validate[optional]"
											id="promotionType_${status.index+1}">
												<option value="">Select</option>
												<c:forEach var="packType" items="${PACK_TYPES}">
													<option value="${packType.key}">${packType.value}</option>
												</c:forEach>
										</select> <input type="hidden" id="temppackTyp"
											value="${promotionMethod.pacType}"></td>
										<td><select name="calculationType"
											class="width96 calculationType validate[optional]"
											id="calculationType_${status.index+1}">
												<option value="">Select</option>
												<option value="O">Off</option>
												<option value="E">Extra</option>
										</select> <input type="hidden" id="tempcalculationType"
											value="${promotionMethod.calculationType}">
										</td>
										<td><input type="text"
											class="width96 promotion validate[optional,custom[number]]"
											id="promotion_${status.index+1}"
											value="${promotionMethod.promotionAmount}"
											style=" text-align: right;" />
										</td>
										<td><span class="coupon" id="coupon_${status.index+1}">${promotionMethod.coupon.couponName}</span>
											<span class="button float-right"> <a
												style="cursor: pointer;" id="couponPopup_${status.index+1}"
												class="btn ui-state-default ui-corner-all common-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" id="couponId_${status.index+1}"
											value="${promotionMethod.coupon.couponId}">
										</td>
										<td><input type="text" name="promotionPoints"
											id="promotionPoints_${status.index+1}" class="width96"
											value="${promotionMethod.productPoint}" />
										</td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status.index+1}" class="width96"
											value="${promotionMethod.description}" />
										</td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" id="promotionMethodId_${status.index+1}"
											value="${promotionMethod.promotionMethodId}" />
										</td>
									</tr>
								</c:forEach>
								<c:forEach var="i"
									begin="${fn:length(PROMOTION_INFO.promotionMethods)}"
									end="${fn:length(PROMOTION_INFO.promotionMethods)+1}" step="1"
									varStatus="status1">
									<tr class="rowid" id="fieldrow_${status1.index+1}">
										<td id="lineId_${status1.index+1}" style="display: none;">${status1.index+1}</td>
										<td><select name="promotionType"
											class="width96 promotionType validate[optional]"
											id="promotionType_${status1.index+1}">
												<option value="">Select</option>
												<c:forEach var="packType" items="${PACK_TYPES}">
													<option value="${packType.key}">${packType.value}</option>
												</c:forEach>
										</select>
										</td>
										<td><select name="calculationType"
											class="width96 calculationType validate[optional]"
											id="calculationType_${status1.index+1}">
												<option value="">Select</option>
												<option value="O">Off</option>
												<option value="E">Extra</option>
										</select>
										</td>
										<td><input type="text"
											class="width96 promotion validate[optional,custom[number]]"
											id="promotion_${status1.index+1}"
											style=" text-align: right;" />
										</td>
										<td><span class="coupon" id="coupon_${status1.index+1}"></span>
											<span class="button float-right"> <a
												style="cursor: pointer;" id="couponPopup_${status1.index+1}"
												class="btn ui-state-default ui-corner-all common-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
											type="hidden" id="couponId_${status1.index+1}">
										</td>
										<td><input type="text" name="promotionPoints"
											id="promotionPoints_${status1.index+1}"
											class="width96 promotionPoints" />
										</td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status1.index+1}" class="width96" />
										</td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status1.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status1.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status1.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status1.index+1}"
											style="display: none; cursor: pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status1.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" id="promotionMethodId_${status1.index+1}" />
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right "
					id="promotion-discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="promotion-save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<c:if
				test="${PROMOTION_INFO.promotionOptions ne null && fn:length(PROMOTION_INFO.promotionOptions) > 0}">
				<c:forEach var="discountOption"
					items="${PROMOTION_INFO.promotionOptions}" varStatus="statusIndex">
					<span class="discountOptions"
						id="discountOptions_${statusIndex.index+1}" style="display: none;">
						<c:choose>
							<c:when
								test="${discountOption.product ne null && discountOption.product ne ''}">
								<input type="hidden" id="recordbyid_${statusIndex.index+1}"
									value="${discountOption.product.productId}" />
								<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
									value="${discountOption.product.productName}" />
								<input type="hidden" id="recordbytableid_${statusIndex.index+1}"
									value="Product" />
							</c:when>
							<c:when
								test="${discountOption.customer ne null && discountOption.customer ne ''}">
								<input type="hidden" id="recordbyid_${statusIndex.index+1}"
									value="${discountOption.customer.customerId}" />
								<c:if
									test="${discountOption.customer.personByPersonId ne null && discountOption.customer.personByPersonId ne ''}">
									<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
										value="${discountOption.customer.personByPersonId.firstName} ${discountOption.customer.personByPersonId.lastName}" />
								</c:if>
								<c:if
									test="${discountOption.customer.company ne null && discountOption.customer.company ne ''}">
									<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
										value="${discountOption.customer.company.companyName}" />
								</c:if>
								<input type="hidden" id="recordbytableid_${statusIndex.index+1}"
									value="Customer" />
							</c:when>
						</c:choose> </span>
				</c:forEach>
			</c:if>
			<div style="display: none;" id="promotion_option_popup"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>