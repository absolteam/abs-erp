<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<link href="${pageContext.request.contextPath}/css/tables.css" rel="stylesheet"  type="text/css"  media="all" />
<title>CHART OF ACCOUNTS REPORT</title>
<style>
.width96{width:96%!important;}
 .width100{width:100%!important};
</style>
<script type="text/javascript">
</script>
<body onload="window.print();"> 
<div class="hastable width100">  
	<table id="hastab" class="width100"> 
		<thead>
	    	<tr> 
	    		<th>No </th>  
	    		<th>Name</th> 
	    		<th>Code</th> 
	    		<th>Combination</th> 
	    		<th>Segment</th> 
	    		<th>Account Type</th>
	    	</tr>
	    </thead>	
		<%int i=0; %>
		<tbody class="tab">
		<c:choose>
			<c:when test="${COMBINATION_TREE_VIEW ne null && COMBINATION_TREE_VIEW ne '' }">
				<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}" varStatus="status">  
					<tr>
						<%i=i+1;%>
						<td><%=i%></td>
						<td>
							${bean.accountByCompanyAccountId.account}
						</td> 
						<td>
							${bean.accountByCompanyAccountId.code}
						</td>
						<td>
							${bean.accountByCompanyAccountId.code}
						</td>
						<td>
							${bean.accountByCompanyAccountId.segment.segment}
						</td>
						<td></td>
					<tr> 
					<c:choose>
						<c:when test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
							<c:forEach var="costbean" items="${bean.costVos}" varStatus="cstatus1">
								<tr>
									<%i=i+1;%>
									<td><%=i%></td>	
									<td>
										${costbean.accountByCostcenterAccountId.account}
									</td> 
									<td>
										${costbean.accountByCostcenterAccountId.code}
									</td>
									<td>
										${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}
									</td>
									<td>
										${costbean.accountByCostcenterAccountId.segment.segment}
									</td>
									<td></td>
								</tr> 
								<c:choose>
									<c:when test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
										<c:forEach var="naturalbean" items="${costbean.naturalVos}" varStatus="nstatus1">
											<tr>	
												<%i=i+1;%>
												<td><%=i%></td>	
												<td>
													${naturalbean.accountByNaturalAccountId.account}
												</td> 
												<td>
													${naturalbean.accountByNaturalAccountId.code}
												</td>
												<td>
													${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}
												</td>
												<td>
													${naturalbean.accountByNaturalAccountId.segment.segment}
												</td>
												<td>
													${naturalbean.accountType}
												</td>
											</tr> 
											<c:choose>
												<c:when test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
													<c:forEach var="analysisbean" items="${naturalbean.analysisVos}" varStatus="astatus1">
														<tr>	
															<%i=i+1;%>
															<td><%=i%></td>	
															<td>
																${analysisbean.accountByAnalysisAccountId.account}
															</td> 
															<td>
																${analysisbean.accountByAnalysisAccountId.code}
															</td>
															<td>
																${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}
															</td>
															<td>
																${analysisbean.accountByAnalysisAccountId.segment.segment}
															</td>
															<td>${naturalbean.accountType}</td>
														</tr> 
														<c:choose>
														   <c:when test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
														   		<c:forEach var="buffer1bean" items="${analysisbean.buffer1Vos}" varStatus="b1status">
														   			<tr>	
																		<%i=i+1;%>
																		<td><%=i%></td>	
																		<td>
																			${buffer1bean.accountByBuffer1AccountId.account}
																		</td> 
																		<td>
																			${buffer1bean.accountByBuffer1AccountId.code}
																		</td>
																		<td>
																			${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}
																		</td>
																		<td>
																			${buffer1bean.accountByBuffer1AccountId.segment.segment}
																		</td>
																		<td>${naturalbean.accountType}</td>
																	</tr>	
																	<c:choose>
														   				<c:when test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
														   					<c:forEach var="buffer2bean" items="${buffer1bean.buffer2Vos}" varStatus="b2status">
														   						<tr>	
																					<%i=i+1;%>
																					<td><%=i%></td>	
																					<td>
																						${buffer2bean.accountByBuffer2AccountId.account}
																					</td> 
																					<td>
																						${buffer2bean.accountByBuffer2AccountId.code}
																					</td>
																					<td>
																						${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}
																					</td>
																					<td>
																						${buffer2bean.accountByBuffer2AccountId.segment.segment}
																					</td>
																					<td>${naturalbean.accountType}</td>
																				</tr>	
														   					</c:forEach>
														   				</c:when>
														   			</c:choose>	
														   		</c:forEach>
														   	</c:when>
														</c:choose>   			
													</c:forEach>
												</c:when>
											</c:choose>
										</c:forEach>
									</c:when>
								</c:choose> 
							</c:forEach>
						</c:when>
					</c:choose>	 
				</c:forEach>
			</c:when>
		</c:choose>
		</tbody>
	</table>
   </div>	 
</body>