<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var salesOrderDetails="";
var chargesDetails = "";
var tempid = "";
var accessCode ="";
var sectionRowId = 0;
var tableRowId = 0;
var globalRowId=null;
$(function(){   
	  
	$jquery("#salesOrderValidation").validationEngine('attach'); 
	$jquery(".amount,.totalAmountH").number(true, 2);
	$('#orderDate,#expiryDate,#shippingDate').datepick({
		onSelect: customRange, showTrigger: '#calImg'}); 

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_sales_order.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
					$('#baseprice-popup-dialog').dialog('destroy');		
					$('#baseprice-popup-dialog').remove(); 
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/salesorder_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrowscharges').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabcharges>.rowidcharges:last").attr('id'))); 
		  id=id+1;  
		  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/salesorder_charges_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabcharges tr:last').before(result);
					 if($(".tabcharges").height()>255)
						 $(".tabcharges").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidcharges').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#chargesLineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.shippingmethod-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id");  
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.ordertype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id");  
	      $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.shippingterm-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	 $('.chargestype-lookup').live('click',function(){
		 $('.ui-dialog-titlebar').remove(); 
		 var str = $(this).attr("id");
			 accessCode = str.substring(0, str.lastIndexOf("_"));
			 sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
		    $.ajax({
		         type:"POST",
		         url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		         data:{accessCode: accessCode},
		         async: false,
		         dataType: "html",
		         cache: false,
		         success:function(result){ 
		              $('.common-result').html(result);
		              $('#common-popup').dialog('open');
						   $($($('#common-popup').parent()).get(0)).css('top',0);
		              return false;
		         },
		         error:function(result){
		              $('.common-result').html(result);
		         }
		     });
		 return false;
		});	
		
	//Lookup Data Roload call
	$('#save-lookup').live('click',function(){  
 		if(accessCode=="SHIPPING_TERMS"){
			$('#shippingTerm').html("");
			$('#shippingTerm').append("<option value=''>Select</option>");
			loadLookupList("shippingTerm"); 
		} else if(accessCode=="SHIPPING_SOURCE"){
			$('#shippingMethod').html("");
			$('#shippingMethod').append("<option value=''>Select</option>");
			loadLookupList("shippingMethod"); 
		} else if(accessCode=="SALES_ORDER_TYPE"){
			$('#orderType').html("");
			$('#orderType').append("<option value=''>Select</option>");
			loadLookupList("orderType"); 
		} else if(accessCode=="CHARGES_TYPE"){
			$('#chargesTypeId_'+sectionRowId).html("");
			$('#chargesTypeId_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("chargesTypeId_"+sectionRowId); 
		}  
	});

	 $('#salesorder_save').click(function(){
		 salesOrderDetails = "";
		 chargesDetails = "";
		 if($jquery("#salesOrderValidation").validationEngine('validate')){
 	 			var salesOrderId = Number($('#salesOrderId').val()); 
	 			var referenceNumber = $('#tempreferenceNumber').val();
	 			var orderDate = $('#orderDate').val(); 
	 			var expiryDate = $('#expiryDate').val();  
	 			var shippingDate = $('#shippingDate').val();  
	 			var shippingMethod = Number($('#shippingMethod').val());  
	 			var shippingTerm = Number($('#shippingTerm').val());  
 	 			var customerId = Number($('#customerId').val());  
	 			var salesPersonId = Number($('#salesPersonId').val());  
 	 			var paymentMode =  Number($('#paymentMode').val()); 
	 			var status = Number($('#status').val()); 
	 			var customerQuotationId = Number($('#customerQuotationId').val()); 
	 			var description=$('#description').val();  
	 			var currencyId = Number($('#currency').val());  
	 			var orderType = Number($('#orderType').val());
	 			var shippingDetailId = Number($('#shippingDetail').val());
	 			var continuousSales = $('#continuousSales').attr('checked');
	 			var allowSave = validateSalesOrderPrices();  
	 			var creditTerm = Number($('#creditTerm').val());  
	 		 
	 			if(allowSave == "true"){
	 				salesOrderDetails = getSalesOrderDetails();   
		   			if(salesOrderDetails!=null && salesOrderDetails!=""){
		   				chargesDetails = getSalesOrderCharges();   
		 				$.ajax({
		 					type:"POST",
		 					url:"<%=request.getContextPath()%>/save_sales_order.action", 
		 				 	async: false, 
		 				 	data:{	salesOrderId: salesOrderId, referenceNumber: referenceNumber, orderDate: orderDate, customerId: customerId,
		 				 			status: status, expiryDate: expiryDate, shippingDate: shippingDate, shippingMethod: shippingMethod, 
			 				 	 	paymentMode: paymentMode, currencyId: currencyId, orderType: orderType, continuousSales: continuousSales,
			 				 	 	salesPersonId: salesPersonId, description: description, chargesDetails: chargesDetails, shippingTerm : shippingTerm,
			 				 	 	shippingDetailId: shippingDetailId, customerQuotationId: customerQuotationId, salesOrderDetails: salesOrderDetails,
			 				 	 	creditTermId:creditTerm
		 					 	 },
		 				    dataType: "html",
		 				    cache: false,
		 					success:function(result){   
		 						 $(".tempresult").html(result);
		 						 var message=$('.tempresult').html();  
		 						 if(message.trim()=="SUCCESS"){
		 							 $.ajax({
		 									type:"POST",
		 									url:"<%=request.getContextPath()%>/show_sales_order.action", 
		 								 	async: false,
		 								    dataType: "html",
		 								    cache: false,
		 									success:function(result){
		 										$('#common-popup').dialog('destroy');		
		 										$('#common-popup').remove();  
		 										 $('#baseprice-popup-dialog').dialog('destroy');		
		 										 $('#baseprice-popup-dialog').remove(); 
		 										$("#main-wrapper").html(result); 
		 										if(salesOrderId==0)
		 											$('#success_message').hide().html("Record created.").slideDown(1000);
		 										else
		 											$('#success_message').hide().html("Record updated.").slideDown(1000);
		 										$('#success_message').delay(2000).slideUp();
		 									}
		 							 });
		 						 } 
		 						 else{
		 							 $('#page-error').hide().html(message).slideDown(1000);
		 							 $('#page-error').delay(2000).slideUp();
		 							 return false;
		 						 }
		 					},
		 					error:function(result){  
		 						$('#page-error').hide().html("Internal error.").slideDown(1000);
		 						$('#page-error').delay(2000).slideUp();
		 					}
		 				}); 
		 			}else{
		 				$('#page-error').hide().html("Please enter Order details.").slideDown(1000);
		 				$('#page-error').delay(2000).slideUp();
		 				return false;
		 			}
	 			}else{
	 				$('#page-error').hide().html("Product shouldn't sale below cost price.").slideDown(1000);
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			} 
	 		}else{
	 			return false;
	 		}
	 	});

	 var validateSalesOrderPrices = function(){ 
		 var flag= "true";
		$('.rowid').each(function(){  
			 var rowId = getRowId($(this).attr('id'));  
			 var allowsave = $('#allowSave_'+rowId).val();  
			 if(allowsave!= '' && allowsave == "false"){
				 flag = "false"; 
			 } 
		 }); 
		 return flag;
	 };
	 
	 var getSalesOrderDetails = function(){ 
		 var productArray = new Array();
		 var quantityArray = new Array();
	 	 var amountArray = new Array(); 
	 	 var modeArray = new Array(); 
	 	 var discountArray = new Array(); 
	 	 var detailIdArray = new Array(); 
	 	 var storeArray = new Array(); 
	 	 var packageTypeArray = new Array(); 
	 	 var packageUnitArray = new Array(); 
 	 	 var salesOrderDetail = ""; 
 		 $('.rowid').each(function(){  
 			 var rowId = getRowId($(this).attr('id'));  
 			 var productId = $('#productid_'+rowId).val();  
 			 var storeId = Number($('#storeid_'+rowId).val()); 
 			 var quantity = $('#productQty_'+rowId).val();  
 			 var amount = $jquery('#amount_'+rowId).val();    
 			 var amountMode=$('#amountmode_'+rowId).attr('checked');
 			 var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 			 var discount = Number($('#discount_'+rowId).val());
 			 var salesOrderDetailId = Number($('#salesOrderDetailId_'+rowId).val());
  			 var productPackageDetailId = Number($('#packageType_'+rowId).val());
  			 var packageUnit = Number($('#packageUnit_'+rowId).val());
  			 
 			 if(typeof productId != 'undefined' && productId!=null && 
 					productId!="" && quantity!='' && amount!=''){
 				productArray.push(productId); 
 				storeArray.push(storeId);
 				quantityArray.push(quantity); 
  				amountArray.push(amount); 
 				if(amountMode == false && pecentageMode==false)
  					modeArray.push("##");
 				else if(amountMode == true)
 					modeArray.push('A'); 
 				else if(pecentageMode == true)
 					modeArray.push('P'); 
  				discountArray.push(discount); 
  				packageTypeArray.push(productPackageDetailId); 
  				packageUnitArray.push(packageUnit); 
  				detailIdArray.push(salesOrderDetailId);  
 			 } 
 		});
 		 
  		for(var j=0;j<productArray.length;j++){ 
  			salesOrderDetail += productArray[j]+"__"+storeArray[j]+"__"+quantityArray[j]+"__"+amountArray[j]+"__"+
  								modeArray[j]+"__"+discountArray[j]+"__"+packageTypeArray[j]+"__"+packageUnitArray[j]+"__"+detailIdArray[j];
 			if(j==productArray.length-1){   
 			} 
 			else{
 				salesOrderDetail += "@#";
 			}
 		}  
 		return salesOrderDetail; 
 	 }; 

 	 var getSalesOrderCharges = function(){ 
		 var chargesTypeArray = new Array(); 
	 	 var chargesArray = new Array(); 
	 	 var descriptionArray = new Array(); 
 	 	 var chargesIdArray = new Array(); 
 	 	 var chargesDetail = ""; 
 		 $('.rowidcharges').each(function(){  
 			 var rowId = getRowId($(this).attr('id'));  
 			 var chargesTypeId = $('#chargesTypeId_'+rowId).val();  
 			 var charges = Number($('#charges_'+rowId).val());  
 			 var description = $('#description_'+rowId).val();     
 			 var salesOrderChargeId = Number($('#salesOrderChargeId_'+rowId).val()); 
  			 if(typeof chargesTypeId != 'undefined' && chargesTypeId!=null && 
 					chargesTypeId!="" && charges>0){
 				chargesTypeArray.push(chargesTypeId); 
 				chargesArray.push(charges);  
  				if(description ==null || description=="")
  					descriptionArray.push("##");
 				else 
 					descriptionArray.push(description); 
  				chargesIdArray.push(salesOrderChargeId);  
 			 }   
 		});
  		 
  		for(var j=0;j<chargesTypeArray.length;j++){ 
  			chargesDetail += chargesTypeArray[j]+"__"+chargesArray[j]+"__"+descriptionArray[j]+"__"+
  							chargesIdArray[j];
 			if(j==chargesTypeArray.length-1){   
 			} 
 			else{
 				chargesDetail += "@#";
 			}
 		} 
  		return chargesDetail; 
 	 };

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.delrowcharges').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidcharges').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#chargesLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	 $('#customer-common-popup').live('click',function(){  
    	$('#common-popup').dialog('close'); 
     });

	 $('#product-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 }); 
	
	 $('#store-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 });
	
	 $('#price-common-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 }); 
	
	 $('#customer-quotation-popup').live('click',function(){  
		$('#common-popup').dialog('close'); 
	 }); 

  $('.common_person_list').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {  personTypes: "1"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.show_customer_list').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false,  
		 	data: { pageInfo: "sales_order"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.show_customer_quotation').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_quotation_list.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	{
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_sales_product_common_popup_preload.action", 
		 	async: false,  
		 	data: {itemType: "I"},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$('#sales-product-inline-popup').html(result);  
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});
	}

	$('.show_product_list_sales_order').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		globalRowId = Number($(this).attr('id').split("_")[1]);  
		$('#sales-product-inline-popup').dialog('open'); 
		return false;
	});
	
	$('#sales-product-inline-popup').dialog({
		autoOpen : false,
		width : 800,
		height : 600,
		bgiframe : true,
		modal : true,
		buttons : {
			"Close" : function() {
				$(this).dialog("close");
			}
		}
	});

	$('.show_store_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
		 	async: false,  
		 	data: { rowId: tableRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0); 
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('.show_productpricing_list').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = Number($(this).attr('id').split("_")[1]);  
		var productId = Number($('#productid_'+tableRowId).val());
		var storeId = Number($('#storeid_'+tableRowId).val()); 
		if(productId > 0){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_common_product_pricing.action", 
			 	async: false,  
			 	data: {productId: productId, storeId: storeId, rowId: tableRowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			}); 
		} 
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});

	$('.baseprice-popup').click(function(){ 
		$('.ui-dialog-titlebar').remove();   
		var productId = Number($('#productid_'+tableRowId).val());
		var storeId = Number($('#storeid_'+tableRowId).val());  
		$('#amount_' + tableRowId).val('');
		$('#productPricingDetailId_' + tableRowId).val('');
		if(productId > 0) {
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_stock_details.action",  
			 	async: false,
			 	data:{productId: productId, storeId: storeId},
			    dataType: "json",
			    cache: false,
				success:function(response){  
		 			if(response.stockVO != null && response.stockVO.availableQuantity!=null && response.stockVO.availableQuantity!=''){
						$('#productCode').text(response.stockVO.productName);
						$('#unitName').text(response.stockVO.productUnit);
						$('#costingType').text(response.stockVO.costingType);
						$('#storeQuantity').text(response.stockVO.availableQuantity);   
  						$jquery('#amount_'+tableRowId).val(response.stockVO.unitRate);

						var amount = Number($jquery('#amount_'+tableRowId).val());
				 	 	var productQty = Number($('#packageUnit_'+tableRowId).val());
				 	 	if(amount > 0 && productQty > 0){
				 	 		var discount = Number($('#discount_'+tableRowId).val());
				 	 		if(discount > 0){
				 	 			var totalAmount = Number(amount * productQty); 
				 	 			var discountAmount = 0; 
				 	 	 	 	var chargesMode = $('#chargesmodeDetail_'+tableRowId).val();
				 	 	 		if(chargesMode == "true") { 
				 	 	 			discountAmount = Number(Number(amount * discount) / 100);
				 	 	 			totalAmount -= discountAmount;
				 	 	 		}else{
				 	 	 			totalAmount -= discount;
				 	 	 	 	}
				 	 	 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
				 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
				 	 	 	}else{
				 	 	 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
				 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 	
				 	 	 	}   
				 	 	}
				 	 	triggerAddRow(tableRowId);
 						//$('#baseprice-popup-dialog').dialog('open');  
					}else {
						$('#line-error').hide().html("For Product "+$('#product_'+tableRowId).text()
									+" in store "+$('#store_'+tableRowId).text()+" is empty.").slideDown(2000);
						$('#line-error').delay(3000).slideUp();
					}
					return false;
				},error:function(response){  
					$('#line-error').hide().html("For Product "+$('#product_'+tableRowId).text()
							+" in store "+$('#store_'+tableRowId).text()+" is empty.").slideDown(2000);
					$('#line-error').delay(3000).slideUp();
				}
			});
			return false;
		}
		return false; 
	});

	$('#baseprice-popup-dialog').dialog({
 		autoOpen: false,
 		width: 400,
 		height: 200,
 		bgiframe: true,
 		modal: true,
 		buttons: { 
		 	"Close": function(){  
 				$('#baseprice-popup-dialog').dialog("close");  
 			} 
 		}
 	});
	
	$('.packageType').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		$('#productQty_'+rowId).val('');
		$('#baseDisplayQty_'+rowId).html('');
		var packageUnit = Number($('#packageUnit_'+rowId).val());
		var packageType = Number($('#packageType_'+rowId).val());   
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		} 
		triggerAddRow(rowId);
		return false;
	});
	
	$('.packageUnit').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		$('#baseUnitConversion_'+rowId).text('');
		$('#productQty_'+rowId).val('');
		var packageUnit = Number($(this).val()); 
		var packageType = Number($('#packageType_'+rowId).val()); 
 		if(packageType == -1 && packageUnit > 0){
 			$('#productQty_'+rowId).val(packageUnit);
 			var amount = Number($jquery('#amount_' + rowId).val());
			var productQty = Number($('#productQty_' + rowId).val());
			var discount = Number($('#discount_'+rowId).val());
 	 		var totalAmount = Number(amount * productQty); 
 	 		if(discount > 0){  
 	 	 		var basePrice = Number($('#basePrice_'+rowId).val());
 	 		 	var standardPrice = Number($('#standardPrice_'+rowId).val());
 	 			var discountAmount = 0; 
 	 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 	 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 			if(pecentageMode == true) { 
 	 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 	 			totalAmount -= discountAmount;
 	 	 		}else if(amountMode == true){
 	 	 			totalAmount -= discount;
 	 	 	 	}
 	 			var productQty1 = Number($('#productQty_'+rowId).val());
 	  	 		var averagePrice = Number(totalAmount / productQty1);  
 	 	 		if(averagePrice < basePrice){ 
 	 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 	 				$('#allowSave_'+rowId).val('false');
 		 		}
 	 	 		else if(averagePrice < standardPrice){
 	 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 	 		}else
 	 	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 	 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	 	}else{
 	 	 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val());
 	 	 	}
 	 			
		}
		if(packageUnit > 0 && packageType > 0){ 
			getProductBaseUnit(rowId);
		}
		triggerAddRow(rowId);
		return false;
	}); 
	
	$('.productQty').live('change',function(){   
 		var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($jquery('#amount_'+rowId).val());
 	 	var productQty = Number($('#productQty_'+rowId).val());
 	 	$('#allowSave_'+rowId).val('true'); 
		var packageType = Number($('#packageType_'+rowId).val()); 
 	 	if(amount > 0 && productQty > 0){
 	 		var discount = Number($('#discount_'+rowId).val());
 	 		var totalAmount = Number(amount * productQty); 
 	 		if(discount > 0){  
 	 	 		var basePrice = Number($('#basePrice_'+rowId).val());
 	 		 	var standardPrice = Number($('#standardPrice_'+rowId).val());
 	 			var discountAmount = 0; 
 	 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 	 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 			if(pecentageMode == true) { 
 	 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 	 			totalAmount -= discountAmount;
 	 	 		}else if(amountMode == true){
 	 	 			totalAmount -= discount;
 	 	 	 	}
 	 			var productQty1 = Number($('#productQty_'+rowId).val());
 	  	 		var averagePrice = Number(totalAmount / productQty1);    
 	 	 		if(averagePrice < basePrice){ 
 	 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 	 				$('#allowSave_'+rowId).val('false');
 		 		}
 	 	 		else if(averagePrice < standardPrice){
 	 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
 					$('#discount-error').delay(2000).slideUp();
 	 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 	 		}else
 	 	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 	 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	 	}else{
 	 	 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val());
 	 	 	} 
 	 		
 	 		if(productQty > 0 && packageType > 0){ 
				getProductConversionUnit(rowId);
			}else{
				$('#packageUnit_'+rowId).val(productQty);
			}
 	 		triggerAddRow(rowId);
 	 	} 
 		return false;
 	 });

 	$('.amount').live('change',function(){ 
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($jquery('#amount_'+rowId).val());
 	 	var productQty = Number($('#packageUnit_'+rowId).val());
 		var basePrice = Number($('#basePrice_'+rowId).val());
	 	var standardPrice = Number($('#standardPrice_'+rowId).val());
		$('#allowSave_'+rowId).val('true');
 	 	if(amount > 0 && productQty > 0){
 	 		$('#baseSellingPrice_' + rowId).val('');
 	 		var discount = Number($('#discount_'+rowId).val());
 	 		if(discount > 0){
 	 			var totalAmount = Number(amount * productQty); 
 	 			var discountAmount = 0; 
 	 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 	 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 	 		if(pecentageMode == true) { 
 	 	 			discountAmount = Number(Number(totalAmount * discount) / 100);
 	 	 			totalAmount -= discountAmount;
 	 	 		}else if(amountMode == true){
 	 	 			totalAmount -= discount;
 	 	 	 	}
 	 	 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	 		var productQty1 = Number($('#productQty_'+rowId).val());
 	  	 		var averagePrice = Number(totalAmount / productQty1);  
	  	 	 	if(averagePrice < basePrice){ 
	 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
					$('#discount-error').delay(2000).slideUp();
	 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
	 				$('#allowSave_'+rowId).val('false');
		 		}
	 	 		else if(averagePrice < standardPrice){
	 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
					$('#discount-error').delay(2000).slideUp();
	 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
	 	 		}else
	 	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 	 	}else{
 	 	 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
 	 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	 	}
 	 		 
 	 		triggerAddRow(rowId);
 	 	} 
 		return false;
 	 });

 	$('.chargestypeid').live('change',function(){ 
 		triggerChargesAddRow(getRowId($(this).attr('id')));
 		return false;
 	 }); 

 	$('.charges').live('change',function(){ 
 		triggerChargesAddRow(getRowId($(this).attr('id')));
 		return false;
 	 }); 

 	$('.chargesmodeDetail').live('change',function(){  
 		var rowId = getRowId($(this).attr('id'));
		var amount = Number($jquery('#amount_' + rowId).val());
		var discount = Number($('#discount_' + rowId).val());
		var amountMode=$('#amountmode_'+rowId).attr('checked');
		var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
		var productQty = Number($('#packageUnit_' + rowId)
				.val());
		var basePrice = Number($('#basePrice_'+rowId).val());
	 	var standardPrice = Number($('#standardPrice_'+rowId).val());
		$('#allowSave_'+rowId).val('true');
		if (amount > 0
				&& discount > 0) {
			var totalAmount = Number(amount * productQty);
			var discountAmount = 0; 
			if (pecentageMode == true) {
				discountAmount = Number(Number(totalAmount
						* discount) / 100);
				totalAmount -= discountAmount;
			} else if (amountMode == true) {
				totalAmount -= discount;
			}
			$jquery('#totalAmountH_' + rowId).val(totalAmount);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
			var productQty1 = Number($('#productQty_'+rowId).val());
  	 		var averagePrice = Number(totalAmount / productQty1);  
	 		if(averagePrice < basePrice){ 
				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
				$('#allowSave_'+rowId).val('false');
 			}
	 		else if(averagePrice < standardPrice){
	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
			$('#discount-error').delay(2000).slideUp();
	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
	 		}else
	 			$('#fieldrow_'+rowId).css('background', '#fff');
		} else {
			$jquery('#totalAmountH_' + rowId).val(amount * productQty);
			$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
		} 
 	});

 	$('.discount').live('change',function(){  
 	 	var rowId = getRowId($(this).attr('id'));
 	 	var amount = Number($jquery('#amount_'+rowId).val());
 	 	var discount = Number($('#discount_'+rowId).val());
 	 	var chargesmodeDetail = $('#chargesmodeDetail_'+rowId).val();
 	 	var productQty = Number($('#packageUnit_'+rowId).val());
 	 	var totalAmount = Number(amount * productQty); 
 		var basePrice = Number($('#basePrice_'+rowId).val());
	 	var standardPrice = Number($('#standardPrice_'+rowId).val());
		$('#allowSave_'+rowId).val('true');
 	 	if(chargesmodeDetail!='' && amount > 0 && discount > 0) { 
 			var discountAmount = 0; 
 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 	 	 	if(pecentageMode == true) { 
	 	 			discountAmount = Number(Number(totalAmount * discount) / 100);
	 	 			totalAmount -= discountAmount;
	 	 		}else if(amountMode == true){
	 	 			totalAmount -= discount;
	 	 	 	}
 	 	 	$jquery('#totalAmountH_' + rowId).val(totalAmount);
 	 	 	$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	 	var productQty1 = Number($('#productQty_'+rowId).val());
  	 		var averagePrice = Number(totalAmount / productQty1);  
	 		if(averagePrice < basePrice){ 
				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
				$('#allowSave_'+rowId).val('false');
 			}
	 		else if(averagePrice < standardPrice){
	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
			$('#discount-error').delay(2000).slideUp();
	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
	 		}else
	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 	} else{
 	 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 	
 	 	} 
 	 	
 	});
 	
 	if (Number($('#salesOrderId').val()) > 0) { 
 		$('#currency').val($('#tempCurrency').val());
 		$('#shippingMethod').val($('#tempShippingMethod').val());
 		$('#shippingTerm').val($('#tempShippingTerm').val());
  		$('#status').val($('#tempStatus').val());
 		$('#shippingDetail').val($('#tempShippingDetail').val());
 		$('#orderType').val($('#tempOrderType').val());
 		$('#paymentMode').val($('#tempPaymentMode').val());
 		$('#creditTerm').val($('#termpCreditTermId').val()); 
 		{
 			$('.rowid').each(function(){
 				var rowId = getRowId($(this).attr('id')); 
 				$('#chargesmodeDetail_'+rowId).val($('#tempChargesModeDetail_'+rowId).val()); 
 				$('#totalAmount_'+rowId).text($('#totalAmountH_'+rowId).val()); 
 				$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
 			}); 
 	 	}
 	 	{
 	 		$('.rowidcharges').each(function(){
 				var rowId = getRowId($(this).attr('id')); 
 				$('#chargesTypeId_'+rowId).val($('#tempChargesTypeId_'+rowId).val()); 
 				$('#chargesMode_'+rowId).val($('#tempChargesMode_'+rowId).val()); 
 			});
 	 	} 
 	 	$('#quotation').remove();  
 	 	if(Number($('#customerQuotationId').val()) > 0){
 	 		$('.prodID').remove();  
 	 		$('.pricingDetail').remove(); 
 	 		$('.chargestype-lookup').remove(); 
 	 		$('.rowid').each(function(){ 
 				var rowId = getRowId($(this).attr('id')); 
 				var mode = $('#chargesmodeDetail_'+rowId).val();  
 				if(mode == 'true')
 					$('#chargesmodeDetail_'+rowId).after('<span>Percentage</span>'); 
 				else if(mode == 'false')
 					$('#chargesmodeDetail_'+rowId).after('<span>Amount</span>');
 			}); 
 	 		$('.chargesmodeDetail').hide();
 	 		$('.amount,.discount,.charges').attr('readonly',true);
 	 		$('.chargestypeid').attr('disabled',true); 
 		}else {
 			manupulateLastRow();
 	 	}
	}else {
 		$('#currency').val($('#defaultCurrency').val());
 		$('#status').val(7); 
 		manupulateLastRow();
 	}
});

function triggerPriceCalc(rowId){
  	var amount = Number($jquery('#amount_'+rowId).val());
 	var discount = Number($('#discount_'+rowId).val());
 	var chargesmodeDetail = $('#chargesmodeDetail_'+rowId).val();
 	var productQty = Number($('#productQty_'+rowId).val());
 	if(chargesmodeDetail!='' && amount > 0 && discount > 0) {
 		var totalAmount = Number(amount * productQty); 
		var discountAmount = 0; 
 	 	var chargesMode = $('#chargesmodeDetail_'+rowId).val();
 		if(chargesMode == "true") { 
 			discountAmount = Number(Number(amount * discount) / 100);
 			totalAmount -= discountAmount;
 		}else{
 			totalAmount -= discount;
 	 	}
 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	}else{
 		$jquery('#totalAmountH_' + rowId).val(amount * productQty);
 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	} 
 	triggerAddRow(rowId);
 	return false;
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	var tdSize = 0;
	var actualSize = 0;
	{
		hiddenSize=0;
		$($(".tab>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		tdSize=$($(".tab>tr:first").children()).size();
		actualSize=Number(tdSize-hiddenSize);  
		
		$('.tab>tr:last').removeAttr('id');  
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
		$($('.tab>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}  
	{
		hiddenSize=0;
		tdSize = 0;
		actualSize = 0;
		$($(".tabcharges>tr:first").children()).each(function(){
			if($(this).is(':hidden')){
				++hiddenSize;
			}
		});
		tdSize=$($(".tabcharges>tr:first").children()).size();
		actualSize=Number(tdSize-hiddenSize);  
		
		$('.tabcharges>tr:last').removeAttr('id');  
		$('.tabcharges>tr:last').removeClass('rowidcharges').addClass('lastrow');  
		$($('.tabcharges>tr:last').children()).remove();  
		for(var i=0;i<actualSize;i++){
			$('.tabcharges>tr:last').append("<td style=\"height:25px;\"></td>");
		} 
	}
}
 
function triggerAddRow(rowId){  
	 
	var productid = $('#productid_'+rowId).val();  
	var productQty = $('#productQty_'+rowId).val(); 
	var amount = $('#amount_'+rowId).val();
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid!=null && productid!=""
			&& productQty!=null && productQty!=""  
			&& amount!=null && amount!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
}

function triggerChargesAddRow(rowId){  
	var chargesTypeId = $('#chargesTypeId_'+rowId).val();   
	var charges = $('#charges_'+rowId).val();
	var nexttab=$('#fieldrowcharges_'+rowId).next();  
	if(chargesTypeId!=null && chargesTypeId!="" 
			&& charges!=null && charges!=""
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImagecharges_'+rowId).show();
		$('.addrowscharges').trigger('click');
	} 
} 
function personPopupResult(personId, personName, commonParam){  
	$('#salesPerson').val(personName);
	$('#salesPersonId').val(personId); 
}

function commonProductPricePopup(productPricingCalcId, calculationTypeCode, 
		calculationSubTypeCode, salesAmount, rowId) {  
	$('#amount_' + rowId).val(salesAmount);
	$('#productPricingDetailId_' + rowId).val(productPricingCalcId);
	triggerPriceCalc(rowId);
	return false;
}
function showShippingSite(customerId) {
	var customerId = $('#customerId').val(); 
	$('#shippingDetail').html('');
	$('#shippingDetail')
	.append(
			'<option value="">Select</option>');
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_shipping_site.action", 
	 	async: false,  
	 	data: {customerId: customerId},
	 	dataType: "json",
	    cache: false,
		success:function(response){  
			if(null != response && null != response.aaData){
				$(response.aaData)
				.each(
						function(index) { 
					$('#shippingDetail')
						.append('<option value='
								+ response.aaData[index].shippingId
								+ '>' 
								+ response.aaData[index].name
								+ '</option>');
				});
			   $('#shippingDetail').val(response.aaData[0].shippingId);
			} 
 		}   
	});  
	return false;
}
function loadQuotationDetails(customerQuotationId, quotationNumber,creditTermId) {
	$('#customerQuotationId').val(customerQuotationId);
	$('#customerQuotation').val(quotationNumber); 
	$('#creditTerm').val(creditTermId); 
	$('#status').val(7);
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_quotation_info.action",
					async : false,
					data : {
						customerQuotationId : customerQuotationId
					},
					dataType : "json",
					cache : false,
					success : function(response) {
						$('#shippingDetail').html('');
						$('#shippingDetail').append(
								'<option value="">Select</option>');

						$('#orderDate').val(
								response.customerQuotationVO.orderDateFormat);
						$('#expiryDate').val(
								response.customerQuotationVO.expiryDateFormat);
						$('#shippingDate')
								.val(
										response.customerQuotationVO.shippingDateFormat);
						if (response.customerQuotationVO.lookupDetailByShippingMethod != null)
							$('#shippingMethod')
									.val(
											response.customerQuotationVO.lookupDetailByShippingMethod.lookupDetailId);
						if (response.customerQuotationVO.lookupDetailByShippingTerm != null)
							$('#shippingTerm')
									.val(
											response.customerQuotationVO.lookupDetailByShippingTerm.lookupDetailId);
						if (response.customerQuotationVO.lookupDetailByModeOfPayment != null)
							$('#paymentMode')
									.val(
											response.customerQuotationVO.modeOfPayment);
						if (response.customerQuotationVO.person != null) {
							$('#salesPerson')
									.val(
											response.customerQuotationVO.person.firstName
													+ " "
													+ response.customerQuotationVO.person.lastName);
							$('#salesPersonId')
									.val(
											response.customerQuotationVO.person.personId);
						}
						if (response.customerQuotationVO.customer != null) {
							$('#customerName')
									.val(
											response.customerQuotationVO.customer.customerName);
							$('#customerId')
									.val(
											response.customerQuotationVO.customer.customerId);

							$(
									response.customerQuotationVO.customer.shippingDetails)
									.each(
											function(sindex) {
												$('#shippingDetail')
														.append(
																'<option value='
							+ response.customerQuotationVO.customer.shippingDetails[sindex].shippingId
							+ '>'
																		+ response.customerQuotationVO.customer.shippingDetails[sindex].name
																		+ '</option>');
											});
						} 
						$('.tab').html('');
						$('.tabcharges').html('');
						$('.options').hide();
						
						{
							var counter = 0;
							$(
									response.customerQuotationVO.customerQuotationDetailVOs)
									.each(
											function(index) {
												counter += 1;
												var storeId = 0;
												var storeName = "";
												var isPercentage = "";
												var mode = "";
												var discount = "";
												var basePrice = 0;
 												var productPricingCalcId = 0; 
												var baseUnitName = "";
												var packageDetailId = response.customerQuotationVO.customerQuotationDetailVOs[index].packageDetailId;
												var quantity = response.customerQuotationVO.customerQuotationDetailVOs[index].baseQuantity;
												var packageUnit = response.customerQuotationVO.customerQuotationDetailVOs[index].packageUnit;
												var unitRate = response.customerQuotationVO.customerQuotationDetailVOs[index].unitRate; 
												var baseDisplayQty = response.customerQuotationVO.customerQuotationDetailVOs[index].baseQuantity; 
												var totalAmount = Number(packageUnit
														* unitRate);
												if (response.customerQuotationVO.customerQuotationDetailVOs[index].store != null) {
													storeId = response.customerQuotationVO.customerQuotationDetailVOs[index].store.storeId;
													storeName = response.customerQuotationVO.customerQuotationDetailVOs[index].store.storeName;
												}
												if (response.customerQuotationVO.customerQuotationDetailVOs[index].baseUnitName != null) 
													baseUnitName = response.customerQuotationVO.customerQuotationDetailVOs[index].baseUnitName;
												if(response.customerQuotationVO.customerQuotationDetailVOs[index].productPricingCalc !=null) 
													productPricingCalcId = response.customerQuotationVO.customerQuotationDetailVOs[index].productPricingCalc.productPricingCalcId;
												if (response.customerQuotationVO.customerQuotationDetailVOs[index].isPercentage != null) {
													discount = response.customerQuotationVO.customerQuotationDetailVOs[index].discount;
													var discountAmount = 0;
													if (response.customerQuotationVO.customerQuotationDetailVOs[index].isPercentage == true) {
														discountAmount = Number(totalAmount
																* discount) / 100;
														totalAmount -= discountAmount;
														isPercentage = "true";
														mode = "Percentage";
													} else {
														totalAmount -= discount;
														isPercentage = "false";
														mode = "Amount";
													}
												} else {
													$('#discount_' + counter)
															.hide();
												}
												 
												$('.tab')
														.append(

																"<tr id='fieldrow_"+counter+"' class='rowid'>"
																		+ "<td id='lineId_"+counter+"' style='display:none;'>"
																		+ counter
																		+ "</td>"
																		+ "<td>"
																		+ "<input type='hidden' id='productid_"+counter+"'"
																			+" value='"+response.customerQuotationVO.customerQuotationDetailVOs[index].product.productId+"' name='productId'/>"
																		+ "<span id='product_"+counter+"' class='width60 float-left'>"
																			+ response.customerQuotationVO.customerQuotationDetailVOs[index].product.productName
																		+ "</span>"
																		+ "<span id='unitCode_"+counter+"' class='width10 float-left'>"
																			+ response.customerQuotationVO.customerQuotationDetailVOs[index].unitCode
																		+ "</span>"
																		+ "</td>"
																		+ "<td style='display: none;'>"
																		+ "<input type='hidden' value='"+storeId+"' id='storeid_"+counter+"' name='storeId'>"
																		+ "<span id=store_"+counter+">"
																		+ storeName
																		+ "</span>"
																		+ "<span class='button float-right'>"
																		+ "<a class='show_store_list btn ui-state-default ui-corner-all width100' id='storeID_"+counter+"'"
																			 +"style='cursor: pointer;'>"
																		+ "<span class='ui-icon ui-icon-newwin'></span>"
																		+ "</a>"
																		+ "</span>"
																		+ "</td>"
																		+ "<td>"
																		+ response.customerQuotationVO.customerQuotationDetailVOs[index].conversionUnitName
																		+ "<input type='hidden' id='packageType_"+counter+"' name='packageType' value='"+packageDetailId+"'"
																			+"class='packageType'/>"
																		+ "</td>"
																		+ "<td>"
																			+ "<input type='text' id='packageUnit_"+counter+"' name='packageUnit' value='"+packageUnit+"'"
																			+"class='packageUnit width80'/>"
																		+ "</td>"
																		+ "<td>"
																		+ "<input type='hidden' id='productQty_"+counter+"' name='productQty' value='"+quantity+"'"
																			+"class='productQty width80'/>"
																		+ "<span id='baseUnitConversion_"+counter+"' class='width10' style='display: none;'>"+baseUnitName+"</span>"
																		+ "<span id='baseDisplayQty_"+counter+"' class='width10'>"+baseDisplayQty+"</span>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span id='unitrate_"+counter+"'>"+unitRate+"</span>"
																		+ "<input type='text' readOnly='readOnly' name='amount' id='amount_"+counter+"' value='"+unitRate+"'"
																			+"class='amount width90 right-align' style='text-align:right;display: none;'/>"
																		+ "<input type='hidden' name='productPricingDetailId' value="+productPricingCalcId+" id='productPricingDetailId_"+counter+"'/>" 
																		+ "</td>"
																		+ "<td>"
																		+ "<span class='width98'>"
																		+ mode
																		+ "</span>"
																		+ "<input type='hidden' value='"+isPercentage+"' id='chargesmodeDetail_"+counter+"'"
																			+" class='chargesmodeDetail' name='chargesmodeDetail'/>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span>"+discount+"</span>"
																		+ "<input type='hidden' readOnly='readOnly' value='"+discount+"' id='discount_"+counter+"'"
																			+" class='discount width98 right-align' name='discount' style='text-align:right;'/>"
																		+ "</td>"
																		+ "<td>"
																		+ "<span style='float: right;' id='totalAmount_"+counter+"'>"
																		+ "</span>"
																		+ "<input type='text' style='display: none' id='totalAmountH_"+counter+"' value="+totalAmount+" class='totalAmountH'/>"
																		+ "</td>"
																		+ "<td style='display:none;'>"
																		+ "<input type='hidden' id='salesOrderDetailId_"+counter+"' name='salesOrderDetailId'>"
																		+ "</td>"
																		+ "</tr>");
																		$jquery('#totalAmountH_'+counter).number(true, 2);
																		$jquery('#amount_'+counter).number(true, 2);
																		$jquery('#amount_'+counter).val(unitRate); 
																		$('#unitrate_'+counter).text($('#amount_'+counter).val());
																		$jquery('#totalAmountH_'+counter).val(totalAmount); 
																		$('#totalAmount_'+counter).text($('#totalAmountH_'+counter).val());
											}); 
						}
						{
							if (response.customerQuotationVO.customerQuotationCharges != null
									&& typeof response.customerQuotationVO.customerQuotationCharges[0] != "undefined") { 
								var ccounter = 0;
								$(
										response.customerQuotationVO.customerQuotationCharges)
										.each(
												function(cindex) {
													ccounter += 1;
													var chargesType = response.customerQuotationVO.customerQuotationCharges[cindex].lookupDetail.displayName;
													var chargesTypeId = response.customerQuotationVO.customerQuotationCharges[cindex].lookupDetail.lookupDetailId;
													var charges = response.customerQuotationVO.customerQuotationCharges[cindex].charges;
													$('.tabcharges')
															.append(
																	"<tr id='fieldrowcharges_"+ccounter+"' class='rowidcharges'>"
																			+ "<td id='chargesLineId_"+ccounter+"'>"
																			+ ccounter
																			+ "</td>"
																			+ "<td>"
																			+ "<span class='width98'>"
																			+ chargesType
																			+ "</span>"
																			+ "<input type='hidden' value='"+chargesTypeId+"' id='chargesTypeId_"+ccounter+"'"
											+" class='chargestypeid width90' name='chargesTypeId'/>"
																			+ "</td>" 
																			+ "<td>"
																			+ "<span>"+charges+"</span>"
																			+ "<input type='hidden' readOnly='readOnly' value='"+charges+"' id='charges_"+ccounter+"'"
											+" class='charges width98 right-align' name='charges' style='text-align:right;'/>"
																			+ "</td>"
																			+ "<td>"
																			+ "<input type='text' value='"+response.customerQuotationVO.customerQuotationCharges[cindex].description+"'"
											+" id='description_"+ccounter+"' class='description width98' name='description'/>"
																			+ "</td>"
																			+ "<td style='display:none;'>"
																			+ "<input type='hidden' id='salesOrderChargeId_"+ccounter+"' name='salesOrderChargeId'>"
																			+ "</td>"
																			+ "</tr>");
												});
							}else{
								$('.sales_charges_block').hide();
							}
						}
					}
				});
		return false;
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName);
		showShippingSite(customerId);
		return false;
	}

	function commonProductPopup(rowId, aData) {
		if(rowId==null || row==0)
			rowId=globalRowId;
		
		$('#productid_' + rowId).val(aData.productId);
		$('#product_' + rowId).html(aData.code +" - "+aData.productName); 
		$('#unitCode_' + rowId).html(aData.unitCode); 
		$('#productPricingDetailId_' + rowId).val(aData.productPricingCalcId); 
		$('#basePrice_' + rowId).val(aData.basePrice);  
		$('#standardPrice_' + rowId).val(aData.standardPrice); 
		var customerId = Number($('#customerId').val());
		
		{
			getProductPackagings("packageType",aData.productId, rowId);
		}
		
		if(customerId > 0){
			checkCustomerSalesPrice(aData.productId, customerId, aData.sellingPrice, rowId);
		}else{
			$jquery('#amount_' + rowId).val(aData.sellingPrice); 
			$('#baseSellingPrice_' + rowId).val(aData.sellingPrice); 
		} 
		$('#sales-product-inline-popup').dialog("close");
		triggerAddRow(rowId);
		return false;
	}
	
	function getProductPackagings(idName, productId, rowId){
		$('#'+idName+'_'+rowId).html('');
		$('#'+idName+'_'+rowId)
		.append(
				'<option value="-1">Select</option>');
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_packaging_detail.action", 
		 	async: false,  
		 	data:{productId: productId},
		    dataType: "json",
		    cache: false,
			success:function(response){   
				if(response.productPackageVOs != null){ 
					$.each(response.productPackageVOs, function (index) {
						if(response.productPackageVOs[index].productPackageId == -1){ 
							$('#'+idName+'_'+rowId)
							.append('<option value='
									+ response.productPackageVOs[index].productPackageId
									+ '>' 
									+ response.productPackageVOs[index].packageName
									+ '</option>');
						}else{ 
							var optgroup = $('<optgroup>');
				            optgroup.attr('label',response.productPackageVOs[index].packageName);
				            optgroup.css('color', '#c85f1f'); 
				             $.each(response.productPackageVOs[index].productPackageDetailVOs, function (i) {
				                var option = $("<option></option>");
				                option.val(response.productPackageVOs[index].productPackageDetailVOs[i].productPackageDetailId);
				                option.text(response.productPackageVOs[index].productPackageDetailVOs[i].conversionUnitName); 
				                option.css('color', '#000'); 
				                option.css('margin-left', '10px'); 
				                optgroup.append(option);
				             });
				             $('#'+idName+'_'+rowId).append(optgroup);
						}  
					}); 
					$('#'+idName+'_'+rowId).multiselect('refresh'); 
					$('#'+idName+'_'+rowId).val(-1); 
				} 
			} 
		});  
		return false;
	}
	
	function checkCustomerSalesPrice(productId, customerId, sellingPrice, rowId){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getcustomer_salesprice.action",
				data : {
					productId : productId, customerId: customerId
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) { 
					if(response.sellingPrice != null && response.sellingPrice != 0){
						$('#amount_' + rowId).val(response.sellingPrice); 
						$('#baseSellingPrice_' + rowId).val(response.sellingPrice); 
					}else{
						$('#amount_' + rowId).val(sellingPrice); 
						$('#baseSellingPrice_' + rowId).val(sellingPrice); 
					}
				}
			});
	}
	
	function commonStorePopup(storeId, storeName, rowId) {
		$('#storeid_' + rowId).val(storeId);
		$('#store_' + rowId).html(storeName);
		$('.baseprice-popup').trigger('click');
	}
	function customRange(dates) {
		if (this.id == 'orderDate') {
			$('#expiryDate').datepick('option', 'minDate', dates[0] || null);
			if ($('#orderDate').val() != "") {
				addPeriods();
			}
		} else {
			$('#orderDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	
	function getProductConversionUnit(rowId){
 		var packageQuantity = Number($('#productQty_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
 			} 
		});   
		return false;
	}
	
	function getProductBaseUnit(rowId){
		$('#baseUnitConversion_'+rowId).text('');
		var packageQuantity = Number($('#packageUnit_'+rowId).val());
		var productPackageDetailId = Number($('#packageType_'+rowId).val());  
		var basePrice = Number($('#baseSellingPrice_' + rowId).val());  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#productQty_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
				if(response.productPackagingDetailVO.basePrice != null && response.productPackagingDetailVO.basePrice > 0){
					$jquery('#amount_' + rowId).val(response.productPackagingDetailVO.basePrice);
					if(response.productPackagingDetailVO.unitQuantity != null && response.productPackagingDetailVO.unitQuantity > 0){
						var costPrice = Number($('#basePrice_'+rowId).val());
						var standardPrice = Number($('#standardPrice_'+rowId).val());
 						$('#basePrice_'+rowId).val(Number((costPrice * packageQuantity) / response.productPackagingDetailVO.unitQuantity)); 
 						$('#standardPrice_'+rowId).val(Number((standardPrice * packageQuantity) / response.productPackagingDetailVO.unitQuantity));
					} 
 				}  
 				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
			} 
		});  
		var amount = Number($jquery('#amount_' + rowId).val());
 		var discount = Number($('#discount_'+rowId).val());
 		var totalAmount = Number(amount * packageQuantity); 
 		if(discount > 0){  
 	 		var basePrice = Number($('#basePrice_'+rowId).val());
 		 	var standardPrice = Number($('#standardPrice_'+rowId).val());
 			var discountAmount = 0; 
 			var amountMode=$('#amountmode_'+rowId).attr('checked');
 			var pecentageMode = $('#pecentagemode_'+rowId).attr('checked');
 			if(pecentageMode == true) { 
 	 			discountAmount = Number(Number(totalAmount * discount) / 100); 
 	 			totalAmount -= discountAmount;
 	 		}else if(amountMode == true){
 	 			totalAmount -= discount;
 	 	 	}
 			var productQty1 = Number($('#productQty_'+rowId).val());
  	 		var averagePrice = Number(totalAmount / productQty1);  
 	 		if(averagePrice < basePrice){ 
 				$('#discount-error').hide().html("Product selling below cost price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 				$('#fieldrow_'+rowId).css('background', '#FE2E64'); 
 				$('#allowSave_'+rowId).val('false');
	 		}
 	 		else if(averagePrice < standardPrice){
 	 			$('#discount-error').hide().html("Product selling below standard price").slideDown(1000);
				$('#discount-error').delay(2000).slideUp();
 	 			$('#fieldrow_'+rowId).css('background', '#F7BE81'); 
 	 		}else
 	 			$('#fieldrow_'+rowId).css('background', '#fff');
 	 		$jquery('#totalAmountH_' + rowId).val(totalAmount);
 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	}else{
 	 		$jquery('#totalAmountH_' + rowId).val(amount * packageQuantity);
 	 		$('#totalAmount_' + rowId).text($('#totalAmountH_' + rowId).val()); 
 	 	}
		return false;
	}
	
	function addPeriods() {
		var date = new Date($('#orderDate').datepick('getDate')[0].getTime());
		$.datepick.add(date, parseInt(90, 10), 'd');
		$('#expiryDate').val($.datepick.formatDate(date));
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Sales
			Order
		</div> 
		<form id="salesOrderValidation" method="POST" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="salesOrderId" name="salesOrderId"
					value="${SALES_ORDER.salesOrderId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="height: 250px;">
							<div>
								<label class="width30">Sales Person</label> 
								<c:choose>
									<c:when
										test="${SALES_ORDER.person ne null && SALES_ORDER.person ne ''}">
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50"
											value="${SALES_ORDER.person.firstName } ${SALES_ORDER.person.lastName}" />
										<input type="hidden" readonly="readonly" name="salesPersonId" id="salesPersonId"
												value="${SALES_ORDER.person.personId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50" value="${requestScope.personName}"/>
										<input type="hidden" readonly="readonly" name="salesPersonId" id="salesPersonId"
												value="${requestScope.salesPersonId}" />
									</c:otherwise>
								</c:choose>
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common_person_list width100">
										<span class="ui-icon ui-icon-newwin"></span> </a> </span>
							</div>
							<div>
								<label class="width30"> Shipping Date </label>
								<c:choose>
									<c:when
										test="${SALES_ORDER.shippingDate ne null && SALES_ORDER.shippingDate ne ''}">
										<c:set var="shippingDate" value="${SALES_ORDER.shippingDate}" />
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("shippingDate").toString())%>"
											class="shippingDate width50">
									</c:when>
									<c:otherwise>
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											class="shippingDate width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">Order Type</label> <select
									name="orderType" id="orderType" class="width51">
									<option value="">Select</option>
									<c:forEach var="ORDER" items="${ORDER_TYPE}">
										<option value="${ORDER.lookupDetailId}">${ORDER.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempOrderType" name="lookupDetailId"
									value="${SALES_ORDER.lookupDetailByOrderType.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SALES_ORDER_TYPE"
											class="btn ui-state-default ui-corner-all ordertype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30">Credit Term</label> <select
									name="creditTerm" id="creditTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${CREDIT_TERM}">
										<option value="${TERMS.creditTermId}">${TERMS.name}</option>
									</c:forEach>
								</select> <input type="hidden" id="termpCreditTermId"
									name="termpCreditTermId"
									value="${SALES_ORDER.creditTerm.creditTermId}" />
							</div>
							<div>
								<label class="width30"> Payment Mode</label> <select
									name="paymentMode" id="paymentMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="paymentMode" items="${PAYMENT_MODE}">
										<option value="${paymentMode.key}">${paymentMode.value}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempPaymentMode"
									id="tempPaymentMode"
									value="${SALES_ORDER.modeOfPayment}" />
							</div> 
							<div>
								<label class="width30"> Shipping Method</label> <select
									name="shippingMethod" id="shippingMethod" class="width51">
									<option value="">Select</option>
									<c:forEach var="shippingMethod" items="${SHIPPING_METHOD}">
										<option value="${shippingMethod.lookupDetailId}">${shippingMethod.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempShippingMethod"
									id="tempShippingMethod"
									value="${SALES_ORDER.lookupDetailByShippingMethod.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SHIPPING_SOURCE"
											class="btn ui-state-default ui-corner-all shippingmethod-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"> Shipping Terms</label> <select
									name="shippingTerm" id="shippingTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="shippingTerm" items="${SHIPPING_TERMS}">
										<option value="${shippingTerm.lookupDetailId}">${shippingTerm.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" name="tempShippingTerm"
									id="tempShippingTerm"
									value="${SALES_ORDER.lookupDetailByShippingTerm.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="SHIPPING_TERMS"
											class="btn ui-state-default ui-corner-all shippingterm-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${SALES_ORDER.description}</textarea>
							</div> 
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="height: 250px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${SALES_ORDER.referenceNumber ne null && SALES_ORDER.referenceNumber ne ''}">
												${SALES_ORDER.referenceNumber} 
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${SALES_ORDER.referenceNumber}"
									name="tempreferenceNumber" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30">Quotation</label> <input type="text"
									readonly="readonly" name="customerQuotation"
									value="${SALES_ORDER.customerQuotation.referenceNo}"
									id="customerQuotation" class="width50" /> <input type="hidden"
									id="customerQuotationId" name="customerQuotationId"
									value="${SALES_ORDER.customerQuotation.customerQuotationId}" />
								<span class="button"
									style="position: relative; "> <a
									style="cursor: pointer;" id="quotation"
									class="btn ui-state-default ui-corner-all show_customer_quotation width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
							<div>
								<label class="width30"> Order Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${SALES_ORDER.orderDate ne null && SALES_ORDER.orderDate ne ''}">
										<c:set var="orderDate" value="${SALES_ORDER.orderDate}" />
										<input name="orderDate" type="text" readonly="readonly"
											id="orderDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("orderDate").toString())%>"
											class="orderDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="orderDate" type="text" readonly="readonly"
											id="orderDate" class="orderDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Expiry Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${SALES_ORDER.expiryDate ne null && SALES_ORDER.expiryDate ne ''}">
										<c:set var="expiryDate" value="${SALES_ORDER.expiryDate}" />
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("expiryDate").toString())%>"
											class="expiryDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate" class="expiryDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30"> Currency<span
									style="color: red;">*</span></label> <select name="currency"
									id="currency" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${SALES_ORDER.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div> 
							<div>
								<label class="width30">Customer Name<span
									style="color: red;">*</span></label>
								<c:set var="customerName" />
								<c:choose>
									<c:when test="${SALES_ORDER.customer.personByPersonId ne null}">
										<c:set var="customerName"
											value="${SALES_ORDER.customer.personByPersonId.firstName} ${SALES_ORDER.customer.personByPersonId.lastName}" />
									</c:when>
									<c:when test="${SALES_ORDER.customer.company ne null}">
										<c:set var="customerName"
											value="${SALES_ORDER.customer.company.companyName}" />
									</c:when>
									<c:when test="${SALES_ORDER.customer.cmpDeptLocation ne null}">
										<c:set var="customerName"
											value="${SALES_ORDER.customer.cmpDeptLocation.company.companyName}" />
									</c:when>
								</c:choose>
								<input type="text" readonly="readonly" name="customerName"
									value="${customerName}" id="customerName" class="width50 validate[required]" /> <input
									type="hidden" id="customerId" name="customerId"
									value="${SALES_ORDER.customer.customerId}" /> <span
									class="button"> <a
									style="cursor: pointer;" id="customer"
									class="btn ui-state-default ui-corner-all show_customer_list width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30"> Shipping Address</label> <select
									name="shippingDetail" id="shippingDetail" class="width51">
									<option value="">Select</option>
									<c:if
										test="${SALES_ORDER.customer ne null &&  SALES_ORDER.customer ne ''}">
										<c:forEach var="SHIPPING"
											items="${SALES_ORDER.customer.shippingDetails}">
											<option value="${SHIPPING.shippingId}">${SHIPPING.name}</option>
										</c:forEach>
									</c:if>
								</select> <input type="hidden" id="tempShippingDetail"
									name="tempShippingDetail"
									value="${SALES_ORDER.shippingDetail.shippingId}" />
							</div> 
							<div>
								<label class="width30" for="continuousSales">Continuous Sales</label>  
								<c:choose>
									<c:when test="${SALES_ORDER.continuousSales ne null && SALES_ORDER.continuousSales eq true}">
										<input type="checkbox" name="continuousSales" id="continuousSales" class="width3" checked="checked"/>
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="continuousSales" id="continuousSales" class="width3" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Status</label> <select name="status"
									id="status" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${ORDER_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus" name="lookupDetailId"
									value="${SALES_ORDER.orderStatus}" />
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div> 
				<div id="line-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<div id="discount-error"
										class="response-msg error ui-corner-all width80"
										style="display: none;"></div>
					<fieldset>
						<legend>Sales Detail<span
									class="mandatory">*</span></legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 6%;">Product</th>
										<th style="width: 1%; display: none;">Store</th>
										<th style="width: 4%;">Packaging</th> 
										<th style="width: 4%">Quantity</th>
										<th style="width: 5%;">Base Qty</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 3%;">Discount Mode</th>
										<th style="width: 3%;">Discount</th>
										<th style="width: 3%;">Total</th>
										<th style="width: 1%;" class="options"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${SALES_ORDER.salesOrderDetailVOs}" varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span class="width60 float-left"
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span>
												<span class="width10 float-right" id="unitCode_${status.index+1}"
													style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_product_list_sales_order prodID width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
											</td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.store.storeId}" /> <span
												id="store_${status.index+1}">${DETAIL.store.storeName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="storeID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show_store_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td>
												<select name="packageType" id="packageType_${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
											</td>
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
											</td> 
											<td><input type="hidden" name="productQty"
												id="productQty_${status.index+1}"
												value="${DETAIL.baseQuantity}" class="productQty width80"> 
												<span id="baseUnitConversion_${status.index+1}"  style="display: none;"
													class="width10">${DETAIL.baseUnitName}</span>
												 <span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;"
												class="amount validate[optional,custom[number]] width60 right-align">
												<span class="button float-right"> <a
													style="cursor: pointer; position: relative; top: 7px;"
													id="pricingDetail_${status.index+1}"
													class="btn ui-state-default ui-corner-all pricingDetail show_productpricing_list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
												<input type="hidden" name="basePrice" id="basePrice_${status.index+1}" value="${DETAIL.basePrice}"/> 
												<input type="hidden" name="standardPrice" id="standardPrice_${status.index+1}" value="${DETAIL.standardPrice}"/>
												<input type="hidden" name="allowSave" id="allowSave_${status.index+1}" value="true"/>
												<input type="hidden" id="baseSellingPrice_${status.index+1}" value="${DETAIL.unitRate}"/>
											</td>
											<td><input type="hidden" name="tempChargesModeDetail"
												id="tempChargesModeDetail_${status.index+1}"
												value="${DETAIL.isPercentage}" /> <c:set var="isPercentage"
													value="${DETAIL.isPercentage}" /> 
													<c:choose>
														<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq true}">
															<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																	 id="pecentagemode_${status.index+1}" checked="checked"/>
																<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
																<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																	id="amountmode_${status.index+1}" style="width:5px;"/>
																<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
														</c:when>
														<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq false && DETAIL.discount ne null && DETAIL.discount gt 0}">
															<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																	 id="pecentagemode_${status.index+1}"/>
																<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
																<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																	id="amountmode_${status.index+1}" value="false" style="width:5px;" checked="checked"/>
																<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
														</c:when>
														<c:otherwise>
															<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
																	 id="pecentagemode_${status.index+1}" value="true"/>
																<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
																<div class="float-left">
																<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
																	id="amountmode_${status.index+1}" value="false" style="width:5px;"/>
																<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
														</c:otherwise> 
											</c:choose> 
											</td>
											<td>
												<c:choose>
													<c:when test="${DETAIL.discount ne null && DETAIL.discount gt 0 }">
														<input type="text" name="discount" id="discount_${status.index+1}" value="${DETAIL.discount}"
															style="text-align: right;" class="discount validate[optional,custom[number]] width98 right-align"> 
													</c:when>
													<c:otherwise>
														<input type="text" name="discount" id="discount_${status.index+1}"
															style="text-align: right;" class="discount validate[optional,custom[number]] width98 right-align"> 
													</c:otherwise>
												</c:choose>
												<c:set
													var="discount" value="${DETAIL.discount}" />
											</td>
											<td> 
											<c:set var="totalAmount"
													value="${DETAIL.packageUnit * DETAIL.unitRate}" /> <%
												 	double discount = 0;
												 		double totalAmount = Double.parseDouble(pageContext
												 				.getAttribute("totalAmount").toString());
												 		if (null != pageContext.getAttribute("isPercentage")) {
												 			if (("true").equals(pageContext
												 					.getAttribute("isPercentage").toString())) {
												 				discount = Double.parseDouble(pageContext.getAttribute(
												 						"discount").toString());
												 				discount = (totalAmount * discount) / 100;
												 				totalAmount -= discount;
												 			} else {
												 				discount = Double.parseDouble(pageContext.getAttribute(
												 						"discount").toString());
												 				totalAmount -= discount;
												 			}
												 		}
												 %> <span id="totalAmount_${status.index+1}" style="float: right;"></span>
												 <input type="text" style="display: none" id="totalAmountH_${status.index+1}" value="<%=totalAmount%>"
									 				class="totalAmountH"/>
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="salesOrderDetailId"
												id="salesOrderDetailId_${status.index+1}"
												value="${DETAIL.salesOrderDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:if
										test="${SALES_ORDER.customerQuotation.customerQuotationId eq null || SALES_ORDER.customerQuotation.customerQuotationId eq ''}">
										<c:forEach var="i"
											begin="${fn:length(SALES_ORDER.salesOrderDetails)+1}"
											end="${fn:length(SALES_ORDER.salesOrderDetails)+2}" step="1"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${i}">
												<td style="display: none;" id="lineId_${i}">${i}</td>
												<td><input type="hidden" name="productId"
													id="productid_${i}" /> <span id="product_${i}" class="width60 float-left"></span> 
													 <span class="width10 float-right" id="unitCode_${i}"
												 		 style="position: relative;"></span>
													<span
													class="button float-right"> <a
														style="cursor: pointer;" id="prodID_${i}"
														class="btn ui-state-default ui-corner-all show_product_list_sales_order width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td style="display: none;"><input type="hidden" name="storeId"
													id="storeid_${i}" /> <span id="store_${i}"></span> <span
													class="button float-right"> <a
														style="cursor: pointer;" id="storeID_${i}"
														class="btn ui-state-default ui-corner-all show_store_list width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td>
													<select name="packageType" id="packageType_${i}" class="packageType">
														<option value="">Select</option>
													</select> 
												</td> 
												<td><input type="text"
													class="width96 packageUnit validate[optional,custom[number]]"
													id="packageUnit_${i}"  />
		 										</td>
												<td><input type="hidden" name="productQty"
													id="productQty_${i}"  class="productQty validate[optional,custom[number]] width80"> <span
													class="button float-right"> <a
														style="cursor: pointer; position: relative; top: 7px; display: none;"
														id="showBasePrice_${i}"
														class="btn ui-state-default ui-corner-all baseprice-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
														<span id="baseUnitConversion_${i}" class="width10" style="display: none;"></span>
														<span id="baseDisplayQty_${i}"></span>
												</td>
												<td><input type="text" name="amount" id="amount_${i}"
													style="text-align: right;"
													class="amount width60  validate[optional,custom[number]] right-align">  <input type="hidden"
													name="basePrice" id="basePrice_${i}" /> 
													<input type="hidden"
													name="standardPrice" id="standardPrice_${i}" />
													<input type="hidden"
													name="allowSave" id="allowSave_${i}" />
													<span
													class="button float-right"> <a
														style="cursor: pointer; position: relative; top: 7px;"
														id="pricingDetail_${i}"
														class="btn ui-state-default ui-corner-all show_productpricing_list pricingDetail width100">
															<span class="ui-icon ui-icon-newwin"></span> </a> </span>
													<input type="hidden" id="baseSellingPrice_${i}"/>
												</td>
												<td><div class="float-left">
												<input type="radio" name="chargesmodeDetail_${i}" class="chargesmodeDetail float-left" style="width:5px;"
													 id="pecentagemode_${i}" value="true"/>
												<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
												<div class="float-left">
												<input type="radio" name="chargesmodeDetail_${i}" class="chargesmodeDetail float-left" 
													id="amountmode_${i}" value="false" style="width:5px;"/>
												<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</td>
												<td><input type="text" name="discount"
													id="discount_${i}" style="text-align: right;"
													class="discount width98 validate[optional,custom[number]] right-align">
												</td>
												<td><span id="totalAmount_${i}" style="float: right;"></span>
													 <input type="text" style="display: none" id="totalAmountH_${i}" class="totalAmountH"/>
												</td>
												<td style="width: 1%;" class="opn_td" id="option_${i}">
													<a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="cursor: pointer; display: none;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span
														class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="salesOrderDetailId"
													id="salesOrderDetailId_${i}" />
												</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90 sales_charges_block" id="hrm"
				style="margin-top: 5px;">
				<fieldset>
					<legend>Sales Charges</legend>
					<div id="hrm" class="hastable width100">
						<table id="hastab_charges" class="width100">
							<thead>
								<tr>
									<th style="width: 1%">L.NO</th>
									<th class="width10">Charges Type</th>
									<th style="width: 5%; display: none;">Mode</th>
									<th style="width: 5%;">Charges</th>
									<th class="width10">Description</th>
									<th style="width: 1%;" class="options"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tabcharges">
								<c:forEach var="CHARGES"
									items="${SALES_ORDER.salesOrderCharges}" varStatus="status">
									<tr class="rowidcharges" id="fieldrowcharges_${status.index+1}">
										<td id="chargesLineId_${status.index+1}">${status.index+1}</td>
										<td><input type="hidden" name="tempchargesTypeId"
											id="tempChargesTypeId_${status.index+1}"
											value="${CHARGES.lookupDetail.lookupDetailId}" /> <select
											name="chargesTypeId" id="chargesTypeId_${status.index+1}"
											class="chargestypeid width70">
												<option value="">Select</option>
												<c:forEach var="TYPE" items="${CHARGES_TYPE}">
													<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
												</c:forEach>
										</select>
										<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="CHARGES_TYPE_${status.index+1}"
											class="btn ui-state-default ui-corner-all chargestype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
										</td>
										<td style="display: none;"><input type="hidden" name="tempChargesMode"
											id="tempChargesMode_${status.index+1}"
											value="${CHARGES.isPercentage}" /> <select
											name="chargesMode" id="chargesMode_${status.index+1}"
											class="chargesmode width90">
												<option value="">Select</option>
												<option value="true">Percentage</option>
												<option value="false">Amount</option>
										</select>
										</td>
										<td><input type="text" name="charges"
											id="charges_${status.index+1}" value="${CHARGES.charges}"
											class="charges validate[optional,custom[number]] width98" style="text-align: right;">
										</td>
										<td><input type="text" name="description"
											id="description_${status.index+1}"
											value="${CHARGES.description}" class="description width98">
										</td>
										<td style="width: 1%;" class="opn_td"
											id="optioncharges_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
											id="AddImagecharges_${status.index+1}"
											style="cursor: pointer; display: none;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
											id="EditImagecharges_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
											id="DeleteImagecharges_${status.index+1}"
											style="cursor: pointer;" title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImagecharges_${status.index+1}"
											style="display: none;" title="Working"> <span
												class="processing"></span> </a> <input type="hidden"
											name="salesOrderChargeIdId"
											id="salesOrderChargeId_${status.index+1}"
											value="${CHARGES.salesOrderChargeId}" />
										</td>
									</tr>
								</c:forEach>
								<c:if
									test="${SALES_ORDER.customerQuotation.customerQuotationId eq null || SALES_ORDER.customerQuotation.customerQuotationId eq ''}">
									<c:forEach var="i"
										begin="${fn:length(SALES_ORDER.salesOrderCharges)+1}"
										end="${fn:length(SALES_ORDER.salesOrderCharges)+2}" step="1"
										varStatus="status">
										<tr class="rowidcharges" id="fieldrowcharges_${i}">
											<td id="chargesLineId_${i}">${i}</td>
											<td><select name="chargesTypeId" id="chargesTypeId_${i}"
												class="chargestypeid width70">
													<option value="">Select</option>
													<c:forEach var="TYPE" items="${CHARGES_TYPE}">
														<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
													</c:forEach>
											</select>
											<span class="button" style="position: relative;"> <a
											style="cursor: pointer;" id="CHARGES_TYPE_${i}"
											class="btn ui-state-default ui-corner-all chargestype-lookup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
											<td style="display: none;"><select name="chargesMode" id="chargesMode_${i}"
												class="chargesmode width90">
													<option value="">Select</option>
													<option value="true">Percentage</option>
													<option value="false">Amount</option>
											</select>
											</td>
											<td><input type="text" name="charges" id="charges_${i}"
												class="charges validate[optional,custom[number]] width98" style="text-align: right;">
											</td>
											<td><input type="text" name="description"
												id="description_${i}" class="description width98">
											</td>
											<td style="width: 1%;" class="opn_td" id="optioncharges_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
												id="AddImagecharges_${i}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
												id="EditImagecharges_${i}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
												id="DeleteImagecharges_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImagecharges_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="salesOrderChargeIdId"
												id="salesOrderChargeId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="salesorder_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
				<div
					class="portlet-header ui-widget-header float-left addrowscharges"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
				<div class="clearfix"></div>
				<div id="sales-product-inline-popup" style="display: none;"
					class="width100 float-left" id="hrm">
					
				</div>
			</div>
			
			<div class="clearfix"></div>
			<div id="baseprice-popup-dialog" style="display: none;">
				<div class="width100 float-left" id="hrm" style="padding: 5px;">
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Product</label> <span id="productCode"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Unit Name</label> <span id="unitName"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Costing Type</label> <span id="costingType"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Store Quantity</label> <span
							id="storeQuantity"></span>
					</div>
					<div class="width60 float-left" style="padding: 2px;">
						<label class="width40">Base Price</label> <span id="basePrice"></span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>