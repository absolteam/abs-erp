<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.wastagesType>label{float: none !important;}
</style>
<script type="text/javascript">
var slidetab = "";
var accountGroupDetails = [];
var accessCode = "";
var detailRowId = 0;
var tempid=null;
$(function(){
	manupulateLastRow();
	$jquery("#materialWastageValidation").validationEngine('attach');  
	 
	
	 $('#account_cancel').click(function(event){  
		 accountGroupDiscard("");
		 return false;
	 });

	 $('#account_save').click(function(){
		 accountGroupDetails = [];
		 if($jquery("#accountGroupValidation").validationEngine('validate')){
 		 		
	 			var code = $('#code').val(); 
 	 			var title = $('#title').val(); 
	 			var accountGroupId = Number($('#accountGroupId').val());   
	 			var description=$('#description').val();   
	 			var accountType=$('#accountType').val();   
	 			 
	 			accountGroupDetails = getAccountGroupDetails();  
	 			 
	   			if(accountGroupDetails!=null && accountGroupDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/account_group_save.action", 
	 				 	async: false, 
	 				 	data:{	code:code,title:title,accountGroupId:accountGroupId,accountType:accountType,
	 				 		description: description, groupDetails: JSON.stringify(accountGroupDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							accountGroupDiscard(accountGroupId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown();
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter combination details.").slideDown();
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 

	  var getAccountGroupDetails = function(){
		  accountGroupDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var combinationId = Number($('#combinationId_'+rowId).val());  
				var accountGroupDetailId = Number($('#accountGroupDetailId_'+rowId).val());  
				
				if(typeof combinationId != 'undefined' && combinationId > 0){ 
					accountGroupDetails.push({
						"accountGroupDetailId" : accountGroupDetailId,
						"combinationId" : combinationId
					});
				} 
			});  
			return accountGroupDetails;
		 }; 


	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/account_group_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

 	 	
 	//Combination pop-up config
  	$('.codecombination-popup').live('click',function(){ 
  	    tempid=$(this).parent().get(0);  
  	    $('.ui-dialog-titlebar').remove();   
  	 	$.ajax({
  			type:"POST",
  			url:"<%=request.getContextPath()%>/combination_treeview.action", 
  		 	async: false, 
  		    dataType: "html",
  		    cache: false,
  			success:function(result){   
  				 $('.codecombination-result').html(result);
  				return false;
  			},
  			error:function(result){ 
  				 $('.codecombination-result').html(result); 
  				return false;
  			}
  		});  
  		return false;
  	});
  	
  	 $('#codecombination-popup').dialog({
  			autoOpen: false,
  			minwidth: 'auto',
  			width:800, 
  			bgiframe: false,
  			modal: true
  	 });

 	
  	var accountGroupId = Number($('#accountGroupId').val());
  	if(accountGroupId!=null && accountGroupId>0){
		$('#accountType').val($('#accountTypeId').val());
  	}

});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function setCombination(combinationTreeId, combinationTree) {
	var idVals = $(tempid).attr('id').split("_");
	var rowids = Number(idVals[1]);
	$('#combinationId_' + rowids).val(combinationTreeId);
	$('#codeCombination_' + rowids).text(combinationTree);
	$('#codeCombination_' + rowids).text().replace(/[\s\n\r]+/g, ' ')
			.trim();
	validateCombinationAccountType(combinationTreeId,rowids);
	triggerAddRow(rowids);
	return false;
}
function validateCombinationAccountType(combinationId,rowids){
	var accountType=Number($('#accountType option:selected').val());
	var name=$('#accountType option:selected').text();
	if(accountType>0){
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/validate_account_type.action", 
			 	async: false, 
			 	data:{combinationId:combinationId,accountType:accountType},
			    dataType: "json",
			    cache: false,
				success:function(response){ 
					if(response.returnMessage!="SUCCESS"){
						$('#combinationId_' + rowids).val("");
						$('#codeCombination_' + rowids).text("");  
						$('#page-error').hide().html("choosen combination is not a valid '"+name+"' account").slideDown(1000); 
						$('#page-error').delay(2000).slideUp();
					}
					return false;
				},
				error:function(result){ 
					$('#combinationId_' + rowids).val("");
					$('#codeCombination_' + rowids).text("");  
					$('#page-error').hide().html("choosen combination is not a valid '"+name+"' account").slideDown(1000); 
					$('#page-error').delay(2000).slideUp();
					return false;
				}
			}); 
		return false;
	}else{
		$('#combinationId_' + rowids).val("");
		$('#codeCombination_' + rowids).text("");  
		$('#page-error').hide().html("choose account type").slideDown(1000); 
		$('#page-error').delay(2000).slideUp();
		return false;
	}
	return false;
}
function triggerAddRow(rowId){   
	var combinationId = Number($('#combinationId_'+rowId).val());
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(combinationId > 0  && $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	}
	return false;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
	return false;
}
function accountGroupDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_account_group.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
 return false;
} 

</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>COA Group
		</div> 
	
		<form name="accountGroupValidation" id="accountGroupValidation"
			style="position: relative; display: ${showhide};">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<input type="hidden" id="accountGroupId" value="${ACCOUNT_GROUP.accountGroupId}"/>
 				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 95px;"> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									class="width51">${ACCOUNT_GROUP.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Code<span
									style="color: red;">*</span> </label>
								<input type="text" id="code" name="code" disabled="disabled" readonly="readonly"
											value="${ACCOUNT_GROUP.code}" 	class="validate[required] width50" />
							</div> 
							<div>
								<label class="width30"> Title<span
									style="color: red;">*</span> </label>
								<input type="text" id="title" name="title"
											value="${ACCOUNT_GROUP.title}" 	class="validate[required] width50" />
							</div> 
							<div>
								<label class="width30"> Account Type<span
									style="color: red;">*</span> </label>
								<select name="accountType" id="accountType" class="width51">
									<option value="">Select</option>
									<option value="1">Assets</option>
									<option value="2">Liabilities</option>
									<option value="3">Revenue</option>
									<option value="4">Expenses</option>
									<option value="5">Owners Equity</option>
								</select>
								<input type="hidden" id="accountTypeId" name="accountTypeId"
											value="${ACCOUNT_GROUP.accountType}" 	class="width50" />
							</div> 
							
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							COA Combination Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Account Combination</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${ACCOUNT_GROUP_DETAIL}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><span
												id="codeCombination_${status.index+1}">${DETAIL.combinationName}</span>
												<span class="button float-right" id="tempCombID_${status.index+1}"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all codecombination-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												<input type="hidden" name="combinationId" id="combinationId_${status.index+1}" 
												value="${DETAIL.accountGroupDetail.combination.combinationId}" />
											</td> 
											
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="accountGroupDetailId"
												id="accountGroupDetailId_${status.index+1}"
												value="${DETAIL.accountGroupDetail.accountGroupDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(ACCOUNT_GROUP_DETAIL)+1}"
										end="${fn:length(ACCOUNT_GROUP_DETAIL)+2}"
										step="1" varStatus="status"> 
										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><span
												id="codeCombination_${i}"></span>
												<span class="button float-right" id="tempCombID_${i}"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all codecombination-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												<input type="hidden" name="combinationId" id="combinationId_${i}" value="" />
											</td> 
										
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="accountGroupDetailId"
												id="accountGroupDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="account_cancel" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="account_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>