<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>  
<script type="text/javascript"> 
var currentId=""; var tempvar=""; var idarray=""; var rowid="";
$(function(){ 
		$($($('#store-popup').parent()).get(0)).css('width', 500);
		$($($('#store-popup').parent()).get(0)).css('height', 250);
		$($($('#store-popup').parent()).get(0)).css('padding', 0);
		$($($('#store-popup').parent()).get(0)).css('left', 0);
		$($($('#store-popup').parent()).get(0)).css('top', 100);
		$($($('#store-popup').parent()).get(0)).css('overflow', 'hidden');

		$('#store-popup').dialog('open'); 

		$('.rackidmrp').live('click', function() {
			$('.rackid ').each(function() {
				currentId = $(this);
				tempvar = $(currentId).attr('id');
				idarray = tempvar.split('_');
				rowid = Number(idarray[1]);
				if ($('#rackselect_' + rowid).hasClass('selected')) {
					$('#rackselect_' + rowid).removeClass('selected');
				}
				currentId = 0;
			});
			currentId = $(this);
			tempvar = $(currentId).attr('id');
			idarray = tempvar.split('_');
			rowid = Number(idarray[1]);
			$('#rackselect_' + rowid).addClass('selected');
		});

		

		$('.store_closemrp').click(function() {
			$('#store-popup').dialog('close');
			$('#common-popup').dialog('close');
		});

		$("#store_tree_view").treeview({
			collapsed: true,
			animated: "medium",
			control:"#sidetreecontrol",
			persist: "location"
		}); 
});
</script>
<style>
.selected {
	background: url("./images/checked.png");
	background-repeat: no-repeat;
}
</style>
<div class="portlet-content">
	<div class="mainhead portlet-header ui-widget-header">
		<span style="display: none;"
			class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Store
		Details
	</div>
	<span style="display: none;" id="store_row_idmrp">${requestScope.rowId}</span>
	<ul id="store_tree_view">
		<li><strong>Stores</strong></li>
		<c:choose>
			<c:when test="${STORE_INFO ne null && STORE_INFO ne''}">
				<c:forEach items="${STORE_INFO}" var="store" varStatus="status1">
					<li id="store_${status1.index+1}"><span style="display: none;"
						id="storeid_${status1.index+1}">${store.storeId}</span> <span
						id="storename_${status1.index+1}" class="storeid">${store.storeName}
					</span>
						<ul>
							<c:forEach items="${store.aisleVOs}" var="aisle"
								varStatus="status">
								<span style="display: none;" id="aisleid_${status.index+1}">${aisle.aisleId}</span>
								<span style="display: none;" id="aisleNO_${status.index+1}">${aisle.aisleNumber}</span>
								<li id="aislenumber_${status.index+1}" class="accountid width30"
									style="cursor: pointer;">${aisle.sectionName}
									(${aisle.lookupDetail.displayName}) <c:if
										test="${aisle.shelfVOs ne null && aisle.shelfVOs ne '' && fn:length(aisle.shelfVOs)>0}">
										<ul>
											<c:forEach items="${aisle.shelfVOs}" var="shelf"
												varStatus="cstatus">
												<span style="display: none;" id="shelfid_${shelf.shelfId}">${shelf.shelfId}</span>
												<li id="shelf_${shelf.shelfId}" class="shelfid width50"
													style="cursor: pointer;">${shelf.name}(${shelf.shelfTypeName})
													<c:if
														test="${shelf.rackDetails ne null && shelf.rackDetails ne '' && fn:length(shelf.rackDetails)>0}">
														<ul>
															<c:forEach items="${shelf.rackDetails}" var="rack"
																varStatus="rstatus"> 
																<span style="display: none;" id="rackstoreid_${rack.shelfId}">${store.storeId}</span>
																<span style="display: none;" id="rackstorename_${rack.shelfId}">${store.storeName} &gt;&gt; ${aisle.lookupDetail.displayName} &gt;&gt; ${rack.shelfTypeName}</span>
																<span style="display: none;" id="rackidmrp_${rack.shelfId}">${rack.shelfId}</span>
																<li id="rack_${rack.shelfId}" class="rackidmrp width80"
																	style="cursor: pointer;">${rack.name}(${rack.shelfTypeName})</li>
																<span
																	id="rackselect_${rack.shelfId}"
																	class="float-right"
																	style="height: 30px; width: 20px; position: relative; top: -31px;"></span>
															</c:forEach>
														</ul>
													</c:if></li>
											</c:forEach>
										</ul>
									</c:if></li>
							</c:forEach>
						</ul></li>
				</c:forEach>
			</c:when>
		</c:choose>
	</ul>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="margin: 10px;">
		<div class="portlet-header ui-widget-header float-right store_closemrp"
			style="cursor: pointer;">close</div>
	</div>
</div> 