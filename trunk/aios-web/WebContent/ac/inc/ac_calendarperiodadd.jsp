<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<td colspan="6" class="tdidentity" id="childRowId_${requestScope.rowId}">
	<div id="errMsg"></div>
	<form name="periodform" id="periodEditAddValidate" style="position: relative;">
		<input type="hidden" name="addEditFlag" id="addEditFlag"
			value="${requestScope.AddEditFlag}" />
		<div class="width50  float-right" id="hrm">
			<fieldset>
				<legend>
					<fmt:message key="accounts.calendar.label.periodvalidity" />
				</legend>
				<span class="float-left"><fmt:message
						key="accounts.calendar.label.fromdate" /><span
					style="color: red;">*</span> </span> <input type="text" 
					name="startDate" id="startPicker_"
					class="width30 float-left from validate[required]"> <span><fmt:message
						key="accounts.calendar.label.todate" /> <span style="color: red;">*</span>
				</span> <input type="text" id="endPicker_" class="width30 to"
					name="endDate" style="margin-bottom: 5px;">
				<fieldset>
					<legend>
						Period Status<span style="color: red;">*</span>
					</legend>
					<select name="status"
						id="periodStatus_${requestScope.rowid}"
						class="width60 periodStatus validate[required]">
						<c:forEach var="period" items="${PERIOD_STATUS}">
							<c:choose>
								<c:when test="${period.key ne 3}">
									<option value="${period.key}">${period.value}</option>
								</c:when>
								<c:otherwise>
									<option value="${period.key}" selected="selected">${period.value}</option>
								</c:otherwise> 
							</c:choose>
						</c:forEach> 
					</select>
				</fieldset>
			</fieldset>
			<div id="othererror" class="response-msg error ui-corner-all"
				style="width: 60%; display: none;"></div>
		</div>

		<div class="float-left width48" id="hrm" style="margin-left: 10px;">
			<fieldset>
				<legend>
					<fmt:message key="accounts.calendar.label.perioddetails" />
				</legend>
				<div>
					<label><fmt:message
							key="accounts.calendar.label.periodname" /> <span
						style="color: red;">*</span>
					</label> <input type="text" name="name" id="name"
						class="width50 validate[required]">
				</div>
				<div>
					<label><fmt:message
							key="accounts.calendar.label.adjustingperiod" /> <span
						style="color: red;">*</span>
					</label><input type="text" name="extention" maxlength="2"
						id="extention" class="width50 validate[custom[onlyNumber]]">
				</div>
			</fieldset>
		</div>
		<div class="clearfix"></div>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
			style="margin: 20px;">
			<div
				class="portlet-header ui-widget-header float-right cancel_period"
				id="cancel_${requestScope.rowid}" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right add_period"
				id="update_${requestScope.rowid}" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
	</form></td>
<script type="text/javascript">
	 var trval=$('.tdidentity').parent().get(0);
	 var fromFormat='dd-MMM-yyyy';
     var toFormat='yyyyMMdd';
     var usrfromFormat='dd-MMM-yyyy';
     var usrtoFormat='yyyyMMdd';	
     var statusFlag=false;
     var statusNameFlag=false;
     var addFlag=false;
	 $(function(){   

		$('#name').change(function(){ 
			$('#othererror').hide();
			var rowVal=""; var tempVar="";var idVal=0;
			var name=$(this).val().trim().toLowerCase();  
			statusNameFlag=false;
			if(name!=null && name!=""){
				$('.rowid').each(function(){
					  rowVal=$(this).attr('id');
					  tempVar=rowVal.split('_');
					  idVal=Number(tempVar[1]); 
					  var nametext=$('#name_'+idVal).text().toLowerCase().trim(); 
					  if(nametext==name && statusNameFlag==false){
						  $('#othererror').hide().html("Period Name already exits").slideDown(1000);
						  statusNameFlag=true;
						  return false;
					  } 
				 });
			} 
		});
		 
		  $('.cancel_period').click(function(){ 
			  $('.formError').remove();	
				//Find the Id for fetch the value from Id
				//var tempvar=$(this).parent().parent().attr("id");
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=Number(idarray[1]); 

				if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
				{
  					$("#AddFinancialImage_"+rowid).show();
  				 	$("#EditFinancialImage_"+rowid).hide();
				}else{
					$("#AddFinancialImage_"+rowid).hide();
  				 	$("#EditFinancialImage_"+rowid).show();
				}
				 $("#childRowId_"+rowid).remove();	
				 $("#DeleteFinancialImage_"+rowid).show();
				 $("#WorkingFinancialImage_"+rowid).hide();  
		 
				var totalTR = $(".tabFinancial>tr").size();
				totalTR = totalTR -1 ; 
				var tempvar = $('.tabFinancial>#fieldrow_'+totalTR).attr('id');  
				idval=tempvar.split("_");
				rowid=Number(idval[1]);   
				$('#DeleteFinancialImage_'+rowid).hide(); 
				$('#openFlag').val(0);
		  });

		  $jquery("#periodEditAddValidate").validationEngine('attach');
		  
		  $('.add_period').click(function(){ 
			 
				var tempvar=$('.tdidentity').attr('id');
				var idarray = tempvar.split('_');
				var rowid=0;
				rowid=Number(idarray[1]); 
				if($jquery("#periodEditAddValidate").validationEngine('validate')){  
					if(statusNameFlag==false){  
						 var periodId=$('#periodId_'+rowid).val();	
						 var name=$('#name').val();
						 var startTime=$('.from').val();
						 var endTime=$('.to').val();
						 var extention=$('#extention').val();
						 var lineId=$('#lineId_'+rowid).text();
						 var periodStatus=$('#periodStatus_'+rowid).val();
						 var status=$('#periodStatus_'+rowid +' option:selected').text();  
						 var dynamicAction="";
			
							if($('#addEditFlag').val()!=null && $('#addEditFlag').val()=='A')
							{
								
							 	dynamicAction="<%=request.getContextPath()%>/gl_calendar_period_add_save_action.action";
							 	addFlag=true;
							}else{
								
							 	dynamicAction="<%=request.getContextPath()%>/gl_calendar_period_edit_save_action.action";
								addFlag=false;
							} 
						//Mater date and period date Validation 
					        var fromdate=Number(formatDate(new Date(getDateFromFormat($("#startPicker").val(),fromFormat)),toFormat));
					        var todate=Number(formatDate(new Date(getDateFromFormat($("#endPicker").val(),fromFormat)),toFormat));
					        var usrfromdate=Number(formatDate(new Date(getDateFromFormat(startTime,usrfromFormat)),usrtoFormat));
					        var usrtodate=Number(formatDate(new Date(getDateFromFormat(endTime,usrfromFormat)),usrtoFormat));
					        if(todate==19700101 || todate==99991231 || todate==80991231){
					        	 todate=99991231;
						    	 if(usrtodate==19700101){
							    	 usrtodate=99991231;
							    	 endDate=99991231;
							     }   
						     } 
					        if(fromdate <= usrfromdate && todate >= usrtodate && todate >= usrfromdate && usrfromdate<= usrtodate){
					        	 var flag=false;
					        	$.ajax({
									type:"POST", 
									url:dynamicAction,
								 	async: false, 
								 	data:{	periodId:periodId, name: name, startTime: startTime, endTime: endTime, extention: extention, 
									 		rowId: lineId, periodStatus: periodStatus
									 	},
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('.tempresult').html(result); 
									     if(result!=null) 
			                                 flag = true;
									     $('#openFlag').val(0);
								 	}
					        	});  
					        	if(flag==true){ 
						        	//Bussiness parameter
					        			$('#name_'+rowid).text(name); 
					        			$('#startTime_'+rowid).text(startTime); 
					        			$('#endTime_'+rowid).text(endTime); 
					        			$('#extention_'+rowid).text(extention);
					        			$('#periodstatus_'+rowid).text(status);
					        			$("#statusline_"+rowid).val(periodStatus);
									//Button(action) hide & show 
					    				 $("#AddFinancialImage_"+rowid).hide();
					    				 $("#EditFinancialImage_"+rowid).show();
					    				 $("#DeleteFinancialImage_"+rowid).show();
					    				 $("#WorkingFinancialImage_"+rowid).hide(); 
					        			 $("#childRowId_"+rowid).remove();	 
					        			 triggerFinancialAddRow(rowid);	
			
									  	var childCount=Number($('#childCount').val());
										childCount=childCount+1;
										$('#childCount').val(childCount);
			
										if(childCount>=1){ 
											$('#startPicker').attr("disabled",true);
											$('#endPicker').attr("disabled",true);  
										}
					        		}		  	 
					        	} 
					        else{
					        	$("#othererror").hide().html("Period dates should be less then or equal to master dates.").slideDown();  
					        	return false;
					        } 
					  } else{
							 $("#othererror").hide().html("Period Name Already exits.").slideDown(1000);
							 return false;
						 }
				} 
			      else
				      return false;  
		  }); 
		  $('#startPicker_,#endPicker_').datepick({
				 onSelect: customRange_,showTrigger: '#calImg'});   
		 
});
	//Prevent selection of invalid dates through the select controls
	 function checkLinkedDays(){
	     var daysInMonth =$.datepick.daysInMonth($('#selectedYear').val(), $('#selectedMonth').val());
	     $('#selectedDay option:gt(27)').attr('disabled', false);
	     $('#selectedDay option:gt(' + (daysInMonth - 1) +')').attr('disabled', true);
	     if ($('#selectedDay').val() > daysInMonth){
	         $('#selectedDay').val(daysInMonth);
	     }
	 } 
	 function customRange_(dates) {
	 	if (this.id == 'startPicker_') {
	 		$('#endPicker_').datepick('option', 'minDate', dates[0] || null); 
	 		addFinancePeriod();
	 	}
	 	else{
	 		$('#startPicker_').datepick('option', 'maxDate', dates[0] || null);  
	 	} 
	 }
	 function addFinancePeriod(){
		 var date = new Date($('#startPicker_').datepick('getDate')[0].getTime());  
		    $.datepick.add(date, parseInt(30, 10), 'd');
		    $('#endPicker_').val($.datepick.formatDate(date)); 
	 } 
</script>
