<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<script type="text/javascript">
var slidetab="";
var tempid = "";
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
	
	if (Number($('#productPricingId').val()) > 0) {
 		$('#currency').val($('#tempCurrency').val()); 
	}else {
 		$('#currency').val($('#defaultCurrency').val());
 	}  
 	 
	$('input, select').attr('disabled', true); 
	
}); 
 
</script>
<div id="main-content">
	<div id="pr_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Product
			Pricing
		</div> 
		<form id="productPricingValidation" method="POST"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="productPricingId" name="productPricingId"
					value="${PRODUCT_PRICING.productPricingId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30"> Currency </label> <select name="currency"
									id="currency" class="width51">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${PRODUCT_PRICING.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${PRODUCT_PRICING.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30">Pricing Title </label> <input name="startDate"
									type="text" value="${PRODUCT_PRICING.pricingTitle}"
									id="pricingTitle"
									class="pricingTitle validate[required] width50">
							</div>
							<div>
								<label class="width30"> Start Date  </label>
								<c:choose>
									<c:when
										test="${PRODUCT_PRICING.startDate ne null && PRODUCT_PRICING.startDate ne ''}">
										<c:set var="startDate" value="${PRODUCT_PRICING.startDate}" />
										<input name="startDate" type="text" readonly="readonly"
											id="startDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("startDate").toString())%>"
											class="startDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="startDate" type="text" readonly="readonly"
											id="startDate" class="startDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> End Date
								</label>
								<c:choose>
									<c:when
										test="${PRODUCT_PRICING.endDate ne null && PRODUCT_PRICING.endDate ne ''}">
										<c:set var="endDate" value="${PRODUCT_PRICING.endDate}" />
										<input name="endDate" type="text" readonly="readonly"
											id="endDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("endDate").toString())%>"
											class="endDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="endDate" type="text" readonly="readonly"
											id="endDate" class="endDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Pricing Detail
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<c:if test="${sessionScope.unified_price eq 'false' && PRODUCT_PRICING.productPricingId gt 0}">
											<th style="width: 5%;">Store</th>
										</c:if>
										<th style="width: 5%;">Cost Price</th>
										<th style="width: 5%;">Standard Price</th>
										<th style="width: 5%;">MSRP</th>
										<th style="width: 5%;">Selling Price</th>
										<th style="width: 5%;">Description</th>
										<th style="width: 5%; display : none;">Define Price</th>
										<th style="width: 1%; display : none;" class="options"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab"
									style="max-height: 255px; overflow-x: hidden; overflow-y: auto;">
									<c:forEach var="DETAIL"
										items="${PRODUCT_PRICING.productPricingDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> 
												<span id="productscroll_${DETAIL.product.productId}"></span> 
												 <span
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span> 
											</td>
											<c:if test="${sessionScope.unified_price eq 'false'}">
												<td> 
													${DETAIL.store.storeName}
												</td>
											</c:if>
											<td><input type="text" name="basicCostPrice"
												id="basicCostPrice_${status.index+1}"
												value="${DETAIL.basicCostPrice}"
												class="basicCostPrice width80">
											</td>
											<td><input type="text" name="standardPrice"
												id="standardPrice_${status.index+1}"
												value="${DETAIL.standardPrice}"
												class="standardPrice width80 validate[optional,custom[number]]">
											</td>
											<td><input type="text" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${status.index+1}"
												value="${DETAIL.suggestedRetailPrice}"
												class="suggestedRetailPrice width80 validate[optional,custom[number]]">
											</td>
											<td><input type="text" name="sellingPrice"
												id="sellingPrice_${status.index+1}"
												value="${DETAIL.sellingPrice}"
												class="sellingPrice width80">
											</td>
											<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${DETAIL.description}" class="description width98">
											</td>
											<td style="display: none;"><input type="text"
												id="forFocus_${DETAIL.productPricingDetailId}"
												value="${DETAIL.productPricingDetailId}" class="width98">
											</td>
											<td style="display : none;">
												<div
													class="portlet-header ui-widget-header float-left editData"
													style="cursor: pointer;" id="EditImage_${status.index+1}">
													Define Price</div>
											</td>
											<td style="width: 1%; display : none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productPricingDetailId"
												id="productPricingDetailId_${status.index+1}"
												value="${DETAIL.productPricingDetailId}" />
												<input type="hidden" name="storeDetailId"
														id="storedetailid_${status.index+1}"
														value="${DETAIL.store.storeId}" />
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>  
		</form>
	</div>
	<input type="hidden" value="${requestScope.productPricingDetailId}" id="idForFocus" />
</div>
<div class="clearfix"></div>