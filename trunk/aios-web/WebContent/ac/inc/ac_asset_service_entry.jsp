<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var serviceDetails="";
var popval = "";
var accessCode = "";
$(function(){  

	manupulateLastRow();
	$jquery("#asset_services").validationEngine('attach');  

	$('.serviceDate,.extimatedDate,.nextServiceDate').datepick();

	$('.serviceDate').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	});

	$('.priority').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	}); 

	$('.servicetype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	


	$('.serviceterm-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	


	$('.contracttype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="ASSET_SERVICE_TYPE"){
			$('#serviceType').html("");
			$('#serviceType').append("<option value=''>Select</option>");
			loadLookupList("serviceType"); 
		} else if(accessCode=="ASSET_SERVICE_TERM"){
			$('#serviceTerm').html("");
			$('#serviceTerm').append("<option value=''>Select</option>");
			loadLookupList("serviceTerm"); 
		}  else if(accessCode=="ASSET_CONTRACT_TYPE"){
			$('#contractType').html("");
			$('#contractType').append("<option value=''>Select</option>");
			loadLookupList("contractType"); 
		}  
	});
	

	$('.addrows').click(function(){ 
		  var i = Number(1); 
		  var id = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id = id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/asset_service_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	$('.delrow').live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);  
      	$(slidetab).remove();  
      	var i=1;
   	 	$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	 });

	 $('#service_discard').click(function(){ 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_service.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#common-popup').dialog('destroy');		
				 $('#common-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		 });
	 });

	 $('#service_save').click(function(){   
		if($jquery("#asset_services").validationEngine('validate')){
 			var assetCreationId = Number($('#assetCreationId').val());   
			var serviceNumber = $('#serviceNumber').val();
			var assetServiceId = Number($('#assetServiceId').val());
			var serviceType = Number($('#serviceType').val());
			var serviceLevel = Number($('#serviceLevel').val()); 
	 		var serviceTerm = Number($('#serviceTerm').val());
	 		var serviceInChargeId = Number($('#serviceInChargeId').val());
	 		var contactNumber= $('#contactNumber').val();
	 		var contactName = $('#contactName').val();
	 		var contractAmount = Number($('#contractAmount').val());
	 		var provider = $('#provider').val();
	 		var contractType = Number($('#contractType').val()); 
	 		var contractNumber = $('#contractNumber').val();  
			var description = $("#description").val(); 
			serviceDetails = getServiceDetails(); 
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_asset_service.action", 
				 	async: false,
				 	data:{
					 		assetCreationId: assetCreationId, serviceNumber: serviceNumber, assetServiceId: assetServiceId, serviceType: serviceType,
					 		serviceLevel: serviceLevel, serviceTerm: serviceTerm, serviceInChargeId: serviceInChargeId, contactNumber: contactNumber, 
					 		contactName: contactName, contractAmount: contractAmount, provider: provider, contractType: contractType, 
					 		description: description, contractNumber: contractNumber, serviceDetails: serviceDetails
				 		 },
				    dataType: "json",
				    cache: false,
					success:function(response){  
						 if(response.returnMessage == "SUCCESS"){
							 $('#common-popup').dialog('destroy');		
							 $('#common-popup').remove(); 
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/show_asset_service.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){ 
										$("#main-wrapper").html(result); 
										if(assetServiceId > 0)
											$('#success_message').hide().html("Record updated successfully.").slideDown(1000);
										else
											$('#success_message').hide().html("Record created successfully.").slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Internal error!!!").slideDown(1000);
					}
			 }); 
		 }else{return false;}
	 });

	 var getServiceDetails = function(){
		serviceDetails = ""; 
		var serviceDateArray = new Array();
		var extimatedDateArray = new Array();
		var nextServiceDateArray = new Array();
		var serviceManagerArray = new Array();
		var priorityArray = new Array();
		var descriptionArray = new Array(); 
		var assetServiceDetailIdArray =new Array(); 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var serviceDate = $('#serviceDate_'+rowId).val();
			var extimatedDate = $('#extimatedDate_'+rowId).val();
			var serviceManager = $('#serviceManagerId_'+rowId).val();
			var nextServiceDate = $('#nextServiceDate_'+rowId).val();
			var priority = $('#priority_'+rowId).val();
			var description = $('#description_'+rowId).val();
			var assetServiceDetailId = Number($('#assetServiceDetailId_'+rowId).val());   
			if(typeof serviceDate != 'undefined' && serviceDate!=null && serviceDate!="" && extimatedDate!=null && extimatedDate!=""){
				serviceDateArray.push(serviceDate);
				extimatedDateArray.push(extimatedDate); 
				serviceManagerArray.push(serviceManager); 
				nextServiceDateArray.push(nextServiceDate); 
				priorityArray.push(priority); 
				if(description!=null && description!="")
					descriptionArray.push(description);
				else
					descriptionArray.push("##");
				assetServiceDetailIdArray.push(assetServiceDetailId);
			} 
		});
		for(var j=0;j<serviceDateArray.length;j++){ 
			serviceDetails += serviceDateArray[j]+"__"+extimatedDateArray[j]+"__"+serviceManagerArray[j]+"__"+nextServiceDateArray[j]+"__"+priorityArray[j]+"__"+descriptionArray[j]+"__"+assetServiceDetailIdArray[j];
			if(j==serviceDateArray.length-1){   
			} 
			else{
				serviceDetails += "#@";
			}
		} 
		return serviceDetails;
	 };

	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('.common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		popval = $(this).attr('id');  
		var actionValue = "";
		if(popval == "asset"){
			actionValue = "show_common_asset_popup";
		}else{
			actionValue = "common_person_list";
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionValue+".action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
				return false;
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 

	if(Number($('#assetServiceId').val())>0){
		$('#serviceType').val($('#tempServiceType').val());
		$('#serviceLevel').val($('#tempServiceLevel').val());
		$('#serviceTerm').val($('#tempServiceTerm').val());
		$('#contractType').val($('#tempContractType').val()); 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			$('#priority_'+rowId).val($('#tempPriority_'+rowId).val()); 
		});
	}
	
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var serviceDate=$('#serviceDate_'+rowId).val();  
	 var priority=$('#priority_'+rowId).val(); 
	 var nexttab=$('#fieldrow_'+rowId).next(); 
	if(serviceDate!=null && serviceDate!=""
			&& priority!=null && priority!=""  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');  
	}
}

function personPopupResult(personid, personname, commonParam){
	var rowId = popval.split('_')[1]; 
	if(typeof(rowId)!="undefined"){  
		$('#serviceManagerId_'+rowId).val(personid);
		$('#serviceManager_'+rowId).val(personname);
	}else{
		$('#serviceInChargeId').val(personid);
		$('#serviceInCharge').val(personname);
	}
	
	$('#common-popup').dialog("close");   
}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationId').val(assetId);
	$('#assetCreationName').val(assetName); 
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>service
			contract
		</div> 
		<form name="asset_services" id="asset_services" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetServiceId" name="assetServiceId"
					value="${ASSET_SERVICE.assetServiceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset>
							<div>
								<label class="width30" for="contractNumber">Contract
									Number</label> <input type="text" id="contractNumber" class="width50"
									value="${ASSET_SERVICE.contractNumber}">
							</div>
							<div>
								<label class="width30" for="contractType">Contract Type<span
									class="mandatory">*</span>
								</label> <select name="contractType" id="contractType"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="contractType" items="${ASSET_CONTRACT_TYPE}">
										<option value="${contractType.lookupDetailId}">${contractType.displayName}</option>
									</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_CONTRACT_TYPE"
									class="btn ui-state-default ui-corner-all contracttype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" id="tempContractType"
									value="${ASSET_SERVICE.lookupDetailByContractType.lookupDetailId}" />
							</div>
							<div>
								<label class="width30" for="provider">Provider<span
									class="mandatory">*</span>
								</label> <input type="text" id="provider" class="width50"
									value="${ASSET_SERVICE.provider}">
							</div>
							<div>
								<label class="width30" for="contractAmount">Contract
									Amount</label> <input type="text" id="contractAmount" class="width50"
									value="${ASSET_SERVICE.contractAmount}">
							</div>
							<div>
								<label class="width30" for="contactName">Contact Name</label> <input
									type="text" id="contactName" class="width50"
									value="${ASSET_SERVICE.contactPerson}">
							</div>
							<div>
								<label class="width30" for="contactNumber">Contact No</label> <input
									type="text" id="contactNumber" class="width50"
									value="${ASSET_SERVICE.contactNumber}">
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_SERVICE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 225px;">
							<div>
								<label class="width30" for="serviceNumber">Service
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_SERVICE.assetServiceId ne null && ASSET_SERVICE.assetServiceId ne ''}">
										<input type="text" readonly="readonly" name="serviceNumber"
											id="serviceNumber" class="width50 validate[required]"
											value="${ASSET_SERVICE.serviceNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="serviceNumber"
											id="serviceNumber" class="width50 validate[required]"
											value="${requestScope.serviceNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetCreationName">Asset
									Name<span class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="assetCreationName"
									value="${ASSET_SERVICE.assetCreation.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="assetCreationId"
									value="${ASSET_SERVICE.assetCreation.assetCreationId}" /> <span
									class="button"> <a style="cursor: pointer;" id="asset"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="serviceType">Service Type<span
									class="mandatory">*</span> </label> <select name="serviceType"
									id="serviceType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceType" items="${ASSET_SERVICE_TYPE}">
										<option value="${serviceType.lookupDetailId}">${serviceType.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceType"
									value="${ASSET_SERVICE.lookupDetailByServiceType.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_SERVICE_TYPE"
									class="btn ui-state-default ui-corner-all servicetype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="serviceLevel">Service Level<span
									class="mandatory">*</span> </label> <select name="serviceLevel"
									id="serviceLevel" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceLevel" items="${ASSET_SERVICE_LEVEL}">
										<option value="${serviceLevel.key}">${serviceLevel.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceLevel"
									value="${ASSET_SERVICE.serviceLevel}" />
							</div>
							<div>
								<label class="width30" for="serviceTerms">Service Terms<span
									class="mandatory">*</span> </label> <select name="serviceTerm"
									id="serviceTerm" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceTerm" items="${ASSET_SERVICE_TERM}">
										<option value="${serviceTerm.lookupDetailId}">${serviceTerm.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceTerm"
									value="${ASSET_SERVICE.lookupDetailByServiceTerms.lookupDetailId}" />
								<span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="ASSET_SERVICE_TERM"
									class="btn ui-state-default ui-corner-all serviceterm-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="serviceInCharge">Service
									Incharge<span class="mandatory">*</span>
								</label> <input type="text" name="serviceInCharge" id="serviceInCharge"
									class="width50 validate[required]"
									value="${ASSET_SERVICE.person.firstName} ${ASSET_SERVICE.person.lastName}"
									readonly="readonly" /> <input type="hidden" readonly="readonly"
									id="serviceInChargeId" value="${ASSET_SERVICE.person.personId}" />
								<span class="button"> <a style="cursor: pointer;"
									id="servicecharge"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Contract Detail<span class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Service Date</th>
									<th style="width: 5%">Estimated Completion Date</th>
									<th style="width: 3%">Priority</th>
									<th style="width: 5%">Next Service</th>
									<th style="width: 8%">Incharge</th>
									<th style="width: 10%">Description</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail"
									items="${ASSET_SERVICE.assetServiceDetails}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
										<td><c:set var="serviceDate"
												value="${detail.serviceDate}" /> <c:set var="extimatedDate"
												value="${detail.extimatedDate}" /> <c:set
												var="nextServiceDate" value="${detail.nextServiceDate}" /> <%
											String serviceDate = pageContext.getAttribute("serviceDate").toString();
											serviceDate = DateFormat.convertDateToString(serviceDate);
											
											String extimatedDate = pageContext.getAttribute("extimatedDate").toString();
											extimatedDate = DateFormat.convertDateToString(extimatedDate);
											
											String nextServiceDate = pageContext.getAttribute("nextServiceDate").toString();
											if(null!=nextServiceDate && !nextServiceDate.equals(""))
												nextServiceDate = DateFormat.convertDateToString(nextServiceDate);
											else
												nextServiceDate = "";
										%> <input type="text" id="serviceDate_${status.index+1}"
											class="width96 serviceDate" value="<%=serviceDate%>"
											readonly="readonly" />
										</td>
										<td><input type="text" class="width96 extimatedDate"
											readonly="readonly" id="extimatedDate_${status.index+1}"
											value="<%=extimatedDate%>" />
										</td>
										<td><select name="priority"
											id="priority_${status.index+1}" class="width96 priority">
												<option value="">Select</option>
												<c:forEach var="priority" items="${ASSET_PRIORITY_TYPE}">
													<option value="${priority.key}">${priority.value}</option>
												</c:forEach>
										</select> <input type="hidden" id="tempPriority_${status.index+1}"
											value="${detail.priority}" /></td>
										<td><input type="text" class="width96 nextServiceDate"
											readonly="readonly" id="nextServiceDate_${status.index+1}"
											value="<%=nextServiceDate%>" />
										</td>
										<td><input type="text" class="width70 serviceManager"
											readonly="readonly" id="serviceManager_${status.index+1}"
											value="${detail.person.firstName} ${detail.person.lastName}" />
											<input type="hidden" id="serviceManagerId_${status.index+1}"
											value="${detail.person.personId}" /> <span class="button">
												<a style="cursor: pointer;" id="incharge_${status.index+1}"
												class="btn ui-state-default ui-corner-all common-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
										<td><input type="text" class="width96 linedescription"
											id="description_${status.index+1}"
											value="${detail.description}" /> <input type="hidden"
											id="assetServiceDetailId_${status.index+1}"
											value="${detail.assetServiceDetailId}" />
										</td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a></td>
									</tr>
								</c:forEach>
								<c:forEach var="i"
									begin="${fn:length(ASSET_SERVICE.assetServiceDetails)+1}"
									end="${fn:length(ASSET_SERVICE.assetServiceDetails)+2}"
									step="1" varStatus="status1">
									<tr class="rowid" id="fieldrow_${i}">
										<td style="display: none;" id="lineId_${i}">${i}</td>
										<td><input type="text" id="serviceDate_${i}"
											class="width96 serviceDate" readonly="readonly" />
										</td>
										<td><input type="text" class="width96 extimatedDate"
											readonly="readonly" id="extimatedDate_${i}" />
										</td>
										<td><select name="priority" id="priority_${i}"
											class="width96 priority">
												<option value="">Select</option>
												<c:forEach var="priority" items="${ASSET_PRIORITY_TYPE}">
													<option value="${priority.key}">${priority.value}</option>
												</c:forEach>
										</select></td>
										<td><input type="text" readonly="readonly"
											class="width96 nextServiceDate" id="nextServiceDate_${i}" />
										</td>
										<td><input type="text" class="width70 serviceManager"
											readonly="readonly" id="serviceManager_${i}" /> <input
											type="hidden" id="serviceManagerId_${i}" /> <span
											class="button"> <a style="cursor: pointer;"
												id="incharge_${i}"
												class="btn ui-state-default ui-corner-all common-popup width100">
													<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
										<td><input type="text" class="width96 linedescription"
											id="description_${i}" /> <input type="hidden"
											id="assetServiceDetailId_${i}" />
										</td>
										<td style="width: 0.01%;" class="opn_td" id="option_${i}">
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${i}" style="display: none; cursor: pointer;"
											title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${i}" style="display: none; cursor: pointer;"
											title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${i}" style="display: none; cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${i}" style="display: none;" title="Working">
												<span class="processing"></span> </a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="service_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="service_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>