<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fieldset>
	<legend>
		Workin Process Detail<span class="mandatory">*</span>
	</legend>
	<input type="hidden" id="mixCount" value="${fn:length(MATERIAL_IDLEMIX_DETAILS)}"/>
	<c:choose>
		<c:when
			test="${MATERIAL_IDLEMIX_DETAILS ne null && fn:length(MATERIAL_IDLEMIX_DETAILS)>0}">
			<div id="line-error" class="response-msg error ui-corner-all width90"
				style="display: none;"></div>
			<div id="hrm" class="hastable width100">
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th class="width10">Material</th>
							<th class="width10">Shelf</th>
							<th style="width: 8%;">Packaging</th> 
							<th class="width5">Quantity</th>
							<th class="width5">Base Qty</th>
							<th class="width10">Description</th>
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach var="DETAIL"
							items="${MATERIAL_IDLEMIX_DETAILS}"
							varStatus="status">
							<c:set var="backgrd" value="#fff"/>
							<c:if test="${DETAIL.insufficientBalance eq false}">
								<c:set var="backgrd" value="#F3F781"/></c:if>
							<tr class="rowid" id="fieldrow_${status.index+1}" style="background-color:${backgrd}">
								<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
								<td><input type="hidden" name="productId"
									id="productid_${status.index+1}"
									value="${DETAIL.product.productId}" /> <span class="width60 float-left"
									id="product_${status.index+1}">${DETAIL.product.productName}
										[${DETAIL.product.code}]</span>
									<span class="width10 float-left" id="unitCode_${status.index+1}"
											style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
								</td>
								<td><span class="float-left width60"
									id="storeNameWork_${status.index+1}">${DETAIL.storeName}</span> <input type="hidden"
									name="shelfId" id="shelfId_${status.index+1}" value="${DETAIL.shelfId}"/> <span
									class="button float-right"> <a style="cursor: pointer;"
										id="prodIDStr_${status.index+1}"
										class="btn ui-state-default ui-corner-all workinprocessdetail-store-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</td>
								<td>
									<select name="packageType" id="packageType_${status.index+1}" class="packageType">
										<option value="-1">Select</option>
										<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
											<c:choose>
												<c:when test="${packageType.productPackageId gt 0}">
													<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
													 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
													 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
													 	</c:forEach> 
													 </optgroup>
												</c:when>
												<c:otherwise>
													<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
												</c:otherwise>
											</c:choose> 
										</c:forEach>
									</select>
									<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
								</td>
								<td><input type="text"
									class="width96 packageUnit validate[optional,custom[number]]"
									id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
									<input type="hidden"
 									id="temppackageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
								</td> 
								<td><input type="hidden" name="issueQuantity"
									id="issueQuantity_${status.index+1}"
									value="${DETAIL.baseQuantity}"
									class="issueQuantity validate[optional,custom[number]] width80">
									<input type="hidden" id="typicalWastage_${status.index+1}"> 
									<input type="hidden" name="tempissueQuantity"
									id="tempissueQuantity_${status.index+1}"
									value="${DETAIL.baseQuantity}">
									<span id="baseUnitConversion_${status.index+1}" class="width10" 
										style="display: none;">${DETAIL.baseUnitName}</span>
									<span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
								</td>
								<td><input type="text" name="description"
									id="description_${status.index+1}" class="description  width80">
									<input type="hidden" name="workinProcessDetailId"
									id="workinProcessDetailId_${status.index+1}" /></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</c:when>
		<c:otherwise>
			<span style="color: #ca1e1e;">Material Idle Mix not found for this product.</span>
		</c:otherwise>
	</c:choose>
</fieldset>
<script type="text/javascript">
$(function(){
	$('.rowid').each(function(){
		var rowId = getRowId($(this).attr('id'));  
		$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
	});
});
</script>