<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var serviceDetails="";
var popval = "";
var accessCode = "";
$(function(){  

	 $('input,select').attr('disabled', true);

	$('.priority').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	}); 

	$('.servicetype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	


	$('.serviceterm-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	


	$('.contracttype-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
 		if(accessCode=="ASSET_SERVICE_TYPE"){
			$('#serviceType').html("");
			$('#serviceType').append("<option value=''>Select</option>");
			loadLookupList("serviceType"); 
		} else if(accessCode=="ASSET_SERVICE_TERM"){
			$('#serviceTerm').html("");
			$('#serviceTerm').append("<option value=''>Select</option>");
			loadLookupList("serviceTerm"); 
		}  else if(accessCode=="ASSET_CONTRACT_TYPE"){
			$('#contractType').html("");
			$('#contractType').append("<option value=''>Select</option>");
			loadLookupList("contractType"); 
		}  
	});
	

	$('.addrows').click(function(){ 
		  var i = Number(1); 
		  var id = Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id = id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/asset_service_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 }); 

	$('.delrow').live('click',function(){ 
		slidetab=$(this).parent().parent().get(0);  
      	$(slidetab).remove();  
      	var i=1;
   	 	$('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	 });

	  
	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	 $('#asset-common-popup').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('.common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		popval = $(this).attr('id');  
		var actionValue = "";
		if(popval == "asset"){
			actionValue = "show_common_asset_popup";
		}else{
			actionValue = "common_person_list";
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionValue+".action", 
		 	async: false,  
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
				$('.common-result').html(result);  
				return false;
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 top: 0,
		 modal: true 
	}); 

	if(Number($('#assetServiceId').val())>0){
		$('#serviceType').val($('#tempServiceType').val());
		$('#serviceLevel').val($('#tempServiceLevel').val());
		$('#serviceTerm').val($('#tempServiceTerm').val());
		$('#contractType').val($('#tempContractType').val()); 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			$('#priority_'+rowId).val($('#tempPriority_'+rowId).val()); 
		});
	}
	
});
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){  
	 var serviceDate=$('#serviceDate_'+rowId).val();  
	 var priority=$('#priority_'+rowId).val(); 
	 var nexttab=$('#fieldrow_'+rowId).next(); 
	if(serviceDate!=null && serviceDate!=""
			&& priority!=null && priority!=""  
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');  
	}
}

function personPopupResult(personid, personname, commonParam){
	var rowId = popval.split('_')[1]; 
	if(typeof(rowId)!="undefined"){  
		$('#serviceManagerId_'+rowId).val(personid);
		$('#serviceManager_'+rowId).val(personname);
	}else{
		$('#serviceInChargeId').val(personid);
		$('#serviceInCharge').val(personname);
	}
	
	$('#common-popup').dialog("close");   
}
function commonAssetPopup(assetId, assetName){
	$('#assetCreationId').val(assetId);
	$('#assetCreationName').val(assetName); 
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
				data : {
					accessCode : accessCode
				},
				async : false,
				dataType : "json",
				cache : false,
				success : function(response) {

					$(response.lookupDetails)
							.each(
									function(index) {
										$('#' + id)
												.append(
														'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																+ response.lookupDetails[index].displayName
																+ '</option>');
									});
				}
			});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>service
			contract
		</div> 
		<form name="asset_services" id="asset_services" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetServiceId" name="assetServiceId"
					value="${ASSET_SERVICE.assetServiceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset>
							<div>
								<label class="width30" for="contractNumber">Contract
									Number</label> <input type="text" id="contractNumber" class="width50"
									value="${ASSET_SERVICE.contractNumber}">
							</div>
							<div>
								<label class="width30" for="contractType">Contract Type<span
									class="mandatory">*</span>
								</label> <select name="contractType" id="contractType"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="contractType" items="${ASSET_CONTRACT_TYPE}">
										<option value="${contractType.lookupDetailId}">${contractType.displayName}</option>
									</c:forEach>
								</select>  <input
									type="hidden" id="tempContractType"
									value="${ASSET_SERVICE.lookupDetailByContractType.lookupDetailId}" />
							</div>
							<div>
								<label class="width30" for="provider">Provider<span
									class="mandatory">*</span>
								</label> <input type="text" id="provider" class="width50"
									value="${ASSET_SERVICE.provider}">
							</div>
							<div>
								<label class="width30" for="contractAmount">Contract
									Amount</label> <input type="text" id="contractAmount" class="width50"
									value="${ASSET_SERVICE.contractAmount}">
							</div>
							<div>
								<label class="width30" for="contactName">Contact Name</label> <input
									type="text" id="contactName" class="width50"
									value="${ASSET_SERVICE.contactPerson}">
							</div>
							<div>
								<label class="width30" for="contactNumber">Contact No</label> <input
									type="text" id="contactNumber" class="width50"
									value="${ASSET_SERVICE.contactNumber}">
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_SERVICE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 225px;">
							<div>
								<label class="width30" for="serviceNumber">Service
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_SERVICE.assetServiceId ne null && ASSET_SERVICE.assetServiceId ne ''}">
										<input type="text" readonly="readonly" name="serviceNumber"
											id="serviceNumber" class="width50 validate[required]"
											value="${ASSET_SERVICE.serviceNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="serviceNumber"
											id="serviceNumber" class="width50 validate[required]"
											value="${requestScope.serviceNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetCreationName">Asset
									Name<span class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" id="assetCreationName"
									value="${ASSET_SERVICE.assetCreation.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="assetCreationId"
									value="${ASSET_SERVICE.assetCreation.assetCreationId}" />  
							</div>
							<div>
								<label class="width30" for="serviceType">Service Type<span
									class="mandatory">*</span> </label> <select name="serviceType"
									id="serviceType" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceType" items="${ASSET_SERVICE_TYPE}">
										<option value="${serviceType.lookupDetailId}">${serviceType.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceType"
									value="${ASSET_SERVICE.lookupDetailByServiceType.lookupDetailId}" /> 
							</div>
							<div>
								<label class="width30" for="serviceLevel">Service Level<span
									class="mandatory">*</span> </label> <select name="serviceLevel"
									id="serviceLevel" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceLevel" items="${ASSET_SERVICE_LEVEL}">
										<option value="${serviceLevel.key}">${serviceLevel.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceLevel"
									value="${ASSET_SERVICE.serviceLevel}" />
							</div>
							<div>
								<label class="width30" for="serviceTerms">Service Terms<span
									class="mandatory">*</span> </label> <select name="serviceTerm"
									id="serviceTerm" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="serviceTerm" items="${ASSET_SERVICE_TERM}">
										<option value="${serviceTerm.lookupDetailId}">${serviceTerm.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempServiceTerm"
									value="${ASSET_SERVICE.lookupDetailByServiceTerms.lookupDetailId}" /> 
							</div>
							<div>
								<label class="width30" for="serviceInCharge">Service
									Incharge<span class="mandatory">*</span>
								</label> <input type="text" name="serviceInCharge" id="serviceInCharge"
									class="width50 validate[required]"
									value="${ASSET_SERVICE.person.firstName} ${ASSET_SERVICE.person.lastName}"
									readonly="readonly" /> <input type="hidden" readonly="readonly"
									id="serviceInChargeId" value="${ASSET_SERVICE.person.personId}" /> 
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content quotation_detail"
				style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Contract Detail</legend> 
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Service Date</th>
									<th style="width: 5%">Estimated Completion Date</th>
									<th style="width: 3%">Priority</th>
									<th style="width: 5%">Next Service</th>
									<th style="width: 8%">Incharge</th>
									<th style="width: 10%">Description</th> 
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="detail"
									items="${ASSET_SERVICE.assetServiceDetails}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
										<td><c:set var="serviceDate"
												value="${detail.serviceDate}" /> <c:set var="extimatedDate"
												value="${detail.extimatedDate}" /> <c:set
												var="nextServiceDate" value="${detail.nextServiceDate}" /> <%
											String serviceDate = pageContext.getAttribute("serviceDate").toString();
											serviceDate = DateFormat.convertDateToString(serviceDate);
											
											String extimatedDate = pageContext.getAttribute("extimatedDate").toString();
											extimatedDate = DateFormat.convertDateToString(extimatedDate);
											
											String nextServiceDate = pageContext.getAttribute("nextServiceDate").toString();
											if(null!=nextServiceDate && !nextServiceDate.equals(""))
												nextServiceDate = DateFormat.convertDateToString(nextServiceDate);
											else
												nextServiceDate = "";
										%> <input type="text" id="serviceDate_${status.index+1}"
											class="width96 serviceDate" value="<%=serviceDate%>"
											readonly="readonly" />
										</td>
										<td><input type="text" class="width96 extimatedDate"
											readonly="readonly" id="extimatedDate_${status.index+1}"
											value="<%=extimatedDate%>" />
										</td>
										<td><select name="priority"
											id="priority_${status.index+1}" class="width96 priority">
												<option value="">Select</option>
												<c:forEach var="priority" items="${ASSET_PRIORITY_TYPE}">
													<option value="${priority.key}">${priority.value}</option>
												</c:forEach>
										</select> <input type="hidden" id="tempPriority_${status.index+1}"
											value="${detail.priority}" /></td>
										<td><input type="text" class="width96 nextServiceDate"
											readonly="readonly" id="nextServiceDate_${status.index+1}"
											value="<%=nextServiceDate%>" />
										</td>
										<td><input type="text" class="width70 serviceManager"
											readonly="readonly" id="serviceManager_${status.index+1}"
											value="${detail.person.firstName} ${detail.person.lastName}" />
											<input type="hidden" id="serviceManagerId_${status.index+1}"
											value="${detail.person.personId}" /> </td>
										<td><input type="text" class="width96 linedescription"
											id="description_${status.index+1}"
											value="${detail.description}" /> <input type="hidden"
											id="assetServiceDetailId_${status.index+1}"
											value="${detail.assetServiceDetailId}" />
										</td> 
									</tr>
								</c:forEach> 
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div>