<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<script type="text/javascript">
$(function(){ 
	
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
	
	$("#tree").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	});  
});
</script>
<div id="main-content">
	<div id="fy_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s">
				</span>Combination Approval</div>	 
			<div class="portlet-content">   
			  	<div class="width100 float-left" id="hrm" style="height: 400px; overflow-y :auto;">   
					<div class="float-left  width50">  
						<div class="divpadding" style="display: none;">
							<label class="width30">Combination</label> <span
								class="width60 spanfont">${COMBINATION_VIEW.accountByCompanyAccountId.account}</span> <input
								type="hidden" name="combinationId"
								value="${COMBINATION_VIEW.combinationId}" id="combinationId" />
						</div> 
						<div id="main">
							<div id="sidetree">
								<div id="sidetreecontrol"></div> 
									<c:choose>
										<c:when test="${COMBINATION_TREE_VIEW ne null && COMBINATION_TREE_VIEW ne ''}">  
											<ul id="tree">  
												<li><a  style="font-weight:bold;" class="createcombi">Combination</a> 
												<c:forEach var="bean" items="${COMBINATION_TREE_VIEW}" varStatus="status">
													<li><a id="company_${status.index+1}_${status.index+1}" class="combination_ companys">
																<input type="hidden" id="companyId_${status.index+1}_${status.index+1}" value="${bean.combinationId}"/>
																${bean.accountByCompanyAccountId.account} [${bean.accountByCompanyAccountId.code}]</a> 
														
														   	<c:choose>
														   		<c:when test="${bean.costVos ne null && bean.costVos ne '' && fn:length(bean.costVos)>0}">
														   		<ul>
														   			<c:forEach var="costbean" items="${bean.costVos}" varStatus="cstatus">
																		<li>
																			<a class="combination_ costscenter" id="costcenter_${status.index+1}_${cstatus.index+1}">
																				<input type="hidden" id="costcenterId_${status.index+1}_${cstatus.index+1}" value="${costbean.combinationId}"/>
																				${costbean.accountByCostcenterAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}]
																			</a>
																				<c:choose>
														   							<c:when test="${costbean.naturalVos ne null && costbean.naturalVos ne '' && fn:length(costbean.naturalVos)>0}">
														   								<ul>
																							<c:forEach var="naturalbean" items="${costbean.naturalVos}" varStatus="nstatus">
																								<li>
																									<a  class="combination_ naturals" id="natural_${cstatus.index+1}_${nstatus.index+1}">
																										<input type="hidden" id="naturalId_${cstatus.index+1}_${nstatus.index+1}" value="${naturalbean.combinationId}"/>
																										${naturalbean.accountByNaturalAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}]</a>
																								<c:choose>
														   											<c:when test="${naturalbean.analysisVos ne null && naturalbean.analysisVos ne '' && fn:length(naturalbean.analysisVos)>0}">
														   												<ul>
																											<c:forEach var="analysisbean" items="${naturalbean.analysisVos}" varStatus="astatus">
																												<li>
																													<a  class="combination_ analysis" id="analysis_${nstatus.index+1}_${astatus.index+1}">
																														<input type="hidden" id="analysisId_${nstatus.index+1}_${astatus.index+1}" value="${analysisbean.combinationId}"/>
																														${analysisbean.accountByAnalysisAccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}]</a>
																													<c:choose>
														   																<c:when test="${analysisbean.buffer1Vos ne null && analysisbean.buffer1Vos ne '' && fn:length(analysisbean.buffer1Vos)>0}">
																															<ul>
																																<c:forEach var="buffer1bean" items="${analysisbean.buffer1Vos}" varStatus="b1status">
																																	<li>
																																		<a  class="combination_ buffer1s" id="buffer1_${astatus.index+1}_${b1status.index+1}">
																																			<input type="hidden" id="buffer1Id_${astatus.index+1}_${b1status.index+1}" value="${buffer1bean.combinationId}"/>
																																			${buffer1bean.accountByBuffer1AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}]</a>
																																		<c:choose>
														   																					<c:when test="${buffer1bean.buffer2Vos ne null && buffer1bean.buffer2Vos ne '' && fn:length(buffer1bean.buffer2Vos)>0}">
																																				<ul>
																																					<c:forEach var="buffer2bean" items="${buffer1bean.buffer2Vos}" varStatus="b2status">
																																						<li>
																																							<a class="combination_ buffer2s"  id="buffer2_${b1status.index+1}_${b2status.index+1}">
																																								<input type="hidden" id="buffer2Id_${b1status.index+1}_${b2status.index+1}" value="${buffer2bean.combinationId}"/>
																																								${buffer2bean.accountByBuffer2AccountId.account} [${bean.accountByCompanyAccountId.code}.${costbean.accountByCostcenterAccountId.code}.${naturalbean.accountByNaturalAccountId.code}.${analysisbean.accountByAnalysisAccountId.code}.${buffer1bean.accountByBuffer1AccountId.code}.${buffer2bean.accountByBuffer2AccountId.code}]</a></li>
																																					</c:forEach>
																																				</ul>
																																			</c:when>
																																		</c:choose>
																																	</li>
																																</c:forEach>
																															</ul>
																													</c:when>
																												</c:choose>	
																												</li>	
																											</c:forEach>
																										</ul>
														   											</c:when>
														   										</c:choose>	 
																								</li>
																							</c:forEach>
																						</ul>
														   							</c:when>
														   						</c:choose> 
																			</li>
																	</c:forEach> 
																	</ul>
														   		</c:when>
														   	</c:choose>  
													</li>  
												</c:forEach>  
												</li>
											</ul>  
										</c:when>
									</c:choose>
								</div> 
							</div> 
						</div>  
				</div>  
			</div>  
			<div class="clearfix"></div>   
  	</div> 
</div>
<div class="clearfix"></div>  