<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
	<fieldset> 	
		<legend>Payment Information<span class="mandatory">*</span></legend>
		 <table id="hastab" class="width100"> 
			<thead>
			   <tr>  
			   		<th style="width:1% ;display:none;">No.</th>  
			   	    <th style="width:1%">Choose</th> 
				    <th style="width:10%">Bank Account</th>   
				    <th style="width:2%">Amount</th>  
				    <th style="width:2%">Charges Type</th>  
				    <th style="width:2%; display: none;">Date</th>  
					<th style="width:0.01%;display:none;"><fmt:message key="accounts.common.label.options"/></th> 
			  </tr>
			</thead> 
			<tbody class="tab">  
				<c:choose>
					<c:when test="${BANK_DEPOSIT_DETAIL ne null && BANK_DEPOSIT_DETAIL ne ''}">
						<c:forEach var="PAYMENT_DETAIL" items="${BANK_DEPOSIT_DETAIL}" varStatus="status"> 
							<tr class="rowid" id="fieldrow_${status.index+1}"> 
								<td id="lineId_${status.index+1}" style="display:none;">${status.index+1}</td> 
								<td>
									<input type="checkbox" class="width98 paymentChecked" id="paymentChecked_${status.index+1}"/>
								</td> 
								<td>
									<input type="text" id="accountNumber_${status.index+1}" class="width80 accountNumber" name="accountNumber" value=""/> 
									<span class="button width20 float-right" style="position: relative!important;top:-20px!important; right:-20px!important;">
										<a style="cursor: pointer;" id="bankaccountid_${status.index+1}" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span> 
									<input type="hidden" readonly="readonly" name="accountId_${status.index+1}" id="accountId_${status.index+1}" value=""/>
									<input type="hidden" name="codeCombination_${status.index+1}" id="codeCombination_${status.index+1}" value="" >
								</td>
								<td>
									<input type="text" class="width98 validate[optional,custom[onlyFloat]] amount" id="amount_${status.index+1}" value="${PAYMENT_DETAIL.recordAmount}" style="text-align: right;"/>
								</td> 
								<td id="chargesType_${status.index+1}">
									${PAYMENT_DETAIL.chargesType}
								</td>
								<td style="display: none;">
									<c:choose>
										<c:when test="${PAYMENT_DETAIL.contractPayment.chequeDate ne null && PAYMENT_DETAIL.contractPayment.chequeDate ne ''}">
											<c:set var="payDate" value="${PAYMENT_DETAIL.contractPayment.chequeDate}"/>  
											<%String date1 = DateFormat.convertDateToString(pageContext.getAttribute("payDate").toString());%>
											<input type="text" name="depositDate_${status.index+1}" value="<%=date1%>" 
											id="depositDate_${status.index+1}" readonly="readonly" class="width60 validate[required]" TABINDEX=2> 
										</c:when>  
										<c:otherwise>  
											<input type="text" name="depositDate_${status.index+1}" id="depositDate_${status.index+1}" readonly="readonly" value="-NA-" class="width60 validate[required]" TABINDEX=2> 
										</c:otherwise>
									</c:choose> 
								</td>
								<td style="display:none;">
									<input type="hidden" id="bankDepositDetailId_${status.index+1}" value="${PAYMENT_DETAIL.bankDepositDetail.bankDepositDetailId}"/>
									<input type="hidden" id="recordId_${status.index+1}" value="${PAYMENT_DETAIL.recordId}"/>
									<input type="hidden" id="useCase_${status.index+1}" value="${PAYMENT_DETAIL.useCase}"/> 
									<c:set var="useCaseVar" value="${PAYMENT_DETAIL.useCase}"/>
								</td> 
								 <td style="width:0.01%; display:none;" class="opn_td" id="option_${status.index+1}">
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span>
									  </a>	
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
											<span class="ui-icon ui-icon-wrench"></span>
									  </a> 
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="display:none; cursor:pointer;" title="Delete Record">
											<span class="ui-icon ui-icon-circle-close"></span>
									  </a>
									  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
											<span class="processing"></span>
									  </a>
								</td>   
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr>
							<td>No Records</td>
						</tr>
					</c:otherwise>
				</c:choose>
 			</tbody>
		</table>
	</fieldset>
</div>  
<c:choose>
	<c:when test="${RENTPDC_RETURN_DETAIL ne null && RENTPDC_RETURN_DETAIL ne '' && fn:length(RENTPDC_RETURN_DETAIL)>0}">
		<div id="hrm" class="hastable width100 float-left" style="margin-top: 10px;">
			<fieldset> 	
				<legend>Rent Returns Information<span class="mandatory">*</span></legend>
				 <table id="hastab" class="width100"> 
					<thead>
					   <tr>  
					   		<th style="width:1% ;">No.</th>   
					   		<th style="width:2%">Charges Type</th>   
					   		<th style="width:2%">Amount</th>   
							<th style="width:0.01%;display:none;"><fmt:message key="accounts.common.label.options"/></th> 
					  </tr>
					</thead> 
					<tbody class="tab">   
						<c:forEach var="RENTPDC_DETAIL" items="${RENTPDC_RETURN_DETAIL}" varStatus="status1"> 
							<tr id="rentrow_${status1.index+1}">
								<td>${status1.index+1}</td>
								<td id="rentCharges_${status1.index+1}">
									${RENTPDC_DETAIL.chargesType}
								</td>
								<td id="rentAmount_${status1.index+1}">
									${RENTPDC_DETAIL.recordAmount}
								</td>  
							</tr>
						</c:forEach> 
					</tbody>
				</table>
			</fieldset>					
		</div>
	</c:when>
</c:choose>
<c:choose>
	<c:when test="${useCaseVar ne null && useCaseVar ne '' && useCaseVar eq 'Release'}">
		<div class="width50 float-left" id="hrm">
			<div>
				<label class="width30">Deposits : </label> <span>${RECEIPT_PAYMENTS.depositAmount}(-)</span>
			</div>
			<div>
				<label class="width30">Release Charges : </label> <span>${RECEIPT_PAYMENTS.otherAmount}(+)</span>
			</div>
			<div>
				<c:if test="${RECEIPT_PAYMENTS.rentPDCAmount ne null && RECEIPT_PAYMENTS.rentPDCAmount ne '' && RECEIPT_PAYMENTS.rentPDCAmount ne 0}">
					<label class="width30">Rent&PDC : </label> <span>${RECEIPT_PAYMENTS.rentPDCAmount}(+)</span>
				</c:if>
			</div>
			<div>
			<c:if test="${RECEIPT_PAYMENTS.tenantAmount ne null && RECEIPT_PAYMENTS.tenantAmount ne '' && RECEIPT_PAYMENTS.tenantAmount ne 0}">
				<label class="width30">Tenant Penalty : </label> <span>${RECEIPT_PAYMENTS.tenantAmount}(+)</span>
			</c:if> 
			<c:if test="${RECEIPT_PAYMENTS.mangtAmount ne null && RECEIPT_PAYMENTS.mangtAmount ne '' && RECEIPT_PAYMENTS.mangtAmount ne 0}">
				<label class="width30">Management Penalty : </label> <span>${RECEIPT_PAYMENTS.mangtAmount}(-)</span>
			</c:if> 
			</div>
			<div> 
				<div> 
					<label class="width30" style="visibility: hidden;">Dashed Line</label>
					<span>-----------------</span>
				</div>
				<c:if test="${RECEIPT_PAYMENTS.paymentType eq false}">
					<label class="width30">Receive Amount : </label> <span>${RECEIPT_PAYMENTS.recordAmount}</span>
				</c:if>
				<c:if test="${RECEIPT_PAYMENTS.paymentType eq true}">
					<label class="width30">Refund Amount : </label> <span>${RECEIPT_PAYMENTS.recordAmount}</span>
				</c:if> 
			</div>
		</div>  
	</c:when>
</c:choose>
<script  type="text/javascript">
var actionname="";
$(function(){
$('.common-popup').click(function(){  
    $('.ui-dialog-titlebar').remove();  
    var id=getRowId($(this).attr('id'));  
    actionname="getbankaccount_details_multiple";  
	$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/"+actionname+".action", 
		 	async: false,  
		 	data:{id:id},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
});
</script> 