<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tr class="rowidcalc" id="fieldrowcalc_${requestScope.ROW_ID}">
	<td style="display: none;" id="lineIdcalc_${requestScope.ROW_ID}">${requestScope.ROW_ID}</td>
	<td><select name="pricingType"
		id="pricingType_${requestScope.ROW_ID}" class="pricingType width70">
			<option value="">Select</option>
			<c:forEach var="TYPE" items="${PRICING_TYPE}">
				<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;"
			id="PRODUCT_PRICING_LEVEL_${requestScope.ROW_ID}"
			class="btn ui-state-default ui-corner-all product-pricing-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
	</td>
	<td><select name="calculationType"
		id="calculationType_${requestScope.ROW_ID}"
		class="calculationType width98">
			<option value="">Select</option>
			<c:forEach var="CAL_TYPE" items="${CALCULATION_TYPE}">
				<option value="${CAL_TYPE.key}">${CAL_TYPE.value}</option>
			</c:forEach>
	</select>
	</td>
	<td><select name="calculationSubType"
		id="calculationSubType_${requestScope.ROW_ID}"
		class="calculationSubType width98">
			<option value="">Select</option>
			<c:forEach var="SUBTYPE" items="${CALCULATION_SUB_TYPE}">
				<option value="${SUBTYPE.key}">${SUBTYPE.value}</option>
			</c:forEach>
	</select>
	</td>
	<td><input type="text" id="priceValue_${requestScope.ROW_ID}"
		name="priceValue" style="text-align: right;"
		class="priceValue width98 validate[optional, custom[number]]" />
	</td>
	<td id="calculatedPrice_${requestScope.ROW_ID}" style="text-align: right;"></td>
	<td><input type="hidden" name="storeId"
		id="storeid_${requestScope.ROW_ID}" /> <span
		id="store_${requestScope.ROW_ID}"></span> <span
		class="button float-right"> <a style="cursor: pointer;"
			id="storeID_${requestScope.ROW_ID}"
			class="btn ui-state-default ui-corner-all show-store-list width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span></td>
	<td><span class="button float-right"> <a
			style="cursor: pointer;" id="customerID_${requestScope.ROW_ID}"
			class="btn ui-state-default ui-corner-all show-customer-list width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span>
		<div id="customerInfo_${requestScope.ROW_ID}" class="customerInfos"></div>
	</td>
	<td style="display: none;"><input type="checkbox"
		id="defaultPrice_${requestScope.ROW_ID}" name="defaultPrice"
		class="defaultPrice width98" />
	</td>
	<td><input type="text" name="linedescription"
		id="linedescription_${requestScope.ROW_ID}"
		value="${bean.description}" />
	</td>
	<td style="width: 0.01%;" class="opn_td"
		id="optionrc_${requestScope.ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_pc"
		id="DeleteCImage_${requestScope.ROW_ID}" style="cursor: pointer;display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <input type="hidden" id="productPricingCalcId_${requestScope.ROW_ID}" />
	</td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#productCalcValidation").validationEngine('attach');
	});
</script>