<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tr class="rowid" id="fieldrow_${rowId}"> 
	<td id="lineId_${rowId}" style="display:none;">${rowId}</td>
	<td>
		 ${GOODS_RECEIVE_BYID.receiveNumber}
	</td>  
	<td>
		 ${GOODS_RECEIVE_BYID.referenceNo}
	</td>  
	<td>
		<c:set var="receivedate" value="${GOODS_RECEIVE_BYID.receiveDate}"/>
		<%=DateFormat.convertDateToString(pageContext.getAttribute("receivedate").toString())%> 
	</td> 
	<td style="display: none;">
		 ${GOODS_RECEIVE_BYID.totalReceiveQty}
	</td>
	<td style="text-align: right;" id="totalamt_${rowId}">
		<c:set var="totalamt" value="${GOODS_RECEIVE_BYID.totalReceiveAmount}"/>
		<%= AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
	</td>
	<td>
		${GOODS_RECEIVE_BYID.description}
	</td>   
	<td style="display:none;">
		<input type="hidden" id="receiveId_${rowId}" value="${GOODS_RECEIVE_BYID.receiveId}"/> 
	</td> 
	 <td class="opn_td" id="option_${rowId}"> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv" id="EditImage_${rowId}" style="cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
	</td>  
</tr>