<tr class="rowidc" id="fieldrowc_${rowId}">
	<td id="lineIdc_${rowId}" style="display:none;">${rowId}</td>
	<td>
		<select class="width90 chargesType" id="chargesType_${rowId}"style="border:0px;">
			<option value="">Select</option>
			<option value="A">Amount</option>
			<option value="I">Incremental</option>
		</select>
	</td> 
	<td>
		<input type="text" class="width80 chargesName" id="chargesName_${rowId}" style="border:0px;"/>
	</td> 
	<td>
		<input type="text" class="width70 chargesValue" id="chargesValue_${rowId}" style="border:0px;text-align: right;"/>
	</td>  
	<td>
		<span class="otherChargesCode width50" id="otherChargesCode_${rowId}"></span>
		<span class="button float-right width20" id="otherchargescode_${rowId}"  style="position: relative; top:6px;">
			<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
				<span class="ui-icon ui-icon-newwin"> 
				</span> 
			</a>
		</span> 
		<input type="hidden" name="otherChargesCombinationId" id="otherChargesCombinationId_${rowId}"
			value=""/> 
	</td> 
	<td style="display:none;">
		<input type="hidden" id="loanLineChargesId_${rowId}" style="border:0px;"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="optionc_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDataC" id="AddImageC_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDataC" id="EditImageC_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowC" id="DeleteImageC_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImageC_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>