<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 
$(function(){ 
	  $('input, select').attr('disabled', true); 
	  {
		  
		  $.ajax({
				type: "POST", 
				url: "<%=request.getContextPath()%>/show_coupon_entry.action", 
		     	async: false, 
		     	data:{couponId: Number($('#couponId').val())},
				dataType: "json",
				cache: false,
				success: function(response){ 
					 $('#couponName').val(response.couponVO.couponName);
					 $('#couponType').val(response.couponVO.couponType);
					 $('#startDigit').val(response.couponVO.startDigit);
					 $('#endDigit').val(response.couponVO.endDigit);
					 $('#status').val(response.couponVO.status);
					 $('#fromDate').val(response.couponVO.validFrom);
					 $('#toDate').val(response.couponVO.validTo);
					 $('#description').val(response.couponVO.description);
				} 		
			});  
	  }
		
	 
	});

 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>coupon
		</div>
		<div class="portlet-content"> 
			<div class="clearfix"></div>
			<form id="coupon_validation" style="position: relative;">
			<input type="hidden" id="couponId" value="${requestScope.couponId}"/>
				<div class="width100" id="hrm">
					<div class="edit-result">
						<div class="width48 float-right">
							<fieldset style="min-height: 145px;">
								<div>
									<label for="fromDate">From Date<span
										style="color: red;">*</span> </label> <input type="text"
										name="fromDate" id="fromDate" tabindex="7"
										class="width50 validate[required]" readonly="readonly" />
								</div>
								<div>
									<label for="toDate">To Date<span
										style="color: red;">*</span> </label> <input type="text"
										name="toDate" id="toDate" tabindex="8" class="width50 validate[required]"
										readonly="readonly" />
								</div>
								<div>
									<label for="description">Description</label>
									<textarea class="width50" tabindex="9" id="description"></textarea>
								</div>
							</fieldset>
						</div>
						<div class="width50 float-left">
							<fieldset>
								<div>
									<label for="couponName">Coupon Name<span
										style="color: red;">*</span> </label> <input type="text"
										name="couponName" id="couponName" tabindex="1"
										class="width50 validate[required]" />
								</div>
								<div>
									<label for="couponType">Coupon Type<span
										style="color: red;">*</span> </label> <select name="couponType"
										id="couponType" class="width51 validate[required]"
										tabindex="2">
										<option value="">Select</option>
										<c:forEach var="couponType" items="${COUPON_TYPES}">
											<option value="${couponType.key}">${couponType.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label for="startDigit">Start Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="startDigit" id="startDigit" tabindex="3"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="endDigit">End Digit<span
										style="color: red;">*</span> </label> <input type="text"
										name="endDigit" id="endDigit" tabindex="4"
										class="width50 validate[required,custom[onlyNumber]]" />
								</div>
								<div>
									<label for="status">Status<span style="color: red;">*</span>
									</label> <select name="status" id="status"
										class="width51 validate[required]" tabindex="6">
										<option value="true" selected="selected">Active</option>
										<option value="false">Inactive</option>
									</select>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="clearfix"></div> 
				</div>
			</form>  
		</div>
	</div>
</div>
<div class="clearfix"></div>