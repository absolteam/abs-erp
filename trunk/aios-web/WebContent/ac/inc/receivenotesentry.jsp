<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>   
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
var slidetab="";
var addedReceipt = 0;
var proArray=new Array();
var checkQuantity=true;
$(function(){
	$("#receivenotesValidation").validationEngine({
		 validationEventTriggers:"keyup blur",  //will validate on keyup and blur  
	     success :  false,
	     failure : function() { callFailFunction()}
	});
	
	if(!($('#receiveId')).val()>0){
		calculateTotalReceive();
		$('#receiveDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
		//manupulateLastRow(); 
	}else{
		$('.common-popup').remove();
	} 
 
	$('.unitrate').live('change',function(){ 
		var rowId=getRowId($(this).attr('id'));
		var unitrate=Number($(this).val()); 
		var quantity=Number($('#receiveQty_'+rowId).val());
		var total=Number(0);
		if(quantity!=null && unitrate>0 && quantity>0){
			total=unitrate*quantity;
			$('#total_'+rowId).text(total); 
			calculateTotalReceive();
		}
	});

	$('.quantity').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		var quantity=Number($(this).val());
		var total=Number(0);
		var unitrate=Number($('#unitrateQty_'+rowId).val());
		checkQuantity=validateQuantity(Number(quantity), Number($('#purchaseqty_'+rowId).text().trim()),Number($('#returnqty_'+rowId).text().trim()));
		if(checkQuantity==true){
			if(unitrate!=null && quantity>0 && unitrate>0){
				total=unitrate*quantity;
				$('#total_'+rowId).text(total); 
			}
			calculateTotalReceive();
		}else{
			$('#receiveQty_'+rowId).val(""); 
			return false;
		}
	});

	var validateQuantity=function(quantity, purchaseQty, returnQty){
		checkQuantity=false;
		var totalQuantity = Number(quantity + returnQty);
		if(totalQuantity<=purchaseQty){
			checkQuantity=true;
		}else{
			checkQuantity=false;
		}
		return checkQuantity; 
	};
	
	$('#receiveDate').datepick({
		minDate:0, showTrigger: '#calImg'}); 
	
	$('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		var actionname=$(this).attr('id'); 
		if(actionname=="purchase"){
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/purchase_orderreturns.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result); 
				},
				error:function(result){  
					 $('.common-result').html(result); 
				}
			});  
			return false;   
		}else{
			getPurchaseDetails($('#purchaseId').val());
		}
		return false;  
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
	
	$('.receive_save').click(function(){  
		if($("#receivenotesValidation").validationEngine({returnIsValid:true})){
			var receiveDate=$('#receiveDate').val();
			var receiveId=Number($('#receiveId').val());
			var receiveNumber=Number($('#receiveNumber').val());  
			var purchaseId=$('#purchaseId').val();
			var description=$('#description').val();
			var referenceNo=$('#referenceNo').val();
			receiveDetails=getReceiveDetails();   
			if(receiveDetails!=null && receiveDetails!=""){ 
			 $.ajax({
					type:"POST",
					url:'<%=request.getContextPath()%>/receive_save.action',
				 	async: false,
				 	data:{receiveDate:receiveDate, receiveId:receiveId, receiveNumber:receiveNumber, referenceNo:referenceNo,
					 		 purchaseId:purchaseId,receiveDetails:receiveDetails, description:description},
				    dataType: "html",
				    cache: false,
					success:function(result){
						 $(".tempresult").html(result);
						 var message=$('#returnMsg').html(); 
						 if(message.trim()=="Success"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/receive_notes_retrieve.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
										$('#common-popup').remove(); 
										$('.otherx-popup').dialog('destroy');		
										$('.otherx-popup').remove();  
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html("Receipt Note Created.").slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#receive-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					}
			});
		   }else{
			   $('#receive-error').hide().html("Please enter, product information.").slideDown(1000);
				 return false;
			}
		} 
		else{
		 return false;
	 	}
	 });

	 var getReceiveDetails=function(){
		 var receiveLines=new Array(); 
		 var productLines=new Array(); 
		 var unitLines=new Array(); 
		 var lineId=new Array(); 
		 var aisleIds=new Array();
		 var returnqty=new Array();
		 var lineDescriptions=new Array(); 
		 var productId="";
		 var receiveQty=""; 
		 var unitrateQty="";
		 var returnQuantity="";
		 var aisleId="";
		 receiveDetails="";
		 var receiveLineId=""; 
		 $('.rowid').each(function(){
		 	 var rowId=getRowId($(this).attr('id')); 
			 productId=$('#productid_'+rowId).val();
			 receiveQty=$('#receiveQty_'+rowId).val(); 
			 unitrateQty=$('#unitrateQty_'+rowId).val();
			 receiveLineId=Number($('#receiveLineId_'+rowId).val());
			 returnQuantity=$('#returnqty_'+rowId).text().trim();
			 var linedescription=$('#linedescription_'+rowId).val();
			 aisleId=$('#storeDetail_'+rowId).val();
			 if(typeof productId!='undefined' &&
					typeof receiveQty!='undefined' && productId!='' && receiveQty!='' && receiveQty>0
					&& typeof unitrateQty!='undefined' && unitrateQty!='' && aisleId!=null && aisleId!=''){ 
				receiveLines.push(receiveQty); 
				productLines.push(productId);
				unitLines.push(unitrateQty);
				aisleIds.push(aisleId);
				if(linedescription!=null && linedescription!="")
					lineDescriptions.push(linedescription);
				else
					lineDescriptions.push("##"); 
				if(typeof receiveLineId!='undefined' && receiveLineId>0)
					lineId.push(receiveLineId);
				else
					lineId.push(0);
				if(returnQuantity==null || returnQuantity=="" || returnQuantity<=0)
					returnqty.push(0);
				else
					returnqty.push(returnQuantity);
			 } 
		  }); 
		  for(var j=0;j<receiveLines.length;j++){ 
			receiveDetails+=productLines[j]+"__"+receiveLines[j]+"__"+unitLines[j]+"__"+lineId[j]+"__"+returnqty[j]+"__"+aisleIds[j]+"__"+lineDescriptions[j];
			if(j==receiveLines.length-1){   
			} 
			else{
				receiveDetails+="#@";
			}
		 } 
		 return receiveDetails;
	 };
	
	$('.receive_discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/receive_notes_discard.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $('#common-popup').dialog('destroy');		
					 $('#common-popup').remove(); 
					 $('.otherx-popup').dialog('destroy');		
					 $('.otherx-popup').remove();  
					$("#main-wrapper").html(result);  
				}
			});
	 });
	
	$('.addrows').click(function(){ 
		  var i=Number(0); 
		  var id=Number(0);
		  $('.rowid').each(function(){
			  var temparray=$(this).attr('id');
			  var idarray=temparray.split('_');
			  var rowid=Number(idarray[1]);  
			  id=Number($('#lineId_'+rowid).text());  
			  id+=1;
		  }); 
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/receive_notes_addrow.action", 
			 	async: false,
			 	data:{id: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"});
					 i=0;
					 $('.rowid').each(function(){
							i=i+1;
					}); 
				}
			});
	 });

	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
       	 $(slidetab).remove();  
       	 var i=1;
    	 $('.rowid').each(function(){   
    		 var rowId=getRowId($(this).attr('id')); 
    		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
 		 }); 
    	 calculateTotalReceive();  
	 });
	
	  
	 $(".autocomplete").each(function(){
		 slidetab=$(this).attr('id'); 
		 var idarray =slidetab.split('_');
	  	 var rowid=Number(idarray[1]); 
	  	 $('#productName_'+rowid+' option').each(function(){  
	 	  	 var tempProdName=$('#tempProdName_'+rowid).val();
	 	  	 var tempProductText=$(this).text(); 
	 	  	 var tempProductId=$('#productid_'+rowid).val(); 
			 if(tempProductText==tempProdName){  
				$('#productName_'+rowid+' option[text='+tempProdName+']').attr("selected","selected");
				$('#productCode_'+rowid+' option[value='+tempProductId+']').attr("selected","selected");
			 }
	  	}); 
	 }); 
	 
	 $(".autocomplete").combobox({ 
       selected: function(event, ui){
	      	var productVal=$(this).val();
	      	proArray=productVal.split('@@');
	        var unitName=proArray[1];
	        slidetab=$(this).attr('id'); 
	  		var idarray =slidetab.split('_');
	  		var rowid=Number(idarray[1]);  
	  		$('#uom_'+rowid).text(unitName);
	  		var receiveQty=$('#receiveQty_'+rowid).val();
	  		 var nexttab=slidetab=$(this).parent().parent().get(0);  
		  	 nexttab=$(nexttab).next();
	  		triggerAddRow(receiveQty,unitName,nexttab,rowid);  
	  		//change product code
	  		var productName=$(this).val(); 
	  		$('#productCode_'+rowid+' option').each(function(){
		 		 var productId=$(this).val(); 
		 		 var splitval=productName.split("@@");
		 		 productName=splitval[0]; 
		 		 if(productId==productName){
		 			productName=$(this).text();   
		 			$('.productcodeclass_'+rowid).combobox('autocomplete', productName);
		 			$('#productid_'+rowid).val(productId);
		 		 }
		 	 });
       }
    }); 
	 
	 $('.autocompletecode').combobox({ 
		 selected: function(event, ui){ 
			 var elVal=$(this).val(); 
			 slidetab=$(this).attr('id'); 
		  	 var idarray =slidetab.split('_');
		  	 var rowid=Number(idarray[1]);  
		  	 var nexttab=slidetab=$(this).parent().parent().get(0);  
		  	 nexttab=$(nexttab).next();
		  	 $('#productName_'+rowid+' option').each(function(){
		 		 var productId=$(this).val(); 
		 		 var splitval=productId.split("@@");
		 		 productId=splitval[0]; 
		 		 if(elVal==productId){
		 			var productName=$(this).text();  
		 			$('.productnameclass_'+rowid).combobox('autocomplete', productName); 
		 			$('#uom_'+rowid).text(splitval[1]);
		 			$('#productid_'+rowid).val(productId);
		 		 }
		 	 });
		  	var receiveQty=$('#receiveQty_'+rowid).val();
		  	var unitVal=$('#uom_'+rowid).text();
		  	triggerAddRow(receiveQty,unitVal,nexttab,rowid); 
         } 
	 });
	 
	 $('.receiveQty').live('blur',function(){
		slidetab=$(this).parent().parent().get(0);   
		var nexttab=$(slidetab).next(); 
		var receiveQty=$(this).val(); 
		var tempvar=$(this).attr("id");  
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);   
		var unitVal=$('#uom_'+rowid).text(); 
		triggerAddRow(receiveQty,unitVal,nexttab,rowid);  
	 });	 
});
 
function triggerAddRow(receiveQty,unitVal,nexttab,rowid){  
	if(receiveQty!=null && receiveQty!="" && unitVal!="" && $(nexttab).hasClass('lastrow')){
		$('#DeleteImage_'+rowid).show(); 
		$('.addrows').trigger('click');
	}  
	return false;
}

function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid'); 
	$('tr:last').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function getPurchaseDetails(purchaseId){   
	$('.ui-dialog-titlebar').remove(); 
    $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_returngoods.action", 
	 	async: false, 
	 	data:{purchaseId:purchaseId},
	    dataType: "html",
	    cache: false,
		success:function(result){
			 $('.otherx-popup').dialog('destroy');		
			 $('.otherx-popup').remove();   
			 $('.common-result').html(result);  
		} 
	}); 
}
function calculateTotalReceive(){
	var totalReceive=0;
	$('.rowid').each(function(){
		var rowId=getRowId($(this).attr('id'));
		var totalamount=Number($('#total_'+rowId).text().trim());
		totalReceive=Number(totalReceive+totalamount);
	}); 
	$('#totalReceiveAmount').text(Number(totalReceive).toFixed(2));
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Receive Information</div>	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>General Information</div>	
		  <form name="receivenotesform" id="receivenotesValidation"> 
			<div class="portlet-content">  
				<div id="receive-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="receiveId" name="receiveId" value="${RECEIVE_INFO.receiveId}"/>
				<div class="width100 float-left" id="hrm"> 
				<div class="float-right width48">
					<fieldset> 
						<div>
							<label class="width30" for="supplierName">Supplier Number<span class="mandatory">*</span></label> 
							<input type="text" readonly="readonly" name="supplierNumber" id="supplierNumber"
								 class="width50 validate[required]" value="${RECEIVE_INFO.supplier.suppierNumber}"/>
						</div> 
						<div>
							<label class="width30" for="siteName">Supplier Name<span class="mandatory">*</span></label> 
							<input type="text" readonly="readonly" name="supplierName" id="supplierName" 
								class="width50 validate[required]" value="${RECEIVE_INFO.supplier.person.firstName} ${RECEIVE_INFO.supplier.person.lastName}${RECEIVE_INFO.supplier.cmpDeptLocation.company.companyName}"/>
						</div> 
						<div>
							<label class="width30" for="termId">Payment Term</label> 
							<input type="text" readonly="readonly" name="termName" id="termName" 
								class="width50" value="${RECEIVE_INFO.creditTerm.name}"/>
						</div> 
						<div>
							<label class="width30 tooltip">Description</label>
							<textarea rows="2" id="description" class="width60 float-left">${PURCHASE_INFO.description}</textarea> 
						</div>
					</fieldset>
				</div>
				<div class="float-left width50">
					<fieldset style="height: 127px;"> 
						<div>
							<label class="width30" for="receiveNumber"> 
							Receive number</label>
							<c:choose>
								 <c:when test="${RECEIVE_INFO.receiveId ne null && RECEIVE_INFO.receiveId ne ''}">	
								 		<input name="receiveNumber" type="text" readonly="readonly" id="receiveNumber" value="${RECEIVE_INFO.receiveNumber}" class="width40 validate[required]"> 
								 </c:when>
								 <c:otherwise>
								 		<input name="receiveNumber" type="text" readonly="readonly" id="receiveNumber" value="${receiveNumber}" class="width40 validate[required]"> 
								 </c:otherwise>
							</c:choose>
						</div> 
						<div>
							<label class="width30" for="receiveDate">Reference NO.</label>
							<input type="text" name="referenceNo" class="width40" id="referenceNo"/>
						</div>
						<div>
							<label class="width30"  for="receiveDate">Receive Date<span style="color: red;">*</span></label>
							 <c:set var="receiveDate" value="${RECEIVE_INFO.receiveDate}"/>
							 <c:choose>
							 	<c:when test="${RECEIVE_INFO.receiveDate ne null && RECEIVE_INFO.receiveDate ne ''}">
							 		<%String date= DateFormat.convertDateToString(pageContext.getAttribute("receiveDate").toString());
									pageContext.setAttribute("date", date);%>
							 		
									<input type="text" name="receiveDate" readonly="readonly" class="width40 validate[required]" value="${date}" style="margin-bottom:5px;" id="receiveDate"/> 
								</c:when>
								<c:otherwise>
									<input type="text" name="receiveDate" readonly="readonly" class="width40 validate[required]" style="margin-bottom:5px;" id="receiveDate"/>
								</c:otherwise>
							</c:choose>
						</div>	
						<div>
							<label class="width30" for="purchaseId">Purchase Number<span style="color: red;">*</span></label> 
							<input type="hidden" id="purchaseId" name="purchaseId" value="${RECEIVE_INFO.purchase.purchaseId}"/>
							<input type="text" readonly="readonly" name="purchaseNumber" id="purchaseNumber" class="width40 validate[required]"
								 value="${RECEIVE_INFO.purchase.purchaseNumber}"/>
							<c:choose>
								<c:when test="${RECEIVE_INFO.receiveId eq null || RECEIVE_INFO.receiveId ne '' || RECEIVE_INFO.receiveId eq 0}">
									<span class="button">
										<a style="cursor: pointer;" id="purchase" class="btn ui-state-default ui-corner-all common-popup width100"> 
											<span class="ui-icon ui-icon-newwin"> 
											</span> 
										</a>
									</span>  
								</c:when>
							</c:choose> 
						</div>  
						<div>
							<label class="width30" for="returnDetail">Return Detail</label> 
							<input type="text" readonly="readonly" name="returnDetail" id="returnDetail" class="width40"/>
							<span class="button goodsreturns" style="display:none;">
								<a style="cursor: pointer;" id="returns" class="btn ui-state-default ui-corner-all common-popup width100"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>  
						</div>
					</fieldset>
				</div>
			 </div>
			<div class="clearfix"></div>   
			<div id="hrm" class="portlet-content width100" style="margin-top:10px;"> 
				<fieldset id="hrm">
					<legend>Product Information</legend> 
		 			<div class="portlet-content"> 
		 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
		 			<div id="warning_message" class="response-msg notice ui-corner-all width90" style="display:none;"></div>
						<div id="hrm" class="hastable width100"  > 
							 <input type="hidden" name="childCount" id="childCount" value="${fn:length(RECEIVE_LINE_INFO)}"/>  
							 <table id="hastab" class="width100"> 
								<thead>
								   <tr>  
								   		<th style="width:3%">Product Code</th> 
									    <th style="width:3%">Product Name</th> 
									    <th style="width:2%">UOM</th>
									    <th style="width:2%">Purchase Qty</th>  
									    <th style="width:2%">Return Qty</th>  
									    <th style="width:2%">Receive Qty</th>   
									    <th style="width:3%">Unit Rate</th>  
									    <th style="width:3%">Total</th>  
									    <th style="width:5%">Store</th> 
									    <th style="width:5%">Description</th> 
									    <th style="width:1%">Option</th> 
								  </tr>
								</thead> 
								<tbody class="tab"> 
								<c:choose>
									<c:when test="${requestScope.RECEIVE_LINE_INFO ne null && requestScope.RECEIVE_LINE_INFO ne '' && fn:length(RECEIVE_LINE_INFO)>0}">
										<c:forEach var="bean" items="${RECEIVE_LINE_INFO}" varStatus ="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
												<td id="productcode_${status.index+1}">  
													${bean.product.code}
												</td>
												<td id="productname_${status.index+1}"> 
													${bean.product.productName}
												</td> 
												<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
												<td id="receiveqty_${status.index+1}" >
													<input type="text" class="width90 quantity validate[optional,custom[onlyFloat]]" style="border:0px;" value="${bean.receiveQty}" name="receiveQty" id="receiveQty_${status.index+1}"/>
												</td>
												<td id="unitrate_${status.index+1}" >
													<input type="text" class="width90 unitrate validate[optional,custom[onlyFloat]]" style="border:0px;" value="${bean.unitRate}" name="unitrate" id="unitrateQty_${status.index+1}"/>
												</td>  
												<td id="total_${status.index+1}" >
													 <c:out value="${bean.receiveQty*bean.unitRate}"/>
												</td>  
												<td style="display:none">
													<input type="hidden" name="receivelineid" value="${bean.receiveDetailId}" id="receivelineid_${status.index+1}"/>
													<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
												</td>  
												<td></td>  
												<td style="width:0.01%;" class="opn_td" id="option_${status.index+1}">
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
							 								<span class="ui-icon ui-icon-plus"></span>
													  </a>	
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
															<span class="ui-icon ui-icon-wrench"></span>
													  </a> 
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
															<span class="ui-icon ui-icon-circle-close"></span>
													  </a>
													  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
															<span class="processing"></span>
													  </a>
												</td>   
											</tr>
										</c:forEach>
									</c:when> 
									<c:otherwise>
										<c:forEach var="j" begin="1" end="2" step="1">
											<tr class="rowid" id="fieldrow_${j}" style="height:23px;">
												<c:forEach var="i" begin="1" end="11" step="1">
													<td id="${i}"></td>
												</c:forEach> 
											</tr>
										</c:forEach> 
									</c:otherwise>
								</c:choose>  
						 </tbody>
					</table>
				</div> 
			</div>   
		</fieldset>
		</div>
	</div>  
	<div class="clearfix"></div>
	<div class="width30 float-right" style="font-weight: bold;">
		<span>Total: </span>
		<span id="totalReceiveAmount"></span>
	</div>
	<div class="clearfix"></div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right receive_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
		<div class="portlet-header ui-widget-header float-right receive_save" id="${showPage}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	 </div> 
	<div style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div>
	</div> 
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="common-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div>  
  </form>
  </div> 
</div>