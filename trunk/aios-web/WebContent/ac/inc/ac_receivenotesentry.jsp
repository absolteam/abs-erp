<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script src="fileupload/FileUploader.js"></script>
	
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var slidetab ="";
var purchaseDetails ="";
var purchsaeDetailJS = [];
$(function(){ 
	//Special Div Show/Hide
	var grnReferenceAccess='${sessionScope.show_grn_delivery_more_reference}';
	if(grnReferenceAccess=='show')
		$('#extraReferenceDiv').show();  
	 
	 
	$jquery("#receiveNoteValidation").validationEngine('attach'); 


	  $('#supplier-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
	   
	$('.supplierreceive-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_supplier_popup.action", 
		 	async: false,  
		 	data:{pageInfo : "receive"},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#storeflag').val(false);  
				$('.common-result').html(result);  
				$('#common-popup').dialog('open');
				$($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
	
	$('.store-popuprcv').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		var rowId = Number($(this).attr('id'));
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_store_batchreceive_session.action", 
		 	async: false,  
		 	data:{rowId : rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.store-resultrcv').html(result);  
			},
			error:function(result){  
				 $('.store-resultrcv').html(result); 
			}
		});  
		return false;
	});


	$('.store-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		var rowId = Number($(this).attr('id'));
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_store_session.action", 
		 	async: false,  
		 	data:{rowId : rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.store-result').html(result);  
			},
			error:function(result){  
				 $('.store-result').html(result); 
			}
		});  
		return false;
	});

	


	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.grnpackageType').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		$('#receiveQty_'+rowId).val('');
		var packageUnit = Number($('#grnPackageUnit_'+rowId).val());
		var packageType = Number($('#grnpackageType_'+rowId).val());   
		var tempgrnpackageType = $('#tempgrnpackageType_'+rowId).val();
		if(packageUnit > 0 && packageType > 0){ 
			if(tempgrnpackageType > 0){
				getProductBaseUnit(rowId);
			} else{
 				$('#grnPackageUnit_'+rowId).val($('#receiveMaxQty_'+rowId).val());
			}
		} else{
			$('#grnPackageUnit_'+rowId).val($('#receiveMaxQty_'+rowId).val());
		}
		return false;
	});
	
	$('.grnPackageUnit').live('change',function(){
		var rowId=getRowId($(this).attr('id')); 
		var packageUnit = Number($(this).val()); 
		var packageType = Number($('#grnpackageType_'+rowId).val());   
		$('.returnerr').remove();
		var tempgrnpackageType = $('#tempgrnpackageType_'+rowId).val();
		var receiveMaxQty = $('#receiveMaxQty_'+rowId).val();
		if(packageUnit > 0 && packageType > 0){  
			getProductBaseUnit(rowId);
		} else if(packageUnit > 0 && packageType == -1){ 
 			if(tempgrnpackageType == -1){
  				if(packageUnit > receiveMaxQty){
					$('#grnPackageUnit_'+rowId).val(receiveMaxQty);
					$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' style='font-size: 9px; color: red;' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");   
 					$('.returnerr').delay(1500).slideUp();
				} else{
					$('#receiveQty_'+rowId).val(packageUnit);
					$('.receiveQtybatch').trigger('change');
				}
			} else{ 
				if(packageUnit > receiveMaxQty){
					$('#grnPackageUnit_'+rowId).val(receiveMaxQty);
					$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' style='font-size: 9px; color: red;' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");   
 					$('.returnerr').delay(1500).slideUp();
				}
			}
		}
		return false;
	}); 
	
	$('.editDatarc').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
      	 var purchaseId = $('#purchaseId_'+rowId).val();  
      	 var continuousPurchase =  $('#continuousPurchase_'+rowId).attr('checked'); 
       	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_purchasedetail.action", 
		 	async: false,  
		 	data:{purchaseId: purchaseId, rowId: rowId, continuousPurchase: continuousPurchase},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click');   
				 $('.storeDetail').each(function(){ 
					 var rowid = getRowId($(this).attr('id'));
					 $(this).val($('#tempstoreDetailid_'+rowid).val());
				 }); 
			} 
		});  
 		return false;
	}); 
	 

	$('.returnQty').live('change',function(){
		$('.returnerr').remove();
		$('.receiveerr').remove();
		var rowId = getRowId($(this).attr('id')); 
		var currentReceiveQty = Number($('#receiveQty_'+rowId).val()); 
		var returnQty =  Number($('#returnQty_'+rowId).val()); 
		var returnedQty =  Number($('#returnedQty_'+rowId).text()); 
		var poQty = Number($('#purchaseqty_'+rowId).text());
		var receivedQty = Number($('#receivedqty_'+rowId).text());
		var totalAvailed =Number(currentReceiveQty+receivedQty+returnQty+returnedQty); 
		if(totalAvailed > poQty) {
			$(this).parent().append("<span class='returnerr' id=returnerror_"+rowId+">Return Qty should be lt or eq receive qty.</span>"); 
			$('#returnQty_'+rowId).val('');
			$('#receiveQty_'+rowId).val($('#receiveHiddenQty_'+rowId).val());
		}  
		return false;
	});
	
	$('.returnQtyrtn').live('change',function(){
		$('.returnerr').remove(); 
		var rowId = getRowId($(this).attr('id')); 
		var currentReceiveQty = Number($.trim($('#receiveQtyrcv_'+rowId).val())); 
		var returnQty =  Number($.trim($('#returnQtyrtn_'+rowId).val())); 
		var returnedQty =  Number($.trim($('#returnedqtyrcv_'+rowId).text())); 
		var poQty = Number($.trim($('#purchaseqtyrcv_'+rowId).text()));
		var receivedQty = Number($.trim($('#receivedqtyrcv_'+rowId).text()));
		var totalAvailed =Number(currentReceiveQty+receivedQty+returnQty+returnedQty); 
		if(totalAvailed > poQty) {
			$(this).parent().append("<span class='returnerr' id=returnerror_"+rowId+">Return Qty should be lt or eq receive qty.</span>"); 
			$('#returnQtyrtn_'+rowId).val(''); 
		}else{
			$('#returnQtyrcv_'+rowId).val(returnQty); 
		}
		return false;
	});

	$('.unitRate').live('change',function(){ 
		var rowId = getRowId($(this).attr('id')); 
		var currentReceiveQty = Number($('#receiveQty_'+rowId).val()); 
		var unitRate = Number($jquery('#unitRate_'+rowId).val()); 
		var total = Number(currentReceiveQty * unitRate);
		$jquery('#totalAmount_'+rowId).val(total);
		$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
		return false;
	});
	
	$('.unitRatercv').live('change',function(){ 
		var rowId = getRowId($(this).attr('id')); 
		var currentReceiveQty = Number($('#receiveQtyrcv_'+rowId).val().trim()); 
		var unitRate = Number($jquery('#unitRatercv_'+rowId).val().trim()); 
		var total = Number(currentReceiveQty * unitRate);
		$jquery('#totalAmountrcv_'+rowId).val(total);
		$('#totalrcv_'+rowId).text($('#totalAmountrcv_'+rowId).val());
		return false;
	});

	$('.receiveQty').live('change',function(){  
		var responseMessage = "false"; 
		$('.returnerr').remove(); 
		var rowId = getRowId($(this).attr('id')); 
		var receiveQty = Number($.trim($('#receiveQty_'+rowId).val()));
		var poQty = Number($.trim($('#purchaseqty_'+rowId).text()));  
		$('#returnQty1rcv_'+rowId).text('');  
		$('#returnQtyrcv_'+rowId).val('');  
   		if(poQty >= receiveQty){ 
 			var receivedQty = Number($.trim($('#receivedqty_'+rowId).text()));
 			var unitrate = Number($.trim($jquery('#unitRate_'+rowId).val())); 
 			$jquery('#totalAmount_'+rowId).val(Number(receiveQty * unitrate));
 			$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
  			var returnedQty =  Number($.trim($('#returnedqty_'+rowId).text()));  
  			var totalAvailedQty = Number(receiveQty+receivedQty+returnedQty);   
  			var purchaseId = Number($.trim($('#purchasePOId_'+rowId).val())); 
  			$.ajax({
  				type:"POST",
  				url:"<%=request.getContextPath()%>/check_receivepurchasebatch.action", 
  			 	async: false,  
  			 	data:{purchaseId: purchaseId},
  			    dataType: "json",
  			    cache: false,
  				success:function(response){ 
  					responseMessage = response.sqlReturnMessage;
  				} 
  			});   
  			if(responseMessage == "true"){
  				var cnfrm = confirm("Changing quantity will delete batch information. \n\t\t Do you want to continue?");
  				if(!cnfrm){
  					$('#receiveQty_'+rowId).val($('#receiveHiddenQty_'+rowId).val());
  					return false;
  				} 
  				else{
  					$.ajax({
  		  				type:"POST",
  		  				url:"<%=request.getContextPath()%>/delete_receivepurchasebatch_session.action", 
  		  			 	async: false,  
  		  			 	data:{purchaseId: purchaseId},
  		  			    dataType: "json",
  		  			    cache: false,
  		  				success:function(response){ 
  		  					responseMessage = response.sqlReturnMessage;
  		  				} 
  		  			});  
  				}
  			} else{
  				responseMessage =  "true";
  			}
  			if(responseMessage == "true"){
  				if(totalAvailedQty>poQty) {  
  	 				$(this).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
  	 				$('#receiveQty_'+rowId).val($('#receiveHiddenQty_'+rowId).val());
  	 				var receiveHidden = $('#receiveHiddenQty_'+rowId).val();
   	 				$jquery('#totalAmount_'+rowId).val(Number(receiveHidden * unitrate));
  	 	 			$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
  	 				$('#returnQty_'+rowId).val('');
  	 	 			$('#returnQty1_'+rowId).text('');
  	 			} else{ 
  	  				var continuousPO = $('#continuousPurchase').attr('checked'); 
  	 				if(continuousPO ==  false){
  	 					var rtrnQty = Number(poQty) - Number(totalAvailedQty);
  	 	 				$('#returnQty1_'+rowId).text(rtrnQty); 
  	 	 	 			$('#returnQty_'+rowId).val($.trim($('#returnQty1_'+rowId).text()));
  	 				} 
  	 			}
  			} 
 		}else{
 			$(this).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
 			$('#receiveQty_'+rowId).val('');
 			$('#returnQty_'+rowId).val('');
 			$('#returnQty1_'+rowId).text('');
 		} 
		return false;
	});
	
	$('.receiveQtybatch').live('change',function(){  
		$('.returnerr').remove(); 
		var rowId = getRowId($(this).attr('id')); 
		var receiveQty = Number($.trim($('#receiveQty_'+rowId).val()));
		var poQty = Number($.trim($('#purchaseqty_'+rowId).text()));  
		$('#returnQty1rcv_'+rowId).text('');  
		$('#returnQtyrcv_'+rowId).val('');   
       	if(poQty >= receiveQty){
			var receivedQty = Number($.trim($('#receivedqty_'+rowId).text()));
			var unitrate = Number($.trim($jquery('#unitRate_'+rowId).val())); 
			$jquery('#totalAmount_'+rowId).val(Number(receiveQty * unitrate));
			$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
			var returnedQty =  Number($.trim($('#returnedqty_'+rowId).text()));  
			var totalAvailedQty = Number(receiveQty+receivedQty+returnedQty);   
			if(totalAvailedQty > poQty) {  
				$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");   
				$('#receiveQty_'+rowId).val($('#receiveHiddenQty_'+rowId).val()); 
				var receiveHidden = $('#receiveHiddenQty_'+rowId).val();
				$jquery('#totalAmount_'+rowId).val(Number(receiveHidden * unitrate));
				$('#total_'+rowId).text($('#totalAmount_'+rowId).val());
				$('#returnQty_'+rowId).val('');
				$('#returnQty1_'+rowId).text('');
			} else{ 
				getPendingBatch(rowId);
			}
 		}else{
 			$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
 			$('#receiveQty_'+rowId).val('');
 			$('#returnQty_'+rowId).val('');
 			$('#returnQty1_'+rowId).text('');
 		} 
		return false;
	});
	
	
	$('.receiveQtyrcv').live('change',function(){ 
		var responseMessage = "false"; 
		$('.returnerr').remove(); 
		var rowId = getRowId($(this).attr('id')); 
		var receiveQty = Number($.trim($('#receiveQtyrcv_'+rowId).val()));
		var poQty = Number($.trim($('#purchaseqtyrcv_'+rowId).text()));   
		$('#returnQtyrcv_'+rowId).val('');
		$('#returnQtyrtn_'+rowId).val('');
		$('.batch_returns').hide(); 
   		if(poQty >= receiveQty){ 
 			var receivedQty = Number($.trim($('#receivedqtyrcv_'+rowId).text()));
 			var unitrate = Number($.trim($jquery('#unitRatercv_'+rowId).val())); 
 			$jquery('#totalAmountrcv_'+rowId).val(Number(receiveQty * unitrate));
 			$('#totalrcv_'+rowId).text($('#totalAmountrcv_'+rowId).val());
 			var returnedQty =  Number($.trim($('#returnedqtyrcv_'+rowId).text()));  
   			var totalAvailedQty = Number(receiveQty+receivedQty+returnedQty);    
   			var purchaseId = Number($.trim($('#purchasePOId_'+rowId).val())); 
   			if(totalAvailedQty>poQty) {  
 				$(this).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
 				$('#receiveQtyrcv_'+rowId).val($('#receiveHiddenQtyrcv_'+rowId).val());
 				$('#receiveqtyrtn_'+rowId).val($('#receiveHiddenQtyrcv_'+rowId).val());
 				var receiveHidden = $('#receiveHiddenQtyrcv_'+rowId).val();
 				$jquery('#totalAmountrcv_'+rowId).val(Number(receiveHidden * unitrate));
 	 			$('#totalrcv_'+rowId).text($('#totalAmountrcv_'+rowId).val());
  			} else{
 				$.ajax({
 	  				type:"POST",
 	  				url:"<%=request.getContextPath()%>/check_receivepurchasebatch.action", 
 	  			 	async: false,  
 	  			 	data:{purchaseId: purchaseId},
 	  			    dataType: "json",
 	  			    cache: false,
 	  				success:function(response){ 
 	  					responseMessage = response.sqlReturnMessage;
 	  				} 
 	  			});  
 	   			if(responseMessage == "true"){
 	   				var cnfrm = confirm("Changing quantity will delete batch information. \n\t\t Do you want to continue?");
 	  				if(!cnfrm){
 	  					$('#receiveQtyrcv_'+rowId).val($('#receiveHiddenQtyrcv_'+rowId).val());
 	  					return false;
 	  				} 
 	  				else{
 	  					$.ajax({
 	  		  				type:"POST",
 	  		  				url:"<%=request.getContextPath()%>/delete_receivepurchasebatch_session.action", 
 	  		  			 	async: false,  
 	  		  			 	data:{purchaseId: purchaseId},
 	  		  			    dataType: "json",
 	  		  			    cache: false,
 	  		  				success:function(response){ 
 	  		  					responseMessage = response.sqlReturnMessage;
 	  		  				} 
 	  		  			});  
 	  				}
 	   			}  
 			} 
 		}else{
 			$(this).parent().append("<span class='returnerr' id=receiveerrorrcv_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
 			$('#receiveQtyrcv_'+rowId).val(''); 
 		} 
		return false;
	}); 

	$('.delrowrc').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 var deleteId = getRowId($(slidetab).attr('id'));
      	 var deletePurchaseId = $('#purchaseId_'+deleteId).val(); 
      	 $('.activePONumber').each(function(){   
      		var purchaseId = getRowId($(this).attr('id')); 
      		 if(deletePurchaseId == purchaseId){
				$(this).attr('checked',false);
				 return false;
			 }
      	 });
      	 $(slidetab).remove();  
      	 var i=1;
   	 	 $('.rowid').each(function(){   
   			 var rowId=getRowId($(this).attr('id')); 
   		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
   	 	if($('.tab tr').size()==0)
			$('.purchaseline-info').hide();
		return false;
	 }); 

	 $('.delrow_po').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deletePurchaseId = $('#purchasePOId_'+idval).val();
 		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidpo').each(function(){   
   			 var rowId=getRowId($(this).attr('id')); 
   		 	 $('#lineIdpo_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-1 tr').size()==0){ 
			 $('.activePONumber').each(function(){   
	      		var purchaseId = getRowId($(this).attr('id'));  
	      		 if(deletePurchaseId == purchaseId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId=getRowId($(this).attr('id')); 
			   		 	 var pagePurchaseId = $('#purchaseId_'+rowId).val();
			   		 	 if(pagePurchaseId == purchaseId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.purchaseline-info').hide();
			 $('#store-popup').dialog('destroy');		
				$('#store-popup').remove();  
				$('#store-popuprcv').dialog('destroy');		
				$('#store-popuprcv').remove();  
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 } 
		 return false;
	 });
	 
	 $('.delrow_batchpo').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
  		 var rowId1 = getRowId($(this).attr('id'));  
		 var purchaseDetailId = Number($.trim($('#purchaseLineId_'+rowId1).val())); 
		 var delflag = false;
		 $('.rowidpo').each(function(){   
   			var rowId=getRowId($(this).attr('id')); 
   		 	var purchaseDetailIdtmp = Number($.trim($('#purchaseLineId_'+rowId).val()));
			if(purchaseDetailId == purchaseDetailIdtmp && rowId1 != rowId){
				delflag = true; 
				var grnpackageTypeDel = $('#grnpackageType_'+rowId1).val();
				var grnpackageTypeOd = $('#grnpackageType_'+rowId).val();
				if(grnpackageTypeDel == grnpackageTypeOd){ 
					var grnPackageUnitDel = Number($('#grnPackageUnit_'+rowId1).val());
					var grnPackageUnitOd = Number($('#grnPackageUnit_'+rowId).val());
					$('#grnPackageUnit_'+rowId).val(Number(grnPackageUnitDel + grnPackageUnitOd));
 					getProductBaseUnitSpl(rowId);
 				}else{
					
				}
				return false;
			}
		 }); 
		 if(delflag == true){
			 $(slidetab).remove();  
			 var i=0;
			 $('.rowidpo').each(function(){   
	   			 var rowId=getRowId($(this).attr('id')); 
	   		 	 $('#lineIdpo_'+rowId).html(i);
				 i=i+1;   
			 }); 
		 } 
		 return false;
	 });
	 
	 $('.delrow_porcv').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deletePurchaseId = $('#purchasePOIdrcv_'+idval).val();
 		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidporcv').each(function(){   
   			 var rowId=getRowId($(this).attr('id')); 
   		 	 $('#lineIdporcv_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-2 tr').size()==0){ 
			 $('.activePONumber').each(function(){   
	      		var purchaseId = getRowId($(this).attr('id'));  
	      		 if(deletePurchaseId == purchaseId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId=getRowId($(this).attr('id')); 
			   		 	 var pagePurchaseId = $('#purchaseId_'+rowId).val();
			   		 	 if(pagePurchaseId == purchaseId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.purchaseline-info').hide();
			 $('#store-popup').dialog('destroy');		
				$('#store-popup').remove();  
				$('#store-popuprcv').dialog('destroy');		
				$('#store-popuprcv').remove();  
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 } 
		 return false;
	 });

	 $('.closeReceiveDom').live('click',function(){
		 $('#store-popup').dialog('destroy');		
			$('#store-popup').remove();  
			$('#store-popuprcv').dialog('destroy');		
			$('#store-popuprcv').remove();  
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
	 });
	 
	 $('.showReturnsTable').live('click',function(){
		 $('.rowidporcv').each(function(){
			 var rowid = getRowId($(this).attr('id'));   
			 $('#receiveqtyrtn_'+rowid).text($('#receiveQtyrcv_'+rowid).val());
		 });
		 $('.batch_returns').show();
		 return false;
 	 }); 
	 
	 $('.moreoption_batch').live('click',function(){  
		var rowId = $('#receiveLineId').val();
		var purchaseId = Number($('#purchasePOId').val());
		var continuousPurchase = false;
  		purchsaeDetailJS = getReceiveProductDetails();  
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receivedetail_moreoption.action", 
		 	async: false,  
		 	data:{continuousPurchase: continuousPurchase, rowId: rowId, purchaseDetails: JSON.stringify(purchsaeDetailJS), purchaseId: purchaseId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$("#DOMWindow").html(result); 
			} 
		}); 
		return false; 
	 });  
	 
	 var getReceiveProductDetails = function(){
		purchsaeDetailJS = []; 
		$('.rowidpo').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = $('#productid_' + rowId).val();
			var poQty = Number($.trim($('#purchaseqty_' + rowId).text()));
			var receivedQty = Number($.trim($('#receivedqty_' + rowId).text()));
			var returnedQty = Number($.trim($('#returnedqty_' + rowId).text()));
			var returnQty = Number($('#returnQty_' + rowId).val());
			var receiveQty = Number($.trim($('#receiveQty_' + rowId).val()));
			var unitRate = Number($jquery('#unitRate_' + rowId).val());
			var shelfId = Number($(
					'#storeDetail_' + rowId).val());
			var purchaseDetailId = Number($(
					'#purchaseLineId_' + rowId).val());
			var purchaseId = Number($(
					'#purchasePOId_' + rowId).val());
			var receiveDetailId = Number($(
					'#receivelineid_' + rowId).val());
			var batchNumber = $('#batchNumber_' + rowId).val();
			var expiryDate = $('#expiryBatchDate_' + rowId).val();
 			var linesDescription = $('#linedescription_' + rowId)
					.val(); 
		 
			if(typeof productId != 'undefined' && productId > 0 && poQty > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"poQty" : poQty, 
					"receivedQty" : receivedQty,
					"returnedQty" : returnedQty,
					"returnQty" : returnQty,
					"receiveQty" : receiveQty,
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"receiveDetailId": receiveDetailId,
					"purchaseDetailId": purchaseDetailId,
					"purchaseId": purchaseId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	}; 
	
	$('.moreoption_continiousbatch').live('click',function(){  
		var rowId = $('#receiveLineId').val(); 
  		purchsaeDetailJS = getContinuousReceiveProductDetails();  
  		var purchaseId = Number($('#purchasePOId').val());
  		var continuousPurchase = true;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receivedetail_moreoption.action", 
		 	async: false,  
		 	data:{rowId: rowId, purchaseDetails: JSON.stringify(purchsaeDetailJS), continuousPurchase: continuousPurchase, purchaseId: purchaseId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				$("#DOMWindow").html(result); 
			} 
		}); 
		return false; 
	 }); 
	
	 var getContinuousReceiveProductDetails = function(){
		purchsaeDetailJS = []; 
		$('.rowidporcv').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = $('#productidrcv_' + rowId).val();
			var poQty = Number($.trim($('#purchaseqtyrcv_' + rowId).text()));
			var receivedQty = Number($.trim($('#receivedqtyrcv_' + rowId).text()));
			var returnedQty = Number($.trim($('#returnedqtyrcv_' + rowId).text()));
			var receiveQty = Number($.trim($('#receiveQtyrcv_' + rowId).val()));
			var unitRate = Number($jquery('#unitRatercv_' + rowId).val());
			var returnQty = Number($('#returnQtyrcv_' + rowId).val());
			var shelfId = Number($(
					'#storeDetailrcv_' + rowId).val());
			var purchaseDetailId = Number($(
					'#purchaseLineIdrcv_' + rowId).val());
			var purchaseId = Number($(
					'#purchasePOIdrcv_' + rowId).val());
			var receiveDetailId = Number($(
					'#receivelineidrcv_' + rowId).val());
			var batchNumber = $('#batchNumberrcv_' + rowId).val();
			var expiryDate = $('#expiryBatchDatercv_' + rowId).val(); 
 			var linesDescription = $('#linedescriptionrcv_' + rowId)
					.val(); 
		 
			if(typeof productId != 'undefined' && productId > 0 && poQty > 0){
				var jsonData = [];
				jsonData.push({
					"productId" : productId,
					"poQty" : poQty, 
					"receivedQty" : receivedQty,
					"returnedQty" : returnedQty,
					"receiveQty" : receiveQty,
					"returnQty" : returnQty,
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"receiveDetailId": receiveDetailId,
					"purchaseDetailId": purchaseDetailId,
					"purchaseId": purchaseId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	}; 
	
	var getPurchaseDetailsNw = function(){ 
		purchsaeDetailJS = []; 
		$('.rowidpo').each(function(){ 
			var rowid = getRowId($(this).attr('id'));   
			var productId = Number($('#productid_'+rowid).val()); 
			var shelfId = Number($('#storeDetail_'+rowid).val()); 
			var receiveQty =  Number($('#receiveQty_'+rowid).val()); 
			var returnQty = Number($('#returnQty_'+rowid).val()); 
			var linesDescription = $('#linedescription_'+rowid).val(); 
			var purchaseDetailId = $('#purchaseLineId_'+rowid).val();
			var unitRate = Number($jquery('#unitRate_'+rowid).val());
			var rackName = $('#rackname_'+rowid).text();
			var batchNumber = $('#batchNumber_' + rowid).val();
			var expiryDate = $('#expiryBatchDate_' + rowid).val(); 
			var receiveDetailId = Number($('#receivelineid_' + rowid).val()); 
			var packageDetailId = Number($('#grnpackageType_' + rowid).val()); 
			var packageUnit = Number($('#grnPackageUnit_' + rowid).val()); 
			if(typeof shelfId != 'undefined' && shelfId > 0){
				var jsonData = [];
				jsonData.push({ 
					"productId" : productId,
					"receiveQty" : receiveQty,
					"packageUnit": packageUnit,
					"unitRate" : unitRate, 
					"shelfId" : shelfId, 
					"returnQty" : returnQty,
					"batchNumber" : batchNumber, 
					"productExpiry" : expiryDate, 
					"linesDescription" : linesDescription, 
					"rackName": rackName,
					"receiveDetailId": receiveDetailId,
					"packageDetailId": packageDetailId,
					"purchaseDetailId": purchaseDetailId
				});   
				purchsaeDetailJS.push({
					"purchsaeDetailJS" : jsonData
				});
			} 
		});  
		return purchsaeDetailJS;
	 };
	
	$('.sessionReceiveBatchSave').live('click',function(){ 
		purchsaeDetailJS = getPurchaseDetailsNw();
		var rowId = Number($('#receiveLineId').val());
		var purchaseId = Number($('#purchasePOId').val());
		var batchProduct = true; 
		if (purchsaeDetailJS != null && purchsaeDetailJS != '') {
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_purchase_session.action", 
			 	async: false,  
			 	data:{batchProduct: batchProduct, purchaseDetails: JSON.stringify(purchsaeDetailJS), continuousPurchase: continuousPurchase,
			 			purchaseId: purchaseId},
				    dataType: "json",
			    cache: false,
				success:function(response){   
					$('#totalpoamt_'+rowId).text(response.receiveAmount);
					$('#continuousPurchase_'+rowId).attr('checked',continuousPurchase);
					$('#store-popup').dialog('destroy');		
  					$('#store-popup').remove();  
  					$('#store-popuprcv').dialog('destroy');		
  					$('#store-popuprcv').remove();  
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
				} 
			});  
			return false;
		} else{
			 $('#warning_message-popup').hide().html("Please select store to proceed").slideDown(1000);
			 $('#warning_message-popup').delay(2000).slideUp(1000);
		}
		return false;
	 });

	 $('.sessionReceiveSave').live('click',function(){ 
		 var rowId = Number($('#receiveLineId').val());
		 var continuousPurchase = $('#continuousPurchase').attr('checked');
		 var batchProduct = false;
		 purchsaeDetailJS = [];
		 if(continuousPurchase == false){
			 purchsaeDetailJS = getPurchaseDetailsNw();
		 }else{
			 purchsaeDetailJS = getContinuousReceiveProductDetails();  
		 }  
		 if (purchsaeDetailJS != null && purchsaeDetailJS != '') {
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/update_purchase_session.action", 
				 	async: false,  
				 	data:{batchProduct: batchProduct, purchaseDetails: JSON.stringify(purchsaeDetailJS), continuousPurchase: continuousPurchase},
				    dataType: "json",
				    cache: false,
					success:function(response){   
						$('#totalpoamt_'+rowId).text(response.receiveAmount);
						$('#continuousPurchase_'+rowId).attr('checked',continuousPurchase);
						$('#store-popup').dialog('destroy');		
	  					$('#store-popup').remove();  
	  					$('#store-popuprcv').dialog('destroy');		
	  					$('#store-popuprcv').remove();  
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
					} 
				});  
				return false;
		 } else{
			 $('#warning_message-popup').hide().html("Please select store to proceed").slideDown(1000);
			 $('#warning_message-popup').delay(2000).slideUp(1000);
		 }
		return false;
	 }); 

	 $('.receive_save').click(function(){
		 if($jquery("#receiveNoteValidation").validationEngine('validate')){
			 if($('.tab tr').size() > 0) {
				 var purchaseArray = new Array();
				 $('.activePONumber').each(function(){
					 if($(this).attr('checked')){ 
						 purchaseArray.push(getRowId($(this).attr('id')));
					 }
				 });
				 purchaseDetails = "";
				 for(var i=0; i<purchaseArray.length;i++){
					 purchaseDetails+=purchaseArray[i]+"#@";
				 } 
				 var receiveDate = $('#receiveDate').val();
				 var referenceNo =$('#referenceNo').val();
				 var deliveryNo =$('#deliveryNo').val();
				 var supplierId =$('#supplierId').val();
				 var description =$('#description').val();
				 var combinationId = $('#supplierCombinationId').val();
				 var supplierName = $('#supplierName').val();
				 var reference1 =$('#reference1').val();
				 var reference2 =$('#reference2').val();
				 var receiveId = Number($('#receiveId').val());   
				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_receive_note.action", 
				 	async: false,  
				 	data:{purchaseDetail: purchaseDetails, receiveDate: receiveDate, referenceNo: referenceNo,deliveryNo:deliveryNo,
					 	 supplierId: supplierId, description: description, combinationId:combinationId, supplierName: supplierName,
					 	reference1:reference1,reference2:reference2, receiveId: receiveId},
				    dataType: "json",
				    cache: false,
					success:function(response){  
 						 if(response.sqlReturnMessage=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/receive_notes_retrieve.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
					  					$('#common-popup').remove();  
					  					$('#store-popup').dialog('destroy');		
					  					$('#store-popup').remove();  
					  					$('#store-popuprcv').dialog('destroy');		
					  					$('#store-popuprcv').remove();  
					  					$('#DOMWindow').remove();
										$('#DOMWindowOverlay').remove();
										$("#main-wrapper").html(result); 
									}
							 });
							 if(receiveId > 0){
								 $('#success_message').hide().html("Record updated.").slideDown(1000); 
								 $('#success_message').delay(2000).slideUp(); 
							 }else{
								 $('#success_message').hide().html("Record created.").slideDown(1000); 
								 $('#success_message').delay(2000).slideUp();
							 }
							 
						 }
						 else{
							 $('#receive-error').hide().html(response.sqlReturnMessage).slideDown(1000);
							 $('#receive-error').delay(2000).slideUp();
							 return false;
						 } 
					} 
				});
		     } else {
		    	 $('#receive-error').hide().html("Please enter receive details.").slideDown(1000);
		    	 $('#receive-error').delay(2000).slideUp();
				 return false;
			  }
		 }else
			 return false;
	 });

	 $('.receive_discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/receive_notes_discard.action", 
		 	async: false, 
		    dataType: "html",
		    cache: false,
			success:function(result){
				 $('#common-popup').dialog('destroy');		
				 $('#common-popup').remove();  
				 $('#store-popup').dialog('destroy');		
				 $('#store-popup').remove();  
				 $('#store-popuprcv').dialog('destroy');		
				 $('#store-popuprcv').remove();  
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
				 $("#main-wrapper").html(result);  
			}
		});
	 });
	 
	 $('.activePONumberEdit').click(function(){   
		validatePurchaseInfo($(this)); 
	});
	 
	 var receiveId1 = Number($('#receiveId').val());   
	 $('#receive_document_information').click(function(){
			if(receiveId1>0){
				AIOS_Uploader.openFileUploader("doc","receiveDocs","Receive",receiveId1,"ReceiveDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","receiveDocs","Receive","-1","ReceiveDocuments");
			}
		});
	 
	 if(receiveId1>0){
		populateUploadsPane("doc","receiveDocs","Receive",receiveId1);
		$('#receiveDate').datepick();
	 }else{
		$('#receiveDate').datepick({ 
		    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	 }
});
function showActivePurchaseOrder(supplierId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_supplier_activepurchase.action", 
	 	async: false,  
	 	data:{supplierId: supplierId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.purchase-info').html(result);   
			 if(typeof($('.activePONumber').attr('id'))=="undefined"){   
				$('.purchaseline-info').hide();
				$('.tab').html('');
				$('.purchase-info').find('div').css("color","#ca1e1e").html("No active purchase order found for this supplier.");
			 } 
		},
		error:function(result){   
		}
	});  
	return false;
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function callDefaultPOSelect(purchaseObj){
	if (typeof purchaseObj != 'undefined') {
		var id= Number(1); 
		var purchaseId = getRowId(purchaseObj); 
		$('#'+purchaseObj).attr('checked',true); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_receive_purchase_info.action", 
		 	async: false,  
		 	data:{purchaseId: purchaseId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.purchaseline-info').show();
				$('.tab').html(result);  
			} 
		});  
	}
	return false;
}
function validatePurchaseInfo(currentObj){ 
	var purchaseId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.purchaseline-info').show();
		} else {
			$('.purchaseline-info').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_receive_purchase_info.action",
						async : false,
						data : {
							purchaseId : purchaseId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
						}
					});
		} else {
			$('.rowid').each(function() {
				var rowId = getRowId($(this).attr('id'));
				var removePurchaseId = $('#purchaseId_' + rowId).val();
				if (removePurchaseId == purchaseId) {
					$('#fieldrow_' + rowId).remove();
					return false;
				}
			});
			if ($('.tab tr').size() == 0)
				$('.purchaseline-info').hide();
		}
		return false;
	}

function getProductBaseUnit(rowId){
	$('#grnbaseUnitConversion_'+rowId).text('');
	var packageQuantity = Number($('#grnPackageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#grnpackageType_'+rowId).val());  
	var basePrice = Number(0);
	$('.returnerr').remove();
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			var maxRecevie = Number($('#receiveMaxQty_'+rowId).val());
			if(maxRecevie < response.productPackagingDetailVO.conversionQuantity){
				$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' style='font-size:9px; color: red;' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");   
				$('#grnPackageUnit_'+rowId).val($('#receiveHiddenQty_'+rowId).val()); 
 			}else{
				$('#receiveQty_'+rowId).val(packageQuantity);  
 				$('#receiveMaxQty_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity); 
				$('#grnbaseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
				$('.receiveQtybatch').trigger('change');
			} 
			return false;
		} 
	});  
	return false;
}
function getProductBaseUnitSpl(rowId){
	$('#baseDisplayQty_'+rowId).text('');
	var packageQuantity = Number($('#grnPackageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#grnpackageType_'+rowId).val());  
	var basePrice = Number(0);
	if(productPackageDetailId > 0){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#receiveQty_'+rowId).val(packageQuantity);  
				$('#receiveHiddenQty_'+rowId).val(packageQuantity);  
				$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity); 
				$('#receiveMaxQty_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
	 			return false;
			} 
		});  
	} 
	return false;
}
function getProductConversionUnitSpl(rowId){
	
}

function getProductConversionUnit(rowId){
	var packageQuantity = Number($('#receiveQty_'+rowId).val());
	var productPackageDetailId = Number($('#grnpackageType_'+rowId).val());  
	if(productPackageDetailId > 0){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
		 	async: false,  
		 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				$('#grnPackageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
			} 
		});   
	}else{
		$('#grnPackageUnit_'+rowId).val($('#receiveQty_'+rowId).val());
	} 
	return false;
}

function commonSupplierPopup(supplierId, supplierName,
		combinationId, accountCode, param){
	$('#supplierId').val(supplierId);
	$('#supplierName').val(supplierName);
	$('#supplierCombinationId').val(combinationId);
	showActivePurchaseOrder(supplierId);
	return false;
}
function getPendingBatch(rowId){
	purchsaeDetailJS = []; 
	var purchaseDetailId = Number($(
			'#purchaseLineId_' + rowId).val());
	var purchaseId = Number($(
			'#purchasePOId').val());
	var receiveHiddenQty = Number($.trim($('#receiveHiddenQty_' + rowId).val()));
	$('.rowidpo').each(function(){ 
		var tempRowId = getRowId($(this).attr('id')); 
		var productIdtmp = $('#productid_' + tempRowId).val();
		var receiveQtytmp = Number($.trim($('#receiveQty_' + tempRowId).val()));
		var purchaseDetailIdtmp = Number($(
				'#purchaseLineId_' + tempRowId).val()); 
		if(typeof productIdtmp != 'undefined' && productIdtmp > 0 && receiveQtytmp > 0){
			var jsonData = [];
			jsonData.push({
				"productId" : productIdtmp,
				"receiveQty" : receiveQtytmp,  
				"purchaseDetailId": purchaseDetailIdtmp
			});   
			purchsaeDetailJS.push({
				"purchsaeDetailJS" : jsonData
			});
		} 
	});
	
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/validatebatch_pendingreceive_purchase.action",
		async : false,
		data:{purchaseId: purchaseId, purchaseDetailId: purchaseDetailId, purchaseDetails: JSON.stringify(purchsaeDetailJS)},
		dataType : "json",
		cache : false,
		success : function(response) {
			if(response.returnMessage == "SUCCESS"){
				var productId = $('#productid_' + rowId).val();
				var poQty = Number($.trim($('#purchaseqty_' + rowId).text()));
				var receivedQty = Number($.trim($('#receivedqty_' + rowId).text()));
				var returnedQty = Number($.trim($('#returnedqty_' + rowId).text()));
				var receiveQty = Number($.trim($('#receiveQty_' + rowId).val()));
 				receiveHiddenQty = Number($.trim($('#receiveHiddenQty_' + rowId).val()));
				var pendingQty = Number(receiveHiddenQty - receiveQty);
				var unitRate = Number($jquery('#unitRate_' + rowId).val());
				var shelfId = Number($(
						'#storeDetail_' + rowId).val());
				purchaseDetailId = Number($(
						'#purchaseLineId_' + rowId).val());
				purchaseId = Number($(
						'#purchasePOId_' + rowId).val());
				var receiveDetailId = Number($(
						'#receivelineid_' + rowId).val());
				var batchNumber = $('#batchNumber_' + rowId).val();
				var expiryDate = $('#expiryBatchDate_' + rowId).val();
				var linesDescription = $('#linedescription_' + rowId)
						.val(); 
				var packageDetailId = Number($(
						'#grnpackageType_' + rowId).val());
				var packageUnit= Number($(
						'#grnPackageUnit_' + rowId).val());
			 	var fieldArray = [];
				$('.rowidpo').each(function(){ 
					var rowId = getRowId($(this).attr('id')); 
					fieldArray.push(rowId);
				});
				var lastRowId = Math.max.apply(Math, fieldArray);
				lastRowId = Number(lastRowId + 1); 
				purchsaeDetailJS = [];  
				if(pendingQty > 0){
					if(typeof productId != 'undefined' && productId > 0 && poQty > 0){ 
						purchsaeDetailJS.push({
							"productId" : productId,
							"poQty" : poQty, 
							"receivedQty" : receivedQty,
							"returnedQty" : returnedQty,
							"packageDetailId" : packageDetailId,
							"packageUnit" : packageUnit,
							"receiveQty" : receiveQty,
							"receiveHiddenQty" : receiveHiddenQty,
							"unitRate" : unitRate, 
							"shelfId" : shelfId, 
							"batchNumber" : batchNumber, 
							"productExpiry" : expiryDate, 
							"linesDescription" : linesDescription, 
							"receiveDetailId": receiveDetailId,
							"purchaseDetailId": purchaseDetailId,
							"purchaseId": purchaseId,
							"lastRowId": lastRowId
						}); 
					}  
					$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/showbatch_pendingreceive_purchase.action",
						async : false,
						data:{purchaseDetails: JSON.stringify(purchsaeDetailJS)},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('#receiveHiddenQty_'+rowId).val(receiveQty);
 							$('#fieldrowpo_'+rowId).after(result); 
							return false;
						}
					});
				} 
			}else{
				$('#grnPackageUnit_'+rowId).parent().append("<span class='returnerr' id=receiveerror_"+rowId+">Receive Qty should be lt or eq PO qty.</span>");  
				$('#grnPackageUnit_'+rowId).val('');
				return false;
			}
		}
	}); 
	return false;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			goods receive notes
		</div> 
		<form name="receiveNoteEntry" id="receiveNoteValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="receive-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="receiveId" name="receiveId"
					value="${RECEIVE_INFO.receiveId}" />
				<c:set var="display" value="none"/>
				<c:if test="${RECEIVE_INFO.receiveId ne null && RECEIVE_INFO.receiveId ne ''}"> 
					<c:set var="display" value="show"/>
				</c:if>
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48">
						<fieldset style="min-height: 90px;">
							<div style="display: none;">
								<label class="width30" for="supplierNumber"><fmt:message
										key="accounts.grn.label.suppliernumber" /><span
									class="mandatory">*</span> </label> <input type="hidden"
									id="supplierId" name="supplierId"
									value="${RECEIVE_INFO.supplier.supplierId}" /> <input
									type="hidden" id="supplierCombinationId"
									name="supplierCombinationId" value="${RECEIVE_INFO.supplier.combination.combinationId}"/> <input type="text"
									readonly="readonly" name="supplierNumber" id="supplierNumber"
									class="width50 validate[required]"
									value="${RECEIVE_INFO.supplier.supplierNumber}" />
							</div>
							<div>
								<label class="width30" for="supplierName"><fmt:message
										key="accounts.grn.label.suppliername" /><span
									class="mandatory">*</span> </label> <input type="text"
									readonly="readonly" name="supplierName" id="supplierName"
									class="width50 validate[required]"
									value="${RECEIVE_INFO.supplierName}" />
								<c:if test="${display eq 'none'}">
									<span class="button"> <a style="cursor: pointer;"
											class="btn ui-state-default ui-corner-all supplierreceive-common-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</c:if> 
							</div>
							<div>
								<label class="width30 tooltip" for="description"><fmt:message
										key="accounts.grn.label.description" /> </label>
								<textarea id="description" class="width51 float-left">${RECEIVE_INFO.description}</textarea>
							</div>
							<div class="width100 float-left ">									
							
								<div id="receive_document_information" class="width90" style="cursor: pointer; color: blue;">
									<u>Upload Documents Here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px;" class="width90">
									<span id="receiveDocs"></span>
								</div>
								
							</div>		
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30" for="receiveNumber"> <fmt:message
										key="accounts.grn.label.receivenumber" /> </label>
								<c:choose>
									<c:when
										test="${RECEIVE_INFO.receiveId ne null && RECEIVE_INFO.receiveId ne ''}">
										<input name="receiveNumber" type="text" readonly="readonly"
											id="receiveNumber" value="${RECEIVE_INFO.receiveNumber}"
											class="width40 validate[required]">
									</c:when>
									<c:otherwise>
										<input name="receiveNumber" type="text" readonly="readonly"
											id="receiveNumber" value="${receiveNumber}"
											class="width40 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="receiveDate"><fmt:message
										key="accounts.grn.label.receivedate" /><span
									style="color: red;">*</span> </label>
								<input type="text" name="receiveDate" readonly="readonly" value="${RECEIVE_INFO.strReceiveDate}"
									class="width40 validate[required]" style="margin-bottom: 5px;" id="receiveDate"/>
							</div>
							<div>
								<label class="width30" for="referenceNo">Invoice No.</label> <input type="text"
									name="referenceNo" class="width40" id="referenceNo" value="${RECEIVE_INFO.referenceNo}"/>
							</div>
							<div>
								<label class="width30" for="deliveryNo">Delivery No.</label> <input type="text"
									name="deliveryNo" class="width40" id="deliveryNo" value="${RECEIVE_INFO.deliveryNo}"/>
							</div>
							<div id="extraReferenceDiv" style="display:none;">
								<div>
								<label class="width30" for="reference1">Bill Of Entry</label> <input type="text"
									name="reference1" class="width40" id="reference1" value="${RECEIVE_INFO.reference1}"/>
								</div>
								<div>
									<label class="width30" for="reference2">Other Ref.</label> <input type="text"
										name="reference2" class="width40" id="reference2" value="${RECEIVE_INFO.reference2}"/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="purchase-info" id="hrm">
					<c:if test="${display eq 'show'}"> 
						<fieldset> 
							<legend><fmt:message key="accounts.grn.label.activepurchaseorder"/></legend>
							<div>
								<c:forEach var="ACTIVE_EDITPO" items="${PURCHASE_EDITINFO}">
									<label for="purchase_${ACTIVE_EDITPO.purchaseId}" class="width10">
										<input type="checkbox" checked="checked" name="activePONumber" class="activePONumberEdit activePONumber" id="purchase_${ACTIVE_EDITPO.purchaseId}"/>
										${ACTIVE_EDITPO.purchaseNumber}
									</label>
								</c:forEach>
								<c:forEach var="ACTIVE_PO" items="${PURCHASE_INFO}">
									<label for="purchase_${ACTIVE_PO.purchaseId}" class="width10">
										<input type="checkbox" name="activePONumber" class="activePONumberEdit activePONumber" id="purchase_${ACTIVE_PO.purchaseId}"/>
										${ACTIVE_PO.purchaseNumber}
									</label>
								</c:forEach>
							</div>
						</fieldset>
					</c:if>
				</div>
			</div> 
			<div id="hrm" class="portlet-content width100 purchaseline-info"
				style="display: ${display};">
				<input type="hidden" id="storeflag" value="false" />
				<fieldset id="hrm">
					<legend>PO Details<span
									class="mandatory">*</span></legend>
					<div class="portlet-content">
						<div id="page-error"
							class="response-msg error ui-corner-all width98"
							style="display: none;"></div>
						<div id="warning_message"
							class="response-msg notice ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100"> 
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 3%"><fmt:message
												key="accounts.grn.label.pono" /></th>
										<th style="width: 3%"><fmt:message
												key="accounts.grn.label.podate" /></th>
										<th style="width: 2%"><fmt:message
												key="accounts.grn.label.poexpiry" /></th>
										<th style="display: none;"><fmt:message
												key="accounts.grn.label.poquantity" /></th>
										<th style="width: 2%"><fmt:message
												key="accounts.grn.label.poamount" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.grn.label.description" /></th>
										<th style="width: 1%"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="PURCHASE_BYID" items="${PURCHASE_BYIDS}" varStatus="statuspo">
										<tr class="rowid" id="fieldrow_${statuspo.index+1}"> 
											<td id="lineId_${statuspo.index+1}" style="display:none;">${statuspo.index+1}</td>
											<td>
												 ${PURCHASE_BYID.purchaseNumber}
											</td>  
											<td>
												<c:set var="podate" value="${PURCHASE_BYID.date}"/>
												<%=DateFormat.convertDateToString(pageContext.getAttribute("podate").toString())%> 
											</td> 
											<td> 
												<c:set var="expirydate" value="${PURCHASE_BYID.expiryDate}"/>
												<%=DateFormat.convertDateToString(pageContext.getAttribute("expirydate").toString())%>  
											</td> 
											<td style="display: none;">
												 ${PURCHASE_BYID.totalPurchaseQty}
											</td>
											<td style="text-align: right;" id="totalpoamt_${statuspo.index+1}">
												<c:set var="totalamt" value="${PURCHASE_BYID.totalPurchaseAmount}"/>
												<%=AIOSCommons.formatAmount(pageContext.getAttribute("totalamt"))%> 
											</td>
											<td>
												${PURCHASE_BYID.description}
											</td>   
											<td style="display:none;">
												<input type="hidden" id="purchaseId_${statuspo.index+1}" value="${PURCHASE_BYID.purchaseId}"/>
												<c:choose>
													<c:when test="${PURCHASE_BYID.continuousPurchase ne null && PURCHASE_BYID.continuousPurchase eq true}">
														<input type="checkbox" name="continuousPurchase" id="continuousPurchase_${statuspo.index+1}" class="width3" checked="checked"/>
													</c:when>
													<c:otherwise>
														<input type="checkbox" name="continuousPurchase" id="continuousPurchase_${statuspo.index+1}" class="width3" />
													</c:otherwise>
												</c:choose>
												</td> 
											 <td style="width:0.01%;" class="opn_td" id="option_${statuspo.index+1}"> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatarc" id="EditImage_${statuspo.index+1}" style="cursor:pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span>
												  </a> 
												  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowrc" id="DeleteImage_${statuspo.index+1}" style="cursor:pointer;" title="Delete Record">
														<span class="ui-icon ui-icon-circle-close"></span>
												 </a> 
											</td>   
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<div class="width30 float-right totalReceives"
				style="font-weight: bold; display: none;">
				<span><fmt:message key="accounts.grn.label.total" />: </span> <span
					id="totalReceiveAmount"></span>
			</div>
			<span class="callJq"></span>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div
					class="portlet-header ui-widget-header float-right receive_discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div
					class="portlet-header ui-widget-header float-right receive_save"
					id="saveGRN" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>
