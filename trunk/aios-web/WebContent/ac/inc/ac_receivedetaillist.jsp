 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<c:forEach var="bean" varStatus="status" items="${RECEIVE_DETAIL_LIST}"> 
	<tr class="rowid" id="fieldrow_${status.index+1}">
		<td style="display:none;" id="lineId_${status.index+1}">${status.index+1}</td> 
		<td id="productcode_${status.index+1}"> 
			${bean.product.code}
		</td>
		<td id="productname_${status.index+1}"> 
			${bean.product.productName}
		</td> 
		<td id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
		<td id="unitrate_${status.index+1}" style="text-align: right;">${bean.unitRate}</td> 
		<td id="receiveqty_${status.index+1}">${bean.receiveQty}</td> 
		<td>
			<input type="text" class="width90 returnqty validate[optional,custom[onlyFloat]]" style="border:0px;" value="${bean.receiveQty}" name="returnQty" id="returnQty_${status.index+1}"/>
		</td>   
		<td id="total_${status.index+1}" style="text-align: right;">
			 <c:out value="${bean.receiveQty * bean.unitRate}"/>
		</td>  
		<td style="display:none"> 
			<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/>
			<input type="hidden" name="debitLineId" id="debitLineId_${status.index+1}" value="0"/> 
		</td> 
		<td>
			<a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status.index+1}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		    </a>	
		   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		   </a> 
		   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		   </a>
		   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status.index+1}" style="display:none;" title="Working">
				<span class="processing"></span>
		   </a>
		</td> 
	</tr>
</c:forEach>