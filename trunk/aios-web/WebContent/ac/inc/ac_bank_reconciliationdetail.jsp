<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		$('.transactionDate').datepick();
	});
</script>
<c:choose>
	<c:when
		test="${BANK_RECONCILIATION_STATEMENT.bankReconciliationDetailVOs ne null 
	&& fn:length(BANK_RECONCILIATION_STATEMENT.bankReconciliationDetailVOs)>0}">
		<fieldset>
			<legend>Bank Reconciliation Detail</legend>
			<input type="hidden" id="processFlag" value="1" />
			<div id="hrm" class="hastable width100">
				<input type="hidden"
					value="${BANK_RECONCILIATION_STATEMENT.accountBalanceStr}"
					id="accountBalanceTemp" />
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th style="width: 1%"><input type="hidden"
								id="checkbankreconAll" name="checkbankreconAll"
								class="checkbankreconAll" /></th>
							<th style="width: 3%">Date</th>
							<th style="width: 3%">Transaction Type</th>
							<th style="width: 3%">Reference</th>
							<th style="width: 3%">Cheque Date</th>
							<th style="width: 3%">Cheque No</th>
							<th style="width: 5%">Description</th>
							<th style="width: 3%;">Payment</th>
							<th style="width: 3%;">Receipt</th>
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach var="RECONCILIATION_DETAIL"
							items="${BANK_RECONCILIATION_STATEMENT.bankReconciliationDetailVOs}"
							varStatus="status">
							<tr class="rowid" id="fieldrow_${status.index+1}">
								<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
								<td>
									<input type="checkbox" id="reconcile_${status.index+1}" class="reconcile" checked="checked"/>
									<input type="hidden" id="flowId_${status.index+1}" class="flowId" value="${RECONCILIATION_DETAIL.flowId}"/>
									<input type="hidden" id="chequeEntry_${status.index+1}" class="chequeEntry" value="${RECONCILIATION_DETAIL.chequeEntry}"/>
									<input type="hidden" id="useCaseId_${status.index+1}" class="useCaseId" value="${RECONCILIATION_DETAIL.usecaseRecordId}"/>
									<input type="hidden" id="useCase_${status.index+1}" class="useCase" value="${RECONCILIATION_DETAIL.useCaseName}"/>
									<input type="hidden" id="bankReconciliationDetailId_${status.index+1}" class="bankReconciliationDetailId"/>
								</td>
								<td>${RECONCILIATION_DETAIL.transactionDateStr}</td>
								<td>${RECONCILIATION_DETAIL.useCase}</td>
								<td>${RECONCILIATION_DETAIL.referenceNumber}</td>
								<td>${RECONCILIATION_DETAIL.chequeDateStr}</td>
								<td>${RECONCILIATION_DETAIL.chequeNumber}</td>
								<td>${RECONCILIATION_DETAIL.description}</td>
								<td style="text-align: right;">
									${RECONCILIATION_DETAIL.paymentAmount}</td>
								<td style="text-align: right;">
									${RECONCILIATION_DETAIL.receiptAmount}</td>
							</tr>
						</c:forEach>
						<tr>
							<td style="text-align: right; font-weight: bold;" colspan=7>Total</td> 
							<td style="text-align: right; font-weight: bold;" id="paymentTotal">${BANK_RECONCILIATION_STATEMENT.paymentTotal}</td>
							<td style="text-align: right; font-weight: bold;" id="receiptTotal">${BANK_RECONCILIATION_STATEMENT.receiptTotal}</td>
						</tr>
					</tbody>
				</table> 
				<div class="clearfix"></div>
				<table class="width40 float-left" style="position: relative; top: 5px;">
					<thead>
						<tr> 
							<th>Matched Balance</th>
							<th>Statement Balance</th>
							<th>Difference</th>
						</tr>
					</thead>
					<tr>
						<td class="right-align width30" id="accountBalanceStr">${BANK_RECONCILIATION_STATEMENT.accountBalanceStr}</td>
						<td class="right-align width30" id="statementBalanceStr">
							${BANK_RECONCILIATION_STATEMENT.statementBalanceStr}</td>
						<td class="right-align width30" id="differenceTotal">${BANK_RECONCILIATION_STATEMENT.differenceTotal}</td>
					</tr> 
				</table>
			</div>
		</fieldset>
	</c:when>
	<c:when test="${RETURN_MESSAGE ne null && RETURN_MESSAGE ne ''}">
		<input type="hidden" id="processFlag" value="0" />
		<fieldset>
			<legend>Bank Reconciliation Detail</legend>
			<div id="hrm" class="hastable width100">
				<span style="font-weight: bold; color: #ff255c;">${RETURN_MESSAGE}</span>
			</div>
		</fieldset>
	</c:when>
	<c:otherwise>
		<input type="hidden" id="processFlag" value="0" />
		<fieldset>
			<legend>Bank Reconciliation Detail</legend>
			<div id="hrm" class="hastable width100">
				<span style="font-weight: bold; color: #ff255c;">Result not
					found.</span>
			</div>
		</fieldset>
	</c:otherwise>
</c:choose> 