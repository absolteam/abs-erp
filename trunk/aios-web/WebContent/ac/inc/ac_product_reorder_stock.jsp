<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}


.portlet-content{padding:1px;}
.buttons { margin:4px;}
table.display td{
padding:3px;
}
.ui-widget-header{
	padding:4px;
}
.ui-autocomplete {
		width:17%!important;
		height: 200px!important;
		overflow: auto;
	} 
	.ui-autocomplete-input {
		width: 50%;
	}
	.ui-combobox-button{height: 32px;}
	button {
		position: absolute !important;
		margin-top: 2px;
		height: 22px;
	}
	label {
		display: inline-block;
	}


</style>
<script type="text/javascript"> 


var productId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
 
$(function(){ 
	 
	 $('.formError').remove();
	
	 getProductReOrderList();  
		
		/* Click event handler */ 
		
		 $(".print-call").click(function(){ 
			 window.open('product_stockreorder_htmreport.action?format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px'); 
			 });
		 
		  $(".pdf-download-call").click(function(){ 
			  if (purchaseId != 0) {
					 fromDate=$('.fromDate').val();
					 toDate=$('.toDate').val();
					 percentage=$('#percentage').val();
		
					window.open('<%=request.getContextPath()%>/purchase_order_report_download_jasper.action?fromDate='
							+ fromDate + '&toDate='
							+ toDate + '&purchaseId='
							+ purchaseId, +'_blank',
					'width=800,height=700,scrollbars=yes,left=100px,top=2px');	
					return false;
		 		 }  else {
		 			$('#error_message').hide().html("Please select a record to view.").slideDown();
					$('#error_message').delay(3000).slideUp();
				 }
				
			}); 
		  
		  $(".xls-download-call").click(function(){
			 
				window.open('<%=request.getContextPath()%>/product_stockreorder_xls_report.action?','_blank',
									'width=800,height=700,scrollbars=yes,left=100px,top=2px');
		
				return false;
			});
	}); 
	 
	function getProductReOrderList() { 
		oTable = $('#ProductReorderDT').dataTable({ 
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers",
			"bFilter" : true,
			"bInfo" : true,
			"bSortClasses" : true,
			"bLengthChange" : true,
			"bProcessing" : true,
			"bDestroy" : true,
			"iDisplayLength" : 10,
			"sAjaxSource" : "show_reorder_stock_list.action",

			"aoColumns" : [ { 
				"mDataProp" : "productCode"
			 }, {
				"mDataProp" : "productName"
			 }, {
				"mDataProp" : "categoryName"
			 }, {
				"mDataProp" : "reorderQuantity"
			 }, {
				"mDataProp" : "availableQuantity"
			 },{
				"mDataProp" : "costPrice"
			 } ], 
		});
		
		//init datatable
		oTable = $('#ProductReorderDT').dataTable();

	} 
	  
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>product
			reorder
		</div>
		<div class="portlet-content"> 
			<div id="product_reorder_stock">
					<table id="ProductReorderDT" class="display">
						<thead>
							<tr>
								<th>Product Code</th> 
								<th>Product</th> 
								<th>Category</th> 
								<th>ReOrder Quantity</th> 
								<th>Available Quantity</th> 
 								<th>Cost Price</th> 
 							</tr>
						</thead> 
					</table> 
				</div> 
		</div>
	</div> 
</div>
<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div style="display: none;" class="width5 float-right height30" title="Download as PDF">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div style="" class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>