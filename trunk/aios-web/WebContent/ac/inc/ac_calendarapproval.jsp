<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.opn_td {
	width: 6%;
}

#DOMWindow {
	width: 95% !important;
	height: 90% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.spanfont {
	font-weight: normal;
}

.divpadding {
	padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var processFlag=0;	
$(function(){
	$('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#main-content', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	$('.callJq').trigger('click');  

	$('#DOMWindowOverlay').click(function(){
		$('#DOMWindow').remove();
		$('#DOMWindowOverlay').remove();
		window.location.reload();
	}); 
}); 
</script>
<div id="main-content">
	<div id="fy_cancel" style="display: none; z-index: 9999999">
		<div class="width98 float-left"
			style="margin-top: 10px; padding: 3px;">
			<span style="font-weight: bold;">Comment:</span>
			<textarea rows="5" id="comment" class="width100"></textarea>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Financial
			Year
		</div>
		<form name="newFields" id="editCalendarVal"
			style="position: relative;">
			<div class="portlet-content width100">
				<div class="width100" id="hrm">
					<div class="width50 float-left">
						<fieldset>
							<div class="divpadding">
								<label class="width30">Financial Name</label> <span
									class="width60 spanfont">${CALENDAR_DETAILS.name}</span> <input
									type="hidden" name="editCalHederId"
									value="${CALENDAR_DETAILS.calendarId}" id="calHeaderId" />
							</div>
							<div class="divpadding">
								<label class="width30">From Date</label> <span
									class="width60 spanfont">${CALENDAR_DETAILS.start}</span>
							</div>
							<div class="divpadding">
								<label class="width30">To Date</label> <span
									class="width60 spanfont">${CALENDAR_DETAILS.end}</span>
							</div>
							<div class="divpadding">
								<label class="width30">Year Status</label> <span
									class="width60 spanfont">${CALENDAR_DETAILS.statusStr}</span>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content">
				<div id="hrm" class="hastable width100">
					<fieldset>
						<legend>Period Details</legend>
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th class="width20"><fmt:message
											key="accounts.calendar.label.periodname" />
									</th>
									<th class="width5"><fmt:message
											key="accounts.calendar.label.fromdate" />
									</th>
									<th class="width5"><fmt:message
											key="accounts.calendar.label.todate" />
									</th>
									<th class="width5"><fmt:message
											key="accounts.calendar.label.adjustingperiod" />
									</th>
									<th class="width5">Period Status</th> 
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach items="${CALENDAR_DETAILS.periodVOs}" var="result"
									varStatus="status">
									<tr id="fieldrow_${status.index+1}" class="rowid">
										<td class="width20" id="name_${status.index+1}">${result.name}</td>
										<td class="width5" id="startTime_${status.index+1}">${result.start}</td>
										<td class="width5" id="endTime_${status.index+1}">${result.end}</td>
										<td class="width5" id="extention_${status.index+1}">${result.extention}</td>
										<td class="width5" id="periodstatus_${status.index+1}">${result.statusStr}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</fieldset>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="clearfix"></div>