<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
#common-popup{
	overflow: hidden;
}
 td .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; right:3px;}
	td  .ui-button { margin: 0px; height:24px;width:2%; position: absolute; left:475px;}
	.ui-autocomplete{
		width:194px!important;
	} 
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript"> 
var slidetab="";  
var collateralLineDetail="";
$(function(){
	manupulateLastRow();
	 $(".assetDetail").each(function(){
		 var rowId=getRowId($(this).attr('id'));  
		 $('#asset_'+rowId+' option').each(function(){    
	 	  	var assetId=$(this).val(); 
	 	  	var tempAssetId=$('#tempAssetId_'+rowId).val();  
			if(assetId==tempAssetId){  
				var assetName=$(this).text(); 
				console.debug(assetId+"------"+tempAssetId+"------"+rowId);
				$('#asset_'+rowId+' option[text='+assetName+']').attr("selected","selected"); 
				$('#asset_'+rowId+' option[value='+assetId+']').attr("selected","selected"); 
			}
	  	}); 
	 }); 
	$('.common-popup').click(function(){ 
	       tempid=$(this).attr('id'); 
	       $('.ui-dialog-titlebar').remove();  
	       var currentaction="";
	       var bankId=Number(0);
	       var accountId=Number(0);
	       if(tempid=="bankid"){
	    	   $('#accountId').val("");
	    	   $('#accountNumber').val(""); 
	    	   currentaction="getbankdetails";
	       }
	       else if(tempid=="accountid"){
	    	   bankId=$('#bankId').val();
	    	   currentaction="getbankaccount_details";
	       }
	       else if(tempid=="loanid"){
	    	   accountId=$('#accountId').val();
	    	   currentaction="getloanpopup_details";
	       }
		 	 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/"+currentaction+".action", 
			 	async: false, 
			 	data:{bankId:bankId, accountId:accountId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.popup-result').html(result);  
				},
				error:function(result){  
					 $('.popup-result').html(result); 
				}
			});  
		});
		 $('#common-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});
		 
		 $('.delrow').live('click',function(){ 
			 slidetab=$(this).parent().parent().get(0);  
	       	 $(slidetab).remove();  
	       	 var i=1;
	    	 $('.rowid').each(function(){   
	    		 var rowId=getRowId($(this).attr('id')); 
	    		 $('#lineId_'+rowId).html(i);
				 i=i+1; 
	 		 });   
		 });
		 
		 $('.assetDetail').combobox({ 
			 selected: function(event, ui){  
				 var assetId=$(this).val(); 
				 var rowId=getRowId($(this).attr('id'));  
				 if(assetId!=null && assetId!="" && $('#amount_'+rowId).val()!=null && $('#amount_'+rowId).val()!=""){
					 triggerAddRow(rowId);
				 } 
			 }
		 });  
		 
		 $('.amount').live('change',function(){
			 var rowId=getRowId($(this).attr('id'));  
			 triggerAddRow(rowId);
		 });
		 
		 $('.addrows').click(function(){ 
			  var i=Number(1); 
			  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
			  id=id+1;  
			  $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/collateral_addrow.action", 
				 	async: false,
				 	data:{id: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tab tr:last').before(result);
						 if($(".tab").height()>255)
							 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowid').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#lineId_'+rowId).html(i); 
							 i=i+1; 
				 		 });  
					}
				});
		 });
		 
		 $('.save').click(function(){
			 var loanId=$('#loanId').val();
			 collateralLineDetail=getCollateralLineDetails(); 
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/savecollateraldetails.action", 
				 	async: false, 
				 	data:{loanId:loanId, collateralLineDetail:collateralLineDetail},
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $(".tempresult").html(result);
						 var message=$('.tempresult').html(); 
						 if(message.trim()=="SUCCESS"){
							 $.ajax({
									type:"POST",
									url:"<%=request.getContextPath()%>/collateral_showlist.action", 
								 	async: false,
								    dataType: "html",
								    cache: false,
									success:function(result){
										$('#common-popup').dialog('destroy');		
					  					$('#common-popup').remove();  
										$("#main-wrapper").html(result); 
										$('#success_message').hide().html(message).slideDown(1000);
									}
							 });
						 }
						 else{
							 $('#page-error').hide().html(message).slideDown(1000);
							 return false;
						 }
					},
					error:function(result){  
						$('#page-error').hide().html("Service not found").slideDown(1000);
					}
				}); 
		 });
		 
		 $('.discard_c').live('click',function(){
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/collateral_showlist.action", 
			 	async: false,
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
  					$('#common-popup').remove();  
					$("#main-wrapper").html(result);
					return false;
				}
			 });
			 return false;
		});
		 
		 var getCollateralLineDetails=function(){
				var assetIds=new Array();
				var amounts=new Array(); 
				var collateralIds=new Array();
				$('.rowid').each(function(){
					var rowId=getRowId($(this).attr('id')); 
					var assetId=$('#asset_'+rowId).val();
					var amount=$('#amount_'+rowId).val(); 
					var collateralId=Number($('#collateralid_'+rowId).val());
					if(isNaN(collateralId)){
						collateralId=0;
					}
					if(typeof assetId != 'undefined' && assetId!=null && assetId!="" && amount!=null && amount!=""){
						assetIds.push(assetId);
						amounts.push(amount);  
						collateralIds.push(collateralId);
					}
				});
				for(var j=0;j<assetIds.length;j++){ 
					collateralLineDetail+=assetIds[j]+"↕"+amounts[j]+"↕"+collateralIds[j];
					if(j==assetIds.length-1){   
					} 
					else{
						collateralLineDetail+="↕↕";
					}
				} 
				return collateralLineDetail;
			};
});
function manupulateLastRow(){ 
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('tr:last').removeAttr('id');  
	$('tr:last').removeClass('rowid').addClass('lastrow');  
	$($('tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('tr:last').append("<td style=\"height:25px;\"></td>");
	}
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function triggerAddRow(rowId){
	 var lineAsset=$('#asset_'+rowId).val();  
	 var lineAmount=$('#amount_'+rowId).val();  
	 var nexttab=$('#fieldrow_'+rowId).next();  
	if(lineAsset!=null && lineAsset!=""
			&& lineAmount!=null && lineAmount!="" && $(nexttab).hasClass('lastrow')){ 
		$('.addrows').trigger('click'); 
		$('#DeleteImage_'+rowId).show(); 
	}
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>collateral information</div>	
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>general information</div>
		  <form name="loanentry_details" id="loanentry_details"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="collateralId" name="collateralId" value="${COLLATERAL_INFO.collateralId}"/>
				<div class="width100 float-left" id="hrm"> 
					<fieldset> 
						<div class="width100 float-left">
							<div>
								<label class="width10" for="accountNumber">Account No</label>
								<input type="text" readonly="readonly" name="accountNumber" id="accountNumber" tabindex="2" class="width30 validate[required]" value="${COLLATERAL_INFO.loan.bankAccount.accountNumber}"/>
								<span class="button">
									<a style="cursor: pointer;" id="accountid" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<input type="hidden" readonly="readonly" name="accountId" id="accountId" value="${COLLATERAL_INFO.loan.bankAccount.bankAccountId}"/>
							</div> 
							<div>
								<label class="width10" for="bankName">Bank Details</label>
								<input type="text" readonly="readonly" name="bankName" id="bankName" tabindex="1" class="width30 validate[required]" value="${COLLATERAL_INFO.loan.bankAccount.bank.bankName}"/>
								<span class="button"  style="display:none;">
									<a style="cursor: pointer;" id="bankid" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<input type="hidden" readonly="readonly" name="bankId" id="bankId"  value="${COLLATERAL_INFO.loan.bankAccount.bank.bankId}"/>
							</div>  
							<div>
								<label class="width10" for="loanNumber">Loan Number</label>
								<input type="text" readonly="readonly" name="loanNumber" id="loanNumber" tabindex="3" class="width30 validate[required]" value="${COLLATERAL_INFO.loan.loanNumber}"/>
								<span class="button">
									<a style="cursor: pointer;" id="loanid" class="btn ui-state-default ui-corner-all common-popup width100"> 
										<span class="ui-icon ui-icon-newwin"> 
										</span> 
									</a>
								</span>
								<input type="hidden" readonly="readonly" name="loanId" id="loanId" value="${COLLATERAL_INFO.loan.loanId}"/>
						   </div>  
					</div> 
				</fieldset>
			</div>
			<div class="clearfix"></div> 
 			<div class="portlet-content" style="margin-top:10px;" id="hrm"> 
 			<fieldset>
 				<legend>Asset Information</legend>
	 			<div  id="line-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
					<div id="hrm" class="hastable width100"  >  
						<input type="hidden" name="childCount" id="childCount" value="${fn:length(COLLATERAL_INFO_LIST)}"/>  
						<table id="hastab" class="width100"> 
							<thead>
							   <tr> 
									<th style="width:1%">Line No</th> 
								    <th style="width:5%">Asset</th>  
								    <th style="width:5%">Collateral Value</th>   
									<th style="width:0.01%;"><fmt:message key="accounts.common.label.options"/></th> 
							  </tr>
							</thead> 
							<tbody class="tab">  
							<c:forEach var="bean" items="${COLLATERAL_INFO_LIST}" varStatus="status1">
								<tr class="rowid" id="fieldrow_${status1.index+1}">
									<td id="lineId_${status1.index+1}">${status1.index+1}</td> 
									<td>
										<select name="assetDetail" class="width70 assetDetail" id="asset_${status1.index+1}" style="border:0px;text-align; right;"> 
											<option value="">Select</option>
											<c:forEach var="entry" items="${ASSET_DETAIL}">
												<option value="${entry.key}">${entry.value}</option>
											</c:forEach> 
										</select>
										<input type="hidden" id="tempAssetId_${status1.index+1}" value="${bean.assetId}"/>
									</td> 
									<td>
										<input type="text" style="text-align:right;border:0px;" class="width50 amount" id="amount_${status1.index+1}" value="${bean.amount}"/>
									</td> 
									<td style="display: none;">
										<input type="hidden" id="collateralid_${status1.index+1}" value="${bean.collateralId}"/>
									</td>
									 <td style="width:0.01%;" class="opn_td" id="option_${status1.index+1}">
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${status1.index+1}" style="display:none;cursor:pointer;" title="Add Record">
				 								<span class="ui-icon ui-icon-plus"></span>
										  </a>	
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${status1.index+1}" style="display:none; cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
										  </a> 
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${status1.index+1}" style="cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
										  </a>
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${status1.index+1}" style="display:none;" title="Working">
												<span class="processing"></span>
										  </a>
									</td>   
								</tr>
							</c:forEach>
							<c:forEach var="i" begin="${fn:length(COLLATERAL_INFO_LIST)+1}" end="${fn:length(COLLATERAL_INFO_LIST)+2}" step="1" varStatus ="status"> 
								<tr class="rowid" id="fieldrow_${i}">
									<td id="lineId_${i}">${i}</td> 
									<td>
										<select name="assetDetail" class="width70 assetDetail" id="asset_${i}" style="border:0px;text-align; right;"> 
											<option value="">Select</option>
											<c:forEach var="entry" items="${ASSET_DETAIL}">
												<option value="${entry.key}">${entry.value}</option>
											</c:forEach> 
										</select>
									</td> 
									<td>
										<input type="text" style="text-align:right;border:0px;" class="width50 amount" id="amount_${i}"/>
									</td> 
									<td style="display: none;">
										<input type="hidden" id="collateralid_${i}"/>
									</td>
									 <td style="width:0.01%;" class="opn_td" id="option_${i}">
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${i}" style="display:none;cursor:pointer;" title="Add Record">
				 								<span class="ui-icon ui-icon-plus"></span>
										  </a>	
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${i}" style="display:none; cursor:pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span>
										  </a> 
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${i}" style="display:none;cursor:pointer;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span>
										  </a>
										  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${i}" style="display:none;" title="Working">
												<span class="processing"></span>
										  </a>
									</td>   
								</tr>
							</c:forEach>
					 </tbody>
				</table>
			</div> 
		</fieldset>
	</div>  
	</div>  
	<div class="clearfix"></div>
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
		<div class="portlet-header ui-widget-header float-right discard_c" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
		<div class="portlet-header ui-widget-header float-right save" id="${showPage}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
	 </div> 
	<div  style="display: none;" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons"> 
		<div class="portlet-header ui-widget-header float-left addrows" style="cursor:pointer;"><fmt:message key="accounts.common.button.addrow"/></div> 
	</div>  
	<div class="clearfix"></div> 
	 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 60px!important; left: -228px!important;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="common-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="popup-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
	</div> 
  </form>
  </div> 
</div>