<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
 <script type="text/javascript">
var materialRequisitionId = Number(0);
var storeId = Number(0);
var slidetab = "";
var requisitionDetails = [];

$(function(){
	 
	  $('input, select').attr('disabled', true);

	 $('#product-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 $('#store-common-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 }); 
	 $('.requisition-store-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();   
 	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
			 	async: false,  
			 	data: {rowId: Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('.requisition_product_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_common_popup.action", 
			 	async: false,  
			 	data: {itemType: "I", rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		}); 
		
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/material_requisition_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });
	 
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
 
	 	 
 	$('.productQty').live('change',function(){  
 		triggerAddRow(getRowId($(this).attr('id'))); 
 	 	return false;
	}); 

	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function commonStorePopup(storeid, storename, param) {
	storeId = storeid;
	$('#store').val(storename);
}
function commonProductPopup(productId, productName, rowId) {
	$('#productid_' + rowId).val(productId);
	$('#product_' + rowId).html(productName); 
} 
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
 	var productQty = Number($('#productQty_'+rowId).val()); 
 	var nexttab=$('#fieldrow_'+rowId).next();  
 	if(productid > 0 && productQty > 0 
 		 		&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
	return false;
}
 
 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material
			requisition
		</div> 
		<form name="materialRequisitionValidation"
			id="materialRequisitionValidation" style="position: relative;">
			<input type="hidden" id="useCase" value="${MATERIAL_REQUISITION.useCase}"/>
			<input type="hidden" id="recordId" value="${MATERIAL_REQUISITION.recordId}"/>
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 60px;">  
							<div style="display: none;">
								<label class="width30" for="searchLocation">Store
								 </label> <input type="text"
									readonly="readonly" name="store" id="store"
									class="width50"
									value="${MATERIAL_REQUISITION.store.storeName}" />
								<span class="button"
									style="position: relative !important; top: 6px !important; right: 40px; float: right;">
									<a style="cursor: pointer;" 
									class="btn ui-state-default ui-corner-all requisition-store-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="description"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${MATERIAL_REQUISITION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 60px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_REQUISITION.referenceNumber ne null && MATERIAL_REQUISITION.referenceNumber ne ''}"> 
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${MATERIAL_REQUISITION.referenceNumber} "
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="requisitionDate"> Requisition Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_REQUISITION.requisitionDate ne null && MATERIAL_REQUISITION.requisitionDate ne ''}">
										<c:set var="requisitionDate"
											value="${MATERIAL_REQUISITION.requisitionDate}" />
										<input name="requisitionDate" type="text" readonly="readonly"
											id="requisitionDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("requisitionDate").toString())%>"
											class="requisitionDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="requisitionDate" type="text" readonly="readonly"
											id="requisitionDate"
											class="requisitionDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
							<c:choose>
								<c:when test="${STORE_LIST ne null}">
									<div>
										<label class="width30" for="storeId">Requesting Store<span
									style="color: red;">*</span> </label>
										 <input type="text" readonly="readonly" class="width50" value="${MATERIAL_REQUISITION.store.storeName}"/>
									</div>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="storeId" value="0"/>
								</c:otherwise>
							</c:choose>	
							 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Material Requisition Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th> 
 										<th class="width5">Quantity</th>
 										<th class="width10"><fmt:message
												key="accounts.journal.label.desc" /></th>
										<th style="width: 1%; display: none;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab1">
									<c:forEach var="DETAIL"
										items="${MATERIAL_REQUISITION.materialRequisitionDetails}"
										varStatus="status">
										<tr>
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												value="${DETAIL.product.productId}" /> <span
												>${DETAIL.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer; display: none;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all requisition_product_list width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td>
 											<td><input type="text" name="productQty"
												value="${DETAIL.quantity}"
												class="productQty validate[optional,custom[number]] width80"> 
											</td> 
											<td><input type="text" name="linesDescription"
												value="${DETAIL.description}" class="width98"
												maxlength="150">
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a>  
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div> 
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div> 