<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">  
var selectRow=""; var aSelected = []; var aData="";
var oTable;
$(function(){  
	 oTable = $('#VoidChequePayment').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allcheque_jsonpayments.action",

		"aoColumns" : [ { 
			"mDataProp" : "paymentNumber"
		 },{
			"mDataProp" : "chequeNo"
		 }, {
			"mDataProp" : "bankName"
		 },{
			"mDataProp" : "accountNumber"
		 },{
			"mDataProp" : "payeeName"
		 },{
			"mDataProp" : "chequeDate"
		 }], 
	}); 

	oTable = $('#VoidChequePayment').dataTable();
	
	/* Click event handler */
	$('#VoidChequePayment tbody tr').live('dblclick', function () {   
		aData = oTable.fnGetData(this); 
 		commonPaymentPopup(aData.directPaymentId, aData.paymentId,
							aData.chequeBookNo, aData.paymentType,
							aData.paymentNumber, aData.chequeNo,
							aData.accountNumber, aData.bankName,
							aData.payeeName);
					$('#payment-common-popup').trigger('click');
				});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>payments
		</div>
		<div class="portlet-content">
			<div id="paymentChequeDiv">
				<table id="VoidChequePayment" class="display">
					<thead>
						<tr>
							<th>Reference</th> 
							<th>Cheque No</th>
							<th>Bank Name</th>
							<th>Account</th>
							<th>Payee</th>
							<th>Cheque Date</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="payment-common-popup">close</div>
			</div>
		</div>
	</div>
</div>