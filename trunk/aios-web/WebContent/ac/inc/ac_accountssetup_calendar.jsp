<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<form name="newFields" id="editCalendarVal" style="position: relative;">
	<input type="hidden" name="page" id="page-val" value="${showPage}" />
	<input type="hidden" name="tempcalendarName" id="tempcalendarName"
		value="${CALENDAR_DETAILS.name}" /> <input type="hidden"
		name="financial-status" id="financial-status"
		value="${FINANCIAL_STATUS}" />
	<div class="portlet-content width100">
		<input type="hidden" name="childCount" id="childCount"
			value="${fn:length(PERIOD_DETAILS)}" />
		<div class="width100" id="hrm">
			<div class="width48 float-right">
				<div>
					<label class="width30">From Date<span style="color: red;">*</span>
					</label> <input type="text" tabindex="2" name="startDate"
						readonly="readonly" value="${CALENDAR_DETAILS.startTime}"
						id="startPicker" class="width50 validate[required]">
				</div>
				<div>
					<label class="width30">To Date<span style="color: red;">*</span>
					</label> <input id="endPicker" type="text"
						class="width50 validate[required]" readonly="readonly"
						name="endDate" tabindex="3" value="${CALENDAR_DETAILS.endTime}">
				</div>
			</div>
			<div class="width50 float-left">
				<div>
					<label class="width30">Financial Name<span
						style="color: red;">*</span> </label> <input type="text" tabindex="1"
						name="calendarName" value="${CALENDAR_DETAILS.name}"
						id="calendarName" class="width50 validate[required]"> <input
						type="hidden" name="editCalHederId"
						value="${CALENDAR_DETAILS.calendarId}" id="calHeaderId" /> <span
						class="calendarflag"></span>
				</div>
				<div>
					<label class="width30">Year Status<span style="color: red;">*</span>
					</label> <select name="status" id="status"
						class="width51 validate[required]">
						<option value="1" selected="selected">Active</option>
						<option value="2">Inactive</option>
					</select> <input type="hidden" name="tempstatus" id="tempstatus"
						value="${CALENDAR_DETAILS.status}" />
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div id="temperror" class="response-msg error ui-corner-all"
		style="width: 80%; display: none"></div>
	<div class="portlet-content">
		<div id="hrm" class="hastable width100">
			<fieldset>
				<legend>
					Period Details<span class="mandatory">*</span>
				</legend>
				<div class="childCountErr response-msg error ui-corner-all"
					style="width: 80%; display: none;"></div>
				<div id="wrgNotice" class="response-msg notice ui-corner-all"
					style="width: 80%; display: none;"></div>
				<input type="hidden" name="trnValue" value="" readonly="readonly"
					id="transURN" /> <input type="hidden" name="openFlag"
					id="openFlag" value="0" />
				<table id="hastab" class="width100">
					<thead>
						<tr>
							<th class="width20"><fmt:message
									key="accounts.calendar.label.periodname" />
							</th>
							<th class="width5"><fmt:message
									key="accounts.calendar.label.fromdate" />
							</th>
							<th class="width5"><fmt:message
									key="accounts.calendar.label.todate" />
							</th>
							<th class="width5"><fmt:message
									key="accounts.calendar.label.adjustingperiod" />
							</th>
							<th class="width5">Period Status</th>
							<th style="width: 2%;"><fmt:message
									key="accounts.calendar.label.options" />
							</th>
						</tr>
					</thead>
					<tbody class="tab">
						<c:forEach items="${requestScope.PERIOD_DETAILS}" var="result"
							varStatus="status">
							<tr id="fieldrow_${status.index+1}" class="rowid">
								<td class="width20" id="name_${status.index+1}">${result.name}</td>
								<td class="width5" id="startTime_${status.index+1}">${result.startTime}</td>
								<td class="width5" id="endTime_${status.index+1}">${result.endTime}</td>
								<td class="width5" id="extention_${status.index+1}">${result.extention}</td>
								<c:choose>
									<c:when test="${result.status eq 1}">
										<td class="width5" id="periodstatus_${status.index+1}">Open</td>
									</c:when>
									<c:when test="${result.status eq 2}">
										<td class="width5" id="periodstatus_${status.index+1}">Close</td>
									</c:when>
									<c:when test="${result.status eq 3}">
										<td class="width5" id="periodstatus_${status.index+1}">Inactive</td>
									</c:when>
									<c:otherwise>
										<td class="width5" id="periodstatus_${status.index+1}">Temporary
											Closed</td>
									</c:otherwise>
								</c:choose>
								<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
								<c:choose>
									<c:when test="${result.status ne 2}">
										<td style="width: 2%;"><input type="hidden"
											name="periodId_${status.index+1}"
											id="periodId_${status.index+1}" value="${result.periodId}" />
											<input type="hidden" name="statusline_${status.index+1}"
											id="statusline_${status.index+1}" value="${result.status}" />
											<a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}" style="cursor: pointer;"
											title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
										</a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Working">
												<span class="processing"></span> </a></td>
									</c:when>
									<c:otherwise>
										<td style="width: 2%;"></td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
						<c:forEach var="i" begin="${fn:length(PERIOD_DETAILS)+1}"
							end="${fn:length(PERIOD_DETAILS)+2}" step="1" varStatus="status">
							<tr id="fieldrow_${i}" class="rowid">
								<td class="width20" id="name_${i}"></td>
								<td class="width5" id="startTime_${i}"></td>
								<td class="width5" id="endTime_${i}"></td>
								<td class="width5" id="extention_${i}"></td>
								<td class="width5" id="periodstatus_${i}"></td>
								<td style="display: none;" id="lineId_${i}">${i}</td>
								<td style="width: 2%;" id="option_${i}"><input
									type="hidden" name="periodId_${i}" id="periodId_${i}" value="0" />
									<input type="hidden" name="statusline" id="statusline_${i}" />
									<a style="cursor: pointer;"
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
									id="AddImage_${i}" title="Add Record"> <span
										class="ui-icon ui-icon-plus"></span> </a> <a
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
									id="EditImage_${i}" style="display: none; cursor: pointer;"
									title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
								</a> <a style="cursor: pointer; display: none;"
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
									id="DeleteImage_${i}" title="Delete Record"> <span
										class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
									class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
									id="WorkingImage_${i}" style="display: none;" title="Working">
										<span class="processing"></span> </a></td>

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</fieldset>
		</div>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="margin: 10px;">
		<div
			class="portlet-header ui-widget-header float-right cancel_calendar"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
		<div
			class="portlet-header ui-widget-header float-right delete_calendar"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.delete" />
		</div>
		<div class="portlet-header ui-widget-header float-right save_calendar"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.save" />
		</div>
	</div>
	<div style="display: none;"
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
		<div class="portlet-header ui-widget-header float-left addrows"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.addrow" />
		</div>
	</div>
	<input type="hidden" name="availflag" id="availflag" value="1" />
</form>