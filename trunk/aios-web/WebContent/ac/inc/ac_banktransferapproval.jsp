<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var tempid="";
var slidetab="";
var actionname=""; 
var clickedval="";
var accessCode = "";
$(function(){ 
 	if(Number($('#bankTransferId').val()> 0)){    
		$('#currency option[value='+$('#tempcurrency').val()+']').attr("selected","selected");  
		$('#transNature option[value='+$('#tempTransNature').val()+']').attr("selected","selected"); 
			populateUploadsPane("doc","transferDocs","BankTransfer", $('#bankTransferId').val());  
	} 
}); 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>internal
			transfers
		</div>
		<div class="tempresult" style="display: none;"></div>
		<div class="portlet-content">
			<div id="page-error"
				class="response-msg error commonErr ui-corner-all"
				style="display: none;"></div>
			<form id="banktransfervalidation" method="post" style="position: relative;">
				<input type="hidden" id="bankTransferId"
					value="${BANK_TRANSFER_INFO.bankTransferId}" />
				<div id="tabs"
					class="ui-tabs ui-widget ui-widget-content ui-corner-all width99"
					Style="float: left; *float: none;">
					<ul
						class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all width99">
						<li
							class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a
							href="#tabs-1">Transfer Detail</a>
						</li>
						<li class="ui-state-default ui-corner-top ui-tabs-selected"
							id="identitytab"><a href="#tabs-2">Bank Accounts Detail</a>
						</li>
					</ul>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
						id="tabs-1">
						<div id="hrm" class="width50 float-left">
							<fieldset style="min-height: 175px;">
								<div>
									<label class="width30">Transfer No.<span
										style="color: red">*</span>
									</label>
									<c:choose>
										<c:when
											test="${BANK_TRANSFER_INFO.transferNo ne null && BANK_TRANSFER_INFO.transferNo ne ''}">
											<input type="text" readonly="readonly" id="transferNo"
												class="validate[required] width50"
												value="${BANK_TRANSFER_INFO.transferNo}" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" id="transferNo"
												class="validate[required] width50"
												value="${requestScope.transferNo}" />
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30">Date<span style="color: red">*</span>
									</label>
									<c:choose>
										<c:when
											test="${BANK_TRANSFER_INFO.date ne null && BANK_TRANSFER_INFO.date ne ''}">
											<c:set var="date" value="${BANK_TRANSFER_INFO.date}" />
											<%
												String date = DateFormat.convertDateToString(pageContext
																.getAttribute("date").toString());
											%>
											<input type="text" readonly="readonly" 
												class="validate[required] width50" value="<%=date%>" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly"
												class="validate[required] width50" />
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30">Currency<span style="color: red">*</span>
									</label> <select name="currency" id="currency" disabled="disabled"
										class="validate[required] width51">
										<option value="">Select</option>
										<c:forEach var="CURRENCY" items="${CURRENCY_INFO}">
											<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempcurrency"
										value="${BANK_TRANSFER_INFO.currency.currencyId}" />
								</div>
								<div style="display: none;">
									<label class="width30">Transfer Nature<span
										style="color: red">*</span>
									</label> <select name="transNature" id="transNature" disabled="disabled"
										class="validate[required] width51">
										<option value="">Select</option>
										<c:forEach var="TRANS_NATURE" items="${TRANSFER_NATURE}">
											<option value="${TRANS_NATURE.lookupDetailId}">${TRANS_NATURE.displayName}</option>
										</c:forEach>
									</select> <span class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="INTER_BANK_TRANSFER_NATURE"
										class="btn ui-state-default ui-corner-all interbank-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
										type="hidden" id="tempTransNature"
										value="${BANK_TRANSFER_INFO.lookupDetail.lookupDetailId}" />
								</div>
								<div>
									<label class="width30"><fmt:message
											key="common.description" />
									</label>
									<textarea class="width50" name="description" id="description" readonly="readonly"
										>${BANK_TRANSFER_INFO.description}</textarea>
								</div>
							</fieldset>
						</div>
						<div id="hrm" class="width48 float-right">
							<fieldset style="min-height: 175px;">
								<div style="height: 160px;">
									<div id="banktransfer_document_information"
										style="cursor: pointer; color: blue; display: none;">
										<u>Upload document here</u>
									</div>
									<div
										style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
										<span id="transferDocs"></span>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="ui-tabs-panel ui-widget-content ui-corner-bottom"
						id="tabs-2">
						<div id="hrm" class="float-left width50">
							<fieldset style="min-height: 85px;">
								<div>
									<label class="width30">Credit Account<span
										style="color: red">*</span>
									</label> <input type="text" readonly="readonly" id="creditBankAccount" 
										class="validate[required] width50"
										value="${BANK_TRANSFER_INFO.bankAccountByCreditBankAccount.accountNumber}" />
									<input type="hidden" id="creditBankAccountId" readonly="readonly"
										value="${BANK_TRANSFER_INFO.bankAccountByCreditBankAccount.bankAccountId}" />
									<input type="hidden" id="creditCombinationId" readonly="readonly"
										value="${BANK_TRANSFER_INFO.bankAccountByCreditBankAccount.combination.combinationId}" />
									<span class="button"
										style="display: none;">
										<a style="cursor: pointer;" id="creditaccount"
										class="btn ui-state-default ui-corner-all banktransfer-common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30">Debit Account<span
										style="color: red">*</span>
									</label> <input type="text" readonly="readonly" id="debitBankAccount"
										class="validate[required] width50"
										value="${BANK_TRANSFER_INFO.bankAccountByDebitBankAccount.accountNumber}" />
									<input type="hidden" id="debitBankAccountId" readonly="readonly"
										value="${BANK_TRANSFER_INFO.bankAccountByDebitBankAccount.bankAccountId}" />
									<input type="hidden" id="debitCombinationId" readonly="readonly"
										value="${BANK_TRANSFER_INFO.bankAccountByDebitBankAccount.combination.combinationId}" />
									<span class="button"
										style="display: none;">
										<a style="cursor: pointer;" id="debitaccount"
										class="btn ui-state-default ui-corner-all banktransfer-common-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
							</fieldset>
						</div>
						<div id="hrm" class="float-right width48">
							<fieldset style="min-height: 85px;">
								<div>
									<label class="width30">Transfer Amount<span
										style="color: red">*</span>
									</label> <input type="text" id="transferAmount" readonly="readonly"
										class="validate[required,custom[number]] width50"
										value="${BANK_TRANSFER_INFO.transferAmount}" />
								</div>
								<div>
									<label class="width30">Transfer Charges</label> <input
										type="text" id="transferCharges"
										class="validate[optional,custom[number]] width50" readonly="readonly"
										value="${BANK_TRANSFER_INFO.transferCharges}" />
								</div>
								<div>
									<label class="width30">Charges Account</label> <input
										type="hidden" id="combinationId"
										value="${BANK_TRANSFER_INFO.combination.combinationId}" />
									<c:choose>
										<c:when
											test="${BANK_TRANSFER_INFO.combination.accountByAnalysisAccountId ne null && BANK_TRANSFER_INFO.combination.accountByAnalysisAccountId ne null}">
											<input type="text" id="combination" readonly="readonly"
												class="width50"
												value="${BANK_TRANSFER_INFO.combination.accountByAnalysisAccountId.account}" />
										</c:when>
										<c:otherwise>
											<input type="text" id="combination" readonly="readonly"
												class="width50"
												value="${BANK_TRANSFER_INFO.combination.accountByNaturalAccountId.account}" />
										</c:otherwise>
									</c:choose>

									<span class="button"> <a style="cursor: pointer;display: none;"
										class="btn ui-state-default ui-corner-all codecombination-popup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
							</fieldset>
						</div>
						<div class="clearfix"></div>
						<div class="float-left width30" style="display: none;">
							<div style="padding:5px;">
								<label class="width40">Credit Account<span style="margin-left:3px;">:</span></label>  <span id="creditBalance"></span>
							</div>
							<div style="padding:5px;">
								<label class="width40">Debit Account <span  style="margin-left:3px;">:</span></label>  <span id="debitBalance"></span>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="clearfix"></div>
		<input type="hidden" id="defaultCurrency"
			value="${DEFAULT_CURRENCY.currencyId}" />
		
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span>
				</a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<div
		style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
		class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
		tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
		<div
			class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
			unselectable="on" style="-moz-user-select: none;">
			<span class="ui-dialog-title" id="ui-dialog-title-dialog"
				unselectable="on" style="-moz-user-select: none;">Dialog
				Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
				role="button" unselectable="on" style="-moz-user-select: none;"><span
				class="ui-icon ui-icon-closethick" unselectable="on"
				style="-moz-user-select: none;">close</span>
			</a>
		</div>
		<div id="codecombination-popup"
			class="ui-dialog-content ui-widget-content"
			style="height: auto; min-height: 48px; width: auto;">
			<div class="codecombination-result width100"></div>
			<div
				class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
				<button type="button" class="ui-state-default ui-corner-all">Ok</button>
				<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
			</div>
		</div>
	</div>
</div>
