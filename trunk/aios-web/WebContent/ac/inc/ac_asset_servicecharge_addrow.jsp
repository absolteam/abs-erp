<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><select name="chargesType" id="chargesType_${ROW_ID}"
		class="width70 chargesType">
			<option value="">Select</option>
			<c:forEach var="chargesType" items="${COST_TYPES}">
				<option value="${chargesType.lookupDetailId}">${chargesType.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;" id="ASSET_SERVICE_COST_TYPE_${ROW_ID}"
			class="btn ui-state-default ui-corner-all assetcost-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" class="width96 amount"
		id="amount_${ROW_ID}" style="text-align: right;" />
	</td>
	<td><input type="text" class="width96 linedescription"
		id="description_${ROW_ID}" style="border: 0px;" /> <input
		type="hidden" id="assetServiceChargeId_${ROW_ID}" />
	</td>
	<td style="width: 0.01%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a>
	</td>
</tr>