<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript">
var toDate = "";
$(function(){
	$('.print-call').click(function(){ 
 		toDate=$('#toDate').val();  
		window.open('show_balancesheet_reporthtm.action?toDate=' + toDate + '&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');  
		return false;
	}); 

	$(".xls-download-call").click(function(){   
 		toDate=$('#toDate').val();  
		window.open('show_balancesheet_reportxls.action?toDate=' + toDate, '_blank',
				'width=0,height=0');  
	}); 
	
	$('.pdf-download-call').click(function(){  
		toDate=$('#toDate').val();  
		window.open('showbalancesheet_reportpdf.action?toDate=' + toDate +'&format=HTML', '',
				'width=800,height=800,scrollbars=yes,left=100px');  
	});
	
	$('#toDate').datepick({
		showTrigger : '#calImg'
	});
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>balance sheet report
		</div>
		<div class="portlet-content">
			<form name="category_details" id="category_details">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="width50 float-left">
						<fieldset>
							<div class="float-left width98"> 
								<div>
									<label class="width30" for="toDate">As at</label> <input
										type="text" id="toDate" class="width50" readonly="readonly" />
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
	</div>
</div>
