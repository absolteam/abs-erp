<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
#pos-error-div {
	margin-left: 10px;
}

</style>
<script type="text/javascript">
	var slidetab = "";
	$(function() {
		
		
		/*  $jquery('#posReference').keypad({keypadOnly: false, 
		    layout: $jquery.keypad.qwertyLayout});  */

		/*  $jquery('#salesReference').keypad({ keypadOnly: false,closeText: 'Done',
			separator: '|', prompt: '', 
	    layout: ['q|w|e|r|t|y|u|i|o|p|7|8|9', 
	        'a|s|d|f|g|h|j|k|l|4|5|6' , 
	       'z|x|c|v|b|n|m|.|0|00|1|2|3' , 
	      $jquery.keypad.CLEAR+'|' + $jquery.keypad.BACK+'|'+$jquery.keypad.CLOSE],onClose: function(value, inst) { 
	        	$(this).trigger( "change" );}});  */
		
		$('#salesReference').change(function(){   
			var salesRefNumber = $(this).val();
			$('#pos-details').html("");
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_sales_details_byreference.action",
							async : false,
							dataType : "html",
							data : {
								referenceNumber : salesRefNumber
							},
							cache : false,
							success : function(result) {
								$('#pos-details').append(result);
							}
						});
				$('#salesReference').val('');
				return false;
			});
	        	
	});    
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sales
			Return
		</div>

		<form name="merchandise_return_details"
			id="merchandise_return_details">
			<div class="portlet-content" id="pos-search-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="width50 float-left">
						<div style="margin: 10px 0 0 10px;">
							<label class="width20">Sales Reference</label> <input type="text"
								name="salesReference" id="salesReference" class="width50" />
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content" id="pos-details"></div>
		</form>
	</div>
</div>