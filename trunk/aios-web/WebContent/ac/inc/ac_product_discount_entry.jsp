<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var slidetab="";
var discountMethods = "";
var discountOptions = new Object();
var accessCode = "";
	$(function(){  
		 $jquery("#product_discount_details").validationEngine('attach'); 

	manupulateLastRow();

	$('#fromDate,#toDate').datepick({
		showTrigger: '#calImg'}); 

	$('#discount-discard').click(function(){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_discount.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('destroy');		
											$('#common-popup').remove();   
											$("#main-wrapper").html(result);
										}
									});
						});

		$('.productdiscount-lookup').click(function(){
			 $('.ui-dialog-titlebar').remove(); 
			 accessCode=$(this).attr("id"); 
		        $.ajax({
		             type:"POST",
		             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
		             data:{accessCode: accessCode},
		             async: false,
		             dataType: "html",
		             cache: false,
		             success:function(result){ 
		                  $('.common-result').html(result);
		                  $('#common-popup').dialog('open');
		  				   $($($('#common-popup').parent()).get(0)).css('top',0);
		                  return false;
		             },
		             error:function(result){
		                  $('.common-result').html(result);
		             }
		         });
	       return false;
		});	

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="PRODUCT_DISCOUNT_NAME"){
				$('#discountName').html("");
				$('#discountName').append("<option value=''>Select</option>");
				loadLookupList("discountName"); 
			}  
		});
		
		$('#common-popup').dialog({
			 autoOpen: false,
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});

		$('#product-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });

		$('#customer-common-popup').live('click',function(){  
			   $('#common-popup').dialog('close'); 
		   });
		$('#discountOption').change(function(){
			var discountOption = $(this).val(); 
			$("#view_option").hide();
			if(discountOption!=null && discountOption!= ''){
				$(".common-result").html('');
				$('.ui-dialog-titlebar').remove();
				discountOptions = [];
				var actionName = "";
				if(discountOption == 1)
					actionName = "show_product_common_popup";
				else if(discountOption == 2)
					actionName = "show_product_multiple_popup";
				else if(discountOption == 3)
					actionName = "show_all_category_popup";
				else if(discountOption == 4)
					actionName = "show_all_sub_category_popup";
				else if(discountOption == 5)
					actionName = "common_multiselect_customer_list";
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/"+actionName+".action",
											async : false,
											dataType : "html",
											data : {rowId: Number(0), itemType: 'I'},
											cache : false,
											success : function(result) { 
												$(".common-result").html(result);
												$('#common-popup').dialog('open');
												$($($('#common-popup').parent()).get(0)).css('top',0);
											}
										});
							}
							return false;
						});

		$('.addrows')
				.click(
						function() {
							var i = Number(1);
							var id = Number(getRowId($(".tab>.rowid:last")
									.attr('id')));
							id = id + 1;
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/product_discount_addrow.action", 
				 	async: false,
				 	data:{rowId: id},
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tab tr:last').before(result);
						 if($(".tab").height()>255)
							 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
						 $('.rowid').each(function(){   
				    		 var rowId=getRowId($(this).attr('id')); 
				    		 $('#lineId_'+rowId).html(i);
							 i=i+1; 
				 		 }); 
					}
				});
			  return false;
		 }); 

		 $('.delrow').live('click',function(){ 
			 slidetab = $(this).parent().parent().get(0);  
	       	 $(slidetab).remove();  
	       	 var i=1;
	    	 $('.rowid').each(function(){   
	    		 var rowId = getRowId($(this).attr('id')); 
	    		 $('#lineId_'+rowId).html(i);
				 i=i+1; 
	 		 });   
		 });

		 if(Number($('#discountId').val()) > 0) {
			 $('#discountType').val($('#tempdiscountType').val()); 
			 $('#discountOnPrice').val($('#tempdiscountOnPrice').val());
			 $('#status').val($('#tempstatus').val());
			 $('#minimumSales').val($('#tempminimumSales').val());
			 $('#discountOption').val($('#tempdiscountOption').val());
			 var jsonData = [];
			 $('.discountOptions')
				.each(
						function() {
							var rowId = getRowId($(this).attr('id'));
							jsonData.push({
								"recordId" : Number($('#recordbyid_' + rowId).val()),
								"recordName" : $('#recordbynameid_' + rowId).val(),
								"entityName" : $('#recordbytableid_' + rowId).val()
							}); 
				 });
			 
			 if('${PRODUCT_DISCOUNT.status}' == "true")
				 $('#status').val(1);
			 else
				 $('#status').val(0);
			 $('#view_option').show();
			 var selectiontitle = "";
			if($('#discountOption').val() == '1')
				selectiontitle = "Single Product Discount";
			else if($('#discountOption').val() == '2')
				selectiontitle = "Multiple Product Discount";
			else if($('#discountOption').val() == '5')
				selectiontitle = "Customer Discount";
			var jsonObject = {
				"selectiontitle" : selectiontitle,
				"record" : jsonData
			};
			 setJsonDiscountData(jsonObject);
		 }

		 $('#discount_option_popup').dialog({
		 		autoOpen: false,
		 		width: 800,
		 		height: 400,
		 		bgiframe: true,
		 		modal: true,
		 		buttons: {
		 			"OK": function(){  
		 				$(this).dialog("close"); 
		 			} 
		 		}
		 	});

		 $('#view_option').click(function(){   
			 $('.ui-dialog-titlebar').remove();
				var htmlString = "";
							htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>"
									+ discountOptions.selectiontitle + "</div>";
									var count = 0;
							
				$(discountOptions.record)
									.each(
											function(index) {
												count += 1;
												htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
														+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
														+ count
														+ ".</span>"
														+ "<span class='width20 float-left'>"
														+ discountOptions.record[index].recordName
														+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
											});

							$('#discount_option_popup').dialog('open');

							$('#discount_option_popup').html(htmlString);
						});

		 $('.deleteoption')
			.live('click',
					function() {
				var rowId = getRowId($(this).attr('id')); 
  				var count_index =  $('#countindex_'+rowId).attr('id'); 
				$('#div_'+rowId).remove(); 
				discountOptions.record.splice(count_index.split('.')[0], 1); 
				var i = 0;
				$('.count_index')
				.each(
						function() {
							i = i +1;
					$(this).text(i+".");		
				});
				if(discountOptions.record.length == 0){
					$('#view_option').hide();
					$('#discountOption').val('');
					$('#discount_option_popup').dialog("close");
				} 
				return false;
		 }); 

		$('#discount-save')
				.click(
						function() {

							 if($jquery("#product_discount_details").validationEngine('validate')){
								if(discountOptions!=null && discountOptions!= ''){  
								var discountId = Number($(
										'#discountId').val());
								var discountName = $('#discountName').val();
								 
								var discountOnPrice = Number($('#discountOnPrice').val());
								var status = Number($('#status').val());
								 
								var fromDate = $('#fromDate').val();
								var toDate = $('#toDate').val();
								var minimumSales = $('#minimumSales').val();
								var minimumValue = $('#minimumValue').val();
								var maxLimit = $('#maxLimit').val();
								var discountOption = Number($(
										'#discountOption').val());
								var description = $('#description').val();
								discountMethods = getDiscountMethods(); 
								
 								if (discountMethods != null
										&& discountMethods != "") { 
									$
											.ajax({
												type : "POST",
												url : "<%=request.getContextPath()%>/save_product_discount.action", 
						 	async: false,
						 	data : { discountId: discountId, discountName: discountName, discountOnPrice: discountOnPrice, 
						 			 status: status, fromDate: fromDate, toDate: toDate, minimumSales: minimumSales,
						 			 minimumValue: minimumValue, maxLimit: maxLimit, discountOption: discountOption, description: description,
						 			 discountMethods: discountMethods, aaData: JSON.stringify(discountOptions)
							 	   },
						    dataType: "json",
						    cache: false,
							success:function(response){
								if(response.returnMessage == "SUCCESS"){
									$('#common-popup').dialog('destroy');		
									$('#common-popup').remove();   
									 $.ajax({
											type:"POST",
											url:"<%=request.getContextPath()%>/show_product_discount.action",
																		async : false,
																		dataType : "html",
																		cache : false,
																		success : function(
																				result) {
																			$(
																					"#main-wrapper")
																					.html(
																							result);
																			if (discountId > 0)
																				$(
																						'#success_message')
																						.hide()
																						.html(
																								"Record updated")
																						.slideDown(
																								1000);
																			else
																				$(
																						'#success_message')
																						.hide()
																						.html(
																								"Record created")
																						.slideDown(
																								1000);
																			$(
																					'#success_message')
																					.delay(
																							3000)
																					.slideUp();
																		}
																	});
														} else {
															$('#page-error')
																	.hide()
																	.html(
																			response.returnMessage)
																	.slideDown(
																			1000);
															$('#page-error')
																	.delay(3000)
																	.slideUp();
															return false;
														}
													},
													error : function(result) {
														$('#page-error')
																.hide()
																.html(
																		"Save failure")
																.slideDown(1000);
														$('#page-error').delay(
																3000).slideUp();
													}
												});
									} else {
										$('#page-error')
												.hide()
												.html(
														"Please enter discount methods.")
												.slideDown(1000);
										$('#page-error').delay(3000).slideUp();
										return false;
									}
								} else {
									$('#page-error').hide().html(
											"Please enter discount option.")
											.slideDown(1000);
									$('#page-error').delay(3000).slideUp();
									return false;
								}
							} else {
								return false;
							}
						});

		var getDiscountMethods = function() { 
			discountMethods = ""; 
			var flatpercentageArray = new Array();
			var flatamountArray = new Array(); 
			var methodIdArray = new Array();
			if($('#percentage').attr('checked')){ 
				flatpercentageArray.push(Number($('#discountValue').val())); 
				flatamountArray.push(Number(0));
			}else{
				flatamountArray.push(Number($('#discountValue').val()));
				flatpercentageArray.push(Number(0));
			} 
			var methodId = Number($(
					'#discountMethodId_1').val());
			methodIdArray.push(methodId);
			for ( var j = 0; j < flatpercentageArray.length; j++) {
				discountMethods += flatpercentageArray[j] + "__"
						+ flatamountArray[j]+ "__"+methodIdArray[j];
				if (j == flatpercentageArray.length - 1) {
				} else {
					discountMethods += "#@";
				}
			} 
			return discountMethods;
		};
	});

	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function triggerAddRow(rowId) {
		var salesamount = $('#salesamount_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (salesamount != null && salesamount != "" && salesamount != null
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function selectedCustomerList(customerData) {  
		var jsonData = [];
		$(customerData)
		.each(
				function(index) {
					jsonData.push({ 
						"recordId" : Number(customerData[index].customerId),
						"recordName" : customerData[index].customerName,
						"entityName" : "Customer"
					}); 
				}); 
		var jsonObject = {
				"selectiontitle" : "Customer Discount",
				"record" : jsonData
			};
		setJsonDiscountData(jsonObject);
	}
	
	function commonProductPopup(productId, productName, rowId) { 
		var jsonObject = {
			"selectiontitle" : "Single Product Discount",
			"record" : [ {
				"recordId" : Number(productId),
				"recordName" : productName,
				"entityName" : "Product"
			} ]
		};
		setJsonDiscountData(jsonObject);
	}

	function setJsonDiscountData(jsonData) {
		discountOptions = jsonData;
		if (discountOptions.record.length > 0)
			$('#view_option').show();
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Product Discount
		</div> 
		<form name="product_discount_details" id="product_discount_details" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="discountId" name="discountId"
					value="${PRODUCT_DISCOUNT.discountId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 195px;">
							<div>
								<label class="width30" for="discountOnPrice">Price Based </label> <select name="discountOnPrice"
									id="discountOnPrice" class="width51">
									<option value="">Select</option>
									<c:forEach var="discountOnPrice" items="${DISCOUNT_ON_PRICE}">
										<option value="${discountOnPrice.lookupDetailId}">${discountOnPrice.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempdiscountOnPrice"
									value="${PRODUCT_DISCOUNT.lookupDetail.lookupDetailId}">
							</div> 
							<div>
								<label class="width30" for="minimumSales">Minimum Sales</label>
								<select name="minimumSales" id="minimumSales" class="width51">
									<option value="">Select</option>
									<option value="Q">Quantity</option>
									<option value="A">Amount</option>
								</select> <input type="hidden" id="tempminimumSales"
									value="${PRODUCT_DISCOUNT.minimumSales}" />
							</div>
							<div>
								<label class="width30" for="minimumValue">Minimum Value</label>
								<input type="text" name="minimumValue" id="minimumValue"
									value="${PRODUCT_DISCOUNT.minimumValue}" class="width50" />
							</div>
							<div>
								<label class="width30" for="maxLimit">Max Discount</label> <input
									type="text" name="maxLimit" id="maxLimit" class="width50"
									value="${PRODUCT_DISCOUNT.maxDiscountLimit}" />
							</div>
							<div>
								<label class="width30" for="status">Status </label> 
									<select name="status" id="status"
									class="width51 validate[required]"> 
									<option value="1" selected="selected">Active</option>
									<option value="0">Inactive</option>
								</select> <input type="hidden" id="tempstatus"
									value="${PRODUCT_DISCOUNT.status}">
							</div> 
							<div>
								<label class="width30">Description</label>
								<textarea id="description" name="description" class="width50">${PRODUCT_DISCOUNT.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 195px;">
							<div>
								<label class="width30" for="discountName">Discount Name
									<span class="mandatory">*</span> </label>  
								<input type="text" name="discountName" id="discountName" class="width50 validate[required]" 
									value="${PRODUCT_DISCOUNT.discountName}" />
							</div>
							<div style="display: none;">
								<label class="width30" for="discountType">Discount Type<span
									class="mandatory">*</span> </label> <select name="discountType"
									id="discountType" class="width51">
									<option value="">Select</option>
									<c:forEach var="discountType" items="${DISCOUNT_METHODS}">
										<option value="${discountType.key}">${discountType.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempdiscountType"
									value="${PRODUCT_DISCOUNT.discountType}">
							</div> 
							<div>
								<div>
									<label class="width30" for="fromDate">From Date<span class="mandatory">*</span>  </label>
									<c:choose>
										<c:when
											test="${PRODUCT_DISCOUNT.fromDate ne null && PRODUCT_DISCOUNT.fromDate ne ''}">
											<c:set var="fromDate" value="${PRODUCT_DISCOUNT.fromDate}" />
											<%
												String fromDate = DateFormat
																.convertDateToString(pageContext.getAttribute(
																		"fromDate").toString());
											%>
											<input type="text" readonly="readonly" name="fromDate"
												id="fromDate" class="width50 validate[required]" value="<%=fromDate%>" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="fromDate"
												id="fromDate" class="width50 validate[required]" />
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30" for="toDate">To Date <span class="mandatory">*</span> </label>
									<c:choose>
										<c:when
											test="${PRODUCT_DISCOUNT.toDate ne null && PRODUCT_DISCOUNT.toDate ne ''}">
											<c:set var="toDate" value="${PRODUCT_DISCOUNT.toDate}" />
											<%
												String toDate = DateFormat.convertDateToString(pageContext
																.getAttribute("toDate").toString());
											%>
											<input type="text" readonly="readonly" name="toDate"
												id="toDate" class="width50 validate[required]" value="<%=toDate%>" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="toDate"
												id="toDate" class="width50 validate[required]" />
										</c:otherwise>
									</c:choose>
								</div>
								<div>
									<label class="width30" for="discountOption">Discount
										Option<span class="mandatory">*</span> </label> <select
										name="discountOption" id="discountOption"
										class="width50 validate[required] float-left">
										<option value="">Select</option>
										<c:forEach var="discountOption" items="${DISCOUNT_OPTIONS}">
											<option value="${discountOption.key}">${discountOption.value}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempdiscountOption"
										value="${PRODUCT_DISCOUNT.discountOption}" /> 
										<span style="cursor: pointer; position: relative; top: 8px; display: none; float: left;
												width:17px; height:17px; background-image: url('./images/icons/view.ico');background-repeat: no-repeat;" id="view_option"></span>
								</div>
								<div>
									<label class="width30" for="discountValue">Discount <span class="mandatory">*</span> </label>
									<c:set var="flatType" value="0"/>
									<c:forEach var="flatvalue" items="${PRODUCT_DISCOUNT.discountMethods}">
										<c:choose>
											<c:when test="${flatvalue.flatPercentage ne null && flatvalue.flatPercentage gt 0}">
												<input type="text" id="discountValue" name="discountValue" 
													class="validate[required,custom[number]] width50" value="${flatvalue.flatPercentage}"/>
												<c:set var="flatType" value="1"/>
											</c:when>
											<c:when test="${flatvalue.flatAmount ne null && flatvalue.flatAmount gt 0}">
												<input type="text" id="discountValue" name="discountValue"
													 class="validate[required,custom[number]] width50" value="${flatvalue.flatAmount}"/>
											</c:when> 
										</c:choose>
									</c:forEach>
									<c:if test="${PRODUCT_DISCOUNT.discountId eq null || PRODUCT_DISCOUNT.discountId eq 0}">
										<input type="text" id="discountValue" name="discountValue" 
													class="validate[required,custom[number]] width50"/>
									</c:if>
								</div> 
								<div>
									<label class="width30" for="percentage">Percentage</label>
									<c:choose>
										<c:when test="${flatType eq 0}">
											<input type="checkbox" id="percentage" name="percentage"/>
										</c:when>
										<c:otherwise>
											<input type="checkbox" id="percentage" name="percentage" checked="checked"/>
										</c:otherwise>
									</c:choose> 
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top: 10px; display: none;" id="hrm">
				<fieldset>
					<legend>Discount method<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<input type="hidden" name="childCount" id="childCount"
							value="${fn:length(PRODUCT_DISCOUNT.discountMethods)}" />
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Flat(%)</th>
									<th style="width: 5%">Flat(Amount)</th>
									<th style="width: 5%">Sales Amount</th>
									<th style="width: 5%; display: none;">Discount Amount</th>
									<th style="width: 5%; display: none;">Discount(%)</th>
									<th style="width: 5%">Description</th>
									<th style="width: 0.01%;"><fmt:message
											key="accounts.common.label.options" />
									</th>
								</tr>
							</thead>
							<tbody class="tab">
								<c:forEach var="discountMethods"
									items="${PRODUCT_DISCOUNT.discountMethods}" varStatus="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
										<td><input type="text"
											class="width96 flatpercentage validate[optional,custom[number]]"
											id="flatpercentage_${status.index+1}"
											value="${discountMethods.flatPercentage}"
											 /></td>
										<td><input type="text"
											class="width96 flatamount validate[optional,custom[number]]"
											id="flatamount_${status.index+1}"
											value="${discountMethods.flatAmount}"  />
										</td>
										<td><input type="text"
											class="width96 salesamount validate[optional,custom[number]]"
											id="salesamount_${status.index+1}"
											value="${discountMethods.salesAmount}"
											style=" text-align: right;" /></td>
										<td style="display: none;"><input type="text"
											class="width96 discountamount validate[optional,custom[number]]"
											id="discountamount_${status.index+1}"
											value="${discountMethods.discountAmount}"
											style=" text-align: right;" /></td>
										<td style="display: none;"><input type="text"
											class="width96 discountpercentage validate[optional,custom[number]]"
											id="discountpercentage_${status.index+1}"
											value="${discountMethods.discountPercentage}"
											style=" text-align: right;" /></td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status.index+1}" class="width96"
											value="${discountMethods.description}" /></td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status.index+1}" style="cursor: pointer;"
											title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" id="discountMethodId_${status.index+1}"
											value="${discountMethods.discountMethodId}" /></td>
									</tr>
								</c:forEach>
								<c:forEach var="i"
									begin="${fn:length(PRODUCT_DISCOUNT.discountMethods)}"
									end="${fn:length(PRODUCT_DISCOUNT.discountMethods)+1}" step="1"
									varStatus="status1">
									<tr class="rowid" id="fieldrow_${status1.index+1}">
										<td id="lineId_${status1.index+1}" style="display: none;">${status1.index+1}</td>
										<td><input type="text"
											class="width96 flatpercentage validate[optional,custom[number]]"
											id="flatpercentage_${status1.index+1}"  />
										</td>
										<td><input type="text"
											class="width96 flatamount validate[optional,custom[number]]"
											id="flatamount_${status1.index+1}"  /></td>
										<td><input type="text"
											class="width96 salesamount validate[optional,custom[number]]"
											id="salesamount_${status1.index+1}"
											style=" text-align: right;" /></td>
										<td style="display: none;"><input type="text"
											class="width96 discountamount validate[optional,custom[number]]"
											id="discountamount_${status1.index+1}"
											style=" text-align: right;" /></td>
										<td style="display: none;"><input type="text"
											class="width96 discountpercentage validate[optional,custom[number]]"
											id="discountpercentage_${status1.index+1}"
											style=" text-align: right;" /></td>
										<td><input type="text" name="linedescription"
											id="linedescription_${status1.index+1}" class="width96" /></td>
										<td style="width: 0.01%;" class="opn_td"
											id="option_${status1.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
											id="AddImage_${status1.index+1}"
											style="display: none; cursor: pointer;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
											id="EditImage_${status1.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
											id="DeleteImage_${status1.index+1}"
											style="cursor: pointer; display: none;" title="Delete Record">
												<span class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImage_${status1.index+1}" style="display: none;"
											title="Working"> <span class="processing"></span> </a> <input
											type="hidden" id="discountMethodId_${status1.index+1}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right "
					id="discount-discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="discount-save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<c:if
				test="${PRODUCT_DISCOUNT.discountOptions ne null && fn:length(PRODUCT_DISCOUNT.discountOptions) > 0}">
				<c:forEach var="discountOption"
					items="${PRODUCT_DISCOUNT.discountOptions}" varStatus="statusIndex">
					<span class="discountOptions"
						id="discountOptions_${statusIndex.index+1}" style="display: none;">
						<c:choose>
							<c:when
								test="${discountOption.product ne null && discountOption.product ne ''}">
								<input type="hidden" id="recordbyid_${statusIndex.index+1}"
									value="${discountOption.product.productId}" />
								<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
									value="${discountOption.product.productName}" />
								<input type="hidden" id="recordbytableid_${statusIndex.index+1}"
									value="Product" />
							</c:when>
							<c:when
								test="${discountOption.customer ne null && discountOption.customer ne ''}">
								<input type="hidden" id="recordbyid_${statusIndex.index+1}"
									value="${discountOption.customer.customerId}" />
								<c:if
									test="${discountOption.customer.personByPersonId ne null && discountOption.customer.personByPersonId ne ''}">
									<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
										value="${discountOption.customer.personByPersonId.firstName} ${discountOption.customer.personByPersonId.lastName}" />
								</c:if>
								<c:if
									test="${discountOption.customer.company ne null && discountOption.customer.company ne ''}">
									<input type="hidden" id="recordbynameid_${statusIndex.index+1}"
										value="${discountOption.customer.company.companyName}" />
								</c:if>
								<input type="hidden" id="recordbytableid_${statusIndex.index+1}"
									value="Customer" />
							</c:when>
						</c:choose> </span>
				</c:forEach>
			</c:if>
			<div style="display: none;" id="discount_option_popup"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>