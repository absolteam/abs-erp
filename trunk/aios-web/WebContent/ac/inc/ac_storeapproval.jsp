<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
#DOMWindow {
	width: 95% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}
</style>
<script type="text/javascript">
var aisleDetails = "";
var binDetails = "";
var slidetab = "";
var rackDetails = "";
var sectionRowId = 0;
var accessCode = "";
$(function(){
	  
	$('input,select').attr('disabled', true);
	
	$('.addBins').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	});
	
 
	$('.closeDom').live('click',function(){
		var sectionReferenceId = Number($('#sectionReferenceId').val()); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/store_bin_discard.action", 
		 	async: false,
		 	data:{sectionReferenceId: sectionReferenceId},
		    dataType: "json",
		    cache: false,
			success:function(response){
				if(response.returnMessage == "SUCCESS"){
					 $('#DOMWindow').remove();
					 $('#DOMWindowOverlay').remove();
				}else{
					$('#bin-page-error').hide().html(response.returnMessage).slideDown();
					$('#bin-page-error').delay(2000).hide();
				} 
			}
		 }); 
		return false;
	 });

	 

	 if(Number($('#storeId').val())>0){
			$('#storageType').val($('#tempStorageType').val());
			 var rowId= 0;
			 $('.rowid').each(function(){   
		   		 rowId = getRowId($(this).attr('id')); 
		   		 $('#storageSection_'+rowId).val($('#tempStorageSection_'+rowId).val()); 
			 }); 
			
			$('#binType').val($('#tempBinType').val());
		}
		
	$('.aisleNumber').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});
	
	$('.storageSection').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});
	
	$('.dimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});

	$('.binSection').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerBinAddRow(rowId);
	});

	$('.bindimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerBinAddRow(rowId);
	});

	$('.rackType').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerRackAddRow(rowId);
	});

	$('.rackdimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerRackAddRow(rowId);
	});
	 
	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	   	return false; 
	 });

	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	$('.store-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action",
										async : false,
										dataType : "html",
										data:{personTypes: "1"},
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});


	 $('#department-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });
	 
	$('.locationstore-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			modal : true
		});
	});
	
	 
	
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	
	 

	function getBinDetails() {
		binDetails = "";
		var binSectionArray = new Array();
		var sectionNameArray = new Array();
		var bindimensionArray = new Array();
		var confirmSaveArray = new Array();
		var shelfidArray = new Array();
		var referenceIdArray = new Array();
		var aisleidArray = new Array();
		$('.binrowid').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var binSection = $('#binSection_' + rowId).val();
					var bindimension = $('#bindimension_' + rowId).val();
					var shelfid = Number($('#shelfid_' + rowId).val());
					var confirmSave = $('#confirmSave_' + rowId).val();
					var referenceId = $('#binlineId_' + rowId).text();
					var aisleId =  Number($('#shelfaisleId_' + rowId).val());
					var binSectionName = $('#binSectionName_' + rowId).val();
					if (typeof binSection != 'undefined' && binSection != null
							&& binSection != "" && bindimension != ""
							&& bindimension != "") {
						binSectionArray.push(binSection);
						bindimensionArray.push(bindimension);
						confirmSaveArray.push(confirmSave);
						referenceIdArray.push(referenceId);
						shelfidArray.push(shelfid);
						aisleidArray.push(aisleId);
						sectionNameArray.push(binSectionName);
					}
				});
		for ( var j = 0; j < binSectionArray.length; j++) {
			binDetails += binSectionArray[j] + "__" + bindimensionArray[j]
					+ "__" + confirmSaveArray[j] + "__" + referenceIdArray[j]
					+ "__" + shelfidArray[j] + "__" + aisleidArray[j]+"__"+sectionNameArray[j];
			if (j == binSectionArray.length - 1) {
			} else {
				binDetails += "#@";
			}
		}
		return binDetails;
	}

	function getRackDetails() {
		rackDetails = "";
		var rackTypeArray = new Array();
		var rackdimensionArray = new Array();
		var rackidArray = new Array();
		var aisleidArray = new Array();
		var shelfNameArray = new Array();
		$('.rackrowid').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var rackType = $('#rackType_' + rowId).val();
					var rackdimension = $('#rackdimension_' + rowId).val();
					var rackid = Number($('#rackid_' + rowId).val());
					var aisleId =  Number($('#rackaisleId_' + rowId).val());
					var shelfName = $('#shelfName_' + rowId).val();
					if (typeof rackType != 'undefined' && rackType != null
							&& rackType != "" && rackdimension != null
							&& rackdimension != "") {
						rackTypeArray.push(rackType);
						rackdimensionArray.push(rackdimension);
						rackidArray.push(rackid);
						aisleidArray.push(aisleId);
						shelfNameArray.push(shelfName);
					}
				});
		for ( var j = 0; j < rackTypeArray.length; j++) {
			rackDetails += rackTypeArray[j] + "__" + rackdimensionArray[j]
					+ "__" + rackidArray[j] + "__" + aisleidArray[j] +"__" +shelfNameArray[j];
			if (j == rackTypeArray.length - 1) {
			} else {
				rackDetails += "#@";
			}
		}
		return rackDetails;
	}

	 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Stores
		</div> 
		<form name="storeValidation" id="storeValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="store-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="storeId" name="storeId"
					value="${STORE_INFO.storeId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48">
						<fieldset style="min-height: 120px;"> 
							<div>
								<label class="width30" for="locationId">Location<span
									style="color: red;">*</span> </label> <input type="hidden"
									id="locationId" name="locationId"
									value="${STORE_INFO.cmpDeptLocation.cmpDeptLocId}" />
								<c:choose>
									<c:when test="${STORE_INFO.cmpDeptLocation ne null && 
										STORE_INFO.cmpDeptLocation.cmpDeptLocId > 0}">
										<input type="text" readonly="readonly"
											value="${STORE_INFO.cmpDeptLocation.department.departmentName} &gt; ${STORE_INFO.cmpDeptLocation.location.locationName}"
											name="locationName" id="locationName"
											class="width50 validate[required]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="locationName"
											id="locationName" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
								<span class="button"> <a style="cursor: pointer; display: none;"
									id="person"
									class="btn ui-state-default ui-corner-all locationstore-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="address">Address</label>
								<textarea class="width50" id="address" name="address">${STORE_INFO.address}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30" for="storeNumber"> Store number<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${STORE_INFO.storeId ne null && STORE_INFO.storeId ne ''}">
										<input name="storeNumber" type="text" readonly="readonly"
											id="storeNumber" value="${STORE_INFO.storeNumber}"
											class="width50 validate[required]">
									</c:when>
									<c:otherwise>
										<input name="storeNumber" type="text" readonly="readonly"
											id="storeNumber" value="${STORE_NUMBER}"
											class="width50 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="address">Store Name<span
									style="color: red;">*</span></label> <input
									type="text" id="storeName" name="storeName"
									value="${STORE_INFO.storeName}" class="width50 validate[required]" />
							</div>
							<div>
								<label class="width30" for="storageType"> Storage Type </label> <select name="storageType"
									id="storageType" class="width51">
									<option value="">Select</option>
									<c:forEach var="STORAGE" items="${STORAGE_TYPES}">
										<option value="${STORAGE.lookupDetailId}">${STORAGE.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStorageType"
									value="${STORE_INFO.lookupDetail.lookupDetailId}" /> <span
									class="button" style="position: relative;display: none;"> <a
									style="cursor: pointer;" id="STORAGE_TYPE"
									class="btn ui-state-default ui-corner-all store-storage-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="personId">Person<span
									style="color: red;">*</span> </label> 
								<c:choose>
									<c:when test="${STORE_INFO.person ne null && STORE_INFO.person.personId > 0}">
										<input type="text" readonly="readonly"
											value="${STORE_INFO.person.firstName} ${STORE_INFO.person.lastName}"
											name="personNumber" id="personNumber"
											class="width50 validate[required]" />
											<input type="hidden" id="personId" name="personId"
												value="${STORE_INFO.person.personId}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="personNumber"
											id="personNumber" class="width50 validate[required]" value="${requestScope.personName}"/>
										<input type="hidden" id="personId" name="personId"
												value="${requestScope.personId}" />
									</c:otherwise>
								</c:choose>
								<span class="button"> <a style="cursor: pointer;display: none;"
									id="person"
									class="btn ui-state-default ui-corner-all store-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="hrm" class="portlet-content width100"
					style="margin-top: 10px;">
					<fieldset id="hrm">
						<legend>Section Information<span
									style="color: red;">*</span></legend>
						<div class="portlet-content">
							<div id="page-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="childCount" id="childCount"
									value="${fn:length(AISLE_LINE_INFO)}" />
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th style="width: 3%">Reference No</th>
											<th style="width: 5%">Name</th>
											<th style="width: 5%">Section Type</th>
											<th style="width: 3%">Dimension (l*b*h)</th> 
											<th style="width: 0.3%;display: none;">Options</th>
										</tr>
									</thead>
									<tbody class="tab">
										 
										<c:choose>
											<c:when
												test="${requestScope.AISLE_LINE_INFO ne null && requestScope.AISLE_LINE_INFO ne '' && fn:length(AISLE_LINE_INFO)>0}">
												<c:forEach var="bean" items="${AISLE_LINE_INFO}"
													varStatus="status">
													<tr class="rowid" id="fieldrow_${status.index+1}">
														<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
														<td><input type="text" maxlength="8"
															class="width90 aisleNumber" value="${bean.aisleNumber}"
															name="aisleNumber" id="aisleNumber_${status.index+1}" />
														</td>
														<td><input type="text" class="width90 sectionName" value="${bean.sectionName}"
															name="sectionName" id="sectionName_${status.index+1}" />
														</td>
														<td><select name="storageSection"
															id="storageSection_${status.index+1}" class="width80">
																<option value="">Select</option>
																<c:forEach var="SECTION" items="${STORAGE_SECTIONS}">
																	<option value="${SECTION.lookupDetailId}">${SECTION.displayName}</option>
																</c:forEach>
														</select> <input type="hidden"
															id="tempStorageSection_${status.index+1}"
															value="${bean.lookupDetail.lookupDetailId}" /></td>
														<td><input type="text" maxlength="4"
															class="width90 dimension" value="${bean.dimension}"
															name="dimension" id="dimension_${status.index+1}" /></td>
													 
														<td style="width: 0.01%; display: none;" class="opn_td"
															id="option_${status.index+1}"><a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
															id="AddImage_${status.index+1}"
															style="display: none; cursor: pointer;"
															title="Add Record"> <span
																class="ui-icon ui-icon-plus"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
															id="EditImage_${status.index+1}"
															style="display: none; cursor: pointer;"
															title="Edit Record"> <span
																class="ui-icon ui-icon-wrench"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
															id="DeleteImage_${status.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
																class="ui-icon ui-icon-circle-close"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
															id="WorkingImage_${status.index+1}"
															style="display: none;" title="Working"> <span
																class="processing"></span> </a> <input type="hidden"
															id="aisleid_${status.index+1}" name="asileid"
															value="${bean.aisleId}" /></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose> 
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="temp-result" style="display: none;"></div> 
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>