<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style> 
#DOMWindow{	
	width:95%!important;
	height:88%!important;
} 
.ui-dialog{
	z-index: 99999!important;
}
</style>
<script type="text/javascript">
var accountTypeId=0;
var slidetab;
var tempid;
$(function(){
	 $jquery("#coavalidate-form").validationEngine('attach'); 
	//Combination pop-up config
	$('.codecombination-popup').click(function(){ 
	      tempid=$(this).parent().get(0); 
	      $('.ui-dialog-titlebar').remove();   
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/combination_treeview.action", 
			 	async: false,  
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.codecombination-result').html(result);  
				},
				error:function(result){ 
					 $('.codecombination-result').html(result); 
				}
			});  
	});
	
	$('#codecombination-popup').dialog({
		autoOpen: false,
		minwidth: 'auto',
		width:800, 
		bgiframe: false,
		modal: false
	}); 
	 
	 $('.discard').click(function(){
		 $.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_setup_wizard.action",  
	     	async: false, 
			dataType: "html",
			cache: false,
			success: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);
			},
			error: function(result){ 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$('#codecombination-popup').dialog('destroy');		
		  		$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
			}
		});
		 return false;
	 }); 
	 
	 $('.save').click(function(){ 
		 $('.error').hide();
		 var ownersEquityId=Number($('#ownersEquityId').val());
		 var clearingAccountControlId = Number($('#clearingAccountControlId').val());
		 var accountsPayableControlId = Number($('#accountsPayableControlId').val());
		 var accountsReceivableControlId = Number($('#accountsReceivableControlId').val());
		 var productionExpenseControlId = Number($('#productionExpenseControlId').val());
		 var productionRevenueControlId = Number($('#productionRevenueControlId').val());
		 var accumulatedDeprControlId = Number($('#accumulatedDeprControlId').val());
		 var unearnedRevenueId=Number($('#unearnedRevenueId').val());
		 var pdcReceivedId=Number($('#pdcReceivedId').val());
		 var pdcIssuedId= Number($('#pdcIssuedId').val()); 
		 var cashAccountDPControlId = Number($('#cashAccountDPControlId').val()); 
		 var salesDiscountControlId = Number($('#salesDiscountControlId').val());  
		 var pettyCashControlId = Number($('#pettyCashControlId').val());  
		 var bankClearingControlId = Number($('#bankClearingControlId').val());   
		 if(ownersEquityId==0 && clearingAccountControlId == 0 && accountsPayableControlId == 0 && unearnedRevenueId == 0
				 && pdcReceivedId == 0 && pdcIssuedId == 0 && accountsReceivableControlId == 0 && productionExpenseControlId == 0
				 && productionRevenueControlId == 0 && accumulatedDeprControlId == 0 && cashAccountDPControlId == 0 && salesDiscountControlId == 0 &&
				 pettyCashControlId == 0){
			 $('.discard').trigger('click');
			 return false;
		} 
 		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/save_customise_generalaccounts.action",
			data:{ ownersEquityId: ownersEquityId, clearingAccount: clearingAccountControlId, accountsPayableId: accountsPayableControlId,
				   unearnedRevenue: unearnedRevenueId, pdcReceived: pdcReceivedId, pdcIssued: pdcIssuedId, pettyCashControlId: pettyCashControlId,
				   accountsReceivableControlId: accountsReceivableControlId, productionExpenseControlId: productionExpenseControlId, 
				   productionRevenueControlId: productionRevenueControlId, accumulatedDeprControlId: accumulatedDeprControlId,
				   salesDiscountControlId: salesDiscountControlId, cashAccountDPControlId: cashAccountDPControlId, bankClearingControlId: bankClearingControlId
				   },
		 	async: false,
		    dataType: "json",
		    cache: false,
			success:function(response){ 
				var message=response.sqlReturnMessage; 
				if(message=="Success"){  
					 $.ajax({
						type: "POST", 
						url: "<%=request.getContextPath()%>/show_setup_wizard.action", 
				     	async: false, 
						dataType: "html",
						cache: false,
						success: function(result){  
							$('#DOMWindow').remove();
							$('#DOMWindowOverlay').remove();
							$('#codecombination-popup').dialog('destroy');		
					  		$('#codecombination-popup').remove(); 
							$("#main-wrapper").html(result);
							$('#success_message').hide().html(message).slideDown(1000);
							$.scrollTo(0,300);
						} 		
					});  
				 }
				else{
					$('.page-error').hide().html($('#returnMsg').html()).slideDown(1000);
					return false;
				}
			}
		}); 
	 });
	 return false;
});
function setCombination(combinationTreeId, combinationTree){  
	tempid=$(tempid).attr('id');  
	if(typeof tempid!='undefined'&& tempid=="ownersEquityCode"){
		$('#ownersEquityId').val(combinationTreeId); 
		$('#ownersEquity').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="clearingAccountControlCode"){
		$('#clearingAccountControlId').val(combinationTreeId); 
		$('#clearingAccountControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountsPayableControlCode"){
		$('#accountsPayableControlId').val(combinationTreeId); 
		$('#accountsPayableControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountReceivableControlCode"){
		$('#accountsReceivableControlId').val(combinationTreeId); 
		$('#accountsReceivableControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountProductionExpenseControlCode"){
		$('#productionExpenseControlId').val(combinationTreeId); 
		$('#productionExpenseControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accountProductionRevenueControlCode"){
		$('#productionRevenueControlId').val(combinationTreeId); 
		$('#productionRevenueControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="accumulatedDeprControlCode"){
		$('#accumulatedDeprControlId').val(combinationTreeId); 
		$('#accumulatedDeprControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="unearnedRevenueCode"){
		$('#unearnedRevenueId').val(combinationTreeId); 
		$('#unearnedRevenue').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="pdcReceivedCode"){
		$('#pdcReceivedId').val(combinationTreeId); 
		$('#pdcReceived').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="pdcIssuedCode"){
		$('#pdcIssuedId').val(combinationTreeId); 
		$('#pdcIssued').val(combinationTree);
	}  else if(typeof tempid!='undefined'&& tempid=="accountCashAccountDPControlCode"){
		$('#cashAccountDPControlId').val(combinationTreeId); 
		$('#cashAccountDPControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="bankClearingControlCode"){
		$('#bankClearingControlId').val(combinationTreeId); 
		$('#bankClearingControl').val(combinationTree);
	}  else if(typeof tempid!='undefined'&& tempid=="pettyCashControlCode"){
		$('#pettyCashControlId').val(combinationTreeId); 
		$('#pettyCashControl').val(combinationTree);
	} else if(typeof tempid!='undefined'&& tempid=="salesDiscountControlCode"){
		$('#salesDiscountControlId').val(combinationTreeId); 
		$('#salesDiscountControl').val(combinationTree);
	} 
} 
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>General Accounts Setup</div>
		<div class="portlet-content">
			<div class="tempresult" style="display:none;"></div>
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:80% !important; display:none;"></div> 	
			<form id="coavalidate-form" name="coaentrydetails" style="position: relative;"> 
				<div class="width48 float-right" id="hrm">
					<fieldset>
					<div>
						<label class="width30">POS Clearing Account</label>
						<input type="hidden" name="clearingAccountControlId" id="clearingAccountControlId" value="${CLEARNING_ACCOUNT.combinationId}"/> 
						<c:choose>
							<c:when test="${CLEARNING_ACCOUNT.accountByCostcenterAccountId.account ne null}">
								<input type="text" name="clearingAccountControl" id="clearingAccountControl" value="${CLEARNING_ACCOUNT.accountByCostcenterAccountId.account}.${CLEARNING_ACCOUNT.accountByNaturalAccountId.account}.${CLEARNING_ACCOUNT.accountByAnalysisAccountId.account}"
									readonly="readonly" class="width48 ">
							</c:when>
							<c:otherwise>
								<input type="text" name="clearingAccountControl" id="clearingAccountControl" readonly="readonly" class="width48 ">
							</c:otherwise>
						</c:choose> 
						<span class="button" id="clearingAccountControlCode">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
					</div> 
					<div>
						<label class="width30">Accounts Receivable
						</label> <input type="hidden" name="accountsReceivableControlId"
							id="accountsReceivableControlId" value="${ACCOUNTS_RECEIVABLE.combinationId}"/> 
							<c:choose>
								<c:when test="${ACCOUNTS_RECEIVABLE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="accountsReceivableControl" id="accountsReceivableControl" value="${ACCOUNTS_RECEIVABLE.accountByCostcenterAccountId.account}.${ACCOUNTS_RECEIVABLE.accountByNaturalAccountId.account}.${ACCOUNTS_RECEIVABLE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="accountsReceivableControl" id="accountsReceivableControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="accountReceivableControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div>
					<div>
						<label class="width30">Expense
						</label> <input type="hidden" name="productionExpenseControlId"
							id="productionExpenseControlId" value="${PRODUCT_EXPENSE.combinationId}"/> 
							<c:choose>
								<c:when test="${PRODUCT_EXPENSE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="productionExpenseControl" id="productionExpenseControl" value="${PRODUCT_EXPENSE.accountByCostcenterAccountId.account}.${PRODUCT_EXPENSE.accountByNaturalAccountId.account}.${PRODUCT_EXPENSE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="productionExpenseControl" id="productionExpenseControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="accountProductionExpenseControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div> 
					<div>
						<label class="width30">Revenue
						</label> <input type="hidden" name="productionRevenueControlId"
							id="productionRevenueControlId" value="${PRODUCT_REVENUE.combinationId}"/> 
							<c:choose>
								<c:when test="${PRODUCT_REVENUE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="productionRevenueControl" id="productionRevenueControl" value="${PRODUCT_REVENUE.accountByCostcenterAccountId.account}.${PRODUCT_REVENUE.accountByNaturalAccountId.account}.${PRODUCT_REVENUE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="productionRevenueControl" id="productionRevenueControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="accountProductionRevenueControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div> 
					<div>
						<label class="width30">Sales Discount
						</label> <input type="hidden" name="salesDiscountControlId"
							id="salesDiscountControlId" value="${SALESDISCOUNT_ACCOUNT.combinationId}"/> 
							<c:choose>
								<c:when test="${SALESDISCOUNT_ACCOUNT.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="salesDiscountControl" id="salesDiscountControl" value="${SALESDISCOUNT_ACCOUNT.accountByCostcenterAccountId.account}.${SALESDISCOUNT_ACCOUNT.accountByNaturalAccountId.account}.${SALESDISCOUNT_ACCOUNT.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="salesDiscountControl" id="salesDiscountControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="salesDiscountControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div> 
					<div>
						<label class="width30">Cash Account
						</label> <input type="hidden" name="cashAccountDPControlId"
							id="cashAccountDPControlId" value="${CASH_ACCOUNTDP.combinationId}"/> 
							<c:choose>
								<c:when test="${CASH_ACCOUNTDP.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="cashAccountDPControl" id="cashAccountDPControl" value="${CASH_ACCOUNTDP.accountByCostcenterAccountId.account}.${CASH_ACCOUNTDP.accountByNaturalAccountId.account}.${CASH_ACCOUNTDP.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="cashAccountDPControl" id="cashAccountDPControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="accountCashAccountDPControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div> 
					<div>
						<label class="width30">Accumulated Depreciation
						</label> <input type="hidden" name="accumulatedDeprControlId"
							id="accumulatedDeprControlId" value="${ACCUMULATED_DEPR.combinationId}"/> 
							<c:choose>
								<c:when test="${ACCUMULATED_DEPR.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="accumulatedDeprControl" id="accumulatedDeprControl" value="${ACCUMULATED_DEPR.accountByCostcenterAccountId.account}.${ACCUMULATED_DEPR.accountByNaturalAccountId.account}.${ACCUMULATED_DEPR.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="accumulatedDeprControl" id="accumulatedDeprControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
						<span class="button" id="accumulatedDeprControlCode">
							<a style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all width100 codecombination-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div> 
					</fieldset>
				</div>
				<div class="width50 float-left" id="hrm">
					<fieldset> 
						<div>
							<label class="width30">Accounts Payable</label>
							<input type="hidden" name="accountsPayableControlId" id="accountsPayableControlId" value="${ACCOUNTS_PAYABLE.combinationId}"/> 
							<c:choose>
								<c:when test="${ACCOUNTS_PAYABLE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="accountsPayableControl" id="accountsPayableControl" value="${ACCOUNTS_PAYABLE.accountByCostcenterAccountId.account}.${ACCOUNTS_PAYABLE.accountByNaturalAccountId.account}.${ACCOUNTS_PAYABLE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									<input type="text" name="accountsPayableControl" id="accountsPayableControl" readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose> 
							
							<span class="button" id="accountsPayableControlCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">Unearned Revenue</label>
							<input type="hidden" name="unearnedRevenueId" id="unearnedRevenueId" value="${UNEARNED_REVENUE.combinationId}"/> 
							<c:choose>
								<c:when test="${UNEARNED_REVENUE.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="unearnedRevenue" id="unearnedRevenue" value="${UNEARNED_REVENUE.accountByCostcenterAccountId.account}.${UNEARNED_REVENUE.accountByNaturalAccountId.account}.${UNEARNED_REVENUE.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									<input type="text" name="unearnedRevenue" id="unearnedRevenue" readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose> 
 							<span class="button" id="unearnedRevenueCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">Owners Equity</label>
							<input type="hidden" name="ownersEquityId" id="ownersEquityId" value="${OWNERS_EQUITY.combinationId}"/> 
							<c:choose>
								<c:when test="${OWNERS_EQUITY.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="ownersEquity" id="ownersEquity" value="${OWNERS_EQUITY.accountByCostcenterAccountId.account}.${OWNERS_EQUITY.accountByNaturalAccountId.account}.${OWNERS_EQUITY.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									<input type="text" name="ownersEquity" id="ownersEquity" readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose> 
							
							<span class="button" id="ownersEquityCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">PDC Received</label>
							<input type="hidden" name="pdcReceivedId" id="pdcReceivedId" value="${PDC_RECEIVED.combinationId}"/> 
							<c:choose>
								<c:when test="${PDC_RECEIVED.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="pdcReceived" id="pdcReceived" value="${PDC_RECEIVED.accountByCostcenterAccountId.account}.${PDC_RECEIVED.accountByNaturalAccountId.account}.${PDC_RECEIVED.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									<input type="text" name="pdcReceived" id="pdcReceived" readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose> 
							<span class="button" id="pdcReceivedCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">PDC Issued</label>
							<input type="hidden" name="pdcIssuedId" id="pdcIssuedId" value="${PDC_ISSUED.combinationId}"/> 
							<c:choose>
								<c:when test="${PDC_ISSUED.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="pdcIssued" id="pdcIssued" value="${PDC_ISSUED.accountByCostcenterAccountId.account}.${PDC_ISSUED.accountByNaturalAccountId.account}.${PDC_ISSUED.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									<input type="text" name="pdcIssued" id="pdcIssued" readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>  
							<span class="button" id="pdcIssuedCode">
								<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all width100 codecombination-popup"> 
									<span class="ui-icon ui-icon-newwin"> 
									</span> 
								</a>
							</span>
						</div> 
						<div>
							<label class="width30">Bank Clearing Account
							</label> <input type="hidden" name="bankClearingControlId"
							id="bankClearingControlId" value="${BANKCLEARING_ACCOUNT.combinationId}"/> 
							<c:choose>
								<c:when test="${BANKCLEARING_ACCOUNT.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="bankClearingControl" id="bankClearingControl" value="${BANKCLEARING_ACCOUNT.accountByCostcenterAccountId.account}.${BANKCLEARING_ACCOUNT.accountByNaturalAccountId.account}.${BANKCLEARING_ACCOUNT.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="bankClearingControl" id="bankClearingControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
							<span class="button" id="bankClearingControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div> 
						<div>
							<label class="width30">PettyCash Account
							</label> <input type="hidden" name="pettyCashControlId"
							id="pettyCashControlId" value="${PETTYCASH_ACCOUNT.combinationId}"/> 
							<c:choose>
								<c:when test="${PETTYCASH_ACCOUNT.accountByCostcenterAccountId.account ne null}">
									<input type="text" name="pettyCashControl" id="pettyCashControl" value="${PETTYCASH_ACCOUNT.accountByCostcenterAccountId.account}.${PETTYCASH_ACCOUNT.accountByNaturalAccountId.account}.${PETTYCASH_ACCOUNT.accountByAnalysisAccountId.account}"
										readonly="readonly" class="width48 ">
								</c:when>
								<c:otherwise>
									 <input type="text"
								name="pettyCashControl" id="pettyCashControl"
								readonly="readonly" class="width48 ">
								</c:otherwise>
							</c:choose>   
							<span class="button" id="pettyCashControlCode">
								<a style="cursor: pointer;"
								class="btn ui-state-default ui-corner-all width100 codecombination-popup">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</div> 
					</fieldset>
				</div> 
				<div class="clearfix"></div>  
				<div class="unearned_revenue" style="font-weight:bold;"></div> 
				<div class="pdc_received" style="font-weight:bold;"></div> 
				<div class="pdc_issued" style="font-weight:bold;"></div>
				<div class="clearfix"></div>  
				 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
					<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
					<div class="portlet-header ui-widget-header float-right save" id="add"><fmt:message key="accounts.common.button.save"/></div>
				</div>  
			</form>
		</div>	
	</div>		
	<div style="display: none; position: absolute; overflow: auto; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="codecombination-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
		<div class="codecombination-result width100"></div>
		<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
 	</div>
</div>