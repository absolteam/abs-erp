<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<tr>
	<td colspan="8" class="tdidentity" id="childRowId_${rowId}">
		<div class="width48 float-right" id="hrm">
			<fieldset>					 
				<legend>Receive Detail</legend> 
					<div>
						<label class="width30">UOM<span style="color: red;">*</span></label>
						<input type="text" name="uom" id="uom" class="width40 validate[required]">
					</div>
					<div>
						<label class="width30">Receive Quantity<span style="color: red;">*</span></label>
						<input type="text" name="receiveQty" id="receiveQty" class="width40 validate[required]">
					</div>
			</fieldset>  
			<div id="othererror" class="response-msg error ui-corner-all" style="width:60%; display:none;"></div>    
		 </div>			 
		 <div class="width50 float-left" id="hrm">
				<fieldset>
					<legend>Receive Detail</legend> 
					<div>
						<label class="width20">Line Number</label>
						<input type="text" name="lineNumber" id="lineNumber" class="width40 validate[required]" readonly="readonly">
					</div>
					<div>
						<label class="width20">Item Nature</label>
						<select name="itemNature" id="itemNature" class="width40 validate[required]">
							<option value="">Select</option>
							<option value="A">Asset</option>
							<option value="G">Goods</option>
							<option value="S">Service</option>
						</select>
					</div>
					<div>
						<label class="width20">Product Name<span style="color: red;">*</span></label>
						 <input type="hidden" name="productId" id="productId"/> 
						 <input type="text" name="productName" id="productName" class="width40 validate[required] ">
						<span class="button">
							<a style="cursor: pointer;" class="btn ui-state-default ui-corner-all product-popup width100"> 
								<span class="ui-icon ui-icon-newwin"> 
								</span> 
							</a>
						</span>
					</div> 
				</fieldset> 
		 </div>  
 		 <input type="hidden" name="codeCombinationdesc" id="codeCombinationdesc"/> 
		 <div class="clearfix"></div> 
		 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"> 
		 	<input type="hidden" id="showPage" value="${showPage}"/>
			<div class="portlet-header ui-widget-header float-right rowcancel" id="cancel_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div> 
			<div class="portlet-header ui-widget-header float-right rowsave" id="rowsave_${rowId}" style="cursor:pointer;"><fmt:message key="accounts.common.button.save"/></div>
		</div>
		 <div class="clearfix"></div> 
		 <div style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px!important; top: 116.5px; left: 366.5px;" class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on" style="-moz-user-select: none;"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on" style="-moz-user-select: none;">Dialog Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on" style="-moz-user-select: none;"><span class="ui-icon ui-icon-closethick" unselectable="on" style="-moz-user-select: none;">close</span></a></div><div id="product-popup" class="ui-dialog-content ui-widget-content" style="height: auto; min-height: 48px; width: auto;">
			<div class="product-result width100"></div>
			<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><button type="button" class="ui-state-default ui-corner-all">Ok</button><button type="button" class="ui-state-default ui-corner-all">Cancel</button></div></div>
		 </div>
	</td>  
</tr>
<script type="text/javascript">
var rowidentity=$('.tdidentity').parent().get(0);
var trval=$('.tdidentity').parent().get(0);
$(function(){  
	$('.rowcancel').click(function(){ 
		$('.formError').remove(); 
		//Find the Id for fetch the value from Id
		var showPage=$('#showPage').val();
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		$("#childRowId_"+rowid).remove();	
		if(showPage=="editedit"){
			$("#AddImage_"+rowid).hide();
			$("#EditImage_"+rowid).show();
			$("#DeleteImage_"+rowid).show();
			$("#WorkingImage_"+rowid).hide(); 
		 }
		 else if(showPage=="addedit"){
			 $("#AddImage_"+rowid).hide();
			 $("#EditImage_"+rowid).show();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 }
		 else{
			 $("#AddImage_"+rowid).show();
			 $("#EditImage_"+rowid).hide();
			 $("#DeleteImage_"+rowid).show();
			 $("#WorkingImage_"+rowid).hide(); 
		 } 
	});

	$('.rowsave').click(function(){
		 //Find the Id for fetch the value from Id
		var tempvar=$('.tdidentity').attr("id");
		var idarray = tempvar.split('_');
		var rowid=Number(idarray[1]);  
		
		 //var tempObject=$(this);	 
		 var productId=$('#productId').val();
		 var receiveQty=$('#receiveQty').val();  
		 var showPage=$('#showPage').val(); 
		 var url_action="";
		 var lineId=$('#lineId_'+rowid).text();   
		 var receiveId=Number($('#receiveId').val()); 
		 var receiveLineId=Number($('#receivelineid_'+rowid).val()); 
		 if(showPage=="editedit")
			 url_action="receivelines_addedit";
		 else if(showPage=="editadd")
			 url_action="receivelines_addadd";
		 else if(showPage=="addedit")
			 url_action="receivelines_addedit";
		 else if(showPage=="addadd")
			 url_action="receivelines_addadd";
		 else{
			 $('#othererror').hide().html("There is no Action mapped.").slideDown(1000);
			 return false;
		 } 
		 
		 if($("#addMasterJournalValidation").validationEngine({returnIsValid:true})){      
			 var flag=false;  
			 $.ajax({
					type:"POST",  
					url:"<%=request.getContextPath()%>/"+url_action+".action", 
				 	async: false, 
				 	data:{	productId:productId, receiveQty:receiveQty, 
					 		id:rowid, showPage:showPage, receiveId:receiveId, receiveDetailId:receiveLineId
					 	 },
				    dataType: "html",
				    cache: false,
					success:function(result){ 
						$('.tempresult').html(result);  
					     if(result!=null) {
                             flag = true; 
                             addedReceipt = addedReceipt + 1;
 	   							if(addedReceipt == 2) {								
	 	   							$('.addrows').trigger('click');
	 	   							addedReceipt = 1;
 	   							}
					     } 
				 	},  
				 	error:function(result){
				 	  $('.tempresult').html(result);
                     $("#othererror").html($('#returnMsg').html()); 
                     return false;
				 	} 
	          });
			 if(flag==true){   
		        	//Bussiness parameter  
		        	$('#itemnature_'+rowid).text($('#itemNature :selected').text());  
	        		$('#productname_'+rowid).text($('#productName').val());  
	        		$('#uom_'+rowid).text($('#uom').val());  
	        		$('#receiveqty_'+rowid).text(receiveQty);  
	        		$('#productid_'+rowid).val(productId);  
	        		
       			 	$("#AddImage_"+rowid).hide();
       			 	$("#EditImage_"+rowid).show();
       			 	$("#DeleteImage_"+rowid).show();
       			 	$("#WorkingImage_"+rowid).hide();  
       			 	
	        		$("#childRowId_"+rowid).remove();
					//Row count+1
					if(showPage=="addadd" || showPage=="editadd"){
						var childCount=Number($('#childCount').val());
						childCount=childCount+1;
						$('#childCount').val(childCount);
					}
	        	}
		 }
		 else{
			 return false;
		 } 
	});


	//product pop-up config
	$('.product-popup').click(function(){ 
	      tempid=$(this).parent().get(0);  
	      var itemNature=$('#itemNature').val();
		  $('#product-popup').dialog('open');
		 	$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/showreceive_productdetails.action", 
			 	async: false, 
			 	data:{itemNature:itemNature},
			    dataType: "html",
			    cache: false,
				success:function(result){   
					 $('.product-result').html(result);  
				},
				error:function(result){ 
					 $('.product-result').html(result); 
				}
			});  
	});
	
	 $('#product-popup').dialog({
			autoOpen: false,
			minwidth: 'auto',
			width:800,
			bgiframe: false,
			modal: true,
			buttons: { 
				"Cancel": function() {  
					$(this).dialog("close"); 
				}, 	
				"Ok": function() {  
					$(this).dialog("close"); 
				} 		
			} 	
		});
});	
</script>