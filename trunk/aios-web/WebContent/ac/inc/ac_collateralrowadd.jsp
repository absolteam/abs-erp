<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td id="lineId_${rowId}">${rowId}</td> 
	<td>
		<select name="assetDetail" class="width70 assetDetail" id="asset_${rowId}" style="border:0px;text-align; right;"> 
			<option value="">Select</option>
			<c:forEach var="entry" items="${ASSET_DETAIL}">
				<option value="${entry.key}">${entry.value}</option>
			</c:forEach> 
		</select>
	</td> 
	<td>
		<input type="text" style="text-align:right;border:0px;" class="width50 amount" id="amount_${rowId}"/>
	</td> 
	 <td style="width:0.01%;" class="opn_td" id="option_${rowId}">
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData" id="AddImage_${rowId}" style="display:none;cursor:pointer;" title="Add Record">
					<span class="ui-icon ui-icon-plus"></span>
		  </a>	
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData" id="EditImage_${rowId}" style="display:none; cursor:pointer;" title="Edit Record">
				<span class="ui-icon ui-icon-wrench"></span>
		  </a> 
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow" id="DeleteImage_${rowId}" style="display:none;cursor:pointer;" title="Delete Record">
				<span class="ui-icon ui-icon-circle-close"></span>
		  </a>
		  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${rowId}" style="display:none;" title="Working">
				<span class="processing"></span>
		  </a>
	</td>   
</tr>
<script type="text/javascript">
$(function(){
	 $('.assetDetail').combobox({ 
		 selected: function(event, ui){  
			 var assetId=$(this).val(); 
			 var rowId=getRowId($(this).attr('id'));  
			 if(assetId!=null && assetId!="" && $('#amount_'+rowId).val()!=null && $('#amount_'+rowId).val()!=""){
				 triggerAddRow(rowId);
			 }
		 }
	 });	
});
</script>