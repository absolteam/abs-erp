<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var tempid = "";
var sectionRowId = 0;
var accessCode = "";
var customerObject = [];

$(function(){   
	
	manupulateLastRow();
	 $jquery("#productPricingValidation").validationEngine('attach'); 
	 
	
	$('#startDate,#endDate').datepick({
		showTrigger: '#calImg'});  
	 
	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_pricing.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
					$('#pricing-common-popup').dialog('destroy');		
					$('#pricing-common-popup').remove();   
					$('#product-pricing-lookup').dialog('destroy');		
					$('#product-pricing-lookup').remove();  
					$('#customer_multiselect_popup').dialog('destroy');	
					$('#customer_multiselect_popup').remove();  
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 });

	 $('.addcalcrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabcalc>.rowidcalc:last").attr('id')));
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/product_pricing_calc_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabcalc tr:last').before(result);
					 if($(".tabcalc").height()>255)
						 $(".tabcalc").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidcalc').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineIdcalc_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  var productPricingId = Number($('#productPricingId').val());
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/product_pricing_addrow.action", 
			 	async: false,
			 	data:{rowId: id, productPricingId: productPricingId},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('#product_pricing_save').click(function(){ 
		 if($jquery("#productPricingValidation").validationEngine('validate')){
 	 			var productPricingId = Number($('#productPricingId').val()); 
 	 			var startDate = $('#startDate').val(); 
  	 			var endDate = $('#endDate').val();  
 	 			var description=$('#description').val();  
	 			var currencyId = Number($('#currency').val());  
	 			var pricingTitle =  $('#pricingTitle').val();
	 			 
 	 			$.ajax({
	 				type:"POST",
	 				url:"<%=request.getContextPath()%>/save_product_pricing.action", 
	 				async: false, 
	 				data:{	
		 					productPricingId: productPricingId, startDate: startDate, endDate: endDate, currencyId: currencyId,
		 					description: description, pricingTitle: pricingTitle
	 					 },
	 				 dataType: "json",
	 				 cache: false,
	 				 success:function(response){    
					 if(response.returnMessage=="SUCCESS"){
						 var message ="Record created.";
						 if(productPricingId > 0)
							 message ="Record updated.";
						 $.ajax({
								type:"POST",
								url:"<%=request.getContextPath()%>/show_product_pricing.action", 
							 	async: false,
							    dataType: "html",
							    cache: false,
								success:function(result){
									$('#pricing-common-popup').dialog('destroy');		
									$('#pricing-common-popup').remove();  
									$('#customer_multiselect_popup').dialog('destroy');	
									$('#customer_multiselect_popup').remove();  
									$('#product-pricing-lookup').dialog('destroy');		
									$('#product-pricing-lookup').remove();  
									$('#DOMWindow').remove();
									$('#DOMWindowOverlay').remove();
									$("#main-wrapper").html(result);  
									$('#success_message').hide().html(message).slideDown(1000); 
									$('#success_message').delay(2000).slideUp();
								}
						 });
					 } 
					 else{
						 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
						 $('#page-error').delay(2000).slideUp();
						 return false;
					 }
	 			},
				error:function(result){  
					$('#page-error').hide().html("Internal error.").slideDown(1000);
					$('#page-error').delay(2000).slideUp();
				}
			}); 
 		}else{
 			return false;
 		}
 	}); 

	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var lineId = $('#lineId_'+rowId).text();  
     	 var productPricingDetailId = Number($('#productPricingDetailId_'+rowId).val()); 
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/delete_product_pricing_detail.action", 
		 	async: false,  
		 	data:{parentId: lineId, productPricingDetailId: productPricingDetailId},
		    dataType: "json",
		    cache: false,
			success:function(result){ 
			  $(slidetab).remove();  
	      	  var i=1;
		   	  $('.rowid').each(function(){   
		   		 var rowId=getRowId($(this).attr('id')); 
		   		 $('#lineId_'+rowId).html(i);
				 i=i+1; 
			  });  
			} 
		});  
		 return false; 
	});

	 $('.delrow_pc').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
		 $(slidetab).remove();  
     	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineIdcalc_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	$('.priceValue').live('change',function(){  
		 calculatePriceValue(getRowId($(this).attr('id')));
	});

	$('.calculationType').live('change',function(){  
		var rowId = getRowId($(this).attr('id')); 
		if($('#calculationType_'+rowId+" option:selected").text() == "Fixed Price"){
			$('#calculationSubType_'+rowId).attr('disabled', false);
		}else {
			$('#calculationSubType_'+rowId).attr('disabled', true);
		}
		$('#calculationSubType_'+rowId).val('');
		calculatePriceValue(rowId);
	}); 

	 $('.editData').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id')); 
		 var productPricingDetailId = Number($('#productPricingDetailId_'+rowId).val());
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_pricing_calculation.action", 
		 	async: false,  
		 	data:{parentId: rowId, productPricingDetailId: productPricingDetailId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		}); 
		return false; 
	});  
	 
	 $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	 $('.product-pricing-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.pricecommon-result').html(result);
	                  $('#pricing-common-popup').dialog('open');
	  				  $($($('#pricing-common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.pricecommon-result').html(result);
	             }
	         });
	          return false;
	 	});	 

		//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="PRODUCT_PRICING_LEVEL"){
				$('#pricingType_'+sectionRowId).html("");
				$('#pricingType_'+sectionRowId).append("<option value=''>Select</option>");
				loadLookupList("pricingType_"+sectionRowId); 
			} 
		});

	$('.pricecloseDom').live('click',function(){
	  $('#DOMWindow').remove();
	  $('#DOMWindowOverlay').remove();
	  return false;
   });
	
	$('.sellingPrice,.basicCostPrice,.standardPrice,.suggestedRetailPrice').live('change',function(){
		var rowId = getRowId($(this).attr('id'));  
		var productId = $('#productid_'+rowId).val(); 
		var storeId = Number($('#storedetailid_'+rowId).val()); 
		var basicCostPrice = Number($('#basicCostPrice_'+rowId).val());    
		var standardPrice = Number($('#standardPrice_'+rowId).val());
		var sellingPrice = Number($('#sellingPrice_'+rowId).val());
		var suggestedRetailPrice = Number($('#suggestedRetailPrice_'+rowId).val());
		var description = $('#description_'+rowId).val(); 
		var productPricingDetailId =  Number($('#productPricingDetailId_'+rowId).val());   
		var calculationDetails = "";
		if(productId > 0 && basicCostPrice> 0 && sellingPrice > 0) {
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_session_product_pricing.action",  
			 	async: false,
			 	data:{
			 			basicCostPrice: basicCostPrice, productId: productId, description: description, standardPrice: standardPrice,
				 		suggestedRetailPrice: suggestedRetailPrice, productPricingDetailId: productPricingDetailId, storeId: storeId,
				 		priceDetailMirrorId: rowId, sellingPrice: sellingPrice, calculationDetails: calculationDetails
				 	},
			    dataType: "json",
			    cache: false,
				success:function(response){   
					if(response.returnMessage == "SUCCESS") { 
						triggerAddRow(rowId);
						$('#EditImage_'+rowId).show();
					}else {
						
					} 
				},error:function(response){  
					 
				}
			});
		} else{$('#EditImage_'+rowId).hide();}
		return false;
	});

	$('.sessionPricingSave').live('click',function(){
	  var calculationDetails = getCalculationDetails(); 
	  var rowId = $('#parentId').val(); 
	  var productId = $('#productid_'+rowId).val(); 
	  var storeId = Number($('#storedetailid_'+rowId).val()); 
	  var basicCostPrice = $('#basicCostPrice_'+rowId).val();    
	  var standardPrice = $('#standardPrice_'+rowId).val();
	  var sellingPrice = $('#sellingPrice_'+rowId).val();
	  var suggestedRetailPrice = $('#suggestedRetailPrice_'+rowId).val();
	  var description = $('#description_'+rowId).val();
	  var productPricingDetailId =  Number($('#productPricingDetailId_'+rowId).val());   
	  if(calculationDetails != null && calculationDetails!=''){
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/update_session_product_pricing.action",  
			 	async: false,
			 	data:{
			 			basicCostPrice: basicCostPrice, productId: productId, description: description, standardPrice: standardPrice,
				 		calculationDetails: calculationDetails, suggestedRetailPrice: suggestedRetailPrice, productPricingDetailId: productPricingDetailId,
				 		priceDetailMirrorId: rowId, sellingPrice: sellingPrice, storeId: storeId
				 	},
			    dataType: "json",
			    cache: false,
				success:function(response){   
					if(response.returnMessage == "SUCCESS") {
						$('#DOMWindow').remove();
						$('#DOMWindowOverlay').remove();
						triggerAddRow(rowId);
					}else {
						
					} 
				},error:function(response){  
					 
				}
			}); 
	  } else{
		  $('#page-error-popup').hide().html("Please enter calculation details.").slideDown(1000);
			 $('#page-error-popup').delay(2000).slideUp(); 
			 return false;
		  }
	  return false;
   });

   var getCalculationDetails = function(){
	 var pricingTypeArray = new Array();
	 var calculationTypeArray = new Array();
 	 var calculationSubTypeArray = new Array(); 
 	 var priceValueArray = new Array(); 
 	 var calculatedPriceArray = new Array(); 
 	 var defaultPriceArray = new Array(); 
 	 var linedescriptionArray = new Array();
 	 var pricingCalcArray = new Array();
 	 var storeArray = new Array();
 	 var customerArray = new Array();
 	 var calculationDetail = ""; 
 	 $('.rowidcalc').each(function(){  
		 var rowId = getRowId($(this).attr('id'));  
		 var pricingType = $('#pricingType_'+rowId).val();   
		 var calculationType = $('#calculationType_'+rowId).val();  
		 var calculationSubType = $('#calculationSubType_'+rowId).val();    
		 var priceValue=$('#priceValue_'+rowId).val();
		 var calculatedPrice = $('#calculatedPrice_'+rowId).text();
		 var defaultPrice = $('#defaultPrice_'+rowId).attr('checked');
		 var linedescription = $('#linedescription_'+rowId).val();
		 var productPricingCalcId = Number($('#productPricingCalcId_'+rowId).val());
		 var storeId = Number($('#storeid_'+rowId).val());
		 var customerIds = ""; 
		 $('.customerObject_'+rowId).each(function(){   
			 var customerRow = getRowId($(this).attr('id'));
			 var customerId = Number($('#customerId_'+customerRow).val());
			 if(customerId > 0)
				 customerIds += $('#customerId_'+customerRow).val()+",";
		 }); 
 		 if(typeof pricingType != 'undefined' && pricingType!=null && 
				 pricingType!="" && calculationType!='' && priceValue!=''){
			 pricingTypeArray.push(pricingType); 
			 calculationTypeArray.push(calculationType); 
			 if(calculationSubType ==null || calculationSubType=="")
				 calculationSubTypeArray.push("##");
			 else 
				 calculationSubTypeArray.push(calculationSubType);
			 if(linedescription ==null || linedescription=="")
				 linedescriptionArray.push("##");
			else 
				linedescriptionArray.push(linedescription); 
			 priceValueArray.push(priceValue); 
			 calculatedPriceArray.push(calculatedPrice);
			 defaultPriceArray.push(defaultPrice);
			 pricingCalcArray.push(productPricingCalcId);
			 storeArray.push(storeId);
			 if(customerIds ==null || customerIds=="")
				 customerArray.push("##");
			 else
				 customerArray.push(customerIds);
		 } 
	 });
 	 for(var j=0;j<pricingTypeArray.length;j++){ 
 		calculationDetail += pricingTypeArray[j]+"__"+calculationTypeArray[j]+"__"+ calculationSubTypeArray[j]+"__"+
							linedescriptionArray[j]+"__"+priceValueArray[j]+"__"+calculatedPriceArray[j]+"__"+defaultPriceArray[j]
							+"__"+pricingCalcArray[j]+"__"+customerArray[j]+"__"+storeArray[j];
		if(j==pricingTypeArray.length-1){   
		} 
		else{
			calculationDetail += "@#";
		}
	}  
	return calculationDetail; 
   }; 

   $('#product-common-popup').live('click',function(){  
	   $('#pricing-common-popup').dialog('close'); 
   });

   $('#store-common-popup').live('click',function(){  
	   $('#pricing-common-popup').dialog('close'); 
   });

   $('#customer-common-popup').live('click',function(){  
	   $('#pricing-common-popup').dialog('close'); 
   });

   $('.show-customer-list').live('click',function(){  
	    $('.ui-dialog-titlebar').remove();   
	    var rowId = $(this).attr('id').split("_")[1];  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_multiselect_customer_list.action", 
			 	async: false,   
			    dataType: "html",
			    data:{rowId: rowId},
			    cache: false,
				success:function(result){  
					$('.pricecommon-result').html(result);   
					$('#pricing-common-popup').dialog('open');
					$($($('#pricing-common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.pricecommon-result').html(result);  
				}
			});  
			return false;
		}); 
	

   	$('.show-productpricing-list').live('click',function(){  
	    $('.ui-dialog-titlebar').remove();   
	    var rowId = $(this).attr('id').split("_")[1];  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_product_common_popup.action", 
			 	async: false,   
			    dataType: "html",
			    data: {itemType: "I", rowId: rowId, pageInfo: "ProductPricing"},
			    cache: false,
				success:function(result){  
					$('.pricecommon-result').html(result);   
					$('#pricing-common-popup').dialog('open');
					$($($('#pricing-common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.pricecommon-result').html(result);  
				}
			});  
			return false;
		});

   	$('.show-store-list').live('click',function(){  
	    $('.ui-dialog-titlebar').remove();   
	    var rowId = $(this).attr('id').split("_")[1];  
		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
			 	async: false,   
			    dataType: "html",
			    data: {rowId: rowId},
			    cache: false,
				success:function(result){  
					$('.pricecommon-result').html(result);   
					$('#pricing-common-popup').dialog('open');
					$($($('#pricing-common-popup').parent()).get(0)).css('top',0); 
				},
				error:function(result){  
					 $('.pricecommon-result').html(result);  
				}
			});  
			return false;
		});

	$('#pricing-common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 
 	
 	if (Number($('#productPricingId').val()) > 0) {
 		$('#currency').val($('#tempCurrency').val()); 
	}else {
 		$('#currency').val($('#defaultCurrency').val());
 	}

 	$('.deleteoption')
		.live('click',
			function() {
		var rowId =  $(this).attr('id').split("_")[1];
   		$('#div_'+rowId).remove(); 
 		var i = 0;
		$('.count_index')
		.each(
				function() {
					i = i +1;
			$(this).text(i+".");		
		});
		return false;
 	});

 	$('#view_option').click(function(){   
		  $('.ui-dialog-titlebar').remove();
			var htmlString = "";
						htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>"
								+ customerObject.title + "</div>";
								var count = 0;
						
			$(customerObject.record)
								.each(
										function(index) {
											count += 1;
											htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
													+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
													+ count
													+ ".</span>"
													+ "<span class='width40 float-left'>"
													+ customerObject.record[index].customerName
													+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
										});

						$('#customer_multiselect_popup').dialog('open');

						$('#customer_multiselect_popup').html(htmlString);
					});

 	 $('#customer_multiselect_popup').dialog({
	 		autoOpen: false,
	 		width: 800,
	 		height: 400,
	 		bgiframe: true,
	 		modal: true,
	 		buttons: {
	 			"OK": function(){  
	 				$(this).dialog("close"); 
	 			} 
	 		}
	 	}); 
 	 
 	/* var productval = '${requestScope.productId}'; 
 	 $('.rowid').each(function(){   
   		 var rowId=getRowId($(this).attr('id')); 
   		 var currentId = $('#productid_'+rowId).val();
   		 if(productval == currentId){
   			 $('#fieldrow_'+rowId).css('background','#F2F5A9');
   		 } 
	  });  */
	  
 	{
 		var forFocus = $("#idForFocus").val();
 		
 		$('#forFocus_'+ forFocus).closest('tr').children('td').css('background-color','#F2F5A9');
 		$('#forFocus_'+ forFocus).closest('tr').children('td').slice(2).find('input[type=text]').focus();
 		 		
 	}; 	
 	
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	var tdSize = 0;
	var actualSize = 0; 
	hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	}); 
	tdSize=$($(".tab>tr:first").children()).size();
	actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}   
function triggerAddRow(rowId){   
	var nexttab=$('#fieldrow_'+rowId).next();  
	if($(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
} 
function triggerCalcAddRow(rowId){   
	var nexttab=$('#fieldrowcalc_'+rowId).next();  
	if($(nexttab).hasClass('lastrow')){ 
		$('#DeleteCImage_'+rowId).show();
		$('.addcalcrows').trigger('click');
	} 
} 

function calculatePriceValue(currentObject){ 
	 var priceValue = Number($('#priceValue_'+currentObject).val());
 	 var pricingType = Number($('#pricingType_'+currentObject).val());
	 var calculationType = Number($('#calculationType_'+currentObject).val());
	 var calculationSubType = Number($('#calculationSubType_'+currentObject).val());
	 var parentId = $('#parentId').val();
 	 var basicCostPrice =  Number($('#basicCostPrice_'+parentId).val());
	 var standardPrice =  Number($('#standardPrice_'+parentId).val());
	 if(priceValue > 0 && calculationType >0) {
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/calculate_product_price.action",
						async : false,
						data : {
							priceValue : priceValue,
							pricingType : pricingType,
							calculationType : calculationType,
							calculationSubType : calculationSubType,
							basicCostPrice : basicCostPrice,
							standardPrice : standardPrice 
						},
						dataType : "json",
						cache : false,
						success : function(response) {
							$('#calculatedPrice_' + currentObject).text(
									response.calculatedPrice);
							triggerCalcAddRow(currentObject);
						} 
					});
		}
		return false;
	}
	function commonProductPopup(productId, productName, rowId) {
		$('#productid_' + rowId).val(productId);
		$('#product_' + rowId).html(productName);
		return false;
	}

	function commonStorePopup(storeId, storeName, rowId) {
		$('#storeid_' + rowId).val(storeId); 
		$('#store_' + rowId).html(storeName); 
		return false;
 	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
	function selectedCustomerList(jsonData, rowId) {
		customerObject = [];
		customerObject = {
			"title" : "Customers",
			"record" : jsonData
		};
		if (customerObject.record.length > 0) {
			$('#view_option').show();
		}
		var htmlString = "";
		var count = Number(0);
		$(customerObject.record)
				.each(
						function(index) {
							count += 1;
							htmlString += "<div id=div_"+rowId+""+customerObject.record[index].customerId+""+count+" class='width100 float-left customerObject customerObject_"+rowId+"' style='padding: 3px;' >"
									+ "<span id='countindex_"+rowId+""+customerObject.record[index].customerId+""+count+"' class='float-left count_index' style='margin: 0 5px;'>"
									+ count
									+ ".</span>"
									+ "<span class='width40 float-left'>"
									+ customerObject.record[index].customerName
									+ "</span> <input type='hidden' id='customerId_"+rowId+""+customerObject.record[index].customerId+""+count+"' value='"+customerObject.record[index].customerId+"'/>"
									+ "<span class='deleteoption' id='deleteoption_"+rowId+""+customerObject.record[index].customerId+""+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
						});
		$('#customerInfo_' + rowId).html(htmlString);
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Product
			Pricing
		</div> 
		<form id="productPricingValidation" method="POST"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="productPricingId" name="productPricingId"
					value="${PRODUCT_PRICING.productPricingId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30"> Currency<span
									style="color: red;">*</span> </label> <select name="currency"
									id="currency" class="width51">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${PRODUCT_PRICING.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${PRODUCT_PRICING.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30">Pricing Title<span
									style="color: red;">*</span> </label> <input name="startDate"
									type="text" value="${PRODUCT_PRICING.pricingTitle}"
									id="pricingTitle"
									class="pricingTitle validate[required] width50">
							</div>
							<div>
								<label class="width30"> Start Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${PRODUCT_PRICING.startDate ne null && PRODUCT_PRICING.startDate ne ''}">
										<c:set var="startDate" value="${PRODUCT_PRICING.startDate}" />
										<input name="startDate" type="text" readonly="readonly"
											id="startDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("startDate").toString())%>"
											class="startDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="startDate" type="text" readonly="readonly"
											id="startDate" class="startDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> End Date<span class="mandatory">*</span>
								</label>
								<c:choose>
									<c:when
										test="${PRODUCT_PRICING.endDate ne null && PRODUCT_PRICING.endDate ne ''}">
										<c:set var="endDate" value="${PRODUCT_PRICING.endDate}" />
										<input name="endDate" type="text" readonly="readonly"
											id="endDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("endDate").toString())%>"
											class="endDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="endDate" type="text" readonly="readonly"
											id="endDate" class="endDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Pricing Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<c:if test="${sessionScope.unified_price eq 'false' && PRODUCT_PRICING.productPricingId gt 0}">
											<th style="width: 5%;">Store</th>
										</c:if>
										<th style="width: 5%;">Cost Price</th>
										<th style="width: 5%;">Standard Price</th>
										<th style="width: 5%;">MSRP</th>
										<th style="width: 5%;">Selling Price</th>
										<th style="width: 5%;">Description</th>
										<th style="width: 5%;">Define Price</th>
										<th style="width: 1%;" class="options"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab"
									style="max-height: 255px; overflow-x: hidden; overflow-y: auto;">
									<c:forEach var="DETAIL"
										items="${PRODUCT_PRICING.productPricingDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> 
												<span id="productscroll_${DETAIL.product.productId}"></span> 
												 <span
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all show-productpricing-list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
											</td>
											<c:if test="${sessionScope.unified_price eq 'false'}">
												<td> 
													${DETAIL.store.storeName}
												</td>
											</c:if>
											<td><input type="text" name="basicCostPrice"
												id="basicCostPrice_${status.index+1}"
												value="${DETAIL.basicCostPrice}"
												class="basicCostPrice width80">
											</td>
											<td><input type="text" name="standardPrice"
												id="standardPrice_${status.index+1}"
												value="${DETAIL.standardPrice}"
												class="standardPrice width80 validate[optional,custom[number]]">
											</td>
											<td><input type="text" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${status.index+1}"
												value="${DETAIL.suggestedRetailPrice}"
												class="suggestedRetailPrice width80 validate[optional,custom[number]]">
											</td>
											<td><input type="text" name="sellingPrice"
												id="sellingPrice_${status.index+1}"
												value="${DETAIL.sellingPrice}"
												class="sellingPrice width80">
											</td>
											<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${DETAIL.description}" class="description width98">
											</td>
											<td style="display: none;"><input type="text"
												id="forFocus_${DETAIL.productPricingDetailId}"
												value="${DETAIL.productPricingDetailId}" class="width98">
											</td>
											<td>
												<div
													class="portlet-header ui-widget-header float-left editData"
													style="cursor: pointer;" id="EditImage_${status.index+1}">
													Define Price</div>
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip "
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productPricingDetailId"
												id="productPricingDetailId_${status.index+1}"
												value="${DETAIL.productPricingDetailId}" />
												<input type="hidden" name="storeDetailId"
														id="storedetailid_${status.index+1}"
														value="${DETAIL.store.storeId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(PRODUCT_PRICING.productPricingDetails)+1}"
										end="${fn:length(PRODUCT_PRICING.productPricingDetails)+2}"
										step="1" varStatus="status">
										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all show-productpricing-list width100">
														<span class="ui-icon ui-icon-newwin"></span> </a> </span>
											</td>
											<c:if test="${sessionScope.unified_price eq 'false' && PRODUCT_PRICING.productPricingId gt 0}">
												<td></td>
											</c:if>
											<td><input type="text" name="basicCostPrice"
												id="basicCostPrice_${i}" class="basicCostPrice width80">
											</td>
											<td><input type="text" name="standardPrice"
												id="standardPrice_${i}"
												class="standardPrice validate[optional,custom[number]] width80">
											</td>
											<td><input type="text" name="suggestedRetailPrice"
												id="suggestedRetailPrice_${i}"
												class="suggestedRetailPrice width80 validate[optional,custom[number]]">
											</td>
											<td><input type="text" name="sellingPrice"
												id="sellingPrice_${i}" class="sellingPrice width80">
											</td>
											<td><input type="text" name="description"
												id="description_${i}" class="description width98">
											</td>
											<td>
												<div
													class="portlet-header ui-widget-header float-left editData"
													style="cursor: pointer; display: none;" id="EditImage_${i}">Define
													Price</div>
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="productPricingDetailId"
												id="productPricingDetailId_${i}" />
												<input type="hidden" name="storeDetailId" id="storedetailid_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div style="display: none;" id="customer_multiselect_popup"></div>
			<div id="temp-result" style="display: none;"></div>
			<span class="callJq"></span>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="product_pricing_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
				<div class="portlet-header ui-widget-header float-left addcalcrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="pricing-common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="pricecommon-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<input type="hidden" value="${requestScope.productPricingDetailId}" id="idForFocus" />
</div>