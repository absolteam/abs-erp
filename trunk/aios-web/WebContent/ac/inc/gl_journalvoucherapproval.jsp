<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<style>
.opn_td{
	width:6%;
}
#DOMWindow{	
	width:95%!important;
	height:90%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.spanfont{
	font-weight: normal;
	position: relative;
	top:2px;
}
.divpadding{
		padding-bottom: 7px;
}
.divpadding label{padding-top:0px!important}
</style>
<c:if test="${param['lang'] != null}">
    <fmt:setLocale value="${param['lang']}" scope="session" />
</c:if> 
<div id="main-content">  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.journalvoucher"/>  
		</div> 
		  <form name="addMasterJournal" id="addMasterJournalValidation"> 
			<div class="portlet-content">   
				<div id="journal-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
			  	<input type="hidden" id="tempJournalId" name="tempJournalId" value="${JOURNAL_INFO.transactionId}"/> 
				<div class="width100 float-left" id="hrm"> 
					<fieldset>
						<legend><fmt:message key="accounts.jv.label.generalinfo"/></legend>
							<div class="float-right  width48"> 
								<div class="divpadding">
									<label class="width20"><fmt:message key="accounts.jv.label.jvcurrency"/></label>
									<span class="width60 spanfont">${JOURNAL_INFO.currency.currencyPool.code}</span> 
								</div>	
								<div>
									<label class="width20"><fmt:message key="accounts.jv.label.description"/></label>
									<span class="width60 spanfont">${JOURNAL_INFO.description}</span> 
								</div> 
							</div>
							<div class="float-left width50">
								<div class="divpadding">
									<label class="width20"> 
									<fmt:message key="accounts.jv.label.vouchernumber"/></label> 
									 <span class="spanfont" id="journalNumber">${JOURNAL_INFO.journalNumber}</span>  
								</div>
								<div class="clearfix"></div>
								<div class="divpadding">
									<label class="width20"> 
									<fmt:message key="accounts.jv.label.jvgldate"/></label> 
									<c:set var="journalDate" value="${JOURNAL_INFO.transactionTime}"/>
									<span class="spanfont"><%=DateFormat.convertDateToString(pageContext.getAttribute("journalDate").toString()) %></span> 
								</div> 
								<div class="divpadding">
									<label class="width20"><fmt:message key="accounts.jv.label.period"/></label> 
									<span class="spanfont">${JOURNAL_INFO.period.name}</span> 
								</div>	 
								<div>
									<label class="width20"><fmt:message key="accounts.jv.label.category"/></label> 
									<span class="spanfont">${JOURNAL_INFO.category.name}</span> 
								</div>	
							</div>
					</fieldset>
				 </div>
				 
			<div class="clearfix"></div> 
 			<div class="portlet-content class90" id="hrm" style="margin-top:10px;"> 
 			<fieldset>
				<legend><fmt:message key="accounts.jv.label.transactiondetail"/></legend>
 			<div  id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
 			<div id="warning_message" class="response-msg notice ui-corner-all width90"  style="display:none;"></div>
				<div id="hrm" class="hastable width100"  > 
					 <input type="hidden" readonly="readonly" name="trnValue" id="trnValue"/>  
					<table id="hastab" class="width100"> 
						<thead>
						   <tr> 
								<th style="width:1%"><fmt:message key="accounts.jv.label.jvlinno"/></th> 
							    <th style="width:5%"><fmt:message key="accounts.jv.label.jvcc"/></th> 
							    <th style="width:1%""><fmt:message key="accounts.jv.label.transactionflag"/></th>  
							    <th style="width:2%"><fmt:message key="accounts.jv.label.amount"/></th>  
							    <th style="width:5%"><fmt:message key="accounts.journal.label.desc"/></th>  
						  </tr>
						</thead> 
						<tbody class="tab">
						
						<c:choose>
							<c:when test="${JOURNAL_LINE_INFO ne null && JOURNAL_LINE_INFO ne ''}">
								<c:forEach var="bean" items="${JOURNAL_LINE_INFO}" varStatus ="status">
									<tr class="rowid" id="fieldrow_${status.index+1}">
										<td id="lineId_${status.index+1}">${status.index+1}</td>
										<td class="codecombination_info" id="codecombination_${status.index+1}" style="cursor: pointer;">
											<input type="hidden" name="combinationId_${status.index+1}" value="${bean.combination.combinationId}" id="combinationId_${status.index+1}"/> 
											<span class="spanfont">${bean.combination.accountByCompanyAccountId.code}.${bean.combination.accountByCostcenterAccountId.code}.${bean.combination.accountByNaturalAccountId.code} 
											<c:if test="${bean.combination.accountByAnalysisAccountId ne null && bean.combination.accountByAnalysisAccountId ne ''}">
												.${bean.combination.accountByAnalysisAccountId.code}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer1AccountId ne null && bean.combination.accountByBuffer1AccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.code}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer2AccountId ne null && bean.combination.accountByBuffer2AccountId ne ''}">
												.${bean.combination.accountByBuffer2AccountId.code}
											</c:if>  
											( ${bean.combination.accountByCompanyAccountId.account}.${bean.combination.accountByCostcenterAccountId.account}.${bean.combination.accountByNaturalAccountId.account} 
											<c:if test="${bean.combination.accountByAnalysisAccountId ne null && bean.combination.accountByAnalysisAccountId ne ''}">
												.${bean.combination.accountByAnalysisAccountId.account}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer1AccountId ne null && bean.combination.accountByBuffer1AccountId ne ''}">
												.${bean.combination.accountByBuffer1AccountId.account}
											</c:if> 
											<c:if test="${bean.combination.accountByBuffer2AccountId ne null && bean.combination.accountByBuffer2AccountId ne ''}">
												.${bean.combination.accountByBuffer2AccountId.account}
											</c:if> )</span>
										</td>
										<td id="transactionflag_${status.index+1}">
											<span>
												<c:choose>
													<c:when test="${bean.isDebit}">
														Debit
													</c:when>
													<c:otherwise>Credit</c:otherwise>
												</c:choose>
											</span> 
										</td>
										<td id="amount_${status.index+1}" style="text-align; right;">
											<span style="text-align:right;">
												${bean.amount}
											</span> 
										</td> 
										<td id="linedecription_${status.index+1}">
											<span>${bean.description}</span> 
										</td> 
									</tr>  
								</c:forEach>
							</c:when> 
						</c:choose>  
				</tbody>
			</table>
		</div> 
		</fieldset>
	</div>  
	<div class="clearfix"></div>
	<table class="width50 float-right" id="hrm">
		<tr>
			<td>
				<div>
					<span class="width10 float-left">Debit</span> <span
						class="debitTotal width16 right-align">${requestScope.totalAmount}</span>
				</div></td>
		</tr>
		<tr>
			<td>
				<div>
					<span class="width10 float-left">Credit</span> <span
						class="creditTotal width16 right-align">${requestScope.totalAmount}</span>
				</div></td>
			<td style="display: none;"><span
				class="convertedDebitTotal width16 right-align"
				style="display: block; float: left; width: 12%;"></span> <span
				class="convertedCreditTotal width16 right-align"
				style="display: block; float: left; width: 12%;"></span>
			</td>
		</tr>
	</table> 
	<div class="clearfix"></div> 
	<div id="codecombinationbox" style="display: none;" class="float-left">   
	</div> 
	</div>  
	<div class="clearfix"></div> 
  </form>
  </div> 
</div>
<div class="clearfix"></div> 