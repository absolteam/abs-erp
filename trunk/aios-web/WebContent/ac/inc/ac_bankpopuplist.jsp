<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			bank details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="banks" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${BANK_DETAILS}" var="bean" varStatus="status">
	     		<option value="${bean.bankId}">${bean.bankName}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 bank_list" style="padding:4px;"> 
     	<ul>
     		<li class="width40 float-left"><span>Bank Number</span></li>  
			<li class="width50 float-left"><span>Bank Name</span></li>
			<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
     		<c:forEach items="${BANK_DETAILS}" var="bean" varStatus="status">
	     		<li id="bank_${status.index+1}" class="bank_list_li" style="height: 20px;">
	     			<input type="hidden" value="${bean.bankId}" id="bankid_${status.index+1}">  
	     			<input type="hidden" value="${bean.bankName}" id="bankname_${status.index+1}">  
	     			<span id="banknumber_${status.index+1}" style="cursor: pointer;" class="width40 float-left">${bean.bankNumber}</span>
	     			<span id="bankname_${status.index+1}" style="cursor: pointer;" class="width50 float-left">${bean.bankName}
	     				<span id="bankselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
	     			</span>  
	     		</li>
	     	</c:forEach> 
     	</ul> 
	</div> 
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:88%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>	
 </div> 
<script type="text/javascript">
var currentId="";
$(function(){
	$($($('#bank-popup').parent()).get(0)).css('width',500);
	$($($('#bank-popup').parent()).get(0)).css('height',250);
	$($($('#bank-popup').parent()).get(0)).css('padding',0);
	$($($('#bank-popup').parent()).get(0)).css('left',0);
	$($($('#bank-popup').parent()).get(0)).css('top',100);
	$($($('#bank-popup').parent()).get(0)).css('overflow','hidden');
	$('#bank-popup').dialog('open');
	$('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	       $('#bankId').val($(this).val());
	       $('#bankName').val($(".autoComplete option:selected").text());
	       $('#bank-popup').dialog("close"); 
	   } 
	}); 
	$('.bank_list_li').live('click',function(){
		var tempvar="";var idarray = ""; var rowid="";
		$('.bank_list_li').each(function(){  
			 currentId=$(this); 
			 tempvar=$(currentId).attr('id');  
			 idarray = tempvar.split('_');
			 rowid=Number(idarray[1]);   
			 if($('#bankselect_'+rowid).hasClass('selected')){ 
				 $('#bankselect_'+rowid).removeClass('selected'); 
			 }
		}); 
		currentId=$(this); 
		tempvar=$(currentId).attr('id');  
		idarray = tempvar.split('_');
		rowid=Number(idarray[1]);  
		$('#bankselect_'+rowid).addClass('selected');
	});
	
	$('.bank_list_li').live('dblclick',function(){
		var tempvar="";var idarray = ""; var rowid="";
		 currentId=$(this);  
		 tempvar=$(currentId).attr('id');   
		 idarray = tempvar.split('_'); 
		 rowid=Number(idarray[1]);  
		 $('#bankId').val($('#bankid_'+rowid).val());
		 $('#bankName').val($('#bankname_'+rowid).val()); 
		 $('#bank-popup').dialog("close"); 
	});
	$('#close').click(function(){
		$('#bank-popup').dialog("close"); 
	});
});  
</script>
<style>
	.ui-button { margin-left: -1px;}
	.ui-button-icon-only .ui-button-text { padding: 0.35em; } 
	.ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:relative; float:left;}
	.ui-button-icon-primary span{
		margin:-6px 0 0 -8px !important;
	}
	button{
		height:18px; 
		float: right!important;
		margin:4px 0 0 -36px!important;
	}
	.ui-autocomplete{
		width:194px!important;
	} 
	.bank_list ul li{
		padding:3px;
	}
	.bank_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
</style>