<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accountTypeId = 0;
var combinationType ="";
$(function(){
	  
	if(Number($('#creditTermId').val()) > 0){
		$('#discountMode').val($('#tempDiscountMode').val());
		$('#penaltyMode').val($('#tempPenaltyMode').val());
		$('#penaltyType').val($('#tempPenaltyType').val());
		$('#dueDate').val($('#tempDueDate').val()); 
		var isSuppleirFlag = '${CREDIT_TERM_INFO.isSupplier}';
 		if(isSuppleirFlag == "false"){
			$('#customerChecked').attr('checked', true); 
		} else if(isSuppleirFlag == "true"){ 
			$('#supplierChecked').attr('checked', true);
		}
	}  
 
	$('input,select').attr('disabled', true);
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>credit term</div>	
		  <form name="creditterm_details" id="creditterm_details" style="position: relative;"> 
			<div class="portlet-content">  
				<div id="page-error" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
			  	<div class="tempresult" style="display:none;"></div>
				<div class="width100 float-left" id="hrm">  
					<div class="width48 float-right"> 
						<fieldset style="min-height: 170px;"> 
							<div>
								<label class="width30" for="penaltyType"><fmt:message key="accounts.creditterm.credittermpenaltytype"/></label>
								<select name="penaltyType" id="penaltyType" class="width51">
									<option value="">Select</option>
									<c:forEach var="penaltyType" items="${PENALTY_TYPE}">
										<option value="${penaltyType.key}">${penaltyType.value}</option>
									</c:forEach>
								</select>
								<input type="hidden" id="tempPenaltyType" value="${CREDIT_TERM_INFO.penaltyType}"/> 
							</div> 
							<div>
								<label class="width30" for="penaltyMode"><fmt:message key="accounts.creditterm.credittermpenaltymode"/></label>
								<select name="penaltyMode" id="penaltyMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="penaltyMode" items="${PAYMENT_MODE}">
										<option value="${penaltyMode.key}">${penaltyMode.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempPenaltyMode" value="${CREDIT_TERM_INFO.penaltyMode}"/>
							</div> 
							<div>
								<label class="width30" for="penalty"><fmt:message key="accounts.creditterm.credittermpenalty"/></label> 
								<input type="text" name="penalty" id="penalty" maxlength="3" class="width50" value="${CREDIT_TERM_INFO.penalty}"/>
							</div> 
							<div>
								<label class="width30" for="description">Description</label> 
								<textarea class="width51" id="description">${CREDIT_TERM_INFO.description}</textarea>
							</div> 
						</fieldset>
					</div> 
					<div class="width50 float-left"> 
						<fieldset style="min-height: 170px;"> 
							<div>
								<label class="width30" for="termName"><fmt:message key="accounts.creditterm.credittermname"/></label> 
								<input type="hidden" id="creditTermId" name="creditTermId" value="${CREDIT_TERM_INFO.creditTermId}"/>
								<input type="hidden" id="termTempName" value="${CREDIT_TERM_INFO.name}"/>
								<input type="text" name="termName" id="termName" maxlength="20" class="width50" value="${CREDIT_TERM_INFO.name}"/>
							</div> 
							<div>
								<label class="width30" for="dueDate"><fmt:message key="accounts.creditterm.credittermduedate"/></label> 
								<select name="dueDate" id="dueDate" class="width51">
									<option value="">Select</option>
									<c:forEach var="dueDate" items="${CREDIT_TERM_BASEDAY}">
										<option value="${dueDate.key}">${dueDate.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempDueDate" value="${CREDIT_TERM_INFO.dueDay}"/>
							</div> 
							<div>
								<label class="width30" for="discountDay"><fmt:message key="accounts.creditterm.credittermdiscountday"/></label> 
								<input type="text" name="discountDay" id="discountDay" class="width50" value="${CREDIT_TERM_INFO.discountDay}"/>
							</div> 
							<div>
								<label class="width30" for="discountMode"><fmt:message key="accounts.creditterm.credittermdiscountmode"/></label> 
								<select name="discountMode" id="discountMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="discountMode" items="${PAYMENT_MODE}">
										<option value="${discountMode.key}">${discountMode.value}</option>
									</c:forEach>
								</select> 
								<input type="hidden" id="tempDiscountMode" value="${CREDIT_TERM_INFO.discountMode}"/>
							</div> 
							<div>
								<label class="width30" for="discount"><fmt:message key="accounts.creditterm.credittermdiscount"/></label> 
								<input type="text" name="discount" id="discount" class="width50" value="${CREDIT_TERM_INFO.discount}"/>
							</div> 
							<div>
								<label class="width30">Discount For</label>
								<div class="float-left width20">
									<input type="radio" id="customerChecked" name="discountfor" class="float-left">
									<label for="customerChecked" class="width70">Customer</label>   
								</div>
								<div class="float-left width30">
									<input type="radio" id="supplierChecked" name="discountfor" class="float-left">
									<label for="supplierChecked" class="width40">Supplier</label>  
								</div>
							</div>
						</fieldset>
					</div> 
			</div>  
		</div>    
		<div class="clearfix"></div>   
  </form>
  </div> 
</div>
<div class="clearfix"></div>   