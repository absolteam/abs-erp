<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var tempid = "";
var deliveryDetails = "";
$(function(){   
	showActiveDeliveryNotesEdit();
	 
	 $('input,select').attr('disabled', true);  

	 $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.salesInvoicecloseDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 return false;
	 });

	  
		 	

	 $('.editDatainv').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var salesDeliveryNoteId = Number($('#salesDeliveryNoteId_'+rowId).val()); 
     	 var salesInvoiceId = Number($('#salesInvoiceId').val()); 
     	 var currentRowId = getRowId($(slidetab).attr('id'));
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_detail.action", 
		 	async: false,  
		 	data:{salesDeliveryNoteId: salesDeliveryNoteId, salesInvoiceId: salesInvoiceId, currentRowId: currentRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});  
		return false;
	}); 
 
	$('.activeNoteNumberUpdate1').click(function(){   
		validateNoteInfo($(this));   
	});
 	
 	if (Number($('#salesInvoiceId').val()) > 0) {
 		$('.invoice_lines').show();
 		$('#customer').remove();
 		$('.rowid1').each(function(){
	    	var rowid = getRowId($(this).attr('id')); 
	    	var deliveryId = $('#salesDeliveryNoteId_'+rowid).val(); 
	    	$('#activeNoteNumber_'+deliveryId).attr('checked', true); 
	    	$('#noteLabel_'+deliveryId).css('color','#37b134'); 
		 });
 		$('#currency').val($('#tempCurrency').val());
 		$('#creditTermId').val($('#termpCreditTermId').val()); 
 		$('#invoiceType').val('${SALES_INVOICE.invoiceType}'); 
 		$('#salesType').val($('#tempSalesType').val()); 
 		$('#status').val($('#tempStatus').val()); 
	}else {
		$('#status').val(7); 
 		$('#currency').val($('#defaultCurrency').val());
 	}
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function showActiveDeliveryNotes() {
	var customerId = Number($('#customerId').val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_deliverynote.action", 
	 	async: false,  
	 	data:{customerId: customerId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.invoice-info').html(result);   
			 if(typeof($('.activeNoteNumber').attr('id'))=="undefined"){   
				$('.invoice_lines').hide();
				$('.tab').html('');
				$('.invoice-info').find('div').css("color","#ca1e1e").html("No active Delivery Note found for this customer.");
			 }  
		},
		error:function(result){   
		}
	});  
}
function showActiveDeliveryNotesEdit() {
	var salesInvoiceId = Number($('#salesInvoiceId').val()); 
	var customerId = Number($('#customerId').val());  
	 var salesInvoiceIdParent = Number($('#parentInvoiceId').val()); 
 	 if(salesInvoiceIdParent!=null && salesInvoiceIdParent>0)
 		salesInvoiceId=salesInvoiceIdParent;
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_deliverynote_edit.action", 
	 	async: false,  
	 	data:{customerId: customerId,salesInvoiceId:salesInvoiceId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.invoice-info').html(result);   
			 if(typeof($('.activeNoteNumber').attr('id'))=="undefined"){   
				$('.invoice_lines').hide();
				$('.tab').html('');
				$('.invoice-info').find('div').css("color","#ca1e1e").html("No active Delivery Note found for this customer.");
			 }  
		},
		error:function(result){   
		}
	});  
}
function callDefaultNoteSelect(noteObj){
	if (typeof noteObj != 'undefined') {
		var id= Number(1); 
		var salesDeliveryNoteId = getRowId(noteObj); 
		$('#'+noteObj).attr('checked',true);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_session.action", 
		 	async: false,  
		 	data:{salesDeliveryNoteId: salesDeliveryNoteId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.invoice_lines').show();
				$('.tab').html(result);  
				calculateTotalInvoice();
			} 
		});  
	}
	return false;
}
function validateNoteInfo(currentObj){
	var salesDeliveryNoteId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.invoice_lines').show();
		} else {
			$('.invoice_lines').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_session.action",
						async : false,
						data : {
							salesDeliveryNoteId : salesDeliveryNoteId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
						}
					});
		} else {
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var removeDeliveryId = $(
								'#salesDeliveryNoteId_' + rowId).val();
						if (removeDeliveryId == salesDeliveryNoteId) {
							$('#fieldrow_' + rowId).remove();
							return false;
						}
					});
			if ($('.tab tr').size() == 0)
				$('.invoice_lines').hide();
		}
		calculateTotalInvoice();
		return false;
	}
	 
	
	function calculateTotalInvoice(){
		var totalAmount=0.0;
		var linesAmount=0.0;
		var addtionalCharges = 0; 
		$('.rowid1').each(function(){  
			var idvals=getRowId($(this).attr('id')); 
			linesAmount=$.trim($('#deliveryAmount_'+idvals).val()); 
			linesAmount=linesAmount.split(',').join(''); 
			addtionalCharges = Number($.trim($('#addtionalCharges_'+idvals).text()));
			totalAmount = Number(Number(totalAmount) + Number(linesAmount) + Number(addtionalCharges));  
		});  
		$('#totalInvoice').val(totalAmount);
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Sales
			Invoice
		</div> 
		<form id="salesInvoiceValidation" method="POST" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="salesInvoiceId" name="salesInvoiceId"
					value="${SALES_INVOICE.salesInvoiceId}" /> 
				<input type="hidden" id="parentInvoiceId" name="parentInvoiceId"
					value="${SALES_INVOICE.salesInvoice.salesInvoiceId}" /> 
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 130px;"> 
							<div>
								<label class="width30">Customer Name<span
									style="color: red;">*</span> </label>
				
								<input type="text" readonly="readonly" name="customerName"
											id="customerName" class="width50" value="${SALES_INVOICE.customerName}" />
								
								<input type="hidden" id="customerId" name="customerId"
									value="${SALES_INVOICE.customer.customerId}" /> <span
									class="button"> <a
									style="cursor: pointer;" id="customer"
									class="btn ui-state-default ui-corner-all customer-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30">Credit Term</label> <select
									name="creditTerm" id="creditTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${CREDIT_TERM}">
										<option value="${TERMS.creditTermId}">${TERMS.name}</option>
									</c:forEach>
								</select> <input type="hidden" id="termpCreditTermId"
									name="termpCreditTermId"
									value="${SALES_INVOICE.creditTerm.creditTermId}" />
							</div>
							<div>
								<label class="width30"> Status<span style="color: red;">*</span>
								</label> <select name="status" id="status"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${INVOICE_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus" name="tempStatus"
									value="${SALES_INVOICE.status}" />
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${SALES_INVOICE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 130px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${SALES_INVOICE.referenceNumber ne null && SALES_INVOICE.referenceNumber ne ''}">
												${SALES_INVOICE.referenceNumber} 
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${SALES_INVOICE.referenceNumber}"
									name="tempreferenceNumber" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Invoice Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${SALES_INVOICE.invoiceDate ne null && SALES_INVOICE.invoiceDate ne ''}">
										<c:set var="invoiceDate" value="${SALES_INVOICE.invoiceDate}" />
										<input name="invoiceDate" type="text" readonly="readonly"
											id="invoiceDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("invoiceDate").toString())%>"
											class="invoiceDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="invoiceDate" type="text" readonly="readonly"
											id="invoiceDate"
											class="invoiceDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Currency<span
									style="color: red;">*</span>
								</label> <select name="currency" id="currency" class="width51">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${SALES_INVOICE.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div> 
							<div>
								<label class="width30"> Total Invoice </label>
								 <input name="totalInvoice" type="text" readonly="readonly"
										id="totalInvoice" class="totalInvoice width50">
							</div>
							<div>
								<label class="width30"> Paid Invoice </label>
								 <input name="paidInvoice" type="text" readonly="readonly" value="${SALES_INVOICE.paidInvoice}"
										id="paidInvoice" class="paidInvoice width50">
							</div>
							<div>
								<label class="width30"> Invoice Amount<span
									style="color: red;">*</span> </label>
								 <input name="invoiceAmount" type="text" id="invoiceAmount" value="${SALES_INVOICE.invoiceAmount}"
								 	 class="invoiceAmount validate[required, custom[number]] width50">
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="invoice-info" id="hrm">
				<c:if
					test="${DELIVERY_NOTE_INFO ne null && DELIVERY_NOTE_INFO ne ''}">
					<fieldset>
						<legend>Active Notes<span
									class="mandatory">*</span></legend>
						<div>
							<c:forEach var="ACTIVE_NOTE" items="${DELIVERY_NOTE_INFO}">
								<label for="activeNoteNumber_${ACTIVE_NOTE.salesDeliveryNoteId}"
									class="width10"
									id="noteLabel_${ACTIVE_NOTE.salesDeliveryNoteId}"> <input
									type="checkbox" name="activeNoteNumber"
									class="activeNoteNumber activeNoteNumberUpdate1"
									id="activeNoteNumber_${ACTIVE_NOTE.salesDeliveryNoteId}" />
									${ACTIVE_NOTE.referenceNumber} </label>
							</c:forEach>
						</div>
					</fieldset>
				</c:if>
			</div>
			<div class="clearfix"></div>
			<div id="hrm" class="portlet-content width100 invoice_lines"
				style="display: none;">
				<fieldset id="hrm">
					<legend>Invoice Detail<span
									class="mandatory">*</span></legend>
					<div class="portlet-content">
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%;">Delivery NO</th>
										<th style="width: 5%;">Delivery Date</th>
										<th style="width: 5%; display: none;">Delivery Quantity</th>
										<th style="width: 5%;">Addtional Charges</th>
										<th style="width: 5%;">Total Amount</th>
										<th style="width: 5%;">Description</th>
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${SALES_INVOICE_DETAIL ne null && SALES_INVOICE_DETAIL ne ''}">
											<c:forEach var="INVOICE_DETAIL"
												items="${SALES_INVOICE_DETAIL}" varStatus="status">
												<tr class="rowid1" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td>${INVOICE_DETAIL.referenceNumber}</td>
													<td><c:set var="deliveryDate"
															value="${INVOICE_DETAIL.deliveryDate}" /> <%=DateFormat.convertDateToString(pageContext
								.getAttribute("deliveryDate").toString())%></td>
													<td style="display: none;">${INVOICE_DETAIL.totalDeliveryQty}</td>
													<td style="text-align: right;"><c:set
															var="addtionalCharges"
															value="${INVOICE_DETAIL.addtionalCharges}" /> <%=AIOSCommons.formatAmount(pageContext
								.getAttribute("addtionalCharges"))%></td>
													<td style="text-align: right;"><c:set
															var="totalAmount"
															value="${INVOICE_DETAIL.totalDeliveryAmount}" /> <%=AIOSCommons.formatAmount(pageContext
								.getAttribute("totalAmount"))%></td>
													<td>${INVOICE_DETAIL.description}</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}"><input type="hidden"
														id="salesDeliveryNoteId_${status.index+1}"
														value="${INVOICE_DETAIL.salesDeliveryNoteId}" /> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv"
														id="EditImage_${status.index+1}" style="cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a>  
													</td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div> 
			<span class="callJq"></span>
			<div id="temp-result" style="display: none;"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="price-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="price-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>