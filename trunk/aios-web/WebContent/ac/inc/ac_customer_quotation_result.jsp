<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"> 
 var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){ 
	 oTable = $('#CustomerQuotationPopup').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_customer_quotation_jsonlist.action",

		"aoColumns" : [ { 
			"mDataProp" : "referenceNo"
		 }, {
			"mDataProp" : "customerName"
		 }, {
			"mDataProp" : "orderDateFormat"
		 },{
			"mDataProp" : "shippingDateFormat"
		 },{
			"mDataProp" : "expiryDateFormat"
		 },{
			"mDataProp" : "strStatus"
		 }], 
		 "aaSorting": [[0, 'desc']], 
		 "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		 }
	}); 
	 
	/* Click event handler */
	$('#CustomerQuotationPopup tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		loadQuotationDetails(aData.customerQuotationId,aData.referenceNo,aData.creditTermId);  
		$('#customer-quotation-popup').trigger('click');
		return false;
  	});

	$('#CustomerQuotationPopup tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) 
			  $(this).addClass('row_selected');
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
 	      }
	}); 
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>customer quotation</div>	 	 
		<div class="portlet-content"> 
  			 	<div id="customer_popup_list">
					<table id="CustomerQuotationPopup" class="display">
						<thead>
							<tr>
								<th>Reference</th> 
								<th>Customer</th> 
								<th>Quotation Date</th> 
								<th>Shipping Date</th> 
								<th>Expiry Date</th> 
								<th>Status</th>  
							</tr>
						</thead> 
					</table> 
 			</div> 
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
				<div class="portlet-header ui-widget-header float-right" id="customer-quotation-popup" >close</div> 
 			</div>
		</div>
	</div>
</div>