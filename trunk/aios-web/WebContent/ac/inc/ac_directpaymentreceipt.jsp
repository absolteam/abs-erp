<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>DIRECT PAYEMENT</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>DIRECT PAYMENT VOUCHER</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 175px; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<div class="width40 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width30 text-bold">D.P.NO. <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${DIRECT_PAYMENT.paymentNumber}</span></span>  
			</div>
			<div class="width98 div_text_info">
				<span class="float_left left_align width30 text-bold">DATE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="date" value="${DIRECT_PAYMENT.paymentDate}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
				</span>  		
			</div> 
		</div> 
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width30 text-bold">PAYABLE TO<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					<c:choose>
						<c:when test="${DIRECT_PAYMENT.supplier.person ne null && DIRECT_PAYMENT.supplier.person ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.supplier.person.firstName} ${ DIRECT_PAYMENT.supplier.person.lastName}</span>
						</c:when>
						<c:when test="${DIRECT_PAYMENT.supplier.company ne null && DIRECT_PAYMENT.supplier.company ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.supplier.company.companyName}</span>
						</c:when>
						<c:when test="${DIRECT_PAYMENT.supplier.cmpDeptLocation ne null && DIRECT_PAYMENT.supplier.cmpDeptLocation ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyName}</span>
						</c:when>  
						 <c:when test="${DIRECT_PAYMENT.customer.personByPersonId ne null && DIRECT_PAYMENT.customer.personByPersonId ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.customer.personByPersonId.firstName} ${ DIRECT_PAYMENT.customer.personByPersonId.lastName}</span>
						</c:when>
						<c:when test="${DIRECT_PAYMENT.customer.company ne null && DIRECT_PAYMENT.customer.company ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.customer.company.companyName}</span>
						</c:when>
						<c:when test="${DIRECT_PAYMENT.customer.cmpDeptLocation ne null && DIRECT_PAYMENT.customer.cmpDeptLocation ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.customer.cmpDeptLocation.company.companyName}</span>
						</c:when> 
						<c:when test="${DIRECT_PAYMENT.personByPersonId ne null && DIRECT_PAYMENT.personByPersonId ne '' }">
							<span class="width60 view">${DIRECT_PAYMENT.personByPersonId.firstName} ${DIRECT_PAYMENT.personByPersonId.lastName}</span>
						</c:when> 
						<c:otherwise>
							<span class="width60 view">${DIRECT_PAYMENT.others}</span>
						</c:otherwise>
					</c:choose>  
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width30 text-bold">MOBILE NO <span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:choose>
						<c:when test="${DIRECT_PAYMENT.supplier.person ne null && DIRECT_PAYMENT.supplier.person ne ''}">
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.supplier.person.mobile ne null && DIRECT_PAYMENT.supplier.person.mobile ne ''}">
									${DIRECT_PAYMENT.supplier.person.mobile} 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose> 
						</c:when>
						<c:when test="${DIRECT_PAYMENT.supplier.cmpDeptLocation.company ne null && DIRECT_PAYMENT.supplier.cmpDeptLocation.company ne ''}">
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyPhone ne null && DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyPhone ne ''}">
									${DIRECT_PAYMENT.supplier.cmpDeptLocation.company.companyPhone} 
								</c:when> 
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>	
						</c:when>
						<c:otherwise>
							-N/A-
						</c:otherwise>
					</c:choose>
				</span>
			</div>
			 
			<c:choose>
				<c:when test="${DIRECT_PAYMENT.paymentMode eq 1 || DIRECT_PAYMENT.paymentMode eq 5}"> 
					<div class="width100 div_text_info">
						<span class="float_left left_align width28 text-bold">CASH ACCOUNT<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.combination.accountByAnalysisAccountId ne null &&
									DIRECT_PAYMENT.combination.accountByAnalysisAccountId ne ''}">
										${DIRECT_PAYMENT.combination.accountByNaturalAccountId.account}.${DIRECT_PAYMENT.combination.accountByAnalysisAccountId.account}
								</c:when>
								<c:otherwise>
									${DIRECT_PAYMENT.combination.accountByNaturalAccountId.account}
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</c:when>
				<c:when test="${DIRECT_PAYMENT.paymentMode eq 2}">
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">BANK<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${DIRECT_PAYMENT.bankAccount.bank.bankName}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">ACCOUNT NO.<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${DIRECT_PAYMENT.bankAccount.accountNumber}
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CHEQUE NO.<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
							<c:choose> 
								<c:when test="${DIRECT_PAYMENT.chequeNumber ne null && DIRECT_PAYMENT.chequeNumber ne ''}">
									${DIRECT_PAYMENT.chequeNumber}
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span>
					</div>
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CHEQUE DATE<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							<c:choose>
								<c:when test="${DIRECT_PAYMENT.chequeDate ne null && DIRECT_PAYMENT.chequeDate ne ''}">
									<c:set var="chequeDate" value="${DIRECT_PAYMENT.chequeDate}"/>  
									<%=DateFormat.convertDateToString(pageContext.getAttribute("chequeDate").toString())%> 
								</c:when>
								<c:otherwise>
									-N/A-
								</c:otherwise>
							</c:choose>
						</span>
					</div>
				</c:when>
				<c:when test="${DIRECT_PAYMENT.paymentMode eq 3 || DIRECT_PAYMENT.paymentMode eq 4}">
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">CARD NUMBER<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.cardNumber}]
						</span>
					</div>
				</c:when>
				<c:when test="${DIRECT_PAYMENT.paymentMode eq 6}">
					<div class="width100 div_text_info">
						<span class="float_left left_align width30 text-bold">Transfer From<span class="float_right">:</span> </span>
						<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
							${DIRECT_PAYMENT.bankAccount.bank.bankName} [${DIRECT_PAYMENT.bankAccount.accountNumber}]
						</span>
					</div>
				</c:when>
			</c:choose>
			</div> 
	</div>
	<div style="margin-top: 10px; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float-left">
		<table class="width100">
			<tr> 
				<th style="width:2%;">S.NO:</th>
				<th style="width:5%;">INVOICE</th>
				<th class="width30">ACCOUNT CODE</th>
				<th class="width30">DESCRIPTION</th>
				<th class="width10">AMOUNT</th>
			</tr>
			<tbody>  
				<c:choose>
					<c:when test="${DIRECT_PAYMENT_DETAILS ne null && DIRECT_PAYMENT_DETAILS ne ''}">
						<c:set var="totalAmount" value="0"/>
						<tr style="height: 25px;">
							<c:forEach begin="0" end="4" step="1">
								<td/>
							</c:forEach>
						</tr>
						<%double totalAmount=0; %>
						<c:forEach items="${DIRECT_PAYMENT_DETAILS}" var="PAYMENT_DETAIL" varStatus="status"> 
							<tr>
								<td>${status.index+1}</td>
								<td>${PAYMENT_DETAIL.invoiceNumber}</td>
								<td>
									<c:choose>
										<c:when test="${PAYMENT_DETAIL.combination.accountByAnalysisAccountId ne null &&
											PAYMENT_DETAIL.combination.accountByAnalysisAccountId ne ''}">
												${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}.${PAYMENT_DETAIL.combination.accountByAnalysisAccountId.account}
										</c:when>
										<c:otherwise>
											${PAYMENT_DETAIL.combination.accountByNaturalAccountId.account}
										</c:otherwise>
									</c:choose>
								</td>
								<td>${PAYMENT_DETAIL.description}</td> 
								<td class="right_align">
									<c:set var="amount" value="${PAYMENT_DETAIL.amount}"/> 
									<%totalAmount+=Double.parseDouble(pageContext.getAttribute("amount").toString());%>
									<%=AIOSCommons.formatAmount(pageContext.getAttribute("amount"))%> 
								</td>
							</tr>
						</c:forEach> 
						<c:if test="${fn:length(DIRECT_PAYMENT.directPaymentDetails)<5}">
							<c:set var="emptyLoop" value="${5 - fn:length(DIRECT_PAYMENT.directPaymentDetails)}"/> 
							<c:forEach var="i" begin="0" end="${emptyLoop}" step="1">
								<tr style="height: 25px;">
									<c:forEach begin="0" end="4" step="1">
										<td/>
									</c:forEach>
								</tr>
							</c:forEach>
						</c:if> 
						<tr>
							<td colspan="2" class="bottomTD" style="font-weight: bold;">TOTAL AMOUNT</td>
							<td colspan="2" class="bottomTD left_align" style="font-weight: bold;"> 
								<%
								 	String decimalAmount = String.valueOf(AIOSCommons
								 					.formatAmount(totalAmount));
								 			decimalAmount = decimalAmount.substring(decimalAmount
								 					.lastIndexOf('.') + 1);
								 			String amountInWords = AIOSCommons
								 					.convertAmountToWords(totalAmount);
								 			if (Double.parseDouble(decimalAmount) > 0) {
								 				amountInWords = amountInWords.concat(" and ") 
								 						.concat(AIOSCommons
								 								.convertAmountToWords(decimalAmount))
								 						.concat(" Fils ");
								 				String replaceWord = "Only";
								 				amountInWords = amountInWords.replaceAll(replaceWord,
								 						"");
								 				amountInWords = amountInWords.concat(" " + replaceWord);
								 			}
								 %> <%=amountInWords%>
							</td> 
							<td class="right_align bottomTD" style="font-weight: bold;">
								<%=AIOSCommons.formatAmount(totalAmount)%> 
							</td> 
						</tr>
						<tr>
							<td colspan="5">
								<table style="border: 0px;">
									<tbody> 
									<tr>
										<td colspan="3" style="border: 0px; border-bottom: 2px double #000; border-width: 3px; font-weight: bold;">ACCOUNT DESCRIPTION</td>
									</tr> 
									<tr> 
										<th>CHART OF ACCOUNTS</th>
										<th>DEBIT</th>
										<th>CREDIT</th> 
									</tr> 
									 <c:forEach var="PAYMENT_DETAIL" items="${DIRECTPAYMENT_TRANSACTIONS}">
 										 <tr style="height: 30px;">
										 	<td>
										 		${PAYMENT_DETAIL.accountDescription}
										 	</td> 
										 	<c:choose>
										 		<c:when test="${PAYMENT_DETAIL.debitAmount ne null &&  PAYMENT_DETAIL.debitAmount ne ''}">
											 		<td class="right_align">
											 			${PAYMENT_DETAIL.debitAmount}
											 		</td>
											 		<td/> 
											 	</c:when>
											 	<c:otherwise>
											 		<td/> 
											 		<td class="right_align">
											 			${PAYMENT_DETAIL.creditAmount}
											 		</td> 
											 	</c:otherwise>
										 	</c:choose>  
										</tr>
									 </c:forEach> 
									</tbody>
								</table>
							</td> 
						</tr>
					</c:when>
				</c:choose> 
			</tbody>
		</table>
	</div> 
	<div class="width100  div_text_info">
		<span class="float_left left_align width20 text-bold">DESCRIPTION<span class="float_right">:</span> </span>
		<span class="span_border left_align text_info" style="width:77%;display: -moz-inline-stack;">
			<c:choose>
				<c:when test="${DIRECT_PAYMENT.description ne null && DIRECT_PAYMENT.description ne ''}">
					${DIRECT_PAYMENT.description}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose>  
		</span>
	</div>
	<div class="width100 float_left div_text_info" style="margin-top: 50px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Prepared by:</span> 
			</div> 
			<div class="width40 float_left">
				<span>Recommended by:</span> 
			</div>
			<div class="width30 float_left">
				<span>Approved by:</span> 
			</div>
		</div>
		<div class="width100 float_left" style="margin-top: 30px;">
			<div class="width30 float_left">
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width40 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
			<div class="width30 float_left"> 
				<span class="span_border width70" style="display: -moz-inline-stack;"></span>
			</div>
		</div>
	</div>
	<div class="width100 float_left div_text_info" style="margin-top: 20px;">
		<div class="width100 float_left">
			<div class="width30 float_left">
				<span>Received by:</span> 
			</div>  
		</div>
	</div>
	</body>
</html>