<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var tempid = "";
var deliveryDetails = "";
$(function(){   
	 
	$jquery("#salesInvoiceValidation").validationEngine('attach'); 
	$jquery(".totalInvoice,.paidInvoice,.invoiceAmount").number(true, 2); 
	$('#invoiceDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	
	$('.discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_continious_sales_invoice.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#price-common-popup').dialog('destroy');		
				$('#price-common-popup').remove(); 
				$('#DOMWindow').remove();
				$('#DOMWindowOverlay').remove();
				$("#main-wrapper").html(result); 
			} 
		});  
	 });  

	$('.unitrate').live('change',function(){ 
		var currentRowId = getRowId($(this).attr('id')); 
		var unitRate = Number($('#amount_'+currentRowId).val().split(',').join(''));
		var invoiceQty = Number($('#invoiceQty_'+currentRowId).val());
		$('#total_'+currentRowId).text(Number(invoiceQty * unitRate).toFixed(2));
	});

	$('.invoiceQty').live('change',function(){ 
		$('.quantityerr').remove(); 
		var currentRowId = getRowId($(this).attr('id')); 
		var currentProductId = Number($('#productid_'+currentRowId).val());
		var invoiceQty = Number($('#invoiceQty_'+currentRowId).val());
 		var deliveryNoteDetailId = $('#deliveryNoteDetailId_'+currentRowId).val();
		var deliveryQty = Number($('#deliveryqty_'+currentRowId).text());
		var unitRate = Number($('#amount_'+currentRowId).val().split(',').join(''));
		var otherInvoiceQty = 0;
 		$('.rowidrc').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var detailId = Number($('#deliveryNoteDetailId_'+rowId).val());
			var productId = Number($('#productid_'+rowId).val());
			if(currentRowId != rowId && detailId == deliveryNoteDetailId && productId == currentProductId){
				otherInvoiceQty += Number($('#invoiceQty_'+rowId).val()); 
			} 
		});
		var totalInvoiceQty = Number(invoiceQty + otherInvoiceQty);
 		if(totalInvoiceQty > deliveryQty){
 			$(this).parent().append("<span class='quantityerr' id=inverror_"+currentRowId+">Invoice Qty should be lt or eq delivery qty.</span>"); 
 			$('#invoiceQty_'+currentRowId).val($('#invoiceHiddenQty_'+currentRowId).val());
 	 	} 
		else
			$('#total_'+currentRowId).text(Number(invoiceQty * unitRate).toFixed(2));
		return false;
	});

	$('.delrowinv').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
    	 var deleteId = getRowId($(slidetab).attr('id')); 
    	 var deleteDeliveryId = $('#salesDeliveryNoteId_'+deleteId).val();  
    	 $('.activeNoteNumber').each(function(){   
    		var deliveryId = getRowId($(this).attr('id')); 
    		 if(deleteDeliveryId == deliveryId){
				$(this).attr('checked',false);
				 return false;
			 }
    	 });
    	 $(slidetab).remove();  
    	 var i=1;
 	 	 $('.rowid').each(function(){   
 			 var rowId=getRowId($(this).attr('id')); 
 		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
 	 	if($('.tab tr').size()==0)
			$('.invoice_lines').hide();
		 return false;
	 }); 

	$('.delrow_rc').live('click',function(){
		 slidetab=$(this).parent().parent().get(0);  
		 var idval = getRowId($(this).parent().attr('id')); 
		 var deleteNoteId = $('#deliveryNoteId_'+idval).val();
		 $(slidetab).remove();  
		 var i=0;
		 $('.rowidpo').each(function(){   
 			 var rowId=getRowId($(this).attr('id')); 
 		 	 $('#lineIdrc_'+rowId).html(i);
			 i=i+1;   
		 });  
		 if($('.tab-1 tr').size()==0){ 
			 $('.activeNoteNumber').each(function(){   
	      		var deliveryId = getRowId($(this).attr('id'));  
	      		 if(deleteNoteId == deliveryId){
					$(this).attr('checked', false);
					$('.rowid').each(function(){   
			   			 var rowId = getRowId($(this).attr('id')); 
			   		 	 var pageNoteId = $('#salesDeliveryNoteId_'+rowId).val();
			   		 	 if(pageNoteId == deliveryId) {  
			   		 		$(this).remove();
				   		 }
					 }); 
					 return false;
				 }
	      	 });
			 
			 if($('.tab tr').size()==0)
			 	$('.invoice_lines').hide();
			 $('#DOMWindow').remove();
			 $('#DOMWindowOverlay').remove();
		 } 
	 });

	 $('#salesinvoice_save').click(function(){ 
 	 	if($jquery("#salesInvoiceValidation").validationEngine('validate')){
	 			var salesInvoiceId = Number($('#salesInvoiceId').val());  
 	 			var invoiceDate = $('#invoiceDate').val(); 
   	 			var customerId = Number($('#customerId').val());  
 	 			var status = Number($('#status').val()); 
 	 			var creditTerm = Number($('#creditTerm').val());  
 	 			var description=$('#description').val();  
	 			var currencyId = Number($('#currency').val());  
	 			var invoiceAmount = $jquery('#invoiceAmount').val();
	 			var invoiceId = Number($('#invoiceId').val()); 
 	 			$.ajax({
 					type:"POST",
 					url:"<%=request.getContextPath()%>/save_continuous_salesinvoice.action", 
 				 	async: false, 
 				 	data:{	salesInvoiceId: salesInvoiceId, invoiceDate: invoiceDate, status: status, customerId: customerId, currencyId:currencyId,
 				 			creditTerm: creditTerm, description: description, invoiceAmount: invoiceAmount, invoiceId: invoiceId
 					 	 },
 				    dataType: "json",
 				    cache: false,
 					success:function(response){   
 						 if(response.returnMessage=="SUCCESS"){
	 						var message ="Record created.";
 							if(salesInvoiceId > 0)
 								message ="Record updated.";
 							 $.ajax({
 									type:"POST",
 									url:"<%=request.getContextPath()%>/show_continious_sales_invoice.action", 
 								 	async: false,
 								    dataType: "html",
 								    cache: false,
 									success:function(result){
 										$('#common-popup').dialog('destroy');		
 										$('#common-popup').remove();  
 										$('#price-common-popup').dialog('destroy');		
 										$('#price-common-popup').remove(); 
 										$('#DOMWindow').remove();
 										$('#DOMWindowOverlay').remove();
 										$("#main-wrapper").html(result); 
 										$('#success_message').hide().html(message).slideDown(1000); 
 										$('#success_message').delay(2000).slideUp();
 									}
 							 });
 						 } 
 						 else{
 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
 							 $('#page-error').delay(2000).slideUp();
 							 return false;
 						 }
 					},
 					error:function(response){  
 						$('#page-error').hide().html("Internal error.").slideDown(1000);
 						$('#page-error').delay(2000).slideUp();
 					}
 				}); 
	 		}else{
	 			return false;
	 		}
	 	}); 

	 $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 

	$('.salesInvoicecloseDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
		 return false;
	 });

	$('.salesInvoicesessionSave').live('click',function(){  
		 deliveryDetails = getDeliveryDetails();  
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_deliverydetails_session.action", 
		 	async: false,  
		 	data:{salesDeliveryDetails: deliveryDetails},
		    dataType: "json",
		    cache: false,
			success:function(response){  
				if(response.returnMessage == "SUCCESS"){
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove();
				}else{
					$('#page-error-popup').hide().html(response.returnMessage).slideDown();
					$('#page-error-popup').delay(2000).slideUp();
					return false;
				}
			} 
		});
		 calculateTotalInvoice();
		return false;
	 });

	var getDeliveryDetails = function(){ 
		 var invoiceArr = new Array(); 
		 var deliveryDetailArr = new Array();  
		 var unitRateArr = new Array();
		 var invDetailArray = new Array();
		 deliveryDetails = "";
		 var totalInvoiceAmount = 0;
		 var currentRowId = $('#currentRowId').val();
		 $('.rowidrc').each(function(){ 
			 var rowid = getRowId($(this).attr('id'));  
			 var invoiceQty =  Number($('#invoiceQty_'+rowid).val());  
			 var deliveryNoteDetailId = $('#deliveryNoteDetailId_'+rowid).val(); 
			 var unitRate = Number($('#amount_'+rowid).val().split(',').join(''));
			 var invoiceDetailId = Number($('#invocieLineId_'+rowid).val()); 
			 if(typeof deliveryNoteDetailId != 'undefined' && deliveryNoteDetailId!=null && deliveryNoteDetailId!=""){ 
				 invoiceArr.push(invoiceQty);
				 deliveryDetailArr.push(deliveryNoteDetailId);  
				 unitRateArr.push(unitRate);  
				 invDetailArray.push(invoiceDetailId);  
				 totalInvoiceAmount += invoiceQty * unitRate;
			 }
		 });
		 for(var j=0;j<deliveryDetailArr.length;j++){ 
			 deliveryDetails += invoiceArr[j]+"__"+deliveryDetailArr[j]+"__"+unitRateArr[j]+"__"+invDetailArray[j];
			if(j==deliveryDetailArr.length-1){   
			}
			else{
				deliveryDetails += "#@";
			}
		}  
		$('#deliveryAmount_'+currentRowId).val(totalInvoiceAmount);
		return deliveryDetails;
	 };
	 
		 	

	 $('.editDatainv').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
     	 var salesDeliveryNoteId = Number($('#salesDeliveryNoteId_'+rowId).val()); 
     	 var salesInvoiceId = Number($('#salesInvoiceId').val()); 
     	 var currentRowId = getRowId($(slidetab).attr('id'));
     	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_detail.action", 
		 	async: false,  
		 	data:{salesDeliveryNoteId: salesDeliveryNoteId, salesInvoiceId: salesInvoiceId, currentRowId: currentRowId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click'); 
			} 
		});  
		return false;
	});
	 	
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.delrowcharges').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidcharges').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#chargesLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	 $('.delrowpacks').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidpacks').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#packingLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('#customer-common-popup').live('click',function(){  
    	 $('#common-popup').dialog('close'); 
     });

 	$('.customer-common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		tempid = $(this).parent().get(0);    
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
		 	async: false,  
		 	data: {pageInfo: ""},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $($($('#common-popup').parent()).get(0)).css('top',0);
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 

	$('.price-common-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();  
		tempid = $(this).parent().get(0);    
		var rowId = getRowId($(slidetab).attr('id'));
		var customerId = Number($('#customerId').val());
		var productId = Number($('#productid_'+rowId).val());
		var storeId = Number($('#storeid_'+rowId).val());
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_common_product_pricing.action", 
		 	async: false,  
		 	data: {pageInfo: "sales_invoice", rowId: rowId, customerId: customerId, productId: productId, storeId: storeId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.price-common-result').html(result);  
 			},
			error:function(result){   
				 $('.price-common-result').html(result); 
			}
		});  
		return false;
	});
	
	$('#invoiceAmount').change(function(){
		 var invoiceAmount =  Number($jquery('#invoiceAmount').val());
		 var pendedInvoice = Number($('#pendedInvoice').val());
		 if(invoiceAmount > pendedInvoice){
			 $('#invoiceAmount').removeClass('validate[required,custom[number]]').addClass('validate[required,custom[number],max['+pendedInvoice+']]'); 
		 }else{
			 $('#invoiceAmount').removeClass('validate[required,custom[number],max['+pendedInvoice+']]').addClass('validate[required,custom[number]]'); 
		 } 
	  });

	$('#price-common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:600, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: false 
	}); 

	$('.activeNoteNumberUpdate').click(function(){   
		validateNoteInfo($(this));   
	});
 	
 	if (Number($('#salesInvoiceId').val()) > 0) {
 		$('.invoice_lines').show();
 		$('#customer').remove();
 		$('.rowid').each(function(){
	    	var rowid = getRowId($(this).attr('id')); 
	    	var deliveryId = $('#salesDeliveryNoteId_'+rowid).val(); 
	    	$('#activeNoteNumber_'+deliveryId).attr('checked', true); 
	    	$('#noteLabel_'+deliveryId).css('color','#37b134'); 
		 });
 		$('#currency').val($('#tempCurrency').val());
 		$('#creditTermId').val($('#termpCreditTermId').val()); 
 		$('#invoiceType').val('${SALES_INVOICE.invoiceType}'); 
 		$('#salesType').val($('#tempSalesType').val()); 
 		$('#status').val($('#tempStatus').val()); 
	}else {
		$('#status').val(7); 
		$('#currency').val($('#tempCurrency').val());
 	}
 	$('#currency').attr('disabled', true);
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
} 
function showActiveDeliveryNotes() {
	var customerId = Number($('#customerId').val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_customer_deliverynote.action", 
	 	async: false,  
	 	data:{customerId: customerId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.invoice-info').html(result);   
			 if(typeof($('.activeNoteNumber').attr('id'))=="undefined"){   
				$('.invoice_lines').hide();
				$('.tab').html('');
				$('.invoice-info').find('div').css("color","#ca1e1e").html("No active Delivery Note found for this customer.");
			 }  
		},
		error:function(result){   
		}
	});  
}

function callDefaultNoteSelect(noteObj){
	if (typeof noteObj != 'undefined') {
		var id= Number(1); 
		var salesDeliveryNoteId = getRowId(noteObj); 
		$('#'+noteObj).attr('checked',true);
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_session.action", 
		 	async: false,  
		 	data:{salesDeliveryNoteId: salesDeliveryNoteId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.invoice_lines').show();
				$('.tab').html(result);  
				calculateTotalInvoice();
			} 
		});  
	}
	return false;
}
function validateNoteInfo(currentObj){
	var salesDeliveryNoteId = getRowId($(currentObj).attr('id')); 
	var id= Number(0); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.invoice_lines').show();
		} else {
			$('.invoice_lines').show();
			++id;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/get_deliverynote_session.action",
						async : false,
						data : {
							salesDeliveryNoteId : salesDeliveryNoteId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
						}
					});
		} else {
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var removeDeliveryId = $(
								'#salesDeliveryNoteId_' + rowId).val();
						if (removeDeliveryId == salesDeliveryNoteId) {
							$('#fieldrow_' + rowId).remove();
							return false;
						}
					});
			if ($('.tab tr').size() == 0)
				$('.invoice_lines').hide();
		}
		calculateTotalInvoice();
		return false;
	}
	function triggerPriceCalc(rowId) {
		var unitRate = Number($('#amount_' + rowId).val().replace(/,/g, '')
				.trim());
		var invoicedQty = Number($('#invoiceQty_' + rowId).val().trim());
		$('#total_' + rowId).text(Number(invoicedQty * unitRate).toFixed(2));  
	}

	function commonCustomerPopup(customerId, customerName, combinationId,
			accountCode, creditTermId, commonParam) {
		$('#customerId').val(customerId);
		$('#customerName').val(customerName);
 		$('#creditTerm').val(creditTermId);
		showActiveDeliveryNotes();
 	}
	
	function calculateTotalInvoice(){
		var totalAmount=0.0;
		var linesAmount=0.0;
		$('.rowid').each(function(){  
			var idvals=getRowId($(this).attr('id')); 
			linesAmount=$.trim($('#deliveryAmount_'+idvals).val()); 
			linesAmount=linesAmount.split(',').join(''); 
			totalAmount=Number(Number(totalAmount)+Number(linesAmount));  
		});  
		$jquery('#totalInvoice').val(totalAmount);
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Sales
			Continuous Invoice
		</div> 
		<form id="salesInvoiceValidation" method="POST" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="salesInvoiceId" name="salesInvoiceId"
					value="${SALES_INVOICE.salesInvoice.salesInvoiceId}" />
				<input type="hidden" id="invoiceId" name="invoiceId"
					value="${SALES_INVOICE.salesInvoiceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 155px;"> 
							<div>
								<label class="width30">Customer Name<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${SALES_INVOICE.customer.personByPersonId ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${SALES_INVOICE.customer.personByPersonId.firstName} ${SALES_INVOICE.customer.personByPersonId.lastName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:when test="${SALES_INVOICE.customer.company ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${SALES_INVOICE.customer.company.companyName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:when
										test="${SALES_INVOICE.customer.cmpDeptLocation ne null}">
										<input type="text" readonly="readonly" name="customerName"
											value="${SALES_INVOICE.customer.cmpDeptLocation.company.companyName}"
											id="customerName" class="width50 validate[required]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="customerName"
											id="customerName" class="width50" />
									</c:otherwise>
								</c:choose>
								<input type="hidden" id="customerId" name="customerId"
									value="${SALES_INVOICE.customer.customerId}" />  
							</div>
							<div>
								<label class="width30">Credit Term</label> <select
									name="creditTerm" id="creditTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${CREDIT_TERM}">
										<option value="${TERMS.creditTermId}">${TERMS.name}</option>
									</c:forEach>
								</select> <input type="hidden" id="termpCreditTermId"
									name="termpCreditTermId"
									value="${SALES_INVOICE.creditTerm.creditTermId}" />
							</div>
							<div>
								<label class="width30"> Status<span style="color: red;">*</span>
								</label> <select name="status" id="status"
									class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${INVOICE_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus" name="tempStatus"
									value="${SALES_INVOICE.status}" />
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${SALES_INVOICE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="min-height: 155px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label> <span
									style="font-weight: normal;" id="referenceNumber">  
										${requestScope.referenceNumber}
									 </span> 
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Invoice Date<span
									style="color: red;">*</span> </label> 
								<input name="invoiceDate" type="text" readonly="readonly"
									id="invoiceDate"
									class="invoiceDate validate[required] width50"> 
							</div>
							<div>
								<label class="width30"> Currency<span
									style="color: red;">*</span>
								</label> <select name="currency" id="currency" class="width51">
									<option value="">Select</option>
									<c:forEach var="CURRENCY" items="${CURRENCY_ALL}">
										<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCurrency" name="tempCurrency"
									value="${SALES_INVOICE.currency.currencyId}" /> <input
									type="hidden" id="defaultCurrency" name="defaultCurrency"
									value="${requestScope.DEFAULT_CURRENCY}" />
							</div> 
							<div>
								<label class="width30"> Total Invoice </label>
								 <input name="totalInvoice" type="text" readonly="readonly" value="${SALES_INVOICE_INFO.totalInvoice}"
										id="totalInvoice" class="totalInvoice width50">
							</div> 
							<div>
								<label class="width30"> Paid Invoice<span
									style="color: red;">*</span> </label>
								 <input name="paidInvoice" type="text" readonly="readonly" id="paidInvoice" value="${SALES_INVOICE_INFO.invoiceAmount}"
								 	 class="invoiceAmount width50">
								 	 <input type="hidden" name="pendedInvoice" value="${SALES_INVOICE_INFO.pendingInvoice}"id="pendedInvoice"> 
							</div>
							<div>
								<label class="width30">Pending Amount<span
									style="color: red;">*</span> </label>
								 <input name="invoiceAmount" type="text" id="invoiceAmount" value="${SALES_INVOICE_INFO.pendingInvoice}"
								 	 class="invoiceAmount validate[required, custom[number]] width50">
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div> 
			<div id="hrm" class="portlet-content width100 invoice_lines">
				<fieldset id="hrm">
					<legend>Invoice Detail<span
									class="mandatory">*</span></legend>
					<div class="portlet-content">
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%;">Delivery NO</th>
										<th style="width: 5%;">Delivery Date</th>
										<th style="width: 5%;">Delivery Quantity</th>
										<th style="width: 5%;">Addtional Charges</th>
										<th style="width: 5%;">Total Amount</th>
										<th style="width: 5%;">Description</th>
										<th style="width: 1%; display: none;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${SALES_DELIVERY ne null && SALES_DELIVERY ne ''}">
											<c:forEach var="INVOICE_DETAIL"
												items="${SALES_DELIVERY}" varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td>${INVOICE_DETAIL.referenceNumber}</td>
													<td>${INVOICE_DETAIL.dispatchDate}</td>
													<td>${INVOICE_DETAIL.totalDeliveryQty}</td>
													<td style="text-align: right;">${INVOICE_DETAIL.addtionalCharges}</td>
													<td style="text-align: right;"><c:set
															var="totalAmount"
															value="${INVOICE_DETAIL.totalDeliveryAmount}" /> <%=AIOSCommons.formatAmount(pageContext
								.getAttribute("totalAmount"))%></td>
													<td>${INVOICE_DETAIL.description}</td>
													<td style="width: 0.01%; display: none;" class="opn_td"
														id="option_${status.index+1}"><input type="hidden"
														id="salesDeliveryNoteId_${status.index+1}"
														value="${INVOICE_DETAIL.salesDeliveryNoteId}" /> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatainv"
														id="EditImage_${status.index+1}" style="cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowinv"
														id="DeleteImage_${status.index+1}"
														style="cursor: pointer;" title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a>
													</td>
												</tr>
											</c:forEach>
										</c:when>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="salesinvoice_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<span class="callJq"></span>
			<div id="temp-result" style="display: none;"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="price-common-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="price-common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>