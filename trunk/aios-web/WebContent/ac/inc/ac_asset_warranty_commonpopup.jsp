<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var assetWarrantyId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
 	 oTable = $('#AssetWarrantyPopupDT').dataTable({ 
		"bJQueryUI" : true,
		"sPaginationType" : "full_numbers",
		"bFilter" : true,
		"bInfo" : true,
		"bSortClasses" : true,
		"bLengthChange" : true,
		"bProcessing" : true,
		"bDestroy" : true,
		"iDisplayLength" : 10,
		"sAjaxSource" : "show_allasset_warranty.action",

		"aoColumns" : [ { 
			"mDataProp" : "assetName"
		 },{ 
			"mDataProp" : "warrantyNumber"
		 },{ 
			"mDataProp" : "warrantyType"
		 },{
			"mDataProp" : "fromDate"
		 },{
			"mDataProp" : "toDate"
		 }]
	});	 
	 
	/* Click event handler */
	$('#AssetWarrantyPopupDT tbody tr').live('dblclick', function () {  
		aData = oTable.fnGetData(this); 
		assetWarrantyPopupResult(aData.assetWarrantyId, aData.warrantyNumber);
		 $('#common-popup').dialog("close");
  	});

	$('#AssetWarrantyPopupDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) 
			  $(this).addClass('row_selected');
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	      }
	}); 
});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>claim
			warranty
		</div>
		<div class="portlet-content"> 
			<div id="asset_warranty_list">
				<table id="AssetWarrantyPopupDT" class="display">
					<thead>
						<tr>
							<th>Asset Name</th>
								<th>Warranty Number</th>
								<th>Warranty Type</th>
								<th>Start Date</th>
								<th>End Date</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="commom-warranty-close">close</div>
			</div>
		</div>
	</div>
</div>