<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="hidden" name="productId" id="productid_${requestScope.rowId}" />
		<span id="product_${requestScope.rowId}"></span> <span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all requisition-product-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" name="quantity" id="quantity_${requestScope.rowId}"
		class="quantity validate[optional,custom[number]] width80"></td>
	<td><input type="text" name="description" id="description_${requestScope.rowId}"
		value="${DETAIL.description}" class="description width98" /></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;" title="Working"> <span
			class="processing"></span> </a> <input type="hidden"
		name="requisitionDetailId" id="requisitionDetailId_${requestScope.rowId}" /></td>
</tr>