<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td style="display: none;" id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><input type="hidden" name="productId" id="productid_${ROW_ID}" />
		<span id="product_${ROW_ID}"></span> <span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${ROW_ID}"
			class="btn ui-state-default ui-corner-all show-productpricing-list width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span>
	</td>
	<c:if test="${sessionScope.unified_price eq 'false' 
		&& requestScope.productPricingId gt 0}">
		<td></td>
	</c:if>
	<td><input type="text" name="basicCostPrice"
		id="basicCostPrice_${ROW_ID}" class="basicCostPrice width80">
	</td>
	<td><input type="text" name="standardPrice"
		id="standardPrice_${ROW_ID}"
		class="standardPrice validate[optional,custom[number]] width80">
	</td>
	<td><input type="text" name="suggestedRetailPrice"
		id="suggestedRetailPrice_${ROW_ID}"
		class="suggestedRetailPrice  validate[optional,custom[number]] width80">
	</td>
	<td><input type="text" name="sellingPrice"
		id="sellingPrice_${ROW_ID}"
		class="sellingPrice validate[optional,custom[number]] width80">
	</td>
	<td><input type="text" name="description"
		id="description_${ROW_ID}" class="description width98">
	</td>
	<td>
		<div class="portlet-header ui-widget-header float-left editData"
			style="cursor: pointer; display: none;"
			id="EditImage_${requestScope.ROW_ID}">Define Price</div></td>
	<td style="width: 1%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		name="productPricingDetailId" id="productPricingDetailId_${ROW_ID}" />
		<input type="hidden" name="storeDetailId" id="storedetailid_${ROW_ID}" />
	</td>
</tr>
