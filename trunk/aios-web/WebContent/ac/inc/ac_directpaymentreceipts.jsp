<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@page import="com.aiotech.aios.accounts.domain.entity.DirectPayment"%>
<%@page import="com.aiotech.aios.accounts.domain.entity.DirectPaymentDetail"%>
<%@page import="com.aiotech.aios.accounts.domain.entity.Supplier"%>
<%@page import="com.aiotech.aios.hr.domain.entity.CmpDeptLoc"%>
<%@page import="com.aiotech.aios.hr.domain.entity.Person"%>
<%@page import="com.aiotech.aios.hr.domain.entity.Company"%>
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@page import="com.aiotech.aios.common.to.Constants"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-core/jquery-1.4.2.min.js"></script>
</head>
<body>	
	<% 
		DirectPayment directPayment = (DirectPayment)ServletActionContext.getRequest().getAttribute("DIRECT_PAYMENT");
		Boolean SecondCopy = false;
		String type = "";
		String invNo = "";
		String account = "";
		String amounts = "";
		String name="";
		String temp="";
		String payDate="";
		double amount=0d;
	%>
</body>
</html>