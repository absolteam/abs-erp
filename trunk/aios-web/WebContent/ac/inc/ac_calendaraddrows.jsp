<tr id="fieldrow_${requestScope.rowId}" class="rowFinancialid">
	<td class="width20" id="name_${requestScope.rowId}"></td>
    <td class="width5" id="startTime_${requestScope.rowId}"></td>
    <td class="width5" id="endTime_${requestScope.rowId}"></td>
    <td class="width5" id="extention_${requestScope.rowId}"></td>
    <td class="width5" id="periodstatus_${requestScope.rowId}"></td>
    <td style="display:none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td> 
	<td  style="width:2%;" id="option_${requestScope.rowId}">
		<input type="hidden" name="periodId_${requestScope.rowId}" id="periodId_${requestScope.rowId}"  value="0"/>   
		<input type="hidden" name="statusline" id="statusline_${requestScope.rowId}"/>  
	  	<a style="cursor: pointer;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addFinancialData" id="AddFinancialImage_${requestScope.rowId}"  title="Add Record">
	 	 <span class="ui-icon ui-icon-plus"></span>
	   </a>	
	   <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editFinancialData"  id="EditFinancialImage_${requestScope.rowId}" style="display:none; cursor: pointer;" title="Edit Record">
			<span class="ui-icon ui-icon-wrench"></span>
		</a> 
		<a style="cursor: pointer;display:none;" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delFinancialrow"  id="DeleteFinancialImage_${requestScope.rowId}" title="Delete Record">
			<span class="ui-icon ui-icon-circle-close"></span>
		</a>
		<a href="#" class="btn_no_text del_btn ui-state-default ui-corner-all tooltip" id="WorkingImage_${requestScope.rowId}" style="display:none;" title="Working">
			<span class="processing"></span>
		</a>
	</td> 
</tr>