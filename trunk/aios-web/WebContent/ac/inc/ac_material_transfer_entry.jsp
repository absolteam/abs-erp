<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
var transferPerson = Number(0);
var materialTransferId = Number(0);
var slidetab = "";
var transferDetails = [];
var detailRowId = 0;
var accessCode = "";
var storeAccess ="";
var storeId = 0;
$(function(){
	manupulateLastRow();
	$jquery("#materialTransferValidation").validationEngine('attach');   

	$('#transferDate').datepick({ 
	    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	
	 $('#material_transfer_discard').click(function(event){  
		 materialTransferDiscard("");
		 return false;
	 });

	 $('#material_transfer_save').click(function(){
		 transferDetails = [];
		 requisitionDetails = [];
		 if($jquery("#materialTransferValidation").validationEngine('validate')){
 		 		
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var transferDate = $('#transferDate').val(); 
 	 			var description=$('#description').val();  
 	 			var materialTransferId = Number($('#materialTransferId').val());
  	 			if(storeId == 0) 
 	 				transferDetails = getTransferDetails(); 
 	 			else{ 
 					 $('.activeRQNumber').each(function(){
 						 if($(this).attr('checked')){ 
 							requisitionDetails.push(getRowId($(this).attr('id')));
 						 }
 					 });
 	 	 		}
 	 	 		 
 	 	 		var checkStore = validateTransfers(); 
 	 	 		if(checkStore == true){
 	 	 			if((transferDetails!=null && transferDetails!="") || storeId > 0){
 		 				$.ajax({
 		 					type:"POST",
 		 					url:"<%=request.getContextPath()%>/save_material_transfer.action", 
 		 				 	async: false, 
 		 				 	data:{	materialTransferId: materialTransferId, referenceNumber: referenceNumber, transferDate: transferDate, 
 		 				 			transferPerson: transferPerson, description: description, transferDetails: JSON.stringify(transferDetails),
 		 				 			requisitionDetails: requisitionDetails.join(), storeId: storeId
 		 					 	 },
 		 				    dataType: "json",
 		 				    cache: false,
 		 					success:function(response){   
 		 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
 		 							materialTransferDiscard(materialTransferId > 0 ? "Record updated.":"Record created.");
 		 						 else{
 		 							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
 		 							 $('#page-error').delay(2000).slideUp();
 		 							 return false;
 		 						 }
 		 					},
 		 					error:function(result){  
 		 						$('#page-error').hide().html("Internal error.").slideDown(1000);
 		 						$('#page-error').delay(2000).slideUp();
 		 					}
 		 				}); 
 		 			}else{
 		 				$('#page-error').hide().html("Please enter material transfer details.").slideDown(1000);
 		 				$('#page-error').delay(2000).slideUp();
 		 				return false;
 		 			}
 	 	 		}else{
 	 	 			$('#page-error').hide().html("Product can't be transfered in same shelf.").slideDown(1000);
		 			$('#page-error').delay(2000).slideUp();
		 			return false;
 	 	 		} 
	 		}else{
	 			return false;
	 		}
	 	}); 

	 var validateTransfers = function(){
		 var checkStore = true;
		 $('.rowid').each(function(){ 
			 var rowId = getRowId($(this).attr('id'));  
			 var storeFromId = Number($('#storeFromId_'+rowId).val());  
			 var shelfId = Number($('#shelfId_'+rowId).val());  
			 if(storeFromId > 0 && shelfId > 0 && storeFromId == shelfId){
				 checkStore = false;
				 return checkStore;
			 }
		 });
		 return checkStore;
	 };
	 
	  var getTransferDetails = function(){
		  transferDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val()); 
				var storeFromId = Number($('#storeFromId_'+rowId).val());  
				var shelfId = Number($('#shelfId_'+rowId).val()); 
 				var productQty = Number($('#productQty_'+rowId).val()); 
				var amount = Number($('#amount_'+rowId).val()); 
				var transferType = Number($('#transferType_'+rowId).val()); 
				var transferReason = Number($('#transferReason_'+rowId).val()); 
				var linesDescription = $.trim($('#linesDescription_'+rowId).val());  
				var materialTransferDetailId =  Number($('#materialTransferDetailId_'+rowId).val()); 
				if(typeof productId != 'undefined' && productId > 0 && shelfId > 0 && productQty > 0
						 && transferType > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"transferType" : transferType,
						"transferReason" : transferReason,
						"storeFromId" : storeFromId,
						"shelfId" : shelfId,
						"productQty" : productQty,
						"amount": amount,
						"linesDescription": linesDescription,
						"materialTransferDetailId": materialTransferDetailId,
						"materialRequisitionDetailId": Number(0)
					});   
					transferDetails.push({
						"transferDetails" : jsonData
					});
				} 
			});  
			return transferDetails;
		 };

	 $('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });

	 $('#product-stock-popup').live('click',function(){  
		 $('#common-popup').dialog('close'); 
	 });

	 $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });

	 $('.material_product_list').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_noncraftservice_product_stock_popup.action", 
			 	async: false,  
			 	data: {itemType: 'I',rowId: rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('.store-shelf-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();  
			var rowId = getRowId($(this).attr('id'));  
			var storeRId = Number($('#storeRId').val());
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_materialtransfer.action", 
			 	async: false,  
			 	data:{rowId : rowId, storeId: storeRId},
			    dataType: "html",
			    cache: false,
				success:function(result){   
 					$('.store-result').html(result);  
				},
				error:function(result){  
					 $('.store-result').html(result); 
				}
			});  
			return false;
		});

		$('#store-popup').dialog({
			 autoOpen: false,
			 minwidth: 'auto',
			 width:800, 
			 bgiframe: false,
			 overflow:'hidden',
			 modal: true 
		});   

	 $('.store-from-popup').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));  
			var productId = Number($('#productid_'+rowId).val());
			$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_store_product_popup.action", 
				 	async: false,   
				    dataType: "html",
				    data: {rowId: rowId, productId: productId},
				    cache: false,
					success:function(result){  
						$('.common-result').html(result);   
						$('#common-popup').dialog('open');
						$($($('#common-popup').parent()).get(0)).css('top',0); 
						$(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('z-index',100000);
					},
					error:function(result){  
						 $('.common-result').html(result);  
					}
				});  
				return false;
			});

	 $('.reqtransfer-store-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_common_popup.action", 
			 	async: false,  
			 	data: {rowId: Number(0)},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
		
	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/material_transfer_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });
	 
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.callJq').openDOMWindow({  
			eventType:'click', 
			loader:1,  
			loaderImagePath:'animationProcessing.gif', 
			windowSourceID:'#temp-result', 
			loaderHeight:16, 
			loaderWidth:17 
		}); 
	 
	 $('.rowid').each(function(){
			var id = Number($(this).attr('id').split('_')[1]);  
			$('#transferType_'+id).val($('#tempTransferType_'+id).val());
			$('#transferReason_'+id).val($('#tempTransferReason_'+id).val());
		});
		
	 $('.editDataRQ').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
		 var rowId = getRowId($(slidetab).attr('id'));
      	 var materialRequisitionId = $('#materialRequisitionId_'+rowId).val();  
      	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_requisition_detail.action", 
		 	async: false,  
		 	data:{materialRequisitionId: materialRequisitionId},
		    dataType: "html",
		    cache: false,
			success:function(result){ 
				 $('#temp-result').html(result); 
				 $('.callJq').trigger('click');   
			} 
		});  
 		return false;
	});
		
	 $('.delrowRQ').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 var deleteId = getRowId($(slidetab).attr('id'));
      	 var deleteMaterialRequisitionId = $('#materialRequisitionId_'+deleteId).val(); 
      	 $('.activeRQNumber').each(function(){   
      		var materialRequisitionId = getRowId($(this).attr('id')); 
      		 if(deleteMaterialRequisitionId == materialRequisitionId){
				$(this).attr('checked',false);
				 return false;
			 }
      	 });
      	 $(slidetab).remove();  
      	 var i=1;
   	 	 $('.rowid').each(function(){   
   			 var rowId=getRowId($(this).attr('id')); 
   		 	 $('#lineId_'+rowId).html(i);
			 i=i+1;   
		 });  
   	 	if($('.tab tr').size()==0)
			$('.requisitionline-info').hide();
		return false;
	 }); 

	 $('.transfer-type-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        detailRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	  				  $($($('#common-popup').parent()).get(0)).css('z-index',100000);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	 

	 $('.transfer-reason-lookup').live('click',function(){
	        $('.ui-dialog-titlebar').remove(); 
	        var str = $(this).attr("id");
	        accessCode = str.substring(0, str.lastIndexOf("_"));
	        detailRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				  $($($('#common-popup').parent()).get(0)).css('top',0);
	  				  $($($('#common-popup').parent()).get(0)).css('z-index',100000);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	          return false;
	 	});	

	 $('.closeTransferDom').live('click',function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
	 });

	 if(Number($('#materialTransferId').val())>0){
		 transferPerson = '${MATERIAL_TRANSFER.personByTransferPerson.personId}';
	 }else{
		 transferPerson = '${requestScope.transferPerson}';
	 }
	 	
	 
	 $('.sessionTransferSave').live('click',function(){ 
		 transferDetails = getTransferRequisitionDetails(); 
		 if (transferDetails != null && transferDetails != '') {
			 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/update_requisition_session.action", 
				 	async: false,  
				 	data:{requisitionDetails: JSON.stringify(transferDetails)},
				    dataType: "json",
				    cache: false,
					success:function(response){  
						if(response.returnMessage = "SUCCESS"){
							$('#DOMWindow').remove();
							$('#DOMWindowOverlay').remove();
						} else{
							 $('#page-error-popup').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error-popup').delay(2000).slideUp(1000);
						}
					} 
				});  
				return false;
		 } else{
			 $('#warning_message-popup').hide().html("Please enter the mantatory details.").slideDown(1000);
			 $('#warning_message-popup').delay(2000).slideUp(1000);
		 }
		return false;
	 });

	 var getTransferRequisitionDetails = function(){
		 transferDetails = []; 
		 $('.rowidrq').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val()); 
				var storeFromId = Number($('#storeFromId_'+rowId).val());   
				var productQty = Number($('#productQty_'+rowId).val()); 
				var shelfId = Number($('#shelfId_'+rowId).val());   
				var amount = Number($('#amount_'+rowId).val()); 
				var transferType = Number($('#transferType_'+rowId).val()); 
				var transferReason = Number($('#transferReason_'+rowId).val()); 
				var linesDescription = $.trim($('#linesDescription_'+rowId).text());  
				var storeName = $.trim($('#storeFromName_'+rowId).text());  
				var transferStoreName = $.trim($('#shelf_'+rowId).text());  
 				var materialRequisitionDetailId =  Number($('#materialRequisitionDetailId_'+rowId).val()); 
				if(typeof productId != 'undefined' && productId > 0 && storeFromId > 0 && productQty > 0
						&&   amount > 0 && transferType > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"transferType" : transferType,
						"transferReason" : transferReason,
						"storeFromId" : storeFromId,
						"storeName" : storeName,
						"shelfId" : shelfId,
						"productQty" : productQty,
						"transferStoreName": transferStoreName,
						"amount": amount,
						"linesDescription": linesDescription,
						"materialRequisitionDetailId": materialRequisitionDetailId
					});   
					transferDetails.push({
						"requisitionDetails" : jsonData
					});
				} 
			});  
			return transferDetails;
		 };

		 $('.delrowrq').live('click',function(){
			 slidetab=$(this).parent().parent().get(0);  
			 var idval = getRowId($(this).parent().attr('id')); 
			 var deleteMaterialRequisitionId = $('#materialRequisitionId_'+idval).val();
			 $(slidetab).remove();  
			 var i=0;
			 $('.delrowrq').each(function(){   
	   			 var rowId=getRowId($(this).attr('id')); 
	   		 	 $('#lineIdRQ_'+rowId).html(i);
				 i=i+1;   
			 });  
			 if($('.tab-1 tr').size()==0){ 
				 $('.activeRQNumber').each(function(){   
		      		var materialRequisitionId = getRowId($(this).attr('id'));  
		      		 if(deleteMaterialRequisitionId == materialRequisitionId){
						$(this).attr('checked', false);
						$('.rowid').each(function(){   
				   			 var rowId=getRowId($(this).attr('id')); 
				   		 	 var pageRequisitionId = $('#materialRequisitionId_'+rowId).val();
				   		 	 if(pageRequisitionId == materialRequisitionId) {  
				   		 		$(this).remove();
					   		 }
						 }); 
						 return false;
					 }
		      	 });
				 
				 if($('.tab tr').size()==0)
				 	$('.requisitionline-info').hide();
				 $('#DOMWindow').remove();
				 $('#DOMWindowOverlay').remove();
			 } 
			 return false;
		 });
	 
	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="MATERIAL_TRANSFER_TYPE"){
	 			$('#transferType_'+detailRowId).html("");
				$('#transferType_'+detailRowId).append("<option value=''>Select</option>");
				loadLookupList("transferType_"+detailRowId); 
			}  
	 		else if(accessCode=="MATERIAL_TRANSFER_REASON"){
	 			$('#transferReason_'+detailRowId).html("");
				$('#transferReason_'+detailRowId).append("<option value=''>Select</option>");
				loadLookupList("transferReason_"+detailRowId); 
			}  
		});

 	 	$('.transferType,.transferReason,.linesDescription').live('change',function(){  
 	 		var rowId = getRowId($(this).attr('id'));
 	 		triggerAddRow(rowId);
	 	});
	 	
	 	$('.productQty').live('change',function(){  
	 	 	var rowId = getRowId($(this).attr('id'));
	 		var productQty = Number($('#productQty_'+rowId).val());
	 		var amount = Number($('#amount_'+rowId).val());
 	 		if(productQty > 0){
	 	 		var totalAmount = Number(productQty * amount).toFixed(2);
	 	 		$('#totalAmount_'+rowId).html(totalAmount); 
	 	 	}
 	 		triggerAddRow(rowId);
	 	 	return false;
		});

	 $('.transfer-person-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();  
			var personTypes="1";
	  		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data: {personTypes: personTypes},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $(
								$(
										$('#common-popup')
												.parent())
										.get(0)).css('top',
								0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	}); 
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function personPopupResult(personid, personname, commonParam) {
	$('#transferPerson').val(personname);
	transferPerson = personid;
	$('#common-popup').dialog("close");
}
function commonProductPopup(aData, rowId) {
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName);
 	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
	$('#productQty_' + rowId).val(aData.availableQuantity);
	$('#amount_' + rowId).val(aData.unitRate); 
	$('#storeFromId_' + rowId).val(aData.shelfId);
	$('.amount').trigger('change');
}
function commonProductStockPopup(aData, rowId) {
  	$('#totalAvailQty_' + rowId).val(aData.availableQuantity);
  	if(aData.availableQuantity < Number($('#productQty_' + rowId).val()))
		$('#productQty_' + rowId).val(aData.availableQuantity);
	$('#amount_' + rowId).val(aData.unitRate); 
	$('#storeFromName_' + rowId).text(aData.storeName);
	$('#storeFromId_' + rowId).val(aData.shelfId);
	$('.amount').trigger('change');
}
function commonStorePopup(storeid, storeName, rowId) { 
	$('#store').val(storeName); 
	$('#storeRId').val(storeid); 
	storeId = storeid;
	showActiveMaterialRequisition(storeId); 
}
function showActiveMaterialRequisition(storeId) {
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_store_material_requisition.action", 
	 	async: false,  
	 	data:{storeId: storeId},
	    dataType: "html",
	    cache: false,
		success:function(result){  
			 $('.requisition-info').html(result);   
			 if(typeof($('.activeRQNumber').attr('id'))=="undefined"){   
				$('.requisitionline-info').hide();
				$('.tab').html('');
				$('.requisition-info').find('div').css("color","#ca1e1e").html("No active Requisition found for this store.");
			 } 
		},
		error:function(result){   
		}
	});  
}
function validatRequisitionInfo(currentObj){ 
	var materialRequisitionId = getRowId($(currentObj).attr('id')); 
	var id= Number(1); 
	if($(currentObj).attr('checked')) {   
	 	if(typeof($('.rowid')!="undefined")){ 
	 		$('.rowid').each(function(){
		 		var rowid = getRowId($(this).attr('id'));
	 			id=Number($('#lineId_'+rowid).text());  
				id=id+1;
	 		}); 
	 		$('.requisitionline-info').show();
		} else {
			$('.requisitionline-info').show();
			id=id+1;
		}
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_requisition_transfer.action",
						async : false,
						data : {
							materialRequisitionId : materialRequisitionId,
							rowId : id
						},
						dataType : "html",
						cache : false,
						success : function(result) {
							$('.tab').append(result);
						}
					});
		} else {
			$('.rowid').each(function() {
				var rowId = getRowId($(this).attr('id'));
				var removematerialRequisitionId = $('#materialRequisitionId_' + rowId).val();
				if (removematerialRequisitionId == materialRequisitionId) {
					$('#fieldrow_' + rowId).remove();
					return false;
				}
			});
			if ($('.tab tr').size() == 0)
				$('.requisitionline-info').hide();
		}
		return false;
	}
function callDefaultActiveRQSelect(requisitionObj){
	if (typeof requisitionObj != 'undefined') {
		var id= Number(1); 
		var materialRequisitionId = getRowId(requisitionObj); 
		$('#'+requisitionObj).attr('checked',true); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_material_requisition_transfer.action", 
		 	async: false,  
		 	data:{materialRequisitionId: materialRequisitionId, rowId: id},
		    dataType: "html",
		    cache: false,
			success:function(result){   
				$('.requisitionline-info').show();
				$('.tab').html(result);  
			} 
		});  
	}
	return false;
}
function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
	var shelfid = Number($('#shelfId_'+rowId).val());  
	var productQty = Number($('#productQty_'+rowId).val()); 
 	var transferType = Number($('#transferType_'+rowId).val()); 
	var nexttab=$('#fieldrow_'+rowId).next();  
 	if(productid > 0 && shelfid > 0
			&& productQty > 0
			&& transferType > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
	return false;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}
function materialTransferDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_material_transfers.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove();
			$('#store-popup').dialog('destroy');		
			$('#store-popup').remove();  
			$("#main-wrapper").html(result);
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function loadLookupList(id){
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
						+ response.lookupDetails[index].lookupDetailId
						+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
		triggerAddRow(detailRowId);
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> material
			transfer
		</div> 
		<form name="materialTransferValidation"
			id="materialTransferValidation" style="position: relative;">
			<input type="hidden" id="materialTransferId" name="materialTransferId" 
				value="${MATERIAL_TRANSFER.materialTransferId}"/>
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30" for="searchLocation">Store </label> <input
									type="text" readonly="readonly" name="store" id="store"
									class="width50" /> <span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all reqtransfer-store-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									<input type="hidden" id="storeRId"/>
							</div>
							<div>
								<label class="width30" for="description"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width51">${MATERIAL_TRANSFER.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 90px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_TRANSFER.referenceNumber ne null && MATERIAL_TRANSFER.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${MATERIAL_TRANSFER.referenceNumber} "
											class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber"
											name="referenceNumber"
											value="${requestScope.referenceNumber}"
											class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="transferDate"> Transfer Date<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${MATERIAL_TRANSFER.transferDate ne null && MATERIAL_TRANSFER.transferDate ne ''}">
										<c:set var="transferDate"
											value="${MATERIAL_TRANSFER.transferDate}" />
										<input name="transferDate" type="text" readonly="readonly"
											id="transferDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("transferDate").toString())%>"
											class="transferDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="transferDate" type="text" readonly="readonly"
											id="transferDate"
											class="transferDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="transferPerson">Transfer
									Person<span style="color: red;">*</span> </label> 
									<c:choose>
										<c:when test="${MATERIAL_TRANSFER.personByTransferPerson ne null && MATERIAL_TRANSFER.personByTransferPerson ne ''}">
											<input type="text" readonly="readonly" name="transferPerson" id="transferPerson" class="width50 validate[required]"
												value="${MATERIAL_TRANSFER.personByTransferPerson.firstName } ${MATERIAL_TRANSFER.personByTransferPerson.lastName }" />
										</c:when>
										<c:otherwise>
											<input type="text" readonly="readonly" name="transferPerson" id="transferPerson" 
												class="width50 validate[required]" value="${personName}"/>
										</c:otherwise>
									</c:choose>
									
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all transfer-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="requisition-info" id="hrm">
					<div class="portlet-content class90" id="hrm"
						style="margin-top: 10px;">
						<fieldset>
							<legend>
								Material Transfer Detail<span class="mandatory">*</span>
							</legend>
							<div id="line-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th class="width10">Product</th>
											<th class="width5">Quantity</th>
											<th class="width10">Transfer Type</th>
											<th class="width10" style="display: none;">Reason</th>
											<th class="width10">Transfer Store</th> 
											<th class="width5" style="display: none;">Price</th>
											<th class="width5" style="display: none;" >Total</th>
											<th class="width10"><fmt:message
													key="accounts.journal.label.desc" /></th>
											<th style="width: 1%;"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
										<c:forEach var="DETAIL"
											items="${MATERIAL_TRANSFER.materialTransferDetails}"
											varStatus="status">
											<tr class="rowid" id="fieldrow_${status.index+1}">
												<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
												<td><input type="hidden" name="productId"
													id="productid_${status.index+1}"
													value="${DETAIL.product.productId}" /> <span
													id="product_${status.index+1}">${DETAIL.product.productName}</span>
													<span class="button float-right"> <a
														style="cursor: pointer;" id="prodID_${status.index+1}"
														class="btn ui-state-default ui-corner-all material_product_list width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="text" name="productQty"
													id="productQty_${status.index+1}"
													value="${DETAIL.quantity}"
													class="productQty validate[optional,custom[number]] width80">
													<input type="hidden" name="totalAvailQty"
													id="totalAvailQty_${status.index+1}">
												</td>
												<td style="display: none;"><input type="hidden"
													name="storeFromId" id="storeFromId_${status.index+1}"
													value="${DETAIL.shelfByFromShelfId.shelfId}" />
												</td>
												<td ><select name="transferType"
													id="transferType_${status.index+1}"
													class="width75 transferType">
														<option value="">Select</option>
														<c:forEach var="transferType" items="${TRANSFER_TYPE}">
															<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
														</c:forEach>
												</select> <input type="hidden"
													id="tempTransferType_${status.index+1}"
													value="${DETAIL.lookupDetailByTransferType.lookupDetailId}" />
													<span class="button" style="position: relative;"> <a
														style="cursor: pointer;"
														id="MATERIAL_TRANSFER_TYPE_${status.index+1}"
														class="btn ui-state-default ui-corner-all transfer-type-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td style="display: none;"><select name="transferReason"
													id="transferReason_${status.index+1}"
													class="width75 transferReason">
														<option value="">Select</option>
														<c:forEach var="transferReason" items="${TRANSFER_REASON}">
															<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
														</c:forEach>
												</select> <input type="hidden"
													id="tempTransferReason_${status.index+1}"
													value="${DETAIL.lookupDetailByTransferReason.lookupDetailId}" />
													<span class="button" style="position: relative;"> <a
														style="cursor: pointer;"
														id="MATERIAL_TRANSFER_REASON_${status.index+1}"
														class="btn ui-state-default ui-corner-all transfer-reason-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="hidden" name="shelfId"
													id="shelfId_${status.index+1}"
													value="${DETAIL.shelfByShelfId.shelfId}" /> <span
													id="shelf_${status.index+1}">${DETAIL.shelfByShelfId.name}</span>
													<span class="button float-right"> <a
														style="cursor: pointer;" id="shelfID_${status.index+1}"
														class="btn ui-state-default ui-corner-all store-shelf-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td> 
												<td style="display: none;"><input type="text" name="amount"
													id="amount_${status.index+1}" value="${DETAIL.unitRate}"
													style="text-align: right;" readonly="readonly"
													class="amount width98 validate[optional,custom[number]] right-align">
												</td>
												<td style="display: none;"><c:set var="totalAmount"
														value="${DETAIL.quantity * DETAIL.unitRate}" /> <span
													id="totalAmount_${status.index+1}" style="float: right;">${totalAmount}</span>
												</td>
												<td><input type="text" name="linesDescription"
													id="linesDescription_${status.index+1}"
													value="${DETAIL.description}" class="width98 linesDescription"
													maxlength="150">
												</td>
												<td style="width: 1%;" class="opn_td"
													id="option_${status.index+1}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${status.index+1}"
													style="cursor: pointer; display: none;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${status.index+1}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${status.index+1}" style="cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${status.index+1}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="materialTransferDetailId"
													id="materialTransferDetailId_${status.index+1}"
													value="${DETAIL.materialTransferDetailId}" />
												</td>
											</tr>
										</c:forEach>
										<c:forEach var="i"
											begin="${fn:length(MATERIAL_TRANSFER.materialTransferDetails)+1}"
											end="${fn:length(MATERIAL_TRANSFER.materialTransferDetails)+2}"
											step="1" varStatus="status">

											<tr class="rowid" id="fieldrow_${i}">
												<td style="display: none;" id="lineId_${i}">${i}</td>
												<td><input type="hidden" name="productId"
													id="productid_${i}" /> <span id="product_${i}"></span> <span
													class="button float-right"> <a
														style="cursor: pointer;" id="prodID_${i}"
														class="btn ui-state-default ui-corner-all material_product_list width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="text" name="productQty"
													id="productQty_${i}"
													value="${DETAIL.quantity}"
													class="productQty validate[optional,custom[number]] width80">
													<input type="hidden" name="totalAvailQty"
													id="totalAvailQty_${i}">
												</td>
												<td style="display: none;"><input type="hidden"
													name="storeFromId" id="storeFromId_${i}"
													value="" />
												</td>
												<td><select name="transferType" id="transferType_${i}"
													class="width75 transferType" style="width: 75%;">
														<option value="">Select</option>
														<c:forEach var="transferType" items="${TRANSFER_TYPE}">
															<option value="${transferType.lookupDetailId}">${transferType.displayName}</option>
														</c:forEach>
												</select> <span class="button" style="position: relative;"> <a
														style="cursor: pointer;" id="MATERIAL_TRANSFER_TYPE_${i}"
														class="btn ui-state-default ui-corner-all transfer-type-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td style="display: none;"><select name="transferReason"
													id="transferReason_${i}" class="width75 transferReason" style="width: 75%;">
														<option value="">Select</option>
														<c:forEach var="transferReason" items="${TRANSFER_REASON}">
															<option value="${transferReason.lookupDetailId}">${transferReason.displayName}</option>
														</c:forEach>
												</select> <span class="button" style="position: relative;"> <a
														style="cursor: pointer;"
														id="MATERIAL_TRANSFER_REASON_${i}"
														class="btn ui-state-default ui-corner-all transfer-reason-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td>
												<td><input type="hidden" name="shelfId"
													id="shelfId_${i}"
													value="" /> <span
													id="shelf_${i}"></span>
													<span class="button float-right"> <a
														style="cursor: pointer;" id="shelfID_${i}"
														class="btn ui-state-default ui-corner-all store-shelf-popup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
												</td> 
												<td style="display: none;"><input type="text" name="amount" id="amount_${i}"
													readonly="readonly" style="text-align: right;"
													class="amount width98 validate[optional,custom[number]] right-align">
												</td>
												<td style="display: none;"><span id="totalAmount_${i}" style="float: right;"></span>
												</td>
												<td><input type="text" name="linesDescription" 
													id="linesDescription_${i}" class="width98 linesDescription" maxlength="150">
												</td>
												<td style="width: 1%;" class="opn_td" id="option_${i}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}" style="cursor: pointer; display: none;"
													title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
												</a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}" style="display: none; cursor: pointer;"
													title="Edit Record"> <span
														class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}"
													style="cursor: pointer; display: none;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="materialTransferDetailId"
													id="materialTransferDetailId_${i}" />
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="hrm" class="portlet-content width100 requisitionline-info"
					style="display: none;">
 					<fieldset id="hrm">
						<legend>
							Material Requisitions<span class="mandatory">*</span>
						</legend>
						<div class="portlet-content">
							<div id="page-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th style="width: 5%">Reference No</th>
											<th style="width: 5%">Requisition Date</th>
											<th style="width: 2%">Quantity</th>
											<th style="width: 5%">Description</th>
											<th style="width: 1%"><fmt:message
													key="accounts.common.label.options" /></th>
										</tr>
									</thead>
									<tbody class="tab">
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="material_transfer_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right save"
					id="material_transfer_save" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
				<div id="temp-result" style="display: none;"></div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="codecombination-popup"
					class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="codecombination-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<span class="callJq"></span>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="store-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="store-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>