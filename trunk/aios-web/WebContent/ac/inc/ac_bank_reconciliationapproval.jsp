<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
$(function(){    
	
	 $jquery("#reconciliationValidation").validationEngine('attach');
	 
	$('#reconciliationDate,#statementEndDate').datepick({
		showTrigger: '#calImg'});  
	
	$('#print_statement').show();

	 $('.discard').click(function(){
		 $('.formError').remove();	
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_bank_reconciliation_list.action", 
			 	async: false, 
			    dataType: "html",
			    cache: false,
				success:function(result){
					$('#common-popup').dialog('destroy');		
					$('#common-popup').remove();   
			 		$("#main-wrapper").html(result);  
				}
			});
			return false;
	 }); 

	
	 $('#generate_statement').click(function(){ 
		 var reconciliationDate = $('#reconciliationDate').val();
		 var statementEndDate = $('#statementEndDate').val();
		 var bankAccountId = Number($('#accountId').val());
		 var statementBalance = Number($('#statementBalance').val());
		 if($jquery("#reconciliationValidation").validationEngine('validate')){
	 		 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_reconciliation_details.action", 
				 	async: false,  
				 	data : {bankAccountId : bankAccountId, reconciliationDate: reconciliationDate, 
					 		statementEndDate: statementEndDate, statementBalance: statementBalance},  
				    dataType: "html",
				    cache: false,
					success:function(result){   
						 $('.bank-reconciliation-detail').html(result);  
						 if(Number($('#processFlag').val()) == 1){
							 $('#reconciliation_save').show();
							 $('#print_statement').show();
					     } else{
					    	 $('#reconciliation_save').hide();
							 $('#print_statement').hide();
						  }
					},
					error:function(result){  
						 $('.bank-reconciliation-detail').html(result); 
					}
				});  
			}else
				 return false;
	 });
	 
	 $('#print_statement').click(function(){ 
		 var reconciliationDate = $('#reconciliationDate').val();
		 var statementEndDate = $('#statementEndDate').val();
		 var bankAccountId = $('#accountId').val();
		 var statementBalance = $('#statementBalance').val();
		 var reconciliationId = Number($('#reconciliationId').val());
		 window.open('<%=request.getContextPath()%>/show_reconciliation_print.action?reconciliationId='+reconciliationId+'&bankAccountId='+bankAccountId+'&reconciliationDate='+reconciliationDate+'&statementEndDate='+
				 statementEndDate+'&statementBalance='+statementBalance,
					'_blank','width=900,height=700,left=100px,top=2px');
			return false;
	 });

 	$('.common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getbankaccount_details.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
			},
			error:function(result){  
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	}); 

	$('.reconciliationStatus').live('change',function(){
		var index =  getRowId($(this).attr('id'));
		var reconciliationStatus = $('#reconciliationStatus_'+index).attr('checked');
		var transactionDate = $('#transactionDate_'+index).val();
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/update_reconciliation_details.action",
										async : false,
										data : {
											index : index,
											reconciliationStatus : reconciliationStatus,
											transactionDate:transactionDate
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$('.bank-reconciliation-detail')
													.html(result);
										},
										error : function(result) {
											$('.bank-reconciliation-detail')
													.html(result);
										}
									});
						});

		var reconciliationId = $('#reconciliationId').val();
		$('#bank_reconciliation_document_information').click(
				function() {
					if (reconciliationId > 0) {
						AIOS_Uploader
								.openFileUploader("doc",
										"bankReconciliationDocs",
										"BankReconciliation", reconciliationId,
										"BankReconciliationDocuments");
					} else {
						AIOS_Uploader.openFileUploader("doc",
								"bankReconciliationDocs", "BankReconciliation",
								"-1", "BankReconciliationDocuments");
					}
				});
	});
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> Bank
			Reconciliation
		</div>
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			<fmt:message key="accounts.jv.label.generalinfo" />
		</div>
		<form name="reconciliationValidation" id="reconciliationValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="reconciliationId" name="reconciliationId"
					value="${BANK_RECONCILIATION.bankReconciliationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width35 float-left" id="hrm">
						<fieldset style="min-height: 145px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${BANK_RECONCILIATION.referenceNumber ne null && BANK_RECONCILIATION.referenceNumber ne ''}">
													${BANK_RECONCILIATION.referenceNumber} 
												</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${BANK_RECONCILIATION.referenceNumber}" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30">Bank Account<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${BANK_RECONCILIATION.bankAccount ne null && BANK_RECONCILIATION.bankAccount ne ''}">
										<input type="text" readonly="readonly"
											name="bankAccountNumber" id="accountNumber"
											class="width50 validate[required]"
											value="${BANK_RECONCILIATION.bankAccount.bank.bankName} [${BANK_RECONCILIATION.bankAccount.accountNumber}]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly"
											name="bankAccountNumber" id="accountNumber"
											class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
								<span class="button"
									style="position: relative !important; top: 9px !important; right: 20px; float: right;">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="bankAccountId"
									id="accountId"
									value="${BANK_RECONCILIATION.bankAccount.bankAccountId}" />
							</div>
							<div>
								<label class="width30">Start Date<span style="color: red;">*</span>
								</label>
								<c:choose>
									<c:when
										test="${BANK_RECONCILIATION.reconciliationDate ne null && BANK_RECONCILIATION.reconciliationDate ne ''}">
										<c:set var="date"
											value="${BANK_RECONCILIATION.reconciliationDate}" />
										<input name="reconciliationDate" type="text"
											readonly="readonly" id="reconciliationDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("date").toString())%>"
											class="reconciliationDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="reconciliationDate" type="text"
											readonly="readonly" id="reconciliationDate"
											class="reconciliationDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30">End Date<span style="color: red;">*</span>
								</label>
								<c:choose>
									<c:when
										test="${BANK_RECONCILIATION.statementEndDate ne null && BANK_RECONCILIATION.statementEndDate ne ''}">
										<c:set var="endDate"
											value="${BANK_RECONCILIATION.statementEndDate}" />
										<input name="statementEndDate" type="text" readonly="readonly"
											id="statementEndDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("endDate").toString())%>"
											class="statementEndDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="statementEndDate" type="text" readonly="readonly"
											id="statementEndDate"
											class="statementEndDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30">Statement Balance<span
									style="color: red;">*</span> </label> <input type="text"
									name="statementBalance" id="statementBalance"
									value="${BANK_RECONCILIATION.statementBalance}"
									class="width50 validate[required]" />
							</div> 
						</fieldset>
					</div>
					<div class="width35 float-left" id="hrm">
						<fieldset style="min-height: 145px;">
							
							<div>
								<label class="width30">Statement Ref</label> <input
									name="statementReference" type="text" id="statementReference"
									class="width60">
							</div>
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width60">${BANK_RECONCILIATION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width30 float-left" id="hrm">
						<fieldset style="min-height: 145px;">
							<div>
								<div id="bank_reconciliation_document_information"
									style="cursor: pointer; color: blue;">
									<u>Upload document here</u>
								</div>
								<div
									style="padding-top: 10px; margin-top: 3px; height: 85px; overflow: auto;">
									<span id="bankReconciliationDocs"></span>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content bank-reconciliation-detail class90"
					id="hrm" style="margin-top: 10px;">
					<c:if test="${BANK_RECONCILIATION_STATEMENT.bankReconciliationDetails ne null 
	&& BANK_RECONCILIATION_STATEMENT.bankReconciliationDetails ne ''}">
	<fieldset>
		<legend>Bank Reconciliation Detail</legend>
		<input type="hidden" id="processFlag" value="1"/>
		<div id="hrm" class="hastable width100"  >  
			<input type="hidden" value="${BANK_RECONCILIATION_STATEMENT.accountBalance}" id="accountBalanceTemp"/>
			<table id="hastab" class="width100"> 
				<thead>
				   <tr> 
						<th style="width:1%"><fmt:message key="accounts.jv.label.jvlinno"/></th> 
					    <th style="width:3%">Date</th>  
					    <th style="width:3%">Reference</th>  
					    <th style="width:5%">Description</th> 
						<th style="width:3%;">Payment</th> 
						<th style="width:3%;">Receipt</th> 
						<th style="width:1%">Status</th>
				  </tr>
				</thead> 
				<tbody class="tab"> 
					<%
						double paymentAmount = 0;
						double receiptAmount = 0;
					%>
					<c:forEach var="RECONCILIATION_DETAIL" items="${BANK_RECONCILIATION_STATEMENT.bankReconciliationDetails}"
						varStatus="status"> 
						<tr class="rowid" id="fieldrow_${status.index+1}">
							<td id="lineId_${status.index+1}">${status.index+1}</td>  
							<td> 
								<c:set var="date" value="${RECONCILIATION_DETAIL.transactionDate}"/>
								<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString()) %>
								<input type="hidden" id="transactionId_${status.index+1}" 
								class="transactionId" value="${RECONCILIATION_DETAIL.recordId}"/>
							</td>
							<td>
								${RECONCILIATION_DETAIL.referenceNumber}
							</td> 
							<td>
								${RECONCILIATION_DETAIL.description}
							</td> 
							<td style="text-align: right;">
								${RECONCILIATION_DETAIL.payment}
								<c:set var="payment" value="${RECONCILIATION_DETAIL.payment}"/>
								<c:choose>
							 		<c:when test="${RECONCILIATION_DETAIL.reconciliationStatus eq true}">
										<c:if test="${payment ne null && payment ne ''}"> 
											<%paymentAmount += (Double)pageContext.getAttribute("payment"); %>
										</c:if>
									</c:when>
								</c:choose>
							</td> 
							<td style="text-align: right;">
								<c:set var="receipt" value="${RECONCILIATION_DETAIL.receipt}"/>
								<c:choose>
							 		<c:when test="${RECONCILIATION_DETAIL.reconciliationStatus eq true}">
										<c:if test="${receipt ne null && receipt ne ''}">
											<%receiptAmount += (Double)pageContext.getAttribute("receipt"); %>
										</c:if> 
									</c:when>
								</c:choose>
								${RECONCILIATION_DETAIL.receipt}
							</td> 
							 <td> 
							 	<c:choose>
							 		<c:when test="${RECONCILIATION_DETAIL.reconciliationStatus eq true}">
							 			Reconciliated
							 		</c:when>
							 		<c:otherwise>
							 			Pending
							 		</c:otherwise>
							 	</c:choose> 
							</td>  
						</tr> 
					</c:forEach>  
				</tbody>
			</table>
			<table style="border: 0px; width:50%; position: relative; left:40%;">
				<tr> 
					<td style="text-align: right;border: 0px;font-weight: bold;">Total</td>
					<td style="text-align: right;border: 0px;font-weight: bold;"><%=paymentAmount%></td>
					<td style="text-align: right;border: 0px;font-weight: bold;"><%=receiptAmount%></td> 
				</tr>
			</table>
			<div class="clearfix"></div>
			<table class="width30 float-left" style="margin-top:10px;"> 
				<tr>
					<td class="width40">Account Balance</td>
					<td class="right-align width30" id="accountBalance">${BANK_RECONCILIATION_STATEMENT.accountBalance}</td>
				</tr>
				<tr>
					<td class="width40">Matched Balance</td>
					<td  class="right-align width30" ><%=((receiptAmount * 1) - paymentAmount)%></td>
				</tr>
				<tr>
					<td class="width40">Statement Balance</td>
					<td  class="right-align width30" >
						<c:set var="statementBalance" value="${BANK_RECONCILIATION_STATEMENT.statementBalance}"/>
						${statementBalance}
					</td>
				</tr>
				<tr>
					<td class="width40">Difference</td>
					<td  class="right-align width30" ><%=((receiptAmount * 1) - paymentAmount) - (Double)pageContext.getAttribute("statementBalance")%></td>
				</tr>
			</table> 
		</div> 
	</fieldset>
</c:if>
<c:if test="${BANK_RECONCILIATION_STATEMENT.bankReconciliationDetails eq null 
	|| BANK_RECONCILIATION_STATEMENT.bankReconciliationDetails eq ''}">
	<input type="hidden" id="processFlag" value="0"/>
	<fieldset>
		<legend>Bank Reconciliation Detail</legend>
		<div id="hrm" class="hastable width100"  >  
			<span style="font-weight: bold; color:#ff255c;">Result not found.</span>
		</div>
	</fieldset>	
</c:if>	
					
					</div>
			</div>
			<div class="clearfix"></div>
			
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>