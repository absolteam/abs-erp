<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accessCode = "";
$(function(){ 
	$jquery("#asset_claim_insurance").validationEngine('attach'); 

	$('#claimDate').datepick();

	 $('#claim_insurace_discard').click(function(event){  
		 claimInsuranceDiscard("");
		 return false;
	 });

	 $('.payoutnature-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="CLAIM_PAYOUT_NATURE"){
				$('#payOutNature').html("");
				$('#payOutNature').append("<option value=''>Select</option>");
				loadLookupList("payOutNature"); 
			}  
		});
	 

	 if(Number($('#assetClaimInsuranceId').val())> 0){
 		 $('#claimStatus').val("${ASSET_CLAIM_INSURANCE.status}");
		 $('#payOutNature').val("${ASSET_CLAIM_INSURANCE.lookupDetail.lookupDetailId}");
		 $('#paymentMode').val("${ASSET_CLAIM_INSURANCE.modeOfPayment}");
	 }else
		 $('#claimStatus').val(1);
		 

	 $('#claim_insurace_save').click(function(){   
		 if($jquery("#asset_claim_insurance").validationEngine('validate')){
				var assetClaimInsuranceId = Number($('#assetClaimInsuranceId').val());   
 				var assetInsuranceId = Number($('#assetInsuranceId').val());
				var claimAmount = Number($('#claimAmount').val());
				var claimExpense = Number($('#claimExpense').val()); 
		 		var payOutNature = Number($('#payOutNature').val());
		 		var paymentMode = Number($('#paymentMode').val());
		 		var payOutAmount = Number($('#payOutAmount').val());
		 		var status = Number($('#claimStatus').val());
 		 		var claimDate = $('#claimDate').val();
  				var description = $("#description").val(); 
  				var referenceNumber = $("#referenceNumber").val(); 
  				$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/save_asset_claim_insurance.action", 
					 	async: false,
					 	data:{
					 		assetClaimInsuranceId : assetClaimInsuranceId,
												assetInsuranceId : assetInsuranceId,
												claimAmount : claimAmount,
												claimExpense : claimExpense,
												payOutNature : payOutNature,
												paymentMode : paymentMode,
												payOutAmount : payOutAmount,
												claimDate : claimDate, 
												status: status,
												description : description,
												referenceNumber: referenceNumber
											},
											dataType : "json",
											cache : false,
											success : function(response) {
												if (response.returnMessage == "SUCCESS") {
													$('#common-popup').dialog(
															'destroy');
													$('#common-popup').remove();
													claimInsuranceDiscard(assetClaimInsuranceId > 0 ? "Record updated successfully.":"Record created successfully.");
 												} else {
													$('#page-error')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													return false;
												}
											}
										});
							} else {
								return false;
							}
							return false;
						});

		$('.common-popup-claiminsurance').click(
						function() {
							$('.ui-dialog-titlebar').remove();
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/show_common_asset_insurance.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			top : 0,
			modal : true
		}); 

		$('#commom-insurance-close')
		.live(
				'click',
				function() {
					$('#common-popup').dialog('close');
				});
	});
function claimInsuranceDiscard(message){ 
 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_claim_insurance.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$('#common-popup').dialog('destroy');
						$('#common-popup').remove();
						$("#main-wrapper").html(result);
						if (message != null && message != '') {
							$('#success_message').hide().html(message)
									.slideDown(1000);
							$('#success_message').delay(2000).slideUp();
						}
					}
				});
	}

	function assetInsurancePopupResult(assetInsuranceId, policyNumber) {
 		$('#assetInsuranceId').val(assetInsuranceId);
		$('#policyNumber').val(policyNumber);
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			claim insurance
		</div> 
		<form name="asset_claim_insurance" id="asset_claim_insurance" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetClaimInsuranceId" name="assetClaimInsuranceId"
					value="${ASSET_CLAIM_INSURANCE.assetClaimInsuranceId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="claimStatus">Status<span
									class="mandatory">*</span> </label> <select name="claimStatus"
									id="claimStatus" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="claimStatus" items="${CLAIM_STATUS}">
										<option value="${claimStatus.key}">${claimStatus.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="claimExpense">Claim Expense
								</label> <input type="text" id="claimExpense"
									class="width50 validate[optional,custom[number]]"
									value="${ASSET_CLAIM_INSURANCE.claimExpenses}">
							</div>
							<div style="display: none;">
								<div>
									<label class="width30" for="payOutNature">PayOut Nature<span
										class="mandatory">*</span> </label> <select name="payOutNature"
										id="payOutNature" class="width51">
										<option value="">Select</option>
										<c:forEach var="payOutNature" items="${PAYOUT_NATURE}">
											<option value="${payOutNature.lookupDetailId}">${payOutNature.displayName}</option>
										</c:forEach>
									</select>
									<span class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="CLAIM_PAYOUT_NATURE"
										class="btn ui-state-default ui-corner-all payoutnature-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30" for="paymentMode">Payment Mode </label>
									<select name="paymentMode" id="paymentMode" class="width51">
										<option value="">Select</option>
										<c:forEach var="paymentMode" items="${PAYMENT_MODE}">
											<option value="${paymentMode.key}">${paymentMode.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label class="width30" for="payOutAmount">PayOut Amount</label>
									<input type="text" id="payOutAmount"
										class="width50 validate[optional,custom[number]]"
										value="${ASSET_CLAIM_INSURANCE.payoutAmount}">
								</div>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_CLAIM_INSURANCE.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="referenceNumber">Reference
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CLAIM_INSURANCE.referenceNumber ne null && ASSET_CLAIM_INSURANCE.referenceNumber ne ''}">
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${ASSET_CLAIM_INSURANCE.referenceNumber}">
									</c:when>
									<c:otherwise>
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${requestScope.referenceNumber}">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="policyNumber">Policy Number<span
									class="mandatory">*</span> </label> <input type="text"
									id="policyNumber" class="width50 validate[required]"
									readonly="readonly"
									value="${ASSET_CLAIM_INSURANCE.assetInsurance.policyNumber}">
								<span class="button"> <a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all common-popup-claiminsurance width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" id="assetInsuranceId"
									value="${ASSET_CLAIM_INSURANCE.assetInsurance.assetInsuranceId}">
							</div>
							<div>
								<label class="width30" for="claimDate">Claim Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CLAIM_INSURANCE.claimDate ne null && ASSET_CLAIM_INSURANCE.claimDate ne ''}">
										<c:set var="claimDate"
											value="${ASSET_CLAIM_INSURANCE.claimDate}" />
										<%
											String claimDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"claimDate").toString());
										%>
										<input type="text" id="claimDate"
											class="width50 validate[required]" value="<%=claimDate%>"
											readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="claimDate"
											class="width50 validate[required]" readonly="readonly">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="claimAmount">Claim Amount <span
									class="mandatory">*</span> </label> <input type="text" id="claimAmount"
									class="width50 validate[required,custom[number]]"
									value="${ASSET_CLAIM_INSURANCE.claimAmount}">
							</div> 
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="claim_insurace_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="claim_insurace_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>