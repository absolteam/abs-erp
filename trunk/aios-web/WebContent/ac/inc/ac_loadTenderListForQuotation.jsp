 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			tender details<input type="hidden" value="${ROW_ID}" id="rowid"/>  
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="tenderlist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${TENDERS_LIST}" var="bean" varStatus="status">  
	     		<option value="${bean.tenderId}|${bean.tenderNumber}|${bean.currency.currencyId}">${bean.tenderNumber}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 product_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width25 float-left"><span>Tender Number</span></li>  
   				<li class="width25 float-left"><span>Date</span></li>  
   				<li class="width25 float-left"><span>Expiry</span></li>   
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${TENDERS_LIST}" var="bean" varStatus="status">
		     		<li id="tender_${status.index+1}" class="tender_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.tenderId}" id="tenderbyid_${status.index+1}">   
		     			<input type="hidden" value="${bean.currency.currencyId}" id="currbynameid_${status.index+1}"> 
		     			<span id="tenderNumber_${status.index+1}" style="cursor: pointer;" class="float-left width25">${bean.tenderNumber}</span>
		     			<span id="tenderDate_${status.index+1}" style="cursor: pointer;" class="float-left width25">${bean.tenderDate}</span>
		     			<span id="tenderDate_${status.index+1}" style="cursor: pointer;" class="float-left width25">${bean.tenderExpiry} 
		     				<span id="tenderNumberSelect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var tenderRecord = $(this).val();
		       var tendersArray = tenderRecord.split("|"); 
		       $('#tenderId').val(tendersArray[0]);
		       $('#tenderNumber').val(tendersArray[1]); 
		       $('#quotationCurrency').val(tendersArray[2]);
		       $('#common-popup').dialog("close");   
		       callTendarDetails($('#tenderId').val());
	   } 
	}); 
	 
	 $('.tender_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.tender_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#tenderNumberSelect_'+rowid).hasClass('selected')){ 
					 $('#tenderNumberSelect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#tenderNumberSelect_'+rowid).addClass('selected');
		});
	 
	 $('.tender_list_li').live('dblclick',function(){ 
		  var tempvar="";var idarray = ""; var rowid="";
		  currentId=$(this);  
		  tempvar=$(currentId).attr('id');   
		  idarray = tempvar.split('_'); 
		  rowid=Number(idarray[1]);
		  $('#tenderId').val($('#tenderbyid_'+rowid).val());
		  $('#tenderNumber').val($('#tenderNumber_'+rowid).text());
		  $('#quotationCurrency').val($('#currbynameid_'+rowid).val());
		  $('#common-popup').dialog('close');  
		  callTendarDetails($('#tenderId').val());
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  

		var callTendarDetails =function(){ 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/gettenderdetail_list.action", 
			 	async: false,
			 	data:{tenderId:$('#tenderId').val()},  
			    dataType: "html",
			    cache: false,
				success:function(result){
					 $('.tab').html(result);  
				},
				error:function(result){  
					 $('.tab').html(result); 
				}
			});  
		};
 });
  
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.product_list ul li{
		padding:3px;
	}
	.product_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>