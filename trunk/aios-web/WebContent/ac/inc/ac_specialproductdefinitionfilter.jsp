<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 98% !important;
	height: 85% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
}
#filter-splproduct-result>fieldset{
	border: 1px solid #dddddd;
	padding: 5px;
}
.icon-search-class{
	cursor: pointer;
	position: absolute;
	right: 7%;
	z-index: 1;
	margin: 4px 0px !important;
}
</style>
<script type="text/javascript">
	$(function() {
		var tdsize = $($("#definition-table>tbody>tr:first").children()).size();
		if (tdsize == 1) {
			$($("#definition-table>tbody>tr:first").children()).each(
					function() {
						$(this).addClass('float-left width20');
					});
		}

		$('.definitionspl').click(
				function() {
					var productDefinitionId = getRowId($(this).find('input')
							.attr('id'));
					var productId = Number($('#specialProductId').val());
					showSpecialProductDefinitionFilterPopup(productDefinitionId,productId);
					return false;
				});
		
		$('.speicalproduct-close').click(
				function() { 
					$('#DOMWindow').remove();
					$('#DOMWindowOverlay').remove(); 
					return false;
				});  
		
		$('.subCategoryDef').click(function(){ 
			var categoryId = getRowId($(this).find('input').attr('id'));
			var specialProductId = Number($('#specialProductId').val()); 
			$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_categorydef_popupproduct.action",
										async : false,
										data : {
											categoryId : categoryId, productId: specialProductId
										},
										dataType : "html",
										cache : false,
										success : function(result) {
											$("#filter-splproduct-result").html(
													result);
											$("#filter-splproduct-result").show();
										}
									});
							return false;
						});
	});
</script>
 <fieldset class="splproductset"
 	 style="max-height: 95px; overflow-y: auto; overflow-x: hidden;
 	 padding: 1px !important; border: 1px solid #dddddd;">
	<legend>
		<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Sub
		Category
	</legend>
	<div class="width100 content-item-table"
		style="max-height: 80px; overflow-y: auto; overflow-x: hidden;">
		<table id="definition-table" class="pos-item-table width100">
			<c:set var="definitionRow" value="0" />
			<c:set var="subCategoryLoop" value="true" />
			<c:choose>
				<c:when
					test="${PRODUCT_DEFINITIONS ne null && fn:length(PRODUCT_DEFINITIONS) > 0}">
					<c:forEach begin="0" step="5" items="${PRODUCT_DEFINITIONS}">
						<tr>
							<c:if
								test="${PRODUCT_SUBCATEGORY ne null && PRODUCT_SUBCATEGORY ne '' && subCategoryLoop eq true}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="subCategoryDef">${PRODUCT_SUBCATEGORY.categoryName}
											<input type="hidden"
											id="categoryId_${PRODUCT_SUBCATEGORY.productCategoryId}"
											class="categoryId" /> </span>
									</div>
								</td>
								<c:set var="subCategoryLoop" value="false" />
							</c:if>
							<!-- TD 1 -->
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="definitionspl">${PRODUCT_DEFINITIONS[definitionRow+0].definitionLabel}
										<input type="hidden"
										id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+0].productDefinitionId}"
										class="definitionId" /> </span>
								</div>
							</td>
							<!-- TD 2 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+1]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+1].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="definitionspl">${PRODUCT_DEFINITIONS[definitionRow+1].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+1].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 3 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+2]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+2].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="definitionspl">${PRODUCT_DEFINITIONS[definitionRow+2].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+2].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 4 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+3]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+3].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #81F7D8;">
										<span class="definitionspl">${PRODUCT_DEFINITIONS[definitionRow+3].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+3].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<!-- TD 5 -->
							<c:if test="${PRODUCT_DEFINITIONS[categoryRow+4]!= null &&
								PRODUCT_DEFINITIONS[definitionRow+4].definitionLabel ne null}">
								<td>
									<div class="box-model" style="background: #9FF781;">
										<span class="definitionspl">${PRODUCT_DEFINITIONS[definitionRow+4].definitionLabel}
											<input type="hidden"
											id="definitionId_${PRODUCT_DEFINITIONS[definitionRow+4].productDefinitionId}"
											class="definitionId" /> </span>
									</div>
								</td>
							</c:if>
							<c:set var="definitionRow" value="${definitionRow + 5}" />
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<c:if
							test="${PRODUCT_SUBCATEGORY ne null && PRODUCT_SUBCATEGORY ne ''}">
							<td>
								<div class="box-model" style="background: #9FF781;">
									<span class="subCategoryDef">${PRODUCT_SUBCATEGORY.categoryName}
										<input type="hidden"
										id="categoryId_${PRODUCT_SUBCATEGORY.productCategoryId}"
										class="categoryId" /> </span>
								</div>
							</td>
						</c:if>
					</tr>
				</c:otherwise>
			</c:choose>

		</table>
		<input type="hidden" id="specialProductId"
			value="${requestScope.productId}" /> 
	</div>
</fieldset> 
<div style="margin-top: 5px; display:none;" id="filter-splproduct-result" class="product-spllisting">
</div>
<div
	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
	<div class="portlet-header ui-widget-header float-right speicalproduct-close"
		id="speicalproduct-close"
		style="background: #DF0101; color: #fff; padding: 10px; margin-top:150px;">
		Close</div>
</div> 