<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<style>
#DOMWindow {
	width: 95% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}
</style>
<script type="text/javascript">
var aisleDetails = "";
var binDetails = "";
var slidetab = "";
var rackDetails = "";
var sectionRowId = 0;
var accessCode = "";
$(function(){
	$jquery("#storeValidation").validationEngine('attach'); 
	 
	manupulateLastRow(); 
	
	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/store_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });  

	$('.store-storage-lookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        accessCode=$(this).attr("id"); 
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	$('.store-section-lookup').live('click',function(){
        $('.ui-dialog-titlebar').remove(); 
        var str = $(this).attr("id");
        accessCode = str.substring(0, str.lastIndexOf("_"));
        sectionRowId = str.substring(str.lastIndexOf("_") + 1, str.length);
          $.ajax({
             type:"POST",
             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
             data:{accessCode: accessCode},
             async: false,
             dataType: "html",
             cache: false,
             success:function(result){ 
                  $('.common-result').html(result);
                  $('#common-popup').dialog('open');
  				   $($($('#common-popup').parent()).get(0)).css('top',0);
                  return false;
             },
             error:function(result){
                  $('.common-result').html(result);
             }
         });
          return false;
 	});	 

	//Lookup Data Roload call
 	$('#save-lookup').live('click',function(){  
		if(accessCode=="STORAGE_TYPE"){
			$('#storageType').html("");
			$('#storageType').append("<option value=''>Select</option>");
			loadLookupList("storageType"); 
		}else if(accessCode=="STORAGE_SECTION"){
			$('#storageSection_'+sectionRowId).html("");
			$('#storageSection_'+sectionRowId).append("<option value=''>Select</option>");
			loadLookupList("storageSection_"+sectionRowId); 
		}   
	});
	
	$('.binaddrows').live('click',function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".bintab>.binrowid:last").attr('id'))); 
		  id=id+1;  
		  var bintabId = Number(getRowId($(".bintab>.binrowid:first").attr('id'))); 
		  var aisleId = $('#shelfaisleId_'+bintabId).val();
 		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/store_bin_addrow.action", 
			 	async: false,
			 	data:{rowId: id, aisleId: aisleId},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.bintab tr:last').before(result);
					 if($(".bintab").height()>255)
						 $(".bintab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.binrowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#binlineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	$('.rackaddrows').live('click',function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".racktab>.rackrowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/store_rack_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.racktab tr:last').before(result);
					 if($(".racktab").height()>255)
						 $(".racktab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rackrowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#racklineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	$('.addBins').click(function(){  
		var currentId = Number(getRowId($(this).attr('id'))); 
		var sectionReferenceId = 0;
		var aisleId = Number($('#aisleid_'+currentId).val());
		if (aisleId > 0)
			sectionReferenceId = aisleId;
		else
			sectionReferenceId = Number($('#lineId_'+currentId).text());
		$(".bin-entry").remove();
  		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/show_bin_entry.action", 
	     	async: false, 
	     	data:{sectionReferenceId: sectionReferenceId},
			dataType: "html",
			cache: false,
			success: function(result){   
				$("#temp-result").html(result);   
				manupulateBinLastRow(); 
				$('.tempbinrowid').each(function(){   
					 var rowId=getRowId($(this).attr('id')); 
					 $('#binSection_'+rowId).val($('#tempbinSection_'+rowId).val());
				 });
			} 		
		}); 
		return false;
	 });

	$('.backToBin').live('click',function(){  
		var sectionReferenceId = Number($('#sectionReferenceId').val()); 
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/show_bin_entry.action", 
	     	async: false, 
	     	data:{sectionReferenceId: sectionReferenceId},
			dataType: "html",
			cache: false,
			success: function(result){   
				$(".bin-entry").html(result); 
				$('.tempbinrowid').each(function(){   
					 var rowId=getRowId($(this).attr('id')); 
					 $('#binSection_'+rowId).val($('#tempbinSection_'+rowId).val());
				 });
				manupulateBinLastRow();
			} 		
		}); 
		return false;
	});

	$('.bin_save').live('click',function(){  
		var sectionReferenceId = Number($('#sectionReferenceId').val());
		binDetails = getBinDetails();
		var confirmSave = true;
		var binReferenceId = Number(0);
  		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/save_bin_entry.action", 
	     	async: false, 
	     	data:{sectionReferenceId: sectionReferenceId, binDetails: binDetails, confirmSave: confirmSave, binReferenceId: binReferenceId},
			dataType: "json",
			cache: false,
			success: function(response){   
				if(response.returnMessage == "SUCCESS"){
					 $('#DOMWindow').remove();
					 $('#DOMWindowOverlay').remove();
				}else{
					$('#bin-page-error').hide().html(response.returnMessage).slideDown();
					$('#bin-page-error').delay(2000).hide();
				}
			} 		
		}); 
		return false;
	 });

	$('.rack_save').live('click',function(){  
		var sectionReferenceId = Number($('#sectionReferenceId').val());
		var binReferenceId = Number($('#binReferenceId').val());
		rackDetails = getRackDetails(); 
 		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/save_rack_entry.action", 
	     	async: false, 
	     	data:{sectionReferenceId: sectionReferenceId, binReferenceId: binReferenceId, rackDetails: rackDetails},
			dataType: "json",
			cache: false,
			success: function(response){   
				if(response.returnMessage == "SUCCESS"){
					$('.backToBin').trigger('click');
				}else{
					$('#bin-page-error').hide().html(response.returnMessage).slideDown();
					$('#bin-page-error').delay(2000).hide();
				}
			} 		
		}); 
		return false;
	 });

	$('.bindelrow').live('click',function(){ 
		 slidetab = $(this).parent().parent().get(0);  
		 $(slidetab).remove();  
     	 var i=1;
	   	 $('.binrowid').each(function(){   
	   		 var rowId = getRowId($(this).attr('id')); 
	   		 $('#binlineId_'+rowId).html(i);
			 i=i+1; 
		 }); 
	   	return false; 
	 });

	$('.rackdelrow').live('click',function(){ 
		 slidetab = $(this).parent().parent().get(0);  
		 $(slidetab).remove();  
     	 var i=1;
	   	 $('.rackrowid').each(function(){   
	   		 var rowId = getRowId($(this).attr('id')); 
	   		 $('#rackrowid_'+rowId).html(i);
			 i=i+1; 
		 }); 
	   	return false;
	 });

	$('.addBins').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	});
	
 
	$('.closeDom').live('click',function(){
		var sectionReferenceId = Number($('#sectionReferenceId').val()); 
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/store_bin_discard.action", 
		 	async: false,
		 	data:{sectionReferenceId: sectionReferenceId},
		    dataType: "json",
		    cache: false,
			success:function(response){
				if(response.returnMessage == "SUCCESS"){
					 $('#DOMWindow').remove();
					 $('#DOMWindowOverlay').remove();
				}else{
					$('#bin-page-error').hide().html(response.returnMessage).slideDown();
					$('#bin-page-error').delay(2000).hide();
				} 
			}
		 }); 
		return false;
	 });

	$('.store_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/getstore_details.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$("#main-wrapper").html(result);  
				return false;
			}
		 });
		 return false;
	 });


	 $('.store_save').click(function(){ 
		 if($jquery("#storeValidation").validationEngine('validate')){
 			 var storeId = Number($('#storeId').val()); 
			 var storeName = $('#storeName').val();
			 var personId = Number($('#personId').val()); 
			 var address = $('#address').val(); 
			 var storeNumber = $('#storeNumber').val();
			 var storageType = Number($('#storageType').val());
			 var locationId = Number($('#locationId').val());
 			 aisleDetails = getAisleDetails();  
			 
			 if(aisleDetails!="" && aisleDetails!=null){
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/savestore.action", 
					 	async: false,
					 	data:{
						 		storeId: storeId, storeName: storeName,personId: personId, address: address, storeNumber: storeNumber,
						 		storageType: storageType, locationId: locationId, aisleDetails: aisleDetails
						 	},
					    dataType: "json",
					    cache: false,
						success:function(response){
 							 if(response.returnMessage == "SUCCESS"){
								 $.ajax({
										type:"POST",
										url:"<%=request.getContextPath()%>/getstore_details.action", 
									 	async: false,
									    dataType: "html",
									    cache: false,
										success:function(result){
											$('#DOMWindow').remove();
											$('#DOMWindowOverlay').remove();
											$('#common-popup').dialog('destroy');		
						  					$('#common-popup').remove();  
											$("#main-wrapper").html(result); 
											if(storeId > 0)
												$('#success_message').hide().html("Record updated").slideDown(1000);
											else
												$('#success_message').hide().html("Record created").slideDown(1000);
											$('#success_message').delay(3000).slideUp();
										}
								 });
							 }
							 else{
								 $('#store-error').hide().html(response.returnMessage).slideDown(1000);
								 $('#store-error').delay(3000).slideUp();
								 return false;
							 }
						},
						error:function(response){  
							$('#store-error').hide().html("Record creation failure.").slideDown(1000);
							$('#store-error').delay(3000).slideUp();
						}
				 });
			 }else{
				 $('#store-error').hide().html("Please enter section details.").slideDown(1000);
				 $('#store-error').delay(3000).slideUp();
				 }
			 }
		 else{return false;}
		 return false;
	 });

	 var getAisleDetails=function(){
		aisleDetails=""; 
		var aisleIdArray = new Array();
		var aisleNumberArray = new Array();
		var dimensionArray = new Array();
		var sectionArray = new Array();
		var referenceIdArray = new Array();
		var nameArray = new Array();
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var aisleNumber = $('#aisleNumber_'+rowId).val();  
			var storageSection = $('#storageSection_'+rowId).val();  
			var dimension = $('#dimension_'+rowId).val();
			var aisleId = Number($('#aisleid_'+rowId).val()); 
			var referenceId = Number($('#lineId_'+rowId).text());
			var sectionName = $('#sectionName_'+rowId).val();
			if(typeof aisleNumber != 'undefined' && aisleNumber!=null && storageSection!="" && storageSection!="" && dimension!=""){
				referenceIdArray.push(referenceId);
				aisleNumberArray.push(aisleNumber);
				dimensionArray.push(dimension);
				sectionArray.push(storageSection); 
				aisleIdArray.push(aisleId); 
				nameArray.push(sectionName);
			} 
		});
		for(var j=0;j<aisleNumberArray.length;j++){ 
			aisleDetails += referenceIdArray[j]+"__"+aisleNumberArray[j]+"__"+dimensionArray[j]+"__"+sectionArray[j]+"__"+aisleIdArray[j]+"__"+nameArray[j];
			if(j==aisleNumberArray.length-1){   
			} 
			else{
				aisleDetails += "#@";
			}
		} 
		 
		return aisleDetails;
	 };

	 if(Number($('#storeId').val())>0){
			$('#storageType').val($('#tempStorageType').val());
			 var rowId= 0;
			 $('.rowid').each(function(){   
		   		 rowId = getRowId($(this).attr('id')); 
		   		 $('#storageSection_'+rowId).val($('#tempStorageSection_'+rowId).val()); 
			 }); 
			
			$('#binType').val($('#tempBinType').val());
		}
		
	$('.aisleNumber').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});
	
	$('.storageSection').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});
	
	$('.dimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerAddRow(rowId);
	});

	$('.binSection').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerBinAddRow(rowId);
	});

	$('.bindimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerBinAddRow(rowId);
	});

	$('.rackType').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerRackAddRow(rowId);
	});

	$('.rackdimension').live('change',function(){
		var rowId=getRowId($(this).attr('id'));
		triggerRackAddRow(rowId);
	});
	 
	$('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);  
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
	   	return false; 
	 });

	 $('#person-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });

	$('.store-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action",
										async : false,
										dataType : "html",
										data:{personTypes: "1"},
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});


	 $('#department-list-close').live('click',function(){  
		 $('#common-popup').dialog('close');
	 });
	 
	$('.locationstore-common-popup').click(function(){  
		$('.ui-dialog-titlebar').remove(); 
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			modal : true
		});
	});
	
	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid');
		$('.tab>tr:last').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}
	
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	
	function triggerAddRow(rowId) {
		var aisleNumber = $('#aisleNumber_' + rowId).val();
		var dimension = $('#dimension_' + rowId).val();
		var storageSection = $('#storageSection_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (aisleNumber != null && aisleNumber != "" && dimension != null
				&& dimension != "" && storageSection != null
				&& storageSection != "" && $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
	}

	function triggerBinAddRow(rowId) {
		var binSection = $('#binSection_' + rowId).val();
		var bindimension = $('#bindimension_' + rowId).val(); 
		var nexttab = $('#binfieldrow_' + rowId).next();
		if (binSection != null && binSection != "" && bindimension != null
				&& bindimension != "" && $(nexttab).hasClass('lastrow')) {
			$('#BinDeleteImage_' + rowId).show();
			$('.binaddrows').trigger('click');
		}
	}

	function triggerRackAddRow(rowId) {
		var rackType = $('#rackType_' + rowId).val();
		var rackdimension = $('#rackdimension_' + rowId).val(); 
		var nexttab = $('#rackfieldrow_' + rowId).next();
 		if (rackType != null && rackType != "" && rackdimension != null
				&& rackdimension != "" && $(nexttab).hasClass('lastrow')) {
			$('#RackDeleteImage_' + rowId).show();
			$('.rackaddrows').trigger('click');
		}
	}
	
	function personPopupResult(personid, personname, commonParam) {
		$('#personId').val(personid);
		$('#personNumber').val(personname);
		$('#common-popup').dialog("close");
	}

	function departmentBranchPopupResult(locationId, locationName, commonParam){
		$('#locationId').val(locationId);
		$('#locationName').val(locationName);
		$('#department-list-close').trigger('click');
		return false;
	}
	
	function addRacks(binReferenceId){  
		var sectionReferenceId = Number($('#sectionReferenceId').val());
		binDetails = getBinDetails();  
   		if(binDetails!=null && binDetails!=""){
			var confirmSave = false; 
	 		$.ajax({
				type: "POST",
				url: "<%=request.getContextPath()%>/show_rack_entry.action",
				async : false, 
				dataType : "html",
				data : {
					binReferenceId : Number(binReferenceId),
					confirmSave : confirmSave,
					binDetails : binDetails,
					sectionReferenceId : sectionReferenceId
				},
				cache : false,
				success : function(result) { 
					$(".bin-entry").html(result);
					$('.temprackrowid').each(
							function() {
								var rowId = getRowId($(this).attr('id'));
								$('#rackType_' + rowId).val(
										$('#temprackType_' + rowId).val());
							});
					manupulateRackLastRow();
				}
			});
			$('.addRacks').openDOMWindow({
				eventType : 'click',
				loader : 1,
				loaderImagePath : 'animationProcessing.gif',
				windowSourceID : '#temp-result',
				loaderHeight : 16,
				loaderWidth : 17
			});
		} else {
			$('#bin-page-error').hide().html(
					"Please select a type & dimensions").slideDown(1000);
			$('#bin-page-error').delay(2000).slideUp();
		}
		return false;
	}

	function manupulateBinLastRow() {
		var hiddenSize = 0;
		$($(".bintab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		//var tdSize = $($(".bintab>tr:first").children()).size();
		//var actualSize = Number(tdSize - hiddenSize);

		$('.bintab>tr:last').removeAttr('id');
		$('.bintab>tr:last').removeClass('binrowid');
		$('.bintab>tr:last').addClass('lastrow');
		$($('.bintab>tr:last').children()).remove();

		for ( var i = 0; i < 5; i++) {
			$('.bintab>tr:last').append("<td style=\"height:15px;\"></td>");
		}
	}

	function manupulateRackLastRow() {
		var hiddenSize = 0;
		$($(".racktab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".racktab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);

		$('.racktab>tr:last').removeAttr('id');
		$('.racktab>tr:last').removeClass('rackrowid');
		$('.racktab>tr:last').addClass('lastrow');
		$($('.racktab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.racktab>tr:last').append("<td style=\"height:15px;\"></td>");
		}
	}

	function getBinDetails() {
		binDetails = "";
		var binSectionArray = new Array();
		var sectionNameArray = new Array();
		var bindimensionArray = new Array();
		var confirmSaveArray = new Array();
		var shelfidArray = new Array();
		var referenceIdArray = new Array();
		var aisleidArray = new Array();
		$('.binrowid').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var binSection = $('#binSection_' + rowId).val();
					var bindimension = $('#bindimension_' + rowId).val();
					var shelfid = Number($('#shelfid_' + rowId).val());
					var confirmSave = $('#confirmSave_' + rowId).val();
					var referenceId = $('#binlineId_' + rowId).text();
					var aisleId =  Number($('#shelfaisleId_' + rowId).val());
					var binSectionName = $('#binSectionName_' + rowId).val();
					if (typeof binSection != 'undefined' && binSection != null
							&& binSection != "" && bindimension != ""
							&& bindimension != "") {
						binSectionArray.push(binSection);
						bindimensionArray.push(bindimension);
						confirmSaveArray.push(confirmSave);
						referenceIdArray.push(referenceId);
						shelfidArray.push(shelfid);
						aisleidArray.push(aisleId);
						sectionNameArray.push(binSectionName);
					}
				});
		for ( var j = 0; j < binSectionArray.length; j++) {
			binDetails += binSectionArray[j] + "__" + bindimensionArray[j]
					+ "__" + confirmSaveArray[j] + "__" + referenceIdArray[j]
					+ "__" + shelfidArray[j] + "__" + aisleidArray[j]+"__"+sectionNameArray[j];
			if (j == binSectionArray.length - 1) {
			} else {
				binDetails += "#@";
			}
		}
		return binDetails;
	}

	function getRackDetails() {
		rackDetails = "";
		var rackTypeArray = new Array();
		var rackdimensionArray = new Array();
		var rackidArray = new Array();
		var aisleidArray = new Array();
		var shelfNameArray = new Array();
		$('.rackrowid').each(
				function() {
					var rowId = getRowId($(this).attr('id'));
					var rackType = $('#rackType_' + rowId).val();
					var rackdimension = $('#rackdimension_' + rowId).val();
					var rackid = Number($('#rackid_' + rowId).val());
					var aisleId =  Number($('#rackaisleId_' + rowId).val());
					var shelfName = $('#shelfName_' + rowId).val();
					if (typeof rackType != 'undefined' && rackType != null
							&& rackType != "" && rackdimension != null
							&& rackdimension != "") {
						rackTypeArray.push(rackType);
						rackdimensionArray.push(rackdimension);
						rackidArray.push(rackid);
						aisleidArray.push(aisleId);
						shelfNameArray.push(shelfName);
					}
				});
		for ( var j = 0; j < rackTypeArray.length; j++) {
			rackDetails += rackTypeArray[j] + "__" + rackdimensionArray[j]
					+ "__" + rackidArray[j] + "__" + aisleidArray[j] +"__" +shelfNameArray[j];
			if (j == rackTypeArray.length - 1) {
			} else {
				rackDetails += "#@";
			}
		}
		return rackDetails;
	}

	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<c:choose>
		<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
							&& COMMENT_IFNO.commentId gt 0}">
			<div class="width85 comment-style" id="hrm">
				<fieldset>
					<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
					<label class="width70">${COMMENT_IFNO.comment}</label>
				</fieldset>
			</div> 
		</c:when>
	</c:choose> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Stores
		</div> 
		<form name="storeValidation" id="storeValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="store-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="storeId" name="storeId"
					value="${STORE_INFO.storeId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right width48">
						<fieldset style="min-height: 120px;"> 
							<div>
								<label class="width30" for="locationId">Location<span
									style="color: red;">*</span> </label> <input type="hidden"
									id="locationId" name="locationId"
									value="${STORE_INFO.cmpDeptLocation.cmpDeptLocId}" />
								<c:choose>
									<c:when test="${STORE_INFO.cmpDeptLocation ne null && 
										STORE_INFO.cmpDeptLocation.cmpDeptLocId > 0}">
										<input type="text" readonly="readonly"
											value="${STORE_INFO.cmpDeptLocation.department.departmentName} &gt; ${STORE_INFO.cmpDeptLocation.location.locationName}"
											name="locationName" id="locationName"
											class="width50 validate[required]" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="locationName"
											id="locationName" class="width50 validate[required]" />
									</c:otherwise>
								</c:choose>
								<span class="button"> <a style="cursor: pointer;"
									id="person"
									class="btn ui-state-default ui-corner-all locationstore-common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="address">Address</label>
								<textarea class="width50" id="address" name="address">${STORE_INFO.address}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 120px;">
							<div>
								<label class="width30" for="storeNumber"> Store number<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when
										test="${STORE_INFO.storeId ne null && STORE_INFO.storeId ne ''}">
										<input name="storeNumber" type="text" readonly="readonly"
											id="storeNumber" value="${STORE_INFO.storeNumber}"
											class="width50 validate[required]">
									</c:when>
									<c:otherwise>
										<input name="storeNumber" type="text" readonly="readonly"
											id="storeNumber" value="${STORE_NUMBER}"
											class="width50 validate[required]">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="address">Store Name<span
									style="color: red;">*</span></label> <input
									type="text" id="storeName" name="storeName"
									value="${STORE_INFO.storeName}" class="width50 validate[required]" />
							</div>
							<div>
								<label class="width30" for="storageType"> Storage Type </label> <select name="storageType"
									id="storageType" class="width51">
									<option value="">Select</option>
									<c:forEach var="STORAGE" items="${STORAGE_TYPES}">
										<option value="${STORAGE.lookupDetailId}">${STORAGE.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStorageType"
									value="${STORE_INFO.lookupDetail.lookupDetailId}" /> <span
									class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="STORAGE_TYPE"
									class="btn ui-state-default ui-corner-all store-storage-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="personId">Person<span
									style="color: red;">*</span> </label> 
								<c:choose>
									<c:when test="${STORE_INFO.person ne null && STORE_INFO.person.personId > 0}">
										<input type="text" readonly="readonly"
											value="${STORE_INFO.person.firstName} ${STORE_INFO.person.lastName}"
											name="personNumber" id="personNumber"
											class="width50 validate[required]" />
											<input type="hidden" id="personId" name="personId"
												value="${STORE_INFO.person.personId}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="personNumber"
											id="personNumber" class="width50 validate[required]" value="${requestScope.personName}"/>
										<input type="hidden" id="personId" name="personId"
												value="${requestScope.personId}" />
									</c:otherwise>
								</c:choose>
								<span class="button"> <a style="cursor: pointer;"
									id="person"
									class="btn ui-state-default ui-corner-all store-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="hrm" class="portlet-content width100"
					style="margin-top: 10px;">
					<fieldset id="hrm">
						<legend>Section Information<span
									style="color: red;">*</span></legend>
						<div class="portlet-content">
							<div id="page-error"
								class="response-msg error ui-corner-all width90"
								style="display: none;"></div>
							<div id="warning_message"
								class="response-msg notice ui-corner-all width90"
								style="display: none;"></div>
							<div id="hrm" class="hastable width100">
								<input type="hidden" name="childCount" id="childCount"
									value="${fn:length(AISLE_LINE_INFO)}" />
								<table id="hastab" class="width100">
									<thead>
										<tr>
											<th style="width: 3%">Reference No</th>
											<th style="width: 5%">Name</th>
											<th style="width: 5%">Section Type</th>
											<th style="width: 3%">Dimension (l*b*h)</th>
											<th style="width: 3%">Rack</th>
											<th style="width: 0.3%">Options</th>
										</tr>
									</thead>
									<tbody class="tab">
										 
										<c:choose>
											<c:when
												test="${requestScope.AISLE_LINE_INFO ne null && requestScope.AISLE_LINE_INFO ne '' && fn:length(AISLE_LINE_INFO)>0}">
												<c:forEach var="bean" items="${AISLE_LINE_INFO}"
													varStatus="status">
													<tr class="rowid" id="fieldrow_${status.index+1}">
														<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
														<td><input type="text" maxlength="8"
															class="width90 aisleNumber" value="${bean.aisleNumber}"
															name="aisleNumber" id="aisleNumber_${status.index+1}" />
														</td>
														<td><input type="text" class="width90 sectionName" value="${bean.sectionName}"
															name="sectionName" id="sectionName_${status.index+1}" />
														</td>
														<td><select name="storageSection"
															id="storageSection_${status.index+1}" class="width80">
																<option value="">Select</option>
																<c:forEach var="SECTION" items="${STORAGE_SECTIONS}">
																	<option value="${SECTION.lookupDetailId}">${SECTION.displayName}</option>
																</c:forEach>
														</select> <input type="hidden"
															id="tempStorageSection_${status.index+1}"
															value="${bean.lookupDetail.lookupDetailId}" /> <span
															class="button" style="position: relative;"> <a
																style="cursor: pointer;" id="STORAGE_SECTION_${status.index+1}"
																class="btn ui-state-default ui-corner-all store-section-lookup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
														<td><input type="text" maxlength="4"
															class="width90 dimension" value="${bean.dimension}"
															name="dimension" id="dimension_${status.index+1}" /></td>
														<td>
															<div
																class="portlet-header ui-widget-header float-right  addBins"
																id="addBins_${status.index+1}" style="cursor: pointer;">Edit
																Rack</div></td>
														<td style="width: 0.01%;" class="opn_td"
															id="option_${status.index+1}"><a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
															id="AddImage_${status.index+1}"
															style="display: none; cursor: pointer;"
															title="Add Record"> <span
																class="ui-icon ui-icon-plus"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
															id="EditImage_${status.index+1}"
															style="display: none; cursor: pointer;"
															title="Edit Record"> <span
																class="ui-icon ui-icon-wrench"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
															id="DeleteImage_${status.index+1}"
															style="cursor: pointer;" title="Delete Record"> <span
																class="ui-icon ui-icon-circle-close"></span> </a> <a
															class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
															id="WorkingImage_${status.index+1}"
															style="display: none;" title="Working"> <span
																class="processing"></span> </a> <input type="hidden"
															id="aisleid_${status.index+1}" name="asileid"
															value="${bean.aisleId}" /></td>
													</tr>
												</c:forEach>
											</c:when>
										</c:choose>
										<c:forEach var="i" begin="${fn:length(AISLE_LINE_INFO)+1}"
											end="${fn:length(AISLE_LINE_INFO)+2}" step="1"
											varStatus="status1">
											<tr class="rowid" id="fieldrow_${i}">
												<td style="display: none;" id="lineId_${i}">${i}</td>
												<td><input type="text" maxlength="8"
													class="width90 aisleNumber" name="aisleNumber"
													id="aisleNumber_${i}" /></td>
												<td><input type="text" class="width90 sectionName"
															name="sectionName" id="sectionName_${i}" />
												</td>
												<td><select name="storageSection"
													id="storageSection_${i}" class="width80">
														<option value="">Select</option>
														<c:forEach var="SECTION" items="${STORAGE_SECTIONS}">
															<option value="${SECTION.lookupDetailId}">${SECTION.displayName}</option>
														</c:forEach>
												</select> <span class="button" style="position: relative;"> <a
														style="cursor: pointer;" id="STORAGE_SECTION_${i}"
														class="btn ui-state-default ui-corner-all store-section-lookup width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
												<td><input type="text" class="width90 dimension"
													name="dimension" id="dimension_${i}" /></td>
												<td>
													<div
														class="portlet-header ui-widget-header float-right  addBins"
														id="addBins_${i}" style="cursor: pointer;">Add
														Rack</div></td>
												<td style="width: 0.01%;" class="opn_td"
													id="option_${i}"><a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
													id="AddImage_${i}"
													style="display: none; cursor: pointer;" title="Add Record">
														<span class="ui-icon ui-icon-plus"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
													id="EditImage_${i}"
													style="display: none; cursor: pointer;" title="Edit Record">
														<span class="ui-icon ui-icon-wrench"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
													id="DeleteImage_${i}"
													style="display: none; cursor: pointer;"
													title="Delete Record"> <span
														class="ui-icon ui-icon-circle-close"></span> </a> <a
													class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
													id="WorkingImage_${i}" style="display: none;"
													title="Working"> <span class="processing"></span> </a> <input
													type="hidden" name="asileid" value="0"
													id="aisleid_${i}" /></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div id="temp-result" style="display: none;"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div
					class="portlet-header ui-widget-header float-right store_discard"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right store_save"
					id="${showPage}" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div style="display: none;"
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>