<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
	var tenderId = 0; var rowId = "";
	var slidetab="";
	$(function() {
		if($('#tenderId').val()==null || $('#tenderId').val()=="" || $('#tenderId').val()==0){ 
			var default_currency=$('#default_currency').val();
			$('#tenderCurrency option').each(function(){  
				var curval=$(this).val();
				if(curval==default_currency){   
					$('#tenderCurrency option[value='+$(this).val()+']').attr("selected","selected"); 
					return false;
				}
			});
		}else{
			$("#tenderCurrency").val("${currencyId}"); 
			var total=0;
			$('.rowid').each(function(){
				var rowId=getRowId($(this).attr('id'));
				var totalamount=Number($('#totalamount_'+rowId).text().trim());
				$('#totalamount_'+rowId).text(Number(totalamount).toFixed(2));
				total=Number(total+totalamount).toFixed(2); 
			}); 
			$('#tenderAmount').val(total);
		} 

		$('.rowid').each(function(){
			var rowId=getRowId($(this).attr('id'));
			$('#itemnature_'+rowId).val($('#tempitemnature_'+rowId).val());
		});
		
		 
		$jquery("#tenderCreation").validationEngine('attach'); 
		$('#selectedDay,#selectedMonth,#selectedYear').change(checkLinkedDays);
		$('#selectedMonth,#linkedMonth').change();
		$('#l10nLanguage,#rtlLanguage').change();
		if ($.browser.msie) {
		       $('#themeRollerSelect option:not(:selected)').remove();
		}
		$('#tenderDate,#tenderExpiry').datepick({
		onSelect: customRange, showTrigger: '#calImg'});
		 
		if(Number($('#tenderId').val())==0){
			manupulateLastRow();
		} 
		
		
		$("#save").click(function() { 
			if($jquery("#tenderCreation").validationEngine('validate')){
 				if($("#tenderId").val() != "" || $("#tenderId").val() != null)
					tenderId = Number($("#tenderId").val()); 
					var tenderNumber = $("#tenderNumber").val(); 
					var tenderCurrency = Number($("#tenderCurrency").val());
					var tenderDate = $("#tenderDate").val();
					var tenderExpiry = $("#tenderExpiry").val(); 
					var description = $("#description").val();
					var tenderDetails = getTenderDetails(); 
					if(tenderDetails.trim()!=null && tenderDetails.trim()!='')
					{
						$('#loading').fadeIn();
						$.ajax({
							type: "POST", 
							url: "<%=request.getContextPath()%>/save_tender.action", 
				     		async: false,
				     		data: {
				     			tenderId: tenderId,
				     			tenderNumber: tenderNumber, 
				     			currencyId: tenderCurrency,
				     			tenderDate: tenderDate,
				     			tenderExpiry: tenderExpiry, 
				     			description : description,
				     			tenderDetails: tenderDetails
				     		},
							dataType: "json",
							cache: false,
							success: function(response){ 
								if(response.returnMessage=="SUCCESS"){
									$.ajax({
										type: "POST", 
										url: "<%=request.getContextPath()%>/tenderListRedirect.action", 
								     	async: false, 
										dataType: "html",
										cache: false,
										success: function(result){ 
											$('#common-popup').dialog('destroy');		
											$('#common-popup').remove();
											$("#main-wrapper").html(result);   
										} 		
									}); 
									if(tenderId > 0)
										$('#success_message').hide().html("Record Updated").slideDown(1000); 
									else
										$('#success_message').hide().html("Record Created").slideDown(1000); 
									$('#success_message').delay(2000).slideUp();
								}
								else{
									$('#page-error').hide().html(response.returnMessage).slideDown(1000);
									$('#page-error').delay(2000).slideUp();
									return false;
								}  
							} 		
						}); 
					}else{
						 $('#page-error').hide().html("Enter Product Information").slideDown(1000);
						 $('#page-error').delay(2000).slideUp(2000);
					}
 			} else {
				return false;
			}
			});
		
			$("#discard").click(function(){  
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/tenderListRedirect.action", 
			     	async: false,
					dataType: "html",
					cache: false,
					success: function(result) {  
						$('#common-popup').dialog('destroy');		
						$('#common-popup').remove();  
						$("#main-wrapper").html(result); 
					} 		
				});  
			}); 
			
			$('#common-popup').dialog({
				 autoOpen: false,
				 minwidth: 'auto',
				 width:800,   
				 bgiframe: false,
				 overflow:'hidden',
				 modal: true 
			});
			
			$('.delrow').live('click',function(){ 
				 slidetab=$(this).parent().parent().get(0);   
		       	 $(slidetab).remove();  
		       	 var i=1;
		    	 $('.rowid').each(function(){   
		    		 var rowId=getRowId($(this).attr('id')); 
		    		 $('#lineId_'+rowId).html(i);
					 i=i+1; 
		 		 });  
		 		 return false; 
			});
			
			$('.tquantity').live('change',function(){
				rowId=getRowId($(this).attr('id'));
				var quantity=Number($(this).val());
				var total=Number(0); 
				var unitrate=Number($('#unitrate_'+rowId).val());
				if(quantity>0){
					total=unitrate*quantity;
					total = Number(total).toFixed(2);
					if(total>0)
						$('#totalamount_'+rowId).text(total);
					triggerAddRow(rowId);
				}
			});

			$('.tunitrate').live('change',function(){
				rowId=getRowId($(this).attr('id'));
				var unitrate=Number($(this).val()).toFixed(2);
				$(this).val(unitrate);
				var total=Number(0); 
				var quantity=Number($('#quantity_'+rowId).val());
				if(unitrate!=null && quantity>0 && unitrate>0 ){
					total=unitrate*quantity;
					total = Number(total).toFixed(2);
					$('#totalamount_'+rowId).text(total);
					triggerAddRow(rowId);
				}else if(unitrate==0){
					total=unitrate*quantity;
					total = Number(total).toFixed(2);
					$('#totalamount_'+rowId).text(total);
					triggerAddRow(rowId);
				}
			});

			 $('#product-common-popup').live('click',function(){  
				   $('#common-popup').dialog('close'); 
			   });
			
			$('.product-common-popup').live('click',function(){  
				$('.ui-dialog-titlebar').remove();  
				var rowId=getRowId($(this).attr('id'));
				var itemType=$('#itemnature_'+rowId).val();
 				 $.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/show_purchase_product_common_popup.action", 
				 	async: false,  
				 	data:{itemType: itemType, rowId:rowId},
				    dataType: "html",
				    cache: false,
					success:function(result){  
						 $('.common-result').html(result);  
						 $('#common-popup').dialog('open');
						 $($($('#common-popup').parent()).get(0)).css('top',0);
					},
					error:function(result){  
						 $('.common-result').html(result); 
					}
				});  
					return false;
			});
			
			 $('.addrows').click(function(){ 
				  var i=Number(1); 
				  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
				  id=id+1;  
				  $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/tender_addrow.action",
				async : false,
				data : {
					rowId : id
				},
				dataType : "html",
				cache : false,
				success : function(result) {
					$('.tab tr:last').before(result);
					if ($(".tab").height() > 255)
						$(".tab").css({
							"overflow-x" : "hidden",
							"overflow-y" : "auto"
						});
					$('.rowid').each(function() {
						var rowId = getRowId($(this).attr('id'));
						$('#lineId_' + rowId).html(i);
						i = i + 1;
					});
				}
			});
			return false;
		});

		var getTenderDetails = function() {
			var tenderDetails = "";
			var productid = new Array();
			var unitrate = new Array();
			var totalunit = new Array();
			var lineDescriptions = new Array();
			var lineid = new Array();
			var itemnatures = new Array();
			$('.rowid').each(
					function() {
						var rowId = getRowId($(this).attr('id'));
						var productId = $('#productid_' + rowId).val();
						var unitRate = Number($('#unitrate_' + rowId).val());
						var quantity = Number($('#quantity_' + rowId).val());
						var tenderLineId = Number($('#tenderLineId_' + rowId)
								.val());
						var linedescription = $('#linedescription_' + rowId)
								.val();
						var itemnature = $('#itemnature_' + rowId).val();
						if (typeof productId != 'undefined'
								&& productId != null && productId != ""
								&& quantity != null && quantity != ""
								&& quantity > 0) {
							productid.push(productId);
							unitrate.push(unitRate);
							totalunit.push(quantity);
							lineid.push(tenderLineId);
							if (linedescription != null
									&& linedescription != "")
								lineDescriptions.push(linedescription);
							else
								lineDescriptions.push("##");
							itemnatures.push(itemnature);
						}
					});
			for ( var j = 0; j < productid.length; j++) {
				tenderDetails += productid[j] + "__" + unitrate[j] + "__"
						+ totalunit[j] + "__" + lineid[j] + "__"
						+ lineDescriptions[j] + "__" + itemnatures[j];
				if (j == productid.length - 1) {
				} else {
					tenderDetails += "#@";
				}
			}
			return tenderDetails;
		};
	});

	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}

	function checkLinkedDays() {

		var daysInMonth = $.datepick.daysInMonth($('#selectedYear').val(), $(
				'#selectedMonth').val());
		$('#selectedDay option:gt(27)').attr('disabled', false);
		$('#selectedDay option:gt(' + (daysInMonth - 1) + ')').attr('disabled',
				true);
		if ($('#selectedDay').val() > daysInMonth) {
			$('#selectedDay').val(daysInMonth);
		}
	}

	function customRange(dates) {
		if (this.id == 'tenderDate') {
			$('#tenderExpiry').datepick('option', 'minDate', dates[0] || null);
			if ($('#tenderDate').val() != "") {
				$('.tenderDateformError').hide();
				addPeriods();
			}
		} else {
			$('#tenderDate').datepick('option', 'maxDate', dates[0] || null);
		}
	}
	function addPeriods() {
		var date = new Date($('#tenderDate').datepick('getDate')[0].getTime());
		$.datepick.add(date, parseInt(30, 10), 'd');
		$('#tenderExpiry').val($.datepick.formatDate(date));
	}
	function triggerAddRow(rowId) {
		var itemNature = $('#itemnature_' + rowId).val();
		var productid = $('#productid_' + rowId).val();
		//var unitrate=$('#unitrate_'+rowId).val();
		var quantity = $('#quantity_' + rowId).val();
		var nexttab = $('#fieldrow_' + rowId).next();
		if (itemNature != null && itemNature != "" && productid != null
				&& productid != ""
				//&& unitrate!=null && unitrate!=""
				&& quantity != null && quantity != ""
				&& $(nexttab).hasClass('lastrow')) {
			$('#DeleteImage_' + rowId).show();
			$('.addrows').trigger('click');
		}
		var total = 0;
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var temptotal = Number($('#totalamount_' + rowId).text().trim());
			total = Number(temptotal + total);
			$('#tenderAmount').val(total);
		});
	}

	function manupulateLastRow() {
		var hiddenSize = 0;
		$($(".tab>tr:first").children()).each(function() {
			if ($(this).is(':hidden')) {
				++hiddenSize;
			}
		});
		var tdSize = $($(".tab>tr:first").children()).size();
		var actualSize = Number(tdSize - hiddenSize);
		$('.tab>tr:last').removeAttr('id');
		$('.tab>tr:last').removeClass('rowid').addClass('lastrow');
		$($('.tab>tr:last').children()).remove();
		for ( var i = 0; i < actualSize; i++) {
			$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
		}
	}

	function commonProductPopup(productId, productName, rowId) {
		$('#productid_' + rowId).val(productId);
		$('#product_' + rowId).html(productName);
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			tender
		</div> 
		<div>
			<form id="tenderCreation" style="position: relative;">
				<div class="portlet-content">
					<div class="width100 float-left" id="hrm">
						<div>
							<div class="tempresult" style="display: none;"></div>
							<div id="page-error" class="response-msg error ui-corner-all"
								style="display: none;"></div>

							<div class="width48 float-left" id="hrm">
								<fieldset style="height: 100px;">
									<input type="hidden" id="tenderId" value="${TENDER.tenderId}">
									<div>
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.add.tenderNo" /><span
											class="mandatory">*</span> </label>
										<c:choose>
											<c:when
												test="${TENDER.tenderNumber ne null && TENDER.tenderNumber ne ''}">
												<input type="text" name="tenderNumber"
													value="${TENDER.tenderNumber}" id="tenderNumber"
													readonly="readonly" class="width60 validate[required]"
													TABINDEX=1>
											</c:when>
											<c:otherwise>
												<input type="text" name="tenderNumber"
													value="${TENDER_NUMBER}" id="tenderNumber"
													readonly="readonly" class="width60 validate[required]"
													TABINDEX=1>
											</c:otherwise>
										</c:choose>
									</div>
									<div style="display: none;">
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.amount" /> </label> <input type="text"
											name="tenderAmount" id="tenderAmount" readonly="readonly"
											class="width60" TABINDEX=2>
									</div>
									<div>
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.add.tenderDate" /><span
											class="mandatory">*</span> </label> <input type="text"
											name="tenderDate" value="${TENDER_tenderDate}"
											readonly="readonly" id="tenderDate"
											class="width60 validate[required]" TABINDEX=4>
									</div>
									<div>
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.add.tenderExpiry" /><span
											class="mandatory">*</span> </label> <input type="text"
											name="tenderExpiry" value="${TENDER_tenderExpiry}"
											readonly="readonly" id="tenderExpiry"
											class="width60 validate[required]" TABINDEX=5>
									</div>
								</fieldset>
							</div>
							<div class="width48 float-right" id="hrm">
								<fieldset style="height: 100px;"> 
								<div>
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.currency" /><span class="mandatory">*</span>
										</label> <select name="tenderCurrency" id="tenderCurrency"
											class="width60 validate[required]" TABINDEX=3>
											<option value="">
												<fmt:message key="accounts.tender.add.selectCurrency" />
											</option>
											<c:choose>
												<c:when
													test="${TENDER_CURRENCY ne null and TENDER_CURRENCY ne ''}">
													<c:forEach items="${TENDER_CURRENCY}" var="CURRENCY">
														<option value="${CURRENCY.currencyId}">${CURRENCY.currencyPool.code}</option>
													</c:forEach>
												</c:when>
											</c:choose>
										</select> <input type="hidden" id="default_currency"
											value="${DEFAULT_CURRENCY}" />
									</div> 
									<div>
										<label class="width30 tooltip"><fmt:message
												key="accounts.tender.add.description" /> </label>
										<textarea rows="2" id="description" class="width60 float-left">${TENDER.description}</textarea>
									</div>
									<div style="display: none;">
										<label class="width30 tooltip">Status<span
											class="mandatory">*</span> </label> <select disabled="disabled"
											name="tenderStatus" id="tenderStatus"
											class="width60 validate[required]" TABINDEX=6>
											<option value="true">Approved</option>
											<option value="false" selected="selected">Un-Approved</option>
										</select>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content quotation_detail"
					style="margin-top: 10px;" id="hrm">
					<fieldset>
						<legend>
							<fmt:message key="accounts.tender.add.productInfo" />
							<span class="mandatory">*</span>
						</legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.itemType" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.product" /></th>
										<th style="display: none;"><fmt:message
												key="accounts.tender.add.uom" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.unitRate" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.quantity" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.totalAmount" /></th>
										<th style="width: 5%"><fmt:message
												key="accounts.tender.add.description" /></th>
										<th style="width: 0.01%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:choose>
										<c:when
											test="${TENDER_DETAIL_INFO ne null && TENDER_DETAIL_INFO ne '' && fn:length(TENDER_DETAIL_INFO)>0}">
											<c:forEach var="detail" items="${TENDER_DETAIL_INFO}"
												varStatus="status">
												<tr class="rowid" id="fieldrow_${status.index+1}">
													<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
													<td><select name="" class="width98 itemnature"
														disabled="disabled" id="itemnature_${status.index+1}"> 
															<option value="A">
																<fmt:message key="accounts.type.options.asset" />
															</option>
															<option value="E">
																<fmt:message key="accounts.type.options.expense" />
															</option>
															<option value="I">
																<fmt:message key="accounts.type.options.inventory" />
															</option>
													</select> <input type="hidden" id="tempitemnature_${status.index+1}"
														name="itemnature" value="${detail.product.itemType}" />
													</td>
													<td><span class="float-left width60"
														id="product_${status.index+1}">${detail.product.productName}</span>
														<span class="button float-right prod quoteprod"
														style="height: 16px;"> <a
															style="cursor: pointer; height: 100%;"
															id="productline_${status.index+1}"
															class="btn ui-state-default ui-corner-all product-common-popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
														type="hidden" id="productid_${status.index+1}"
														value="${detail.product.productId}" style="border: 0px;" />
													</td>
													<td style="display: none;" id="uom_${status.index+1}" class="uom">
														${detail.product.lookupDetailByProductUnit.displayName}</td>
													<td><input type="text"
														class="width96 tunitrate validate[optional,custom[number]]"
														id="unitrate_${status.index+1}" value="${detail.unitRate}"
														style="text-align: right;" />
													</td>
													<td><input type="text"
														class="width96 tquantity validate[optional,custom[number]]"
														id="quantity_${status.index+1}" value="${detail.quantity}"/>
													</td>
													<td class="totalamount" id="totalamount_${status.index+1}"
														style="text-align: right;"><c:out
															value="${detail.unitRate * detail.quantity}" />
													</td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status.index+1}"
														value="${detail.description}" />
													</td>
													<td style="display: none;"><input type="hidden"
														id="tenderLineId_${status.index+1}"
														value="${detail.tenderDetailId}" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status.index+1}"
														style="display: none; cursor: pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status.index+1}"
														style="display: none; cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status.index+1}" style="display: none;"
														title="Working"> <span class="processing"></span> </a>
													</td>
												</tr>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<c:forEach var="i" begin="0" end="1" step="1"
												varStatus="status1">
												<tr class="rowid" id="fieldrow_${status1.index+1}">
													<td id="lineId_${status1.index+1}" style="display: none;">${status1.index+1}</td>
													<td><select name="" class="width98 itemnature"
														id="itemnature_${status1.index+1}">
 															<option value="A">Asset</option>
															<option value="E">Expense</option>
															<option value="I" selected="selected">Inventory</option>
													</select> 
													</td>
													<td><span class="float-left width60"
														id="product_${status1.index+1}"></span> <span
														class="button float-right prod" style="height: 16px;">
															<a style="cursor: pointer; height: 100%;"
															id="productline_${status1.index+1}"
															class="btn ui-state-default ui-corner-all product-common-popup width100 float-right">
																<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
														type="hidden" id="productid_${status1.index+1}"
														style="border: 0px;" />
													</td>
													<td style="display:none;" id="uom_${status1.index+1}" class="uom"></td>
													<td><input type="text"
														class="width96 tunitrate validate[optional,custom[number]]"
														id="unitrate_${status1.index+1}"
														style="text-align: right;" />
													</td>
													<td><input type="text"
														class="width96 tquantity validate[optional,custom[number]]"
														id="quantity_${status1.index+1}" />
													</td>
													<td class="totalamount" id="totalamount_${status1.index+1}"
														style="text-align: right;"></td>
													<td><input type="text" name="linedescription"
														id="linedescription_${status1.index+1}" />
													</td>
													<td style="display: none;"><input type="hidden"
														id="tenderLineId_${status1.index+1}" />
													</td>
													<td style="width: 0.01%;" class="opn_td"
														id="option_${status1.index+1}"><a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
														id="AddImage_${status1.index+1}"
														style="display: none; cursor: pointer;" title="Add Record">
															<span class="ui-icon ui-icon-plus"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
														id="EditImage_${status1.index+1}"
														style="display: none; cursor: pointer;"
														title="Edit Record"> <span
															class="ui-icon ui-icon-wrench"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
														id="DeleteImage_${status1.index+1}"
														style="display: none; cursor: pointer;"
														title="Delete Record"> <span
															class="ui-icon ui-icon-circle-close"></span> </a> <a
														class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
														id="WorkingImage_${status1.index+1}"
														style="display: none;" title="Working"> <span
															class="processing"></span> </a>
													</td>
												</tr>
											</c:forEach>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</form>
		</div>
		<div class="clearfix"></div>
		
		<div style="display: none;"
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
			<div class="portlet-header ui-widget-header float-left addrows"
				style="cursor: pointer;">
				<fmt:message key="accounts.common.button.addrow" />
			</div>
		</div>
		<div
			style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
			class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
			tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
			<div
				class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
				unselectable="on" style="-moz-user-select: none;">
				<span class="ui-dialog-title" id="ui-dialog-title-dialog"
					unselectable="on" style="-moz-user-select: none;">Dialog
					Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
					role="button" unselectable="on" style="-moz-user-select: none;"><span
					class="ui-icon ui-icon-closethick" unselectable="on"
					style="-moz-user-select: none;">close</span> </a>
			</div>
			<div id="common-popup" class="ui-dialog-content ui-widget-content"
				style="height: auto; min-height: 48px; width: auto;">
				<div class="common-result width100"></div>
				<div
					class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
					<button type="button" class="ui-state-default ui-corner-all">Ok</button>
					<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
