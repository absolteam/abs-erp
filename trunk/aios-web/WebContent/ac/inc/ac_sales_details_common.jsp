<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/thickbox-compressed.js"></script>
<script src="fileupload/FileUploader.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/thickbox.css"
	type="text/css" />
	
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/dateDifference.js"></script>

<style>
.container {float: left;padding: 1px;}
.container select{width: 31em;height:15em;}
input[type="button"]{width: 5em;}
.low{position: relative; top: 55px;} 

#allowance-manipulation-div>#main-content{
	overflow-y: hidden;
	border:none;
}
#allowance-manipulation-div>#main-content #main-content{
	overflow-y: hidden;
	border:none;
}
</style> 

<!-- AJAX SUCCESS TEST FUNCTION	-->
 <script type="text/javascript"> 
 var projectInventories =[]

$(function() {
	 $('#return_discard').click(function(event){  
    		returnDiscard("");
    		 return false;
    	 });
		 $('#return_save').click(function(){ 
			 if($jquery("#projectValidation").validationEngine('validate')){ 
	 			var referenceNumber = $('#referenceNumber').val(); 
	 			var salesReference = $('#salesReferenceGiven').val(); 
		 		var useCase = $('#useCase').val(); 
	 			var recordId = Number($('#recordId').val());
	 			var customerId = Number($('#customerId').val());   
	 			var description=$('#description').val();  
	 			var totalAmount=Number($('#grantTotal').text());  
	 			var status	= Number($('#status option:selected').val());   
	 			var isPercentage=$('input[name=processType]:checked').val();
	 			if(isPercentage=='true')
	 				isPercentage=true;
	 			else if(isPercentage=='false')
	 				isPercentage=false;
	 			else
	 				isPercentage=null;
	 			projectInventories=getInventoryDetails();
				$.ajax({
					type:"POST",
					url:"<%=request.getContextPath()%>/save_sales_return.action", 
				 	async: false, 
				 	data:{referenceNumber:referenceNumber,useCase:useCase,recordId:recordId,
				 		customerId:customerId,
				 		description:description,
				 		totalAmount:totalAmount,
				 		status:status,
				 		isPercentage:isPercentage,
				 		salesReference:salesReference,
				 		projectInventories:JSON.stringify(projectInventories)
					 	 },
				    dataType: "json",
				    cache: false,
					success:function(response){   
						var creditId= Number(response.returnMessage);
						
						 if(creditId>0){
							 window.open("<%=request.getContextPath()%>/sales_return_printout.action?recordId="+creditId
					 					+"&format=HTML",
									'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
							 returnDiscard("");
						 }else{
							 $('#page-error').hide().html(response.returnMessage).slideDown(1000);
							 $('#page-error').delay(2000).slideUp();
							 return false;
						 };
					},
					error:function(response){  
						$('#page-error').hide().html("Error!! Sales Detail has been deleted already for the given reference").slideDown(1000);
						$('#page-error').delay(2000).slideUp();
					}
				}); 
			}else{
				return false;
			}
		 }); 
		 
		 var getInventoryDetails = function(){ 
			 projectInventories = [];
		 		$('.inventoryrowid').each(function(){ 
			 		
		 			var rowId = getRowId($(this).attr('id'));  
		 			var productId = Number($('#productId_'+rowId).val());  
		 			var shelfId = Number($('#shelfId_'+rowId).val());
		 			var quantity = $('#quantity_'+rowId).text();  
		 			var unitRate = $('#unitRate_'+rowId).text();
		 			var discount = Number($('#discount_'+rowId).text());

		 			//Importent variables to process the return
		 			var recordId = Number($('#recordId_'+rowId).val());
		 			var returnQuantity = Number($('#returnQuantity_'+rowId).val());  
		 			var deduction = $('#returnDeduction_'+rowId).val();
		 			var returnAmount = Number($('#returnAmount_'+rowId).text());
		 			if(typeof productId != 'undefined' && productId !=null  && productId!='' && returnQuantity !=null  && returnQuantity!=''
		 					&& returnAmount!='' && returnAmount>0){
			 			var jsonData = [];
						jsonData.push({
							"productId" : productId,
							"shelfId":shelfId,
							"unitRate":unitRate,
							"quantity" : quantity,
							"returnAmount": returnAmount,
							"recordId" : recordId,
							"discount":discount,
							"returnQuantity":returnQuantity,
							"deduction":deduction
						});   
						projectInventories.push({
							"projectInventories" : jsonData
						});
		 			}
		 		}); 
			
				return projectInventories;
			};
			
			
			
			$('#returnAllItem').change(function(){ 
				if($('#returnAllItem').attr("checked")){
					$('.inventoryrowid').each(function() {
						 var rowId = getRowId($(this).attr('id'));
				 	 	 var returnQuantity =  Number($('#quantity_'+rowId).text());
				 	 	 var quantity = Number($('#quantity_'+rowId).text());  
				 	 	 
			 			 var totalAmount = Number($('#totalAmount_'+rowId).text()); 
			 			var oneQantity=totalAmount/quantity;
			 			
				 	 	 if(returnQuantity> 0){
				 	 		$('#returnQuantity_'+rowId).val(returnQuantity);
				 	 		$('#returnAmount_'+rowId).text((Number(returnQuantity) *  Number(oneQantity)));
				 	 	 }
						
					});
				}else{
					$('.inventoryrowid').each(function() {
					 var rowId = getRowId($(this).attr('id'));
				 	 		$('#returnQuantity_'+rowId).val("");
				 	 		$('#returnAmount_'+rowId).text("");
						
					});
				}
				
				calculateGradeTotal();
		 		return false;
		 	 });

			$('.returnQuantity').change(function(){ 
		 		 var rowId = getRowId($(this).attr('id'));
		 	 	 var returnQuantity =  Number($('#returnQuantity_'+rowId).val());
		 	 	 var quantity = Number($('#quantity_'+rowId).text());  
		 	 	 if(returnQuantity>quantity){
		 	 		 $('#page-error').hide().html("Return quantity can not be more than actual quantity").slideDown(1000);
					 $('#page-error').delay(3000).slideUp();
					 $('#returnQuantity_'+rowId).val("");
					 $('#returnAmount_'+rowId).text("");
					 $('#returnDeduction_'+rowId).val("");
					 return false;
		 	 	 }
		 	 		 
	 			var totalAmount = Number($('#totalAmount_'+rowId).text()); 
	 			var oneQantity=totalAmount/quantity;
	 			
		 	 	 if(returnQuantity> 0){
		 	 		$('#returnAmount_'+rowId).text((Number(returnQuantity) *  Number(oneQantity)));
		 	 	 }
		 	 	calculateGradeTotal();
		 		return false;
		 	 });
			
			$('.returnDeduction').change(function(){ 
		 		 var rowId = getRowId($(this).attr('id'));
		 	 	 var returnQuantity =  Number($('#returnQuantity_'+rowId).val());
		 	 	 var returnDeduction =  Number($('#returnDeduction_'+rowId).val());
		 	 	 var quantity = Number($('#quantity_'+rowId).text());  
	 			 var totalAmount = Number($('#totalAmount_'+rowId).text()); 
	 			var oneQantity=totalAmount/quantity;
	 			
		 	 	 if(returnQuantity> 0){
		 	 		$('#returnAmount_'+rowId).text((Number(returnQuantity) *  Number(oneQantity))-returnDeduction);
		 	 	 }
		 	 	calculateGradeTotal();
		 		return false;
		 	 });
	});
 
 	function calculateGradeTotal(){
 		var totalAmount=Number(0.0);
		$('.inventoryrowid').each(function() {
			 var rowId = getRowId($(this).attr('id'));
		 	 totalAmount+=Number($('#returnAmount_'+rowId).text());
				
		});
		$('#grantTotal').text(totalAmount);
 	}
	function getRowId(id){   
		var idval=id.split('_');
		var rowId=Number(idval[1]);
		return rowId;
	}
	function returnDiscard(message){ 
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/sales_return_list.action",
				async : false,
				dataType : "html",
				cache : false,
				success : function(result) {
					$('#common-popup').dialog('destroy');
					$('#common-popup').remove(); 
					$('#descriptive-popup').dialog('destroy');
					$('#descriptive-popup').remove();
					$('#accessrights-popup').dialog('destroy');
					$('#accessrights-popup').remove();
					$("#main-wrapper").html(result); 
					if (message != null && message != '') {
						$('#success_message').hide().html(message).slideDown(1000);
						$('#success_message').delay(2000).slideUp();
					}
				}
			});
		}
</script>

<div>
	<c:choose>
	<c:when test="${SALES_INFORMATION.referenceNumber ne null && SALES_INFORMATION.pointOfSaleDetailVOs ne null}">
  	<form id="projectValidation" class="" name="projectValidation" method="post" style="position: relative;">
		<div class="width100 float-left" id="hrm">
			<div class="width100 float-left">
				<input type="hidden" id="useCase" value="${SALES_INFORMATION.useCase}">
				<input type="hidden" id="recordId" value="${SALES_INFORMATION.recordId}">
				<input type="hidden" id="referenceNumber" value="${referenceNumber}">
				<input type="hidden" id="customerId" value="${SALES_INFORMATION.customer.customerId}">
				<input type="hidden" id="salesReferenceGiven" value="${SALES_INFORMATION.salesReceiptNumber}">
				<fieldset style="min-height: 100px;">
					<legend>
						Customer Information
					</legend>
						<div class="width50 float-left">
						<div class="float-left width100" style="margin-bottom: 10px;font-size: 20px;">
							<label class="width30">Return Reference :</label> 
							<label class="width60">${referenceNumber}</label> 
						</div>
						<div class="float-left width100" style="margin-bottom: 10px;font-size: 16px;">
							<label class="width30">Sales Receipt :</label> 
							<label class="width60">${SALES_INFORMATION.salesReceiptNumber}</label> 
						</div>
						<div class="float-left width100" style="margin-bottom: 10px;">
							<label class="width30">Sales Reference :</label> 
							<label class="width60">${SALES_INFORMATION.referenceNumber}</label> 
						</div>
						<div class="float-left width100" style="margin-bottom: 10px;">
							<label class="width30">Sales Date :</label> 
							<label class="width60">${SALES_INFORMATION.date}</label> 
						</div>
						<div class="float-left width100" style="margin-bottom: 10px;">
							<label class="width30">Customer :</label> 
							<label class="width60">${SALES_INFORMATION.customerName}</label> 
							
						</div>
						<div class="float-left width100" style="margin-bottom: 10px;">
							<label class="width30">Customer Number :</label> 
							<label class="width60">${SALES_INFORMATION.customer.customerNumber}</label> 
						</div>
						
					</div>
					<div class="width50 float-left">
						<div class="float-left width100">
							<div class="float-left width60">
								<fieldset style="min-height: 50px;">
									<legend>
										Reimpersment Option
									</legend>
									<input type="radio" name="processType" checked="checked" class="float-left width5" value="true">
									<label class="float-left width40">Direct Payment</label> 
									<input type="radio" name="processType" class="float-left width5" value="false">
									<label class="float-left width40">Sales Exchange</label> 
								<!-- 	<input type="radio" name="processType" class="float-left width5" value="free">
									<label class="float-left width20">Free</label>  -->
								</fieldset>
							</div>
						</div>
						<div class="float-left width100" style="display: none;">
							<label class="width100">Status</label>
							<select name="status"
								class="width61 status"
								id="status">
								<c:forEach var="resourceType" items="${RETURN_STATUS}">
									<option value="${resourceType.key}">${resourceType.value}</option>
								</c:forEach>
							</select>
						</div>
						<div class="float-left width100">
							<label class="width100">Description :</label> 
							<textarea rows="3" class="width61" name="description" id="description"></textarea>
						</div>
						
					</div>
				</fieldset>
				<div class="clearfix"></div>
				<fieldset style="min-height: 100px;">
				<legend>
						Order Details
					</legend>
				<div class="float-left width50" style="margin:5px;">
					<input type="checkbox" id="returnAllItem" class="float-left width5">
					<label class="float-left width20" style="margin-right: 10px;margin-top: 3px;">Return All Item</label> 
				</div>
				<div id="hrm" class="width100 hastable float-left">
					<table id="inventoryhastab" class="width100">
						<thead>
							<tr>
								<th>No.</th>
								<th>Product</th>
								<th>Quantity</th>
								<th>Unit Rate</th>
								<th>Discount</th>
								<th>Total</th>
								<th>Return Quantity</th>
								<th>Penalty / Deduction</th>
								<th>Return Amount</th>
							</tr>
						</thead>
						<tbody class="inventorytab">
							<c:forEach var="DETAIL"
								items="${SALES_INFORMATION.pointOfSaleDetailVOs}"
								varStatus="status">
								<tr class="inventoryrowid" id="inventoryfieldrow_${status.index+1}">
									<td id="inventorylineId_${status.index+1}">${status.index+1}</td>
									<td id="product_${status.index+1}">${DETAIL.productByProductId.code}-${DETAIL.productByProductId.productName}</td>
									<td id="quantity_${status.index+1}">${DETAIL.quantity}</td>
									<td id="unitRate_${status.index+1}">${DETAIL.unitRate}</td>
									<td id="discount_${status.index+1}">${DETAIL.discountValue}</td>
									<td id="totalAmount_${status.index+1}">${(DETAIL.quantity * DETAIL.unitRate)-DETAIL.discountValue}</td>
									<td>
										<input type="text" id="returnQuantity_${status.index+1}" 
										class="returnQuantity width98 validate[optional,custom[number]] right-align" />
									</td>
									<td>
										<input type="text" id="returnDeduction_${status.index+1}" 
										class="returnDeduction width98 validate[optional,custom[number]] right-align" />
									</td>
									<td id="returnAmount_${status.index+1}"></td>
									<td style="display: none;">
										<input type="hidden" id="productId_${status.index+1}" value="${DETAIL.productByProductId.productId}">
										<input type="hidden" id="recordId_${status.index+1}" value="${DETAIL.recordId}">
										<input type="hidden" id="shelfId_${status.index+1}" value="${DETAIL.shelfId}">
									</td>
								</tr>
							</c:forEach>
							<tr class="inventoryrowid" >
								<td colspan="8" style="text-align: right;">Total Return</td>
								<td id="grantTotal" colspan="1" style="font-size: 16px;font-weight: bold;"></td>
							</tr>
						</tbody>
					</table>
				</div>		
				</fieldset>
			</div>
		</div>
		</form>
		<div
			class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
			style="margin: 10px;">
			<div class="portlet-header ui-widget-header float-right"
				id="return_discard" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.cancel" />
			</div>
			<div class="portlet-header ui-widget-header float-right"
				id="return_save" style="cursor: pointer;">
				<fmt:message key="accounts.common.button.save" />
			</div>
		</div>
	</c:when>
	<c:when test="${SALES_INFORMATION.referenceNumber ne null}">
		<fieldset style="min-height: 100px;">
			<div id="hrm" class="width90">${SALES_INFORMATION.referenceNumber}</div>
		</fieldset>
	</c:when>
	<c:otherwise>
		<fieldset style="min-height: 100px;">
			<div id="hrm" class="width90">No Sales Found</div>
		</fieldset>
	</c:otherwise>
	</c:choose>
</div>