<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
$(function() {
	$('.popup-close').click(function() {
		$('#common-popup-new').dialog("close");
	});
	$('.approver-submit').click(function() {
		var password = $('#password').val(); 
		var pointOfSaleId = $('#pointOfSaleId').val(); 
		var pointOfSaleTempId = $('#pointOfSaleTempId').val(); 
		if(typeof pointOfSaleId == 'undefined' ||  pointOfSaleId == '')
			pointOfSaleId = pointOfSaleTempId;
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/verify-password.action",
			data:{password:password,pointOfSaleId:pointOfSaleId},
			async : false,
			dataType : "json",
			cache : false,
			success : function(response) {
				$('#password').val("");
				if (response.returnMessage == "SUCCESS"){ 
					$('#common-popup-new').dialog("close");
				}else{
					
					alert("Password is not correct");
				}
			}
		});
			
		return false;
	});
	
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Transaction Modification Approval</div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    
	   	    <form id="POSUserTillForm" name="POSUserTillForm" style="position: relative;">
				<div id="hrm" class="float-left width70"  >
					<input type="hidden" id="posUserTillId"  />
					<fieldset style="min-height: 170px;">
						  <legend>Approver Information</legend> 
					      
			      	  	<div class="float-left width100" style="padding:10px;position: relative;">
			      	  		<label class="float-left width40">Name</label>
							<label class="float-left width50">${PERSON_INFO.firstName} ${PERSON_INFO.lastName}</label>
						</div>
						<div class="float-left width100" style="padding:10px;position: relative;">
			      	  		<label class="float-left width40">Identification Number</label>
							<label class="float-left width50">${PERSON_INFO.personNumber}</label>
						</div>
						<div class="float-left width100" style="padding:10px;position: relative;">
			      	  		<label class="float-left width40">Login UserName</label>
			      	  		<c:set var="string1" value="${USER_INFO.username}"/>
			      	  		<c:set var="string2" value="${fn:split(string1, '_')}"/>
							<label class="float-left width50">${string2[0]}</label>
						</div>
						<div class="float-left width100" style="padding:10px;position: relative;">
			      	  		<label class="float-left width40">Password<span style="color:red">*</span></label>
							<input type="password" id="password"  class=" float-left width50 validate[required]"
								value=""/>
							<input type="hidden" id="tempPassword" value="${USER_INFO.password}" class="tempPassword" />
							<input type="hidden" id="posSessionId" value="${posSessionId}" class="posSessionId" />
						</div>	
						<div style="font-weight: bold;font-size: 11pt;color: red;">
						Note: After the verification window will reload. User must follow the same steps again to modify/delete</div>						
				 </fieldset>
			</div>
		</form>
 		<div class="clearfix"></div>
		<div class="float-right buttons ui-widget-content ui-corner-all">
		<div class="portlet-header ui-widget-header float-right popup-close" id="popup-close">Close</div>
			<div class="portlet-header ui-widget-header float-right approver-submit" id="approver-submit">Submit</div>
		</div>		
			
		</div>
	</div>
	
</div>