<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<style type="text/css">
#purchase_print {
	overflow: hidden;
}

.portlet-content {
	padding: 1px;
}

.buttons {
	margin: 4px;
}

table.display td {
	padding: 3px;
}

.ui-widget-header {
	padding: 4px;
}

.ui-autocomplete {
	width: 17% !important;
	height: 200px !important;
	overflow: auto;
}

.ui-autocomplete-input {
	width: 65%!important;
}

.ui-combobox-button{height: 32px;}

button {
	position: absolute !important;
	margin-top: 2px;
	height: 22px;
}

label {
	display: inline-block;
}
</style>
<script type="text/javascript">
var selectedRowId =0;
var selectedRowIds = [];
var oTable;
var totalSales = Number(0);
var fromDate = null;
var toDate = null;

var selectedUserTillId = 0;
var selectedStoreId = 0;
var selectedPersonId = 0;

populateDatatable(); 

function populateDatatable(){ 
	$('#SalesReportDT').dataTable({ 
		"sAjaxSource": "get_pos_sales_report_list_json.action?userTillId="+selectedUserTillId
		+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&reportType=pos",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "bDestroy" : true,
	    "iDisplayLength": 25,
	    "aoColumns": [ 
			{ "sTitle": "POS Id", "bVisible": false},
			{ "sTitle": "S.No"},
			{ "sTitle": "Reference"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Store Name"},
			{ "sTitle": "User"},
			{ "sTitle": "Total Sales"},
	     ], 
		"sScrollY": $("#main-content").height() - 275, 
		"aaSorting": [[1, 'asc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) { 
		}
	});	
	 
	oTable = $('#SalesReportDT').dataTable();  
	 
	 setTimeout(function () {
		getTotalSales();
     }, 1000);
}

function getTotalSales(){
	totalSales = 0;  
	$('#SalesReportDT tbody tr').each(function(){
		 aData =oTable.fnGetData( this );
		 if(null!=aData){
			 totalSales = Number(totalSales) + Number(aData[6]); 
		 }  
	});
	$('#totalSales').text(totalSales.toFixed(2));
}
$(function(){  
	
	$('#SalesReportDT_first, #SalesReportDT_previous, .fg-button, #SalesReportDT_next, #SalesReportDT_last').live('click', function () {  
		getTotalSales();
	});
	
	$('#SalesReportDT_length').live('change', function () {  
		getTotalSales();
	});
	
	$('#SalesReportDT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).removeClass('row_selected');
		      aData =oTable.fnGetData( this );
		      selectedRowId=aData[0]; 
		  }
		  else {
		      oTable.$('tr.row_selected').removeClass('row_selected');
		      $(this).addClass('row_selected');
		      aData =oTable.fnGetData( this ); 
		      selectedRowId=aData[0];  
		  }
		});
	$('#startPicker,#endPicker').datepick({
		onSelect : customRange,
		showTrigger : '#calImg'
	});
	
	$('#list_grid').click(function(){
		totalSales = Number(0); 
		populateDatatable();
	});
						
		$(".xls-download-call").click(function(){
				window.open("<%=request.getContextPath()%>/get_POS_sales_report_XLS.action?pointOfSaleIds="+selectedRowIds+"&userTillId="+selectedUserTillId
						+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&reportType=pos",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			return false; 
		});
	 	
	 	$(".print-call").click(function(){
	 			window.open("<%=request.getContextPath()%>/get_pos_sales_report_printout.action?pointOfSaleId="+selectedRowId+"&userTillId="+selectedUserTillId
	 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=HTML&reportType=pos",
					'_blank','width=850,height=700,scrollbars=yes,left=100px,top=2px');
			return false;
			
		});
	 	
	 	$(".pdf-download-call").click(function(){ 
	 			window.open("<%=request.getContextPath()%>/get_POS_sales_report_pdf.action?pointOfSaleIds="+selectedRowIds+"&userTillId="+selectedUserTillId
	 					+"&storeId="+selectedStoreId+"&personId="+selectedPersonId+"&fromDate="+fromDate+"&toDate="+toDate+"&format=PDF&reportType=pos",
					'_blank','width=800,height=700,scrollbars=yes,left=100px,top=2px');
			return false;
			
		});
	 	
	 	$(".delete-call").click(function(){
	 		if(selectedRowId!=null && selectedRowId!=0)	{
		 		$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/delete_point_of_sale.action", 
			     	async: false, 
			     	data:{pointOfSaleId: selectedRowId},
					dataType: "json",
					cache: false,
					success: function(response){ 
						if(response.returnMessage=="SUCCESS")
							populateDatatable();
						else{
							alert("Sorry can not delete");
						}
								
					} 		
				}); 
	 		}else{
	 			alert("Please select the sale to delete");
	 		}
			return false;
			
		});

		$('.fromDate').change(function() {

			fromDate = $('.fromDate').val(); 

		});

		$('.toDate').change(function() {

			toDate = $('.toDate').val(); 

		});
		
		$('#userTills').combobox({
			selected : function(event, ui) {
				selectedUserTillId = $('#userTills :selected').val(); 
			}
		});

		$('#stores').combobox({
			selected : function(event, ui) {
				selectedStoreId = $('#stores :selected').val(); 
			}
		});


		$('#users').combobox({
			selected : function(event, ui) {
				selectedPersonId = $('#users :selected').val(); 
			}
		});
});


var selectedType = null;



function customRange(dates) {
	if (this.id == 'startPicker') {
		$('.fromDate').trigger('change');
		$('#endPicker').datepick('option', 'minDate', dates[0] || null);
	} else {
		$('.toDate').trigger('change');
		$('#startPicker').datepick('option', 'maxDate', dates[0] || null);
	}
}


	
</script>
<div id="main-content">


	<input type="hidden" id="assetUsageId" />
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Point of Sales Reports
		</div>
		<div class="portlet-content">
			<table width="100%">
				<tr style="display: none;">
					<td>Till :</td>
					<td>
						<select id="userTills">
							<option value="">Select</option>
							<c:forEach var="pos" items="${TILL}">
								<option value="${pos.posUserTillId}">${pos.tillNumber}</option>						
							</c:forEach>
						</select>
					</td> 
				</tr>
				<tr>
					<td>Store :</td>
					<td>
						<select id="stores">
							<option value="">Select</option>
							<c:forEach var="st" items="${STORE_LIST}">
								<option value="${st.storeId}">${st.storeName}</option>						
							</c:forEach>
						</select>
					</td>
					<td width="10%" align="center">
						From :
					</td>
					<td width="35%">
						<input type="text"
						name="fromDate" class="width65 fromDate" id="startPicker"
						readonly="readonly">
					</td>
				</tr>
				<tr>
					<td>User :</td>
					<td>
						<select id="users">
							<option value="">Select</option>
							<c:forEach var="pr" items="${PERSON_LIST}">
								<option value="${pr.personId}">${pr.firstName} ${pr.lastName}</option>						
							</c:forEach>
						</select>
					</td>
					<td width="10%" align="center">
						To :
					</td>
					<td width="35%">
						<input type="text"
						name="toDate" class="width65 toDate" id="endPicker"
						readonly="readonly">
					</td> 
				</tr> 
			</table>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px; background: none repeat scroll 0 0 #d14836;">
				<div class="portlet-header ui-widget-header float-right"
					id="list_grid" style="cursor: pointer; color: #fff;">
					list gird
				</div> 
			</div>
			<div class="tempresult" style="display: none;width:100%"></div>

			<div id="rightclickarea">
				<div id="SalesReport">
					<table class="display" id="SalesReportDT">

					</table>
				</div>
			</div>
			
			<div class="float-right" style="width: 20%;">
				<span style="font-weight: bold;">Total Sales :</span>
				<span id="totalSales" style="font-weight: bold;"></span>
			</div>
		</div>
	</div>

</div>

<div class="process_buttons">
	<div id="ac" class="width100 float-right ">
		<div class="width5 float-right height30" title="Download as PDF" style="display: none;">
			<img width="30" height="30" src="images/pdf_icon.png"
				class="pdf-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Download as XLS" style="display: none;">
			<img width="30" height="30" src="images/xls_icon.png"
				class="xls-download-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Print">
			<img width="30" height="30" src="images/print_icon.png"
				class="print-call" style="cursor: pointer;" />
		</div>
		<div class="width5 float-right height30" title="Delete">
			<img width="30" height="30" src="images/cancel.png"
				class="delete-call" style="cursor: pointer;" />
		</div>
	</div>
</div> 