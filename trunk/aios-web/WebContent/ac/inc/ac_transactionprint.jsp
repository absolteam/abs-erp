<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.aiotech.aios.common.util.DateFormat"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Journal Voucher</title>
	<link href="${pageContext.request.contextPath}/css/print.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">  
th {
	text-align: center; 
	border-top:1px solid #000; 
	border-right:1px solid #000; 
	border-left:1px solid #000; 
	border-bottom:3px double #000;  
	font-size:13px;
}
table {
	font-size:14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse; 
}
td {
	border: 1px solid #000; 
} 
.bottomTD{
	border-bottom:3px double #000;  
}
.heading {
	padding:10px;
	font-size:20px;
	font-family: "CenturyGothicRegular", "Century Gothic", Arial, Helvetica, sans-serif;
    text-align: center;
    color: #000;
    font-weight:bold;
    width:100%;
    float:right;
}
</style>
	<body onload="window.print();" style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100" >
		<span class="title-heading"><span>${THIS.companyName}</span></span> 
	</div>
	
	<div style="clear: both;"></div>
	<div class="width98" ><span class="heading"><span>JOURNAL VOUCHER</span></span></div>
	<div style="clear: both;"></div>
	<div style="height: 100%; border-style: solid; border-width: medium; -moz-border-radius: 6px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<div class="width40 float_right">
			 <div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">JV.NO.<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"><span style="color: #CC2B2B;">${TRANSACTION_PRINT.journalNumber}</span></span>  
			 </div> 
			<div class="width98 div_text_info">
				<span class="float_left left_align width28 text-bold">DATE<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="date" value="${TRANSACTION_PRINT.transactionTime}"/>  
					<%=DateFormat.convertDateToString(pageContext.getAttribute("date").toString())%> 
				</span>  		
			</div> 
		</div>  
		<div class="width50 float_left">
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">PERIOD<span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;"> 
					 ${TRANSACTION_PRINT.period.name}
				</span> 
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">CATEGORY <span class="float_right">:</span></span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">  
					${TRANSACTION_PRINT.category.name}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">CURRENCY<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					 ${TRANSACTION_PRINT.currency.currencyPool.code}
				</span>
			</div>
			<div class="width100 div_text_info">
				<span class="float_left left_align width28 text-bold">AMOUNT<span class="float_right">:</span> </span>
				<span class="span_border left_align width65 text_info" style="display: -moz-inline-stack;">
					<c:set var="transactionAmount" value="${requestScope.transactionAmount}"/>
					<%=AIOSCommons.formatAmount(pageContext.getAttribute("transactionAmount")) %> 
				</span>
			</div> 
		</div> 
	</div>
	<div class="clear-fix"></div>
	<div style="position:relative; top: 10px; height:100%; border-style: solid; border-width: 2px; -moz-border-radius: 3px; 
		 -webkit-border-radius:6px; border-radius: 6px;" class="width98 float_left">
		<table class="width100">
			<tr>  
				<th>COMBINATION</th>
				<th>DEBIT</th>
				<th>CREDIT</th> 
				<th>DESCRIPTION</th>
			</tr>
			<tbody>  
				 <tr style="height: 25px;">
					<c:forEach begin="0" end="3" step="1">
						<td/>
					</c:forEach>
				</tr>
				<c:forEach items="${TRANSACTION_DETAIL_PRINT}" var="TRANSACTION_DETAIL" varStatus="status"> 
					<tr> 
						<td style="text-align: left;">
							<c:choose>
								<c:when test="${TRANSACTION_DETAIL.combination.accountByAnalysisAccountId ne null &&
										 TRANSACTION_DETAIL.combination.accountByAnalysisAccountId ne ''}">
									${TRANSACTION_DETAIL.combination.accountByCostcenterAccountId.account}.${TRANSACTION_DETAIL.combination.accountByNaturalAccountId.account}.${TRANSACTION_DETAIL.combination.accountByAnalysisAccountId.account}
								</c:when>
								<c:otherwise>
									${TRANSACTION_DETAIL.combination.accountByCostcenterAccountId.account}.${TRANSACTION_DETAIL.combination.accountByNaturalAccountId.account}
								</c:otherwise>
							</c:choose> 
						</td>
						<c:set var="linesamount" value="${TRANSACTION_DETAIL.amount}"/>  
						<c:choose>
							<c:when test="${TRANSACTION_DETAIL.isDebit eq true &&  TRANSACTION_DETAIL.isDebit ne false}">  
								<td class="right_align">
									<%=AIOSCommons.formatAmount(pageContext.getAttribute("linesamount"))%> 
								</td><td/>
							</c:when>
							<c:otherwise>
								<td/>
								<td class="right_align"><%=AIOSCommons.formatAmount(pageContext.getAttribute("linesamount"))%></td> 
							</c:otherwise>
						</c:choose> 
						<td style="text-align: left;">${TRANSACTION_DETAIL.description}</td> 
					</tr>
				</c:forEach> 
				<c:forEach begin="0" end="1" step="1">
					<tr style="height: 25px;">
						<c:forEach begin="0" end="3" step="1">
							<td/>
						</c:forEach>
					</tr>  
				</c:forEach>
			</tbody>
		</table>
	</div>  
	<div class="clear-fix"></div>
	<div class="width100  div_text_info float_left" style="position:relative; top: 15px;">
		<span class="float_left left_align width20 text-bold">JV.DESCRIPTION<span class="float_right">:</span> </span>
		<span class="span_border left_align text_info" style="width:77%;display: -moz-inline-stack;">
			<c:choose>
				<c:when test="${TRANSACTION_PRINT.description ne null && TRANSACTION_PRINT.description ne ''}">
					${TRANSACTION_PRINT.description}
				</c:when>
				<c:otherwise>-N/A-</c:otherwise>
			</c:choose>  
		</span>
	</div> 
	</body>
</html>