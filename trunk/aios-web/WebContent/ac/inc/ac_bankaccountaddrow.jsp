<tr id="fieldrow_${requestScope.id}" class="rowid">
	<td class="width10" id="accountNumber_${requestScope.id}"></td>
	<td class="width10" id="accountHolder_${requestScope.id}"></td>
	<td class="width10" id="accountType_${requestScope.id}"></td>  
	<td class="width10" id="rountingNumber_${requestScope.id}"></td>
	<td class="width10" id="iban_${requestScope.id}"></td> 
	<td class="width10" id="currency_${requestScope.id}"></td>
	<td class="width10" id="mode_${requestScope.id}"></td> 
	<td class="width10" id="accountPurpose_${requestScope.id}"></td>
	<td class="width10" id="branchName_${requestScope.id}"></td> 
	<td style="display: none;" class="width10" id="creditLimit_${requestScope.id}"></td>
	<td class="width10" id="option_${requestScope.id}">
	<input type="hidden" id="bankAccountId_${requestScope.id}" />
	<input type="hidden" id="lineId_${requestScope.id}" value="${requestScope.id}" /> 
	<input type="hidden" id="swift_${requestScope.id}"/> 
	<input type="hidden" id="holderidval_${requestScope.id}"/>
	<input type="hidden" id="currencyidval_${requestScope.id}"/>
	<input type="hidden" id="typeidval_${requestScope.id}"/>
	<input type="hidden" id="modeidval_${requestScope.id}"/>
	<input type="hidden" id="sign1_${requestScope.id}"/> 
	<input type="hidden" id="sign2_${requestScope.id}"/>
	<input type="hidden" id="address_${requestScope.id}"/>
	<input type="hidden" id="branchContact_${requestScope.id}"/>
	<input type="hidden" id="contactPerson_${requestScope.id}"/>
     <a
		style="cursor: pointer;"
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankaddData"
		id="AddImage_${requestScope.id}" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankeditData"
		id="EditImage_${requestScope.id}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a style="cursor: pointer; display: none;"
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip bankdelrow"
		id="DeleteImage_${requestScope.id}" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a href="#"
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.id}" style="display: none;" title="Working">
			<span class="processing"></span> </a>
	</td>

</tr>
