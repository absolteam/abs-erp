<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<style>
.ui-autocomplete-input {
	width: 48% !important;
} 
label {
	margin-top: 5px;
}
</style>
<script type="text/javascript">
var accessCode=null;
var specialProducts = new Object();
$(function(){
	$('.definition-autocomplete').combobox({
		selected : function(event, ui) { 
			if($(this).attr('id') == "specialProduct")
				treeProductPreview(Number($(this).val()));
		}
	}); 

	$jquery("#productdefinition_validation").validationEngine('attach');
	
	$('.product_definition_save').click(function(){   
		 if($jquery("#productdefinition_validation").validationEngine('validate')){ 
			var productId = Number($('#specialProduct').val());
			var definitionLabel = $('#definitionLabel').val();  
			var productDefinitionParentId = Number(0);   
			if(specialProducts.record.length > 0){
				$.ajax({
					type: "POST", 
					url: "<%=request.getContextPath()%>/save_product_definition.action", 
			     	async: false,
			     	data:{
			     			productId : productId, definitionLabel: definitionLabel, productDefinitionParentId: productDefinitionParentId,
			     			aaData: JSON.stringify(specialProducts)
			     		 },
					dataType: "json",
					cache: false,
					success: function(response){  
						if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){ 
								callScreenDiscard("Record created.");
						}else{
							$('#error_message').hide().html(response.returnMessage).slideDown(
									1000);
							$('#error_message').delay(2000).slideUp();
						}
					}
				});
			} else{
				$('#error_message').hide().html("Please select products.").slideDown(
						1000);
				$('#error_message').delay(2000).slideUp();
			} 					
		}else
			return false;
		 return false;
	});

	$('.product_definition_discard').click(function(){  
		 callScreenDiscard("");
	 });
	
	$('#product-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   });
	
	 $('#definition_specialprd_popup').dialog({
	 		autoOpen: false,
	 		width: 800,
	 		height: 400,
	 		bgiframe: true,
	 		modal: true,
	 		buttons: {
	 			"OK": function(){  
	 				$(this).dialog("close"); 
	 			} 
	 		}
	 	});
	
	$('#view_option').click(function(){   
		 $('.ui-dialog-titlebar').remove();
			var htmlString = "";
						htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>Special Products</div>";
								var count = 0;
						
			$(specialProducts.record)
								.each(
										function(index) {
											count += 1;
											htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
													+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
													+ count
													+ ".</span>"
													+ "<span class='width20 float-left'>"
													+ specialProducts.record[index].recordName
													+ "</span><span class='deleteoption' id='deleteoption_"+count+"' style='cursor: pointer;'><img src='./images/cancel.png' width=10 height=10></img></span></div>";
										});

						$('#definition_specialprd_popup').dialog('open');

						$('#definition_specialprd_popup').html(htmlString);
					});
	
			$('.deleteoption')
			.live('click',
					function() {
				var rowId = getRowId($(this).attr('id')); 
					var count_index =  $('#countindex_'+rowId).attr('id'); 
				$('#div_'+rowId).remove(); 
				specialProducts.record.splice(count_index.split('.')[0], 1); 
				var i = 0;
				$('.count_index')
				.each(
						function() {
							i = i +1;
					$(this).text(i+".");		
				});
				if(specialProducts.record.length == 0){
					$('#view_option').hide();
		 			$('#definition_specialprd_popup').dialog("close");
				} 
				return false;
		 }); 
			
			$('.definition_product_delete')
			.live('click',
					function() {	
				var productDefinitionId = getRowId($(this).attr('id')); 
				var productId = Number($('#specialProduct').val());
				$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/delete_specialproduct_definition.action",
						async : false,
						data:{productDefinitionId: productDefinitionId},
						dataType : "json", 
						cache : false,
						success : function(response) { 
							if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){ 
								viewProductDefinitionTree(productId);
							}else{
								$('#error_message').hide().html(response.returnMessage).slideDown(
										1000);
								$('#error_message').delay(2000).slideUp();
							} 
							return false;
						}
					}); 
				 return false;
			});
			
			$('.definition_product_edit')
			.live('click',
					function() {	
				var defaultPricing = $(this).attr('checked');
				var productDefinitionId = getRowId($(this).attr('id'));  
				var productId = Number($('#specialProduct').val());
				
				 $.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/update_product_combopricedetail.action",
						async : false,
						data:{productDefinitionId: productDefinitionId, defaultPricing: defaultPricing},
						dataType : "json", 
						cache : false,
						success : function(response) { 
							if(response.returnMessage!=null && response.returnMessage=="SUCCESS"){ 
								viewProductDefinitionTree(productId);
							}else{
								$('#error_message').hide().html(response.returnMessage).slideDown(
										1000);
								$('#error_message').delay(2000).slideUp();
							} 
							return false;
						}
					}); 
				 return false;
			});		
	
	$('.special-product-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_specialproduct_multiple_popup.action",
				async : false,
				dataType : "html",
				data : {rowId: Number(0), itemType: 'I'},
				cache : false,
				success : function(result) { 
					$(".common-result").html(result);
					$('#common-popup').dialog('open');
					$($($('#common-popup').parent()).get(0)).css('top',0);
				}
			}); 
			return false;
		});
	 
	
	$('.definitionselect').live('click',function(){ 
		$('#definitionLabel').val($.trim($(this).text())); 
		$('#definition_option_popup').dialog('close');
	});

	$('.definition-lookup').click(function(){
        $('.ui-dialog-titlebar').remove(); 
        var productId = Number($('#specialProduct').val());
        $.ajax({
              type:"POST",
              url:"<%=request.getContextPath()%>/show_allproduct_definitionlabel.action",
              data:{productId: productId},
              async: false,
              dataType: "json",
              cache: false,
              success:function(response){
            	  var htmlString = "";
					htmlString += "<div class='heading' style='padding: 5px; font-weight: bold;'>Level Labels</div>";
							var count = 0; 
						$(response.aaData)
							.each(
									function(index) {
										count += 1;
										htmlString += "<div id=div_"+count+" class='width100 float-left' style='padding: 3px;' >"
												+ "<span id='countindex_"+count+"' class='float-left count_index' style='margin: 0 5px;'>"
												+ count
												+ ".</span>"
												+ "<span id='definitionselect_"+count+"' class='width20 definitionselect float-left' style='cursor: pointer;'>"
												+ response.aaData[index]
												+ "</span></div>";
									});

					$('#definition_option_popup').dialog('open');

					$('#definition_option_popup').html(htmlString);
                   return false;
              }
          });
           return false;
  	}); 
	
	$('#definition_option_popup').dialog({
 		autoOpen: false,
 		width: 400,
 		height: 300,
 		bgiframe: true,
 		modal: true,
 		buttons: {
 			"OK": function(){  
 				$(this).dialog("close"); 
 			} 
 		}
 	});	
	
	$('#common-popup').dialog({
		 autoOpen: false,
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
});
function treeProductPreview(productId){
	if(productId > 0){
		$.ajax({
			type: "POST", 
			url: "<%=request.getContextPath()%>/show_product_definition_subcategory.action",
				async : false,
				data:{productId: productId},
				dataType : "json",
				cache : false,
				success : function(response) {
					$("#parentDefinition").html(''); 
					$('#parentDefinition')
					.append(
							"<option value=''></option>");
					$(response.aaData)
					.each(
							function(index) {
								$('#parentDefinition')
								.append(
										'<option value='
		+ response.aaData[index].productDefinitionId
		+ '>'
												+ response.aaData[index].definitionLabel
												+ '</option>');
							});
					viewProductDefinitionTree(productId);
				}
			}); 
	}return false;
}

function viewProductDefinitionTree(productId){
	var productName = $('#specialProduct option:selected').text();
 	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/show_product_definition_simpletree.action",
			async : false,
			data:{productId: productId, productName: productName},
			dataType : "html",
			cache : false,
			success : function(result) {
				$("#sidetree").html(result); 
			}
		});
	return false;
}
function callScreenDiscard(statusMessage){
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/show_product_definition.action",
			async : false,
			dataType : "html",
			cache : false,
			success : function(result) {
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();   
				$('#definition_option_popup').dialog('destroy');		
				$('#definition_option_popup').remove(); 
				$("#main-wrapper").html(result);
				if (statusMessage != null && statusMessage != '') {
					$('#success_message').hide().html(statusMessage).slideDown(
							1000);
					$('#success_message').delay(2000).slideUp();
				}
			}
		});
	}
function setJsonDiscountData(jsonData) {
	specialProducts = jsonData;
	if (specialProducts.record.length > 0)
		$('#view_option').show();
}
function getRowId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>pos product
			detail level
		</div> 
		<form id="productdefinition_validation" style="position: relative;">
			<div class="width100" id="hrm">
				<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
				<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
				<div class="width50 float-right">
					<div id="main">
						<div id="sidetree"> 
						</div>
					</div>
				</div>
				<div class="width40 float-left" style="padding: 3px;">
					<div>
						<label>Product Name<span style="color: red;">*</span> </label> <select
							name="specialProduct" id="specialProduct"
							class="definition-autocomplete">
							<option value=""></option>
							<c:forEach var="specialProduct" items="${SPECIAL_PRODUCTS}">
								<option value="${specialProduct.productId}">${specialProduct.productName}</option>
							</c:forEach>
						</select>
					</div>
					<div>
						<label>Definition<span style="color: red;">*</span> </label> <input
							type="text" name="definitionLabel" id="definitionLabel" class="width48 validate[required]">
						 <span class="button"> <a
							style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all definition-lookup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
					</div>
					<div>
						<label>Product<span style="color: red;">*</span></label> 
						<span class="button" style="position: relative; top: 10px;"> <a
							style="cursor: pointer;"
							class="btn ui-state-default ui-corner-all special-product-popup">
								<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
						<span style="cursor: pointer; position: relative; top: 8px; display: none; float: left;
							width:17px; height:17px; background-image: url('./images/icons/view.ico');background-repeat: no-repeat;" id="view_option"></span>
					</div>
					<div style="display: none;">
						<label>Parent Definition</label> <select name="parentDefinition"
							id="parentDefinition" class="definition-autocomplete">
							<option value=""></option>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				<div
					class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
					<div
						class="portlet-header ui-widget-header float-right product_definition_discard">
						<fmt:message key="accounts.common.button.cancel" />
					</div>
					<div
						class="portlet-header ui-widget-header float-right product_definition_save">
						<fmt:message key="accounts.common.button.save" />
					</div>
				</div>
			</div>

			<div style="display: none;" id="definition_option_popup"></div>
			<div style="display: none;" id="definition_specialprd_popup"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>