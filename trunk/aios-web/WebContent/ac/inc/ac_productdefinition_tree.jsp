<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.apache.struts2.ServletActionContext"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page
	import="com.aiotech.aios.accounts.domain.entity.ProductDefinition"%>
<%@page
	import="com.aiotech.aios.accounts.domain.entity.vo.ProductDefinitionVO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Collections"%>
<script type="text/javascript">
$(function(){
	$("#product-definition-view").treeview({
		collapsed: true,
		animated: "medium", 
		control:"#sidetreecontrol",
		persist: "location"
	}); 
});
</script>
<div id="sidetreecontrol"></div>
<ul id="product-definition-view">
	<li>${requestScope.productName} <c:if
			test="${PRODUCT_DEFINITION_LIST ne null && PRODUCT_DEFINITION_LIST ne ''}">
			<ul>
				<%!int count = 0;
	String className = "";

	void renderGuiMenu(JspWriter out) throws Exception {

		List<ProductDefinitionVO> definitionItems = (List<ProductDefinitionVO>) ServletActionContext
				.getRequest().getAttribute("PRODUCT_DEFINITION_LIST");
	
 		for (ProductDefinitionVO definitionItem : definitionItems) {
			boolean flag = false;

			if (definitionItem.getProductDefinitionVOs() != null
					&& definitionItem.getProductDefinitionVOs().size() > 0) {
				out.println("<li>"
						+ definitionItem.getDefinitionLabel());
				 
				flag = true;

			} else {
				out.println("<li>"
						+ definitionItem.getDefinitionLabel());
				out.println("<ul");
				out.println("<li>"
						+ definitionItem.getSpecialProductName());
				out.println("</li>");
				out.println("</ul>");
			}

			if (flag) {

				out.println("<ul>");
				recursivelyPopulateMenu(definitionItem, out);

				out.println("</ul>");
			}
			out.println("</li>");

		}

	}

	void recursivelyPopulateMenu(ProductDefinitionVO definition, JspWriter out)
			throws Exception {

		List<ProductDefinitionVO> subDefinition = definition
				.getProductDefinitionVOs();

		for (ProductDefinitionVO subItem : subDefinition) {

			if (subItem.getProductDefinitionVOs() == null
					|| subItem.getProductDefinitionVOs().size() == 0) {

				className = "";
				out.println("<li>"
						+ subItem.getDefinitionLabel());
				out.println("<ul>");
				out.println("<li>"
						+ subItem.getSpecialProductName());
				out.println("</li>");
				out.println("</ul>");
				out.println("</li>");
				continue;
			} else {
				className = "parent";
				out.println("<li>"
						+ subItem.getDefinitionLabel()); 
				out.println("<ul>");
				recursivelyPopulateMenu(subItem, out);
				out.println("</ul>");
				out.println("</li>");
			}

		}

	}%>

				<%
											renderGuiMenu(out);
										%>



			</ul>

		</c:if>
	</li>
</ul>