<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td id="lineId_${requestScope.rowId}" style="display: none;">${requestScope.rowId}</td>
	<td><input type="text"
		class="width96 flatcommission validate[optional,custom[number]]"
		id="flatcommission_${requestScope.rowId}"  /></td>
	<td><input type="text"
		class="width96 targetsale validate[optional,custom[number]]"
		id="targetsale_${requestScope.rowId}"
		value="${commissionMethod.targetSale}"
		style=" text-align: right;" /></td>
	<td><select name="isPercentage"
		id="isPercentage_${requestScope.rowId}" class="width96">
			<option value="">Select</option>
			<option value="1">Percentage</option>
			<option value="0">Amount</option>
	</select></td>
	<td><input type="text"
		class="width96 commissionvalue validate[optional,custom[number]]"
		id="commissionvalue_${requestScope.rowId}"  /></td>
	<td><input type="text" name="linedescription"
		id="linedescription_${requestScope.rowId}" class="width96" /></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}"
		style="display: none; cursor: pointer;" title="Add Record"> <span
			class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}"
		style="display: none; cursor: pointer;" title="Edit Record"> <span
			class="ui-icon ui-icon-wrench"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}"
		style="cursor: pointer; display: none;" title="Delete Record"> <span
			class="ui-icon ui-icon-circle-close"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;"
		title="Working"> <span class="processing"></span> </a> <input
		type="hidden" id="commissionMethodId_${requestScope.rowId}" /></td>
</tr>
<script type="text/javascript">
	$(function() {
		$jquery("#commissionrule_entry_details").validationEngine('attach'); 
	});
</script>