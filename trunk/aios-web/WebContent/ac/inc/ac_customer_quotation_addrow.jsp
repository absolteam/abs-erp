<tr class="rowid" id="fieldrow_${ROW_ID}">
	<td style="display: none;" id="lineId_${ROW_ID}">${ROW_ID}</td>
	<td><input type="hidden" name="productId" id="productid_${ROW_ID}" />
		<span id="product_${ROW_ID}" class="width60 float-left"></span> 
		<span class="width10 float-right" id="unitCode_${ROW_ID}"
			style="position: relative;"></span>
		<span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${ROW_ID}"
			class="btn ui-state-default ui-corner-all show_product_list_cstquotation width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
	</td>
	<td style="display: none;"><input type="hidden" name="storeId" id="storeid_${ROW_ID}" />
		<span id="store_${ROW_ID}"></span> <span class="button float-right">
			<a style="cursor: pointer;" id="storeID_${ROW_ID}"
			class="btn ui-state-default ui-corner-all show_store_list width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span>
	</td>
	<td>
		<select name="packageType" id="packageType_${ROW_ID}" class="packageType">
			<option value="">Select</option>
		</select> 
	</td> 
	<td><input type="text"
		class="width96 packageUnit validate[optional,custom[number]]"
		id="packageUnit_${ROW_ID}"  />
	</td>
	<td><input type="hidden" name="productQty" id="productQty_${ROW_ID}"
		class="productQty validate[optional,custom[number]] width80"> 
		 <span class="button float-right">
			<a
			style="cursor: pointer; position: relative; top: 7px; display: none;"
			id="showBasePrice_${ROW_ID}"
			class="btn ui-state-default ui-corner-all baseprice-popup width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span>
		 <span id="baseUnitConversion_${ROW_ID}" class="width10" style="display: none;"></span>
		 <span id="baseDisplayQty_${ROW_ID}"></span>
	</td>
	<td><input type="text" name="amount" id="amount_${ROW_ID}" style="text-align: right;"
		class="amount validate[optional,custom[number]] width60 right-align">
		<span class="button float-right"> <a
			style="cursor: pointer; position: relative; top: 7px;"
			id="pricingDetail_${ROW_ID}"
			class="btn ui-state-default ui-corner-all show_productpricing_list width100">
				<span class="ui-icon ui-icon-newwin"></span> </a> </span>
		<input type="hidden" id="baseSellingPrice_${ROW_ID}"/>
	</td>
	<td> 
		<div class="float-left">
		<input type="radio" name="chargesmodeDetail_${ROW_ID}" class="chargesmodeDetail float-left" style="width:5px;"
			 id="pecentagemode_${ROW_ID}" value="true"/>
		<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
		<div class="float-left">
		<input type="radio" name="chargesmodeDetail_${ROW_ID}" class="chargesmodeDetail float-left" 
			id="amountmode_${ROW_ID}" value="false" style="width:5px;"/>
		<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
	</td>
	<td><input type="text" name="discount" id="discount_${ROW_ID}"
		style="text-align: right;" class="discount validate[optional,custom[number]] width90 right-align">
	</td>
	<td>
		<span id="totalAmount_${ROW_ID}" style="float: right;"></span>
		<input type="text" style="display: none" id="totalAmountH_${ROW_ID}" class="totalAmountH"/>
	</td>
	<td style="width: 1%;" class="opn_td" id="option_${ROW_ID}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${ROW_ID}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${ROW_ID}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${ROW_ID}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden"
		name="customerQuotationDetailId"
		id="customerQuotationDetailId_${ROW_ID}" />
	</td>
</tr>
<script type="text/javascript">
$(function(){
	$jquery("#amount_${ROW_ID},#totalAmountH_${ROW_ID}").number(true, 2);
}); 
</script>