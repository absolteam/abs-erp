<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
#DOMWindow {
	width: 95% !important;
	height: 88% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
}

.returnerr {
	font-size: 9px !important;
	color: #ca1e1e;
}
.ui-widget-overlay,.ui-dialog {z-index: 100000!important;}
</style>
<div id="hrm" class="portlet-content width100 material-transferdetail-info"
	style="height: 90%;">
	<div class="portlet-content">
		<div id="page-error-popup"
			class="response-msg error ui-corner-all width90"
			style="display: none;"></div>
		<div id="warning_message-popup"
			class="response-msg notice ui-corner-all width90"
			style="display: none;"></div>
		<div id="hrm" class="hastable width100">
			<table id="hastab-1" class="width100">
				<thead>
					<tr> 
						<th style="width:5%;">Product</th>
						<th style="width:2%;">Transfered Qty</th>
						<th style="width:2%;">Returned Qty</th> 
						<th style="width:2%;">Return Qty</th>
						<th style="width:3%;">Condition</th>
						<th style="width:5%;">Description</th>
						<th><fmt:message key="accounts.common.label.options" />
						</th>
					</tr>
				</thead>
				<tbody class="tab-1">
					<c:forEach var="bean" varStatus="status"
						items="${MATERIAL_TRANSFER_DETAIL}">
						<tr class="rowidrt" id="fieldrowpo_${status.index+1}">
							<td style="display: none;" id="lineIdrt_${status.index+1}">${status.index+1}</td> 
							<td id="productname_${status.index+1}">
								${bean.product.code} -- ${bean.product.productName}</td>
							<td id="transferedqty_${status.index+1}">${bean.quantity}</td>
							<td id="returnedqty_${status.index+1}">${bean.returnedQty}</td> 
							<td><input type="text" name="returnQty"
								id="returnQty_${status.index+1}" class="returnQty"
								value="${bean.returnQty}" /></td>
							<td><select name="condition"
								id="condition_${status.index+1}" class="width60">
									<option value="">Select</option>
									<c:forEach var="CONDITION" items="${MATERIAL_CONDITION}">
										<option value="${CONDITION.lookupDetailId}">${CONDITION.displayName}</option>
									</c:forEach>
								</select> <span class="button" style="position: relative;"> <a
									style="cursor: pointer;" id="MATERIAL_CONDITION_${status.index+1}"
									class="btn ui-state-default ui-corner-all material-condition-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									<input type="hidden" id="materialContition_${status.index+1}" value="${bean.materialCondition}"/>
							</td>
							<td><input type="text" name="linedescription"
								id="linedescription_${status.index+1}"
								value="${bean.description}" /></td>
							<td style="width: 0.01%;" class="opn_td"
								id="optionrtntrns_${status.index+1}"><a
								class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_rtntrns"
								id="DeleteCImage_${status.index+1}" style="cursor: pointer;"
								title="Delete Record"> <span
									class="ui-icon ui-icon-circle-close"></span> </a> <input
								type="hidden" name="returnlineid"
								id="returnlineid_${status.index+1}" /> <input type="hidden"
								name="materialTransferID"
								id="materialTransferID_${status.index+1}"
								value="${requestScope.materialTransferId}" /> <input
								type="hidden" name="productid" id="productid_${status.index+1}"
								value="${bean.product.productId}" /> <input type="hidden"
								name="materialTransferDetailId"
								id="materialTransferDetailId_${status.index+1}"
								value="${bean.materialTransferDetailId}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
		<div class="portlet-header ui-widget-header float-left sessionTransferReturnSave"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.ok" />
		</div>
		<div class="portlet-header ui-widget-header float-left returnTransferCloseDom"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
	</div>
</div> 
<script type="text/javascript">
$(function(){
	$('.rowidrt').each(function(){
		var rowId = getRowId($(this).attr('id'));
		$('#condition_'+rowId).val($('#materialContition_'+rowId).val());
	});
});
</script>