 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@page import="com.aiotech.aios.common.util.DateFormat"%>   
<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			quotation details
</div>
 <div id="hrm">  
 	<div class="float-right width50" style="position: relative; top: -29px;">
 		<select name="banklist" class="autoComplete width50">
	     	<option value=""></option>
	     	<c:forEach items="${QUOTATION_DETAIL}" var="bean" varStatus="status"> 
	     		<c:set var="date" value="${bean.quotationDate}"/> 
	     		<% 
	     			String date = DateFormat.convertDateToString(pageContext.getAttribute("date").toString());
					pageContext.setAttribute("date", date);
	     		%>
	     		<option value="${bean.quotationId}|${bean.currency.currencyPool.code}|${bean.currency.currencyId}|${bean.supplier.supplierId}|${bean.supplier.supplierNumber}|${bean.supplier.person.firstName} ${bean.supplier.person.lastName}${bean.supplier.cmpDeptLocation.company.companyName}">${bean.quotationNumber}</option>
	     	</c:forEach>
     	</select>  
     	<span style="margin-top:2px; cursor: pointer; position: absolute;" class="float-right"><img width="24" height="24" src="images/icons/Glass.png"></span>
 	</div> 
 	<div class="float-left width100 quotation_list" style="padding:2px;">  
	     	<ul> 
   				<li class="width20 float-left"><span>Quotation.No</span></li>
   				<li class="width20 float-left"><span>Currency</span></li> 
   				<li class="width25 float-left"><span>Supplier No</span></li> 
   				<li class="width25 float-left"><span>Supplier</span></li>  
   				<li style="border-bottom: 1px solid #FFE5B1; margin-left:-4px; padding-right:5px; margin-bottom: 5px;" class="width100 float-left"></li>
	     		<c:forEach items="${QUOTATION_DETAIL}" var="bean" varStatus="status">
		     		<li id="bank_${status.index+1}" class="quotation_list_li width100 float-left" style="height: 20px;">
		     			<input type="hidden" value="${bean.quotationId}" id="quotationbyid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.quotationNumber}" id="quotationbynumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.currency.currencyId}" id="quotationbycurrencyid_${status.index+1}">
		     			<input type="hidden" value=" ${bean.supplier.supplierNumber}" id="quotationbysuppliernumberid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.supplier.supplierId}" id="quotationbysupplierid_${status.index+1}"> 
		     			<input type="hidden" value="${bean.supplier.person.firstName}  ${bean.supplier.cmpDeptLocation.company.companyName}" id="quotationbysuppliernameid_${status.index+1}">
		     			<span id="quotationbynumber_${status.index+1}" style="cursor: pointer;" class="width20 float-left">${bean.quotationNumber}</span>
		     			<span id="quotationbycurrency_${status.index+1}" style="cursor: pointer;" class="float-left width20">${bean.currency.currencyPool.code}</span>
		     			<span id="quotationbysupplierno_${status.index+1}" style="cursor: pointer;" class="float-left width25">${bean.supplier.supplierNumber}</span>
		     			<span id="quotationbysupplier_${status.index+1}" style="cursor: pointer; width:22%;" class="float-left">${bean.supplier.person.firstName}  ${bean.supplier.cmpDeptLocation.company.companyName}
		     				<span id="quotationNameselect_${status.index+1}" class="float-right" style="height: 30px; width:20px; margin-top: -10px; "></span>
		     			</span>  
		     		</li>
		     	</c:forEach> 
	     	</ul>  
	 </div>
	 <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right" style="position: absolute; top:86%;left:92%;">
			<div class="portlet-header ui-widget-header float-right close" id="close" style="cursor:pointer;">close</div>  
	</div>
 </div> 
 <script type="text/javascript"> 
 $(function(){
	 $($($('#common-popup').parent()).get(0)).css('width',800);
	 $($($('#common-popup').parent()).get(0)).css('height',250);
	 $($($('#common-popup').parent()).get(0)).css('padding',0);
	 $($($('#common-popup').parent()).get(0)).css('left',283);
	 $($($('#common-popup').parent()).get(0)).css('top',100);
	 $($($('#common-popup').parent()).get(0)).css('overflow','hidden');
	 $('#common-popup').dialog('open'); 
	 
	 $('.autoComplete').combobox({ 
	       selected: function(event, ui){  
	    	   var quotationdetails=$(this).val();
		       var quotationarray=quotationdetails.split("|");
		       $('#quotationId').val(quotationarray[0]);  
		       $('#quotationNumber').val($(".autoComplete option:selected").text());
		       $('#currencyId').val(quotationarray[2]);
		       $('#supplierId').val(quotationarray[3]);  
		       $('#supplierNumber').val(quotationarray[4]);  
		       $('#supplierName').val(quotationarray[5]);  
		       $('#currencyId').attr('disabled',true);
		       $('.supp-pop').css('display','none');
		       $('.site-pop').css('display','none'); 
		       $('#common-popup').dialog("close");  
		       callQuotationDetails($('#quotationId').val());
	   } 
	}); 
	 
	 $('.quotation_list_li').live('click',function(){
			var tempvar="";var idarray = ""; var rowid="";
			$('.quotation_list_li').each(function(){  
				 currentId=$(this); 
				 tempvar=$(currentId).attr('id');  
				 idarray = tempvar.split('_');
				 rowid=Number(idarray[1]);  
				 if($('#quotationNameselect_'+rowid).hasClass('selected')){ 
					 $('#quotationNameselect_'+rowid).removeClass('selected');
				 }
			}); 
			currentId=$(this); 
			tempvar=$(currentId).attr('id');  
			idarray = tempvar.split('_');
			rowid=Number(idarray[1]);  
			$('#quotationNameselect_'+rowid).addClass('selected');
		});
	 
	 $('.quotation_list_li').live('dblclick',function(){ 
			var tempvar="";var idarray = ""; var rowid="";
			 currentId=$(this);  
			 tempvar=$(currentId).attr('id');   
			 idarray = tempvar.split('_'); 
			 rowid=Number(idarray[1]);  
			 $('#quotationId').val($('#quotationbyid_'+rowid).val());
			 $('#quotationNumber').val($('#quotationbynumberid_'+rowid).val());  
			 $('#currencyId').val($("#quotationbycurrencyid_"+rowid).val());
	         $('#supplierId').val($("#quotationbysupplierid_"+rowid).val());  
	         $('#supplierName').val($("#quotationbysuppliernameid_"+rowid).val()); 
	         $('#supplierNumber').val($("#quotationbysuppliernumberid_"+rowid).val());
	         
	         $('#currencyId').attr('disabled',true);
	         $('.supp-pop').css('display','none');
	         $('.site-pop').css('display','none'); 
			 $('#common-popup').dialog('close'); 
			 callQuotationDetails($('#quotationId').val());
		});
		$('#close').click(function(){
			 $('#common-popup').dialog('close'); 
		});  
 });
 function callQuotationDetails(quotationId){ 
	var rowId=Number(0);
	var  itemNature='D'; 
	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/showquotation_detail.action", 
		 	async: false,  
		 	data:{quotationId:quotationId, rowId:rowId, itemNature:itemNature},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.quotation_detail').html(result);  
			},
			error:function(result){  
				 $('.quotation_detail').html(result); 
			}
		});  
}
 </script>
 <style>
 .ui-autocomplete-input {padding: 0.48em 0 0.47em 0.45em; width:80%; position:absolute; right:3px;}
	  .ui-button { margin: 0px;} 
	.ui-autocomplete{
		width:194px!important;
	} 
	.quotation_list ul li{
		padding:3px;
	}
	.quotation_list {
	    border: 1px solid #E5E5E5;
	    float: left;
	    height:175px;
	    overflow-y: auto;
	    overflow-x: hidden;
	    padding: 4px;
	    position: relative;
	    top: -25px;
	}
	.selected{
		background: url("./images/checked.png");
		background-repeat: no-repeat; 
	}
	.width38{
		width:38%;
	}
	.width28{
		width:28%;
	}
</style>