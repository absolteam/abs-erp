<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript"> 
var pettyCashId=0;  
var oTable; var selectRow=""; var aSelected = []; var aData="";
var categoryId = 0;
$(function(){
	$('.formError').remove();
	filterJsonPettyCashTransaction();
	$('.print-call').click(function(){
		if(pettyCashId!=null && pettyCashId!="" && pettyCashId > 0){
			window.open('show_pettycash_report.action?recordId='+ pettyCashId + '&format=HTML', '',
			'width=1200,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a record to view.").slideDown();
			$('#error_message').delay(3000).slideUp();
		}
		return false;
	}); 
	 
	/* Click event handler */
	$('#PettyCashDTR tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          pettyCashId=aData[0];   
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          pettyCashId=aData[0];  
	      }
	});

	$('.pdf-download-call').click(function(){
		if(pettyCashId!=null && pettyCashId!="" && pettyCashId > 0){
			window.open("<%=request.getContextPath()%>/get_petty_cash_report_pdf.action?recordId="+ pettyCashId+"&format=PDF", '_blank',
			'width=800,height=800,scrollbars=yes,left=100px');
		 }else{
			$('#error_message').hide().html("Please select a record to view.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});

	$(".xls-download-call").click(function(){ 
		if(pettyCashId!=null && pettyCashId!="" && pettyCashId > 0){
			window.open('<%=request.getContextPath()%>/show_pettycash_excel_report.action?recordId='+pettyCashId,
				'_blank','width=0,height=0'); 
		}else{
			$('#error_message').hide().html("Please select a record to view.").slideDown();
			$('#error_message').delay(3000).slideUp();
			return false;
		}
	});  

	$('#reset').click(function(){ 
		 checkDataTableExsist(); 
	});
});
function checkDataTableExsist(){
	oTable = $('#PettyCashDTR').dataTable();
	oTable.fnDestroy();
	$('#PettyCashDTR').remove(); 
	$('#pettycash').html("<table class='display' id='PettyCashDTR'></table>");
	filterJsonPettyCashTransaction();
}
function filterJsonPettyCashTransaction(){ 
	$('#PettyCashDTR').dataTable({ 
		"sAjaxSource": "showall_pettycash_parent.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Petty Cash", "bVisible": false},
			{ "sTitle": "Voucher No"},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Transaction Type"}, 
			{ "sTitle": "Receiver"}, 
			{ "sTitle": "Ref.Type"},  
			{ "sTitle": "COA"}, 
			{ "sTitle": "Narration"}, 
			{ "sTitle": "Created By"},  
		], 
		"sScrollY": $("#main-content").height() - 225,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 
	//init datatable
	oTable = $('#PettyCashDTR').dataTable();
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container"> 
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"  class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			petty cash
		</div> 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
	   	    <div id="rightclickarea">
			 	<div id="pettycash">
					<table class="display" id="PettyCashDTR"></table>
				</div> 
			</div>		
			<div class="vmenu">
	 	       	<div class="first_li"><span>Print</span></div> 
			</div>  
		</div> 
	</div> 
</div>
<div class="process_buttons">
	<div id="hrm" class="width100 float-right ">
	  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
	 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
	  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
	 </div>
</div> 