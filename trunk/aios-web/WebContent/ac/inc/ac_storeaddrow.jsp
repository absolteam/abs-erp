<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<tr class="rowid" id="fieldrow_${rowId}">
	<td style="display: none;" id="lineId_${rowId}">${rowId}</td>
	<td><input type="text" maxlength="8" class="width90 aisleNumber"
		name="aisleNumber" id="aisleNumber_${rowId}" /></td>
	<td><input type="text" class="width90 sectionName"
		name="sectionName" id="sectionName_${rowId}" />
	</td>
	<td><select name="storageSection" id="storageSection_${rowId}"
		class="width80">
			<option value="">Select</option>
			<c:forEach var="SECTION" items="${STORAGE_SECTIONS}">
				<option value="${SECTION.lookupDetailId}">${SECTION.displayName}</option>
			</c:forEach>
	</select> <span class="button" style="position: relative;"> <a
			style="cursor: pointer;" id="STORAGE_SECTION_${rowId}"
			class="btn ui-state-default ui-corner-all store-section-lookup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="text" class="width90 dimension" name="dimension"
		id="dimension_${rowId}" /></td>
	<td>
		<div class="portlet-header ui-widget-header float-right addBinsAddRow"
			id="addBins_${rowId}" style="cursor: pointer;">Add Rack</div></td>
	<td style="width: 0.01%;" class="opn_td" id="option_${rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${rowId}" style="display: none; cursor: pointer;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${rowId}" style="display: none; cursor: pointer;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${rowId}" style="display: none;" title="Working">
			<span class="processing"></span> </a> <input type="hidden" name="asileid"
		value="0" id="aisleid_${rowId}" /></td>
</tr>
<script type="text/javascript">
$(function(){
	$('.addBinsAddRow').click(function(){  
		var currentId = Number(getRowId($(this).attr('id'))); 
		var sectionReferenceId = 0;
		var aisleId = Number($('#aisleid_'+currentId).val());
		if (aisleId > 0)
			sectionReferenceId = aisleId;
		else
			sectionReferenceId = Number($('#lineId_'+currentId).text());
		$(".bin-entry").remove();
  		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/show_bin_entry.action", 
	     	async: false, 
	     	data:{sectionReferenceId: sectionReferenceId},
			dataType: "html",
			cache: false,
			success: function(result){   
				$("#temp-result").html(result);   
				manupulateBinLastRow(); 
				$('.tempbinrowid').each(function(){   
					 var rowId=getRowId($(this).attr('id')); 
					 $('#binSection_'+rowId).val($('#tempbinSection_'+rowId).val());
				 });
				return false;
			} 		
		}); 
		return false;
	 });
	$('.addBinsAddRow').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
});
</script>