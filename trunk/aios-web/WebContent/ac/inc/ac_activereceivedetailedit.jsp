<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
#DOMWindow{	
	width:95%!important;
	height:88%!important;
	overflow-x: hidden!important;
	overflow-y: auto!important;
} 
.quantityerr{ font-size: 9px !important;
	color: #ca1e1e;
}
</style>
<div id="hrm" class="portlet-content width100 invoiceline-info" style="height: 90%;"> 
	<div class="portlet-content"> 
	<div  id="page-error-popup" class="response-msg error ui-corner-all width90" style="display:none;"></div>  
	<div id="warning_message-popup" class="response-msg notice ui-corner-all width90" style="display:none;"></div> 
	<div id="hrm" class="hastable width100"  >  
		 <table id="hastab-1" class="width100"> 
			<thead>
			   <tr>  
			   		<th>Product Code</th> 
				    <th>Product</th> 
				    <th style="display: none;">UOM</th>
				    <th>Receive Qty</th> 
				    <th>Unit Rate</th> 
				    <th>Total</th>  
			  </tr>
			</thead> 
			<tbody class="tab-1">  
				<c:forEach var="bean" varStatus="status" items="${RECEIVE_DETAIL_BYID}"> 
					<tr class="rowidrc" id="fieldrowrc_${status.index+1}">
						<td style="display:none;" id="lineIdrc_${status.index+1}">${status.index+1}</td> 
						<td id="productcode_${status.index+1}"> 
							${bean.product.code}
						</td>
						<td id="productname_${status.index+1}"> 
							${bean.product.productName}
						</td> 
						<td style="display: none;" id="uom_${status.index+1}">${bean.product.lookupDetailByProductUnit.displayName}</td> 
						<td id="receiveqty_${status.index+1}">${bean.receiveQty}</td>
						<td style="display: none;">  
							<%-- <c:choose>
								<c:when test="${bean.selectedInvQty ne null && bean.selectedInvQty ne '' && bean.selectedInvQty gt 0}">
									<span>${bean.selectedInvQty}</span>
									<input type="text" class="width90 invoiceQty" style="border:0px;" value="${bean.selectedInvQty}" name="invoiceQty" id="invoiceQty_${status.index+1}"/>
									<c:set var="total" value="${bean.selectedInvQty * bean.unitRate}"/>
									<input type="hidden" value="${bean.selectedInvQty}" name="invoiceHiddenQty" id="invoiceHiddenQty_${status.index+1}"/>
								</c:when> 
								<c:otherwise> --%>
									<span>${bean.invoiceQty}</span>
									<c:set var="total" value="${bean.invoiceQty * bean.unitRate}"/>
									<input type="hidden" class="width90 invoiceQty" style="border:0px;" value="${bean.invoiceQty}" name="invoiceQty" id="invoiceQty_${status.index+1}"/>
									<input type="hidden" value="${bean.invoiceQty}" name="invoiceHiddenQty" id="invoiceHiddenQty_${status.index+1}"/>
							<%-- 	</c:otherwise>
							</c:choose> 
 --%>					</td>  
						<td id="unitRate_${status.index+1}" style="text-align: right;">
							<c:set var="unitrate" value="${bean.unitRate}"/>
							 <%=AIOSCommons.formatAmount(pageContext.getAttribute("unitrate")) %> 
						</td>  
						<td id="total_${status.index+1}" style="text-align: right;"> 
							<%=AIOSCommons.formatAmount(pageContext.getAttribute("total")) %>
							 
						</td>  
						<td style="display:none"> 
							<input type="hidden" name="receiveInvId" id="receiveInvId_${status.index+1}" value="${requestScope.receiveId}"/>
							<input type="hidden" name="productid" id="productid_${status.index+1}" value="${bean.product.productId}"/> 
							<input type="hidden" name="receiveLineId" id="receiveLineId_${status.index+1}" value="${bean.receiveDetailId}"/> 
							<input type="hidden" id="invocieLineId_${status.index+1}" value="${bean.invoiceDetailId}"/>
						</td>  
						<td style="display:none">
							 <input type="text" name="linedescription" id="linedescription_${status.index+1}" value="${bean.description}"/>
						</td>
						<td style="width:0.01%;display:none; " class="opn_td" id="optionrc_${status.index+1}">
							 <%--  <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow_rc" id="DeleteCImage_${status.index+1}" style="cursor:pointer;" title="Delete Record">
									<span class="ui-icon ui-icon-circle-close"></span>
							  </a> --%>
							  
							<c:choose>
							  <c:when test="${bean.invoicedQty ne null && bean.invoicedQty ne '' && bean.invoicedQty gt 0}">
							  	<input type="checkbox" checked="checked" id="InvoicedCheck_${status.index+1}" class="delrow_rc width10"/>
							  </c:when> 
							  <c:otherwise> 
							  	<input type="checkbox" checked="checked" id="InvoicedCheck_${status.index+1}" class="delrow_rc width10"/>
							  </c:otherwise>
							</c:choose>
						</td>   
					</tr>
				</c:forEach>
			</tbody>
			</table>
		</div>  
	</div>   
	<div class="clearfix"></div>  
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons"> 
			<div class="portlet-header ui-widget-header float-left sessionSave" style="cursor:pointer;display:none;"><fmt:message key="accounts.common.button.ok"/></div>
			<div class="portlet-header ui-widget-header float-left closeDom" style="cursor:pointer;"><fmt:message key="accounts.common.button.cancel"/></div>
	</div> 
</div> 
<script type="text/javascript">
$(function() {
	$jquery("#invoicevalidation").validationEngine('attach'); 
});
</script>