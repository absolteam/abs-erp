<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@page import="org.apache.struts2.ServletActionContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script> 
<script type="text/javascript"> 
var closingPeriodDetail ="";
$(function(){
	$('#process').click(function(){
		var alertId = $('#alertRecordId').val();
		closingPeriodDetail=getClosingPeriodDetail();   
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/closing_period_transaction.action", 
		 	async: false, 
		 	data: {alertId: alertId, transactionDetail: closingPeriodDetail},
		    dataType: "html",
		    cache: false,
			success:function(result){
		 		$("#main-wrapper").html(result);  
			}
		}); 
	});
	
	var getClosingPeriodDetail = function(){
		var closingDetails = "";
		var combinationArray=new Array(); 
		var amountArray=new Array(); 
		$('.rowid').each(function(){ 
			var rowId=getRowId($(this).attr('id'));  
			var codeCombinationID=$('#codeCombinationId_'+rowId).val();  
			var rentAmount=$('#rentAmount_'+rowId).text().trim();
			rentAmount=rentAmount.split(',').join(''); 
			rentAmount=Number(rentAmount); 
			if(typeof codeCombinationID != 'undefined' && codeCombinationID!=null 
					&& rentAmount!="" && rentAmount>0) {
				combinationArray.push(codeCombinationID);
				amountArray.push(rentAmount);   
			} 
		});
		for(var j=0;j<combinationArray.length;j++){  
			closingDetails+=combinationArray[j]+"__"+amountArray[j]; 
			if(j==combinationArray.length-1){   
			} 
			else{
				closingDetails+="#@";
			}
		}   
		return closingDetails;
	 };
});
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>Period Closing</div>
		<div class="portlet-content">
		    <div id="temp-result" style="display:none;"></div> 
			<div id="success-msg" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div> 
			<div class="page-error response-msg error ui-corner-all" style="width:98% !important; display:none;"></div> 	
			<input type="hidden" name="alertRecordId" id="alertRecordId" value="${PERIOD_ALERT_ID}"/>  
			<form id="contractreceipt-form" name="contractdetails"> 
				<div class="clearfix"></div>  
					<div id="hrm" class="hastable width100"> 
					<fieldset>
					<legend>Contract Rent</legend>
							<table id="hastab" class="width100"> 
								<thead>
							   <tr>  
							    <th>Property Name</th>  
						   		 <th style="width:5px;">Contract No.</th>   
								 <th>Bank</th>
								 <th>Cheque No.</th> 
								 <th>Cheque Date</th> 
								 <th>Amount</th> 
								 <th>Combination</th> 
							  </tr>
							</thead> 
							<tbody class="tab"> 
								<c:choose>
									<c:when test="${fn:length(PERIOD_TRANSACTIONVO.periodVOList)gt 0}">
										<c:forEach items="${PERIOD_TRANSACTIONVO.periodVOList}" var="PERIOD_CLOSING" varStatus="status1" >
										 	<tr class="rowid" id="fieldrow_${status1.index+1}" style="height:25px;">  
										 		<td>${PERIOD_CLOSING.propertyName}</td>
								 	 			<td style="width:5px;">
								 	 				 ${PERIOD_CLOSING.contractNumber}
								 	 			</td> 
												<td>${PERIOD_CLOSING.bankName}</td>
												<td>${PERIOD_CLOSING.chequeNo}</td> 
												<td>${PERIOD_CLOSING.chequesDate}</td> 
												<td id="rentAmount_${status1.index+1}">${PERIOD_CLOSING.rentAmount}</td> 
												<td> 
													<span>${PERIOD_CLOSING.rentRevenueCombination}</span>
													<input type="hidden" name="codeCombinationId" id="codeCombinationId_${status1.index+1}" value="${PERIOD_CLOSING.rentRevenueCombinationId}"/>  
												</td>  
											</tr>
										</c:forEach>
									  </c:when>
								</c:choose>	  
							</tbody>
						</table>
					</fieldset>
				</div> 
				<div class="portlet-content">  
					<div class="clearfix"></div>  
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons" > 
						<div class="portlet-header ui-widget-header float-right discard"  id="cancel"><fmt:message key="accounts.common.button.discard"/></div> 
						<div class="portlet-header ui-widget-header float-right process" id="process">process</div>
					</div>  
				</div>
			</form>
		</div>	
	</div>	 
</div>