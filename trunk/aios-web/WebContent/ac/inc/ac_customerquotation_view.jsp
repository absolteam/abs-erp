<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var quotationDetails="";
var chargesDetails = "";
var tempid = "";
var accessCode = "";
var sectionRowId = 0;
var tableRowId = 0;
var globalRowId=null;
$(function(){   
	  
	
	  

	 

	 $('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_quotation_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	 $('.addrowscharges').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tabcharges>.rowidcharges:last").attr('id'))); 
		  id=id+1;  
		  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/customer_quotation_charges_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tabcharges tr:last').before(result);
					 if($(".tabcharges").height()>255)
						 $(".tabcharges").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowidcharges').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#chargesLineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });

	  $('input, select').attr('disabled', true);

	  
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});

	 $('.delrowcharges').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowidcharges').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#chargesLineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	}); 

	  $('#customer-common-popup').live('click',function(){  
	    	 $('#common-popup').dialog('close'); 
	     });

	  $('#product-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('#store-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('#price-common-popup').live('click',function(){  
		   $('#common-popup').dialog('close'); 
	   }); 

	  $('.common_person_list').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/common_person_list.action", 
			 	async: false,  
			 	data: {  personTypes: "1"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});

	  $('.show_customer_list').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_customer_common_popup.action", 
			 	async: false,  
			 	data: { pageInfo: "customer_quotation"},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		});
	  
	  
		if (Number($('#customerQuotationId').val()) > 0) {
			$('#shippingMethod').val($('#tempShippingMethod').val());
			$('#shippingTerm').val($('#tempShippingTerm').val());
			$('#status').val($('#tempStatus').val());
			$('#paymentMode').val($('#tempPaymentMode').val());
			{
				$('.rowid')
						.each(
								function() {
									var rowId = getRowId($(this).attr('id'));
									$('#chargesmodeDetail_' + rowId)
											.val(
													$(
															'#tempChargesModeDetail_'
																	+ rowId)
															.val());
								});
			}
			{
				$('.rowidcharges').each(
						function() {
							var rowId = getRowId($(this).attr('id'));
							$('#chargesTypeId_' + rowId).val(
									$('#tempChargesTypeId_' + rowId).val());
							$('#chargesMode_' + rowId).val(
									$('#tempChargesMode_' + rowId).val());
						});
			}
		}else
			$('#status').val(7);
	});
	 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			Quotation
		</div> 
		<form name="customerQuotationValidation"
			id="customerQuotationValidation" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="customerQuotationId"
					name="customerQuotationId"
					value="${CUSTOMER_QUOTATION.customerQuotationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="height: 160px;">
							<div>
								<label class="width30">Sales Person</label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.person ne null && CUSTOMER_QUOTATION.person ne ''}">
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50"
											value="${CUSTOMER_QUOTATION.person.firstName } ${CUSTOMER_QUOTATION.person.lastName}" />
										<input type="hidden" readonly="readonly" name="personId" id="personId"
												value="${CUSTOMER_QUOTATION.person.personId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="salesPerson"
											id="salesPerson" class="width50" value="${requestScope.personName}"/>
										<input type="hidden" readonly="readonly" name="personId" id="personId"
												value="${requestScope.personId}" />
									</c:otherwise>
								</c:choose> 
							</div> 
							<div>
								<label class="width30"> Payment Mode</label> <select
									name="paymentMode" id="paymentMode" class="width51">
									<option value="">Select</option>
									<c:forEach var="MODE" items="${PAYMENT_MODE}">
										<option value="${MODE.key}">${MODE.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempPaymentMode"
									value="${CUSTOMER_QUOTATION.modeOfPayment}" />
							</div> 
							<div>
								<label class="width30"> Shipping Method</label> <select
									name="shippingMethod" id="shippingMethod" class="width51">
									<option value="">Select</option>
									<c:forEach var="METHOD" items="${SHIPPING_METHOD}">
										<option value="${METHOD.lookupDetailId}">${METHOD.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingMethod"
									value="${CUSTOMER_QUOTATION.lookupDetailByShippingMethod.lookupDetailId}" /> 
							</div>
							<div>
								<label class="width30"> Shipping Terms</label> <select
									name="shippingTerm" id="shippingTerm" class="width51">
									<option value="">Select</option>
									<c:forEach var="TERMS" items="${SHIPPING_TERMS}">
										<option value="${TERMS.lookupDetailId}">${TERMS.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempShippingTerm"
									value="${CUSTOMER_QUOTATION.lookupDetailByShippingTerm.lookupDetailId}" /> 
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea rows="2" cols="4" id="description" name="description"
									class="width50">${CUSTOMER_QUOTATION.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width50">
						<fieldset style="height: 160px;">
							<div style="padding-bottom: 7px;">
								<label class="width30"> Reference No.  </label> <span
									style="font-weight: normal;" id="referenceNumber"> <c:choose>
										<c:when
											test="${CUSTOMER_QUOTATION.referenceNo ne null && CUSTOMER_QUOTATION.referenceNo ne ''}">
												<input tyepe="text" readonly="readonly" value="${CUSTOMER_QUOTATION.referenceNo}" class="width50"/>
											</c:when>
										<c:otherwise>${requestScope.referenceNumber}</c:otherwise>
									</c:choose> </span> <input type="hidden" id="tempreferenceNumber"
									value="${CUSTOMER_QUOTATION.referenceNo}" />
							</div>
							<div class="clearfix"></div>
							<div>
								<label class="width30"> Quotation Date </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.date ne null && CUSTOMER_QUOTATION.date ne ''}">
										<c:set var="quotationDate" value="${CUSTOMER_QUOTATION.date}" />
										<input name="quotationDate" type="text" readonly="readonly"
											id="quotationDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("quotationDate").toString())%>"
											class="quotationDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="quotationDate" type="text" readonly="readonly"
											id="quotationDate"
											class="quotationDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Expiry Date </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.expiryDate ne null && CUSTOMER_QUOTATION.expiryDate ne ''}">
										<c:set var="expiryDate"
											value="${CUSTOMER_QUOTATION.expiryDate}" />
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("expiryDate").toString())%>"
											class="expiryDate validate[required] width50">
									</c:when>
									<c:otherwise>
										<input name="expiryDate" type="text" readonly="readonly"
											id="expiryDate" class="expiryDate validate[required] width50">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30"> Status </label> <select name="status"
									id="status" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="STATUS" items="${QUOTATION_STATUS}">
										<option value="${STATUS.key}">${STATUS.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempStatus"
									value="${CUSTOMER_QUOTATION.status}" />
							</div>
							<div>
								<label class="width30">Customer</label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.customer ne null && CUSTOMER_QUOTATION.customer ne ''}">
										<c:set var="customerName" />
										<c:choose>
											<c:when
												test="${CUSTOMER_QUOTATION.customer.personByPersonId ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.personByPersonId.firstName} ${CUSTOMER_QUOTATION.customer.personByPersonId.lastName}" />
											</c:when>
											<c:when test="${CUSTOMER_QUOTATION.customer.company ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.company.companyName}" />
											</c:when>
											<c:when
												test="${CUSTOMER_QUOTATION.customer.cmpDeptLocation ne null}">
												<c:set var="customerName"
													value="${CUSTOMER_QUOTATION.customer.cmpDeptLocation.company.companyName}" />
											</c:when>
										</c:choose>
										<input type="text" name="customerName" id="customerName"
											value="${customerName}" class="width50" />
										<input type="hidden" id="customerId" name="customerId"
											value="${CUSTOMER_QUOTATION.customer.customerId}" />
									</c:when> 
									<c:otherwise>
										<input type="text" readonly="readonly" name="customerName"
											id="customerName" class="width50" />
										<input type="hidden" id="customerId" name="customerId" />
									</c:otherwise>
								</c:choose> 
							</div>
							<div>
								<label class="width30"> Shipping Date </label>
								<c:choose>
									<c:when
										test="${CUSTOMER_QUOTATION.shippingDate ne null && CUSTOMER_QUOTATION.shippingDate ne ''}">
										<c:set var="shippingDate"
											value="${CUSTOMER_QUOTATION.shippingDate}" />
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											value="<%=DateFormat.convertDateToString(pageContext
							.getAttribute("shippingDate").toString())%>"
											class="shippingDate width50">
									</c:when>
									<c:otherwise>
										<input name="shippingDate" type="text" readonly="readonly"
											id="shippingDate"
											class="shippingDate width50">
									</c:otherwise>
								</c:choose>
							</div> 
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div id="line-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>Quotation Detail</legend>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 5%;">Product</th>
										<th style="width: 5%; display: none;">Store</th>
										<th style="width: 3%;">Quantity</th>
										<th style="width: 5%;">Price</th>
										<th style="width: 3%;">Discount Mode</th>
										<th style="width: 3%;">Discount</th>
										<th style="width: 3%;">Total</th>
										<th style="width: 1%; display: none;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${CUSTOMER_QUOTATION.customerQuotationDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.code} - ${DETAIL.product.productName}</span> 
											</td>
											<td style="display: none;"><input type="hidden" name="storeId"
												id="storeid_${status.index+1}"
												value="${DETAIL.store.storeId}" /> <span
												id="store_${status.index+1}">${DETAIL.store.storeName}</span> 
											</td>
											<td><input type="text" name="productQty"
												id="productQty_${status.index+1}" value="${DETAIL.quantity}"
												class="productQty width98">  
											</td>
											<td><input type="text" name="amount"
												id="amount_${status.index+1}" value="${DETAIL.unitRate}"
												style="text-align: right;" readonly="readonly"
												class="amount width60 right-align">  
											</td>
											<td><input type="hidden" name="tempChargesModeDetail"
												id="tempChargesModeDetail_${status.index+1}"
												value="${DETAIL.isPercentage}" /> <c:set var="isPercentage"
													value="${DETAIL.isPercentage}" />  
											<c:choose>
												<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq true}">
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}" checked="checked"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" style="width:5px;"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:when>
												<c:when test="${DETAIL.isPercentage ne null && DETAIL.isPercentage eq false}">
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" value="false" style="width:5px;" checked="checked"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:when>
												<c:otherwise>
													<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" style="width:5px;"
															 id="pecentagemode_${status.index+1}" value="true"/>
														<span class="float-left" style="position: relative; top: 3px;">Percentage</span></div>
														<div class="float-left">
														<input type="radio" name="chargesmodeDetail_${status.index+1}" class="chargesmodeDetail float-left" 
															id="amountmode_${status.index+1}" value="false" style="width:5px;"/>
														<span class="float-left" style="position: relative; top: 3px;">Amount</span></div>
												</c:otherwise> 
											</c:choose> 
											</td>
											<td><input type="text" name="discount"
												id="discount_${status.index+1}" value="${DETAIL.discount}"
												style="text-align: right;"
												class="discount width90 right-align"> <c:set
													var="discount" value="${DETAIL.discount}" />
											</td>
											<td><c:set var="totalAmount"
													value="${DETAIL.quantity * DETAIL.unitRate}" /> <%
									 		double discount = 0;
									 		double totalAmount = Double.parseDouble(pageContext
									 				.getAttribute("totalAmount").toString());
									 		if (null != pageContext.getAttribute("isPercentage")) {
									 			if (("true").equals(pageContext
									 					.getAttribute("isPercentage").toString())) {
									 				discount = Double.parseDouble(pageContext.getAttribute(
									 						"discount").toString());
									 				discount = (totalAmount * discount) / 100;
									 				totalAmount -= discount;
									 			} else {
									 				discount = Double.parseDouble(pageContext.getAttribute(
									 						"discount").toString());
									 				totalAmount -= discount;
									 			}
									 		}%> 
									 		<span id="totalAmount_${status.index+1}" style="float: right;"><%=totalAmount%></span>
											</td>
											<td style="width: 1%; display: none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="customerQuotationDetailId"
												id="customerQuotationDetailId_${status.index+1}"
												value="${DETAIL.customerQuotationDetailId}" />
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90" id="hrm"
				style="margin-top: 5px;">
				<fieldset>
					<legend>Quotation Charges</legend>
					<div id="hrm" class="hastable width100">
						<table id="hastab_charges" class="width100">
							<thead>
								<tr>
									<th style="width: 1%">L.NO</th>
									<th class="width10">Charges Type</th>
									<th style="width: 5%; display: none;">Mode</th>
									<th style="width: 5%;">Charges</th>
									<th class="width10">Description</th>
									<th style="width: 1%; display: none;"><fmt:message
											key="accounts.common.label.options" /></th>
								</tr>
							</thead>
							<tbody class="tabcharges">
								<c:forEach var="CHARGES"
									items="${CUSTOMER_QUOTATION.customerQuotationCharges}"
									varStatus="status">
									<tr class="rowidcharges" id="fieldrowcharges_${status.index+1}">
										<td id="chargesLineId_${status.index+1}">${status.index+1}</td>
										<td><input type="hidden" name="tempchargesTypeId"
											id="tempChargesTypeId_${status.index+1}"
											value="${CHARGES.lookupDetail.lookupDetailId}" /> <select
											name="chargesTypeId" id="chargesTypeId_${status.index+1}"
											class="chargestypeid width70">
												<option value="">Select</option>
												<c:forEach var="TYPE" items="${CHARGES_TYPE}">
													<option value="${TYPE.lookupDetailId}">${TYPE.displayName}</option>
												</c:forEach>
										</select> 
										</td>
										<td style="display: none;"><input type="hidden" name="tempChargesMode"
											id="tempChargesMode_${status.index+1}"
											value="${CHARGES.isPercentage}" /> <select
											name="chargesMode" id="chargesMode_${status.index+1}"
											class="chargesmode width90">
												<option value="">Select</option>
												<option value="true">Percentage</option>
												<option value="false">Amount</option>
										</select>
										</td>
										<td><input type="text" name="charges"
											id="charges_${status.index+1}" value="${CHARGES.charges}"
											class="charges validate[optional,custom[number]] width98" style="text-align: right;">
										</td>
										<td><input type="text" name="description"
											id="description_${status.index+1}"
											value="${CHARGES.description}" class="description width98">
										</td>
										<td style="width: 1%;display: none;" class="opn_td"
											id="optioncharges_${status.index+1}"><a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addDatacharges"
											id="AddImagecharges_${status.index+1}"
											style="cursor: pointer; display: none;" title="Add Record">
												<span class="ui-icon ui-icon-plus"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editDatacharges"
											id="EditImagecharges_${status.index+1}"
											style="display: none; cursor: pointer;" title="Edit Record">
												<span class="ui-icon ui-icon-wrench"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrowcharges"
											id="DeleteImagecharges_${status.index+1}"
											style="cursor: pointer;" title="Delete Record"> <span
												class="ui-icon ui-icon-circle-close"></span> </a> <a
											class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
											id="WorkingImagecharges_${status.index+1}"
											style="display: none;" title="Working"> <span
												class="processing"></span> </a> <input type="hidden"
											name="customerQuotationChargesId"
											id="customerQuotationChargesId_${status.index+1}"
											value="${CHARGES.customerQuotationChargeId}" />
										</td>
									</tr>
								</c:forEach> 
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>  
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div>