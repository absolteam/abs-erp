<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"
		class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> product
	details
</div>
<div id="hrm">
	<div class="float-right width50"
		style="position: relative; top: -29px;">
		<select name="productdetaillistmul" class="autoComplete width50">
			<option value=""></option>
			<c:forEach items="${PRODUCT_DETAIL}" var="bean" varStatus="status">
				<option
					value="${bean.productId}|${bean.lookupDetailByProductUnit.displayName}|${bean.code}|${bean.lookupDetailByItemNature.displayName}">${bean.productName}</option>
			</c:forEach>
		</select> <span style="margin-top: 2px; cursor: pointer; position: absolute;"
			class="float-right"><img width="24" height="24"
			src="images/icons/Glass.png"> </span>
	</div>
	<div class="float-left width100 product_list_mul" style="padding: 2px;">
		<ul>
			<li class="width30 float-left"><span>Product Code</span>
			</li>
			<li class="width30 float-left"><span>Product Name</span>
			</li>
			<li class="width30 float-left"><span>Unit Name</span>
			</li>
			<li
				style="border-bottom: 1px solid #FFE5B1; margin-left: -4px; padding-right: 5px; margin-bottom: 5px;"
				class="width100 float-left"></li>
			<c:forEach items="${PRODUCT_DETAIL}" var="bean" varStatus="status">
				<li id="productdetmul_${status.index+1}"
					class="product_list_mul_li width100 float-left" style="height: 20px;">
					<input type="hidden" value="${bean.productId}"
					id="productbyid_${status.index+1}"> <input type="hidden"
					value="${bean.lookupDetailByProductUnit.displayName}"
					id="productbyuomid_${status.index+1}"> <input type="hidden"
					value="${bean.code}" id="productbycodeid_${status.index+1}">
					<input type="hidden" value="${bean.productName}"
					id="productbynameid_${status.index+1}"> <input
					type="hidden" value="${bean.lookupDetailByItemNature.displayName}"
					id="costingtypebyid_${status.index+1}"> <span
					id="productbycode_${status.index+1}" style="cursor: pointer;"
					class="float-left width30">${bean.code}</span> <span
					id="productbyname_${status.index+1}" style="cursor: pointer;"
					class="float-left width30">${bean.productName}</span> <span
					id="productbyuom_${status.index+1}" style="cursor: pointer;"
					class="float-left width30">${bean.lookupDetailByProductUnit.displayName} <span
						id="productNameselect_${status.index+1}" class="float-right"
						style="height: 30px; width: 20px; margin-top: -10px;"></span> </span></li>
			</c:forEach>
		</ul>
	</div>
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right"
		style="position: absolute; top: 87%; right: 0; float: right;">
		<div class="portlet-header ui-widget-header float-right close"
			id="close" style="cursor: pointer;">close</div>
		<div class="portlet-header ui-widget-header float-right close_oki"
			id="close_oki" style="cursor: pointer;">oki</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$($($('#common-popup').parent()).get(0)).css('width', 800);
		$($($('#common-popup').parent()).get(0)).css('height', 250);
		$($($('#common-popup').parent()).get(0)).css('padding', 0);
		$($($('#common-popup').parent()).get(0)).css('left', 283);
		$($($('#common-popup').parent()).get(0)).css('top', 100);
		$($($('#common-popup').parent()).get(0)).css('overflow', 'hidden');
		$('#common-popup').dialog('open');

		$('.autoComplete').combobox({
			selected : function(event, ui) {

				//$('#common-popup').dialog("close");
			}
		});

		$('.product_list_mul_li').live('click', function() {
			var tempvar = "";
			var idarray = "";
			var rowid = ""; 
			currentId = $(this);
			tempvar = $(currentId).attr('id');
			idarray = tempvar.split('_');
			rowid = Number(idarray[1]);
			$('#productNameselect_'+rowid).addClass('selected');
		});

		$('#close_oki').click(function() { 
			var jsonData = [];
			$('.product_list_mul_li').each(function() {
				currentId = $(this);
				tempvar = $(currentId).attr('id');
				idarray = tempvar.split('_');
				rowid = Number(idarray[1]);
				if ($('#productNameselect_' + rowid).hasClass('selected')) {
					jsonData.push({
						"recordId" : Number($('#productbyid_' + rowid).val()),
						"recordName" : $('#productbynameid_' + rowid).val(),
						"entityName" : "Product"
					}); 
				}
			});  
			var jsonObject = {
					"selectiontitle" : "Multiple Product Discount",
					"record" : jsonData
				};
			setJsonDiscountData(jsonObject);
			$('#common-popup').dialog('close');
		});

		$('#close').click(function() { 
			$('#common-popup').dialog('close');
		});
	});
</script>
<style>
.ui-autocomplete-input {
	padding: 0.48em 0 0.47em 0.45em;
	width: 80%;
	position: absolute;
	right: 3px;
}

.ui-button {
	margin: 0px;
}

.ui-autocomplete {
	width: 194px !important;
}

.product_list_mul ul li {
	padding: 3px;
}

.product_list_mul {
	border: 1px solid #E5E5E5;
	float: left;
	height: 175px;
	overflow-y: auto;
	overflow-x: hidden;
	padding: 4px;
	position: relative;
	top: -25px;
}

.selected {
	background: url("./images/checked.png");
	background-repeat: no-repeat;
}

.width38 {
	width: 38%;
}

.width28 {
	width: 28%;
}
</style>