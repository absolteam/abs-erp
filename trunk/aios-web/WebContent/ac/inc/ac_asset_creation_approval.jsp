<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var idname="";
var slidetab="";
var actionname="";
var assetDetails="";
var accessCode = "";
var sectionRowId = 0;
var assetClassType ="";
$(function(){  

	$('input,select').attr('disabled', true); 
	$('.costType').live('change',function(){
		var rowId = getRowId($(this).attr('id'));
		triggerAddRow(rowId);
		return false;
	}); 

		if (Number($('#assetCreationId').val()) > 0) {
			$('#assetClass').val($('#tempAssetClass').val());
			$('#assetSubClass').val($('#tempAssetSubClass').val());
			$('#assetType').val($('#tempAssetType').val());
			$('#ownerType').val($('#tempOwnerType').val());
			$('#assetColor').val($('#tempAssetColor').val());
			$('#assetCondition').val($('#tempAssetCondition').val());
			$('#category').val($('#tempCategory').val());
			$('#plateColor').val($('#tempPlateColor').val());
			$('#plateType').val($('#tempPlateType').val());
			$('#convention').val($('#tempconvention').val());
			$('#depreciationMethod').val($('#tempdepreciationMethod').val());
			$('.rowid').each(function() {
				var rowId = getRowId($(this).attr('id'));
				$('#costType_' + rowId).val($('#tempcostType_' + rowId).val());
			});
			
			calculateTotalAmount();
		}

	});
	 
	function getRowId(id) {
		var idval = id.split('_');
		var rowId = Number(idval[1]);
		return rowId;
	}
	 
	function calculateTotalAmount() {
		var totalCost = 0;
		$('#totalAmount').text('');
		$('.rowid').each(function() {
			var rowId = getRowId($(this).attr('id'));
			var amount = Number($('#amount_' + rowId).val().trim());
			totalCost = Number(totalCost + amount);
		});
		$('#totalAmount').text(Number(totalCost).toFixed(2));
	}
 
</script>
<div id="main-content"> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>asset
			creation
		</div> 
		<form name="asset_creations" id="asset_creations" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetCreationId" name="assetCreationId"
					value="${ASSET_CREATION.assetCreationId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width40 float-left">
						<fieldset style="min-height: 235px;">
							<div>
								<label class="width30" for="assetNumber">Asset Number<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_CREATION.assetCreationId ne null && ASSET_CREATION.assetCreationId ne ''}">
										<input type="text" readonly="readonly" name="assetNumber"
											id="assetNumber" class="width50 validate[required]"
											value="${ASSET_CREATION.assetNumber}" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" name="assetNumber"
											id="assetNumber" class="width50 validate[required]"
											value="${assetNumber}" />
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="assetName">Asset Name<span
									class="mandatory">*</span> </label> <input type="text"
									id="assetName"
									value="${ASSET_CREATION.assetName}"
									class="width50 validate[required]" /> <input type="hidden"
									id="storeId"/> <input type="hidden"
									id="productId"/><span
									class="button"> <a style="cursor: pointer; display: none;"
									class="btn ui-state-default ui-corner-all show_asset_product_detail width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetClass">Asset Class<span
									class="mandatory">*</span> </label> <select name="assetClass"
									id="assetClass" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="assetClass" items="${ASSET_CLASS}">
										<option value="${assetClass.key}">${assetClass.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetClass"
									value="${ASSET_CREATION.assetClass}" />
							</div>
							<div>
								<label class="width30" for="assetSubClass">Sub Class<span
									class="mandatory">*</span> </label> <select name="assetSubClass"
									id="assetSubClass" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="assetSubClass" items="${ASSET_SUB_CLASS}">
										<option value="${assetSubClass.lookupDetailId}">${assetSubClass.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetSubClass"
									value="${ASSET_CREATION.lookupDetailByAssetSubclass.lookupDetailId}" />
								<span class="button" style="position: relative; display: none;"> <a
									style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all assetsubclass-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetType">Asset Type<span
									class="mandatory">*</span> </label> <select name="assetType"
									id="assetType" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetType" items="${ASSET_TYPE}">
										<option value="${assetType.lookupDetailId}">${assetType.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempAssetType"
									value="${ASSET_CREATION.lookupDetailByAssetType.lookupDetailId}" />
								<span class="button" style="position: relative; display: none;"> <a
									style="cursor: pointer;" id="ASSET_TYPE"
									class="btn ui-state-default ui-corner-all assettype-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="assetCondition">Asset
									Condition</label> <select name="assetCondition" id="assetCondition"
									class="width51">
									<option value="">Select</option>
									<c:forEach var="assetCondition" items="${ASSET_CONDITION}">
										<option value="${assetCondition.key}">${assetCondition.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempAssetCondition"
									value="${ASSET_CREATION.assetCondition}">
							</div>
							<div>
								<label class="width30" for="ownerType">Owner Type</label> <select
									name="ownerType" id="ownerType" class="width51">
									<option value="">Select</option>
									<option value="P">Person</option>
									<option value="C">Company</option>
								</select> <input type="hidden" value="${ASSET_CREATION.ownerType}"
									id="tempOwnerType" />
							</div>
							<div>
								<label class="width30" for="ownerName">Owner</label> <input
									type="text" readonly="readonly" id="ownerName"
									value="${ASSET_CREATION.ownerName}" class="width50" /> <input
									type="hidden" readonly="readonly" id="ownerId"
									value="${ASSET_CREATION.ownerId}" /> <span class="button">
									<a style="cursor: pointer; display: none;" 
									class="btn ui-state-default ui-corner-all common_person_company_list width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
						</fieldset>
					</div>

					<div class="width30 float-left">
						<fieldset style="min-height: 235px;">
							<div>
								<label class="width30" for="brand">Brand</label> <input
									type="text" name="brand" id="brand" class="width50"
									value="${ASSET_CREATION.brand}" />
							</div>
							<div>
								<label class="width30" for="manufacturer">Manufacturer</label> <input
									type="text" name="manufacturer" id="manufacturer"
									class="width50" value="${ASSET_CREATION.manufacturer}" />
							</div>
							<div>
								<label class="width30" for="assetMake">Asset Make</label> <input
									type="text" name="assetMake" id="assetMake"
									value="${ASSET_CREATION.assetMake}" class="width50" />
							</div>
							<div>
								<label class="width30" for="assetModel">Asset Model</label> <input
									type="text" name="assetModel"
									value="${ASSET_CREATION.assetModel}" id="assetModel"
									class="width50">
							</div>
							<div>
								<label class="width30" for="serialNumber">Serial No.</label> <input
									type="text" name="serialNumber" id="serialNumber"
									value="${ASSET_CREATION.serialNumber}" class="width50" />
							</div>
							<div style="display: none;">
								<label class="width30" for="assetColor">Asset Color</label> <select
									name="assetColor" id="assetColor" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetColor" items="${ASSET_COLOR}">
										<option value="${assetColor.key}">${assetColor.value}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempAssetColor"
									value="${ASSET_CREATION.assetColor}">
							</div>
							<div>
								<label class="width30" for="category">Category</label> <select
									name="category" id="category" class="width51">
									<option value="">Select</option>
									<c:forEach var="assetCategory" items="${ASSET_CATEGORY}">
										<option value="${assetCategory.lookupDetailId}">${assetCategory.displayName}</option>
									</c:forEach>
								</select> <input type="hidden" id="tempCategory"
									value="${ASSET_CREATION.lookupDetailByCategory.lookupDetailId}">
								<span class="button" style="position: relative; display: none;"> <a
									style="cursor: pointer;" id="ASSET_CATEGORY"
									class="btn ui-state-default ui-corner-all assetcategory-lookup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_CREATION.description}</textarea>
							</div>
							<div id="asset_vehicle" style="display: none">
								<div>
									<label class="width30" for="yearMake">Make Year</label> <input
										type="text" name="yearMake" id="yearMake"
										value="${ASSET_CREATION.yearMake}" class="width50" />
								</div>
								<div>
									<label class="width30" for="assetVin">VIN</label> <input
										type="text" name="assetVin" id="assetVin"
										value="${ASSET_CREATION.assetVin}" class="width50" />
								</div>
								<div>
									<label class="width30" for="plateNumber">Plate No</label> <input
										type="text" name="plateNumber" id="plateNumber"
										value="${ASSET_CREATION.plateNumber}" class="width50" />
								</div>
								<div>
									<label class="width30" for="tcNumber">TC No</label> <input
										type="text" name="tcNumber" id="tcNumber"
										value="${ASSET_CREATION.tcNumber}" class="width50" />
								</div>
								<div>
									<label class="width30" for="plateColor">Plate Color</label> <select
										name="plateColor" id="plateColor" class="width50">
										<option value="">Select</option>
										<c:forEach var="assetColor" items="${ASSET_COLOR}">
											<option value="${assetColor.key}">${assetColor.value}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempPlateColor"
										value="${ASSET_CREATION.plateColor}">
								</div>
								<div>
									<label class="width30" for="plateType">Plate Type</label> <select
										name="plateType" id="plateType" class="width51">
										<option value="">Select</option>
										<c:forEach var="plateType" items="${PLATE_TYPE}">
											<option value="${plateType.key}">${plateType.value}</option>
										</c:forEach>
									</select> <input type="hidden" id="tempPlateType"
										value="${ASSET_CREATION.plateType}">
								</div>
							</div>
						</fieldset>
					</div>
					<div class="width30 float-left">
						<fieldset style="min-height: 235px;"> 
							<div>
								<label class="width30" for="depreciationMethod">Dep.Method<span
									class="mandatory">*</span> </label> <select name="depreciationMethod"
									id="depreciationMethod" class="validate[required] width51">
									<option value="">Select</option>
									<c:forEach var="depreciationMethod"
										items="${DEPRECIATION_METHOD}">
										<option value="${depreciationMethod.key}">${depreciationMethod.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly"
									id="tempdepreciationMethod"
									value="${ASSET_CREATION.depreciationMethod}" />
							</div>
							<div>
								<label class="width30" for="decliningVariance">Variance</label>
								<input type="text" name="decliningVariance"
									id="decliningVariance"
									value="${ASSET_CREATION.decliningVariance}" class="width50 validate[optional,custom[number]]" />
							</div>
							<div>
								<label class="width30" for="commenceDate">Commence<span
									class="mandatory">*</span> </label> <input type="text"
									id="commenceDate" class="validate[required] width50"
									readonly="readonly" value="${ASSET_CREATION.depreciationDate}" />
							</div>
							<div>
								<label class="width30" for="Convention">Convention<span
									class="mandatory">*</span> </label> <select name="convention"
									id="convention" class="validate[required] width51">
									<option value="">Select</option>
									<c:forEach var="convention" items="${CONVENTION}">
										<option value="${convention.key}">${convention.value}</option>
									</c:forEach>
								</select> <input type="hidden" readonly="readonly" id="tempconvention"
									value="${ASSET_CREATION.convention}" />
							</div>
							<div>
								<label class="width30" for="usefulLife">Useful Life<span
									class="mandatory">*</span> </label> <input type="text"
									name="usefulLife" id="usefulLife"
									value="${ASSET_CREATION.usefulLife}"
									class="width50 validate[required,custom[number]]" />
							</div> 
							<div>
								<label class="width30" for="scrapValue">Scrap Value</label> <input
									type="text" name="scrapValue" id="scrapValue"
									value="${ASSET_CREATION.scrapValue}"
									class="width50 validate[optional,custom[number]]" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<c:if test="${ASSET_CREATION.assetDetails ne null && fn:length(ASSET_CREATION.assetDetails)>0}"> 
				<div class="portlet-content quotation_detail"
					style="margin-top: 10px;" id="hrm">
					<fieldset>
						<legend>Asset Detail</legend> 
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th style="width: 1%">Line No</th>
										<th style="width: 5%">Cost Type</th>
										<th style="width: 5%">Invoice</th>
										<th style="width: 5%">Amount</th>
										<th style="width: 10%">Description</th>
										<th style="width: 0.01%; display: none;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="detail" items="${ASSET_CREATION.assetDetails}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td id="lineId_${status.index+1}">${status.index+1}</td>
											<td><select name="costType" class="width70 costType"
												id="costType_${status.index+1}">
													<option value="">Select</option>
													<c:forEach var="costType" items="${COST_TYPE}">
														<option value="${costType.lookupDetailId}">${costType.displayName}</option>
													</c:forEach>
											</select> <input type="hidden" id="tempcostType_${status.index+1}"
												value="${detail.lookupDetail.lookupDetailId}" /> <span
												class="button" style="position: relative; display: none;"> <a
													style="cursor: pointer;"
													id="ASSET_COST_TYPE_${status.index+1}"
													class="btn ui-state-default ui-corner-all assetcost-lookup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
											<td>
												<input type="text"
													class="width70" readonly="readonly"
													id="paymentDetail_${status.index+1}" value="${detail.directPaymentDetail.invoiceNumber}" />
												<input type="hidden" readonly="readonly"
													id="paymentDetailId_${status.index+1}" value="${detail.directPaymentDetail.directPaymentDetailId}"/>
												<span class="button" style="position: relative; display: none;"> <a
														style="cursor: pointer;" id="paymentAssetDetail_${status.index+1}"
														class="btn ui-state-default ui-corner-all paymentAssetDetail width100">
															<span class="ui-icon ui-icon-newwin"> </span> </a>
												 </span>
											</td>
											<td><input type="text" readonly="readonly"
												class="width96 amount"
												id="amount_${status.index+1}" value="${detail.directPaymentDetail.amount}"
												style="text-align: right;" /></td>
											<td><input type="text" name="linedescription"
												id="linedescription_${status.index+1}"
												value="${detail.description}" /> <input type="hidden"
												id="assetDetailId_${status.index+1}"
												value="${detail.assetDetailId}" /></td>
											<td style="width: 0.01%; display: none;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a>
											</td>
										</tr>
									</c:forEach> 
								</tbody>
							</table>
						</div>
					</fieldset>
				</div> 
			<div class="clearfix"></div>
			<div class="width30 float-right" style="font-weight: bold;">
				<span>Total: </span> <span id="totalAmount"></span>
			</div> 
			</c:if>
			<div class="clearfix"></div> 
		</form>
	</div>
</div>
<div class="clearfix"></div> 