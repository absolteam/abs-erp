<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#DOMWindow {
	width: 30% !important;
	height: 20% !important;
	overflow-x: hidden !important;
	overflow-y: auto !important;
	z-index: 10000;
	top: 100px !important;;
}

.ui-autocomplete-input {
	min-height: 26px !important;
	width: 51% !important;
}

.ui-autocomplete {
	height: 250px;
	overflow-y: auto;
	overflow-x: hidden;
}
</style>
<script type="text/javascript">
$(function(){
	$('.accountCode').combobox({ 
	   selected: function(event, ui){  
		   var accountId = getRowId($.trim($(this).val())); 
		   var segmentId = getLastId($.trim($(this).val())); 
		   var accessId = Number($('#accessId').val()); 
		   var combinationId = Number($('#combinationId').val());
		   if(accountId > 0){  
			   saveCombination(accountId, accessId, segmentId, combinationId); 
		   }  
	   } 
	});
});
</script>
<div class="mainhead portlet-header ui-widget-header">
	<span style="display: none;"
		class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> 
		Chart of Account
		 
</div>
<div id="hrm" class="width100 float-left">
	<input type="hidden" id="combinationId"
		value="${SEGMENT_DETAIL.combinationId}" /> <input type="hidden"
		id="accessId" value="${SEGMENT_DETAIL.parentAccessId}" />
		<input type="hidden"
		id="segmentId" value="${SEGMENT_DETAIL.segmentId}" />
		<input type="hidden" id="segmentNameCst" value="${SEGMENT_DETAIL.segmentName}"/> 
	<div class="width100 float-left">
		<div id="pageerror_message" class="response-msg error ui-corner-all" 
				style="display: none;  width: 200px;"></div>  
		<c:choose>
			<c:when
				test="${ACCOUNTS_LIST ne null && fn:length(ACCOUNTS_LIST)>0 }">
				<label class="width20" id="segmentName">${SEGMENT_DETAIL.parentName}</label>
				<select name="accountCode" class="accountCode width50" id="accountCode">
					<option value=""></option>
					<c:forEach var="detail" items="${ACCOUNTS_LIST}">
						<option value="${detail.accountId}_${SEGMENT_DETAIL.segmentId}">${detail.account} [${detail.code}]</option>
					</c:forEach> 
				</select>
			</c:when>
		</c:choose>
	</div> 
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
		style="position: absolute; right: 0px; margin: 10px; bottom: 0px; overflow: hidden;"> 
		<div
			class="portlet-header ui-widget-header float-right combinationadd_discard"
			style="cursor: pointer;">
			<fmt:message key="accounts.common.button.cancel" />
		</div>
		<c:if test="${SEGMENT_DETAIL.showSubAccountBtn eq true}">
			<div
				class="portlet-header ui-widget-header float-left combination_subadd"
				style="cursor: pointer; background: #0B610B;">
				Add Sub ${SEGMENT_DETAIL.segmentName} 
			</div> 
		</c:if> 
	</div>
</div>