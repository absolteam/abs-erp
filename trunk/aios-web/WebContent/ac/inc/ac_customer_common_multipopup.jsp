<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
<script type="text/javascript">
	var oTable;
	var selectRow = "";
	var aSelected = [];
	var aData = "";
	$(function() {
		oTable = $('#customerMultiSelect')
				.dataTable(
						{
							"bJQueryUI" : true,
							"sPaginationType" : "full_numbers",
							"bFilter" : true,
							"bInfo" : true,
							"bSortClasses" : true,
							"bLengthChange" : true,
							"bProcessing" : true,
							"bDestroy" : true,
							"iDisplayLength" : 10,
							"sAjaxSource" : "show_customer_common_jsonlist.action?pageInfo="
									+ $('#pageInfo').val(),

							"aoColumns" : [ {
								"mDataProp" : "customerNumber"
							}, {
								"mDataProp" : "customerName"
							}, {
								"mDataProp" : "type"
							} ],
						});

		/* Click event handler */
		$('#customerMultiSelect tbody tr').live('click', function() {
 			if ($(this).hasClass('row_multi_selected'))
				$(this).removeClass('row_multi_selected');
			else
				$(this).addClass('row_multi_selected');
			return false;
		});
		
		$('#customer-list-ok').live(
				'click',
				function() {
					var jsonData = [];
					
					$('.row_multi_selected').each(
							function() { 
								aData = oTable.fnGetData(this); 
								jsonData.push({
									"customerId" : aData.customerId,
									"customerName" : aData.customerName 
								});
							}); 
					selectedCustomerList(jsonData,$('#customerRowId').val());
 					$('#common-popup').dialog('close');
				});
	});
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>customer
		</div>
		<div class="portlet-content">
			<div id="customer_common_json_list">
				<table id="customerMultiSelect" class="display">
					<thead>
						<tr>
							<th>Reference</th>
							<th>Customer</th>
							<th>Type</th>
						</tr>
					</thead>
				</table>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="customer-list-ok">ok</div>
				<div class="portlet-header ui-widget-header float-right"
					id="customer-common-popup">
					<fmt:message key="common.button.close" />
				</div>
			</div>
			<input type="hidden" id="pageInfo" value="${requestScope.pageInfo}" />
			<input type="hidden" id="customerRowId" value="${requestScope.rowId}" />
		</div>
	</div>
</div>