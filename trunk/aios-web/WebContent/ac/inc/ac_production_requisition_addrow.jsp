<tr class="rowid" id="fieldrow_${requestScope.rowId}">
	<td style="display: none;" id="lineId_${requestScope.rowId}">${requestScope.rowId}</td>
	<td><input type="hidden" name="productId" id="productid_${requestScope.rowId}" />
		<span id="product_${requestScope.rowId}" class="width60 float-left"></span> 
		<span class="width10 float-right" id="unitCode_${requestScope.rowId}" style="position: relative;"></span>
		<span class="button float-right">
			<a style="cursor: pointer;" id="prodID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all finished-product-popup width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td><input type="hidden" name="shelfId" id="shelfid_${requestScope.rowId}" /> <span
		id="shelf_${requestScope.rowId}"></span> <span class="button float-right"> <a
			style="cursor: pointer;" id="shelfID_${requestScope.rowId}"
			class="btn ui-state-default ui-corner-all show_store_list width100">
				<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
	<td>
		<select name="packageType" id="packageType_${requestScope.rowId}" class="packageType">
			<option value="">Select</option>
		</select> 
	</td> 
	<td><input type="text"
		class="width96 packageUnit validate[optional,custom[number]]"
		id="packageUnit_${requestScope.rowId}"  />
	</td>
	<td><input type="hidden" name="quantity" id="quantity_${requestScope.rowId}"
		class="quantity validate[optional,custom[number]] width80">
		<span id="baseDisplayQty_${requestScope.rowId}"></span>
	</td>
	<td><input type="text" name="description" id="description_${requestScope.rowId}"
		class="description width98"></td>
	<td style="width: 1%;" class="opn_td" id="option_${requestScope.rowId}"><a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
		id="AddImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Add Record"> <span class="ui-icon ui-icon-plus"></span> </a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
		id="EditImage_${requestScope.rowId}" style="display: none; cursor: pointer;"
		title="Edit Record"> <span class="ui-icon ui-icon-wrench"></span>
	</a> <a
		class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
		id="DeleteImage_${requestScope.rowId}" style="cursor: pointer; display: none;"
		title="Delete Record"> <span class="ui-icon ui-icon-circle-close"></span>
	</a> <a class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
		id="WorkingImage_${requestScope.rowId}" style="display: none;" title="Working"> <span
			class="processing"></span> </a> <input type="hidden"
		name="productionRequisitionDetailId"
		id="productionRequisitionDetailId_${requestScope.rowId}" /></td>
</tr>