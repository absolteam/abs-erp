<%@page import="com.aiotech.aios.common.util.AIOSCommons"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Fiscal Trial Balance</title>
<link href="${pageContext.request.contextPath}/css/report-print.css"
	rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
th {
	text-align: center;
	border-top: 1px solid #000;
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 3px double #000;
	font-size: 13px;
}

table {
	font-size: 14px;
	font: Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	margin: 5px 0 5px 0;
}

td {
	border: 1px solid #000;
}

.bottomTD {
	border-bottom: 3px double #000;
}

</style>
<body 
	style="margin-top: 15px; font-size: 12px;"> 
	<div class="width100">
		<div class="width98">
			<span class="heading"><span style="font-size: 25px !important;">${THIS.companyName}</span> </span>
		</div> 
	</div> 
	<div class="clear-fix"></div>
	<div class="width98">
		<span class="heading"><span style="font-size: 20px !important;">FISCAL TRIAL BALANCE REPORT</span> </span>
	</div> 
	<div class="clear-fix"></div>
	<div class="width100">
		<span class="side-heading" style="float: right; padding: 5px;">Printed
			on : ${requestScope.PRINT_DATE}</span>
	</div>
	<c:choose>
		<c:when
			test="${TRIAL_BALANCE_REPORT ne null && TRIAL_BALANCE_REPORT ne '' && fn:length(TRIAL_BALANCE_REPORT) >0 }">
			<div class="data-list-container width100 float-left"
					style="margin-top: 10px;">
				<fieldset style="background-color: #f5ecce;">
					<table class="width98" style="padding: 2px;">
						<tr>
							<th style="width:30%;">ACCOUNTS</th> 
							<th style="width:20%;">DEBIT</th>
							<th style="width:20%;">CREDIT</th>
							<th style="width:20%;">CLOSING BALANCE</th>
						</tr>
						<tbody>
							<c:forEach items="${TRIAL_BALANCE_REPORT}" var="trialBalance"
								varStatus="status">
								<c:forEach items="${trialBalance.debitCreditList}"
									var="TRIAL_BALANCE" varStatus="status">
									<tr>
										<td>${TRIAL_BALANCE.accountCode}</td> 
										<td style="text-align: right;">${TRIAL_BALANCE.viewDebitBalance}</td>
										<td style="text-align: right;">${TRIAL_BALANCE.viewCreditBalance}</td>
										<td style="text-align: right;">${TRIAL_BALANCE.viewClosingBalance}</td>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td style="font-weight: bold;">Total</td> 
									<td style="font-weight: bold; text-align: right;">${trialBalance.debitBalance}</td>
									<td style="font-weight: bold; text-align: right;">${trialBalance.creditBalance}</td>
									<td>&nbsp;</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</fieldset> 
			</div>
		</c:when>
		<c:otherwise>
			<div class="noRecordFound">No Record found.</div>
		</c:otherwise>
	</c:choose>
</body>
</html>
