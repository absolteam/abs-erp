<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript">
var accessCode = "";
$(function(){ 
	$jquery("#asset_warranty_claim").validationEngine('attach'); 

	$('#claimDate').datepick();

	 $('#warranty_claim_discard').click(function(event){  
		 warrantyClaimDiscard("");
		 return false;
	 });

	 $('.payoutnature-lookup').click(function(){
		 $('.ui-dialog-titlebar').remove(); 
		 accessCode=$(this).attr("id"); 
	        $.ajax({
	             type:"POST",
	             url:"<%=request.getContextPath()%>/add_lookup_detail.action",
	             data:{accessCode: accessCode},
	             async: false,
	             dataType: "html",
	             cache: false,
	             success:function(result){ 
	                  $('.common-result').html(result);
	                  $('#common-popup').dialog('open');
	  				   $($($('#common-popup').parent()).get(0)).css('top',0);
	                  return false;
	             },
	             error:function(result){
	                  $('.common-result').html(result);
	             }
	         });
	   return false;
	});	

	//Lookup Data Roload call
	 	$('#save-lookup').live('click',function(){  
	 		if(accessCode=="ASSET_WARRANTY_PAYOUT_NATURE"){
				$('#payoutNature').html("");
				$('#payoutNature').append("<option value=''>Select</option>");
				loadLookupList("payoutNature"); 
			}  
		});
		
	 if(Number($('#assetWarrantyClaimId').val())> 0){
		 $('#payoutNature').val("${ASSET_WARRANTY_CLAIM.lookupDetail.lookupDetailId}");
		 $('#claimStatus').val("${ASSET_WARRANTY_CLAIM.status}");
		 $('#paymentMode').val("${ASSET_WARRANTY_CLAIM.modeOfPayment}");
 	 }else
 		 $('#claimStatus').val(1);
		 

	 $('#warranty_claim_save').click(function(){   
		 if($jquery("#asset_warranty_claim").validationEngine('validate')){
				var assetWarrantyClaimId = Number($('#assetWarrantyClaimId').val());   
 				var assetWarrantyId = Number($('#assetWarrantyId').val());
				var payoutNature = Number($('#payoutNature').val());
				var claimAmount = Number($('#claimAmount').val()); 
		 		var claimExpense = Number($('#claimExpense').val());
		 		var payoutAmount = Number($('#payoutAmount').val());
		 		var paymentMode = Number($('#paymentMode').val());
		 		var status = Number($('#claimStatus').val()); 
		 		var claimDate = $('#claimDate').val();
		 		var description = $('#description').val(); 
		 		var referenceNumber = $('#referenceNumber').val(); 
  				$.ajax({
						type:"POST",
						url:"<%=request.getContextPath()%>/save_asset_warranty_claim.action", 
					 	async: false,
					 	data:{ 
								assetWarrantyClaimId : assetWarrantyClaimId,
								assetWarrantyId : assetWarrantyId,
								payoutNature : payoutNature,
								claimAmount : claimAmount,
								claimExpense : claimExpense,
								payoutAmount : payoutAmount,
								paymentMode : paymentMode,
								status : status,
								claimDate : claimDate,
								description : description,
								referenceNumber: referenceNumber
							},
											dataType : "json",
											cache : false,
											success : function(response) {
												if (response.returnMessage == "SUCCESS")
													warrantyClaimDiscard(assetWarrantyClaimId > 0 ? "Record updated successfully."
															: "Record created successfully.");
												else {
													$('#page-error')
															.hide()
															.html(
																	response.returnMessage)
															.slideDown(1000);
													return false;
												}
											}
										});
							} else {
								return false;
							}
							return false;
						});

		$('.common-popup').click(
						function() {
							$('.ui-dialog-titlebar').remove();
							$
									.ajax({
										type : "POST",
										url : "<%=request.getContextPath()%>/show_common_asset_warranty.action",
										async : false,
										dataType : "html",
										cache : false,
										success : function(result) {
											$('#common-popup').dialog('open');
											$(
													$(
															$('#common-popup')
																	.parent())
															.get(0)).css('top',
													0);
											$('.common-result').html(result);
										},
										error : function(result) {
											$('.common-result').html(result);
										}
									});
							return false;
						});

		$('#common-popup').dialog({
			autoOpen : false,
			minwidth : 'auto',
			width : 800,
			bgiframe : false,
			overflow : 'hidden',
			top : 0,
			modal : true
		}); 

		$('#commom-warranty-close')
		.live(
				'click',
				function() {
					$('#common-popup').dialog('close');
				});
	});
function warrantyClaimDiscard(message){ 
 	 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_asset_warranty_claim.action",
					async : false,
					dataType : "html",
					cache : false,
					success : function(result) {
						$('#common-popup').dialog('destroy');
						$('#common-popup').remove();
						$("#main-wrapper").html(result);
						if (message != null && message != '') {
							$('#success_message').hide().html(message)
									.slideDown(1000);
							$('#success_message').delay(2000).slideUp();
						}
					}
				});
	}
	function assetWarrantyPopupResult(assetWarrantyId, warrantyNumber) {
		$('#assetWarrantyId').val(assetWarrantyId);
		$('#warrantyNumber').val(warrantyNumber);
	}
	function loadLookupList(id){
		 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/load_lookup_detail.action",
					data : {
						accessCode : accessCode
					},
					async : false,
					dataType : "json",
					cache : false,
					success : function(response) {

						$(response.lookupDetails)
								.each(
										function(index) {
											$('#' + id)
													.append(
															'<option value='
							+ response.lookupDetails[index].lookupDetailId
							+ '>'
																	+ response.lookupDetails[index].displayName
																	+ '</option>');
										});
					}
				});
	}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			claim warranty
		</div> 
		<form name="asset_warranty_claim" id="asset_warranty_claim" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<div class="tempresult" style="display: none;"></div>
				<input type="hidden" id="assetWarrantyClaimId"
					name="assetWarrantyClaimId"
					value="${ASSET_WARRANTY_CLAIM.assetWarrantyClaimId}" />
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="claimStatus">Status<span
									class="mandatory">*</span> </label> <select name="claimStatus"
									id="claimStatus" class="width51 validate[required]">
									<option value="">Select</option>
									<c:forEach var="claimStatus" items="${CLAIM_STATUS}">
										<option value="${claimStatus.key}">${claimStatus.value}</option>
									</c:forEach>
								</select>
							</div>
							<div>
								<label class="width30" for="claimExpense">Claim Expense
								</label> <input type="text" id="claimExpense"
									class="width50 validate[optional,custom[number]]"
									value="${ASSET_WARRANTY_CLAIM.claimExpense}">
							</div>
							<div style="display: none;"> 
								<div>
									<label class="width30" for="payoutNature">PayOut Nature<span
										class="mandatory">*</span> </label> <select name="payoutNature"
										id="payoutNature" class="width51">
										<option value="">Select</option>
										<c:forEach var="payoutNature" items="${PAYOUT_NATURE}">
											<option value="${payoutNature.lookupDetailId}">${payoutNature.displayName}</option>
										</c:forEach>
									</select>
									<span class="button" style="position: relative;"> <a
										style="cursor: pointer;" id="ASSET_WARRANTY_PAYOUT_NATURE"
										class="btn ui-state-default ui-corner-all payoutnature-lookup width100">
											<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
								</div>
								<div>
									<label class="width30" for="paymentMode">Payment Mode </label> <select
										name="paymentMode" id="paymentMode" class="width51">
										<option value="">Select</option>
										<c:forEach var="paymentMode" items="${PAYMENT_MODE}">
											<option value="${paymentMode.key}">${paymentMode.value}</option>
										</c:forEach>
									</select>
								</div>
								<div>
									<label class="width30" for="payoutAmount">PayOut Amount</label>
									<input type="text" id="payoutAmount"
										class="width50 validate[optional,custom[number]]"
										value="${ASSET_WARRANTY_CLAIM.payoutAmount}">
								</div>
							</div>
							<div>
								<label class="width30" for="description">Description</label>
								<textarea rows="2" id="description" class="width50 float-left">${ASSET_WARRANTY_CLAIM.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset style="min-height: 115px">
							<div>
								<label class="width30" for="referenceNumber">Reference
									Number<span class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_WARRANTY_CLAIM.claimReference ne null && ASSET_WARRANTY_CLAIM.claimReference ne ''}">
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${ASSET_WARRANTY_CLAIM.claimReference}">
									</c:when>
									<c:otherwise>
										<input type="text" id="referenceNumber"
											class="width50 validate[required]" readonly="readonly"
											value="${requestScope.referenceNumber}">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="warrantyNumber">Warranty
									Number<span class="mandatory">*</span> </label> <input type="text"
									id="warrantyNumber" class="width50 validate[required]"
									readonly="readonly"
									value="${ASSET_WARRANTY_CLAIM.assetWarranty.warrantyNumber}">
								<span class="button"> <a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all common-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" id="assetWarrantyId"
									value="${ASSET_WARRANTY_CLAIM.assetWarranty.assetWarrantyId}">
							</div>
							<div>
								<label class="width30" for="claimDate">Claim Date<span
									class="mandatory">*</span> </label>
								<c:choose>
									<c:when
										test="${ASSET_WARRANTY_CLAIM.claimDate ne null && ASSET_WARRANTY_CLAIM.claimDate ne ''}">
										<c:set var="claimDate"
											value="${ASSET_WARRANTY_CLAIM.claimDate}" />
										<%
											String claimDate = DateFormat
															.convertDateToString(pageContext.getAttribute(
																	"claimDate").toString());
										%>
										<input type="text" id="claimDate"
											class="width50 validate[required]" value="<%=claimDate%>"
											readonly="readonly">
									</c:when>
									<c:otherwise>
										<input type="text" id="claimDate"
											class="width50 validate[required]" readonly="readonly">
									</c:otherwise>
								</c:choose>
							</div>
							<div>
								<label class="width30" for="claimAmount">Claim Amount <span
									class="mandatory">*</span> </label> <input type="text" id="claimAmount"
									class="width50 validate[required,custom[number]]"
									value="${ASSET_WARRANTY_CLAIM.claimAmount}">
							</div> 
						</fieldset>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right buttons">
				<div class="portlet-header ui-widget-header float-right"
					id="warranty_claim_discard">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
				<div class="portlet-header ui-widget-header float-right"
					id="warranty_claim_save">
					<fmt:message key="accounts.common.button.save" />
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 60px !important; left: -228px !important;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
