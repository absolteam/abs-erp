<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style> 
#DOMWindow{	
	width:40%!important;
	height:40%!important;
}  
</style>
<script type="text/javascript">
var slidetab = ""; 
var tableRowId = 0;
var tempRowId = 0;
var workinProcessDetails = [];
var workinProcessProductions = [];
$(function(){
	
	$jquery("#workinProcessValidation").validationEngine('attach');  
	 
	 $('#workinprocess_discard').click(function(event){  
		 workinProcessDiscard("");
		 return false;
	 }); 
	 
	 $('.callJq').openDOMWindow({  
		eventType:'click', 
		loader:1,  
		loaderImagePath:'animationProcessing.gif', 
		windowSourceID:'#temp-result', 
		loaderHeight:16, 
		loaderWidth:17 
	}); 
	 
	 $('.workinprocess_altersave').click(function(){
		 var processQuantity = Number(0);
		 var alterFinishedGoods = getRowId($(this).attr('id'));
		 var finishedProductId = Number($('#finishedProductId').val());
		 var rowId = Number(0);
		 tempRowId = 0;
		 if(finishedProductId > 0){
			 processQuantity = Number($('#alterProcessQuantity').val()); 
			 rowId = 1;
		 } else{  
			 rowId = Number(0);
			 $('.processRequisitionMode').each(function(){ 
				 var mode = $(this).attr('checked');
				 if(mode == true){
					rowId = getRowId($(this).attr('id')); 
				 	return false;
				 }
			 });
			 if(rowId > 0){
				 tempRowId = rowId;
				 processQuantity = Number($('#processQuantity_'+rowId).val());
			 } 
		 } 
		 if(rowId > 0){
			 $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_workinprocess_alterinventory.action", 
			 	async: false,  
			 	data :{processQuantity: processQuantity, alterFinishedGoods: alterFinishedGoods},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					 $('#temp-result').html(result); 
					 $('.callJq').trigger('click');   
					 $('.storeDetail').each(function(){ 
						 var rowid = getRowId($(this).attr('id'));
						 $(this).val($('#tempstoreDetailid_'+rowid).val());
					 }); 
				} 
			 });  
		 } else{
			 alert("Please select one finished product to alter.");
		 }
		 return false;
	 });
	 
	 $('.workinprocess_alteredsave').live('click', function(){ 
		 if($jquery("#workinProcessValidation").validationEngine('validate')){    
			 if(tempRowId > 0){
				 $('#processQuantity_'+tempRowId).val($('#processQuantity').val());
			 } 
			 var workinProcessStatus = getRowId($(this).attr('id'));
			 saveWorkInProcess(workinProcessStatus); 
			 return false;
		 }else{return false;} 
	 });
	 
	 $('.workinprocess_save').click(function(){
		 var idName = $(this).attr('id');
		 workinProcessDetails = [];
  		 if($jquery("#workinProcessValidation").validationEngine('validate')){
			var workinProcessStatus = getRowId($(this).attr('id'));
		    var cnfrmMessage = "";
		    if($("#"+idName).hasClass('issuegoods')){
		    	cnfrmMessage = "Save & Issue Items";
		    }else if($("#"+idName).hasClass('addfinishedgoods')){
		    	cnfrmMessage = "Save & Add Finished Goods";
		    }
		    var cnfrm = confirm(cnfrmMessage);
			if(!cnfrm)
				return false;
			saveWorkInProcess(workinProcessStatus); 
 		 }else{
 			return false;
 		 }
 	 });   
	 
	 $('.workinprocess-productionreq-popup').click(function(){  
			$('.ui-dialog-titlebar').remove();   
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_workinprocess_productionreqpopup.action", 
			 	async: false,   
			    dataType: "html", 
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
					 return false;
				},
				error:function(result){   
					 $('.common-result').html(result); 
				}
			});  
			return false;
		 });
	 
	 $('.workinprocess-product-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var id = Number(0);
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_workinprocess_product_popup.action", 
		 	async: false,   
		    dataType: "html",
		    data:{id: id},
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	 });
	 
	 $('.workinprocessdetail-store-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		tableRowId = getRowId($(this).attr('id'));
		var productId = Number($('#productid_'+tableRowId).val());
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_product_stock_common_popup.action", 
		 	async: false,  
		 	data: { rowId: tableRowId, productId: productId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0); 
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	 });
	 
	  $('.workinprocess-store-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
		var rowId = Number(0);
 		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_store_session_common.action", 
		 	async: false,   
		 	data: {rowId : rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $( $(
							$('#common-popup')
									.parent())
							.get(0)).css('top',
					0);
				 return false;
			} 
		});  
		return false;
	 });
	  
	  $('.workinprocess-store-popup-production').live('click',function(){  
			$('.ui-dialog-titlebar').remove();   
			var rowId = getRowId($(this).attr('id'));
	 		$.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/show_store_session_commonnew.action", 
			 	async: false,   
			 	data: {rowId : rowId},
			    dataType: "html",
			    cache: false,
				success:function(result){  
					 $('.common-result').html(result);  
					 $('#common-popup').dialog('open'); 
					 $( $(
								$('#common-popup')
										.parent())
								.get(0)).css('top',
						0);
					 return false;
				} 
			});  
			return false;
		 });
	  
	  $('.rackid').live(
				'dblclick',
				function() {
					var currentId = $(this);
					var tempvar = $(currentId).attr('id');
					var idarray = tempvar.split('_');
					var rowid = Number(idarray[1]); 
					$('#shelfId').val($('#rackid_' + rowid).html()); 
					$('#shelfNameWork').val($('#rackstorename_' + rowid).html());  
					$('#common-popup').dialog('close');
					return false;
		}); 
	 
	  $('.packageType').live('change',function(){
			var rowId=getRowId($(this).attr('id')); 
			$('#issueQuantity_'+rowId).val('');
			var packageUnit = Number($('#packageUnit_'+rowId).val());
			var packageType = Number($('#packageType_'+rowId).val());  
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			} else if(packageUnit > 0 && packageType == -1){
				$('#issueQuantity_'+rowId).val(packageUnit); 
 			} 
			return false;
		});
		
		$('.packageUnit').live('change',function(){
			var rowId=getRowId($(this).attr('id'));
			$('#baseUnitConversion_'+rowId).text('');
			$('#issueQuantity_'+rowId).val('');
			var packageUnit = Number($(this).val()); 
			var packageType = Number($('#packageType_'+rowId).val()); 
			if(packageType == -1 && packageUnit > 0){  
		 	 	$('#issueQuantity_'+rowId).val(packageUnit);
			}
			if(packageUnit > 0 && packageType > 0){ 
				getProductBaseUnit(rowId);
			}
 			return false;
		}); 
		
	 $('.issueQuantity').live('change',function(){
 		 var rowId = getRowId($(this).attr('id'));
		 var issueQuantity = Number($('#issueQuantity_'+rowId).val());
		 if(issueQuantity > 0){
 			 var packageType = Number($('#packageType_'+rowId).val()); 
 			 if(packageType == -1 && issueQuantity > 0){  
			 	 $('#packageUnit_'+rowId).val(issueQuantity);
			 }else{
				 getProductConversionUnit(rowId);
			 } 
		 }  
		 return false;
	 });
	 
	 $('#popup_discard').live('click', function(){
		 $('#DOMWindow').remove();
		 $('#DOMWindowOverlay').remove();
	 }); 
	 
	 $('#expectedQuantity').change(function(){
		 var expectedQuantity = Number($(this).val());
		 $('.rowid').each(function(){
			 var rowId = getRowId($(this).attr('id'));
			 var issueQuantity = Number($('#tempissueQuantity_'+rowId).val());
			 var packageUnit = Number($('#temppackageUnit_'+rowId).val());
			 if(issueQuantity > 0){
				 issueQuantity = Number(issueQuantity * expectedQuantity);
				 packageUnit = Number(packageUnit * expectedQuantity);
				 $('#issueQuantity_'+rowId).val(issueQuantity.toFixed(2));
				 $('#packageUnit_'+rowId).val(packageUnit.toFixed(2));
				 $('#baseDisplayQty_'+rowId).html(issueQuantity.toFixed(2));
			 }
		 });
	 });
	 
	 $('#processQuantity').change(function(){
		 var processQuantity = Number($(this).val());
		 if(processQuantity > 0){
			 $('#issuegoods_1').removeClass('workinprocess_save');
			 $('.packageType,.packageUnit,.typicalWastage').attr('disabled', true); 
			 $('#issuegoods_1').removeClass('issuegoods'); 
			 $('#issuegoods_1').css('opacity',0.5);
		 } 
	 });
	 
	 $('.processfQuantity').live('change',function(){
		 var processQuantity = Number($(this).val());
		 var rowId1 = getRowId($(this).attr('id')); 
 		 $('.rowid'+rowId1).each(function(){
			 var rowId = getRowId($(this).attr('id')); 
			 var issueQuantity = Number($('#actualQuantity_'+rowId).val()); 
			 if(issueQuantity > 0){
				 issueQuantity = Number(issueQuantity * processQuantity);
				 $('#issueQuantity_'+rowId).val(issueQuantity.toFixed(2));
			 }
		 });
		 return false;
	 });
	 
	 if(Number('${WORKIN_PROCESS.workinProcessId}')>0){
		 $('#processDate').datepick(); 
		 $('.workinprocess-productionreq-popup').remove();
		 $('.workinprocess-product-popup').remove();
		 $('.rowid').each(function(){
			var rowId = getRowId($(this).attr('id'));  
			$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
		 }); 
		 $('.rowid1').each(function(){
			var rowId = getRowId($(this).attr('id'));  
			$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
		 });
	 }else{
		 $('#processDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
	 }
	 
	 $('#common-popup').dialog({
		autoOpen : false,
		minwidth : 'auto',
		width : 800,
		bgiframe : false,
		overflow : 'hidden',
		top : 0,
		modal : true
	});  
});
function getProductBaseUnit(rowId){
	$('#baseUnitConversion_'+rowId).text('');
	var packageQuantity = Number($('#packageUnit_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	var basePrice = Number($('#baseSellingPrice_' + rowId).val());   
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_baseconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, basePrice: basePrice, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#issueQuantity_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
			$('#baseUnitConversion_'+rowId).text(response.productPackagingDetailVO.conversionUnitName);
			$('#baseDisplayQty_'+rowId).html(response.productPackagingDetailVO.conversionQuantity);
 		} 
	}); 
	return false;
}
function getProductConversionUnit(rowId){
	var packageQuantity = Number($('#issueQuantity_'+rowId).val());
	var productPackageDetailId = Number($('#packageType_'+rowId).val());  
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_packageconversion_productunit.action", 
	 	async: false,  
	 	data:{packageQuantity: packageQuantity, productPackageDetailId: productPackageDetailId},
	    dataType: "json",
	    cache: false,
		success:function(response){ 
			$('#packageUnit_'+rowId).val(response.productPackagingDetailVO.conversionQuantity); 
		} 
	});   
	return false;
}
function saveWorkInProcess(workinProcessStatus){ 
	var referenceNumber = $('#referenceNumber').val(); 
	var productId = Number($('#finishedProductId').val()); 
	var shelfId =  Number($('#shelfId').val()); 
	var personId = Number($('#personId').val());   
	var processQuantity = Number($('#processQuantity').val());   
	var workinProcessId = Number($('#workinProcessId').val());   
	var description=$('#description').val();  
	var processDate = $('#processDate').val();  
	var alertComment = $('#comment').val(); 
	if(typeof alertComment == 'undefined')
		alertComment = '';
	var productionRequisitionId = Number($('#productionRequisitionId').val());
	workinProcessProductions = getWorkinProcessProduction();  
	if(workinProcessProductions!=null && workinProcessProductions!=""){
		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/workinprocess_save.action", 
		 	async: false, 
		 	data:{	workinProcessId: workinProcessId, referenceNumber: referenceNumber, productId: productId, shelfId: shelfId,
		 			processQuantity: processQuantity, workinProcessId: workinProcessId, processDate: processDate, description: description,
		 			workinProcessStatus: workinProcessStatus, personId: personId, productionRequisitionId: productionRequisitionId,
		 			alertComment: alertComment, workinProcessProductions: JSON.stringify(workinProcessProductions)
			 	 },
		    dataType: "json",
		    cache: false,
			success:function(response){   
				 if(($.trim(response.returnMessage)=="SUCCESS")) 
					workInProcessReload(workinProcessId > 0 ? "Record updated.":"Record created.", response.workinProcessId);
				 else{
					 $('#page-error').hide().html(response.returnMessage).slideDown();
					 $('#page-error').delay(2000).slideUp();
					 return false;
				 }
			} 
		}); 
	}else{
		$('#page-error').hide().html("Please enter material mix details.").slideDown();
		$('#page-error').delay(2000).slideUp();
		return false;
	}
}

function getWorkinProcessProduction(){
	workinProcessProductions = [];
	var finishedProductId = Number($('#finishedProductId').val());
	if(finishedProductId > 0){
		var shelfNameWork = Number($('#shelfId').val());  
		var processQuantity = Number($('#processQuantity').val());
		var workinProcessProductionId = Number($('#workinProcessProductionId').val());
		var expectedQuantity = Number($('#expectedQuantity').val());
		var productionRequisitionDetailId = Number(0);
		var jsonData = [];
		var workinProcessDetails = []; 
		$('.rowid').each(function(){ 
			var rowId = getRowId($(this).attr('id'));  
			var productId = Number($('#productid_'+rowId).val());  
			var shelfRowId = Number($('#shelfId_'+rowId).val());  
			var issueQuantity = Number($('#issueQuantity_'+rowId).val()); 
			var description = $('#description_'+rowId).val();  
			var workinProcessDetailId =  Number($('#workinProcessDetailId_'+rowId).val()); 
			var wastageQuantity =  Number($('#typicalWastage_'+rowId).val()); 
			var packageType =  Number($('#packageType_'+rowId).val()); 
			var packageUnit =  Number($('#packageUnit_'+rowId).val()); 
			if(typeof productId != 'undefined' && productId > 0 && issueQuantity > 0 && shelfRowId > 0){ 
				workinProcessDetails.push({
					"productId" : productId,
					"shelfId" : shelfRowId,
					"issueQuantity" : issueQuantity, 
					"wastageQuantity" : wastageQuantity,
					"description" : description,
					"packageType" : packageType,
					"packageUnit" : packageUnit,
					"workinProcessDetailId": workinProcessDetailId 
				}); 
			} 
		}); 
		if(workinProcessDetails != null  && workinProcessDetails.length > 0){
			jsonData.push({
				"finishedProductId" : finishedProductId,
				"shelfId" : shelfNameWork,
				"processQuantity" : processQuantity,
				"expectedQuantity" : expectedQuantity,
				"workinProcessProductionId" : workinProcessProductionId,
				"productionRequisitionDetailId": productionRequisitionDetailId, 
				"workinProcessDetails" : workinProcessDetails
			}); 
			workinProcessProductions.push({
				"workinProcessProductions" : jsonData
			});
		} 
	} else {
		$('.processRequisitionMode').each(function(){ 
			var mode = $(this).attr('checked');
			if(mode == true){
				var rowId = getRowId($(this).attr('id'));  
				var finishedProductId = Number($('#finishedProductId_'+rowId).val());
				var shelfNameWork = Number($('#shelfId_'+rowId).val());  
				var processQuantity = Number($('#processQuantity_'+rowId).val());
				var expectedQuantity = Number($('#expectedQuantity_'+rowId).val());
				var workinProcessProductionId = Number($('#workinProcessProductionId_'+rowId).val());
				var productionRequisitionDetailId = Number($('#productionRequisitionDetailId_'+rowId).val());
				var jsonData = [];
				var workinProcessDetails = []; 
				$('.rowid'+rowId).each(function(){  
					var rowId1 = getRowId($(this).attr('id'));  
					var productId = Number($('#productid_'+rowId1).val());  
					var shelfId = Number($('#shelfId_'+rowId1).val());  
					var issueQuantity = Number($('#issueQuantity_'+rowId1).val()); 
					var packageType =  Number($('#packageType_'+rowId1).val()); 
					var packageUnit =  Number($('#packageUnit_'+rowId1).val()); 
					var wastageQuantity =  Number($('#typicalWastage_'+rowId1).val()); 
					var description = $('#description_'+rowId1).val();  
					var workinProcessDetailId =  Number($('#workinProcessDetailId_'+rowId1).val()); 
					if(typeof productId != 'undefined' && productId > 0 && issueQuantity > 0 && shelfId > 0){ 
						workinProcessDetails.push({
							"productId" : productId,
							"shelfId" : shelfId,
							"issueQuantity" : issueQuantity, 
							"wastageQuantity" : wastageQuantity,
							"description" : description, 
							"packageType" : packageType,
							"packageUnit" : packageUnit,
							"workinProcessDetailId": workinProcessDetailId 
						}); 
					} 
				}); 
				jsonData.push({
					"finishedProductId" : finishedProductId,
					"shelfId" : shelfNameWork,
					"processQuantity" : processQuantity,
					"expectedQuantity" : expectedQuantity,
					"workinProcessProductionId" : workinProcessProductionId,
					"productionRequisitionDetailId": productionRequisitionDetailId,
					"workinProcessDetails" : workinProcessDetails
				});
				
				workinProcessProductions.push({
					"workinProcessProductions" : jsonData
				});
			}
		});
	}
	return workinProcessProductions;
}

function getWorkinProcessDetails(){
	workinProcessDetails = []; 
	$('.rowid').each(function(){ 
		var rowId = getRowId($(this).attr('id'));  
		var productId = Number($('#productid_'+rowId).val());  
		var shelfId = Number($('#shelfId_'+rowId).val());  
		var issueQuantity = Number($('#issueQuantity_'+rowId).val()); 
		var description = $('#description_'+rowId).val();  
		var workinProcessDetailId =  Number($('#workinProcessDetailId_'+rowId).val()); 
		var packageType =  Number($('#packageType_'+rowId1).val()); 
		var packageUnit =  Number($('#packageUnit_'+rowId1).val()); 
		var wastageQuantity =  Number($('#typicalWastage_'+rowId1).val()); 
		if(typeof productId != 'undefined' && productId > 0 && issueQuantity > 0 && shelfId > 0){
			var jsonData = [];
			jsonData.push({
				"productId" : productId,
				"shelfId" : shelfId,
				"issueQuantity" : issueQuantity, 
				"wastageQuantity" : wastageQuantity,
				"description" : description, 
				"packageType" : packageType,
				"packageUnit" : packageUnit,
				"workinProcessDetailId": workinProcessDetailId
			});   
			workinProcessDetails.push({
				"workinProcessDetails" : jsonData
			});
		} 
	});  
	return workinProcessDetails;
}
function commonProductPopup(aData, rowId) {  
	$('#storeNameWork_' + rowId).html(aData.storeName);
	$('#shelfId_' + rowId).val(aData.shelfId);  
	$('#product-stock-popup').dialog('close');
	$('#common-popup').dialog('close');
	return false;
}
function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function productionRequisitionPopup(aaData){
	$('#productionRequisition').val(aaData.referenceNumber);
	$('#productionRequisitionId').val(aaData.productionRequisitionId); 
	$('.workinprocess-product-popup').remove();
	$('.finishedProductdiv,.shelfdiv').hide();
	$('#shelfNameWork').removeClass('validate[required]');
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_productionrequisitiondetail_process.action",
		async : false,
		dataType : "html",
		data: {productionRequisitionId: aaData.productionRequisitionId},
		cache : false,
		success : function(result) {
			$('.issuegoods').css('opacity', 1);
			$('.mix-product-detail').html(result);  
		}
	}); 
	return false;
}
function finishedProductPopup(aaData, rowId){
	$('.workinprocess-productionreq-popup').remove();
	$('.shelfdiv').show();
	$('.exceptedQuantityDiv').show();
	$('#finishedProduct').val(aaData.code+" -- "+aaData.productName);
	$('#finishedProductId').val(aaData.productId);  
	$('#shelfNameWork').addClass('validate[required]');
	if(aaData.storeId != null && Number(aaData.shelfId)>0){
		$('#shelfId').val(aaData.shelfId); 
		$('#shelfNameWork').val(aaData.storeName); 
	}else{
		$('#shelfNameWork').val(''); 
		$('#shelfId').val(''); 
	}
	$.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_materialmix_detail.action",
		async : false,
		dataType : "html",
		data: {craftProductId: aaData.productId},
		cache : false,
		success : function(result) {
			$('.mix-product-detail').html(result);  
			var expectedQuantity = Number($('#expectedQuantity').val());
			var mixCount = Number($('#mixCount').val()); 
			if(mixCount > 0){
				$('.issuegoods').css('opacity', 1);
				if(expectedQuantity > 0){
					$('.rowid').each(function(){
						 var rowId = getRowId($(this).attr('id'));
						 var issueQuantity = Number($('#tempissueQuantity_'+rowId).val());
						 if(issueQuantity > 0){
							 issueQuantity = Number(issueQuantity * expectedQuantity);
							 $('#issueQuantity_'+rowId).val(issueQuantity.toFixed(2));
						 }
					 });
				}  
			} 
		}
	});
	
	return false;
}
function workinProcessDiscard(message){ 
 $.ajax({
		type:"POST",
		url:"<%=request.getContextPath()%>/show_workinprocess_list.action",
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
function workInProcessReload(message, workinProcessId){
	$.ajax({
		type: "POST", 
		url: "<%=request.getContextPath()%>/workinprocess_entry.action", 
     	async: false, 
     	data:{workinProcessId: workinProcessId},
		dataType: "html",
		cache: false,
		success: function(result){ 
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$('#DOMWindow').remove();
			$('#DOMWindowOverlay').remove();
			$("#main-wrapper").html(result); 
			$('.workinprocess-productionreq-popup').remove();
			$('.workinprocess-product-popup').remove();
			if (message != null && message != '') {
				$('#success_pagemessage').hide().html(message).slideDown(1000);
				$('#success_pagemessage').delay(2000).slideUp();
			}
		} 		
	}); 
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> workin process
		</div> 
		<form name="workinProcessValidation" id="workinProcessValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div id="success_pagemessage" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
 				<input type="hidden" id="workinProcessId" value="${WORKIN_PROCESS.workinProcessId}"/>
				<input type="hidden" id="workinProcessStatus" value="${WORKIN_PROCESS.status}"/>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 130px;"> 
							<c:set var="display" value="none"/>
							<c:if test="${WORKIN_PROCESS.productionRequisition eq null 
								&& WORKIN_PROCESS.workinProcessId ne null}">
								<c:set var="display" value="show"/>
							</c:if>
							<div class="exceptedQuantityDiv" style="display:${display};">
								<label class="width30" for="processQuantity">Expected Quantity</label>
								<c:if test="${WORKIN_PROCESS.productionRequisition eq null}"> 
									<c:choose>
										<c:when test="${WORKIN_PROCESS.workinProcessId ne null && 
												WORKIN_PROCESS.status == requestScope.alterFinishedGoods
												|| WORKIN_PROCESS.status == requestScope.addFinishedGoods}">
											<input type="text" name="expectedQuantity" id="expectedQuantity" readonly="readonly"
												value="${WORKIN_PROCESS_PRODUCTION.expectedQuantity}" class="width50 validate[optional,custom[number]]" /> 
										</c:when>
										<c:otherwise>
											<input type="text" id="expectedQuantity" name="expectedQuantity"
												value="${WORKIN_PROCESS_PRODUCTION.expectedQuantity}" class="width50 validate[optional,custom[number]]" />
										</c:otherwise>
									</c:choose> 
								</c:if>
							</div>  
							<div class="processQuantityDiv" style="display:${display};">
								<label class="width30" for="processQuantity">Actual Quantity</label>
								<c:if test="${WORKIN_PROCESS.productionRequisition eq null}"> 
									<input type="text" name="processQuantity" id="processQuantity" value="${WORKIN_PROCESS_PRODUCTION.productionQuantity}" 
										class="width50 validate[optional,custom[number]]" /> 
								</c:if>
							</div> 
							<div>
								<label class="width30"> Person  </label> 
								<c:choose>
									<c:when
									test="${WORKIN_PROCESS.person ne null && WORKIN_PROCESS.person ne ''}"> 
										<input type="text" readonly="readonly" id="personName" name="personName" 	
											value="${WORKIN_PROCESS.person.firstName} ${WORKIN_PROCESS.person.lastName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${WORKIN_PROCESS.person.personId}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="personName" name="personName"  
											value="${requestScope.personName}" class="validate[required] width50" />
										<input type="hidden" id="personId" value="${requestScope.personId}"/>
									</c:otherwise>
								</c:choose>  
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									class="width51">${WORKIN_PROCESS.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 130px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${WORKIN_PROCESS.referenceNumber ne null && WORKIN_PROCESS.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${WORKIN_PROCESS.referenceNumber}" class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div>  
							<div>
								<label class="width30" for="processDate">Process Date<span
									style="color: red;">*</span></label>
								<input type="text" readonly="readonly" id="processDate" name="processDate"
									value="${WORKIN_PROCESS.date}" class="width50" />
							</div>
							<c:set var="display1" value="show"/>
							<c:if test="${WORKIN_PROCESS_PRODUCTION.product ne null}">
								<c:set var="display1" value="none"/>
							</c:if>
							<div class="productionRequisitiondiv" style="display:${display1};">
								<label class="width30"> Production Requisition </label>  
								<input type="text" readonly="readonly" id="productionRequisition" name="productionRequisition"  
									value="${WORKIN_PROCESS.productionRequisition.referenceNumber}" class="width50" />
								<input type="hidden" id="productionRequisitionId" value="${WORKIN_PROCESS.productionRequisition.productionRequisitionId}"/>
								<span class="button">
									<a style="cursor: pointer;"
									class="btn ui-state-default ui-corner-all workinprocess-productionreq-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> 
							</div> 
							<c:choose>
								<c:when test="${WORKIN_PROCESS.productionRequisition eq null}"> 
									<div class="finishedProductdiv">
										<label class="width30"> Finished Product </label>  
										<input type="text" readonly="readonly" id="finishedProduct" name="finishedProduct"  
											value="${WORKIN_PROCESS_PRODUCTION.product.productName}" class="width50" />
										<input type="hidden" id="finishedProductId" value="${WORKIN_PROCESS_PRODUCTION.product.productId}"/>
										<input type="hidden" id="workinProcessProductionId" value="${WORKIN_PROCESS_PRODUCTION.workinProcessProductionId}"/>
	 									<span class="button" style="display:${display1};">
											<a style="cursor: pointer;" id="person"
											class="btn ui-state-default ui-corner-all workinprocess-product-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</div> 
									<div style="display:${display};" class="shelfdiv">
										<label class="width30">Destination Shelf<span
											style="color: red;">*</span> </label>  
										<input type="text" readonly="readonly" id="shelfNameWork" name="shelfNameWork"  
											value="${WORKIN_PROCESS_PRODUCTION.storeName}" class="width50" />
										<input type="hidden" id="shelfId" value="${WORKIN_PROCESS_PRODUCTION.shelf.shelfId}"/>
										<span class="button">
											<a style="cursor: pointer;" id="person"
											class="btn ui-state-default ui-corner-all workinprocess-store-popup width100">
												<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
									</div> 
								</c:when> 
								<c:otherwise>
									<input type="hidden" id="finishedProductId" value="0"/>
									<input type="hidden" id="shelfId"  value="0"/>
								</c:otherwise>
							</c:choose> 
						</fieldset>
					</div>
				</div>
				<span class="callJq"></span>
				<div class="clearfix"></div>
				<div class="portlet-content class90 mix-product-detail" id="hrm"
					style="margin-top: 10px;">
					<c:choose>
						<c:when test="${WORKIN_PROCESS.productionRequisition eq null}">
							<c:if test="${WORKIN_PROCESS_PRODUCTION.workinProcessDetailVOs ne null 
								&& fn:length(WORKIN_PROCESS_PRODUCTION.workinProcessDetailVOs) > 0}">
								<fieldset>
									<legend>
										Workin Process Detail<span class="mandatory">*</span>
									</legend>
									<div id="line-error"
										class="response-msg error ui-corner-all width90"
										style="display: none;"></div>
									<div id="hrm" class="hastable width100">
										<table id="hastab" class="width100">
											<thead>
												<tr>
													<th class="width10">Product</th>
													<th class="width10">Store</th>
													<th style="width: 8%;">Packaging</th> 
													<th class="width5">Quantity</th>
													<th class="width5">Base Qty</th>
													<th class="width5">Wastage Qty</th>
			 										<th class="width10">Description</th>   
												</tr>
											</thead>
											<tbody class="tab">
												<c:forEach var="DETAIL"
													items="${WORKIN_PROCESS_PRODUCTION.workinProcessDetailVOs}"
													varStatus="status">
													<tr class="rowid" id="fieldrow_${status.index+1}">
														<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
														<td><input type="hidden" name="productId"
															id="productid_${status.index+1}"
															value="${DETAIL.product.productId}" /> <span class="width60 float-left"
															id="product_${status.index+1}">${DETAIL.product.productName} [${DETAIL.product.code}]</span>
															<span class="width10 float-left" id="unitCode_${status.index+1}"
																style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
														</td> 
														<td>
															<span class="float-left width60" id="storeNameWork_${status.index+1}">
																${DETAIL.storeName}
															</span>
		 													<input type="hidden" name="shelfId" id="shelfId_${status.index+1}" value="${DETAIL.shelf.shelfId}"/> 
															<c:if test="${WORKIN_PROCESS.status eq null || WORKIN_PROCESS.status == requestScope.issueStatus}">
																<span
																	class="button float-right"> <a
																	style="cursor: pointer;" id="prodIDStr_${status.index+1}"
																	class="btn ui-state-default ui-corner-all workinprocessdetail-store-popup width100">
																	<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															</c:if> 
														</td>
														<td>
															<c:choose>
																<c:when test="${WORKIN_PROCESS.status eq null || WORKIN_PROCESS.status == requestScope.issueStatus}">
																	<select name="packageType" id="packageType_${status.index+1}" class="packageType">
																		<option value="-1">Select</option>
																		<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
																			<c:choose>
																				<c:when test="${packageType.productPackageId gt 0}">
																					<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																					 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																					 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																					 	</c:forEach> 
																					 </optgroup>
																				</c:when> 
																			</c:choose> 
																		</c:forEach>
																	</select> 
																</c:when> 
																<c:otherwise>
																	${DETAIL.conversionUnitName}
																	<input type="hidden" name="packageType" id="packageType_${status.index+1}" class="packageType"/>
																</c:otherwise>
															</c:choose> 
															<input type="hidden" id="temppackageType_${status.index+1}" value="${DETAIL.packageDetailId}"/>
														</td>
														<td>
															<c:choose>
																<c:when test="${WORKIN_PROCESS.status eq null || WORKIN_PROCESS.status == requestScope.issueStatus}">
																	<input type="text" class="width96 packageUnit validate[optional,custom[number]]"
																		id="packageUnit_${status.index+1}" value="${DETAIL.packageUnit}" /> 
																</c:when> 
																<c:otherwise>
																	<span>${DETAIL.packageUnit}</span>
																	<input type="hidden" name="packageUnit" id="packageUnit_${status.index+1}" 
																		value="${DETAIL.packageUnit}" class="packageUnit"/>
																</c:otherwise>
															</c:choose>
														</td> 
														<td> 
															<c:choose>
																<c:when test="${WORKIN_PROCESS.status eq null || WORKIN_PROCESS.status == requestScope.issueStatus}">
 																	<input type="hidden" name="issueQuantity"
																		id="issueQuantity_${status.index+1}" value="${DETAIL.baseQuantity}"
																		class="issueQuantity validate[optional,custom[number]] width80">
																</c:when>
																<c:otherwise>
 																	<input type="hidden" name="issueQuantity"
																		id="issueQuantity_${status.index+1}" value="${DETAIL.baseQuantity}"
																		class="issueQuantity validate[optional,custom[number]] width80">
																</c:otherwise>
															</c:choose> 
															<span id="baseUnitConversion_${status.index+1}" 
																style="display: none;" class="width10">${DETAIL.baseUnitName}</span>
															<span id="baseDisplayQty_${status.index+1}">${DETAIL.baseQuantity}</span>
														</td> 
														<td>
															<c:choose>
																<c:when test="${WORKIN_PROCESS.status eq null || WORKIN_PROCESS.status == requestScope.issueStatus}">
 																	<input type="text" name="typicalWastage"
																		id="typicalWastage_${status.index+1}" value="${DETAIL.wastageQuantityStr}"
																		class="typicalWastage validate[optional,custom[number]] width80">
																</c:when>
																<c:otherwise>
																	<span>${DETAIL.wastageQuantityStr}</span>
 																	<input type="hidden" name="typicalWastage"
																		id="typicalWastage_${status.index+1}" value="${DETAIL.wastageQuantityStr}"
																		class="typicalWastage validate[optional,custom[number]] width80">
																</c:otherwise>
															</c:choose>  
														</td> 
														<td><input type="text" name="description"
															id="description_${status.index+1}" value="${DETAIL.description}"
															class="description  width80">
															<input
															type="hidden" name="workinProcessDetailId"
															id="workinProcessDetailId_${status.index+1}"
															value="${DETAIL.workinProcessDetailId}" />
														</td>   
													</tr>
												</c:forEach> 
											</tbody>
										</table>
									</div>
								</fieldset>
							</c:if> 
						</c:when>
						<c:otherwise>
							<span>Workin Process Detail</span>
							<c:forEach var="wkdetail" items="${WORKIN_PROCESS.workinProcessProductionVOs}" varStatus="status3">
								<fieldset>
									<c:choose>
										<c:when test="${WORKIN_PROCESS.status == requestScope.addFinishedGoods || WORKIN_PROCESS.status == requestScope.alterFinishedGoods}">
											<input type="checkbox" id="processRequisition_${status3.index+1}" class="processRequisitionMode"/>
										</c:when>
										<c:otherwise>
											<input type="checkbox" id="processRequisition_${status3.index+1}" checked="checked" class="processRequisitionMode"/>
										 </c:otherwise>
									</c:choose> 
									<div class="width100 float-left" id="hrm">
										<div class="float-left width48">
											<div>
												<label class="width30"> Finished Product<span
													style="color: red;">*</span> </label> <input type="text"
													readonly="readonly" id="finishedProduct_${status3.index+1}" name="finishedProduct"
													value="${wkdetail.product.productName}" class="width50" /> <input
													type="hidden" id="finishedProductId_${status3.index+1}"
													value="${wkdetail.product.productId}" />
													<input type="hidden" id="workinProcessProductionId_${status3.index+1}" value="${wkdetail.workinProcessProductionId}"/>
													<input type="hidden" id="productionRequisitionDetailId_${status3.index+1}" 
														value="${wkdetail.productionRequisitionDetail.productionRequisitionDetailId}"/>
											</div>
											<div>
												<label class="width30"> Shelf<span style="color: red;">*</span>
												</label> <input type="text" readonly="readonly" id="shelfNameWork_${status3.index+1}"
													name="shelfNameWork" value="${wkdetail.storeName}"
													class="width50" /> <input type="hidden" id="shelfId_${status3.index+1}"
													value="${wkdetail.shelf.shelfId}" /> 
											</div>
											 <div>
												<label class="width30" for="expectedQuantity_${status3.index+1}">Expected Quantity</label> <input
													type="text" id="expectedQuantity_${status3.index+1}" name="expectedQuantity"
													value="${wkdetail.expectedQuantity}"
													class="width50 expectedQuantity validate[optional,custom[number]]" />
											</div>
											<div>
												<label class="width30" for="processQuantity">Actual Quantity</label> <input
													type="text" id="processQuantity_${status3.index+1}" name="processQuantity"
													value="${wkdetail.productionQuantity}"
													class="width50 processfQuantity validate[optional,custom[number]]" />
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="portlet-content class90" id="hrm"
										style="margin-top: 10px;">
										<div id="line-error"
											class="response-msg error ui-corner-all width90"
											style="display: none;"></div> 
										<div id="hrm" class="hastable width100">
											<table id="hastab" class="width100">
												<thead>
													<tr>
														<th class="width10">Product</th>
														<th class="width10">Store</th>
														<th style="width: 8%;">Packaging</th> 
														<th class="width5">Quantity</th>
														<th class="width5">Base Qty</th> 
														<th class="width5">Wastage Qty</th> 
														<th class="width10">Description</th>
													</tr>
												</thead>
												<tbody class="tab">
													<c:forEach var="DETAIL"
														items="${wkdetail.workinProcessDetailVOs}" varStatus="status4">
														<tr class="rowid${status1.index+1}" id="fieldrow_${status3.index+1}${status4.index+1}">
															<td style="display: none;" id="lineId_${status3.index+1}${status4.index+1}">${status4.index+1}</td>
															<td><input type="hidden" name="productId"
																id="productid_${status3.index+1}${status4.index+1}"
																value="${DETAIL.product.productId}" /> <span
																id="product_${status3.index+1}${status4.index+1}">${DETAIL.product.productName}
																	[${DETAIL.product.code}]</span>
																<span class="width10 float-left" id="unitCode_${status3.index+1}${status4.index+1}"
																	style="position: relative;">${DETAIL.product.lookupDetailByProductUnit.accessCode}</span>
															</td>
															<td><span class="float-left width60"
																id="storeNameWork_${status3.index+1}${status4.index+1}">${DETAIL.storeName}</span> <input
																type="hidden" name="shelfId" id="shelfId_${status3.index+1}${status4.index+1}"
																value="${DETAIL.shelf.shelfId}" />  
																<span class="button float-right"> <a
																	style="cursor: pointer;" id="prodIDStr_${status3.index+1}${status4.index+1}"
																	class="btn ui-state-default ui-corner-all workinprocessdetail-store-popup width100">
																		<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
															</td>
															<td>
																<select name="packageType" id="packageType_${status3.index+1}${status4.index+1}" class="packageType">
																	<option value="-1">Select</option>
																	<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
																		<c:choose>
																			<c:when test="${packageType.productPackageId gt 0}">
																				<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																				 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																				 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																				 	</c:forEach> 
																				 </optgroup>
																			</c:when> 
																		</c:choose> 
																	</c:forEach>
																</select>
																<input type="hidden" id="temppackageType_${status3.index+1}${status4.index+1}" value="${DETAIL.packageDetailId}"/>
															 </td>
															<td><input type="text"
																class="width96 packageUnit validate[optional,custom[number]]"
																id="packageUnit_${status3.index+1}${status4.index+1}" value="${DETAIL.packageUnit}" /> 
															</td> 
															<td><input type="hidden" name="issueQuantity"
																id="issueQuantity_${status3.index+1}${status4.index+1}"
																value="${DETAIL.baseQuantity}"
																class="issueQuantity validate[optional,custom[number]] width80">
																<input type="hidden" name="actualQuantity"
																id="actualQuantity_${status3.index+1}${status4.index+1}" class="actualQuantity">
																<span id="baseUnitConversion_${status3.index+1}${status4.index+1}" style="display: none;"
																	class="width10">${DETAIL.baseUnitName}</span>
																<span id="baseDisplayQty_${status3.index+1}${status4.index+1}">${DETAIL.baseQuantity}</span>
															</td>
															<td>
																<input type="text" name="typicalWastage"
																	id="typicalWastage_${status3.index+1}${status4.index+1}" value="${DETAIL.wastageQuantityStr}"
																	class="typicalWastage validate[optional,custom[number]] width80">
															</td> 
															<td><input type="text" name="description" value="${DETAIL.description}"
																id="description_${status3.index+1}${status4.index+1}"
																class="description width80">
																<input type="hidden" name="workinProcessDetailId" value="${DETAIL.workinProcessDetailId}"
																id="workinProcessDetailId_${status3.index+1}${status4.index+1}"
																class="workinProcessDetailId width80">
															</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div> 
									</div>
								</fieldset>
								<div class="clearfix"></div>
							</c:forEach>
						</c:otherwise>
					</c:choose> 
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="workinprocess_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>  
				<c:choose>
					<c:when test="${WORKIN_PROCESS.workinProcessId ne null && WORKIN_PROCESS.status == requestScope.addFinishedGoods}">
						<div class="portlet-header ui-widget-header float-right addfinishedgoods workinprocess_save"
							id="addfinishedgoods_${requestScope.addFinishedGoods}" style="cursor: pointer;">
							Add Finished Goods
						</div>
						<div class="portlet-header ui-widget-header float-right"
							id="issuegoods_${requestScope.issueStatus}" style="cursor: pointer; opacity: 0.5!important;">
							Issue
						</div>
					</c:when> 
					<c:when test="${WORKIN_PROCESS.workinProcessId ne null && WORKIN_PROCESS.status == requestScope.issueStatus}">
						<div class="portlet-header ui-widget-header float-right addfinishedgoods workinprocess_save"
							id="addfinishedgoods_${requestScope.addFinishedGoods}" style="cursor: pointer;">
							Add Finished Goods
						</div>
						<div class="portlet-header ui-widget-header float-right issuegoods workinprocess_save"
							id="issuegoods_${requestScope.issueStatus}" style="cursor: pointer;">
							Issue
						</div>  
					</c:when> 
					<c:otherwise>
						<div class="portlet-header ui-widget-header float-right"
							id="addfinishedgoods_${requestScope.addFinishedGoods}" style="cursor: pointer; opacity: 0.5!important;">
							Add Finished Goods
						</div>
						<div class="portlet-header ui-widget-header float-right issuegoods workinprocess_save"
							id="issuegoods_${requestScope.issueStatus}" style="cursor: pointer;">
							Issue
						</div> 
					</c:otherwise>
				</c:choose>  
			</div> 
			<div class="clearfix"></div> 
			<div id="temp-result" style="display: none;"></div>
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>