 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/contextMenu.js"></script>
 <style type="text/css">
table.display td{
	padding:7px 10px;
}
</style>
<script type="text/javascript"> 
var directPaymentId=0; 
var oTable; var selectRow=""; var aSelected = []; var aData="";
$(function(){
	$('.formError').remove(); 
	$('#DirectPaymentRT').dataTable({ 
		"sAjaxSource": "getdirectpayment_jsonlist.action",
	    "sPaginationType": "full_numbers",
	    "bJQueryUI": true, 
	    "iDisplayLength": 25,
		"aoColumns": [
			{ "sTitle": "Direct Payment", "bVisible": false},
			{ "sTitle": "Payment NO."},
			{ "sTitle": "Date"}, 
			{ "sTitle": "Payment Mode"}, 
			{ "sTitle": "Amount"},
			{ "sTitle": "Payable"}, 
			{ "sTitle": "Bank Name"}, 
			{ "sTitle": "Account NO."},  
			{ "sTitle": "Cheque NO."},
			{ "sTitle": "Cheque Date"},
		], 
		"sScrollY": $("#main-content").height() - 190,
		//"bPaginate": false,
		"aaSorting": [[1, 'desc']], 
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			if (jQuery.inArray(aData.DT_RowId, aSelected) !== -1) {
				$(nRow).addClass('row_selected');
			}
		}
	});	 

	$('.print-call').click(function(){  
   		// HTML PRINT  
   		if(directPaymentId>0){ 
    		window.open('direct_payment_printview.action?directPaymentId='+ directPaymentId +'&format=HTML', '',
						'width=1000,height=800,scrollbars=yes,left=100px');  
   	   	}else{
			$('#error_message').hide().html("Please select a record to print.").slideDown();
			$('#error_message').delay(3000).hide();
			return false;
		}
	}); 

	$(".xls-download-call").click(function(){ 
		window.open('<%=request.getContextPath()%>/direct_payment_excel_report.action?recordId='+directPaymentId,
				'_blank','width=100,height=100'); 
	}); 

	//init datatable
	oTable = $('#DirectPaymentRT').dataTable();
	 
	/* Click event handler */
	$('#DirectPaymentRT tbody tr').live('click', function () {  
		  if ( $(this).hasClass('row_selected') ) {
			  $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this );
	          directPaymentId=aData[0];  
	      }
	      else {
	          oTable.$('tr.row_selected').removeClass('row_selected');
	          $(this).addClass('row_selected');
	          aData =oTable.fnGetData( this ); 
	          directPaymentId=aData[0];  
	      }
	});
});
</script>
<div id="main-content">
	<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header"><span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span><fmt:message key="accounts.directpayment.directpaymentinformation"/></div>	 	 
		<div class="portlet-content"> 
			<div id="success_message" class="response-msg success ui-corner-all" style="width:90%; display:none;"></div>
			<div id="error_message" class="response-msg error ui-corner-all" style="width:90%; display:none;"></div> 
	   	    <div class="tempresult" style="display:none;"></div> 
		 	<div id="direct_payment_list"> 
				<table class="display" id="DirectPaymentRT"></table> 
			</div> 
		</div>
	</div>
</div>
<div class="process_buttons">
		<div id="hrm" class="width100 float-right ">
		  	<div class="width5 float-right height30" title="Download as PDF"><img width="30" height="30" src="images/pdf_icon.png" class="pdf-download-call" style="cursor:pointer;"/></div>
		 	<div class="width5 float-right height30" title="Download as XLS"><img width="30" height="30" src="images/xls_icon.png" class="xls-download-call" style="cursor:pointer;"/></div>
		  	<div class="width5 float-right height30" title="Print"><img width="30" height="30" src="images/print_icon.png" class="print-call" style="cursor:pointer;"/></div>
		 </div>
</div> 