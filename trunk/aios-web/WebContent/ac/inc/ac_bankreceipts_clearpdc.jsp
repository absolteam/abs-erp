<%@page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/dateDifference.js"></script>
<script type="text/javascript">
$(function(){ 
     $('#receipt_discard').click(function(){
		 $.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_receipts_voucher.action", 
		 	async: false,
		    dataType: "html",
		    cache: false,
			success:function(result){
				$('#common-popup').dialog('destroy');		
				$('#common-popup').remove();  
				$('#codecombination-popup').dialog('destroy');		
				$('#codecombination-popup').remove(); 
				$("#main-wrapper").html(result);  
				return false;
			}
		 }); 
	});
     
     $('#receipt_clear').click(function(){  
 		var bankReceiptsDetailId=Number($('#bankReceiptsDetailId').val());  
 		$.ajax({
 			type:"POST",
 			url:"<%=request.getContextPath()%>/bank_receipt_pdc_clearance.action", 
 		 	async: false,
 		 	data:{bankReceiptsDetailId: bankReceiptsDetailId},
 		    dataType: "json",
 		    cache: false,
 			success:function(response){
 				if(response.returnMessage == "SUCCESS"){ 
 					 $('#receipt_discard').trigger('click');
 				}else{
 					$('#page-error').hide().html(
 					"Error, please try again.").slideDown(
 							1000);
 					$('#page-error').delay(
 							2000).slideUp();
 					return false;
 				}  
 			}
 		 }); 
 	}); 
}); 	 
function getRowId(id) {
	var idval = id.split('_');
	var rowId = Number(idval[1]);
	return rowId;
}
	 
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span class="toggle-div ui-icon ui-icon-circle-arrow-s"></span>
			receipts cheque clearance
		</div> 
		<form name="bankReceiptsForm" id="bankReceiptsForm" style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all"
					style="display: none; position: fixed; right: 10px; top: 40px; width: 200px;"></div>
				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="width48 float-right">
						<input type="hidden" name="alertId" id="alertId"
							value="${alertId}" />
						<input type="hidden" name="useCaseName" id="useCaseName"
							value="${BANK_RECEIPTS.useCase}" />
						<input type="hidden" name="useCaseId" id="useCaseId"
							value="${BANK_RECEIPTS.recordId}" />
						<fieldset>
							<div>
								<label class="width20" for="payeeType">Receipts Source<span
									class="mandatory">*</span> </label>  
								<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.receiptSourceType}"
											id="supplierName" class="width50" readonly="readonly" />
							</div>
							<div>
								<label class="width20">Receiver</label> 
									<input type="text" name="supplierName"
											value="${BANK_RECEIPTS.receiptSourceName}"
											id="supplierName" class="width50" readonly="readonly" /> 
							</div>
							<div>
								<label class="width20">Account Code<span
									class="mandatory">*</span> </label> <input type="hidden"
									name="combinationId" id="codeCombinationId"
									value="${BANK_RECEIPTS.combination.combinationId}" />
								<c:choose>
									<c:when
										test="${BANK_RECEIPTS.combination.combinationId ne null && BANK_RECEIPTS.combination.combinationId ne ''}">
										<c:choose>
											<c:when
												test="${BANK_RECEIPTS.combination.accountByAnalysisAccountId ne null  &&
														 BANK_RECEIPTS.combination.accountByAnalysisAccountId ne ''}">
												<input type="text" name="codeCombination"
													readonly="readonly" id="codeCombination"
													value="${BANK_RECEIPTS.combination.accountByAnalysisAccountId.code}[${BANK_RECEIPTS.combination.accountByAnalysisAccountId.account}]"
													class="codeCombination width50 ">
											</c:when>
											<c:otherwise>
												<input type="text" name="codeCombination"
													readonly="readonly" id="codeCombination"
													value="${BANK_RECEIPTS.combination.accountByNaturalAccountId.code}[${BANK_RECEIPTS.combination.accountByNaturalAccountId.account}]"
													class="codeCombination width50 ">
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<input type="text" name="codeCombination" readonly="readonly"
											id="codeCombination"
											class="codeCombination width50 ">
									</c:otherwise>
								</c:choose>
								 
							</div>
							<div>
								<label class="width20">Description</label>
								<textarea rows="2" id="description" class="width50 description">${BANK_RECEIPTS.description}</textarea>
							</div>
						</fieldset>
					</div>
					<div class="width50 float-left">
						<fieldset>
							<div>
								<label class="width20" for="receiptsNo">Receipt Number<span
									class="mandatory">*</span> </label> <input type="hidden"
									name="bankReceiptsId" id="bankReceiptsId"
									value="${BANK_RECEIPTS.bankReceiptsId}" />
								 <input type="text" readonly="readonly" name="receiptsNo"
											id="receiptsNo" class="width50 "
											value="${BANK_RECEIPTS.receiptsNo}" />
							</div>
							<div>
								<label class="width20" for="receiptsDate">Date<span
									class="mandatory">*</span> </label> 
									<input type="text" readonly="readonly" name="receiptsDate" value="${BANK_RECEIPTS.strReceiptsDate}"
											id="receiptsDate" class="width50 " /> 
							</div>
							<div>
								<label class="width20" for="receiptsTypeId">Receipts
									Type<span class="mandatory">*</span> </label> 
										<input type="text" readonly="readonly" name="receiptType" value="${BANK_RECEIPTS.receiptType}"
											id="receiptType" class="width50 " /> 
							</div>
							<div>
								<label class="width20" for="currencyId">Currency<span
									class="mandatory">*</span> </label> 
								<input type="text" readonly="readonly" name="currencyName" value="${BANK_RECEIPTS.currencyName}"
											id="currencyName" class="width50 " /> 
							</div>
							<div>
								<label class="width20" for="referenceNo">Ref.No</label> <input
									type="text" id="referenceNo" readonly="readonly"
									value="${BANK_RECEIPTS.referenceNo}" class="width50" />
							</div>
						</fieldset>
					</div>
				</div>
			</div>
 			 
			<div class="clearfix"></div>
			<div class="portlet-content" style="margin-top: 10px;" id="hrm">
				<fieldset>
					<legend>Receipts Detail<span
									class="mandatory">*</span></legend>
					<div id="line-error"
						class="response-msg error ui-corner-all width90"
						style="display: none;"></div>
					<div id="warning_message"
						class="response-msg notice ui-corner-all width90"
						style="display: none;"></div>
					<div id="hrm" class="hastable width100">
						<table id="hastab" class="width100">
							<thead>
								<tr>
									<th style="width: 5%">Payment Mode</th>
									<th style="width: 5%">Institution Name</th>
									<th style="width: 5%">Instrument No</th>
									<th style="width: 5%">Instrument Date</th>
 									<th style="width: 5%">Amount</th>
 									<th style="width: 5%">Description</th> 
								</tr>
							</thead>
							<tbody class="tab"> 
								<tr class="rowid" id="fieldrow_${status.index+1}">
									<td id="lineId_${status.index+1}" style="display: none;">${status.index+1}</td>
									<td> 
										${BANK_RECEIPTS_DETAIL.paymentModeName}
									</td>
									<td>
										${BANK_RECEIPTS_DETAIL.institutionName}
									</td>
									<td> 
										${BANK_RECEIPTS_DETAIL.bankRefNo}
									</td>
									<td>
										${BANK_RECEIPTS_DETAIL.strChequeDate}
									</td>  
									<td>
										${BANK_RECEIPTS_DETAIL.amount}
									</td> 
									<td>
										${BANK_RECEIPTS_DETAIL.description}
									</td>
									<td style="display: none;"><input type="hidden"
										id="bankReceiptsDetailId"
										value="${BANK_RECEIPTS_DETAIL.bankReceiptsDetailId}" />  
									</td> 
								</tr> 
							</tbody>
						</table>
					</div>
				</fieldset>
			</div>
			<div class="clearfix"></div>
			<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right " style="margin:10px;"> 
				<div class="portlet-header ui-widget-header float-right" id="receipt_discard" style="cursor:pointer;"><fmt:message key="accounts.common.button.discard"/></div>  
				<div class="portlet-header ui-widget-header float-right" id="receipt_clear" style="cursor:pointer;">Clear Receipt</div>
			</div>  
		</form>
	</div>
</div>