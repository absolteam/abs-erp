<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="portlet-content">
	<span>Workin Process Detail</span>
	<c:forEach var="wkdetail" items="${PRODUCTION_REQUISITION_DETAIL}" varStatus="status1">
		<fieldset>
			<c:choose>
				<c:when
					test="${wkdetail.materialIdleMixDetailVOs ne null && fn:length(wkdetail.materialIdleMixDetailVOs)>0}">
					<input type="checkbox" id="processRequisition_${status1.index+1}" checked="checked" class="processRequisitionMode"/>
				</c:when>
				<c:otherwise>
					<input type="checkbox" id="processRequisition_${status1.index+1}" disabled="disabled" class="processRequisitionMode"/>
				</c:otherwise>
			</c:choose>
			<div class="width100 float-left" id="hrm">
				<div class="float-left width48">
					<div>
						<label class="width30"> Finished Product<span
							style="color: red;">*</span> </label> <input type="text"
							readonly="readonly" id="finishedProduct_${status1.index+1}" name="finishedProduct"
							value="${wkdetail.product.productName}" class="width50" /> <input
							type="hidden" id="finishedProductId_${status1.index+1}"
							value="${wkdetail.product.productId}" />
							<input type="hidden" id="workinProcessProductionId_${status1.index+1}"/>
							<input type="hidden" id="productionRequisitionDetailId_${status1.index+1}" value="${wkdetail.productionRequisitionDetailId}"/>
					</div>
					<div>
						<label class="width30"> Shelf<span style="color: red;">*</span>
						</label> <input type="text" readonly="readonly" id="shelfNameWork_${status1.index+1}"
							name="shelfNameWork" value="${wkdetail.storeName}"
							class="width50" /> <input type="hidden" id="shelfId_${status1.index+1}"
							value="${wkdetail.shelf.shelfId}" />
						<c:if test="${wkdetail.shelf eq null}">
							<span class="button"> <a style="cursor: pointer;"
								id="workinprocessstorepopup_${status1.index+1}"
								class="btn ui-state-default ui-corner-all workinprocess-store-popup-production width100">
									<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
						</c:if>
					</div>
					<div>
						<label class="width30" for="processQuantity">Quantity</label> <input
							type="text" id="processQuantity_${status1.index+1}" name="processQuantity"
							value="${wkdetail.quantity}"
							class="width50 processfQuantity validate[optional,custom[number]]" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="portlet-content class90" id="hrm"
				style="margin-top: 10px;">
				<div id="line-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<c:choose>
					<c:when
						test="${wkdetail.materialIdleMixDetailVOs ne null && fn:length(wkdetail.materialIdleMixDetailVOs)>0}">
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th class="width10">Store</th>
										<th style="width: 8%;">Package Type</th> 
										<th style="display: none;">Package Unit</th>
										<th class="width5">Quantity</th>
										<th class="width10">Description</th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${wkdetail.materialIdleMixDetailVOs}" varStatus="status">
										<c:set var="backgrd" value="#fff"/>
										<c:if test="${DETAIL.insufficientBalance eq false}">
											<c:set var="backgrd" value="#F3F781"/></c:if>
										<tr class="rowid${status1.index+1} rowidtmp" id="fieldrow_${status1.index+1}${status.index+1}" style="background-color:${backgrd}">
											<td style="display: none;" id="lineId_${status1.index+1}${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status1.index+1}${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status1.index+1}${status.index+1}">${DETAIL.product.productName}
													[${DETAIL.product.code}]</span></td>
											<td><span class="float-left width60"
												id="storeNameWork_${status1.index+1}${status.index+1}">${DETAIL.storeName}</span> <input
												type="hidden" name="shelfId" id="shelfId_${status1.index+1}${status.index+1}"
												value="${DETAIL.shelfId}" /> <span class="button float-right"> <a
													style="cursor: pointer;" id="prodIDStr_${status1.index+1}${status.index+1}"
													class="btn ui-state-default ui-corner-all workinprocessdetail-store-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span></td>
											<td>
												<select name="packageType" id="packageType_${status1.index+1}${status.index+1}" class="packageType">
													<option value="">Select</option>
													<c:forEach var="packageType" items="${DETAIL.productPackageVOs}">
														<c:choose>
															<c:when test="${packageType.productPackageId gt 0}">
																<optgroup label="${packageType.packageName}" style="color: #c85f1f;">
																 	<c:forEach var="packageDetailType" items="${packageType.productPackageDetailVOs}">
																 		<option style="margin-left: 10px; color: #000;" value="${packageDetailType.productPackageDetailId}">${packageDetailType.conversionUnitName}</option> 
																 	</c:forEach> 
																 </optgroup>
															</c:when>
															<c:otherwise>
																<option value="${packageType.productPackageId}">${packageType.packageName}</option> 
															</c:otherwise>
														</c:choose> 
													</c:forEach>
												</select>
												<input type="hidden" id="temppackageType_${status1.index+1}${status.index+1}" value="${DETAIL.packageDetailId}"/>
											</td>
											<td><input type="text"
												class="width96 packageUnit validate[optional,custom[number]]"
												id="packageUnit_${status1.index+1}${status.index+1}" value="${DETAIL.packageUnit}" /> 
											</td> 
											<td style="display: none;"><input type="text" name="issueQuantity"
												id="issueQuantity_${status1.index+1}${status.index+1}"
												value="${DETAIL.productionQuantity}"
												class="issueQuantity validate[optional,custom[number]] width80">
												<input type="hidden" name="actualQuantity"
												id="actualQuantity_${status1.index+1}${status.index+1}"
												value="${DETAIL.materialQuantity}" class="actualQuantity">
											</td>
											<td><input type="text" name="description"
												id="description_${status1.index+1}${status.index+1}"
												class="description width80">
												<input type="hidden" name="workinProcessDetailId"
												id="workinProcessDetailId_${status1.index+1}${status.index+1}"
												class="workinProcessDetailId width80">
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</c:when>
					<c:otherwise>
						<span style="color: #ca1e1e;">Material Idle Mix not found
							for this product.</span>
					</c:otherwise>
				</c:choose>
			</div>
		</fieldset>
	</c:forEach>
</div>
<script type="text/javascript">
$(function(){
 	$('.rowidtmp').each(function(){ 
		var rowId = getRowId($(this).attr('id'));  
  		$('#packageType_'+rowId).val($('#temppackageType_'+rowId).val());
	});
});
</script>