<%@ page import="com.aiotech.aios.common.util.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jsforAjaxPage.js"></script>
<c:if test="${param['lang'] != null}">
	<fmt:setLocale value="${param['lang']}" scope="session" />
</c:if>
<script type="text/javascript">
var slidetab="";
var requisitionDetails= [];
var tempid = ""; var accessCode = "";
var selfFlag=false;
$(function(){   
	manupulateLastRow();
	$jquery("#requisitionValidation").validationEngine('attach'); 
	
	var requisitionId = Number($('#requisitionId').val());   
	 $('#requisition_document_information').click(function(){
			if(requisitionId>0){
				AIOS_Uploader.openFileUploader("doc","requisitionDocs","Requisition",requisitionId,"RequisitionDocuments");
			}else{
				AIOS_Uploader.openFileUploader("doc","requisitionDocs","Requisition","-1","RequisitionDocuments");
			}
		});
	 
	 if(requisitionId>0)
		populateUploadsPane("doc","requisitionDocs","Requisition",requisitionId);	

	
	selfFlag='${requestScope.selfFlag}';
	$('.requisition-product-popup').live('click',function(){  
		$('.ui-dialog-titlebar').remove();   
		var rowId = getRowId($(this).attr('id'));  
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/show_requisition_product.action", 
		 	async: false,   
		 	data: {id: rowId},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
	
	$('.common-person-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();  
		var personTypes="1";
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_person_list.action", 
		 	async: false,  
		 	data: {personTypes: personTypes},
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});

 	$('.common-department-popup').click(function(){  
		$('.ui-dialog-titlebar').remove();   
  		$.ajax({
			type:"POST",
			url:"<%=request.getContextPath()%>/common_departmentbranch_list.action", 
		 	async: false,   
		    dataType: "html",
		    cache: false,
			success:function(result){  
				 $('.common-result').html(result);  
				 $('#common-popup').dialog('open'); 
				 $(
							$(
									$('#common-popup')
											.parent())
									.get(0)).css('top',
							0);
				 return false;
			},
			error:function(result){   
				 $('.common-result').html(result); 
			}
		});  
		return false;
	});
 	
 	$('#common-popup').dialog({
		 autoOpen: false,
		 minwidth: 'auto',
		 width:800, 
		 bgiframe: false,
		 overflow:'hidden',
		 modal: true 
	});
 	
 	$('.addrows').click(function(){ 
		  var i=Number(1); 
		  var id=Number(getRowId($(".tab>.rowid:last").attr('id'))); 
		  id=id+1;  
		  $.ajax({
				type:"POST",
				url:"<%=request.getContextPath()%>/requisition_addrow.action", 
			 	async: false,
			 	data:{rowId: id},
			    dataType: "html",
			    cache: false,
				success:function(result){ 
					$('.tab tr:last').before(result);
					 if($(".tab").height()>255)
						 $(".tab").css({"overflow-x":"hidden","overflow-y":"auto"}); 
					 $('.rowid').each(function(){   
			    		 var rowId=getRowId($(this).attr('id')); 
			    		 $('#lineId_'+rowId).html(i);
						 i=i+1; 
			 		 }); 
				}
			});
		  return false;
	 });
 	
 	 $('#requisition_discard').click(function(event){  
 		requisitionDiscard("");
		return false;
	 });
 	 
 	 $('#idlemix_save').click(function(){
 		requisitionDetails = [];
		 if($jquery("#requisitionValidation").validationEngine('validate')){
 		 		
	 			var referenceNumber = $('#referenceNumber').val(); 
 	 			var locationId = Number($('#locationId').val()); 
	 			var personId = Number($('#personId').val());   
	 			var requisitionId = Number($('#requisitionId').val());   
	 			var description=$('#description').val();  
	 			var requisitionDate = $('#requisitionDate').val();  
	 			requisitionDetails = getRequisitionDetails();  
	 			 
	   			if(requisitionDetails!=null && requisitionDetails!=""){
	 				$.ajax({
	 					type:"POST",
	 					url:"<%=request.getContextPath()%>/requisition_save.action", 
	 				 	async: false, 
	 				 	data:{	requisitionId: requisitionId, referenceNumber: referenceNumber, locationId: locationId, personId: personId,
	 				 			requisitionDate: requisitionDate, description: description, requisitionDetails: JSON.stringify(requisitionDetails)
	 					 	 },
	 				    dataType: "json",
	 				    cache: false,
	 					success:function(response){   
	 						 if(($.trim(response.returnMessage)=="SUCCESS")) 
	 							requisitionDiscard(requisitionId > 0 ? "Record updated.":"Record created.");
	 						 else{
	 							 $('#page-error').hide().html(response.returnMessage).slideDown();
	 							 $('#page-error').delay(2000).slideUp();
	 							 return false;
	 						 }
	 					} 
	 				}); 
	 			}else{
	 				$('#page-error').hide().html("Please enter requisition details.").slideDown();
	 				$('#page-error').delay(2000).slideUp();
	 				return false;
	 			}
	 		}else{
	 			return false;
	 		}
	 	}); 
 	 
 	  var getRequisitionDetails = function(){
 		 requisitionDetails = []; 
			$('.rowid').each(function(){ 
				var rowId = getRowId($(this).attr('id'));  
				var productId = Number($('#productid_'+rowId).val());  
				var quantity = Number($('#quantity_'+rowId).val()); 
				var description = $('#description_'+rowId).val();  
				var requisitionDetailId =  Number($('#requisitionDetailId_'+rowId).val()); 
				if(typeof productId != 'undefined' && productId > 0 && quantity > 0){
					var jsonData = [];
					jsonData.push({
						"productId" : productId,
						"quantity" : quantity, 
						"description" : description, 
						"requisitionDetailId": requisitionDetailId
					});   
					requisitionDetails.push({
						"requisitionDetails" : jsonData
					});
				} 
			});  
			return requisitionDetails;
	 }; 
	 
	 $('.delrow').live('click',function(){ 
		 slidetab=$(this).parent().parent().get(0);   
      	 $(slidetab).remove();  
      	 var i=1;
	   	 $('.rowid').each(function(){   
	   		 var rowId=getRowId($(this).attr('id')); 
	   		 $('#lineId_'+rowId).html(i);
			 i=i+1; 
		 });  
		 return false; 
	});
 	 
 	$('#person-list-close').live('click',function(){
		 $('#common-popup').dialog('close');
	 });
 	
 	$('.quantity').live('change',function(){
 		var rowId = getRowId($(this).attr('id'));  
 		triggerAddRow(rowId);
 		return false;
	 });
	
	{
		if(Number('${REQUISITION.requisitionId}')>0){
			$('#requisitionDate').datepick();
		}else{
			$('#requisitionDate').datepick({ 
			    defaultDate: '0', selectDefaultDate: true, showTrigger: '#calImg'});
		} 
	}
	
});	

function getRowId(id){   
	var idval=id.split('_');
	var rowId=Number(idval[1]);
	return rowId;
}
function manupulateLastRow(){
	var hiddenSize=0;
	$($(".tab>tr:first").children()).each(function(){
		if($(this).is(':hidden')){
			++hiddenSize;
		}
	});
	var tdSize=$($(".tab>tr:first").children()).size();
	var actualSize=Number(tdSize-hiddenSize);  
	
	$('.tab>tr:last').removeAttr('id');  
	$('.tab>tr:last').removeClass('rowid').addClass('lastrow');  
	$($('.tab>tr:last').children()).remove();  
	for(var i=0;i<actualSize;i++){
		$('.tab>tr:last').append("<td style=\"height:25px;\"></td>");
	} 
}

function departmentBranchPopupResult(locationId,locationName){
	$('#locationId').val(locationId);
	$('#locationName').val(locationName);
}

function personPopupResult(personid, personname, commonParam) {
	$('#personId').val(personid);
	$('#personName').val(personname);
	$('#common-popup').dialog("close");
}

function requisitionProductPopup(aData, rowId) { 
	$('#productid_' + rowId).val(aData.productId);
	$('#product_' + rowId).html(aData.productName+" ["+aData.code+"]"); 
	triggerAddRow(rowId);
	return false;
}

function triggerAddRow(rowId){   
	var productid = Number($('#productid_'+rowId).val());  
	var productQty = Number($('#quantity_'+rowId).val());  
	var nexttab=$('#fieldrow_'+rowId).next();  
	if(productid > 0
			&& productQty > 0
			&& $(nexttab).hasClass('lastrow')){ 
		$('#DeleteImage_'+rowId).show();
		$('.addrows').trigger('click');
	} 
	return false;
}
function requisitionDiscard(message){
	var url="<%=request.getContextPath()%>/show_requisition_list.action";
	if(selfFlag=='true'){
		url="<%=request.getContextPath()%>/show_requisition_list_self.action";

	}
	 $.ajax({
		type:"POST",
		url:url,
		async : false,
		dataType : "html",
		cache : false,
		success : function(result) {
			$('#common-popup').dialog('destroy');
			$('#common-popup').remove(); 
			$("#main-wrapper").html(result); 
			if (message != null && message != '') {
				$('#success_message').hide().html(message).slideDown(1000);
				$('#success_message').delay(2000).slideUp();
			}
		}
	});
}
</script>
<div id="main-content">
	<div
		class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container">
		<div class="mainhead portlet-header ui-widget-header">
			<span style="display: none;"
				class="toggle-div ui-icon ui-icon-circle-arrow-s"></span> item requisition
		</div> 
		<c:choose>
			<c:when test="${COMMENT_IFNO.commentId ne null && COMMENT_IFNO.commentId ne ''
								&& COMMENT_IFNO.commentId gt 0}">
				<div class="width85 comment-style" id="hrm">
					<fieldset>
						<label class="width10"> Comment From   :<span> ${COMMENT_IFNO.name}</span> </label>
						<label class="width70">${COMMENT_IFNO.comment}</label>
					</fieldset>
				</div> 
			</c:when>
		</c:choose> 
		<form name="requisitionValidation" id="requisitionValidation"
			style="position: relative;">
			<div class="portlet-content">
				<div id="page-error"
					class="response-msg error ui-corner-all width90"
					style="display: none;"></div>
				<input type="hidden" id="requisitionId" value="${REQUISITION.requisitionId}"/>
 				<div class="tempresult" style="display: none;"></div>
				<div class="width100 float-left" id="hrm">
					<div class="float-right  width48">
						<fieldset style="min-height: 100px;"> 
							<div>
								<label class="width30"> Person </label> 
								<c:choose>
									<c:when
									test="${REQUISITION.person ne null && REQUISITION.person ne ''}"> 
										<input type="text" readonly="readonly" id="personName" name="personName" 	
											value="${REQUISITION.personName}" class="width50" />
										<input type="hidden" id="personId" value="${REQUISITION.person.personId}"/>
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="personName" name="personName"  
											value="${requestScope.personName}" class="width50" />
										<input type="hidden" id="personId" value="${requestScope.personId}"/>
									</c:otherwise>
								</c:choose>  
								<span class="button">
									<a style="cursor: pointer;" id="person"
									class="btn ui-state-default ui-corner-all common-person-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
							</div> 
							<div>
								<label class="width30"><fmt:message
										key="accounts.jv.label.description" /> </label>
								<textarea id="description" name="description"
									class="width51">${REQUISITION.description}</textarea>
							</div>
							<div class="width100 float-left">									
							
								<div id="requisition_document_information" class="width90" style="cursor: pointer; color: blue;">
									<u>Upload Documents Here</u>
								</div>
								<div style="padding-top: 10px; margin-top:3px;" class="width90">
									<span id="requisitionDocs"></span>
								</div>
								
							</div>		
						</fieldset>
					</div>
					<div class="float-left width48">
						<fieldset style="min-height: 95px;">
							<div>
								<label class="width30"> Reference No.<span
									style="color: red;">*</span> </label>
								<c:choose>
									<c:when test="${REQUISITION.referenceNumber ne null && REQUISITION.referenceNumber ne ''}">
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${REQUISITION.referenceNumber}" 	class="validate[required] width50" />
									</c:when>
									<c:otherwise>
										<input type="text" readonly="readonly" id="referenceNumber" name="referenceNumber"
											value="${requestScope.referenceNumber}"	class="validate[required] width50" />
									</c:otherwise>
								</c:choose>
							</div> 
							<div>
								<label class="width30">Requisition Date<span
									style="color: red;">*</span> </label>  
								 <input type="text" readonly="readonly" id="requisitionDate" name="requisitionDate"
									value="${REQUISITION.date}" class="validate[required] width50" />
							</div>  
							<div>
								<label class="width30">Location Name<span
									style="color: red;">*</span> </label> <input type="text"
									readonly="readonly" name="locationName" id="locationName"
									class="width50 validate[required]"
									value="${REQUISITION.locationName}" />
								<span class="button">
									<a style="cursor: pointer;" id="location"
									class="btn ui-state-default ui-corner-all common-department-popup width100">
										<span class="ui-icon ui-icon-newwin"> </span> </a> </span> <input
									type="hidden" readonly="readonly" name="locationId"
									id="locationId"
									value="${REQUISITION.cmpDeptLoc.cmpDeptLocId}" />
							</div>
						</fieldset>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="portlet-content class90" id="hrm"
					style="margin-top: 10px;">
					<fieldset>
						<legend>
							Requisition Detail<span class="mandatory">*</span>
						</legend>
						<div id="line-error"
							class="response-msg error ui-corner-all width90"
							style="display: none;"></div>
						<div id="hrm" class="hastable width100">
							<table id="hastab" class="width100">
								<thead>
									<tr>
										<th class="width10">Product</th>
										<th class="width5">Quantity</th>
 										<th class="width5">Description</th>  
										<th style="width: 1%;"><fmt:message
												key="accounts.common.label.options" /></th>
									</tr>
								</thead>
								<tbody class="tab">
									<c:forEach var="DETAIL"
										items="${REQUISITION.requisitionDetailVOs}"
										varStatus="status">
										<tr class="rowid" id="fieldrow_${status.index+1}">
											<td style="display: none;" id="lineId_${status.index+1}">${status.index+1}</td>
											<td><input type="hidden" name="productId"
												id="productid_${status.index+1}"
												value="${DETAIL.product.productId}" /> <span
												id="product_${status.index+1}">${DETAIL.product.productName} [${DETAIL.product.code}]</span>
												<span class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${status.index+1}"
													class="btn ui-state-default ui-corner-all requisition-product-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="text" name="quantity"
												id="quantity_${status.index+1}" value="${DETAIL.quantity}"
												class="quantity validate[optional,custom[number]] width80">
											</td>  
											<td><input type="text" name="description"
												id="description_${status.index+1}"
												value="${DETAIL.description}" class="description width98"/>
											</td>
											<td style="width: 1%;" class="opn_td"
												id="option_${status.index+1}"><a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${status.index+1}"
												style="cursor: pointer; display: none;" title="Add Record">
													<span class="ui-icon ui-icon-plus"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${status.index+1}"
												style="display: none; cursor: pointer;" title="Edit Record">
													<span class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${status.index+1}" style="cursor: pointer;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${status.index+1}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="requisitionDetailId"
												id="requisitionDetailId_${status.index+1}"
												value="${DETAIL.requisitionDetailId}" />
											</td>
										</tr>
									</c:forEach>
									<c:forEach var="i"
										begin="${fn:length(REQUISITION.requisitionDetailVOs)+1}"
										end="${fn:length(REQUISITION.requisitionDetailVOs)+2}"
										step="1" varStatus="status">

										<tr class="rowid" id="fieldrow_${i}">
											<td style="display: none;" id="lineId_${i}">${i}</td>
											<td><input type="hidden" name="productId"
												id="productid_${i}" /> <span id="product_${i}"></span> <span
												class="button float-right"> <a
													style="cursor: pointer;" id="prodID_${i}"
													class="btn ui-state-default ui-corner-all requisition-product-popup width100">
														<span class="ui-icon ui-icon-newwin"> </span> </a> </span>
											</td> 
											<td><input type="text" name="quantity"
												id="quantity_${i}"
												class="quantity validate[optional,custom[number]] width80">
											</td> 
											<td>
												<input type="text" name="description" id="description_${i}"
												value="${DETAIL.description}" class="description width98"/>
											</td>
											<td style="width: 1%;" class="opn_td" id="option_${i}">
												<a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip addData"
												id="AddImage_${i}" style="cursor: pointer; display: none;"
												title="Add Record"> <span class="ui-icon ui-icon-plus"></span>
											</a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip editData"
												id="EditImage_${i}" style="display: none; cursor: pointer;"
												title="Edit Record"> <span
													class="ui-icon ui-icon-wrench"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip delrow"
												id="DeleteImage_${i}"
												style="cursor: pointer; display: none;"
												title="Delete Record"> <span
													class="ui-icon ui-icon-circle-close"></span> </a> <a
												class="btn_no_text del_btn ui-state-default ui-corner-all tooltip"
												id="WorkingImage_${i}" style="display: none;"
												title="Working"> <span class="processing"></span> </a> <input
												type="hidden" name="requisitionDetailId"
												id="requisitionDetailId_${i}" />
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"></div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-right "
				style="margin: 10px;">
				<div class="portlet-header ui-widget-header float-right discard"
					id="requisition_discard" style="cursor: pointer;">
					<fmt:message key="accounts.common.button.cancel" />
				</div>
			</div>
			<div
				class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all form-container float-left buttons">
				<div class="portlet-header ui-widget-header float-left addrows"
					style="cursor: pointer; display: none;">
					<fmt:message key="accounts.common.button.addrow" />
				</div>
			</div>
			<div class="clearfix"></div> 
			<div
				style="display: none; position: absolute; overflow: auto; z-index: 1007; outline: 0px none; width: 600px !important; top: 116.5px; left: 366.5px;"
				class="ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable width100"
				tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog">
				<div
					class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"
					unselectable="on" style="-moz-user-select: none;">
					<span class="ui-dialog-title" id="ui-dialog-title-dialog"
						unselectable="on" style="-moz-user-select: none;">Dialog
						Title</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all"
						role="button" unselectable="on" style="-moz-user-select: none;"><span
						class="ui-icon ui-icon-closethick" unselectable="on"
						style="-moz-user-select: none;">close</span> </a>
				</div>
				<div id="common-popup" class="ui-dialog-content ui-widget-content"
					style="height: auto; min-height: 48px; width: auto;">
					<div class="common-result width100"></div>
					<div
						class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
						<button type="button" class="ui-state-default ui-corner-all">Ok</button>
						<button type="button" class="ui-state-default ui-corner-all">Cancel</button>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>